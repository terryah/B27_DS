// * Solution *
Electrical="电气";
// * Discipline *
ElecDiscipline="规则";

// **********
// * Signal *
// **********

ElecSignal="信号";
ElecGroupSignal="信号组";
ElecGroundSignal="接地信号";
ElecShieldingSignal="屏蔽信号";
ElecVideoSignal="视频信号";
ElecPowerSignal="电源信号";
ElecCommandSignal="命令信号";
ElecSignalRoute="信号线路";

ElecSignalExtremity="信号端点";
ElecSignalRouteExtremity="信号线路端点"; 
ElecOffSheet="超越图纸连接器";

// **************
// * Wire/Cable *
// **************

ElecWire="线";
ElecWireLight="线";
ElecWireExtremity="线端点";
ElecWireExtremityLight="线端点";
ElecWireGroup="线组";


// **********
// * System *
// **********

ElecSystem="系统";

// *******************
// * Fnct Composants *
// *******************

ElecFnctCon="功能连接器";
ElecFnctEqt="功能设备";

ElecFnctCntPt="接触点";


// ********************
// * Physical Devices *
// ********************

ElecBayArea="海湾区域";

ElecConShell="接线盒";

ElecConPart="连接器零件";

ElecEqtPart="设备零件";

ElecSicCon="单插式连接器";

ElecMicCon="多插式连接器";

ElecStud="接线柱";

ElecIntSplice="内部绞接";

ElecExtSplice="外部绞接";

ElecTermBlock="接线板";

ElecTermStrip="接线条";

ElecContact="接插件";

ElecFillerPlug="填充塞";

ElecBackShell="背板";

// ***********************
// * Device's connectors *
// ***********************

ElecCavity="电气腔";

ElecTermination="电气接线装置";

ElecTerminationCst="电气接线装置（几何约束）";

ElecCavityCnctPt="电气腔连接点";

ElecConnectorCnctPt="电气连接器连接点";

ElecBundleCnctPt="电气束连接点";

ElecBackShellCnctPt="电气背板连接点";

ElecShellCnctPt="电气板连接点";

ElecSupport="电气支持";

// ***********
// * Harness *
// ***********

ElecBundle="束";

ElecBundleLink="束链接";

ElecGeoBundle="几何束";

ElecBundleSegmentC="束段";
ElecBundleSegment="束段";
ElecMultipleBundleSegment="分支束段";
ElecMultiBranchable="多分支";
ElecBranchableC="分支";
ElecBranchable="分支";

ElecTape = "胶带";
ElecCorrugateTube = "波纹管";
ElecProtection = "保护";
ElecBundleSurface="束曲面";

ElecBundleSegmentExtremity="段端点";
ElecBppAttr="束段定位点";
ElecBsfExtremity="束曲面端点";

ElecCurve="柔性曲线";
ElecRouteBody="电气线路几何体";
EhiBundleSegmentBody="几何体";
EhiBundleSegmentLoft="多截面实体";
EhiBundleSegmentRib="肋";

// *************
// * Schematic *
// *************
ElecSchCable="电缆";
ElecSchCableExtremityCtr="电缆端点";
ElecSchWire="线";
ElecSchWireExtremityCtr="线端点";
ElecSchEquipment="设备";
ElecSchSocket="插座";
ElecSchJunctionBox="接线盒";
ElecSchSwitch="开关";
ElecSchPlug="插头";
ElecSchTerminalBoard="接线端子板";
ElecSchConnector="连接器";
ElecSchPinCtr="引脚";
ElecSchWidePinCtr="宽引脚";
ElecSchBusBar		=	"总线";
ElecSchDevice		=	"设备";
ElecSchFuse			=	"熔断器";
ElecSchMatingCtr	=	"接头连接器";
ElecSchAssemblyCtr	=	"装配连接器";
ElecSchGenEquipment			=	"普通设备";
ElecSchGenJunctionBox		=	"普通接线盒";
ElecSchGenContinuityDevice	=	"普通连续设备";
AC3PPower				=		"三相交流电";
AC3PGenerator			=		"三相交流发电机";
AC3PUPS					=		"三相交流 UPS";
AC3PLoad				=		"三相交流加载";
AC3PCapacitor			=		"三相交流电容器";
AC3PZigZag				=		"三相交流电之字形图标";
AC3PTransformer			=		"三相交流变压器";
AC3PReactor				=		"三相交流电抗器";
AC1PEquipment			=		"单相交流设备";
AC1PPower				=		"单相交流电源";
AC1PGenerator			=		"单相交流发电机";
AC1PCapacitor			=		"单相交流电容器";
AC1PTransformer			=		"单相交流变压器";
AC1PReactor				=		"单相交流电抗器";
DCLoad					=		"直流加载";
DCMotor					=		"直流电动机";
DCBattery				=		"直流电池";
DCRectifier				=		"直流整流器";
AC3PBusbar				=		"三相交流总线";
AC1PBusbar				=		"单相交流总线";
AC3PSwitch				=		"三相交流开关";
AC1PSwitch				=		"单相交流开关";
DCSwitch				=		"直流开关";
AC1PFuse				=		"单相交流熔断器";
DCFuse					=		"直流熔断器";
AC3PBreaker				=		"三相交流断路器";
AC1PBreaker				=		"单相交流断路器";
AC3PEquipment			=		"三相交流设备";
AC3PSynchronousMotor	=		"三相交流同步电动机";
AC3PBranchCapacitor		=		"三相交流支路电容器";
AC3PMotor				=		"三相交流电动机";
AC3PWTransformer		=		"三相交流 W 变压器";
AC3PPhaseShiftingTransformer	=	"三相交流移相变压器";
AC3PDuplexReactor		=		"三相交流双工电抗器";
AC3PAutoTransformer		=		"三相交流自动变压器";
AC1PMotor				=		"单相交流电动机";
AC1PBranchCapacitor		=		"单相交流支路电容器";
DCEquipment				=		"直流设备";
DCBusbar				=		"直流总线";
AC3PFuse				=		"三相交流熔断器";
DCBreaker				=		"直流断路器";
DCGenerator			    =		"直流发电机";


// **********
// * Others *
// **********

ElecLogicalContact="逻辑接插件";
ElecLogicalTermination="逻辑引脚";
ElecWireCnx="线连接";
ElecSignalRouteCnx="信号线路连接";
ElecFromToCnx="信号端点连接";
ElecOffSheetCnx="超越图纸连接";
ElecFnctCnx="功能连接";
ElecBundleCnx="束连接";
ElecCnx="连接";
ElecRelationCnx="关系连接";
ElecLogTerm="逻辑接线装置";
ElecMountingEquipment = "安装设备";

// * Properties *
Elec_BendRadius="弯曲半径";
Elec_CATALOG="目录";
Elec_Color="颜色";
Elec_Cutting_Length="剪切长度";
Elec_Diameter="直径";
Elec_External_Reference="外部参考";
Elec_Extra_Length="附加长度";
Elec_Ground_Unicity="接地线唯一性";
Elec_Id_Number="ID 号";
Elec_Is_Disconnect="已断开连接";
Elec_Is_Routable="可布线";
Elec_Is_Multi_Sheet="多页";
Elec_Length="长度";
Elec_Nominal_Part_Num="额定零件编号";
Elec_Nominal_Voltage="额定电压";
Elec_Number="数目";
Elec_Recom_Wire_Type="推荐线";
Elec_Ref_Des="参考指示符";
Elec_Routing_Priority="传递优先级";
Elec_Section="截面";
Elec_Signal_Section="截面";
Elec_Sep_Code="分隔代码";
Elec_Shielding_Term="屏蔽接线装置";
Elec_Signal_IO="信号 I/O";
Elec_Signal_Unicity="信号唯一性";
Elec_Sub_Type="子类型";
Elec_Joint_Type = "接头类型";
Elec_User_Desc="用户描述";
Elec_User_Name="函数";
Elec_V4PARTNUM="V4 零件编号";
Elec_V4LIBRARY="V4 库";
Elec_V4FAMILY="V4 系列";
Elec_Ws_Ref="Ws_Ref";
Elec_X_Position="位置 X";
Elec_Y_Position="位置 Y";
Elec_Z_Position="位置 Z";
Elec_Line_Weight="质量";
Elec_Segreg="分隔代码";
Elec_Bend_Radius="弯曲半径";
Elec_Creation_Mode="创建模式";
Elec_Di_SlackOUT="分布式松弛";
Elec_Di_Slack="分布式松弛";
Elec_LengthOUT="长度";
Elec_Slack="本地松弛";
Elec_Name="名称";
Elec_Zone="电气带";
Elec_ListPhysical = "列出物理实现信号";
Elec_Signal = "电线所实现的电气信号";
Elec_FullConnected = "完全连接";
Elec_External_Diameter ="外径";
Elec_Continuity	=	"连续";
Elec_Offset = "偏移";
Elec_Route_Validation = "经验证的线路";
Elec_Separation_Distance = "间距";
3DExtr = "3D 端点";
Elec_IsRouted = "已布线";
Elec_IsNetworkConnex = "网络连接性";
Elec_Bend_Radius_OK = "弯曲半径正确";
Elec_Inner_Diameter = "内径";
Elec_Line_Type = "线型";
Elec_Thickness = "线宽";
Elec_Bend_Radius_Delta = "弯曲半径增量";
Elec_Covering_Length = "交叠长度";
Elec_Number_Layer = "编号图层";
Elec_Tape_Thickness = "胶带厚度";
Elec_Tape_Width= "胶带宽度" ;
Elec_Taping_Angle = "包绕角度" ;
Elec_Total_Tape_Length = "胶带总长度" ;
Elec_Tape_Length = "胶带总长度" ;
Elec_Total_Thickness = "总线宽" ;
Elec_Bend_Radius_Protection_OK ="弯曲半径保护成功";
Elec_FromDevice	= "源设备";
Elec_ToDevice	= "目标设备";
Elec_FromConnectionPoint =	"源连接点";
Elec_ToConnectionPoint	 =	"目标连接点";
Elec_Signal_Id = "信号 ID ";
Elec_Ref_PartNumber = "参考零件编号";
IsSingleInBundleSegment = "至少在一个束段中单一";
IncludedWires = "包含线列表";
CoveredBundleSegments = "已覆盖的束段列表";
Elec_WireGroup_Length = "线组长度";
Elec_Wire_Length_Coeff = "线和线组长度之间的系数";
Elec_WireGroup_Type = "线组类型";
Elec_Parent = "父级";
Elec_Min_Bend_Radius= "最小弯曲半径";
NumberOfConnectedBundles="已连接至指定端点的束数";
NumberOfConnectedDevices="已连接至束段的设备数";
Elec_FromContactPartNumber="与该线第一端点相连的腔上所连接插件的零件编号值";
Elec_ToContactPartNumber="与该线第二端点相连的腔上所连接插件的零件编号值";
Elec_Computed_BendRadius="计算所得曲线半径";
Elec_Implements="实施";

// ***********
// * Formboard *
// ***********
Elec_Fmbd_Modify      = "同步";
Elec_True_Length      = "设计长度";
Elec_LT_LengthOUT     = "扁平长度";

Elec_Length_Tolerance_Apply   = "长度公差";
Elec_Tolerance_Value          = "公差值";

Elec_ProtectionLevel = "保护级别";

//ayz
IsGeoAndElecConnectionSame = "电气连接点位于相同的几何坐标吗";

Elec_Text = "添加到保护的文本";
RoutedThrough  = "列出线路段部分";
Elec_Preferred_Length = "首选长度";
Elec_Flexibility = "灵活性";
Elec_Annotation = "标注";

// start cvv 07:01:09
Elec_ConstructionMode = "分支创建模式";
Elec_BranchableSlack = "分支松弛";
Elec_LocalSlack = "添加在分支端点上的本地松弛";
SpecificationId = "规格中所链接对象的 ID";
SpecifiedConnectedId = "规格中所连接对象的 ID";
SpecifiedConnectedPartNumber = "规格中匹配对象的零件编号";
ConnectedObjects = "连接到此连接器的对象";
BendRadius_OK = "弯曲半径正确吗";
IsElectricalCurve = "中心曲线是电曲线吗";
IsNotCollapsed = "分支不能折叠吗";
IsResultOK = "分支正确吗";
// end cvv 07:01:09

// INTERNAL PROTECTIONS
ElecInternalProtection = "内部保护";
EleRefInternalProtection = "内部保护";
EleRefInternalTapeProtection = "内部胶带保护";
EleRefInternalFixedProtection = "内部固定保护";
EleRefInternalCorrugatedProtection = "内部波纹保护";
EleRefInternalAdaptiveProtection = "内部适应性保护";
ElecAdaptiveProtection = "内部适应性保护";
ElecFixedProtection = "内部固定保护";
ElecTapeProtection = "内部胶带保护";
ElecCorrugatedProtection = "内部波纹保护";
ElecProtectionSet = "保护集";

//vya Start 08:01:07
//IsAssignedDiameterValid = "Is Diameter Assigned To Bundle Segment Valid";
Elec_FromConnectionPointName =	"源连接点名称";
Elec_ToConnectionPointName	 =	"目标连接点名称";
//vya End 08:01:07
//DiameterComputedFromWires = "Segment Diameter Computed From Routed Wires and Wire Groups"; //ayz 08:02:09
DiameterComputedFromWires = "从线计算所得直径"; //ayz 08:02:09

// cvv 08:04:09 Fis for IR 0620895 - added missing attributes/ types
ElecMultipleSegments = "多段";
Elec_Barrel_Diameter = "允许线通过的孔直径。";
Elec_Mass = "质量";
Elec_Equivalent_Thickness = "等效厚度";
SpecificationPartNumber = "规格中对象的零件编号";
Elec_Shielding_Signal = "屏蔽信号";
Elec_Catalog = "目录";


