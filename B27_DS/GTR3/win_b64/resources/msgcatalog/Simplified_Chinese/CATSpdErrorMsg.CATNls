// COPYRIGHT ImpactXoft 2003
//===================================================================
SpdInternalError="出现意外错误情况。";

SpdProfileInp1="未指定或已取消激活草图。";
SpdProfileInp2="此特征要求关闭关联草图。";
SpdProfileInp3="此特征要求关联草图不为空。";
SpdProfileInp4="此特征要求关联草图处于活动状态。";
SpdProfileInp5="扫掠轮廓必须为平面。";
SpdProfileInp6="选定的草图包含定义为标准几何元素的点。\n必须将这些点转换为构造元素。";
SpdProfileInp7="从封闭线抽壳运算符：\n警告：输入线自相交。\n请修改输入线。";
SpdProfileInp8="可抽壳特征形成的包络体未完全通过棱镜，\n请尝试移动几何图形，放大可抽壳包络体或缩小轮廓。";
SpdProfileInp9="至少有一个偏移值必须为非零值。";
SpdProfileInp10="棱镜和可抽壳特征形成的包络体之间的交互作用过于复杂，\n最可能的原因是特征的轮廓仅部分位于可抽壳包络体内。";
SpdProfileInp11="棱镜和限制选择之间的交互作用过于复杂，\n最可能的原因是没有完全按限制曲面修剪拉伸草图。";
SpdProfileInp12="“达到盒体”选项要求必须存在可抽壳特征。";
SpdProfileInp13="选定的轮廓线自相交，\n拉伸运算中不能使用此配置，\n请选择一条不是自相交的线。";
SpdProfileInp14="棱镜限制规格过于复杂，\n请更改棱镜限制规格。";

SpdDraftTypeInvalid="基于特征的扫掠仅支持角的拔模类型。";
SpdDraftTypeInvalid2="如果尺寸限制为“穿透”或“到达盒体”，系统只支持角的拔模类型。";
SpdDraftLimit1Invalid="如果第一限制尺寸为“穿透”或“到达盒体”，那么拔模中性元素不能为第一限制平面。";
SpdDraftLimit2Invalid="如果第二限制尺寸为“穿透”或“到达盒体”，那么拔模中性元素不能为第二限制平面。";
SpdDraftAngleInvalid="拔模角必须介于 -85.0 度到 85 度之间。";
SpdDraftDoubleFillet="进行二次拔模时不能进行侧面倒角。";
SpdDraftSketchHasNoPlane="草图支持面无效，无法确定拔模中性平面。";
SpdDraftPropsMissing="拔模属性丢失。";
SpdDraftNeutralMissing="拔模中性元素丢失。";
SpdDraftNeutralInvalid="选定的拔模中性元素无效。";
SpdDraftPartingUnresolved="无法解析拔模分离元素。";
SpdDraftPartingMissing="将分离元素用作中性元素时，必须选择一个分离元素。";
SpdDraftBadPull="拔模牵引方向与中性元素平行。";
SpdDraftBadPartingPull="拔模牵引方向与分离元素平行。";
SpdDraftFacesFailed="拔模牵引方向与正在拔模的面垂直。";
SpdDraftFailed="拔模操作失败，\n请尝试更改某些拔模参数或几何图形。";
SpdDraftFailedKindly="拔模操作失败 － 局部拓扑或几何图形过于复杂。";

SpdReferencedPoint="某个顶点不再可识别。";
SpdReferencedAxis="该轴不再可识别。";
SpdReferencedLine="某条边不再可识别。";
SpdReferencedFace="某个面不再可识别。";
SpdReferencedCuttingFace="某个用作剪切元素的面不再可识别。";
SpdReferencedPartingFace="某个用作分离元素的面不再可识别。";

SpdEvalDataPath="此特征要求指定路径。";
SpdEvalDataPathActive="此特征要求关联路径处于活动状态。";
SpdEvalDataDirection="长度值的总和不得等于零。";
SpdEvalDataPathInvalid="路径牵引方向不受支持。";
SpdEvalDataPathSupport="路径和轮廓的草图支持面相同。";
SpdEvalDataPathParallel="路径和轮廓的支持面相互平行。";

SpdEvalDataFillet="不允许圆角半径为负数。";
SpdEvalDataThickFillet="在加厚的特征上每个圆角半径必须小于或等于总厚度的一半。";
SpdEvalDataThickShell="可抽壳特征的厚度必须大于盒体厚度的两倍。";

SpdEvalDataFillet1Sides="对于该特征的侧面而言，第一个圆角太大";
SpdEvalDataFillet2Sides="对于该特征的侧面而言，第二个圆角太大";
SpdEvalDataFillet12Sides="对于特征的侧面而言，第一个圆角和第二个圆角结合起来太大。";
SpdEvalDataFillet1End="对于特征的第 1 个限制面而言，第一个圆角太大。";
SpdEvalDataFillet2End="对于特征的第 2 个限制面而言，第二个圆角太大。";
SpdFilletFailure="不能初始化特征所请求的圆角操作。";

SpdReinforcementFillet="对于特征的侧面而言，侧面的圆角太大。";
SpdEvalDraftFillets="此特征类型不支持拔模圆角选项";
SpdEvalDraftFilletsErr="圆角拔模失败，\n请尝试更改某些拔模参数、圆角参数或几何图形。";

SpdEvalDataWall="墙壁厚度无效。";
SpdEvalDataWallHeight="特征长度必须大于墙壁厚度。";
SpdEvalDataVolume="如果不使用加厚选项，则无法使用开放草图将该特征转为实体。";
SpdEvalDataThin="所输入的厚度值不会产生包络体。";

SpdEvalDataSplitDraft="厚度大于高度的特征无法进行分割拔模。";

SpdAxis="必须指定解析轴。";
SpdAngle="这两个角将不会创建包络体。";

SpdBoundaryInside="边界草图平面的位置和/或方向错误，\n该平面不应处于盒体的内部，\n 并且其方向应朝向该盒体的内部。";
SpdBoundaryTooLarge="边界草图过大或可能缺少盒体。";
SpdBoundaryTooLoopy="不允许出现多个边界环。";
SpdBoundarySketch="必须指定边界草图。";
SpdBoundaryActive="边界草图必须处于活动状态。";
SpdBoundaryEmpty="边界草图不能为空。";
SpdBoundaryClosed="边界草图必须是闭合的。";
SpdBoundaryPoints="边界草图中包含多个定义为标准几何元素的点，\n必须将这些点转换为构造元素。";
SpdHeight="边界高度必须为正数。";
SpdWidth="边界宽度必须为正数。";
SpdSpikeHeight="钉的高度必须为正数。";
SpdSpikeWidth="钉的宽度必须为正数。";
SpdIslandSketch="岛状草图必须是闭合的。";
SpdSpikeMustExist="选定的拔模中性元素需要钉状几何图形。";
SpdRibMustExist="选定的拔模中性元素需要条状几何图形。";

SpdSpikePositionRib="钉不得位于条之下。";
SpdSpikePositionBdy="钉不得位于边界之下。";
SpdSpikeOffsets="钉的偏移量不得超过钉本身。";
SpdRibHeight="条的高度值必须为正数。";
SpdRibWidth="条的宽度值必须为正数。";


SpdThicknessClosed="如果厚度为零，则草图必须是闭合的。";
SpdNoCuttingFaces="未选择剪切面。";
SpdMultipleTargets="特征不支持多个目标。";

SpdNoThickenSrf="选来进行加厚操作的曲面不再存在。";
SpdUnboundedSrf="选来进行较厚操作的曲面没有边界。";

SpdUnresolvedDirectionId="无法使用该参考方向，\nCATIA 将选择默认方向。";
SpdUnresolvedPathControlId="无法使用该路径控制参考，\nCATIA 将选择默认路径控制。";
SpdUnresolvedPathLimitsId="无法使用用于限制路径的参考点，\nCATIA 将选择默认路径限制。";
SpdUnresolvedFaceId="无法解析某个面，\n系统将移除其规格。";
SpdUnresolvedFacesId="无法解析 /p1 面，\n系统将移除其规格。";
SpdUnresolvedRevolveAxesId="无法使用该参考轴，\n系统将移除其规格。";
SpdUnresolvedCuttingFaceId="无法解析某个剪切面，\n系统将移除其规格。";
SpdUnresolvedCuttingFacesId="无法解析 /p1 剪切面，\n系统将移除其规格。";
SpdUnresolvedPartingFaceId="无法解析该分离元素面，\n系统将移除其规格。";
SpdUnresolvedPullingDirection="无法解析该牵引方向。\n系统将移除其规格。";
SpdUnresolvedLimitsId="无法解析某个限制面。";
SpdUnresolvedPathEdgeId="无法解析某条路径边线，\n系统将移除其规格。";
SpdUnresolvedPathEdgesId="无法解析 /p1 路径边线，\n系统将移除其规格。";
SpdUnresolvedDraftProperties="无法解析拔模属性，\n拔模操作将被重置为“无”且系统将不对该特征进行拔模。";
SpdUnresolvedDraftPartingFace="无法解析拔模属性的分离元素面。";
SpdUnresolvedDraftPullingDir="无法解析拔模属性的牵引方向。";
SpdUnresolvedDraftNeutralElement="无法解析该中性元素，\n系统将移除其规格。";
SpdUnresolvedLipPullingDirection="无法解析该牵引方向。\n系统将使用分隔元素法线。";
SpdUnresolvedPatternDir1="无法解析阵列的第一个方向，\n系统将移除其规格。";
SpdUnresolvedPatternDir2="无法解析阵列的第二个方向，\n系统将移除其规格。";
SpdUnresolvedMirrorElement="无法解析该镜像元素，\n系统将移除其规格。";
SpdUnresolvedSymmetryRef="无法解析对称转换参考元素，\n系统将移除其规格。";
SpdUnresolvedScalingRef="无法解析缩放转换参考元素，\n系统将移除其规格。";
SpdUnresolvedStartPt="无法解析转换起点，\n系统将移除其规格。";
SpdUnresolvedEndPt="无法解析转换终点，\n系统将移除其规格。";
SpdUnresolvedAxisOrigin="无法解析转换轴原点，\n系统将移除其规格。";
SpdUnresolvedXYPlane="无法解析转换轴 XY 平面，\n系统将移除其规格。";
SpdUnresolvedXAxis="无法解析转换 X 轴，\n系统将移除其规格。";
SpdUnresolvedRefAxis="无法解析转换参考轴系，\n系统将移除其规格。";
SpdUnresolvedTgtAxis="无法解析转换目标轴系，\n系统将移除其规格。";
SpdUnresolvedElementId="无法解析某个轮廓元素，\n系统将移除其规格。";
SpdUnresolvedElementsId="无法解析 /p1 轮廓元素，\n系统将移除其规格。";
SpdUnresolvedBrepEdgeFilletId="无法解析圆角边线，\n系统将移除其规格。";
SpdUnresolvedBrepEdgesFilletId="无法解析 /p1 边线，\n系统将移除其规格。";

SpdWallInterference="厚度必须大于或等于几何体厚度，否则平台内部将被底切。";

SpdInvalidThickness="厚度值不能为负数。";

SpdBadDirection="选定的方向与草图平面平行。";
SpdBadRevolveAxis="必须指定放置在草图平面上的轴。";
SpdBadPath="选定的路径与草图平面平行。";

SpdPathLimitPtsAndMulti="不允许在带有多条轮廓线的路径上出现路径限制点。";
SpdPathMoveToNeeded="如果路径带有多条轮廓线，则必须使用“将轮廓移动到路径”选项。";

SpdMissingLimitSurface="限制平面或限制曲面丢失。";
SpdLimitSketchBadNormal="限制草图平面法线不能与拉伸方向平行。";

SpdShellDeleteErr="该元素是功能几何体强制规格的一部分，不能将其删除。";
SpdShellDeleteErr2="墙体规格存在在功能几何体中，不能删除该元素。";
SpdShellCopyErr="该元素是功能几何体强制规格的一部分，不能对其进行复制。";
SpdShellNoOutput="盒体属性特征：几何体所有的面都已被选为要移除的面，\n结果几何体将为空，这可能是由自动相切传播导致的。";

SpdFeatureCopyErr="不能复制该元素。";

SpdBodyRefNoBody="不能检索到所参考的几何体的结果。";
SpdCloseOpError="闭合运算发生意外错误。";

SpdTwistError="选定的轮廓和中心曲线导致外形自交，\n请使用曲率较小的中心曲线。";

SpdPatNYI="阵列功能尚未实现。";
SpdPatKey1="第一个方向上的参考位置必须大于零。";
SpdPatKey2="使用 <Spacing&Length> 选项时，长度必须大于或等于间距。";
SpdPatSpaceTotalAng="使用 <Angular spacing&Total angle> 选项时，总角度必须大于或等于角间距。";
SpdPatSpaceTotalLen="使用 <Spacing&Length> 选项时，长度必须大于或等于间距。";
SpdPatCountNeg="实例计数必须大于零。";
SpdPatCountTooBig="实例计数必须小于或等于 10000。";
SpdPatSpaceLen="间距不能为零。";
SpdPatTotalLen="长度不能为零。";
SpdPatSpaceAng="角间距不能为零。";
SpdPatTotalAng="总角度不能为零。";
SpdPatTotalAng360="总角度必须小于 360 度，如有必要，请使用 <Complete crown> 选项。";
SpdPatKeySuppress="不能取消阵列的参考对象。";
SpdPatUnresolvedDir1="无法解析阵列的第一个方向，";
SpdPatUnresolvedDir2="无法解析阵列的第二个方向，";
SpdPatNoAxes="进行阵列时至少需要一个方向。";
SpdPatParallelAxes="阵列的第一个方向和第二个方向不能平行。";
SpdPatNoSketch="用户定义阵列需要草图。";
SpdPatSketchEmpty="用户定义阵列的草图必须包含非构造点。";
SpdPatKeepSpec="阵列后的特征作为修饰特征时，不可以使用“保持规格”选项。";

SpdJoinSketchEmpty="接合草图必须包含非构造点。";
SpdJoinProfilePosition="未能产生与指定轮廓位置的有效接合。";
SpdJoinGuideHeight="引出线高度必须大于或等于零。";
SpdJoinDraftAngle="所有接合拔模角必须大于或等于零。";
SpdJoinClearance="除螺纹间隙外，所有接合\n间隙必须大于或等于零。";
SpdScrewHeight="螺孔高度必须大于或等于零。";
SpdScrewHeight2="螺孔高度必须大于零。";
SpdScrewDiameter="所有螺孔直径必须大于或等于零。";
SpdThreadDiameter="未定义有效螺钉，\n螺钉的螺纹直径必须大于零。";
SpdThreadHeight="未定义有效螺钉，\n螺钉的螺纹高度必须大于零。";
SpdJoinNoEVT="不能产生有效接合，接合特征使用了几何体中的\n 几何图形来定义自身的几何图形。";
SpdJoinLump0="螺钉位于零件材料内部。";
SpdJoinLump1="螺孔位置或头/螺纹参数值导致接合几何体延伸到零件材料之外。";
SpdThreadWall="螺纹壁与紧固套筒相交，要解决此问题，\n请增大紧固拔模角、紧固边间隙或\n紧固高度间隙；或者减小螺纹外部拔模角。";
SpdJoinSiblingAbsent="不能使用同属接合，此时无法替换草图。";
SpdUnderCutShank="参数组合将导致在两个方向上底切柄区域。";
SpdShankDraftLarge="柄拔模角太大，头部被底切。";
SpdHeadOuterDraft="头部的外部拔模角或内部的紧固拔模角太大，\n紧固套筒被切割。";
SpdThreadTooLarge="该螺纹与紧固套筒不匹配，请移除套筒或减小螺纹壁、螺纹\n直径、螺纹边间隙或紧固边间隙。";
SpdInvalidScrewDia="给定的螺钉直径尺寸无效。";
SpdInvalidHeadDraft="头部的外部拔模角必须大于或等于头部的内部拔模角。";
SpdMissingTrim="无法获得用于修剪螺钉外围的正包络体。";
SpdJoinSSCTooSmall="柄半径加上柄边间隙小于螺纹半径。";
SpdNoOSVDCC="接合特征所创建的结构可用于连接两个墙体几何体，\n如果其中一个墙体几何体是实体，则要使用孔特征。";

SpdJoinGussetTopOffset="结点板顶部偏移量不能为负数。";
SpdJoinGussetHeadWidth="无法创建头部结点板，请尝试增大侧翼宽度。";
SpdJoinGussetThreadWidth="无法创建螺纹结点板，请尝试增大侧翼宽度。";
SpdJoinGussetThickness="结点板壁厚度不能为负数。";
SpdJoinGussetFilletRad1="结点板圆角半径不能为负数。";
SpdJoinGussetFilletRad2="结点板圆角半径对于所需厚度来说过大。";
SpdJoinGussetBlendRad="结点板桥接曲面半径不能为负数。";
SpdJoinGussetLimit1="无法创建结点板，这极有可能是因为参数导致结点板横截了盒体属性移除面。";

SpdScrewDescNoRead="无法从目录读取螺钉描述。";

SpdDivideCutFailure="使用分割元素作为切割器，未能产生有效的分割。";
SpdCutCutFailure="未能使用剪切元素产生有效的剪切。";
SpdCutTooSmall="剪切元素没有完全切除选定的特征，请尝试展开剪切曲面。";
SpdCutSurfaceParallelToExtrusion="无法生成该剪切元素，剪切曲面拉伸方向与剪切曲面不平行，请修改参考曲面。";

SpdDraftPropsPartingFaceId="缺少分离元素面。";
SpdDraftPropsPullingDirection="缺少牵引方向。";

SpdPPFClearance1="间隙距离必须大于或等于零。";
SpdPPFClearance2="间隙距离或其他间隙面必须大于零。";

SpdLipSimpleAllLimitation="由于当前的限制，无法提取用于简化所有唇缘的环。";
SpdLipSimpleHeight="唇缘高度必须为正值。";
SpdLipSimpleThickness="唇缘厚度必须为正值。";
SpdLipSimpleAngle="唇缘角必须介于 -85 度到 85 度之间。";
SpdLipSimpleInnerAngle="唇缘内角必须介于 -85 度到 85 度之间。";
SpdLipSimpleClearance="唇缘间隙必须为正数或为零。";
SpdLipPullParallel="唇缘牵引方向不得与路径平行。";
SpdLipProfilePerp="唇缘牵引方向不得位于轮廓平面中。";
SpdLipSimpleSweep="唇缘情况过于复杂，\n请尝试使用较简单的路径、较小的厚度或较小的高度。";
SpdLipPullingDirectionUnr="无法解析该牵引方向。";
SpdLipProfileClosed="轮廓必须是开放的且是平面的。";
SpdLipUpperProfileClosed="上轮廓必须是开放的且是平面的。";
SpdLipLowerProfileClosed="下轮廓必须是开放的且是平面的。";
SpdLipCutWithDivElemFail="面面相交失败。\n请检查与分隔元素接近相切的唇缘轮廓曲线。";
SpdLipIntxWithDivBodyFail="面面相交失败。\n请检查与分隔几何体接近相切的唇缘轮廓曲线。";
SpdLipMultiContourProfile="唇缘轮廓不能包含多条轮廓线。";
SpdLipNoContourProfile="唇缘轮廓不包含轮廓线。";

SpdMirrorElementRequired="需要镜像元素。";
SpdMirrorElementInActive="镜像元素必须处于活动状态。";

SpdReinforcementThickness="参数组合可能会导致添加整片材料，\n请减小厚度值";

SpdAffinityOrigin="仿射轴原点必须为一个点。";
SpdAffinityOriginSolve="选定的仿射轴原点无法解析。";
SpdAffinityMissingSelec="由于为仿射轴系选择了 X 轴、原点或 XY 平面，因此必须选定所有内容。";
SpdAffinityPlane="仿射轴 XY 平面必须为一个平面。";
SpdAffinityPlaneSolve="选定的仿射轴 XY 平面无法解析。";
SpdAffinityXScale="仿射 X 轴比率必须大于零。";
SpdAffinityYScale="仿射 Y 轴比率必须大于零。";
SpdAffinityZScale="仿射 Z 轴比率必须大于零。";
SpdAffinityXAxis="仿射 X 轴必须为一条直线。";
SpdAffinityXAxisSolve="选定的仿射 X 轴无法解析。";
SpdAffinityXAxisInvalid="选定的 X 轴投影为选定的 XY 平面上的一个点。";

SpdSweepPullParallel="牵引方向不得与路径平行。";
SpdSweepRefSurface="输入线不在盒体支持面上。请将它投影到支持面上，并使用该投影。";
SpdSweepDistanceError="扫掠的起点和终点相同。无法创建扫掠";

SpdExtractBadVolType="设置了无效的包络体类型，只支持已添加的和受保护的包络体类型。";
SpdExtractNoSource="需要源几何体。";
SpdExtractOldGrill="选定的格栅特征是在此软件的早期版本中创建，无法提取。";
SpdExtractMissingGrill="必须选择一个格栅特征。";
SpdExtractMissingFeatures="必须选择一个或多个特征。";

SpdExtBehaviorOverrideConflict="目标特征不允许更改其操作。";
SpdExtBehaviorInvalidTarget="不能提取该目标特征，不能提取以下特征的操作：\n   属性特征 \n   多几何体特征 \n   带有多个目标的修饰特征";

SpdPocketBadWallVersion="在版本 13 之前创建的槽腔不支持间隙壁。";
SpdPocketOOnlyNoCWall="“仅外部槽腔”上的间隙壁值必须为零。";
SpdPocketCWallRequiresPWall="间隙壁需要槽腔壁。";

SpdImportWallDirMismatch="盒体的墙体方向与导入特征的墙体方向之间不匹配。";
SpdImportWallDirMismatch2="源盒体的墙体方向与目标盒体的墙体方向之间不匹配，\n请在“目标实体功能设置”中更改盒体的墙体方向。";
SpdImportWallDirMismatch3="源盒体的墙体方向与目标盒体的墙体方向之间不匹配，\n请在“目标实体功能设置”中创建盒体属性并更改盒体的墙体方向。";
SpdImportWallDirMismatch4="源盒体的墙体方向与目标盒体的墙体方向之间不匹配，\n请在“目标实体功能设置”中更改盒体的墙体方向，同时同步更新导入的盒体。";

SpdImportWallTypeUseBodyTh="粘贴结果可能与预期结果不相同：请检查是否将参考特征墙体选项设置成了“使用几何体厚度”。";
SpdImportExtAcrRemdFaces="粘贴结果可能与预期结果不相同：请检查是否取消了参考特征选项中的“扩展已移除面”设置。";
SpdImportLimitUpToShell="粘贴结果可能与预期结果不相同：请检查是否设置了盒体的两个参考特征限制。";

SpdImportWithTRPropagation="通过导入传播技术结果失败。";

SpdFlangeTargetNeeded="只能在带有分隔的零件上创建凸缘。";
SpdFlangeTargetData1="凸缘分隔丢失数据。";
SpdFlangeTargetData2="凸缘分隔结果不支持凸缘。";
SpdFlangeTargetData3="沿牵引方向的分隔边线投影未产生有效结果，\n请注意分隔曲面必须足够大以便包住凸缘。";
SpdFlangeTargetData4="由于当前限制，不能计算分隔边线的投影。";
SpdFlangeTargetData5="分隔零件导致无法产生带凸缘的边线，\n这极有可能是因为使用了受保护的包络体来定义此零件的外部边界。";
SpdFlangeWidth="凸缘宽度必须大于零。";
SpdFlangeThickness="凸缘厚度必须大于零。";
SpdFlangeTopRadius="凸缘顶部半径必须为正值。";
SpdFlangeTopRadius2Big="凸缘顶部半径太大。";
SpdFlangeBotRadius="凸缘底部半径必须为正值。";
SpdFlangeBotRadius2Big="凸缘底部半径太大。";
SpdFlangeSidRadius="与底部半径结合的凸缘顶部半径太大。";
SpdFlangeBothRadius2Big="凸缘的顶部半径和底部半径过大。";
SpdFlangePullDirUnr="无法解析凸缘牵引方向。";
SpdFlangeBoundaryProblem="不能将分界环扩展到所请求的宽度，\n请尝试设置较小的宽度。";
SpdFlangePullBad="\n尝试更改凸缘牵引方向，以便清晰地投影分界线前的边环。";

SpdSnapLockBadLength="敏捷移动锁定长度必须大于零。";
SpdSnapLockRetAngle="保留角必须大于 0 度且小于 180 度。";
SpdSnapLockRetAngle2="保留角必须大于 0 度且小于或等于 90 度。";
SpdSnapLockInsertAngle="插入角必须大于或等于 0 度且小于 90 度。";
SpdSnapLockInsertAngle2="插入角必须大于 0 度且小于或等于 90 度。";
SpdSnapLockWidth="墙体宽度必须大于或等于保留宽度。";
SpdSnapLockThick="墙体厚度必须大于或等于保留厚度。";
SpdSnapLockRetLength="保留长度必须大于或等于钩尖长度。";
SpdSnapLockTolerance="公差必须大于或等于零。";
SpdSnapLockWidth2="（宽度 - 侧面 1 厚度 - 侧面 2 厚度）必须大于零。";
SpdSnapLockBadLength2="（长度 - 顶部厚度 - 环宽度）必须大于零。";
SpdSnapLockWidth3="位于保留孔内的卡子部分必须小于或等于环宽度。";
SpdSnapLockWidth4="（宽度 - 侧面 1 厚度 - 侧面 2 厚度 - (2 * 卡子宽度公差)）必须大于零。";
SpdSnapLockBadLength3="指定的角度和钩尖长度导致保留长度超出指定的最大值，\n 请尝试增大角度或保留长度，或减小钩尖长度。";
SpdSnapLockBadHeight1="指定的角度导致产生的保留曲线出现自相交，\n请尝试增大角度。";
SpdSnapLockBadAngle="指定的角度导致卡子底部长度小于零，\n请尝试更改角度或增大卡子长度和偏移量。";
SpdSnapLockNoPos="未选择剪裁位置。";
SpdSnapLockNoEngage="卡子偏移量大于或等于卡子高度，\n因此卡子不会与环接合。";

SpdCFSBooleanNVStar="从核心包络体中移除内部包络体时出现错误。\n";
SpdCFSBooleanSVC="从几何体中移除核心包络体时出现错误。\n";
SpdCFSBooleanFNV="从几何体中移除受保护包络体时出现错误。\n";
SpdCFSBooleanENV="从几何体中移除负包络体时出现错误。\n";

SpdEdgeFilletPartial="部分结果：某些元素无法圆角化。";
SpdEdgeFilletUseLess="若规格更改，则特征可能无用。";

SpdThickenFailed="加厚操作失败，无法加厚一个或多个面，请尝试修改输入。";
SpdThickenUnresolved="加厚操作失败，有些面无法解析。";

SpdUpdateCycleError="选择产生特征更新周期。";
SpdShellPropsError="包含依赖于盒体属性特征的实体功能集中没有可用的盒体属性特征。";
SpdShellPropsError01="实体功能集不能包含多个壳体属性特征。";

SPDCFSCompatibleFeature="特征与当前环境不兼容。其选择被禁止。";

SpdLLMCopyError="如果目标为一个局部修改符，则您无法复制/剪切局部实时修改符。";
SpdLLMCopyError1="不允许剪切实时局部修改器。";

