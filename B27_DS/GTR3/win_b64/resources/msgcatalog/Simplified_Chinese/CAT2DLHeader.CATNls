// Reviewed by LGK Jan 04, 2005

//------------------------------------------------------------------------------
// Output profile
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLProfileHdr.Title="3D 轮廓";
CAT2DLHeader.CAT2DLProfileHdr.ShortHelp="3D 轮廓";
CAT2DLHeader.CAT2DLProfileHdr.Help="创建 3D 轮廓";
CAT2DLHeader.CAT2DLProfileHdr.LongHelp="3D 轮廓\n通过选择 2D 几何图形创建 3D 轮廓。";

//------------------------------------------------------------------------------
// Output plane
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLPlaneHdr.Title="3D 平面";
CAT2DLHeader.CAT2DLPlaneHdr.ShortHelp="3D 平面";
CAT2DLHeader.CAT2DLPlaneHdr.Help="创建 3D 平面";
CAT2DLHeader.CAT2DLPlaneHdr.LongHelp="3D 平面\n通过选择 2D 线创建 3D 平面。";

//------------------------------------------------------------------------------
// Output axis
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLProfileAxisHdr.Title="3D 轴";
CAT2DLHeader.CAT2DLProfileAxisHdr.ShortHelp="3D 轴";
CAT2DLHeader.CAT2DLProfileAxisHdr.Help="创建 3D 轴";
CAT2DLHeader.CAT2DLProfileAxisHdr.LongHelp="3D 轴\n通过选择点创建 3D 轴。";

//------------------------------------------------------------------------------
// View creation
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLCreateViewHdr.Title="主视图";
CAT2DLHeader.CAT2DLCreateViewHdr.ShortHelp="主视图";
CAT2DLHeader.CAT2DLCreateViewHdr.Help="新建主视图";
CAT2DLHeader.CAT2DLCreateViewHdr.LongHelp="主视图\n新建视图。";

//------------------------------------------------------------------------------
// View from 
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLCreateViewFromHdr.Title="参考角度视图";
CAT2DLHeader.CAT2DLCreateViewFromHdr.ShortHelp="参考角度视图";
CAT2DLHeader.CAT2DLCreateViewFromHdr.Help="从平面、平面的面、草图、视图、FT&A 视图或 FT&A 捕获创建新视图";
CAT2DLHeader.CAT2DLCreateViewFromHdr.LongHelp="参考角度视图\n从平面、面、草图、视图、FTA 视图或 FTA 捕获创建一个新视图。";

//------------------------------------------------------------------------------
// View aux/section
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLCreateViewAuxHdr.Title="轮廓角度视图";
CAT2DLHeader.CAT2DLCreateViewAuxHdr.ShortHelp="轮廓角度视图";
CAT2DLHeader.CAT2DLCreateViewAuxHdr.Help="创建新剖视图、截面分割或辅助视图";
CAT2DLHeader.CAT2DLCreateViewAuxHdr.LongHelp="轮廓角度视图\n从轮廓创建新截面视图、截面分割或辅助视图。";

//------------------------------------------------------------------------------
// Section from plane
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLSectionFromPlaneHdr.Title="从两个平面新建截面";
CAT2DLHeader.CAT2DLSectionFromPlaneHdr.ShortHelp="从两个平面新建截面";
CAT2DLHeader.CAT2DLSectionFromPlaneHdr.Help="从两个 3D 平面创建新的对齐剖视图或对齐截面分割";
CAT2DLHeader.CAT2DLSectionFromPlaneHdr.LongHelp="从两个平面新建截面\n从两个 3D 平面创建新的对齐剖视图或对齐截面分割。";

//------------------------------------------------------------------------------
// 2D Component
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLCreateViewDetailHdr.Title="2D 部件";
CAT2DLHeader.CAT2DLCreateViewDetailHdr.ShortHelp="2D 部件";
CAT2DLHeader.CAT2DLCreateViewDetailHdr.Help="创建可实例化的 2D 部件参考";
CAT2DLHeader.CAT2DLCreateViewDetailHdr.LongHelp="2D 部件\n创建可实例化的 2D 部件参考。";

//------------------------------------------------------------------------------
// Use-Edge Intersection
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLUseEdgeIntHdr.Title="与 3D 元素相交";
CAT2DLHeader.CAT2DLUseEdgeIntHdr.ShortHelp="与 3D 元素相交";
CAT2DLHeader.CAT2DLUseEdgeIntHdr.Help="与 3D 元素相交以在当前视图中创建 2D 几何图形";
CAT2DLHeader.CAT2DLUseEdgeIntHdr.LongHelp="与 3D 元素相交\n与 3D 元素相交以在当前视图中创建 2D 几何图形。";

//------------------------------------------------------------------------------
// Use-Edge Project
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLUseEdgeProjHdr.Title="投影 3D 元素";
CAT2DLHeader.CAT2DLUseEdgeProjHdr.ShortHelp="投影 3D 元素";
CAT2DLHeader.CAT2DLUseEdgeProjHdr.Help="投影 3D 元素以在当前视图中创建 2D 几何图形";
CAT2DLHeader.CAT2DLUseEdgeProjHdr.LongHelp="投影 3D 元素\n投影 3D 元素以在当前视图中创建 2D 几何图形。";

//------------------------------------------------------------------------------
// Use-Edge Silhouette
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLUseEdgeSilhHdr.Title="投影 3D 轮廓边线";
CAT2DLHeader.CAT2DLUseEdgeSilhHdr.ShortHelp="投影 3D 轮廓边线";
CAT2DLHeader.CAT2DLUseEdgeSilhHdr.Help="在当前视图中投影 3D 元素的侧影轮廓边线";
CAT2DLHeader.CAT2DLUseEdgeSilhHdr.LongHelp="投影 3D 侧影轮廓边线\n在当前视图中投影 3D 元素的侧影轮廓边线。";

// UseEdge by Canonical Silhouette
CAT2DLHeader.CAT2DLUseEdgeCanonicalSilhHdr.Title="投影 3D 标准侧影轮廓边线";
CAT2DLHeader.CAT2DLUseEdgeCanonicalSilhHdr.Help="在当前视图中投影 3D 元素的标准侧影轮廓边线";
CAT2DLHeader.CAT2DLUseEdgeCanonicalSilhHdr.ShortHelp="投影 3D 标准侧影轮廓边线";
CAT2DLHeader.CAT2DLUseEdgeCanonicalSilhHdr.LongHelp="投影 3D 标准侧影轮廓边线\n在当前视图中投影 3D 元素的侧影轮廓边线。";

//--------------------------------------
// Use edge offset
//--------------------------------------
CAT2DLHeader.CAT2DLUseEdgeOffsetHdr.Title="偏移 3D 元素";
CAT2DLHeader.CAT2DLUseEdgeOffsetHdr.ShortHelp="偏移 3D 元素";
CAT2DLHeader.CAT2DLUseEdgeOffsetHdr.Help="偏移 3D 元素以在当前视图中创建 2D 几何图形。";
CAT2DLHeader.CAT2DLUseEdgeOffsetHdr.LongHelp="偏移 3D 元素\n偏移 3D 元素以在当前视图中创建 2D 几何图形";

//------------------------------------------------------------------------------
// Visu background
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLDispBackAsInFilterHdr.Title="为每个视图显示指定的背景";
CAT2DLHeader.CAT2DLDispBackAsInFilterHdr.ShortHelp="为每个视图显示指定的背景";
CAT2DLHeader.CAT2DLDispBackAsInFilterHdr.Help="在每个视图的上下文菜单中显示指定的背景可视化选项";
CAT2DLHeader.CAT2DLDispBackAsInFilterHdr.LongHelp="为每个视图显示指定的背景\n在每个视图的上下文菜单中显示指定的背景可视化选项。";

CAT2DLHeader.CAT2DLViewBackManipHdr.Title="视图背景操作";
CAT2DLHeader.CAT2DLViewBackManipHdr.ShortHelp="视图背景操作";
CAT2DLHeader.CAT2DLViewBackManipHdr.Help="允许通过背景视图内容对视图进行操作。";
CAT2DLHeader.CAT2DLViewBackManipHdr.LongHelp="视图背景操作\n允许通过背景视图内容对视图进行操作。";

CAT2DLHeader.CAT2DLIntegrateBackgroundInViewFrameHdr.Title="包括背景";
CAT2DLHeader.CAT2DLIntegrateBackgroundInViewFrameHdr.ShortHelp="视图框架中包括背景";
CAT2DLHeader.CAT2DLIntegrateBackgroundInViewFrameHdr.Help="在显示视图框架时考虑背景";
CAT2DLHeader.CAT2DLIntegrateBackgroundInViewFrameHdr.LongHelp="包括背景\n在显示视图框架时考虑背景。";

//------------------------------------------------------------------------------
// Hide 3DVisu
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DL3DVisuOffHdr.Title="在 3D 中隐藏";
CAT2DLHeader.CAT2DL3DVisuOffHdr.ShortHelp="在 3D 中隐藏";
CAT2DLHeader.CAT2DL3DVisuOffHdr.Help="在 3D 中隐藏图纸或视图";
CAT2DLHeader.CAT2DL3DVisuOffHdr.LongHelp="在 3D 中隐藏\n在 3D 中隐藏图纸或视图。";

//------------------------------------------------------------------------------
// Show 3DVisu
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DL3DVisuOnHdr.Title="在 3D 中显示";
CAT2DLHeader.CAT2DL3DVisuOnHdr.ShortHelp="在 3D 中显示";
CAT2DLHeader.CAT2DL3DVisuOnHdr.Help="在 3D 中显示图纸或视图";
CAT2DLHeader.CAT2DL3DVisuOnHdr.LongHelp="在 3D 中显示\n在 3D 中显示图纸或视图。";

//------------------------------------------------------------------------------
// Clipping plane
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLClippingPlaneHdr.Title="切除面";
CAT2DLHeader.CAT2DLClippingPlaneHdr.ShortHelp="切除面";
CAT2DLHeader.CAT2DLClippingPlaneHdr.Help="沿每个视图的定义平面切除布局视图的 3D 背景";
CAT2DLHeader.CAT2DLClippingPlaneHdr.LongHelp="切除面\n沿每个视图的定义平面切除布局视图的 3D 背景。";

//------------------------------------------------------------------------------
// Filters
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLFiltersHdr.Title="布局视图过滤器...";
CAT2DLHeader.CAT2DLFiltersHdr.ShortHelp="布局视图过滤器";
CAT2DLHeader.CAT2DLFiltersHdr.Help="为布局视图中的可视化元素定义过滤器";
CAT2DLHeader.CAT2DLFiltersHdr.LongHelp="布局视图过滤器\n为布局视图中的可视化元素定义过滤器。";

CAT2DLHeader.CAT2DLViewFilterHdr.Title="更改视图过滤器";
CAT2DLHeader.CAT2DLViewFilterHdr.ShortHelp="更改视图过滤器";
CAT2DLHeader.CAT2DLViewFilterHdr.Help="更改视图过滤器";
CAT2DLHeader.CAT2DLViewFilterHdr.LongHelp="更改视图过滤器\n更改视图过滤器。";


//------------------------------------------------------------------------------
// Parent/Children
//------------------------------------------------------------------------------
CAT2DLHeader.CATParentChildren.Title="父级/子级...";
CAT2DLHeader.CATParentChildren.ShortHelp="父级/子级...";
CAT2DLHeader.CATParentChildren.Help="定义父级和子级";
CAT2DLHeader.CATParentChildren.LongHelp="父级/子级\n定义父级和子级。";

//------------------------------------------------------------------------------
// Working views from contextual menu
//------------------------------------------------------------------------------
CAT2DLHeader.CATDrwEditSheetFromContextHdr.Title="编辑图纸";
CAT2DLHeader.CATDrwEditSheetFromContextHdr.ShortHelp="编辑图纸";
CAT2DLHeader.CATDrwEditSheetFromContextHdr.Help="切换到图纸编辑器";
CAT2DLHeader.CATDrwEditSheetFromContextHdr.LongHelp="编辑图纸\n切换到图纸编辑器。";

CAT2DLHeader.CATDrwActivateMainViewHdr.Title="激活图纸";
CAT2DLHeader.CATDrwActivateMainViewHdr.ShortHelp="激活图纸";
CAT2DLHeader.CATDrwActivateMainViewHdr.Help="切换到图纸编辑器";
CAT2DLHeader.CATDrwActivateMainViewHdr.LongHelp="激活图纸\n切换到图纸编辑器。";

//------------------------------------------------------------------------------
// Background view from contextual menu
//------------------------------------------------------------------------------
CAT2DLHeader.CATDrwEditFrameFromContextHdr.Title="编辑背景";
CAT2DLHeader.CATDrwEditFrameFromContextHdr.ShortHelp="编辑背景";
CAT2DLHeader.CATDrwEditFrameFromContextHdr.Help="切换到框架和标题编辑器";
CAT2DLHeader.CATDrwEditFrameFromContextHdr.LongHelp="编辑背景\n切换到框架和标题编辑器。";

CAT2DLHeader.CATDrwActivateBackViewHdr.Title="激活背景";
CAT2DLHeader.CATDrwActivateBackViewHdr.ShortHelp="激活背景";
CAT2DLHeader.CATDrwActivateBackViewHdr.Help="切换到框架和标题编辑器";
CAT2DLHeader.CATDrwActivateBackViewHdr.LongHelp="激活背景\n切换到框架和标题编辑器。";

//------------------------------------------------------------------------------
// Background view from contextual menu
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLUpdate3DCmdHdr.Title="更新";
CAT2DLHeader.CAT2DLUpdate3DCmdHdr.ShortHelp="更新";
CAT2DLHeader.CAT2DLUpdate3DCmdHdr.Help="更新布局";
CAT2DLHeader.CAT2DLUpdate3DCmdHdr.LongHelp="更新\n更新布局。";

// Display view in 3D
CAT2DLHeader.CAT2DLDisplayViewIn3DHdr.Title="在 3D 中显示视图";
CAT2DLHeader.CAT2DLDisplayViewIn3DHdr.ShortHelp="在 3D 中显示视图";
CAT2DLHeader.CAT2DLDisplayViewIn3DHdr.Help="激活 3D 选项卡、转动视图使其与屏幕平行并根据视图显示区域定义重新构造";
CAT2DLHeader.CAT2DLDisplayViewIn3DHdr.LongHelp="在 3D 中显示视图\n激活 3D 选项卡、转动视图使其与屏幕平行并根据视图显示区域定义重新构造。";

// Axonometric
CAT2DLHeader.CAT2DLCreateAxonometricViewHdr.Title="轴测视图";
CAT2DLHeader.CAT2DLCreateAxonometricViewHdr.ShortHelp="轴测视图";
CAT2DLHeader.CAT2DLCreateAxonometricViewHdr.Help="创建轴测视图";
CAT2DLHeader.CAT2DLCreateAxonometricViewHdr.LongHelp="等轴测视图\n通过捕捉一个 3D 视点和可选的参考点来创建等轴测视图。";

//------------------------------------------------------------------------------
// View plane
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLViewPlaneHdr.Title="在 3D 中显示视图平面";
CAT2DLHeader.CAT2DLViewPlaneHdr.ShortHelp="在 3D 中显示视图平面";
CAT2DLHeader.CAT2DLViewPlaneHdr.Help="在 3D 窗口中显示视图平面、其法线向量和视图名称";
CAT2DLHeader.CAT2DLViewPlaneHdr.LongHelp="在 3D 中显示视图平面\n在 3D 窗口中显示视图平面、其法线向量和视图名称";

//------------------------------------------------------------------------------
// Set As Current Sheet Command (Contextual Menu)
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLActivateSheetHdr.Title="设置为当前";
CAT2DLHeader.CAT2DLActivateSheetHdr.ShortHelp="设置为当前";
CAT2DLHeader.CAT2DLActivateSheetHdr.Help="将选定的图纸设置为活动图纸";
CAT2DLHeader.CAT2DLActivateSheetHdr.LongHelp="将选定的图纸设置为活动图纸。";



//------------------------------------------------------------------------------
// Visu background mod
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLBckgndVisuModHdr.Title="3D 可视化";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.ShortHelp="3D 可视化";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.Help="定义如何在布局中可视化 3D 元素";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.LongHelp="3D 可视化\n定义如何在布局中可视化 3D 元素。";

CAT2DLHeader.CAT2DLBckgndVisuModHdr.NbIcon="5";

CAT2DLHeader.CAT2DLBckgndVisuModHdr.Title1="标准";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.ShortHelp1="标准";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.Help="显示 2D 和 3D 背景";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.LongHelp1="标准\n显示 2D 和 3D 背景。";

CAT2DLHeader.CAT2DLBckgndVisuModHdr.Title2="不可视";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.ShortHelp2="不可视";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.Help2="隐藏 2D 和 3D 背景";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.LongHelp2="不可视\n隐藏 2D 和 3D 背景。";

CAT2DLHeader.CAT2DLBckgndVisuModHdr.Title3="不可拾取";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.ShortHelp3="不可拾取";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.Help3="不允许在 2D 或 3D 背景中选择元素";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.LongHelp3="不可拾取\n不允许在 2D 或 3D 背景中选择元素。";

CAT2DLHeader.CAT2DLBckgndVisuModHdr.Title4="低亮度";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.ShortHelp4="低亮度";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.Help4="以低亮度在 2D 和 3D 背景中显示元素";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.LongHelp4="低亮度\n以低亮度在 2D 和 3D 背景中显示元素。";

CAT2DLHeader.CAT2DLBckgndVisuModHdr.Title5="不可拾取的低亮度";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.ShortHelp5="不可拾取的低亮度";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.Help5="以低亮度在 2D 和 3D 背景中显示不可选择的元素";
CAT2DLHeader.CAT2DLBckgndVisuModHdr.LongHelp5="不可拾取的低亮度\n以低亮度在 2D 和 3D 背景中显示不可拾取的元素。";


CAT2DLHeader.CAT2DLBckgndNoHdr.Title="不可视";
CAT2DLHeader.CAT2DLBckgndNoHdr.ShortHelp="不可视";
CAT2DLHeader.CAT2DLBckgndNoHdr.Help="隐藏 2D 和 3D 背景";
CAT2DLHeader.CAT2DLBckgndNoHdr.LongHelp="不可视\n隐藏 2D 和 3D 背景。";

CAT2DLHeader.CAT2DLBckgndHdr.Title="标准";
CAT2DLHeader.CAT2DLBckgndHdr.ShortHelp="标准";
CAT2DLHeader.CAT2DLBckgndHdr.Help="显示 2D 和 3D 背景";
CAT2DLHeader.CAT2DLBckgndHdr.LongHelp="标准\n显示 2D 和 3D 背景。";

CAT2DLHeader.CAT2DLBckgndNoPickHdr.Title="不可拾取";
CAT2DLHeader.CAT2DLBckgndNoPickHdr.ShortHelp="不可拾取";
CAT2DLHeader.CAT2DLBckgndNoPickHdr.Help="不允许在 2D 或 3D 背景中选择元素";
CAT2DLHeader.CAT2DLBckgndNoPickHdr.LongHelp="不可拾取\n不允许在 2D 或 3D 背景中选择元素。";

CAT2DLHeader.CAT2DLBckgndLowIntHdr.Title="低亮度";
CAT2DLHeader.CAT2DLBckgndLowIntHdr.ShortHelp="低亮度";
CAT2DLHeader.CAT2DLBckgndLowIntHdr.Help="以低亮度在 2D 和 3D 背景中显示元素";
CAT2DLHeader.CAT2DLBckgndLowIntHdr.LongHelp="低亮度\n以低亮度在 2D 和 3D 背景中显示元素。";

CAT2DLHeader.CAT2DLBckgndLowIntNoPickHdr.Title="不可拾取的低亮度";
CAT2DLHeader.CAT2DLBckgndLowIntNoPickHdr.ShortHelp="不可拾取的低亮度";
CAT2DLHeader.CAT2DLBckgndLowIntNoPickHdr.Help="以低亮度在 2D 和 3D 背景中显示不可选择的元素";
CAT2DLHeader.CAT2DLBckgndLowIntNoPickHdr.LongHelp="不可拾取的低亮度\n以低亮度在 2D 和 3D 背景中显示不可拾取的元素。";


//------------------------------------------------------------------------------
// Local update
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLLocalUpdateHdr.Title="本地更新";
CAT2DLHeader.CAT2DLLocalUpdateHdr.ShortHelp="本地更新";
CAT2DLHeader.CAT2DLLocalUpdateHdr.Help="更新特征";
CAT2DLHeader.CAT2DLLocalUpdateHdr.LongHelp="本地更新\n。更新特征";

//------------------------------------------------------------------------------
// Background clipping plane edition
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLBackClippingPlaneHdr.Title="编辑远端裁剪平面...";
CAT2DLHeader.CAT2DLBackClippingPlaneHdr.ShortHelp="编辑远端裁剪平面";
CAT2DLHeader.CAT2DLBackClippingPlaneHdr.Help="编辑远端裁剪平面";
CAT2DLHeader.CAT2DLBackClippingPlaneHdr.LongHelp="编辑远端裁剪平面";

//------------------------------------------------------------------------------
// Filter Edition
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLFilterEditionHdr.Title="编辑过滤器";
CAT2DLHeader.CAT2DLFilterEditionHdr.ShortHelp="编辑过滤器";
CAT2DLHeader.CAT2DLFilterEditionHdr.Help="编辑视图过滤器";
CAT2DLHeader.CAT2DLFilterEditionHdr.LongHelp="编辑过滤器\n编辑视图过滤器";

//------------------------------------------------------------------------------
// Activate clipping frame
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLActivateClipFrameHdr.Title="激活裁剪框架";
CAT2DLHeader.CAT2DLActivateClipFrameHdr.ShortHelp="激活裁剪框架";
CAT2DLHeader.CAT2DLActivateClipFrameHdr.Help="激活视图的裁剪框架";
CAT2DLHeader.CAT2DLActivateClipFrameHdr.LongHelp="激活裁剪框架\n激活视图的裁剪框架";

//------------------------------------------------------------------------------
// Deactivate clipping frame
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLDeactivateClipFrameHdr.Title="取消激活裁剪框架";
CAT2DLHeader.CAT2DLDeactivateClipFrameHdr.ShortHelp="取消激活裁剪框架";
CAT2DLHeader.CAT2DLDeactivateClipFrameHdr.Help="取消激活视图的裁剪框架";
CAT2DLHeader.CAT2DLDeactivateClipFrameHdr.LongHelp="取消激活裁剪框架\n取消激活视图的裁剪框架";

//------------------------------------------------------------------------------
// Activate back clippping plane
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLActivateBackClipPlaneHdr.Title="激活远端裁剪平面";
CAT2DLHeader.CAT2DLActivateBackClipPlaneHdr.ShortHelp="激活远端裁剪平面";
CAT2DLHeader.CAT2DLActivateBackClipPlaneHdr.Help="激活视图的远端裁剪平面";
CAT2DLHeader.CAT2DLActivateBackClipPlaneHdr.LongHelp="激活远端裁剪平面\n激活视图的远端裁剪平面";

//------------------------------------------------------------------------------
// Deactivate back clippping plane
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLDeactivateBackClipPlaneHdr.Title="取消激活远端裁剪平面";
CAT2DLHeader.CAT2DLDeactivateBackClipPlaneHdr.ShortHelp="取消激活远端裁剪平面";
CAT2DLHeader.CAT2DLDeactivateBackClipPlaneHdr.Help="取消激活视图的远端裁剪平面";
CAT2DLHeader.CAT2DLDeactivateBackClipPlaneHdr.LongHelp="取消激活远端裁剪平面\n取消激活视图的远端裁剪平面";

//------------------------------------------------------------------------------
// Change View Support
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLViewChangeViewSupportHdr.Title="更改视图支持面...";
CAT2DLHeader.CAT2DLViewChangeViewSupportHdr.ShortHelp="更改视图支持面";
CAT2DLHeader.CAT2DLViewChangeViewSupportHdr.Help="关联、隔离或反转已编辑视图的法向";
CAT2DLHeader.CAT2DLViewChangeViewSupportHdr.LongHelp="更改视图支持面\n关联、隔离或反转已编辑视图的法向";

//------------------------------------------------------------------------------
// Change View Support Axo
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLViewChangeAxoViewSupportHdr.Title="更改轴测视图支持面...";
CAT2DLHeader.CAT2DLViewChangeAxoViewSupportHdr.ShortHelp="更改轴测视图支持面";
CAT2DLHeader.CAT2DLViewChangeAxoViewSupportHdr.Help="更改轴测视图的视图支持面";
CAT2DLHeader.CAT2DLViewChangeAxoViewSupportHdr.LongHelp="更改轴测视图支持面\n更改轴测视图的视图支持面";

Semicolon=": ";

//------------------------------------------------------------------------------
// Section Hatching
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLHatchingSectionHdr.Title="根据材料显示截面";
CAT2DLHeader.CAT2DLHatchingSectionHdr.ShortHelp="根据材料显示截面";
CAT2DLHeader.CAT2DLHatchingSectionHdr.Help="根据材料显示截面";
CAT2DLHeader.CAT2DLHatchingSectionHdr.LongHelp="根据材料显示截面";


//------------------------------------------------------------------------------
// Activate 2D mode
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLActiv2DModeHdr.Title="激活 2D 可视化模式";
CAT2DLHeader.CAT2DLActiv2DModeHdr.ShortHelp="激活 2D 可视化模式";
CAT2DLHeader.CAT2DLActiv2DModeHdr.Help="激活视图的 2D 可视化模式。";
CAT2DLHeader.CAT2DLActiv2DModeHdr.LongHelp="激活 2D 可视化模式\n激活视图的 2D 可视化模式。";

//------------------------------------------------------------------------------
// Deactivate 2D mode
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLDeactiv2DModeHdr.Title="取消激活 2D 可视化模式";
CAT2DLHeader.CAT2DLDeactiv2DModeHdr.ShortHelp="取消激活 2D 可视化模式";
CAT2DLHeader.CAT2DLDeactiv2DModeHdr.Help="取消激活视图的 2D 可视化模式。";
CAT2DLHeader.CAT2DLDeactiv2DModeHdr.LongHelp="取消激活 2D 可视化模式\n取消激活视图的 2D 可视化模式。";

//------------------------------------------------------------------------------
// Clip view
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLClipViewHdr.Title="裁剪视图";
CAT2DLHeader.CAT2DLClipViewHdr.ShortHelp="裁剪视图";
CAT2DLHeader.CAT2DLClipViewHdr.Help="裁剪视图。";
CAT2DLHeader.CAT2DLClipViewHdr.LongHelp="裁剪视图\n裁剪视图。";

//------------------------------------------------------------------------------
// Unclip view
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLUnclipViewHdr.Title="取消裁剪视图";
CAT2DLHeader.CAT2DLUnclipViewHdr.ShortHelp="取消裁剪视图";
CAT2DLHeader.CAT2DLUnclipViewHdr.Help="取消裁剪视图。";
CAT2DLHeader.CAT2DLUnclipViewHdr.LongHelp="取消裁剪视图\n取消裁剪视图。";

//------------------------------------------------------------------------------
// Replace clip view
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLReplaceClipViewHdr.Title="替换裁剪";
CAT2DLHeader.CAT2DLReplaceClipViewHdr.ShortHelp="替换裁剪";
CAT2DLHeader.CAT2DLReplaceClipViewHdr.Help="替换裁剪。";
CAT2DLHeader.CAT2DLReplaceClipViewHdr.LongHelp="替换裁剪\n替换裁剪";

//------------------------------------------------------------------------------
// Modify clip view
//------------------------------------------------------------------------------
CAT2DLHeader.CAT2DLModifyClipViewHdr.Title="修改裁剪";
CAT2DLHeader.CAT2DLModifyClipViewHdr.ShortHelp="修改裁剪";
CAT2DLHeader.CAT2DLModifyClipViewHdr.Help="修改裁剪。";
CAT2DLHeader.CAT2DLModifyClipViewHdr.LongHelp="修改裁剪\n修改裁剪";

//--------------------------------------
// Toolbox view mode
//--------------------------------------
CAT2DLHeader.CAT2DLRenderStyleHdr.Title="视图模式";

//--------------------------------------
//  View/Nhr
//--------------------------------------
CAT2DLHeader.Nhr.Title="线框 (NHR)";
CAT2DLHeader.Nhr.Help="以线框模式显示几何图形";
CAT2DLHeader.Nhr.ShortHelp="线框 (NHR)";
CAT2DLHeader.Nhr.LongHelp="线框 (NHR)（“视图”菜单）
以线框模式显示几何图形。";
CAT2DLHeader.Nhr.Category="视图";

//--------------------------------------
//  View/Hlr
//--------------------------------------
CAT2DLHeader.Hlr.Title="移除隐藏线 (HLR)";
CAT2DLHeader.Hlr.Help="移除隐藏线后显示几何图形";
CAT2DLHeader.Hlr.ShortHelp="移除隐藏线 (HLR)";
CAT2DLHeader.Hlr.LongHelp="移除隐藏线 (HLR)（“视图”菜单）
移除隐藏线后显示几何图形。";
CAT2DLHeader.Hlr.Category="视图";

//--------------------------------------
//  View/Hrd
//--------------------------------------
CAT2DLHeader.Hrd.Title="动态移除隐藏线";
CAT2DLHeader.Hrd.Help="动态移除隐藏线后显示几何图形";
CAT2DLHeader.Hrd.ShortHelp="动态移除隐藏线";
CAT2DLHeader.Hrd.LongHelp="动态移除隐藏线（“视图”菜单）
动态移除隐藏线后显示几何图形。";
CAT2DLHeader.Hrd.Category="视图";

//--------------------------------------
//  View/Shading
//--------------------------------------
CAT2DLHeader.Shading.Title="着色 (SHD)";
CAT2DLHeader.Shading.Help="以着色模式显示几何图形";
CAT2DLHeader.Shading.ShortHelp="着色 (SHD)";
CAT2DLHeader.Shading.LongHelp="着色 (SHD)（“视图”菜单）
以着色模式显示几何图形。";
CAT2DLHeader.Shading.Category="视图";

//--------------------------------------
//  View/ShadingWithEdges
//--------------------------------------
CAT2DLHeader.ShadingWithEdges.Title="含边线着色";
CAT2DLHeader.ShadingWithEdges.Help="以含边线着色的模式显示几何图形视图";
CAT2DLHeader.ShadingWithEdges.ShortHelp="含边线着色";
CAT2DLHeader.ShadingWithEdges.LongHelp="带边着色（“视图”菜单）
显示含边线着色的几何图形。";
CAT2DLHeader.ShadingWithEdges.Category="视图";

//--------------------------------------
//  View/ShadingWithEdgesWithoutSmoothEdges
//--------------------------------------
CAT2DLHeader.ShadingWithEdgesWithoutSmoothEdges.Title="带边着色但不光顺边线";
CAT2DLHeader.ShadingWithEdgesWithoutSmoothEdges.Help="以带边着色但不光顺边线的模式显示几何图形视图";
CAT2DLHeader.ShadingWithEdgesWithoutSmoothEdges.ShortHelp="带边着色但不光顺边线";
CAT2DLHeader.ShadingWithEdgesWithoutSmoothEdges.LongHelp="带边着色但不光顺边线（“视图”菜单）
显示带边着色但不光顺边线的几何图形。";
CAT2DLHeader.ShadingWithEdgesWithoutSmoothEdges.Category="视图";

//--------------------------------------
//  View/ShadingWithEdgesAndHiddenEdges
//--------------------------------------
CAT2DLHeader.ShadingWithEdgesAndHiddenEdges.Title="含边线和隐藏边线着色";
CAT2DLHeader.ShadingWithEdgesAndHiddenEdges.Help="以含边线和隐藏边线着色的模式显示几何图形视图";
CAT2DLHeader.ShadingWithEdgesAndHiddenEdges.ShortHelp="含边线和隐藏边线着色";
CAT2DLHeader.ShadingWithEdgesAndHiddenEdges.LongHelp="带边和隐藏边着色（“视图”菜单）
显示含边线和隐藏边线着色的几何图形。";
CAT2DLHeader.ShadingWithEdgesAndHiddenEdges.Category="视图";

//--------------------------------------
//  View/ShadingWithTexture
//--------------------------------------
CAT2DLHeader.ShadingWithTexture.Title="含材料着色";
CAT2DLHeader.ShadingWithTexture.Help="以含材料着色的模式显示几何图形视图";
CAT2DLHeader.ShadingWithTexture.ShortHelp="含材料着色";
CAT2DLHeader.ShadingWithTexture.LongHelp="带材料着色（“视图”菜单）
显示含材料着色的几何图形。";
CAT2DLHeader.ShadingWithTexture.Category="视图";

//--------------------------------------
//  View/ShadingWithTextureAndEdges
//--------------------------------------
CAT2DLHeader.ShadingWithTextureAndEdges.Title="含材料和边线着色";
CAT2DLHeader.ShadingWithTextureAndEdges.Help="以含边线和边线着色的模式显示几何图形视图";
CAT2DLHeader.ShadingWithTextureAndEdges.ShortHelp="含材料和边线着色";
CAT2DLHeader.ShadingWithTextureAndEdges.LongHelp="带材料和边线着色（“视图”菜单）
显示含材料和边线着色的几何图形。";
CAT2DLHeader.ShadingWithTextureAndEdges.Category="视图";

//--------------------------------------
//  View/CustomizeViewMode
//--------------------------------------
CAT2DLHeader.CustomizeViewMode.Title="自定义视图";
CAT2DLHeader.CustomizeViewMode.Help="激活视图自定义模式";
CAT2DLHeader.CustomizeViewMode.ShortHelp="自定义视图参数";
CAT2DLHeader.CustomizeViewMode.LongHelp="自定义视图（“视图”菜单）
通过激活自定义视图模式，
您可以自定义视图参数。";
CAT2DLHeader.CustomizeViewMode.Category="视图";

//--------------------------------------
//  Isolate use edges
//--------------------------------------
CAT2DLHeader.CAT2DLIsolateUseEdgeHdr.Title="隔离使用边线";
CAT2DLHeader.CAT2DLIsolateUseEdgeHdr.Help="隔离使用边线";
CAT2DLHeader.CAT2DLIsolateUseEdgeHdr.ShortHelp="隔离使用边线";
CAT2DLHeader.CAT2DLIsolateUseEdgeHdr.LongHelp="隔离使用边线\n隔离使用边线。";

//--------------------------------------
//  Replace import
//--------------------------------------
CAT2DLHeader.CAT2DLReplaceImportHdr.Title="替换参考几何图形";
CAT2DLHeader.CAT2DLReplaceImportHdr.ShortHelp="替换参考几何图形";
CAT2DLHeader.CAT2DLReplaceImportHdr.Help="修改使用边线输入";
CAT2DLHeader.CAT2DLReplaceImportHdr.LongHelp="替换参考几何图形\n修改使用边线输入。";

//--------------------------------------
// Reframe
//--------------------------------------
CAT2DLHeader.Reframe.Title="适屏缩放";
CAT2DLHeader.Reframe.ShortHelp="适屏缩放";
CAT2DLHeader.Reframe.Help="重新构造查看器以适应图纸格式或几何图形区域内的所有可见元素。";
CAT2DLHeader.Reframe.LongHelp="适屏缩放\n重新构造查看器以适应图纸格式或几何图形区域内的所有可见元素。";

//--------------------------------------
//  Define In Work Object
//--------------------------------------
CAT2DLHeader.CAT2DLDefineInWorkObjectHdr.Title="定义工作对象";
CAT2DLHeader.CAT2DLDefineInWorkObjectHdr.ShortHelp="定义工作对象";
CAT2DLHeader.CAT2DLDefineInWorkObjectHdr.Help="将选定对象设置为工作对象。";
CAT2DLHeader.CAT2DLDefineInWorkObjectHdr.LongHelp="定义工作对象\n将选定对象设置为工作对象。";


//--------------------------------------
//  Add callout to other view
//--------------------------------------
CAT2DLHeader.CAT2DLCalloutCreationHdr.Title="在参考视图中创建标注";
CAT2DLHeader.CAT2DLCalloutCreationHdr.ShortHelp="在参考视图中创建标注";
CAT2DLHeader.CAT2DLCalloutCreationHdr.Help="在参考视图中创建标注";
CAT2DLHeader.CAT2DLCalloutCreationHdr.LongHelp="在参考视图中创建标注\n在参考视图中创建标注。";

//--------------------------------------
//  Boundaries
//--------------------------------------
CAT2DLHeader.CAT2DLBoundariesHideAllHdr.Title="遮盖边界";
CAT2DLHeader.CAT2DLBoundariesHideAllHdr.ShortHelp="遮盖边界";
CAT2DLHeader.CAT2DLBoundariesHideAllHdr.Help="遮盖截面的所有边界。";
CAT2DLHeader.CAT2DLBoundariesHideAllHdr.LongHelp="遮盖边界\n遮盖截面的所有边界。";

CAT2DLHeader.CAT2DLBoundariesShowAllHdr.Title="显示边界";
CAT2DLHeader.CAT2DLBoundariesShowAllHdr.ShortHelp="显示边界";
CAT2DLHeader.CAT2DLBoundariesShowAllHdr.Help="显示截面的所有边界。";
CAT2DLHeader.CAT2DLBoundariesShowAllHdr.LongHelp="显示边界\显示截面的所有边界。";

CAT2DLHeader.CAT2DLBoundariesHideFstHdr.Title="遮盖第一边界";
CAT2DLHeader.CAT2DLBoundariesHideFstHdr.ShortHelp="遮盖第一边界";
CAT2DLHeader.CAT2DLBoundariesHideFstHdr.Help="遮盖子截面视图的第一边界。";
CAT2DLHeader.CAT2DLBoundariesHideFstHdr.LongHelp="遮盖第一边界\n遮盖子截面视图的第一边界。";

CAT2DLHeader.CAT2DLBoundariesShowFstHdr.Title="显示第一边界";
CAT2DLHeader.CAT2DLBoundariesShowFstHdr.ShortHelp="显示第一边界";
CAT2DLHeader.CAT2DLBoundariesShowFstHdr.Help="显示子截面视图的第一边界。";
CAT2DLHeader.CAT2DLBoundariesShowFstHdr.LongHelp="显示第一边界\n显示子截面视图的第一边界。";

CAT2DLHeader.CAT2DLBoundariesHideSndHdr.Title="遮盖第二边界";
CAT2DLHeader.CAT2DLBoundariesHideSndHdr.ShortHelp="遮盖第二边界";
CAT2DLHeader.CAT2DLBoundariesHideSndHdr.Help="遮盖子截面视图的第二边界。";
CAT2DLHeader.CAT2DLBoundariesHideSndHdr.LongHelp="遮盖第二边界\n遮盖子截面视图的第二边界。";

CAT2DLHeader.CAT2DLBoundariesShowSndHdr.Title="显示第二边界";
CAT2DLHeader.CAT2DLBoundariesShowSndHdr.ShortHelp="显示第二边界";
CAT2DLHeader.CAT2DLBoundariesShowSndHdr.Help="显示子截面视图的第二边界。";
CAT2DLHeader.CAT2DLBoundariesShowSndHdr.LongHelp="显示第二边界\n显示子截面视图的第二边界。";

CAT2DLHeader.CAT2DLBoundaryHideHdr.Title="遮盖边界";
CAT2DLHeader.CAT2DLBoundaryHideHdr.ShortHelp="遮盖边界";
CAT2DLHeader.CAT2DLBoundaryHideHdr.Help="遮盖选定边界。";
CAT2DLHeader.CAT2DLBoundaryHideHdr.LongHelp="遮盖边界\n遮盖选定边界。";

CAT2DLHeader.CAT2DLResizeBoundariesHdr.Title="在视图背景上调整边界";
CAT2DLHeader.CAT2DLResizeBoundariesHdr.ShortHelp="在视图背景上调整边界";
CAT2DLHeader.CAT2DLResizeBoundariesHdr.Help="在视图背景上调整边界。";
CAT2DLHeader.CAT2DLResizeBoundariesHdr.LongHelp="在视图背景上调整边界\n在视图背景上调整边界。";

CAT2DLHeader.CAT2DLRestoreShowStateHdr.Title="恢复显示状态";
CAT2DLHeader.CAT2DLRestoreShowStateHdr.ShortHelp="恢复显示状态";
CAT2DLHeader.CAT2DLRestoreShowStateHdr.Help="恢复显示状态";
CAT2DLHeader.CAT2DLRestoreShowStateHdr.LongHelp="恢复显示状态\n恢复显示状态。";

CAT2DLHeader.CAT2DLCloseWndHdr.Title="关闭窗口";
CAT2DLHeader.CAT2DLCloseWndHdr.ShortHelp="关闭窗口";
CAT2DLHeader.CAT2DLCloseWndHdr.Help="关闭该窗口并激活机械零件。";
CAT2DLHeader.CAT2DLCloseWndHdr.LongHelp="关闭窗口\n关闭该窗口并激活机械零件。";

// View from profile
CAT2DLHeader.CAT2DLCreateMPSVHdr.Title="轮廓角度视图";
CAT2DLHeader.CAT2DLCreateMPSVHdr.ShortHelp="轮廓角度视图";
CAT2DLHeader.CAT2DLCreateMPSVHdr.Help="创建新剖视图、截面分割或辅助视图";
CAT2DLHeader.CAT2DLCreateMPSVHdr.LongHelp="轮廓角度视图\n创建新截面视图、截面分割或辅助视图。";

// Replace Profile
CAT2DLHeader.CAT2DLReplaceProfileHdr.Title="替换轮廓";
CAT2DLHeader.CAT2DLReplaceProfileHdr.ShortHelp="替换轮廓";
CAT2DLHeader.CAT2DLReplaceProfileHdr.Help="替换轮廓";
CAT2DLHeader.CAT2DLReplaceProfileHdr.LongHelp="替换轮廓。";

// Reverse Normal
CAT2DLHeader.CAT2DLReverseNormalHdr.Title="反转视图方向";
CAT2DLHeader.CAT2DLReverseNormalHdr.ShortHelp="反转视图方向";
CAT2DLHeader.CAT2DLReverseNormalHdr.Help="反转切除（或投影）方向";
CAT2DLHeader.CAT2DLReverseNormalHdr.LongHelp="反转视图方向\n反转切除（或投影）方向。";

// 3D Grid Automatic Size
CAT2DLHeader.CAT2DL3DGridAutoSizeCheckHdr.Title="自动网格大小";
CAT2DLHeader.CAT2DL3DGridAutoSizeCheckHdr.ShortHelp="自动网格大小";
CAT2DLHeader.CAT2DL3DGridAutoSizeCheckHdr.Help="定义是自动根据视图大小计算网格范围还是手动进行设置";
CAT2DLHeader.CAT2DL3DGridAutoSizeCheckHdr.LongHelp="自动网格大小\n定义是自动根据视图大小计算网格范围\n还是手动进行设置。";

// 3D Grid Edit Size
CAT2DLHeader.CAT2DL3DGridEditSizeHdr.Title="编辑网格大小";
CAT2DLHeader.CAT2DL3DGridEditSizeHdr.ShortHelp="编辑网格大小";
CAT2DLHeader.CAT2DL3DGridEditSizeHdr.Help="允许手动编辑网格大小";
CAT2DLHeader.CAT2DL3DGridEditSizeHdr.LongHelp="编辑网格大小\n允许手动编辑网格大小。";

// 3D Grid Reset Size
CAT2DLHeader.CAT2DLReset3DGridViewHdr.Title="重置为 3D 网格大小";
CAT2DLHeader.CAT2DLReset3DGridViewHdr.ShortHelp="重置为 3D 网格大小";
CAT2DLHeader.CAT2DLReset3DGridViewHdr.Help="重置网格大小以完整显示网格。";
CAT2DLHeader.CAT2DLReset3DGridViewHdr.LongHelp="重置为 3D 网格大小\n重置网格大小以完整显示网格。";

// Rendering style on view / No specific Rendering Style
CAT2DLHeader.CAT2DLNoSpecificRenderStyleOnViewCheckHdr.Title="没有特定的渲染样式";
CAT2DLHeader.CAT2DLNoSpecificRenderStyleOnViewCheckHdr.ShortHelp="没有特定的渲染样式";
CAT2DLHeader.CAT2DLNoSpecificRenderStyleOnViewCheckHdr.Help="重置视图上定义的渲染样式";
CAT2DLHeader.CAT2DLNoSpecificRenderStyleOnViewCheckHdr.LongHelp="重置渲染样式\n重置视图上定义的渲染样式，以便视图和布局查看器上定义的渲染样式将可视。";

// Rendering style on view / Shading
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingCheckHdr.Title="着色 (SHD)";
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingCheckHdr.ShortHelp="着色 (SHD)";
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingCheckHdr.Help="以着色模式显示几何图形";
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingCheckHdr.LongHelp="着色\n以着色模式显示几何图形。";

// Rendering style on view / Shading with Edges
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingEdgesCheckHdr.Title="含边线着色";
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingEdgesCheckHdr.ShortHelp="含边线着色";
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingEdgesCheckHdr.Help="以含边线着色的模式显示几何图形视图";
CAT2DLHeader.CAT2DLRenderStyleOnViewShadingEdgesCheckHdr.LongHelp="含边线着色\n显示含边线的着色几何图形。";

// Rendering style on view / Edges
CAT2DLHeader.CAT2DLRenderStyleOnViewEdgesCheckHdr.Title="线框 (NHR)";
CAT2DLHeader.CAT2DLRenderStyleOnViewEdgesCheckHdr.ShortHelp="线框 (NHR)";
CAT2DLHeader.CAT2DLRenderStyleOnViewEdgesCheckHdr.Help="以线框模式显示几何图形";
CAT2DLHeader.CAT2DLRenderStyleOnViewEdgesCheckHdr.LongHelp="线框 (NHR)\n以线框模式显示几何图形";

// Rendering style on view / Custom
CAT2DLHeader.CAT2DLRenderStyleOnViewCustomCheckHdr.Title="自定义样式";
CAT2DLHeader.CAT2DLRenderStyleOnViewCustomCheckHdr.ShortHelp="自定义样式参数";
CAT2DLHeader.CAT2DLRenderStyleOnViewCustomCheckHdr.Help="激活自定义样式";
CAT2DLHeader.CAT2DLRenderStyleOnViewCustomCheckHdr.LongHelp="自定义样式
激活自定义样式，
这使您可以自定义自己的样式参数。";

// View Layout
CAT2DLHeader.CAT2DLViewLayoutHdr.Title="FTA 中的视图布局";
CAT2DLHeader.CAT2DLViewLayoutHdr.ShortHelp="FTA 中的视图布局";
CAT2DLHeader.CAT2DLViewLayoutHdr.Help="根据 FTA 结构创建 2DL 视图，然后在不同图纸中自动布局这些视图。";
CAT2DLHeader.CAT2DLViewLayoutHdr.LongHelp="FTA 中的视图布局\n根据 FTA 结构（FTA 捕获和 FTA 视图）创建 2DL 视图，然后在不同图纸中自动布局这些视图。";
