//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGen
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet Generative
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   13.01.1999  Nouvelles options gendim
//      03           fgx   21.01.1999  Nouvelles options elements generes
//      04           fgx   28.01.1999  Ajout de GenDimFilter et GenDimAnal
//      05           fgx   05.02.1999  Ajout de GenDimAutoTrans
//      06           fgx   14.04.1999  Modif de GenGeo
//	  08		   lgk   26.06.2002  Revision
//	  09 	    	   lgk   15.10.02    Revision
//=====================================================================================

Title="生成";

frameGenDim.HeaderFrame.Global.Title="尺寸生成";
frameGenDim.HeaderFrame.Global.LongHelp=
"尺寸生成
定义是否根据 3D 零件的
约束生成尺寸。
可以生成以下约束：距离、
长度、角度、半径和直径。";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDim.Title="更新图纸时生成尺寸";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFilter.Title="生成前过滤";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFilter.LongHelp=
"生成前过滤
生成前显示“尺寸生成过滤器”
对话框。";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoPos.Title="生成后自动定位";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoPos.LongHelp=
"生成后自动定位
生成后自动定位尺寸。";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoTrans.Title="允许视图间的自动转移";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoTrans.LongHelp=
"允许视图间的自动转移
重新生成时，自动将尺寸转移至最佳视图。";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAnal.Title="生成后分析";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAnal.LongHelp=
"生成后分析
生成后显示“尺寸生成分析”
对话框。";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFromPart.Title="从包括在装配视图中的零件生成尺寸";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFromPart.LongHelp=
"从包括在装配视图中的零件生成尺寸
从包括在装配视图中的零件生成尺寸。";

frameGenDim.IconAndOptionsFrame.OptionsFrame.labelGenDimSemiAuto.Title="逐步生成之间的延迟： ";
frameGenDim.IconAndOptionsFrame.OptionsFrame.labelGenDimSemiAuto.LongHelp=
"逐步模式的生成之间的延迟
指定逐步生成尺寸时，
各尺寸生成之间的延迟。";



frameGenBal.HeaderFrame.Global.Title="零件序号生成";
frameGenBal.HeaderFrame.Global.LongHelp=
"定义生成零件序号时所使用的选项。";
frameGenBal.IconAndOptionsFrame.OptionsFrame.checkGenBalloonByInstance.Title="为每个产品实例创建零件序号";
frameGenBal.IconAndOptionsFrame.OptionsFrame.checkGenBalloonByInstance.LongHelp="为每个产品实例创建零件序号
为每个产品实例创建零件序号。";
