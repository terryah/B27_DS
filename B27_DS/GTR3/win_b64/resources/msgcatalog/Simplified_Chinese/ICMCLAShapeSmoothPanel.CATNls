DialogBoxTitle="光顺";

//Selection options ------------------------------------------------------------------------------------

LabelType.Title="类型： ";
LabelType.ShortHelp="选择类型";

//ComboBox options!
ComboType.Smooth="光顺";
ComboType.Reduction="减小";
ComboType.LongHelp="设置类型：
- 光顺 -
  将光顺计算应用到
选定元素。
- 减小-
  将数据减小计算
应用到选定元素。
  减少 减少公差范围内
 B 样条曲线、B 样条曲面和
B 样条面的
基础曲面。
可使用此方式移除不必要插入的段。";
ComboType.ShortHelp="选择类型";

LabelElement.Title="元素： ";
LabelElement.LongHelp="指定要光顺的元素。";
LabelElement.ShortHelp="选择元素";


//Smooth tab--------------------------------------------------------------------------------------------

TabPageSmooth.Title="光顺";

//Approximation frame-----------------------------------------------------------------------------------

FrameApproximation.Title="近似值";

//Type frame--------------------------------------------------------------------------------------------

FrameType.Title="类型";

RadioButtonEquidistant.Title="等距";
RadioButtonEquidistant.LongHelp="根据曲线参数
均匀分配给定的点。";
RadioButtonEquidistant.ShortHelp="等距点分布";

RadioButtonChordal.Title="弦";
RadioButtonChordal.LongHelp="假定给定点
之间的距离也可确定它们的参数化。";
RadioButtonChordal.ShortHelp="弦点分布";

//Priority frame----------------------------------------------------------------------------------------

FramePriority.Title="优先级";

RadioButtonVariation.Title="变化";
RadioButtonVariation.LongHelp="对相同的控制点分布提供优先级。";
RadioButtonVariation.ShortHelp="同等分布的优先级";

RadioButtonShape.Title="形";
RadioButtonShape.LongHelp="对输入几何图形的形状提供优先级。
对控制点分布提供较低的优先级。
保留的偏差尽可能小。";
RadioButtonShape.ShortHelp="形状优先级";

RadioButtonArcLength.Title="弧长";
RadioButtonArcLength.LongHelp="最小化控制点之间的距离。";
RadioButtonArcLength.ShortHelp="弧长优先级";

//Options frame-----------------------------------------------------------------------------------------

FrameOptions.Title="选项";

LabelAlong.Title="沿： ";
LabelAlong.LongHelp="设置将在其中应用光顺的方向。";
LabelAlong.ShortHelp="光顺方向";

//ComboBox options!
ComboAlong.Both="两者";
ComboAlong.U="U";
ComboAlong.V="V";
ComboAlong.LongHelp="计算 U 和 V 两个方向上（或仅一个方向上）的光顺。";
ComboAlong.ShortHelp="光顺方向";

LabelFactor.Title="系数： ";
LabelFactor.LongHelp="通过控制控制点之间
多边形直线长度的光顺系数
来影响近似值质量。";
LabelFactor.ShortHelp="光顺系数";

LabelBlend.Title="桥接： ";
LabelBlend.LongHelp="允许在原始几何图形和修改的几何图形之间进行连续转换。";
LabelBlend.ShortHelp="桥接";

LabelBlendMin.Title="0 ";
LabelBlendMax.Title=" 1";

SliderBlend.LongHelp="在原始几何图形和修改的几何图新之间桥接。";
SliderBlend.ShortHelp="桥接";


//Reduction tab-----------------------------------------------------------------------------------------

TabPageReduction.Title="减小";

CheckButtonReductionTypeDeviation.Title="偏差： ";
CheckButtonReductionTypeDeviation.LongHelp="如果最大偏差值
比此处指定的值小，将仅重新排列控制点。";
CheckButtonReductionTypeDeviation.ShortHelp="考虑偏差";

CheckButtonReductionTypeAngle.Title="角度： ";
CheckButtonReductionTypeAngle.LongHelp="如果终点切线的角度差
小于此处指定的值，将仅重新排列控制点。";
CheckButtonReductionTypeAngle.ShortHelp="考虑角度";

CheckButtonReductionKeepSeg.Title="保留分段";
CheckButtonReductionKeepSeg.LongHelp="保留段时减少控制点的数量。";
CheckButtonReductionKeepSeg.ShortHelp="保留段";


//Result frame------------------------------------------------------------------------------------------

FrameReductionResult.Title="结果";
FrameReductionResult.LongHelp="在减小之前显示
 U 和 V 方向上的控制点数和段数。";

LabelReductionResultControlPointsStat.Title="控制点： ";
LabelReductionResultControlPointsStat.ShortHelp="控制点数";

LabelReductionResultSegUStat.Title="U 方向段数： ";
LabelReductionResultSegUStat.ShortHelp="U 方向上的段数";

LabelReductionResultSegVStat.Title="V 方向段数： ";
LabelReductionResultSegVStat.ShortHelp="V 方向上的段数";


//More info---------------------------------------------------------------------------------------------

FrameReductionDisplay.Title="显示";

CheckButtonReductionDisplayDeviation.Title="偏差";
CheckButtonReductionDisplayDeviation.LongHelp="在“体验区域”中显示值。";
CheckButtonReductionDisplayDeviation.ShortHelp="显示偏差";

FrameDeviation.Title="最大偏差";
FrameDeviation.LongHelp="从输入几何图形显示
已创建的几何图形的偏差。";
