// COPYRIGHT ICEM Technologies 2005

//Frame Geometry to be imported
ImportOptions.HeaderFrame.Global.Title="要导入的几何图形";
ImportOptions.HeaderFrame.Global.LongHelp="请选择要导入的 ICEM DB 元素类型并
确定以哪种零件元素类型来转换它们。";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportPoints.Title="点";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportPoints.ShortHelp="将 ICEM DB 点转换为零件基准点。";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedPoints.Title="基准点";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedPoints.ShortHelp="将 ICEM DB 点转换为零件基准点。";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportRawData.Title="原始数据";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportRawData.ShortHelp="将 ICEM DB 原始数据转换为零件基准点云
或线的基准线";
Combo.MappedRawData.DatumPoints="基准点云";
Combo.MappedRawData.DatumSingle="线的基准线";
//ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedRawData.Title    = "Datum Line Wire";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportCurves.Title="曲线";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportCurves.ShortHelp="将 ICEM DB 曲线转换为零件基准曲线。";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedCurves.Title="基准曲线";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedCurves.ShortHelp="将 ICEM DB 曲线转换为零件基准曲线。";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportScans.Title="扫描";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportScans.ShortHelp="将 ICEM DB 扫描转换为零件基准网格或基准点云。";
Combo.MappedScans.DatumMesh="基准网格";
Combo.MappedScans.DatumPoints="基准点云";
//ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedScans.Title      = "Datum CloudData";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportSurfaces.Title="曲面";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportSurfaces.ShortHelp="将 ICEM DB 曲面转换为零件基准曲面。";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedSurfaces.Title="基准曲面";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedSurfaces.ShortHelp="将 ICEM DB 曲面转换为零件基准曲面。";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportFaces.Title="面";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportFaces.ShortHelp="将 ICEM DB 面转换为零件基准曲面。";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedFaces.Title="基准曲面";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedFaces.ShortHelp="将 ICEM DB 面转换为零件基准曲面。";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWires.Title="曲线复合";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWires.ShortHelp="将 ICEM DB 曲线复合转换为零件曲线接合、基准线或基准曲线。";
Combo.MappedWires.Join="曲线接合";
Combo.MappedWires.DatumSingle="基准线";
Combo.MappedWires.Split="基准曲线";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportShells.Title="盒体";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportShells.ShortHelp="将 ICEM DB 壳体转换为零件基准曲面接合、基准壳体或基准曲面。";
Combo.MappedShells.Join="曲面接合";
Combo.MappedShells.DatumSingle="基准壳体";
Combo.MappedShells.Split="基准曲面";
//ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedShells.Title     = "Shells";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWorkplanes.Title="工作平面";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWorkplanes.ShortHelp="将 ICEM DB 曲面转换为零件平面或轴系统。";
Combo.MappedWorkplanes.Plane="平面";
Combo.MappedWorkplanes.Axis="轴系";

// Frame Continuity Options
ContinuityOptions.HeaderFrame.Global.Title="边线曲线与独立曲线的连续优化";
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.NoOptimize.Title="无优化";
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.NoOptimize.ShortHelp="通过将元素在不连续点处剪切
使其适应建模器。";
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.AutoOptimize.Title="自动优化";
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.AutoOptimize.ShortHelp="通过自动控制几何变形
使元素适应建模器。";
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.ToleranceOptimize.Title="公差";
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.ToleranceOptimize.ShortHelp="在曲线和曲面的优化中
允许最大变形（以毫米为单位）。";

// Frame Attributes to be imported
ImportAttributes.HeaderFrame.Global.Title="要导入的属性";
ImportAttributes.HeaderFrame.Global.LongHelp="请导入在已导入 ICEM 数据库中定义的
颜色、图层、列表、选择集、光源和材料。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportColors.Title="颜色";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportColors.ShortHelp="导入 ICEM DB 颜色。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLayers.Title="图层";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLayers.ShortHelp="导入 ICEM DB 图层。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLists.Title="列表";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLists.ShortHelp="导入 ICEM DB 列表并将其转换为选择集。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportSelectionSets.Title="选择集";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportSelectionSets.ShortHelp="导入 ICEM DB 选择集。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLights.Title="光源";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLights.ShortHelp="导入 ICEM DB 光源。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportMaterials.Title="材料";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportMaterials.ShortHelp="导入 ICEM DB 材料。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.TexturePathes.Title="纹理路径";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.TexturePathes.ShortHelp="打开“纹理路径”对话框，
您可以在此添加和移除纹理路径。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.AllowGeomSetMaterial.Title="允许几何图形集中的材料";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.AllowGeomSetMaterial.ShortHelp="指定一个材料到几何图形集，
而所有材料属于展示的集。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.AllowGeomSetMaterial.LongHelp="您可以指定其他材料到子集以用此材料将其
显示。
如果所有二维元素组具有特定材料，
则常用材料属于相应几何图形集。
如果包含未分配的特定材料二维
几何图形对象，则没有材料属于该群组。
一种材料将仅在属于不同几何图形集材料时
才会附加到几何图形元素。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportAnnotations.Title="文本";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportAnnotations.ShortHelp="导入带引出线的 ICEM DB 文本。";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportViews.Title="视图";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportViews.ShortHelp="导入 ICEM DB 视图为相机。";

//Frame Additional Options
ImportAddOptions.HeaderFrame.Global.Title="其他选项";
ImportAddOptions.IconAndOptionsFrame.OptionsFrame.SkipIcemdbRootNode.Title="导入时跳过 ICEM DB 的根节点";
ImportAddOptions.IconAndOptionsFrame.OptionsFrame.Use2DCurveForC2TrimmedSurfaces.Title="导入 C2 连接修整的曲面时尝试使用二维曲线";
ImportAddOptions.IconAndOptionsFrame.OptionsFrame.ImportSurfaceAsPlane.Title="如果可能，将平面曲面导入为平面";

//Frame Geometry to be exported
ExportGeometry.HeaderFrame.Global.Title="要导出的几何图形";
ExportGeometry.HeaderFrame.Global.LongHelp="所有选中的 CATPart 元素类型都将保存到 ICEM DB 中。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPoints.Title="点";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPoints.ShortHelp="将零件点转换为 ICEM DB 点。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurves.Title="曲线";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurves.ShortHelp="将零件曲线转换为 ICEM DB 曲线。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaces.Title="曲面";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaces.ShortHelp="将零件曲面转换为 ICEM DB 面。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPointClouds.Title="点云";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPointClouds.ShortHelp="将零件点云转换为 ICEM DB 原始数据。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportMeshes.Title="网格";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportMeshes.ShortHelp="将零件网格转换为 ICEM DB 扫描。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaceJoins.Title="曲面接合";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaceJoins.ShortHelp="将零件曲面接合转换为 ICEM DB 壳体。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurveJoins.Title="曲线接合";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurveJoins.ShortHelp="将零件曲线接合转换为 ICEM DB 曲线复合。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportAxisSystems.Title="轴系";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportAxisSystems.ShortHelp="将零件轴系转换为 ICEM DB 工作平面。";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPlanes.Title="平面";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPlanes.ShortHelp="将零件平面转换为 ICEM DB 工作平面。";

//Frame Attributes to be exported
ExportAttributes.HeaderFrame.Global.Title="要导出的属性";
ExportAttributes.HeaderFrame.Global.LongHelp="零件中已定义的选定属性将被保存到 ICEM DB 中。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportColors.Title="颜色";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportColors.ShortHelp="导出零件颜色。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLayers.Title="图层";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLayers.ShortHelp="导出零件图层。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportSelectionSets.Title="选择集";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportSelectionSets.ShortHelp="导出零件选择集，并将其转换为 ICEM DB 列表。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportDisplaySets.Title="显示设置";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportDisplaySets.ShortHelp="导出零件显示集，并将其转换为 ICEM DB 列表。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLights.Title="光源";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLights.ShortHelp="导出零件光源。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportMaterials.Title="材料";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportMaterials.LongHelp="可以将材料与纹理数据一起导出。
只有使用 ICEM DB 版本 4.12 才能导出材料和纹理数据。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportMaterials.ShortHelp="导出零件材料。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.TextureFrame.ExportBrowse.Title="纹理路径";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.TextureFrame.ExportBrowse.LongHelp="打开用于选择纹理路径（将导出纹理图像至该路径）
的文件浏览器。路径显示在文本字段中。
如果该文本字段为空，则不导出任何纹理数据。
只有使用 ICEM DB 版本 4.12 才能导出材料和纹理数据。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.TextureFrame.ExportBrowse.ShortHelp="选择纹理路径";
ExportFolderChooser.Title="选择纹理路径";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportAnnotations.Title="带引出线的文本";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportAnnotations.ShortHelp="导出带引出线的文本。";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportViews.Title="视图";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportViews.ShortHelp="导出视图。";

//Frame Additional Export Options
ExportAddOptions.HeaderFrame.Global.Title="其他导出选项";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.IcemdbVersion.Title="ICEM DB 版本";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.IcemdbVersion.LongHelp="选择用于导出的 ICEM DB 版本。
只能将材料和纹理导出至 ICEM DB 版本 4.12。
如果选择 4.11，则“要导出的属性”区域中的
“材料”复选框和“纹理路径”按钮将被禁用，
因此将不会导出任何材料。";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.IcemdbVersion.ShortHelp="选择用于导出的 ICEM DB 版本";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.ExportHidden.Title="导出隐藏元素";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.ExportHidden.ShortHelp="还会将处于隐藏模式下的元素导出至 ICEM DB 中。";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.TruncateUnderlyingSurface.Title="剪切基础曲面";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.TruncateUnderlyingSurface.ShortHelp="从面边界处剪切基础曲面。";
