//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGeo
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet Geometry
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   20.01.1999  Ajout des options de visu des cst
//      03           fgx   20.01.1999  Deplacement des options de la grid de geo a gal
//      04           fgx   20.01.1999  Ajout de CreateCenter
//      05           fgx   22.02.1999  Ajout de CstCreation, retrait de CreateConstraint
//      06           fgx   23.08.1999  Manipulation directe de la geometrie
//      07           lgk   18.12.02    revision
//=====================================================================================

Title="几何图形";

frameCenter.HeaderFrame.Global.Title = "几何图形";
frameCenter.HeaderFrame.Global.LongHelp = "提供在几何图形中创建\n附加元素的功能。";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.Title = "创建圆心和椭圆中心";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.LongHelp = "创建圆和椭圆时\n创建圆心和椭圆中心。";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.Title = "拖动元素（包括其端点）";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.LongHelp = "拖动元素（包括其端点）。";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.Title = "复制生成的几何图形时创建端点";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.LongHelp = "复制从 3D 生成的几何图形时，创建端点。";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.Title = "在“工具控制板”中显示 H 和 V 字段";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.LongHelp = "创建 2D 几何图形或偏移元素时，\n在“工具控制板”中显示 H 和 V 字段。";

//drag elements
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.Title="允许直接操作";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.LongHelp="您可以使用鼠标移动几何图形。";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.Title="求解模式...";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.LongHelp = "您可以为移动元素定义求解模式。";

frameCstCreation.HeaderFrame.Global.Title="创建约束";
frameCstCreation.HeaderFrame.Global.LongHelp=
"创建检测到的和
基于特征的约束。";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.Title="创建检测到的和基于特征的约束";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.LongHelp=
"创建检测到的和
基于特征的约束。";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.Title="智能拾取...";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.LongHelp="动态检测逻辑约束。";

frameCstVisu.HeaderFrame.Global.Title="约束显示";
frameCstVisu.HeaderFrame.Global.LongHelp=
"为 2D 约束显示定义选项";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.LongHelp=
"为 2D 约束显示定义选项";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.Title="显示约束";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.LongHelp=
"使以下约束类型可视。";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.Title="参考大小： ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.LongHelp="定义用来显示约束符号
的参考大小。更改此参考大小将更改
所有约束展示的大小。";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.editorCstRefSize.LongHelp="定义用来显示约束符号
的参考大小。更改此参考大小将更改
所有约束展示的大小。";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.Title="约束颜色： ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.comboCstColor.LongHelp="定义用来显示约束符号的颜色。
更改它将更改所有约束展示
的颜色。";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.LongHelp="定义用来显示约束符号的颜色。
更改它将更改所有约束展示
的颜色。";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.Title="约束类型...";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.LongHelp="定义要显示的\n不同约束类型。";


frameGeomManipDirect.HeaderFrame.Global.Title="操作器";
frameGeomManipDirect.HeaderFrame.Global.LongHelp="定义是否必须通过单击激活操作器
才能移动几何图形。";

frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.Title="允许直接操作";
frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.LongHelp="允许直接操作
定义是否必须通过单击激活操作器
才能移动几何图形。";


frameDiagColors.HeaderFrame.Global.Title = "颜色";
frameDiagColors.HeaderFrame.Global.LongHelp = "颜色\n定义元素的颜色。";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.Title = "诊断的可视化";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.LongHelp = "在受约束的元素上\n定义诊断的可视化。";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.Title = "颜色...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.LongHelp = "定义诊断颜色。";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.Title = "元素的其他颜色";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.LongHelp = "定义元素的其他颜色。";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.Title = "颜色...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.LongHelp = "定义元素的其他颜色。";


// NLS push button solving mode dans onglet geometry
geoAllowGeomManip.Title="拖动元素";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.Title = "拖动元素";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.LongHelp = "定义移动元素方式。";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.Title = "拖动元素及其端点";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.LongHelp = "拖动元素及其端点。";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.Title = "求解模式";//for moving elements";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.Title = "标准模式";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.LongHelp = "移动元素时使用标准模式。";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.Title = "最小移动";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.LongHelp = "执行最小移动。";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.Title = "松弛";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.LongHelp = "使用松弛。";

// Push Button SmartPick
geoSmartPick.Title="智能拾取";
geoSmartPick.frameSmartPick.HeaderFrame.Global.Title="智能拾取";
geoSmartPick.frameSmartPick.HeaderFrame.Global.LongHelp=
"智能拾取
几何图形智能拾取。自动检测 2D 几何元素的位置。";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.Title="支持线和圆";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.LongHelp=
"检测支持线和圆。";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.Title="对齐";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.LongHelp=
"定义互相对齐的两个元素。";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.Title="平行、垂直和相切";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.LongHelp=
"定义两个元素相互平行、
垂直或相切。";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.Title="水平和垂直";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.LongHelp=
"定义两个元素相互水平或垂直。";

// Constraints types
geoTypeCst.Title="约束类型";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.Title="约束类型";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.LongHelp="定义要可视化的不同的\n约束类型。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.Title="水平";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.LongHelp="水平
显示水平约束。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.Title="垂直";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.LongHelp="垂直
显示垂直约束。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.Title="平行";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.LongHelp="平行
显示平行约束。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.Title="垂直";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.LongHelp="垂直
显示垂直约束。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.Title="同心";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.LongHelp="同心
显示同心约束。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.Title="相合";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.LongHelp="相合
显示相合约束。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.Title="相切";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.LongHelp="相切
显示相切约束。";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.Title="对称";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.LongHelp="对称
显示对称约束。";

// NLS de l'onglet Geometry pour le diagnostic des couleurs
geoVisuDiagColor.Title="诊断颜色";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.Title = "诊断颜色";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.LongHelp = "诊断颜色\n定义诊断颜色。";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.Title = "过分约束的元素";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.LongHelp = "过分约束的元素\n定义过分约束的模型中元素使用的颜色。";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.Title = "等约束元素";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.LongHelp = "等约束元素\n定义等约束模型中元素使用的颜色。";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.Title = "不一致的元素";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.LongHelp = "不一致的元素\n定义不一致模型中元素使用的颜色。";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.Title = "未更改的元素";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.LongHelp = "未更改的元素\n定义未更改元素使用的颜色。";

// NLS de l'onglet Geometry pour Other Colors
geoOtherColor.Title="其他元素的颜色";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.Title = "颜色";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.LongHelp = "颜色\n定义元素的颜色。";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.Title = "构造元素（适用于 V5R11 之前的工程图）";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.LongHelp = "构造元素\n定义 V5R11 之前工程图中构造元素使用的颜色。";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.Title = "智能拾取";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.LongHelp = "智能选取元素\n定义智能选取元素使用的颜色。";
