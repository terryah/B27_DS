//=====================================================================================
//                          COPYRIGHT DASSAULT SYSTEMES 2001
//                          CHECK AND CLEAN RULES FOR PARTDESIGN FEATURES
//=====================================================================================

Draft_0.ShortMessage="拔模 0 的基本规则";
Draft_0.ErrorMessage=" 用于拔模的 RSurs 特征应该聚集在零件几何体中。";
Draft_0.CleanMessage=" 用于拔模的 RSurs 特征现在已聚集在零件几何体中。";

Draft_1.ShortMessage="拔模 1 的基本规则";
Draft_1.ErrorMessage="默认拔模方向存在已损坏的数据。";
Draft_1.CleanMessage="已清除拔模方向的数据。";

Thread_0.ShortMessage="螺纹 0 的基本规则";
Thread_0.ErrorMessage="应断开至螺纹标准参考文件的链接。";
Thread_0.CleanMessage="现已断开至螺纹标准参考文件的链接。";

Mirror_0.ShortMessage="镜像 0 的基本规则";
Mirror_0.ErrorMessage="缺少一个或多个参考元素。";
Mirror_0.CleanMessage="已创建缺少的参考元素。";

DefaultValuesBag_0.ShortMessage="零件默认值包 0 的基本规则";
DefaultValuesBag_0.ErrorMessage="要移除的螺纹标准设计表。";
DefaultValuesBag_0.CleanMessage="已移除螺纹标准设计表。";

CleanPRTFeature_0.ShortMessage="清除 PRT 特征 0 的基本规则";
CleanPRTFeature_0.ErrorMessage="在特征“/p2”中找到未参考的 Brep 元素“/p1”。";
CleanPRTFeature_0.CleanMessage="已清除特征中未参考的 Brep 元素。";

EdgeFillet_0.ShortMessage="倒圆角 0 的基本规则";
EdgeFillet_0.ErrorMessage="一些半径对象（如边线上的点）设置在错误的圆角筋中或无支持面。";
EdgeFillet_0.CleanMessage="半径对象现已设置在正确的圆角筋中。";

Boolean_0.ShortMessage="布尔 0 的基本规则";
Boolean_0.ErrorMessage="（旧）实体和（新）混合几何体之间的布尔运算。
操作数几何体在布尔运算下显示，导致出现可视化问题。
为解决该问题，操作数几何体将被移到零件下。";
Boolean_0.CleanMessage="操作数几何体现已移至零件下";

Pattern_0.ShortMessage="阵列 0 的基本规则";
Pattern_0.ErrorMessage="用户模式的常规工具中存在冗余元素。";
Pattern_0.CleanMessage="已清除用户模式的常规工具中的冗余元素。";

Boolean_1.ShortMessage="布尔 1 的基本规则";
Boolean_1.ErrorMessage="两个几何体间的布尔运算未正确显示操作数几何体。
为更正此问题，将修改操作数几何体的显示位置。";
Boolean_1.CleanMessage="操作数几何体现已在正确位置显示。";

DeleteErrorBody_0.ShortMessage="删除错误几何体 0 的基本规则";
DeleteErrorBody_0.ErrorMessage="要删除的错误几何体";
DeleteErrorBody_0.CleanMessage="已删除错误几何体。";

DefaultValuesBag_1.ShortMessage="零件默认值包 1 的基本规则";
DefaultValuesBag_1.ErrorMessage="CATPart 中存在错误链接。";
DefaultValuesBag_1.CleanMessage="已移除错误链接。";

Prism_0.ShortMessage="棱柱 0 的基本规则";
Prism_0.ErrorMessage="CATPart 中存在错误链接。";
Prism_0.CleanMessage="已移除错误链接。";

Prism_1.ShortMessage="凹槽或凸台包含的作为自动编码数据的参考无效。";
Prism_1.ErrorMessage="包含的作为自动编码数据的参考无效。";
Prism_1.CleanMessage="自动编码数据已修复。";

Thread_1.ShortMessage="螺纹 1 的基本规则";
Thread_1.ErrorMessage="已为螺纹描述属性定义关系，因此需要更新螺纹描述属性。";
Thread_1.CleanMessage="已更新螺纹/孔的螺纹描述属性。";

Hole_0.ShortMessage="孔 0 的基本规则";
Hole_0.ErrorMessage="关系设置在无效的孔活动参数上。";
Hole_0.CleanMessage="关系现在设置在有效的孔活动参数上。";

Pattern_1.ShortMessage="阵列 1 的基本规则";
Pattern_1.ErrorMessage="阵列元素外表已损坏，常规工具中可能存在冗余元素。";
Pattern_1.CleanMessage="已在阵列的常规工具中更正了元素列表并清除了冗余元素。";

Boolean_2.ShortMessage="布尔 2 的基本规则";
Boolean_2.ErrorMessage="布尔特征的范围丢失。";
Boolean_2.CleanMessage="布尔特征的范围已创建。";

Pattern_2.ShortMessage="阵列 2 的基本规则";
Pattern_2.ErrorMessage="用户阵列中定位草图的点列表设置错误。";
Pattern_2.CleanMessage="用户阵列中定位草图的点列表设置正确。";

Pattern_3.ShortMessage="阵列 3 的基本规则";
Pattern_3.ErrorMessage="已创建实例的一个或多个草图未正确隔离。没有正确定位实例";
Pattern_3.CleanMessage="实例位置设置正确。";

Pattern_4.ShortMessage="阵列 4 的基本规则";
Pattern_4.ErrorMessage="未正确填充内部阵列工具的部件。";
Pattern_4.CleanMessage="已正确填充内部阵列工具的部件。";

Thread_2.ShortMessage="螺纹 2 的基本规则";
Thread_2.ErrorMessage="虽然被定义为“非标准”，但此螺纹包含无用描述。";
Thread_2.CleanMessage="已取消设置螺纹中的无用描述。";

Boolean_3.ShortMessage="布尔 3 的基本规则";
Boolean_3.ErrorMessage="特征是使用两个不同极性的实体在 V5-6R2012 与 V5-6R2014 之间创建的布尔运算。升级此运算可能会导致修改几何图形。";
Boolean_3_Add.ErrorMessage="特征是使用两个不同极性的实体在 V5-6R2014 中创建的布尔运算。升级此运算可能会导致修改几何图形。";

Pattern_5.ShortMessage="阵列 5 的基本规则";
Pattern_5.ErrorMessage="未正确设置矩形阵列的要阵列对象。";
Pattern_5.CleanMessage="已正确设置矩形阵列的要阵列对象。";

