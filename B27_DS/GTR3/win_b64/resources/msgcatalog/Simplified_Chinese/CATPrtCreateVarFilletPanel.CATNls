PanelVarFilletTitle="倒圆角定义";
PanelEdgeFilletTitle="倒圆角定义";
PanelChordalFilletTitle="弦圆角定义";

LongHelp="定义（可变）倒圆角";

FrmRoot.FrmMain.CkeLabel.Title="半径： ";
FrmRoot.FrmMain.ChordLabel.Title="弦长： ";

CkeLabel="半径： ";
ChordLabel="弦长： ";


FrmRoot.FrmMain.PropagationLabel.Title="传播：";
FrmMore.KeepEdgeFrame.RelimitObjectLabel.Title="要保留的边线： ";
FrmRoot.FrmMain.PointLabel.Title="点： ";
FrmRoot.FrmMain.VariationLabel.Title="变化：";
FrmRoot.FrmMain.BoundaryLabel.Title="端点：";
FrmRoot.FrmMain.OptionsFrame.LabOptionsSeparator.Title=" 选项 "; // @validatedUsage OZL 2013/10/30 option
FrmRoot.FrmMain.VariationFrame.LabVariationSeparator.Title=" 变化 ";

FrmRoot.FrmMain.VariationFrame.VariationSeparator2.Title=" 变化 ";

PanelVarFilletEdge="要圆角化的边线： ";
PanelEdgeFilletObject="要圆角化的对象： ";
PanelVarEdgeFilletKeepButton="保留边线";
PanelEdgeFilletSelectedFeatures="所选特征： ";
PanelEdgeFilletSelectedFeaturesMulti="所选特征";
PanelVarPoints="点：";

PanelKeepEdgeFillet="要保留的边线 ";


PanelVarFilletCubic="立方体";
PanelVarFilletLinear="线性";

PanelVarFilletTangency="相切";
PanelVarFilletMinimal="最小";
PanelVarFilletFreeEdges="内部边线";
PanelVarFilletPartAgainstCurrent="与当前实体相交";
PanelVarFilletPDGIntersection="相交";
PanelVarFilletGeneratedEdges="相交";
PanelVarFilletAgainstCore="与芯相交";
PanelVarFilletAgainstCavity="与腔体相交";
PanelVarFilletAgainstFeatures="与选定特征相交";

CreateBlendByEdges="按边线或顶点创建";
CreateBlendByVertex="按边线或顶点创建";
EditBlend="编辑... "; // @validatedUsage OZL 2013/10/30 edit
RemoveBlend="移除";
ReframeOn="居中";

PanelEdgeFilletUV="直线";
PanelEdgeFilletConnect="光顺";
PanelEdgeFilletMinimum="最小值";
PanelEdgeFilletMaximum="最大值";

PanelVarFilletVarButton="变量";
PanelConstFilletConstButton="常量";


FrmRoot.FrmMain.ReferenceElementLabel.Title="支持面： ";
FrmRoot.FrmMain.Trim.Title="修剪支持面";
FrmRoot.FrmMain.RelimitationMode.Title="修剪带";
FrmRoot.FrmMain.EditValuesPanel.Title="编辑圆角值"; // @validatedUsage OZL 2013/10/30 edit

PanelLimitingFillet="限制元素：";
PanelMultiLimitingFillet="限制元素：";

FrmMore.SpineFrame.CircleFillet.Title="圆弧圆角";
FrmMore.SpineFrame.SpineObjectLabel.Title="脊线：";

FrmMore.BlendFrame.SetbackValueLabel.Title="缩进距离： ";
FrmMore.BlendFrame.BlendVertex.Title="桥接曲面圆角 ";
FrmMore.BlendFrame.ObjectLabel.Title="桥接曲面圆角 ";
PanelMultiEdgeFilletEdges="圆角边线";
PanelMultiEdgeFilletObjects="圆角对象";
FrmMore.ChkSharpEdgeRemoveFrame.SharpEdgeRemove.Title="没有内部锐化边线";

LimitingElements="限制元素";
PointElements="指向元素";
PanelPartingFillets="分离元素";

FrmRoot.FrmMain.CkeLabel.LongHelp="如果是常量圆角，请指定半径值。
如果是可变圆角，请指定突出显示的半径值。
半径值必须大于
或等于 0。";
FrmRoot.FrmMain.ChordLabel.LongHelp="指定突出显示的弦长度值。
弦长度值必须大于
或等于 0。";

FrmRoot.FrmMain.RadiusFrame.ParamFrame.Spinner.LongHelp="如果是常量圆角，请指定半径值。
如果是可变圆角，请指定突出显示的半径值。
半径值必须大于
或等于 0。";
FrmRoot.FrmMain.RadiusFrame.ParamFrame.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="如果是常量圆角，请指定半径值。
如果是可变圆角，请指定突出显示的半径值。
半径值必须大于
或等于 0。";

FrmRoot.FrmMain.RadiusFrame.ChordalLength.Spinner.LongHelp="指定突出显示的弦长度值。
弦长度值必须大于
或等于 0。";
FrmRoot.FrmMain.RadiusFrame.ChordalLength.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="指定突出显示的弦长度值。
弦长度值必须大于
或等于 0。";
FrmRoot.FrmMain.RadiusFrame.EditValuesPanel.LongHelp="编辑圆角值"; // @validatedUsage OZL 2013/10/30 edit
FrmRoot.FrmMain.ObjectLabel.LongHelp="要圆角化的边线：
请指定要圆角化的锐化边线（如果是常量）、面。";
FrmRoot.FrmMain.ObjectFrame.Sel.Edt.LongHelp="要圆角化的边线：
请指定要圆角化的锐化边线（如果是常量）、面。";

FrmRoot.FrmMain.PropagationLabel.LongHelp="指定进行圆角化时要考虑的边线。
可以使用多种方法进行传播：

相切：在圆角化过程中，应用程序会持续
在所选边线之外执行圆角化，直到遇到一条
相切不连续的边线为止。
最小：几何图形将一直传播到
第一自然重新限定。
相交（GSD 倒圆角）：若单击激活（相切）相交边线图标，
相交边线将定义圆角。
相交（PGD 倒圆角）：选定边线是选定特征与
当前实体的相交边线。";

FrmRoot.FrmMain.PropagationCombo.LongHelp="指定进行圆角化时要考虑的边线。
可以使用多种方法进行传播：

相切：在圆角化过程中，应用程序会持续
在所选边线之外执行圆角化，直到遇到一条
相切不连续的边线为止。
最小：几何图形将一直传播到
第一自然重新限定。
相交（GSD 倒圆角）：若单击激活（相切）相交边线图标，
相交边线将定义圆角。
相交（PGD 倒圆角）：选定边线是选定特征与
当前实体的相交边线。";

FrmRoot.FrmMain.PointLabel.LongHelp="指定圆角将要
经过的点。 
单击选定边线上的
任一点可创建此点。";
FrmRoot.FrmMain.PointFrame.Sel.Edt.LongHelp="指定圆角将要
经过的点。 
单击选定边线上的
任一点可创建此点。";

FrmRoot.FrmMain.VariationLabel.LongHelp="指定圆角
在不同的值之间的更改方式。

可以使用多种变化方式：
三次曲线：圆角逐渐
从一个值更改到另一个值。
线性：圆角直接
从一个值更改到另一个值。";
FrmRoot.FrmMain.VariationCombo.LongHelp="指定圆角
在不同的值之间的更改方式。

可以使用多种变化方式：
三次曲线：圆角逐渐
从一个值更改到另一个值。
线性：圆角直接
从一个值更改到另一个值。";

FrmRoot.FrmMain.RadiusFrame.ParamFrame.Spinner.Title="半径";
FrmRoot.FrmMain.RadiusFrame.ParamFrame.EnglobingFrame.IntermediateFrame.Spinner.Title="半径";
FrmRoot.FrmMain.RadiusFrame.ChordalLength.Spinner.Title="弦长";
FrmRoot.FrmMain.RadiusFrame.ChordalLength.EnglobingFrame.IntermediateFrame.Spinner.Title="弦长";

FrmMore.BlendFrame.ParamFrame.EnglobingFrame.IntermediateFrame.Spinner.Title="缩进";

FrmRoot.FrmMain.BoundaryLabel.LongHelp="指定将盒体圆角化时
要考虑的边界重新限定模式。";

FrmRoot.FrmMain.BoundaryCombo.LongHelp="指定将盒体圆角化时
要考虑的边界重新限定模式。";

FrmRoot.FrmMain.ReferenceElementFrame.LongHelp="指定要对盒体执行圆角化的几何体。";

FrmMore.KeepEdgeFrame.RelimitObjectLabel.LongHelp="在圆角化边线时，\n根据指定半径值\n圆角可能影响\n其它不需要圆角化的\n零件的边线。若预计会出现\n此情况，请在确认圆角化\n操作之前通过填写“要保留的边线”\n字段指定不需要圆角化的边线。";
FrmMore.KeepEdgeFrame.RelimitObjectFrame.Sel.Edt.LongHelp="指定边线，对所选对象进行圆角化时
不可修改该边线。";

FrmRoot.FrmMain.RelimitationMode.LongHelp="修剪支持面并将它装配到圆角";

FrmMore.PartingFrame.PartingObjectFrame.LongHelp="指定限定元素。";

FrmMore.BlendFrame.BlendObjectFrame.LongHelp="指定要桥接的圆角。";
FrmMore.BlendFrame.BlendVertex.LongHelp="指定圆角化时要修改的圆角
按下列方式桥接圆角：
  
单击“桥接圆角”按钮
可以重塑圆角化操作中被修改的圆角的外形。
圆角是三个或更多边线的公共点的集合，
这些边线已被选中作为圆角化的对象。";

FrmMore.PartingElementFrame.PartingObjectElementFrame.LongHelp="指定分离元素";

FrmRoot.FrmMain.Conic.Title="二次曲线参数：";
FrmRoot.FrmMain.Conic.LongHelp="指定二次曲线参数。
二次曲线参数必须大于 0（不包括 0）
且小于 1（不包括 1）。";
FrmRoot.FrmMain.ConicParm.Spinner.LongHelp="指定二次曲线参数。
二次曲线参数必须大于 0（不包括 0）
且小于 1（不包括 1）。";
FrmRoot.FrmMain.ConicParm.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="指定二次曲线参数。
二次曲线参数必须大于 0（不包括 0）
且小于 1（不包括 1）。";

ImplicitMode="不能在二次曲线模式中使用隐式保留边线模式，\n强制二次曲线模式将移除隐式模式。要执行此更改吗？";

FrmRoot.FrmMain.CkeLabel.Title="半径：";

FrmRoot.FrmMain.ObjectFrameMulti.LongHelp="要圆角化的对象：\n指定锐边，\n如果是常量圆角，则指定面或特征。";

FrmMore.KeepEdgeFrame.RelimitObjectFrameMulti.LongHelp="在圆角化边线时，\n根据指定半径值\n圆角可能影响\n其它不需要圆角化的\n零件的边线。若预计会出现\n此情况，请在确认圆角化\n操作之前通过填写“要保留的边线”\n字段指定不需要圆角化的边线。";

FrmMore.PartingFrame.PanelMultiLimitingFillet.LongHelp="指定限定元素。";

FrmMore.PartingElementFrame.PanelPartingFillets.LongHelp="指定分离元素。";

FrmMore.BlendFrame.ObjectLabel.LongHelp="指定要桥接的圆角。";

FrmMore.BlendFrame.SetbackValueLabel.LongHelp="后退距离。";

FrmMore.SpineFrame.CircleFillet.LongHelp="若要圆角化切线\n不连续但在逻辑上\n为单条边线的连续\n边线，则可以使用\n脊线来完成。圆角化\n此类边线时，\n该软件将使用\n脊线法向平面中\n的圆。这样便可\n控制圆角的形状。";

FrmMore.ChkSharpEdgeRemoveFrame.SharpEdgeRemove.LongHelp="计算可变半径圆角时，\n若要连接的曲面是\n切线连续但不是曲率连续，\n则该软件可能生成\n意料之外的锐化边线。\n为了改善设计，\n此选项移除了所有可能生成的边线。"; // @validatedUsage OZL 2013/10/30 option

FrmRoot.FrmMain.PointFrame.LongHelp="指定圆角将穿过的点。\n在选定边线上单击即可创建点。";

FrmMore.SpineFrame.SpineObjectLabel.LongHelp="若要圆角化\n切线不连续\n但在逻辑上为\n单条边线的连续\n边线，则可以使用\n脊线来完成。";

FrmMore.SpineFrame.SpineObjectFrame.LongHelp="若要圆角化\n切线不连续\n但在逻辑上为\n单条边线的连续\n边线，则可以使用\n脊线来完成。";
