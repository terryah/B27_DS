// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRdgShootingQualityFrame
//
//===========================================================================

//--------------------------------------
// Raytracing frame
//--------------------------------------
raytracingFrame.HeaderFrame.Global.Title = "渲染";

raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingCheck.Title = "射线跟踪：";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingCheck.Help = "切换到射线跟踪模式";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingCheck.ShortHelp = "射线跟踪模式";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingCheck.LongHelp =
"打开/关闭射线跟踪模式。";

raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReflectionsLabel.Title = "反射：";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReflectionsSpinner.Help = "定义反射次数";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReflectionsSpinner.ShortHelp = "反射次数";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReflectionsSpinner.LongHelp =
"定义光线的最大反射次数。";

raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingRefractionsLabel.Title = "折射：";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingRefractionsSpinner.Help = "定义折射次数";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingRefractionsSpinner.ShortHelp = "折射次数";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingRefractionsSpinner.LongHelp =
"定义光线的最大折射次数。";

raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReboundsLabel.Title = "反弹：";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReboundsSpinner.Help = "定义反弹次数";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReboundsSpinner.ShortHelp = "反弹次数";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingReboundsSpinner.LongHelp =
"定义光线的
最大反弹次数（反射次数和折射次数）";

raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingTexturesCheck.Title = "显示结构";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingTexturesCheck.Help = "切换结构渲染";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingTexturesCheck.ShortHelp = "切换结构渲染";
raytracingFrame.IconAndOptionsFrame.OptionsFrame.raytracingParametersFrame.raytracingTexturesCheck.LongHelp =
"打开/关闭结构渲染";

//--------------------------------------
// Shadows frame
//--------------------------------------
shadowsFrame.HeaderFrame.Global.Title = "阴影";

shadowsFrame.IconAndOptionsFrame.OptionsFrame.shadowsParametersFrame.shadowsCheck.Title = "显示阴影";
shadowsFrame.IconAndOptionsFrame.OptionsFrame.shadowsParametersFrame.shadowsCheck.Help = "切换阴影计算";
shadowsFrame.IconAndOptionsFrame.OptionsFrame.shadowsParametersFrame.shadowsCheck.ShortHelp = "切换阴影计算";
shadowsFrame.IconAndOptionsFrame.OptionsFrame.shadowsParametersFrame.shadowsCheck.LongHelp =
"打开/关闭阴影计算。";

//--------------------------------------
// Antialiasing frame
//--------------------------------------
aliasingFrame.HeaderFrame.Global.Title = "精确度";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedRadio.Title = "预定义：";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedRadio.Help = "预定义的质量";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedRadio.ShortHelp = "预定义的质量";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedRadio.LongHelp =
"预定义的渲染质量。";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomRadio.Title = "自定义：";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomRadio.Help = "自定义质量";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomRadio.ShortHelp = "自定义质量";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomRadio.LongHelp =
"抗锯齿参数包括最小采样、最大采样和对比度。";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingQualityLowLabel.Title = "低";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingQualityHighLabel.Title = "高";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingQualitySlider.Help = "定义渲染质量";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingQualitySlider.ShortHelp = "渲染质量";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingQualitySlider.LongHelp =
"定义渲染质量。";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingCheck.Title = "抗锯齿";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingCheck.Help = "切换抗锯齿模式";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingCheck.ShortHelp = "切换抗锯齿模式";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingPredefinedFrame.aliasingCheck.LongHelp =
"打开/关闭抗锯齿模式。";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingMinSampleCombo.Help = "最小采样数";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingMinSampleCombo.ShortHelp = "最小采样数";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingMinSampleCombo.LongHelp =
"设置 1 个像素、4 个像素或 16 个像素的最小射线投射次数。";

minSample1 = "16 平方像素";
minSample2 = "4 平方像素";
minSample3 = "像素";
minSample4 = "1/4 平方像素";
minSample5 = "1/16 平方像素";
minSample6 = "1/64 平方像素";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingCustomText1Label.Title = "在每个角落投射一束射线";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingCustomText2Label.Title = ".";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingCustomText3Label.Title = "若对应的颜色与每个 RGB 部件的颜色相差甚远，则";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingCustomText4Label.Title = "可将方块细分为 4 个，";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingCustomText5Label.Title = "最多不得超过";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingCustomText6Label.Title = ".";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingMaxSampleCombo.Help = "最大采样数";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingMaxSampleCombo.ShortHelp = "最大采样数";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingMaxSampleCombo.LongHelp =
"设置 1 个像素、4 个像素或 16 个像素的最大射线投射次数。";

maxSample1 = "每 16 个像素投射 1 束射线";
maxSample2 = "每 4 个像素投射 1 束射线";
maxSample3 = "每个像素投射 1 束射线";
maxSample4 = "每个像素投射 4 束射线";
maxSample5 = "每个像素投射 16 束射线";
maxSample6 = "每个像素投射 64 束射线";

aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingContrastSpinner.Help = "未过采样的对比度阈值";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingContrastSpinner.ShortHelp = "过采样的对比度阈值";
aliasingFrame.IconAndOptionsFrame.OptionsFrame.aliasingParametersFrame.aliasingCustomFrame.aliasingContrastSpinner.LongHelp =
"定义自适应过采样的对比度阈值，也就是说，
低于像素间的最大对比度阈值就不会
出现过采样问题。若当前计算的像素和相邻像素间的 RGB 部件的对比度值大于该阈值，
则像素
会出现过采样问题。
注意：该值越小，过采样频率越高，
渲染时间越长。";


