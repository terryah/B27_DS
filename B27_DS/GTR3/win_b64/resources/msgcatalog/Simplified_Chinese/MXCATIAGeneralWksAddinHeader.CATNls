// COPYRIGHT DASSAULT SYSTEMES 2007

//===========================================================================
// Message catalog for the MXCATIAGeneralWksAddinHeader command headers of the
// general workshop addin.
//
// There are two setps of NLS keys
// One set dedicated to 10.7.1 (or older) servers
// Another set dedicated to V6 (or later) servers
//===========================================================================

//===========================================================================
// V6 server
//===========================================================================

//Connect
MXCATIAGeneralWksAddinHeader.V6CATIAConnectHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAConnectHdr.Title="连接至 V6...";
MXCATIAGeneralWksAddinHeader.V6CATIAConnectHdr.ShortHelp="连接至 V6";
MXCATIAGeneralWksAddinHeader.V6CATIAConnectHdr.Help="开始或启动 ENOVIA V6 会话";
MXCATIAGeneralWksAddinHeader.V6CATIAConnectHdr.LongHelp="开始或启动 ENOVIA V6 会话";

//R1X4CT5010 Implementation: START
// Quick Check In Active Model
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionActiveHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionActiveHdr.Title="活动模型";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionActiveHdr.ShortHelp="快速签入";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionActiveHdr.Help="单击将活动模型签入到 ENOVIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionActiveHdr.LongHelp="单击将活动模型签入到 ENOVIA 中";

// Quick Check In All open Models
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionAllHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionAllHdr.Title="所有打开的模型";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionAllHdr.ShortHelp="单击签入所有打开的模型";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionAllHdr.Help="单击将所有打开的模型签入到 ENOVIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinNoOptionAllHdr.LongHelp="单击将所有打开的模型签入到 ENOVIA 中";
//R1X4CT5010 Implementation: END

// Checkin Active Model
MXCATIAGeneralWksAddinHeader.V6CATIACheckinActiveHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinActiveHdr.Title="活动模型...";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinActiveHdr.ShortHelp="签入";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinActiveHdr.Help="将活动模型签入到 ENOVIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinActiveHdr.LongHelp="将活动模型签入到 ENOVIA 中";

// Checkin all open Models
MXCATIAGeneralWksAddinHeader.V6CATIACheckinAllHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinAllHdr.Title="所有打开的模型...";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinAllHdr.ShortHelp="签入所有打开的模型";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinAllHdr.Help="将所有打开的模型签入到 ENOVIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckinAllHdr.LongHelp="将所有打开的模型签入到 ENOVIA 中";

//Open Load
MXCATIAGeneralWksAddinHeader.V6CATIAOpenLoadHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenLoadHdr.Title="加载";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenLoadHdr.ShortHelp="打开并加载";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenLoadHdr.Help="打开模型并将其加载到 CATIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenLoadHdr.LongHelp="打开模型并将其加载到 CATIA 中";

//Open Insert
MXCATIAGeneralWksAddinHeader.V6CATIAOpenInsertHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenInsertHdr.Title="插入";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenInsertHdr.ShortHelp="打开并插入";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenInsertHdr.Help="打开模型并将其插入到 CATIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIAOpenInsertHdr.LongHelp="打开模型并将其插入到 CATIA 中";

//Checkout Load
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutLoadHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutLoadHdr.Title="加载...";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutLoadHdr.ShortHelp="签出";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutLoadHdr.Help="签出模型并将其加载到 CATIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutLoadHdr.LongHelp="签出模型并将其加载到 CATIA 中";

//Checkout Insert
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutInsertHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutInsertHdr.Title="插入...";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutInsertHdr.ShortHelp="签出并插入";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutInsertHdr.Help="签出模型并将其插入到 CATIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckoutInsertHdr.LongHelp="签出模型并将其插入到 CATIA 中";

//LockUnlock
MXCATIAGeneralWksAddinHeader.V6CATIALockUnlockHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIALockUnlockHdr.Title="锁定/解锁...";
MXCATIAGeneralWksAddinHeader.V6CATIALockUnlockHdr.ShortHelp="在 ENOVIA 锁定/解锁活动设计";
MXCATIAGeneralWksAddinHeader.V6CATIALockUnlockHdr.Help="在 ENOVIA 锁定/解锁活动设计";
MXCATIAGeneralWksAddinHeader.V6CATIALockUnlockHdr.LongHelp="在 ENOVIA 锁定/解锁活动设计";

//Attrib Synch From matrix -> Active File
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchMxToCadHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchMxToCadHdr.Title="活动文件...";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchMxToCadHdr.ShortHelp="将活动文件的属性从 ENOVIA 同步到 CATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchMxToCadHdr.Help="将活动文件的属性从 ENOVIA 同步到 CATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchMxToCadHdr.LongHelp="将活动文件的属性从 ENOVIA 同步到 CATIA";

//Attrib Synch From matrix -> Active Structure
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchMxToCadHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchMxToCadHdr.Title="活动结构";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchMxToCadHdr.ShortHelp="将活动结构的属性从 ENOVIA 同步到 CATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchMxToCadHdr.Help="将活动结构的属性从 ENOVIA 同步到 CATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchMxToCadHdr.LongHelp="将活动结构的属性从 ENOVIA 同步到 CATIA";

//Attrib Synch To matrix -> Active File
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchCadToMxHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchCadToMxHdr.Title="活动文件...";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchCadToMxHdr.ShortHelp="将活动文件的属性同步到 ENOVIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchCadToMxHdr.Help="将活动文件的属性同步到 ENOVIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveFileAttrSynchCadToMxHdr.LongHelp="将活动文件的属性同步到 ENOVIA";

//Attrib Synch To matrix -> Active Structure
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchCadToMxHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchCadToMxHdr.Title="活动结构";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchCadToMxHdr.ShortHelp="将活动结构的属性同步到 ENOVIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchCadToMxHdr.Help="将活动结构的属性同步到 ENOVIA";
MXCATIAGeneralWksAddinHeader.V6CATIAActiveStructureAttrSynchCadToMxHdr.LongHelp="将活动结构的属性同步到 ENOVIA";

//List Versions
MXCATIAGeneralWksAddinHeader.V6CATIAVersionsHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAVersionsHdr.Title="列出版本...";
MXCATIAGeneralWksAddinHeader.V6CATIAVersionsHdr.ShortHelp="列出版本";
MXCATIAGeneralWksAddinHeader.V6CATIAVersionsHdr.Help="在 ENOVIA 中显示修订版/版本";
MXCATIAGeneralWksAddinHeader.V6CATIAVersionsHdr.LongHelp="在 ENOVIA 中显示修订版/版本";
V6RevisionsTitle="列出修订版...";
V6RevisionsShortHelp="列出修订版";

//Recently Accessed Designs
MXCATIAGeneralWksAddinHeader.V6CATIARecentlyAccessedDesignsHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIARecentlyAccessedDesignsHdr.Title="最近访问的设计...";
MXCATIAGeneralWksAddinHeader.V6CATIARecentlyAccessedDesignsHdr.ShortHelp="最近访问的设计";
MXCATIAGeneralWksAddinHeader.V6CATIARecentlyAccessedDesignsHdr.Help="在 ENOVIA 中列出最近访问的设计";
MXCATIAGeneralWksAddinHeader.V6CATIARecentlyAccessedDesignsHdr.LongHelp="在 ENOVIA 中列出最近访问的设计";



//Recognize Version
MXCATIAGeneralWksAddinHeader.V6CATIARecognizeVersionHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIARecognizeVersionHdr.Title="识别版本...";
MXCATIAGeneralWksAddinHeader.V6CATIARecognizeVersionHdr.ShortHelp="识别版本";
MXCATIAGeneralWksAddinHeader.V6CATIARecognizeVersionHdr.Help="在 ENOVIA 中识别活动模型的版本";
MXCATIAGeneralWksAddinHeader.V6CATIARecognizeVersionHdr.LongHelp="在 ENOVIA 中识别活动模型的版本";

//Check For Update -> All
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateAllHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateAllHdr.Title="所有...";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateAllHdr.ShortHelp="检查以更新所有";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateAllHdr.Help="检查以将所有打开的模型更新到 ENOVIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateAllHdr.LongHelp="检查以将所有打开的模型更新到 ENOVIA 中";

//Check For Update -> Current
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateCurrentHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateCurrentHdr.Title="当前...";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateCurrentHdr.ShortHelp="检查更新";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateCurrentHdr.Help="检查以将当前模型更新到 ENOVIA 中";
MXCATIAGeneralWksAddinHeader.V6CATIACheckForUpdateCurrentHdr.LongHelp="检查以将当前模型更新到 ENOVIA 中";

//Start Design
MXCATIAGeneralWksAddinHeader.V6CATIAStartDesignHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAStartDesignHdr.Title="开始设计...";
MXCATIAGeneralWksAddinHeader.V6CATIAStartDesignHdr.ShortHelp="开始设计";
MXCATIAGeneralWksAddinHeader.V6CATIAStartDesignHdr.Help="从 ENOVIA 中开始新设计并将其签出";
MXCATIAGeneralWksAddinHeader.V6CATIAStartDesignHdr.LongHelp="从 ENOVIA 中开始新设计并将其签出";

//Manage Workspace
MXCATIAGeneralWksAddinHeader.V6CATIAManageWorkspaceHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAManageWorkspaceHdr.Title="管理工作区...";
MXCATIAGeneralWksAddinHeader.V6CATIAManageWorkspaceHdr.ShortHelp="启动工作区管理器";
MXCATIAGeneralWksAddinHeader.V6CATIAManageWorkspaceHdr.Help="启动工作区管理器";
MXCATIAGeneralWksAddinHeader.V6CATIAManageWorkspaceHdr.LongHelp="启动工作区管理器";

// Disconnect
MXCATIAGeneralWksAddinHeader.V6CATIADisconnectHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIADisconnectHdr.Title="断开连接...";
MXCATIAGeneralWksAddinHeader.V6CATIADisconnectHdr.ShortHelp="断开连接";
MXCATIAGeneralWksAddinHeader.V6CATIADisconnectHdr.Help="断开与 ENOVIA 会话的连接";
MXCATIAGeneralWksAddinHeader.V6CATIADisconnectHdr.LongHelp="断开与 ENOVIA 会话的连接";

//Preferences
MXCATIAGeneralWksAddinHeader.V6CATIAPreferencesHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAPreferencesHdr.Title="首选项...";
MXCATIAGeneralWksAddinHeader.V6CATIAPreferencesHdr.ShortHelp="打开用户首选项页";
MXCATIAGeneralWksAddinHeader.V6CATIAPreferencesHdr.Help="打开用户首选项页";
MXCATIAGeneralWksAddinHeader.V6CATIAPreferencesHdr.LongHelp="打开用户首选项页";

//Help
MXCATIAGeneralWksAddinHeader.V6CATIAHelpHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAHelpHdr.Title="帮助...";
MXCATIAGeneralWksAddinHeader.V6CATIAHelpHdr.ShortHelp="“帮助”菜单";
MXCATIAGeneralWksAddinHeader.V6CATIAHelpHdr.Help="“帮助”菜单";
MXCATIAGeneralWksAddinHeader.V6CATIAHelpHdr.LongHelp="“帮助”菜单";

//About
MXCATIAGeneralWksAddinHeader.V6CATIAAboutHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIAAboutHdr.Title="关于";
MXCATIAGeneralWksAddinHeader.V6CATIAAboutHdr.ShortHelp="显示程序信息";
MXCATIAGeneralWksAddinHeader.V6CATIAAboutHdr.Help="显示程序信息";
MXCATIAGeneralWksAddinHeader.V6CATIAAboutHdr.LongHelp="显示程序信息";

//Preferences
MXCATIAGeneralWksAddinHeader.V6CATIASilentCheckoutCmdHdr.Category="MxCATIA";
MXCATIAGeneralWksAddinHeader.V6CATIASilentCheckoutCmdHdr.Title="静默签出命令";
MXCATIAGeneralWksAddinHeader.V6CATIASilentCheckoutCmdHdr.ShortHelp="连接至 ENOVIA 并以静默方式执行签出";
MXCATIAGeneralWksAddinHeader.V6CATIASilentCheckoutCmdHdr.Help="连接至 ENOVIA 并以静默方式执行签出";
MXCATIAGeneralWksAddinHeader.V6CATIASilentCheckoutCmdHdr.LongHelp="连接至 ENOVIA 并以静默方式执行签出";

