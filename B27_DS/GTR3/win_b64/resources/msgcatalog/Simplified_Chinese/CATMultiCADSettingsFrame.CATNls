// -----------------------------------
// Nouvelle interface utilisateur - R5
// -----------------------------------
visuFrame.HeaderFrame.Global.Title="可视化格式单位";

visuFrame.IconAndOptionsFrame.OptionsFrame.labelVisuUnit.Title="每单位的毫米数： ";
visuFrame.IconAndOptionsFrame.OptionsFrame.labelVisuUnit.ShortHelp="每单位的毫米数。";
visuFrame.IconAndOptionsFrame.OptionsFrame.labelVisuUnit.LongHelp="在导入可视化格式（byu、obj、slp、stl、_ps）时指示每单位的毫米数。";

visuFrame.IconAndOptionsFrame.OptionsFrame.visuUnit.ShortHelp="每单位的毫米数。";
visuFrame.IconAndOptionsFrame.OptionsFrame.visuUnit.LongHelp="在导入可视化格式（byu、obj、slp、stl、_ps）时指示每单位的毫米数。";

// -----------------------------------
convTechnoFrame.HeaderFrame.Global.Title="首选的转换技术";

convTechnoFrame.IconAndOptionsFrame.OptionsFrame.indirect.Title="间接";
convTechnoFrame.IconAndOptionsFrame.OptionsFrame.indirect.ShortHelp="首先使用间接技术。";
convTechnoFrame.IconAndOptionsFrame.OptionsFrame.indirect.LongHelp="如有可能，可使用间接转换技术。
该技术基于 Foreign CAD APIs 并支持 ProE、UG 以及 Ideas。";

convTechnoFrame.IconAndOptionsFrame.OptionsFrame.direct.Title="直接";
convTechnoFrame.IconAndOptionsFrame.OptionsFrame.direct.ShortHelp="首先使用直接技术。";
convTechnoFrame.IconAndOptionsFrame.OptionsFrame.direct.LongHelp="如有可能，可使用直接转换技术。
该技术基于反向工程并支持 ProE、UG、SolidWorks、
SolidEdge、Acis（sat）、Parasolid（x_t）以及 DXF3D。";

// -----------------------------------
linkModeFrame.HeaderFrame.Global.Title="链接模式";

linkModeFrame.IconAndOptionsFrame.OptionsFrame.visuMode.Title="可视化   ";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.visuMode.ShortHelp="只激活可视化模式。";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.visuMode.LongHelp="只激活可视化模式。";

linkModeFrame.IconAndOptionsFrame.OptionsFrame.visuSnap.Title="可视化捕捉  ";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.visuSnap.ShortHelp="激活含捕捉功能的可视化模式。";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.visuSnap.LongHelp="用规范的形状属性标识激活可视化模式。
该可视化模式将启用零件捕捉功能";

linkModeFrame.IconAndOptionsFrame.OptionsFrame.exactGeoMode.Title="精确的几何图形   ";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.exactGeoMode.ShortHelp="激活精确几何图形支持面。";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.exactGeoMode.LongHelp="激活精确几何图形支持面。例如，它使您能够作出精确的定位
或精确的测量。";

linkModeFrame.IconAndOptionsFrame.OptionsFrame.partMode.Title="CATPart   ";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.partMode.ShortHelp="激活 CATPart 模式。";
linkModeFrame.IconAndOptionsFrame.OptionsFrame.partMode.LongHelp="激活 CATPart 模式支持面。它将使您能够链接到 CATPart 上，
还能够为生成的数据设置约束。";

// -----------------------------------
translatorModeFrame.HeaderFrame.Global.Title="首选平移模式";

translatorModeFrame.IconAndOptionsFrame.OptionsFrame.multicadMode.Title="Multicad";
translatorModeFrame.IconAndOptionsFrame.OptionsFrame.multicadMode.ShortHelp="激活 Multicad 模式。";
translatorModeFrame.IconAndOptionsFrame.OptionsFrame.multicadMode.LongHelp="激活平移的 Multicad 模式。
此模式产生指向原始数据链接的只读几何图形（可进一步更新）。";

translatorModeFrame.IconAndOptionsFrame.OptionsFrame.translatorMode.Title="转换器";
translatorModeFrame.IconAndOptionsFrame.OptionsFrame.translatorMode.ShortHelp="激活转换器模式。";
translatorModeFrame.IconAndOptionsFrame.OptionsFrame.translatorMode.LongHelp="激活平移的转换器模式。
此模式产生不指向原始数据链接的可修改的几何图形（不能进一步更新）。";

// -----------------------------------
otherFrame.HeaderFrame.Global.Title="其他";

otherFrame.IconAndOptionsFrame.OptionsFrame.saveCoorsys.Title="在 CGR 中保存坐标系";
otherFrame.IconAndOptionsFrame.OptionsFrame.saveCoorsys.ShortHelp="在 CGR 中激活坐标系的保存。";
otherFrame.IconAndOptionsFrame.OptionsFrame.saveCoorsys.LongHelp="在 CGR 中激活坐标系的保存
本功能仅在使用间接技术时可用。";

otherFrame.IconAndOptionsFrame.OptionsFrame.partsParam.Title="零件参数化";
otherFrame.IconAndOptionsFrame.OptionsFrame.partsParam.ShortHelp="参考设置生效。";
otherFrame.IconAndOptionsFrame.OptionsFrame.partsParam.LongHelp="参考设置生效
本功能仅在使用间接技术时可用。";

otherFrame.IconAndOptionsFrame.OptionsFrame.Annotation3D.Title="3D 标注";
otherFrame.IconAndOptionsFrame.OptionsFrame.Annotation3D.ShortHelp="转换 3D 图形标注。";
otherFrame.IconAndOptionsFrame.OptionsFrame.Annotation3D.LongHelp="转换 3D 图形标注。";

// -----------------------------------
axisFrame.HeaderFrame.Global.Title="轴方向";

axisFrame.IconAndOptionsFrame.OptionsFrame.labelOrientVertical.Title="垂直轴： ";
axisFrame.IconAndOptionsFrame.OptionsFrame.labelOrientVertical.ShortHelp="选择与垂直轴匹配的轴。";
axisFrame.IconAndOptionsFrame.OptionsFrame.labelOrientVertical.LongHelp="可以选择与垂直轴匹配的轴。";

axisFrame.IconAndOptionsFrame.OptionsFrame.orientVertical.ShortHelp="选择与垂直轴匹配的轴。";
axisFrame.IconAndOptionsFrame.OptionsFrame.orientVertical.LongHelp="可以选择与垂直轴匹配的轴。";

axisFrame.IconAndOptionsFrame.OptionsFrame.labelOrientFront.Title="前轴： ";
axisFrame.IconAndOptionsFrame.OptionsFrame.labelOrientFront.ShortHelp="选择与前轴匹配的轴。";
axisFrame.IconAndOptionsFrame.OptionsFrame.labelOrientFront.LongHelp="可以选择与前轴匹配的轴。";

axisFrame.IconAndOptionsFrame.OptionsFrame.orientFront.ShortHelp="选择与前轴匹配的轴。";
axisFrame.IconAndOptionsFrame.OptionsFrame.orientFront.LongHelp="可以选择与前轴匹配的轴。";

// -----------------------------------
assocModeFrame.HeaderFrame.Global.Title="输出生成数据";

assocModeFrame.IconAndOptionsFrame.OptionsFrame.assocMode.Title="关联模式";
assocModeFrame.IconAndOptionsFrame.OptionsFrame.assocMode.ShortHelp="激活关联模式。";
assocModeFrame.IconAndOptionsFrame.OptionsFrame.assocMode.LongHelp="激活关联模式。该功能只在激活高速缓存时可用。
因此您将拥有一个指向原始零件的链接，从而可以始终拥有最新的零件。";

assocModeFrame.IconAndOptionsFrame.OptionsFrame.labelOutputPath.Title="输出路径： ";
assocModeFrame.IconAndOptionsFrame.OptionsFrame.labelOutputPath.ShortHelp="转换期间生成的数据的放置路径。";
assocModeFrame.IconAndOptionsFrame.OptionsFrame.labelOutputPath.LongHelp="指定生成数据的放置路径。";

assocModeFrame.IconAndOptionsFrame.OptionsFrame.outputPath.ShortHelp="转换期间生成的数据的放置路径。";
assocModeFrame.IconAndOptionsFrame.OptionsFrame.outputPath.LongHelp="指定生成数据的放置路径。";

assocModeFrame.IconAndOptionsFrame.OptionsFrame.outputPathPush.Title="浏览...";
assocModeFrame.IconAndOptionsFrame.OptionsFrame.outputPathPush.ShortHelp="浏览并选择输出目录。";
assocModeFrame.IconAndOptionsFrame.OptionsFrame.outputPathPush.LongHelp="浏览并选择将放置生成数据的输出目录。";

outputPathBrowser.Title="输出目录的路径";

// -----------------------------------
proEFrame.HeaderFrame.Global.Title="Creo? Parametric? - PRO/ENGINEER?";

proEFrame.IconAndOptionsFrame.OptionsFrame.proEQuilts.Title="装填读取";
proEFrame.IconAndOptionsFrame.OptionsFrame.proEQuilts.ShortHelp="激活 ProE 装填的读取。";
proEFrame.IconAndOptionsFrame.OptionsFrame.proEQuilts.LongHelp="在 ProEngineer? 文件中激活装填的读取。";

proEFrame.IconAndOptionsFrame.OptionsFrame.proESimpRep.Title="简化的展示";
proEFrame.IconAndOptionsFrame.OptionsFrame.proESimpRep.ShortHelp="激活简化展示支持面。";
proEFrame.IconAndOptionsFrame.OptionsFrame.proESimpRep.LongHelp="激活 ProEngineer? 文件的简化展示支持面。";

proEFrame.IconAndOptionsFrame.OptionsFrame.simpRepName.ShortHelp="要激活的简化展示的名称。";
proEFrame.IconAndOptionsFrame.OptionsFrame.simpRepName.LongHelp="在导入 ProEngineer? 装配时
要使用的简化展示的名称。";

proEFrame.IconAndOptionsFrame.OptionsFrame.proEInstanceMode.Title="实例名称";
proEFrame.IconAndOptionsFrame.OptionsFrame.proEInstanceMode.ShortHelp="指定读取常规装配时要转换的实例名称。";
proEFrame.IconAndOptionsFrame.OptionsFrame.proEInstanceMode.LongHelp="指定读取常规装配时要转换的实例名称。";

proEFrame.IconAndOptionsFrame.OptionsFrame.proEInstanceName.ShortHelp="要转换的实例的名称。";
proEFrame.IconAndOptionsFrame.OptionsFrame.proEInstanceName.LongHelp="常规 ProEngineer? 装配的特定实例的名称。
将只转换该实例。";

// -----------------------------------
ugFrame.HeaderFrame.Global.Title="NX?";

ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgLayers.Title="层： ";
ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgLayers.ShortHelp="指定要转换的层列表（如：“1、3、5-7”）。";
ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgLayers.LongHelp="指定文件中要转换的 Unigraphics? 层的列表。
正确格式示例：
   -“*”表示将转换所有的层
   -“1、3、9”表示将转换第 1 层、第 3 层和第 9 层
   -“5-7”表示将转换第 5 层到第 7 层
   -“2、7-9”表示将转换第 2 层、第 7 层、第 8 层和第 9 层";

ugFrame.IconAndOptionsFrame.OptionsFrame.ugLayers.ShortHelp="指定要转换的层列表（如：“1、3、5-7”）。";
ugFrame.IconAndOptionsFrame.OptionsFrame.ugLayers.LongHelp="指定文件中要转换的 Unigraphics? 层的列表。
正确格式示例：
   -“*”表示将转换所有的层
   -“1、3、9”表示将转换第 1 层、第 3 层和第 9 层
   -“5-7”表示将转换第 5 层到第 7 层
   -“2、7-9”表示将转换第 2 层、第 7 层、第 8 层和第 9 层";

ugFrame.IconAndOptionsFrame.OptionsFrame.ugActiveLayers.Title="只转换活动层";
ugFrame.IconAndOptionsFrame.OptionsFrame.ugActiveLayers.ShortHelp="是否只平移活动层。";
ugFrame.IconAndOptionsFrame.OptionsFrame.ugActiveLayers.LongHelp="在转换 Unigraphics? 文件时，可以指定平移所有的层
或只平移活动层。它可与 UG 层设置一起使用。";

ugFrame.IconAndOptionsFrame.OptionsFrame.ugOpenSurfaces.Title="转换开放曲面";
ugFrame.IconAndOptionsFrame.OptionsFrame.ugOpenSurfaces.ShortHelp="指定是否要转换“开放曲面”。";
ugFrame.IconAndOptionsFrame.OptionsFrame.ugOpenSurfaces.LongHelp="可以选择是否在 Unigraphics? 文件中转换“开放曲面”。";

ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgDrawing.Title="工程图名称： ";
ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgDrawing.ShortHelp="指定要针对 UG 文件进行转换的工程图名称。";
ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgDrawing.LongHelp="指定要转换的工程图（将 Unigraphics? 文件转换为
CATDrawing）的名称（只用于批处理模式）。";

ugFrame.IconAndOptionsFrame.OptionsFrame.ugDrawing.ShortHelp="指定要针对 UG 文件进行转换的工程图名称。";
ugFrame.IconAndOptionsFrame.OptionsFrame.ugDrawing.LongHelp="指定要转换的工程图（将 Unigraphics? 文件转换为
CATDrawing）的名称（只用于批处理模式）。";

ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgRefSetName.Title="参考设置名称： ";
ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgRefSetName.ShortHelp="指定要激活的参考设置的名称。";
ugFrame.IconAndOptionsFrame.OptionsFrame.labelUgRefSetName.LongHelp="指定在导入 Unigraphics? 零件文件时将使用的
参考设置的名称。";

ugFrame.IconAndOptionsFrame.OptionsFrame.refSetName.ShortHelp="指定要激活的参考设置的名称。";
ugFrame.IconAndOptionsFrame.OptionsFrame.refSetName.LongHelp="指定在导入 Unigraphics? 零件文件时将使用的
参考设置的名称。";

// Warning Messages ------------------
RestartWarning="请重新启动会话以使修改生效。";
ExactGeoWarning="该设置将只应用于新的导入文档。
该设置对已在会话中的文档无效。";
IncorrectPathWarning="输出路径不正确。使用了旧值。";
IncorrectLayerWarning="层定义格式不正确。使用了旧值。";

// NLS values for combo ------------------
ideasCompTypePart="零件";
ideasCompTypeAssembly="装配件";

