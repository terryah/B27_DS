// COPYRIGHT DASSAULT SYSTEMES 2003
//===========================================================================
//
// CATRdgOptionsGeneralPropertyFrame
//
//===========================================================================


//--------------------------------------
// Environment creation options
//--------------------------------------
environmentFrame.HeaderFrame.Global.Title = "新建环境";

environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.Title = "捕捉至几何图形";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.Help = "将环境捕捉至几何图形。";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.ShortHelp = "将环境捕捉至几何图形";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.LongHelp =
"如果选中，将会创建新环境，使其
底面（若是球形环境，则为其中心）捕捉至
几何图形的最低点。";

//--------------------------------------
// Light creation options
//--------------------------------------
creationLightFrame.HeaderFrame.Global.Title = "新建光源位置";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.Title = "默认模式";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.Help = "光源向下照射。";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.ShortHelp = "默认光源方向";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.LongHelp =
"在该模式下，光源方向与当前视点方向垂直，
向下照射。";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.Title = "视点方向";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.Help = "光源方向与当前视点方向一致";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.ShortHelp = "方向与视点方向一致";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.LongHelp =
"在该模式下，光源方向与当前视点方向一致。 
光源原点位于用户双眼中心处。";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.Title = "重力方向";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.Help = "光源与所选绝对轴平行";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.ShortHelp = "光源与绝对轴平行";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.LongHelp =
"在该模式下，光源方向与 X、Y 或 Z 绝对轴平行，
向下照射。";


creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis1.Title = "X";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.Help = "X 绝对轴";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.ShortHelp = "X 绝对轴";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.LongHelp =
"光源方向与 X 绝对轴平行。";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis2.Title = "Y";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.Help = "Y 绝对轴";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.ShortHelp = "Y 绝对轴";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.LongHelp =
"光源方向与 Y 绝对轴平行。";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis3.Title = "Z";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.Help = "Z 绝对轴";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.ShortHelp = "Z 绝对轴";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.LongHelp =
"光源方向与 Z 绝对轴平行。";

viewPointFrame.HeaderFrame.Global.Title = "视图模式";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.Title = "激活";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.Help = "激活/取消激活视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.ShortHelp = "激活/取消激活视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.LongHelp =
"进入工作台时，激活/取消激活视点。";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.Title = "平行";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.Help = "在平行模式下设置视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.ShortHelp = "在平行模式下设置视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.LongHelp =
"进入工作台时，在平行模式下设置视点。";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.Title = "透视";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.Help = "在透视模式下设置视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.ShortHelp = "在透视模式下设置视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.LongHelp =
"进入工作台时，在透视模式下设置视点。";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.Title = "最近保存";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.Help = "在最近保存模式下设置视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.ShortHelp = "在最近保存模式下设置视点";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.LongHelp =
"进入工作台时，在最近保存模式下设置视点。";

//-----------------------------------------------------
// Material View Setting Option
//-----------------------------------------------------
materialViewFrame.HeaderFrame.Global.Title = "材料显示";

materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.Title = "查看材料";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.Help = "查看材料";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.ShortHelp = "查看材料";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.LongHelp =
"进入工作台时，激活或取消激活材料显示。";

//--------------------------------------
// Plug-in options
//--------------------------------------
pluginFrame.HeaderFrame.Global.Title = "渲染引擎";

pluginFrame.IconAndOptionsFrame.OptionsFrame.pluginCombo.Help = "选择软件渲染引擎";
pluginFrame.IconAndOptionsFrame.OptionsFrame.pluginCombo.ShortHelp = "选择渲染引擎";
pluginFrame.IconAndOptionsFrame.OptionsFrame.pluginCombo.LongHelp =
"选择图像渲染引擎。";

Default = "默认值";

