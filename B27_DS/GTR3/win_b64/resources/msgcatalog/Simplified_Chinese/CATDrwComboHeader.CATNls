//=============================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwComboHeader
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// DATE        :    10.02.99
//------------------------------------------------------------------------------
// DESCRIPTION :    
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01		  fgx	  03.03.99	cles pour le NLS des unites
//     02           lgk   14.10.02  Revision
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Font Name
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwFontName.Title="字体名称" ;
CATDrwComboHeader.DrwFontName.ShortHelp="字体名称";
CATDrwComboHeader.DrwFontName.Help="设置一段文本的字体" ;
CATDrwComboHeader.DrwFontName.LongHelp =
"字体名称
设置一段文本的字体。";

//------------------------------------------------------------------------------
// Font Size
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwFontSize.Title="字体大小" ;
CATDrwComboHeader.DrwFontSize.ShortHelp="字体大小";
CATDrwComboHeader.DrwFontSize.Help="设置字体大小" ;
CATDrwComboHeader.DrwFontSize.LongHelp =
"字体大小
设置字体大小。";

//------------------------------------------------------------------------------
// Tolerance
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwTolerance.Title="公差" ;
CATDrwComboHeader.DrwTolerance.ShortHelp="公差";
CATDrwComboHeader.DrwTolerance.Help="设置尺寸公差" ;
CATDrwComboHeader.DrwTolerance.LongHelp =
"公差
设置尺寸公差。";

//------------------------------------------------------------------------------
// Precision
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwPrecision.Title="精度" ;
CATDrwComboHeader.DrwPrecision.ShortHelp="精度";
CATDrwComboHeader.DrwPrecision.Help="设置用于显示数值的精度" ;
CATDrwComboHeader.DrwPrecision.LongHelp =
"精度
设置用于显示数值的精度。";

//------------------------------------------------------------------------------
// Unit
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwUnit.Title="单位" ;
CATDrwComboHeader.DrwUnit.ShortHelp="单位";
CATDrwComboHeader.DrwUnit.Help="设置尺寸主单位" ;
CATDrwComboHeader.DrwUnit.LongHelp =
"单位
设置尺寸主单位。";

//------------------------------------------------------------------------------
// Numerical Display
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwNumDisp.Title="数字显示说明" ;
CATDrwComboHeader.DrwNumDisp.ShortHelp="数字显示说明";
CATDrwComboHeader.DrwNumDisp.Help="设置用于尺寸主值的数字显示说明" ;
CATDrwComboHeader.DrwNumDisp.LongHelp =
"数字显示说明
设置用于尺寸主值的
数字显示说明。";

//------------------------------------------------------------------------------
// Tolerance Type
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwTolDescrip.Title="公差说明" ;
CATDrwComboHeader.DrwTolDescrip.ShortHelp="公差说明";
CATDrwComboHeader.DrwTolDescrip.Help="设置用于尺寸主值的公差说明" ;
CATDrwComboHeader.DrwTolDescrip.LongHelp =
"公差说明
设置用于尺寸主值的
公差说明。";

//------------------------------------------------------------------------------
// Style
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwStyle.Title="样式" ;
CATDrwComboHeader.DrwStyle.ShortHelp="样式";
CATDrwComboHeader.DrwStyle.Help="设置用于创建新对象的样式" ;
CATDrwComboHeader.DrwStyle.LongHelp =
"样式
设置用于创建新对象的样式：
- 原始：在应用程序中预定义的值，可由工具栏值覆盖；
- 用户默认值：用户定义的值，可由工具栏值覆盖；
- 仅用户默认值：用户定义的值，不可覆盖。";
NORMAL="原始属性";
USER_DEFAULT="用户默认属性";
ONLY_USER_DEFAULT="仅用户默认属性";

//------------------------------------------------------------------------------
// Keys for Units' NLS
//------------------------------------------------------------------------------
MM="mm";
INCH="英寸";

DEG="度";
MIN="度-分";
SEC="度-分-秒";
RAD="弧度";

NO_TOLERANCE="（无公差）";

