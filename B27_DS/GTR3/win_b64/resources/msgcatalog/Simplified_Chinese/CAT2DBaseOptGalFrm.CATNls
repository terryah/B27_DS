//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGal
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet General
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   14.01.1999  Ajout du check rotation snap auto
//      03           fgx   20.01.1999  Deplacement des options de la grid de geo a gal
//	    04		     ogk   16.06.1999  Ajout pour le coloring
//	    05		     ogk   29.10.1999  Ajout repere de vue
//=====================================================================================

Title="常规";

frameRuler.HeaderFrame.Global.Title="标尺";
frameRuler.HeaderFrame.Global.LongHelp=
"标尺
定义是否在框架周围显示标尺。";
frameRuler.IconAndOptionsFrame.OptionsFrame.checkRuler.Title="显示标尺";
frameRuler.IconAndOptionsFrame.OptionsFrame.checkRuler.LongHelp=
"显示或隐藏标尺。";

frameGrid.HeaderFrame.Global.Title="网格";
frameGrid.HeaderFrame.Global.LongHelp=
"网格
定义将帮助您绘制几何图形或对齐视图的网格。";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkGrid.Title="显示";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkGrid.LongHelp=
"定义将要显示网格。";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkSnap.Title="点捕捉";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkSnap.LongHelp=
"帮您在所给位置创建点。";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridSpacing.Title="原始间距： ";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridSpacing.LongHelp=
"定义将应用于网格的主间距。";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridGraduation.Title="刻度： ";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridGraduation.LongHelp=
"将指定数量的刻度应用于每个原始
间距。";

frameGrid.IconAndOptionsFrame.OptionsFrame.checkAllowDistortions.Title="允许变形";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkAllowDistortions.LongHelp="允许变形\n允许水平方向和垂直方向上具有不同的刻度和间距。";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelH.Title="H：";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelV.Title="V：";

frameColor.HeaderFrame.Global.Title="颜色（对于 V5R14 之前的工程图）";
frameColor.HeaderFrame.Global.LongHelp=
"颜色
定义应用于正常和 2D 部件详图的背景颜色。";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColor.Title="图纸背景：";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColor.LongHelp=
"图纸背景
定义图纸背景颜色。";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColor.Title="渐变颜色";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColor.LongHelp=
"渐变颜色
定义图纸背景颜色是否渐变。";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColorDD.Title="细节背景：";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColorDD.LongHelp=
"图纸背景
定义应用于细节 2D 部件详图的图纸背景颜色。";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColorDD.Title="渐变颜色";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColorDD.LongHelp=
"渐变颜色
定义图纸背景颜色是否针对 2D 部件详图渐变。";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewer.viewer.Title="Preview:";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewer.viewer.LongHelp=
//"Preview
//Gives the feedback of the user selections.";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewerDD.viewerDD.Title="Preview:";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewerDD.viewerDD.LongHelp=
//"Preview
//Gives the feedback of the user selections.";

frameTreeVisu.HeaderFrame.Global.Title="结构树";
frameTreeVisu.HeaderFrame.Global.LongHelp="结构树
您可以指定参数和/或关系是否必须
在结构树中显示。";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeParameters.Title="显示参数";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeParameters.LongHelp=
"显示参数
显示结构树中用于当前工程图的
参数。";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeFormulas.Title="显示关系";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeFormulas.LongHelp=
"显示关系
显示结构树中用于当前工程图的
关系。";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame.checkFeatUnderView.Title="显示视图特征";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame.checkFeatUnderView.LongHelp="显示视图特征。";


frameViewReferential.HeaderFrame.Global.Title="视图轴";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewReferential.Title="在当前视图中显示";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewReferential.LongHelp="显示或不显示当前视图的轴。";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewRefZoom.Title="可缩放";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewRefZoom.LongHelp="可缩放
定义视图轴是否可以像几何图形一样进行缩放。";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.labelViewAxisSize.Title="参考大小：";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.labelViewAxisSize.LongHelp="参考大小
指定用来显示视图轴的参考大小。";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.spinnerViewAxisSize.LongHelp="参考大小
指定用来显示视图轴的参考大小。";


frameAreaFill.HeaderFrame.Global.Title="区域填充";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.labelAreaFillTol.Title="打开轮廓公差";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.labelAreaFillTol.LongHelp="打开要填充的轮廓的公差。";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.spinnerAreaFillTol.Title="打开轮廓公差";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.spinnerAreaFillTol.LongHelp="打开要填充的轮廓的公差。";


frmHideStartWkb.HeaderFrame.Global.Title="启动工作台";
frmHideStartWkb.IconAndOptionsFrame.OptionsFrame.chkHideStartWkb.Title="启动工作台时隐藏新对话框";
frmHideStartWkb.IconAndOptionsFrame.OptionsFrame.chkHideStartWkb.LongHelp="当启动工作台时隐藏新对话框。";


frmDrwUnits.HeaderFrame.Global.Title="纸张单元";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.lblPaperLengthUnit.Title="长度";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.lblPaperLengthUnit.LongHelp="长度。";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.cmbPaperLengthUnit.Title="长度";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.cmbPaperLengthUnit.LongHelp="长度。";
mmUnit="毫米 (mm)";
cmUnit="厘米 (cm)";
inUnit="英寸 (in)";
