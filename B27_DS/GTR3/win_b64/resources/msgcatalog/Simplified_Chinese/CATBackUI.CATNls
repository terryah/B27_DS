//  Messages for CATIA V5 (Downward)Compatibility
//  MODULE :  DownwardCompatibility
//  US Version
//====================================================================
Title="CATIA V5 向下兼容性批处理";
Main.Title="向下兼容性";
BatchMonitor.Title="在最新发行版到 V5R6 之间向下兼容"; 
Downward.Abstract="在最新发行版到 V5R6 之间向下兼容"; 
Downward.Domain="数据兼容性"; 

// User Interfaces resources
CATBack.comboVersion.Title="CATIA 级别";
CATBack.comboAction.Title="工作指令";

CATBack.BrowseDir.Title="...";
CATBack.BrowseDirBox.Title="目录选择";
CATBack.LblTargetDirectory.Title="目标目录";
CATBack.BtnTargetDirectory.Title="...";

CATBack.WarningPanel.Title="警告";
CATBack.LblDocSelected.Title="选定的 V5 文档：";
CATBack.LblDocProcessed.Title="要处理的 V5 文档：";

CATBack.BtnBrowse.Title="浏览文件";
CATBack.CheckMMLSupport.Title="被指向的文档";
CATBack.CheckMMLSupport.Help="选择被指向的文档";
CATBack.BtnRemoveDoc.Title="移除";
CATBack.BtnReplaceDoc.Title="替换";
CATBack.Replace.Help = "替换已存在的部件";

CATBack.ListSelDoc.Name="名称";
CATBack.ListSelDoc.Path="路径";

ComboAction.Convert="转换";
ComboAction.Info="信息";
ComboAction.Synchro="同步";
CATBack.CheckHtmlOpen.Title="打开 HTML 报告";

//============================================================
// Error messages
//============================================================
WarningDoublonMessage="此文件已包含在要处理的文档集中";
WarningDoublonMsgMultiSel="这些文件已包含在要处理的文档集中：";

WarningBadTypeMessage="无法处理此类型的文件";
WarningBadTypeMsgMultiSel="无法处理这些文件：";
WarningInvalidFormat="文件格式无效";
//============================================================
// Report messages
//============================================================
ReportBatchAborted1="批处理异常中止 - 返回代码 =";
ReportBatchAborted2="没有已处理的文档";
ReportBatchPartial="批处理部分完成 - 返回代码 =";
ReportBatchSuccess="批处理成功完成 - 返回代码 =";
ReportBegin="报告";
ReportDocumentTitle="要处理的文档";
ReportDocumentKO="未处理文档";
ReportDocumentOK="已成功处理文档";
ReportEnd="报告结束";
ReportAction.Convert= "“/p1”的转换" ;	  
ReportAction.Synchro= "“/p1”的同步" ;	  
ReportAction.Info= "“/p1”的信息" ;	  
ReportAction.Undef= "错误：未定义工作指令
\t 需要声明 <Convert>、<Synch> 或 <Info> 工作指令
\t 使用“-h”选项查阅已知参数和必要参数列表";
ReportAction.Unknown= "警告：工作指令“/p1”未知， 
\t  支持的工作指令为：" ;	  
ReportVersion.Unknown= "警告：版本“/p1”不支持，或者并非 CATIA 版本，
\t  支持的版本为：" ;	  
Report.Convert.CurrentVersion="当前版本：“/p1”";
Report.Convert.OutputDirectory="输出目录：“/p1”";
Report.Convert.TargetedVersion="目标版本：“/p1”";

//============================================================
// Specific messages    "Info" action : Infoxxx
//============================================================
Info.DocVersion="   打开文档的最低版本：“/p1”";
Info.DocNotFound="   文档不存在或无法找到";
Info.RefPath="   文档是从以下文件转换的图像：“/p1”";
Info.RefNotFound="   参考不存在或无法找到";
Info.RefVersion="   打开参考的最低版本：“/p1”";
Info.InfoKO="无法处理文档";
Info.InfoOK="已成功处理文档";
//============================================================
// Specific messages "Convert" action : Convertxxx
//============================================================
ConvertSelectionDocKo1="此文件是已转换的图像：";
ConvertSelectionDocKo2="选择异常中止，请选择原始文件：";
ConvertSelectionDocKo3="但是无法找到或读取原始文件：";
Convert.NotADoc="   要转换的文档不是 CATIA V5 文档";
Convert.DocNotFound="   文档不存在或无法找到";
Convert.MinimalVersion="   打开文档的最低版本：“/p1”";
Convert.Needed="   文档需要转换";
Convert.NotNeeded="   文档不需要转换";
Convert.NoReadPermission="   没有读访问权。\n更改访问权限";
Convert.ReadAborted="   无法读取文档";
Convert.CreateAborted="   无法创建输出文档“/p1”";
Convert.AlreadyExist="   文档已存在但无法覆盖。\n移动文档。";
Convert.WriteAborted="   无法写入文档。\n检查访问权限";
Convert.ErrorDummy="   转换期间发生内部错误";
Convert.ConvertKO="无法转换文档";
Convert.ConvertOK="已成功处理文档";
//============================================================
// Specific messages "Synchro" action : Synchroxxx
//============================================================
SynchroSelectionDocKo1="无法同步文档：";
SynchroSelectionDocKo2="选择异常中止，此文件不是已转换图像";
SynchroSelectionDocKo3="选择异常中止，无法找到或读取原始文件";
Synchro.NotADoc="   要同步的文档不是 CATIA V5 文档";
Synchro.DocNotFound="   文档不存在或无法找到";
Synchro.NotAConvertedDoc="   文档不是已转换图像";
Synchro.NoReadPermission="   没有读访问权。\n更改访问权限";
Synchro.NoWritePermission="   没有写访问权。\n更改访问权限";
Synchro.RefNotADoc="   参考不是 CATIA V5 文档 ";
Synchro.RefNotFound="   参考不存在或无法找到";
Synchro.RefNoReadPermission="   没有对参考的读访问权。\n更改访问权限";
Synchro.RefPath="   文档是从以下文件转换的图像：/p1";
Synchro.RefVersion="   打开参考的最低版本：“/p1”";
Synchro.DocVersion="   打开文档的最低版本：“/p1”";
Synchro.ReadAborted="   无法读取文档";
Synchro.ReadRefAborted="   无法读取参考";
Synchro.WriteAborted="   无法写入文档。\n检查访问权限";
Synchro.NoModif="   文档不需要同步。";
Synchro.ErrorDummy="   同步期间发生内部错误";
Synchro.SynchroKO="无法同步文档";
Synchro.SynchroOK="已成功处理文档";

Downward.Compatibility = "向下兼容性";

//============================================================
// Errors messages 
//============================================================
Error.Title="错误";
ErrorDummy="内部错误 - 代码 =";

HelpMsg="用法： 
    CATDownwardCompatibility -id inputDirectory [-if inputfile/-il inputlist] -od outputDirectory 
                             -action [Convert/Synchro/Info] -version CATIAV5Ri 
                             -report reportFile.txt

       -id	 ：输入目录或 DLNAME
       -if       ：包含要处理文档列表的输入文件
       -il       ：要处理文档的输入列表
                   -if 或 -il 参数是必需的
       -action   ：请求的工作指令
                   Convert：将文档转换为 -version 参数中定义的版本的可读文档
                   Synch：同步原始文件的转换文档
                   Info：提供有关选定成员的信息
                   -action 参数是必需的
       -version  ：目标 CATIA 版本
                   对于“Convert”工作指令是必需的（不在“Synch”或“Info”工作指令模式下使用）                   
       -od	 ：输出目录或 DLNAME
                   对于“Convert”工作指令是必需的（不在“Synch”或“Info”工作指令模式下使用）
       -report   ：报告文件名"; 

