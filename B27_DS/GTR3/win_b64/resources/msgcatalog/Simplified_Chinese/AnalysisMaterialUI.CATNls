// COPYRIGHT DASSAULT SYSTEMES 2000
//===========================================================================
//
// CATSamMaterialFrame (English)
//
//===========================================================================
Analysis="分析";

INF_PoissonRatioModifed="泊松比不能超过 0.5。已将此值设置为 0.4999。";
INF_PoissonRatioExceed="泊松比大于允许的最大值。已将此值设置为允许的最大值。";
INF_YoungsModulusRatio="杨氏模量比大于允许的最大值。杨氏模量和泊松比值已设置为 0。";
INF_PoissonRatioOrthotropic3D="泊松比值大于允许的最大值。已将这些值设置为允许的最大值。";
INF_YoungsModulusRatioOrthotropic3D="杨氏模量比值大于允许的最大值。杨氏模量和泊松比值已设置为 0。";
INF_CombinedOrthotropic3DExceed="杨氏模量和泊松比值的组合大于允许的值 1。杨氏模量和泊松比值已设置为 0。";
MaterialInfoUITitle="信息";

analysisFrame.Title="结构属性";

SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Title="杨氏模量";
SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的杨氏模量";
SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="描述材料硬度的杨氏模量";

SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Title="泊松比";
SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的泊松比";
SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="材料的泊松比
（机械扩张，值介于 0 和 0.5 之间）";

SAMDensity.EnglobingFrame.IntermediateFrame.Label.Title="密度";
SAMDensity.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的密度";
SAMDensity.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="材料的密度（用于模态计算）";

SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Title="热膨胀";
SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的热膨胀";
SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="材料的热膨胀（热扩张效应）";

SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Title="屈服强度";
SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的屈服强度";
SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="材料的屈服强度。
与“米塞斯”最大值比较，查看零件是否失败";

// SAMOrthotropic
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Title="纵向杨氏模量";
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的纵向杨氏模量";
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="纵向杨氏模量";

SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Title="横向杨氏模量";
SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的横向杨氏模量";
SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="横向杨氏模量";

SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Title="XY 平面的泊松比";
SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Value.Help="XY 平面的材料泊松比";
SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="XY 平面的泊松比";

SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Title="XZ 平面的泊松比";
SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Value.Help="XZ 平面的材料泊松比";
SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="XZ 平面的泊松比";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Title="YZ 平面的泊松比";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Value.Help="YZ 平面的材料泊松比";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="YZ 平面的泊松比";

SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Title="XY 平面的剪切模量";
SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料 XY 平面剪切模量";
SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Value.longHelp="XY 平面的剪切模量";

SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Title="XZ 平面的剪切模量";
SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料 XZ 平面剪切模量";
SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="XZ 平面的剪切模量";

SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Title="YZ 平面的剪切模量";
SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料 YZ 平面剪切模量";
SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="YZ 平面的剪切模量";

SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Title="XZ 平面的剪切模量";
SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料 XZ 平面剪切模量";
SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="XZ 平面的剪切模量";

SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Title="YZ 平面的剪切模量";
SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料 YZ 平面剪切模量";
SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="YZ 平面的剪切模量";

SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="纵向抗拉应力";
SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的纵向抗拉应力";
SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="纵向抗拉应力";

SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="纵向压应力";
SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的纵向压应力";
SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="纵向压应力";

SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="横向抗拉应力";
SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的横向抗拉应力";
SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="横向抗拉应力";

SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="横向压应力";
SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的横向压应力";
SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="横向压应力";

SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="纵向抗拉应变";
SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的纵向抗拉应变";
SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="纵向抗拉应变";

SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="纵向压应变";
SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的纵向压应变";
SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="纵向压应变";

SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="横向抗拉应变";
SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的横向抗拉应变";
SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="横向抗拉应变";

SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="横向压应变";
SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的横向压应变";
SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="横向压应变";

SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Title="纵向热膨胀";
SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的纵向热膨胀";
SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="纵向热膨胀";

SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Title="横向热膨胀";
SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的横向热膨胀";
SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="横向热膨胀";

SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Title="法向热膨胀";
SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的法向热膨胀";
SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="法向热膨胀";

analysisFrame.stressFrame.Title="极限应力";
analysisFrame.strainFrame.Title="应变极限";


// combo de selection de type de materaiux
MaterialType.Help="定义材料种类";
MaterialType.MaterialTypeLabel.Title="材料";
MaterialType.MaterialTypeCombo.SAMIsotropicMaterial.Title="各向同性材料";
MaterialType.MaterialTypeCombo.SAMOrthotropicMaterial.Title="正交各向异性材料 2D";
MaterialType.MaterialTypeCombo.SAMFiberMaterial.Title="纤维材料";
MaterialType.MaterialTypeCombo.SAMHoneyCombMaterial.Title="蜂窝材料";
MaterialType.MaterialTypeCombo.SAMOrthotropic3DMaterial.Title="正交各向异性材料 3D";
MaterialType.MaterialTypeCombo.SAMAnisotropicMaterial.Title="各向异性材料";


// Material HONEYCOMB
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Title="法向杨氏模量";
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的法向杨氏模量";
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="法向杨氏模量";

SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Title="XZ 平面极限剪切应力";
SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的 XZ 平面极限剪切应力";
SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="XZ 平面极限剪切应力";

SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Title="YZ 平面极限剪切应力";
SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的 YZ 平面极限剪切应力";
SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="YZ 平面极限剪切应力";

// Material FIBER
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Title="XY 平面极限剪切应力";
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的 XY 平面极限剪切应力";
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="XY 平面极限剪切应力";

// Material ANISOTROPE
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Title="法向热膨胀";
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的法向热膨胀";
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="法向热膨胀";

SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Title="纵向剪切模量";
SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的纵向剪切模量";
SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="纵向剪切模量";

SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Title="横向剪切模量";
SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的横向剪切模量";
SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="横向剪切模量";

SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Title="法向剪切模量";
SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的法向剪切模量";
SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="法向剪切模量";

SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Title="抗拉应力";
SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的抗拉应力";
SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="抗拉应力";

SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Title="压应力";
SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的压应力";
SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="压应力";

SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Title="剪应力";
SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help="材料的剪应力";
SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="剪应力";

