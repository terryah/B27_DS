//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATFramePattern
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    Hichem BEN CHEIKH
// DATE        :    Janv. 01 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01		lgk 17.12.02 Revision
//------------------------------------------------------------------------------

// * Label *
EditorMaterialTitle="工程图";

Pattern.Hatching.labelNbHatching.Title="阴影数：";

Pattern.Dotting.DottinglabelPitch.Title="间距：";
Pattern.Dotting.DottinglabelCol.Title="颜色：";
Pattern.Dotting.DottinglabelZig.Title="锯齿形";

Pattern.Coloring.ColoringlabelCol.Title="颜色：";

Pattern.Motif.BrowseButton.Title="浏览...";
Pattern.Motif.ModifAngle.Title="角度：";
Pattern.Motif.ModifScale.Title="标度：";

Pattern.None.NonelabelCol.Title="无关联的阵列";

Pattern.Hatching.labelNbHatching.LongHelp="定义阴影数。";
Pattern.Dotting.DottinglabelPitch.LongHelp="设置阵列间距。";
Pattern.Dotting.DottinglabelCol.LongHelp="设置阵列颜色。";
Pattern.Dotting.DottinglabelZig.LongHelp="将阵列设置为锯齿形。";
Pattern.Coloring.ColoringlabelCol.LongHelp="设置阵列颜色。";
Pattern.Motif.BrowseButton.LongHelp="您可以选择一个文件，定义作为阵列使用的图像。";
Pattern.Motif.ModifAngle.LongHelp="设置图像的角度。";
Pattern.Motif.ModifScale.LongHelp="设置图像的标度。";
Pattern.None.NonelabelCol.LongHelp="表示不存在关联阵列。";

Parameters.PatternNameLabel.Title="名称：";
Parameters.TypePatternLabel.Title="类型："; 

labelAngle="角度：";
labelPitch="间距：";
labelOffset="偏移：";
labelThickness="线宽：";
labelColor="颜色：";
labelTxt="线型：";

labelAngle.LongHelp="设置角度。";
labelPitch.LongHelp="设置间距。";
labelOffset.LongHelp="设置偏移。";
labelThickness.LongHelp="设置线宽。";
labelColor.LongHelp="设置颜色。";
labelTxt.LongHelp="设置线型。";

Parameters.TypePattern.ShortHelp="类型"; 
Pattern.Hatching.labelThk.Title="阴影数";
TypeHatching="阴影";
TypeDotting="点线";
TypeColoring="着色";
TypeMotif="图像";
TypeNone="无";
TabTitle="阵列";

Parameters.TypePattern.LongHelp="设置类型。"; 
Pattern.Hatching.labelThk.LongHelp="设置阴影数。";
TypeHatching.LongHelp="设置阴影。";
TypeDotting.LongHelp="设置点线。";
TypeColoring.LongHelp="设置着色。";
TypeNone.LongHelp="不使用阵列。";

Parameters.PatternTable.Title="...";
Parameters.PatternTable.LongHelp="设置阵列表。";
TabTitle.LongHelp="设置阵列。";

MaterialTitle.Title="材料";
PreviewTitle.Title=" 预览";

HatchingPage="阴影";
PatternChooser="阵列选择器";

Material.LongHelp="读取材料参数。";
Parameters.LongHelp="设置参数。";
Pattern.LongHelp="设置阵列。";
Pattern.Hatching.LongHelp="设置阴影。";
Pattern.Dotting.LongHelp="设置点线。";
Pattern.Coloring.LongHelp="设置着色。";
Pattern.None.LongHelp="不使用阵列。";
Preview.LongHelp="设置预览。";
HatchingPage.LongHelp="设置阴影。";
PatternChooser.LongHelp="设置阵列选择器。";

Material.MaterialButton.Title="使用零件材料阵列重置";
Material.HasNOMaterial.Title="零件上没有材料";

Material.MaterialButton.LongHelp="使用为零件材料定义的阵列重置。";
