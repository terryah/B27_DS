// ==============================================================================================
// Messages STEP
// 
// 16/10/2002: JHH : Creation
// 25/06/2003: ARH : InvalidPositionOfComponent
// 08/09/2003: ARH : OldStyleAssemblyStructure et DuplicatedProduct
// 05/01/2004: ARH : DuplicatedProduct a l'export
// 08/04/2004: ARH : RI446208: message
// 04/06/2004: ARH : RI453883: message AssemblyCycle et AssemblyAsPart
// 08/06/2004: ARH : RI455372: message WrongHeader
// 20/07/2004: JHH : Modification message OldStyleAssemblyStructure (devient une information)
// 22/10/2004: ARH : RI470187: message ErrorWriting
// 16/05/2006: ARH : RI534401: message GVPofPart
// 17/01/2007: ARH : messages Version
// 14/09/2007: ARH : RI592111: message DuplicateProductKO
// 21/09/2007: ARH : message LightProduct
// 22/01/2008: ARH : messages import des FTA
// 22/04/2008: YFZ : Traduction pour le HEADER STEP
// 16/09/2008: ARH : Messages pour FTA
// 25/08/2009: ARH : RI11316: message pour FTA invalide sans vue
// 09/09/2009: ARH : message FTA_ValidationPropertiesKO
// 23/02/2010: ARH : IR-040076: Ajout message WarningNoLicense
// 29/12/2010: ARH : messages UDA_VP
// 20/07/2012: YFZ: Message pour avertire que la convertion de la geometries step ne sera pas faite. IR-169572V5-6R2013WIM
// 02/10/2012: ARH: Message CATStepMsg.Exp.FTA_EmptyView
// 09/07/2013: ARH: Message CATStepMsg.Imp.FTA_ValidationProperties3
// 15/11/2013: ARH: Messages CATStepMsg.Imp.Tessellation_TolerancesVP*
// 17/12/2013: ARH: Message CATStepMsg.ProcessingDate
// 23/07/2015: ACS1: Message Export STEP XML
// ==============================================================================================

CATStepMsg.Exp.Version="预处理器版本： ";
CATStepMsg.Imp.Version="预处理器版本： ";

CATStepMsg.ProcessingDate="处理日期： ";

CATStepMsg.Imp.InvalidStepFile="<E> 读取 STEP 文件时检测到错误： ";

CATStepMsg.Imp.WrongHeader="<E> STEP 文件无效：文件头不完整";

CATStepMsg.Exp.ErrorWriting="<E> 写 STEP 文件时出错";

CATStepMsg.Imp.TooManySDAIerrors="<E> 在该 STEP 文件中检测到 50 个以上的错误；将不报告接下来的错误。";

CATStepMsg.SimpleMsg="/p1";

CATStepMsg.Exp.InvalidPositionOfComponent="<E> /p3 未正确定位产品“/p2”的实例“/p1”";

CATStepMsg.Exp.InvalidPositionOfComponentTess="<E> 有实体未被 /p1 正确定位。";

CATStepMsg.Imp.OldStyleAssemblyStructure="<I> 旧式装配结构。";

CATStepMsg.Imp.DuplicatedProduct="<E> 装配结构中有若干个名为“/p1”的产品。";

CATStepMsg.Exp.DuplicatedProduct="<W> 名为“/p1”的复制产品被重命名为“/p2”";

CATStepMsg.Exp.DuplicatedProductKO="<E> 名为“/p1”的产品被定义了两次。仅导出了第一个定义";

CATStepMsg.Exp.FlexibleProduct="<W> 名为“/p1”的柔性产品被导出为新产品";

CATStepMsg.Exp.LightProduct="<W> 名为“/p1”的轻量级产品被导出为零件";

CATStepMsg.Exp.MissingDocument="<E> 缺少与零件编号“/p1”相关联的文档";

CATStepMsg.Exp.WrongWire="<E> 未导出线“/p1”";

CATStepMsg.Imp.AssemblyCycle="<E> 在产品 /p1 和 /p2 之间的装配结构中有一个周期";

CATStepMsg.Imp.AssemblyAsPart="<E> 装配结构错误。只转录几何图形";

CATStepMsg.Imp.MatrixInvalide="<W> 产品“/p1”的一个实例位置错误。其矩阵无效";

CATStepMsg.Exp.GVPofPart="零件级别的几何验证属性：";

CATStepMsg.Exp.TVPofPart="零件级别的 3D 网格数据验证属性：";

CATStepMsg.Imp.FTA_Unknown="<E> 3D 标注 /p1 有未知 STEP 类型 /p1";

CATStepMsg.Imp.FTA_Empty="<E> 3D 标注 /p1 无图形展示";

CATStepMsg.Exp.FTA_NoView="<E> 在 3D 标注范围（/p1 中的 /p2）中未找到 3D 标注视图。请使用规则 FTA_12 的 CATDUA 清除 CATPart，再导出";

CATStepMsg.Exp.FTA_EmptyView="<W> 3D 标注视图 /p1 为空。不受 STEP 支持。";

CATStepMsg.Imp.FTA_NoLinks="<W> 3D 标注 /p1 没有几何链接";

CATStepMsg.Imp.FTA_ValidationPropertiesKO="<E> 3D 标注 /p1 的属性验证失败";

CATStepMsg.Imp.FTA_ValidationProperties1="3D 标注期望数量：/p1，找到：/p2";

CATStepMsg.Imp.FTA_ValidationProperties2="捕获期望数量：/p1，找到：/p2";

CATStepMsg.Imp.FTA_ValidationProperties="3D 标注验证属性检查：";

CATStepMsg.Imp.FTA_ValidationProperties3="捕获：/p1 - 3D 标注期望数量：/p2，找到：/p3";

CATStepMsg.Imp.FTA_TolerancesVP="3D 标注曲线长度和 质心最大偏差：/p1mm";

CATStepMsg.Imp.Tessellation_TolerancesVP1="网格曲面面积最大偏差：/p1% 或 /p2mm2";

CATStepMsg.Imp.Tessellation_TolerancesVP2="网格曲线长度最大偏差：/p1% 或 /p2mm";

CATStepMsg.Imp.Tessellation_TolerancesVP3="网格曲面或曲线质心最大偏差：/p1mm";

CATStepMsg.Imp.AVP_Tolerance="假设的质心最大偏差： /p1mm";

// Traduction pour le HEADER STEP

CATStepMsg.Imp.OriginatingSystem="原始系统                               ： ";

CATStepMsg.Imp.PreprocessorVersion="预处理器版本                             ： ";

CATStepMsg.Imp.FileSchema="文件模式                                      ： ";

CATStepMsg.Exp.WarningNoLicense="未授权 STEP 许可证。请仅确认产品结构的 STEP 导出";

CATStepMsg.Imp.WarningNoLicense="未授权 STEP 许可证。几何图形 STEP 未转换。";

CATStepMsg.Exp.WarningTitle="警告";

CATStepMsg.Imp.UDA_ValidationProperties="检查产品 /p1 的 UDA 验证属性";

CATStepMsg.Imp.UDA_ComputedReadProperties="/p1：已计算的属性：/p2 - 读取属性：/p3";

CATStepMsg.Imp.UDA_StatusOK="/p1 - UDA 状态：OK";

CATStepMsg.Imp.UDA_StatusKO="/p1 - UDA 状态：KO";

CATStepMsg.Imp.UDA_Part="零件";

CATStepMsg.Imp.UDA_Point="点";

CATStepMsg.Imp.UDA_Wire="线";

CATStepMsg.Imp.UDA_Shell="盒体";

CATStepMsg.Imp.UDA_Solid="实体";

CATStepMsg.Imp.UDA_String="字符串";

CATStepMsg.Imp.UDA_Integer="整数";

CATStepMsg.Imp.UDA_Real="实数";

CATStepMsg.Imp.UDA_Boolean="布尔";

CATStepMsg.Imp.Warning_UDA_unsupported="警告：/p1 UDA 导入为“字符串”，而非“使用单位测量”";

CATStepMsg.Exp.WrongUnitForStpx="<W> 装配中使用的单位是毫米，而不是英寸";

ExportOneFile_stpx.Request="无法导出";
ExportOneFile_stpx.Diagnostic="STEP XML 导出与选项“一个 STEP 文件”不兼容";
ExportOneFile_stpx.Advice="在 STEP 选项面板中，选择应用协议 242，并为导出的装配选择不同于“一个 STEP 文件”的类型";

ExportWrongAP_stpx.Request="无法导出";
ExportWrongAP_stpx.Diagnostic="仅在使用应用协议 242(AP242) 时，STEP XML 导出才可用";
ExportWrongAP_stpx.Advice="在 STEP 选项面板中，选择 AP242";


