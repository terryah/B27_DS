//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 2000 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptManip
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    30.10.2000
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
// 	01		lgk	17.12.02 Revision
//=====================================================================================

toolViewVisu.HeaderFrame.Global.Title="视图可视化";
toolViewVisu.HeaderFrame.Global.LongHelp="视图可视化";
toolViewVisu.IconAndOptionsFrame.OptionsFrame.chkHideIn3D.Title="在 3D 中隐藏";
toolViewVisu.IconAndOptionsFrame.OptionsFrame.chkHideIn3D.LongHelp="在 3D 中隐藏";

toolLayoutView.HeaderFrame.Global.Title="视图背景";
toolLayoutView.HeaderFrame.Global.LongHelp="视图背景";
toolLayoutView.IconAndOptionsFrame.OptionsFrame.lblBackgroundMod.Title="显示模式： ";
toolLayoutView.IconAndOptionsFrame.OptionsFrame.lblBackgroundMod.LongHelp="显示模式： ";
toolLayoutView.IconAndOptionsFrame.OptionsFrame.chkActiv2DMode.Title="激活 2D 的可视化模式";
toolLayoutView.IconAndOptionsFrame.OptionsFrame.chkActiv2DMode.LongHelp="在所创建的视图中激活 2D 可视化模式。";

toolViewFilter.HeaderFrame.Global.Title="过滤器";
toolViewFilter.HeaderFrame.Global.LongHelp="过滤器";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radNoFilter.Title="不应用任何过滤器";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radNoFilter.LongHelp="在新创建的视图中不应用任何过滤器。";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radDefaultFilter.Title="应用默认过滤器";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radDefaultFilter.LongHelp="应用默认过滤器。";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radDialogBoxFilter.Title="显示“布局视图过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radDialogBoxFilter.LongHelp="显示“布局视图过滤器”对话框。";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radDedicatedFilter.Title="创建专用过滤器";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmFilterCreation.frmFilterCreationlocked.radDedicatedFilter.LongHelp="创建专用过滤器。";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.Lbl3.Title="类型：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.Lbl3.LongHelp="类型";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmDedicatedFilter.frmDedicatedFilterlocked.radDisplayInBack.Title="在背景中显示";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmDedicatedFilter.frmDedicatedFilterlocked.radDisplayInBack.LongHelp="在背景中显示。";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmDedicatedFilter.frmDedicatedFilterlocked.radMaskInBack.Title="在背景中遮罩";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.frmDedicatedFilter.frmDedicatedFilterlocked.radMaskInBack.LongHelp="在背景中遮罩。";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.chkEditFilterDlgBox.Title="显示“编辑过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.chkEditFilterDlgBox.LongHelp="显示“编辑过滤器”对话框。";

toolViewFilter.IconAndOptionsFrame.OptionsFrame.LblPrimaryView.Title="主视图：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.EditFilterDlgForPrimaryView.Title="显示“编辑过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.LblPrincipalView.Title="主视图：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.EditFilterDlgForPrincipalView.Title="显示“编辑过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.HeaderFrame.Global.Title="参考角度视图";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.HeaderFrame.Global.LongHelp="参考角度视图";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.LblAuxViewFrom.Title="辅助视图：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.EditFilterDlgForAuxViewFrom.Title="显示“编辑过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.LblAxoViewFrom.Title="轴测视图：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.EditFilterDlgForAxoViewFrom.Title="显示“编辑过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.LblSectionViewFrom.Title="剖视图和截面分割视图：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.EditFilterDlgForSectionViewFrom.Title="显示“编辑过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.chkFilterBodyOfSupport.Title="根据选定的参考填充过滤器";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolViewFromFilter.IconAndOptionsFrame.OptionsFrame.chkFilterBodyOfSupport.LongHelp="选择几何图形作为视图的支持面时，其包含集（2D 视图截面、零件几何体、几何图形集或有序几何图形集）将添加至创建的显示过滤器。\n此选项仅适用于“参考角度视图”命令。";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolProfileViewFilter.HeaderFrame.Global.Title="轮廓角度视图";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolProfileViewFilter.HeaderFrame.Global.LongHelp="轮廓角度视图";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolProfileViewFilter.IconAndOptionsFrame.OptionsFrame.LblAuxProfileView.Title="辅助视图：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolProfileViewFilter.IconAndOptionsFrame.OptionsFrame.EditFilterDlgForAuxProfileView.Title="显示“编辑过滤器”对话框";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolProfileViewFilter.IconAndOptionsFrame.OptionsFrame.LblSectionProfileView.Title="剖视图和截面分割视图：";
toolViewFilter.IconAndOptionsFrame.OptionsFrame.toolProfileViewFilter.IconAndOptionsFrame.OptionsFrame.EditFilterDlgForSectionProfileView.Title="显示“编辑过滤器”对话框";
FilterOption_NoFilter="无过滤器";
FilterOption_DefaultFilter="默认过滤器";
FilterOption_DisplayFilter="显示过滤器";
FilterOption_MaskFilter="掩码过滤器";
FilterOption_ListFilters="列表过滤器";
FilterOption_ActiveViewFilter="活动视图过滤器";
FilterOption_ReferenceViewFilter="参考视图过滤器";

tool3DGrid.HeaderFrame.Global.Title="3D 网格";
tool3DGrid.HeaderFrame.Global.LongHelp="3D 网格";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridDisplay.Title="显示";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridDisplay.LongHelp="显示 3D 网格。";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.lblLabelPosition.Title="标注位置：";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.lblLabelPosition.LongHelp="标注位置。";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelTop.Title="顶部";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelTop.LongHelp="在 3D 网格顶部显示标注。";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelBottom.Title="底部";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelBottom.LongHelp="在 3D 网格底部显示标注。";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelLeft.Title="左侧";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelLeft.LongHelp="在 3D 网格左侧显示标注。";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelRight.Title="右侧";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridLabelRight.LongHelp="在 3D 网格右侧显示标注。";

tool3DGrid.IconAndOptionsFrame.OptionsFrame.lbl3DGridFontName.Title="字体：";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.lbl3DGridFontName.LongHelp="网格标号的字体名称。";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.lbl3DGridFontSize.Title="大小：";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.lbl3DGridFontSize.LongHelp="网格标号的字体大小。";

tool3DGrid.IconAndOptionsFrame.OptionsFrame.lblGridOverrun.Title="网格超限：";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.lblGridOverrun.LongHelp="网格超限。";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridAutoGridSize.Title="自动网格大小";
tool3DGrid.IconAndOptionsFrame.OptionsFrame.chk3DGridAutoGridSize.LongHelp="根据视图大小自动计算网格大小";

toolClipFrm.HeaderFrame.Global.Title="裁剪框架";
toolClipFrm.HeaderFrame.Global.LongHelp="裁剪框架";
toolClipFrm.IconAndOptionsFrame.OptionsFrame.chkClippingFrame.Title="激活";
toolClipFrm.IconAndOptionsFrame.OptionsFrame.chkClippingFrame.LongHelp="创建视图时激活裁剪框架";

toolClipOut.HeaderFrame.Global.Title="裁剪轮廓";
toolClipOut.HeaderFrame.Global.LongHelp="裁剪轮廓";
toolClipOut.IconAndOptionsFrame.OptionsFrame.chkDisplayClipOut.Title="显示";
toolClipOut.IconAndOptionsFrame.OptionsFrame.chkDisplayClipOut.LongHelp="显示已裁剪视图的裁剪轮廓。";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.lblColor.Title="颜色";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.lblColor.LongHelp="定义裁剪轮廓的颜色。";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.lblLinetype.Title="线型";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.lblLinetype.LongHelp="定义裁剪轮廓的线型。";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.lblThickness.Title="厚度";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.lblThickness.LongHelp="定义裁剪轮廓的线宽。";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.cmbOutlineColor.Title="颜色";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.cmbOutlineColor.LongHelp="定义裁剪轮廓的颜色。";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.cmbOutlineLinetype.Title="线型";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.cmbOutlineLinetype.LongHelp="定义裁剪轮廓的线型。";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.cmbOutlineThickness.Title="厚度";
toolClipOut.IconAndOptionsFrame.OptionsFrame.frmOutProp.cmbOutlineThickness.LongHelp="定义裁剪轮廓的线宽。";

toolBackClip.HeaderFrame.Global.Title="远端裁剪平面";
toolBackClip.HeaderFrame.Global.LongHelp="远端裁剪平面";
toolBackClip.IconAndOptionsFrame.OptionsFrame.chkBackClippingPlane.Title="激活";
toolBackClip.IconAndOptionsFrame.OptionsFrame.chkBackClippingPlane.LongHelp="在所创建的视图中激活远端裁剪平面";

toolMultiPlaneSection.HeaderFrame.Global.Title="多平面剖视图";
toolMultiPlaneSection.HeaderFrame.Global.LongHelp="在多平面剖视图创建期间定义选项";
toolMultiPlaneSection.IconAndOptionsFrame.OptionsFrame.chkDisplaySubViewsInSpecTree.Title="在结构树中显示子视图";
toolMultiPlaneSection.IconAndOptionsFrame.OptionsFrame.chkDisplaySubViewsInSpecTree.LongHelp="显示结构树中多平面剖视图节点下的子视图子节点";
toolMultiPlaneSection.IconAndOptionsFrame.OptionsFrame.chkDispBoundaries.Title="显示除末端边界之外的所有边界";
toolMultiPlaneSection.IconAndOptionsFrame.OptionsFrame.chkDispBoundaries.LongHelp="创建视图时显示除末端边界之外的所有边界";
toolMultiPlaneSection.IconAndOptionsFrame.OptionsFrame.frmColProp.lblColor.Title="颜色";
toolMultiPlaneSection.IconAndOptionsFrame.OptionsFrame.frmColProp.lblLinetype.Title="线型";
toolMultiPlaneSection.IconAndOptionsFrame.OptionsFrame.frmColProp.lblThickness.Title="厚度";


toolCallout.HeaderFrame.Global.Title="标注";
toolCallout.HeaderFrame.Global.LongHelp="标注";
toolCallout.IconAndOptionsFrame.OptionsFrame.chkCreateCallout.Title="在“参考角度视图”命令末尾的活动视图中创建标注。";
toolCallout.IconAndOptionsFrame.OptionsFrame.chkCreateCallout.LongHelp="在“参考角度视图”命令末尾的活动视图中创建标注。";

toolNewViewFrom.HeaderFrame.Global.Title="参考角度视图";
toolNewViewFrom.HeaderFrame.Global.LongHelp="定义“参考角度视图”命令的选项。"; // @validatedUsage DCF 13:10:14 options
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkSetOrientAsActiveView.Title="根据活动视图设置方向";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkSetOrientAsActiveView.LongHelp="根据当前 3D 视图相对的方向设置已创建视图图纸上的方向。";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkAlignAsActiveView.Title="根据活动视图对齐（SHIFT 切换）";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkAlignAsActiveView.LongHelp="已创建视图的图纸位置将根据其相对于 3D 中当前视图的位置来对齐。
按“SHIFT”键来临时激活/取消激活此选项。";

toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.HeaderFrame.Global.Title="垂直于活动视图的参考";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.HeaderFrame.Global.LongHelp="垂直于活动视图的参考";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.HeaderFrame.Global.Title="非垂直于活动视图的参考";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.HeaderFrame.Global.LongHelp="非垂直于活动视图的参考";

toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.IconAndOptionsFrame.OptionsFrame.PerLabel.Title="根据活动视图对齐（SHIFT 切换）";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnFoldingDirForPerpRef.Title="沿折叠方向";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnFoldingDirForPerpRef.LongHelp="根据当前 3D 视图相对的位置，沿折叠方向对齐已创建视图图纸上的位置。";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnPerpDirForPerpRef.Title="垂直于折叠方向";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnPerpDirForPerpRef.LongHelp="根据当前 3D 视图相对的位置，垂直于折叠方向对齐已创建视图图纸上的位置。";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkSetOrientAsActiveViewForPerpRef.Title="根据活动视图设置方向";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkSetOrientAsActiveViewForPerpRef.LongHelp="根据当前 3D 视图相对的方向设置已创建视图图纸上的方向。";

toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.IconAndOptionsFrame.OptionsFrame.NonPerLabel.Title="根据活动视图对齐（SHIFT 切换）";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnFoldingDirForNonPerpRef.Title="沿折叠方向";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnFoldingDirForNonPerpRef.LongHelp="根据当前 3D 视图相对的位置，沿折叠方向对齐已创建视图图纸上的位置。";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnPerpDirForNonPerpRef.Title="垂直于折叠方向";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkAlignOnPerpDirForNonPerpRef.LongHelp="根据当前 3D 视图相对的位置，垂直于折叠方向对齐已创建视图图纸上的位置。";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkSetOrientAsActiveViewForNonPerpRef.Title="根据活动视图设置方向";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.ptoolNonPerpRefSetting.IconAndOptionsFrame.OptionsFrame.chkSetOrientAsActiveViewForNonPerpRef.LongHelp="根据当前 3D 视图相对的方向设置已创建视图图纸上的方向。";

toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkCreateCallout.Title="在活动视图中创建标注";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkCreateCallout.LongHelp="在“参考角度视图”命令末尾的活动视图中创建标注。";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkActivateCreatedView.Title="激活创建的视图";
toolNewViewFrom.IconAndOptionsFrame.OptionsFrame.chkActivateCreatedView.LongHelp="创建标注（如需要）后，在“参考角度视图”命令结束时激活创建的视图。";

toolCalloutCreation.HeaderFrame.Global.Title="创建标注";
toolCalloutCreation.HeaderFrame.Global.LongHelp="创建标注";
toolCalloutCreation.IconAndOptionsFrame.OptionsFrame.chkEnableCreateCallout.Title="允许创建非垂直视图的标注";
toolCalloutCreation.IconAndOptionsFrame.OptionsFrame.chkEnableCreateCallout.LongHelp="垂直于参考视图的视图标注创建不再受限。";
toolCalloutCreation.IconAndOptionsFrame.OptionsFrame.lblCalloutSizeOverrun.Title="标注大小超限：";
toolCalloutCreation.IconAndOptionsFrame.OptionsFrame.lblCalloutSizeOverrun.LongHelp="标注大小超限。";

toolViewLayout.HeaderFrame.Global.Title="视图布局";
toolViewLayout.HeaderFrame.Global.LongHelp="视图布局";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblOffsetFromSheet.Title="从图纸的偏移：";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblOffsetFromSheet.LongHelp="在“视图布局”命令期间定义从图纸大小的偏移尺寸以放置视图。";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblOffsetBetweenViews.Title="视图间的偏移：";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblOffsetBetweenViews.LongHelp="在“视图布局”命令期间定义视图之间的偏移尺寸以放置视图。";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblMinReductionMultiplier.Title="最小缩小倍数：";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblMinReductionMultiplier.LongHelp="在视图布局命令期间定义可应用于视图的最小缩小倍数。";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblMaxEnlargementMultiplier.Title="最大放大倍数：";
toolViewLayout.IconAndOptionsFrame.OptionsFrame.lblMaxEnlargementMultiplier.LongHelp="在视图布局命令期间定义可应用于视图的最大放大倍数。";
