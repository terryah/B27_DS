//-----------------------------------------------------------------
// Fichier de ressource utilise par la classe CATGSMUIFilletPanel
// En_US
//-----------------------------------------------------------------


Title="Fillet Definition";

FraCombo.LabelList.Title=" Fillet type: ";

FilletTypeSphere="Sphere";
FilletTypeCircle="Circle";

FilletBiTangent="BiTangent Fillet";
FilletTriTangent="TriTangent Fillet";

FraFillet.FraFillet1.LabelElem1.Title="Support 1: ";
FraFillet.FraFillet1.LabelElem2.Title="Support 2: ";
FraFillet.FraFillet1.LabelElem3.Title="Support to remove: ";
FraFillet.FraFillet1.LabelSpine.Title="Spine: ";
FraFillet.FraFillet1.LabelLitRadius.Title="Radius: ";
FraFillet.FraFillet1.FraRadius.EnglobingFrame.IntermediateFrame.Spinner.Title="Radius";
FraFillet.Variable.LabelHoldCurve.Title="Hold Curve: ";
FraFillet.Variable.LabelSpine.Title="Spine: ";
FraFillet.Variable.LabEltsToKeep.Title="Faces to keep: ";

FraFillet.FraFillet1.Law.Title="Law...";
FraFillet.Variable.LabLawDelimiter1.Title="Law Relimiter 1: ";
FraFillet.Variable.LabLawDelimiter2.Title="Law Relimiter 2: ";

FraFillet.FraFillet1.LabelRibbonRelimitation.Title="Extremities: ";
RibbonRelimitationModePLine="Straight";
RibbonRelimitationModeSpline="Smooth";
RibbonRelimitationModeMin="Minimum";
RibbonRelimitationModeMax="Maximum";

// FraFillet.CheckTrimSupports.Title="Trim support elements";
FraFillet.FraFillet1.CheckTrimSupport1.Title="Trim support 1";
FraFillet.FraFillet1.CheckTrimSupport2.Title="Trim support 2";


FraFillet.FraFillet1.RadiusTypeFrame.RadiusRadioButton.Title="Radius";
FraFillet.FraFillet1.RadiusTypeFrame.ChordalRadioButton.Title="Chordal";

FraFillet.FraFillet1.ConicalSectionFrame.ConicCheckButton.Title="Conic parameter:";

FraFillet.FraFillet1.LabelLitChordLength.Title="Length:";

FraFillet.Variable.Title="Optional elements";

MoreInfo="More >>";
LessInfo="Less <<";
FraFillet.FraFillet1.ButtonMore.ShortHelp="Hide or show extended information";

//-----------------------------------------------------------------
FraCombo.LabelList.LongHelp=
"Type of fillet to be made 
between support surfaces.";

FraCombo.CmbList.LongHelp=
"Type of fillet to be made 
between support surfaces.";

FraFillet.FraFillet1.LabelElem1.LongHelp=
"Specifies first support surface.";

FraFillet.FraFillet1.FraElem1.LongHelp=
"Specifies first support surface.";

FraFillet.FraFillet1.LabelElem2.LongHelp=
"Specifies second support surface.";

FraFillet.FraFillet1.FraElem2.LongHelp=
"Specifies second support surface.";

FraFillet.FraFillet1.LabelElem3.LongHelp=
"Specifies the support surface to remove.";

FraFillet.FraFillet1.FraElem3.LongHelp=
"Specifies the support surface to remove.";

FraFillet.Variable.LabelSpine.LongHelp=
"Specifies spine.";

FraFillet.Variable.FraHoldSpine.LongHelp=
"Specifies spine.";

FraFillet.FraFillet1.LabelLitRadius.LongHelp="Specifies the fillet radius value.";

FraFillet.FraFillet1.LabelRibbonRelimitation.LongHelp=
"Type of fillet connection 
between support surfaces";

FraFillet.FraFillet1.CmbRibbonRelimitation.LongHelp=
"Type of fillet connection 
between support surfaces";

FraFillet.FraFillet1.CheckTrimSupport1.LongHelp=
"Trim the first support surface 
with the fillet.";

FraFillet.FraFillet1.CheckTrimSupport2.LongHelp=
"Trim the second support surface 
with the fillet.";

FraFillet.Variable.LabelHoldCurve.LongHelp=
"Specifies a hold curve 
to control fillet radius.";

FraFillet.Variable.FraHoldCurve.LongHelp=
"Specifies a hold curve 
to control fillet radius.";

FraFillet.FraFillet1.Law.LongHelp=
"Specifies a radius law
in case of fillet with spine.";

FraFillet.Variable.LabLawDelimiter1.LongHelp=
"Specifies a point on spine 
to delimit the radius law range.";

FraFillet.Variable.FraLawDelimiter1.LongHelp=
"Specifies a point on spine 
to delimit the radius law range.";

FraFillet.Variable.LabLawDelimiter2.LongHelp=
"Specifies a point on spine 
to delimit the radius law range.";

FraFillet.Variable.FraLawDelimiter2.LongHelp=
"Specifies a point on spine 
to delimit the radius law range.";

FraFillet.Variable.LabEltsToKeep.LongHelp=
"Specifies the faces to keep 
to manage multi-ribbons.";

FraFillet.Variable.FraEltsToKeep.LongHelp=
"Specifies the faces to keep 
to manage multi-ribbons.";

FraFillet.FraFillet1.RadiusTypeFrame.RadiusRadioButton.LongHelp=
"Chooses between radius mode and chord length mode.";

FraFillet.FraFillet1.RadiusTypeFrame.ChordalRadioButton.LongHelp=
"Chooses between radius mode and chord length mode.";

FraFillet.FraFillet1.ConicalSectionFrame.ConicCheckButton.LongHelp=
"Specifies conic parameter for fillet conical cross section.";

FraFillet.FraFillet1.LabelLitChordLength.LongHelp=
"Specifies the fillet chord length value.";


ElementsListTitle="Faces to keep";

ChaineManipulator="Manipulator";
	
