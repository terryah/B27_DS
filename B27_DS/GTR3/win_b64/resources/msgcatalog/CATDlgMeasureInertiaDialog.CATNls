CommandModeDialogTitle    = "Measure Inertia";
EditModeDialogTitle       = "Measure Inertia (edit)";

IndicateInertiaMsg        = "Indicate item to be measured";

IndicateAxisMsg           = "Indicate axis to be selected";

IndicatePtMsg             = "Indicate point to be selected";

IndicateAxisSystemMsg     = "Indicate axis system to be selected";

SwitchMode				  = "Switch to Design or Visu Mode to compute a valid measure inertia \n and take into account the correct density value";

CancelButton              = "Close";

// Example usage:
//    "Point on Product.1"
//    "Line in Product.2"

OnLabel = " on ";
InLabel = " in ";

// Definition

DefFrame.SeparatorDefFrame.LabelDef.Title = "Definition ";
//MainFrame.DefFrame.SeparatorDefFrame.LabelDef.Title = "Definition ";

// Selection

DefFrame.SubDefFrame.SelectionFrame.LabelSelection.Title = "Selection :";
//MainFrame.DefFrame.SubDefFrame.SelectionFrame.LabelSelection.Title = "Selection :";
NoSelection = "No selection";

// Button

DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.Title     = "Measure Inertia 3D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.ShortHelp = "Measure Inertia 3D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.Help      = "Measure inertia of surfacic or volumic item";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.LongHelp  = "Measure inertia of surfacic or volumic item";

DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.Title     = "Measure Inertia 2D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.ShortHelp = "Measure Inertia 2D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.Help      = "Measure inertia of plane item";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.LongHelp  = "Measure inertia of plane item";

// Result

MainFrame.ResultFrame.SeparatorResultFrame.LabelResult.Title = "Result ";


MainFrame.ResultFrame.DescriptionFrame.CalculationModeFrame.InertiaCalculationMode.Title = "Calculation mode :  ";
MainFrame.ResultFrame.DescriptionFrame.TypeItemFrame.TypeItemLabel.Title = "Type :  ";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.Title    = "Characteristics";
MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.LongHelp = "Gives the area, volume, density and mass of the selected item"; 

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.AreaFrame_L.AreaLabel.Title         = "Area";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.AreaFrame_L.AreaLabel.LongHelp    = "Area";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.VolumeFrame_L.VolumeLabel.Title       = "Volume";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.VolumeFrame_L.VolumeLabel.LongHelp  = "Volume";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.DensityFrame_L.DensityLabel.Title    = "Density";
MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.DensityFrame_L.DensityLabel.LongHelp = "Gives the density  or surfacic mass of the selected item. Density is that of the material applied to the part.\nDefault values: 1000 kg/m3 for volumes and 10 kg/m2 for surfaces.\nNot uniform: sub-products have different densities.";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.MassFrame_L.MassLabel.Title         = "Mass";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.MassFrame_L.MassLabel.LongHelp    = "Mass";

MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.Title                   = "Center Of Gravity (G)";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.LongHelp                = "Gives the coordinates of the center of gravity";


MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGxFrame_L.GXLabel.Title = "Gx";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGyFrame_L.GYLabel.Title = "Gy";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGzFrame_L.GZLabel.Title = "Gz";


// CalculationMode

IdCalculationMode                       = "Calculation mode :  ";
IdCalculationModeResultExactThenApprox  = "Exact else approximate";
IdCalculationModeResultExact            = "Exact";
IdCalculationModeResultApprox           = "Approximate";

// type

SurfaceType = "Surface";
VolumeType  = "Volume";

// density

SurfacicMass = "Surfacic mass";
VolumicMass  = "Density";


//
// Item labels
// Enforce same length temporarily
//
Results.Title                                        = "Results";

Results.IdentifierFrame.Title                        = "Description";
Results.IdentifierFrame.LongHelp                     = "Identifies the item measured and indicates whether it is a volume or surface";

Results.EquivalentFrame.Title                        = "Equivalent";
Results.EquivalentFrame.LongHelp                     = "Indicates whether inertia equivalents (user parameters imported via CATIA knowledgeware) are taken into account.\n0: the measure is made on the selection, geometry or assembly.\n1 or more: 1 or more inertia equivalents are taken into account";

Results.CharacCoFGFrame.CharacFrame.Title                            = "Characteristics";
Results.CharacCoFGFrame.CharacFrame.LongHelp                         = "Gives the area, volume, density and mass of the selected item"; 

Results.CharacCoFGFrame.CharacFrame.AreaLabel_F.AreaLabel.Title         = "Area";
//Results.CharacCoFGFrame.CharacFrame.AreaLabel_F.AreaLabel.LongHelp    = "Area";

Results.CharacCoFGFrame.CharacFrame.VolumeLabel_F.VolumeLabel.Title       = "Volume";
//Results.CharacCoFGFrame.CharacFrame.VolumeLabel_F.VolumeLabel.LongHelp  = "Volume";

Results.CharacCoFGFrame.CharacFrame.DensityLabel_F.DensityLabel.Title    = "Density";
Results.CharacCoFGFrame.CharacFrame.DensityLabel_F.DensityLabel.LongHelp = "Gives the density  or surface density of the selected item. Density is that of the material applied to the part.\nDefault values: 1000 kg/m3 for volumes and 10 kg/m2 for surfaces.\nNot uniform: sub-products have different densities.";

Results.CharacCoFGFrame.CharacFrame.MassLabel_F.MassLabel.Title         = "Mass";
//Results.CharacCoFGFrame.CharacFrame.MassLabel_F.MassLabel.LongHelp    = "Mass";

      
Results.CharacCoFGFrame.CenterOfGravityFrame.Title                   = "Center Of Gravity (G)";
Results.CharacCoFGFrame.CenterOfGravityFrame.LongHelp                = "Gives the coordinates of the center of gravity";

Results.MomentsFrame.Title                           = "Principal Moments / G";
Results.MomentsFrame.LongHelp                        = "Gives the principal moments M of inertia calculated with respect to the center of gravity";

Results.AxesFrame.Title        	                     = "Principal Axes";
Results.AxesFrame.LongHelp     	                     = "Gives the principal axes A about which inertia is calculated";

Results.InertiaFrame.Title                           = "Inertia Matrix / G";
Results.InertiaFrame.LongHelp                        = "Gives the matrix of inertia I calculated with respect to the center of gravity G";

Results.AxisFrame.Title                              = "Moment / Axis";
Results.AxisFrame.LongHelp                           = "Gives the equation O and direction vector D of the selected axis as well as the moment of inertia Ma about the axis and the radius of gyration R";
 
Results.Inertia_O_Frame.Title                        = "Inertia Matrix / O";
Results.Inertia_O_Frame.LongHelp                     = "Gives the matrix of inertia I calculated with respect to the origin  O of the document";

Results.Inertia_P_Frame.Title                        = "Inertia Matrix / P";
Results.Inertia_P_Frame.LongHelp                     = "Gives the matrix of inertia I with respect to the selected point P as well as the coordinates of the point";

Results.Inertia_AS_Frame.Title                       = "Inertia Matrix / Axis System A";
Results.Inertia_AS_Frame.LongHelp                    = "Gives the matrix of inertia I with respect to the selected axis system A as well as the origin O and (U, V, W)-vectors of the axis system";



// SpacerLabel enables offset equivalent to X or Y or Z
SpacerLabel   = "   ";


AreaLabel     =  "Area";
VolumeLabel   =  "Volume";
DensityLabel  =  "Density";
MassLabel     =  "Mass";

DensityInfoLabel       =  "  Default value";
DensityInfoLabelEmpty  =  "               ";

MaterialLabel          =  "Material";
MaterialLabelDefault   =  "Default";


// ----------- Inertia Center

Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GXLabel.Title = "Gx";
Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GYLabel.Title = "Gy";
Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GZLabel.Title = "Gz";

GXLabel = "Gx";
GYLabel = "Gy";
GZLabel = "Gz";


// ----------- Principal Moments

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.Title    = "Principal Moments / G";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.LongHelp = "Gives the principal moments M of inertia calculated with respect to the center of gravity";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M1_F.M1Label.Title = "M1";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M2_F.M2Label.Title = "M2";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M3_F.M3Label.Title = "M3";


Results.MomentsFrame.DataFrame.M1Label.Title = "M1";
Results.MomentsFrame.DataFrame.M2Label.Title = "M2";
Results.MomentsFrame.DataFrame.M3Label.Title = "M3";


M1Label = "M1";
M2Label = "M2";
M3Label = "M3";

// ----------- Principals Axes

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.Title    = "Principal Axes";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.LongHelp = "Gives the principal axes A about which inertia is calculated";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1X_F.A1XLabel.Title = "A1x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1Y_F.A1YLabel.Title = "A1y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1Z_F.A1ZLabel.Title = "A1z";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2X_F.A2XLabel.Title = "A2x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2Y_F.A2YLabel.Title = "A2y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2Z_F.A2ZLabel.Title = "A2z";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3X_F.A3XLabel.Title = "A3x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3Y_F.A3YLabel.Title = "A3y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3Z_F.A3ZLabel.Title = "A3z";


Results.AxesFrame.DataFrame.A1XLabel.Title = "A1x";
Results.AxesFrame.DataFrame.A1YLabel.Title = "A1y";
Results.AxesFrame.DataFrame.A1ZLabel.Title = "A1z";

Results.AxesFrame.DataFrame.A2XLabel.Title = "A2x";
Results.AxesFrame.DataFrame.A2YLabel.Title = "A2y";
Results.AxesFrame.DataFrame.A2ZLabel.Title = "A2z";

Results.AxesFrame.DataFrame.A3XLabel.Title = "A3x";
Results.AxesFrame.DataFrame.A3YLabel.Title = "A3y";
Results.AxesFrame.DataFrame.A3ZLabel.Title = "A3z";

A1XLabel = "A1x";
A1YLabel = "A1y";
A1ZLabel = "A1z";

A2XLabel = "A2x";
A2YLabel = "A2y";
A2ZLabel = "A2z";

A3XLabel = "A3x";
A3YLabel = "A3y";
A3ZLabel = "A3z";

// ----------- Inertia Matrix

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.Title = "Inertia / G";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.LongHelp = "Gives the matrix of inertia I calculated with respect to the center of gravity G";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.Title    = "Inertia Matrix / G";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXX_F.IXXLabel.Title = "IoxG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IYY_F.IYYLabel.Title = "IoyG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IZZ_F.IZZLabel.Title = "IozG";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXY_F.IXYLabel.Title = "IxyG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXZ_F.IXZLabel.Title = "IxzG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IYZ_F.IYZLabel.Title = "IyzG";

Results.InertiaFrame.DataFrame.IXXLabel.Title = "IoxG";
Results.InertiaFrame.DataFrame.IXYLabel.Title = "IxyG";
Results.InertiaFrame.DataFrame.IXZLabel.Title = "IxzG";
Results.InertiaFrame.DataFrame.IYXLabel.Title = "IyxG";
Results.InertiaFrame.DataFrame.IYYLabel.Title = "IoyG";
Results.InertiaFrame.DataFrame.IYZLabel.Title = "IyzG";
Results.InertiaFrame.DataFrame.IZXLabel.Title = "IzxG";
Results.InertiaFrame.DataFrame.IZYLabel.Title = "IzyG";
Results.InertiaFrame.DataFrame.IZZLabel.Title = "IozG";

IXXLabel = "IoxG";
IYYLabel = "IoyG";
IZZLabel = "IozG";
IXYLabel = "IxyG";
IXZLabel = "IxzG";
IYZLabel = "IyzG";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.Title = "Inertia / O";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.LongHelp = "Gives the matrix of inertia I calculated with respect to the origin  O of the document";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.Title    = "Inertia Matrix / O";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXX_O_F.IXXLabel.Title = "IoxO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IYY_O_F.IYYLabel.Title = "IoyO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IZZ_O_F.IZZLabel.Title = "IozO";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXY_O_F.IXYLabel.Title = "IxyO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXZ_O_F.IXZLabel.Title = "IxzO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IYZ_O_F.IYZLabel.Title = "IyzO";

Results.Inertia_O_Frame.DataFrame.IXXLabel.Title = "IoxO";
Results.Inertia_O_Frame.DataFrame.IXYLabel.Title = "IxyO";
Results.Inertia_O_Frame.DataFrame.IXZLabel.Title = "IxzO";
Results.Inertia_O_Frame.DataFrame.IYXLabel.Title = "IyxO";
Results.Inertia_O_Frame.DataFrame.IYYLabel.Title = "IoyO";
Results.Inertia_O_Frame.DataFrame.IYZLabel.Title = "IyzO";
Results.Inertia_O_Frame.DataFrame.IZXLabel.Title = "IzxO";
Results.Inertia_O_Frame.DataFrame.IZYLabel.Title = "IzyO";
Results.Inertia_O_Frame.DataFrame.IZZLabel.Title = "IozO";

IOXXLabel = "IoxO";
IOYYLabel = "IoyO";
IOZZLabel = "IozO";
IOXYLabel = "IxyO";
IOXZLabel = "IxzO";
IOYZLabel = "IyzO";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.Title = "Inertia / P";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.LongHelp = "Gives the matrix of inertia I with respect to the selected point P as well as the coordinates of the point";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.Title    = "Inertia Matrix / P";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXX_P_F.IXXLabel.Title = "IoxP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IYY_P_F.IYYLabel.Title = "IoyP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IZZ_P_F.IZZLabel.Title = "IozP";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXY_P_F.IXYLabel.Title = "IxyP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXZ_P_F.IXZLabel.Title = "IxzP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IYZ_P_F.IYZLabel.Title = "IyzP";


Results.Inertia_P_Frame.P_I1_Editor_F.IXXLabel.Title = "IoxP";
Results.Inertia_P_Frame.P_I1_Editor_F.IYYLabel.Title = "IoyP";
Results.Inertia_P_Frame.P_I1_Editor_F.IZZLabel.Title = "IozP";

Results.Inertia_P_Frame.P_I2_Editor_F.IXYLabel.Title = "IxyP";
Results.Inertia_P_Frame.P_I2_Editor_F.IXZLabel.Title = "IxzP";
Results.Inertia_P_Frame.P_I2_Editor_F.IYZLabel.Title = "IyzP";


IPXXLabel = "IoxP";
IPYYLabel = "IoyP";
IPZZLabel = "IozP";
IPXYLabel = "IxyP";
IPXZLabel = "IxzP";
IPYZLabel = "IyzP";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.Title = "Inertia / Axis System";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.LongHelp = "Gives the matrix of inertia I with respect to the selected axis system A as well as the origin O and (U, V, W)-vectors of the axis system";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.Title    = "Inertia Matrix / Axis System A";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXX_AS_F.IXXLabel.Title = "IoxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IYY_AS_F.IYYLabel.Title = "IoyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IZZ_AS_F.IZZLabel.Title = "IozA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXY_AS_F.IXYLabel.Title = "IxyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXZ_AS_F.IXZLabel.Title = "IxzA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IYZ_AS_F.IYZLabel.Title = "IyzA";

Results.Inertia_AS_Frame.AS_I1_Editor_F.IXXLabel.Title = "IoxA";
Results.Inertia_AS_Frame.AS_I1_Editor_F.IYYLabel.Title = "IoyA";
Results.Inertia_AS_Frame.AS_I1_Editor_F.IZZLabel.Title = "IozA";

Results.Inertia_AS_Frame.AS_I2_Editor_F.IXYLabel.Title = "IxyA";
Results.Inertia_AS_Frame.AS_I2_Editor_F.IXZLabel.Title = "IxzA";
Results.Inertia_AS_Frame.AS_I2_Editor_F.IYZLabel.Title = "IyzA";


IAXXLabel = "IoxA";
IAYYLabel = "IoyA";
IAZZLabel = "IozA";
IAXYLabel = "IxyA";
IAXZLabel = "IxzA";
IAYZLabel = "IyzA";


// ----------- Axis System

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.Title = "Axis System";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OX_AS_F.OX_AS_Label.Title = "OxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OY_AS_F.OY_AS_Label.Title = "OyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OZ_AS_F.OZ_AS_Label.Title = "OzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UX_AS_F.UX_AS_Label.Title = "UxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UY_AS_F.UY_AS_Label.Title = "UyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UZ_AS_F.UZ_AS_Label.Title = "UzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VX_AS_F.VX_AS_Label.Title = "VxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VY_AS_F.VY_AS_Label.Title = "VyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VZ_AS_F.VZ_AS_Label.Title = "VzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WX_AS_F.WX_AS_Label.Title = "WxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WY_AS_F.WY_AS_Label.Title = "WyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WZ_AS_F.WZ_AS_Label.Title = "WzA";





Results.Inertia_AS_Frame.AS_O_Editor_F.OX_AS_Label.Title = "OxA";
Results.Inertia_AS_Frame.AS_O_Editor_F.OY_AS_Label.Title = "OyA";
Results.Inertia_AS_Frame.AS_O_Editor_F.OZ_AS_Label.Title = "OzA";

OX_AS_Label = "OxA";
OY_AS_Label = "OyA";
OZ_AS_Label = "OzA";

Results.Inertia_AS_Frame.AS_U_Editor_F.UX_AS_Label.Title = "UxA";
Results.Inertia_AS_Frame.AS_U_Editor_F.UY_AS_Label.Title = "UyA";
Results.Inertia_AS_Frame.AS_U_Editor_F.UZ_AS_Label.Title = "UzA";

UX_AS_Label = "UxA";
UY_AS_Label = "UyA";
UZ_AS_Label = "UzA";

Results.Inertia_AS_Frame.AS_V_Editor_F.VX_AS_Label.Title = "VxA";
Results.Inertia_AS_Frame.AS_V_Editor_F.VY_AS_Label.Title = "VyA";
Results.Inertia_AS_Frame.AS_V_Editor_F.VZ_AS_Label.Title = "VzA";

VX_AS_Label = "VxA";
VY_AS_Label = "VyA";
VZ_AS_Label = "VzA";

Results.Inertia_AS_Frame.AS_W_Editor_F.WX_AS_Label.Title = "WxA";
Results.Inertia_AS_Frame.AS_W_Editor_F.WY_AS_Label.Title = "WyA";
Results.Inertia_AS_Frame.AS_W_Editor_F.WZ_AS_Label.Title = "WzA";

WX_AS_Label = "WxA";
WY_AS_Label = "WyA";
WZ_AS_Label = "WzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.SelectAxisSytemAS.AxisSystem.Title    = "Select Axis System";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.SelectAxisSytemAS.AxisSystem.LongHelp = "Click here then select an axis system in the specification tree.\nSubsequent calculations are done with respect to the same axis system.\nTo change axis system, click the checkbox again and select another axis system";

Results.Inertia_AS_Frame.AS_Button_F.AxisSystem.Title    = "Select Axis System";
Results.Inertia_AS_Frame.AS_Button_F.AxisSystem.LongHelp = "Click here then select an axis system in the specification tree.\nSubsequent calculations are done with respect to the same axis system.\nTo change axis system, click the checkbox again and select another axis system";

AxisSystem            = "Select axis system";
AxisSystemNoSelection = "No selection";
AxisSystemUnknown     = "V4 Axis System";

AxisSystemDeleted     = "Deleted or unset";

AxisSystemNotValidSelection     = "This selection is not valid, because this axis system\nis not in the activated Part";


// ----------- Axis

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OX_A_F.OXAxisLabel.Title = "Ox";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OY_A_F.OYAxisLabel.Title = "Oy";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OZ_A_F.OZAxisLabel.Title = "Oz";

Results.AxisFrame.A_O_Editor_F.OXAxisLabel.Title = "Ox";
Results.AxisFrame.A_O_Editor_F.OYAxisLabel.Title = "Oy";
Results.AxisFrame.A_O_Editor_F.OZAxisLabel.Title = "Oz";

OXAxisLabel = "Ox";
OYAxisLabel = "Oy";
OZAxisLabel = "Oz";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DX_A_F.DXAxisLabel.Title = "Dx";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DY_A_F.DYAxisLabel.Title = "Dy";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DZ_A_F.DZAxisLabel.Title = "Dz";

Results.AxisFrame.A_D_Editor_F.DXAxisLabel.Title = "Dx";
Results.AxisFrame.A_D_Editor_F.DYAxisLabel.Title = "Dy";
Results.AxisFrame.A_D_Editor_F.DZAxisLabel.Title = "Dz";

DXAxisLabel = "Dx";
DYAxisLabel = "Dy";
DZAxisLabel = "Dz";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.SelectAxisA.ButtonAxisA.Title    = "Select Axis";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.SelectAxisA.ButtonAxisA.LongHelp = "Click here then select an axis in the geometry area.\nSubsequent calculations are done with respect to the same axis.\nTo change axis, click the checkbox again and select another axis";


Results.AxisFrame.A_Button_F.Axis.Title    = "Select Axis";
Results.AxisFrame.A_Button_F.Axis.LongHelp = "Click here then select an axis in the geometry area.\nSubsequent calculations are done with respect to the same axis.\nTo change axis, click the checkbox again and select another axis";

Axis = "Select axis";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.Title    = "Inertia / Axis";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.LongHelp = "Gives the equation O and direction vector D of the selected axis as well as the moment of inertia Ma about the axis and the radius of gyration R";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.Title = "Moment / Axis";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.MomentAFrame_L.MomentALabel.Title = "Ma ";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.RadiusMomentAFrame_L.RadiusMomentALabel.Title = "Radius ";



Results.AxisFrame.A_A_Editor_F.AxisMomentLabel.Title = "Ma ";
AxisMomentLabel = "Ma ";

Results.AxisFrame.A_A_Editor_F.RadiusMomentLabel.Title = "Radius ";
RadiusMomentLabel = "Radius ";

// ----------- Bounding Box

BBOXLabel = "BBOx";
BBOYLabel = "BBOy";
BBOZLabel = "BBOz";

BBLXLabel = "BBLx";
BBLYLabel = "BBLy";
BBLZLabel = "BBLz";

// -------------
MainFrame.ButtonFrame.CreateGeometry.Title = "Create geometry";
MainFrame.ButtonFrame.Customise.Title = "Customize...";
MainFrame.ButtonFrame.KeepState.Title = "Keep measure";
MainFrame.ButtonFrame.ExportButton.Title = "Export";
MainFrame.ButtonFrame.OnlyMainBodies.Title = "only main bodies";
MainFrame.MeasShownElemsFrame.MeasShownElemsCheckButton.Title = "Measure only shown elements";

Results.CustomiseFrame.CreateGeometry.Title = "Create geometry";
Results.CustomiseFrame.Customise.Title = "Customize...";
Results.CustomiseFrame.KeepState.Title = "Keep measure";
Results.CustomiseFrame.ExportButton.Title = "Export";
Results.CustomiseFrame.OnlyMainBodies.Title = "only main bodies";
Results.MeasShownElemsFrame.MeasShownElemsCheckButton.Title = "Measure only shown elements";

// ------------- Point

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OXPt_F.OXPtLabel.Title = "Px";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OYPt_F.OYPtLabel.Title = "Py";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OZPt_F.OZPtLabel.Title = "Pz";

Results.Inertia_P_Frame.P_P_Editor_F.OXPtLabel.Title = "Px";
Results.Inertia_P_Frame.P_P_Editor_F.OYPtLabel.Title = "Py";
Results.Inertia_P_Frame.P_P_Editor_F.OZPtLabel.Title = "Pz";

OXPtLabel = "Px";
OYPtLabel = "Py";
OZPtLabel = "Pz";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.SelectPointP.ButtonPointP.Title    = "Select Point";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.SelectPointP.ButtonPointP.LongHelp = "Click here then select a point in the geometry area.\nSubsequent calculations are done with respect to the same point.\nTo change point, click the checkbox again and select another point";


Results.Inertia_P_Frame.P_Button_F.Pt.Title    = "Select Point";
Results.Inertia_P_Frame.P_Button_F.Pt.LongHelp = "Click here then select a point in the geometry area.\nSubsequent calculations are done with respect to the same point.\nTo change point, click the checkbox again and select another point";
Pt = "Select point";

// ---------------- Equivalent

MainFrame.ResultFrame.EquivalentFrame.EquivalentFrame_L.EquivalentLabel.Title = "Equivalent";

Results.EquivalentFrame.DataFrame.EQUIVLabel.Title = "        ";

Equivalent = "Equivalent";


// --------------- Error

PtNotify          = "Error";
AxisNotify        = "Error";
AxisSysNotify     = "Error";

SelectionNotValid = "Selection not valid, please select an other component";


Warning           = "Warning";

NotSelectionAxis        = "This selection is not an axis";
NotSelectionPt          = "This selection is not a point";
NotSelectionAxisSystem  = "This selection is not an axis system";

NotValidSelectionNotify = "Selection not valid";



DensityNotValid   = "Density not valid, please enter another value";
DensityTooSmall   = "Density is too small! It should be greater than ";

Information = "Information";
MeasureInertiaDelete = "Measure Inertia is no longer valid because the measured item does not exist any more.";
MeasureUpdateNotPossible = "Measure update is not possible because the measure is not associative.";



//Factory

InertiaVolume    = "InertiaVolume";
InertiaSurface   = "InertiaSurface";
Inertia2DSurface = "Inertia2DSurface";

//HIX - Restrict Measure When CATAnalysis is opened with Product as Active.
MeasureInertiaInCATAnalysis  =  "For measuring inside the product, please open this product in a new Window.";
