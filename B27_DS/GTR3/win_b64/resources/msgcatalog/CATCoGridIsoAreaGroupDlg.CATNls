Title = "Iso-Thickness Areas";

CreateFromFrame.Title = "Create From ";

CreateFromFrame.SelTypeFrame.StackingRB.Title = "Complete stacking";
CreateFromFrame.SelTypeFrame.StackingRB.LongHelp = "Creates Iso-Thickness Areas from the complete stacking.";
CreateFromFrame.SelTypeFrame.StackingRB.Help = "Creates Iso-Thickness Areas from the complete stacking";
CreateFromFrame.SelTypeFrame.SelectedPGRB.Title = "Selection of groups"; 
CreateFromFrame.SelTypeFrame.SelectedPGRB.LongHelp = "Creates Iso-Thickness Areas for selected plies group."; 
CreateFromFrame.SelTypeFrame.SelectedPGRB.Help = "Creates Iso-Thickness Areas for selected plies group"; 

// Deprecated
AssistPB.Title = "3D Assistant";
CSFrame.PtIndicRB.Title = "Point Indication ";
CSFrame.PtIndicRB.LongHelp = "Selects a virtual point in the authoring window as the core sampling point.";
CSFrame.PtIndicRB.Help = "Selects a virtual point in the authoring window as the core sampling point";

CSFrame.PtSelectRB.Title = "Point Selection "; 
CSFrame.PtSelectRB.LongHelp = "Selects an actual point in the authoring window as the core sampling point.";
CSFrame.PtSelectRB.Help = "Selects an actual point in the authoring window as the core sampling point";

BoundariesCB.Title = "Generate shell boundaries";
BoundariesCB.LongHelp = "Generates automatically shell boundaries for each Iso-Thickness Area shell.
By default, this checkbox is not selected.";
BoundariesCB.Help = "Generates automatically shell boundaries for each Iso-Thickness Area shell";

OffsetSurfCB.Title = "Generate offset surface"; 
OffsetSurfCB.LongHelp = "Generates automatically an offset surface for each Iso-Thickness Area shell. 
By default, this checkbox is not selected.";
OffsetSurfCB.Help = "Generates automatically an offset surface for each Iso-Thickness Area shell";

SavePointsCB.Title = "Save indication points in work object";
SavePointsCB.LongHelp = "Defines how indication points are saved:
When this checkbox is selected, all the picked indication points are saved in the current openbody. 
When this checkbox is cleared, each indication point is aggregated under its iso-thickness area.
By default, this checkbox is not selected.";
SavePointsCB.Help = "Defines how indication points are saved: in the current openbody (checkbox selected) or aggregated under its ITA (checkbox cleared)";   

AddThicknessParamFrame.EnglobingFrame.IntermediateFrame.Label.Title = "Additional thickness ratio ";
AddThicknessParamFrame.LongHelp = "Defines an  additional thickness as a ratio of the current thickness (e.g. 0.1 means +10%, default is 0).
At creation, this ratio is applied to all the Iso-Thickness Areas of  the new group.
At edition, the modification applies to all the Iso-Thickness Areas, provided the group previous additional thickness ratio was applied to them all. Otherwise, a message lets you decide how the modification is applied.";
AddThicknessParamFrame.Help = "Defines an  additional thickness as a ratio of the current thickness (e.g. 0.1 means +10%, default is 0)";

UpgradeFrame.UpgradeLabel.Title = "Upgrade ITA group to enable smart computation of bottom of slopes. ";
UpgradeFrame.UpgradeLabel.ShortHelp = "You may cancel current command and launch upgrade command on Iso-Thickness Area (ITA) group object.";
UpgradeFrame.UpgradeLabel.LongHelp = "You may cancel current command and launch upgrade command on Iso-Thickness Area (ITA) group object.";
UpgradeFrame.UpgradeLabel.Help = "You may cancel current command and launch upgrade command on Iso-Thickness Area (ITA) group object";

// new messages
SlopeFrame.Title = "Drop-Off Values ";
SlopeFrame.LongHelp = "Defines the Drop-off values to compute the bottom of the slope.
Those parameters apply to non-grid cases only and are ignored for plies created from a Virtual Stacking.";
SlopeFrame.Help = "Defines the Drop-off values to compute the bottom of the slope";
SlopeFrame.MaxDropOffLabel.Title =  " Maximum: ";
SlopeFrame.MaxDropOffLabel.LongHelp = "Limits the search of the drop-off value (for Iso-Thickness Area edges \n    that are not based on a grid ramp support curve).";
SlopeFrame.MaxDropOffLabel.Help = "Limits the search of the drop-off value (for Iso-Thickness Area edges that are not based on a grid ramp support curve)";

SlopeFrame.DefaultDropOffLabel.Title = " Default: ";
SlopeFrame.DefaultDropOffLabel.LongHelp = "Defines the value used when the drop-off value cannot be retrieved nor estimated (single ply or drop-off greater than maximum).";
SlopeFrame.DefaultDropOffLabel.Help = "Defines the value used when the drop-off value cannot be retrieved nor estimated (single ply or drop-off greater than maximum)";

SlopeFrame.IgnoreRampSupportsCB.Title =  "Ignore ramp supports";
SlopeFrame.IgnoreRampSupportsCB.LongHelp = "Ignores the ramp supports when computing Iso-Thickness Area shells:\nThe shells are relimited by the bottom slope, computed with the distance to neighboring ply contours, with a geometrical approach.";
SlopeFrame.IgnoreRampSupportsCB.Help = "Ignore ramp supports during Iso-thickness area shell computation";

CompGeomFrame.ITABoundary.Title = "Limit Boundary";
CSFrame.MinimumWidthITAForAutoSelectParmFrame.EnglobingFrame.IntermediateFrame.Spinner.LongHelp = "Defines the value used for the automatic selection of points: only wider areas will be selected.";
CSFrame.MinimumWidthITAForAutoSelectParmFrame.EnglobingFrame.IntermediateFrame.Spinner.Help = "Defines the value used for the automatic selection of points: only wider areas will be selected";

CSFrame.SavePointsCB.Title = "Save indication points in work object";
CSFrame.SavePointsCB.LongHelp = "Defines how indication points are saved:
When this checkbox is selected, all the picked indication points are saved in the current openbody. 
When this checkbox is cleared, each indication point is aggregated under its iso-thickness area.
By default, this checkbox is not selected.";
CSFrame.SavePointsCB.Help = "Defines how indication points are saved: in the current openbody (checkbox selected) or aggregated under its ITA (checkbox cleared)";  

CSFrame.Title = "Points Defining Areas";
OtherOutputFrame.Title = "Other Outputs";
OtherOutputFrame.LongHelp = "Lets you select additional outputs.";
OtherOutputFrame.Help = "Lets you select additional outputs";

OtherOutputFrame.BoundariesCB.Title = "Generate shell boundaries";
OtherOutputFrame.BoundariesCB.LongHelp = "Generates automatically shell boundaries for each Iso-Thickness Area shell.
By default, this checkbox is not selected.";
OtherOutputFrame.BoundariesCB.Help = "Generates automatically shell boundaries for each Iso-Thickness Area shell";

OtherOutputFrame.OffsetSurfCB.Title = "Generate offset surface";
OtherOutputFrame.OffsetSurfCB.LongHelp = "Generates automatically an offset surface for each Iso-Thickness Area shell. 
By default, this checkbox is not selected.";
OtherOutputFrame.OffsetSurfCB.Help = "Generates automatically an offset surface for each Iso-Thickness Area shell";

OtherOutputFrame.SavePointsCB.Title = "Save indication points in work object";
OtherOutputFrame.SavePointsCB.LongHelp = "Defines how indication points are saved:
When this checkbox is selected, all the picked indication points are saved in the current openbody. 
When this checkbox is cleared, each indication point is aggregated under its iso-thickness area.
By default, this checkbox is not selected.";
OtherOutputFrame.SavePointsCB.Help = "Defines how indication points are saved: in the current openbody (checkbox selected) or aggregated under its ITA (checkbox cleared)"; 

// CSFrame.AutoSelectPB.Title = " Initialize ";
CSFrame.AutoSelectPB.LongHelp = "Automatically computes Iso-Thickness Areas wider than the requested minimum value\nand creates a central point as if it was indicated by designer.";
CSFrame.AutoSelectPB.Help = "Automatically computes Iso-Thickness Areas wider than the requested minimum value and creates a central point as if it was indicated by designer";
AutoSelectLabelMsg = " Compute and select areas wider than /p1 ";
ModifiersFrame.Title = "Modifiers";
ModifiersFrame.LongHelp = "Defines an additional thickness ratio, specific to the current Iso-Thickness Area.";
ModifiersFrame.Help = "Defines an additional thickness ratio, specific to the current Iso-Thickness Area";

ModifiersFrame.AddThicknessParamFrame.EnglobingFrame.IntermediateFrame.Label.Title = "Additional thickness ratio ";
ModifiersFrame.AddThicknessParamFrame.EnglobingFrame.LongHelp = "Defines an additional thickness ratio, specific to the current Iso-Thickness Area.";
ModifiersFrame.AddThicknessParamFrame.EnglobingFrame.Help = "Defines an additional thickness ratio, specific to the current Iso-Thickness Area";

More = "More...";
Less = "Less...";
