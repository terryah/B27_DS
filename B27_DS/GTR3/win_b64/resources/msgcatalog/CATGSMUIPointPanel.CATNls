//---------------------------------------
// Resource file for CATGSMUIPoint class
// En_US
//---------------------------------------

Title="Point Definition";

PointTypeCoord="Coordinates";
PointTypeOnCrv="On curve";
PointTypeOnPln="On plane";
PointTypeOnSur="On surface";
PointTypeCtr="Circle / Sphere / Ellipse center";
PointTypeTgt="Tangent on curve";
PointTypeCKE="Formula";
PointTypeExpl="Explicit";
PointTypeBetween="Between";
stOnCurveLength="Length";
stOnCurveDisDir="Offset";

frame1.LabList.Title="Point type: ";
frame1.CmbList.LongHelp="Lists the available methods for creating points.";

//coord sans axe local
OnCoord.LabelCoordX.Title="X = ";
OnCoord.LabelCoordY.Title="Y = ";
OnCoord.LabelCoordZ.Title="Z = ";
OnCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="X";
OnCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Y";
OnCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.Title="Z";
OnCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnCoord.FraSepar.LabelSepar.Title="Reference ";
OnCoord.LabelOnCoordPoint.Title="Point: ";
OnCoord.LabRef.Title="   ";
OnCoord.FrameErase.FraElem2.FraEdt.Edt.LongHelp="Specifies the reference point.\nIf no point is selected, the origin point is used as reference.";
OnCoord.FrameErase.PushIcon.Title="Remove";
OnCoord.FrameErase.PushIcon.ShortHelp="Remove reference point.";
OnCoord.FrameErase.PushIcon.LongHelp="Allows to remove the reference point,\nand to use the default one.";
OnCoord.CompassLocationV5.Title="Compass Location";
OnCoord.CompassLocationV5.LongHelp="Allows to create the point at compass location.";
OnCoord.CompassLocation.Title="Robot Location";
OnCoord.CompassLocation.LongHelp="Allows to create the point at Robot location.";

//coord avec axe local
OnCoord.FraCoord.LabelCoordX.Title="X = ";
OnCoord.FraCoord.LabelCoordY.Title="Y = ";
OnCoord.FraCoord.LabelCoordZ.Title="Z = ";
OnCoord.FraCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="X";
OnCoord.FraCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnCoord.FraCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Y";
OnCoord.FraCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnCoord.FraCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.Title="Z";
OnCoord.FraCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
//OnCoord.ChkAxis.Title="Coordinates in absolute axis system";
//OnCoord.ChkAxis.LongHelp="Allows to view and modify coordinates\nin the absolute axis system.";
OnCoord.FraRef.FrameErase.LabelOnCoordPoint.Title="Point: ";
OnCoord.FraRef.LabRef.Title="   ";
OnCoord.FraRef.FrameErase.FraElem2.FraEdt.Edt.LongHelp="Specifies the reference point.\nIf no point is selected, the origin point is used as reference.";
OnCoord.FraRef.FrameErase.PushIcon.Title="Remove";
OnCoord.FraRef.FrameErase.PushIcon.ShortHelp="Remove reference point.";
OnCoord.FraRef.FrameErase.PushIcon.LongHelp="Allows to remove the reference point,\nand to use the default one.";
// added code 3dplm for Axis System
OnCoord.LabelAxis.Title="Axis System: ";
OnCoord.FraAxis.LongHelp="Specifies the reference Local Axis System\nin which the coordinates are converted. \nIf not specified then absolute axis system \nis used as default axis system.";

OnCrv.LabelOnCrvCurve.Title="Curve: ";
OnCrv.FraDistRef1.LabelOnCrvPoint.Title="Point: ";
OnCrv.FraSepar0.LabelSepar0.Title="Distance to reference ";
OnCrv.FraDistRef.FraRadio.Radio1.Title="Distance on curve";
OnCrv.FraDistRef.FraRadio.Radio2.Title="Ratio of curve length";
OnCrv.FraDistRef.FraRadio.Radio3.Title="Distance along direction";
OnCrv.FraDistRef.FraRadio.LongHelp="Determines whether the new point \nis to be created with: \n  - a given distance along the curve \n    from the reference point \n-  a given curve length ratio\n    from the reference point\n  or along the direction specified.";
OnCrv.FraDynamic1.FraPush.PushNear.Title="Nearest extremity";
OnCrv.FraDynamic1.FraPush.PushNear.LongHelp="Allows you to create the curve extremity\nthat is the nearest to the current defined point";
OnCrv.FraDynamic1.FraPush.PushMid.Title="Middle point";
OnCrv.FraDynamic1.FraPush.PushMid.LongHelp="Allows you to create the curve middle point.\nWhen a reference point is set,\nit creates the middle point between\nthe reference point and the extremity.";
OnCrv.FraDynamic1.FraRadioEuc.RadioEuc1.Title="Geodesic";
OnCrv.FraDynamic1.FraRadioEuc.RadioEuc2.Title="Euclidean";
OnCrv.FraDynamic1.FraRadioEuc.LongHelp="Determines whether the distance is measured : \n    - along the curve (geodesic)\n    - in absolute from the reference point (euclidean)";
OnCrv.FraDistRef1.PushB.Title="Reverse Direction";
OnCrv.FraDynamic1.LabDirection.Title="Direction: ";
OnCrv.FraDynamic1.FraDirection.LongHelp="Defines the direction from the reference point,\n in which the point will be created.";
OnCrv.FraDynamic1.LabDyn1.Title  ="    ";
OnCrv.FraDynamic1.LabDyn2.Title  ="   ";
OnCrv.FraSepar.LabelSepar.Title="Reference ";
OnCrv.FraDynamic1.LengthCont.FraCke.LabelOnCrvLength.Title="Length: ";
OnCrv.FraDynamic1.LengthCont.FraCke.LabelOnCrvRatio.Title="Ratio: ";
OnCrv.FraDynamic1.LengthCont.FraCke.LabelOnCrvDistAlongDir.Title="Offset:     ";
OnCrv.FraDynamic1.LengthCont.FraCke.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Value";
OnCrv.FraDynamic1.LengthCont.FraCke.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnCrv.FraElem1.LongHelp="Specifies the curve on which\nthe point is to be created.";
OnCrv.FraDistRef1.FraElem2.LongHelp="Specifies the point that is to be the reference\nfor the distance on the curve. If no point is selected,\nthe curve's extremity is used.";
OnCrv.FraDistRef1.PushB.LongHelp="Displays the point:\n- on the other side of the reference point \n(if a point was selected initially)\n- computed from the other extremity of the curve\n(if no point was selected initially).";
OnCrv.FraDistRef.LabDistRef.Title="   ";
OnCrv.FraDistRef1.LabDistRef1.Title="   ";
OnCrv.FraDistRef1.PushIcon.Title="Remove";
OnCrv.FraDistRef1.PushIcon.ShortHelp="Remove reference point.";
OnCrv.FraDistRef1.PushIcon.LongHelp="Allows to remove the reference point,\nand to use the default one.";


OnPln.LabelOnPlnPlane.Title="Plane: ";
OnPln.LabelOnPlnPoint.Title="Point: ";
OnPln.LabelOnPlnX.Title="H: ";
OnPln.LabelOnPlnY.Title="V: ";
OnPln.FraSepar.LabelSepar.Title="Reference ";
OnPln.FraElem1.LongHelp="Specifies the plane on which the\npoint is to be created.";
OnPln.FrameErase.FraElem2.LongHelp="Specifies the reference point in the plane\nfor computing coordinates. If no point is selected,\nthe projection of origin point in the plane is used as reference.";
OnPln.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="H";
OnPln.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnPln.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="V";
OnPln.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OnPln.LabRef.Title="   ";
OnPln.FrameErase.PushIcon.Title="Remove";
OnPln.FrameErase.PushIcon.ShortHelp="Remove reference point.";
OnPln.FrameErase.PushIcon.LongHelp="Allows to remove the reference point,\nand to use the default one.";
// start code 3dplm
OnPln.FraSeparSur.LabelSeparSur.Title="Projection ";
OnPln.LabelSur.Title="Surface: ";
OnPln.FrameEditSur.FraElem2.LongHelp="Specifies the projection support \ncomputing point on plane. Default no support is selected.";
// end code 3dplm

OnSurf.LabelOnSurfSur.Title="Surface: ";
OnSurf.LabelOnSurfPoint.Title="Point: ";
OnSurf.LabelOnSurfDir.Title="Direction: ";
OnSurf.LabelOnSurfDist.Title="Distance: ";
OnSurf.FraDirection.LongHelp="Defines the direction on the surface from the reference point,\n in which the point will be created.";
OnSurf.FraSepar.LabelSepar.Title="Reference ";
OnSurf.FraElem1.LongHelp="Specifies the surface on which the point\nis to be created.";
OnSurf.FrameErase.FraElem2.LongHelp="Specifies the reference point.If no point is selected,\nthe surface middle point is used as reference.";
OnSurf.FraDirection.Editor.LongHelp="Specifies the line whose orientation determines the\ndirection or the plane whose normal determines the\ndirection.\nYou can also use contextual menu\nto enter the X, Y, Z components of the\ndirection.";
OnSurf.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Distance";
OnSurf.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Defines the distance on the surface from the reference point.\nYou can also use the contextual menu to specify formula";
OnSurf.LabRef.Title="   ";
OnSurf.FrameErase.PushIcon.Title="Remove";
OnSurf.FrameErase.PushIcon.ShortHelp="Remove reference point.";
OnSurf.FrameErase.PushIcon.LongHelp="Allows to remove the reference point,\nand to use the default one.";
//start 3dplm code 27th august'04
OnSurf.FraSeparDynPos.LabSeparDynPos.Title="Dynamic positioning ";
OnSurf.FraSeparDynPos.Coarse.Title="Coarse";
OnSurf.FraSeparDynPos.Fine.Title="Fine";
OnSurf.FraSeparDynPos.Coarse.ShortHelp="Fast, but approximate algorithm.";
OnSurf.FraSeparDynPos.Coarse.LongHelp="Uses euclidean algorithm to compute the distance between reference point and mouse click.";
OnSurf.FraSeparDynPos.Fine.ShortHelp="Accurate algorithm.";
OnSurf.FraSeparDynPos.Fine.LongHelp="Uses geodesic algorithm to compute the distance between reference point and mouse click, if possible.";
//end 3dplm code


OnCtr.LabelCtrCurve.Title="Circle / Sphere / Ellipse: ";
OnCtr.FraElem1.LongHelp="Specifies the circle or circular arc \nwhose center is to be used for creating a point.";

OnTgt.LabelTgtCurve.Title="Curve: ";
OnTgt.LabelTgtDir.Title="Direction: ";
OnTgt.FraElem1.LongHelp="Specifies the curve on which tangent point\nis to be created.";
OnTgt.FraDirection.LongHelp="Specifies the line whose orientation determines the\ndirection or the plane whose normal determines the\ndirection.\nYou can also use contextual menu\nto enter the X, Y, Z components of the\ndirection.";

PointCKE.FraPointCKE.LabelPointCKE.Title="Point : ";

Between.FraPrincipal.LabelBetweenFirst.Title="Point 1: ";
Between.FraPrincipal.FraElem1.LongHelp="First Point";
Between.FraPrincipal.LabelBetweenSecond.Title="Point 2: ";
Between.FraPrincipal.FraElem2.LongHelp="Second Point";
Between.FraPrincipal.LabelBetweenRatio.Title="Ratio : ";
Between.FraPrincipal.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="if d1 is the distance between the first point and the created point,\nand d2 is the distance between the first point and the second point,\nthen Ratio = d1/d2";
Between.FraPrincipal.Support.Title="Support:";
Between.FraPrincipal.Support.LongHelp="Select the support";
Between.FraPrincipal.FraSupport.LongHelp="Specifies the support surface onto which\nthe geodesic point is to be created.\nThis data is optional.";
Between.FraInverse.PushB.Title="Reverse Direction";
Between.FraInverse.PushB.LongHelp="Reverse the direction to go in the opposite direction";
Between.FraInverse.PushMid.Title="Middle Point";
Between.FraInverse.PushMid.LongHelp="Allows to build the middle point between\n the First point and the Second point";
// start code 3dplm
//Combo lock help
PointLockShort="Click to enable automatic type change.";
PointUnlockShort="Click to disable automatic type change.";
PointLockLong="On clicking this button automatic type change will be enabled.";
PointUnlockLong="On clicking this button automatic type change will be disabled.";
// end code 3dplm

