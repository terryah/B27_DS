// AUTHOR      :    gha
// DATE        :    		Mar. 20 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to LineUp  Window
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01           fgx   24.06.99  Nouvelle version de la commande
//------------------------------------------------------------------------------

Title="Line Up";
Help="Define parameters for lining up dimensions";

_frameOffset._labelLength.Title="Linear offset: ";
_frameOffset._editorLength.Help="Enter the linear offset value";
_frameOffset._editorLength.LongHelp="Defines the linear offset between
the reference and the dimension to be aligned.";

_frameOffset._labelAngle.Title="Angular offset: ";
_frameOffset._editorAngle.Help="Enter the angular offset value";
_frameOffset._editorAngle.LongHelp="Defines the angular offset between
the reference and the dimension to be aligned.";

_frameOptions._systemsOnly.Title="Only organize into systems";
_frameOptions._systemsOnly.LongHelp="Only organize into systems
Only organize dimensions into systems.
Neither the smallest dimension of each system,
nor dimensions that cannot be organized into system
are moved";

_frameOffset._labelBaseOffset.Title="Offset to reference: ";
_frameOffset._labelBaseOffset.LongHelp="Value of the offset to the reference,
for length/distance/angle dimensions
and for radius/diameter dimensions.";
_frameOffset._spinnerBaseLength.LongHelp="Value of the offset to the reference,
for length/distance/angle dimensions.";
_frameOffset._spinnerBaseAngle.LongHelp="Value of the offset to the reference,
for radius/diameter dimensions.";

_frameOffset._labelOffset.Title="Offset between dimensions: ";
_frameOffset._labelOffset.LongHelp="Value of the offset between dimensions,
for length/distance/angle dimensions
and for radius/diameter dimensions.";
_frameOffset._spinnerLength.LongHelp="Value of the offset between dimensions,
for length/distance/angle dimensions.";
_frameOffset._spinnerAngle.LongHelp="Value of the offset between dimensions,
for radius/diameter dimensions.";

_frameOptions._checkValStack.Title="Align stacked dimension values";
_frameOptions._checkValStack.LongHelp="Align all the values of a group of stacked dimensions
on the value of the smallest dimension of the group.";
_frameOptions._checkValCumul.Title="Align cumulated dimension values";
_frameOptions._checkValCumul.LongHelp="Align all the values of a group of cumulated dimensions
on the value of the smallest dimension of the group.";

_systemsOnly.Title="Only organize dimensions into systems";
_systemsOnly.LongHelp="Only organizes dimensions into systems.
Neither the smallest dimension of each system,
nor dimensions that cannot be organized into system
are moved.";

_frameOptions._frameFunnel._checkAutoFunnel.Title="Automatically add a funnel";
_frameOptions._frameFunnel._checkAutoFunnel.LongHelp="Automatically adds a funnel
whenever the value of the dimension 
cannot be displayed correctly without it.";

