StandardComment="This design table was created by /P1 on ";
EmptyNameMessage="You can't create a design table with an empty name";
EmptyNameHeader="Empty name forbidden";
FileTitle1="Select the Design Table File";
FileTitle2="Select the Pathname of the File to be created";
CreationError="The /P1 file couldn't be created. Following causes are possible :\n- An exception has occured\n- The directory or disk is full\n- The file creation process was so slow that it was impossible to assert that the file has been well created";
CreationErrorHeader="Design Table sheet creation error";
LinkError="The system couldn't create a link with the Excel or Lotus 1-2-3 sheet";
LinkErrorHeader="Excel or Lotus 1-2-3 sheet linking: error";
ParsingError="Error while parsing the design table file.\nThe design table cannot be activated";
ParsingErrorHeader="File parsing error";
ChooseParmDlgInitialState="Select the parameters you want to insert in the new design table";
ExcelFile="Microsoft Excel worksheets (*.xlsx;*.xls;*.xlsm)";
ExcelFile2003="Microsoft Excel worksheets (*.xls)";
LotusFile="Lotus 1-2-3 worksheets (*.123)";
TextFile="Text files (*.txt)";
AllFile="All files (*.*)";
Title="Creation of a Design Table";

NameLabel.Title="Name: ";
NameEditor.Help="Enter the design table name";
NameEditor.ShortHelp="Enter the design table name";
NameEditor.LongHelp="Enter the design table name";
CommentLabel.Title="Comment: ";
CommentEditor.Help="Enter a comment to describe this design table";
CommentEditor.ShortHelp="Enter a comment to describe this design table";
CommentEditor.LongHelp="Enter a comment to describe this design table";
FromFileRButton.Title="Create a design table from a pre-existing file";
FromFileRButton.Help="Creates a design table from a file. The file should be either a .txt file, a .123 file or a .xls file (on Windows NT)";
FromFileRButton.ShortHelp="Creates a design table from a file.\nThe file should be either a .txt file, a .123 file or a .xls file (on Windows NT)";
FromFileRButton.LongHelp="Creates a design table from a file.\nThe file should be either a .txt file, a .123 file or a .xls file (on Windows NT)";
FromCurrentRButton.Title="Create a design table with current parameter values";
FromCurrentRButton.Help="Creates a table from a set of parameters (names and values included in the new file)";
FromCurrentRButton.ShortHelp="Creates a table from a set of parameters.";
FromCurrentRButton.LongHelp="Creates a table from a set of parameters.\nThe names and values of these parameters will be included in the new file";
EmptyRButton.Title="Create an empty design table";
EmptyRButton.Help="Create an empty table. You will have to fill it";
EmptyRButton.ShortHelp="Create an empty table. You will have to fill it";
EmptyRButton.LongHelp="Create an empty table. You will have to fill it";
HelpEditor.Title="You should create a design table:
either from a text file, an Excel sheet or a Lotus 1-2-3 sheet (on NT).
Here is an example of a design table:

PadHeight (mm)	PadWidth (mm)	Material
15		12		Steel
17		1.3 cm		Aluminium

In a text file, columns should be separated by tabulations.";
WrongName="This name isn't allowed";
WrongNameHd="Wrong Name";
OverwriteQuestion="Warning! You are going to overwrite the file /P1. Do you want to continue?";
OverwriteQuestionHd="This file already exists";
NameEmpty="You can't use this file name";
NameEmptyHd="Bad file name";
InvalidFileName = "The selected file name is invalid";

SaveFile="Save";
SheetIndexFrame.SheetIndexLabel.Title="For Excel or Lotus 1-2-3 sheets, sheet index : ";
SheetIndexFrame.SheetIndexEditor.ShortHelp="Useful if you want to get data in another sheet than the first one in the workbook";
OrientationFrame.OrientationLabel.Title="Orientation :         ";
OrientationFrame.VerticalButton.Title="Vertical         ";
OrientationFrame.VerticalButton.ShortHelp="Default : columns are vertical in the file";
OrientationFrame.HorizontalButton.Title="Horizontal";
OrientationFrame.HorizontalButton.ShortHelp="Choose this option if data in your file is organized horizontaly";

FileCreationFailed="An error occured during this operation. Check that you can write in this folder and that file name is correct";
FileReplacement="Do you want this file to become the new source file of the design table?";

CanNotOverwriteFile = "The selected file can not be overwriten because it is pointed by an other Design Table in the current session.\nBypass : choose the option 'Create Design Table from pre-existing file'.";
CanNotOverwriteFileHd = "Can not overwrite file";
