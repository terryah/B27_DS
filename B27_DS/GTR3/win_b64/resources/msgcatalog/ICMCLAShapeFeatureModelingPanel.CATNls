DialogBoxTitle                                   = "Feature Modeling";

//Selection options ------------------------------------------------------------------------------------

LabelFeature.Title                               = "Feature: ";
LabelFeature.LongHelp                            = "Selects the feature to be modified.
Only one feature element can be selected.
Only one single-cell feature element can be selected.";
LabelFeature.ShortHelp                           = "Feature element";


//Options tab ------------------------------------------------------------------------------------------

TabOptions.Title                                 = "Options";

PushButtonReset.Title                            = "Reset";
PushButtonReset.LongHelp                         = "Restores the original shape of the modified feature element.";
PushButtonReset.ShortHelp                        = "Restore the original shape";

CheckButtonDisplay.Title                         = "Show input feature";
CheckButtonDisplay.ShortHelp                     = "Show/hide input feature";

CheckButtonUseSpecifiedOrder.Title               = "Use specified order";
CheckButtonUseSpecifiedOrder.LongHelp            = "Defines the order
- CLEARED -
  The order of the input feature is always adopted.
- SELECTED -
  The order of the input feature can be changed 
  via the order manipulator.";
CheckButtonUseSpecifiedOrder.ShortHelp           = "Define order";

//Constraints frame ------------------------------------------------------------------------------------

FrameConstraints.Title                           = "Constraints";
FrameConstraints.LongHelp                        = "Defines constraints for the modification 
of the input geometry.";

//ComboBox options!
ComboConstraints.Title                           = "No Constraints";
ComboConstraints.LongHelp                        = "The input geometry can be modified by defining constraints:
- No Constraints -
  Modifies all control points except those with G0 continuity.
  The constraints defined for the original feature are overwritten.
- Intrinsic Constraints -
  Restricts control point manipulation to respect 
  the constraints of the input feature.  
  The intrinsic constraints are not only applied during the
  modification of the feature, but also during a feature update.
- Specified Constraints -
  The constraints can be set manually using
  U/V Min and U/V Max options.";
ComboConstraints.ShortHelp                       = "Define constraints";

//ComboBox options!
ComboConstraints.NoConstraints                   = "No Constraints";
ComboConstraints.IntrinsicConstraints            = "Intrinsic Constraints";
ComboConstraints.SpecifiedConstraints            = "Specified Constraints";

//Continuity frame ------------------------------------------------------------------------------------

FrameContinuity.Title                            = "Continuity";
FrameContinuity.LongHelp                         = "Sets the continuity condition of the result manually.
In case of curves being selected, only U Min. and U Max. are valid options. 
In case of surfaces being selected, all conditions are valid.
Options are only available if Specified Constraints is selected.";

LabelUMin.Title                                  = "U Min: ";
LabelUMin.LongHelp                               = "Keeps G0/G1/G2/G3 continuity at the surface boundary with U=0.";
LabelUMin.ShortHelp                              = "U=0";

LabelUMax.Title                                  = "U Max: ";
LabelUMax.LongHelp                               = "Keeps G0/G1/G2/G3 continuity at the surface boundary with U=1";
LabelUMax.ShortHelp                              = "U=1";

LabelVMin.Title                                  = "V Min: ";
LabelVMin.LongHelp                               = "Keeps G0/G1/G2/G3 continuity at the surface boundary with V=0";
LabelVMin.ShortHelp                              = "V=0";

LabelVMax.Title                                  = "V Max: ";
LabelVMax.LongHelp                               = "Keeps G0/G1/G2/G3 continuity at the surface boundary with V=1";
LabelVMax.ShortHelp                              = "V=1";


//Modification tab -------------------------------------------------------------------------------------

TabModification.Title                            = "Modification";
