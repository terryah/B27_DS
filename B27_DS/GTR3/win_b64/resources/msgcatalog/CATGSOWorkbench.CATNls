// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATGSOWorkbench 
//
//===========================================================================

CATGSOWorkbench.Title       = "Generative Shape Morphing";
CATGSOWorkbench.Help        = "Generative Shape Morphing App";
CATGSOWorkbench.LongHelp    = "This app allows you to deform shapes from wireframe and surface geometry.";
CATGSOWorkbench.ShortHelp   = "Generative Shape Morphing";

ShapeDesignToolBarSelect.Title      = "Select";
ShapeDesignToolBarWireFrame.Title   = "Wireframe";
ShapeDesignToolBarLaw.Title         = "Law";
ShapeDesignToolBarSurf.Title        = "Surfaces";
ShapeDesignToolBarModif.Title       = "Operations";
ShapeDesignToolBarReplicate.Title   = "Replication";
ShapeDesignToolBarTools.Title       = "Tools";
ShapeDesignToolBarConstraintAnalyse.Title  = "Constraints and Analysis";
ShapeDesignToolBarConstraint.Title  = "Constraints";
ShapeDesignToolBarAnalyse.Title     = "Analysis";
CATSdwToolBarGSOTlb.Title           = "Advanced Surfaces";
CATSdwToolBarDL1Tlb.Title           = "Developed Shapes";
CATSdwSurfacesTlb.Title             = "Surfaces";
CATSdwToolBarAnnotationTlb.Title    = "Annotations";
CATSdwToolBarViewsTlb.Title         = "Views/Annotation Planes";
CATSdwSketchTlb.Title               = "Sketcher";
CATSdwToolBar2DCW1Tlb.Title         = "Sections Management";

ShapeDesignAxisSystems.Title        = "Axis Systems";
ShapeDesignWireFrame.Title          = "Wireframe";
ShapeDesignLaw.Title                = "Law";
ShapeDesignSurfac.Title             = "Surfaces";
ShapeDesignModif.Title              = "Operations";
ShapeDesignConstraintAnalyse.Title  = "Constraints and Analysis";
ShapeDesignConstraint.Title         = "Constraints";
ShapeDesignAnalyse.Title            = "Analysis";
ShapeDesignAdvance.Title            = "Advanced Replication Tools";
ShapeDesignTemplate.Title           = "Power Copy";
ShapeDesignUdf.Title                = "Userfeatures";
CATSdwGSOSnu.Title                  = "Advanced Surfaces";
CATSdwDL1Snu.Title                  = "Developed Shapes";
CATSdwSurfacesSnu.Title             = "Surfaces";
CATSdwUdfSnu.Title                  = "UserFeature";
CATSdwAnnotationSnu.Title           = "Annotations";
CATSdwViewsSnu.Title                = "Views/Annotation Planes";
CATSdwSketcherSnu.Title             = "Sketcher";

ContainerPoint.Title                = "Points";
LineAxisPolyLine.Title		        = "Line-Axis";
ProjectCombineReflectLine.Title     = "Project-Combine";
CircleConic.Title                   = "Circle-Conic";
Curve.Title                         = "Curves";
Connection.Title                    = "Corner-Connect";
ExtrudeRevol.Title                  = "Extrude-Revolution";
SweepS.Title                        = "Sweeps";
SectionBckGroundMode.Title					= "Section Background Visualization";
Extracts.Title                      = "Extracts";
AssembleHealing.Title               = "Join-Healing";
TrimSplit.Title                     = "Trim-Split";
FilletAll.Title                     = "Fillets";
Transfor.Title                      = "Transformations";
Multiple.Title                      = "Repetitions";
Pattern.Title                       = "Patterns";
PowerCopy.Title                     = "Power Copy";
Analyse.Title                       = "Analysis";
Grid.Title                          = "Grid";
CATSdwUserFeatureIcb.Title          = "UserFeature";
CATSdwProductKnowledgeIcb.Title     = "Instantiation";
CATSdwAnnotationIcb.Title           = "Annotations";
CATSdwViewsIcb.Title                = "Views/Annotation Planes";
CATSdwSketcherIcb.Title             = "Sketcher";
CATSdwSketchIcb.Title               = "Sketcher";
CATSdwScanIcb.Title                 = "Scan";
VolumeFeature.Title                 = "Volumes";
Extrapola.Title 		    = "Extrapolate-Invert-Near";
ExtrapolaX.Title		    = "Extrapolate-Invert";
OffsetVar.Title                     = "Offsets";
KeepNoKeep.Title                    = "Keep-No Keep";
Offset2D3D.Title                    = "Curve Offsets";

//Action bar
AddOnSection.Title="Morphing";
WireframeSection.Title="Wireframe";
SurfaceSection.Title="Surface";
VolumeSection.Title="Volume";
TransformSection.Title="Transform";
RefineSection.Title="Refine";
ReviewSection.Title="Review";
ShapeDesignModes.Title="Modes";
ShapeDesignObjectProperties.Title="General Properties";
