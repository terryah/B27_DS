// COPYRIGHT ImpactXoft 2003
//===================================================================
//Title = "Shell Properties";

DefaultTitleId = "Shell Properties";

MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.Title = "Shell";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.Title = "Advanced";


MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.BlankLineLabel001Id.Title = "   ";
MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.ShellThicknessFrameId.ShellThicknessLabelId.Title = "Thickness:            ";
MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.ShellResetIgnoredFacesId.Title = "Reset ignored faces";
MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.WallDirLabelId.Title = "Wall direction:       ";
MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.BlankLineLabel003Id.Title = "   ";

MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.Title = "Other Thickness Faces";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.BlankLineLabel007Id.Title = "  ";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedFaceThicknessLabelId.Title = "Thickness: ";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.BlankLineLabel005Id.Title = "  ";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedThicknessFacesLabelId.Title = "Faces:       ";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedThicknessFacesComboId.AdvancedThicknessFacesCtxMenuId.AdvancedThicknessFacesCtxMenuClearAllPushItemId.Title = "Clear Selections";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.BlankLineLabel006Id.Title = "  ";
MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedApplyThicknessCheckButtonId.Title = "Apply thickness to all selected faces";


MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.ShellListFrameId.Title = " ";
MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.CoreMainFrameId.Title = " ";	
MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.ShellListFrameId.CoreBlankLabel002Id.Title = "   ";
MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.ShellListFrameId.ShellableListId.Tittle = " ";
MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.ShellListFrameId.ShellableListId.CoreCtxMenuId.Title = "";
MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.ShellListFrameId.ShellableListId.CoreCtxMenuId.CoreCtxMenuActivateId.Title = "";
MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.ShellListFrameId.ShellableListId.CoreCtxMenuId.CoreCtxMenuExtractId.Title = "Extract Surfaces";
MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.CoreMainFrameId.CoreCompFrameId.Title = " ";


NoSelectionId = "No selection";

WallDirInsideId = "Inside";
WallDirOutsideId = "Outside";

ShellLst0	= "  ";
ShellLst1   = "  ";
ShellLst2	= "Feature";
ShellLst3	= "Type";

CoreTypeInterconnectedId = "Interconnected core";
CoreTypeIsolatedId       = "Isolated core";
CoreTypeSelectId         = "Select core";

CavityTypeInterconnectedId = "Interconnected cavity";
CavityTypeIsolatedId       = "Isolated cavity";
CavityTypeSelectId         = "Select cavity";

ExtractGeometryId	= "Extracted Geometry";
ActivateId	= "Activate";
DeActivateId = "Deactivate";

ShellRemovedFacesLabelId = "Faces to remove: ";

CoreId = "Core";
CavityId = "Cavity";

//===================================================================
// LongHelp
//===================================================================

MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.ShellThicknessFrameId.ShellThicknessLabelId.LongHelp =
"Specifies the shelled body thickness.";

MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.ShellRemovedFacesSelectorMultiId.LongHelp =
"Specifies the faces to remove from the
shelled body.";

MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.WallDirLabelId.LongHelp = 
"Specifies whether Shell feature walls are contructed
to the Inside or Outside of selected boundary profile";

MainFrameId.PropFrameId.PropTabContainerId.ShellPropTabPageId.ShellFrameId.WallDirComboId.LongHelp = 
"Specifies whether Shell feature walls are contructed
to the Inside or Outside of selected boundary profile";

MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.LongHelp =
"Parameters for other thickness faces";

MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedFaceThicknessLabelId.LongHelp =
"Specifies the thickness for selected face.";

MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedThicknessFacesLabelId.LongHelp =
"Specifies faces which have their own
thickness value set.";

MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedThicknessFacesComboId.LongHelp =
"Specifies faces which have their own
thickness value set.";

MainFrameId.PropFrameId.PropTabContainerId.AdvancedPropTabPageId.AdvancedFrameId.AdvancedApplyThicknessCheckButtonId.LongHelp =
"Indicates that the current thickness 
value is to be applied to all selected
faces.";

MainFrameId.PropFrameId.PropTabContainerId.CorePropTabPageId.ShellListFrameId.LongHelp = "Select from the list.";


//===================================================================
// Helper picture
//===================================================================

InsideFacesToRemoveTextId = "Faces to remove";
InsideOtherTextId = "Other";
InsideThickness1TextId = "Thickness";
InsideThickness2TextId = "Thickness";
InsideOtherThicknessFacesTextId = "Other thickness faces";


OutsideFacesToRemoveTextId = "Faces to remove";
OutsideOtherTextId = "Other";
OutsideThickness1TextId = "Thickness";
OutsideThickness2TextId = "Thickness";
OutsideOtherThicknessFacesTextId = "Other thickness faces";


SelectCavityCavityBodyTextId = "Cavity Body";

SelectCoreCoreBodyTextId = "Core Body";
