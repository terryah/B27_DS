NewPartTOFrame.HeaderFrame.Global.Title     = "When creating part";
NewPartTOFrame.HeaderFrame.Global.ShortHelp = "Options applied when creating a new part.";
NewPartTOFrame.HeaderFrame.Global.LongHelp  = "Options applied when creating a new part.";

NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateAxisSystemModeCheck.Title = "Create an axis system";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateAxisSystemModeCheck.ShortHelp = "An axis system will be created, 
when creating a Mechanical Part."; 
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateAxisSystemModeCheck.LongHelp = "An axis system will be created, 
when creating a Mechanical Part."; 

NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.PartSizeFrame.PartSizeLabel.Title = "Part's size: ";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.PartSizeFrame.PartSizeCombo.ShortHelp = "Size of the new part"; 
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.PartSizeFrame.PartSizeCombo.LongHelp = "Definition of the part's size that 
will be applied when creating a new part";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.PartSizeFrame.PartSizeCombo.LargeScale = "Large Part";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.PartSizeFrame.PartSizeCombo.StandardScale = "Standard Part";

NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateOpenBodyModeCheck.Title = "Create a geometrical set";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateOpenBodyModeCheck.ShortHelp = "A geometrical set will be created 
when creating a Mechanical Part."; 
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateOpenBodyModeCheck.LongHelp = "A geometrical set will be created 
when creating a Mechanical Part."; 

NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateOGSCheck.Title = "Create an ordered geometrical set";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateOGSCheck.ShortHelp = "An ordered geometrical set will be created 
when creating a Mechanical Part."; 
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateOGSCheck.LongHelp = "An ordered geometrical set will be created 
when creating a Mechanical Part."; 

NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateWorkingSupportModeCheck.Title = "Create a 3D work support";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateWorkingSupportModeCheck.ShortHelp = "A 3D work support will be created
when creating a Mechanical Part."; 
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.CreateWorkingSupportModeCheck.LongHelp = "A 3D work support will be created
when creating a Mechanical Part."; 

NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.DisplayPanelModeCheck.Title = "Display the `New Part` dialog box"; 
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.DisplayPanelModeCheck.ShortHelp = "A dialog box with extra options is
displayed when creating a Mechanical Part.";
NewPartTOFrame.IconAndOptionsFrame.OptionsFrame.DisplayPanelModeCheck.LongHelp = "The `New Part` dialog box 
enable to set the Part Number
and to create a geometrical set or not."; 

HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeLabel.Title = "When created, locate wireframe and surface elements";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeLabel.ShortHelp = "Option to decide where wireframe and surface elements will be created.";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeLabel.LongHelp = "Option to decide where wireframe and surface elements will be created.";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeFrame.WireInBodyButton.Title= "In a body";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeFrame.WireInBodyButton.ShortHelp= "The elements will be created directly in a body.";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeFrame.WireInBodyButton.LongHelp= "The elements will be created directly in a body.";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeFrame.WireInGSButton.Title="In a geometrical set";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeFrame.WireInGSButton.ShortHelp="The elements will be created directly in a geometrical set.";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.WireframeFrame.WireInGSButton.LongHelp="The elements will be created directly in a geometrical set.";

HybridDesignTOFrame.HeaderFrame.Global.Title = "Hybrid design";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.HybridDesignCheck.Title="Enable hybrid design inside part bodies and bodies";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.HybridDesignCheck.ShortHelp="Enable to mix wirframe/surfacic and solid designs in part bodies and bodies.";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.HybridDesignCheck.LongHelp="Enable to mix wirframe/surfacic and solid designs in part bodies and bodies.";

HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.KnowledgeInHybridDesignCheck.Title="Enable hybrid design for parameters and relations inside part bodies and bodies";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.KnowledgeInHybridDesignCheck.ShortHelp="Enable to insert parameters and relations inside part bodies and bodies.";
HybridDesignTOFrame.IconAndOptionsFrame.OptionsFrame.KnowledgeInHybridDesignCheck.LongHelp="Enable to insert parameters and relations inside part bodies and bodies.";

TrueColorTOFrame.HeaderFrame.Global.Title = "Color inheritance management";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorLabel.Title = "When created under ordered sets, absorbing features";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorLabel.ShortHelp = "Option to decide what color inheritance mode will be defined when new parts are created.";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorLabel.LongHelp = "Option to decide what color inheritance mode will be defined when new parts are created.";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorModeActivated.Title= "Inherit color from all inputs.";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorModeActivated.ShortHelp= "The coloration of absorbing feature will be defined using all their input features.";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorModeActivated.LongHelp= "The coloration of absorbing feature will be defined using all their input features.";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorModeDeactivated.Title="Inherit color from main input.";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorModeDeactivated.ShortHelp="The coloration of absorbing feature will be defined using only their main input features.";
TrueColorTOFrame.IconAndOptionsFrame.OptionsFrame.TrueColorModeDeactivated.LongHelp="The coloration of absorbing feature will be defined using only their main input features.";

V5="V5";
Custom="Custom";
Settings="Settings";
V6WhiteTheme="3DExperience White";
V6BlackTheme="3DExperience Black";

ColorSynchronizationTOFrame.HeaderFrame.Global.Title = "Color on import management";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationLabel.Title = "When creating a new part, features created through copy paste as result with or without link";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationLabel.ShortHelp = "Option to decide whether an imported feature will report reference feature color on its faces or not.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationLabel.LongHelp = "Option to decide whether an imported feature will report reference feature color on its faces or not.";

ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationMode.Title= "All Colors: each color visible in 3D is reported on its corresponding face";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationMode.ShortHelp= "The imported feature will report reference feature graphic properties on its faces.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationMode.LongHelp= "The imported feature will report reference feature graphic properties on its faces.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchroSubOptionsFrame.ColorSynchronizationModeOnFeature.Title= "Color of features";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchroSubOptionsFrame.ColorSynchronizationModeOnFeature.ShortHelp= "The imported feature will report reference feature graphic properties.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchroSubOptionsFrame.ColorSynchronizationModeOnFeature.LongHelp= "The imported feature will report reference feature graphic properties.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationModeManage.Title= "Manage the color transfer";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationModeManage.ShortHelp= "Give access to specific options for color report.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationModeManage.LongHelp= "Give access to specific options for color report.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchroSubOptionsFrame.ColorSynchronizationModeOnSubElements.Title= "Color of overloaded faces";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchroSubOptionsFrame.ColorSynchronizationModeOnSubElements.ShortHelp= "The imported feature will report reference feature graphic properies on its overloaded faces.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchroSubOptionsFrame.ColorSynchronizationModeOnSubElements.LongHelp= "The imported feature will report reference feature graphic properies on its overloaded faces.";

ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationEditabilityLabel.Title = "When editing Part properties";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationEditabilityLabel.ShortHelp = "Option to decide whether option of Part property are interactively editable or not.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationEditabilityLabel.LongHelp = "Option to decide whether option of Part property are interactively editable or not.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationEditability.Title= "Color on import management property is editable";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationEditability.ShortHelp= "If the option is checked, Part property for color on import management is interactively editable.";
ColorSynchronizationTOFrame.IconAndOptionsFrame.OptionsFrame.ColorSynchronizationEditability.LongHelp= "If the option is checked, Part property for color on import management is interactively editable.";

GraphicPropertiesManagementTOFrame.HeaderFrame.Global.Title = "Graphic properties management";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.Colors3DExperienceManagementLabel.Title = "When creating a new Part";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.Colors3DExperienceManagementLabel.ShortHelp = "a faire";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.Colors3DExperienceManagementLabel.LongHelp = "a faire";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.Colors3DExperienceManagement.Title= "Use 3DExperience colors management";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.Colors3DExperienceManagement.ShortHelp= "If the option is checked, 3DExperience color management is displayed.";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.Colors3DExperienceManagement.LongHelp= "If the option is checked, 3DExperience color management is displayed.";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.DefaultColorsEditabilityLabel.Title = "When editing Part properties";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.DefaultColorsEditabilityLabel.ShortHelp = "Option to decide whether Part default graphic properties property are interactively editable or not.";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.DefaultColorsEditabilityLabel.LongHelp = "Option to decide whether Part default graphic properties property are interactively editable or not.";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.DefaultColorsEditability.Title= "Default graphic properties are editable in 3DExperience context";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.DefaultColorsEditability.ShortHelp= "If the option is checked, Default graphic properties are editable in 3DExperience context.";
GraphicPropertiesManagementTOFrame.IconAndOptionsFrame.OptionsFrame.DefaultColorsEditability.LongHelp= "If the option is checked, Default graphic properties are editable in 3DExperience context.";





