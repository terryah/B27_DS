//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    DraftingIntCommandsErrorCatalog
// LOCATION    :    DraftingIntCommands/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Jun. 05 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  Error Messages.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
// 			   LGK 25.06.2002	
//------------------------------------------------------------------------------

ApplyMoveOnOverRunOrBlanking_NoAPIError=
"The local modification of this item
is not implemented yet.";

UpdateValueModification_WrongWindowOperation=
"An error occurred while modifying
value parameters.";

UpdateFunnelModification_WrongWindowOperation=
"An error occurred while modifying
funnel parameters.";

CreateProposalDimension_CreateDimension=
"The dimension creation failed.";

NonAssocDimensionNotAllowed=
"Only a non-associative dimension
can be created in this configuration.
Current dimension creation options do not 
allow the creation of such a dimension.";

GeometryAnalysisFailedInDimensionCreationContext=
"Geometry decoding or dimension case analysis failed.";

UndoRedoErrorInQuickDim=
"An error occurred during the Undo/Redo operation.";

DimensionCanNotBeChained=
"It is impossible to create a dimension of the same type 
as the previous ones for the last item you selected.";

DimensionCaseNotYetSupported=
"It is currently impossible to 
create this type of dimension.";

CoordDimensionOn3DNotYetSupported=
"It is impossible to create a dimension 
in this geometric configuration.";

InvertDimensionLineSymbol_InvalidDimensionType=
"Invalid dimension type error:
Error raised in CATDimAgent::InvertDimensionLineSymbol.";

InvertDimensionLineSymbol_InvalidSymbolMode=
"Invalid symbol mode error:
Error raised in CATDimAgent::InvertDimensionLineSymbol.";

CATDimAgent_ConstructionFailed=
"It is impossible to build a dimension agent.";

RefreshHandles_DimSkeletonOperationFailed=
"Error while processing dimension skeleton:
Error raised in CATDimAgent::RefreshHandles.";

CATExtensionLineSkeleton_WrongExtensionLineReference=
"Error while building dimension skeleton.";

CATDimAgent_MoveLdrBagError=
"Error while managing ambiguity nullifier.";

AngleSectorSubMenuContent_WrongItem=
"Wrong angle dimension.";

CATDicAreaNoObjectToCompute=
"There is no geometrical element to process within this view." ;

CATDicAreaNoValidArea=
"This area cannot be filled. 
You must select a closed area.";

CATDicAreaCSONoValidArea=
"The elements you selected do not make up a closed area. 
You must select a closed area.";

CATDicAxisLineInvalidEdge=
"The selected edge is not valid to create an axis line.";

CATDicAxisLineInvalidEdges=
"The selected edges are not valid to create an axis line.";

CATDicAxisLineInvalidViews=
"The selected objects are not in the same view.";

CATDicAxisLineCreationFailed = "The creation of the axis line failed.";
CATDicCenterLineCreationFailed = "The creation of the center line failed.";
CATDicThreadCreationFailed = "The creation of the thread failed.";

CATDicAxlNonAssoc="Created axis line is not associative to 3D";
CATDicCtrlNonAssoc="Created center line is not associative to 3D";
CATDicThreadNonAssoc="Created thread is not associative to 3D";
CATDicAxlCtrlNonAssoc="One or several elements are not associative";

CATDrwDimFrameEditValue.NullValue="The dimension value cannot be null.";

CATDicRerootDimInvalidParent="The selected dimension cannot be rerouted to this element.";

Warning="Warning";
