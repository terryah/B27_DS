//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwCheckItemHeader
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog/
// AUTHOR      :    ogk
// DATE        :    05.01.99
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to the overview
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//    01            lgk   14.10.02  Revision
//------------------------------------------------------------------------------


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Menu View
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//------------------------------------------------------------------------------
// Drafting' Overview
//------------------------------------------------------------------------------
CATDrwCheckItemHeader.DrwToggleOverView.Title="Drawing Overview" ;
CATDrwCheckItemHeader.DrwToggleOverView.ShortHelp="Overview of the geometry";
CATDrwCheckItemHeader.DrwToggleOverView.Help="Shows a preview of the drawing" ;
CATDrwCheckItemHeader.DrwToggleOverView.LongHelp =
"Overview of the geometry
Shows a preview of the drawing.";

CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.Title="Extend to Center";
CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.ShortHelp= "Extend to Center";
CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.Help="Extends a radius or one-symbol diameter dimension line to center";
CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.LongHelp =
"Extend to Center
Extends a radius or one-symbol diameter dimension line to center.";

//------------------------------------------------------------------------------
// Change Orientation Command
//------------------------------------------------------------------------------
CATDrwCheckItemHeader.CATDrwDimCheckAutoHdr.Title="Force Dimension on Element";
CATDrwCheckItemHeader.CATDrwDimCheckHorizHdr.Title="Force Horizontal Dimension in View";
CATDrwCheckItemHeader.CATDrwDimCheckVertHdr.Title="Force Vertical Dimension in View";
CATDrwCheckItemHeader.CATDrwDimCheckLinearHdr.Title="Linear";
CATDrwCheckItemHeader.CATDrwDimCheckOffsetHdr.Title="Offset";
CATDrwCheckItemHeader.CATDrwDimCheckParallelHdr.Title="Parallel";
CATDrwCheckItemHeader.CATDrwDimCheckTrueDimHdr.Title="True Length Dimension";

CATDrwCheckItemHeader.CATDrwDimCheckRepHorHdr.Title="Linear Horizontal";
CATDrwCheckItemHeader.CATDrwDimCheckRepVertHdr.Title="Linear Vertical";
CATDrwCheckItemHeader.CATDrwDimCheckRepLinearHdr.Title="Linear";
CATDrwCheckItemHeader.CATDrwDimCheckRepIsometricHdr.Title="Isometric";
CATDrwCheckItemHeader.CATDrwDimCheckRepParallelHdr.Title="Parallel";
CATDrwCheckItemHeader.CATDrwDimCheckRepOffsetHdr.Title="Offset";


//------------------------------------------------------------------------------
// Change Angular Sector Command
//------------------------------------------------------------------------------
CATDrwCheckItemHeader.CATDrwDimCheckSector1Hdr.Title="Sector 1";
CATDrwCheckItemHeader.CATDrwDimCheckSector2Hdr.Title="Sector 2";
CATDrwCheckItemHeader.CATDrwDimCheckSector3Hdr.Title="Sector 3";
CATDrwCheckItemHeader.CATDrwDimCheckSector4Hdr.Title="Sector 4";
CATDrwCheckItemHeader.CATDrwDimCheckComplementaryHdr.Title="Complementary";

CATDrwCheckItemHeader.CATDrwCheckAssocPosRigidHdr.Title="Rigid";

CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.Title="Position Texts Manually";
CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.ShortHelp= "Position Callout Texts Manually";
CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.Help="Turns callout texts in manual or automatic position mode";
CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.LongHelp="Position Texts Manually\nTurns callout texts in manual or automatic position mode.";
