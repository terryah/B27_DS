// COPYRIGHT DASSAULT SYSTEMES 
//----------------------------
// NLS message for the CATBIWInputError Error 

//
//-- Ridge
RidgeIdenticalInputERR_1001.Request    = "Identical elements.";
RidgeIdenticalInputERR_1001.Diagnostic = "/p1 cannot be used both as reference and base surface.";
RidgeIdenticalInputERR_1001.Advice     = "Please use two different elements.";

RidgeNonConnexInputERR_1002.Request    = "/p1 is a non connex element.";
RidgeNonConnexInputERR_1002.Diagnostic = "Base surface element has to be connex.";
RidgeNonConnexInputERR_1002.Advice     = "Use another element, or extract connex entity with EXTRACT operator.";

RidgeInfiniteInputERR_1003.Request    = "/p1 is an infinite element.";
RidgeInfiniteInputERR_1003.Diagnostic = "Base surface element has to be a limited element.";
RidgeInfiniteInputERR_1003.Advice     = "Use another element.";
 
RidgeInfiniteInputERR_1004.Request    = "Reference direction need to be defined.";
RidgeInfiniteInputERR_1004.Diagnostic = "Default reference direction can not be computed on reference curve /p1.";
RidgeInfiniteInputERR_1004.Advice     = "Please define an explicit reference direction.";

RidgeInvalidReferenceCurveERR_1005.Request    = "/p1 curve can not be used as a reference.";
RidgeInvalidReferenceCurveERR_1005.Diagnostic = "The curve must have a projection on the base surface along the Reference direction.\n And the projection curve must be long enough to join the base surface boundaries.";
RidgeInvalidReferenceCurveERR_1005.Advice     = "Use another element, or extrapolate it with EXTRAPOLATE operator.";

RidgeInvalidReferenceSurfaceERR_1006.Request    = "/p1 surface can not be used as a reference.";
RidgeInvalidReferenceSurfaceERR_1006.Diagnostic = "The surface must have an intersection with the base surface.\n And the intersection curve must be long enough to join the base surface boundaries.";
RidgeInvalidReferenceSurfaceERR_1006.Advice     = "Use another element, or extrapolate it with EXTRAPOLATE operator.";

RidgeNonConnexReferenceLocationERR_1007.Request    = "Non connex reference location.";
RidgeNonConnexReferenceLocationERR_1007.Diagnostic = "Base and reference elements lead to a non connex reference location.";
RidgeNonConnexReferenceLocationERR_1007.Advice     = "Use another elements.";

RidgeClosedReferenceLocationERR_1008.Request    = "Closed reference location.";
RidgeClosedReferenceLocationERR_1008.Diagnostic = "Base and reference elements lead to a closed reference location.";
RidgeClosedReferenceLocationERR_1008.Advice     = "Use another elements.";

RidgeReferenceLocationIntersectionERR_1009.Request    = "Both reference location curves intersect.";
RidgeReferenceLocationIntersectionERR_1009.Diagnostic = "Reference location curves must not intersect themselves.";
RidgeReferenceLocationIntersectionERR_1009.Advice     = "Use reference elements which are not intersecting.";

RidgeMultipleG1ValuesERR_1010.Request    = "Conflicting thickness values.";
RidgeMultipleG1ValuesERR_1010.Diagnostic = "More than one value are defined on the highlighted elements.";
RidgeMultipleG1ValuesERR_1010.Advice     = "Please remove the useless values.";

RidgeImpossibleSplitERR_1011.Request    = "Split operation cannot be performed.";
RidgeImpossibleSplitERR_1011.Diagnostic = "Current Reference element(s) cannot correctly split the base surface.";
RidgeImpossibleSplitERR_1011.Advice     = "You can switch off the split option.";

RidgeDirectionIsMandatoryERR_1012.Request    = "Reference direction has to be defined.";
RidgeDirectionIsMandatoryERR_1012.Diagnostic = "The reference direction, associated to the following reference location, is not defined: /p1";
RidgeDirectionIsMandatoryERR_1012.Advice     = "Please define this direction.";

//Ridge
RidgeInvalidLocalOffsetValue_1013.Request    = "Change local offset value to 0mm of highlighted faces containing a hole.";
RidgeInvalidLocalOffsetValue_1013.Diagnostic = "";
RidgeInvalidLocalOffsetValue_1013.Advice     = "";

//
//-- Based Curve Law
BCLAlreadyDefinedInputERR_2001.Request    = "Already defined.";
BCLAlreadyDefinedInputERR_2001.Diagnostic = "A constraint value is already defined on this element.";
BCLAlreadyDefinedInputERR_2001.Advice     = "Please use the already defined constraint.";

BCLAConflictingValueERR_2002.Request    = "Conflicting value.";
BCLAConflictingValueERR_2002.Diagnostic = "A constraint value is already defined on a connex element that is tangent continuous.";
BCLAConflictingValueERR_2002.Advice     = "Please use the already defined constraint.";

//
//-- Hole
PunchDirIsNotDefinedERR_3001.Request    = "Punch direction is not properly defined.";
PunchDirIsNotDefinedERR_3001.Diagnostic = "/p1 point does not lie on /p2 base surface. \n Default punch direction cannot be computed as normal.";
PunchDirIsNotDefinedERR_3001.Advice     = "Use another point lying on the base surface \n or define explicitly a punch direction";

HoleInfiniteInputERR_3002.Request    = "/p1 is an infinite element.";
HoleInfiniteInputERR_3002.Diagnostic = "Hole cannot be defined on an infinite element.";
HoleInfiniteInputERR_3002.Advice     = "Use another element.";

HoleInputDimensionIsNullERR_3003.Request    = "The /p1 parameter value must be greater than zero.";
HoleInputDimensionIsNullERR_3003.Diagnostic = "The parameter value is invalid = /p1.";
HoleInputDimensionIsNullERR_3003.Advice     = "Change the parameter value.";

HoleInputDimensionIsGreaterThanERR_3004.Request    = "The /p1 parameter value must be greater than the /p2 parameter value.";
HoleInputDimensionIsGreaterThanERR_3004.Diagnostic = "The /p1 parameter value is lower than the /p2 parameter value = /p3.";
HoleInputDimensionIsGreaterThanERR_3004.Advice     = "Change a parameter value.";

HoleInputDimensionIsNTimesGreaterThanERR_3005.Request    = "The /p1 parameter value must be /p3 times greater than the /p2 parameter value.";
HoleInputDimensionIsNTimesGreaterThanERR_3005.Diagnostic = "The /p1 parameter value is lower than /p4 times the /p2 parameter value = /p3.";
HoleInputDimensionIsNTimesGreaterThanERR_3005.Advice     = "Change a parameter value.";

HoleAxisLineOriginProjectionERR_3006.Request = "The axis can not be defined.";
HoleAxisLineOriginProjectionERR_3006.Diagnostic = "The hole center point can not be projected on the support surface.";
HoleAxisLineOriginProjectionERR_3006.Advice = "Change the center point.";

//
//-- TriangleBead
TriangleBeadInvalidLocationElementERR_4001.Request    = "/p1 does not lie on base element.";
TriangleBeadInvalidLocationElementERR_4001.Diagnostic = "/p1 point does not lie on /p2 base surface.";
TriangleBeadInvalidLocationElementERR_4001.Advice     = "Use another element.";

TriangleBeadInvalidLocationElementERR_4002.Request    = "/p1 does not lie on a sharp edge of the base element.";
TriangleBeadInvalidLocationElementERR_4002.Diagnostic = "/p1 point does not lie on a /p2 sharp edge.";
TriangleBeadInvalidLocationElementERR_4002.Advice     = "Use another element.";

TriangleBeadLocationElementAmbiguityERR_4003.Request    = "Bead reference localisation.";
TriangleBeadLocationElementAmbiguityERR_4003.Diagnostic = "There is a ambiguity. /p1 lies on more than two /p2 sharp edges.";
TriangleBeadLocationElementAmbiguityERR_4003.Advice     = "Use another element.";

TriangleBeadValueOutOfRangeERR_4004.Request    = "The /p1 parameter value is out of range.";
TriangleBeadValueOutOfRangeERR_4004.Diagnostic = "The maximum value (estimated) is /p1.";
TriangleBeadValueOutOfRangeERR_4004.Advice     = "Decrease the /p1 parameter.";

TriangleBeadValueOutOfRangeERR_4005.Request    = "The /p1 parameter value is out of range.";
TriangleBeadValueOutOfRangeERR_4005.Diagnostic = "The result shape has to lie on the indicated subpart.";
TriangleBeadValueOutOfRangeERR_4005.Advice     = "Decrease the /p1 parameter.";

TriangleBeadValueOutOfRangeERR_4006.Request    = "Incompatible /p1 parameter value and Reference direction.";
TriangleBeadValueOutOfRangeERR_4006.Diagnostic = "Red curves must have an intersection.";
TriangleBeadValueOutOfRangeERR_4006.Advice     = "Decrease the /p1 parameter or modify the direction in order to define an intersection.";

TriangleBeadRefFaceNotAdjacentERR_4007.Request    = "Incorrect reference face.";
TriangleBeadRefFaceNotAdjacentERR_4007.Diagnostic = "Reference face could not be used to resolve the destabilization.";
TriangleBeadRefFaceNotAdjacentERR_4007.Advice     = "Specify any face adjacent to the list of tangent continuous edges lying at location point.";

TriangleBeadRefEdgeIncorrectERR_4008.Request    = "Incorrect reference edge.";
TriangleBeadRefEdgeIncorrectERR_4008.Diagnostic = "Reference edge could not be used to resolve the destabilization.";
TriangleBeadRefEdgeIncorrectERR_4008.Advice     = "Specify any edge in the list of tangent continuous edges lying at location point.";

TriangleBeadRefElemIncorrectERR_4009.Request    = "Incorrect reference element.";
TriangleBeadRefElemIncorrectERR_4009.Diagnostic = "Reference element is not part of the base surface.";
TriangleBeadRefElemIncorrectERR_4009.Advice     = "Specify any face adjacent to location point or edge in the list \n of tangent continuous edges lying at location point.";

TriangleBeadRefFaceFederatedERR_4010.Request    = "Incorrect reference face.";
TriangleBeadRefFaceFederatedERR_4010.Diagnostic = "Reference face is present in both the left and right side lists of face adjacent to location point.";
TriangleBeadRefFaceFederatedERR_4010.Advice     = "Specify any face which is present on only one side of the location point.";

TriangleBeadFederatedERR_4011.Request    = "Default reference element selection failed.";
TriangleBeadFederatedERR_4011.Diagnostic = "Faces adjacent to location point and edge lying at location point is federated.";
TriangleBeadFederatedERR_4011.Advice     = "Specify an external curve feature which could be laid down on one of the tangent \n continuous edges obtained from the location point.";

TriangleBeadFederatedEdgeERR_4012.Request    = "Incorrect reference edge.";
TriangleBeadFederatedEdgeERR_4012.Diagnostic = "Specified reference edge is federated.";
TriangleBeadFederatedEdgeERR_4012.Advice     = "Specify an external curve feature which could be laid down on one of the tangent \n continuous edges obtained from the location point.";

//-- Bead
BeadInfiniteInputERR_1003.Request    = "/p1 is an infinite element.";
BeadInfiniteInputERR_1003.Diagnostic = "Base surface element has to be a limited element.";
BeadInfiniteInputERR_1003.Advice     = "Use another element.";
