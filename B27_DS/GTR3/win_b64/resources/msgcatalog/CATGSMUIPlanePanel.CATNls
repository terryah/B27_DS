//---------------------------------------------------------------
// Resource file for CATGSMUIPlanePanel class
// US
//---------------------------------------------------------------

Title="Plane Definition";

PlaneTypeEq="Equation";
PlaneType3Pts="Through three points";
PlaneType2Pts="Through two lines";
PlaneType1Pt1Ln="Through point and line";
PlaneType1Crv="Through planar curve";
PlaneTypeTgt="Tangent to surface";
PlaneTypeNrm="Normal to curve";
PlaneTypeOff="Offset from plane";
PlaneTypeOffPt="Parallel through point";
PlaneTypeAng="Angle/Normal to plane";
PlaneTypeMean="Mean through points";
PlaneTypeExpl="Explicit";

frame1.LabList.Title="Plane type: ";
frame1.CmbList.LongHelp="Lists the available methods for creating planes.";


EqFrame.LabelEqTitle.Title=" Ax+By+Cz = D";
EqFrame.FraGeo.LabelEqA.Title="A: ";
EqFrame.FraGeo.LabelEqB.Title="B: ";
EqFrame.FraGeo.LabelEqC.Title="C: ";
EqFrame.FraGeo.LabelEqD.Title="D: ";
EqFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="A";
EqFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="B";
EqFrame.FraGeo.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.Title="C";
EqFrame.FraGeo.FraLengTang4.EnglobingFrame.IntermediateFrame.Spinner.Title="D";
EqFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
EqFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
EqFrame.FraGeo.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
EqFrame.FraGeo.FraLengTang4.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
EqFrame.FraGeo.FraElem1.LongHelp="Specifies the point through which plane should pass. ";
EqFrame.FraGeo.LabelEqPt.Title="Point: ";
// added code 3dplm for Axis System
EqFrame.FraGeo.LabelAxis.Title="Axis System: ";
EqFrame.FraGeo.FraAxis.LongHelp="Specifies the reference Local Axis System\nin which the coordinates are converted. \nIf not specified then absolute axis system \nis used as default axis system.";
//EqFrame.ChkAxis.Title="Coordinates in absolute axis system";
//EqFrame.ChkAxis.LongHelp="Allows to view and modify coordinates\nin the absolute axis system.";
EqFrame.PushCompassV5.Title="Normal to compass";
EqFrame.PushCompassV5.LongHelp="Position the plane perpendicular to the compass direction.";
EqFrame.PushCompass.Title="Normal to Robot";
EqFrame.PushCompass.LongHelp="Position the plane perpendicular to the Robot direction.";
EqFrame.PushScreen.Title="Parallel to screen";
EqFrame.PushScreen.LongHelp="Position the plane parallel to the screen current view.";


P3PtsFrame.Label3PtsPt1.Title="Point 1: ";
P3PtsFrame.Label3PtsPt2.Title="Point 2: ";
P3PtsFrame.Label3PtsPt3.Title="Point 3: ";
P3PtsFrame.FraElem1.LongHelp="Specifies the first point through which the plane is to pass.";
P3PtsFrame.FraElem2.LongHelp="Specifies the second point through which the plane is to pass.";
P3PtsFrame.FraElem3.LongHelp="Specifies the third point through which the plane is to pass.";


P2LnFrame.Label2LnLn1.Title="Line 1: ";
P2LnFrame.Label2LnLn2.Title="Line 2: ";
P2LnFrame.FraElem1.LongHelp="Specifies the first line through which the plane is to pass.";
P2LnFrame.FraElem2.LongHelp="Specifies the second line through which the plane is to pass.\nIf lines are not in the same plane, the second line\nis just used to compute the plane second direction.";
P2LnFrame.ChkCop.Title="Forbid non coplanar lines";
P2LnFrame.ChkCop.LongHelp="Specifies that both lines have to be in the same plane";

PtLnFrame.LabelPtLnPt.Title="Point: ";
PtLnFrame.LabelPtLnLn.Title="Line: ";
PtLnFrame.FraElem1.LongHelp="Specifies the point through which the plane is to pass.";
PtLnFrame.FraElem2.LongHelp="Specifies the line through which the plane is to pass.";


CrvFrame.LabelCrvCurve.Title="Curve: ";
CrvFrame.FraElem1.LongHelp="Specifies the planar curve through which the plane is to pass.";

TgtFrame.LabelTgtSurf.Title="Surface: ";
TgtFrame.LabelTgtPoint.Title="Point: ";
TgtFrame.FraElem1.LongHelp="Specifies the reference surface whose tangent is to be used\nto determine the plane.";
TgtFrame.FraElem2.LongHelp="Specifies the point on the reference surface\nat which the tangent is to be used to determine the plane.";

NrmFrame.LabelNrmCurve.Title="Curve: ";
NrmFrame.LabelNrmPoint.Title="Point: ";
NrmFrame.CkeParamFrame.RatioOfCurveLength.Title="Ratio Of Curve Length";
NrmFrame.CkeParamFrame.LabelOnCrvLength.Title="Ratio:";
NrmFrame.PushRev.Title="Reverse Direction";
NrmFrame.FraElem1.LongHelp="Specifies the reference curve whose normal is to be used\nto determine the plane.";
NrmFrame.FraElem2.LongHelp="Specifies the point on the reference curve\nat which the normal is to be used to determine the plane.";

OffFrame.FraGeo.LabelOffRef.Title="Reference: ";
OffFrame.FraGeo.LabelOffValue.Title="Offset: ";
OffFrame.FraGeo.FraElem1.LongHelp="Specifies the reference plane to be offset.";
OffFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
OffFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Offset";
OffFrame.PushRev.Title="Reverse Direction";
OffFrame.PushRev.LongHelp="Displays the offset plane on the other side of the reference plane.";

OffPtFrame.LabelOffPtRef.Title="Reference: ";
OffPtFrame.LabelOffPt.Title="Point: ";
OffPtFrame.FraElem1.LongHelp="Specifies the reference plane to which\nthe created plane is to be parallel.";
OffPtFrame.FraElem2.LongHelp="Specifies the point through which\nthe parallel plane is to pass.";


AngFrame.FraGeo.LabelAngRef.Title="Reference: ";
AngFrame.FraGeo.LabelAngDir.Title="Rotation axis: ";
AngFrame.FraGeo.LabelAngValue.Title="Angle: ";
AngFrame.FraGeo.FraElem2.LongHelp="Specifies the reference plane to be rotated.";
AngFrame.FraGeo.FraElem1.LongHelp="Specifies the line to be used as the axis\nfor rotating the reference plane.";
AngFrame.FraGeo.FraAngTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="You can also use the contextual menu to specify formula";
AngFrame.FraGeo.FraAngTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Angle";
AngFrame.PushNrm.Title="Normal to plane";
AngFrame.PushNrm.LongHelp="Allows you to specify that an angle of 90 degrees\nis to be used to define new plane.";
AngFrame.AxisProject.Title="Project rotation axis on reference plane";
AngFrame.AxisProject.LongHelp="Allows you to project rotation axis on reference plane.";

MeanFrame.LabelMeanPoints.Title="Points: ";
//MeanFrame.FraList.LongHelp="Lists the points that are to be used to determine\nthe mean plane.";

ExpFrame.FrameExpNrm.Title="Normal: ";
ExpFrame.FrameExpNrm.LabelExpNrmX.Title="X: ";
ExpFrame.FrameExpNrm.LabelExpNrmY.Title="Y: ";
ExpFrame.FrameExpNrm.LabelExpNrmZ.Title="Z: ";
ExpFrame.FrameExpNrm.UxValue.LongHelp="Specifies the X component of the plane's normal.";
ExpFrame.FrameExpNrm.UyValue.LongHelp="Specifies the Y component of the plane's normal.";
ExpFrame.FrameExpNrm.UzValue.LongHelp="Specifies the Z component of the plane's normal.";

PlaneManipulator="Move";
// start code 3dplm
//Combo lock help
PlaneLockShort="Click to enable automatic type change.";
PlaneUnlockShort="Click to disable automatic type change.";
PlaneLockLong="On clicking this button automatic type change will be enabled.";
PlaneUnlockLong="On clicking this button automatic type change will be disabled.";
// for axis system
AngFrame.FraGeo.LabelAngRef.LongHelp="Specify the reference element.";
AngFrame.FraGeo.ReferenceSelector.LongHelp="Select a point, a line or a plane as reference element.";
AngFrame.FraGeo.LabelAngValue.LongHelp="Modify the value of angle.";
// end code 3dplm
