//
toDPIPrefixFrame.HeaderFrame.Global.Title = "Default Prefix for Power Input";

toDPIPrefixFrame.IconAndOptionsFrame.OptionsFrame.DPIPrefixCmb.ShortHelp =
"Default prefix implicitely used in Power Input";
toDPIPrefixFrame.IconAndOptionsFrame.OptionsFrame.DPIPrefixCmb.LongHelp =
"Defines the prefix to be used by default at the beginning of the string entered in the Power Input
if none has been explicitly typed.
For example, choose c: if you use Power Input mainly to start commands,
this will avoid typing \"c:\" before each command name.";

//
toDPISCScopeFrame.HeaderFrame.Global.Title = "Default Search Scope for Power Input ";

toDPISCScopeFrame.IconAndOptionsFrame.OptionsFrame.DPISCScopeCmb.ShortHelp =
"Default search scope implicitely used in Power Input";
toDPISCScopeFrame.IconAndOptionsFrame.OptionsFrame.DPISCScopeCmb.LongHelp =
"Defines the context scope to be used by default for search queries entered in the Power Input
if none has been explicitly typed.
For example, choose InWorkbench if you search mainly in the active element,
this will avoid typing \",in\" after each criterion.";

toDPISCScopeFrame.IconAndOptionsFrame.OptionsFrame.DPISCPriorityChkBtn.Title = "Replaces scope defined in favorite queries";
toDPISCScopeFrame.IconAndOptionsFrame.OptionsFrame.DPISCPriorityChkBtn.ShortHelp =
"Default scope is used when a favorite query is used in Power Input";
toDPISCScopeFrame.IconAndOptionsFrame.OptionsFrame.DPISCPriorityChkBtn.LongHelp =
"Check this option if you want the default context scope to be used with
favorite queries used in the Power Input.";

//
toDeepSearchFrame.HeaderFrame.Global.Title = "Deep Search";

toDeepSearchFrame.IconAndOptionsFrame.OptionsFrame.DeepSearchChkBtn.Title = "Deep search activation";
toDeepSearchFrame.IconAndOptionsFrame.OptionsFrame.DeepSearchChkBtn.ShortHelp =
"Activates the Deep search.";
toDeepSearchFrame.IconAndOptionsFrame.OptionsFrame.DeepSearchChkBtn.LongHelp =
"Check this option to activate the Deep search option when performing a search.
In that case, documents in visualization mode will be transiently loaded in order to find elements.
This option is useless if you do not work with the cache system.";

//
toMaxDisplayFrame.HeaderFrame.Global.Title = "Maximum Displayed Results";

toMaxDisplayFrame.IconAndOptionsFrame.OptionsFrame.MaxDisplaySlider.Title = "Maximum Displayed Results";
toMaxDisplayFrame.IconAndOptionsFrame.OptionsFrame.MaxDisplaySlider.ShortHelp =
"Defines the maximum number of elements that can be displayed in the Search results page.";
toMaxDisplayFrame.IconAndOptionsFrame.OptionsFrame.MaxDisplaySlider.LongHelp =
"Displaying too many lines can slow down the result list so it is recommended not to set a too high number.
Set this number to 0 if you do not want any limitation and always want all the elements to be displayed.";

//
toMaxPSOFrame.HeaderFrame.Global.Title = "Maximum Pre-highlighted Elements";

toMaxPSOFrame.IconAndOptionsFrame.OptionsFrame.MaxPSOSlider.Title = "Maximum Pre-highlighted Elements";
toMaxPSOFrame.IconAndOptionsFrame.OptionsFrame.MaxPSOSlider.ShortHelp =
"Defines the maximum number of elements that can be pre-highlighted.";
toMaxPSOFrame.IconAndOptionsFrame.OptionsFrame.MaxPSOSlider.LongHelp =
"Pre-highlighting too many elements can slow down the session so it is strongly recommended not to set a too high number.
Set this number to 0 if you do not want any limitation and always want all the elements to be pre-highlighted.";
