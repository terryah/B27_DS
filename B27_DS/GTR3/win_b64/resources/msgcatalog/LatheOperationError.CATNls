// =====================================================================
//
// Erreurs Framework 'CATLatheMach'
// =================================
//
// Deux manieres de formater vos messages : 
//
// a- Specifier en delimitant avec le caractere '\n' :
//       Request      (ce que l'on tentait de faire)
//       Diagnostic   (ce qui est arrive)
//       Advice       (...ce qu'il pourrait faire si possible)
//
//     Exemple :
//       ERR_3000 = "Initializing '/p1' attribute in '/p2'.\nAttribute does not exist.\nContact your local support.";  
//       
//               NOTE : Le parametrage des messages se fait via SetNLSParameter
//
// ou bien
//
// b- Specifier vos messages avec la cle + les 3 facettes Request/Diagnostic/Advice
//
//     Exemple :
//       ERR_3000.Request    "Initializing '/p1' attribute in '/p2'." 
//       ERR_3000.Diagnostic "Attribute does not exist."
//       ERR_3000.Advice     "\nContact your local support."
// 
//               NOTE : Le parametrage de chaque facette se fait par :
//                         Request    : SetNLSRequestParams
//                         Diagnostic : SetNLSDiagnosticParams
//                         Advice     : SetNLSAdviceParams
//
//
//
// NOTE : de 8670 � 8699 = WARNING
// 
// V5R13 
// Creneaux libres erreurs severes
//          8728 a 8791
//          8800 a 8819
//          8823 a infini
// V5R14 
// 10000 19999 ERREURS 
// 20000 29999 WARNINGS
// =====================================================================


//====================================================================================================
// Erreurs Turning Operations
//====================================================================================================
ERR_1.Diagnostic = "Malfunction occurred.";

ERR_2.Diagnostic = "Malfunction occurred.";

//------------------------
// Sequential operation 
//------------------------
ERR_21.Diagnostic = "Potential collision at starting point position.";

ERR_23.Diagnostic = "Impossible to determine the position of the tool.";
ERR_23.Advice     = "\nCheck the selected geometries and the parameters of the tool motions.";

ERR_27.Diagnostic = "Tool position cannot be determined with 'Actual' limit.
The tool has been positioned at the nearest extremity of the profile.";

ERR_196.Request    = "Computing tool motion n� /p1."; 
ERR_196.Diagnostic = "Tool motion definition leads to an unchanged tool position.";
ERR_196.Advice     = "\nCheck if the tool motion is correctly defined.";

ERR_199.Diagnostic = "Malfunction occurred.";

ERR_1000.Diagnostic = "Malfunction occurred.";
ERR_1001.Advice    = "Tool path computation with limit elements is not compatible for tool of two different radius.";
ERR_1002.Diagnostic = "Malfunction occurred when valuating the command '/p1'.";

ERR_1003.Request    = "Computing tool path."; 
ERR_1003.Diagnostic = "There is just a list of NC Commands in this operation.";
ERR_1003.Advice     = "\nSome NC Code (Point, Feed and Speed) might be wrong.\nInsert at least one point or use the NC Command Activity.";

ERR_1004.Request    = ""; 
ERR_1004.Diagnostic = "Spindle rotation direction selected by user is not compatible with tool parameters.";
ERR_1004.Advice     = "\nModify tool parameters or reverse spindle rotation direction.";


//====================================================================================================
// Erreurs Turning Operations
//====================================================================================================

ERR_8600.Diagnostic = "Part geometry projected on turning plane gives an inconsistent profile.";
ERR_8600.Advice     = "\nCheck if the part geometry is consistent with turning plane.";
 
ERR_8601.Diagnostic = "Stock geometry projected on turning plane gives an inconsistent profile.";
ERR_8601.Advice     = "\nCheck if the stock geometry is consistent with turning plane.";

ERR_8647.Diagnostic = "Operation and tooling parameters are incompatible with the part.";
ERR_8647.Advice     = "\nCheck operation and tooling parameters.";

ERR_8648.Diagnostic = "Operation and tooling parameters are incompatible with the stock.";
ERR_8648.Advice     = "\nCheck operation and tooling parameters.";

ERR_8649.Diagnostic = "Selected geometry and operation parameters lead to an unmachinable area.";
ERR_8649.Advice     = "\nCheck the following items :
\na- Stock and part compatibility. 
\nb- Main parameters influencing the relimitation of the area to machine :
\t- 'Orientation' and 'Location' on operation.
\t- 'Trailing' and 'Leading' angles on operation added to those defined on the insert-holder.
\t- 'Setup angle' on tooling Assembly.";
 
ERR_8650.Diagnostic = "Collision has been detected in lift-off motion.";
ERR_8650.Advice     = "\nCheck lift-off parameters.";

ERR_8651.Diagnostic = "Collision has been detected in lead-in motion.";
ERR_8651.Advice     = "\nCheck lead-in parameters.";

ERR_8652.Diagnostic = "Axial and radial depth of cut values cannot be both equal to zero.";
ERR_8652.Advice     = "\nModify one of these values";

ERR_8653.Diagnostic = "Part profile becomes inconsistent when offsets are applied.";
ERR_8653.Advice     = "\nAssign a smaller part offset.";

ERR_8654.Request    = "Checking start element."; 
ERR_8654.Diagnostic = "Position on part profile cannot be determined if operation is finishing operation.
Position on stock profile cannot be determined if operation is rough turning operation";
ERR_8654.Advice     = "\nSelect another start element.";

ERR_8655.Request    = "Checking end element."; 
ERR_8655.Diagnostic = "Position on part profile cannot be determined.";
ERR_8655.Advice     = "\nSelect another end element";

ERR_8656.Request    = "Checking machining direction."; 
ERR_8656.Diagnostic = "Start or end element is not compatible with the defined machining direction.";
ERR_8656.Advice     = "\nModify one of these parameters.";

ERR_8657.Diagnostic = "Insert is incompatible with the part profile to machine.";
ERR_8657.Advice     = "\nCheck operation and insert parameters.";

ERR_8658.Diagnostic = "'Max life time' specified on the insert-holder has been reached. All paths cannot be machined.";
ERR_8658.Advice     = "\nModify the parameters of the operation or the insert-holder.
If 'Start of path' is selected, check that the time of interruption is greater than the duration of the first path.";

ERR_8659.Diagnostic = "Offset on elements machined with the inverted direction is too large.";
ERR_8659.Advice     = "\nModify the offset value.";

ERR_8671.Diagnostic = "'Lead-in radius' is too small. ";
ERR_8671.Advice     = "\n'Lead-in radius' must be greater than the 'Nose radius' of the Insert.";

ERR_8672.Diagnostic = "'Lift-off radius' is too small.";
ERR_8672.Advice     = "\n'Lift-off radius' must be greater than the 'Nose radius' of the Insert.";

ERR_8673.Diagnostic = "'Last lead-in radius' is too small.";
ERR_8673.Advice     = "\n'Last lead-in radius' must be greater than the 'Nose radius' of the Insert.";

ERR_8674.Diagnostic = "'Other lead-in radius' is too small.";
ERR_8674.Advice     = "\n'Other lead-in radius' must be greater than the 'Nose radius' of the Insert.";

ERR_8675.Diagnostic = "Appropriate corrector number not defined on insert-holder.";
ERR_8675.Advice     = "\nDefine the appropriate corrector on the insert-holder with respect to 'Orientation' and 'Location' parameters.";

ERR_8676.Diagnostic = "Collision is detected between the Insert and the Part profile during a rapid motion.";
ERR_8676.Advice     = "\nCheck the operation parameters.";

ERR_8677.Diagnostic = "Collision is detected between the Insert and the Part profile during a lift-off motion.";
ERR_8677.Advice     = "\nCheck lift-off parameters.";

ERR_8678.Diagnostic = "'Change output point' option cannot be taken into account.\nAppropriate correctors are not defined on the insert-holder.";
ERR_8678.Advice     = "\nEither deactivate the option or define suitable correctors on the insert-holder with respect to the tooling orientation.";

ERR_8679.Diagnostic = "Groove bottom width is too small.\nNext flank clearance and overlap are too large compared with the groove bottom width.";
ERR_8679.Advice     = "\nModify the operation parameters or the Insert.";

ERR_8680.Diagnostic = "Last depth of cut is too large. The last passes machine the thread completely.";
ERR_8680.Advice     = "\nModify the operation parameters.";

ERR_8681.Diagnostic = "First section of cut is too large. The first passes machine the thread completely.";
ERR_8681.Advice     = "\nModify the operation parameters.";

ERR_8682.Diagnostic = "Clearance is too large. The machining direction on one element cannot be inverted.";
ERR_8682.Advice     = "\nModify the operation parameters.";

ERR_8683.Diagnostic = "Overlap is too large. Overlap is larger than one of the elements machined in the inverted direction.";
ERR_8683.Advice     = "\nModify the operation parameters.";

ERR_8684.Diagnostic = "Area to machine is reduced due to 'Max boring depth' value defined on insert-holder.";
ERR_8684.Advice     = "\nThis value can be ignored using the 'Insert-holder constraints' option at operation level.";

ERR_8685.Diagnostic = "Area to machine is reduced due to 'Max cut depth' value defined on insert-holder.";
ERR_8685.Advice     = "\nThis value can be ignored using the 'Insert-holder constraints' option at operation level.";

ERR_8686.Diagnostic = "Specified 'Min machining radius' value does not affect area to machine.";

ERR_8687.Diagnostic = "Specified 'Max Machining Radius' value does not affect area to machine.";

ERR_8688.Diagnostic = "'Axial Limit for Chuck Jaws' is not compatible with the Part to machine.";
ERR_8688.Advice     = "\nCheck the parameter value.";

ERR_8689.Diagnostic = "Plunge distances are too large compared with the depth of cut.";

ERR_8690.Diagnostic = "'Max depth of level' is reduced due to 'Max cut depth' specified on the insert-holder.";
ERR_8690.Advice     = "\nThis value can be ignored using the 'Insert-holder constraints' option at operation level.";

ERR_8691.Diagnostic = "'Insert width' is smaller or equal to the 'Max depth of cut'.\n'Max depth of cut' is reduced to generate a second pass to completely machine the groove.";
ERR_8691.Advice     = "If you do not want the second pass, you must set the 'Max depth of cut' greater than 'Insert width'.";

ERR_8692.Diagnostic = "'Max depth of cut' is greater than twice 'Insert width'. Material remains after groove machining.";
ERR_8692.Advice     = "If you want to completely machine the groove, you can either reduce the 'Max depth of cut' or choose an insert with a greater 'Insert width'.";

ERR_8693.Diagnostic = "'Max cut width' value on the insert-holder is less than 'Max depth of cut'.\n'Max depth of cut' is reduced with respect to 'Max cut width'.";

ERR_8694.Diagnostic = "Inverse lift-off radius is too small.";
ERR_8694.Advice     = "\nLocal inverse lift-off radius must be greater than the 'Nose radius' of the tooling.";
 
ERR_8695.Diagnostic = "Axial and radial cutting depths are not compatible with the part to machine.";
ERR_8695.Advice     = "\nModify the parameter values.";

ERR_8696.Diagnostic = "Malfunction occurred when processing start element information.";

ERR_8697.Diagnostic = "Malfunction occurred when processing end element information.";

ERR_8698.Diagnostic = "CUTCOM order cannot be output automatically before the lead-in.";
ERR_8698.Advice     = "\nInsert the CUTCOM order manually in the approach macro";

ERR_8699.Diagnostic = "'Tool compensation' is not activated because the specified machine is not a lathe machine.";
ERR_8699.Advice     = "\nSelect a lathe machine.";

ERR_8700.Diagnostic = "Selected 'Tool compensation' not defined on insert-holder '/p1'.\nBy default, the Type 'P9' is taken into account.";
ERR_8700.Advice     = "\nIf needed, select another 'Tool compensation' type defined on the insert-holder.";

ERR_8701.Diagnostic = "Profile is not machinable by the operation.";
ERR_8701.Advice     = "\nSplit the profile and machine the new profiles combining 'Ramp Recess Turning' and 'Ramp Rough Turning' operations.";

ERR_INTERRUPT_TIM_0.Diagnostic = "'Interruption Time' must be greater than 0.";
ERR_INTERRUPT_TIM_0.Advice     = "\nIf 'On time' option is desired, specify a correct value.";

ERR_COMPUTED_STOCK_NOT_UP_TO_DATE.Diagnostic = "Input stock previously computed for operation '/p1' is no more up to date.";
ERR_COMPUTED_STOCK_NOT_UP_TO_DATE.Advice     = "\nTo get the updated tool path, any manual stock selection should be removed.
You can either update manually the stock or it will be automatically computed and selected during the tool path computation.";

ERR_30003.Diagnostic = "Malfunction occurred when processing end element information in Groove Roughing.
\nGroove geometry has no flank for the Set First plunge or Next plunge position Strategy Parameter.";
ERR_30003.Advice     = "\nGroove machined with end element must have a flank. Define a flank for
\nthe above mentioned Strategy parameter.";

//====================================================================================================
// erreurs preparation geometrie 
//====================================================================================================

ERR_8703.Diagnostic = "Profile of the groove is incompatible with the grooving tool.";
ERR_8703.Advice     = "\nSelect an appropriate tooling.";

ERR_8704.Diagnostic = "Impossible to compute resulting depth of cut.";
ERR_8704.Advice     = "\nCheck that geometry orientation is compatible with the operation.";

ERR_8705.Diagnostic = "Input Stock cannot be computed.";
ERR_8705.Advice     = "\nManually select another Stock Profile.";

ERR_8706.Diagnostic = "Stock Profile continuity is not respected.";
ERR_8706.Advice     = "\nManually select another Stock Profile.";

ERR_8707.Diagnostic = "Stock Profile orientation is inconsistent.";
ERR_8707.Advice     = "\nManually select another Stock Profile.";

ERR_8708.Diagnostic = "Part Profile cannot be correctly processed.";
ERR_8708.Advice     = "\nCheck Part Profile and global & local offsets on Part.";

ERR_8709.Diagnostic = "Machined Area extension leads to an inconsistency.";
ERR_8709.Advice     = "\nCheck relative position of Part Profile and Stock Profile .";

ERR_8710.Diagnostic = "Invalid area to machine.";

ERR_8711.Diagnostic = "No pass could be computed for this Ramping.";
ERR_8711.Advice     = "\nCheck relative position of Stock and Part and Depth of Cut.";

ERR_8712.Diagnostic = "No pass could be computed for this Grooving.";
ERR_8712.Advice     = "\nCheck relative position of Stock and Part and Depth of Cut.";

ERR_8713.Diagnostic = "No pass could be computed for this Zig-Zag Recessing.";
ERR_8713.Advice     = "\nCheck relative position of Stock and Part and Depth of Cut.";

ERR_8714.Diagnostic = "No pass could be computed for this Ramping Recessing.";
ERR_8714.Advice     = "\nCheck relative position of Stock and Part and Depth of Cut.";

ERR_8715.Diagnostic = "No resulting profile to machine.";
ERR_8715.Advice     = "\n1- Check if selected geometry lies on turning plane.
\n2- Check the local and global offsets applied on Part if any.
\n3-  Check the global offset applied on Stock if any.
\n4-  Check the tooling parameters.";

ERR_8716.Diagnostic = "External mode is not compatible with geometry.";
ERR_8716.Advice     = "\nSelect a geometry that is consistent with external mode.";

ERR_8717.Diagnostic = "Internal mode is not compatible with geometry.";
ERR_8717.Advice     = "\nSelect a geometry that is consistent with internal mode.";

ERR_8718.Diagnostic = "Frontal mode is not compatible with geometry.";
ERR_8718.Advice     = "\nSelect a geometry that is consistent with frontal mode.";

ERR_8719.Diagnostic = "'Other' orientation mode is not compatible with geometry.";
ERR_8719.Advice     = "\nSelect a mode compatible with the profile.";

ERR_8720.Diagnostic = "Impossible to process the projection of the stock on turning plane.";
ERR_8720.Advice     = "\nCheck consistency between stock and turning plane.";


//====================================================================================================
// erreurs op�ration sequential
//====================================================================================================

ERR_8620.Diagnostic = "Start point is on the contour and cannot be computed.";
ERR_8620.Advice     = "\nSelect another element.";

ERR_8623.Diagnostic = "Impossible to determine the position of the tool.";
ERR_8623.Advice     = "\nCheck the validity of the first tool motion with a 'NC' status (Not Computable).";

ERR_8624.Diagnostic = "Start element problem.";
ERR_8624.Advice     = "\nCheck the selected geometry and parameters of the tool motions.";

ERR_8625.Diagnostic = "End element loop.";
ERR_8625.Advice     = "\nCheck the selected geometry and parameters of the tool motions.";

ERR_8727.Diagnostic = "Geometry computation of the tool motion has failed.";

ERR_8792.Diagnostic = "Conflict between the insert and the side of the geometry to machine.";
ERR_8792.Advice     = "\nCheck compatibility between the previous tool positioning and the geometry to machine.";

ERR_8793.Diagnostic = "Collision detected between the left edge of the insert and the geometry.";

ERR_8794.Diagnostic = "Collision detected between the right edge of the insert and the geometry.";

ERR_8795.Diagnostic = "Insert and geometry conflict in collision avoidance mode.";

ERR_8796.Diagnostic = "Tool position unchanged.";

ERR_8797.Diagnostic = "Tool position cannot be reached.";

ERR_8798.Diagnostic = "No current drive has been defined on previous motions.";

ERR_8799.Diagnostic = "Geometry to define the tool motion is not completely specified.";
ERR_8799.Advice     = "\nCheck the geometry of the current tool motion.";

ERR_8820.Diagnostic = "Incompatible 'Extended Check Limit' and 'Collision avoidance' mode.  
It leads to a conflict on the left edge of the insert.";
ERR_8820.Advice     = "\nModify one of the parameters.";

ERR_8821.Diagnostic = "Incomptabible 'Extended Check Limit' and 'Collision avoidance' mode.
It leads to a conflict on the right edge of the insert.";
ERR_8821.Advice     = "\nModify one of the parameters.";

ERR_8822.Diagnostic = "'Collision avoidance' or 'Check positioning' mode lead to an undetermined position.";
ERR_8822.Advice     = "\nModify one of the parameters.";

//====================================================================================================
// warning sortie profil 8830 a 8834
// erreurs infrastructure nc
//====================================================================================================
ERR_8830.Request    = "'Output profile' option not taken into account for this operation."; 
ERR_8830.Diagnostic = "\nOption is available for Profile Finish Turning and Rough Turning Face/Longitudinal mode.";
ERR_8830.Advice     = "\nDeactivate Output profile option.";

ERR_8831.Request    = "Output profile not compatible with chamfer or round."; 
ERR_8831.Diagnostic = "\nOutput profile cannot be processed with chamfer or round.";
ERR_8831.Advice     = "\nDeactivate chamfer or round.";

ERR_8832.Request    = "Recess in Roughing is not machined with 'Output profile' option."; 
ERR_8832.Diagnostic = "\nRecess option is ignored because 'Output profile' is performed.";
ERR_8832.Advice     = "\nDeactivate Recess machining.";

ERR_8833.Request    = "End Element in Roughing is ignored with Output profile."; 
ERR_8833.Diagnostic = "\nEnd Element is ignored because Output profile is performed.";
ERR_8833.Advice     = "\nSelect None option for the End element in Output Profile.";

ERR_8834.Request    = "Interrupt in Roughing is ignored with Output profile."; 
ERR_8834.Diagnostic = "\nInterrupt is ignored because Output profile is performed.";
ERR_8834.Advice     = "\nSelect None option for Interrupt mode in Output Profile.";

ERR_8835.Request    = "Output profile elements radius should be larger than insert radius.";
ERR_8835.Diagnostic = "\nOne of the output profile element radius is smaller than insert radius.";
ERR_8835.Advice     = "\nModify profile elements/insert radius for better results.";

ERR_8836.Request    = "Output profile not compatible with Chamfer/LocalInvert.";
ERR_8836.Diagnostic = "\nOutput profile cannot be processed with Chamfer/LocalInvert.";
ERR_8836.Advice     = "\nDeactivate or set to none options Chamfer/LocalInvert.";

ERR_9701.Diagnostic = "Malfunction occurred when reading operation parameters.";

ERR_9702.Diagnostic = "Malfunction occurred when reading the geometry to machine.";

ERR_9703.Diagnostic = "Malfunction occurred when reading the parameters of the tooling.";

ERR_9710.Diagnostic = "Malfunction occurred when processing the contour to machine.";

ERR_9735.Diagnostic = "Not enough memory for computation or malfunction occurred.";

ERR_9736.Diagnostic = "Not enough memory for computation.";

ERR_9737.Diagnostic = "Malfunction occurred.";

//====================================================================================================
// Geometry management
//====================================================================================================
ERR_256.Diagnostic = "Malfunction occurred when processing the area to machine.";

ERR_10000 = "Malfunction occurred when processing the part geometry.";
ERR_10001 = "Malfunction occurred when processing the stock geometry.";
ERR_10002 = "Malfunction occurred when processing the end relimiting element geometry.";
ERR_10003 = "Malfunction occurred when processing the start relimiting element geometry.";
ERR_10004 = "Malfunction occurred when processing the geometry to machine.";

//====================================================================================================
// Warnings about Multi Slide
//====================================================================================================
ERR_20000 = "Tooling configuration and/or Turret axis definition do not allow to machine on the expected side of the spindle.
The toolpath will appear on the opposite side of the spindle.
\nTo machine in expected location, check the following:
a- Tooling configuration such as setup angle and hand style.
    The configuration must be coherent with the geometry to machine.
b- Radial direction definition in Turning Tool Axis System.
    Radial direction should be from the spindle towards the outside of the part.";

ERR_20001 = "Tooling configuration and/or Turret axis definition do not allow to machine on the expected side of the spindle.
The toolpath will appear on the opposite side of the spindle.
\nTo machine in expected location, check the following:
a- Tooling configuration such as setup angle and hand style.
    The configuration must be coherent with the geometry to machine.
b- Radial direction definition in Turning Tool Axis System.
    Radial direction should be from the spindle towards the outside of the part.";

//====================================================================================================
// More Warnings about Grooving
//====================================================================================================
ERR_21001.Request = "First Plunge Position should be Left/Down instead of Right/Up.";
ERR_21001.Diagnostic = "\nThe groove has a flank compatible with Left/Down or Center value for First Plunge Position.
\n For the value Right/Up there is a risk that material along flank is not completely removed.";
ERR_21001.Advice = "\n1 - Check First Plunge Position.
\n2-  If First Plunge Position is correct then deactivate Part Contouring.";

ERR_21002.Request = "First Plunge Position should be Right/Up instead of Left/Down.";
ERR_21002.Diagnostic = "\nThe groove has a flank compatible with Right/Up or Center value for First Plunge Position.
\n For the value Left/Down there is a risk that material along flank is not completely removed.";
ERR_21002.Advice = "\n1 - Check First Plunge Position.
\n2-  If First Plunge Position is correct then deactivate Part Contouring.";

//====================================================================================================
// MultiTurret MultiSpindle at operation level
//====================================================================================================
ERR_TURRET_AXIS_ON_SPINDLE.Request = "Turning Tool Axis System '/p1' and Spindle are incompatible.";
ERR_TURRET_AXIS_ON_SPINDLE.Diagnostic = "The Origin of '/p1' lies on Spindle Axis of the Part Axis System.";
ERR_TURRET_AXIS_ON_SPINDLE.Advice     = "\nShift this Origin out of the Spindle Axis.";

ERR_SPINDLE_AXIS_NOT_COLINEAR.Request = "Part Axis System '/p1' is not compatible with Main Part Axis System.";
ERR_SPINDLE_AXIS_NOT_COLINEAR.Diagnostic = "The Axial direction of '/p1' is not colinear to Spindle.";
ERR_SPINDLE_AXIS_NOT_COLINEAR.Advice     = "\nModify Part Axis System.";

ERR_SPINDLE_AXIS_NO_SPINDLE.Request = "No Spindle defined on the machining operation '/p1'; Main spindle is used.";
ERR_SPINDLE_AXIS_NO_SPINDLE.Diagnostic = "No Spindle was defined on the machining operation '/p1'. The main spindle is used by default.";
ERR_SPINDLE_AXIS_NO_SPINDLE.Advice     = "\nChoose a spindle in the list of available spindles for this machining operation.";


ERR_NO_SPINDLE_SELECTED.Diagnostic = "Replay is not possible because no spindle is assigned to operation.";
ERR_NO_SPINDLE_SELECTED.Advice = "You must select a spindle at operation level.";

ERR_NO_TURRET_SELECTED.Diagnostic = "Replay is not possible because no turret is assigned to program.";
ERR_NO_TURRET_SELECTED.Advice = "You must select a turret at program level.";

//====================================================================================================
// Plane management
//====================================================================================================
ERR_PART_NOT_ON_LOCAL_TURNING_PLANE.Diagnostic = "Selected part geometry does not lie on current turning plane.";
ERR_PART_NOT_ON_LOCAL_TURNING_PLANE.Advice = "\nSelect part geometry lying on this plane.";

ERR_30000.Diagnostic = "Selected part geometry does not lie on current turning plane.";
ERR_30000.Advice = "\nSelect part geometry lying on this plane.";

ERR_STOCK_NOT_ON_LOCAL_TURNING_PLANE.Diagnostic = "Selected stock geometry does not lie on current turning plane.";
ERR_STOCK_NOT_ON_LOCAL_TURNING_PLANE.Advice = "\nSelect stock geometry lying on this plane.";

ERR_30001.Diagnostic = "Selected stock geometry does not lie on current turning plane.";
ERR_30001.Advice = "\nSelect stock geometry lying on this plane.";
