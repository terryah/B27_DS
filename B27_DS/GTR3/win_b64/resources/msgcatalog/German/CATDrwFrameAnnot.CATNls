//=====================================================================================
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwDimensionFrame
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// DATE        :    26.11.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION:    Tools/Option du Drafting
//-------------------------------------------------------------------------------------


frameAnnotCreation.HeaderFrame.Global.Title="Anmerkungserzeugung";
frameAnnotCreation.HeaderFrame.Global.LongHelp="Definiert Eigenschaften, die zur Erzeugung von Anmerkungen verwendet werden.";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.labelSnappingOn.Title="Den Extrempunkt der Bezugslinie senkrecht zur Referenz erzeugen (Umschalten mit Strg):";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.labelSnappingOn.LongHelp="Erstellt Extrempunkt der Bezugslinie senkrecht zur Referenz. Diese Option kann umgekehrt werden, indem bei der Auswahl der Referenzen die Strg-Taste gedr�ckt wird.";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.TextSnappingOn.Title="Ausrichtungsverkn�pfung zwischen Text und Referenz erzeugen (Umschaltung mit Strg)";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.TextSnappingOn.LongHelp="Aktivieren Sie die Ausrichtungsverkn�pfung, wenn eine Referenz w�hrend der Texterstellung angegeben wird. Diese Option kann umgekehrt werden, indem bei der Auswahl der Referenz die Strg-Taste\ngedr�ckt wird.";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmAnnotCreation.TextLeadSnappingOn.Title="Text";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmAnnotCreation.TextLeadSnappingOn.LongHelp="Erzeugt eine Bezugslinie senkrecht zur Referenz, wenn ein Text erzeugt wird";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmAnnotCreation.GDTSnappingOn.Title="Geometrische Toleranz";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmAnnotCreation.GDTSnappingOn.LongHelp="Geometrische Toleranz.";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.labelRigidPosLink.Title="Starre Positionszuordnung erzeugen:";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.labelRigidPosLink.LongHelp="Diese Einstellungen steuern Positionszuordnungen, die standardm��ig f�r
Anmerkungsk�rper erzeugt werden. Eine Anmerkung mit einer Positionszuordnung muss der Referenz folgen, wenn die Referenz verschoben wird. Es ist weiterhin m�glich, die Anmerkung relativ zum Referenzobjekt zu verschieben, wenn die Positionszuordnung nicht starr ist.
Bei einer starren Positionszuordnung ist keine Verschiebung m�glich.";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmPosLinkCreation.checkTextRigidPosLink.Title="Text";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmPosLinkCreation.checkTextRigidPosLink.LongHelp="Wenn diese Option aktiviert ist, werden standardm��ig starre Positionszuordnungen f�r den Textanmerkungsk�rper erzeugt. Diese
Einstellung hat keine Auswirkung auf Positionszuordnungen von Bezugslinien.";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmPosLinkCreation.checkTableRigidPosLink.Title="Tabelle";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmPosLinkCreation.checkTableRigidPosLink.LongHelp="Wenn diese Option aktiviert ist, werden standardm��ig alle Positionszuordnungen f�r den Tabellenanmerkungsk�rper als starre Zuordnungen erzeugt. Diese
Einstellung hat keine Auswirkung auf Positionszuordnungen von Bezugslinien.";

frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmPosLinkCreation.checkGDTRigidPosLink.Title="Geometrische Toleranz";
frameAnnotCreation.IconAndOptionsFrame.OptionsFrame.frmPosLinkCreation.checkGDTRigidPosLink.LongHelp="Wenn diese Option aktiviert ist, werden standardm��ig alle Positionszuordnungen des Anmerkungsk�rpers f�r geometrische Toleranzen als starre Zuordnungen erzeugt. Diese
Einstellung hat keine Auswirkung auf Positionszuordnungen von Bezugslinien. Dies ist insbesondere f�r geometrische Bema�ungstoleranzen von Nutzen, die f�r Bema�ungstext erzeugt werden.";

frameAnnotMove.HeaderFrame.Global.Title="Bewegen";
frameAnnotMove.HeaderFrame.Global.LongHelp="Bewegen
Definiert, wie Anmerkungen bewegt werden.";

frameAnnotMove.IconAndOptionsFrame.OptionsFrame.AnnotActivSnap.Title="Standardm��ig einrasten (Umschalten mit Umschalttaste)";
frameAnnotMove.IconAndOptionsFrame.OptionsFrame.AnnotActivSnap.LongHelp="Aktiviert beim Bewegen von Anmerkungen das Einrasten. \"Einrasten konfigurieren\" ausw�hlen, um anzugeben, ob Anmerkungen auf dem Gitter und/oder entsprechend der Ausrichtung eingerastet werden sollen. Die \"Umschalttaste\" dr�cken, um diese Option tempor�r zu aktivieren/inaktivieren.";

frameAnnotMove.IconAndOptionsFrame.OptionsFrame.frmConfigSnap.pushAnnotActiveSnap.Title="Einrasten konfigurieren";
frameAnnotMove.IconAndOptionsFrame.OptionsFrame.frmConfigSnap.pushAnnotActiveSnap.LongHelp="Gibt an, ob Anmerkungen auf dem Gitter und/oder entsprechend der Ausrichtung der Referenz eingerastet werden sollen.";


frameDittoInv.HeaderFrame.Global.Title="2D-Komponente";
frameDittoInv.HeaderFrame.Global.LongHelp="Definiert die Optionen, die f�r 2D-Komponenten verwendet werden.";
frameDittoInv.IconAndOptionsFrame.OptionsFrame.DittoInvViewScale.Title="Erzeugen mit einer konstanten Gr��e";
frameDittoInv.IconAndOptionsFrame.OptionsFrame.DittoInvViewScale.LongHelp="Wendet auf das Exemplar einer 2D-Komponente bei der Erzeugung einen Ma�stabsfaktor an, um den Ansichtsma�stab auszugleichen.";
frameDittoInv.IconAndOptionsFrame.OptionsFrame.DittoDrag.Title="Direkte Manipulation verhindern";
frameDittoInv.IconAndOptionsFrame.OptionsFrame.DittoDrag.LongHelp="Verhindert das Bewegen von 2D-Komponenten mit der Maus.";
frameDittoInv.IconAndOptionsFrame.OptionsFrame.DittoScale.Title="Direktes Skalieren verhindern";
frameDittoInv.IconAndOptionsFrame.OptionsFrame.DittoScale.LongHelp="Verhindert das Skalieren von 2D-Komponenten mit der Maus.";


frmBalloon.HeaderFrame.Global.Title="Erzeugung eines Referenzkreises";
frmBalloon.HeaderFrame.Global.LongHelp="Definiert die Optionen, die beim Generieren von Referenzkreisen verwendet werden.";
frmBalloon.IconAndOptionsFrame.OptionsFrame.chkBalAssoc.Title="3D-Assoziativit�t";
frmBalloon.IconAndOptionsFrame.OptionsFrame.chkBalAssoc.LongHelp="Verwendet einen in 3D definierten Parameter zum Erzeugen von Referenzkreisen";
Number="Nummerierung";
Instance="Exemplarname";
Part="Teilenummer";

frmTable.HeaderFrame.Global.Title="Tabelle";
frmTable.HeaderFrame.Global.LongHelp="Definiert die in Tabellen verwendeten Optionen.";
frmTable.IconAndOptionsFrame.OptionsFrame.chkTableResize.Title="Tabelle beim Bearbeiten der Zelle erneut berechnen";
frmTable.IconAndOptionsFrame.OptionsFrame.chkTableResize.LongHelp="Berechnet die Tabelle erneut, wenn eine Zelle bearbeitet wird.";
frmTable.IconAndOptionsFrame.OptionsFrame.chkTableAutoSplit.Title="Tabelle nicht in mehrere Bl�tter aufteilen";
frmTable.IconAndOptionsFrame.OptionsFrame.chkTableAutoSplit.LongHelp="Die Aufteilung wird nur bei St�cklisten, Berichten und Tabellen aus CSV-Dateien ausgef�hrt.";
frmTable.IconAndOptionsFrame.OptionsFrame.tmpFrame.lblTableSplitMargin.Title="Rand:";
frmTable.IconAndOptionsFrame.OptionsFrame.tmpFrame.lblTableSplitMargin.LongHelp="Rand ab der Unterkante des Blattes zur Berechnung der Aufteilung.";

annotSnap.Title="Einrasten von Anmerkungen";
annotSnap.frameAnnotActivSnap.HeaderFrame.Global.Title="Anmerkungen einrasten";
annotSnap.frameAnnotActivSnap.HeaderFrame.Global.LongHelp="Anmerkungen einrasten";
annotSnap.frameAnnotActivSnap.IconAndOptionsFrame.OptionsFrame.frameRadioAnnotActivSnap.frameRadioAnnotActivSnaplocked.radioAnnotSnapOnGrid.Title="Auf dem Gitter";
annotSnap.frameAnnotActivSnap.IconAndOptionsFrame.OptionsFrame.frameRadioAnnotActivSnap.frameRadioAnnotActivSnaplocked.radioAnnotSnapOnGrid.LongHelp="Die Position der Anmerkung rastet auf dem Gitter ein.";
annotSnap.frameAnnotActivSnap.IconAndOptionsFrame.OptionsFrame.frameRadioAnnotActivSnap.frameRadioAnnotActivSnaplocked.radioAnnotSnapOnOrient.Title="Entsprechend der Ausrichtung der Referenz";
annotSnap.frameAnnotActivSnap.IconAndOptionsFrame.OptionsFrame.frameRadioAnnotActivSnap.frameRadioAnnotActivSnaplocked.radioAnnotSnapOnOrient.LongHelp="Einrasten entsprechend der Ausrichtung der Referenz.";
annotSnap.frameAnnotActivSnap.IconAndOptionsFrame.OptionsFrame.frameRadioAnnotActivSnap.frameRadioAnnotActivSnaplocked.radioAnnotSnapBoth.Title="Beide Varianten";
annotSnap.frameAnnotActivSnap.IconAndOptionsFrame.OptionsFrame.frameRadioAnnotActivSnap.frameRadioAnnotActivSnaplocked.radioAnnotSnapBoth.LongHelp="Einrasten entsprechend der Ausrichtung der Referenz und Einrasten der Position auf dem Gitter.";

frmAreaFill.HeaderFrame.Global.Title="Bereichsf�llung";
frmAreaFill.HeaderFrame.Global.LongHelp="Definiert die Optionen, die f�r die Bereichsf�llung verwendet werden.";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.chkAskForDelSupport.Title="L�schen des Geometriest�tzelements verlangen";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.chkAskForDelSupport.LongHelp="L�schen des Geometriest�tzelements verlangen";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.lblAFOptimForGeom.Title="Bereichsf�llungserkennung optimiert f�r Geometrie:";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.lblAFOptimForGeom.LongHelp="Bereichsf�llungserkennung optimiert f�r Geometrie";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.frmAFOptimForGeom.frmAFOptimForGeomlocked.radOptimGenAndGeo.Title="Generiert oder skizziert";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.frmAFOptimForGeom.frmAFOptimForGeomlocked.radOptimGenAndGeo.LongHelp="Generiert oder skizziert";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.frmAFOptimForGeom.frmAFOptimForGeomlocked.radOptimImport.Title="Aus externem Format importiert";
frmAreaFill.IconAndOptionsFrame.OptionsFrame.frmAFOptimForGeom.frmAFOptimForGeomlocked.radOptimImport.LongHelp="Aus externem Format importiert";

