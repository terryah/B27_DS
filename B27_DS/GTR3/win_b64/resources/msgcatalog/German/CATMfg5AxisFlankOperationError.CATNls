// =====================================================================
// Erreurs Flank Contoruing de 3000 a 
//  Le reste est dans CATMfg5AxisOperationError
//
// Essayer de formater vos messages en specifiant (delimiteur = caracteres fin de ligne) :
//       Request      (ce que l'on tentait de faire)
//       Diagnostic   (ce qui est arrive)
//       Advice       (...ce qu'il pourrait faire si possible)
// ou bien
//       Specifier vos messages avec la cle + les 3 facettes :
//           ex: ERR_2002.Request    Reading data. 
//               ERR_2002.Diagnostic Manufacturing software error : Access to a NULL handler.
//               ERR_2002.Advice     Contact your local support.
//
// REMARK:
// No need to type carriage-return characters at the end of your messages
// since the panel automatically handles spaces between messages
//
// =====================================================================

InfoTitle="Herstellungsinformationen";
WarningTitle="Herstellungswarnung";
ErrorTitle="Herstellungsfehler";
GeneralErrorTitle="Fehler";
PanelTitle="Herstellungsfehler";
UpdateError="Interner Fehler bei Aktualisierung";
InternalError="Herstellungssoftwarefehler.\nFehler ist: /p1";
InitError="Fehler bei Initialisierung";
ErrorNotSpecified="Fehler nicht angegeben.\nDie Anwenderunterst�tzung benachrichtigen";



//====================================================================================================
// Erreurs Flank Contouring
//====================================================================================================

ERR_3001="Parameter f�r 5-Achs-Flankenfr�sen OK.";
ERR_3002="\tAxiale Ebenennummer: ";
ERR_3003="\tRadiale Pfadnummer: ";
ERR_3004="\tNummer des Randelements: ";

ERR_3050="Referenzpunkt und/oder Richtung f�r erneuten Start kann nicht automatisch berechnet werden.
Dies kann an einer komplexen Geometrie liegen, f�r die keine zuverl�ssige automatische Berechnung vorhanden ist.
M�glicherweise sind auch mehrere mathematische L�sungen vorhanden (beispielsweise bei Halbzylindern), und 
es gibt keine einfachen Kriterien, um die richtige zu bestimmen.
Es muss manuell ein geeigneter Referenzpunkt und eine geeignete Richtung f�r erneuten Start ausgew�hlt werden.";

ERR_3051="Der manuelle Referenzpunkt ist nicht g�ltig.
Den vorhandenen Punkt entfernen oder einen anderen Punkt ausw�hlen.";

ERR_3060="Probleme beim Festlegen von Makrobewegungsdaten festgestellt. Die Parameter pr�fen.";

ERR_3228="Ung�ltige Randfl�che.
Der als Randfl�che verwendete Geometrietyp ist nicht g�ltig. Bei der Fl�che des Teils und der Randfl�che beispielsweise kann es sich nicht um die gleiche
Einheit handeln.";

ERR_3251="Werkzeugprojektion fehlgeschlagen.
Das Werkzeug konnte nicht auf die Fl�che projiziert werden.
M�glicherweise wurde bei der Fl�che eine zu gro�e �nderung
von Winkeln durchgef�hrt.  Zur Behebung des Problems gegebenenfalls
die Bewegungsoptionen\n(z. B. Bearbeitungstoleranz) �ndern.";

ERR_3314="Ung�ltiges Randelement.
Die ausgew�hlte Geometrie wird nicht unterst�tzt.";

ERR_3315="Ung�ltiges Startelement.
Die ausgew�hlte Geometrie wird nicht unterst�tzt.";

ERR_3316="Ung�ltiges Stoppelement.
Ausgew�hlte Geometrie wird nicht unterst�tzt.";

ERR_3317="Ausgangsrichtung f�r die Bewegung unbekannt.
Es wurde keine Vorw�rtsrichtung angegeben.";

ERR_3318="Ung�ltiger Geometrietyp f�r Randfl�che oder Start-/Endelemente.
F�r ein Start-/Endelement oder ein Randelement sind nur Ebenen
und Teilfl�chen als Geometrietypen zul�ssig.";

ERR_3319="Ung�ltige Fr�serdefinition.
Die bereitgestellten Fr�serparameter definieren keinen g�ltigen Fr�ser.";

ERR_3320="Unbekannte Operation.
Das System wurde aufgefordert, eine
nicht unterst�tzte Operation oder Bedingung auszuf�hren.";

ERR_3321="Richtungskomponenten zu klein.
Die Gr��e des Eingabevektors ist zu klein f�r die Bestimmung seiner Richtung. Bsp.: Ein Vektor mit dem Wert  '0,0,.000001' ist zu klein.";

ERR_3322="Ung�ltiger Geometrietyp f�r Teilefl�che
Eine Teilefl�che kann nur eine Ebene, Teilfl�che, Fl�che oder eine Rsur sein.";

ERR_3323="Falsche Werkzeugachse.
Werte der Werkzeugachsen I, J und K sind zu klein.
Die Gr��e des Eingabevektors ist zu klein f�r die Bestimmung seiner Richtung. Bsp.: Ein Vektor der
Werkzeugachse mit dem Wert '0,0,.000001' ist zu klein.";

ERR_3324="Vorw�rtsrichtung hat sich ge�ndert.
Das System kann die Vorw�rtsrichtung entlang der Randfl�che\nnicht berechnen. H�ufige Ursache: Die Position der Teilefl�che liegt nach einer �nderung parallel zur Randfl�che.";

ERR_3325="Vorw�rtsrichtung hat sich umgekehrt.
Das System kann die Vorw�rtsrichtung entlang der Randfl�che\nnicht berechnen. Ursache: Die Position des Werkzeugs liegt nach einer �nderung
parallel zur Teilefl�che oder senkrecht zur Randfl�che. Dieser Fehler kann auch auftreten, wenn das Werkzeug so bewegt wird, dass von ihm aus die folgende Fl�che\nnicht gesehen werden kann.";

ERR_3328="Werkzeugprojektion fehlgeschlagen.
Beim Versuch, eine in einer Bearbeitungsoperation verwendete Fl�che zu analysieren, ist ein Fehler aufgetreten. M�gliche Ursache: Bei einem oder
mehreren Winkeln der Fl�che wurde eine gro�e Wert�nderung vorgenommen. Wenn es sich um eine Startposition handelt, versuchen, die Position des Referenzpunkts zu �ndern.";

ERR_3329="Falsche Beziehung zwischen Werkzeug und Randfl�che.
Bei der angeforderten Bewegung konnte das System das
Werkzeug der Randfl�che nicht ordnungsgem�� zuordnen.";

ERR_3338="Zuordnung des Werkzeugs zur Fl�che fehlgeschlagen.
Das System konnte die Schneidevorrichtung keiner Fl�che zuordnen.";

ERR_3339="Ung�ltiges Hilfsf�hrungselement.
Mindestens ein F�hrungselement auf dem Rand kann nicht ausgewertet werden.
Sicherstellen, dass jedes ausgew�hlte Hilfsf�hrungselement
auf einen der ausgew�hlten R�nder projiziert werden kann.

Toleranz vergr��ern oder 
die Diskretisierungsparameter bzw. den Parameter Max. Abstand zwischen Schritten verkleinern.";

ERR_3340="Nicht stetige Randelemente.
Eine Richtung f�r den erneuten Start manuell festlegen. Dazu den Kontextbefehl 'Lokale �nderungen' verwenden.";

ERR_3341="Fehler bei der Endposition.
Fehler beim Berechnen der anf�nglichen Bearbeitungsposition.
Das Startelement, die Position und den Offset auf ihm, den ersten Rand und die Teile �berpr�fen.
Weitere �berpr�fungen k�nnen mit folgenden Punkten erfolgen:
- keine Aufma�e auf Elementen, 
- keine Mehrfachwege,
- ein manueller Referenzpunkt,
- eine einfache ebene Teilefl�che,
- ein ebenes Startelement, das senkrecht zum Rand und zum Teil ist, 
- Bedingung Auf oder Tangential.";

ERR_3342="Fehler bei der Endposition.
Fehler beim Verfahren entlang dem ersten Randelement.
Den ersten und den zweiten Rand �berpr�fen (Endelement und Bedingung f�r das Element, wenn es nur einen Rand gibt).
Weitere �berpr�fungen k�nnen mit folgenden Punkten erfolgen:
- keine Aufma�e auf Elementen, 
- keine Mehrfachwege,
- eine manuelle Startrichtung LINKS oder RECHTS,
- eine einfache ebene Teilfl�che,
- ein einziges Randelement verwenden, wobei der aktuelle zweite Rand \nals das Endelement festgelegt ist, 
- Bedingung Auf oder Tangential,
- Hilfsf�hrungselement entfernen und �berpr�fen.
Die lokalen Werte der Randelemente pr�fen und ggf. �ndern.";

ERR_3343="Fehler bei der Endposition.
Fehler beim Verfahren entlang dem zweiten Randelement.
Den zweiten und den dritten Rand �berpr�fen (Endelement und Bedingung f�r das Element, wenn es nur zwei R�nder gibt).
Die Reihenfolge der Randelemente hinsichtlich der Positionen des Start- und des Endelements pr�fen.
�berpr�fen, ob die Kr�mmung und das Aufma� der Verrundung gr��er als der Radius des Werkzeugk�rpers sind.
Weitere �berpr�fungen k�nnen mit folgenden Punkten erfolgen:
- keine Aufma�e auf Elementen, keine Mehrfachwege,
- eine einfache ebene Teilefl�che,
- zwei Randelemente verwenden, wobei der aktuelle dritte Rand \nals das Endelement festgelegt ist, Bedingung Auf oder Tangential.
- ein einziges Randelement verwenden, wobei der aktuelle zweite Rand \nals das Endelement festgelegt ist, Bedingung Auf oder Tangential, damit die vorherige Bewegung entlang dem ersten Rand �berpr�ft werden kann,
- Hilfsf�hrungselement entfernen und �berpr�fen.
Die lokalen Werte der Randelemente pr�fen und ggf. �ndern.
Das Kontrollk�stchen Werkzeugbahn schlie�en pr�fen.";

ERR_3344="Fehler bei der Endposition.
Fehler beim Verfahren entlang einem Randelement.
Dieses und das folgende Randelement �berpr�fen (Endelement und Bedingung f�r das Element, \nwenn sich der Fehler f�r den letzten Rand ergibt).
�berpr�fen, ob die Kr�mmung und das Aufma� der Verrundung gr��er als der Radius des Werkzeugk�rpers sind.
Weitere �berpr�fungen k�nnen mit folgenden Punkten erfolgen:
- keine Aufma�e auf Elementen, keine Mehrfachwege,
- eine einfache ebene Teilefl�che ohne Aufma�,
- weniger Randelemente verwenden, wobei der auf den Fehler folgende Rand \nals das Endelement festgelegt ist, Bedingung Auf oder Tangential.
- weniger Randelemente verwenden, wobei der fehlerhafte Rand nach dem Fehler \nals das Endelement festgelegt ist, Bedingung Auf oder Tangential, um die vorherige Bewegung entlang dem vorherigen Rand zu �berpr�fen,
- Hilfsf�hrungselement entfernen und �berpr�fen.
Die lokalen Werte der Randelemente pr�fen und ggf. �ndern.
Das Kontrollk�stchen Werkzeugbahn schlie�en pr�fen.";

ERR_3363="Geometrievorgang nicht m�glich.
F�r diesen Fehlercode gibt es verschiedene Ursachen:
* Die folgende Fl�che konnte mit der Werkzeugachsenstrategie\nTanto Fan nicht gefunden werden.
* Die Randfl�chennormale wurde parallel zur Werkzeugachse.";

ERR_3372="Ung�ltiger Typ oder ung�ltige Bedingung f�r Begrenzungen.
";

ERR_3455="Berechnung auf Grund von Werkzeugsprung unterbrochen.
W�hrend der Berechnung des Werkzeugpfads wurden unzul�ssige Werkzeugspr�nge festgestellt. Bei Fl�chen mit mehreren Teilen geschieht
dies m�glicherweise h�ufiger. Bitte die Anwenderunterst�tzung benachrichtigen.";

ERR_3456="Ung�ltige Bedingung bei der folgenden Randfl�che.
Beim Feststellen einer Endposition f�r das Werkzeug
in Bezug auf die folgende Randfl�che wurde ein Problem festgestellt. 
Sicherstellen, dass f�r die Anwendung des Werkzeugs 
auf der Fl�che keine ung�ltige Bedingung angefordert wurde.";

ERR_3457="Nicht stetige Randelemente.
Eine Stoppbedingung manuell festlegen. Dazu den Kontextbefehl 'Lokale �nderungen' verwenden.";

ERR_3462="Spezifische Bedingung auf Fl�che.
Scheibenfr�ser und Werkzeugachsenstrategie FANNING erfordern,\ndass sich das Werkzeug AUF der Fl�che befindet.
Es gibt keinen Tangentialpunkt, der weiterhin �ber einen Kontakt zum Randelement verf�gt.
Eine m�gliche Ursache kann eine zu kleine Schnittl�nge sein.";

ERR_3463="Spezifische Bedingung f�r Randfl�che.
Scheibenfr�ser und Werkzeugachsenstrategie FANNING erfordern,\ndass sich das Werkzeug AUF der Fl�che befindet.";

ERR_3474="Vorschubgeschwindigkeit betr�gt null.
Der f�r die Vorschubgeschwindigkeit angegebene Wert betr�gt null. Der Wert f�r die Vorschubgeschwindigkeiten muss gr��er null sein.";

ERR_3479="Falsches Rand- oder Start-/Endelement.
Die Werkzeugachsenstrategie TANTO FAN erfordert eine Fl�che,
eine Teilfl�che, RSUR oder eine Ebene als Randelement\nund Start-/Endelement.";

ERR_3506="Undefinierte Vorw�rtsrichtung.
Vorw�rtsrichtung kann nicht berechnet werden.";

ERR_3508="Ung�ltiger Randelementtyp f�r Werkzeugachsenstrategie TANTO oder PARELM.
Unterst�tzte Typen f�r Randelemente sind Fl�che, Teilfl�che,\nRSUR und Ebene.";

ERR_3553="Kontakth�he ist zu gering
Die entlang der Randfl�che verwendete Kontakth�he ist zu gering.";

ERR_3554="Werkzeugprojektion auf Randfl�che fehlgeschlagen.
Das Werkzeug konnte nicht auf die Randfl�che projiziert werden.
M�glicherweise befindet sich die Referenzposition
nicht vor der Randfl�che. Das Problem sollte durch Verschieben
des Referenzpunkts in die N�he der Randfl�che behoben werden k�nnen.";

ERR_3559="Au�erhalb des Toleranzbereichs.
Werkzeugbewegung Nummer \p1 au�erhalb des Toleranzbereichs.";

ERR_3666="Begrenzungsdiskretisierung fehlgeschlagen.
Bei der Verarbeitung der Begrenzung der Fl�che des Teils, des Rand-, Start- oder Endelements wurde ein Problem festgestellt. \nBitte die Anwenderunterst�tzung benachrichtigen.";

ERR_3667="Zu extreme Bedingungen
Bedingungen sind zu extrem f�r den COMBIN-Modus der Werkzeugachse. Das Werkzeug ist bei der FAN-Operation vom Startpunkt weg oder in Richtung des folgenden
Elements fehlgeschlagen. Dieser Fehler tritt in der Regel auf, wenn der F�cherungsabstand zu gering ist. Das Problem kann behoben werden, indem der Entfernungs-/Ann�herungsabstand so weit
vergr��ert wird, dass er gr��er ist als der \nWerkzeugdurchmesser.";

ERR_3671="Speicherzuordnungsproblem.
Speicher kann nicht zugeordnet werden.";

ERR_3674="Eckenbearbeitung fehlgeschlagen.
Den Eckenradius pr�fen.";

ERR_3710="Fehler bei innerem Modell
Inkoh�renz bei innerem Modell festgestellt.";

ERR_3717="Offset entlang Achse nicht m�glich.
Ursache: Der Winkel zwischen der Werkzeugachse\nund der Normalen des Teils ist gr��er als 90 Grad.";

ERR_3718="Offset entlang Achse nicht m�glich.
Ursache: Die Normale des Teils wurde\nbei der Berechnung nicht korrekt ausgewertet.";

ERR_3723="Interner Kommunikationsfehler.
Es ist ein Fehler aufgetreten. Dieser wurde jedoch nicht richtig auf den 5-Achsen-Monitor �bertragen.\nDie Anwenderunterst�tzung benachrichtigen.";

ERR_3724="Keine automatische Richtungsfestlegung m�glich.
Die Software konnte die\nLinks/Rechts-Richtung f�r das erste Randelement nicht finden.
M�gliche Ursache sind nicht benachbarte Randelemente oder eine komplexe Geometrie.
Auf der Registerkarte Strategie eine manuelle Richtung festlegen.";

ERR_3726="Ung�ltiger Wert f�r Max. Winkel.
Dieser Wert darf weder 0 noch ein Vielfaches von 180 sein.\nDen Wert von Max. Winkel �ndern.";

ERR_3730="Inkonsistente Werkzeugbewegung.
Teile der Geometrie k�nnen nicht ausgewertet werden. Erneut ausw�hlen.";

ERR_3731="Inkonsistente Werkzeugbewegung.
Mindestens ein Teileelement kann nicht ausgewertet werden.
Zur�cksetzen und Teileelemente\nerneut ausw�hlen.";

ERR_3732="Inkonsistente Werkzeugbewegung.
Das Randelement kann nicht ausgewertet werden. Erneut ausw�hlen.";

ERR_3741="Nicht definierte Teilekurve.
Als Teil f�r Rand '/p1' wurde keine Kurve bereitgestellt.";

ERR_3742="Nicht definierte Teilekurve.
Diese Kurve ist keine Begrenzung der ausgew�hlten Randfl�chen.";

ERR_3743="Ein negatives Teileaufma� wird verwendet und dessen absoluter Wert ist gr��er als der Radius des Werkzeugk�rpers. Dieser Wertetyp f�r das Teileaufma� ist nicht kompatibel mit der Kollisionspr�fung hinsichtlich Hindernissen. Soll eine Kollisionspr�fung hinsichtlich Hinderniselementen erfolgen, muss f�r die Teile ein kleineres negatives Aufma� verwendet werden. Anderenfalls muss die Option Hinderniselemente deaktiviert werden.";

ERR_3744="Ung�ltige Randgeometrie.";

ERR_3745.Request="Nahezu ein Rand ist f�r die Bearbeitung mit einer 4-Achsen-Ebene erforderlich";
ERR_3745.Diagnostic="Wenn eine 4-Achsen-Ebene und ein Neigungs-/Steigungswinkel definiert wurden, wird die 4-Achsen-Ebene nicht ber�cksichtigt.";
ERR_3745.Advice="Neigungs-/Steigungswinkel oder 4-Achsen-Ebene ausw�hlen.";
