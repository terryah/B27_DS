//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGen
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet Generative
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   13.01.1999  Nouvelles options gendim
//      03           fgx   21.01.1999  Nouvelles options elements generes
//      04           fgx   28.01.1999  Ajout de GenDimFilter et GenDimAnal
//      05           fgx   05.02.1999  Ajout de GenDimAutoTrans
//      06           fgx   14.04.1999  Modif de GenGeo
//	  08		   lgk   26.06.2002  Revision
//	  09 	    	   lgk   15.10.02    Revision
//=====================================================================================

Title="Erzeugung";

frameGenDim.HeaderFrame.Global.Title="Bema�ungserzeugung ";
frameGenDim.HeaderFrame.Global.LongHelp=
"Bema�ungserzeugung
Legt fest, ob Bema�ungen aus den Bedingungen eines 3D-Teils generiert werden.
Die folgenden Bedingungen k�nnen generiert werden: Abstand, L�nge, Winkel, Radius und
Durchmesser.";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDim.Title="Bei Aktualisierung des Blatts Bema�ungen generieren";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFilter.Title="Filter vor Generierung";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFilter.LongHelp=
"Filtert vor der Generierung
Zeigt vor der Generierung das Dialogfenster \"Bema�ungserzeugungsfilter\" an.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoPos.Title="Automatische Positionierung nach Generierung";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoPos.LongHelp=
"Automatische Positionierung nach Generierung
Positioniert Bema�ungen nach der Generierung automatisch.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoTrans.Title="Automatische �bertragung zwischen Ansichten zulassen";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoTrans.LongHelp=
"Automatische �bertragung zwischen Ansichten zulassen
�bertr�gt beim erneuten Generieren Bema�ungen automatisch in die beste Ansicht.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAnal.Title="Analyse nach Generierung";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAnal.LongHelp=
"Analyse nach der Generierung
Zeigt nach der Generierung das Dialogfenster \"Bema�ungserzeugungsanalyse\" an.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFromPart.Title="Bema�ungen von Teilen generieren, die in Baugruppenansichten enthalten sind";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFromPart.LongHelp=
"Bema�ungen von Teilen generieren, die in Baugruppenansichten enthalten sind
Erzeugt Bema�ungen von Teilen, die in den Baugruppenansichten enthalten sind.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.labelGenDimSemiAuto.Title="Verz�gerungen zwischen Generierungen im schrittweisen Modus: ";
frameGenDim.IconAndOptionsFrame.OptionsFrame.labelGenDimSemiAuto.LongHelp=
"Verz�gerungen zwischen Generierungen im schrittweisen Modus
Gibt beim Generieren von Bema�ungen im schrittweisen Modus die Verz�gerung zwischen den einzelnen Bema�ungserzeugungen an.";



frameGenBal.HeaderFrame.Global.Title="Referenzkreiserzeugung";
frameGenBal.HeaderFrame.Global.LongHelp=
"Definiert die Optionen, die beim Generieren von Referenzkreisen verwendet werden.";
frameGenBal.IconAndOptionsFrame.OptionsFrame.checkGenBalloonByInstance.Title="Erzeugung eines Referenzkreises f�r jedes Produktexemplar";
frameGenBal.IconAndOptionsFrame.OptionsFrame.checkGenBalloonByInstance.LongHelp="Erzeugung eines Referenzkreises f�r jedes Produktexemplar
Erzeugt einen Referenzkreis f�r jedes Produktexemplar.";
