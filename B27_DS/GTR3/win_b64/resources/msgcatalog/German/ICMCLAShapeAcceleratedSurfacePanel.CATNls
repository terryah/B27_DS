DialogBoxTitle="Beschleunigte Fl�che";

//Selection area

FrameSelection.LongHelp=" Auswahl
Wenigstens ein F�hrungselement und eine St�tzfl�che m�ssen als Eingabeelemente ausgew�hlt werden.
Das F�hrungselement muss auf der St�tzfl�che liegen. Falls nicht, wird es in normaler
Richtung auf die St�tze projiziert.
Die beschleunigte Fl�che beginnt an der Offsetkurve des F�hrungselements.";

LabelGuide.Title="F�hrungselement: ";
LabelGuide.LongHelp="Auswahl des F�hrungselements.
G�ltige Eingaben sind 3D-Kurven und Linien vom Typ B�zier,
NURBS und B-Spline sowie Fl�chenkanten.";
LabelGuide.ShortHelp="F�hrungselement ausw�hlen";

LabelSupports.Title="St�tzelemente: ";
LabelSupports.LongHelp="W�hlt St�tzfl�chenelemente aus.
G�ltige Eingaben sind Fl�chen vom Typ B�zier, NURBS und B-Spline.";
LabelSupports.ShortHelp="St�tzelemente ausw�hlen";

CheckButtonTrim.LongHelp="Die St�tzfl�chen werden entlang der Kontaktkante getrimmt.
Die beizubehaltende Seite ist durch die X-Richtung des Manipulators dargestellt.
Wenn die F�hrungskurve kleiner ist als die St�tzfl�che, wird automatisch
eine Extrapolation der Tangentenstetigkeit der Trimmbegrenzung vorgenommen.";
CheckButtonTrim.ShortHelp="St�tzelement trimmen";


//Options tab

TabPageOptions.Title="Optionen";

LabelContinuity.Title="Stetigkeit: ";
LabelContinuity.LongHelp="Die Stetigkeit zwischen der erstellten beschleunigten Fl�che
und dem St�tzelement kann von G1 auf G3 gesetzt werden.";
LabelContinuity.ShortHelp="Stetigkeit zu St�tzelement";
SurfContinuity.Button_G1.LongHelp="G1-Stetigkeit zwischen beschleunigter Fl�che und St�tzelement";
SurfContinuity.Button_G1.ShortHelp="G1-Stetigkeit";
SurfContinuity.Button_G2.LongHelp="G2-Stetigkeit zwischen beschleunigter Fl�che und St�tzelement";
SurfContinuity.Button_G2.ShortHelp="G2-Stetigkeit";
SurfContinuity.Button_G3.LongHelp="G3-Stetigkeit zwischen beschleunigter Fl�che und St�tzelement";
SurfContinuity.Button_G3.ShortHelp="G3-Stetigkeit";

LabelLength.LongHelp="Die L�nge ist der Abstand zwischen dem F�hrungselement und der Kante
der beschleunigten Fl�che, die auf der St�tzfl�che liegt.";
LabelLength.ShortHelp="Abstand zwischen F�hrungselement und Kante auf St�tzelement.";
LabelLength.Title="L�nge: ";
CheckButtonIndividualLength.Title="Einzeln";
CheckButtonIndividualLength.LongHelp="Mit Hilfe der Manipulatoren k�nnen Sie an jedem Ende
der beschleunigten Fl�che einen einzelnen L�ngenwert anwenden.";
CheckButtonIndividualLength.ShortHelp="Einzelne L�nge auf jeder Seite";

LabelType.Title="Typ: ";
LabelType.LongHelp="- Abstand -
  Die beschleunigte Fl�che wird definiert durch den Abstand
  zwischen dem F�hrungselement und der Kante der beschleunigten
  Fl�che, die sich in normaler Position zu dem St�tzelement befindet.
- Radius -
  Die beschleunigte Fl�che ist definiert durch den
  Kr�mmungsradius an der Kante der beschleunigten Fl�che, der
  sich in normaler Position zu der St�tzfl�che befindet.";
LabelType.ShortHelp="Typ: Abstand oder Radius";
Combo.Offset="Abstand";
Combo.Radius="Radius";
Combotype.LongHelp="- Abstand -
  Die beschleunigte Fl�che wird definiert durch den Abstand
  zwischen dem F�hrungselement und der Kante der beschleunigten
  Fl�che, die sich in normaler Position zu dem St�tzelement befindet.
- Radius -
  Die beschleunigte Fl�che ist definiert durch den
  Kr�mmungsradius an der Kante der beschleunigten Fl�che, der
  sich in normaler Position zu der St�tzfl�che befindet.";
Combotype.ShortHelp="Typ: Abstand oder Radius";

LabelOffset.Title="Abstand: ";
LabelOffset.LongHelp="Definition des Abstandswerts.";
LabelOffset.ShortHelp="Abstandswert";
CheckButtonIndividualOffset.Title="Einzeln";
CheckButtonIndividualOffset.LongHelp="Erm�glicht das Anwenden eines individuellen Abstandswerts
an jedem Ende der beschleunigten Fl�che.";
CheckButtonIndividualOffset.ShortHelp="Einzelner Abstand auf jeder Seite";

LabelRadius.Title="Radius: ";
LabelRadius.LongHelp="Definition des Radiuswerts.";
LabelRadius.ShortHelp="Radiuswert";
CheckButtonIndividualRadius.Title="Einzeln";
CheckButtonIndividualRadius.LongHelp="Erm�glicht das Anwenden eines individuellen Radiuswerts
an jedem Ende der beschleunigten Fl�che.";
CheckButtonIndividualRadius.ShortHelp="Einzelner Radius auf jeder Seite";

FrameShape.Title="Form";
FrameShape.LongHelp="�ndert die L�nge des Tangentenvektors.
Ein kleiner Wert erzeugt eine flache beschleunigte Fl�che.
Ein gro�er Wert erzeugt eine gepr�gte beschleunigte Fl�che.";
ScrollBarShape.LongHelp="Ein kleiner Wert erzeugt eine flache beschleunigte Fl�che.
Ein gro�er Wert erzeugt eine gepr�gte beschleunigte Fl�che.
Ein guter Anfangswert ist 1,0.
Der Bereich ist 0 <= Wert von Form <= 2.";
ScrollBarShape.ShortHelp="0 <= Wert von \"Form\" <= 2";
SpinnerShape.LongHelp="Ein kleiner Wert erzeugt eine flache beschleunigte Fl�che.
Ein gro�er Wert erzeugt eine gepr�gte beschleunigte Fl�che.
Ein guter Anfangswert ist 1,0.
Der Bereich ist 0 <= Wert von Form <= 2.";
SpinnerShape.ShortHelp="0 <= Wert von \"Form\" <= 2";

//DomainMode.Title = "Domain Mode has been changed to Force-Single-Domain.
//Domain Mode Use-First-Domain is no longer supported.";
