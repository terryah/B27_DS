//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATPropEditView
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog
// AUTHOR      :    tbe
// DATE        :    14/1/1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to View property
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//		01			ogk		26.01.99	- ajout pour les dressups
//      02          ypr     10/05/00    - ajout pour thread
//		03			ogk		01.09.00	- ajout	viewNameLabel (correction)
//		04			ogk		24.10.00	- nouveau contenu (multi-sel)
//		05			lgk		26.06.02	Revision
//		06			lgk		15.10.02	Revision
//		07			lgk		17.12.02	Revision
//------------------------------------------------------------------------------


// Global Parameters 
// =================
globalParamsFrameTitle.Title="Ma�stab und Ausrichtung";
globalParamsFrame.Title="Globale Parameter";
globalParamsFrame.LongHelp=
"Parameter
�ndert die Ansichtsparameter: \"Rotation\" und \"Ma�stab\"."; 


// Global Parameters / Scale
// =========================
globalParamsFrame.scaleEditors.scaleLabel.Title="     Ma�stab:";
globalParamsFrame.scaleEditors.scaleLabel.Help="�ndert die Ansicht durch einen positiven Wert";
globalParamsFrame.scaleEditors.scaleLabel.ShortHelp="Ma�stab der Ansicht";
globalParamsFrame.scaleEditors.scaleLabel.LongHelp="Ma�stab der Ansicht
�ndert die Ansicht durch einen positiven Wert.";
//
globalParamsFrame.scaleEditors.scaleSepLabel.Title=":";



// Global Parameters / Angle
// =========================
globalParamsFrame.angleLabel.Title="Winkel:";
globalParamsFrame.angleLabel.Help="Ordnet der ausgew�hlten Ansicht einen \"Rotations\"winkelwert zu";
globalParamsFrame.angleLabel.ShortHelp="Blickwinkel";
globalParamsFrame.angleLabel.LongHelp="Blickwinkel
Ordnet der/den ausgew�hlten Ansicht(en) einen \"Rotationswinkelwert\" zu. Standardm��ig wird dieser Wert beim Verwenden der aktuellen Sitzungseinheit
ausgedr�ckt. Beim Eingeben des Werts muss die Einheit nicht angegeben werden. Die Einheit kann jedoch, falls erforderlich. vor�bergehend ge�ndert werden (d. h.  \"180 Grad\" anstelle von
\"3.14\" eingeben, wenn die aktuelle Sitzungseinheit \"Bogenma�\" ist).";
//
globalParamsFrame.angleEdt.Title="Winkel:";
globalParamsFrame.angleEdt.Help="Ordnet der ausgew�hlten Ansicht einen \"Rotations\"winkelwert zu";
globalParamsFrame.angleEdt.ShortHelp="Blickwinkel";
globalParamsFrame.angleEdt.LongHelp="Blickwinkel
Ordnet der/den ausgew�hlten Ansicht(en) einen \"Rotationswinkelwert\" zu. Standardm��ig wird dieser Wert beim Verwenden der aktuellen Sitzungseinheit
ausgedr�ckt. Beim Eingeben des Werts muss die Einheit nicht angegeben werden. Die Einheit kann jedoch, falls erforderlich. vor�bergehend ge�ndert werden (d. h.  \"180 Grad\" anstelle von
\"3.14\" eingeben, wenn die aktuelle Sitzungseinheit \"Bogenma�\" ist).";



// Name Parameters 
// ===============
nameParamsFrameTitle.Title="Name anzeigen";
nameParamsFrame.Title="Name anzeigen";
nameParamsFrame.LongHelp="Name anzeigen
�ndert Parameter f�r den Namen der ausgew�hlten Ansicht(en).";


// Name Parameters / Prefix
// ========================
nameParamsFrame.prefixLabel.Title="Pr�fix";
nameParamsFrame.prefixLabel.Help="Ordnet dem Namen der ausgew�hlten Ansicht(en) ein Pr�fix zu";
nameParamsFrame.prefixLabel.ShortHelp="Pr�fix f�r Namen";
nameParamsFrame.prefixLabel.LongHelp="Pr�fix f�r Namen
Ordnet dem Namen der ausgew�hlten Ansicht(en) ein Pr�fix zu.";


// Name Parameters / Ident
// =======================
nameParamsFrame.identLabel.Title="ID";
nameParamsFrame.identLabel.Help="Ordnet dem Namen der ausgew�hlten Ansicht(en) eine \"Kennung\" zu";
nameParamsFrame.identLabel.ShortHelp="Namenskennung";
nameParamsFrame.identLabel.LongHelp="Namenskennung
Ordnet dem Namen der ausgew�hlten Ansicht(en) eine \"Kennung\" zu.";


// Name Parameters / Suffix
// ========================
nameParamsFrame.suffixLabel.Title="Suffix";
nameParamsFrame.suffixLabel.Help="Ordnet dem Namen der ausgew�hlten Ansicht(en) einen \"Suffix\" zu";
nameParamsFrame.suffixLabel.ShortHelp="Suffix f�r Namen";
nameParamsFrame.suffixLabel.LongHelp="Suffix f�r Namen
Ordnet dem Namen der ausgew�hlten Ansicht(en) einen \"Suffix\" zu.";


// Name Parameters / Name Editor
// =============================
nameParamsFrame.nameCkeEditorFrame.Title="Namenseditor mit Formel:";
nameParamsFrame.nameCkeEditorFrame.Name.EnglobingFrame.IntermediateFrame.Label.Title=" ";


// Detail Name 
// ===========
detailParamsFrame.Title="2D-Komponenten";
detailParamsFrame.ShortHelp="2D-Komponenten";


// Detail Name / Editor
// ===========
detailviewTitle.Title="2D-Komponenten";
detailParamsFrame.detailLabel.Title=" ";
detailParamsFrame.detailLabel.Help="Ordnet der oder den ausgew�hlten Detailansicht(en) einen Namen zu";
detailParamsFrame.detailLabel.ShortHelp="Detailansicht";
detailParamsFrame.detailLabel.LongHelp="Detailansicht
Ordnet der oder den ausgew�hlten Detailansicht(en) einen Namen zu";

// Dressup
// =======
dressupTitle.Title="Aufbereiten";
generativeParamsFrame.Title="Aufbereiten";
generativeParamsFrame.LongHelp=
"Aufbereitung
F�gt der oder den ausgew�hlten abgeleiteten Ansicht(en) Elemente hinzu."; 



// Fillets
// =======
filletFrame.Title="Verrundungen";
filletFrame.LongHelp=
" "; 

// Dressup / Hidden Lines 
// ======================
generativeParamsFrame.hiddenLinesChkBtn.Title="Verdeckte Linien";
generativeParamsFrame.hiddenLinesChkBtn.LongHelp="Verdeckte Linien
Definiert, ob \"verdeckte Linien\" in der ausgew�hlten Ansicht angezeigt werden sollen.";



// Dressup / Axis
// ==============
generativeParamsFrame.axisChkBtn.Title="Achse";
generativeParamsFrame.axisChkBtn.LongHelp="Achse
Definiert, ob \"Achsen\" in der/den ausgew�hlten Ansicht(en) angezeigt werden sollen.";


// Dressup / Center Line
// ======================
generativeParamsFrame.centerLinesChkBtn.Title="Mittellinie";
generativeParamsFrame.centerLinesChkBtn.LongHelp="Mittellinie
Definiert, ob \"Mittellinien\" in der ausgew�hlten Ansicht angezeigt werden sollen.";



// Dressup / Thread
// ================
generativeParamsFrame.threadChkBtn.Title="Gewinde";
//npq: 18/10/2005 IR 0513559 LongHelp added
generativeParamsFrame.threadChkBtn.LongHelp="Gewinde
Definiert, ob \"Gewinde\" in der/den ausgew�hlten Ansicht(en) angezeigt werden sollen.";


// Dressup / Fillets
// =========================
filletFrame.boundFilletChkBtn.Title="Verrundungen:";
filletFrame.boundFilletChkBtn.LongHelp="Verrundungen
Definiert den Darstellungsmodus f�r Verrundungen in den ausgew�hlten Ansichten.";

// Dressup / BoundaryFillets
// =========================
filletFrame.boundaryRdBtn.Title="Begrenzungen";
filletFrame.boundaryRdBtn.LongHelp="Begrenzungen
Definiert, ob die Begrenzungen von Verrundungen in den ausgew�hlten Ansichten angezeigt werden sollen.";

// Dressup / Original Edges
// =========================
filletFrame.ficFilletRdBtn.Title="Symbolisch";
filletFrame.ficFilletRdBtn.LongHelp="Symbolisch
Definiert, ob der Extraktionsmodus \"Symbolische Verrundungen\" in den ausgew�hlten Ansichten verwendet werden muss.
";

filletFrame.projoriginaledgeFilletRdBtn.Title="Projizierte urspr�ngliche Kanten";
filletFrame.projoriginaledgeFilletRdBtn.LongHelp="Projizierte urspr�ngliche Kanten
Definiert, ob der Extraktionsmodus \"Projizierte urspr�ngliche Kanten\" in den ausgew�hlten Ansichten verwendet werden muss.
";

filletFrame.originaledgeFilletRdBtn.Title = "Angen�herte urspr�ngliche Kanten";
filletFrame.originaledgeFilletRdBtn.LongHelp="Angen�herte urspr�ngliche Kanten
Definiert, ob der Extraktionsmodus \"Angen�herte urspr�ngliche Kanten\" in den ausgew�hlten Ansichten verwendet werden muss.";

// Dressup / 3D Spec
// ====================
generativeParamsFrame.uncutSpecChkBtn.Title="3D-Spezifikationen";
generativeParamsFrame.uncutSpecChkBtn.LongHelp="3D-Spezifikationen
Definiert, ob \"3D-Spezifikationen\" auf der/den ausgew�hlten Ansicht(en) angezeigt
werden sollen.";

// Dressup / 3D Wireframe
// =======================
generativeParamsFrame.3DWireframeChkBtn.Title="3D-Drahtmodell";
generativeParamsFrame.3DWireframeChkBtn.LongHelp="3D-Drahtmodell
Definiert, ob \"3D-Drahtmodell\" in der bzw. den ausgew�hlten Ansichten angezeigt werden soll.";

// Dressup / 3D Wireframe A PARTIR DE LA R11
// =========================================
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.Title="3D-Drahtmodell";
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.LongHelp="3D-Drahtmodell
Gibt an, ob ein projiziertes 3D-Drahtmodell verdeckt werden kann oder ob es stets in der/den ausgew�hlten Ansicht(en) sichtbar ist.";
3DPointsFrame.3DWireframeFrame.3DWireframeHLRRdBtn.Title="Kann verdeckt werden";
3DPointsFrame.3DWireframeFrame.3DWireframeProjectionRdBtn.Title="Ist stets sichtbar";

// Dressup / 3D Points
// ====================
generativeParamsFrame.3DPointsChkBtn.Title="3D-Punkte";
generativeParamsFrame.3DPointsChkBtn.LongHelp="3D-Punkte
Definiert, ob \"3D-Punkte\" in der/den ausgew�hlten Ansicht(en) angezeigt werden sollen.";
3DPointsFrame.3DPointsChkBtn.Title="3D-Punkte:";
3DPointsFrame.3DPointsChkBtn.LongHelp="3D-Punkte
Definiert, ob \"3D-Punkte\" in der/den ausgew�hlten Ansicht(en) angezeigt werden sollen.";
3DPointsFrame.3DSymbolRdBtn.Title="Vererbung von 3D-Symbolen";
3DPointsFrame.3DSymbolRdBtn.LongHelp="Vererbung von 3D-Symbolen
Zeigt das von 3D geerbte Symbol an";
3DPointsFrame.UsrSymbolRdBtn.Title="Symbol";
3DPointsFrame.UsrSymbolRdBtn.LongHelp="Symbol
Zeigt das ausgew�hlte Symbol an";

// Dressup / 3D Colors
// ===================
generativeParamsFrame.3DColorsChkBtn.Title="3D-Farben";
generativeParamsFrame.3DColorsChkBtn.LongHelp="3D-Farben
Definiert, ob \"3D-Farben\" in der/den ausgew�hlten Ansicht(en) angezeigt werden sollen.";

// Visu & Behaviour 
// ================
visuTitle.Title="Darstellung und Verhalten";
visuBehaviourFrame.Title="Darstellung und Verhalten";
visuBehaviourFrame.ShortHelp="Darstellung und Verhalten der ausgew�hlten Ansicht(en)";
visuBehaviourFrame.Help="Definiert, ob der/die Ansichtsrahmen angezeigt werden soll(en) und ob die Ansicht(en) gesperrt werden sollen";
visuBehaviourFrame.LongHelp="Darstellung und Verhalten der ausgew�hlten Ansicht(en)
Definiert, ob die Ansichtsumrahmung(en) angezeigt und ob die Ansicht(en) gesperrt werden soll(en).";



// Visu & Behaviour  / Frame
// =========================
visuBehaviourFrame.viewFrameChkBtn.Title="Ansichtsumrahmung anzeigen";
visuBehaviourFrame.viewFrameChkBtn.ShortHelp="Eine Ansichtsumrahmung anzeigen";
visuBehaviourFrame.viewFrameChkBtn.Help="Definiert, ob Ansichtsumrahmungen angezeigt werden sollen oder nicht";
visuBehaviourFrame.viewFrameChkBtn.LongHelp="Ansichtsumrahmung anzeigen
Definiert, ob Ansichtsumrahmungen angezeigt werden sollen oder nicht.";

// Visu & Behaviour / visual clipping
//===================================
visuBehaviourFrame.FramingChkBtn.Title="Visuelle Begrenzung";
visuBehaviourFrame.FramingChkBtn.ShortHelp="Visuelle Begrenzung verwenden";
visuBehaviourFrame.FramingChkBtn.Help="Definiert, ob eine visuelle Begrenzung f�r die Ansicht(en) durchgef�hrt werden soll";
visuBehaviourFrame.FramingChkBtn.LongHelp="Visuelle Begrenzung
Definiert, ob eine visuelle Begrenzung f�r die Ansicht(en) durchgef�hrt werden soll";

// Visu & Behaviour  / Lock
// ========================
visuBehaviourFrame.viewLockChkBtn.Title="Ansicht sperren";
visuBehaviourFrame.viewLockChkBtn.ShortHelp="Sperren ausgew�hlter Ansicht(en)";
visuBehaviourFrame.viewLockChkBtn.Help="Definiert, ob die ausgew�hlte Ansicht gesperrt werden soll";
visuBehaviourFrame.viewLockChkBtn.LongHelp="Ausgew�hlte Ansicht(en) sperren
Definiert, ob eine ausgew�hlte Ansicht gesperrt werden soll.";

// Generation Mode 
// ================
generationModeTitle.Title="Generierungsmodus";
generationModeFrame.Title="Generierungsmodus";
generationModeFrame.ShortHelp="Generierungsmodus der ausgew�hlten Ansicht(en)";
generationModeFrame.LongHelp=
"Gibt an, ob die ausgew�hlten Ansichten aus topologischen Daten oder CGR-Daten generiert sind und generiert diese bei Bedarf neu.";


generationModeFrame.DrwGenerationForBox.Title="Generiert nur Teile, die gr��er sind als";
generationModeFrame.DrwGenerationForBox.LongHelp="Generiert nur Teile, die gr��er sind als";
generationModeFrame.spinGenBox.LongHelp="Generiert nur Teile, die gr��er sind als";
generationModeFrame.OcclusionCullingChkBtn.Title="Ausblendung verdeckter Elemente aktivieren";
generationModeFrame.OcclusionCullingChkBtn.LongHelp="Ausblendung verdeckter Elemente aktivieren
Die vollst�ndig verdeckten Objekte werden bei der Berechnung nicht ber�cksichtigt";
generationModeFrame.Combolabel.Title = "Generierungsmodus f�r Ansichten";
generationModeFrame.GenerationModeOption.Title = "Optionen";
GenerationModeOptionPanelTitle= "Optionen f�r den Generierungsmodus";
GenerationModeOptionPanelTitleHRV= "Ann�herungsmodus";

// Messages divers
// ===============

TabTitle="Ansicht";
TabTitle.LongHelp="Definiert die Ansicht.";

ScaleText="     Ma�stab:";
ScaleOutOfRangeTitle="Ma�stab au�erhalb des zul�ssigen Bereichs";
ScaleOutOfRangeText="Der Ma�stab muss ein positiver Wert kleiner als 10000 sein.
Der falsche Ma�stab wurde automatisch auf den n�chsten zul�ssigen Wert gesetzt.";

gvsFrame.buttonResetStyle.Title="Zur�cksetzen auf Darstellungswerte";
gvsFrame.Title="Abgeleiteter Ansichtstyp";
GVSTitle.Title="Abgeleiteter Ansichtstyp";
GVSOverloadHelp="Parameter durch Benutzer �berschrieben";
gvsFrame.buttonRemoveStyle.Title="Darstellung entfernen";

