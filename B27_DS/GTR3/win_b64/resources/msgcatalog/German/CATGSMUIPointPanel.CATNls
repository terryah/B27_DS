//---------------------------------------
// Resource file for CATGSMUIPoint class
// En_US
//---------------------------------------

Title="Punktdefinition";

PointTypeCoord="Koordinaten";
PointTypeOnCrv="Auf Kurve";
PointTypeOnPln="Auf Ebene";
PointTypeOnSur="Auf Fl�che";
PointTypeCtr="Kreis-/Kugel-/Ellipsenmittelpunkt";
PointTypeTgt="Tangente auf Kurve";
PointTypeCKE="Formel";
PointTypeExpl="Explizit";
PointTypeBetween="zwischen";
stOnCurveLength="L�nge";
stOnCurveDisDir="Offset";

frame1.LabList.Title="Punkttyp: ";
frame1.CmbList.LongHelp="Listet die zum Erzeugen von Punkten verf�gbaren Methoden auf.";

//coord sans axe local
OnCoord.LabelCoordX.Title="X = ";
OnCoord.LabelCoordY.Title="Y = ";
OnCoord.LabelCoordZ.Title="Z = ";
OnCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="X";
OnCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Y";
OnCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.Title="Z";
OnCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnCoord.FraSepar.LabelSepar.Title="Referenz ";
OnCoord.LabelOnCoordPoint.Title="Punkt: ";
OnCoord.LabRef.Title="   ";
OnCoord.FrameErase.FraElem2.FraEdt.Edt.LongHelp="Gibt den Referenzpunkt an.\nWird kein Punkt ausgew�hlt, wird der Ursprungspunkt als Referenzpunkt verwendet.";
OnCoord.FrameErase.PushIcon.Title="Entfernen";
OnCoord.FrameErase.PushIcon.ShortHelp="Referenzpunkt entfernen";
OnCoord.FrameErase.PushIcon.LongHelp="Erm�glicht das Entfernen des Referenzpunkts\nund die ersatzweise Verwendung des Standardreferenzpunkts";
OnCoord.CompassLocationV5.Title="Kompassposition";
OnCoord.CompassLocationV5.LongHelp="Erm�glicht die Erzeugung des Punkts an der Kompassposition.";
OnCoord.CompassLocation.Title="Roboterposition";
OnCoord.CompassLocation.LongHelp="Erm�glicht die Erzeugung des Punkts an der Roboterposition.";

//coord avec axe local
OnCoord.FraCoord.LabelCoordX.Title="X = ";
OnCoord.FraCoord.LabelCoordY.Title="Y = ";
OnCoord.FraCoord.LabelCoordZ.Title="Z = ";
OnCoord.FraCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="X";
OnCoord.FraCoord.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnCoord.FraCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Y";
OnCoord.FraCoord.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnCoord.FraCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.Title="Z";
OnCoord.FraCoord.FraLengTang3.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
//OnCoord.ChkAxis.Title="Coordinates in absolute axis system";
//OnCoord.ChkAxis.LongHelp="Allows to view and modify coordinates\nin the absolute axis system.";
OnCoord.FraRef.FrameErase.LabelOnCoordPoint.Title="Punkt: ";
OnCoord.FraRef.LabRef.Title="   ";
OnCoord.FraRef.FrameErase.FraElem2.FraEdt.Edt.LongHelp="Gibt den Referenzpunkt an.\nWird kein Punkt ausgew�hlt, wird der Ursprungspunkt als Referenzpunkt verwendet.";
OnCoord.FraRef.FrameErase.PushIcon.Title="Entfernen";
OnCoord.FraRef.FrameErase.PushIcon.ShortHelp="Referenzpunkt entfernen";
OnCoord.FraRef.FrameErase.PushIcon.LongHelp="Erm�glicht das Entfernen des Referenzpunkts\nund die ersatzweise Verwendung des Standardreferenzpunkts";
// added code 3dplm for Axis System
OnCoord.LabelAxis.Title="Achsensystem: ";
OnCoord.FraAxis.LongHelp="Gibt das lokale Referenzachsensystem f�r\ndie Umwandlung der Koordinaten an. \nIst dieses Achsensystem nicht angegeben, wird das absolute\nAchsensystem als Standardachsensystem verwendet.";

OnCrv.LabelOnCrvCurve.Title="Kurve: ";
OnCrv.FraDistRef1.LabelOnCrvPoint.Title="Punkt: ";
OnCrv.FraSepar0.LabelSepar0.Title="Abstand zu Referenz ";
OnCrv.FraDistRef.FraRadio.Radio1.Title="Abstand auf Kurve";
OnCrv.FraDistRef.FraRadio.Radio2.Title="Verh�ltnis der Kurvenl�nge";
OnCrv.FraDistRef.FraRadio.Radio3.Title="Abstand entlang Richtung";
OnCrv.FraDistRef.FraRadio.LongHelp="Legt fest, auf welcher Basis der neue Punkt \nerzeugt werden soll: \n  - in einem bestimmten Abstand entlang der Kurve \n vom Referenzpunkt aus \n- in einem bestimmten Verh�ltnis zur Kurvenl�nge\n vom Referenzpunkt aus oder entlang der \n angegebenen Richtung.";
OnCrv.FraDynamic1.FraPush.PushNear.Title="N�chstliegendes Ende";
OnCrv.FraDynamic1.FraPush.PushNear.LongHelp="Erm�glicht die Erzeugung eines Kurvenendes,\n das dem aktuell definierten Punkt am n�chsten liegt.";
OnCrv.FraDynamic1.FraPush.PushMid.Title="Mittelpunkt";
OnCrv.FraDynamic1.FraPush.PushMid.LongHelp="Erm�glicht das Erzeugen des Kurvenmittelpunkts.\nWenn ein Referenzpunkt festgelegt wird, wird der\nMittelpunkt zwischen\nReferenzpunkt und Endpunkt (Extremity) erzeugt.";
OnCrv.FraDynamic1.FraRadioEuc.RadioEuc1.Title="Geod�tisch";
OnCrv.FraDynamic1.FraRadioEuc.RadioEuc2.Title="Euklidisch";
OnCrv.FraDynamic1.FraRadioEuc.LongHelp="Bestimmt, wie der Abstand gemessen wird: \n    - entlang der Kurve (geod�tisch)\n    - absolut vom Referenzpunkt aus (euklidisch)";
OnCrv.FraDistRef1.PushB.Title="Richtung umkehren";
OnCrv.FraDynamic1.LabDirection.Title="Richtung: ";
OnCrv.FraDynamic1.FraDirection.LongHelp="Definiert die Richtung vom Referenzpunkt aus,\n in der der Punkt erzeugt wird.";
OnCrv.FraDynamic1.LabDyn1.Title="    ";
OnCrv.FraDynamic1.LabDyn2.Title="   ";
OnCrv.FraSepar.LabelSepar.Title="Referenz ";
OnCrv.FraDynamic1.LengthCont.FraCke.LabelOnCrvLength.Title="L�nge: ";
OnCrv.FraDynamic1.LengthCont.FraCke.LabelOnCrvRatio.Title="Faktor: ";
OnCrv.FraDynamic1.LengthCont.FraCke.LabelOnCrvDistAlongDir.Title="Offset:     ";
OnCrv.FraDynamic1.LengthCont.FraCke.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Wert";
OnCrv.FraDynamic1.LengthCont.FraCke.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnCrv.FraElem1.LongHelp="Gibt die Kurve an, auf der der\nPunkt erzeugt werden soll.";
OnCrv.FraDistRef1.FraElem2.LongHelp="Gibt den Punkt an, der als Referenz f�r den Abstand\nauf der Kurve verwendet werden soll. Ist kein Punkt\nausgew�hlt, wird das Ende der Kurve verwendet.";
OnCrv.FraDistRef1.PushB.LongHelp="Zeigt den Punkt an:\n- auf der anderen Seite des Referenzpunkts\n (sofern ein Punkt ausgew�hlt war)\n- vom anderen Ende der Kurve aus berechnet\n (wenn kein Punkt ausgew�hlt war).";
OnCrv.FraDistRef.LabDistRef.Title="   ";
OnCrv.FraDistRef1.LabDistRef1.Title="   ";
OnCrv.FraDistRef1.PushIcon.Title="Entfernen";
OnCrv.FraDistRef1.PushIcon.ShortHelp="Referenzpunkt entfernen";
OnCrv.FraDistRef1.PushIcon.LongHelp="Erm�glicht das Entfernen des Referenzpunkts\nund die ersatzweise Verwendung des Standardreferenzpunkts";


OnPln.LabelOnPlnPlane.Title="Ebene: ";
OnPln.LabelOnPlnPoint.Title="Punkt: ";
OnPln.LabelOnPlnX.Title="H: ";
OnPln.LabelOnPlnY.Title="V: ";
OnPln.FraSepar.LabelSepar.Title="Referenz ";
OnPln.FraElem1.LongHelp="Gibt die Ebene an, auf der der\nPunkt erzeugt werden soll.";
OnPln.FrameErase.FraElem2.LongHelp="Gibt den Referenzpunkt auf der Ebene zur Berechnung\nder Koordinaten an. Ist kein Punkt ausgew�hlt, wird die Projektion des\nUrsprungs auf der Ebene als Referenz verwendet.";
OnPln.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="H";
OnPln.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnPln.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="V";
OnPln.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Zur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnPln.LabRef.Title="   ";
OnPln.FrameErase.PushIcon.Title="Entfernen";
OnPln.FrameErase.PushIcon.ShortHelp="Referenzpunkt entfernen";
OnPln.FrameErase.PushIcon.LongHelp="Erm�glicht das Entfernen des Referenzpunkts\nund die ersatzweise Verwendung des Standardreferenzpunkts";
// start code 3dplm
OnPln.FraSeparSur.LabelSeparSur.Title="Projektion ";
OnPln.LabelSur.Title="Fl�che: ";
OnPln.FrameEditSur.FraElem2.LongHelp="Gibt das St�tzelement der Projektion an, \ndas den Punkt auf der Ebene berechnet. Standardm��ig ist kein St�tzelement ausgew�hlt.";
// end code 3dplm

OnSurf.LabelOnSurfSur.Title="Fl�che: ";
OnSurf.LabelOnSurfPoint.Title="Punkt: ";
OnSurf.LabelOnSurfDir.Title="Richtung: ";
OnSurf.LabelOnSurfDist.Title="Abstand: ";
OnSurf.FraDirection.LongHelp="Definiert die Richtung auf der Fl�che vom Referenzpunkt aus,\n in der der Punkt erzeugt wird.";
OnSurf.FraSepar.LabelSepar.Title="Referenz ";
OnSurf.FraElem1.LongHelp="Gibt die Fl�che an, auf der der Punkt\nerzeugt werden soll.";
OnSurf.FrameErase.FraElem2.LongHelp="Gibt den Referenzpunkt an.\nWird kein Punkt ausgew�hlt, wird der Fl�chenmittelpunkt als Referenzpunkt verwendet.";
OnSurf.FraDirection.Editor.LongHelp="Gibt die Linie an, deren Ausrichtung die\nRichtung festlegt, oder die Ebene, deren Normale\ndie Richtung festlegt.\nZur Angabe der Richtung der X-, Y- und Z- Komponenten\nkann auch das Kontextmen� verwendet werden.";
OnSurf.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Abstand";
OnSurf.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Definiert den Abstand auf der Fl�che vom Referenzpunkt aus.\nZur Angabe der Formel kann auch das Kontextmen� verwendet werden.";
OnSurf.LabRef.Title="   ";
OnSurf.FrameErase.PushIcon.Title="Entfernen";
OnSurf.FrameErase.PushIcon.ShortHelp="Referenzpunkt entfernen";
OnSurf.FrameErase.PushIcon.LongHelp="Erm�glicht das Entfernen des Referenzpunkts\nund die ersatzweise Verwendung des Standardreferenzpunkts";
//start 3dplm code 27th august'04
OnSurf.FraSeparDynPos.LabSeparDynPos.Title="Dynamische Positionierung ";
OnSurf.FraSeparDynPos.Coarse.Title="Grob";
OnSurf.FraSeparDynPos.Fine.Title="Fein";
OnSurf.FraSeparDynPos.Coarse.ShortHelp="Schneller, aber angen�herter Algorithmus";
OnSurf.FraSeparDynPos.Coarse.LongHelp="Verwendet einen euklidischen Algorithmus zum Berechnen des Abstands zwischen Referenzpunkt und Mausklick.";
OnSurf.FraSeparDynPos.Fine.ShortHelp="Pr�ziser Algorithmus";
OnSurf.FraSeparDynPos.Fine.LongHelp="Verwendet nach M�glichkeit einen geod�tischen Algorithmus zum Berechnen des Abstands zwischen Referenzpunkt und Mausklick.";
//end 3dplm code


OnCtr.LabelCtrCurve.Title="Kreis/Kugel/Ellipse ";
OnCtr.FraElem1.LongHelp="Gibt den Kreis oder Kreisbogen an, dessen Mittelpunkt\nzum Erzeugen eines Punkts verwendet werden soll.";

OnTgt.LabelTgtCurve.Title="Kurve: ";
OnTgt.LabelTgtDir.Title="Richtung: ";
OnTgt.FraElem1.LongHelp="Gibt die Kurve an, auf der der Tangentialpunkt\nerzeugt werden soll.";
OnTgt.FraDirection.LongHelp="Gibt die Linie an, deren Ausrichtung die\nRichtung festlegt, oder die Ebene, deren Normale\ndie Richtung festlegt.\nZur Angabe der Richtung der X-, Y- und Z- Komponenten\nkann auch das Kontextmen� verwendet werden.";

PointCKE.FraPointCKE.LabelPointCKE.Title="Punkt: ";

Between.FraPrincipal.LabelBetweenFirst.Title="Punkt 1: ";
Between.FraPrincipal.FraElem1.LongHelp="Erster Punkt";
Between.FraPrincipal.LabelBetweenSecond.Title="Punkt 2: ";
Between.FraPrincipal.FraElem2.LongHelp="Zweiter Punkt";
Between.FraPrincipal.LabelBetweenRatio.Title="Faktor: ";
Between.FraPrincipal.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Wenn d1 der Abstand zwischen dem ersten Punkt und dem erzeugten Punkt ist und\nd2 der Abstand zwischen dem ersten Punkt und dem zweiten Punkt,\n ist das Verh�ltnis = d1/d2";
Between.FraPrincipal.Support.Title="St�tzelement:";
Between.FraPrincipal.Support.LongHelp="Das St�tzelement ausw�hlen";
Between.FraPrincipal.FraSupport.LongHelp="Gibt die St�tzfl�che an, auf der der\ngeod�tische Punkt erzeugt werden soll.\nDiese Angaben sind optional.";
Between.FraInverse.PushB.Title="Richtung umkehren";
Between.FraInverse.PushB.LongHelp="Richtung umkehren und in die entgegengesetzte Richtung gehen";
Between.FraInverse.PushMid.Title="Mittelpunkt";
Between.FraInverse.PushMid.LongHelp="Erm�glicht das Erstellen des Mittelpunkts zwischen\n dem ersten und dem zweiten Punkt";
// start code 3dplm
//Combo lock help
PointLockShort="Klicken, um die automatische Typ�nderung zu aktivieren.";
PointUnlockShort="Klicken, um die automatische Typ�nderung zu inaktivieren.";
PointLockLong="Durch Anklicken dieser Schaltfl�che wird die automatische Typ�nderung aktiviert.";
PointUnlockLong="Durch Anklicken dieser Schaltfl�che wird die automatische Typ�nderung inaktiviert.";
// end code 3dplm

