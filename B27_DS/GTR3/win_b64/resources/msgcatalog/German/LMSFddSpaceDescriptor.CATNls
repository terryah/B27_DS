XY_OptionXY="Standard";
XY_OptionXY_XY_Res="Y-Achse";
XY_OptionXY_XY_Arg="X-Achse";

Colormap_OptionXYZ="Standard";
Colormap_OptionXYZ_Colormap_Arg1="X-Achse";
Colormap_OptionXYZ_Colormap_Arg2="Z-Achse";
Colormap_OptionXYZ_Colormap_Res="Achse der Farbskala";

Nyquist_OptionDefault="Standard";
Nyquist_OptionDefault_Nyquist_Real="Reale Achse";
Nyquist_OptionDefault_Nyquist_Imag="Imaginäre Achse";
Nyquist_OptionDefault_Nyquist_Arg="X-Achse";

BodePlot_OptionRealImag="Real/Imaginär";
BodePlot_OptionRealImag_BodePlot_Arg="X-Achse";
BodePlot_OptionRealImag_BodePlot_Y1="Reale Achse";
BodePlot_OptionRealImag_BodePlot_Y2="Imaginäre Achse";

BodePlot_OptionAmplitudePhase="Amplitude//Phase";
BodePlot_OptionAmplitudePhase_BodePlot_Arg="X-Achse";
BodePlot_OptionAmplitudePhase_BodePlot_Y1="Amplitudenachse";
BodePlot_OptionAmplitudePhase_BodePlot_Y2="Phasenachse";

BodePlot_OptionPhaseAmplitude="Phase/Amplitude";
BodePlot_OptionPhaseAmplitude_BodePlot_Arg="X-Achse";
BodePlot_OptionPhaseAmplitude_BodePlot_Y1="Phasenachse";
BodePlot_OptionPhaseAmplitude_BodePlot_Y2="Amplitudenachse";

Vector_OptionIndependent="Unabhängig";
Vector_OptionIndependent_Vector_Ampl="Amplitudenachse";
Vector_OptionIndependent_Vector_Phase="Phasenachse";
Vector_OptionIndependent_Vector_Arg="Argumentachse";

Vector_OptionCumulative="Kumulativ";
Vector_OptionCumulative_Vector_Ampl="Amplitudenachse";
Vector_OptionCumulativet_Vector_Phase="Phasenachse";
Vector_OptionCumulative_Vector_Arg="Argumentachse";

Polar_OptionDefault="Standard";
Polar_OptionDefault_Polar_Arg="Argumentachse";
Polar_OptionDefault_Polar_Ampl="Amplitudenachse";
Polar_OptionDefault_Polar_Phase="Phasenachse";

BarChart_OptionDefault="Standard";
BarChart_OptionDefault_BarChart_ContinuousRes="Ergebnisachse";
BarChart_OptionDefault_BarChart_DiscreteArg="Argumentachse";

Contribution_OptionDefault="Standard";
Contribution_OptionDefault_Contribution_Res="Ergebnisachse";
Contribution_OptionDefault_Contribution_ContinuousArg="Argumentachse";
Contribution_OptionDefault_Contribution_DiscreteArg="Achse der diskreten Argumente";

Matrix_OptionXYZ="Standard";
Matrix_OptionXYZ_Matrix_DiscreteArg1="X-Achse";
Matrix_OptionXYZ_Matrix_DiscreteArg2="Y-Achse";
Matrix_OptionXYZ_Matrix_Res="Z-Achse";

AmplitudeResDofUsage = "Amplitudenergebnis";
RealResDofUsage = "Reelles Ergebnis";
ImagResDofUsage = "Imaginäres Ergebnis";
PhaseResDofUsage = "Phasenergebnis";
ComplexResDofUsage = "Komplexes Ergebnis";
ContArg1DofUsage = "Argument 1";
ContArg2DofUsage = "Argument 2";
DiscrArg1DofUsage = "Diskretes Argument 1";
DiscrArg2DofUsage = "Diskretes Argument 2";


ContArgLinFormat="Linear";
ContArgDecFormat="Dekaden";
ContArgOctFormat="Oktaven";
ContArgPower2Format="Leistung 2";
ComplexResRealFormat="Reelle Zahl";
ComplexResImagFormat="Imaginär";
ComplexResAmplitudeFormat="Amplitude";
ComplexResLogFormat="Logarithmisch";
ComplexResdBFormat="dB(Peak)";
ComplexResdBRMSFormat="dB(RMS)";
ComplexResPhaseUnwrappedDegreesFormat="Synchronisierungsgrade";
ComplexResPhaseWrappedDegreesFormat="Wiederverwendete Synchronisierungsgrade";
ComplexResPhaseUnwrappedRadiansFormat="Synchronisierungsradianten";
ComplexResPhaseWrappedRadiansFormat="Wiederverwendete Synchronisierungsradianten";
DiscrArgFormat="Diskrete Werte";

PhaseResPhaseUnwrappedDegreesFormat="Synchronisierungsgrade";
PhaseResPhaseWrappedDegreesFormat="Wiederverwendete Synchronisierungsgrade";
PhaseResPhaseUnwrappedRadiansFormat="Synchronisierungsradianten";
PhaseResPhaseWrappedRadiansFormat="Wiederverwendete Synchronisierungsradianten";
AmplitudeResFormat="Amplitude";
RealResRealFormat="Reelle Zahl";
ImagResImagFormat="Imaginär";
AmplitudeResLogFormat="Logarithmisch";
AmplitudeResdBFormat="dB(Peak)";
AmplitudeResdBRMSFormat="dB(RMS)";

XY_Graph="XY-Grafik";
BodePlotDisplay="Bodediagramm";
ColormapDisplay="Grafik mit Farbzuordnungstabelle";
ContributionDisplay="Beitragsgrafik";
BarChartDisplay="Grafik mit Balkendiagramm";
VectorDisplay="Vektorgrafik";
PolarDisplay="Polargrafik";
NyquistDisplay="Nyquist-Grafik";
MatrixDisplay="Matrixgrafik";

XY="XY";
BodePlot="Bodediagramm";
Colormap="Farbzuordnungstabelle";
Contribution="Beitrag";
BarChart="Balkendiagramm";
Vector="Vektor";
Polar="Polar";
Nyquist="Nyquist";
Matrix="Matrix";


SingleContArg1CursorDefinition="Einfaches X";
SingleComplexResCursorDefinition="Einfaches Y";
DoubleContArg1CursorDefinition="Doppeltes X";
SingleContArg2CursorDefinition="Einfaches Y";
SingleContArg1ContArg2XYCursorDefinition="Einfaches XY";
DoubleContArg1ContArg2CursorDefinition="Einfaches XY";
SingleOrderCursorDefinition="Grad";
DoubleOrderCursorDefinition="Doppelter Grad";
SingleAmplPhaseRealImagResCursorDefinition="Einfaches Y";
SingleDiscrArg1CursorDefinition="Einfaches X";
DoubleDiscrArg1CursorDefinition="Doppeltes X";
SingleContArg1DiscrArg2CursorDefinition="Einfaches X";
SingleDiscrArg1DiscrArg2CursorDefinition="Einfaches XY";
DoubleDiscrArg1DiscrArg2CursorDefinition="Einfaches XY";

XYToXY1="Standard (XY)";
XYToXY2="YX";
NyquistToXY1="Standard";
BarChartToXY1="Standard (XY)";
BarChartToXY2="YX";
ColormapToXYCS1="Standard";
ContributionToXYCS="Standard (XY)";
ContributionToYXCS="YX";
MatrixToXYZCS1="Standard";
BodePlotToBodePlotA1="Standard (XY)";
BodePlotToBodePlotA2="YX";
BodePlotToBodePlotB1="Standard (XY)";
BodePlotToBodePlotB2="YX";
VectorToPolar1="Standard";
PolarToPolar1="Standard";

RedGreenYellow_Colorscale="Rot/Grün/Gelb";
VioletBlack_Colorscale="Violett";
LightBlue_Colorscale="Hellblau";
GreenWhite_Colorscale="Grün/Weiß";
Atlas_Colorscale="Atlas";
Color_Colorscale="Farbe";
Grey_Colorscale="Grau";
Rainbow_Colorscale="Regenbogen";
AcousticSense_Colorscale="Schallverhältnisse";
AcousticSenseHigh_Colorscale="Schallverhältnisse Hoch";
AcousticSenseInv_Colorscale="Schallverhältnisse umkehren";
AcousticSenseInvHigh_Colorscale="Schallverhältnisse Hoch - umkehren";
LMSFddSetColorScaleCustom="Benutzerdefiniert...";

NoWeighting="Keine Gewichtung";
AWeighting="A-gewichtet";
BWeighting="B-gewichtet";
CWeighting="C-gewichtet";
DWeighting="D-gewichtet";

NoWeightingShort="Keine Gewichtung";
AWeightingShort="A";
BWeightingShort="B";
CWeightingShort="C";
DWeightingShort="D";

FreeLimits="Frei";
OptimizedLimits="Optimiert";
FixedLimits="Fest...";
ADLimits="Durch Anwendung definiert";
AbsoluteLimits="Absolut";

