//==============================================================================================
// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2003
//==============================================================================================
// 26-Mar-2003 - JLH - Creation
//==============================================================================================

Title="Bereinigung des Netzes";

CorruptedSfx1=" Dreieck";
CorruptedSfxN=" Dreiecke";
DuplicatedSfx1=" Dreieck";
DuplicatedSfxN=" Dreiecke";
NonOrientableSfx1=" Dreieck";
NonOrientableSfxN=" Dreiecke";
NMFEdgesSfx1=" Kante";
NMFEdgesSfxN=" Kanten";
NMFVerticesSfx1=" Scheitelpunkt";
NMFVerticesSfxN=" Scheitelpunkte";
NonOrientable="Ein nicht auszurichtendes Netz.";
NonOrientables=" nicht auszurichtende Netze.";
OnlyOneZone="Es ist nur ein Bereich vorhanden -> keine Trennung";
SeveralZones=" verbundene Bereiche";

RootFrame.Pages.DeletionPag.Title="L�schen";
RootFrame.Pages.DeletionPag.LongHelp="L�scht besch�digte/doppelte Dreiecke, Nicht-Mehrfachkanten und/oder Scheitelpunkte.";
RootFrame.Pages.StructurePag.Title="Struktur";
RootFrame.Pages.StructurePag.LongHelp="Bearbeitung der globalen Netzstruktur.";
RootFrame.Pages.EditionPag.Title="Bearbeitung";
RootFrame.Pages.EditionPag.LongHelp="Bearbeitung des Netzes: d�nne Dreiecke unterdr�cken.";

RootFrame.Pages.DeletionPag.AnalyzeBtn.Title="Analyse";
RootFrame.Pages.DeletionPag.AnalyzeBtn.LongHelp="Analysiert das Netz, um die verschiedenen Bearbeitungsoperationen vorbereiten zu k�nnen.";
RootFrame.Pages.DeletionPag.StatusLbl.Title="Statistik";
RootFrame.Pages.DeletionPag.StatusLbl.LongHelp="Liefert eine aktuelle Statistik zum Netz.";
RootFrame.Pages.DeletionPag.ColorsLbl.Title="Farben voranzeigen";
RootFrame.Pages.DeletionPag.ColorsLbl.LongHelp="Definiert die Farben f�r die Voranzeige.";

RootFrame.Pages.DeletionPag.CorruptedBtn.Title="Besch�digte Dreiecke";
RootFrame.Pages.DeletionPag.CorruptedBtn.LongHelp="L�scht besch�digte Dreiecke.";

RootFrame.Pages.DeletionPag.DuplicatedBtn.Title="Doppelte Dreiecke";
RootFrame.Pages.DeletionPag.DuplicatedBtn.LongHelp="L�scht doppelte Dreiecke.";

RootFrame.Pages.DeletionPag.NonOrientableBtn.Title="Inkonsistente Ausrichtung";
RootFrame.Pages.DeletionPag.NonOrientableBtn.LongHelp="L�scht Paare verbundener Dreiecke, deren Ausrichtung zueinander inkonsistent ist.";

RootFrame.Pages.DeletionPag.NMFEdgesBtn.Title="Nicht-Mehrfachkanten";
RootFrame.Pages.DeletionPag.NMFEdgesBtn.LongHelp="L�scht Nicht-Mehrfachkanten.";

RootFrame.Pages.DeletionPag.NMFVerticesBtn.Title="Nicht vielf�ltige Scheitelpunkte";
RootFrame.Pages.DeletionPag.NMFVerticesBtn.LongHelp="L�scht nicht vielf�ltige Scheitelpunkte.";

RootFrame.Pages.DeletionPag.IsolatedBtn.Title="Isolierte Dreiecke";
RootFrame.Pages.DeletionPag.IsolatedBtn.LongHelp="L�scht isolierte Dreiecke.";
RootFrame.Pages.DeletionPag.IsolatedSld.Title="Anzahl Dreiecke";
RootFrame.Pages.DeletionPag.IsolatedSld.LongHelp="Definiert die maximale Anzahl Dreiecke in einem isolierten Unternetz, die gel�scht werden soll.";

RootFrame.Pages.DeletionPag.LongEdgesBtn.Title="Lange Kanten";
RootFrame.Pages.DeletionPag.LongEdgesBtn.LongHelp="L�scht Dreiecke mit langen Kanten.";
RootFrame.Pages.DeletionPag.MaxLengthFrm.Title="Max. L�nge";
RootFrame.Pages.DeletionPag.MaxLengthFrm.LongHelp="Definiert die maximale Kantenl�nge.";

RootFrame.Pages.DeletionPag.SmallAnglesBtn.Title="Kleine Winkel";
RootFrame.Pages.DeletionPag.SmallAnglesBtn.LongHelp="L�scht Dreiecke mit kleinen Winkeln.";
RootFrame.Pages.DeletionPag.MinAngleFrm.Title="Mindestwinkel";
RootFrame.Pages.DeletionPag.MinAngleFrm.LongHelp="Definiert den Mindestwinkel.";

RootFrame.Pages.StructurePag.DirectLbl.Title="Direkte Dreiecke";
RootFrame.Pages.StructurePag.DirectLbl.LongHelp="Farbe f�r direkte Dreiecke voranzeigen.";
RootFrame.Pages.StructurePag.OppositeLbl.Title="Indirekte Dreiecke";
RootFrame.Pages.StructurePag.OppositeLbl.LongHelp="Farbe f�r indirekte Dreiecke voranzeigen.";
RootFrame.Pages.StructurePag.OrientationBtn.Title="Ausrichtung";
RootFrame.Pages.StructurePag.OrientationBtn.LongHelp="Richtet Dreiecke auf dieselbe Art aus (sofern m�glich).";

RootFrame.Pages.StructurePag.CnxZonesBtn.Title="In verbundene Bereiche aufteilen";
RootFrame.Pages.StructurePag.CnxZonesBtn.LongHelp="Teilt das Netz in verbundene Bereiche auf.";
RootFrame.Pages.StructurePag.DistinctBtn.Title="Verschieden";
RootFrame.Pages.StructurePag.DistinctBtn.LongHelp="Teilt in verschiedene Einheiten auf.";
RootFrame.Pages.StructurePag.GroupedBtn.Title="Gruppiert";
RootFrame.Pages.StructurePag.GroupedBtn.LongHelp="Teilt in verschiedene Zellen auf, die in einer Einheit gruppiert sind.";

RootFrame.Pages.EditionPag.EditSmallAnglesBtn.Title="Kleine Winkel";
RootFrame.Pages.EditionPag.EditSmallAnglesBtn.LongHelp="Unterdr�ckt Dreiecke mit kleinen Winkeln.";
