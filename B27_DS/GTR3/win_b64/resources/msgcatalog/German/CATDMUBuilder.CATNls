INFO_HELP="
CATDMUBuilder erstellt Produkte entsprechend einer Liste mit Dokumenten und
              f�llt den Cache mit den relevanten Daten.\n
Verwendung: \n
   Hilfeinformationen abrufen:
      CATDMUBuilder -h\n
   Produkte erstellen und Cache f�llen:
      CATDMUBuilder eingabeposition
                    [-db datenbank -user benutzer -spwd verschl_kennw -role rolle -server server]
                    [-static][-selins]
                    -product prod_verz [-prefix pr�fix]
                    [-replacebycgr cgr_vz][-activate|-deactivate][-onlyone]
                    [-nocache][-force][-copycache]\n
   Cachedaten f�llen:
      CATDMUBuilder eingabeposition
                    [-db datenbank -user benutzer -spwd verschl_kennwort -role rolle -server server]
                    [-static][-selins]\n
Beispiele unter Windows:\n
   CATDMUBuilder -h
   CATDMUBuilder c:\u\eingabe.txt -product c:\prodverz\n
Beispiele unter Unix:\n
   CATDMUBuilder -h
   CATDMUBuilder /u/eingabe1
   CATDMUBuilder /u/eingabe2 -user benutzer1 -spwd benutzer1kennw -role benutzer1rolle -server server2\n
Argumente:\n
   -h                   : Hilfe.
   eingabeposition      : Definiert die Liste der zu verarbeitenden Dokumente.
                          M�gliche Pfadangaben:
                          - Datei mit einer Liste von Dokumenten.
                            Die einzelnen Zeilen m�ssen einen Pfad zu einem Dokument enthalten
                            (DL-Name mit 'CATDLN://' davor zul�ssig).
                          - Verzeichnis mit den Dokumenten.
                            Alle Dokumente in diesem Verzeichnis werden verarbeitet.
                            Dieses Verzeichnis kann ein DL-Name mit 'CATDLN://' davor sein.
                          Die folgenden Dokumenttypen werden verarbeitet: CATProduct, CATVpm,
                          psn, Scene4D, xml oder MultiCAD-Baugruppen.
   -db datenbank        : Definiert die Datenbank (VPM, VPMServer oder ENOVIAV5).
   -user benutzer       : Definiert den Benutzer.
   -pwd kennwort        : Definiert das Kennwort des Benutzers (kann nicht mit �spwd verwendet werden).
   -spwd verschl_kennw  : Definiert das Kennwort des Benutzers.
                          Es wird vor der Verwendung verschl�sselt.
   -role rolle          : Definiert die Rolle des Benutzers.
   -server server       : Definiert den Server.
   -serverid id         : Verbindung zu einem vorhandenen Server (nur bei ENOVIAV5).
   -host hostid         : Definiert die Servermaschine (nur bei VPMServer).
   -static              : �ffnet psn-Dateien mit der Option 'statisch' (nur f�r psn g�ltig).
                          Die Dateien werden standardm��ig mit der Option 'dynamisch' ge�ffnet.
   -selins              : �ffnet psn-Dateien mit den ausgew�hlten Exemplaren (nur f�r psn g�ltig).
                          Die Dateien werden standardm��ig mit allen Exemplaren ge�ffnet.
   -product prod_verz   : Speichert die Produkte im angegebenen Verzeichnis.
                          Dieses Verzeichnis kann ein DL-Name mit 'CATDLN://' davor sein.
   -prefix pr�fix       : Sichert alle Produkte mit einem Pr�fix in ihrem Namen.
   -replacebycgr cgr_vz : Ersetzt alle Komponenten durch die entsprechende cgr-Kopie im
                          Verzeichnis cgr_vz.
                          Dieses Verzeichnis kann ein DL-Name mit 'CATDLN://' davor sein.
   -activate            : Aktiviert alle Formen und sichert den Aktivierungsstatus.
   -deactivate          : Inaktiviert alle Formen und sichert den Aktivierungsstatus.
    -onlyone             : Integriert alle Komponenten in das Rootprodukt.
   -nocache             : Der Schritt f�r die Cachevorbereitung wird ausgelassen.
   -force               : Erzwingt die Neuberechnung zwischengespeicherter Daten.
   -copycache           : Kopiert alle zwischengespeicherten Daten in das lokale Cacheverzeichnis.
   -noreplace           : Verhindert das Ersetzen von Dateien beim Kopieren.
   -mp                  : Aktiviert den Mehrprozessorbetrieb.
   -listcomp komp_verz  : Listet alle Komponenten der einzelnen Produkte auf. Das Verzeichnis
                          enth�lt eine Textdatei pro Produkt (mit demselben Namen).
                          Jede Datei enth�lt eine Komponente pro Zeile.
   -outputformat xx yy  : Exportiert die Produktstruktur in eine Liste angegebener Formate
                          (xx, yy). Diese Option setzt die Option -product voraus. Alle
                          Dateien werden in das Verzeichnis prod_verz exportiert.
   -savedata            : Sichert die mit der Produktstruktur verkn�pften Daten im selben Verzeichnis.\n
   Hinweis: Die Parameter -user, -spwd, -role und -server sind erforderlich, wenn sich die Dokumente auf VPM beziehen.\n
Diagnose:\n
   M�gliche Exit-Statuswerte:\n
      0   Erfolgreicher Abschluss.\n
      1   Fehler, der auf eine der folgenden Ursachen zur�ckzuf�hren ist:
          - Lizenz nicht verf�gbar
          - Cacheverwaltung inaktiviert
          - fehlende Argumente
          - ung�ltiger Parameter
          - fehlender Parameter nach einer Option
          - fehlende Eingabedatei
          - Datei nicht gefunden
          - �ffnen einer Eingabedatei nicht m�glich
          - falscher Dokumenttyp\n
      2   Verarbeitungsfehler.\n
      3   Teilweise verarbeitet.\n
Fehlerbehebung:\n
   Exit-Status = 1:
       Befehlszeile oder Umgebung anhand der Informationen in der Standardfehlerdatei
      �ndern.\n
   Exit-Status = 2:
      Die Anwenderunterst�tzung benachrichtigen.\n
   Exit-Status = 3:
      Die Dateien korrigieren, die das Problem verursachen: ihre Namen sind in der Standardfehlerdatei
      aufgef�hrt. Eine interaktive Verwendung der Dateien gibt m�glicherweise Aufschluss �ber das Problem.";
ERR_PROCESSING="Fehler: CATDMUBuilder kann nicht bearbeitet werden.";
ERR_TREATMENT="Fehler: Das folgende Dokument/Verzeichnis kann nicht bearbeitet werden:";
OK_TREATMENT="OK: Das folgende Dokument/Verzeichnis wurde bearbeitet:";
START_BATCH="CATDMUBuilder startet um:";
STOP_BATCH="CATDMUBuilder stoppt nach einer Dauer von:";
