// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// Catalog for CACkeNemoRootTerm class
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
// Jul 2000  Creation:            Dmitri Ouchakov, LEDAS Ltd. (vtn1)
// Nov 2000  Modification:           D. Ouchakov,  LEDAS Ltd. (vtn1)
//===================================================================
NoInputs="Keine Eingabeparameter ausgewählt\n",
         "(möglicherweise ein Fehler)";
NoOutputs="Keine Ausgabeparameter ausgewählt\n",
          "(möglicherweise ein Fehler)";
NoSolutions="Keine Lösungen gefunden.";
ErrorMessage.NoBlackBoxDetected.Title="Die Bedingungserfüllung hat ein potenzielles Problem erkannt.\n",
                                        "\n",
                                         "Die folgenden Ausdrücke (potenzielle Funktionseinheiten) erkennen unbekannte Parameter nicht.\n\n";
ErrorMessage.NoBlackBoxDetected.Text="\nMögliche Ursachen: \n",
                                        "- Diese Gleichungen weisen keine Messungen auf (Sie können anschließend die Funktion Gleichungs-Set \n",
                                        "  anstelle dieser Funktion verwenden.)\n",
                                        "- Die Messungen innerhalb dieser Komponente sind unabhängig von der Auswahl der\n",
                                        "  variablen Parameter.\n", 
                                        "- Die Bedingungserfüllung konnte nicht bestimmen, \n",
                                        "  welche Variable die Messungen beeinflusst (erhöhen Sie die Präzisionsoptionen der Funktionseinheit).\n\n",
                                        "Die Auflösung kann jedoch fortgesetzt werden\n";
ErrorMessage.SoEBlackBoxDetected.Title="Die Bedingungserfüllung hat ein potenzielles Problem erkannt.\n\n",
                                        "Die folgenden Parameter:\n\n";
ErrorMessage.SoEBlackBoxDetected.Text="\nsind die Ausgabeparameter von Gleichungssetbeziehungen und werden als Funktionseinheiten berechnet.\n",
                                        "Da eine Gleichungssetbeziehung eine mehrdeutige Funktion ist, können für die Funktionseinheit\n", 
                                        "während der Berechnung die Lösungen verloren gehen.\n\n",
                                        "Zur Lösung des Problems wird Folgendes empfohlen:\n", 
                                        "- Den Text der Gleichungssetbeziehungen in das aktuelle Bedingungserfüllungsmodell kopieren.\n",
                                        "- Die Gleichungssetbeziehungen inaktivieren.\n";

InformationMessage.SolvingSuccess.Text = "\n Auflösung war erfolgreich. \n";

Object="\n   Operation: ";
Line  ="\n   Zeile: \n";

Contradiction        ="\nAn der folgenden Stelle ist eine Unvereinbarkeit aufgetreten:";
ContradictionOnRanges="\nWarnung! Inkonsistente Eingabebereiche oder inkompatibles Modell.";
IncompatibleModel    ="\nWarnung! Inkompatibles Modell.";
Interruption         ="Berechnungen wurden durch Benutzeranforderung gestoppt";
InternalError        ="An der folgenden Stelle ist ein interner Fehler aufgetreten: ";
AllSolutionsFound    ="Es wurden alle Lösungen gefunden.";
SolvingSuccess       ="Auflösung war erfolgreich.";

