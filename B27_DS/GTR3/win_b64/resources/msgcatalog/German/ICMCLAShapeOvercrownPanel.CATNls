DialogBoxTitle="Überkronung";

FrameSelection.LongHelp="               Auswahl
Ein Flächenset kann ausgewählt werden.
Diese Flächen müssen nicht verbunden sein.";
LabelElement.Title="Element: ";
LabelElement.LongHelp="Ein Flächenset kann ausgewählt werden.
Diese Flächen müssen nicht verbunden sein.";
LabelElement.ShortHelp="Gibt das Element an, das überkront werden soll.";
TabPageOptions.Title="Optionen";

FrameReference.Title="Referenz";
FrameReference.LongHelp="Bei Auswahl einer Referenz werden alle Parameter 
der Überkronungsfunktion 
für diese Fläche anstelle der intern berechneten übernommen.";

CheckAuto.Title="Auto: ";
CheckAuto.LongHelp="Die Referenz kann automatisch berechnet oder 
benutzerdefiniert werden. Die automatisch erzeugte Referenz ist so klein wie möglich. 
Die Größe der benutzerdefinierten Referenz hängt vom ausgewählten Element ab. 
Das Ergebnis der Überkronung ist nicht dasselbe, sondern hängt von der Referenz ab.";
CheckAuto.ShortHelp="Die Referenz wird automatisch erzeugt ";

FrameCrowning.Title="Überkronung";
FrameCrowning.LongHelp="Die Überkronung wird durch die Positionen 
der Überkronungsvektoren und deren Abständen definiert";
LabelPosition.Title="Position: ";
LabelPosition.LongHelp="Der bestimmte Punkt entspricht der Position eines Überkronungsvektors. 
Der Punkt muss auf einem Element liegen, der zu überkronenden Fläche.";
LabelPosition.ShortHelp="Position eines Überkronungsvektors";
LabelDistance.Title="Abstand: ";
LabelDistance.LongHelp="Gibt den Überkronungsabstand an. 
Die Länge des an den Überkronungspunkten positionierten Vektors 
entspricht diesem Wert, multipliziert mit dem Skalierungswert. 
Der Abstand ist der Überkronungsabstand am aktiven Überkronungsvektor. 
Einer der Überkronungsvektoren ist der aktive.";
LabelDistance.ShortHelp="Überkronungsabstand am aktiven Überkronungsvektor";

FrameDisplay.Title="Anzeigen";
FrameDisplay.LongHelp="Anzeigeparameter sind 
- Skalierung der Überkronungsvektoren 
- Anzeige der Referenz, wenn automatisch erzeugt";
CheckScaling.Title="Skalieren: ";
CheckScaling.LongHelp="Die Überkronungsvektoren können zur besseren Verarbeitung skaliert werden. 
Der Skalierungswert hat keinen Einfluss auf das Ergebnis der Überkronung.";
CheckScaling.ShortHelp="Faktor zur Anzeige der Vektoren";
CheckPreview.Title="Voranzeige";
CheckPreview.LongHelp="Zeigen Sie die Referenz an, wenn sie automatisch erzeugt wurde. 
Eine nicht automatisch erzeugte Referenz kann mit \"Verdecken/Anzeigen\" dargestellt werden.";
CheckPreview.ShortHelp="Automatisch erzeugte Referenz anzeigen";

TabPageAdvanced.Title="Erweitert";
LabelEdge.Title="Kante: ";
LabelEdge.LongHelp="Dieser Wert teilt den Bereich des Elements in einen inneren und einen äußeren
Bereich auf. Der Wert 0,1 entspricht 10 % und setzt die Innen/Außen-Begrenzung
auf 10 % der Elementgröße von der Kante des Elements fest.
Eine Überkronung im äußeren Bereich behält die Stetigkeit G0 nicht bei.
Eine Überkronung im inneren Bereich behält die Kantenstetigkeit bei.";
LabelEdge.ShortHelp="Abstand für Kantenstetigkeit";
SpinnerEdge.LongHelp="Dieser Wert teilt den Bereich des Elements in einen inneren und einen äußeren
Bereich auf. Der Wert 0,1 entspricht 10 % und setzt die Innen/Außen-Begrenzung
auf 10 % der Elementgröße von der Kante des Elements fest.
Eine Überkronung im äußeren Bereich behält die Stetigkeit G0 nicht bei.
Eine Überkronung im inneren Bereich behält die Kantenstetigkeit bei.";
SpinnerEdge.ShortHelp="Abstand für Kantenstetigkeit";
LabelEdgeContinuity.Title="Kantenstetigkeit: ";
LabelEdgeContinuity.LongHelp="Die erforderliche Kantenstetigkeit zur Referenz kann konfiguriert werden. 
Überschreitet die Referenz die Eingabedaten, wird die Stetigkeit 
anhand einer imaginären Erweiterung des Ausgabeergebnisses festgelegt.
Die Stetigkeit wird nur für Kanten mit
inneren Überkronungsänderungen festgelegt.";
LabelEdgeContinuity.ShortHelp="Stetigkeit mit inneren Überkronungsänderungen";
EdgeContinuity.Button_G0.LongHelp="G0-Stetigkeit zur Referenz wird für Kanten mit inneren 
Überkronungsänderungen festgelegt. Überschreitet die Referenz die Eingabedaten, wird die Stetigkeit 
anhand einer imaginären Erweiterung des Ausgabeergebnisses festgelegt.";
EdgeContinuity.Button_G0.ShortHelp="G0 zur Referenz";
EdgeContinuity.Button_G1.LongHelp="G1-Stetigkeit zur Referenz wird für Kanten mit inneren 
Überkronungsänderungen festgelegt. Überschreitet die Referenz die Eingabedaten, wird die Stetigkeit 
anhand einer imaginären Erweiterung des Ausgabeergebnisses festgelegt.";
EdgeContinuity.Button_G1.ShortHelp="G1 zur Referenz";
EdgeContinuity.Button_G2.LongHelp="G2 -Stetigkeit zur Referenz wird für Kanten mit inneren 
Überkronungsänderungen festgelegt. Überschreitet die Referenz die Eingabedaten, wird die Stetigkeit 
anhand einer imaginären Erweiterung des Ausgabeergebnisses festgelegt.";
EdgeContinuity.Button_G2.ShortHelp="G2 zur Referenz";

LabelMinOrder.Title="Min. Grad: ";
LabelMinOrder.LongHelp="Minimaler Grad der resultierenden Elemente";
LabelMinOrder.ShortHelp="Min. Grad der resultierenden Elemente";
SpinnerMinOrder.LongHelp="Minimaler Grad der resultierenden Elemente";
SpinnerMinOrder.ShortHelp="Min. Grad der resultierenden Elemente";

FrameShape.Title="Form";
FrameShape.LongHelp="Die Überkronungsform kann nachfolgend geändert werden. 
Ein kleiner Wert bewirkt eine lokale und ein großer Wert eine globale Änderung. 
Der Überkronungswert wird während des Änderungsvorgangs konstant gehalten. 
Mit diesem Wert kann eine optimale Kontrollpunktverteilung erreicht werden.";
SliderShape.LongHelp="Ein kleiner Wert bewirkt eine lokale und ein 
großer Wert eine globale Änderung";
SliderShape.ShortHelp="Formwert";
SpinnerShape.LongHelp="Ein kleiner Wert bewirkt eine lokale und ein 
großer Wert eine globale Änderung";
SpinnerShape.ShortHelp="Formwert";


//???????????????????????????????????????????
Position="Position";
Tangent="Tangente";
Curvature="Krümmung";
