// =====================================================================
// Erreurs Machinist
// Essayer de formater vos messages en specifiant (delimiteur = caracteres fin de ligne) :
//       Request      (ce que l'on tentait de faire)
//       Diagnostic   (ce qui est arrive)
//       Advice       (...ce qu'il pourrait faire si possible)
// ou bien
//       Specifier vos messages avec la cle + les 3 facettes :
//           ex: ERR_2002.Request    Reading data. 
//               ERR_2002.Diagnostic Manufacturing software error : Access to a NULL handler.
//               ERR_2002.Advice     Contact your local support.
// =====================================================================

InfoTitle="Herstellungsinformationen";
WarningTitle="Herstellungswarnung";
ErrorTitle="Herstellungsfehler";
GeneralErrorTitle="Fehler";
PanelTitle="Herstellungsfehler";
UpdateError="Interner Fehler bei Aktualisierung";
InternalError="Herstellungssoftwarefehler.\nFehler ist: /p1";
InitError="Fehler bei Initialisierung";
ErrorNotSpecified="Fehler nicht angegeben.\nDie Anwenderunterst�tzung benachrichtigen";
MsgExtraFacesKO="Teilfl�che mittig auf";
MsgInvalidFace="Ung�ltige Teilfl�che";
MsgGap="L�cke";
WarningTitleTPComputation="Warnung zur Berechnung der Werkzeugbahn";

// Erreurs VERIFY
ERR_1000="- Fehlerhafte Werkzeugdefinition\n Parameter �berpr�fen.\n";
ERR_1001="- Fehlerhafte Operationsdefinition\n Parameter �berpr�fen.\n";
ERR_1002="- Fehlerhafte Strategieparameter\n Parameter �berpr�fen oder neu definieren.\n";
ERR_1003="- �bersch�ssiges Aufma� muss positiv und kleiner als der Eckenradius des Werkzeugs sein.\n";
ERR_1004="- Bearbeitungstoleranzwert muss positiv sein.\n";
ERR_1005="- Werkzeugdurchmesserwert muss positiv sein.\n";
ERR_1006="- Der Wert f�r den Eckenradius des Werkzeugs muss positiv sein.\n";
ERR_1007="- Werkzeugdurchmesser muss gr��er sein als 2*Eckenradius.\n";
ERR_1008="- Werkzeugachse ist null.\n Bitte eine weitere Achse definieren.\n";
ERR_1009="- Aufma� auf Hindernis darf nicht gr��er sein als der Eckenradius des Werkzeugs.\n";
ERR_1010="- Fehlerhafte Bearbeitungsobjektdefinition\n Parameter �berpr�fen.\n";
ERR_1011="- Begrenzungswerkzeugposition ist nicht korrekt.\n Bitte eine andere Position definieren.\n";
ERR_1012="- Begrenzungswerkzeugposition ist nicht korrekt.\n Bitte eine Position definieren.\n";
ERR_1013="- Stoppmodus des Begrenzungswerkzeugs ist nicht korrekt.\n Bitte einen anderen Modus definieren.\n";
ERR_1014="- Stoppmodus des Begrenzungswerkzeugs ist nicht korrekt.\n Bitte einen Modus definieren.\n";
ERR_1015="- Fehlerhafte geometrische Parameter\n Parameter �berpr�fen oder neu definieren.\n";
ERR_1016="- Werkzeugachse und Bearbeitungsrichtung d�rfen nicht parallel sein.\n";
ERR_1017="- Wert der Rautiefe muss positiv sein.\n";
ERR_1018="- Werkzeugdurchmesser muss gr��er sein als Mindestbahnabstand.\n";
ERR_1019="- Wert f�r maximalen Bahnabstand muss positiv sein.\n";
ERR_1020="- Werkzeugdurchmesser ist kleiner als max. Bahnabstand.\n";
ERR_1021="- Wert f�r Mindestbahnabstand muss positiv sein.\n";
ERR_1022="- 'Durch Zustellung definiert'-Modus von 'Zwischen Arbeitsg�ngen'-Makros kann nicht angewendet werden.\n  Stattdessen wird der Gerade-Modus verwendet.\n";
//ERR_1023 = "- Approach vector is null.\n Please define another one.\n";
//ERR_1024 = "- Retract distance must be positive.\n";
//ERR_1025 = "- Retract vector is null.\n Please define another one.\n";
ERR_1026="- Inselsprungabstand muss positiv sein.\n";
ERR_1027="- Kopierfr�smodus ist 'Zickzack', Eintauchsteuermodus ist nicht NoControl.\n";
ERR_1028="- Einstellungsmodus ist HSM und Eintauchsteuermodus weicht von NoControl ab.\n";
ERR_1029="- Wert f�r min. frontale Steigung muss positiv sein.\n";
ERR_1030="- Wert f�r min. frontale Steigung muss unter 90 Grad sein.\n";
ERR_1031="- Min. seitliche Steigung ist negativ oder null.\n";
ERR_1032="- Min. seitliche Steigung muss kleiner als 90 Grad sein.\n";
ERR_1033="- Wert f�r max. horizontale Steigung muss positiv sein.\n";
ERR_1034="- Max. horizontale Steigung muss kleiner als 45 Grad sein.\n";
ERR_1035="- Sicherheitsebene darf nicht parallel zur Werkzeugachse liegen.\n";
ERR_1036="- Zu bearbeitendes Teil ist nicht definiert.\nBitte ein Teil definieren.\n";
ERR_1037="- Schnitt-Tiefe muss positiv sein.\n";
ERR_1038="- Zu bearbeitendes Teil und Kontext sind identisch.\n Bitte eine der Komponenten �ndern.\n";
ERR_1039="- Kopierfr�sparameter OK.\n";
ERR_1040="- Parameter f�r Kopierschruppen OK.\n";
ERR_1041="- Schrupp-Parameter OK.\n";
ERR_1042="- Parameter f�r konturgef�hrtes Fl�chenfr�sen OK.\n";
ERR_1043="- Parameter f�r Fr�sen in H�henschnitten OK.\n";
ERR_1044="- Ein Rohteil f�r die Operation oder die Teiloperation definieren.\n";
ERR_1045="- Parameter f�r Hohlkehlenfr�sen OK.\n";
ERR_1046="- Sicherheitsebene und Werkzeugachse d�rfen nicht parallel sein,\n wenn eine R�ckzugsbewegung entlang der Werkzeugachse ausgef�hrt wird.\n";
ERR_1047="- Warnung: Die Bearbeitungstoleranz ist h�her als der Offset.\n";
ERR_1048="- Parameter f�r das Spiralfr�sen OK.\n";
ERR_1049="- Parameter f�r prismatisches Schruppen OK.\n";
ERR_1050="- Sicherheitsebene und Werkzeugachse m�ssen in der Schruppoperation senkrecht zueinander liegen\n";
ERR_1051="- Zu bearbeitendes Teil ist ung�ltig.\nBitte erneut definieren.\n";
ERR_1052="- R�ckzug optimieren: Dieser Parameter verkleinert redundante Luftschnitte, ohne dass es dabei zu Kollisionen im Teil kommt. \nKollisionen zwischen Werkzeug und dem anf�nglichen oder aktuellen Rohteil werden nicht gepr�ft.";
ERR_1053="- Sicherheitsebene, die f�r die Aufspannung definiert ist, ist ung�ltig.\nEine erneute Definition vornehmen.\n";
ERR_1054="- Der Parameter 'Ung�ltige Teilfl�chen ignorieren' (Ignore invalid faces) ist auf 'Ja' gesetzt. \nUng�ltige Teilfl�chen des zu bearbeitenden Teils und des Kontexts werden nicht berechnet.\n";
ERR_1055="- Die Bearbeitungsrichtung wurde nicht gefunden oder ist nicht aktuell.\n Die Bearbeitungsstrategie (Werkzeugbahntyp) in 'Zickzack' �ndern, die Bearbeitungsrichtung �ndern und die vorherige Bearbeitungsstrategie wiederherstellen.\n";
ERR_1056="- Die Bearbeitungsrichtung wurde nicht gefunden oder ist nicht aktuell.\n Die Bearbeitungsstrategie (Werkzeugbahntyp) in 'Vor und zur�ck' �ndern, die Bearbeitungsrichtung �ndern und die vorherige Bearbeitungsstrategie wiederherstellen.\n";
ERR_1057="- Der nicht schneidende Durchmesser ist ungleich 0, aber die Filteroption f�r Taschen ist nicht aktiviert: \n Der nicht schneidende Durchmesser wird nicht ber�cksichtigt.\n Die Filteroption f�r Taschen (Strategie/Zone) aktivieren, damit er ber�cksichtigt wird.\n";
ERR_1058="- Ein Rohteil definieren. \n";
ERR_1059="- Parameter f�r das Tauchfr�sen OK.\n";
ERR_1060="- Werkzeugachse und L�ngsrichtung d�rfen nicht parallel sein.\n";
ERR_1061="- Eintauchpunkte definieren.\n";
ERR_1062="- Reihenfolge der Eintauchpunkte ist nicht koh�rent.\n Den Befehl 'Reihenfolge' verwenden.\n";
ERR_1063="- Eintauchkonturen definieren.\n";
ERR_1064="- Reihenfolge der Eintauchkonturen ist nicht koh�rent.\n Den Befehl 'Umnummerieren' verwenden.\n";
ERR_1065="- Der Eckenradius muss kleiner sein als die Mitte des seitlichen R�ckzugswegs.\n Einen der zugeh�rigen Parameter �ndern.\n";
ERR_1066="- Der Eckenradius ist falsch.\n Eckenradius oder axialen R�ckzugsweg neu definieren.\n";


ERR_1067=" Der K�rper f�r den Offset ist leer. Einen anderen K�rper ausw�hlen.";
ERR_1068="- Versteifungsoperation : Parameter OK.\n";
ERR_1069="- Versteifungsoperation: Keine R�nder definiert.\n";

// Erreurs Sweeping
ERR_2000="Lesen der geometrischen Parameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2001="Lesen der Operation nicht m�glich.\n Parameter �berpr�fen.";
ERR_2002="Lesen der Strategieparameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2003="Lesen der Vorschubparameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2004="Fehler beim Lesen der Parameter.\n Parameter �berpr�fen.";
ERR_2005="Werkzeugbahn wurde nicht berechnet.\n Parameter �berpr�fen.";
ERR_2006="Fehler beim Anwenden des Filters.\n Den horizontal-vertikalen Filter entfernen und 'Wiedergabe' dr�cken.";
ERR_2007="Untergruppe des Nachbearbeitungsbereichs ist leer.\nElemente hinzuf�gen.";
ERR_2008="Falsche Daten f�r Untergruppe.\nDen Nachbearbeitungsbereich erneut erzeugen.";
ERR_OPTION="\nAuf Ja klicken, um die aktuelle Aktivit�t zu deaktivieren und mit der n�chsten fortzufahren. \nAuf Nein klicken, um die Berechnung der Werkzeugbahn anzuhalten.";

// Erreurs SoftMaterial Roughing
ERR_2010="Lesen des Werkzeugs nicht m�glich.\n Parameter �berpr�fen.";
ERR_2011="Lesen der geometrischen Parameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2012="Lesen der Strategieparameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2013="Fehler beim Lesen der Parameter.\n Parameter �berpr�fen.";
ERR_2014="Werkzeugbahn wurde nicht berechnet.\n Parameter �berpr�fen.";
ERR_2015="Untergruppe des Neigungsbereichs ist leer.\nDen Bereich pr�fen.";
ERR_2016="Falsche Daten f�r Untergruppe.\nDen Neigungsbereich erneut erzeugen.";

// Erreurs Hard Material Roughing
ERR_2020="Lesen des Werkzeugs nicht m�glich.\n Parameter �berpr�fen.";
ERR_2021="Lesen der geometrischen Parameter nicht m�glich.\n Parameter �berpr�fen";
ERR_2022="Lesen der Strategieparameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2023="Fehler beim Lesen der Parameter.\n Parameter �berpr�fen.";
ERR_2024="Werkzeugbahn wurde nicht berechnet.\n Parameter �berpr�fen.";
ERR_2025="Keine Schnitt-Tiefe.\n Einen positiven Wert eingeben.";
ERR_2026="Rohteil ist ung�ltig.\n Bitte ein anderes definieren.";
ERR_2027="Die radiale �berlappung muss kleiner oder gleich 99% des Werkzeugdurchmessers sein. \nDen �berlappungswert �ndern.";
ERR_2028="Helixmodus kann nicht ber�cksichtigt werden:\nHelixdurchmesser ist kleiner als der nicht schneidende Durchmesser des Werkzeugs.";

// Erreurs Contour Driven Contour
ERR_2029="Bitte Guide2 definieren.\n";
ERR_2030="Lesen der geometrischen Parameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2031="Lesen der Operation nicht m�glich.\n Parameter �berpr�fen.";
ERR_2032="Lesen der Strategieparameter nicht m�glich.\n Parameter �berpr�fen.";
ERR_2033="Lesen der Vorschubparameter nicht m�glich.\n Parameter �berpr�fen";
ERR_2034="Fehler beim Lesen der Parameter.\n Parameter �berpr�fen.";
ERR_2035="Werkzeugbahn wurde nicht berechnet.\n Parameter �berpr�fen.";
//ajout sur contour driven
ERR_2036="Bitte eine geschlossene Kontur ausw�hlen.";
ERR_2037="Bitte P1 definieren.\n";
ERR_2038="Bitte P2 definieren.\n";
ERR_2039="Bitte P3 definieren.\n";
ERR_2040="Bitte P4 definieren.\n";
ERR_2041="Problem beim Berechnen von Guide1.\n Bitte einen anderen Grenzpunkt ausw�hlen.";
ERR_2042="Problem beim Berechnen von Guide2.\n Bitte einen anderen Grenzpunkt ausw�hlen.";
ERR_2043="Problem beim Berechnen von Stopp1.\n Bitte einen anderen Grenzpunkt ausw�hlen.";
ERR_2044="Problem beim Berechnen von Stopp2.\n Bitte einen anderen Grenzpunkt ausw�hlen.";
ERR_2045="Gleiches CATCurv-Element in mehr als einer Kontur.\n Grenzpunkte �berpr�fen.";
ERR_2046="Gleiche Punkte definiert.\n Punkte erneut definieren";
ERR_2047="Bitte Guide1 definieren.\n";
ERR_2048="Es wurde ein vertikaler Filter angewendet - Kein vertikaler Bereich gefunden.";
ERR_2049="Es wurde ein horizontaler Filter angewendet - Kein horizontaler Bereich gefunden.";

// Erreurs Compute of Rework Area
ERR_2050="Beh�lter f�r Nachbesserungsbereich nicht gefunden.\n Bitte die Anwenderunterst�tzung benachrichtigen";
ERR_2051="Factory f�r Nachbesserungsbereich nicht gefunden.\n Bitte die Anwenderunterst�tzung benachrichtigen";
ERR_2052="Bitte ein Teil definieren.\n";
ERR_2053="Kein Ergebnis berechnet.\n Parameter �berpr�fen";
ERR_2054="L�ngenfilter kann nicht angewendet werden.\n Einen anderen Wert ausw�hlen.";
ERR_2055="Breitenfilter kann nicht angewendet werden.\n Einen anderen Wert ausw�hlen.";
ERR_2056="Auswahlkontur kann nicht angewendet werden.\n Einen anderen Wert ausw�hlen.";
ERR_2057="Nachbearbeitungsbereich wird berechnet. \nBerechnung unterbrochen.";

ERR_2060="Fehler in Richtung der Ansicht.\n Fehlerhafter Werkzeugtyp.\n Ein Werkzeug mit kugelf�rmiger Spitze ausw�hlen.";
ERR_2061="Ansichtsachse und Bearbeitungsrichtung d�rfen nicht parallel sein.";
ERR_2062="Wert darf nicht null sein\n";

// Erreurs Compute of Tool Length
ERR_2070="Diese Aktivit�t unterst�tzt keine Berechnung von Werkzeugl�ngen.\nDie Anwenderunterst�tzung benachrichtigen";
ERR_2071="F�r diese Aktivit�t ist keine Werkzeugbahn vorhanden.\nZuerst die Werkzeugbahn berechnen";
ERR_2072="Diese Aktivit�t unterst�tzt keine Strategieparameter.\nDie Anwenderunterst�tzung benachrichtigen";
ERR_2073="Algorithmus f�r die Mindestl�nge des Werkzeugs ist nicht verf�gbar.\nDie Anwenderunterst�tzung benachrichtigen";
ERR_2074="Kein Teil definiert.\nZum Pr�fen eine Geometrie definieren";
ERR_2075="Das Teil ist leer.\nZum Pr�fen eine Geometrie definieren";
ERR_2076="Interner Fehler im Algorithmus.\nAnwenderunterst�tzung benachrichtigen";
ERR_2077="Aktuelle Werkzeugl�nge betr�gt null.\nDie L�nge des Werkzeugs �ndern";
ERR_2078="Der Durchmesser der aktuellen Werkzeughalterung betr�gt null.\nDen Durchmesser der Werkzeughalterung �ndern";
ERR_2079="Interner Fehler.\n Einen kleineren Toleranzbereich nehmen.";
ERR_2080="Mehrere Teilfl�chen des Hauptk�rpers sind nicht g�ltig.\n Die Geometrie pr�fen oder eine andere Toleranz ausw�hlen.";
ERR_2081="Berechnung der Mindestwerkzeugl�nge: Die zu pr�fende Geometrie ist die des Konstruktionsteils.\nDie Geometrie des Konstruktionsteils in der Aufspannung definieren.";
ERR_2082="Zustellung/R�ckzug �ndern: Beim Konstruktionsteil werden die durch Makros hervorgerufenen Kollisionen gepr�ft. \nDie Geometrie des Konstruktionsteils in der Aufspannung definieren.";
ERR_2083="Zustellung/R�ckzug �ndern: Das Teil ist leer.\nDie durch Makros hervorgerufenen Kollisionen k�nnen nicht gepr�ft werden";
ERR_2084="Werkzeugachse ist nicht fixiert.\nMindestwerkzeugl�nge kann nicht berechnet werden";
ERR_2085="Kollisionserkennung fehlgeschlagen.\n Die Parameter �berpr�fen.";
ERR_2086="Trommelwerkzeug wird nicht unterst�tzt.";
ERR_2087="Kollision der gesamten Werkzeugbahn. Die Werkzeugbahn wird aus der anf�nglichen Operation entfernt.";
ERR_2088="Beim Hinzuf�gen von Makros zum Schlie�en der Werkzeugbahn wurden Kollisionen festgestellt.\nDen Sicherheitsabstand am Trennpunkt �ndern.";

// Erreurs compensation
ERR_2091="Korrekturmodus f�r 3D-Kontaktschneide ist nicht unterst�tzt. Diesen Parameter im Maschineneditor auf 'Nein' setzen.";
ERR_2092="Korrekturmodus f�r 3D-Kontaktschneide wird bei dieser Operation nicht unterst�tzt";
ERR_2093="F�r den Korrekturmodus f�r die 3D-Kontaktschneide ist ein Kugel- oder Torusfr�ser erforderlich.";
ERR_2094="Korrekturmodus f�r 3D-Kontaktschneide wird bei dieser Operation nicht unterst�tzt, wenn eine Offsetgruppe definiert ist.";
ERR_2095="Korrekturmodus f�r 3D-Kontaktschneide wird bei dieser Operation nicht unterst�tzt, wenn ein Aufma� auf dem Teil definiert ist.";
ERR_2096="Korrekturmodus f�r 3D-Kontaktschneide: \n      Anzahl der aktualisierten Kontaktpunkte: /p1 \n      Maximale Abweichung: /p2";

// Nouvelles erreurs
ERR_3000="Eine F�hrungskontur definieren.\n";
ERR_3001="Werkzeugdurchmesser betr�gt null.\n Das Werkzeug wechseln.";
ERR_3002="Eine Bahnabstandslinie definieren oder den Bahnabstandsmodus �ndern.\n";
ERR_3003="/p1 Teilfl�chen des zu bearbeitenden Teils oder des Kontext sind ung�ltig.\nJede ung�ltige Teilfl�che ist rot hervorgehoben und mit einem Pfeil gekennzeichnet.";
ERR_3004="Keine freie Kante festgestellt.\n";
ERR_3005="Kein Ziel definiert.\n";
ERR_3006="Richtung betr�gt null.\nAndere Richtung ausw�hlen.";
ERR_3007="Fehler in der Vorschubverwaltung des Sicherheitsbereichsmakros.\nAndere Vorsch�be ausw�hlen.";
ERR_3008="Laden aus dieser Operation nicht m�glich, da Konstruktion auf Aufspannungsebene festgelegt ist.";

// Erreurs MultiPocketFlank
ERR_3100="Die vorgegebenen Ebenen m�ssen senkrecht zur Richtung der Ansicht verlaufen.";
ERR_3101="Die obere Ebene muss senkrecht zur Richtung der Ansicht verlaufen.";
ERR_3102="Die untere Ebene muss senkrecht zur Richtung der Ansicht verlaufen.";
ERR_3103="Inkompatibler Schlichtmodus.\nEine Teileunterseite ausw�hlen.";

// Message envoy�s lors de la destruction du r�sultat de calcul de la RA et de la SA
ERR_4001="Berechnetes Ergebnis wird entfernt. \nEnde des Befehls";
ERR_4002="Ergebnis f�r die Komponente wird entfernt. \nKeine Komponente ausgew�hlt.";

// Message envoy� lors d'upgrade d'une ebauche si une formule existe sur le parametre Offset des lignes lmites
ERR_4100="Aufma� f�r die Definition der Parameterbegrenzung. \nEine �nderung des Betriebsmodus kann das Formelergebnis �ndern.\nDie Formel pr�fen.";

// Erreurs Algo (a partir de 10000)
ERR_10001="Ein negativer Offset mit einem absoluten Wert,\nder gr��er ist als der Werkzeugradius, ist nicht zul�ssig. \n Einen der Werte �ndern.\n";
ERR_10002="Nicht gen�gend Speicherplatz auf dem Datentr�ger zum Schreiben der Datei.\n  Alle nicht verwendeten tempor�ren Dateien im Verzeichnis /p1 l�schen.\nOder\nDie Datentr�ger hat die maximale Gr��e erreicht.";
ERR_10003="Begrenzungskontur ist nicht g�ltig.\n  Eine andere Kontur ausw�hlen.";
ERR_10004="Begrenzungskontur ist nicht g�ltig, da sie sich selbst schneidet.\n  Eine andere Kontur ausw�hlen.";
ERR_10005="Begrenzungskontur ist nicht g�ltig, da sie eine andere Kontur schneidet.\n  Eine andere Kontur ausw�hlen.";
ERR_10006="Begrenzungskontur ist nicht g�ltig, da sie von einer anderen Kontur �berblendet wird.\n  Eine andere Kontur ausw�hlen.";
ERR_10007="Ausgew�hlter Modus 'Zwischen Arbeitsg�ngen' ist nur f�r die Bearbeitungsstrategie 'Zickzack' zul�ssig.\n Einen anderen Modus ausw�hlen.";
ERR_10008="Verschiedene Teilfl�chen des zu bearbeitenden Teils oder des Kontextes sind ung�ltig.\nBitte die folgende Geometrie pr�fen: /p1";
ERR_10009="Sicherheitsebene und Werkzeugachse m�ssen in der Schruppoperation senkrecht zueinander liegen.\n Eine andere Ebene ausw�hlen.";
ERR_10010="Ung�ltige Bearbeitungstoleranz.\n Einen positiven Wert eingeben.";
ERR_10011="Ung�ltige Bahnabstandslinien.";
ERR_10012="Der Abstand auf der Kontur ist gr��er als die L�nge der Linien f�r den Bahnabstand.";
ERR_10013="Die Linien f�r den Bahnabstand k�nnen nicht auf das Teil projiziert werden. Zun�chst dessen Position �berpr�fen. Andernfalls muss der 'Abstand auf Kontur' verringert werden.";
ERR_10014="Ein negatives 'Aufma� auf Hindernis', das gr��er ist als das 'Aufma� auf Teil', ist nicht zul�ssig.";
ERR_10015="Das Delta zwischen 'Aufma� auf Hindernis' und 'Aufma� auf Teil'\n darf nicht gr��er sein als der Werkzeugradius oder der Eckenradius des Werkzeugs.";
ERR_10016="Ein negatives 'Aufma� auf Hindernis' darf keinen absoluten Wert haben,\n der gr��er ist als der Werkzeugradius oder der Eckenradius des Werkzeugs.";
ERR_10017="Die Offsetgruppe im Teil ist nicht kompatibel mit einem negativen Offset im Teil,\n der gr��er als der Eckenradius des Werkzeugs ist.";
ERR_10018="Begrenzungskontur ist nicht g�ltig, da sie sich auf einer Ebene parallel zur Werkzeugachse befindet.\n Eine andere Kontur ausw�hlen.";
ERR_11001="Material nicht geschlossen. \n Stattdessen wird automatischer Zeichenrahmen verwendet.";
ERR_11002="Zu bearbeitendes Teil oder Kontrollelement ist eine Punktewolke. Diese ist nicht mit der Erkennung des horizontalen Bereichs kompatibel.\n Bitte ein anderes Teil oder Kontrollelement ausw�hlen.";
ERR_11003="Die Aufma�gruppe am Teil oder Kontrollelement ist nicht mit der Erkennung des horizontalen Bereichs kompatibel.\n Bitte keine Aufma�gruppe verwenden.";
ERR_11004="Das Aufma� auf den horizontalen Bereich darf nicht gr��er sein als das Aufma� auf das Teil.\n Bitte einen anderen Wert ausw�hlen.";
ERR_11005="Die Erkennung des horizontalen Bereichs steht nicht zur Verf�gung, wenn das Aufma� auf das Teil\n und das Aufma� auf das Kontrollelement nicht denselben Wert aufweisen.";
ERR_11006="Der Werkzeugkerndurchmesser ist nicht kompatibel mit mehreren ansteigenden Bewegungen.";
ERR_11007="Unm�gliche ansteigende Zustellung f�r die ausgew�hlte Begrenzungskontur";
ERR_11008="Unm�gliche ansteigende Zustellung. Die Gr��e des Fr�sbereichs und der Kerndurchmesser sind nicht kompatibel.";
ERR_11009="Der Werkzeugkerndurchmesser ist mit einigen ansteigenden Bewegungen nicht kompatibel.\n Mit 'Am Ende stehen bleiben' versuchen.";
ERR_11010="Der Werkzeugkerndurchmesser ist mit einigen ansteigenden Bewegungen nicht kompatibel.\n Mit einem kleineren Eintauchwinkel versuchen.";
ERR_11011="Einige Zustellungen und R�ckzugsbewegungen f�r Seitenkonturen k�nnen wegen eines Kollisionsereignisses nicht berechnet werden.";
ERR_12000="Kein horizontaler, zu bearbeitender Bereich.";
ERR_12001="Der radiale Bahnabstand muss kleiner gleich dem folgenden Wert sein: Werkzeugdurchmesser + 2*Aufma� auf Teil.";
ERR_14000="In der Zwischenzeit k�nnen\n CATClouds nicht ausgew�hlt werden, wenn die Auswahl des horizontalen Bereichs auf 'Automatisch' gesetzt ist.";
ERR_14001="Konisches Werkzeug ist nicht zul�ssig, wenn die Auswahl des horizontalen Bereichs auf 'Automatisch' gesetzt ist. \n Einen anderen Werkzeugtypen ausw�hlen.";
ERR_15000="F�hrungskonturen sind zu komplex.\n Andere Konturen ausw�hlen.";
ERR_15001="Nachbearbeitung mit maximaler Schnitt-Tiefe vornehmen.\n Die Werkzeugachse f�r die Nachbearbeitung muss die gleiche sein wie die Werkzeugachse f�r den Nachbearbeitungsbereich.";
ERR_16000="Selbstschneidung auf Kontur.\n Eine andere Kontur ausw�hlen.\n";
ERR_16001="Kontur ist nicht mit Werkzeugachse kompatibel.\n Eine andere Kontur ausw�hlen.\n";
ERR_16002="Kontur ist nicht stetig.\n Eine andere Kontur ausw�hlen.\n";
ERR_16003="Offset auf Kontur nicht kompatibel mit F�hrungskontur. \n Eine andere Kontur ausw�hlen oder den Offset �ndern.\n";
ERR_16004="Mit dem Nachbearbeitungsbereich nicht kompatibles Nachbearbeitungswerkzeug.\n Ein anderes Werkzeug ausw�hlen.";
ERR_16005="F�hrungskontur nicht definiert.\n";
ERR_16006="Inkompatibler negativer Offset oder Position auf Kontur mit Stoppkonturen.";
ERR_16007="Bitangentenstetigkeit nicht berechnet.\nDen Werkzeugdurchmesser oder die Geometrie �berpr�fen.";
ERR_16008="Der Konturdurchlauf kann nicht berechnet werden.\nDie Geometrie �berpr�fen.";
ERR_16009="Nicht zug�nglicher Abschnitt der Kontaktf�hrungskontur mit dieser Werkzeugachse (unter dem Schnitt).";
ERR_16010="Fehlerhafte Definition der Kontaktf�hrungskontur.";
ERR_16011="Die Konturen von F�hrungselement und Inseln m�ssen geschlossen sein.";

ERR_16500.Request="Berechnung fehlgeschlagen: Kontaktpunkte fehlen.";
ERR_16500.Diagnostic="F�r den 3-nach-5-Achsenumsetzer sind die Kontaktpunkte der Operation erforderlich.";
ERR_16500.Advice="Die Befehle 'Tools' > 'Optionen...' > 'Bearbeitung' > 'Ausgabe' > 'Kontaktpunkte in Werkzeugbahn speichern' \n ausw�hlen und die Berechnung der Operation erzwingen.";
ERR_16501="Berechnung fehlgeschlagen: Kollisionen wurden nicht behoben.";

// Message d'attention : a partir de 17000
ERR_17000="Teilweises Eintauchen des Werkzeugs in Bohrung\nauf Grund des Schnittbereichs der Werkzeugspitze.";
ERR_17050="Bei der Berechnung der Werkzeugbahn wurden mehrere verdeckte K�rper ber�cksichtigt. \n Wenn dieses Ergebnis nicht erw�nscht ist, die Auswahl der verdeckten K�rper zur�cknehmen.";
ERR_17051="In HSM und im Helixmodus muss der Eckenradius kleiner sein als die H�lfte des \n Bahnabstands. Dieser Wert wird erzwungen.";
ERR_17052="Der Parameter 'Ung�ltige Teilfl�chen ignorieren' (Ignore invalid faces) ist auf 'Ja' gesetzt. \nUng�ltige Teilfl�chen des zu bearbeitenden Teils und des Kontexts werden nicht berechnet.\n";
ERR_17053="Im Fall von konturgef�hrtem Fl�chenfr�sen mit Stift: \nDie Option 'Kontaktpunkte in Werkzeugbahn speichern' wird ignoriert.\n";
ERR_17054="Das Rohteil ist f�r eine abschlie�ende Schruppnachbearbeitung nicht geeignet.\n";
ERR_17055="Die Begrenzungskontur ist auf Grund von /p1 Bohrungen nicht geschlossen.\nWenn das erzielte Ergebnis nicht mit dem erwarteten �bereinstimmt, die Toleranz f�r die Kontur auf /p2 erh�hen oder die L�cken f�llen.";

// Message des algos sur ZLevel
ERR_18000="Kein vertikaler Bereich zum Bearbeiten";
ERR_18010="- Fehlerhafte Werkzeugdefinition\nBeide Ecken (Rc und Rc2) m�ssen den gleichen Radius aufweisen.";
ERR_18011="- Fehlerhafte Werkzeugdefinition\nBeide Fasenwinkel m�ssen null betragen.";
ERR_18012="- Fehlerhafte Werkzeugdefinition\nDer nominale Durchmesser muss gr��er sein als der Durchmesser des Werkzeugk�rpers + 2 * Aufma� auf den Werkzeugschaft.";
ERR_18020="Fehler in den NC_CUTCOM-Anweisungen:\nEine NC_CUTCOM_OFF-Anweisung wird ohne NC_CUTOM_ON-Anweisung verwendet.";
ERR_18021="Fehler in den NC_CUTCOM-Anweisungen:\nDie Anzahl der NC_CUTOM_ON- und NC_CUTCOM_OFF-Anweisungen stimmt nicht �berein.";

// Message des algos sur Plunge Milling
ERR_18500="Rohteil ist ung�ltig f�r das Tauchfr�sen.\n Bitte ein anderes Rohteil definieren.\n";
ERR_18501="F�r das beim Tauchfr�sen verwendete Rohteil k�nnen keine Begrenzungen berechnet werden.\n Bitte ein anderes Rohteil definieren.\n";
ERR_18502="L�ngsschritt ist gr��er als: (Werkzeugdurchmesser - Nicht schneidender Durchmesser)/2.\n Bitte eine andere Definition vornehmen.\n";
ERR_18503="Die Nutbreite liegt �ber dem doppelten Werkzeugdurchmesser.\n Bitte eine andere Nutbreite definieren.\n";
ERR_18504="Der maximale Schnittfortschritt oder der seitliche Schritt ist null.\n Bitte eine andere Definition vornehmen.\n";
ERR_18505="�berlappung von Nut betr�gt null.\n Bitte eine andere �berlappung definieren.\n";
ERR_18506="�berlappung von Nut ist gr��er als: (Werkzeugdurchmesser - Nicht schneidender Durchmesser)/2.\n Bitte eine andere Definition vornehmen.\n";
ERR_18507="Gitter kann nicht berechnet werden.\n Das Rohteil und die Gitterintervalle pr�fen.\n";
ERR_18508="Werkzeugdurchmesser + 2 * Aufma� auf Seite - 2 * Aufma�/Bodenfl�che muss einen positiven Wert ergeben.\n Bitte einen dieser Aufma�werte �ndern.\n";
ERR_18509="L�ngsschritt des Gitters ist gr��er als der Werkzeugdurchmesser.\nBitte einen anderen Wert definieren.\n";
ERR_18510="Der maximale Schnittfortschritt ist gr��er als der Werkzeugdurchmesser.\nBitte einen anderen Wert definieren.\n";
ERR_18511="�berlappung von Nut ist gr��er als Werkzeugdurchmesser.\nBitte einen anderen Wert definieren.\n";
ERR_18512="Der maximale Schnittfortschritt ist gr��er als: (Werkzeugdurchmesser - Nicht schneidender Durchmesser)/2.\n Bitte eine andere Definition vornehmen.\n";
ERR_18513="Der fertige Schnittfortschritt ist gr��er als: (Werkzeugdurchmesser - Nicht schneidender Durchmesser)/2.\n Bitte eine andere Definition vornehmen.\n";
ERR_18514="Der fertige Schnittfortschritt ist gr��er als der Werkzeugdurchmesser.\nBitte einen anderen Wert definieren.\n";
ERR_18515="Der Offsetwert ist gr��er als der Werkzeugdurchmesser.\nBitte einen anderen Wert definieren.\n";
ERR_18516="Der Offsetwert muss gr��er als 0 sein.\nBitte einen anderen Wert definieren.\n";
ERR_18517="Die Nummer der Kontur ist null.\nBitte einen anderen Wert definieren.\n";
ERR_18518="Fehler bei der Berechnung der axialen Eckendaten.\nDer Eckenradius ist gr��er als die Eintauchl�nge.\nEinen anderen Radius angeben.\n";

// Warning des algos a partir de 19000
ERR_19000="HSM - Eckenradius wird nicht �berall beachtet";
ERR_19001="In HSM und im Helixmodus muss der Eckenradius kleiner sein als die H�lfte des \n Bahnabstands. Dieser Wert wurde erzwungen.";
ERR_19002="R�ckzugsbewegung optimieren: Die zwischen dem Werkzeug und dem anf�nglichen oder aktuellen Rohteil bestehenden Kollisionen wurden nicht gepr�ft.";
ERR_19003="Makros 'Zwischen Arbeitsg�ngen' sind nur im Zickzackmodus aktiv.";
ERR_19004="Auf Grund eines Kollisionsereignisses wurden Makros durch eine Benutzeranfrage ge�ndert.";
ERR_19005="Werkzeug der Operation weicht vom Werkzeug f�r Neigungsbereiche ab.\n Die Einhaltung von Bedingungen f�r Neigungsbereiche ist nicht gew�hrleistet.";
ERR_19006="Wert der Bearbeitungstoleranz kann nicht mehr als die H�lfte des Bahnabstands betragen. \n Die Bearbeitungstoleranz wird in Bahnabstand/2 ge�ndert";
ERR_19007="Die Offsetgruppe wird bei der Berechnung der \n Operation ber�cksichtigt.";
ERR_19008="Alle horizontalen Bereiche werden als Taschen angesehen. Es empfiehlt sich daher, den Bearbeitungsmodus '�u�eres Teil und Taschen' zu definieren.";
ERR_19009="Ein negativer Offset, der gr��er ist als der Eckenradius des Werkzeugs, funktioniert nur f�r vertikale und horizontale Bereiche.";
ERR_19010="Ein Zur�ckziehen entlang einer Helixlinie ist nicht m�glich. Die R�ckzugsbewegung wurde in 'Entlang Werkzeugachse' ge�ndert.";
ERR_19011="Ein Offset bei der Steigungszustellung bei einem Konturdurchlauf wurde nicht berechnet.";
ERR_19012="Einige Zustellungen und R�ckzugsbewegungen f�r Seitenkonturen wurden wegen eines Kollisionsereignisses nicht berechnet.";
ERR_19013="Einige Geraden zwischen Bahnen k�nnen nicht berechnet werden.";
ERR_19014="Geometrie des Hindernisses ist leer: mehrere Elemente werden ignoriert.";
ERR_19015="Die Bearbeitungsstrategie f�r au�en und die Bearbeitungsstrategie f�r die Tasche sind nicht mit aktivierter HSM-Option kompatibel.\n Die HSM-Option wurde inaktiviert.";
ERR_19016="Die Bearbeitungsstrategie f�r Au�en ist mit der aktivierten Option f�r vollst�ndige Materialverwaltung nicht kompatibel.";
ERR_19017="Die Bearbeitungsstrategie f�r das Taschenfr�sen ist mit der aktivierten Option f�r vollst�ndige Materialverwaltung nicht kompatibel.";
ERR_19018="Um Kollisionen der Werkzeughalterung mit den ausgew�hlten Geometrien zu beheben,\n wurden einige Positionen in der Werkzeugbahn entfernt und \n durch eine �ber lokale Makros definierte Verbindungsbewegung ersetzt.";
ERR_19019="Es werden nur die F�hrungskontur, an der der Startpunkt definiert ist,\n und die zugeh�rigen inneren Inseln bearbeitet.";

