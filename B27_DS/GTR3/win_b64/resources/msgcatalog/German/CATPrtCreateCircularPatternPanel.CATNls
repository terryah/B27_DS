LongHelp="Definiert das Kreismuster.";

FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2n.Title="Kreis(e):";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1n.Title="Exemplar(e):";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2s.Title="Kreisabstand:";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1s.Title="Winkelabstand:";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2l.Title="Kranzstärke:";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1l.Title="Gesamtwinkel:";
FrmMore.PositionningDefinition.CkeLabelV.Title="Position in radialer Richtung:";
FrmMore.PositionningDefinition.CkeLabelU.Title="Position in Winkelrichtung:";
FrmMore.PositionningDefinition.CkeLabelXY.Title="Rotationswinkel:";

InstanceAndAngularSpacing="Exemplar(e) & Winkelabstand";
AngularSpacingAndTotalAngle="Winkelabstand & Gesamtwinkel";
CompleteCrown="Vollständiger Kranz";
UnequalAngularSpacing="Exemplar(e) & ungleicher Winkelabstand";

CircleAndCrownThickness="Kreis(e) & Kranzstärke";
CircleAndCircleSpacing="Kreis(e) & Kreisabstand";
CircleSpacingAndCrownThickness="Kreisabstand & Kranzstärke";
InstanceAndTotalAngle="Exemplar(e) & Gesamtwinkel";

CircularPatternPanelTitle="Kreismusterdefinition";

FrmRoot.FrmMain.EulerianPattern.AxialReference.Title="Axialreferenz";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.FirstDrivingType.Title="Parameter:";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.FirstDrivingType.LongHelp="Gibt den zum Definieren des Musters erforderlichen Parameter
an: Exemplar(e) und Gesamtwinkel,
Exemplar(e) und Winkelabstand, Winkelabstand
und Gesamtwinkel oder vollständiger Kranz oder
Exemplar(e) und ungleicher Winkelabstand.";

FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.FirstDrivingby.LongHelp="Gibt den zum Definieren des Musters erforderlichen Parameter
an: Exemplar(e) und Gesamtwinkel,
Exemplar(e) und Winkelabstand, Winkelabstand
und Gesamtwinkel oder vollständiger Kranz oder
Exemplar(e) und ungleicher Winkelabstand.";

FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.Title="Referenzrichtung";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.LongHelp="Referenzrichtung\nDefiniert die Achse des Musters";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.FraRefDir.FraEdt.Lab.Title="Referenzelement:";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.FraRefDir.LongHelp="Gibt die Teilfläche, zylindrische Teilfläche, Kante, Achse, Linie, Ebene oder den Punkt an, mit dem die Musterrichtung definiert
werden
soll.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.FraRefDir.FraEdt.Lab.LongHelp="Gibt die Teilfläche, zylindrische Teilfläche, Kante, Achse, Linie, Ebene oder den Punkt an, mit dem die Musterrichtung definiert
werden
soll.";

FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.Reverse.Title="Umkehren";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.Reverse.LongHelp="Kehrt die Achse des Musters um.";

FrmRoot.FrmMain.EulerianPattern.Crown.Title="Kranzdefinition";

FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.SecondDrivingType.Title="Parameter:";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.SecondDrivingType.LongHelp="Gibt den zum Definieren des Musters erforderlichen Parameter an:
Kreis(e) und Kranzstärke, Kreis(e) und Kreisabstand oder Kreisabstand
und Kranzstärke";

FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.SecondDrivingBy.LongHelp="Gibt den zum Definieren des Musters erforderlichen Parameter an:
Kreis(e) und Kranzstärke, Kreis(e) und Kreisabstand oder Kreisabstand
und Kranzstärke";

FrmMore.PositionningDefinition.Title="Position des Objekts im Muster";
FrmMore.PositionningDefinition.LongHelp="Definiert die ursprüngliche Position des Objekts.";

FrmMore.SimpleGeomDefinition.Title="Schraffurmusterdarstellung";
FrmMore.SimpleGeomDefinition.LongHelp="Definiert den Darstellungsmodus so, dass die Teilegeometrie zur Wahrung einer hohen Leistung möglichst einfach bleibt.";

FrmMore.SimpleGeomDefinition.CheckSimpleGeom.Title="Vereinfachte Darstellung";
FrmMore.SimpleGeomDefinition.CheckSimpleGeom.LongHelp="Wenn diese Option aktiviert ist, werden nur die ersten und die letzten vier Exemplare in jeder Richtung der Mustererzeugung berechnet und
dargestellt.";


FrmMore.RotationInstanceDefinition.Title="Rotation des Exemplars/der Exemplare";
FrmMore.RotationInstanceDefinition.LongHelp="Rotation der Exemplare\nDefiniert die Ausrichtung jedes Exemplars.";

FrmMore.RotationInstanceDefinition.RotationStatus.Title="Radiale Ausrichtung des Exemplars/der Exemplare ";
FrmMore.RotationInstanceDefinition.RotationStatus.LongHelp="Bei Auswahl dieser Option bleiben alle Exemplare senkrecht zu Linien,		
die tangential zu Kreisen liegen. 
Wenn die Auswahl dieser Option aufgehoben wird, behalten alle			
Exemplare dieselbe Ausrichtung wie das jeweilige ursprüngliche Objekt."; // @validatedUsage OZL 2013/11/19 option


FrmMore.StaggeredPattern.Title="Definition des gestaffelten Musters";
FrmMore.StaggeredPattern.LongHelp="Definiert die Eingaben für das gestaffelte Muster."; // @validatedUsage OZL 2013/10/30 tag
FrmMore.StaggeredPattern.CkeLabelSS.Title="Staffelungswinkel:"; // @validatedUsage OZL 2013/10/30 option
FrmMore.StaggeredPattern.CkeLabelSS.LongHelp="Definiert den Wert des Staffelungswinkels. Dieser Wert muss kleiner als der Winkel zwischen Exemplaren in der axialen Richtung sein."; // @validatedUsage OZL 2013/10/30 tag
FrmMore.StaggeredPattern.StaggeredPattern.Title="Gestaffelt"; // @validatedUsage OZL 2013/10/30 tag
FrmMore.StaggeredPattern.StaggeredPattern.LongHelp="Aktivieren Sie diese Option, um Exemplare alternativer Kranzkreise des Kreismusters 'Gestaffelt' anhand eines bestimmten Winkels in Axialrichtung zu erzeugen."; // @validatedUsage OZL 2013/10/30 option tag
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1n.LongHelp="Gibt die Anzahl der Exemplare für die Winkelrichtung an.
In dieser Anzahl ist das ursprüngliche Objekt enthalten.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.Instance.EnglobingFrame.IntermediateFrame.LongHelp="Gibt die Anzahl der Exemplare für die Winkelrichtung an.
In dieser Anzahl ist das ursprüngliche Objekt enthalten.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2n.LongHelp="Gibt die Anzahl der Exemplare für die Radialrichtung an.
In dieser Anzahl ist das ursprüngliche Objekt enthalten.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.Circle.EnglobingFrame.IntermediateFrame.LongHelp="Gibt die Anzahl der Exemplare für die Radialrichtung an.
In dieser Anzahl ist das ursprüngliche Objekt enthalten.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1s.LongHelp="Gibt den Winkelabstand zwischen zwei Exemplaren an.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.AngularSpacing.EnglobingFrame.IntermediateFrame.LongHelp="Gibt den Winkelabstand zwischen zwei Exemplaren an.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2s.LongHelp="Gibt den Radialabstand zwischen zwei Exemplaren an.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CircleSpacing.EnglobingFrame.IntermediateFrame.LongHelp="Gibt den Radialabstand zwischen zwei Exemplaren an.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1l.LongHelp="Gibt den zu verwendenden Gesamtwinkel an, in dem 
alle Exemplare für die Winkelrichtung positioniert werden sollen.
Gesamtwinkel ist die Summe der Winkelabstände zwischen einzelnen Exemplaren\nvom ersten bis zum letzten Exemplar.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.TotalAngle.EnglobingFrame.IntermediateFrame.LongHelp="Gibt den beim Positionieren aller Exemplare in
Winkelrichtung zu verwendenden Gesamtwinkel an.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2l.LongHelp="Gibt die beim Positionieren aller Exemplare in
Radialrichtung zu verwendende Gesamtlänge an.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CrownThickness.EnglobingFrame.IntermediateFrame.LongHelp="Gibt die beim Positionieren aller Exemplare in
Radialrichtung zu verwendende Gesamtlänge an.";
FrmMore.PositionningDefinition.CkeLabelU.LongHelp="Gibt die Position des Originalobjekts in Winkelrichtung an.";
FrmMore.PositionningDefinition.RowinAngularDirection.EnglobingFrame.IntermediateFrame.LongHelp="Gibt die Position des Originalobjekts in Winkelrichtung an.";
FrmMore.PositionningDefinition.CkeLabelV.LongHelp="Gibt die Position des Originalobjekts in Radialrichtung an.";
FrmMore.PositionningDefinition.RowinRadialDirection.EnglobingFrame.IntermediateFrame.LongHelp="Gibt die Position des Originalobjekts in Radialrichtung an.";
FrmMore.PositionningDefinition.CkeLabelXY.LongHelp="Gibt die Ausrichtung des Originalobjekts entsprechend den Richtungen des Musters an.";
FrmMore.PositionningDefinition.RotationAngle.EnglobingFrame.IntermediateFrame.LongHelp="Gibt die Ausrichtung des Originalobjekts entsprechend den Richtungen des Musters an.";

FrmMore.StaggeredPattern.CheckHalfStagStep.Title="Hälfte des Abstands festlegen";
FrmMore.StaggeredPattern.CheckHalfStagStep.LongHelp="Wenn diese Option aktiviert ist, wird für den Staffelungswinkel die Hälfte des Winkelabstands festgelegt."; // @validatedUsage OZL 2013/10/30 tag

