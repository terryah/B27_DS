// =====================================================================
//
// MIK common NLS-File
//
// Syntax:
//   <ModuleId>_<MsgId>.<..> = "Text";
//   where 
//     ModuleId: MIK     -> common MIK Messages (MIK1 + MIK CE)
//               MIK1    -> only MIK1 Messages
//               MIKCE   -> only MIK CE Messages
//     MsgId   : PB      -> Progressbar Text
//               EMxxxx  -> Error/Message + Number
//
// Limitations:
//    # max. nb. of parameters supported: /P1 .. /P25
//
// =====================================================================

ERR_39999.Request    = "Generierung von Simulationsdaten";
ERR_39999.Diagnostic = "/p1";
ERR_39999.Advice     = "Prozess pr�fen.";

// ==================================================================
// Common Messages
// ==================================================================

MIK1_Version = "Version (MIK1):";

MIKCE_Version = "Version (MIK CE):";

// ==================================================================
// Progressbar
// ==================================================================

MIK_PB0000 = "Init...";

// ==================================================================
// Information messages (10000..19999)
// ==================================================================

MIK1_EM10000.iSev = "1";
MIK1_EM10000.sMsg = "Anwendungsinformationen";
MIK1_EM10000.lMsg = "Erfolg: MIK1 wurde ohne Fehler oder Warnungen zur�ckgegeben.";

MIKCE_EM10000.iSev = "1";
MIKCE_EM10000.sMsg = "Anwendungsinformationen";
MIKCE_EM10000.lMsg = "Erfolg: MIK CE wurde ohne Fehler oder Warnungen zur�ckgegeben.";

// ==================================================================
// Warning messages (30000..39999)
// ==================================================================

MIK_EM30100.iSev = "3";
MIK_EM30100.sMsg = "Anwendungswarnung";
MIK_EM30100.lMsg = "Definition von Modulname in Parameterdatei (/P1) wird f�r MIK nicht unterst�tzt.\nDefinition wird ignoriert.";

MIK_EM30101.iSev = "3";
MIK_EM30101.sMsg = "Anwendungswarnung";
MIK_EM30101.lMsg = "Definition von Zugangsname cPost in Parameterdatei (/P1) wird f�r MIK nicht unterst�tzt.\nDefinition wird ignoriert.";

MIK_EM30102.iSev = "3";
MIK_EM30102.sMsg = "Anwendungswarnung";
MIK_EM30102.lMsg = "Ung�ltige Option f�r #MIK_DUMP_MODE in Parameterdatei:\n/P1\nZeile: /P2.";

MIK_EM30103.iSev = "3";
MIK_EM30103.sMsg = "Anwendungswarnung";
MIK_EM30103.lMsg = "*********************************************************\n
                     Ung�ltige cPost-Version:\n
                     /P1\n
                     ...Programm k�nnte abst�rzen!!!\n
                     *********************************************************";

MIK_EM30104.iSev = "3";
MIK_EM30104.sMsg = "Anwendungswarnung";
MIK_EM30104.lMsg = "Kein Warmstart f�r mce m�glich.";

MIK_EM30105.iSev = "3";
MIK_EM30105.sMsg = "Anwendungswarnung";
MIK_EM30105.lMsg = "Es wird versucht, Eintrag 'cPostGetMAPtable' aus cPost-Bibliothek (<V1R6 SP2) abzurufen.";

MIK_EM30106.iSev = "3";
MIK_EM30106.sMsg = "Anwendungswarnung";
MIK_EM30106.lMsg = "mce-Bibliothek ist ung�ltig, Simulationsdaten werden von cPost generiert.";

MIK_EM30107.iSev = "3";
MIK_EM30107.sMsg = "Anwendungswarnung";
MIK_EM30107.lMsg = "Das Auslesen der Standardeinstellungen aus der Parameterdatei ist nicht m�glich.\n -> Es werden die Standardwerte festgelegt.";

MIK_EM30108.iSev = "3";
MIK_EM30108.sMsg = "Anwendungswarnung";
MIK_EM30108.lMsg = "cPost-Fehlermanagement kann nicht initialisiert werden.\n-> Siehe cPost-Berichtdatei f�r cPost-Laufzeitnachrichten.";

MIK_EM30109.iSev = "3";
MIK_EM30109.sMsg = "Anwendungswarnung";
MIK_EM30109.lMsg = "Warmstartdaten f�r Aktivit�t /P1 nicht gesichert.\n";

MIK_EM30110.iSev = "3";
MIK_EM30110.sMsg = "Anwendungswarnung";
MIK_EM30110.lMsg = "Kein Fehlermanagement f�r mce verf�gbar.";

MIK_EM30111.iSev = "3";
MIK_EM30111.sMsg = "Anwendungswarnung";
MIK_EM30111.lMsg = "Abrufen der Werkzeugaufspannposition null aus 'DNBIVncMachineKinDetails' ist fehlgeschlagen (SETUPD=0/0/0).";

MIK_EM30112.iSev = "3";
MIK_EM30112.sMsg = "Anwendungswarnung";
MIK_EM30112.lMsg = "Abrufen von 'DNBIVncMachineKinDetails' aus 'NCMachine' ist fehlgeschlagen (SETUPD=0/0/0).";

MIK_EM30113.iSev = "3";
MIK_EM30113.sMsg = "Anwendungswarnung";
MIK_EM30113.lMsg = "Unterbrechung der Tests zum Schlie�en von Bibliothek /P1 nach 21266 Fehlern.";

MIK_EM30114.iSev = "3";
MIK_EM30114.sMsg = "Anwendungswarnung";
MIK_EM30114.lMsg = "ISO-Datei konnte nicht erzeugt werden. Einstellungen f�r Verzeichnis 'NCCode' pr�fen!\n
                     Verzeichnis: /P1";

MIK_EM30115.iSev = "3";
MIK_EM30115.sMsg = "Anwendungswarnung";
MIK_EM30115.lMsg = "Erzeugung eines Links zur ISO-Datei f�r Aktivit�t /P1 fehlgeschlagen.";

MIK_EM30116.iSev = "3";
MIK_EM30116.sMsg = "Anwendungswarnung";
MIK_EM30116.lMsg = "Erzeugung der ISO-Datei fehlgeschlagen. Die Einstellungen f�r Verzeichnis 'NCCode' pr�fen!";

MIK_EM30117.iSev = "3";
MIK_EM30117.sMsg = "Anwendungswarnung";
MIK_EM30117.lMsg = "Parameterdatei kann nicht ge�ffnet werden: /P1.\nDatei wird aus dem Startverzeichnis f�r die Fertigung geladen.";

MIK_EM30118.iSev = "3";
MIK_EM30118.sMsg = "Anwendungswarnung";
MIK_EM30118.lMsg = "Abrufen der aktuellen Werkzeugaufspannposition aus 'DNBIVncMachineKinDetails' ist fehlgeschlagen (festgelegt auf 0/0/0).";


// MIK CE
MIK_EM30201.iSev = "3";
MIK_EM30201.sMsg = "Anwendungswarnung";
MIK_EM30201.lMsg = "Ung�ltige Werte f�r ToolNumber/CorrectorRegister (/P1,/P2) von MCE zur�ckgegeben. AssociateToolToDOFTargets() �bersprungen.";

MIK_EM30202.iSev = "3";
MIK_EM30202.sMsg = "Anwendungswarnung";
MIK_EM30202.lMsg = "Ung�ltige Start-/Endnummer (/P1,/P2) von MCE zur�ckgegeben. AssociateToolToDOFTargets() �bersprungen.";

// ==================================================================
// Error messages (50000..59999)
// 
// ==================================================================
//MIK_EM50000.iSev = "5";
//MIK_EM50000.sMsg = "Internal Error";
//MIK_EM50000.lMsg = "RESERVED FOR INTERNAL ERROR MESSAGES";

MIK_EM50100.iSev = "5";
MIK_EM50100.sMsg = "Interner Fehler";
MIK_EM50100.lMsg = "MIKInit in nicht unterst�tztem Modus (=/P1) aufgerufen.";

MIK_EM50101.iSev = "5";
MIK_EM50101.sMsg = "Interner Fehler";
MIK_EM50101.lMsg = "'mparams' nicht initialisiert.";

MIK_EM50102.iSev = "5";
MIK_EM50102.sMsg = "Interner Fehler in cPost-Bibliothek";
MIK_EM50102.lMsg = "Eingangspunkt /P1 in cPost-Bibliothek /P2 nicht gefunden.";

MIK_EM50103.iSev = "5";
MIK_EM50103.sMsg = "Interner Fehler in mce-Bibliothek";
MIK_EM50103.lMsg = "Eingangspunkt /P1 in mce-Bibliothek /P2 nicht gefunden.";

MIK_EM50104.iSev = "5";
MIK_EM50104.sMsg = "Interner Fehler";
MIK_EM50104.lMsg = "Parameterdatei f�r die Verwendung mit MIK nicht g�ltig:\n/P1\nFehlende Definition von: /P2";

MIK_EM50105.iSev = "5";
MIK_EM50105.sMsg = "Anwendungsfehler";
MIK_EM50105.lMsg = "Parameterdatei kann nicht ge�ffnet werden: /P1.\nDatei wird aus dem Startverzeichnis f�r die Fertigung geladen.";

MIK_EM50106.iSev = "5";
MIK_EM50106.sMsg = "Anwendungsfehler";
MIK_EM50106.lMsg = "cPost-Eintrag /P2 wurde mit Fehler zur�ckgegeben. rc = /P1.";

MIK_EM50107.iSev = "5";
MIK_EM50107.sMsg = "Anwendungsfehler";
MIK_EM50107.lMsg = "mce-Eintrag /P2 wurde mit Fehler zur�ckgegeben. rc = /P1.";

MIK_EM50108.iSev = "5";
MIK_EM50108.sMsg = "Anwendungsfehler";
MIK_EM50108.lMsg = "Simulationsdaten f�r Aktivit�t /P1 nicht gesichert.";

MIK_EM50109.iSev = "5";
MIK_EM50109.sMsg = "Anwendungsfehler";
MIK_EM50109.lMsg = "cPost-Generierung von Simulationsdaten f�r Aktivit�t /P2 wurde mit Fehler zur�ckgegeben. rc = /P1.";

MIK_EM50110.iSev = "5";
MIK_EM50110.sMsg = "Anwendungsfehler";
MIK_EM50110.lMsg = "mce-Generierung von Simulationsdaten f�r Aktivit�t /P2 wurde mit Fehler zur�ckgegeben. rc = /P1.";

MIK_EM50111.iSev = "5";
MIK_EM50111.sMsg = "Anwendungsfehler";
MIK_EM50111.lMsg = "Fehlender mce-Abschnitt in Parameterdatei.";

MIK_EM50112.iSev = "5";
MIK_EM50112.sMsg = "Anwendungsfehler";
MIK_EM50112.lMsg = "Aktuelle Einheit (mm/inch) konnte nicht gelesen werden.";

MIK_EM50113.iSev = "5";
MIK_EM50113.sMsg = "Anwendungsfehler";
MIK_EM50113.lMsg = "mce-Initialisierung f�r MIK fehlgeschlagen.";

MIK_EM50114.iSev = "5";
MIK_EM50114.sMsg = "Anwendungsfehler";
MIK_EM50114.lMsg = "Parameterdatei kann im Startverzeichnis f�r die Fertigung nicht ge�ffnet werden: /P1.";

MIK_EM50115.iSev = "5";
MIK_EM50115.sMsg = "Anwendungsfehler";
MIK_EM50115.lMsg = "MIK1 konnte keinen Wert f�r das MfgBatch FT05-Dateischl�sselwort /P1 abrufen.";

MIK_EM50116.iSev = "5";
MIK_EM50116.sMsg = "Anwendungsfehler";
MIK_EM50116.lMsg = "MIK APT-Generierungsaufruf von LaunchCATMFG fehlgeschlagen mit R�ckkehrcode = /P1.";

MIK_EM50117.iSev = "5";
MIK_EM50117.sMsg = "Anwendungsfehler";
MIK_EM50117.lMsg = "MOAPTIndexes konnte f�r folgende Operation nicht abgerufen werden: /P1.";

MIK_EM50118.iSev = "5";
MIK_EM50118.sMsg = "Anwendungsfehler";
MIK_EM50118.lMsg = "MfgBatch APT-Datei konnte nicht mit Lesezugriff ge�ffnet werden: /P1.";

MIK_EM50119.iSev = "5";
MIK_EM50119.sMsg = "Anwendungsfehler";
MIK_EM50119.lMsg = "Ung�ltiger Simulationsmodus: /P1.";


// MIK CE
MIK_EM50200.iSev = "5";
MIK_EM50200.sMsg = "Anwendungsfehler";
MIK_EM50200.lMsg = "Es wurde keine Post- und Controllerdatei ausgew�hlt.";

MIK_EM50201.iSev = "5";
MIK_EM50201.sMsg = "Anwendungsfehler";
MIK_EM50201.lMsg = "In der Controllerdefinitionsdatei ist kein Maschinencontroller-Emulator definiert.";

MIK_EM50202.iSev = "5";
MIK_EM50202.sMsg = "Interner Fehler in mce-Bibliothek";
MIK_EM50202.lMsg = "mce ist f�r Simulation nicht g�ltig.";

MIK_EM50203.iSev = "5";
MIK_EM50203.sMsg = "Anwendungsfehler";
MIK_EM50203.lMsg = "Aufruf von 'MIKCreateDOFTable()' ohne erfolgreichen Aufruf von 'MIKInit()'.";

MIK_EM50204.iSev = "5";
MIK_EM50204.sMsg = "Anwendungsfehler";
MIK_EM50204.lMsg = "Initialisierung der Sitzung fehlgeschlagen f�r: /P1.";

// common
MIK_EM59100.iSev = "5";
MIK_EM59100.sMsg = "Anwendungsfehler";
MIK_EM59100.lMsg = "NULL-Handlerausnahme f�r: /P1 (in: /P2).";

MIK_EM59998.iSev = "5";
MIK_EM59998.sMsg = "Interner Fehler";
MIK_EM59998.lMsg = "Nicht gen�gend Speicher. Speicherzuordnung fehlgeschlagen.";

MIK_EM59999.iSev = "5";
MIK_EM59999.sMsg = "Interner Fehler";
MIK_EM59999.lMsg = "Interner Fehler, Programm wird abgebrochen.";

