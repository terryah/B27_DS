OptFeature = "Optimierungskomponente";

OptOptimizationSet					= "Set mit Optimierungskomponenten";
OptOptimizationSet.Optimizations	= "Liste der Optimierungskomponenten";

OptConstraint				= "Optimierungsbedingung";
OptConstraint.Satisfaction	= "Ist erfüllt";
OptConstraint.Precision		= "Genauigkeit ";
OptConstraint.Distance		= "Abstand";
OptConstraint.Priority		= "Priorität";

OptGoal				= "Optimierungsziel";
OptGoal.Type		= "Typ";
OptGoal.TargetValue	= "Zielwert";
OptGoal.Precision	= "Genauigkeit ";
OptGoal.Priority	= "Priorität";
OptGoal.Parameter	= "Parameter";
OptGoal.Comment		= "Kommentar zum Ziel";

OptProblem				= "Optimierungsproblem";
OptProblem.Goals		= "Liste der Ziele";
OptProblem.Constraints	= "Liste der Bedingungen";
OptProblem.Comment		= "Kommentar zum Problem";

OptFreeParameter			= "Optimierung - freier Parameter";
OptFreeParameter.Value		= "Wert";
OptFreeParameter.Parm		= "Parameter";
OptFreeParameter.InfRange	= "Untergrenze";
OptFreeParameter.SupRange	= "Obergrenze";
OptFreeParameter.Step		= "Schritt";
OptFreeParameter.HasRangeStep = "Weist Bereiche und/oder Schritt auf";

// PEO Algorithm generic Type
OptGenericAlgorithm						= "PEO-Algorithmus";
OptGenericAlgorithm.NbUpdatesMax		= "Maximale Anzahl Aktualisierungen";
OptGenericAlgorithm.MaxTime				= "Maximale Zeit";
OptGenericAlgorithm.MaxWoImprovement	= "Maximale Anzahl Aktualisierungen ohne Verbesserung";
OptGenericAlgorithm.StoppingCriterion	= "Stoppbedingung";

OptGenericAlgorithm.StopRelativeObjectiveChange = "Relative objektive Änderung";
OptGenericAlgorithm.StopAbsoluteObjectiveChange =  "Absolute objektive Änderung";
OptGenericAlgorithm.StopAbsoluteVariableChange = "Absolute variable Änderung";
OptGenericAlgorithm.StopAbsoluteConstraintChange =  "Absolute Bedingungsänderung";
OptGenericAlgorithm.StopRelativeConstraintChange = "Relative Bedingungsänderung";
OptGenericAlgorithm.StopWindowChange = "Fensteränderung stoppen";

// one generic type for Optimization and one generic type for DoE
OptGenericOptimAlgorithm				= "Optimierungsalgorithmus";
OptGenericDoeAlgorithm					= "DOE-Algorithmus";

// sub types of Optimization generic type
OptGradientAlgorithm		= "Algorithmus für Gradienten ohne Bedingung";
OptGradientAlgorithm.ConvergenceSpeed = "Konvergenzgeschwindigkeit";

OptApproximationGradientAlgorithm = "Algorithmus für Gradienten mit Bedingung(en)";
OptApproximationGradientAlgorithm.ConvergenceSpeed = "Konvergenzgeschwindigkeit";

OptSimAnnealingAlgorithm	= "Algorithmus für simuliertes Ausglühen";
OptSimAnnealingAlgorithm.ConvergenceSpeed = "Konvergenzgeschwindigkeit";

// sub type of DoE generic type
FullDoeAlgorithm						= "Vollständige Erstellung von Experimenten";
FullDoeAlgorithm.ListOfLevels			= "Liste der Parameterstufen";
FullDoeAlgorithm.LastDoneExperimentNb	= "Index des zuletzt durchgeführten Experiments";

OptOptimization					= "Optimierung";
OptOptimization.Problem			= "Problem";
OptOptimization.FreeParameters	= "Liste der freien Parameter";
OptOptimization.Algorithm		= "Algorithmus";
OptOptimization.UpdateVisualization = "Visualisierungsmarkierung aktualisieren";
OptOptimization.OptimizationLog	= "Optimierungsprotokoll";

//SOC - 08/10/02
OptimizationLog							= "Optimierungsprotokoll";
OptimizationLog.DesignTable				= "Berechnungsverwaltung";
OptimizationLog.IndexOfBestSolInDT		= "Index der speziellen Ergebnisse des Protokolls";
OptimizationLog.BestParm				= "Parameter für das optimale berechnete Ergebnis";
OptimizationLog.NbEvalParm				= "Parameter für die aktuelle Bewertungsnummer";

//SOC - 15/11/02 : les attributs du type suivant sont ceux de Set of Equations (Package Advisor)
//				   => on les trouve dans CATKnowledgeAdvisorSearch
OptConstraintSatisfaction	= "Bedingungserfüllung";

// sub types of Optimization generic type
OptApproximationAlgorithm		= "Annäherungsalgorithmus für Bedingungen und Prioritäten";
OptLocalWithPrioritiesAlgorithm		= "Lokaler Algorithmus für Bedingungen und Prioritäten";
OptCstGradAlgorithm		= "Algorithmus zur Bereitstellung von Bedingungen und Derivaten";


