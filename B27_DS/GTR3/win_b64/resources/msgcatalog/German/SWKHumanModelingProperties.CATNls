// ============================================================================
// ============================================================================
// SWKHumanModelingProperties English Message File
// ============================================================================
// ============================================================================

DataNotAvailable="Keine Daten verf�gbar";
NoManikinSelected="Es wurde kein menschl. Darstellungsobjekt ausgew�hlt.";
ResetButton.Title="Zur�cksetzen";
DiffValExist="Verschiedene Werte existieren";

// ---------- SWKManikinAnthroPropFrame ---------------------------------------
LabelDatabase.Title="Population: ";
LabelSex.Title="Geschlecht: ";
LabelStaturePerc.Title="Staturperzentil: ";
LabelWeightPerc.Title="Gewichtsperzentil: ";
Anthro.Column1="Name";
Anthro.Column2="Wert (/p1)";
Anthro.Column3="Wert (%)";

// ---------- SWKManikinAttachPropFrame ---------------------------------------
AttachList.Column1="Segmente";
AttachList.Column2="Objekte";
DetachButton.Title="Abh�ngen";
PickAttachesExist="Die Zuordnung eines oder mehrerer der ausgew�hlten Objekte (vgl. Auswahlaktivit�t) kann in diesem Kontext nicht aufgehoben werden.";

// ---------- SWKManikinColorPropFrame ----------------------------------------
FrameColoring.LabelAnalysis.Title="Analyse:";
ComboAnalysis.PreferentialAngles="Haltungsbewertung";
ComboAnalysis.RULA="RULA";

FrameColoring.LabelShowColors.Title="Farben anzeigen:";
FrameColoring.RadioBNoShow.Title="Kein";
FrameColoring.RadioBAllShow.Title="Alle";
FrameColoring.RadioBAllButMax.Title="Alle au�er den Maximalbewertungen";
FrameColoring.RadioBDiffOptions.Title="Verschiedene Optionen existieren";

FrameColoring.LabelDOFColoring.Title="Freiheitsgrad: ";
FrameColoring.LabelCustomizedJointColoring.Title="Bevorzugte Winkel: ";
FrameColoring.ObjectToColor.Title="Zu f�rbende Elemente: ";
FrameColoring.CheckBSegmentColoring.Title="Segmente";
FrameColoring.CheckBSurfaceColoring.Title="Fl�chen";
FrameColoring.CheckBActivateColoring.Title="Aktiv"; // *** bzk ***
FrameColoring.ButtonResetColoring.Title="Zur�cksetzen";
FrameColoring.Head.Title=" Kopf";
FrameColoring.Thoracic.Title=" Thorax";
FrameColoring.Lumbar.Title=" Lende";

ComboDOFColoring.DOF1="Flexion/Extension (Freiheitsgrad 1)";
ComboDOFColoring.DOF2="Abduktion/Adduktion (Freiheitsgrad 2)";
ComboDOFColoring.DOF3="Rotation (Freiheitsgrad 3)";
ComboDOFColoring.Worst="Schlechtester Freiheitsgrad";

ComboCustomizedJointColoring.PreferedAngle="Bevorzugte Winkel ";
ComboCustomizedJointColoring.Head="Kopf";
ComboCustomizedJointColoring.Thoracic="Thorax";
ComboCustomizedJointColoring.Lumbar="Lende";

FrameColoring.LabelRULAType.Title="Typ: ";
ComboRULAType.General="Allgemein";
ComboRULAType.Detailed="Detailliert";
ShowColorsSetting="Unerwarteter Fehler bei der Einstellung der anzuzeigenden Farben.";
DOFColoredSetting="Unerwarteter Fehler bei der Einstellung des einzuf�rbenden Freiheitsgrads.";
ObjectToColorSetting="Unerwarteter Fehler bei der Einstellung des einzuf�rbenden Objekts.";
RULATypeColoringSetting="Unerwarteter Fehler bei der Einstellung des einzuf�rbenden RULA-Typs.";

// ---------- SWKManikinDisplayPropFrame --------------------------------------
RenderingFrame.Title="Wiedergabe";
RenderingFrame.CheckBSegments.Title="Segmente";
RenderingFrame.CheckBEllipses.Title="Ellipsen";
RenderingFrame.CheckBMeshes.Title="Fl�chen";
RenderingFrame.LabelResolution.Title="Aufl�sung:   ";

VisionFrame.Title="Sichtfeld";
VisionFrame.CheckBLineOfSight.Title="Sichtlinie";
VisionFrame.CheckBPeriphericCone.Title="Ansichtsfeld";
VisionFrame.CheckBCentralCone.Title="Visueller Kegel";
VisionFrame.LabelConeType.Title="Kegeltyp   ";

OthersFrame.Title="Sonstige";
OthersFrame.CheckBCenterOfGravity.Title="Schwerpunkt";
OthersFrame.CheckBBalance.Title="Balance";
OthersFrame.CheckBReferential.Title="Bezugspunkt";
ButtonResetDisplay.Title="Zur�cksetzen";

ComboFlatCone="Flach";
ComboSphericCone="Sph�risch";
ComboBoundingCone="Begrenzungen";
ComboSphericBoundingCone="Begrenzter Kegel";

// ---------- SWKManikinIKBehaviorsPropFrame ----------------------------------
SpineFrame.Title="Leitkurve";
SpineFrame.CheckBSpineThoracic.Title="Thorax";
SpineFrame.CheckBSpineLumbar.Title="Lende";
LineOfSightFrame.Title="Sichtlinie & Kopf";
LineOfSightFrame.CheckBEyes.Title="Augen";
LineOfSightFrame.CheckBHead.Title="Nur Sichtlinie";
PelvisFrame.Title="Becken";
PelvisFrame.CheckBPelvisHTranslation.Title="Horizontale Verschiebung";
PelvisFrame.CheckBPelvisVTranslation.Title="Vertikale Verschiebung";
PelvisFrame.CheckBPelvisRotation.Title="Querrotation";
PelvisFrame.CheckBPelvisOrientation.Title="L�ngsrotation";
LookAtFrame.Title="Betrachten";
LookAtFrame.CheckBLookAtHand.Title="Hand";
LookAtFrame.LabelLookAtSide.Title="Seite:";
LookAtFrame.RadioBActiveHand.Title="Aktiv";
LookAtFrame.RadioBRightHand.Title="Rechts";
LookAtFrame.RadioBLeftHand.Title="Links";
BalanceFrame.Title="Balance";
BalanceFrame.CheckBBalanceWithFeet.Title="Fu�bewegung";
BalanceFrame.CheckBBalanceWithPelvic.Title="Beckenbewegung";
ResetButtonFrame.ResetButton.Title="Zur�cksetzen";
OptimizeFrame.Title="Optimierung";
OptimizeFrame.CheckBPosturalScore.Title="Haltungsbewertung";
OptimizeFrame.CheckBRula.Title="RULA-Bewertung";

// ---------- SWKManikinLimitationsPropFrame ----------------------------------
LabelLimSelection.Title="Segmente mit benutzerdefinierten Begrenzungen (Perzentil):";
SegmentList.Column1="Segmente";
SegmentList.Column2="Freiheitsgrade";
SegmentList.Column3="Untergrenze";
SegmentList.Column4="Obergrenze";
SegmentList.Column5="Menschliches Darstellungsobjekt";
SelectAllButton="Alles ausw�hlen";
Lock="Sperren";
NoLimits="Kein";
LabelOutsideHumanLimitations.Title="* Wert liegt au�erhalb menschlicher Grenzwerte";

// ---------- SWKManikinLocksPropFrame ----------------------------------------
LabelLocksSelection.Title="Diese K�rperelemente sind gesperrt:";
UnlockButton.Title="Entsperren";

// ---------- SWKManikinPrefAngsPropFrame -------------------------------------
RemoveButton.Title="Entfernen";
LabelSelectionPrefAngs.Title="Segmente, f�r die bevorzugte Winkel hinzugef�gt wurden:";

// ---------- SWKManikinReferentialPropFrame ----------------------------------
LabelReferential.Title="Bezugspunkt: ";
ComboRef.EyePoint="Augenpunkt";
ComboRef.HPoint="H-Punkt";
ComboRef.LeftFoot="Linker Fu�";
ComboRef.RightFoot="Rechter Fu�";
ComboRef.LowestFoot="H-Punktprojektion";
ComboRef.Crotch="Weichteile";
ComboRef.LeftHand="Linke Hand";
ComboRef.RightHand="Rechte Hand";
ComboRef.BetweenFeet="Zwischen F��en";
LabelColor.Title="Farbe: ";
ButtonResetRef.Title="Zur�cksetzen";

// ---------- SWKManikinVisionPropFrame ---------------------------------------
FrameType.Title="Typ";

SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.Title="Begrenzungen ";
SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.FieldOfViewFrame.LabelSection.Title="Ansichtsfeld: ";
SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.FieldOfViewFrame.LabelHorizontalMonocular.Title="Horizontal monokular: ";
SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.FieldOfViewFrame.LabelHorizontalAmbinocular.Title="Horizontal ambinokular: ";
SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.FieldOfViewFrame.LabelVerticalTop.Title="Vertikal oben: ";
SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.FieldOfViewFrame.LabelVerticalBottom.Title="Vertikal unten: ";
SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.FieldOfViewFrame.LabelCentral.Title="Visueller Kegel: ";
SWKVisionPropTabCntr.SWKVisionFieldOfViewTabPage.FieldOfViewFrame.PBDefaultFieldOfView.ShortHelp="Die Standardeigenschaften des Sichtfelds wiederherstellen.";

SWKVisionPropTabCntr.SWKVisionDistanceTabPage.Title="Abstand";
SWKVisionPropTabCntr.SWKVisionDistanceTabPage.DistanceFrame.LabelFocusDistance.Title="Fokusentfernung: ";
SWKVisionPropTabCntr.SWKVisionDistanceTabPage.DistanceFrame.ChkBInfFocusDistance.Title="Infinit";
SWKVisionPropTabCntr.SWKVisionDistanceTabPage.DistanceFrame.LabelPonctumProximum.Title="Punctum Proximum:  ";
SWKVisionPropTabCntr.SWKVisionDistanceTabPage.DistanceFrame.LabelPonctumRemotum.Title="Punctum Remotum: ";
SWKVisionPropTabCntr.SWKVisionDistanceTabPage.DistanceFrame.ChkBInfPonctumRemotum.Title="Infinit";
SWKVisionPropTabCntr.SWKVisionDistanceTabPage.DistanceFrame.PBDefaultDistance.ShortHelp="Die Standardeigenschaften der Entfernung wiederherstellen.";

SWKVisionPropTabCntr.SWKVisionWindowTabPage.Title="Fensteranzeige";
ChkBHideTitle.Title="Titel ausblenden";
ChkBPeriphericContour.Title="Periphere Kontur";
ChkBCentralSpot.Title="Brennpunkt";
ChkBBlindSpot.Title="Toter Winkel";
PBRendering.Title="Anzeigemodi...";
LabelScale.Title="Ma�stab:  ";
PBDefaultVisionWindow.ShortHelp="Die Standardeigenschaften der Fensteranzeige \"Blickfeld\" wiederherstellen.";

Binocular="Binokular";
Ambinocular="Ambinokular";
MonocularRight="Monokular rechts";
MonocularLeft="Monokular links";
Stereo="Stereo";

//---------- SWKManikinAppearancePropFrame ------------------------------------
SegmentColorFrame.Title="Ausgew�hlte Segmente";
SegmentColorFrame.LabelSurfaceColor.Title="Fl�chenfarbe";
SegmentColorFrame.LabelTransparency.Title="Transparenz";
SegmentColorFrame.LabelEllipsesColor.Title="Ellipsenfarbe";
LabelDashTitle="Linientyp";
LabelWeightTitle="Breite";
SegmentColorFrame.LabelSegmentsColor.Title="Segmentfarbe";
SegmentColorFrame.ResetColorButton.Title="Zur�cksetzen";
HeadDetailsFrame.Title="Farben Kopfdetails";
HeadDetailsFrame.LabelHair.Title="Haar:   ";
HeadDetailsFrame.LabelEyes.Title="Augen:   ";
HeadDetailsFrame.LabelLips.Title="Lippen:   ";
HeadDetailsFrame.ResetHeadButton.Title="Zur�cksetzen";
