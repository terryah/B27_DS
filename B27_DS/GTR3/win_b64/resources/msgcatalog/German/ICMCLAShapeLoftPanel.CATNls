DialogBoxTitle="Loft";

//Selection---------------------------------------------------------------------------------------------

LabelGuides.Title="F�hrungselemente: ";
LabelGuides.LongHelp="W�hlen Sie zwei oder mehrere 
Kurvenelemente als F�hrungselemente in V-Richtung aus.";
LabelGuides.ShortHelp="F�hrungselemente in V-Richtung";

LabelProfiles.Title="Profile: ";
LabelProfiles.LongHelp="W�hlen?Sie zwei oder mehrere 
Kurvenelemente als Profile in U-Richtung aus.";
LabelProfiles.ShortHelp="Profile in U-Richtung";

CheckButtonSpine.Title="Leitkurve: ";
CheckButtonSpine.LongHelp="W�hlen Sie ein Kurvenelement 
als Leitkurve in V-Richtung aus.";
CheckButtonSpine.ShortHelp="Auswahl einer Leitkurve";


//Options tab-------------------------------------------------------------------------------------------

TabOptions.Title="Optionen";

//Tolerance frame

FrameTolerance.Title="Toleranz";
FrameTolerance.LongHelp="Definition der Toleranz";

LabelIntersection.Title="Verschneidung: ";
LabelIntersection.LongHelp="Der Toleranzwert, an dem sich die 
ausgew�hlten Profil- und F�hrungselementkurven 
schneiden m�ssen, 
damit ein Ergebnis erzielt werden kann.";
LabelIntersection.ShortHelp="Verschneidungstoleranz";

LabelSegment.Title="Segment: ";
LabelSegment.LongHelp="Bestimmt die Segmentbegrenzungen der resultierenden 
Fl�che am Schnittpunkt des F�hrungselements und des 
Profils.";
LabelSegment.ShortHelp="Segmenttoleranz";


//Guide/Profile Segmentation frame----------------------------------------------------------------------

FrameGuideSegmentation.Title="F�hrungselementsegmentierung";
FrameGuideSegmentation.LongHelp="Beeinflusst das Ergebnis gem�� 
den Segmentierungspunkten auf den F�hrungselementen.";

ComboGuideSegmentation.LongHelp="- Erste(s/r) -
  Die Anzahl der Segmente wird durch das erste ausgew�hlte F�hrungselement definiert.
- Max -
  Die Anzahl der Segmente wird durch das F�hrungselement mit der maximalen Anzahl von Segmenten definiert.
- Alle -
  Die Anzahl der Segmente wird durch alle F�hrungselemente definiert.";

FrameProfileSegmentation.Title="Profilsegmentierung";
FrameProfileSegmentation.LongHelp="Beeinflusst das Ergebnis gem�� 
der Segmentierungspunkte auf den Profilen.";

ComboProfileSegmentation.LongHelp="- Erste(s/r) -
  Die Anzahl der Segmente wird durch das erste ausgew�hlte Profil definiert.
- Max -
  Die Anzahl der Segmente wird durch das Profil mit der maximalen Anzahl von Segmenten definiert.
- Alle -
  Die Anzahl der Segmente wird durch alle Profile definiert.";

ComboSegmentation.First="Erste(s/r)";
ComboSegmentation.Max="Max.";
ComboSegmentation.All="Alle";

//Guide/Profile Trim frame------------------------------------------------------------------------------

FrameGuideTrim.Title="Trimmen des F�hrungselements";
FrameGuideTrim.LongHelp="Eingabekurven ohne 
kongruente Endpunkte k�nnen 
vor�bergehend getrimmt werden. W�hlen Sie den Start 
bzw. das Ende oder beide Optionen aus, je nachdem, 
welches Ergebnis von den 
Eingabekurven ben�tigt wird. Die resultierende Fl�che wird dann 
vor�bergehend zwischen den 
getrimmten F�hrungselementen erzeugt.";

CheckButtonGuideTrimStart.Title="Starten";
CheckButtonGuideTrimStart.LongHelp="Trimmen Sie das F�hrungselement am Start.";
CheckButtonGuideTrimStart.ShortHelp="Option: Start";
CheckButtonGuideTrimEnd.Title="Ende";
CheckButtonGuideTrimEnd.LongHelp="Trimmen Sie das F�hrungselement am Ende.";
CheckButtonGuideTrimEnd.ShortHelp="Option: Ende";

FrameProfileTrim.Title="Trimmen des Profils";
FrameProfileTrim.LongHelp="Eingabekurven 
ohne kongruente Endpunkte k�nnen 
vor�bergehend getrimmt werden. W�hlen Sie den Start
 bzw. das Ende oder beide Optionen aus, je nachdem, 
welches Ergebnis von den 
Eingabekurven ben�tigt wird. Die resultierende Fl�che wird 
dann vor�bergehend zwischen den 
getrimmten Profilen erzeugt.";

CheckButtonProfileTrimStart.Title="Starten";
CheckButtonProfileTrimStart.LongHelp="Trimmen Sie das Profil am Start.";
CheckButtonProfileTrimStart.ShortHelp="Option: Start";
CheckButtonProfileTrimEnd.Title="Ende";
CheckButtonProfileTrimEnd.LongHelp="Trimmen Sie das Profil am Ende.";
CheckButtonProfileTrimEnd.ShortHelp="Option: Ende";


//Guide/Profile Coupling frame--------------------------------------------------------------------------

FrameGuideCoupling.Title="F�hrungselementverbindung";
FrameGuideCoupling.LongHelp="Mit diesen Optionen kann die interne Parametrisierung der aus dem Verlauf
durch die F�hrungselemente resultierenden Fl�che verwaltet werden.
F�r die Parameterkonfiguration der Kopplung zwischen den
einzelnen Kurvensets stehen folgende Optionen zur Verf�gung:
- Keine
- Segmente
- Zellen
- Biegungen
- Stetigkeiten der F�hrungselementkopplung.";

ComboGuideCoupling.LongHelp="- Keine -
  Die Kopplung wird automatisch durch den Befehl definiert und der
  Benutzer hat keinen Einfluss auf die Umparametrisierung der F�hrungselemente. 
- Segmente -
  Die Berechnung der Kopplung wird durch die Anzahl der Segmente 
  in einem Kurvenset festgelegt, die die F�hrungselemente definieren, 
  und erm�glicht dem Benutzer eine Umparametrisierung gem�� den 
  internen Stetigkeiten. 
- Zellen -
  Die Berechnung der Kopplung wird durch die Anzahl der Zellen 
  festgelegt, die die F�hrungselemente definieren, und erm�glicht dem 
  Benutzer eine Umparametrisierung gem�� den internen Stetigkeiten. 
- Biegungen -
  Die Berechnung der Kopplung wird durch die Anzahl der Biegungen 
  festgelegt, die die F�hrungselemente definieren, und erm�glicht dem 
  Benutzer eine Umparametrisierung gem�� den internen Stetigkeiten. 
- Stetigkeiten der F�hrungselementkopplung -
  Die Stetigkeitsoptionen sind nur mit den Optionen 
  Segmente, Zellen und Biegungen verf�gbar.";

ContinuityGuideCoupling.Button_G0.LongHelp="Das Ergebnis beh�lt die G0-Stetigkeit entlang der F�hrungselemente bei.";
ContinuityGuideCoupling.Button_G0.ShortHelp="G0-Stetigkeit entlang des F�hrungselements";
ContinuityGuideCoupling.Button_G1.LongHelp="Das Ergebnis beh�lt die G1-Stetigkeit entlang der F�hrungselemente bei.";
ContinuityGuideCoupling.Button_G1.ShortHelp="G1-Stetigkeit entlang des F�hrungselements";
ContinuityGuideCoupling.Button_G2.LongHelp="Das Ergebnis beh�lt die G2-Stetigkeit entlang der F�hrungselemente bei.";
ContinuityGuideCoupling.Button_G2.ShortHelp="G2-Stetigkeit entlang des F�hrungselements";
ContinuityGuideCoupling.LongHelp="Einstellung der Stetigkeit entlang der F�hrungselemente: G0, G1, G2.";
ContinuityGuideCoupling.ShortHelp="Einstellungen f�r die Stetigkeit";

FrameProfileCoupling.Title="Profilverbindung";
FrameProfileCoupling.LongHelp="Mit diesen Optionen kann die interne Parametrisierung der aus dem Verlauf
durch die Profile resultierenden Fl�che verwaltet werden.
F�r die Parameterkonfiguration der Kopplung zwischen den
einzelnen Kurvensets stehen folgende Optionen zur Verf�gung:
- Keine
- Segmente
- Zellen
- Biegungen
- Stetigkeiten der Profilkopplung.";

ComboProfileCoupling.LongHelp="- Keine -
  Die Kopplung wird automatisch durch den Befehl definiert und der
  Benutzer hat keinen Einfluss auf die Umparametrisierung der Profile. 
- Segmente -
  Die Berechnung der Kopplung wird durch die Anzahl der Segmente 
  in einem Kurvenset festgelegt, die die Profile definieren, 
  und erm�glicht dem Benutzer eine Umparametrisierung gem�� den 
  internen Stetigkeiten. 
- Zellen -
  Die Berechnung der Kopplung wird durch die Anzahl der Zellen 
  festgelegt, die die Profile definieren, und erm�glicht dem 
  Benutzer eine Umparametrisierung gem�� den internen Stetigkeiten. 
- Biegungen -
  Die Berechnung der Kopplung wird durch die Anzahl der Biegungen 
  festgelegt, die die Profile definieren, und erm�glicht dem 
  Benutzer eine Umparametrisierung gem�� den internen Stetigkeiten. 
- Stetigkeiten der Profilkopplung -
  Die Stetigkeitsoptionen sind nur mit den Optionen 
  Segmente, Zellen und Biegungen verf�gbar.";

ComboCoupling.Bends="Biegungen";
ComboCoupling.Cells="Zellen";
ComboCoupling.None="Kein";
ComboCoupling.Segments="Segmente";

ContinuityProfileCoupling.Button_G0.LongHelp="Das Ergebnis hat G0-Stetigkeit an den Profilen.";
ContinuityProfileCoupling.Button_G0.ShortHelp="G0-Stetigkeit an den Profilen";
ContinuityProfileCoupling.Button_G1.LongHelp="Das Ergebnis hat G1-Stetigkeit an den Profilen.";
ContinuityProfileCoupling.Button_G1.ShortHelp="G1-Stetigkeit an den Profilen";
ContinuityProfileCoupling.Button_G2.LongHelp="Das Ergebnis hat G2-Stetigkeit an den Profilen.";
ContinuityProfileCoupling.Button_G2.ShortHelp="G2-Stetigkeit an den Profilen";
ContinuityProfileCoupling.LongHelp="Einstellung der Stetigkeit an den Profilen: G0, G1, G2.";
ContinuityProfileCoupling.ShortHelp="Einstellungen f�r die Stetigkeit";


//Guide/Profile Alignment frame-------------------------------------------------------------------------

FrameGuideAlignment.Title="Ausrichtung des F�hrungselements";
FrameGuideAlignment.LongHelp="Erm�glicht es, die 
Stetigkeitsbedingung der ausgew�hlten Start- und Endkurven f�r 
jedes F�hrungselement zu einem angrenzenden Set 
von St�tzfl�chen zu beeinflussen.";

CheckButtonSupportGuide1.Title="St�tzelement: ";
CheckButtonSupportGuide1.LongHelp="W�hlen Sie die Elemente aus, die f�r die Kantenstetigkeit am Start 
des F�hrungselements verwendet werden. 
Die Standardeinstellung ist G0. 
Verf�gbare Stetigkeitswerte f�r die ausgew�hlten 
Elemente sind G0, G1 und G2.";
CheckButtonSupportGuide1.ShortHelp="Auswahl der St�tzelemente am Start des F�hrungselements";

CheckButtonSupportGuide2.Title="St�tzelement: ";
CheckButtonSupportGuide2.LongHelp="W�hlen Sie die Elemente aus, die f�r die Kantenstetigkeit am 
Endpunkt des F�hrungselements verwendet werden. 
Die Standardeinstellung ist G0. 
Verf�gbare Stetigkeitswerte f�r die ausgew�hlten Elemente sind G0, G1 und G2.";
CheckButtonSupportGuide2.ShortHelp="Auswahl der St�tzelemente am Ende des F�hrungselements";

GuideAlignmentStart.Button_G0.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentStart.Button_G0.ShortHelp="G0 bei S";

GuideAlignmentStart.Button_G1.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentStart.Button_G1.ShortHelp="G1 bei S";

GuideAlignmentStart.Button_G2.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentStart.Button_G2.ShortHelp="G2 bei S";

GuideAlignmentStart.Button_G3.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentStart.Button_G3.ShortHelp="G3 bei S";

GuideAlignmentEnd.Button_G0.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Endkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentEnd.Button_G0.ShortHelp="G0 bei E";

GuideAlignmentEnd.Button_G1.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Endkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentEnd.Button_G1.ShortHelp="G1 bei E";

GuideAlignmentEnd.Button_G2.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Endkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentEnd.Button_G2.ShortHelp="G2 bei E";

GuideAlignmentEnd.Button_G3.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Endkurven f�r jedes F�hrungselement 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
GuideAlignmentEnd.Button_G3.ShortHelp="G3 bei E";


FrameProfileAlignment.Title="Profilausrichtung";
FrameProfileAlignment.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Start- und Endkurven f�r jedes Profil 
zu einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";

CheckButtonSupportProfile1.Title="St�tzelement: ";
CheckButtonSupportProfile1.LongHelp="W�hlen Sie die Elemente aus, die f�r die Kantenstetigkeit am 
Startpunkt des Profils verwendet werden. 
Die Standardeinstellung ist G0. 
Verf�gbare Stetigkeitswerte f�r die ausgew�hlten Elemente sind G0, G1 und G2.";
CheckButtonSupportProfile1.ShortHelp="Auswahl der St�tzelemente am Start des Profils";

CheckButtonSupportProfile2.Title="St�tzelement: ";
CheckButtonSupportProfile2.LongHelp="W�hlen Sie die Elemente aus, die f�r die Kantenstetigkeit am 
Ende des Profils verwendet werden. 
Die Standardeinstellung ist G0. 
Verf�gbare Stetigkeitswerte f�r die ausgew�hlten Elemente sind G0, G1 und G2.";
CheckButtonSupportProfile2.ShortHelp="Auswahl der St�tzelemente am Endpunkt des Profils";

ProfileAlignmentStart.Button_G0.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes Profil zu 
einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentStart.Button_G0.ShortHelp="G0 bei S";

ProfileAlignmentStart.Button_G1.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes Profil zu 
einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentStart.Button_G1.ShortHelp="G1 bei S";

ProfileAlignmentStart.Button_G2.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes Profil zu 
einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentStart.Button_G2.ShortHelp="G2 bei S";

ProfileAlignmentStart.Button_G3.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Startkurven f�r jedes Profil zu 
einem angrenzenden Set von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentStart.Button_G3.ShortHelp="G3 bei S";

ProfileAlignmentEnd.Button_G0.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der 
ausgew�hlten Endkurven f�r jedes Profil zu einem 
angrenzenden Set von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentEnd.Button_G0.ShortHelp="G0 bei E";

ProfileAlignmentEnd.Button_G1.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der ausgew�hlten 
Endkurven f�r jedes Profil zu einem angrenzenden Set 
von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentEnd.Button_G1.ShortHelp="G1 bei E";

ProfileAlignmentEnd.Button_G2.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der ausgew�hlten 
Endkurven f�r jedes Profil zu einem angrenzenden Set 
von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentEnd.Button_G2.ShortHelp="G2 bei E";

ProfileAlignmentEnd.Button_G3.LongHelp="Erm�glicht es, die Stetigkeitsbedingung der ausgew�hlten 
Endkurven f�r jedes Profil zu einem angrenzenden Set 
von St�tzfl�chen zu beeinflussen.";
ProfileAlignmentEnd.Button_G3.ShortHelp="G3 bei E";


//Alignment Influence frame-----------------------------------------------------------------------------

FrameGuideAlignmentInfluence.Title="Ausrichtungsbeeinflussung";
FrameGuideAlignmentInfluence.LongHelp="- Lokal -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflusst die
  Fl�chenform nur lokal zwischen der 1. und 2. Kurve gem�� der 
  angegebenen Stetigkeit.
- Streuung -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflussen die 
  Fl�chenform durch alle ausgew�hlten internen F�hrungselemente und Profile
  gem�� der angegebenen Stetigkeit.
- Global -
  Ist die Option Ausrichtungsbeeinflussung auf Global festgelegt, 
  beeinflussen die einer bestimmten Kante zugewiesenen Stetigkeiten die 
  gesamte Fl�chenform.";

ComboGuideAlignmentInfluence.LongHelp="- Lokal -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflussen die 
  Fl�chenform nur lokal zwischen der 1. und 2. Kurve gem�� der angegebenen 
  Stetigkeit.
- Streuung -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflussen die 
  Fl�chenform durch alle ausgew�hlten internen F�hrungselemente und Profile 
  gem�� der angegebenen Stetigkeit.
- Global -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflussen die 
  gesamte Fl�chenform.";

FrameProfileAlignmentInfluence.Title="Ausrichtungsbeeinflussung";
FrameProfileAlignmentInfluence.LongHelp="- Lokal -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflussen die 
  Fl�chenform nur lokal zwischen der 1. und 2. Kurve gem�� der angegebenen 
  Stetigkeit.
- Streuung -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflussen die 
  Fl�chenform durch alle ausgew�hlten internen F�hrungselemente und Profile 
  gem�� der angegebenen Stetigkeit.
- Global -
  Die einer bestimmten Kante zugewiesenen Stetigkeiten beeinflussen die 
  gesamte Fl�chenform.";

ComboInfluence.Global="Global";
ComboInfluence.Local="Lokal";
ComboInfluence.Spread="Streuung";


//Adjust frame------------------------------------------------------------------------------------------

FrameAdjust.Title="Anpassen";
FrameAdjust.LongHelp="Erm�glicht die Auswahl der Priorit�t f�r die Berechnung 
des Ergebnisses basierend auf den Eingaben: 
- An F�hrungselement anpassen -
  Die F�hrungselemente werden zur Ergebnisberechnung priorisiert. 
- Mittel -
  Das Ergebnis wird durch das Mittel aller Eingaben errechnet. 
- An Profil anpassen -
  Die Profile werden zur Ergebnisberechnung priorisiert.";

ComboAdjust.AdjustToGuide="An F�hrungselement anpassen";
ComboAdjust.Mean="Mittel";
ComboAdjust.AdjustToProfile="An Profil anpassen";

//Shape frame-------------------------------------------------------------------------------------------

FrameShape.Title="Form";
FrameShape.LongHelp="Legt die Form der Fl�che fest, d. h. 
die Art des �bergangs zwischen den individuell ausgew�hlten Profilen.";

ComboShape.Linear="Lineare Form";
ComboShape.Local="Lokale Form";
ComboShape.Smooth="Glatte Form";
ComboShape.Global="Globale Form";


TabApproximation.Title="N�herung";


// Result-----------------------------------------------------------------------------------------------

UseSpineAndSupportsOld2New.Title="Diese Komponente wurde in einer �lteren Umgebung erzeugt. 
Das Ergebnis kann in der aktuellen Umgebung ggf. abweichen. 
Die Verwendung des St�tzelements wird unterteilt in
Verwendung des St�tzelements am Start des F�hrungselements,
Verwendung des St�tzelements am Ende des F�hrungselements,
Verwendung des St�tzelements am Start des Profils, und
Verwendung des St�tzelements am Ende des Profils";

UseSpineAndSupportsNew2Old.Title="Diese Komponente wurde in einer neueren Umgebung erzeugt. Das
Ergebnis kann in der aktuellen Umgebung ggf. abweichen. Es wird
empfohlen, die neuere Umgebung f�r diese Komponente zu verwenden.";
