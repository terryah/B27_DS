MfgDwellDefault="Kein Verweilen";
MfgDwellTime="Definierte Zeit";
MfgDwellRound="Definierte Runde";
ApproachLength="Zustellung";
RetractLength="R�ckzugsbewegung";
Breakthrough="Durchbruch";

PlungeValue="Wert";
Plunge="Eintauchen";
Depth="Tiefe";
SpotDepth="Punkttiefe";

MultiplicatorBM="BM-Multiplikator";
DegressionDB="DB-Degression";
DistanceAB="AB-Abstand";
AngleWS="WS-Winkel";
DistanceRTS="RTS-Abstand";
BreakThroughTB="TB-Durchbruch";

ChamferDistance="Fasenabstand";
ChamferDiameter="Fasendurchmesser";
ChamferDiameter2="Fasendurchmesser 2";
ChamferShoulder="Fasenschulter";
RapidShoulder="Schnelle Schulter";

BackDepth="Hinteres Aufma�";
BackDiameter="Hinterer Durchmesser";
BackOffset="Hinteres Aufma�";

SlotDiameter="Spaltdurchmesser";

Shoulder="Schulter";
Tip="Spitze";
Distance="Abstand";
Diameter="Durchmesser";
Rapid="Eilgang";
Dwell="Verweilen";
PreDrilling="Erstes Bohren";
Processing="Verarbeitung";
Finishing="Schlichten";
Retract="R�ckzugsbewegung";
SpindleStart="Spindelstart";
SpindleStop="Spindelstopp";
TransitionPath="�bergangsbahn";
Tool_Tip="Werkzeugspitze";
Path_of_Tool_Tip="Bahn der Werkzeugspitze";
X="X";
Y="Y";
Z="Z";

MfgMOToolPower="Antrieb";
MfgMOToolPower.LongHelp="Definiert den POWER-Wert der axialen Operation ('Von Werkzeugbaugruppe', 'Angetrieben' oder 'Fixiert'):
    'Von Werkzeugbaugruppe' bedeutet, dass der in der Werkzeugbaugruppe definierte POWER-Wert f�r die Operation ber�cksichtigt wird.
    'Angetrieben' bedeutet, dass das Werkzeug sich bewegt und das Teil fixiert ist.
    'Fixiert' bedeutet, dass das Werkzeug fixiert ist und sich das Teil bewegt.
Das Fertigungsattribut ist MFG_MO_TOOL_POW.";

MFG_CLEAR_TIP="Zustellsicherheitsbereich (A)";
MFG_CLEAR_TIP.LongHelp="Definiert einen Sicherheitsabstand entlang der Werkzeugachse zur Ann�herung an die Bohrungsreferenz.
Das Fertigungsattribut ist MFG_CLEAR_TIP.";

MFG_CLEAR_TIP_2="Zustellsicherheitsbereich 2 (A2)";
MFG_CLEAR_TIP_2.LongHelp="Nur f�r Bohren und Anfasen, Anfasen auf zwei Seiten und R�ckw�rts Ausspindeln. Definiert einen Sicherheitsabstand entlang der Werkzeugachse zur Ann�herung an den Arbeitsgang zum Anfasen oder R�ckw�rts Ausspindeln.
Das Fertigungsattribut ist MFG_CLEAR_TIP_2.";

MFG_BREAKTHROUGH="Durchbruch (B)";
MFG_BREAKTHROUGH.LongHelp="Definiert den Abstand in der Werkzeugachsenrichtung, in der das Werkzeug vollst�ndig durch das Teil verl�uft.
Das Fertigungsattribut ist MFG_BREAKTHROUGH.";

MFG_CHAMFER_VAL="Fasendurchmesser (C)";
MFG_CHAMFER_VAL.LongHelp="Definiert den Wert der Fase, der einem Durchmesser am kegelf�rmigen Teil des Werkzeugs entspricht.
Das Fertigungsattribut ist MFG_CHAMFER_VAL.";

// Plunge 
PlungeEnumParam="Eintauchmodus";
PlungeEnumParam.LongHelp="Erm�glicht die Angabe einer axialen Eintauchoperation aus der Lochreferenz.
Diese optionale Eintauchoperation wird durch einen Tiefenwert (der durch die Werkzeugspitze oder den Durchmesser bestimmt wird) und ein Aufma� definiert.
Das Fertigungsattribut ist MFG_PLUNGE_MODE.";

MFG_PLUNGE_OFFST="Eintauchaufma� (Po)";
MFG_PLUNGE_OFFST.LongHelp="Definiert den Wert f�r das Eintauchaufma�.
Das Fertigungsattribut ist MFG_PLUNGE_OFFSET.";

MFG_PLUNGE_DIAMETER="Eintauchdurchmesser (Pd)";
MFG_PLUNGE_DIAMETER.LongHelp="Definiert den Wert f�r den Eintauchdurchmesser.
Das Fertigungsattribut ist MFG_PLUNGE_DIAMETER.";

MFG_PLUNGE_TIP="Eintauchspitze (Pt)";
MFG_PLUNGE_TIP.LongHelp="Definiert den Abstand der Eintauchspitze.
Das Fertigungsattribut ist MFG_PLUNGE_TIP.";

MFG_PLUNGE_SHOULDER="Eintauchschulter";
MFG_PLUNGE_SHOULDER.LongHelp="Eintauchschulter.
Das Fertigungsattribut ist MFG_PLUNGE_SHOULDER.";

MFG_PLUNGE_DISTANCE="Eintauchabstand";
MFG_PLUNGE_DISTANCE.LongHelp="Eintauchabstand.
Das Fertigungsattribut ist MFG_PLUNGE_DISTANCE.";

MFG_PLUNGE_FOR_CHAMFERING="Eintauchen zum Anfasen";
MFG_PLUNGE_FOR_CHAMFERING.LongHelp="Gibt an, ob die Eintauchoperation zum Anfasen erfolgen soll.
Das Fertigungsattribut ist MFG_PLUNGE_FOR_CHAMFERING.";

// Depth
DepthEnumParam="Tiefenmodus";
DepthEnumParam.LongHelp="Definiert, wie die Tiefenberechnung ausgef�hrt wird.
Je nach Typ der Operation kann diese als Funktion der Werkzeugspitze, der Schulter, des Durchmessers oder eines Abstandswerts erfolgen.
Das Fertigungsattribut ist MFG_DEPTH_MODE.";

MFG_DEPTH_TIP="Spitzentiefe (Dt)";
MFG_DEPTH_DIAMETER="Durchmesser (D)";
MFG_DEPTH_OFFSET="Offset";
MFG_DEPTH_SHOULDER="Schultertiefe (Ds)";
MFG_DEPTH_DISTANCE="Abstandstiefe (Dd)";

// Dwell
DwellEnumParam="Verweilmodus";
DwellEnumParam.LongHelp="Definiert das Verweilen anhand der Anzahl Umdrehungen oder der Zeitdauer.
Das Fertigungsattribut ist MFG_DWELL_MODE.";

MFG_DWELL_REVOL="Umdrehungen";
MFG_DWELL_REVOL.LongHelp="Gibt die Anzahl Umdrehungen f�r das Verweilen an.
Das Fertigungsattribut ist MFG_DWELL_REVOL.";

MFG_DWELL_REVOL_DOUBLE="Umdrehungen";
MFG_DWELL_REVOL_DOUBLE.LongHelp="Gibt die Anzahl Umdrehungen f�r das Verweilen an.
Das Fertigungsattribut ist MFG_DWELL_REVOL_DOUBLE.";

MFG_DWELL_TIME="Zeit";
MFG_DWELL_TIME.LongHelp="Gibt die Dauer des Verweilens an.
Das Fertigungsattribut ist MFG_DWELL_TIME.";

// Lift
LiftEnumParam="Verschiebemodus";
LiftEnumParam.LongHelp="Gibt den Verschiebemodus an, um das Aufma� f�r das Werkzeug unmittelbar vor der R�ckzugsbewegung festzulegen.
Das Fertigungsattribut ist MFG_LIFT_MODE.";

MfgLiftOffPolar="Durch Polarkoordinaten";
MfgLiftOffDistance="Durch lineare Koordinaten";
MfgNoLiftOff="Kein(e)";

MFG_XOFF="Verschiebung entlang X";
MFG_XOFF.LongHelp="Gibt die Verschiebung entlang X an.
Das Fertigungsattribut ist MFG_XOFF.";

MFG_YOFF="Verschiebung entlang Y";
MFG_YOFF.LongHelp="Gibt die Verschiebung entlang Y an.
Das Fertigungsattribut ist MFG_YOFF.";

MFG_ZOFF="Verschiebung entlang Z";
MFG_ZOFF.LongHelp="Gibt die Verschiebung entlang Z an.
Das Fertigungsattribut ist MFG_ZOFF.";

MFG_LIFT_DIST="Verschiebungsabstand";
MFG_LIFT_DIST.LongHelp="Gibt den Verschiebungsabstand an.
Das Fertigungsattribut ist MFG_LIFT_DIST.";

MFG_LIFT_ANGLE="Verschiebungswinkel";
MFG_LIFT_ANGLE.LongHelp="Gibt den Verschiebungswinkel an.
Das Fertigungsattribut ist MFG_LIFT_ANGLE.";

// Retract (Back Boring)
MFG_RETRACT_CLEAR_TIP="R�ckzugsbewegung";
MFG_RETRACT_CLEAR_TIP.LongHelp="Gibt die R�ckzugsbewegung an.
Das Fertigungsattribut ist MFG_RETRACT_CLEAR_TIP.";

MFG_AXIAL_DEPTH="Maximale Schnitt-Tiefe (Dc)";
MFG_AXIAL_DEPTH.LongHelp="Definiert die maximale Schnitt-Tiefe f�r:
- jedes Tiefloch in einer Tieflochbohroperation mit Entspanen
- jeden Durchbruchdurchgang in einer Tieflochbohroperation mit Spanbruch
Das Fertigungsattribut ist MFG_AXIAL_DEPTH.";

MFG_OFFSET_RET="R�ckzugsaufma� (Or)";
MFG_OFFSET_RET.LongHelp="Definiert den Wert:
- der R�ckw�rtsbewegung, die f�r den Spanbruch nach jedem Bohrdurchgang in einer Bohroperation mit Spanbruch verwendet wird
- des Aufma�es, bei dem der Bearbeitungsvorschub vor jedem neuen Tiefloch in einer Tieflochbohroperation mit Entspanen startet
Das Fertigungsattribut ist MFG_OFFSET_RET.";

MFG_DEPTH_DEC="Dekrementrate";
MFG_DEPTH_DEC.LongHelp="Verringert die effektive Tiefe eines Schnitts bei jedem neuen Tiefloch, bis die Gesamttiefe erreicht ist.
Wenn der Wert gleich null ist, wird die effektive Schnitttiefe bei jedem neuen Tiefloch als ein konstanter Schnitt angewendet.
Das Fertigungsattribut ist MFG_DEPTH_DEC.";

MFG_DEPTH_LIM="Dekrementbegrenzung";
MFG_DEPTH_LIM.LongHelp="Gibt den h�chsten Wert der Dekrementtiefe an.
Die Spitze eines neuen Tieflochs ist nie kleiner als die effektive Schnitt-Tiefe multipliziert mit der Dekrementbegrenzung.
Dieser Wert muss gr��er als null sein.
Das Fertigungsattribut ist MFG_DEPTH_LIM.";

MFG_ROTABL_OUTPUT="Automatische ROTABL";
MFG_ROTABL_OUTPUT.LongHelp="Verwendet ROTABL-Anweisungen, um den �bergang zwischen zwei Positionen zu generieren.
Das Fertigungsattribut ist MFG_ROTABL_OUTPUT.";

MFG_CYCLE_OUTPUT="Ausgabe mit CYCLE-Syntax";
MFG_CYCLE_OUTPUT.LongHelp="Gibt an, wie die NC-Ausgabe generiert werden muss: Ausgabe im CYCLE- oder im GOTO-Modus.
Das Fertigungsattribut ist MFG_CYCLE_OUTPUT.";

// Compensations outil
MFG_TL_COMP="Erste Korrektur";
MFG_TL_COMP.LongHelp="Eine Werkzeugkorrekturkennung ausw�hlen";

MFG_TL_COMP_2="Zweite Korrektur";
MFG_TL_COMP_2.LongHelp="Die zweite Werkzeugkorrekturkennung ausw�hlen";

// Specifiques au Circular Milling et au Thread Milling

MfgMachiningTolerance="Bearbeitungstoleranz";
MfgMachiningTolerance.LongHelp="Gibt den maximal zul�ssigen Abstand zwischen der theoretischen und der berechneten Werkzeugbahn an.
Das Fertigungsattribut ist MfgMachiningTolerance.";

MfgOutputStyle="Korrekturausgabe";
MfgOutputStyle.LongHelp="Gibt an, wie die Anweisungen f�r die Korrektur des Fr�sers
f�r die NC-Datenausgabe zu generieren sind:

Keine
Keine Berechnung der Korrektur


2D radiale Spitze
Die Korrektur wird in einer Ebene berechnet, die senkrecht zur Werkzeugachse liegt,
und sie wird in Bezug auf eine zu fr�sende Seite aktiviert (links oder rechts).
Dabei wird der Fr�sradius korrigiert.
Ausgabe ist die Werkzeugspitze (XT YT ZT).

2D radiales Profil
Die Korrektur wird in einer Ebene berechnet, die senkrecht zur Werkzeugachse liegt,
und sie wird in Bezug auf eine zu fr�sende Seite aktiviert (links oder rechts).
Dabei wird der Fr�sradius korrigiert.
Ausgabe ist der Werkzeugprofilpunkt (XP YP ZP).
Das Fertigungsattribut ist MFG_OUTPUT_STYLE.";

// Specifiques au Circular Milling

MfgCircularMillingMode="Bearbeitungsmodus";
MfgCircularMillingMode.LongHelp="Gibt den Bearbeitungsmodus an:
- Standard: 2.5-Achsen-Modus, die Bearbeitung erfolgt �ber Ebenen, die senkrecht zur Werkzeugachse liegen.
- Helixf�rmig: Die Bearbeitung erfolgt entlang einer Spirale.
Das Fertigungsattribut ist MfgCircularMillingMode.";

MfgDistanceBetweenPaths="Abstand zwischen Bahnen (Dp)";
MfgDistanceBetweenPaths.LongHelp="Definiert den maximalen Abstand zwischen zwei aufeinander folgenden Werkzeugbahnen in einer radialen Strategie.
Das Fertigungsattribut ist MfgDistanceBetweenPaths.";

MfgNumberOfRadialPaths="Anzahl Bahnen (Np)";
MfgNumberOfRadialPaths.LongHelp="Definiert die Anzahl der Werkzeugbahnen in einer radialen Strategie.
Das Fertigungsattribut ist MfgNumberOfRadialPaths.";

MfgAxialStrategy="Axialer Modus";
MfgAxialStrategy.LongHelp="Definiert, wie der Abstand zwischen zwei aufeinander folgenden Ebenen berechnet werden muss.
Das Fertigungsattribut ist MfgAxialStrategy.";

MfgMaxDepthOfCut="Maximale Schnitt-Tiefe (Mdc)";
MfgMaxDepthOfCut.LongHelp="Definiert die maximale Schnitt-Tiefe in einer axialen Schnittaufteilung.
Das Fertigungsattribut ist MfgMaxDepthOfCut.";

MfgNumberOfLevels="Anzahl Ebenen (Nl)";
MfgNumberOfLevels.LongHelp="Definiert die Anzahl von zu bearbeitenden Ebenen in einer axialen Schnittaufteilung.
Das Fertigungsattribut ist MfgNumberOfLevels.";

MfgSequencingMode="Sequenzmodus";
MfgSequencingMode.LongHelp="Gibt die Reihenfolge an, in der die Bearbeitung durchgef�hrt werden soll:
Axial: Die axiale Bearbeitung wird zuerst ausgef�hrt und anschlie�end die radiale.
Radial: Die radiale Bearbeitung wird zuerst ausgef�hrt und anschlie�end die axiale.
Das Fertigungsattribut ist MfgSequencingMode.";

MfgDirectionOfCut="Schnittrichtung";
MfgDirectionOfCut.LongHelp="Gibt an, wie das Fr�sen durchgef�hrt werden soll:
- Gleichlauffr�sen: 
 Die vordere Seite des Vorlaufwerkzeugs (in der Bearbeitungsrichtung) schneidet zuerst in das Material.
- Gegenlauffr�sen: 
 Die hintere Seite des Vorlaufwerkzeugs (in der Bearbeitungsrichtung) schneidet zuerst in das Material.
Das Fertigungsattribut ist MfgDirectionOfCut.";

MfgOverhang="Prozentsatz �berlappung";
MfgOverhang.LongHelp="Gibt an, wie weit das Werkzeug gem�� eines Prozentsatzes des Werkzeugdurchmessers �ber den Endpunkt einer geschlossenen Werkzeugbahn hinaus gehen muss.
Das Fertigungsattribut ist MfgOverhang.";

MfgAutoDraftAngle="Automatische Auszugsschr�ge";
MfgAutoDraftAngle.LongHelp="Gibt den Winkel der Auszugsschr�ge an, der auf die kreisf�rmige Flanke zwischen dem Anfang und dem Ende des Lochs angewendet werden soll.
Das Fertigungsattribut ist MfgAutoDraftAngle.";

HelixEnumParam="Helixmodus";
HelixEnumParam.LongHelp="Gibt an, wie die Helix zu definieren ist: nach Steigung oder nach Winkel.
Das Fertigungsattribut ist MFG_HELIX_MODE.";

MFG_HELIX_PITCH="Fr�stiefe (P)";
MFG_HELIX_PITCH.LongHelp="Gibt die Helixfr�stiefe an.
Das Fertigungsattribut ist MFG_HELIX_PITCH.";

MFG_HELIX_ANGLE="Winkel (Ang)";
MFG_HELIX_ANGLE.LongHelp="Gibt den Helixwinkel an.
Das Fertigungsattribut ist MFG_HELIX_ANGLE.";

// Panel Outils
MFG_MACH_QUALITY="Qualit�t";
MFG_MACH_QUALITY.LongHelp="Bearbeitungsqualit�t.
Das Fertigungsattribut ist MFG_MACH_QUALITY.";

// Thread Milling 
MfgThreadMillingStrategy="Bearbeitungsrichtung";
MfgThreadMillingStrategy.LongHelp="Gibt die verschiedenen Strategien an, die bei Gewindefr�soperationen angewendet werden k�nnen.
Das Fertigungsattribut ist MfgThreadMillingStrategy.";

MfgThreadMillingMode="Bearbeitungsstrategie";
MfgThreadMillingMode.LongHelp="Gibt die Anzahl Arbeitsg�nge an, die f�r eine Gewindefr�soperation erforderlich sind.
Das Fertigungsattribut ist MfgThreadMillingMode.";

//BFA - Compensation mode enahancement
MfgCompensationMode="Modus der Korrekturanwendung";
MfgCompensationMode.LongHelp="Gibt an, wie der f�r das Werkzeug angegebene Korrekturtyp
(z. B. P1, P2, P3) zur Definition der Werkzeugposition verwendet wird";

//FOH
MfgAxialSeqStrategy="Bearbeitungsstrategie";
MfgAxialSeqStrategy.LongHelp="Gibt den Modus f�r die Bearbeitungsstrategie an";
MfgAxialLevelOffsetOnDiameter="Aufma� auf Durchmesser";
MfgAxialLevelOffsetOnDiameter.LongHelp="Gibt das Aufma� auf dem lokalen Geometriedurchmesser an";

MfgOffsetOnDiameter="Aufma� auf Durchmesser";
MfgOffsetOnDiameter.LongHelp="Gibt das Aufma� auf den Durchmesser an";

MfgAxialLevelPlane="Ebenennummer";
MfgAxialLevelPlane.LongHelp="Gibt die Ebenennummer an";

MfgOffsetOnPlane="Aufma� auf Ebene";
MfgOffsetOnPlane.LongHelp="Gibt das Aufma� auf die Ebene an";

AxialHelixMode_AngPit="Helixmodus";
AxialHelixMode_AngPit.LongHelp="Gibt den Helixmodus an";

MfgHelixPitch="Fr�stiefe (P)";
MfgHelixPitch.LongHelp="Gibt die Helixfr�stiefe an";

MfgHelixAngle="Winkel (Ang)";
MfgHelixAngle.LongHelp="Gibt die Helixfr�stiefe nach Winkel an";

MfgCuttingDirection="Schnittrichtung";
MfgCuttingDirection.LongHelp="Gibt die Schnittrichtung an";

//BFA - Multiple pass and spring pass enhancement
MfgSpringPath="Federungsbahn";
MfgSpringPath.LongHelp="Gibt an, ob eine Federungsbahn generiert werden soll. Die Federungsbahn wird verwendet, um die nat�rliche 'Federung' des Werkzeugs zu kompensieren.
Das Fertigungsattribut ist MFG_SPR_PATH.";

//===================================================================
// YUQ R20
//===================================================================
IPMGenMode="IPM-Generierungsmodus";
IPMGenMode.LongHelp="M�gliche Modi f�r die IPM-Generierung sind 'Von Werkzeug', 'Von Geometrie' oder KEIN (Tools->Optionen->Option 'IPM-Generierungsmodus')";


//DeepHole Dilling
MfgDeepHoleLeadOut="Ausgehende Tiefbohrung";
MfgDeepHoleLeadOut.LongHelp=" Wert f�r die ausgehende Tiefbohrung, Sicherheitsabstand zum Anfang der Kreuzbohrung";
MfgDeepHoleLeadIn="Eingehende Tiefbohrung";
MfgDeepHoleLeadIn.LongHelp=" Wert f�r die eingehende Tiefbohrung, mit stabiler F�hrung f�r das Bohrwerkzeug in das Material eintauchen";
MFG_DEEPHOLE_FEED="Vorschubverringerung f�r Tiefbohrung";

