// ----------------
// Onglet Machining
// ----------------
MfgMachiningTabPage="Bearbeitung";

MfgMachToler="Bearbeitungstoleranz";
MfgMachToler.LongHelp="Das ist eine Abstandstoleranz zum Steuern der Verteilung und der Anzahl Ausgabepositionen.";
MfgMaxDiscretizationAngle="Max. Diskretisierungswinkel";
MfgMaxDiscretizationAngle.LongHelp="Gibt den maximal zulässigen Diskretisierungswinkel zwischen zwei aufeinanderfolgenden Positionen an, die die Maschine erreichen kann. Wenn die theoretische Werkzeugbahnberechnung Paare benachbarter Positionen (i,j,k) angibt, deren Winkel außerhalb des durch die Maschine erreichbaren Winkels liegt, kann die Maschine wahrscheinlich keine Interpolation erzeugen. Um dies zu vermeiden wird eine andere Berechnung durchgeführt, mit der zusätzliche Positionen ausgegeben werden.";

CuttingMode="Schnittmodus";
MfgDirectionOfCut.LongHelp="Gibt an, wie das Fräsen durchgeführt werden soll:
- Gleichlauffräsen: Die vordere Seite des Vorlaufwerkzeugs (in der Bearbeitungsrichtung) schneidet zuerst in das Material
- Gegenlauffräsen: Die hintere Seite des Vorlaufwerkzeugs (in die Bearbeitungsrichtung) schneidet zuerst in das Material";

MfgMachiningMode="Bearbeitungsmodus: ";
MfgMachiningMode1.LongHelp="Ermöglicht die Festlegung, ob die Bearbeitung Ebene für Ebene oder Bereich für Bereich erfolgen soll.";
MfgMachiningMode2.LongHelp="Ermöglicht die Festlegung, nur die Taschen, nur den äußeren Bereich des Teils oder beides zu bearbeiten.";

// ----------------
// Onglet Step-Over
// ----------------
MfgStepOverTabPage="Bahnabstand";

MfgSequencingMode="Reihenfolge";
MfgSequencingMode.LongHelp="Gibt die Reihenfolge an, in der die Bearbeitung durchgeführt werden soll:
- Axial: Die axiale Bearbeitung wird vor der radialen Bearbeitung durchgeführt.
- Radial: Die radiale Bearbeitung wird vor der axialen Bearbeitung durchgeführt.";

RadialStrategyFrame="Radiale Schnittaufteilung";
MfgDistanceBetweenPaths="Abstand zwischen Bahnen";
MfgDistanceBetweenPaths.LongHelp="Definiert den maximalen Abstand zwischen zwei aufeinander folgenden Werkzeugbahnen in einer radialen Strategie.";
MfgNumberOfRadialPaths="Anzahl Bahnen";
MfgNumberOfRadialPaths.LongHelp="Definiert die Anzahl von Werkzeugbahnen in einer radialen Schnittaufteilung.";

SemiAxialStrategyFrame="Axiale Schnittaufteilung";
Mfg5AxisMultiLevelMode=" Modus";
Mfg5AxisRoughMultiLevelMode.LongHelp="Definiert den Berechnungsmodus für die Bahnen in axialer Richtung.";
MfgMaxDepthOfCut="Abstand zwischen Bahnen";
MfgMaxDepthOfCut.LongHelp="Legt den maximalen Abstand zweier aufeinander folgenden Bahnen in axialer Richtung fest.";
MfgScallopHeight="Rautiefe";
MfgScallopHeight.LongHelp="Definiert die Rautiefe in einer axialen Strategie.";

// ----------------
// Onglet Finishing
// ----------------
MfgFinishingTabPage="Schlichten";

SideFinish=" Modus";
SideFinish.LongHelp="Gibt an, ob Schlichtbahnen an den Seiten des zu bearbeitenden Bereichs generiert werden
sollen. Seitenschlichten kann auf jeder Ebene oder nur auf der letzten Ebene der Operation durchgeführt
werden.";
Thickness="Seitenschlichtaufmaß";
Thickness.LongHelp="Gibt das für das Seitenschlichten verwendete Aufmaß an.";
BottomThickness="Seitenaufmaß am Boden";
BottomThickness.LongHelp="Gibt das Aufmaß an, das beim Schlichtfräsen am Boden verwendet wird.";
ThicknessBottom="Schlichtaufmaß am Boden";
ThicknessBottom.LongHelp="Gibt das Aufmaß an, das beim Schlichten am Boden verwendet wird.";
SpringPath="Federungsbahn";
SpringPath.LongHelp="Gibt an, ob eine Federungsbahn an den Seiten unter denselben Bedingungen wie die vorherige
Seitenschlichtbahn durchgeführt werden soll. Die Federungsbahn wird verwendet, um die natürliche 'Federung' des Werkzeugs zu kompensieren.";

Mfg5AxisFinishMultiLevelMode="Bahnabstand beim Schlichten";
Mfg5AxisFinishMultiLevelMode.LongHelp="Definiert den Berechnungsmodus für die Schlichtbahnen.";
MfgMaxDepthOfCutFinish.LongHelp="Legt den maximalen Abstand zweier aufeinander folgenden Bahnen in einer axialen Schlichtstrategie fest.";
MfgScallopHeightFinish.LongHelp="Definiert die Rautiefe in einer axialen Schlichtstrategie.";

// ----------------
// Onglet Tool-Axis
// ----------------
MfgToolAxisTabPage="Werkzeugachse";

Fanning="Fächerungsabstand";


Mfg5AxisMaxTiltAngle="Max. Neigungswinkel";
Mfg5AxisMaxTiltAngle.LongHelp="Gibt den Wert für den maximalen Neigungswinkel an.";

Mfg5AxisMPFAxisStrategy="Führung";
Mfg5AxisMPFAxisStrategy.LongHelp="Gibt die Strategie an, die für Führungen verwendet werden soll.";

// ----------------
// Onglet HSM
// ----------------
MfgHSMTabPage="HSM";

MfgCorneringFlag="Eckenbearbeitung";
MfgCorneringFlag.LongHelp="Gibt an, ob eine Eckenbearbeitung in der Bahn für Hochgeschwindigkeitsfräsen ausgeführt werden soll oder nicht.";

MfgCorneringRadius="Eckenradius";
MfgCorneringRadius.LongHelp="Gibt den für die Verrundung der Ecken entlang der Bahn einer Hochgeschwindigkeitsfräsoperation verwendeten Radius an.";

MfgCorneringFinishFlag="Eckenbearbeitung bei Seitenschlichtbahn";
MfgCorneringFinishFlag.LongHelp="Gibt an, ob eine Werkzeugbahneckenbearbeitung bei Seitenschlichtbahnen durchgeführt werden soll oder nicht";

MfgCorneringFinishRadius="Eckenradius";
MfgCorneringFinishRadius.LongHelp="Gibt den für die Seitenschlichtbahn einer Hochgeschwindigkeitsfräsoperation verwendeten Eckenradius an.";


// Tab page : Output ( For Circular interpolation )
CATMfgOutputTabPage.Title="Ausgabe";

MfgCircInterpo="Kreisförmige Interpolation";
MfgCircInterpo.LongHelp="Aktiviert die kreisförmige Interpolation.";

MfgCircInterpoOptim="Näherungsweise berechnete Arbeitsgänge zum Optimieren der kreisförmigen Interpolation";
MfgCircInterpoOptim.LongHelp="Legt fest, ob Arbeitsgänge der Werkzeugbahn näherungsweise berechnet werden, um die kreisförmige Interpolation zu optimieren.";

MfgToolPathDistanceOverlap="Arbeitsgangüberlappung";
MfgToolPathDistanceOverlap.LongHelp="Gibt die Breite der Überlappung des Endes eines Arbeitsgangs mit seinem Beginn an";

MfgFeedrateDistanceThreshold="Zu schnellem Vorschub wechseln";
MfgFeedrateDistanceThreshold.LongHelp="Gibt die Länge einer Verbindungsbahn an, für die der Vorschub in einen schnellen Vorschub geändert wird";



