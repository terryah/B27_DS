// CATSamValue
Value_CHARACTER = "Zeichen" ; // 0
Value_SHORT = "Kurze ganze Zahl" ; // 1
Value_INTEGER = "Ganze Zahl" ; // 2
Value_REAL = "Reelle Zahl" ; // 3
Value_DOUBLE = "Doppelte reelle Genauigkeit" ; // 4
Value_COMPLEX = "Komplex" ; // 5
Value_COMPLEX_DOUBLE = "Doppelte komplexe Genauigkeit" ; // 6
Value_POINTER = "Zeiger" ; // 7
Value_NODE = "Knoten" ; // 8
Value_ELEMENT = "Element" ; // 9
Value_MATERIAL = "Material" ; // 10
Value_AXIS = "Achsensystem" ; // 11
Value_PROPERTY = "Eigenschaft" ; // 12
Value_RESTRAINT = "Einschr�nkung" ; // 13
Value_LOAD = "Laden" ; // 14
Value_MASS = "Masse" ; // 15
Value_SET = "Set" ; // 16
Value_GROUP = "Gruppe" ; // 17
Value_IEXPRESSION = "I-Ausdruck" ; // 18
Value_VIRTUAL_NODE = "Virtueller Knoten" ; // 19
Value_VIRTUAL_ELEMENT = "Virtuelles Element" ; // 20
Value_BOUNDARY_CONDITION = "Grenzwertbedingung" ; // 21
Value_INVALID_VALUE = "Ung�ltiger Wertetyp" ; // 22

// CATSamMathType
MathType_SCALAR = "Skalar" ; // 0
MathType_VECTOR = "Vektor" ; // 1
MathType_SYMMETRICAL_TENSOR = "Steuerelement f�r symmetrische Spannung" ; // 2
MathType_TENSOR = "Steuerelement f�r Spannung" ; // 3
MathType_EIGEN_VALUES = "Eigenwerte" ; // 4
MathType_BIT_ARRAY = "Bitbereich" ; // 5
MathType_INVALID_MATH_TYPE = "Ung�ltiger mathematischer Typ" ; // 6

// CATSamListType
ListType_CHILDREN = "Kinder" ; // 0
ListType_PARENT = "Eltern" ; // 1
ListType_APPLY_TO = "Anwenden auf" ; // 2
ListType_APPLY_BY = "Anwenden durch" ; // 3
ListType_MEMBERS = "Teile eines physischen Typs" ; // 4
ListType_USER = "Benutzer" ; // 5
ListType_INVALID_LIST_TYPE = "Ung�ltiger Listentyp" ; // 6

// CATSamPosition
Position_NODE = "Knoten" ; // 0
Position_ELEMENT = "Element" ; // 1
Position_CENTER = "Mitte des Elements" ; // 2
Position_DOF = "Freiheitsgrad" ; // 3
Position_NODE_OF_ELEMENT = "Knoten des Elements" ; // 4
Position_EDGE = "Kante des Elements" ; // 5
Position_FACE = "Teilfl�che des Elements" ; // 6
Position_GAUSS = "Gau�scher Punkt des Elements" ; // 7
Position_VIRTUAL_NODE = "Virtueller Knoten" ; // 8
Position_VIRTUAL_ELEMENT = "Virtuelles Element" ; // 9
Position_USER_DEFINED = "Benutzerdefiniert" ; // 10
Position_GROUP = "Gruppe" ; // 11
Position_SET = "Set" ; // 12
Position_INVALID_POSITION_TYPE = "Ung�ltiger Positionstyp" ; // 13

// CATSamDof
Dof_TRANSLATION_X = "X-Verschiebung" ; // 1
Dof_TRANSLATION_Y = "Y-Verschiebung" ; // 2
Dof_TRANSLATION_Z = "Z-Verschiebung" ; // 4
Dof_ROTATION_X = "X-Rotation" ; // 8
Dof_ROTATION_Y = "Y-Rotation" ; // 16
Dof_ROTATION_Z = "Z-Rotation" ; // 32
Dof_TRANSLATION_R = "R-Verschiebung" ; // 64
Dof_ROTATION_RZ = "RZ-Rotation" ; // 128
Dof_TEMPERATURE = "Temperatur" ; // 256
Dof_PRESSURE = "Druck" ; // 512
Dof_ELECTRIC_POTENTIAL = "Elektrisches Potenzial" ; // 1024
Dof_DENSITY = "Dichte" ; // 2048
Dof_VELOCITY_X = "X-Geschwindigkeit" ; // 4096
Dof_VELOCITY_Y = "Y-Geschwindigkeit" ; // 8192
Dof_VELOCITY_Z = "Z-Geschwindigkeit" ; // 16384
Dof_BALL_JOIN = "Kugelgelenkverbindung" ; // 7
Dof_PINNED = "Mit Stiften versehene Verbindung" ; // 56
Dof_CLAMP = "Feste Einspannung" ; // 63
Dof_PIVOT = "Drehpunkt" ; // 31
Dof_SLIDING_PIVOT = "Gleitdrehpunkt" ; // 27
Dof_SURFACE_SLIDER = "Fl�chenloslager" ; // 60
Dof_SLIDER = "Loslager" ; // 59
Dof_FLUID = "Fl�ssig" ; // 31488
Dof_INVALID_DOF = "Ung�ltiger Freiheitsgrad" ; // 32768

// CATSamDataType
DataType_SET = "Set" ; // 0
DataType_ENTITY = "Einheit" ; // 1
DataType_CHARACTERISTIC = "Merkmal" ; // 2
DataType_LIST = "Liste" ; // 3
DataType_MATHEMATICAL = "Mathematisch" ; // 4
DataType_APPLY = "Anwenden" ; // 5
DataType_INVALID_DATA_TYPE = "Ung�ltiger Datentyp" ; // 6

// CATSamStatus
Status_DISPLAYED = "Angezeigt" ; // 1
Status_EXT_POINTER = "Externer Zeiger" ; // 2
Status_DELETED = "Gel�scht" ; // 4
Status_DEAD = "Au�er Betrieb" ; // 8
Status_ISOLATED = "Isoliert" ; // 16
Status_CHECKING = "Pr�fen" ; // 32
Status_DESACTIVATED = "Inaktiviert" ; // 64
Status_EXTERNAL_LINKED = "Bei einem anderen Modell verkn�pft" ; // 128
Status_CONDENSED_MESH = "Komprimierte Netzeinheit" ; // 256
Status_INVALID_STATUS = "Ung�ltiger Status" ; // 512

// CATSamCompareType
CompareType_POINTER = "Zeiger" ; // 0
CompareType_TAG = "Kennung" ; // 1
CompareType_CATEGORY = "Kategorie" ; // 2
CompareType_USER_NUMBER = "Benutzernummer" ; // 3
CompareType_PHYSICAL_TYPE = "Physischer Typ" ; // 4
CompareType_SOLVER_NAME = "Solver-Name" ; // 5
CompareType_USER_NAME = "Benutzername" ; // 6
CompareType_EXTERNAL_POINTER = "Externer Zeiger" ; // 7
CompareType_SEQUENTIAL_NUMBER = "Fortlaufende Zahl" ; // 8
CompareType_DATA_TYPE = "Datentyp" ; // 9
CompareType_EXTERNAL_TAG = "Externe Kennung" ; // 10
CompareType_INVALID_COMPARE_TYPE = "Ung�ltiger Vergleichstyp" ; // 11

// CATSamCheckType
CheckType_POINTER = "Zeiger" ; // 1
CheckType_CHILD = "Kind" ; // 2
CheckType_APPLY_TO = "Anwenden auf" ; // 4
CheckType_RECURSE = "Rekursion" ; // 8
CheckType_VALID = "G�ltig" ; // 16
CheckType_INVALID_CHECK_TYPE = "Ung�ltiger Pr�fungstyp" ; // 32

// CATSamRefFrame
RefFrame_LOCAL = "Lokal" ; // 0
RefFrame_GLOBAL = "Global" ; // 1
RefFrame_NONE = "Kein" ; // 2
RefFrame_INVALID_REF_FRAME = "Ung�ltiger Referenzrahmen" ; // 3

// CATSamRefFrameType
RefFrameType_RECTANGULAR = "Rechteckig" ; // 0
RefFrameType_CYLINDRICAL = "Zylindrisch" ; // 1
RefFrameType_CYLINDRICAL_INWARD = "Zylindrisch (innen)" ; // 2
RefFrameType_SPHERICAL = "Sph�risch" ; // 3
RefFrameType_SPHERICAL_INWARD = "Sph�risch (innen)" ; // 4
RefFrameType_INVALID_REF_FRAME_TYPE = "Ung�ltiger Referenzrahmentyp" ; // 5

// CATSamApplyQualifier
ApplyQualifier_UPPER = "Hoch" ; // 0
ApplyQualifier_MIDDLE = "Auf Senkungsfl�che" ; // 1
ApplyQualifier_LOWER = "Niedrig " ; // 2
ApplyQualifier_MASTER = "Master" ; // 3
ApplyQualifier_SLAVE = "Slave" ; // 4
ApplyQualifier_NODE_OF_ELEMENT = "Knoten des Elements" ; // 5
ApplyQualifier_NONE = "Kein" ; // 6
ApplyQualifier_INVALID_APPLY_QUALIFIER = "Ung�ltiges Anwendungsqualifikationsmerkmal" ; // 7

// CATSamSpace1DType
Space1DType_VECTOR = "Vektor" ; // 0
Space1DType_CONTINUOUS = "Fortlaufend" ; // 1
Space1DType_SPARSE = "Reduziert" ; // 2
Space1DType_INVALID_1DSPACE_TYPE = "Ung�ltiger eindimensionaler Raumtyp" ; // 3

// CATSamSpace2DType
Space2DType_MATRIX = "Matrix " ; // 0
Space2DType_CONTINUOUS = "Fortlaufend" ; // 1
Space2DType_SPARSE = "Reduziert" ; // 2
Space2DType_SYMMETRIC = "Symmetrisch" ; // 3
Space2DType_INVALID_2DSPACE_TYPE = "Ung�ltiger zweidimensionaler Raumtyp" ; // 4

// CATSamSpace3DType
Space3DType_TENSOR = "Steuerelement f�r Spannung" ; // 0
Space3DType_CONTINUOUS = "Fortlaufend" ; // 1
Space3DType_SPARSE = "Reduziert" ; // 2
Space3DType_INVALID_3DSPACE_TYPE = "Ung�ltiger 3D-Raumtyp" ; // 3

// CATSamSpaceVectorType
SpaceVectorType_NOMINAL = "Nominal" ; // 0
SpaceVectorType_ORDINAL = "Ordinal" ; // 1
SpaceVectorType_INTERVAL = "Intervall" ; // 2
SpaceVectorType_RATIO = "Faktor" ; // 3
SpaceVectorType_CONTINUOUS = "Fortlaufend" ; // 4
SpaceVectorType_INVALID_SPACEVECTOR_TYPE = "Ung�ltiger Vektortyp" ; // 5

// CATSamAggregationMode
AggregationMode_ADD = "Hinzuf�gen" ; // 0
AggregationMode_REPLACE = "Ersetzen" ; // 1
AggregationMode_MULTIPLY = "Multiplizieren" ; // 2
AggregationMode_SUPPRESS = "Unterdr�cken" ; // 3
AggregationMode_JUXTAPOSE = "Nebeneinander stellen" ; // 4
AggregationMode_INVALID_AGGREGATION_MODE = "Ung�ltiger Aggregatmodus" ; // 5

// CATSamMeshType
MeshType_PHYSICAL = "Physisch" ; // 1
MeshType_VIRTUAL = "Virtuell" ; // 2
MeshType_INVALID_MESH_TYPE = "Ung�ltiger Netztyp" ; // 4

// CATSamSubType
SubType_NOMINAL = "Nominal" ; // 0
SubType_LOGARITHMIC = "Logarithmisch" ; // 1
SubType_ELASTIC = "Elastisch" ; // 2
SubType_INELASTIC = "Unelastisch" ; // 3
SubType_THERMAL = "Thermisch" ; // 4
SubType_PLASTIC = "Plastik" ; // 5
SubType_INVALID_SUBTYPE = "Ung�ltiger Subtyp" ; // 6

// CATSamComponent
Component_NONE = "Kein" ; // 0
Component_C11 = "C11" ; // 1
Component_C12 = "C12" ; // 2
Component_C13 = "C13" ; // 4
Component_C21 = "C21" ; // 8
Component_C22 = "C22" ; // 16
Component_C23 = "C23" ; // 32
Component_C31 = "C31" ; // 64
Component_C32 = "C32" ; // 128
Component_C33 = "C33" ; // 256
Component_C1 = "C1" ; // 1
Component_C2 = "C2" ; // 16
Component_C3 = "C3" ; // 256
Component_ALL = "Alle" ; // 511
Component_INVALID_COMPONENT = "Ung�ltige Komponente" ; // 512

// CATSamBendingType
BendingType_CENTER_OF_CURVATURE = "Kr�mmungsmittelpunkt" ; // 0
BendingType_TANGENCY = "Tangentenstetigkeit" ; // 1
BendingType_BEND_RADIUS = "Biegungsradius" ; // 2
BendingType_ARC_ANGLE = "Bogenwinkel" ; // 3
BendingType_INVALID_BENDING_TYPE = "Ung�ltiger Biegungstyp" ; // 4

// CATSamWallType
WallType_INFINITE_PLANE = "Infinite Ebene" ; // 0
WallType_LIMITED_PLANE = "Begrenzte Ebene" ; // 1
WallType_CYLINDRICAL = "Zylindrisch" ; // 2
WallType_SPHERICAL = "Sph�risch" ; // 3
WallType_INVALID_WALL_TYPE = "Ung�ltiger Wandtyp" ; // 4

// CATSamEntityType
EntityType_NODE = "Knoten" ; // 0
EntityType_ELEMENT = "Element" ; // 1
EntityType_GEOMETRY = "Geometrie" ; // 2
EntityType_PREPROCESSOR_ENTITY = "Vorprozessoreinheit" ; // 3
EntityType_SET = "Set" ; // 4
EntityType_INVALID_ENTITY_TYPE = "Ung�ltiger Einheitentyp" ; // 5
// CATSamEvalType
EvalType_VALIDITY = "G�ltigkeit" ; // 0
EvalType_COMBINABILITY = "Verbindbarkeit" ; // 1
EvalType_INVALID_EVAL_TYPE = "Ung�ltiges Kriterium" ; // 2
// CATSamPositionData
PositionData_MAIN_NODES = "Hauptknoten" ; // 0
PositionData_ALL_NODES = "Alle Knoten" ; // 1
