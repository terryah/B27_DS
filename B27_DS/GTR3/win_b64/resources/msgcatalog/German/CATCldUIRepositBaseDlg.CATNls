// =============================================================================
// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2006
// =============================================================================
// CATCldUIRepositBaseDlg : NLS resource file for Reposit Base Dialog Box
// =============================================================================
// Implementation Notes:
// =============================================================================
// 27-Oct-2006 YSN: Creation
// =============================================================================

// ------
// Titles
// ------
CmdTitle.Compass.Title="Mit dem Kompass ausrichten";
CmdTitle.BestFit.Title="Nach bester Anpassung ausrichten";
CmdTitle.Constraint.Title="Nach Bedingungen ausrichten";
CmdTitle.RPS.Title="An RPS ausrichten";
CmdTitle.Sphere.Title="Mit Hilfe von Kugeln ausrichten";
CmdTitle.Trsf.Title="An der vorherigen Umwandlung ausrichten";

// ----------
// Base class
// ----------
MainFrame.SourceFrame.SourceLabel.Title="Auszurichtende Punktewolke:";
MainFrame.SourceFrame.SourceLabel.LongHelp="Zeigt die auszurichtende Punktewolke an.";

MoreString=" Mehr >> ";
LessString=" << Weniger";
MainFrame.MoreFrame.MorePushButton.LongHelp="Zeigt die Ausgabestatistik an oder verdeckt sie.";
FrameStats.LongHelp="Zeigt die Ausgabestatistik an.";
FrameStats.Title="Statistik";

MoveBtn.Title="Ursprüngliches Element beibehalten";
MoveBtn.LongHelp="Ist diese Option ausgewählt, wird eine Kopie der ursprünglichen auszurichtenden Punktewolke erzeugt und die Ausrichtung dieser Punktewolke anhand der Referenzen vorgenommen. 
Ist diese Option nicht ausgewählt, wird die ursprüngliche auszurichtende Punktewolke ausgerichtet, ohne dass dabei eine Kopie behalten wird.";

// ------------
// With Compass
// ------------
MainFrame.CompassFrame.VisuFrame.Title="Anzeige";
MainFrame.CompassFrame.VisuFrame.LongHelp="Definiert die Anzeige des Ergebnisses.";
MainFrame.CompassFrame.VisuFrame.PointsCheck.Title="Punkte";
MainFrame.CompassFrame.VisuFrame.TrianglesCheck.Title="Dreiecke";
MainFrame.CompassFrame.VisuFrame.ShadingCheck.Title="Schattierung";
MainFrame.CompassFrame.VisuFrame.SampleSlider.LongHelp="Legt fest, wieviel Prozent der Punkte angezeigt werden sollen.";
MainFrame.CompassFrame.VisuFrame.Percent.Title="%";

MainFrame.CompassFrame.MoveFrame.Title="Bewegen";
MainFrame.CompassFrame.MoveFrame.PushReset.Title="Verschiebung zurücksetzen";
MainFrame.CompassFrame.MoveFrame.PushReset.LongHelp="Setzt die Ausrichtung zurück.";
MainFrame.CompassFrame.MoveFrame.PushInit.Title="Ursprüngliche Verschiebung";
MainFrame.CompassFrame.MoveFrame.PushInit.LongHelp="Ermöglicht ein Ausrichten nach Trägheitsachsen.";
MainFrame.CompassFrame.MoveFrame.CompRdio.Title="Aktiver Kompass";
MainFrame.CompassFrame.MoveFrame.CompRdio.LongHelp="Platziert den Kompass im Schwerpunkt der Punktewolke, die verschoben werden soll.";

// --------
// Best Fit
// ---------
SelectorMulti.Reference.Panel.Title="Referenzelemente";

MainFrame.SourceFrame.SourceSelector.SourceCtxMenu.SourceCtxItem.Title="Aktivieren";

MainFrame.SourceFrame.TargetLabel.Title="Referenzelemente:";
MainFrame.SourceFrame.TargetLabel.ReferenceName="Referenz:";
MainFrame.SourceFrame.TargetLabel.LongHelp="Dient zur Auswahl der Referenzen. Hierbei kann es sich um Folgendes handeln:
– eine Menge von Punktewolken und/oder Flächen oder
– eine Punktemenge 
(Kombination unzulässig).";

MainFrame.SourceFrame.TargetSelector.TargetCtxMenu.TargetCtxItem.Title="Aktivieren";

Activate.LongHelp="Aktiviert Bereiche";
Activate.ShortHelp="Aktivieren";

PushBMulti.LongHelp="Zeigt die Liste der ausgewählten Elemente an.";
PushBMulti.ShortHelp="Multiselektion";

// ---------------------
// by Constraint and RPS
// ---------------------
MainFrame.ConstraintsList.Title="Liste der Bedingungen";
MainFrame.RPSList.Title="Liste der RPS-Bedingungen";

MainFrame.ConstraintsFrame.LongHelp="Listet die für die Ausrichtung einer Punktewolke definierten Bedingungen auf. \nEine Bedingung besteht aus zwei Bedingungselementen (einem Bedingungselement in der auszurichtenden Punktewolke und einem zweiten in der Referenz) \nsowie dem Prioritätswert für das Ausrichten nach Bedingungen oder festen Achsen (bei der RPS-Ausrichtung).";
MainFrame.ConstraintsFrame.PushClear.Title="Alle löschen";
MainFrame.ConstraintsFrame.PushClear.LongHelp="Löscht alle Bedingungen aus der Liste.";
MainFrame.ConstraintsFrame.PushDelete.Title="Löschen";
MainFrame.ConstraintsFrame.PushDelete.LongHelp="Löscht die ausgewählte Bedingung aus der Liste.";
MainFrame.ConstraintsFrame.PushAdd.Title="Hinzufügen";
MainFrame.ConstraintsFrame.PushAdd.LongHelp="Fügt eine neue Bedingung zu der Liste hinzu: Zuerst ein Bedingungselement in der auszurichtenden Punktewolke auswählen, \nanschließend ein weiteres Element in der Referenz auswählen.";
MainFrame.ConstraintsFrame.Input.Title="Auszurichtende Eingangspunktewolke";
MainFrame.ConstraintsFrame.Target.Title="Eingangsreferenz";
MainFrame.ConstraintsFrame.Priority.Title="Priorität";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel1="Strikt";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel2="Hoch";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel3="Überdurchschnittlich";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel4="Durchschnittlich";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel5="Unterdurchschnittlich";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel6="Niedrig";
MainFrame.ConstraintsFrame.CtxItem.Edit="Priorität bearbeiten";
MainFrame.ConstraintsFrame.XYZ.XLabel="X";
MainFrame.ConstraintsFrame.XYZ.YLabel="Y";
MainFrame.ConstraintsFrame.XYZ.ZLabel="Z";
MainFrame.RPSParamFrame.Title="Iterative RPS-Parameter";

MainFrame.OrientFrame.Title="Ausrichtung";
MainFrame.OrientFrame.LongHelp="Legt die Ausrichtung von Linie und Ebene fest. Bei Bedingungselementen des Typs Punkt ist die Ausrichtung bedeutungslos.";

MainFrame.OrientFrame.OrientAutoLabel.Title="Automatisch";
MainFrame.OrientFrame.OrientAutoLabel.LongHelp="Optimiert automatisch die Ausrichtung von Ebenen, Linien und Achsen in der auszurichtenden Wolke (Ausrichtungen werden nicht dargestellt). 
Diese Option ist zeitaufwändiger als die Option Manuell.";

MainFrame.OrientFrame.OrientManuLabel.Title="Manuell";
MainFrame.OrientFrame.OrientManuLabel.LongHelp="Steuert die Ausrichtung der Ebenen, Linien und Achsen in der auszurichtenden Wolke. 
Eine Linie oder ein Achse anklicken, um deren Ausrichtung umzukehren. 
Diese Option verwenden, wenn das Ergebnis der Option Automatisch unzureichend ist oder wenn die Ausrichtung gesteuert werden muss.";

// ---------
// By Sphere
// ---------
MainFrame.SphereFrame.Title="Kugelradius";
MainFrame.SphereFrame.LongHelp="Definiert den Kugelradius";
MainFrame.SphereFrame.ImposedBut.Title="Bedingt";
MainFrame.SphereFrame.ImposedBut.LongHelp="Ist diese Option ausgewählt, werden die Kugeln anhand des vom Benutzer eingegebenen Radius berechnet.";

// Icones

MainFrame.SourceFrame.SourceHideShowB.Title="Auszurichtende Punktewolke verdecken/anzeigen";
MainFrame.SourceFrame.SourceHideShowB.ShortHelp="Auszurichtende Punktewolke verdecken/anzeigen";
MainFrame.SourceFrame.SourceHideShowB.LongHelp="Auszurichtende Punktewolke verdecken/anzeigen
Verdeckt die auszurichtende Punktewolke oder zeigt sie an.";

MainFrame.SourceFrame.TargetHideShowB.Title="Referenzen verdecken/anzeigen";
MainFrame.SourceFrame.TargetHideShowB.ShortHelp="Referenzen verdecken/anzeigen";
MainFrame.SourceFrame.TargetHideShowB.LongHelp="Referenzen verdecken/anzeigen
Verdeckt Referenzen oder zeigt sie an.";

MainFrame.SourceFrame.SourceSphereB.Title="Kugeln auf der auszurichtenden Punktewolke";
MainFrame.SourceFrame.SourceSphereB.ShortHelp="Kugeln auf der auszurichtenden Punktewolke";
MainFrame.SourceFrame.SourceSphereB.LongHelp="Ermöglicht die Definition von Kugeln auf der auszurichtenden Punktewolke.";

MainFrame.SourceFrame.TargetSphereB.Title="Kugeln auf den Referenzen";
MainFrame.SourceFrame.TargetSphereB.ShortHelp="Kugeln auf den Referenzen";
MainFrame.SourceFrame.TargetSphereB.LongHelp="Ermöglicht die Definition von Kugeln auf den Referenzen.";
