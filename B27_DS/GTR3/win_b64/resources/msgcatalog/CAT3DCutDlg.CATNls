// Dialog Box of the 3DCut Command
Title=" 3D Cut ";
//
MainContainer.SelectionTabPage.Title=" Selection ";
MainContainer.SelectionTabPage.ShortHelp="Selection of the Products to cut";
MainContainer.SelectionTabPage.LongHelp="Selection of the Products to cut";

MainContainer.SelectionTabPage.ProductLabel.Title=" Selection: ";
MainContainer.SelectionTabPage.ProductLabel.ShortHelp="Products to cut";
MainContainer.SelectionTabPage.ProductLabel.LongHelp="Products to cut";

MainContainer.SelectionTabPage.ProductSelector.ShortHelp="Products to cut";
MainContainer.SelectionTabPage.ProductSelector.LongHelp="Products to cut";

MainContainer.SelectionTabPage.InputPushButton.Title="...";
MainContainer.SelectionTabPage.InputPushButton.ShortHelp="List of Products";
MainContainer.SelectionTabPage.InputPushButton.LongHelp="List of Products to cut";

MainContainer.SelectionTabPage.RefProductLabel.Title=" Reference Product: ";
MainContainer.SelectionTabPage.RefProductLabel.ShortHelp="Reference Product";
MainContainer.SelectionTabPage.RefProductLabel.LongHelp="Reference Product";

MainContainer.SelectionTabPage.RefProductSelector.ShortHelp="Reference product to position the result.";
MainContainer.SelectionTabPage.RefProductSelector.LongHelp="Reference product to position the result.";

MainContainer.SelectionTabPage.ResultFrame.Title=" Number of Triangles ";
MainContainer.SelectionTabPage.ResultFrame.InitialLabel.Title=" Initial: ";
MainContainer.SelectionTabPage.ResultFrame.InitialLabel.ShortHelp="Number of triangles in the selected components";
MainContainer.SelectionTabPage.ResultFrame.InitialLabel.LongHelp="Number of triangles in the selected components";
MainContainer.SelectionTabPage.ResultFrame.ResultLabel.Title=" Result: ";
MainContainer.SelectionTabPage.ResultFrame.ResultLabel.ShortHelp="Number of triangles after the cut";
MainContainer.SelectionTabPage.ResultFrame.ResultLabel.LongHelp="Number of triangles after the cut";

MainContainer.SelectionTabPage.ShapeNameFrame.Title=" Alternate Shapes Management ";
MainContainer.SelectionTabPage.ShapeNameFrame.ShapeNameLabel.Title=" Shape Name: ";
MainContainer.SelectionTabPage.ShapeNameFrame.ShapeNameEditor.Title=" Shape Name: ";
MainContainer.SelectionTabPage.ShapeNameFrame.ShapeNameEditor.ShortHelp="Name of the 3D Cut representation.";
MainContainer.SelectionTabPage.ShapeNameFrame.ShapeNameEditor.LongHelp="Name given to the 3D Cut representation in tools options.";

MainContainer.DefinitionTabPage.Title=" Definition ";
MainContainer.DefinitionTabPage.ShortHelp=" Definition of the behavior of the cut";
MainContainer.DefinitionTabPage.LongHelp=" Definition of the behavior of the cut";

MainContainer.DefinitionTabPage.Behavior1Frame.Title=" Cutting Type: ";
MainContainer.DefinitionTabPage.Behavior1Frame.3DCutTypeIconBox.Title=" Cutting Type: ";
MainContainer.DefinitionTabPage.Behavior1Frame.3DCutTypeIconBox.ShortHelp=" Cutting Type ";
MainContainer.DefinitionTabPage.Behavior1Frame.3DCutTypeIconBox.LongHelp="Cutting Type\nenables to choose the way you want to cut the components";
MainContainer.DefinitionTabPage.Behavior1Frame.3DCutTypeIconBox.InnerCheckButton.ShortHelp=" Cuts the outer parts of the products ";
MainContainer.DefinitionTabPage.Behavior1Frame.3DCutTypeIconBox.InnerCheckButton.LongHelp=" Cuts the outer parts of the products ";
MainContainer.DefinitionTabPage.Behavior1Frame.3DCutTypeIconBox.OutterCheckButton.ShortHelp=" Cuts the inner parts of the products ";
MainContainer.DefinitionTabPage.Behavior1Frame.3DCutTypeIconBox.OutterCheckButton.LongHelp=" Cuts the inner parts of the products ";

MainContainer.DefinitionTabPage.Behavior2Frame.Title=" On the Border: ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.Title=" On the Border ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.ShortHelp=" On the border ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.LongHelp="On the border\nenables to choose the behavior on the border of the box";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.KeepFacesCheckButton.ShortHelp=" Keeps completely or partially included triangles ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.KeepFacesCheckButton.LongHelp=" Keeps completely or partially included triangles ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.RejectFacesCheckButton.ShortHelp=" Keeps completely included triangles only ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.RejectFacesCheckButton.LongHelp=" Keeps completely included triangles only ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.BooleanCheckButton.ShortHelp=" Cuts the triangles ";
MainContainer.DefinitionTabPage.Behavior2Frame.BorderIconBox.BooleanCheckButton.LongHelp=" Cuts the triangles ";

MainContainer.DefinitionTabPage.Behavior3Frame.Title=" Visual Feedback: ";
MainContainer.DefinitionTabPage.Behavior3Frame.SectionCutCheckButton.Title=" Volume Cut";
MainContainer.DefinitionTabPage.Behavior3Frame.SectionCutCheckButton.ShortHelp=" Volume Cut ";
MainContainer.DefinitionTabPage.Behavior3Frame.SectionCutCheckButton.LongHelp="Volume Cut\nenables to have a first visual interactive feedback of the cut";

MainContainer.PositioningTabPage.Title=" Positioning ";
MainContainer.PositioningTabPage.ShortHelp=" Definition of the box ";
MainContainer.PositioningTabPage.LongHelp=" Definition and manual positioning of the box ";

MainContainer.PositioningTabPage.MovePanelCheckButton.Title=" Move Editor ";
MainContainer.PositioningTabPage.MovePanelCheckButton.ShortHelp=" Edit Position and Dimensions ";
MainContainer.PositioningTabPage.MovePanelCheckButton.LongHelp=" Open/Close a dialog box to edit position and dimensions of the 3D box ";

MainContainer.PositioningTabPage.SnapCheckButton.Title=" Snap ";
MainContainer.PositioningTabPage.SnapCheckButton.ShortHelp=" Positioning by selection ";
MainContainer.PositioningTabPage.SnapCheckButton.LongHelp=" Positioning by selection of geometrical objects ";

MainContainer.PositioningTabPage.ResetCheckButton.Title=" Reset Position ";
MainContainer.PositioningTabPage.ResetCheckButton.ShortHelp=" Reset Position ";
MainContainer.PositioningTabPage.ResetCheckButton.LongHelp=" Reset position of the box to the bounding box of the selected products ";

MainContainer.PositioningTabPage.AutomaticResetCheckButton.Title="Automatic reset";
MainContainer.PositioningTabPage.AutomaticResetCheckButton.ShortHelp=" Automatic reset when selecting ";
MainContainer.PositioningTabPage.AutomaticResetCheckButton.LongHelp="Automatic reset when selecting\nGrows or shrinks the box automatically when selecting products";

OKButtonTitle="Save";
APPLYButtonTitle="Preview";
CANCELButtonTitle="Close";
