//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 2000 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptManip
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    30.10.2000
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
// 	01		lgk	17.12.02 Revision
//=====================================================================================

LayoutFrm.HeaderFrame.Global.Title="Layout Creation";
LayoutFrm.HeaderFrame.Global.LongHelp="Parameters for Layout creation.";
LayoutFrm.IconAndOptionsFrame.OptionsFrame.lblRenderStyle.Title="Default render style: ";
LayoutFrm.IconAndOptionsFrame.OptionsFrame.lblRenderStyle.LongHelp="Defines the default render style value to apply to the Layout at its creation.";
LayoutFrm.IconAndOptionsFrame.OptionsFrame.chkRenderStylePerObject.Title="Activate Rendering Style per Object";
LayoutFrm.IconAndOptionsFrame.OptionsFrame.chkRenderStylePerObject.LongHelp="Activate Rendering Style per Object to the Layout at its creation.";

toolBckg.HeaderFrame.Global.Title="Background And Cutting Plane";
toolBckg.HeaderFrame.Global.LongHelp="Background and cutting plane";
toolBckg.IconAndOptionsFrame.OptionsFrame.chkDisplayBackAndCutting.Title="Display when loading layout";
toolBckg.IconAndOptionsFrame.OptionsFrame.chkDisplayBackAndCutting.LongHelp="Display background and cutting plane when loading layout";

toolHighlight.HeaderFrame.Global.Title="Highlight";
toolHighlight.HeaderFrame.Global.LongHelp="Highlight";
toolHighlight.IconAndOptionsFrame.OptionsFrame.chkPropagateHighlight.Title="Propagate highlight";
toolHighlight.IconAndOptionsFrame.OptionsFrame.chkPropagateHighlight.LongHelp="Propagates highlight";

tool2DLWnd.HeaderFrame.Global.Title="Layout Window";
tool2DLWnd.HeaderFrame.Global.LongHelp="Defines options related to the Layout window.";
tool2DLWnd.IconAndOptionsFrame.OptionsFrame.chkTile.Title="Tile window when activating Layout";
tool2DLWnd.IconAndOptionsFrame.OptionsFrame.chkTile.LongHelp="Tiles Layout and 3D windows when activating the Layout.";
tool2DLWnd.IconAndOptionsFrame.OptionsFrame.chkFitAllInSheetFormat.Title="Fit sheet format only";
tool2DLWnd.IconAndOptionsFrame.OptionsFrame.chkFitAllInSheetFormat.LongHelp="When checked, \"Fit All In\" command zoom the viewer on the sheet format definition\n even if it is not displayed. Otherwise, zoom is done on all geometry area.";
tool2DLWnd.IconAndOptionsFrame.OptionsFrame.chkFitAllInSheetSize.Title="Fit sheet size only";
tool2DLWnd.IconAndOptionsFrame.OptionsFrame.chkFitAllInSheetSize.LongHelp="When checked, \"Fit All In\" command zoom the viewer on the sheet\n even if it is not displayed. Otherwise, zoom is done on all geometry area.";

toolViewFrame.HeaderFrame.Global.Title="View Frame";
toolViewFrame.HeaderFrame.Global.LongHelp="Defines options related to the View Frame.";
toolViewFrame.IconAndOptionsFrame.OptionsFrame.chkBackgroundInViewFrame.Title="Take background into account when displaying view frame";
toolViewFrame.IconAndOptionsFrame.OptionsFrame.chkBackgroundInViewFrame.LongHelp="Defines whether the background of a view is taken into account while computing the extent of its view frame.";
toolViewFrame.IconAndOptionsFrame.OptionsFrame.frmViewFrameMode.frmViewFrameModelocked.radBackgroundInViewFrameAuto.Title="Automatically";
toolViewFrame.IconAndOptionsFrame.OptionsFrame.frmViewFrameMode.frmViewFrameModelocked.radBackgroundInViewFrameAuto.LongHelp="The extent of the background is recomputed and the view frame display updated only when specific commands modifying the background are launched.";
toolViewFrame.IconAndOptionsFrame.OptionsFrame.frmViewFrameMode.frmViewFrameModelocked.radBackgroundInViewFrameDemand.Title="On demand (during update)";
toolViewFrame.IconAndOptionsFrame.OptionsFrame.frmViewFrameMode.frmViewFrameModelocked.radBackgroundInViewFrameDemand.LongHelp="The extent of the background is recomputed and the view frame display updated only when the Update or the Update All commands are launched.";

toolRenderStyleOnView.HeaderFrame.Global.Title="Render Style On View";
toolRenderStyleOnView.HeaderFrame.Global.LongHelp="Defines options related to Render Style On View.";
toolRenderStyleOnView.IconAndOptionsFrame.OptionsFrame.chkForceRenderStyleOnLayoutViewer.Title="Allow modification of Rendering Style of the Layout viewer";
toolRenderStyleOnView.IconAndOptionsFrame.OptionsFrame.chkForceRenderStyleOnLayoutViewer.LongHelp="Allow modification of Rendering Style of the Layout viewer to ensure the Rendering Style visualization on View.";

