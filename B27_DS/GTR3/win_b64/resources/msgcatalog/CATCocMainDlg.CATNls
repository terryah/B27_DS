TopFrm.Scene.Title = "Scene";
TopFrm.Scene.LongHelp = "The name of the Class A scene (the name appears in the tree)";
TopFrm.Scene.Save.Title = "Save";
TopFrm.Scene.Save.LongHelp = "Saves the current scene in the document";
TopFrm.Scene.GlobalFeature.Title = "Global Feature";
TopFrm.Scene.GlobalFeature.LongHelp = "Some features (analysis) are active outside working scene";
TopFrm.Scene.Associativity.Title = "Associative";
TopFrm.Scene.Associativity.LongHelp = "Main switch for the automatic update of associative
commands inside the current scene";

TopFrm.VariantFrm.Title = "Variant";
TopFrm.VariantFrm.LongHelp = "Access to previous steps of modifications listed in the action list";

TopFrm.VariantFrm.UndoRedoFrm.GotoFirst.Title = "<<-";
TopFrm.VariantFrm.UndoRedoFrm.GotoFirst.LongHelp = "Go back to first step";
TopFrm.VariantFrm.UndoRedoFrm.GotoMinus.Title = "<--";
TopFrm.VariantFrm.UndoRedoFrm.GotoMinus.LongHelp = "Go back to previous step";
TopFrm.VariantFrm.UndoRedoFrm.GotoPlus.Title = "-->";
TopFrm.VariantFrm.UndoRedoFrm.GotoPlus.LongHelp = "Go to next step";
TopFrm.VariantFrm.UndoRedoFrm.GotoLast.Title = "->>";
TopFrm.VariantFrm.UndoRedoFrm.GotoLast.LongHelp = "Go to last step";

TopFrm.VariantFrm.UndoLevel.Title = "Undo Level";

TopFrm.VariantFrm.ComparisonFrm.Comparison.Title = "Comparison";
TopFrm.VariantFrm.ComparisonFrm.Comparison.LongHelp = "Main switch for the display of analysis on previous steps";
TopFrm.VariantFrm.DeviationFrm.Relative.Title = "Relative";
TopFrm.VariantFrm.DeviationFrm.Relative.LongHelp = "The step for variant comparison is always one step before the current step";
TopFrm.VariantFrm.DeviationFrm.Deviation.Title = "Deviation";
TopFrm.VariantFrm.DeviationFrm.Deviation.LongHelp = "Main switch for the display deviation related to previous steps";

TopFrm.CommandFrm.Title = "Command";
TopFrm.CommandFrm.ShowOptions.Title = "Options";
TopFrm.CommandFrm.ShowOptions.LongHelp = "Shows the dialog of the current command";
TopFrm.CommandFrm.CommandAccelerator.Title = "";

TopFrm.CommandFrm.Selection.Title = "Select";
TopFrm.CommandFrm.Selection.LongHelp = "Activates the selection for the current command";
TopFrm.CommandFrm.SelectionDone.Title = "Done";
TopFrm.CommandFrm.SelectionDone.LongHelp = "Terminates the current selection";

TopFrm.CommandFrm.ShowTools.Title = "Tools";
TopFrm.CommandFrm.ShowTools.LongHelp = "Shows the tools of the current command";
TopFrm.CommandFrm.ShowFlags.Title = "Flags";
TopFrm.CommandFrm.ShowFlags.LongHelp = "Shows the markers of all geometric objects which are selected for the current command";

BottomFrm.Preferences.Title = "Preferences";

Context.Options = "Options";
Context.ShowAll = "Show All";
Context.HideAll = "Hide All";
Context.InactivateAll = "Inactivate All";
Context.Select = "Select";
Context.Modify = "Modify";
Context.Implicit = "Implicit";
Context.Tools = "Tools";
Context.Flags = "Flags";
Context.Hide = "Hide";

// Toolbar
VariantToolBar.Title = "Commands and Variants";
UndoCombo2.ShortHelp = "Current Variant";
UndoCombo2.LongHelp = "Access to previous steps of modifications listed in the action list";
GotoFirst.ShortHelp = "Initial Variant";
GotoFirst.LongHelp = "Initial Variant";
GotoMinus.ShortHelp = "Undo";
GotoMinus.LongHelp = "Undo";
GotoPlus.ShortHelp = "Redo";
GotoPlus.LongHelp = "Redo";
GotoLast.ShortHelp = "Latest Variant";
GotoLast.LongHelp = "Latest Variant";
CommandCombo2.ShortHelp = "Current Command";
CommandCombo2.LongHelp = "Selects the current command";
ShowOptions.ShortHelp = "Options";
ShowOptions.LongHelp = "Shows the dialog of the current command";
ShowTools.ShortHelp = "Tools";
ShowTools.LongHelp = "Shows the tools of the current command";
SelectionDone.ShortHelp = "Select";
SelectionDone.LongHelp = "Activates/deactivates the selection for the current command";

ControlPointsToolbar.Title = "Control Points";
SysIconBox.Title = "System";
SysIconBox.ShortHelp = "System";
RowIconBox.Title = "Row Diffusion";
RowIconBox.ShortHelp = "Row Diffusion";
CroIconBox.Title = "Cross Diffusion";
CroIconBox.ShortHelp = "Cross Diffusion";
AttIconBox.Title = "Attenuation";
AttIconBox.ShortHelp = "Attenuation";
SysIconBox.SysButtonView.ShortHelp = "View";
SysIconBox.SysButtonModel.ShortHelp = "Model";
SysIconBox.SysButtonCompass.ShortHelp = "Compass";
SysIconBox.SysButtonNormal.ShortHelp = "Normal";
SysIconBox.SysButtonMesh.ShortHelp = "Mesh";
RowIconBox.RowButtonConstant.ShortHelp = "Constant";
RowIconBox.RowButtonLinear.ShortHelp = "Linear";
RowIconBox.RowButtonConcave.ShortHelp = "Concave";
RowIconBox.RowButtonConvex.ShortHelp = "Convex";
RowIconBox.RowButtonBell.ShortHelp = "Bell";
CroIconBox.CroButtonConstant.ShortHelp = "Constant";
CroIconBox.CroButtonLinear.ShortHelp = "Linear";
CroIconBox.CroButtonConcave.ShortHelp = "Concave";
CroIconBox.CroButtonConvex.ShortHelp = "Convex";
CroIconBox.CroButtonBell.ShortHelp = "Bell";
AttIconBox.AttButtonLow.ShortHelp = "Low (0.25)";
AttIconBox.AttButtonMedium.ShortHelp = "Medium (0.1)";
AttIconBox.AttButtonHigh.ShortHelp = "High (0.01)";
CptSelectButton.ShortHelp = "Select";
