//
// CWI
//
// ---------------------------------------------------------------------------
// Open Dialog
// ---------------------------------------------------------------------------
//
CATStjDocumentSelector.Title     = "SMARTEAM (CWI)";
CATStjDocumentSelector.ShortHelp = "Look for a document in SmarTeam database";
CATStjDocumentSelector.LongHelp  = "Look for a document in SmarTeam database
If connected to a SmarTeam database, allow to retrieve a document by using
various search methods including:
- search by attribute
- navigation in the projets/folders tree";


//
// ---------------------------------------------------------------------------
// NeedRestart popup window in Tools/Options
// ---------------------------------------------------------------------------
//
NeedRestart.Title   = "Warning";
NeedRestart.Message = "Restart session to take this modification into account.";


//
// ---------------------------------------------------------------------------
// ErrorInsertComponent popup window
// ---------------------------------------------------------------------------
//
ErrorInsertComponent.Title   = "Warning";
ErrorInsertComponent.Message = "No current product. Please select a Product before calling this command.";


//
// ---------------------------------------------------------------------------
// ErrorNoDocument popup window
// ---------------------------------------------------------------------------
//
ErrorNoDocument.Title   = "Warning";
ErrorNoDocument.Message = "No current document. Please select a document before calling this command.";


//
// ---------------------------------------------------------------------------
// InfoDoesNotExist popup window
// ---------------------------------------------------------------------------
//
InfoDoesNotExist.Title   = "Information";
InfoDoesNotExist.Message = "Current object or document does not exist in the database.";


//
// ---------------------------------------------------------------------------
// Message during Check-Out on the fly
// ---------------------------------------------------------------------------
//
CheckOutOnTheFly.Title = "Check-Out on the Fly";
CheckOutOnTheFly.Message1 = "Check-Out or New-Release is requested on:";
CheckOutOnTheFly.Message2 = "\nWould you like to perform the check-out/new-release operation ?";

// ---------------------------------------------------------------------------
// Messages during "Replace With Selected Revision"
// ---------------------------------------------------------------------------
//
Error1RWSR.Title   = "Warning";
Error1RWSR.Message = "No current product. Please select a Product before calling this command.";
Error2RWSR.Title   = "Error";
Error2RWSR.Message = "Cannot perform action on selected Product.";
Error3RWSR.Title   = "Error";
Error3RWSR.Message = "An error occurred during the operation";


//
// ---------------------------------------------------------------------------
// Messages during "Undo Check-out"
// ---------------------------------------------------------------------------
//
Error1UCO.Title   = "Warning";
Error1UCO.Message = "No current product. Please select a Product before calling this command.";
Error2UCO.Title   = "Error";
Error2UCO.Message = "Cannot perform action on selected Product.";
Error3UCO.Title   = "Error";
Error3UCO.Message = "An error occurred during the operation";
Error4UCO.Title   = "Warning";
Error4UCO.Message = "SmarTeam failed to delete file: \n/p1\n from the Work directory \n OR \n User has not selected delete file option during Undo CheckOut Operation.\nCATIA will continue loading the current document from the Work directory";


//
// ---------------------------------------------------------------------------
// User should not modify content of SMARTEAM Tools/Options page if already
// connected
// ---------------------------------------------------------------------------
//
AlreadyConnected.Title = "Warning";
AlreadyConnected.Text  = "The content of this page should not be modified while connected.";


//
// ---------------------------------------------------------------------------
// CWI Licensing problems
// ---------------------------------------------------------------------------
//
CWILicensingProblem1.Title = "Error";
CWILicensingProblem1.Text  = "CWI not available because of CATIA licensing policy.";

CWILicensingProblem2.Title = "Error";
CWILicensingProblem2.Text  = "CWI not available because of missing SmarTeam components.";

CWILicensingProblem3.Title = "Error";
CWILicensingProblem3.Text  = "No CWI license available.";


//
// ---------------------------------------------------------------------------
// Errors while managing requests from WED thru backbone
// ---------------------------------------------------------------------------
//
RequestFromWED.ConnectionPB   = "Failed to connect to WED";
RequestFromWED.UnknownRequest = "Error while processing WED request";
RequestFromWED.NotProcessed   = "Message from WED will not be processed";


//
// ---------------------------------------------------------------------------
// Error messages reported by SmarTeam code
// ---------------------------------------------------------------------------
//
SmarTeamError.1 = "FAILED: No connection to the SMARTEAM server";
SmarTeamError.2 = "FAILED: SMARTEAM Server return configuration error. Please contact your system administrator";
SmarTeamError.3 = "FAILED: SMARTEAM Server return application error. Please contact your system administrator";


//
// ---------------------------------------------------------------------------
// Undo CheckOut  Doc not unloadable
// ---------------------------------------------------------------------------
//
ErrorNotUnloadable.Title = "Error";
ErrorNotUnloadable.Text  = "Document cannot be unloaded";
ErrorNotUnloadableDirty.Title = "Error";
ErrorNotUnloadableDirty.Text  = "Document cannot be unloaded because it is dirty\nSave the document on the disk and retry\n";
//

//
// ---------------------------------------------------------------------------
// Replace Selected Revision. Doc not unloadable
// ---------------------------------------------------------------------------
//
ErrorReplaceForbidden.Title = "Replace Selected Revision Forbidden";
ErrorReplaceForbidden.Text  = "Command can't be performed on following document:\n/p1\nThis document could be pointed by another modified document in session or one of its pointing documents hold it!";
//


//
// ---------------------------------------------------------------------------
// Replace Selected Revision. Warning
// ---------------------------------------------------------------------------
//
WarningReplaceLooseChange.Title = "Replace Selected Revision. (Modified Documents)";
WarningReplaceLooseChange.Text  = "/p1 is modified in session. Replace is possible but you will lose all your modifications on this document!\nDo you want to continue the operation?";
//

// ---------------------------------------------------------------------------
// Messages for the Report Window
// ---------------------------------------------------------------------------
//
ReportWindow.ERR_0001 = "SMARTEAM Internal Error:\n/p\nContact your Administrator";

ReportWindow.ERR_TMP01 = "ERROR: Document not synchronized with persistency:/p";

//
// ---------------------------------------------------------------------------
// Errors when saving documents on disk
// Can occur during ST/Save, ST/LF/CheckIn, ST/LF/Release operations
// ---------------------------------------------------------------------------
//
DSM.FAILED = "ERROR: FAILED to save file(s) on disk";
DSM.KO     = "ERROR: Document could not be saved on disk:/p";

//
// ---------------------------------------------------------------------------
// Warning
// occur during ST/Save, ST/LF/CheckIn operations for V5Dirty and ReadOnly
// documents
// ---------------------------------------------------------------------------
//

WarningSaveCI.Title = "Warning";
WarningSaveCI.Text = "The following document(s) have been modified, but are in read-only mode:\n/p1\nIt is strongly recommended to solve this problem before saving the assembly.\nDo you want to proceed anyway ? ";

//
// ---------------------------------------------------------------------------
// Highlight - CWI001 - Usability Improvement
// Messages for Progress Indicator Dialog
// 
// ---------------------------------------------------------------------------
//

ProgressIndicatorDlg.DefaultTitle = "TASK IN PROGRESS";
ProgressIndicatorDlg.Downloading_Files = "DownLoading Files";
ProgressIndicatorDlg.Uploading_Files = "Uploading Files";
ProgressIndicatorDlg.Updating_Documents = "Updating Documents";
ProgressIndicatorDlg.Updating_SmarTeam = "Updating SmarTeam";
ProgressIndicatorDlg.Retrieving_data_from_SmarTeam = "Retrieving data from SmarTeam";
ProgressIndicatorDlg.Retrieving_Product_Structure = "Retrieving Product Structure";
ProgressIndicatorDlg.UndefinedTask = "UNDEFINED TASK";

//
// ---------------------------------------------------------------------------
// Highlight - CWI001 - Usability Improvment
// Expose Mode
//
// ---------------------------------------------------------------------------
ExposeMode.Title = "Expose Mode";
ExposeMode.Text = "Expose Mode set to different values in CATIA Options and in SmarTeam System Configuration.\n Please consult your Administrator to set same values.";


//
// ---------------------------------------------------------------------------
// SmarTeamConnect  popup window
// ---------------------------------------------------------------------------
//
SmarTeamConnect.Title   = "SmarTeam Connect";
SmarTeamConnect.Text = "Current CATIA version is not compatable with SmarTeam Version installed.\nPlease install SmarTeam V5R19 or above version";
