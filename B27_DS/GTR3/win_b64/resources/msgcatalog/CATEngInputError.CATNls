// *--------------------------------------------*//
// *               Message example              *//
// *                                            *//
// *------------------------------------------- *//
// ENGINPUT_ERR_XXX.Request    = "Operation failed";
// ENGINPUT_ERR_XXX.Diagnostic = "Curve support /p1 is invalid";
// ENGINPUT_ERR_XXX.Advice     = "Choose a smoother curve";


///////////////////////////////////////////////////////////////////////////
//                            ATTRIBUTE ERRORS                           //
///////////////////////////////////////////////////////////////////////////
// Mandatory input
ENGINPUT_ERR_MANDATORYINPUT.Request    = "Sick feature /p1.";
ENGINPUT_ERR_MANDATORYINPUT.Diagnostic = "/p1 attribute is missing.";
ENGINPUT_ERR_MANDATORYINPUT.Advice     = "Edit your feature and set this attribute.";

// Null value
ENGINPUT_ERR_NULLVALUE.Request    = "Sick feature /p1.";
ENGINPUT_ERR_NULLVALUE.Diagnostic = "/p1 attribute has a null value.";
ENGINPUT_ERR_NULLVALUE.Advice     = "Edit your feature and set this attribute to a non null value.";

// Useless limit
ENGINPUT_ERR_USELESSLIMIT.Request    = "Sick feature /p1.";
ENGINPUT_ERR_USELESSLIMIT.Diagnostic = "The limit /p1 is useless.";
ENGINPUT_ERR_USELESSLIMIT.Advice     = "Remove this unecessary data.";

// limit of trace
ENGINPUT_ERR_OFFSETORIENTLIMIT.Request    = "Sick feature /p1.";
ENGINPUT_ERR_OFFSETORIENTLIMIT.Diagnostic = "The limit can not be used as defined.";
ENGINPUT_ERR_OFFSETORIENTLIMIT.Advice     = "Change the offset or the orientation of the limit.";


///////////////////////////////////////////////////////////////////////////
//                           TOPOLOGICAL ERRORS                          //
///////////////////////////////////////////////////////////////////////////
// No body associated
ENGINPUT_ERR_BODY.Request    = "Sick feature /p1.";
ENGINPUT_ERR_BODY.Diagnostic = "/p2 does not have any body.";
ENGINPUT_ERR_BODY.Advice     = "Try to improve your geometry or build again this feature.";

// Nb of domains wanted
ENGINPUT_ERR_DOM.Request    = "Sick feature /p1.";
ENGINPUT_ERR_DOM.Diagnostic = "The number of domains of /p1 is /p2.";
ENGINPUT_ERR_DOM.Advice     = "The number of domains must be /p1.";

// Multi-domains
ENGINPUT_ERR_MONODOM.Request    = "Sick feature /p1.";
ENGINPUT_ERR_MONODOM.Diagnostic = "Multi-domains result forbidden.";
ENGINPUT_ERR_MONODOM.Advice     = "Try to improve your geometry.";

// Dimension error
ENGINPUT_ERR_DIM.Request    = "Sick feature /p1.";
ENGINPUT_ERR_DIM.Diagnostic = "Dimension of /p1 is /p2.";
ENGINPUT_ERR_DIM.Advice     = "It must be /p1.";

// Infinite plane
ENGINPUT_ERR_INFINITE.Request    = "Sick feature /p1.";
ENGINPUT_ERR_INFINITE.Diagnostic = "/p1 is not infinite.";
ENGINPUT_ERR_INFINITE.Advice     = "Give an infinite plane.";

// Infinite geometry forbidden
ENGINPUT_ERR_FINITE.Request    = "Sick feature /p1.";
ENGINPUT_ERR_FINITE.Diagnostic = "/p1 is infinite.";
ENGINPUT_ERR_FINITE.Advice     = "Give an finite geometry.";

// Opened wire
ENGINPUT_ERR_OPENEDWIRE.Request    = "Sick feature /p1.";
ENGINPUT_ERR_OPENEDWIRE.Diagnostic = "/p1 is an opened wire.";
ENGINPUT_ERR_OPENEDWIRE.Advice     = "Try to improve your geometry.";

// Closed wire
ENGINPUT_ERR_CLOSEDWIRE.Request    = "Sick feature /p1.";
ENGINPUT_ERR_CLOSEDWIRE.Diagnostic = "/p1 is a closed wire.";
ENGINPUT_ERR_CLOSEDWIRE.Advice     = "Try to improve your geometry.";

// Multi-intersections
ENGINPUT_ERR_MULTIINTERSECT.Request    = "Sick feature /p1.";
ENGINPUT_ERR_MULTIINTERSECT.Diagnostic = "/p1 leads to a multi-intersections result.";
ENGINPUT_ERR_MULTIINTERSECT.Advice     = "Try to improve your geometry.";

// Projection ambiguity
ENGINPUT_ERR_PROJECTION.Request    = "Ambigous case of projection";
ENGINPUT_ERR_PROJECTION.Diagnostic = "";
ENGINPUT_ERR_PROJECTION.Advice     = "The 3D curve must be laid down on the 3D support surface.";

// Projection gives no result
ENGINPUT_ERR_NULLPROJECTION.Request    = "Projection failed.";
ENGINPUT_ERR_NULLPROJECTION.Diagnostic = "The projection on /p1 failed.";
ENGINPUT_ERR_NULLPROJECTION.Advice     = "Modify your inputs.";

// Intersection gives no result
ENGINPUT_ERR_NULLINTERSECTION.Request    = "Intersection failed.";
ENGINPUT_ERR_NULLINTERSECTION.Diagnostic = "The intersection between /p1 and /p2 failed.";
ENGINPUT_ERR_NULLINTERSECTION.Advice     = "Modify your inputs.";

// Intersection gives multi-domains result
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Request    = "Multi-domains Intersection.";
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Diagnostic = "The intersection between /p1 and /p2 gives several solutions.";
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Advice     = "Modify your inputs.";

// Line of null length
ENGINPUT_ERR_NULLLENGTH.Request    = "The operation leads to a null length curve.";
ENGINPUT_ERR_NULLLENGTH.Diagnostic = "Two points are confused.";
ENGINPUT_ERR_NULLLENGTH.Advice     = "Modify your inputs.";

// Intersection Curve-Surface is not a point
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Request    = "Intersection failed.";
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Diagnostic = "The intersection between /p1 and /p2 does not give a point.";
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Advice     = "Modify your inputs.";

// Intersection Surface-Surface is not a curve
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Request    = "Intersection failed.";
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Diagnostic = "The intersection between /p1 and /p2 does not give a curve.";
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Advice     = "Modify your inputs.";

// Intersection Surface-Surface-Surface is not a point
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Request    = "Intersection failed.";
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Diagnostic = "The intersection between /p1, /p2 and /p3 does not give a point.";
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Advice     = "Modify your inputs.";

// Offset too big
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Request    = "Point on curve.";
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Diagnostic = "The offset value and the orientation lead to a point outside of the curve.";
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Advice     = "Modify your inputs.";

// Minimal segment length
ENGINPUT_ERR_MINSEGMENTLENGTH.Request    = "Minimal segment length.";
ENGINPUT_ERR_MINSEGMENTLENGTH.Diagnostic = "At least one of the straight segments of the curve /p1 is smaller than the minimal length.";
ENGINPUT_ERR_MINSEGMENTLENGTH.Advice     = "Check your specifications.";

// Curvature check for the guide of the sweep
ENGINPUT_ERR_CURVESWEEPCURVATURE.Request    = "Curvature problem.";
ENGINPUT_ERR_CURVESWEEPCURVATURE.Diagnostic = "The guide curve shows a too small curvature for the size of this section.";
ENGINPUT_ERR_CURVESWEEPCURVATURE.Advice     = "Adjust the size of your section or the curvature of your guide curve.";

// Curve lay down on a surface
ENGINPUT_ERR_CURVELAYDOWN.Request    = "Curve not laid down on the surface.";
ENGINPUT_ERR_CURVELAYDOWN.Diagnostic = "The guide curve /p1 is not laying down on the selected surface /p2.";
ENGINPUT_ERR_CURVELAYDOWN.Advice     = "Fix your geometry.";
