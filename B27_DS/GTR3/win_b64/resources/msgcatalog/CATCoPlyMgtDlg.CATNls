//-----------------------------------------------------------------------------
//  File: CATCoPlyMgtDlg.CATNls
//  Desc: Internationalization
//-----------------------------------------------------------------------------
//  mm-dd-yyyy  History                                                    user
//  2008 xx xx  Cre                                                        HDK
//  2012 07 17  Use "Core Sample" Nls name consistent naming for all command using Core Sample PEY/VMU  
//-----------------------------------------------------------------------------


Title = "Ply management";

PliesFrame.ComponentsFrame.Title = "Components ";
PliesFrame.ComponentsFrame.LongHelp = "Lists the Composites elements found, together with related information.";
VisuAndFilterFrame.FilterFrame.Title="Filters";
VisuAndFilterFrame.FilterFrame.LongHelp="Filters the displayed entities from criteria";
PliesFrame.DisplayFrame.Title = "Display ";
PliesFrame.DisplayFrame.ColumnButton.Title = "Column... ";
PliesFrame.DisplayFrame.ColumnButton.LongHelp = "Lets you select the types of information, thus the columns, to display.";
VisuAndFilterFrame.VisuFrame.Title= " Visualize Shell"; 
PliesFrame.SelectionModeFrame.Title = "Selection Mode ";
PliesFrame.SelectionModeFrame.LongHelp = "Lets you define the type of Composites elements to select, or deselect.\n   Choose a type of elements to select,\n   Select lines in the list on the right,\n   Click Select/Deselect.\n Your selection is identified by a symbol on the left of the lines.";
PliesFrame.SelectionModeFrame.PlyRad.Title = "Ply";
PliesFrame.SelectionModeFrame.SequenceRad.Title = "Sequence";
PliesFrame.SelectionModeFrame.PlyGroupRad.Title = "Plies Group";
PliesFrame.SelectionModeFrame.CutPieceRad.Title = "Cut Piece";
PliesFrame.SelViewParameters.Title ="Customized parameters";	 
PliesFrame.SelViewParameters.PlyParamcheck.Title =" Ply";
PliesFrame.SelViewParameters.CutPieceParamCheck.Title =" Cut Piece";
PliesFrame.MoveFrame.Title = "Move";
PliesFrame.MoveFrame.MoveButton.Title = "Move...";

PliesFrame.CreateFrame.Title = "Create";
PliesFrame.CreateFrame.CreateButton.Title = "Create...";
PliesFrame.TableExportFrame.ExportButton.Title ="Export stacking access";

VisuAndFilterFrame.FilterFrame.PlyGrpFilterLabel.Title="Plies Group ";
VisuAndFilterFrame.FilterFrame.PlyGrpFilterLabel.LongHelp="Filters displayed lines from Plies Group Name";
VisuAndFilterFrame.FilterFrame.SequenceFilterLabel.Title="Sequence ";
VisuAndFilterFrame.FilterFrame.SequenceFilterLabel.LongHelp="Filters displayed lines from Sequence Name";
VisuAndFilterFrame.FilterFrame.PlyFilterLabel.Title="Ply ";
VisuAndFilterFrame.FilterFrame.PlyFilterLabel.LongHelp="Filters displayed lines from Ply Name";
VisuAndFilterFrame.FilterFrame.CutPieceFilterLabel.Title="Cut-piece ";
VisuAndFilterFrame.FilterFrame.CutPieceFilterLabel.LongHelp="Filters displayed lines from Cut-piece Name";
VisuAndFilterFrame.FilterFrame.MaterialFilterLabel.Title="Material ";
VisuAndFilterFrame.FilterFrame.MaterialFilterLabel.LongHelp="Filters displayed lines from Material";
VisuAndFilterFrame.FilterFrame.DirectionFilterLabel.Title="Direction ";
VisuAndFilterFrame.FilterFrame.DirectionFilterLabel.LongHelp="Filters displayed lines from Direction";
VisuAndFilterFrame.FilterFrame.SurfaceFilterLabel.Title="Surface ";
VisuAndFilterFrame.FilterFrame.SurfaceFilterLabel.LongHelp="Filters displayed lines from Surface";
VisuAndFilterFrame.FilterFrame.RosetteFilterLabel.Title="Rosette ";
VisuAndFilterFrame.FilterFrame.RosetteFilterLabel.LongHelp="Filters displayed lines from Rosette";
VisuAndFilterFrame.FilterFrame.DrapingFilterLabel.Title="Draping ";
VisuAndFilterFrame.FilterFrame.DrapingFilterLabel.LongHelp="Filters displayed lines from Draping";
VisuAndFilterFrame.FilterFrame.CSFilterLabel.Title ="Core Sample";
VisuAndFilterFrame.FilterFrame.CSFilterLabel.LongHelp="Filters displayed lines from Presence under Core Sample location";

PliesFrame.SelectRemoveButton.Title="Select/Deselect";

PliesFrame.SummaryFrame.Title = "Summary";
PliesFrame.SummaryFrame.3DContourLabel.Title = "Ply Contour: ";
PliesFrame.SummaryFrame.FlattenContourLabel.Title = "Flatten Contour: ";
PliesFrame.SummaryFrame.ExplodedSurfaceLabel.Title = "Exploded Surface: ";
PliesFrame.SummaryFrame.ProducibilityLabel.Title = "Producibility: ";
	 
PliesFrame.PlyPropertyFrame.Title = "Ply(ies) Properties";
PliesFrame.PlyPropertyFrame.MaterialLabel.Title = "Material ";
PliesFrame.PlyPropertyFrame.DirectionLabel.Title = " Direction ";
PliesFrame.PlyPropertyFrame.RosetteLabel.Title = " Rosette ";
PliesFrame.PlyPropertyFrame.DrapingLabel.Title = " Draping Direction ";
	 
PliesFrame.RenameFrame.Title = "Rename";
PliesFrame.RenameFrame.NameLabel.Title = "Name ";
PliesFrame.RenameFrame.StartLabel.Title = " Start Index ";
PliesFrame.RenameFrame.IncremationLabel.Title = " Step ";
PliesFrame.RenameFrame.DigitNbLabel.Title = " Nb of Digits ";
PliesFrame.RenameFrame.RenameButton.Title = "Rename";

PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle1="  ";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle2="  ";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle3="Plies Group";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle4 ="Sequence";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle5="Ply";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle6 = "Cut Piece";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle7="Material";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle8="Direction";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle9="Rosette";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle10="Surface";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle11="Draping";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle12="3D Contour";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle13="Flatten Contour";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle14="Exploded Surface";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle15="Producibility";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle16 ="Core Sample";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle17 ="Limit Contour";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle18 ="Material Excess";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle19 ="Skin Swapping";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle20 ="Stagger Origin";
PliesFrame.ComponentsFrame.PlySelectorMultiList.ColumnTitle21 ="Darts";

More = "More...";
Less = "Less...";
PliesFrame.MoreLessFrame.LongHelp = "Displays/Hides filter options and a browser to inspect the filtered elements.";
PliesFrame.MoreLessFrame.MorePB.LongHelp = "Displays/Hides filter options and a browser to inspect the filtered elements.";
PliesFrame.MoreLessFrame.MorePB.Title = "More...";

PlyMgt_Warning.Title = "Interactive Ply Table";
PlyMgt_Warning_01.Message = "The draping Direction of ";
PlyMgt_Warning_02.Message = " is/are locked ";
PlyMgt_Error_06.Message = " Failed to export the table";
PlyMgt_OK.Message ="The table has been correctly exported to the file";
FileExists.Message                   = "A file with the same name already exists";
ReplaceFile.Message                  = "Do you want to replace the existing file?";
