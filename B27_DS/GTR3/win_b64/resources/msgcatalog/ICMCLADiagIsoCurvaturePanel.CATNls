DialogBoxTitle                                   = "Iso-Curvature Analysis";

//Selection options------------------------------------------------------------------------------------

LabelElements.Title                              = "Elements: ";
LabelElements.LongHelp                           = "Selects elements used for the iso-curvature analysis calculation.";
LabelElements.ShortHelp                          = "Surfaces to analyze";


//Distribution frame-----------------------------------------------------------------------------------

FrameDistribution.Title                          = "Distribution";
FrameDistribution.LongHelp                       = "Defines the distribution of the iso-curves in u- and v-direction.
The parameter is taken individually from each
surface without taking into account adjacent surfaces.";

LabelDirection.Title                             = "Direction: ";
LabelDirection.LongHelp                          = "";
LabelDirection.ShortHelp                         = "Analysis direction";

ComboIsoCurvatureMode.LongHelp                   = "- Along U -
  Displays all iso-curves parallel to the 
  U direction of each selected surface element.
- Along V -
  Displays all iso-curves parallel to the 
  V direction of each selected surface element.
- Along U&V 
  Displays all iso-curves parallel to both the 
  U and V direction of each selected surface element.";
ComboIsoCurvatureMode.ShortHelp                  = "The analysis runs along the selected parameter.";

IsoCurvatureMode.AlongU                          = "Along U";
IsoCurvatureMode.AlongV                          = "Along V";
IsoCurvatureMode.AlongUV                         = "Along U & V";


//Number frame------------------------------------------------------------------------------------------

FrameNumber.Title                                = "Number";
FrameNumber.LongHelp                             = "Defines the number of iso-curves in U and V direction.";

LabelNIsoLinesU.Title                            = "U: ";
LabelNIsoLinesU.LongHelp                         = "Number of V iso-curves. The analysis runs in U direction.";
LabelNIsoLinesU.ShortHelp                        = "Number of iso-curves along U";

LabelNIsoLinesV.Title                            = "V: ";
LabelNIsoLinesV.LongHelp                         = "Number of U iso-curves. The analysis runs in V direction.";
LabelNIsoLinesV.ShortHelp                        = "Number of iso-curves along V";


//Range frame-------------------------------------------------------------------------------------------

FrameRange.Title                                 = "Range";
FrameRange.LongHelp                              = "Specifies a range for the specified number of iso-curves.
The range defines with its U/V start and end parameters
the distribution of the iso-curves and the interval 
for the analysis on these curves.";

CheckButtonRange.LongHelp                        = "- CLEARED -
  Distributes the defined number of iso-curves 
  in the parameter range 0 to 1.
- SELECTED -
  Specifies a parameter range if the number of iso-curves 
  is larger than 1. The number of iso-curves is equally 
  distributed between the parameter start and end values.";
CheckButtonRange.ShortHelp                       = "Select/clear parameter range";

LabelStart.Title                                 = "Start";
LabelStart.LongHelp                              = "Specifies the position of the first iso-curve for both U and V direction.";
LabelStart.ShortHelp                             = "Start parameter of the range";

LabelEnd.Title                                   = "End";
LabelEnd.LongHelp                                = "Specifies the position of the last iso-curve for both U and V direction.";
LabelEnd.ShortHelp                               = "End parameter of the range";


//Options frame-----------------------------------------------------------------------------------------

FrameOptions.Title                               = "Options";
FrameOptions.LongHelp                            = "Offers additional options.";

CheckButtonBasicSurface.Title                    = "Basic surface";
CheckButtonBasicSurface.LongHelp                 = "Computes the analysis on the basic surface in case of faces.";
CheckButtonBasicSurface.ShortHelp                = "Analysis on basic surface";

CheckButtonShowUVAlignment.Title                 = "Show UV alignment";
CheckButtonShowUVAlignment.LongHelp              = "If an iso-curvature analysis is computed on multiple surfaces 
which have a non-uniform parameter flow, the result along the 
U or V direction will not be displayed in the same direction. 
The UV direction of each feature has to be aligned to adjust 
the direction of the analysis results.
If Show UV alignment is selected, a manipulator will be shown 
for every surface of the selected features. These manipulators 
indicate the current UV alignment as well as the surface normal.
The UV direction can be inverted via a manipulator point 
at the normal vector.";
CheckButtonShowUVAlignment.ShortHelp             = "Shows manipulators for UV alignment";


//Display frame-----------------------------------------------------------------------------------------

FrameDisplay.Title                               = "Display";
FrameDisplay.LongHelp                            = "Offers parameters controlling the display of the analysis spikes.";

CurvatureType.Curvature                          = "Curvature";
CurvatureType.Radius                             = "Radius";

ComboCurvatureType.ShortHelp                     = "Curvature or curvature radius";
ComboCurvatureType.LongHelp                      = "- Curvature -
  The spikes normal to each analysis curve will display 
  the curvatures (1/r). The longer the spike length
  the smaller the curvature will be.
- Radius - 
  The spikes normal to each analysis curve will display 
  the curvature radii (r). The longer the spike length 
  the larger the curvature will be.";

LabelDensity.Title                               = "Density: ";
LabelDensity.LongHelp                            = "Defines the number of spikes to be distributed over the defined range.";
LabelDensity.ShortHelp                           = "Number of spikes";

SliderDensity.LongHelp                           = "Defines the number of spikes to be distributed over the defined range.";
SliderDensity.ShortHelp                          = "Number of spikes";

LabelScale.Title                                 = "Scale: ";
LabelScale.LongHelp                              = "Defines the scaling factor for displaying the curvature spikes.";
LabelScale.ShortHelp                             = "Scale length of the spikes";

SliderScale.LongHelp                             = "Defines the scaling factor for displaying the curvature spikes.";
SliderScale.ShortHelp                            = "Scale length of the spikes";


//Display Options frame---------------------------------------------------------------------------------

FrameDisplayOptions.Title                        = "Options";
FrameDisplayOptions.LongHelp                     = "Offers display options for the analysis.";

CheckButtonMin.Title                             = "Min.";
CheckButtonMin.LongHelp                          = "Displays local minimum radius values in the Experience Area.";
CheckButtonMin.ShortHelp                         = "Minimum radius";

CheckButtonMax.Title                             = "Max.";
CheckButtonMax.LongHelp                          = "Displays local maximum radius values in the Experience Area.";
CheckButtonMax.ShortHelp                         = "Maximum radius";

CheckButtonIndividualMinMax.Title                = "Individual";
CheckButtonIndividualMinMax.LongHelp             = "Displays minimum and maximum radius values 
for each individual surface in the Experience Area.";
CheckButtonIndividualMinMax.ShortHelp            = "Individual minimum/maximum radius";

CheckButtonReverse.Title                         = "Reverse";
CheckButtonReverse.LongHelp                      = "Inverts the normal direction of the curvature spikes.";
CheckButtonReverse.ShortHelp                     = "Invert the spikes";

LabelComb.Title                                  = "Comb";
LabelComb.LongHelp                               = "";
LabelComb.ShortHelp                              = "Color of the comb";

ColorButtonComb.LongHelp                         = "Defines a color for the iso-curvature lines and comb.";
ColorButtonComb.ShortHelp                        = "Color of the comb";

CheckButtonEnvelope.Title                        = "Envelope";
CheckButtonEnvelope.LongHelp                     = "Selects/cleares the envelope that encompasses the 
iso-curvature comb in either U or V direction.";
CheckButtonEnvelope.ShortHelp                    = "Selects/cleares display of envelope";

ColorButtonEnvelope.LongHelp                     = "Defines a color for the envelope.";
ColorButtonEnvelope.ShortHelp                    = "Color of the envelope";
