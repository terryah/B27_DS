//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2006
//=============================================================================
//
// CATStConnectCheckerAnalysisDialog.CATNls : Resource file for NLS purposes: 
// R18 Connect Checker Dialog Box
//
//=============================================================================
// Dec. 2006                                          Shilpa HAWALDAR - 3DPLM
//=============================================================================

DialogBoxTitle="Connect Checker";

ResultOverlapping.Title = "Overlapping";
ResultNoOverlapping.Title = "No overlapping";
SurfaceSelected.Message = " surface(s)";
CurveSelected.Message = " Curve(s)";
ConnectionsDetected.Message = " Connection(s)";
SingularCurve.Title = "SingularCurve";

MainFrame.InputElementsFrame.Title = "Elements";

MainFrame.InputElementsFrame.InputElementsLable.Title = " Source: ";
MainFrame.InputElementsFrame.StateSelector1.ShortHelp = "Shows Number of Elements selected";
MainFrame.InputElementsFrame.StateSelector1.LongHelp = "Shows Total Number of Elements selected for Connection Analysis";
InputElementsListTitle.Message = "Source Elements List";

MainFrame.InputElementsFrame.TargetElementsLable.Title = " Target: ";
MainFrame.InputElementsFrame.StateSelector2.ShortHelp = "Shows Number of Target Elements selected";
MainFrame.InputElementsFrame.StateSelector2.LongHelp = "Shows Total Number of Target Elements selected for Connection Analysis";
TargetElementsListTitle.Message = "Target Elements List";

MainFrame.SecondRowDummyFrame.CCKTypeFrame.Title="Type";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.LongHelp = "Different types of connections to detect";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurSurCCKChkButton.ShortHelp = "Surface-Surface Connection";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurSurCCKChkButton.LongHelp = "Checks connection between two or more surfaces";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.CurCurCCKChkButton.ShortHelp = "Curve-Curve Connection";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.CurCurCCKChkButton.LongHelp = "Checks connection between two or more curves";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurCurCCKChkButton.ShortHelp = "Surface-Curve Connection";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurCurCCKChkButton.LongHelp = "Checks connection between surfaces and curves";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CrvExtremProjectionCCKChkButton.ShortHelp = "Connection between Curve Extremity and its Projection on Target";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CrvExtremProjectionCCKChkButton.LongHelp = "Checks connection between Curve Extremity of Source Curve and its Projection on Target Curve";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurBorderProjectionCCKChkButton.ShortHelp = "Connection between Surface Border and its Projection on Target";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurBorderProjectionCCKChkButton.LongHelp = "Checks connection between Source Surface Border and its Projection on Target Surface";

MainFrame.SecondRowDummyFrame.CCKTypeFrame.ConnectionWayFrame.BoundaryRadButton.Title = "Boundary";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.ConnectionWayFrame.BoundaryRadButton.LongHelp = "Checks connection between Boundaries of Input elements";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.ConnectionWayFrame.ProjectionRadButton.Title = "Projection";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurBorderProjectionCCKChkButton.LongHelp = "Checks connection between Source element Boundary and its projection on Target element";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.Title="Quick";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.ShortHelp = "Overlap Defect";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.LongHelp = "Checks for overlap type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.ShortHelp = "G0 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.LongHelp = "Checks for G0 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.ShortHelp = "G1 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.LongHelp = "Checks for G1 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.ShortHelp = "G2 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.LongHelp = "Checks for G2 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.ShortHelp = "G3 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.LongHelp = "Checks for G3 type continuity";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Text.Title = ">";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.Title="Full";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.ShortHelp = "Overlap Defect";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.LongHelp = "Checks for Overlap type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.ShortHelp = "G0 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.LongHelp = "Checks for G0 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.ShortHelp = "G1 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.LongHelp = "Checks for G1 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.ShortHelp = "G2 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.LongHelp = "Checks for G2 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.ShortHelp = "G3 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.LongHelp = "Checks for G3 type continuity";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Title="Display";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.LongHelp = "Various Display options for connections detected";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.LimitedColorScale.ShortHelp = "Limited Color Scale";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.LimitedColorScale.LongHelp = "Displays the analysis with limited color range";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.FullColorScale.ShortHelp = "Full Color Scale";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.FullColorScale.LongHelp = "Displays the analysis with Full color range";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Comb.ShortHelp = "Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Comb.LongHelp = "Displays Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Envelop.ShortHelp = "Envelop";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Envelop.LongHelp = "Displays Envelop";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.Title="Amplitude";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.LongHelp = "Defines Amplitude of Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.AutoScaling.ShortHelp = "Auto Scaling";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.AutoScaling.LongHelp = "Enables auto scaling of Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.MultiplyTwo.ShortHelp = "Multiply by Two";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.MultiplyTwo.LongHelp = "Multiplies scale factor by 2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.DivideTwo.ShortHelp = "Divide by Two";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.DivideTwo.LongHelp = "Divides scale factor by 2";

MainFrame.ConnectionFrame.Title = "Connection";
MainFrame.ConnectionFrame.LongHelp = "Defines parameters to search connections between input elements";
MainFrame.ConnectionFrame.MinGapLabel.Title = "Minimum Gap";
MainFrame.ConnectionFrame.MaxGapLabel.Title = "Maximum Gap";
MainFrame.ConnectionFrame.InternalEdgeChkButton.ShortHelp = "Internal Edge";
MainFrame.ConnectionFrame.InternalEdgeChkButton.LongHelp = "The internal edges of the joint elements are taken into account to perform the analysis";
//Start AV7 11:05:03
MainFrame.ConnectionFrame.IgnoreFreeEdgesChkButton.ShortHelp = "Ignore Small Free Edges";
MainFrame.ConnectionFrame.IgnoreFreeEdgesChkButton.LongHelp = "Ignores the free edges which have size less than the maximum gap specified";
//End AV7 11:05:03

MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.Title = "Information";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.LongHelp = "Shows all Min and Max values for each connection found";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MinInfo.ShortHelp = "MinInfo";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MinInfo.LongHelp = "Displays minimum values";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MaxInfo.ShortHelp = "MaxInfo";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MaxInfo.LongHelp = "Displays maximum values";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.G1ReturnedValue.ShortHelp = "G1 values within range of 0 to 90 degree";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.G1ReturnedValue.LongHelp = "Displays G1 values within range of 0 to 90 degree only";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.FullG2Concavity.LongHelp = "Checks for Concavity Defect for Surface-Surface and Surface-Curve 
or Angle bewteen osculator planes for Curve-Curve in G2 mode";

MainFrame.FifthRowDummyFrame.DiscretizationFrame.Title = "Discretization";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.LongHelp = "Allows the user to switch between the four predefined discretization steps";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.ShortHelp = "Light Discretization";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.LongHelp = "Sets the Discretization type to Light";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.ShortHelp = "Coarse Discretization";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.LongHelp = "Sets the Discretization type to Coarse";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.ShortHelp = "Medium Discretization";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.LongHelp = "Sets the Discretization type to Medium";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.ShortHelp = "Fine Discretization";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.LongHelp = "Sets the Discretization type to Fine";

MainFrame.MaxValuesDisplayFrame.Title = "Max Deviation";
MainFrame.MaxValuesDisplayFrame.LongHelp = "Shows Max value for each continuity from the current connections";
MainFrame.MaxValuesDisplayFrame.G0TitleLabel.Title = "G0:";
MainFrame.MaxValuesDisplayFrame.G0Label = "----  ";
MainFrame.MaxValuesDisplayFrame.G1TitleLabel.Title = "G1:";
MainFrame.MaxValuesDisplayFrame.G2TitleLabel.Title = "G2:";
MainFrame.MaxValuesDisplayFrame.G3TitleLabel.Title = "G3:";

MinGapCkeFrame.StatusBarHelp = "Change value of parameter: Minimum Gap required.";
MinGapCkeFrame.LongHelp = "Defines the minimum gap value for all the deviations to be displayed.";
MaxGapCkeFrame.StatusBarHelp = "Change value of parameter: Maximum Gap required.";
MaxGapCkeFrame.LongHelp = "Defines the maximum gap value upto which connection analysis 
will be done between input elements.";
G0Cke.StatusBarHelp = "Change value of parameter: G0 threshold value required.";
G0Cke.LongHelp = "Defines G0 threshold value for quick analysis.";
G1Cke.StatusBarHelp = "Change value of parameter: G1 threshold value required.";
G1Cke.LongHelp = "Defines G1 threshold value for quick analysis.";
G2Cke.StatusBarHelp = "Change value of parameter: G2 threshold value required.";
G2Cke.LongHelp = "Defines G2 threshold value for quick analysis.";
G3Cke.StatusBarHelp = "Change value of parameter: G3 threshold value required.";
G3Cke.LongHelp = "Defines G3 threshold value for quick analysis.";

SurfaceSurfaceCCKConcavity.ShortHelp = "Concavity Defect";
CurveCurveCCKConcavity.ShortHelp = "Angle between Osculator Planes";
CCKConcavity.ShortHelp = "Concavity Defect or Angle bewteen osculator planes";


