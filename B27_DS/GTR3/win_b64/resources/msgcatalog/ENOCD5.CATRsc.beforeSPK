//
// How to generate SVG derived outputs
// Should we generate them with 3DPlay compatibility ?
//
// key = DerivedOutput.SVG.Mode
//
// default value  = 2
//
// allowed values = 1 = legacy mode, based on File/SaveAs and honoring
//                      Tools/Options settings
//                  2 = suitable for 3DPlay with hardcoded options
//
// value in case of error/typo/missing key: same as default
//
DerivedOutput.SVG.Mode = "2";

//
// How to generate SVG derived outputs
// In 3DPlay mode, should we take into account the background
// (if not, background will be white & white wireframe/text will be black
//
// key = DerivedOutput.SVG.V5background
//
// default value  = true
//
// allowed values = true     = V5 background is taken into account
//                  false/no = V5 background is not taken into account
//                          background will be white
//                          white wireframe & text will be drawn as black
//
// value in case of error/typo/missing key: same as default
//
DerivedOutput.SVG.V5Background="true";

//
// How to generate SVG derived outputs
// In 3DPlay mode (based on print), should we clip to sheet format?
//
// key = DerivedOutput.SVG.ClipToSheet
//
// default value  = true
//
// allowed values = true/yes  = drawing is clipped to sheet format
//                  false/no  = drawing can extend past sheet format
//
// value in case of error/typo/missing key: same as default
//
DerivedOutput.SVG.ClipToSheet="true";

//
// Configurable search limit
// Set a maximum limit for the search results on Open panel and interactive query in reconciliator
//
// CATRsc key = Search.MaxResult
//
// default value  = 1000
//
// allowed values = any integer value higher than 0 (zero)
//
// value in case of error/typo/missing key: same as default value (1000)
//
Search.MaxResult = "1000";

//
// Configure Visibility of refresh folder button
// on Save panel (on workspaceBroserDialog), open panel (Workspace tab)
//
// It is applicable for 15x and above
//
// CATRsc key: FolderTree.Refresh
//
// allowed values (case Insensitive) = true = Refresh Folder Button visible 
//									 = false = Refresh Folder Button hidden.
//
// Any values other than "true" regarded as "false".
//
FolderTree.Refresh = "true";

//
// Download CGR to V5 cache
// Make it optional to download cgr to V5 cache
//
// key = DownloadCgrToV5Cache
//
// default value  = true
//
// allowed values = true  = cgr will be downloaded to V5 cache
//                  false  = cgr will not be downloaded to V5 cache
//
// value in case of error/typo/missing key: same as default
//
DownloadCgrToV5Cache="true";

//
// Default value of save scope as seen in Advanced Save dialog.
//
// CATRsc key: AdvancedSave.DefaultScope
//
// Possible values (not case sensitive) = �Session� 
//										= �ActiveDocument� 
//										= �CurrentEditor� 
//										= �CATPreference�
//
// Default value: �CATPreference�
//
//Value in case of missing key or incorrect value: �CATPreference�
//
AdvancedSave.DefaultScope = "CATPreference";

//
// Providing Expand / ExpandAll RMB menu items
//
// key = RMB.Expand
//
// default value  = false
//
// allowed values = true/yes  = RMB/Expand and RMB/ExpandAll are provided
//                  false/no  = some RMB/Expand or RMB/ExpandAll may be missing
//
// value in case of error/typo/missing key: same as default
//
RMB.Expand = "false";

//
// Configure explicit expand of nodes for validating revision conflict
//
// key = ExpandForRevisionConflict
//
// default value  = false
//
// allowed values = true/yes  = Nodes expanded explictly prior to revision conflict checking.
//                  false/no  = Nodes are not expanded explictly for checking.  
//
// value in case of error/typo/missing key: same as default
//
ExpandForRevisionConflict = "false";

//
// Explore Menu Item should be visible or Not on
// Open Panel,3DEXPERIENCE Menu Item , SpecTree, Overwrite panel, Save Dialog, LockAndFlyDialog
//
// CATRsc key: ExploreMenuItem
//
// allowed values(not case sensitive) = true/yes = Explore Menu Item visible
//									  = false/no = Explore Menu Item hidden
//
// If value is incorrect or missing : behavior is like true/yes
//									  Explore Menu Item visible
//
// Default value : true
//
ExploreMenuItem = "true";

//
// Launch3DDashboard Menu Item should be visible or not
// It will be added in "3DEXPERIENCE" Menu
//
// CATRsc key: Launch3DDashboardMenuItem
//
// allowed values(not case sensitive) = true/yes = Launch3DDashboard Menu Item will be visible
//                                    = false/no = Launch3DDashboard Menu Item will not be visible
//
// If value is incorrect or missing : behavior is like false/no
//                                    Launch3DDashboardMenuItem Menu Item will not be visible
//
// Default value : false
//
Launch3DDashboardMenuItem = "false";

//
// For Search request("Workspace folder"), new protocol/old protocol will be used.
// Use of new protocol will improve the performance
//
// Old protocol : One criteria node per workspace folder
// New protocol : One criteria node for all workspace folder
//
// CATRsc key: SearchWorkspacefolderMulti
//
// allowed values(not case sensitive) = true/yes = our code will use new protocol.
//									  = false/no = our code will use old protocol  
//									  = automatic = For R2017x GA (or higher) server: our code will use new protocol
//													For older server: our code will use old protocol
//
// If value is incorrect or missing : behavior is like default value
//
// Default value : automatic
//
SearchWorkspacefolderMulti = "automatic";

//
// Allow "Refresh session to Latest" after 3DEXPERIENCE Open
//
// CATRsc key: OpenUI.ReplaceByLatest
//
// allowed values(not case sensitive) = true/yes = Popup will be shown in case selected data is not 'Latest', proposing to refresh session to Latest
//									  = false/no = Popup will not be shown and session will not get refreshed to Latest
//
// If value is incorrect or missing : behavior is like false/no
//									  Popup will not be shown
//
OpenUI.ReplaceByLatest = "false";

//
// Allow "Refresh session to Latest" after Open From Compass
//
// CATRsc key: OpenFromCompass.ReplaceByLatest
//
// allowed values(not case sensitive) = true/yes = Popup will be shown in case selected data is not 'Latest', proposing to refresh session to Latest
//									  = false/no = Popup will not be shown and session will not get refreshed to Latest
//
// If value is incorrect or missing : behavior is like false/no
//									  Popup will not be shown
//
OpenFromCompass.ReplaceByLatest = "false";

//
// Folder column should be visible or Not on Save Dialog
//
// CATRsc key: Save.ShowFolderColumn 
//
// allowed values(not case sensitive) = true/yes = Folder column visible on R2016x and onwards
//									  = false/no = Folder column hidden
//									  = automatic = Folder column visible on R2017x and onwards
//
// If value is incorrect or missing : behavior is like automatic
//
//
// Default value : "automatic"
//
Save.ShowFolderColumn  = "automatic";

//
// if Save.ShowFolderColumn is true, behavior of "OK" button on SaveDialog will be decided by this key
// if folder is assigned to all the documents in save scope, "OK" button will be enable.
// if folder is not assigned to all the documents in save scope, "OK" button will be disable.
//
// CATRsc key: SaveIntoFolderMandatory 
//
// allowed values(not case sensitive) = true/yes = Folder should be assigned to each document in save scope
//									  = false/no = It is not mandatory to assign folder to document in save scope
//
// If value is incorrect or missing : behavior is like false/no
//									  It is not mandatory to assign folder to document in save scope
//
// Default value : false
//
SaveIntoFolderMandatory  = "false";

//
// if Save.EnforceLock is true, behavior of "OK" button on SaveDialog will be decided by this key
// if all the documents in save scope are locked, "OK" button will be enable.
// if all the documents in save scope are not locked, "OK" button will be disable.
//
// CATRsc key: Save.EnforceLock 
//
// allowed values(not case sensitive) = true/yes = each modified documents in save scope should be locked
//									  = false/no = It is not mandatory to lock all modified documents in save scope
//
// If value is incorrect or missing : behavior is like false/no
//									  It is not mandatory to lock all modified documents in save scope
//
// Default value : false
//
Save.EnforceLock  = "false";

//
// Load Menu Item should be visible or Not on
// 3DEXPERIENCE Menu Item , SpecTree
//
// CATRsc key: LoadMenuItem
//
// allowed values(not case sensitive) = true/yes = Load Menu Item visible
//									  = false/no = Load Menu Item hidden
//
// If value is incorrect or missing : behavior is like false/no
//									  Load Menu Item hidden
//
// Default value : false
//
LoadMenuItem = "false";

//
// To activate new organization of the cache i.e. to use CD5 local cache with subfolders named with a UUID
// after activating this key, 3DEXP cache (earlier known as 'ENOVIA Local workspace') will assign a subfolder to each object per revision per iteration.
//
// CATRsc Key : Cache.SubFoldersOrganization
// Default value : false
//
// allowed values(not case sensitive)   = true : Cache is organized with subfolders 
//                                      = false : Cache has one unique root folder
// value in case of error/typo/missing key: same as default                                                
//
Cache.SubFoldersOrganization = "false";

//
// This key will define if 3DEXEPERIENCE V5 Cache folder is available.
// 
// CATRsc key: NLS.3DEXPERIENCE_V5_Cache
//
// allowed values (case Insensitive) = true/yes = folder name is "3DEXEPERIENCE V5 Cache"
//									 = false/no = folder name is "ENOVIA Local Workspace" 
//
// Default Value is false.
//
NLS.3DEXPERIENCE_V5_Cache = "false";

//
// This key will check the Part Number Unicity in CATIA V5.
//
// CATRsc Key : CATIAV5.CheckPartNumberUnicity
// Default value : true/yes
//
// allows values(not case sensitive)	= true/yes : Part number checks are active
//										= false/no : Part number checks are not active
// value in case of error/typo/missing key: same as default 
//
CATIAV5.CheckPartNumberUnicity = "true";

//
// This key will modify parent if Save As New/ Create new Revision is performed on its child.
//
// CATRsc Key : Save.IncludeParentOnSaveAsNewOrCreateNewRevision
// Default value : false/no
//
// allows values(not case sensitive)	= true/yes : Parent document will be modified
//										= false/no : Parent document will not be modified
// value in case of error/typo/missing key: same as default 
Save.IncludeParentOnSaveAsNewOrCreateNewRevision = "false";

//
// This key value is considered only with 3DEXPERIENCE R2016x.  
// On 3DEXPERIENCE R2015x and below, behavior is like �false� irrespective of the CATRsc key value.
// On R2017x GA and higher, behavior is like �true� irrespective of the CATRsc key value.
//
//
// CATRsc key: OpenAndSave.SkipLastCadobjdetails 
//
// allowed values(not case sensitive) = true/yes = cadobjdetail�s request is not sent after every Save/Open
//									  = false/no = cadobjdetail�s request is sent after every Save/Open
//
// If value is incorrect or missing : behavior is like false/no
//									  cadobjdetail�s request is sent after every Save/Open
//
// Default value : false
//
OpenAndSave.SkipLastCadobjdetails = "false";

//
// This key will be used to sync user attributes with 3DEXPERIENCE during save and open.
//
// CATRsc Key : SyncAddedProperties
// Default value : false/no
//
// allows values(not case sensitive)	= true/yes : The attributes will be updated on 3DEXPERIENCE during save and
//												Modified attributes will be updated on V5 side during open.
//										= false/no : No sync of attributes with 3DEXPERIENCE.
// value in case of error/typo/missing key: same as default 
//
SyncAddedProperties = "false";

//
// ReplaceByDuplicate Menu Item should be visible or Not on
// 3DEXPERIENCE ReplaceByDuplicate Item , SpecTree
//
// CATRsc key: ReplaceByDuplicateMenuItem
//
// allowed values(not case sensitive) = true/yes : ReplaceByDuplicate Menu Item visible
//									  = false/no : ReplaceByDuplicate Menu Item hidden
//
// If value is incorrect or missing : behavior is like false/no
//									  ReplaceByDuplicate Menu Item hidden
//
// Default value : false
//
ReplaceByDuplicateMenuItem = "false";

//
// This key value is considered only with 3DEXPERIENCE R2016x.  
// On 3DEXPERIENCE R2015x and below, behavior is like �false� irrespective of the CATRsc key value.
// On R2017x GA and higher, behavior is like �true� irrespective of the CATRsc key value.
//
//
// CATRsc key: Open.ConsistentIcons
//
// allowed values(not case sensitive) = true/yes = Icon generated after partial open command for broken 
//										and unloaded link will be consistent.
//									  = false/no = Icon generated after partial open command for broken 
//										and unloaded link are not consistent.
// If value is incorrect or missing : behavior is like false/no
//									
//
// Default value : false

Open.ConsistentIcons="false";

//
// Skip the flexible components/gathering products, created by user pattern on CATIA V5, during check-in.
//
// CATRsc key: Save.SkipAssemblyUserPatternEC
//
// allowed values(not case sensitive) = true/yes = Flexible components will be skipped during check-in
//									  = false/no = Flexible components will not be skipped during check-in
// If value is incorrect or missing : behavior is like false/no
//									  Flexible components will not be skipped
//
// Default value : false
//
Save.SkipAssemblyUserPatternEC = "false";

//
// Allow specific instances selection in Partial Open panel in order to activate 
// the visualization of these instances in Partial Open result
//
// CATRsc key: OpenUI.SupportInstances
//
// allowed values(not case sensitive) = true/yes : Selecting specific instance in Partial Open panel will 
//                                                 activate the visualization of this instance in Open result
//									  = false/no : Selecting specific instance in Partial Open panel will activate 
//                                                 the visualization of all instances of corresponding reference in Open result
// If value is incorrect or missing : behavior is like false/no
//									  
//
// Default value : false
//
OpenUI.SupportInstances = "false";

//
// This key will define whether client optimization behavior is ON.
//
// CATRsc key: SaveDialog.OptimizeDisplayInformation
// Key will be honored on 16x (It will work on FD05) and above
//
// allowed values(not case sensitive) = true/yes : Performance would be optimized for display of save dialog.
//									  = false/no : There would not be any change in performance or user interface for save dialog display.
//
// If value is incorrect or missing : behavior is like false/no
//									  
//
// Default value : false
//
SaveDialog.OptimizeDisplayInformation = "false";

//
// This key will be used to activate "Recently Used" submenu in 3DEXPERIENCE menu.
//
// CATRsc Key : MRU.Visible
// Default value : false/no
//
// allows values(not case sensitive) = true/yes : Menu "Recently Used" is available in 3DEXPERIENCE menu.
//                                                         false/no : Menu "Recently Used" is NOT available.
// value in case of error/typo/missing key: same as default 
//

MRU.Visible = "false";

//
// This key will be used to define the number of elements in "Recently Used" submenu in 3DEXPERIENCE menu.
//
// CATRsc Key : MRU.MaxNbOfItems
// Default value : 5
//
// allows values(not case sensitive) = 1 to 20
// Maximum number of items is 20
//

MRU.MaxNbOfItems = "5";

//
// This key will decide about if Folder filtered content is available to user
//
// CATRsc key: FolderContent.FilteredContentSupport
//
// allowed values(not case sensitive) = true/yes : Folder filtered content is available to user.
//	                                               �Folders� section in 3DEXPERIENCE Tools/Options page is shown. 
//                                                 User can choose to show all folder content (�Filtered content� option is unchecked) 
//                                                 or only filtered content (�Filtered content� option is checked).
//	                                               Search result shows folder content of selected folder as per option for �Filtered content�
//									  = false/no : Folder filtered content is not available to user.
//	                                               �Folders� section in 3DEXPERIENCE Tools/Options page is not shown
//	                                               Search result always shows all content of the selected folder
//
// If value is incorrect or missing : behavior is like false/no
//									  
// Default value : no
//
FolderContent.FilteredContentSupport = "no";


//
// This key will decide about if Picture File Support is available to user
//
// CATRsc key: PictureFileSupport
//
// allowed values(not case sensitive) = true/yes : PictureFileSupport is available.
//	                                               
//									  = false/no : PictureFileSupport is not available.
//
// If value is incorrect or missing : behavior is like false/no
//									  
// Default value : no
//
PictureFileSupport = "no";


// Default loading mode during 3DEXPERIENCE/Open
//
// CATRsc key: Open.DefaultLoadingMode
//
// Possible values (not case sensitive) = �AllChildren� 
//										= �RequiredChildren� 
//										= �Last� 
//
// Default value: �Last�
//
//Value in case of missing key or incorrect value: �Last�
//
Open.DefaultLoadingMode = "last";
//
// This key will define whether Transaction ID mechanism would be used for better performance.
//
// CATRsc key: Open.PerfoUseTransactionID
// Key will be honored from 3DEXPERIENCE2016x onwards.
// However, it would give benefits after 3DEXPERIENCE2016xFD07.
//
// allowed values(not case sensitive) = true/yes : Performance would be optimized for open operation.
//                                    = false/no : There would not be any change in performance.
//
// If value is incorrect or missing : behavior is like false/no
//									  
//
// Default value : false
//
Open.PerfoUseTransactionID = "false";

//
// This key will be used to allow file rename on new revision in 3DEXPERIENCE �Advanced Save� dialog .
//
// CATRsc Key : AdvancedSave.NewRevision.AllowFileRename
// Default value : false/no
//
// allows values(not case sensitive)	= true/yes : Capability to rename filename on new revision during �3DEXPERIENCE/Advanced Save� is available.
//					                    = false/no : Capability to rename filename on new revision during �3DEXPERIENCE/Advanced Save� is NOT available.
// value in case of error/typo/missing key: same as default 
//
AdvancedSave.NewRevision.AllowFileRename = "false";

//
// This key will be used to allow file rename on save as new/new document in 3DEXPERIENCE �Advanced Save� dialog.
//
// CATRsc Key : AdvancedSave.New.AllowFileRename
// Default value : false/no
//
// allows values(not case sensitive)	= true/yes : Capability to rename filename during �3DEXPERIENCE/Advanced Save� is available.
//					                    = false/no : Capability to rename filename during �3DEXPERIENCE/Advanced Save� is NOT available.
// value in case of error/typo/missing key: same as default 
//
AdvancedSave.New.AllowFileRename = "false";

//
// This key will be used to load containers.
//
// CATRsc Key : LoadContainers
// Default value : List of containers except those which are already loaded by Product structure.
//                 That is : "DMUView|SPPProcessCont|CATDrwCont|CAT2DLCont|CATDrwMirrorCont|PRTCont|CATMldCont|CATXDocLinkCont|CATPrtCont|AUTLciPrdCont|DNBDptCont|CATManufacturingContainer|MfgToolPathCont|CATFeatCont|CATPlantShipExtCont|CATSpatialExtCont|CATPipingExtCont|CATTubingExtCont|CATEquipmentExtCont|CATInstrumentExtCont|CATMultiDisciplineExtCont|CATHVACExtCont"
//Usage(containers' names are case sensitive)
//LoadContainers  = �DMUView|CATDrwCont�              :2 containers will be loaded.
//LoadContainers  = � DMUView | CATDrwCont �          :Spaces will be trimmed. 2 containers will be loaded. 
//LoadContainers  = � �                               :No containers will be loaded.
//LoadContainers  = �DMUView | CATDrwCont|XYZ�        :2 containers will be loaded. Invalid container XYZ will not be loaded.
//LoadContainers  = �DMUView | CATDrwCont/CAT2DLCont� : only 1 will be loaded. Invalid separator�/�made 2 valid containers into invalid.
//
LoadContainers = "DMUView|SPPProcessCont|CATDrwCont|CAT2DLCont|CATDrwMirrorCont|PRTCont|CATMldCont|CATXDocLinkCont|CATPrtCont|AUTLciPrdCont|DNBDptCont|CATManufacturingContainer|MfgToolPathCont|CATFeatCont|CATPlantShipExtCont|CATSpatialExtCont|CATPipingExtCont|CATTubingExtCont|CATEquipmentExtCont|CATInstrumentExtCont|CATMultiDisciplineExtCont|CATHVACExtCont";

//
// The documents located in these folder will be excluded for 3DEXPERIENCE-Save. 
// All other 3DEXPERIENCE command will not be available for these documents.
// There will be exact comparison between the folders mentioned under the key and the folder from which document is opened in CATIA.
//
// CATRsc Key : Save.IgnoreFolders
// Default value : ""
//
// allows values(not case sensitive):	The path of folder. 
//					User can specify multiple path of folders.
//					Separator will be pipe (�|�).
//
// Key not defined or incorrect/blank value:
//					All the documents will be processed for Save. This functionality will not be activated.
//
// 
// Ex: For path of Folders
//					folder on Local Machine:				�C:\temp�
//					folder on Network Machine				�\\TheMachine\home\CATIA\Data�
//					DLNAME folder :							"CATDLN://DLNAME1/�
//				Mapped Network folder on local Machine:		Value must be path of mapped Drive on local machine. Ex: �Z:�, "Z:\folder1"							
// Folder on Local Machine and folder on Network Machine:	�C:\temp|\\TheMachine\home\CATIA\Data�
//
Save.IgnoreFolders = "";

// This key will be used to activate the persistency of folder selection in 3DEXPERIENCE �Advanced Save� dialog.
// 
// CATRsc Key : FolderTree.PersistSelection
// Default value : false/no
//
// allows values(not case sensitive)	= true/yes : Last folder selected in 'Folder Selection' dialog will be persisted and reused whenever possible.
//                                      = false/no : This functionality will not be activated
//
// value in case of error/typo/missing key: same as default 
//
FolderTree.PersistSelection = "false";

//
// This key will be used to activate the Post Filtering mechanism in Open dialog.
// 
// CATRsc Key : Open.PostFilter
// Default value : false/no
//
// allows values(not case sensitive)	= true/yes : A new combobox Filter is available in Open dialog
//                                      = false/no : This functionality will not be activated
//
// value in case of error/typo/missing key: same as default 
//
Open.PostFilter = "false";

//
// This key will be used to activate contextual command "Related revisions" in Open dialog
// 
// CATRsc Key : Open.RelatedRevisionsCommand
// Default value : false/no
//
// allows values(not case sensitive)	= true/yes : "Related revisions" command is proposed when right-mouse click on results of Open dialog
//                                      = false/no : This functionality will not be activated
//
// value in case of error/typo/missing key: same as default 
//
Open.RelatedRevisionsCommand = "false";
