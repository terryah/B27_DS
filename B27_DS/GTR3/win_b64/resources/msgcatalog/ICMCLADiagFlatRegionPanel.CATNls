DialogBoxTitle                                   = "Flat Region Analysis";

//Selection

Label_Elements.Title                             = "Elements: ";
Label_Elements.LongHelp                          = "Display the number of selected elements used for
the Flat Region Analysis. A context menu is available.
The selection window is available.";
Label_Elements.ShortHelp                         = "Supported elements are: Surfaces, Faces, Joins.";
Label_Discs.Title                                = "Points: ";
Label_Discs.LongHelp                             = "Selection of points for the subsequent deviation analysis";
Label_Discs.ShortHelp                            = "Selection of points";

//Tessellation frame

Frame_Tessellation.Title                         = "Tessellation";
Frame_Tessellation.LongHelp                      = "The Analysis is based on triangulated data.
That means that surfaces are converted into a
facet model before they are checked.";

CheckButton_Length.Title                         = "Facet length: ";
CheckButton_Length.LongHelp                      = "Definition of the maximum length for the facets, 
regardless of the surface curvature. 
The smaller the value for the facet length,
the more precise but slower is the computation.";
CheckButton_Length.ShortHelp                     = "Maximum facet length";

Label_Tolerance.Title                            = "Tolerance: ";
Label_Tolerance.LongHelp                         = "Displays the maximum deviation of the facet model
from the surface for a curvature-dependent tessellation.";
Label_Tolerance.ShortHelp                        = "Maximum deviation from the surface";

//Options frame

Frame_Options.Title                              = "Options";
Frame_Options.LongHelp                           = "Following options are available:
Type, Display, Length, Distance, Radius, Rotation Angle.";

//Type subframe
Frame_Type.Title                                 = "Type";
Frame_Type.LongHelp                              = "Two analysis methods are available: the ruler/disk method
(Chord) and the local method (Local).";
RadioButton_Chord.Title                          = "Chord";
RadioButton_Chord.LongHelp                       = "Ruler/Disc method (Chord).
This method yields a very precise result.";
RadioButton_Chord.ShortHelp                      = "Ruler disc method (Chord)";
RadioButton_Local.Title                          = "Local";
RadioButton_Local.LongHelp                       = "Local method (Local) is the default setting.
This method yields a quicker result than Chord.";
RadioButton_Local.ShortHelp                      = "Local method (Local)";

//Display subframe
Frame_Display.Title                              = "Display";
Frame_Display.LongHelp                           = "Display mode for the analysis result:
- Display surfaces
- Display discs only.";
RadioButton_Both.Title                           = "Both";
RadioButton_Both.LongHelp                        = "Display surfaces and discs";
RadioButton_Both.ShortHelp                       = "Display surfaces and discs";
RadioButton_DiscsOnly.Title                      = "Discs only";
RadioButton_DiscsOnly.LongHelp                   = "Display discs only";
RadioButton_DiscsOnly.ShortHelp                  = "Display discs only";


Label_Length.Title                               = "Length: ";
Label_Length.LongHelp                            = "Defintion of the Length of the virtual ruler.";

Label_Distance.Title                             = "Distance: ";
Label_Distance.LongHelp                          = "Minimum distance between ruler and geometry.";

Label_CurvatureRadius.Title                      = "Radius: ";
Label_CurvatureRadius.LongHelp                   = "Minimum curvature radius when in Local mode.";

Label_RotationAngle.Title                        = "Rotation Angle: ";
Label_RotationAngle.LongHelp                     = "Determination of type of flatness. The system checks within
the sector defined by the rotation angle whether the minimum
curvature is kept.";

Slider_RotationAngle.Title                       = "Rotation Angle: ";
Slider_RotationAngle.LongHelp                    = "Determination of type of flatness";
Slider_RotationAngle.ShortHelp                   = "Determination of type of flatness";



ManipulatorWarning.Title                         = "Warning: ";
ManipulatorWarning.Message                       = "Discs are only supported on one input-shell. Please check your input. <<<";
