//=============================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwIconBoxHeader
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// DATE        :    10.02.99
//------------------------------------------------------------------------------
// DESCRIPTION :    
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01           fgx   06.05.99  Prefix Symbol -> Insert Symbol
//	 02 	    	  lgk   26.06.02  Revision
//	 03 	    	  lgk   17.10.02  Revision2
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Anchor Point
//------------------------------------------------------------------------------
CATDrwIconBoxHeader.DrwTextAnchor.Title="Anchor Point";
CATDrwIconBoxHeader.DrwTextAnchor.ShortHelp="Anchor Point";
CATDrwIconBoxHeader.DrwTextAnchor.Help="Sets the anchor point for a piece of text";
CATDrwIconBoxHeader.DrwTextAnchor.LongHelp =
"Anchor Point
Sets the anchor point for a piece of text.";

//------------------------------------------------------------------------------
// Frame
//------------------------------------------------------------------------------
CATDrwIconBoxHeader.DrwTextFrame.Title="Frame";
CATDrwIconBoxHeader.DrwTextFrame.ShortHelp="Frame";
CATDrwIconBoxHeader.DrwTextFrame.Help="Sets a frame around a piece of text";
CATDrwIconBoxHeader.DrwTextFrame.LongHelp =
"Frame
Sets a frame around a piece of text.";

//------------------------------------------------------------------------------
// Dimension Line
//------------------------------------------------------------------------------
CATDrwIconBoxHeader.DrwDimLine.Title="Dimension Line";
CATDrwIconBoxHeader.DrwDimLine.ShortHelp="Dimension Line";
CATDrwIconBoxHeader.DrwDimLine.Help="Sets the dimension line representation";
CATDrwIconBoxHeader.DrwDimLine.LongHelp =
"Dimension Line
Sets the dimension line representation.";

//------------------------------------------------------------------------------
// Tolerance Type
//------------------------------------------------------------------------------
CATDrwIconBoxHeader.DrwDimTolType.Title="Tolerance Type";
CATDrwIconBoxHeader.DrwDimTolType.ShortHelp="Tolerance Type";
CATDrwIconBoxHeader.DrwDimTolType.Help="Sets the type of tolerance to be added to a dimension";
CATDrwIconBoxHeader.DrwDimTolType.LongHelp =
"Tolerance Type
Sets the type of tolerance to be added to a dimension.";

//------------------------------------------------------------------------------
// Justification
//------------------------------------------------------------------------------
CATDrwIconBoxHeader.CATDrwJustificationHdr.Title="Justification";
CATDrwIconBoxHeader.CATDrwJustificationHdr.ShortHelp="Justification";
CATDrwIconBoxHeader.CATDrwJustificationHdr.Help="Aligns or centers selected text";
CATDrwIconBoxHeader.CATDrwJustificationHdr.LongHelp =
"Justification
Aligns or centers selected text.";

//------------------------------------------------------------------------------
// XLine
//------------------------------------------------------------------------------
CATDrwIconBoxHeader.CATDrwXLineHdr.Title="Underline/Overline";
CATDrwIconBoxHeader.CATDrwXLineHdr.ShortHelp="Underline/Overline";
CATDrwIconBoxHeader.CATDrwXLineHdr.Help="Underlines or overlines selected text";
CATDrwIconBoxHeader.CATDrwXLineHdr.LongHelp =
"Underline/Overline
Underlines or overlines selected text.";

//------------------------------------------------------------------------------
// XScript
//------------------------------------------------------------------------------
CATDrwIconBoxHeader.CATDrwXScriptHdr.Title="Superscript/Subscript";
CATDrwIconBoxHeader.CATDrwXScriptHdr.ShortHelp="Superscript/Subscript";
CATDrwIconBoxHeader.CATDrwXScriptHdr.Help="Sets the superscript or subscript attribute of selected text";
CATDrwIconBoxHeader.CATDrwXScriptHdr.LongHelp =
"Superscript/Subscript
Sets the superscript or subscript attribute of selected text.";

