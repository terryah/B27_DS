// -----------------------------
// Analysis Entities : Property
// -----------------------------
SAM3DSolid = "3D Property";
SAMShell = "2D Property";
SAMDesignComposite = "Imported Composite Property";
SAMShellLocal = "Local 2D Property";
SAMBeam = "1D Property";
SAMBeamLocal = "Local 1D Property";
SAMImportedBeamProp = "Imported Beam Property";
SAMMappedPropertyEntity = "Mapping Property";


SAMThickness="Thickness";
SAMSurfaceOffset="Offset";
SAMRadius="Radius";
SAMRadiusExt="Outside radius";
SAMRadiusInt="Inside radius";



SAMRectangleLength="Length (Y)";
SAMRectangleHeight="Length (Z)";
SAMCustoBeam="Type";
SAMBeamOffSet="Offset";
SAMOffSetVectorStart="Start";
SAMOffSetVectorEnd="End";

SAMScalingFactorStart="Starting factor";
SAMScalingFactorEnd="Ending factor";


SAMExtRectLength="Exterior length (Y)";
SAMExtRectHeight="Exterior height (Z)";
SAMIntRectLength="Interior length (Y)";
SAMIntRectHeight="Interior height (Z)";

SAMULength="Global length (Y)";
SAMUHeight="Global height (Z)";
SAMThicknessUGlobal="Global thickness";

SAMILength="Global length (Y)";
SAMThicknessILength="Associated thickness (Y)";
SAMIHeight="Global height (Z)";
SAMThicknessIHeight="Associated thickness (Z)";

SAMTLength="Global length (Y)";
SAMThicknessTLength="Associated thickness (Y)";
SAMTHeight="Global height (Z)";
SAMThicknessTHeight="Associated thickness (Z)";

SAMXLength="Global length (Y)";
SAMThicknessXLength="Associated thickness (Y)";
SAMXHeight="Global height (Z)";
SAMThicknessXHeight="Associated thickness (Z)";

SAMCrossSectionalArea="Cross-sectional area";
SAMInertiaVector="Inertia vector";
SAMMomentOfInertiaX="Ixx";
SAMMomentOfInertiaY="Iyy";
SAMMomentOfInertiaZ="Izz";
SAMShearCenterX="Shear Center (X)";
SAMShearCenterY="Shear Center (Y)";
SAMShearRatioXY="Shear factor (XY)";
SAMShearRatioXZ="Shear factor (XZ)";

SAMVariableBeamStartEnd = "Variable Beam Definition";

SAMBarArea="Cross-sectional area";

SAMOffsetVectorStart="Start";
SAMOffsetVectorEnd="End";

SAMBindOffSet="Offset definition";

SamProxiDistance="Tolerance";

//------------------------------
// Analysis Entities : Materiau
//------------------------------

SAMIsoMaterial = "Material";
SAMThermalMaterial = "Thermal Material";
SAMNonLinearMaterial = "Non Linear Material";
SAMUserIsoMaterial = "User Isotropic Material";
SAMUserMaterial = "User Material";
SAMCompositeMaterial = "Imported Composite Material";

// Basic Component thermalMaterial
//

SAMThermalConductivity = "Thermal Conductivity";
SAMSpecificHeat = "Specific Heat";
SAMEmissivity = "Emissivity";

// Basic Component de MaterialWithNoSupport
//

SAMYoungModulus = "Young Modulus";
SAMPoissonRatio = "Poisson Ratio";
SAMDensity = "Density";
SAMThermalExpansion = "Thermal Expansion";
SAMShearModulus = "Yield Strength";

// -----------------------------
// Analysis Entities : Groups
// -----------------------------
SAMVertexGroup = "Point Group";
SAMEdgeGroup = "Line Group";
SAMSurfaceGroup = "Surface Group";
SAMBodyGroup = "Body Group";
SAMNodeGroup = "Node Group";
SAMBoxFEGroup = "Box Group";
SAMSphereFEGroup = "Sphere Group";
SAMVertexByProxi = "Point Group by Neighborhood";
SAMLineByProxi = "Line Group by Neighborhood";
SAMSurfaceByProxi = "Surface Group by Neighborhood";
SAMLineBoundaryGroup = "Line Group by Boundary";
SAMSurfBoundaryGroup = "Surface Group by Boundary";

SAMVertexGroupMP = "Point Group";
SAMEdgeGroupMP = "Line Group";
SAMSurfaceGroupMP = "Surface Group";
SAMVertexByProxiMP = "Point Group by Neighborhood";
SAMLineByProxiMP = "Line Group by Neighborhood";
SAMSurfaceByProxiMP = "Surface Group by Neighborhood";
SAMLineByProxiMP = "Line Group by Neighborhood";
SAMSurfaceByProxiMP = "Surface Group by Neighborhood";
SAMLineBoundaryGroupMP = "Line Group by Boundary";
SAMSurfBoundaryGroupMP = "Surface Group by Boundary";
// -----------------------------
// Analysis Entities : Restraint
// -----------------------------
SAMClamp = "Clamp";
SAMPivot = "Pivot";
SAMSlidingPivot = "Sliding Pivot";
SAMSlider = "Slider";
SAMSurfaceSlider = "Surface Slider";
SAMBallJoin = "Ball Joint";
SAMRestraint = "User-defined Restraint";
SAMRestIsostatic = "Isostatic";

SAMSliderDirection="Released Direction";
SAMSlidingPivotDirection="Released Direction";
SAMPivotDirection="Released Direction";

// -------------------------
// Analysis Entities : Loads
// -------------------------
SAMPressureMag = "Pressure";
SAMTemperatureField = "Temperature Field";
SAMTempFromThermSolutionEntity = "Temperature Field from Thermal Solution";
SAMTempFromThermSolutionBC = "Thermal Solution";
SAMPressure = "Pressure";
SAMForceVector = "Force Vector";
SAMDistributedForce = "Distributed Force";
SAMLineicForce = "Line Force Density";
SAMSurfacicForce = "Surface Force Density";
SAMVolumicForce = "Volume Force Density";
SAMForceDensity = "Force Density";
SAMMoment = "Moment";
SAMEnforcedDisp = "Enforced Displacement";

SAMTransAcceleration = "Acceleration";
SAMRotationForce = "Rotation Force";

SAMBearingLoad="Bearing Load";

SAMImportedForce="Imported Force";
SAMImportedMoment="Imported Moment";

// pour le type enumere SAMBearOrientation
radial="Radial";
parallel="Parallel";

// pour le type enumere SAMBearProfileType
sinusoidal="Sinusoidal";
parabolic="Parabolic";
userLaw="Law";

inward = "Inward";
outward = "Outward";

SAMMomentVector="Moment Vector";
SAMTransAccelVector="Acceleration Vector";
SAMBearAngle="Angle";
SAMBearOrientation="Orientation";
SAMBearProfile="Profile";
SAMBearProfileType="Type";
SAMDistribInwardOutward="Distributiion";

SAMAngularVelocity="Angular Velocity";
SAMRotAcceleration="Angular Acceleration";

SAMLineicForceVector="Force Vector";
SAMSurfacicForceVector="Force Vector";
SAMVolumicForceVector="Force Vector";

SAMEnfDispTransX="Translation 1";
SAMEnfDispTransY="Translation 2";
SAMEnfDispTransZ="Translation 3";

SAMEnfDispRotX="Rotation 1";
SAMEnfDispRotY="Rotation 2";
SAMEnfDispRotZ="Rotation 3";

SAMTemperatureMag="Temperature";


SAMMassMag="Mass";
SAMLineicMassMag="Line Mass Density";
SAMSurfacicMassMag="Surface Mass density";
SAMInertiaMassMass="Mass";
SAMDistributedInertiaBC="Inertia Tensor";

SAMInertiaIxx="I11";
SAMInertiaIyy="I22";
SAMInertiaIzz="I33";
SAMInertiaIxy="I12";
SAMInertiaIxz="I13";
SAMInertiaIyz="I23";

// ------------------------------
// Analysis Entities : Equipments
// ------------------------------
SAMDistributedMass = "Distributed Mass";
SAMLineicMass = "Line Mass Density";
SAMSurfacicMass = "Surface Mass Density";
SAMInertiaMass = "Distributed Mass and Inertia";
SAMCombineMass = "Combined Mass";
SAMCombineLoad = "Combined Load";

// ---------------------------------------
// Analysis Entities : Assembly Constraint
// ---------------------------------------
SAMFaceFaceFastenedSpring = "Fastened Spring Connection Property";
SAMFaceFaceFastened = "Fastened Connection Property";
SAMFaceFaceSlider = "Slider Connection Property";
SAMFaceFaceContact = "Contact Connection Property";		
SAMDistantRigid	= "Rigid Connection Property";
SAMDistantSmooth = "Smooth Connection Property";	
SAMPressureFitting="Pressure Fitting Connection Property";
SAMBoltTightening="Bolt Tightening Connection Property";
SAMVirtBoltTightening="Virtual Bolt Tightening Connection Property";
SAMVirtSpringBoltTigh="Virtual Spring Bolt Tightening Connection Property";
SAMSpotWeldingConnect="Spot Welding Connection Property";
SAMSeamWeldConnect="Seam Welding Connection Property";
SAMSurfWeldConnect="Surface Welding Connection Property";
SAMHalfPointPropConnect="Node Interface Property";
SAMPointPointPropConnect="Nodes to Nodes Connection Property";
SAMPeriodicConnection="Periodicity Condition Property";
SAMGenericDistant="User-defined Connection Property";

// pour le type enumere SAMBoltTighOrientation
same="Same";
opposite="Opposite";

// --------------------------------
// Analysis Entities : Virtual Part
// --------------------------------
SAMVirPartContact = "Contact Virtual Part";
SAMVirPartRigid = "Rigid Virtual Part";
SAMVirPartSmooth = "Smooth Virtual Part";
SAMVirPartRigidSpring = "Rigid Spring Virtual Part";
SAMVirPartSmoothSpring = "Smooth Spring Virtual Part";

// BasicComponents
// --------------
SAMDTPtrPressure = "Pressure Table";
SAMDTPtrSurfForce = "Surfacic Force Table";
SAMDTPtrLinForce = "Lineic Force Table";
SAMDTPtrVolForce = "Volumic Force Table";
SAMDTPtrShell = "Thickness Table";
SAMDTPtrImportedForceDesignTable.1 = "Imported Force Table";
SAMDTPtrImportedMomentDesignTable.1 = "Imported Moment Table";

// pour le type enumere GPSCustoWeld
Rigid="Rigid";
RigidSpringRigid="Rigid-Spring-Rigid";
SpringRigidSpring="Spring-Rigid-Spring";
Beam="Beam";
Hexahedron="Hexahedron";

GPSCustoSpotWeld="Spot Weld";

SAMGlobalSensor = "Global Sensor";
SAMImageSensor = "Local Sensor";
SAMLoadPreproSensor="Applied Load Sensor";
SAMInternalLoadPreproSensor="Internal Load Sensor";
SAMInternalLoadPreproSensorSingleLoad="Internal Load Sensor";
SAMMultiLoadPreproSensor="Multi Load Sensor";
SAMMassPreproSensor="Inertia Sensor";
SAMReactionSensor = "Reaction Sensor";

// enumere pour les sensors
GPSProcessNone = "None";
GPSProcessMin = "Minimum";
GPSProcessMax = "Maximum";
GPSProcessAverage = "Average";

// Adaptatif
SAMAdaptivitySet = "Adaptivities";
SAMAdaptivityEntity = "Global Adaptivity";
SAMLocalAdaptEntity = "Local Adaptivity";

//Connection Design
SAMGenericConnDesign = "General Analysis Connection";
SAMPointConnDesign = "Point Analysis Connection";
SAMPointConnDesignWOP = "Point Analysis Connection within one Part";
SAMLineConnDesign = "Line Analysis Connection";
SAMLineConnDesignWOP = "Line Analysis Connection within one Part";
SAMSurfConnDesign = "Surface Analysis Connection";
SAMSurfConnDesignWOP = "Surface Analysis Connection within one Part";
SAMPointHalfConnDesign = "Point Analysis Interface";
SAMPointPointConnDesign = "Points to Points Analysis Connection";

// Multi LoadSet
AnalysisMultiLoadSet = "Multi Loads";
//
SAMImportForceUIEntity = "Import Forces";

// Inertia Sensor (manage all output parameters NLS)
ParamMass = "Mass";
ParamInertiaXX = "Ixx";
ParamInertiaYY= "Iyy";
ParamInertiaZZ= "Izz";
ParamInertiaXY= "Iyz";
ParamInertiaXZ= "Ixz";
ParamInertiaYZ= "Ixy";
ParamCouplingXX= "Cxy";
ParamCouplingYY= "Cxz";
ParamCouplingZZ= "Cyx";
ParamCouplingXY= "Cyz";
ParamCouplingXZ= "Czx";
ParamCouplingYZ= "Czy";
ParamCDGx= "Gx";
ParamCDGy= "Gy";
ParamCDGz= "Gz";

//Imported loads
SAMImportForceEntity="Imported Load";
SAMNewImportForceEntity="Imported Load";






