Title = "Signature Calibration Parameters";
Help = "Signature Calibration Parameters";
ShortHelp = "Signature Calibration Parameters";
LongHelp = "Signature Calibration Parameters";

TabContainer.MeasurementInfo.Title = "Measurement Information";
TabContainer.MeasurementInfo.Help = "Enter Measurement Information";
TabContainer.MeasurementInfo.ShortHelp = "Enter Measurement Information";
TabContainer.MeasurementInfo.LongHelp = "Enter Measurement Information for JointOffset Calibration";

TabContainer.BaseAndToolFit.Title = "BaseFit And ToolFit Info";
TabContainer.BaseAndToolFit.Help = "BaseFit And ToolFit Info";
TabContainer.BaseAndToolFit.ShortHelp = "BaseFit And ToolFit Info";
TabContainer.BaseAndToolFit.LongHelp = "BaseFit And ToolFit Info for JointOffet Calibration";

TabContainer.JointOffsets.Title = "Device Joint Offsets";
TabContainer.JointOffsets.Help = "Enter Device Joint Offsets";
TabContainer.JointOffsets.ShortHelp = "Enter Device Joint Offsets";
TabContainer.JointOffsets.LongHelp = "Enter Device Joint Offsets";

TabContainer.DeltaVectors.Title = "Chain Signature";
TabContainer.DeltaVectors.Help = "Enter Delta Vectors";
TabContainer.DeltaVectors.ShortHelp = "Enter Delta Vectors";
TabContainer.DeltaVectors.LongHelp = "Enter Delta Vectors";

TabContainer.MeasurementInfo.TagOrientation.Title = "Tag Orientation";
TabContainer.MeasurementInfo.TagOrientation.Help = "Tag Orientation";
TabContainer.MeasurementInfo.TagOrientation.ShortHelp = "Tag Orientation";
TabContainer.MeasurementInfo.TagOrientation.LongHelp = "Specify if Tag Orientation has to be accounted in JointOffset Calibration";

TabContainer.MeasurementInfo.TagOrientation.SetTagOrientation.Title = "Tag Orientation";
TabContainer.MeasurementInfo.TagOrientation.SetTagOrientation.Help = "Tag Orientation";
TabContainer.MeasurementInfo.TagOrientation.SetTagOrientation.ShortHelp = "Tag Orientation";
TabContainer.MeasurementInfo.TagOrientation.SetTagOrientation.LongHelp = "Specify if Tag Orientation has to be accounted in JointOffset Calibration";

TabContainer.MeasurementInfo.SetNoise.Title = "Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.Help = "Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.ShortHelp = "Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.LongHelp = "Specify the Measurement Noise(Position & Orientation) for JointOffset Calibration";

TabContainer.MeasurementInfo.SetNoise.PositionNoise.Title = "Position Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.PositionNoise.Help = "Position Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.PositionNoise.ShortHelp = "Position Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.PositionNoise.LongHelp = "Specify the Measurement Noise(Position) for JointOffset Calibration. Min values is 0.1mm";

TabContainer.MeasurementInfo.SetNoise.OrientationNoise.Title = "Orientation Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.OrientationNoise.Help = "Orientation Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.OrientationNoise.ShortHelp = "Orientation Measurement Noise";
TabContainer.MeasurementInfo.SetNoise.OrientationNoise.LongHelp = "Specify the Measurement Noise(Orientation) for JointOffset Calibration. Min values is 0.1deg";

TabContainer.BaseAndToolFit.BaseFit.Title = "Base Fit";
TabContainer.BaseAndToolFit.BaseFit.Help = "Base Fit";
TabContainer.BaseAndToolFit.BaseFit.ShortHelp = "Base it";
TabContainer.BaseAndToolFit.BaseFit.LongHelp = "Base Fit Info for JointOffet Calibration";

TabContainer.BaseAndToolFit.BaseFit.BaseTransX.Title = "Base Translate X";
TabContainer.BaseAndToolFit.BaseFit.BaseTransX.Help = "Base Translate X";
TabContainer.BaseAndToolFit.BaseFit.BaseTransX.ShortHelp = "Base Translate X";
TabContainer.BaseAndToolFit.BaseFit.BaseTransX.LongHelp = "Base Translate X";

TabContainer.BaseAndToolFit.BaseFit.BaseTransY.Title = "Base Translate Y";
TabContainer.BaseAndToolFit.BaseFit.BaseTransY.Help = "Base Translate Y";
TabContainer.BaseAndToolFit.BaseFit.BaseTransY.ShortHelp = "Base Translate Y";
TabContainer.BaseAndToolFit.BaseFit.BaseTransY.LongHelp = "Base Translate Y";

TabContainer.BaseAndToolFit.BaseFit.BaseTransZ.Title = "Base Translate Z";
TabContainer.BaseAndToolFit.BaseFit.BaseTransZ.Help = "Base Translate Z";
TabContainer.BaseAndToolFit.BaseFit.BaseTransZ.ShortHelp = "Base Translate Z";
TabContainer.BaseAndToolFit.BaseFit.BaseTransZ.LongHelp = "Base Translate Z";

TabContainer.BaseAndToolFit.BaseFit.BaseRotX.Title = "Base Rotate X";
TabContainer.BaseAndToolFit.BaseFit.BaseRotX.Help = "Base Rotate X";
TabContainer.BaseAndToolFit.BaseFit.BaseRotX.ShortHelp = "Base Rotate X";
TabContainer.BaseAndToolFit.BaseFit.BaseRotX.LongHelp = "Base Rotate X";

TabContainer.BaseAndToolFit.BaseFit.BaseRotY.Title = "Base Rotate Y";
TabContainer.BaseAndToolFit.BaseFit.BaseRotY.Help = "Base Rotate Y";
TabContainer.BaseAndToolFit.BaseFit.BaseRotY.ShortHelp = "Base Rotate Y";
TabContainer.BaseAndToolFit.BaseFit.BaseRotY.LongHelp = "Base Rotate Y";

TabContainer.BaseAndToolFit.BaseFit.BaseRotZ.Title = "Base Rotate Z";
TabContainer.BaseAndToolFit.BaseFit.BaseRotZ.Help = "Base Rotate Z";
TabContainer.BaseAndToolFit.BaseFit.BaseRotZ.ShortHelp = "Base Rotate Z";
TabContainer.BaseAndToolFit.BaseFit.BaseRotZ.LongHelp = "Base Rotate Z";

TabContainer.BaseAndToolFit.ToolFit.Title = "Tool Fit";
TabContainer.BaseAndToolFit.ToolFit.Help = "Tool Fit";
TabContainer.BaseAndToolFit.ToolFit.ShortHelp = "Tool Fit";
TabContainer.BaseAndToolFit.ToolFit.LongHelp = "Tool Fit Info for JointOffet Calibration";

TabContainer.BaseAndToolFit.ToolFit.ToolTransX.Title = "Tool Translate X";
TabContainer.BaseAndToolFit.ToolFit.ToolTransX.Help = "Tool Translate X";
TabContainer.BaseAndToolFit.ToolFit.ToolTransX.ShortHelp = "Tool Translate X";
TabContainer.BaseAndToolFit.ToolFit.ToolTransX.LongHelp = "Tool Translate X";

TabContainer.BaseAndToolFit.ToolFit.ToolTransY.Title = "Tool Translate Y";
TabContainer.BaseAndToolFit.ToolFit.ToolTransY.Help = "Tool Translate Y";
TabContainer.BaseAndToolFit.ToolFit.ToolTransY.ShortHelp = "Tool Translate Y";
TabContainer.BaseAndToolFit.ToolFit.ToolTransY.LongHelp = "Tool Translate Y";

TabContainer.BaseAndToolFit.ToolFit.ToolTransZ.Title = "Tool Translate Z";
TabContainer.BaseAndToolFit.ToolFit.ToolTransZ.Help = "Tool Translate Z";
TabContainer.BaseAndToolFit.ToolFit.ToolTransZ.ShortHelp = "Tool Translate Z";
TabContainer.BaseAndToolFit.ToolFit.ToolTransZ.LongHelp = "Tool Translate Z";

TabContainer.BaseAndToolFit.ToolFit.ToolRotX.Title = "Tool Yaw";
TabContainer.BaseAndToolFit.ToolFit.ToolRotX.Help = "Tool Yaw";
TabContainer.BaseAndToolFit.ToolFit.ToolRotX.ShortHelp = "Tool Yaw";
TabContainer.BaseAndToolFit.ToolFit.ToolRotX.LongHelp = "Tool Yaw";

TabContainer.BaseAndToolFit.ToolFit.ToolRotY.Title = "Tool Pitch";
TabContainer.BaseAndToolFit.ToolFit.ToolRotY.Help = "Tool Pitch";
TabContainer.BaseAndToolFit.ToolFit.ToolRotY.ShortHelp = "Tool Pitch";
TabContainer.BaseAndToolFit.ToolFit.ToolRotY.LongHelp = "Tool Pitch";

TabContainer.BaseAndToolFit.ToolFit.ToolRotZ.Title = "Tool Roll";
TabContainer.BaseAndToolFit.ToolFit.ToolRotZ.Help = "Tool Roll";
TabContainer.BaseAndToolFit.ToolFit.ToolRotZ.ShortHelp = "Tool Roll";
TabContainer.BaseAndToolFit.ToolFit.ToolRotZ.LongHelp = "Tool Roll";
