// COPYRIGHT Dassault Systemes 2008
//===================================================================

BlankLineLabel030Id.Title = "  ";
BlankLineLabel031Id.Title = "  ";
BlankLineLabel032Id.Title = "  ";
DraftBehaviorFrameId.DraftBehaviorLabelId.Title = "Draft behavior:  ";

DraftFirstLimitFrameId.DraftFirstLimitLabelId.Title = "Angle:                ";
DraftFirstLimitFrameId.DraftFirstLimitReverseAnglePushButtonId.Title = "Reverse";

DraftClearanceFrameId.DraftClearanceLabelId.Title = "Clearance volume angle:";
DraftClearanceFrameId.DraftClearanceReverseAnglePushButtonId.Title = "Reverse";

DraftApplyFromFrameId.Title = "Neutral element: ";
DraftApplyFromFrameId.DraftApplyFromSelectionFrameId.DraftApplyFromSelectionLabelId.Title = "Selection:  ";
DraftApplyFromLabelId.Title = "Neutral element:";
FacesToDraftFrameId.Title = "Faces to draft";
 
DraftMiscOptionsFrameId.DraftBothSidesCheckButtonId.Title = "Draft both sides";
DraftMiscOptionsFrameId.DraftProfileEndsCheckButtonId.Title = "Draft profile ends";
DraftMiscOptionsFrameId.DraftFilletSurfacesCheckButtonId.Title = "Draft fillets";
DraftMiscOptionsFrameId.PartingEqualNeutralCheckButtonId.Title = "Parting = Neutral";

DraftBehaviorNoneId = "None";
DraftBehaviorIntrinsicId = "Intrinsic to feature";

DraftApplyFromProfilePlaneId     = "Profile plane";
DraftApplyFromRibsTopSurfId      = "Ribs top surface";
DraftApplyFromRibsBottomSurfId   = "Ribs bottom surface";
DraftApplyFromSpikesTopSurfId    = "Spikes top surface";
DraftApplyFromSpikesBottomSurfId = "Spikes bottom surface";
DraftApplyFromPartingElementId = "Use parting element";

FaceToDraftAllLateralId = "All lateral faces";
FaceToDraftByPullDirId  = "Selected by pull direction";
FaceToDraftSelectId     = "Select profile curves";

NoSelectionId = "No selection";

FacesToDraftCurvesSelectionLabelId = "Curves:     ";

NeutralElement3DLabelId = "Neutral Element";

//===================================================================
// LongHelp
//===================================================================

DraftBehaviorFrameId.LongHelp = 
"Specifies whether feature will be drafted
and if so where draft parameters are defined.";

DraftFirstLimitFrameId.DraftFirstLimitLabelId.LongHelp =
"Specifies the draft angle value. The value
must be between -85 and 85 degrees (inclusive).";

DraftFirstLimitFrameId.DraftFirstLimitReverseAnglePushButtonId.LongHelp =
"Reverses the draft angle indicated by the blue cone.";

DraftClearanceFrameId.DraftClearanceLabelId.LongHelp = 
"Specifies clearance draft angle value. The value
must be between -85 and 85 degrees (inclusive).";

DraftClearanceFrameId.DraftClearanceReverseAnglePushButtonId.LongHelp = 
"Reverses the clearance draft angle indicated by the red cone.";

DraftApplyFromFrameId.LongHelp =
"Neutral element parameters.";

DraftApplyFromFrameId.DraftApplyFromComboId.LongHelp =
"Specifies the location of the draft
neutral element.";

DraftApplyFromFrameId.DraftApplyFromSelectionFrameId.LongHelp =
"Specifies the neutral element plane or surface.";

FacesToDraftFrameId.LongHelp =
"Faces to draft parameters.";

FacesToDraftFrameId.FacesToDraftComboId.LongHelp =
"Specifies the method used for determining
which faces of the feature to draft.
\"All lateral faces\" indicates all faces
except limit capping faces. 
\"Selected by pull direction\" indicates all
vertical faces relative to pull direction.";

FacesToDraftFrameId.FacesToDraftSelectionFrameId.LongHelp =
"Specifies the faces to draft by selecting
profile curves. Lateral faces originating
from the selected curves will be drafted.";

FacesToDraftFrameId.FacesToDraftOptionsFrameId.DraftFirstLimitFaceCheckButtonId.LongHelp =
"Indicates that the capping face at the
limit will be drafted.";

FacesToDraftFrameId.FacesToDraftOptionsFrameId.DraftSecondLimitFaceCheckButtonId.LongHelp =
"Indicates that the capping face at the
limit will be drafted.";

DraftMiscOptionsFrameId.PartingEqualNeutralCheckButtonId.LongHelp =
"Indicates that the parting element is equal
to the neutral element. Availability dependent
upon draft behavior and neutral element style.";

DraftMiscOptionsFrameId.DraftBothSidesCheckButtonId.LongHelp =
"Indicates that the faces will be drafted
in two directions from the parting element.
Only available if \"Parting = Neutral\" 
switch is ON.";

DraftMiscOptionsFrameId.DraftProfileEndsCheckButtonId.LongHelp =
"Indicates that the ends of open profiles
are to be drafted.";

DraftMiscOptionsFrameId.DraftFilletSurfacesCheckButtonId.LongHelp =
"Indicates that the draft will be applied
after the fillet surfaces are created.";
