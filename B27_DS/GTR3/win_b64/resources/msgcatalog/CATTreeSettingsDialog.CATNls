Notify.Title="Warning";
Warning1="Restart session to take modifications into account.";
Warning2="The modification will be taken into consideration 
the next time a document will be opened.";

// -----------------------------------
// Nouvelle interface utilisateur - R5
// -----------------------------------

TypeLabelFr.HeaderFrame.Global.Title="Tree Type";
TypeLabelFr.HeaderFrame.Global.ShortHelp="Defines the type of tree representation";
TypeLabelFr.HeaderFrame.Global.LongHelp="Defines the type of tree representation";

TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeClassic.Title="Classical Windows Style";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeClassic.ShortHelp="Sets the tree type to the classical Windows style";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeClassic.LongHelp="Sets the tree type to the classical Windows style";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeStructure.Title="Structure";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeStructure.ShortHelp="Sets the tree type to a structure-oriented representation";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeStructure.LongHelp="Sets the tree type to a structure-oriented representation";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeHistorical.Title="Constructive Historic";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeHistorical.ShortHelp="Sets the tree type to a constructive historic representation";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeHistorical.LongHelp="Sets the tree type to a constructive historic representation";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeRelational.Title="Relational";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeRelational.ShortHelp="Sets the tree type to a relation-oriented representation";
TypeLabelFr.IconAndOptionsFrame.OptionsFrame.TypeRelational.LongHelp="Sets the tree type to a relation-oriented representation";



OrientLabelFr.HeaderFrame.Global.Title="Tree Orientation";
OrientLabelFr.HeaderFrame.Global.ShortHelp="Defines the tree orientation";
OrientLabelFr.HeaderFrame.Global.LongHelp="Defines the tree orientation";

OrientLabelFr.IconAndOptionsFrame.OptionsFrame.OrientVertical.Title="Vertical";
OrientLabelFr.IconAndOptionsFrame.OptionsFrame.OrientVertical.ShortHelp="Sets the tree orientation to vertical";
OrientLabelFr.IconAndOptionsFrame.OptionsFrame.OrientVertical.LongHelp="Sets the tree orientation to vertical";
OrientLabelFr.IconAndOptionsFrame.OptionsFrame.OrientHorizontal.Title="Horizontal";
OrientLabelFr.IconAndOptionsFrame.OptionsFrame.OrientHorizontal.ShortHelp="Sets the tree orientation to horizontal";
OrientLabelFr.IconAndOptionsFrame.OptionsFrame.OrientHorizontal.LongHelp="Sets the tree orientation to horizontal";



SizeLabelFr.HeaderFrame.Global.Title="Tree Item Size";
SizeLabelFr.HeaderFrame.Global.ShortHelp="Defines the number of characters used for the names of the tree items";
SizeLabelFr.HeaderFrame.Global.LongHelp="Defines the number of characters used for the names of the tree items";

SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeDependant.Title="Text-dependent";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeDependant.ShortHelp="The whole of each item name is shown";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeDependant.LongHelp="The whole of each item name is shown";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeFixed.Title="Fixed:";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeFixed.ShortHelp="Defines a fixed number of characters for the names of the tree items";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeFixed.LongHelp="Defines a fixed number of characters for the names of the tree items";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeNumberLabel.Title=" characters";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeNumberLabel.ShortHelp="Defines a fixed number of characters for the names of the tree items";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeNumberLabel.LongHelp="Defines a fixed number of characters for the names of the tree items";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeNumber.ShortHelp="Defines a fixed number of characters for the names of the tree items";
SizeLabelFr.IconAndOptionsFrame.OptionsFrame.SizeNumber.LongHelp="Defines a fixed number of characters for the names of the tree items";



ShowLabelFr.HeaderFrame.Global.Title="Tree Show/NoShow";
ShowLabelFr.HeaderFrame.Global.ShortHelp="Activates the visualization of Show/NoShow's mode on the tree";
ShowLabelFr.HeaderFrame.Global.LongHelp="Activates the visualization of Show/NoShow's mode on the tree";

ShowLabelFr.IconAndOptionsFrame.OptionsFrame.ShowActivated.Title="Activation of the tree Show/NoShow mode";
ShowLabelFr.IconAndOptionsFrame.OptionsFrame.ShowActivated.ShortHelp="Activates the visualization of Show/NoShow's mode";
ShowLabelFr.IconAndOptionsFrame.OptionsFrame.ShowActivated.LongHelp="Activates the visualization of Show/NoShow's mode";



AutoScrollLabelFr.HeaderFrame.Global.Title="Automatic Scroll";
AutoScrollLabelFr.HeaderFrame.Global.ShortHelp="Activates automatic scroll mode of the tree during a Drag&Drop operation.";
AutoScrollLabelFr.HeaderFrame.Global.LongHelp="Activates the automatic scroll mode of the specification tree when a Drag&Drop operation is performed.";

AutoScrollLabelFr.IconAndOptionsFrame.OptionsFrame.AutoScrollActivated.Title="Automatic scroll activation during Drag&Drop";
AutoScrollLabelFr.IconAndOptionsFrame.OptionsFrame.AutoScrollActivated.ShortHelp="Activates automatic scroll mode of the tree during a Drag&Drop operation.";
AutoScrollLabelFr.IconAndOptionsFrame.OptionsFrame.AutoScrollActivated.LongHelp="Activates the automatic scroll mode of the specification tree when a Drag&Drop operation is performed.";



AutoExpandTOFrame.HeaderFrame.Global.Title="Automatic Expand";
AutoExpandTOFrame.HeaderFrame.Global.ShortHelp="Activates automatic expansion of the tree.";
AutoExpandTOFrame.HeaderFrame.Global.LongHelp="Activates the automatic expansion of the specification tree.";

AutoExpandTOFrame.IconAndOptionsFrame.OptionsFrame.AutoExpandCheckButton.Title="Automatic expand activation";
AutoExpandTOFrame.IconAndOptionsFrame.OptionsFrame.AutoExpandCheckButton.ShortHelp="Activates automatic expansion of the tree.";
AutoExpandTOFrame.IconAndOptionsFrame.OptionsFrame.AutoExpandCheckButton.LongHelp="Activates the automatic expansion of the specification tree.";



ArcSelectionTOFrame.HeaderFrame.Global.Title="Zoom on Tree";
ArcSelectionTOFrame.HeaderFrame.Global.ShortHelp="When activated, the tree can be zoomed in or out after clicking on any branch.";
ArcSelectionTOFrame.HeaderFrame.Global.LongHelp="When activated, the specification tree can be zoomed in or out after clicking on any branch. \nOtherwise, it can still be zoomed but only through a click on the bottom right axis system.";

ArcSelectionTOFrame.IconAndOptionsFrame.OptionsFrame.ArcSelectionActivated.Title="Tree can be zoomed after clicking on any branch";
ArcSelectionTOFrame.IconAndOptionsFrame.OptionsFrame.ArcSelectionActivated.ShortHelp="When activated, the tree can be zoomed in or out after clicking on any branch.";
ArcSelectionTOFrame.IconAndOptionsFrame.OptionsFrame.ArcSelectionActivated.LongHelp="When activated, the specification tree can be zoomed in or out after clicking on any branch. \nOtherwise, it can still be zoomed but only through a click on the bottom right axis system.";

