// English Translation of the CATIA V5 Automotive BiW Fastening Application Attributes Names and Settings
// *******************************************************************************
//
//
// SECTION A: CATIA V5 BiW Fastener Application General Attributes
// *******************************************************************************
// 
// Attributes Names
// -------------------------------------------------------------------------------
PCATS             = "Process Category";
PTYPS             = "Process Type"; 
//
// SECTION B:  CATIA V5 BiW Joint and BiW Joint Body Attributes 
// *******************************************************************************
// 
// Attributes Names
// -------------------------------------------------------------------------------
FELS  = "Forecast Elements Count"; 
//
// Assembly Type Attributes Names
// -------------------------------------------------------------------------------
NTH  = "Thicknesses Count";
STY  = "Stacking";
ZNAM = "Zone ID";
ZSUP = "Support";
ZHEM = "Hem";
ZMATNAM = "Material";
ZTH  = "Thickness";
ZORN = "Orientation";
ZCO1 = "Coating1";
ZCO2 = "Coating2";
CO1TH = "Thickness1";
CO2TH = "Thickness2";
//
// Edit/Search criteria
// -------------------------------------------------------------------------------
JPN   = "Joined Component Name"; 
JCN   = "Joined Components Number"; 
JID   = "BiW Joint Name"; 
JBID  = "BiW JointBody Name"; 
SDF_ES  = "Shape Definition ";

//
// SECTION C: CATIA V5 BiW Fastener Attributes
// *******************************************************************************
//
// SECTION C-1- CATIA V5 BiW Fastener Life Cycle Attributes 
// ***********  ===================================================================
// 
// Attributes Names
// -------------------------------------------------------------------------------
JEID           = "ID";
STD            = "Standard";
U              = "Unspecified";
//
//
// SECTION C-2- CATIA V5 BiW Fastener Type  Attributes 
// ***********  ====================================================================
//
// Attributes Names
// -------------------------------------------------------------------------------
PCA  = "Process Category";
PTY  = "Process Type";

TC1  = "Projection Component";
TPN1 = "Projection Part Number";
TZ1  = "Projection Zone"; 

//
//  Process Category Attribute Code Values 
// -------------------------------------------------------------------------------
PCA_WLD  = "Welding";
PCA_ADH  = "Adhesive";
PCA_SEA  = "Sealant";
PCA_BWM  = "BiW Mechanical";
PCA_U = "Unspecified";
PTY_U = "Unspecified";
//
//
// Process Type Attribute Code Values 
// -------------------------------------------------------------------------------
// For Welding (ISO Code Values):
// ------------------------------
PTY_13   = "MIG (13)";
PTY_131  = "MIG (131)";
PTY_135  = "MAG (135)";
PTY_14   = "TIG (14)";
PTY_141  = "TIG (141)";
PTY_21   = "Resistance (21)";
PTY_22   = "Roll Seam (22)";
PTY_221  = "Lap Seam Welding (221)";
PTY_222  = "Mash Seam Welding (222)";
PTY_23   = "Projection Resistance (23)";   
PTY_24   = "Flash Butt (24)";
PTY_25   = "Resistance Butt (25)";
PTY_41   = "Ultrasonic Welding (41)";
PTY_42   = "Friction Welding (42)";
PTY_52   = "Laser (52)";
PTY_78   = "Stud Welding (78)";
PTY_783  = "Arc Stud Welding (783)";
PTY_784  = "Arc Stud Welding (784)";
PTY_84   = "Laser beam cutting (84)";
PTY_91   = "Brazing (91)";
PTY_93   = "Brazing (93)";
PTY_94   = "Soldering (94)";
//
PTY_UNSW = "Unspecified Welding";
PTY_UPRJ = "Unspecified Projection";
//
// For Welding (ANSI Code Values):
// ------------------------------
PTY_SMAWNC = "SMAWC";
PTY_TIG    = "TIG";
PTY_RSW    = "Resistance";
PTY_L      = "Laser";

//  For Adhesive:
// --------------
PTY_STR   = "Structural Adhesive";
PTY_NSTR  = "Non Structural Adhesive";
PTY_UNSA  = "Unspecified Adhesive";

//  For Sealant:
// -------------
PTY_ROS   = "Robot Sealed";
PTY_MAS   = "Sealed By Hand";
PTY_UNSS  = "Unspecified Sealant";

// For BiW Mechanical:
// -------------------
PTY_CLI1  = "Clinching Point 1";
PTY_CLI2  = "Clinching Point 2";
PTY_R     = "Rivet";
PTY_PR    = "Punch Rivet";
PTY_BR    = "Blind Rivet";
PTY_UNSM  = "Unspecified BiW Mechanical";
//
//
// For Unspecified Process Type:
// -----------------------------
PTY_U1 = "Unspecified 1";
PTY_U2 = "Unspecified 2";
//
//
// Assembly Type Code Values
// -------------------------------------------------------------------------------
STY_LAP  = "Lap Joint";
STY_HEM  = "Hemmed Joint";
STY_UNSP = "Unspecified";
//
// SECTION C-3- CATIA V5 BiW Fastener Function  Attributes 
// ***********  ====================================================================
//
// Design & Engineering Attributes Names
// -------------------------------------------------------------------------------
//
ROB = "Weld Class";
REG = "Joint Class";
FIN = "Finish Class";
//
// Manufacturing Attributes Names
// -------------------------------------------------------------------------------
GFL  = "Geometry Flag";
IFL  = "Inspection Flag";
MID  = "Manufacturing Code";
DISM = "Discretization Method";
DISV = "Discretization Parameter";
//
// Design & Engineering  Function Attributes Code Values 
// -------------------------------------------------------------------------------
// For an attribute XXX, each NLS code value MUST BE BUILT BY CONCATENATION (UNDERSCORE SEPARATOR)
// of the ATTRIBUTE NAME CODE and the Matching  User Code values as specified  in the GbfDSStd.xls File.
// For example , the "Engineering Type" Attribute Code Name is "ENG", its possible values are:A,B,C,U as
// specified  in the CATIA Default GbfDefaultISOStd.xls File.
// By concatenation of "ENG" and "A" with _ separator we get The NLS Code string for value "A" is  "ENG_A"
//   
//
//
// ROB (Robustness) attribute Values
// ----------------------------------------
ROB_A = "Ordinary";
ROB_B = "Critical";
ROB_C = "Safety";
ROB_D = "Manufacturing";
ROB_E = "Common";
ROB_F = "Delta";
ROB_G = "Control";
ROB_H = "Non-Structural";
ROB_U = "Unspecified";
//
// REG (Regulation) attribute Values
// ----------------------------------------
REG_A = "Norm A";
REG_B = "Norm B";
REG_C = "None";
REG_D = "Category A";
REG_E = "Category B";
REG_F = "Category C";
REG_G = "Consequence Class 3";
REG_U = "Unspecified";
//
// FIN (Finish) attribute Values
// ----------------------------------------
FIN_A = "Class A";
FIN_B = "Class B";
FIN_C = "Class C";
FIN_D = "Class D";
FIN_E = "Finish Class 1";
FIN_F = "Finish Class 2";
FIN_G = "Finish Class 3";
FIN_H = "Finish Class 4";
FIN_I = "Finish Class 5";
FIN_J = "Finish Class 3FF";
FIN_U = "Unspecified";
//
// DISM (Discretization method) attributes values:
// --------------------------------------------------------------------------------
DISM_U    = "Unspecified";
DISM_SAG  = "Sag";
DISM_STEP = "Step";
//
//
/// SECTION C-4-CATIA V5 BiW Fastener Shape Attributes 
// ************ ==================================================================
//
// Attributes Names
// --------------------------------------------------------------------------------
DIA = "Diameter";
OFS = "Shank Base Offset";
LEN = "Shank Length";
DIH = "Head Diameter";
LEH = "Head Length";
DIF = "Foot Diameter";
LEF = "Foot Length";
MAT = "Material";
SDF = "Definition";
SRP = "Representation";
BAS = "Base";
HEIGHT = "Height";
STH1 = "Thickness1";
STH2 = "Thickness2";
//
// MAT (Material) attributes Values:
// --------------------------------------------------------------------------------
MAT_WLDA = "Weld material 1";
MAT_WLDB = "Weld material 2";
MAT_ADHA = "Glue 1";
MAT_ADHB = "Glue 2";
MAT_SEAA = "Sealant material 1";
MAT_SEAB = "Sealant material 2";
MAT_U    = "Unspecified";
//

// SDF (Definition) attributes Values:
// --------------------------------------------------------------------------------
SDF_PT3 = "3D Point";
SDF_HSP = "Hemisphere";
SDF_SH  = "Shank";
SDF_SHH = "Shank with Head";
SDF_SHF = "Shank with Head & Foot";

SDF_CPH = "Curve Path";
SDF_CYL = "Cylinder Pipe";
SDF_HCY = "Half Cylinder Pipe";
SDF_DMD = "Diamond Pipe";
SDF_HDM = "Half Diamond Pipe";
SDF_REC = "Rectangle Pipe";
SDF_HRE = "Half Rectangle Pipe";

SDF_THS = "Thin Surface";

// SRP (Representation) attributes Values:
// --------------------------------------------------------------------------------
SRP_SYM = "Symbolic";
SRP_WIR = "Wireframe";
SRP_SUR = "Surfacic";
SRP_SOL = "DMU";
//
// Location Method and Support Spec
// --------------------------------------------------------------------------------
LOC     = "Location";
SUP     = "Support";

// Parameters for BiW Fastener Functional Annotations 
USR      = "User Value";
FN       = "[Number of Fasteners]";
J        = "BiW Joint";
JB       = "BiW Joint Body";
JP       = "Joined Parts";
LENGTH   = "Length";
JPIN      = "Joined Part Instance Names";
JZN      = "Joined Zone Names";
JZTHKMAT = "Joined Zone Material/Thickness"; 
