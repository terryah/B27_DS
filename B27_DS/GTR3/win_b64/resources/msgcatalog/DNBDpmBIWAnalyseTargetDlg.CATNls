Title = "Analyse Target Orientation";
BOK = "OK";
BCancel = "Cancel";

RobotKey = " Robot ";
ToolPositionKey = " ToolPositions ";
ActivitiesKey = " Activities ";
StatusKey = " Status ";
WeldKey = " Welds ";
CurrentRobotKey = " Current Robot ";
RobotsOKKey = " Robots OK ";
YesKey = "Yes";
NoKey = "No";

ResourceKey = "Resources ";
ProfileKey = "Tool Profile";
ConfigKey = "Configs";
TargetKey = "Targets ";
NoFlipKey = "No Flip";
FlipKey   = "Flip";
ReassignKey = "Reassign";

ToolProfile = "Tool Profile";
ToolName = "Tool";
ToolPosition = "Tool Position";
ToolType = "Tool Type";
NoFeasibleWelds.Msg = "No Feasible Welds in No Flip mode";
NoWeldsReassigned.Msg = "No Welds Reassigned. Cannot Create an Activity";
SaveGeometry.Msg = "Use 'Display Pie' before saving Geometry";

TabPageContainer.PathAndTargetTab.Title = "Path and Targets";

TabPageContainer.PathAndTargetTab.RobotFrame.Title = " Robots ";
TabPageContainer.PathAndTargetTab.RobotFrame.RobotNameFrame.RobotLabel.Title = "Robot: ";
TabPageContainer.PathAndTargetTab.RobotFrame.ToolProfileFrame.ToolProfileAvailFrame.ToolProfileAvailLabel.Title = "Available Tool Profiles: ";
TabPageContainer.PathAndTargetTab.RobotFrame.ToolProfileFrame.ToolProfileSelectFrame.ToolProfileSelectLabel.Title = "Selected Tool Profiles: ";
TabPageContainer.PathAndTargetTab.RobotFrame.ConfigFrame.Title = " Available Configs ";
TabPageContainer.PathAndTargetTab.RobotFrame.WeldsFrame.Title = " Welds ";
TabPageContainer.PathAndTargetTab.RobotFrame.WeldsFrame.TargetRemove.Title = " Remove ";
TabPageContainer.PathAndTargetTab.RobotFrame.WeldsFrame.TargetRemove.Help = " Remove ";
TabPageContainer.PathAndTargetTab.RobotFrame.WeldsFrame.TargetRemove.ShortHelp = " Remove Welds from List ";
TabPageContainer.PathAndTargetTab.RobotFrame.WeldsFrame.TargetRemove.LongHelp = " Remove Welds from Association to the Robot ";

TabPageContainer.AnalysisTab.Title = "Analysis Parameters";

TabPageContainer.AnalysisTab.FeasibilityFrame.Title = " Feasibility ";

TabPageContainer.AnalysisTab.FeasibilityFrame.CheckClash.Title = "Check For Collision";
TabPageContainer.AnalysisTab.FeasibilityFrame.CheckClash.Help = "Check For Collision";
TabPageContainer.AnalysisTab.FeasibilityFrame.GraphicUpdate.Title = "Show Graphics Update";
TabPageContainer.AnalysisTab.FeasibilityFrame.GraphicUpdate.Help = "Show Graphics Update";
TabPageContainer.AnalysisTab.FeasibilityFrame.CheckClash.ShortHelp = "Perform Collision Check";
TabPageContainer.AnalysisTab.FeasibilityFrame.CheckClash.LongHelp = "Perform Collision Check";

TabPageContainer.AnalysisTab.FeasibilityFrame.CheckAll.Title = " Analyse All ";
TabPageContainer.AnalysisTab.FeasibilityFrame.CheckAll.Help = " Analyse All ";
TabPageContainer.AnalysisTab.FeasibilityFrame.CheckAll.ShortHelp = " Analysis All Values from Start Angle to End Angle ";
TabPageContainer.AnalysisTab.FeasibilityFrame.CheckAll.LongHelp = " Analysis All Values from Start Angle to End Angle ";

TabPageContainer.AnalysisTab.ClashParametersFrame.Title = " Parameters ";

TabPageContainer.AnalysisTab.ClashParametersFrame.AnalRobotFrame.AnalRobotLabel.Title = " Robots                  ";
TabPageContainer.AnalysisTab.ClashParametersFrame.AnalRobotFrame.AddRobotPartsButton.Title = "...";
 
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.Title = " Collision Objects         ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.RotateTool.Title = " Step Angle  ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.RotateTool.Help = " Rotate Tool by StepAngle  ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.RotateTool.ShortHelp = " Step for Rotation as per tool option settings (Y/Z)  ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.RotateTool.LongHelp = " Step for Rotation as per tool option settings (Y/Z)  ";


TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.ClashRemove.Title = " Remove ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.ClashRemove.Help = " Remove Clash Objects ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.ClashRemove.ShortHelp = " Remove Clash Objects for the Selected Robot ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.ClashRemove.LongHelp = " Remove Clash Objects for the Selected Robot ";


TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.Tolerance.Title = " Collision Tolerance ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.Tolerance.Help = " Collision Tolerance ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.Tolerance.ShortHelp = " Collision Tolerance ";
TabPageContainer.AnalysisTab.ClashParametersFrame.CollisionObjectsFrame.TolFrame.Tolerance.LongHelp = " Collision Tolerance ";

TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.Title = " Range Of Check ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.StartAngle.Title = " Start Angle ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.StartAngle.Help = " Start Angle ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.StartAngle.ShortHelp = " Specify the Start Angle for the Analysis ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.StartAngle.LongHelp = " Specify the Start Angle for the Analysis ";

TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.EndAngle.Title = " End Angle ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.EndAngle.Help = " End Angle ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.EndAngle.ShortHelp = " Specify the End Angle for the Analysis ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.EndAngle.LongHelp = " Specify the End Angle for the Analysis ";

TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.DoFlip.Title = " Do Flip ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.DoFlip.Help = " Do Flip ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.DoFlip.ShortHelp = " Do Flip ";
TabPageContainer.AnalysisTab.ClashParametersFrame.WeldDataFrame.StartEndFrame.DoFlip.LongHelp = " Rotates the Target By 180 degrees and then Performs the Analysis ";


TabPageContainer.ResultTab.Title="Results";

TabPageContainer.ResultTab.TargetReportFrame.Title = " Target Report ";

TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotComputed.Title = "NC : Not Computed";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotComputed.Help = "Not Computed Yet";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotComputed.ShortHelp = "Not Computed Yet";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotComputed.LongHelp = "Not Computed Yet";

TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotReachable.Title = "NR : Not Reachable";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotReachable.Help = "Weld Not Reachable";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotReachable.ShortHelp = "Weld Not Reachable";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotReachable.LongHelp = "Weld Not Reachable";

TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotFeasible.Title = "NF : Not Feasible";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotFeasible.Help = "Weld Not Feasible ( Collision )";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotFeasible.ShortHelp = "Weld Not Feasible ( Collision )";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.NotFeasible.LongHelp = "Weld Not Feasible ( Collision )";

TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.Feasible.Title = "OK : Feasible";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.Feasible.Help = "Weld Feasible";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.Feasible.ShortHelp = "Weld Feasible";
TabPageContainer.ResultTab.TargetReportFrame.ResultReportFrame.Feasible.LongHelp = "Weld Feasible";

TabPageContainer.ResultTab.TargetReportFrame.Sort.Title = " Sort ";

TabPageContainer.ResultTab.TargetReportFrame.Sort.SortRobot.Title = " By Robots ";
TabPageContainer.ResultTab.TargetReportFrame.Sort.SortRobot.Help = " Sort By Robots ";
TabPageContainer.ResultTab.TargetReportFrame.Sort.SortRobot.ShortHelp = " Sort By Robots ";
TabPageContainer.ResultTab.TargetReportFrame.Sort.SortRobot.LongHelp = " Sort by Robot, Weld, Status ";

TabPageContainer.ResultTab.TargetReportFrame.Sort.SortWeld.Title = "  By Welds ";
TabPageContainer.ResultTab.TargetReportFrame.Sort.SortWeld.Help = "  Sort By Welds ";
TabPageContainer.ResultTab.TargetReportFrame.Sort.SortWeld.ShortHelp = "  Sort By Welds ";
TabPageContainer.ResultTab.TargetReportFrame.Sort.SortWeld.LongHelp = "  Sort by Welds, Assigned Robot, OK Robots ";

TabPageContainer.ResultTab.DisplayPieFrame.Title = " Pie Report and Target Manipulation";
TabPageContainer.ResultTab.DisplayPieFrame.DisplayPie.Title = " Display Pie Reports      ";
TabPageContainer.ResultTab.DisplayPieFrame.DisplayPie.Help = " Display Pie Reports ";
TabPageContainer.ResultTab.DisplayPieFrame.DisplayPie.ShortHelp = " Display Pie Reports ";
TabPageContainer.ResultTab.DisplayPieFrame.DisplayPie.LongHelp = " Display Pie Reports ";
TabPageContainer.ResultTab.DisplayPieFrame.PieRobotLabel.Title = " Robot: ";
TabPageContainer.ResultTab.DisplayPieFrame.PieProfileLabel.Title = " Tool Profile: ";
TabPageContainer.ResultTab.DisplayPieFrame.PieConfigLabel.Title = " Config: ";
TabPageContainer.ResultTab.DisplayPieFrame.PieApply.Title = " Display Pie ";
TabPageContainer.ResultTab.DisplayPieFrame.PieApply.Help = "Display Pie";
TabPageContainer.ResultTab.DisplayPieFrame.PieApply.ShortHelp = " Display the pie reports ";
TabPageContainer.ResultTab.DisplayPieFrame.PieApply.LongHelp = " Display the pie reports for selected combination ";

TabPageContainer.ResultTab.DisplayPieFrame.WeldPosManipLabel.Title = " Target Position Manipulation: ";

TabPageContainer.ResultTab.DisplayPieFrame.RotateNearest.Title = "Rotate X( Closest )";
TabPageContainer.ResultTab.DisplayPieFrame.RotateNearest.Help = "Rotate X( Closest )";
TabPageContainer.ResultTab.DisplayPieFrame.RotateNearest.ShortHelp = " Rotate X to the nearest Feasible Position ";
TabPageContainer.ResultTab.DisplayPieFrame.RotateNearest.LongHelp = " Rotate X to the nearest Feasible Position ";

TabPageContainer.ResultTab.DisplayPieFrame.RotateBiggest.Title = "Rotate X( Biggest )";
TabPageContainer.ResultTab.DisplayPieFrame.RotateBiggest.Help = "Rotate X( Biggest )";
TabPageContainer.ResultTab.DisplayPieFrame.RotateBiggest.ShortHelp = " Rotate X to the middle to the biggest Feasible region ";
TabPageContainer.ResultTab.DisplayPieFrame.RotateBiggest.LongHelp = " Rotate X to the middle to the biggest Feasible region ";

TabPageContainer.ResultTab.DisplayPieFrame.RotateXforNFWelds.Title = " Rotate X for NF Welds ";
TabPageContainer.ResultTab.DisplayPieFrame.RotateXforNFWelds.Help = " Allow rotation of X axis for NF welds ";
TabPageContainer.ResultTab.DisplayPieFrame.RotateXforNFWelds.ShortHelp = " Allow rotation of X axis for NF welds ";
TabPageContainer.ResultTab.DisplayPieFrame.RotateXforNFWelds.LongHelp = " Allow rotation of X axis for reachable but unfeasible welds ";

TabPageContainer.ResultTab.SaveFrame.Title = " Save ";

TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Remove.Title = " Remove ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Remove.Help = " Remove the Report ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Remove.ShortHelp = " Remove the Report";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Remove.LongHelp = " Remove the Report ";

TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Save.Title = " Save ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Save.Help = " Save the Report ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Save.ShortHelp = " Save the Report to file ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Save.LongHelp = " Save the Report to file in xml format ";

TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Import.Title = " Import ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Import.Help = " Import Results ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Import.ShortHelp = " Import Results ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.Import.LongHelp = " Import Results from xml file ";

TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveGeometry.Title = " Save Geometry ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveGeometry.Help = " Save the Pie Geometry ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveGeometry.ShortHelp = " Save the Pie Representations of the Welds ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveGeometry.LongHelp = " Save the Pie Representations of the Welds to a CGR(.cgr) file ";

TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveAllPie.Title = "  Save All Pie  ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveAllPie.Help = " Save All Pie ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveAllPie.ShortHelp = " Save all the Pie Representations in one CGR file ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame1.SaveAllPie.LongHelp = " Save all the Pie Representations in one CGR file ";

TabPageContainer.ResultTab.SaveFrame.SaveFrame3.CheckKO.Title = "Check KO             ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame3.CheckKO.Help = "Check KO against other Robots ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame3.CheckKO.ShortHelp = "Check KO against other Robots";
TabPageContainer.ResultTab.SaveFrame.SaveFrame3.CheckKO.LongHelp = "Assign KO welds to other Robots ";

TabPageContainer.ResultTab.SaveFrame.SaveFrame3.ReassignWelds.Title = "Reassign Welds   ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame3.ReassignWelds.Help = "Reassign Welds to other feasible Robots ";
TabPageContainer.ResultTab.SaveFrame.SaveFrame3.ReassignWelds.ShortHelp = "Reassign Welds to other feasible Robots";
TabPageContainer.ResultTab.SaveFrame.SaveFrame3.ReassignWelds.LongHelp = "Reassign Welds to other feasible Robots ";

TabPageContainer.ResultTab.Analyse.Title = " Analyse ";

TabPageContainer.ResultTab.Analyse.Compute.Title = " Compute ";
TabPageContainer.ResultTab.Analyse.Compute.Help = "Compute";
TabPageContainer.ResultTab.Analyse.Compute.ShortHelp = " Start Computation from the First Weld ";
TabPageContainer.ResultTab.Analyse.Compute.LongHelp = " Start Computation from the First Weld ";

TabPageContainer.ResultTab.Analyse.Resume.Title = " Resume ";
TabPageContainer.ResultTab.Analyse.Resume.Help = " Resume ";
TabPageContainer.ResultTab.Analyse.Resume.ShortHelp = " Resumes the Computation from  the Next Weld( after stop ) ";
TabPageContainer.ResultTab.Analyse.Resume.LongHelp = " Resumes the Computation from  the Next Weld( after stop ) ";

TabPageContainer.ResultTab.Analyse.Stop.Title = " Stop ";
TabPageContainer.ResultTab.Analyse.Stop.Help = " Stop ";
TabPageContainer.ResultTab.Analyse.Stop.ShortHelp = " Stops the Computations ";
TabPageContainer.ResultTab.Analyse.Stop.LongHelp = " Stops the Computations ";

TabPageContainer.ResultTab.CreateTaskFrame.CreateRobotTask.Title = "Create Robot Task";
TabPageContainer.ResultTab.CreateTaskFrame.CreateRobotTask.Help = " Create Robot Task ";
TabPageContainer.ResultTab.CreateTaskFrame.CreateRobotTask.ShortHelp = " Creates the Robot Task for OK Welds in No Flip mode ";
TabPageContainer.ResultTab.CreateTaskFrame.CreateRobotTask.LongHelp = " Creates the Robot Task for OK Welds in No Flip mode ";
