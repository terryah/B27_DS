DialogBoxTitle                                   = "Corner Fillet";

//Selection area

FrameType.LongHelp                               = "Selection of the Corner Type Ball or Blend.";
FrameType.Title                                  = "Corner Type: ";

ComboType.Ball                                   = "Ball";
ComboType.Blend                                  = "Blend";
ComboType.LongHelp                               = "- Ball -
  In the corner is positioned a ball with a specified radius.
  The adjacent fillets are trimmed. The connecting surfaces
  between ball and fillets form the ball corner, thus ensuring
  that the minimum radius is directly applied on the ball surface.
  For this corner type must be selected as input elements three
  fillet surfaces created with the Advanced Fillet command. No
  other input type is accepted as valid input, i.e. Styling Fillet
  or Tri-tangent Fillet features.
  If corner type Ball is selected, the input fields Fillet 1 to 3
  are displayed. For each field only one existing Advanced Fillet
  feature can be selected.
  The Support option is not available.
- Blend -
  The corner fillet consists of four edges. Three of them are the
  trimmed fillets and the fourth edge is connected to a basic surface.
  For this corner type three fillet surfaces of any type can be selected
  as input elements.
  If the Corner Type Blend is selected, the input boxes Surface 1 to
  Surface 3 are displayed. For each input box only one surface ca
  be selected.
  The Support option is available for the selection of one or several
  support surfaces.";
ComboType.ShortHelp                              = "Select Corner Fillet type";

//Surface selection for Ball Corner

LabelFillet1.Title                               = "Fillet 1: ";
LabelFillet1.LongHelp                            = "Select a fillet surface created with the Advanced Fillet command.";
LabelFillet1.ShortHelp                           = "Select fillet 1";
LabelFillet2.Title                               = "Fillet 2: ";
LabelFillet2.LongHelp                            = "Select a fillet surface created with the Advanced Fillet command.";
LabelFillet2.ShortHelp                           = "Select fillet 2";
LabelFillet3.Title                               = "Fillet 3: ";
LabelFillet3.LongHelp                            = "Select a fillet surface created with the Advanced Fillet command.";
LabelFillet3.ShortHelp                           = "Select fillet 3";

//Surface selection for Blend Corner

LabelSurface1.Title                              = "Surface 1: ";
LabelSurface1.LongHelp                           = "Select Surface 1.
As input are allowed surfaces of any fillet type.";
LabelSurface1.ShortHelp                          = "Select Surface 1";
LabelSurface2.Title                              = "Surface 2: ";
LabelSurface2.LongHelp                           = "Select Surface 2.
As input are allowed surfaces of any fillet type.";
LabelSurface2.ShortHelp                          = "Select Surface 2";
LabelSurface3.Title                              = "Surface 3: ";
LabelSurface3.LongHelp                           = "Select Surface 3.
As input are allowed surfaces of any fillet type.";
LabelSurface3.ShortHelp                          = "Select Surface 3";
LabelSupport.Title                               = "Support: ";
LabelSupport.LongHelp                            = "Only available for Corner Type Blend.
Selects the common support surface of Fillet 1
and Fillet 3 for the blend corner creation.";
LabelSupport.ShortHelp                           = "Select support";

//_________________________________________________________________________________________________

CheckButtonTrim1.Title                           = "Trim Fillet/Surface 1";
CheckButtonTrim1.LongHelp                        = "Fillet/Surface 1 is trimmed at the appropriate edge of the corner fillet.";
CheckButtonTrim1.ShortHelp                       = "Trim Fillet/Surface 1";
CheckButtonTrim2.Title                           = "Trim Fillet/Surface 2";
CheckButtonTrim2.LongHelp                        = "Fillet 2/Surface is trimmed at the appropriate edge of the corner fillet.";
CheckButtonTrim2.ShortHelp                       = "Trim Fillet/Surface 2";
CheckButtonTrim3.Title                           = "Trim Fillet/Surface 3";
CheckButtonTrim3.LongHelp                        = "Fillet 3/Surface is trimmed at the appropriate edge of the corner fillet.";
CheckButtonTrim3.ShortHelp                       = "Trim Fillet/Surface 3";

//Ball tab

TabPageBall.Title                                = "Ball";
FrameBallContinuity.Title                        = "Ball Parameter";
FrameBallContinuity.LongHelp                     = "Definition of individual continuity conditions 
between Ball corner and fillet supports.";

LabelBallFillet1.Title                           = "Fillet 1: ";
LabelBallFillet1.LongHelp                        = "Continuity between created Ball Corner Fillet and Fillet 1.";
LabelBallFillet1.ShortHelp                       = "Continuity to Fillet 1";
LabelBallFillet2.Title                           = "Fillet 2: ";
LabelBallFillet2.LongHelp                        = "Continuity between created Ball Corner Fillet and Fillet 2.";
LabelBallFillet2.ShortHelp                       = "Continuity to Fillet 2";
LabelBallFillet3.Title                           = "Fillet 3: ";
LabelBallFillet3.LongHelp                        = "Continuity between created Ball Corner Fillet and Fillet 3.";
LabelBallFillet3.ShortHelp                       = "Continuity to Fillet 3";

ContinuityBallFillet1.Button_G0.ShortHelp        = "G0 continuity to Fillet 1";
ContinuityBallFillet1.Button_G1.ShortHelp        = "G1 continuity to Fillet 1";
ContinuityBallFillet1.Button_G2.ShortHelp        = "G2 continuity to Fillet 1";
ContinuityBallFillet1.Button_G3.ShortHelp        = "G3 continuity to Fillet 1";

ContinuityBallFillet2.Button_G0.ShortHelp        = "G0 continuity to Fillet 2";
ContinuityBallFillet2.Button_G1.ShortHelp        = "G1 continuity to Fillet 2";
ContinuityBallFillet2.Button_G2.ShortHelp        = "G2 continuity to Fillet 2";
ContinuityBallFillet2.Button_G3.ShortHelp        = "G3 continuity to Fillet 2";

ContinuityBallFillet3.Button_G0.ShortHelp        = "G0 continuity to Fillet 3";
ContinuityBallFillet3.Button_G1.ShortHelp        = "G1 continuity to Fillet 3";
ContinuityBallFillet3.Button_G2.ShortHelp        = "G2 continuity to Fillet 3";
ContinuityBallFillet3.Button_G3.ShortHelp        = "G3 continuity to Fillet 3";

LabelBallRadius.Title                            = "Ball radius: ";
LabelBallRadius.LongHelp                         = "Defines the size of a theoretical ball which 
touches all three selected support fillets.
The smaller the value the sharper the Ball 
corner will be, the larger the value the 
smoother the Ball corner will be.";
LabelBallRadius.ShortHelp                        = "Defines the ball radius";


//Blend tab

TabPageBlend.Title                               = "Blend";

//Length frame
FrameLength.Title                                = "Length";
FrameLength.LongHelp                             = "The Length options define the blend edge of the corner fillet between
two fillet surfaces used as support. As default, the edge between
fillet 1 and 3 opposite to fillet 2 is defined as Length.";

LabelLength1.Title                               = "Length 1: ";
LabelLength1.LongHelp                            = "Derives from the distance between the intersection point of
the radius runout lines (RRL) of Surface 1 and 3 and the upper
corner points of the corner fillet surface at Surface 1.";
LabelLength1.ShortHelp                           = "Definition of the blend edge length at Surface 1";

LabelLength3.Title                               = "Length 3: ";
LabelLength3.LongHelp                            = "Derives from the distance between the intersection point of
the radius runout lines (RRL) of Surface 1 and 3 and the upper
corner points of the corner fillet surface at Surface 3.";
LabelLength3.ShortHelp                           = "Definition of the blend edge length at Surface 3";

LinkButton_Length.LongHelp                       = "The same value (of Length1) is used for both lengths. Changing
Length 1 automatically sets Length 3 to the same value.";
LinkButton_Length.ShortHelp                      = "Equal lengths on both sides";

//Supports frame
FrameSupports.Title                              = "Supports";
FrameSupports.LongHelp                           = "Defines the continuity between the created Blend Corner Fillet and Supports.";

LabelContinuitySupports.Title                    = "Continuity: ";
LabelContinuitySupports.LongHelp                 = "Defines the continuity between the created Blend Corner Fillet and Supports.";
LabelContinuitySupports.ShortHelp                = "Continuity to Supports";

ContinuitySupports.Button_G0.ShortHelp           = "G0 Continuity to Supports";
ContinuitySupports.Button_G1.ShortHelp           = "G1 Continuity to Supports";
ContinuitySupports.Button_G2.ShortHelp           = "G2 Continuity to Supports";
ContinuitySupports.Button_G3.ShortHelp           = "G3 Continuity to Supports";


//Surface 1 frame
FrameSurface1.Title                              = "Surface 1";
FrameSurface1.LongHelp                           = "Defines the continuity between the created Blend Corner Fillet and Surface 1.";

LabelContinuitySurface1.Title                    = "Continuity: ";
LabelContinuitySurface1.LongHelp                 = "Defines the continuity between the created Blend Corner Fillet and Surface 1.";
LabelContinuitySurface1.ShortHelp                = "Continuity Surface 1";

ContinuitySurface1.Button_G0.ShortHelp           = "G0 continuity to Surface 1";
ContinuitySurface1.Button_G1.ShortHelp           = "G1 continuity to Surface 1";
ContinuitySurface1.Button_G2.ShortHelp           = "G2 continuity to Surface 1";
ContinuitySurface1.Button_G3.ShortHelp           = "G3 continuity to Surface 1";


//Surface 2 frame
FrameSurface2.Title                              = "Surface 2";
FrameSurface2.LongHelp                           = "Defines the continuity between the created Blend Corner Fillet and Surface 2.";

LabelContinuitySurface2.Title                    = "Continuity: ";
LabelContinuitySurface2.LongHelp                 = "Defines the continuity between the created Blend Corner Fillet and Surface 2.";
LabelContinuitySurface2.ShortHelp                = "Continuity to Surface 2";

ContinuitySurface2.Button_G0.ShortHelp           = "G0 continuity to Surface 2";
ContinuitySurface2.Button_G1.ShortHelp           = "G1 continuity to Surface 2";
ContinuitySurface2.Button_G2.ShortHelp           = "G2 continuity to Surface 2";
ContinuitySurface2.Button_G3.ShortHelp           = "G3 continuity to Surface 2";


//Surface 3 frame
FrameSurface3.Title                              = "Surface 3";
FrameSurface3.LongHelp                           = "Defines the continuity between the created Blend Corner Fillet and Surface 3.";

LabelContinuitySurface3.Title                    = "Continuity: ";
LabelContinuitySurface3.LongHelp                 = "Defines the continuity between the created Blend Corner Fillet and Surface 3.";
LabelContinuitySurface3.ShortHelp                = "Continuity to Surface 3";

ContinuitySurface3.Button_G0.ShortHelp           = "G0 continuity to Surface 3";
ContinuitySurface3.Button_G1.ShortHelp           = "G1 continuity to Surface 3";
ContinuitySurface3.Button_G2.ShortHelp           = "G2 continuity to Surface 3";
ContinuitySurface3.Button_G3.ShortHelp           = "G3 continuity to Surface 3";


//Relimitation frame_________________________________________________________________________________________________

FrameRelimitation.Title                          = "Relimitation";
FrameRelimitation.LongHelp                       = "Trims the fillet surface as face or as surface (no face).";

RadioButtonFace.Title                            = "Face: ";
RadioButtonFace.LongHelp                         = "The basic surface remains unchanged. The result is a face.";
RadioButtonFace.ShortHelp                        = "Trim face";

RadioButtonTrim.Title                            = "Trim: ";
RadioButtonTrim.LongHelp                         = "The basic surface changes. The result is not a face.";
RadioButtonTrim.ShortHelp                        = "Trim approx";

