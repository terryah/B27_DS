// * Symbol *
Strategy = "Strategy";

// * Symbol *
Feedrate = "Speeds";

// * Symbol *
RetractFeedrate = "Retract feedrate";

// * Symbol *
MachiningFeedrate = "Machining feedrate";

// * Symbol *
ApproachFeedrate = "Approach feedrate";

// * Symbol *
FirstRevFeedrate  = "Feedrate 1. Rev";

// * Symbol *
SecondRevFeedrate = "Feedrate 2. Rev";

// * Symbol *
FinishingFeedrate = "Finishing feedrate";

// * Symbol *
MfgDwellDefault = "No dwell";

// * Symbol *
MfgDwellTime    = "Dwell in time units";

// * Symbol *
MfgDwellRound   = "Dwell in revolutions";

// * Symbol *
MfgDwellRoundDouble   = "Dwell in revolutions";

// * Symbol *
ApproachLength   = "Approach";

// * Symbol *
RetractLength   = "Retract";

// * Symbol *
Breakthrough   = "Breakthrough";

// * Symbol *
Comment   = "Comment";

// * Symbol *
SpindleSpeed   = "Spindle speed";

// * Symbol *
LowSpindleSpeed= "Low spindle speed";

// * Symbol *
PlungeValue   = "Value";

// * Symbol *
Plunge = "Plunge";

// * Symbol *
Depth = "Depth";

// * Symbol *
SaveDistance = "Save distance";

// * Symbol *
DeltaDepth = "Delta depth";

// * Symbol *
RapidDepth = "Rapid depth";

// * Symbol *
SpotDepth = "Spot depth";

// * Symbol *
MfgToolAxis = "Tool axis";

// Lift
Lift      = "Shift";
MfgLiftOffPolar = "Defined by polar coordinates";
MfgLiftOffDistance = "Defined by linear coordinates";
MfgNoLiftOff = "None";
LiftX = "Shift along X";
LiftY = "Shift along Y";
LiftZ = "Shift along Z";
LiftWay = "Shift distance";
LiftAngle= "Shift angle";

// Compensation
FirstCompensation ="First compensation";
SecondCompensation ="Second compensation";



// =============================//
// Start Common Part            //
// =============================//
Rounds         = "Revolutions";
Time           = "Time";
SecondClear    = "Second Clear";


MfgChamferDiameter = "Chamfer diameter (C)";
MfgChamferDiameter.LongHelp = "MFG_CHAMFER_VAL: Value of the chamfer, which corresponding to a diameter on the conical part of the tool.";


DeltaTip     = "Delta Tip";
DeltaDiameter     = "Delta diameter";
DeltaOffset       = "Delta offset";
DeltaShoulder     = "Delta shoulder";
DeltaIncrement    = "Delta increment";
DeltaDistance     = "Delta distance";

SpotTip           = "Spot tip depth (Dsp)";
SpotDiameter      = "Spot diameter (Dd)";
SpotOffset        = "Spot offset (Osp)";
SpotShoulder      = "Spot shoulder depth (Dsp)";
SpotDistance      = "Spot shoulder depth (Dsp)";

DepthTip          = "Tip depth (Dt)";
DepthDiameter     = "Diameter (D)";
DepthOffset       = "Offset";
DepthShoulder     = "Shoulder depth (Ds)";
DepthDistance     = "Distance depth (Dd)";

DepthBreak        = "Breakthrough (B)";
DepthBreak.LongHelp = "MFG_BREAKTHROUGH: Distance in the tool axis direction that the tool goes completely through the part.";

PlungeOffset      = "Plunge offset";
PlungeDiameter    = "Plunge diameter";
PlungeTip         = "Plunge tip depth";
PlungeShoulder    = "Plunge shoulder depth";
PlungeDistance    = "Plunge distance depth";
SecondPlunge      = "Second plunge";

MultiplicatorBM = "Multiplicator BM";
DegressionDB    = "Degression DB";
DistanceAB      = "Distance AB";
AngleWS         = "Angle WS";
DistanceRTS     = "Distance RTS";
BreakThroughTB  = "BreakThrough TB";

ChamferDistance = "Chamfer distance";
ChamferDiameter = "Chamfer diameter";
ChamferDiameter2 = "Chamfer diameter 2";
ChamferShoulder = "Chamfer shoulder";
RapidShoulder   = "Rapid shoulder";

BackDepth    = "Back depth";
BackDiameter = "Back diameter";
BackOffset   = "Back offset";

Shoulder  = "Shoulder";
Tip       = "Tip";
Distance  = "Distance";
Diameter  = "Diameter";
Rapid     = "Rapid";
Dwell     = "Dwell";
PreDrilling ="First Drilling";
Processing ="Processing"; 
Finishing = "Finishing";
Retract   = "Retract";
SpindleStart ="Spindle Start";
SpindleStop  ="Spindle Stop";
TransitionPath="Transition Path";
Tool_Tip   = "Tool Tip";
Path_of_Tool_Tip= "Path of Tool Tip";
X = "X";
Y = "Y";
Z = "Z"; 
1DOT5MPitch = "1 1/2 * Pitch";

Positionieren = "Transition";
Abfahren = "Retract";
Letzter_Schnitt = "Last Cut";
D = "D";
R_Add_D = "R+D";
CD1Decdep = "C+D*(1-Decdep)";
CD12Decdep = "C+D*(1-2*Decdep)";
RetractOffset = "Retract offset";
DecrementDepth = "Decdep = Decrement depth";
D_EffectiveDepthOfCutComputed = "D = Effective depth of cut (computed)";
C_Retract = "C = Retract offset";
R_Retract = "R = Retract offset";
Distance23 = "Distance (2,3) = Plunge Tip + D";
Distance45 = "Distance (4,5) = Distance (2,3)";
Distance56 = "Distance (5,6) = Plunge Tip + D - C";
Distance67 = "Distance (6,7) = C+D*(1-Decdep)";
Distance1011 = "Distance (10,11) = C+D*(1-2*Decdep)";
Distance35 = "Distance (3,5) = Distance (6,8) = R";
Distance89 = "Distance (5,6) = Distance (8,9) = R + D";

// Point To Point
PtToPtOffset = "Offset";
PtToPtOffset.LongHelp = "MFG_OFF_TL_AXIS: Specifies an offset along the tool axis";


// * Symbol *
EndOfPath = "End of path";

// -----------------------
// Auxiliary operations
// -----------------------

// Tool change information on Setup
MfgToolChgX= "X Tool change";
MfgToolChgY= "Y Tool change";
MfgToolChgZ= "Z Tool change";

CmdSequenceNum="Sequence number";

// PP Instruction
MFG_PPWORDS = "P.P. Instruction";

// Coordinate system

// Table/Head Rotation
MFG_ROTARY_ANGLE  = "Rotary angle";
MFG_ROT_DIR      = "Rotary direction";
MFG_ROT_TYP      = "Rotary type";
MFG_ROTARY_CATEGORY = "Category";
MFG_ROTARY_CATEGORY.LongHelp = "Rotary Category : Table or Head";
MFG_ROTARY_AXIS   = "Rotary axis";
MFG_ANGLE_MODE    = "Definition";



// TransitionPath
AutomaticTransition = "Automatic Transition";

