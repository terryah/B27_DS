// ==========================================================================
// ==========================================================================
// DNBHumanModelingUI English Message File
// ==========================================================================
// ==========================================================================
MemoWindow.Title = "Description (/p1)";
MemoWindow.Editor.AnthroShortHelp = "Anthropometry Comments";
MemoWindow.Editor.ManikinShortHelp = "Manikin Comments";
MemoWindow.Editor.AnthroVarableShortHelp = "Anthropometric variable Comments";
MemoWindow.Editor.VisionShortHelp = "Vision Comments";
MemoWindow.Editor.PostureShortHelp = "Posture Comments";
MemoWindow.Editor.PrefAngleShortHelp = "Preferred angle Comments";
PostureLabel.Title = "\Posture";

ReachDistance.Message = "Reach error margin: ";
SelectSegment = "Select segment...";
SelectManikin = "Select manikin...";
PutCompassOnTarget = "Put compass on target...";
PlaceCompassAndSelectManikin = "Place the compass at the target location and select a manikin...";
PlaceCompassAndSelectSegment = "Place the compass at the target location and select a segment...";
PlaceCompassToMoveManikin = "Place the compass at the target location for manikin '/p1', or select an other manikin...";
LeftArmReachEnvelope = "Left Arm Reach Envelope";
RightArmReachEnvelope = "Right Arm Reach Envelope";
LeftLegReachEnvelope = "Left Leg Reach Envelope";
RightLegReachEnvelope = "Right Leg Reach Envelope";
LeftArmReachEnvelopeConstruction.Title = "Left Arm Reach Envelope Construction";
RightArmReachEnvelopeConstruction.Title = "Right Arm Reach Envelope Construction";
LeftLegReachEnvelopeConstruction.Title = "Left Leg Reach Envelope Construction";
RightLegReachEnvelopeConstruction.Title = "Right Leg Reach Envelope Construction";

ReachEnvelopeFrameType.Title = " Range Of Motion ";
ReachEnvelopeFrameSide.Title = " Side ";
ReachEnvelopeFrameSegment.Title = " Segment ";
DefaultSegmentForReachEnvelope.Title = "Center of grip";

InvalidInputOnReachEnvelope =
"Invalid Input.
Only segments on hand or feet can be used with this command.";

ReachEnvelope.Title = "Reach envelope";
IdealReachEnvelope.Title = "Ideal";
ExtendedReachEnvelope.Title = "Extended, with upper body";
90_Percent_ReachEnvelope.Title = " Physiological maximal";
100_Percent_ReachEnvelope.Title = " Current ";
SpecificType.Title = " Specific ";
CurrnetType.Title = " Current ";

LeftSideReachEnvelope.Title = "Left";
RightSideReachEnvelope.Title = "Right";
BothSideReachEnvelope.Title = "Both";

PushIEdit.Title = "Edit...";
PushIAdd.Title = "Add";
PushIRemove.Title = "Remove";
PushIPostureSubMenu = "Posture";
PushIPrefAngleSubMenu = "Preferred Angles";
PushIAngularLimitationSubMenu = "Limitations";
PushIAttachSubMenu = "Attach";
PushIReset.Title = "Reset";
PushIDelete.Title = "Delete";
PushIFixPermanent.Title = "Fix permanent";
PushIFixOnPermanent.Title = "Fix on permanent";

VisionNodeName = "Vision";

ActiveDocumentIsNotCATProduct =
"This command can only be started
from a document of type 'CATProduct'";

CannotInsertManikin = "This document cannot contain more than one manikin.";
SegmentMustBelongToHandOrFoot = "The selected segment must be a hand, a finger or a foot.";
SegmentMustBelongToHand = "The selected segment must be a hand or a finger";
ConfirmSave = "Do you want to save the changes ?";
ConfirmSave.Title = "Confirmation";
SaveAs.Title = "Save As";
TextFile.Title = "Text file (*.txt)";

DisableAttachConfirm.Title = "Confirmation";
DisableAttachConfirm = 
"This manikin possesses at least one active constraint
with an object that is attached (or picked) by the hand.
Do you want to deactivate the Attach (or Pick) ?

If you choose to deactivate the Attach or Pick,
it will be re-established when exiting the IK Mode.

To disable the attach, click Yes.
To keep the attach, click No.";

DeletePartRelationConfirm =
"There is at least one active part relation on this kinematics chain.
Do you want to delete this part relation ?

To delete the part relation, click Yes.
To keep the part relation active, click No.";

InternalError = "Internal Error.";
UnknownError = "Unknown Error.";
SegmentSelectionFailed = "The segment selection failed.";
LicenseRequired = "A license is required to perform that operation.";
RestrictedHBRMissing = "[Restricted]";
IsNotPartRelation = "The geometry associated with this constraint cannot be found.";

CATPartDocCreatedForOffsets =
"A new CATPart document,
/p1,
has been created.";

ManikinPartNumber = "Manikin";
ForearmPartNumber = "Forearm";

SWKConstraintNode = "Constraints";
SWKLoadNode = "Loads";
BodyNodeName = "Body";
SWKManikinPostureNode = "Posture";
SWKManikinPositionNode = "Position";
SWKManikinPropertiesNode = "Profiles";
SWKManikinSettingsNode = "Settings";
SWKPropDisplayNode = "Display";
SWKPropColouringNode = "Coloring";
SWKPropAppearanceNode = "Appearance";
SWKPropAttachesNode = "Attaches";
SWKPropReferentialNode = "Referential";
SWKPropIKBehavioursNode = "IK Behaviors";
SWKPropSegmentsOffsetsNode = "Offsets";
SWKLineOfSightNode = "Line of sight";
SWKPropAngLimsNode = "Angular Limitations";
SWKPropAngLimsDof1Node = "DOF 1";
SWKPropAngLimsDof2Node = "DOF 2";
SWKPropAngLimsDof3Node = "DOF 3";
SWKPropPrefAngsNode = "Preferred Angles";
SWKPropPrefAngsDof1Node = "DOF 1";
SWKPropPrefAngsDof2Node = "DOF 2";
SWKPropPrefAngsDof3Node = "DOF 3";
SWKReportsNode = "Reports";
SWKEnterNewReportPath = "Enter New Report Path";
SWKReportPathOutputFile = "Output File ";
BrowseButtonTitle = "Browse...";
TxtFilesOnly = "Only .txt, .xml or .html files are valid";

SegmentTrunkNodeName = "Spine";
SegmentLumbarSpinesNodeName = "Lumbar";
SegmentThoracicSpinesNodeName = "Thoracic";
SegmentNeckSpinesNodeName = "Head";
SegmentRightArmNodeName = "Right Arm";
SegmentRightFingersNodeName = "Right Fingers";
SegmentRightThumbNodeName = "Thumb";
SegmentRightIndexNodeName = "Index";
SegmentRightMiddleFingerNodeName = "Middle Finger";
SegmentRightAnnularNodeName = "Annular";
SegmentRightAuricularNodeName = "Auricular";
SegmentLeftArmNodeName = "Left Arm";
SegmentLeftFingersNodeName = "Left Fingers";
SegmentLeftThumbNodeName = "Thumb";
SegmentLeftIndexNodeName = "Index";
SegmentLeftMiddleFingerNodeName = "Middle Finger";
SegmentLeftAnnularNodeName = "Annular";
SegmentLeftAuricularNodeName = "Auricular";
SegmentRightLegNodeName = "Right Leg";
SegmentLeftLegNodeName = "Left Leg";

SelectionNull = "No objects selected.";

SWKManikinEditor.Title = "Manikin";
SWKProfileEditor.Title = "Profiles";
SWKSettingsEditor.Title = "Settings";
SWKManikinAppearanceTabPage.Title = "Appearance";
SWKManikinDisplayTabPage.Title = "Display";
SWKManikinColorTabPage.Title = "Coloring";
SWKManikinVisionTabPage.Title = "Vision";
SWKManikinAttachTabPage.Title = "Attach";
SWKManikinAnthroTabPage.Title = "Anthropometry";
SWKManikinReferentialTabPage.Title = "Referential";
SWKManikinIKBehaviorsTabPage.Title = "IK Behaviors";
SWKManikinSegmentsOffsetsTabPage.Title = "Offsets";
SWKManikinLimitationsTabPage.Title = "Angular Limitations";
SWKManikinPrefAngsTabPage.Title = "Preferred Angles";

SWKVisionEditor.Title = "Vision";
SWKSegmentsOffsetsEditor.Title = "Offsets";
SWKLimitationsEditor.Title = "Angular Limitations";
SWKPrefAngsEditor.Title = "Preferred Angles";
SWKManikinPropertiesEditor.Title = "Manikin";
SWKDisplayEditor.Title = "Display";
SWKColouringEditor.Title = "Coloring";
SWKAppearanceEditor.Title = "Appearance";
SWKIKBehavioursEditor.Title = "IK Behaviors";
SWKAttachesEditor.Title = "Attaches";
SWKReferentialEditor.Title = "Referential";
SWKAnthroEditor.Title = "Anthropometry";

ContextualSubMenu.OpenVision.Title = "Open Vision Window";
ContextualSubMenu.CloseVision.Title = "Close Vision Window";

InvalidInput =
"Invalid Input.
The forearm cannot be used with this command.";

NoMoreIKInThisMode=
"The Inverse Kinematics can no longer be run in this context.
To use the IK Mode on this manikin, you must first
activate (Edit) its parent product.";

DOFLockedMsg = "This DOF is currently locked.";

WrongActiveProductForIK =
"The Inverse Kinematics Mode cannot be applied to
this manikin because its parent product ('/p2')
is not active.

To use the IK Mode on manikin '/p1', you must first
activate (Edit) its parent product.";

CannotExitWorkbench =
"Failed to exit this workbench.
 The manikin cannot be found.";

Locked = "Locked";

NewLine = "\n";
OneSpaceWithNewLine = " \n";
Information = "Hide/Show Information";
InfoMessage = "The command Hide/Show is not available for this node";
NoEditingAvailable = "Editing operation is not available in this context.";
PICKConstraintNotEditable = "PICK constraint edition is not available in this context.";

Text = "Text Files (*.txt)";
Html = "Html Files (*.html)";
Xml  = "Xml Files (*.xml)";
All  = "All Files (*.*)";

ActivityNameDlg.Title = "Label for Update";
ActivityNameDlg.Label = "Please enter your label for this update:";

// --------------------------- CCP -------------------------------
OnlyOneLoadPasting = "Only one load per manikin can be selected for pasting actions.";
DNBHumanModelingUI.UndoTitle = "Exit Workbench";
