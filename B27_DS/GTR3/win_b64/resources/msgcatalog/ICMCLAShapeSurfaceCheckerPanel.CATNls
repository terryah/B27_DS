DialogBoxTitle                                   = "Surface Checker";

//Selection options-------------------------------------------------------------------------------------

LabelElements.Title                              = "Elements: ";
LabelElements.LongHelp                           = "Selects  the surfaces to be checked.";
LabelElements.ShortHelp                          = "Element selection";

//Topology tab------------------------------------------------------------------------------------------

TabPageTopology.Title                            = "Topology";

//Options frame-----------------------------------------------------------------------------------------

FrameOptions.Title                               = "Options";
FrameOptions.LongHelp                            = "Offers additional options.";

LabelSearchDistance.Title                        = "Search distance: ";
LabelSearchDistance.LongHelp                     = "All surfaces with a distance from each other 
which is smaller than specified tolerance are 
searched for. These surfaces are neighbored
building up a topology region.
For getting neighborhood information for 
surfaces, the topology regions within the 
specified tolerance are always searched for at 
first. All checks are calculated within these regions.
If no topology region can be found, all the other 
activated checks will produce no result. 
You will only get a check result for the position 
continuity, for example, if the tolerance value 
for the Search Distance is larger than the 
minimum tolerance value for G0.";	
LabelSearchDistance.ShortHelp                    = "Determination of the search distance";

ComboSearchDistance.Regular                      = "Rough";
ComboSearchDistance.Quick                        = "Fine";
ComboSearchDistance.User                         = "User";
ComboSearchDistance.LongHelp                     = "The following options are available:
- Rough: The search distance is pre-set to 0.1 mm.
- Fine: The search distance is pre-set to 0.001 mm.
- User: You can specify a value.";
ComboSearchDistance.ShortHelp                    = "Rough, Fine, User";

CheckButtonDisplayTopB.Title                     = "Display topology boundaries";
CheckButtonDisplayTopB.ShortHelp                 = "Display topology boundaries";

ColorButtonTopB.Title                            = "Color of topology boundaries";
ColorButtonTopB.ShortHelp                        = "Color of topology boundaries";

CheckButtonG0.Title                              = "G0";
CheckButtonG0.LongHelp                           = "Checks the surface edges within the 
topology regions found with the 
search distance for position continuity. ";
CheckButtonG0.ShortHelp                          = "Check for G0-Continuity";

CheckButtonG1.Title                              = "G1";
CheckButtonG1.LongHelp                           = "Checks the surface edges within the 
topology regions found with the 
search distance for normal continuity.";
CheckButtonG1.ShortHelp                          = "Check for G1-Continuity";

CheckButtonG2.Title                              = "G2";
CheckButtonG2.LongHelp                           = "Checks the surface edges within the 
topology regions found with the 
search distance for curvature continuity.";
CheckButtonG2.ShortHelp                          = "Check for G2-Continuity";

FrameContinuity.Title                            = "Continuity";
FrameContinuity.ShortHelp                        = "Check for G-Continuities";
FrameContinuity.LongHelp                         = "Check for G-Continuities";

FrameMin.Title                                   = "Min.";
FrameMin.ShortHelp                               = "Specification of Values for Min";
FrameMin.LongHelp                                = "Defines the lower value of the tolerance range.";

FrameMax.Title                                   = "Max.";
FrameMax.LongHelp                                = "Defines the upper value of the tolerance range.";
FrameMax.ShortHelp                               = "Specification of Values for Max";

Frame_ColorButtons.Title                         = "Colors";
Frame_ColorButtons.LongHelp                      = "Colors of the topology conditions.";
Frame_ColorButtons.ShortHelp                     = "Colors of the topology conditions.";

ColorButtonG0.Title                              = "G0";
ColorButtonG0.ShortHelp                          = "Color of the topology condition";

ColorButtonG1.Title                              = "G1";
ColorButtonG1.ShortHelp                          = "Color of the topology condition";

ColorButtonG2.Title                              = "G2";
ColorButtonG2.ShortHelp                          = "Color of the topology condition";

FrameResult.Title                                = "Results";
FrameResult.LongHelp                             = "Displays the number of check results 
found for the set options.";

LabelDensity.Title                               = "Density: ";
LabelDensity.LongHelp                            = "Defines the number of points at which the 
position, normal or curvature continuity 
is to be checked along an edge.";
LabelDensity.ShortHelp                           = "Definition of the Point Density";

SliderDensity.Title                              = "Density: ";
SliderDensity.LongHelp                           = "Via the slider bar it is possible to 
increase/decrease the value.";
SliderDensity.ShortHelp                          = "Value of Density";

FrameContinuity2.Title                           = "T-Conn.";
FrameContinuity2.LongHelp                        = "Check for T-Connections";
FrameContinuity2.ShortHelp                       = "Check for T-Connections"; 

CheckButtonT.Title                               = "T";
CheckButtonT.LongHelp                            = "Marks all T-connections in patch clusters where 
the misalignment between the corner points lies 
within the specified tolerance range. 
The regions where a misalignment between the 
selected topologically contiguous surfaces is 
analyzed are displayed in the graphics area 
as blue spheres. Each misalignment region is 
labeled with the associated radius.";
CheckButtonT.ShortHelp                           = "Check for T-Connections";

FrameMin2.Title                                  = "Min.";
FrameMin2.LongHelp                               = "Defines the lower value of the 
tolerance range for T-connections.";
FrameMin2.ShortHelp                              = "Specification of Values for Min";

FrameMax2.Title                                  = "Max.";
FrameMax2.LongHelp                               = "Defines the upper value of the 
tolerance range for T-connections.";
FrameMax2.ShortHelp                              = "Specification of Values for Max";

Frame_ColorButtons2.Title                        = "Color";
Frame_ColorButtons2.LongHelp                     = "Color of the T-Connections.";
Frame_ColorButtons2.ShortHelp                    = "Color of the T-Connections."; 

ColorButtonT.Title                               = "T";
ColorButtonT.LongHelp                            = "Specification of the color of the topology condition.";

FrameResult2.Title                               = "Results";
FrameResult2.LongHelp                            = "Displays the number of check results 
found for the set options.";

PushButtonCreateDisplaySet.Title                 = "Create Display Set";

//Geometry tab------------------------------------------------------------------------------------------

TabPageGeometry.Title                            = "Geometry";

CheckButtonMiniEdges.Title                       = "Mini edges: ";
CheckButtonMiniEdges.LongHelp                    = "Marks all edges with a shorter length 
than the specified value.";

ColorButtonMiniEdges.Title                       = "Mini edges: ";
ColorButtonMiniEdges.ShortHelp                   = "Color of the Mini Edges";

CheckButtonMaxOrder.Title                        = "Max. order: ";
CheckButtonMaxOrder.LongHelp                     = "Marks all elements with a higher order 
than the specified value.";

ColorButtonMaxOrder.Title                        = "Max. order: ";
ColorButtonMaxOrder.ShortHelp                    = "Color of the Max. Order";

CheckButtonMaxSegments.Title                     = "Max. segments: ";
CheckButtonMaxSegments.LongHelp                  = "Marks all objects with a larger number 
of segments than the specified value.";

ColorButtonMaxSegments.Title                     = "Max. segments: ";
ColorButtonMaxSegments.ShortHelp                 = "Color of the Max. Segments";

CheckButtonDupElements.Title                     = "Duplicate elements: ";
CheckButtonDupElements.LongHelp                  = "Marks all duplicate elements.
Elements with identical edges lying 
on top of each other are recognized. 
Moreover, patches lying within another 
patch are found via a tolerance, 
i. e. patches lying on top of each other, 
but whose edges are not identical. ";

ColorButtonDupElements.Title                     = "Duplicate Elements: ";
ColorButtonDupElements.ShortHelp                 = "Color of the Duplicate Elements";

CheckButtonMiniObjects.Title                     = "Mini objects: ";
CheckButtonMiniObjects.LongHelp                  = "Marks all objects with a smaller 
surface area than specified.";

ColorButtonMiniObjects.Title                     = "Mini objects: ";
ColorButtonMiniObjects.ShortHelp                 = "Color of the Mini Objects";

CheckButtonNormalTurnDown.Title                  = "Normal turn down";
CheckButtonNormalTurnDown.LongHelp               = "Finds irregularities of the normals, 
e. g. in the degenerated edge of a triangular patch. ";

ColorButtonNormalTurnDown.Title                  = "Normal turn down: ";
ColorButtonNormalTurnDown.ShortHelp              = "Color of the Normal Turn Down";

//Toplolgy AMM (alternative measurement methods) tab----------------------------------------------------

TabPageTopolgyAMM.Title                          = "G1/G2 Test";

FrameTopolgyAMMOptions.Title                     = "Options";
FrameTopolgyAMMOptions.LongHelp                  = "Determination of the following options
Measure Scale/G1/G2.";

LabelMeasureScale.Title                          = "Measure Scale: ";
LabelMeasureScale.LongHelp                       = "A scale needs to be specified in order to adapt
the measures to a certain use case.";
LabelMeasureScale.ShortHelp                      = "Determination of a measure scale";

FrameContinuityAMM.Title                         = "Continuity";
FrameContinuityAMM.ShortHelp                     = "Check for G-Continuities";
FrameContinuityAMM.LongHelp                      = "Check for G-Continuities";

CheckButtonG1AMM.Title                           = "G1";
CheckButtonG1AMM.LongHelp                        = "The surface edges within the search distance
will be checked for G1-Continuity.";
CheckButtonG1AMM.ShortHelp                       = "Check for G1-Continuity";
CheckButtonG2AMM.Title                           = "G2";
CheckButtonG2AMM.LongHelp                        = "The surface edges within the search distance
will be checked for G2-Continuity.";
CheckButtonG2AMM.ShortHelp                       = "Check for G2-Continuity";

FrameMinAMM.Title                                = "Min.";
FrameMinAMM.LongHelp                             = "Specification of values for Min";
FrameMaxAMM.Title                                = "Max.";
FrameMaxAMM.LongHelp                             = "Specification of values for Max";

FrameColorButtonsAMM.Title                       = "Colors";
FrameColorButtonsAMM.LongHelp                    = "Colors of the topology conditions.";
FrameColorButtonsAMM.ShortHelp                   = "Colors of the topology conditions.";

ColorButtonG1AMM.Title                           = "G1";
ColorButtonG1AMM.LongHelp                        = "Specification of the color of the G1 condition.";
ColorButtonG2AMM.Title                           = "G2";
ColorButtonG2AMM.LongHelp                        = "Specification of the color of the G2 condition.";

FrameResultAMM.Title                             = "Results";
FrameResultAMM.LongHelp                          = "Display the number of topology failed connections.";

