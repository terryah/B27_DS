//=============================================================================
//                                     CNEXT - CXRx
//                          COPYRIGHT DASSAULT SYSTEMES 2001 
//-----------------------------------------------------------------------------
// FILENAME    :    CATWguCommandHeader.CATRsc
// LOCATION    :    CATWGDiagramUI/CNext/resources/msgcatalog
// AUTHOR      :    ehh 
// DATE        :    October, 2001
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to SchematicPlatform
//                  WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATION     user  date      purpose
//   HISTORY       ----  ----      -------
//     01 	       

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Schematic Commands
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Build Create
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Build Component Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguBuildComponentHdr.Icon.Normal="I_Sch_BuildComponent";
CATWguCommandHdr.CATWguBuildComponentHdr.LongHelpId="CATWguCommandHdr.CATWguBuildComponentHdr";

//------------------------------------------------------------------------------
// Build Graphic Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguBuildGraphicHdr.Icon.Normal="I_Sch_BuildGraphic";
CATWguCommandHdr.CATWguBuildGraphicHdr.LongHelpId="CATWguCommandHdr.CATWguBuildGraphicHdr";

//------------------------------------------------------------------------------
// Build Connector Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguBuildConnectorHdr.Icon.Normal="I_Sch_BuildConnector";
CATWguCommandHdr.CATWguBuildConnectorHdr.LongHelpId="CATWguCommandHdr.CATWguBuildConnectorHdr";

//------------------------------------------------------------------------------
// Build Flow Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguBuildFlowHdr.Icon.Normal="I_Sch_BuildFlow";
CATWguCommandHdr.CATWguBuildFlowHdr.LongHelpId="CATWguCommandHdr.CATWguBuildFlowHdr";

//------------------------------------------------------------------------------
// Build Component Group Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguBuildCompGrpHdr.Icon.Normal="I_Sch_BuildCompGroup";
CATWguCommandHdr.CATWguBuildCompGrpHdr.LongHelpId="CATWguCommandHdr.CATWguBuildCompGrpHdr";

//------------------------------------------------------------------------------
// Build Component Group Internal Flow Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguBuildCompGrpFlowHdr.Icon.Normal="I_Sch_BuildIntFlowCompGrp";
CATWguCommandHdr.CATWguBuildCompGrpFlowHdr.LongHelpId="CATWguCommandHdr.CATWguBuildCompGrpFlowHdr";


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Design Create
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Place Component Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguPlaceComponentHdr.Icon.Normal="I_Sch_PlaceComponent";
CATWguCommandHdr.CATWguPlaceComponentHdr.LongHelpId="CATWguCommandHdr.CATWguPlaceComponentHdr";

//------------------------------------------------------------------------------
// Import Component Image Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguImageComponentHdr.Icon.Normal="I_Sch_ImageComponent";
// Comment out until documented
//CATWguCommandHdr.CATWguImageComponentHdr.LongHelpId="CATWguCommandHdr.CATWguImageComponentHdr";

//------------------------------------------------------------------------------
// Route Waveguide Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguRouteWaveguideLineHdr.Icon.Normal="I_Wgd_RouteWaveguide";
CATWguCommandHdr.CATWguRouteWaveguideLineHdr.LongHelpId="CATWguCommandHdr.CATWguRouteWaveguideLineHdr";


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Design Modify
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Rotate Component Left Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguRotateCompLeftHdr.Icon.Normal="I_Sch_RotateLeft";
CATWguCommandHdr.CATWguRotateCompLeftHdr.LongHelpId="CATWguCommandHdr.CATWguRotateCompLeftHdr";

//------------------------------------------------------------------------------
// Rotate Component Right Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguRotateCompRightHdr.Icon.Normal="I_Sch_RotateRight";
CATWguCommandHdr.CATWguRotateCompRightHdr.LongHelpId="CATWguCommandHdr.CATWguRotateCompRightHdr";

//------------------------------------------------------------------------------
// Flip Component Horizontal Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlipCompHorizHdr.Icon.Normal="I_Sch_FlipHorizontal";
CATWguCommandHdr.CATWguFlipCompHorizHdr.LongHelpId="CATWguCommandHdr.CATWguFlipCompHorizHdr";

//------------------------------------------------------------------------------
// Flip Component Vertical Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlipCompVertHdr.Icon.Normal="I_Sch_FlipVertical";
CATWguCommandHdr.CATWguFlipCompVertHdr.LongHelpId="CATWguCommandHdr.CATWguFlipCompVertHdr";

//------------------------------------------------------------------------------
// Flip Component In Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlipCompInLineHdr.Icon.Normal="I_Sch_FlipInline";
CATWguCommandHdr.CATWguFlipCompInLineHdr.LongHelpId="CATWguCommandHdr.CATWguFlipCompInLineHdr";

//------------------------------------------------------------------------------
// Reorient Component Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlipCompConnectionsHdr.Icon.Normal="I_Sch_ReorientComponent";
CATWguCommandHdr.CATWguFlipCompConnectionsHdr.LongHelpId="CATWguCommandHdr.CATWguFlipCompConnectionsHdr";

//------------------------------------------------------------------------------
// Scale Component Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguScaleCompHdr.Icon.Normal="I_Sch_ScaleComponent";
CATWguCommandHdr.CATWguScaleCompHdr.LongHelpId="CATWguCommandHdr.CATWguScaleCompHdr";

//------------------------------------------------------------------------------
// Replace Component Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguReplaceCompHdr.Icon.Normal="I_Sch_ReplaceComponent";
CATWguCommandHdr.CATWguReplaceCompHdr.LongHelpId="CATWguCommandHdr.CATWguReplaceCompHdr";

//------------------------------------------------------------------------------
// Align Component Horizontal Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguAlignCompHorizHdr.Icon.Normal="I_Sch_AlignHorizontal";
CATWguCommandHdr.CATWguAlignCompHorizHdr.LongHelpId="CATWguCommandHdr.CATWguAlignCompHorizHdr";

//------------------------------------------------------------------------------
// Align Component Vertical Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguAlignCompVertHdr.Icon.Normal="I_Sch_AlignVertical";
CATWguCommandHdr.CATWguAlignCompVertHdr.LongHelpId="CATWguCommandHdr.CATWguAlignCompVertHdr";

//------------------------------------------------------------------------------
// Translate Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguTranslateHdr.Icon.Normal="I_Sch_Translate";
CATWguCommandHdr.CATWguTranslateHdr.LongHelpId="CATWguCommandHdr.CATWguTranslateHdr";

//------------------------------------------------------------------------------
// Connect Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguConnectHdr.Icon.Normal="I_Sch_Connect";
CATWguCommandHdr.CATWguConnectHdr.LongHelpId="CATWguCommandHdr.CATWguConnectHdr";

//------------------------------------------------------------------------------
// Disconnect Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguDisconnectHdr.Icon.Normal="I_Sch_Disconnect";
CATWguCommandHdr.CATWguDisconnectHdr.LongHelpId="CATWguCommandHdr.CATWguDisconnectHdr";

//------------------------------------------------------------------------------
// Break Route Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguBreakRouteHdr.Icon.Normal="I_Sch_BreakRoute";
CATWguCommandHdr.CATWguBreakRouteHdr.LongHelpId="CATWguCommandHdr.CATWguBreakRouteHdr";

//------------------------------------------------------------------------------
// Close Route Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguCloseRouteHdr.Icon.Normal="I_Sch_CloseRoute";
CATWguCommandHdr.CATWguCloseRouteHdr.LongHelpId="CATWguCommandHdr.CATWguCloseRouteHdr";

//------------------------------------------------------------------------------
// Place Spec Break Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguSpecBreakHdr.Icon.Normal="I_Sch_SpecBreak";
//CATWguCommandHdr.CATWguSpecBreakHdr.LongHelpId="CATWguCommandHdr.CATWguSpecBreakHdr";

//------------------------------------------------------------------------------
// Place Datum Symbol Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguDatumSymbolHdr.Icon.Normal="I_Sch_DatumSymbol";
//CATWguCommandHdr.CATWguDatumSymbolHdr.LongHelpId="CATWguCommandHdr.CATWguDatumSymbolHdr";

//------------------------------------------------------------------------------
// FlexTube Route Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlexRouteHdr.Icon.Normal="I_Sac_FlexibleRoute";
//CATWguCommandHdr.CATWguFlexRouteHdr.LongHelpId="CATWguCommandHdr.CATWguFlexRouteHdr";

//------------------------------------------------------------------------------
// Flow Show Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlowShowHdr.Icon.Normal="I_Sch_FlowShowOn";
CATWguCommandHdr.CATWguFlowShowHdr.LongHelpId="CATWguCommandHdr.CATWguFlowShowHdr";

//------------------------------------------------------------------------------
// Flow Direction Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlowDirectionHdr.Icon.Normal="I_Sch_FlowDirection";
CATWguCommandHdr.CATWguFlowDirectionHdr.LongHelpId="CATWguCommandHdr.CATWguFlowDirectionHdr";


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Logical Line
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Create Logical Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguCreateLogLineHdr.Icon.Normal="I_Psp_CreateLogicalLine";
CATWguCommandHdr.CATWguCreateLogLineHdr.LongHelpId="CATWguCommandHdr.CATWguCreateLogLineHdr";

//------------------------------------------------------------------------------
// Select Logical Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguSelQryLogLineHdr.Icon.Normal="I_Psp_SelectLogicalLine";
CATWguCommandHdr.CATWguSelQryLogLineHdr.LongHelpId="CATWguCommandHdr.CATWguSelQryLogLineHdr";

//------------------------------------------------------------------------------
// Transfer Logical Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguTransferLogLineHdr.Icon.Normal="I_Psp_TransferLogicalLine";
CATWguCommandHdr.CATWguTransferLogLineHdr.LongHelpId="CATWguCommandHdr.CATWguTransferLogLineHdr";

//------------------------------------------------------------------------------
// Merge Logical Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguMergeLogLineHdr.Icon.Normal="I_Psp_MergeLineID";
CATWguCommandHdr.CATWguMergeLogLineHdr.LongHelpId="CATWguCommandHdr.CATWguMergeLogLineHdr";

//------------------------------------------------------------------------------
// Rename Logical Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguRenameLogLineHdr.Icon.Normal="I_Psp_RenameLogicalLine";
CATWguCommandHdr.CATWguRenameLogLineHdr.LongHelpId="CATWguCommandHdr.CATWguRenameLogLineHdr";

//------------------------------------------------------------------------------
// Delete Logical Line Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguDeleteLogLineHdr.Icon.Normal="I_Psp_DeleteLogicalLine";
CATWguCommandHdr.CATWguDeleteLogLineHdr.LongHelpId="CATWguCommandHdr.CATWguDeleteLogLineHdr";

//------------------------------------------------------------------------------
// Import/Update LineIDs Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguLineIDImportHdr.Icon.Normal="I_Psp_ImportLineID";
CATWguCommandHdr.CATWguLineIDImportHdr.LongHelpId="CATWguCommandHdr.CATWguLineIDImportHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Zone
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Create Zone Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguCreateZoneHdr.Icon.Normal="I_Psp_CreateZone";
CATWguCommandHdr.CATWguCreateZoneHdr.LongHelpId="CATWguCommandHdr.CATWguCreateZoneHdr";

//------------------------------------------------------------------------------
// Select Zone Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguSelectZoneHdr.Icon.Normal="I_Sac_SelectZone";
ATWguCommandHdr.CATWguSelectZoneHdr.LongHelpId="CATWguCommandHdr.CATWguSelectZoneHdr";

//------------------------------------------------------------------------------
// Rename Zone Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguRenameZoneHdr.Icon.Normal="I_Sac_RenameZone";
CATWguCommandHdr.CATWguRenameZoneHdr.LongHelpId="CATWguCommandHdr.CATWguRenameZoneHdr";

//------------------------------------------------------------------------------
// Delete Zone Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguDeleteZoneHdr.Icon.Normal="I_Sac_DeleteZone";
CATWguCommandHdr.CATWguDeleteZoneHdr.LongHelpId="CATWguCommandHdr.CATWguDeleteZoneHdr";

//------------------------------------------------------------------------------
// Define Zone Boundary Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguDefZoneBdryHdr.Icon.Normal="I_Sac_DefineZoneBndry";
CATWguCommandHdr.CATWguDefZoneBdryHdr.LongHelpId="CATWguCommandHdr.CATWguDefZoneBdryHdr";

//------------------------------------------------------------------------------
// Update Zone Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguUpdateZoneHdr.Icon.Normal="I_Sac_UpdateZone";
CATWguCommandHdr.CATWguUpdateZoneHdr.LongHelpId="CATWguCommandHdr.CATWguUpdateZoneHdr";


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// On/Off Sheet Connector
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Place On/Off Sheet Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguOnOffSheetHdr.Icon.Normal="I_Sch_OnOffSheet";
CATWguCommandHdr.CATWguOnOffSheetHdr.LongHelpId="CATWguCommandHdr.CATWguOnOffSheetHdr";

//------------------------------------------------------------------------------
// Query On/Off Sheet Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguOnOffQueryHdr.Icon.Normal="I_Sch_QueryOnOffSheet";
CATWguCommandHdr.CATWguOnOffQueryHdr.LongHelpId="CATWguCommandHdr.CATWguOnOffQueryHdr";

//------------------------------------------------------------------------------
// Link On/Off Sheet Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguOnOffLinkHdr.Icon.Normal="I_Sch_LinkOnOffSheet";
CATWguCommandHdr.CATWguOnOffLinkHdr.LongHelpId="CATWguCommandHdr.CATWguOnOffLinkHdr";

//------------------------------------------------------------------------------
// Unlink On/Off Sheet Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguOnOffUnlinkHdr.Icon.Normal="I_Sch_UnlinkOnOffSheet";
CATWguCommandHdr.CATWguOnOffUnlinkHdr.LongHelpId="CATWguCommandHdr.CATWguOnOffUnlinkHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// ID Management
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Rename ID Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguRenameIDHdr.Icon.Normal="I_Psp_Rename";
CATWguCommandHdr.CATWguRenameIDHdr.LongHelpId="CATWguCommandHdr.CATWguRenameIDHdr";

CATWguCommandHdr.CATWguCopyAttributesHdr.Icon.Normal="I_Psp_CopyPspAttributes";
CATWguCommandHdr.CATWguCopyAttributesHdr.LongHelpId="CATWguCommandHdr.CATWguCopyAttributesHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Logical Publication Management
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Manage Logical Publication Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguManageLogicalPubHdr.Icon.Normal="I_Psp_ManageLogPub";
CATWguCommandHdr.CATWguManageLogicalPubHdr.LongHelpId="CATWguCommandHdr.CATWguManageLogicalPubHdr";

//------------------------------------------------------------------------------
// Measure Tool
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguMeasureHdr.Icon.Normal="I_MeasureBetween";
CATWguCommandHdr.CATWguMeasureHdr.LongHelpId="CATWguCommandHdr.CATWguMeasureHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Part Selection
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Part Selection Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguPartSelectionHdr.Icon.Normal="I_Sac_PartSelection";
CATWguCommandHdr.CATWguPartSelectionHdr.LongHelpId="CATWguCommandHdr.CATWguPartSelectionHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Schematic View Pulldown Menu Items
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Show All Flow Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlowShowAllHdr.Icon.Normal="I_Sch_ShowAllFlowOn";
CATWguCommandHdr.CATWguFlowShowAllHdr.LongHelpId="CATWguCommandHdr.CATWguFlowShowAllHdr";

//------------------------------------------------------------------------------
// NoShow All Flow Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguFlowNoShowAllHdr.Icon.Normal="I_Sch_ShowAllFlowOff";
CATWguCommandHdr.CATWguFlowNoShowAllHdr.LongHelpId="CATWguCommandHdr.CATWguFlowNoShowAllHdr";

//------------------------------------------------------------------------------
// Show Gaps Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguGapShowAllHdr.Icon.Normal="I_Sch_ShowGapsOn";
CATWguCommandHdr.CATWguGapShowAllHdr.LongHelpId="CATWguCommandHdr.CATWguGapShowAllHdr";

//------------------------------------------------------------------------------
// Noshow Gaps Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguGapNoShowAllHdr.Icon.Normal="I_Sch_ShowGapsOff";
CATWguCommandHdr.CATWguGapNoShowAllHdr.LongHelpId="CATWguCommandHdr.CATWguGapNoShowAllHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Schematic Tools Pulldown Menu Items
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Report Definition Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguReportDefHdr.Icon.Normal="I_Rpu_ReportDefinition";
CATWguCommandHdr.CATWguReportDefHdr.LongHelpId="CATWguCommandHdr.CATWguReportDefHdr";

//------------------------------------------------------------------------------
// Report Generation Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguReportGenHdr.Icon.Normal="I_Rpu_ReportGeneration";
CATWguCommandHdr.CATWguReportGenHdr.LongHelpId="CATWguCommandHdr.CATWguReportGenHdr";

//------------------------------------------------------------------------------
// Report Expression Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguReportExpressionsHdr.Icon.Normal="I_Rpu_ReportExpression";
//CATWguCommandHdr.CATWguReportExpressionsHdr.LongHelpId="CATWguCommandHdr.CATWguReportExpressionsHdr";

//------------------------------------------------------------------------------
// Part Catalog Report Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATPspPartCatalogReportHdr.Icon.Normal="I_Psp_CatalogReport";
CATWguCommandHdr.CATPspPartCatalogReportHdr.LongHelpId="CATWguCommandHdr.CATPspPartCatalogReportHdr";

//------------------------------------------------------------------------------
// Export Query Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguExportQueryHdr.Icon.Normal="I_Rpu_ExportQuery";
//CATWguCommandHdr.CATWguExportQueryHdr.LongHelpId="CATWguCommandHdr.CATWguExportQueryHdr";

//------------------------------------------------------------------------------
// Export Data Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguDataExportHdr.Icon.Normal="I_Psp_ExportData";
//CATWguCommandHdr.CATWguDataExportHdr.LongHelpId="CATWguCommandHdr.CATWguDataExportHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Schematic Analyze Pulldown Menu Items
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Network Analysis Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguNetworkAnalHdr.Icon.Normal="I_AnalysePath";
CATWguCommandHdr.CATWguNetworkAnalHdr.LongHelpId="CATWguCommandHdr.CATWguNetworkAnalHdr";

//------------------------------------------------------------------------------
// Related Objects Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguRelatedObjectsHdr.Icon.Normal="I_RelatedObjects";
CATWguCommandHdr.CATWguRelatedObjectsHdr.LongHelpId="CATWguCommandHdr.CATWguRelatedObjectsHdr";

// THIS IS TEMPARARY UNTIL DRAFTING CREATES AND DEFINES THE NEW ATS ICON - PBR
//------------------------------------------------------------------------------
// Related Objects Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguATSTextPlaceHdr.Icon.Normal="I_DrwATSText";
CATWguCommandHdr.CATWguATSTextPlaceHdr.LongHelpId="CATWguCommandHdr.CATWguATSTextPlaceHdr";

//------------------------------------------------------------------------------
// Insert Existing View Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguInsertViewHdr.Icon.Normal="I_Sac_InsertExistingView";
CATWguCommandHdr.CATWguInsertViewHdr.LongHelpId="CATWguCommandHdr.CATWguInsertViewHdr";

//------------------------------------------------------------------------------
// HyperLink Commands
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguURLInsertCmdHdr.Icon.Normal="I_AddHyperLink";
//CATWguCommandHdr.CATWguURLInsertCmdHdr.LongHelpId="CATWguCommandHdr.CATWguURLInsertCmdHdr";

CATWguCommandHdr.CATWguURLOpenCmdHdr.Icon.Normal="I_GotoHyperLink";
//CATWguCommandHdr.CATWguURLOpenCmdHdr.LongHelpId="CATWguCommandHdr.CATWguURLOpenCmdHdr";

//------------------------------------------------------------------------------
// Text Update Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguTextUpdateHdr.Icon.Normal="I_Sch_UpdateText";
CATWguCommandHdr.CATWguTextUpdateHdr.LongHelpId="CATWguCommandHdr.TextUpdate";

//------------------------------------------------------------------------------
// Schematic Update Selection Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguSelectiveUpdateHdr.Icon.Normal="I_Update";
//CATWguCommandHdr.CATWguSelectiveUpdateHdr.LongHelpId="CATWguCommandHdr.CATWguSelectiveUpdateHdr";

//------------------------------------------------------------------------------
// Update Image Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguUpdateImageHdr.Icon.Normal="I_Sac_QueryUpdateMIO";
// Comment out until documented
//CATWguCommandHdr.CATWguUpdateImageHdr.LongHelpId="CATWguCommandHdr.CATWguUpdateImageHdr";

//------------------------------------------------------------------------------
// Schematic Connector Info Command
//------------------------------------------------------------------------------
CATWguCommandHdr.CATWguQueryCntrInfoHdr.Icon.Normal="I_Sch_QueryCntrInfo";
// Comment out until documented
//CATWguCommandHdr.CATWguQueryCntrInfoHdr.LongHelpId="CATWguCommandHdr.CATWguQueryCntrInfoHdr";

CATWguCommandHdr.CATWguImportRulesAndChecksHdr.Icon.Normal  = "I_Psp_ImportRulesandChecks";
// Comment out until documented
//CATTbuCommandHdr.CATWguImportRulesAndChecksHdr.LongHelpId   = "CATTbuCommandHdr.CATWguImportRulesAndChecksHdr";

CATWguCommandHdr.CATWguImportDittosHdr.Icon.Normal  = "I_Psp_ImportDittos";

//------------------------------------------------------------------------------
// Import Picture
//------------------------------------------------------------------------------
CATWguCommandHdr.DrwImportPicture.Icon.Normal="I_DrwImportPicture";
CATWguCommandHdr.DrwImportPicture.Icon.Focused="IF_DrwImportPicture";
CATWguCommandHdr.DrwImportPicture.Icon.Pressed="IP_DrwImportPicture";
CATWguCommandHdr.DrwImportPicture.LongHelpId="CATDrw.DrwImportPicture";

