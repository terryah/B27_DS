//
// ------------------------------------------------------------
// |
// |  NLS Message Catalog
// |  Framework: CATIAApplicationFrame
// |  Module : Search.m
// |
// ------------------------------------------------------------
//

// ------------------------------------------------------------
// | Special characters for Keyboard Input                  
// ------------------------------------------------------------
//
//  character to surround special characters
Quote                  = "'";


// ------------------------------------------------------------
// | Special names for Keyboard Input                  
// ------------------------------------------------------------
// For the following translations, the names of the commands, as well as the name for 
// the shortcuts MUST BE distinctive from each other. 
//
// In case of a clash, choose the name of the shortcut the shortest possible to be 
// differentiable
// Priority rule: The shortcut for Command has the highest priority level:
//   command -> c
//   color   -> col
// Priority rule: The shortcut for Search is independant from others:
//   search -> s
//   set    -> s
//
// Example: If 2 commands' names are plane and planification
//          choose plane and plani for the 2 associate shortcuts

//  no default prefix
None                  = "���";
//  last entered prefix
Last                  = "��������� ���������";

//  name to launch a command
Command               = "�������";
//  shortcut for this name
CommandShortcut       = "c";

//  name to launch a search through a favorite query
Favorite              = "���������";
//  shortcut for this name
FavoriteShortcut      = "f";

//  string to characterize the name of an object 
Name                  = "���";
//  shortcut for this string
NameShortcut          = "n";

//  string to characterize the name of an object as shown in the specification tree (graph)
NameInGraph           = "��� � �����";
//  shortcut for this string
NameInGraphShortcut   = "g";

//  string to characterize the type of an object 
Type                  = "���";
//  shortcut for this string
TypeShortcut         = "t";

//  string to characterize the selection set of an object 
Set              = "�����";
//  shortcut for this string
SetShortcut      = "s";

//  string to characterize the color of an object 
Color              = "����";
//  shortcut for this string
ColorShortcut      = "col";
//
ProblemInColorQuery = "�������� </P1> ����������� ��� �������� ����.";

//  string to characterize the layer of an object 
Layer              = "����";
//  shortcut for this string
LayerShortcut      = "l";

//  string to characterize the dashed type of an object (line)
Dashed              = "���������";
//  shortcut for this string
DashedShortcut      = "d";
//
ProblemInDashedQuery = "�������� </P1> ����������� ��� �������� ���������.";

//  string to characterize the thickness of an object (line)
Weight              = "���";
//  shortcut for this string
WeightShortcut      = "w";
//
ProblemInWeightQuery = "�������� </P1> ����������� ��� �������� ���.";

//  string to characterize the symbol type of an object (point)
Symbol              = "������";
//  shortcut for this string
SymbolShortcut      = "symb";
//
ProblemInWeightQuery = "�������� </P1> ����������� ��� �������� ���.";

// string to characterize the visibility of an object
Visibility             = "���������";
//  shortcut for this string
VisibilityShortcut     = "vis";

// Note for translation:
// The following couple of values is related to the visibility attribute of an object (from a data point of view).
// An object which has the "Visible" attribute may be visible but will not if one of its "parents" object is not visible.
// In other terms, Visible means "visualizable" and Hidden means "not visualizable",
// but users can have recorded theses values in favorite queries so one cannot change them.
Visible                = "�������";
Hidden                 = "�������";

// Note for translation:
// The following couple of values is related to the visibility of an object (from the display point of view).
// An object is told "Shown" if it is shown on the screen: the object as well as each of its parents have the "Visible" attribute.
// An object is told "Invisible" if it is not shown on the screen: the object or any of its parents have the "Hidden" attribute.
Shown                  = "������������";
Invisible              = "���������";
//
ProblemInVisibilityQuery = "�������� </P1> ����������� ��� �������� ���������.";


// ------------------------------------------------------------
// | Special names for the definition of the context,
// | for a Search carried out from the Keyboard Input                  
// ------------------------------------------------------------
// Rules: They are the same as previously, but the shortcuts can be a little longer
// ------   (ie, bot for to bottom)

//  string to characterize a Search context being everywhere 
Everywhere              = "�����";
//  shortcut for this Search context - Everywhere
EverywhereShortcut      = "���";

//  string to characterize a Search context in the element 
InElement               = "� /P1";
DefaultInElement        = "� ��������";
//  shortcut for this Search context - In the element
InElementShortcut       = "�����";

//  string to characterize a Search context from the element to the bottom
FromElement             = "�� /P1 �� �����";
DefaultFromElement      = "�� �������� �� �����";
//  shortcut for this Search context - From Element
FromElementShortcut     = "��";

//  string to characterize a Search context being from the last scope 
FromLastScope           = "� ��������� ������� ������";
//  shortcut for this Search context - From Last Scope
FromLastScopeShortcut   = "����������";

//  string to characterize a Search context being from the selection 
FromSelection           = "�� ��������� ���������";
//  shortcut for this Search context - From Selection
FromSelectionShortcut   = "sel";

//  string to characterize a Search context from the screen 
VisibleOnScreen         = "������������ �� ������";
//  shortcut for this Search context - Visible on screen
VisibleOnScreenShortcut = "scr";

//  string to characterize a Search context in the elements already found
InList             = "�� ������ ����������� ������";


// ------------------------------------------------------------
// | Special names for context options,
// | for a Search carried out from the Keyboard Input                  
// ------------------------------------------------------------

//  string to characterize the "Include Topology" context option 
TopologyShortcut      = "topo";
//  string to characterize the "Published elements only" context option 
PublicationsShortcut  = "pub";
//  string to characterize the "Deep Search" context option 
DeepSearchShortcut    = "deep";


// --------------------------------------------------------------------
// | Names of boolean for a Search carried out from the Keyboard Input                  
// --------------------------------------------------------------------
True  = "������";
False = "����";

//
// ------------------------------------------------------------
// |  Error Messages for  Power Input and Search command
// ------------------------------------------------------------
//
UnknownCommand     = "����������� �������: ";
//
UnavailableCommand = "����������� �������: ";
//
UnknownWorkbench   = "����������� ������: ";
//
UnknownType        = "����������� ���: ";
//
UnknownAttribute   = "����������� �������: ";
//
UnknownColor       = "����������� ����: ";
//
WrongColorNumber   = "������������ ����� �����: ";
//
WrongLayerNumber   = "������������ ����� ����: ";
//
SyntaxError        = "�������������� ������";
//
OddNumberOf        = "�������� ����������: ";
//
MissingClosingParenthesis = "����������� ���� ��� ��������� ����������� ������.";
//
MissingOpeningParenthesis = "����������� ���� ��� ��������� ����������� ������.";
//
BeforeOpeningParenthesis = "����� ( ����� ���� ������:\n\t\ta ( ��� �������� \n\t\tan.";
//
AfterOpeningParenthesis = "����� ( �� ����� ���� :\n\t\tany ) ��� �������� \n\t\tany.";
//
BeforeClosingParenthesis = "����� ) �� ����� ���� (. \n����� ) �� ����� ���� ��������.";
//
AfterClosingParenthesis = "����� ) ����� ���� ������:\n\t\ta ), �� �� �������� \n\t\ta � �� ����������� \n\t\ta."; 
//
OperatorsPosition = "� ����� ��� � ������ ������ �� ����� ���� ����������.";
//
DoubleOperator = "��� ��������� �� ����� ������ �����.";
//
MissingOperator = "������ ��� ������� ������ ��������� ��������.";
//
RefPRT = "������� ������� ������ ���� � ������������ ������.";
//
RefPBD = "������� ������� ������ ���� � ���� ������ ��� � ��������� ����.";
//
RefGST = "������� ������� ������ ���� � �������������� ������.";
//
RefOGS = "������� ������� ������ ���� � ������������� �������������� ������.";

//
// Error Panel Title
PowerInputMessage  = "��������� ��������� ������";
//
NoAttributeOnType = "� ���� </P1> �� ������ ���� ������� ��������.";
//
NoSuchAttributeOnType = "������� </P1> �� ���������� � �������� ���� </P2>.";
//
ErrorIn = "��������� ������ �����������: </P1>";
//
IntegerAttribute = 
"������� ������ ����. �������� ������� ��������� ���
����������� ���������� ������ � ������ </P1>.";
//
RealAttribute = 
"������� ������������� ����. �������� ����������� 
������ ��������� � ������ </P1>.";
//
BooleanAttribute = 
"������� ���� ����������. 
����������� ���������� ����� ���� ������: /P1, /P2, /P3, /P4.";
//
DimensionedAttribute = 
"������� ���������� ����. 
���������� ������� ������� ���������.";
//
WrongToleranceRange = 
"�������� �������� ��������. 
������ �������� ������ ���� ������ �������.";
//
WrongToleranceOperator = 
"��� ������ � �������� 
���������� ��������� � ������ = � !=.";
//
CharacterString = "������ ��������";
BooleanType     = "����������";
EnumerationType = "�������";
WrongOperatorForAttribute = 
"������� </P1> ��������� � ���� /P2, \n��� �������� ����������� ����������� �������� ������ = � !=.";
//
UserAttributeWarning = 
"��� ��� ������� </P1> �������� ����������������, 
������ �������.";
//
NoListOfPossibleValuesForEnumeration = "���������� ��������� ������ ��������� �������� \n��� �������� ���� ������� </P1>.";
InvalidValueForEnumeration = "������������ �������� </P1> ������� \n��� �������� ���� ������� </P2>.";


// ------------------------------------------------------------
// | Special names for Tools/Options,
// ------------------------------------------------------------

//  example : "c: as command:"
As                    = "���";

