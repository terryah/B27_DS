// COPYRIGHT DASSAULT SYSTEMES 2012

//===========================================================================
// Resource catalog for the CATDesCurvePaletteHeader class
// used in the CATDesEngravingCmd PaletteHeader command for the palette
//===========================================================================

CATDesHybridCmdPaletteHeader.ModificationHybridSubHdrCATNls.Title     = "IMA_Modification Mode";
CATDesHybridCmdPaletteHeader.ModificationHybridSubHdrCATNls.ShortHelp = "Modification";
CATDesHybridCmdPaletteHeader.ModificationHybridSubHdrCATNls.Help      = "Modification Mode";
CATDesHybridCmdPaletteHeader.ModificationHybridSubHdrCATNls.LongHelp  = "Select Sub Hybrid to be Modified.";

CATDesHybridCmdPaletteHeader.RemoveHybridSubHdrCATNls.Title     = "IMA_Remove Mode";
CATDesHybridCmdPaletteHeader.RemoveHybridSubHdrCATNls.ShortHelp = "Remove";
CATDesHybridCmdPaletteHeader.RemoveHybridSubHdrCATNls.Help      = "Remove Mode";
CATDesHybridCmdPaletteHeader.RemoveHybridSubHdrCATNls.LongHelp  = "Select Sub Hybrid to be Removed.";

CATDesHybridCmdPaletteHeader.RemoveAllHybridSubHdrCATNls.Title     = "IMA_Remove All";
CATDesHybridCmdPaletteHeader.RemoveAllHybridSubHdrCATNls.ShortHelp = "Remove All";
CATDesHybridCmdPaletteHeader.RemoveAllHybridSubHdrCATNls.Help      = "Remove All";
CATDesHybridCmdPaletteHeader.RemoveAllHybridSubHdrCATNls.LongHelp  = "Remove all hybrid features.";

CATDesHybridCmdPaletteHeader.CreationHybridSubDMHdrCATNls.Title     = "IMA_Creation Detail Modeling";
CATDesHybridCmdPaletteHeader.CreationHybridSubDMHdrCATNls.ShortHelp = "Creation Detail Modeling";
CATDesHybridCmdPaletteHeader.CreationHybridSubDMHdrCATNls.Help      = "Detail Modeling";
CATDesHybridCmdPaletteHeader.CreationHybridSubDMHdrCATNls.LongHelp  = "Creation Detail Modeling.";

CATDesHybridCmdPaletteHeader.CreationHybridSubCLHdrCATNls.Title     = "IMA_Creation Character Line";
CATDesHybridCmdPaletteHeader.CreationHybridSubCLHdrCATNls.ShortHelp = "Creation Character Line";
CATDesHybridCmdPaletteHeader.CreationHybridSubCLHdrCATNls.Help      = "Character Line";
CATDesHybridCmdPaletteHeader.CreationHybridSubCLHdrCATNls.LongHelp  = "Creation Character Line.";

CATDesHybridCmdPaletteHeader.CreationHybridSubMSHdrCATNls.Title     = "IMA_Creation Match Surface";
CATDesHybridCmdPaletteHeader.CreationHybridSubMSHdrCATNls.ShortHelp = "Creation Match Surface";
CATDesHybridCmdPaletteHeader.CreationHybridSubMSHdrCATNls.Help      = "Match Surface";
CATDesHybridCmdPaletteHeader.CreationHybridSubMSHdrCATNls.LongHelp  = "Creation Match Surface.";

CATDesHybridCmdPaletteHeader.PLMChooserHdrCATNls.Title     = "IMA_File Chooser";
CATDesHybridCmdPaletteHeader.PLMChooserHdrCATNls.ShortHelp = "File Chooser";
CATDesHybridCmdPaletteHeader.PLMChooserHdrCATNls.Help      = "File Chooser";
CATDesHybridCmdPaletteHeader.PLMChooserHdrCATNls.LongHelp  = "File Chooser for grey picture.";

CATDesHybridCmdPaletteHeader.SaveAndEditHdrCATNls.Title     = "IMA_Save & Edit Picture";
CATDesHybridCmdPaletteHeader.SaveAndEditHdrCATNls.ShortHelp = "Save & Edit Picture";
CATDesHybridCmdPaletteHeader.SaveAndEditHdrCATNls.Help      = "Save & Edit Picture";
CATDesHybridCmdPaletteHeader.SaveAndEditHdrCATNls.LongHelp  = "Save & Edit Picture.";

//CATDesHybridCmdPaletteHeader.SaveAndEditUnavailableHdrCATNls.Title     = "IMA_Save & Edit Picture  Unavailable";
//CATDesHybridCmdPaletteHeader.SaveAndEditUnavailableHdrCATNls.ShortHelp = "Save & Edit Picture Unavailable";
//CATDesHybridCmdPaletteHeader.SaveAndEditUnavailableHdrCATNls.Help      = "Save & Edit Picture Unavailable";
//CATDesHybridCmdPaletteHeader.SaveAndEditUnavailableHdrCATNls.LongHelp  = "Save & Edit Picture Unavailable.";

CATDesHybridCmdPaletteHeader.UpdateImageHdrCATNls.Title     = "IMA_Update Picture";
CATDesHybridCmdPaletteHeader.UpdateImageHdrCATNls.ShortHelp = "Update Picture";
CATDesHybridCmdPaletteHeader.UpdateImageHdrCATNls.Help      = "Update Picture";
CATDesHybridCmdPaletteHeader.UpdateImageHdrCATNls.LongHelp  = "Update with saved or original Picture.";

CATDesHybridCmdPaletteHeader.UpdateImageUnavailableHdrCATNls.Title     = "IMA_Update Picture Unavailable";
CATDesHybridCmdPaletteHeader.UpdateImageUnavailableHdrCATNls.ShortHelp = "Update Picture Unavailable";
CATDesHybridCmdPaletteHeader.UpdateImageUnavailableHdrCATNls.Help      = "Update Picture Unavailable";
CATDesHybridCmdPaletteHeader.UpdateImageUnavailableHdrCATNls.LongHelp  = "Update Unavailable, no saved or original Picture.";

CATDesHybridCmdPaletteHeader.ReverseUImageHdrCATNls.Title     = "IMA_Reverse U Picture";
CATDesHybridCmdPaletteHeader.ReverseUImageHdrCATNls.ShortHelp = "Reverse U";
CATDesHybridCmdPaletteHeader.ReverseUImageHdrCATNls.Help      = "Reverse U";
CATDesHybridCmdPaletteHeader.ReverseUImageHdrCATNls.LongHelp  = "Reverse U for grey picture.";

CATDesHybridCmdPaletteHeader.ReverseVImageHdrCATNls.Title     = "IMA_Reverse V Picture";
CATDesHybridCmdPaletteHeader.ReverseVImageHdrCATNls.ShortHelp = "Reverse V";
CATDesHybridCmdPaletteHeader.ReverseVImageHdrCATNls.Help      = "Reverse V";
CATDesHybridCmdPaletteHeader.ReverseVImageHdrCATNls.LongHelp  = "Reverse V for grey picture.";

CATDesHybridCmdPaletteHeader.ReverseNImageHdrCATNls.Title     = "IMA_Reverse Z Detail";
CATDesHybridCmdPaletteHeader.ReverseNImageHdrCATNls.ShortHelp = "Reverse Z";
CATDesHybridCmdPaletteHeader.ReverseNImageHdrCATNls.Help      = "Reverse Z";
CATDesHybridCmdPaletteHeader.ReverseNImageHdrCATNls.LongHelp  = "Reverse Z for grey picture.";

CATDesHybridCmdPaletteHeader.EditRealValueHdrCATNls.Title     = "IMA_Edit Amplitude Value";
CATDesHybridCmdPaletteHeader.EditRealValueHdrCATNls.ShortHelp = "Edit Amplitude";
CATDesHybridCmdPaletteHeader.EditRealValueHdrCATNls.Help      = "Edit Amplitude";
CATDesHybridCmdPaletteHeader.EditRealValueHdrCATNls.LongHelp  = "Edit Amplitude Detail Value.";

CATDesHybridCmdPaletteHeader.ZeroLevelColorHdrCATNls.Title     = "IMA_Zero Level Color";
CATDesHybridCmdPaletteHeader.ZeroLevelColorHdrCATNls.ShortHelp = "Color Zero / Detail";
CATDesHybridCmdPaletteHeader.ZeroLevelColorHdrCATNls.Help      = "Color Zero / Detail";
CATDesHybridCmdPaletteHeader.ZeroLevelColorHdrCATNls.LongHelp  = "Zero Level Color / Detail Level.";

CATDesHybridCmdPaletteHeader.DesactivateWZHdrCATNls.Help       = "Deactivate or activate the Working Zone.";
CATDesHybridCmdPaletteHeader.DesactivateWZHdrCATNls.LongHelp   = "Deactivate or activate the Working Zone.";
CATDesHybridCmdPaletteHeader.DesactivateWZHdrCATNls.ShortHelp  = "Working Zone Activation/Deactivation";
CATDesHybridCmdPaletteHeader.DesactivateWZHdrCATNls.Title      = "IMA_Working Zone Activation/Deactivation";
