//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2008
//=============================================================================
//
// Distance Analysis Command :
//   Resource file for NLS purpose. 
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// Fev. 08   Creation                                          Minh-Duc TRUONG
//=============================================================================

Title="Distance Analysis"; // title of distance analysis dialog box

// 1A) Selection 


MainFrmDistanceAnalysis.InputElementsFrame.Title = "Elements";
MainFrmDistanceAnalysis.InputElementsFrame.SourceSetLabel.Title = "Source "; // discretized element(s) 
MainFrmDistanceAnalysis.InputElementsFrame.TargetSetLabel.Title = "Target ";


MainFrmDistanceAnalysis.InputElementsFrame.SourceSetLabel.ShortHelp = "Set of source elements.";
MainFrmDistanceAnalysis.InputElementsFrame.SourceSetLabel.LongHelp = "Set of source elements (discretized elements).";

MainFrmDistanceAnalysis.InputElementsFrame.TargetSetLabel.ShortHelp = 
"Set of target elements.";
MainFrmDistanceAnalysis.InputElementsFrame.TargetSetLabel.LongHelp = "Set of Target Elements";



// For MultiSelector 

SourceSelectorList.ShortHelp = "Set of source elements.";
SourceSelectorList.LongHelp = "Set of source elements (discretized elements).";

TargetSelectorList.ShortHelp = "Set of target elements.";
TargetSelectorList.LongHelp = "Set of target Elements (non discretized elements).";

SourceSetListTitle.Message  = "Source set";

TargetSetListTitle.Message  = "Target set";





// 1B) Invert Analysis 
MainFrmDistanceAnalysis.InputElementsFrame.InvertAnalysisPush.Title="Invert Analysis";
MainFrmDistanceAnalysis.InputElementsFrame.InvertAnalysisPush.ShortHelp="Inverts the analysis computation.";
MainFrmDistanceAnalysis.InputElementsFrame.InvertAnalysisPush.LongHelp=
"Inverts the source and target sets.";








// 2)¨Projection Space
MainFrmDistanceAnalysis.FrmProjectionSpace.Title="Projection Space";     

MainFrmDistanceAnalysis.FrmProjectionSpace.Radio3D.ShortHelp=        "No Projection Of Elements";
MainFrmDistanceAnalysis.FrmProjectionSpace.Radio3D.LongHelp =        "No Projection Of Elements";

MainFrmDistanceAnalysis.FrmProjectionSpace.RadioX.ShortHelp=	     "Projection in X direction";
MainFrmDistanceAnalysis.FrmProjectionSpace.RadioX.LongHelp =
"Computes distance between projection of elements along the X direction.";

MainFrmDistanceAnalysis.FrmProjectionSpace.RadioY.ShortHelp=	     "Projection in Y direction";
MainFrmDistanceAnalysis.FrmProjectionSpace.RadioY.LongHelp =
"Computes distance between projection of elements along the Y direction.";

MainFrmDistanceAnalysis.FrmProjectionSpace.RadioZ.ShortHelp=	     "Projection in Z direction";
MainFrmDistanceAnalysis.FrmProjectionSpace.RadioZ.LongHelp =
"Computes distance between projection of elements along the Z direction.";

MainFrmDistanceAnalysis.FrmProjectionSpace.RadioCompass.ShortHelp=   "Projection in compass direction";
MainFrmDistanceAnalysis.FrmProjectionSpace.RadioCompass.LongHelp =
"Computes distance between projection of elements along the compass direction.";

MainFrmDistanceAnalysis.FrmProjectionSpace.PlanarDistRadio.ShortHelp= "Planar distance";
MainFrmDistanceAnalysis.FrmProjectionSpace.PlanarDistRadio.LongHelp=
"Computes distance between a mono-dim element
and the intersection of the plane
containing this element and the others elements";



// 3) Measurement Direction
MainFrmDistanceAnalysis.FrmMeasurementDirection.Title="Measurement Direction";     

MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.NormalRadio.ShortHelp="Normal distance";
MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.NormalRadio.LongHelp=
"Computes the minimum normal distances.";

MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioXMeasureDir.ShortHelp=	"Distance in X direction";   
MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioXMeasureDir.LongHelp=
"Computes distance along the X direction.";

MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioYMeasureDir.ShortHelp=	"Distance in Y direction";   
MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioYMeasureDir.LongHelp=
"Computes distance along the Y direction.";

MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioZMeasureDir.ShortHelp=	"Distance in Z direction";   
MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioZMeasureDir.LongHelp=
"Computes distance along the Z direction.";

MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioCompassMeasureDir.ShortHelp= "Distance in compass direction";  
MainFrmDistanceAnalysis.FrmMeasurementDirection.FrmMeasurementDirectionIcons.RadioCompassMeasureDir.LongHelp=
"Computes distance along the compass direction.";


// 4) Relimitation options 
            FrmRelimitation.Title="Relimitation";



            // Light Mode only
            FrmRelimitation.TitleLight="Options";
  			MainFrmDistanceAnalysis.FrmRelimitation.QuickColorScaleChk.ShortHelp="Limited color range";
			MainFrmDistanceAnalysis.FrmRelimitation.QuickColorScaleChk.LongHelp=
			"Displays the analysis with a limited color range
			for quick diagnosis.";



MainFrmDistanceAnalysis.FrmRelimitation.MaxDistChk.ShortHelp="User max distance";
MainFrmDistanceAnalysis.FrmRelimitation.MaxDistChk.LongHelp=
"Modification of the user max distance
( no computation above that distance)";


MainFrmDistanceAnalysis.FrmRelimitation.AutoTrapChk.Title="Automatic trap";
MainFrmDistanceAnalysis.FrmRelimitation.AutoTrapChk.ShortHelp="Automatic discretization points elimination";
MainFrmDistanceAnalysis.FrmRelimitation.AutoTrapChk.LongHelp="Authorizes automatic elimination of
irrelevant discretization points for 
target surface(s).";

MainFrmDistanceAnalysis.FrmRelimitation.LimitsPoints.Title="Curve Limits";
MainFrmDistanceAnalysis.FrmRelimitation.LimitsPoints.ShortHelp="Relimits the discretized curve.";
MainFrmDistanceAnalysis.FrmRelimitation.LimitsPoints.LongHelp="Relimits the discretized curve.";
 
MainFrmDistanceAnalysis.FrmRelimitation.LimitsPoints2.Title="Curve Limits";
MainFrmDistanceAnalysis.FrmRelimitation.LimitsPoints2.ShortHelp="Relimits the non discretized curve.";
MainFrmDistanceAnalysis.FrmRelimitation.LimitsPoints2.LongHelp="Relimits the non discretized curve.";

MainFrmDistanceAnalysis.FrmRelimitation.SignedValue.Title="Signed value";
MainFrmDistanceAnalysis.FrmRelimitation.SignedValue.ShortHelp="Signed value distance mode.";
MainFrmDistanceAnalysis.FrmRelimitation.SignedValue.LongHelp=
"Signed value distance mode
(when available).";









// 5) Discretization
MainFrmDistanceAnalysis.FrmDiscretisation.Title="Discretization";
MainFrmDistanceAnalysis.FrmDiscretisation.DiscretSlider.ShortHelp="Discretization value";
MainFrmDistanceAnalysis.FrmDiscretisation.DiscretSlider.LongHelp=
"Modification of the
discretization value
(not  available in texture
mapping mode)";




// 6) Display Options 
MainFrmDistanceAnalysis.FrmDisplayOptions.Title = "Display Options";

MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.QuickColorScaleChk.ShortHelp="Limited color range";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.QuickColorScaleChk.LongHelp=
"Displays the analysis with a limited color range
for quick diagnosis.";

MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.FullColorScaleChk.ShortHelp="Full color range";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.FullColorScaleChk.LongHelp=
"Displays the analysis with a full color range.";


MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.DisplayCombPointsChk.ShortHelp="Displays the colored points.";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.DisplayCombPointsChk.LongHelp=
"Displays the colored points.";

MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.TextureMappingModeChk.ShortHelp="Texture mapping mode.";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons.TextureMappingModeChk.LongHelp=
"Texture mapping mode.";

// 7) Display Option part 2 



MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.2DDiagramChk.ShortHelp="2D Diagram";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.2DDiagramChk.LongHelp=
"Displays the 2D diagram
(for mono-dim mono-cell element only).";



MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.ColorScaleStatChk.ShortHelp="Display statistics";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.ColorScaleStatChk.LongHelp=
"Displays the statistics within the color scale.";


MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.DisplayMinChk.ShortHelp="Min values";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.DisplayMinChk.LongHelp=
"Displays the minimum value locations.";

 
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.DisplayMaxChk.ShortHelp="Max values";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.DisplayMaxChk.LongHelp=
"Displays the maximum value locations.";

  
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.RunningPointChk.ShortHelp="Enables running point";
MainFrmDistanceAnalysis.FrmDisplayOptions.FrmDispIcons2.RunningPointChk.LongHelp=
"Enables a computation of the distance
between the point under the mouse
and the target element(s)."; 



// 7) Comb options
MainFrmDistanceAnalysis.FrmDisplayCombOptions.Title = "Comb Options ";


MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombScale.CombChk.ShortHelp="Displays the comb.";
MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombScale.CombChk.LongHelp=
"Displays the comb.";

  


MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.CombAutoScaleChk.Title="Auto scale";
MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.CombAutoScaleChk.ShortHelp=
"zoom-independent length.";
MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.CombAutoScaleChk.LongHelp=
"Displays the comb spikes with a zoom-independent length.";

MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.InvertCombChk.Title="Inverted";
MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.InvertCombChk.ShortHelp=
"Inverts the spikes.";
MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.InvertCombChk.LongHelp=
"Inverts the spikes.";

MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.CombEnvelopChk.Title="Envelope";
MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.CombEnvelopChk.ShortHelp=
"Envelope display.";
MainFrmDistanceAnalysis.FrmDisplayCombOptions.FrmCombOptionsIcons.CombEnvelopChk.LongHelp=
"Displays the polyline joining the tip of the comb spikes.";


MainFrmDistanceAnalysis.GlobalValuesDisplayFrame.Title = "Global Min\Max";
MainFrmDistanceAnalysis.GlobalValuesDisplayFrame.LongHelp = "Shows Global Min\Max distace values";
MainFrmDistanceAnalysis.GlobalValuesDisplayFrame.GlobalMaxTitleLable.Title = "Max: ";
MainFrmDistanceAnalysis.GlobalValuesDisplayFrame.GlobalMinTitleLable.Title = "Min: ";
GlobalMinMax.Message = "----";
