//=============================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwComboHeader
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// DATE        :    10.02.99
//------------------------------------------------------------------------------
// DESCRIPTION :    
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01		  fgx	  03.03.99	cles pour le NLS des unites
//     02           lgk   14.10.02  Revision
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Font Name
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwFontName.Title="Font Name" ;
CATDrwComboHeader.DrwFontName.ShortHelp="Font Name";
CATDrwComboHeader.DrwFontName.Help="Sets the font for a piece of text" ;
CATDrwComboHeader.DrwFontName.LongHelp =
"Font Name
Sets the font for a piece of text.";

//------------------------------------------------------------------------------
// Font Size
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwFontSize.Title="Font Size" ;
CATDrwComboHeader.DrwFontSize.ShortHelp="Font Size";
CATDrwComboHeader.DrwFontSize.Help="Sets the font size" ;
CATDrwComboHeader.DrwFontSize.LongHelp =
"Font Size
Sets the font size.";

//------------------------------------------------------------------------------
// Tolerance
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwTolerance.Title="Tolerance" ;
CATDrwComboHeader.DrwTolerance.ShortHelp="Tolerance";
CATDrwComboHeader.DrwTolerance.Help="Sets a tolerance on a dimension" ;
CATDrwComboHeader.DrwTolerance.LongHelp =
"Tolerance
Sets a tolerance on a dimension.";

//------------------------------------------------------------------------------
// Precision
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwPrecision.Title="Precision" ;
CATDrwComboHeader.DrwPrecision.ShortHelp="Precision";
CATDrwComboHeader.DrwPrecision.Help="Sets the precision used to display numerical values" ;
CATDrwComboHeader.DrwPrecision.LongHelp =
"Precision
Sets the precision used to display numerical values.";

//------------------------------------------------------------------------------
// Unit
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwUnit.Title="Unit" ;
CATDrwComboHeader.DrwUnit.ShortHelp="Unit";
CATDrwComboHeader.DrwUnit.Help="Sets dimension main unit" ;
CATDrwComboHeader.DrwUnit.LongHelp =
"Unit
Sets dimension main unit.";

//------------------------------------------------------------------------------
// Numerical Display
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwNumDisp.Title="Numerical Display Description" ;
CATDrwComboHeader.DrwNumDisp.ShortHelp="Numerical Display Description";
CATDrwComboHeader.DrwNumDisp.Help="Sets description of numerical display used for dimension main value" ;
CATDrwComboHeader.DrwNumDisp.LongHelp =
"Numerical Display Description
Sets description of numerical display 
used for dimension main value.";

//------------------------------------------------------------------------------
// Tolerance Type
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwTolDescrip.Title="Tolerance Description" ;
CATDrwComboHeader.DrwTolDescrip.ShortHelp="Tolerance Description";
CATDrwComboHeader.DrwTolDescrip.Help="Sets description of tolerance used for dimension main value" ;
CATDrwComboHeader.DrwTolDescrip.LongHelp =
"Tolerance Description
Sets description of tolerance 
used for dimension main value.";

//------------------------------------------------------------------------------
// Style
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwStyle.Title="Style" ;
CATDrwComboHeader.DrwStyle.ShortHelp="Style";
CATDrwComboHeader.DrwStyle.Help="Sets the style used to create a new object" ;
CATDrwComboHeader.DrwStyle.LongHelp =
"Style
Sets the style used to create a new object:
- Original: values predefined in the application, overloaded by toolbars values;
- User Defaults: user-defined values, overloaded by toolbars values;
- Only User Defaults: user-defined values, not overloaded.";
NORMAL="Original Properties";
USER_DEFAULT="User Default Properties";
ONLY_USER_DEFAULT="Only User Default Properties";

//------------------------------------------------------------------------------
// Keys for Units' NLS
//------------------------------------------------------------------------------
MM="mm";
INCH="inch";

DEG="Deg";
MIN="Deg-Min";
SEC="Deg-Min-Sec";
RAD="Rad";

NO_TOLERANCE="(no tolerance)";

