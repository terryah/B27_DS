// English Translation of the CATIA V5 Automotive BiW Fastening Application Attributes Names and Settings
// *******************************************************************************
//
//
// SECTION A: CATIA V5 BiW Fastener Application General Attributes
// *******************************************************************************
// 
// Attributes Names
// -------------------------------------------------------------------------------
PCATS             = "Cat�gorie de processus";
PTYPS             = "Type de processus"; 
//
// SECTION B:  CATIA V5 BiW Joint and BiW Joint Body Attributes 
// *******************************************************************************
// 
// Attributes Names
// -------------------------------------------------------------------------------
FELS  = "Nombre d'�l�ments pr�vus"; 
//
// Assembly Type Attributes Names
// -------------------------------------------------------------------------------
NTH  = "Nombre d'�paisseurs";
STY  = "Superposition";
ZNAM = "Identificateur de zone";
ZSUP = "Support";
ZHEM = "Ourlet";
ZMATNAM = "Mat�riau";
ZTH  = "Epaisseur";
ZORN = "Orientation";
ZCO1 = "Rev�tement1";
ZCO2 = "Rev�tement2";
CO1TH = "Epaisseur1";
CO2TH = "Epaisseur2";
//
// Edit/Search criteria
// -------------------------------------------------------------------------------
JPN   = "Nom du composant li�"; 
JCN   = "Nombre de composants li�s"; 
JID   = "Nom de la liaison BiW"; 
JBID  = "Nom du corps de liaison BiW"; 
SDF_ES  = "D�finition de la forme ";

//
// SECTION C: CATIA V5 BiW Fastener Attributes
// *******************************************************************************
//
// SECTION C-1- CATIA V5 BiW Fastener Life Cycle Attributes 
// ***********  ===================================================================
// 
// Attributes Names
// -------------------------------------------------------------------------------
JEID           = "ID";
STD            = "Standard";
U              = "Quelconque";
//
//
// SECTION C-2- CATIA V5 BiW Fastener Type  Attributes 
// ***********  ====================================================================
//
// Attributes Names
// -------------------------------------------------------------------------------
PCA  = "Cat�gorie de processus";
PTY  = "Type de processus";

TC1  = "Composant de projection";
TPN1 = "R�f�rence de la projection";
TZ1  = "Zone de projection"; 

//
//  Process Category Attribute Code Values 
// -------------------------------------------------------------------------------
PCA_WLD  = "Soudure";
PCA_ADH  = "Adh�sif";
PCA_SEA  = "Enduit d'�tanch�it�";
PCA_BWM  = "BiW Mechanical";
PCA_U = "Quelconque";
PTY_U = "Quelconque";
//
//
// Process Type Attribute Code Values 
// -------------------------------------------------------------------------------
// For Welding (ISO Code Values):
// ------------------------------
PTY_13   = "MIG (13)";
PTY_131  = "MIG (131)";
PTY_135  = "MAG (135)";
PTY_14   = "TIG (14)";
PTY_141  = "TIG (141)";
PTY_21   = "R�sistance (21)";
PTY_22   = "Jonction cylindrique (22)";
PTY_221  = "Soudure � recouvrement (221)";
PTY_222  = "Soudure par �crasement (222)";
PTY_23   = "R�sistance de projection (23)";   
PTY_24   = "Soudure en bout par �tincelage (24)";
PTY_25   = "Soudure par r�sistance (25)";
PTY_41   = "Soudure par ultrasons (41)";
PTY_42   = "Soudure par friction (42)";
PTY_52   = "Laser (52)";
PTY_78   = "Soudure par pression (78)";
PTY_783  = "Soudure � l'arc par pression (783)";
PTY_784  = "Soudure � l'arc par pression (784)";
PTY_84   = "D�coupe de poutre au laser (84)";
PTY_91   = "Brasage (91)";
PTY_93   = "Brasage (93)";
PTY_94   = "Soudure par brasage (94)";
//
PTY_UNSW = "Soudure quelconque";
PTY_UPRJ = "Projection quelconque";
//
// For Welding (ANSI Code Values):
// ------------------------------
PTY_SMAWNC = "SMAWC";
PTY_TIG    = "TIG";
PTY_RSW    = "R�sistance";
PTY_L      = "Laser";

//  For Adhesive:
// --------------
PTY_STR   = "Adh�sif structural";
PTY_NSTR  = "Adh�sif non structural";
PTY_UNSA  = "Adh�sif quelconque";

//  For Sealant:
// -------------
PTY_ROS   = "Soud� par un robot";
PTY_MAS   = "Soud� � la main";
PTY_UNSS  = "Enduit d'�tanch�it� quelconque";

// For BiW Mechanical:
// -------------------
PTY_CLI1  = "Rivetage Point 1";
PTY_CLI2  = "Rivetage Point 2";
PTY_R     = "Rivet";
PTY_PR    = "Rivet � pince";
PTY_BR    = "Rivet aveugle";
PTY_UNSM  = "BiW Mechanical quelconque";
//
//
// For Unspecified Process Type:
// -----------------------------
PTY_U1 = "Quelconque 1";
PTY_U2 = "Quelconque 2";
//
//
// Assembly Type Code Values
// -------------------------------------------------------------------------------
STY_LAP  = "Liaison � recouvrement";
STY_HEM  = "Liaison � bordure";
STY_UNSP = "Quelconque";
//
// SECTION C-3- CATIA V5 BiW Fastener Function  Attributes 
// ***********  ====================================================================
//
// Design & Engineering Attributes Names
// -------------------------------------------------------------------------------
//
ROB = "Classe de soudure";
REG = "Classe de liaison";
FIN = "Classe de finition";
//
// Manufacturing Attributes Names
// -------------------------------------------------------------------------------
GFL  = "Rep�re g�om�trique";
IFL  = "Rep�re d'inspection";
MID  = "Code de fabrication";
DISM = "M�thode de discr�tisation";
DISV = "Param�tre de discr�tisation";
//
// Design & Engineering  Function Attributes Code Values 
// -------------------------------------------------------------------------------
// For an attribute XXX, each NLS code value MUST BE BUILT BY CONCATENATION (UNDERSCORE SEPARATOR)
// of the ATTRIBUTE NAME CODE and the Matching  User Code values as specified  in the GbfDSStd.xls File.
// For example , the "Engineering Type" Attribute Code Name is "ENG", its possible values are:A,B,C,U as
// specified  in the CATIA Default GbfDefaultISOStd.xls File.
// By concatenation of "ENG" and "A" with _ separator we get The NLS Code string for value "A" is  "ENG_A"
//   
//
//
// ROB (Robustness) attribute Values
// ----------------------------------------
ROB_A = "Ordinaire";
ROB_B = "Critique";
ROB_C = "S�curit�";
ROB_D = "Fabrication";
ROB_E = "Commun";
ROB_F = "Delta";
ROB_G = "Contr�le";
ROB_H = "Non-structurel";
ROB_U = "Quelconque";
//
// REG (Regulation) attribute Values
// ----------------------------------------
REG_A = "Norme A";
REG_B = "Norme B";
REG_C = "Aucune";
REG_D = "Cat�gorie A";
REG_E = "Cat�gorie B";
REG_F = "Cat�gorie C";
REG_G = "Cons�quence - Classe 3";
REG_U = "Quelconque";
//
// FIN (Finish) attribute Values
// ----------------------------------------
FIN_A = "Classe A";
FIN_B = "Classe B";
FIN_C = "Classe C";
FIN_D = "Classe D";
FIN_E = "Finition - Classe 1";
FIN_F = "Finition - Classe 2";
FIN_G = "Finition - Classe 3";
FIN_H = "Finition - Classe 4";
FIN_I = "Finition - Classe 5";
FIN_J = "Finition - Classe 3FF";
FIN_U = "Quelconque";
//
// DISM (Discretization method) attributes values:
// --------------------------------------------------------------------------------
DISM_U    = "Quelconque";
DISM_SAG  = "Sag";
DISM_STEP = "Pas";
//
//
/// SECTION C-4-CATIA V5 BiW Fastener Shape Attributes 
// ************ ==================================================================
//
// Attributes Names
// --------------------------------------------------------------------------------
DIA = "Diam�tre";
OFS = "D�calage de la base du corps";
LEN = "Longueur du corps";
DIH = "Diam�tre de la t�te";
LEH = "Longueur de la t�te";
DIF = "Diam�tre du pied";
LEF = "Longueur du pied";
MAT = "Mat�riau";
SDF = "D�finition";
SRP = "Repr�sentation";
BAS = "Base";
HEIGHT = "Hauteur";
STH1 = "Epaisseur1";
STH2 = "Epaisseur2";
//
// MAT (Material) attributes Values:
// --------------------------------------------------------------------------------
MAT_WLDA = "Mat�riau de soudure 1";
MAT_WLDB = "Mat�riau de soudure 2";
MAT_ADHA = "Colle 1";
MAT_ADHB = "Colle 2";
MAT_SEAA = "Mat�riau d'�tanch�it� 1";
MAT_SEAB = "Mat�riau d'�tanch�it� 2";
MAT_U    = "Quelconque";
//

// SDF (Definition) attributes Values:
// --------------------------------------------------------------------------------
SDF_PT3 = "Point 3D";
SDF_HSP = "H�misph�re";
SDF_SH  = "Corps";
SDF_SHH = "Corps avec t�te";
SDF_SHF = "Corps avec t�te et pied";

SDF_CPH = "Trajet courbe";
SDF_CYL = "Tuyau cylindrique";
SDF_HCY = "Tuyau demi-cylindrique";
SDF_DMD = "Tuyau diamant";
SDF_HDM = "Tuyau demi diamant";
SDF_REC = "Tuyau rectangulaire";
SDF_HRE = "Tuyau demi-rectangulaire";

SDF_THS = "Surface fine";

// SRP (Representation) attributes Values:
// --------------------------------------------------------------------------------
SRP_SYM = "Symbolique";
SRP_WIR = "Filaire";
SRP_SUR = "Surfacique";
SRP_SOL = "DMU";
//
// Location Method and Support Spec
// --------------------------------------------------------------------------------
LOC     = "Emplacement";
SUP     = "Support";

// Parameters for BiW Fastener Functional Annotations 
USR      = "Valeur d�finie par l'utilisateur";
FN       = "[Nombre de rivets]";
J        = "Liaison BiW";
JB       = "Corps de liaison BiW";
JP       = "Pi�ces li�es";
LENGTH   = "Longueur";
JPIN      = "Noms d'instance des pi�ces li�es";
JZN      = "Noms des zones li�es";
JZTHKMAT = "Mat�riau/Epaisseur de la zone li�e"; 
