DialogBoxTitle="Analyse d'iso-courbe";

//Selection options------------------------------------------------------------------------------------

LabelElements.Title="El�ments : ";
LabelElements.LongHelp="S�lectionne les �l�ments utilis�s pour le calcul de l'analyse d'iso-courbe.";
LabelElements.ShortHelp="Surfaces � analyser";


//Distribution frame-----------------------------------------------------------------------------------

FrameDistribution.Title="Distribution";
FrameDistribution.LongHelp="D�finit la distribution des iso-courbes dans les directions u et v.
Le param�tre est pris individuellement dans chaque
 surface et ne prend pas en compte les surfaces adjacentes.";

LabelDirection.Title="Direction : ";
LabelDirection.LongHelp="";
LabelDirection.ShortHelp="Direction d'analyse";

ComboIsoCurvatureMode.LongHelp="- Le long de U -
  Affiche toutes les iso-courbes parall�lement � la
  direction U de chaque �l�ment de surface s�lectionn�.
- Le long de V -
  Affiche toutes les iso-courbes parall�lement � la
  direction V de chaque �l�ment de surface s�lectionn�.
- Le long de U et de V
  Affiche toutes les iso-courbes parall�lement aux
  directions U et V de chaque �l�ment de surface s�lectionn�.";
ComboIsoCurvatureMode.ShortHelp="L'analyse suit le param�tre s�lectionn�.";

IsoCurvatureMode.AlongU="Suivant U";
IsoCurvatureMode.AlongV="Suivant V";
IsoCurvatureMode.AlongUV="Suivant U et V";


//Number frame------------------------------------------------------------------------------------------

FrameNumber.Title="Nombre";
FrameNumber.LongHelp="D�finit le nombre d'iso-courbes dans la direction U et V.";

LabelNIsoLinesU.Title="U�: ";
LabelNIsoLinesU.LongHelp="Nombre d'iso-courbes V. L'analyse s'ex�cute dans la direction U.";
LabelNIsoLinesU.ShortHelp="Nombre d'iso-courbes suivant U";

LabelNIsoLinesV.Title="V : ";
LabelNIsoLinesV.LongHelp="Nombre d'iso-courbes U. L'analyse s'ex�cute dans la direction V.";
LabelNIsoLinesV.ShortHelp="Nombre d'iso-courbes suivant V";


//Range frame-------------------------------------------------------------------------------------------

FrameRange.Title="Borne";
FrameRange.LongHelp="Indique une plage pour le nombre sp�cifi� d'iso-courbes.
La plage d�finit, avec ses param�tres de d�but et de fin U/V,
la distribution d'iso-courbes et l'intervalle 
d'analyse de ces courbes.";

CheckButtonRange.LongHelp="- DESACTIVE -
  Distribue le nombre d�fini d'iso-courbes 
  dans la plage de param�tres 0 � 1.
- ACTIVE -
  Sp�cifie une plage de param�tres si le nombre d'iso-courbes 
  est sup�rieur � 1. Le nombre d'iso-courbes est distribu� 
  dans les m�mes proportions entre les valeurs de d�but et de fin du param�tre.";
CheckButtonRange.ShortHelp="Activer/D�sactiver la plage de param�tres";

LabelStart.Title="D�but";
LabelStart.LongHelp="Sp�cifie la position de la premi�re iso-courbe pour les directions U et V.";
LabelStart.ShortHelp="Param�tre de d�but de la plage";

LabelEnd.Title="Fin";
LabelEnd.LongHelp="Sp�cifie la position de la derni�re iso-courbe pour les directions U et V.";
LabelEnd.ShortHelp="Param�tre de fin de la plage";


//Options frame-----------------------------------------------------------------------------------------

FrameOptions.Title="Options";
FrameOptions.LongHelp="Offre les options suppl�mentaires";

CheckButtonBasicSurface.Title="Surface de support";
CheckButtonBasicSurface.LongHelp="Calcule l'analyse sur la surface de base en cas de faces.";
CheckButtonBasicSurface.ShortHelp="Analyse sur la surface de base";

CheckButtonShowUVAlignment.Title="Afficher l'alignement UV";
CheckButtonShowUVAlignment.LongHelp="Si une analyse d'iso-courbe est calcul�e sur plusieurs surfaces
disposant d'un flux de param�tres non uniforme, le r�sultat le long
de la direction U ou V ne sera pas affich� dans la m�me direction. 
La direction UV de chaque composant doit �tre align�e pour ajuster
la direction des r�sultats d'analyse.
Si l'option Afficher l'alignement UV est s�lectionn�e, un manipulateur va s'afficher
pour chaque surface des composants s�lectionn�s. Ces manipulateurs
indiquent l'alignement UV en cours, ainsi que la normale � la surface.
La direction UV peut �tre invers�e via un point de manipulateur.
au niveau du vecteur normal.";
CheckButtonShowUVAlignment.ShortHelp="Affiche les manipulateurs pour l'alignement UV";


//Display frame-----------------------------------------------------------------------------------------

FrameDisplay.Title="Affichage";
FrameDisplay.LongHelp="Offre des param�tres contr�lant l'affichage des pics d'analyse.";

CurvatureType.Curvature="Courbure";
CurvatureType.Radius="Rayon";

ComboCurvatureType.ShortHelp="Courbure ou rayon de courbure";
ComboCurvatureType.LongHelp="- Courbure -
  Les pics perpendiculaires � chaque courbe d'analyse afficheront
  les courbures (1/r). Plus le pic est long,
  plus la courbure est petite.
- Rayon - 
   Les pics perpendiculaires � chaque courbe d'analyse afficheront
   les rayons de courbure (r). Plus le pic est long,
   plus la courbure est grande.";

LabelDensity.Title="Densit� : ";
LabelDensity.LongHelp="D�finit le nombre de pics � distribuer sur la plage d�finie.";
LabelDensity.ShortHelp="Nombre de pics";

SliderDensity.LongHelp="D�finit le nombre de pics � distribuer sur la plage d�finie.";
SliderDensity.ShortHelp="Nombre de pics";

LabelScale.Title="Echelle�: ";
LabelScale.LongHelp="D�finit le facteur d'�chelle pour l'affichage des pics de courbure.";
LabelScale.ShortHelp="Longueur d'�chelle des pics";

SliderScale.LongHelp="D�finit le facteur d'�chelle pour l'affichage des pics de courbure.";
SliderScale.ShortHelp="Longueur d'�chelle des pics";


//Display Options frame---------------------------------------------------------------------------------

FrameDisplayOptions.Title="Options";
FrameDisplayOptions.LongHelp="Offre les options d'affichage de l'analyse.";

CheckButtonMin.Title="Min.";
CheckButtonMin.LongHelp="Affiche les valeurs de rayon minimum locales dans la zone d'exp�rience.";
CheckButtonMin.ShortHelp="Rayon minimum";

CheckButtonMax.Title="Max.";
CheckButtonMax.LongHelp="Affiche les valeurs de rayon maximum locales dans la zone d'exp�rience.";
CheckButtonMax.ShortHelp="Rayon maximal";

CheckButtonIndividualMinMax.Title="Individuel";
CheckButtonIndividualMinMax.LongHelp="Affiche les valeurs de rayon minimum et maximum
pour chaque surface individuelle de la zone d'exp�rience.";
CheckButtonIndividualMinMax.ShortHelp="Rayon minimum/maximum individuel";

CheckButtonReverse.Title="Inverser";
CheckButtonReverse.LongHelp="Inverse la direction normale des pics de courbure.";
CheckButtonReverse.ShortHelp="Inverse les pics";

LabelComb.Title="Peigne";
LabelComb.LongHelp="";
LabelComb.ShortHelp="Couleur du peigne";

ColorButtonComb.LongHelp="D�finit une couleur pour les lignes d'iso-courbes et le peigne.";
ColorButtonComb.ShortHelp="Couleur du peigne";

CheckButtonEnvelope.Title="Enveloppe";
CheckButtonEnvelope.LongHelp="Active/d�sactive l'enveloppe qui englobe le
peigne d'iso-courbure dans la direction U ou V.";
CheckButtonEnvelope.ShortHelp="Active/d�sactive l'affichage de l'enveloppe";

ColorButtonEnvelope.LongHelp="D�finit une couleur pour l'enveloppe.";
ColorButtonEnvelope.ShortHelp="Couleur de l'enveloppe";
