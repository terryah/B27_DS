Title="Usinage";

M3xHardSweepingMode="Style de trajet outil";
M3xHardDistinctMode="Style distinct dans les poches";
MachiningTolerance="Tol�rance d'usinage";
M3xHardMachiningMode="Mode d'usinage : ";
M3xHardCuttingMode="Mode de coupe";
M3xHardEngagementMode="Mode";
M3xStockContouring="Contournage de pi�ce";
M3xPocketProgressionMode="Progression h�lico�dale";
M3xStayOnBottom="Toujours rester sur le fond";
M3xHardMachiningModeByArea="Par zone";
M3xFMManagementMode="Gestion pleine mati�re";
M3xMaxFMDepthOfCut="Profondeur de coupe maximale du mat�riau";
M3xFMTrochoidMinRadDist="Rayon trocho�dal minimum";
M3xFMTrochoidMaxEngagement="Engagement max.";

M3xHardSweepingMode.LongHelp="Sp�cifie les mouvements de l'outil. Le style de trajet outil peut �tre:
- Aller simple: le trajet outil conserve toujours la m�me direction
    lors de passes successives et relie en diagonale la fin d'une passe au
    d�but de la suivante.
- Aller+ retour: le trajet outil conserve toujours la m�me direction
    mais repasse par le d�but de chaque passe avant d'atteindre
    le premier point de la passe suivante.   
- Zig-zag: le trajet outil change de direction � chaque passe d'outil.
- Spirale: le trajet outil est compos� de parall�les successives internes ou externes.
- Suivant contour: l'outil n'effectue que le contournage de la pi�ce.
- Concentrique: l'outil suit des arcs parall�les et concentriques ;
    le mode de coupe et la prise de passe sont constants,
- H�lico�dal: l'outil suit une spirale. ";


MachiningTolerance.LongHelp="Sp�cifie la distance maximum autoris�e entre le
trajet d'usinage th�orique et le trajet d'usinage calcul�.";

M3xHardMachiningMode1.LongHelp="Permet d'opter pour un usinage plan par plan ou zone par zone.";

M3xHardMachiningMode2.LongHelp="Permet d'opter pour l'usinage des poches seules, de la pi�ce ext�rieure seule ou des deux.";

M3xHardDistinctMode.LongHelp="Permet de d�finir un style de trajet outil sp�cifique pour les poches.";

M3xHardCuttingMode.LongHelp="Sp�cifie le mouvement de l'outil par rapport � la surface � traiter:
- En avalant: l'avant de l'outil (avan�ant dans le sens d'usinage) coupe d'abord dans le mat�riau.
- En opposition: l'arri�re de l'outil (avan�ant dans le sens d'usinage) coupe d'abord dans le mat�riau.
Pour le mouvement concentrique en mode Zig-Zag, la premi�re passe s'effectue dans le sens d'usinage s�lectionn� (sens standard),
la deuxi�me passe s'effectue dans l'autre sens d'usinage (sens inverse).";

M3xStockContouring.LongHelp="Ajoute une passe contournant la pi�ce.";

M3xPocketProgressionMode.LongHelp="Sp�cifie la direction de progression de l'outil dans une poche ou une zone externe.
Les modes possibles sont :
- Vers l'int�rieur: l'outil suit des trajets parall�les
� la limite vers l'int�rieur de la zone.
- Vers l'ext�rieur: l'outil suit des trajets parall�les
� la limite vers l'ext�rieur de la zone.
- Mixte: pour les poches, l'outil suit des trajets parall�les
� la limite vers l'ext�rieur de la zone;
pour les zones externes, l'outil suit des trajets parall�les
� la limite vers l'int�rieur de la zone.";

M3xStayOnBottom.LongHelp="Contraint l'outil � rester en contact avec le fond de la
poche pendant les transitions entre domaines d'usinage.";

M3xFMManagementMode.LongHelp="Permet de d�finir la fa�on de g�rer l'outil engag�.
- Aucune: aucune action particuli�re.
- Trocho�dale: remplacer le trajet en cours par un trajet trocho�dal.
- Multi-passes: ajouter des plans d'usinage pour r�duire la charge de coupe.";

M3xMaxFMDepthOfCut.LongHelp="Permet d'indiquer la distance entre les plans d'usinage dans des zones o� l'outil est engag�.";
M3xFMTrochoidMinRadDist.LongHelp="Permet de d�finir le rayon minimal des passes trocho�dales dans des zones o� l'outil est engag�.";
M3xFMTrochoidMaxEngagement.LongHelp="D�finit l'engagement du mouvement trocho�dal.";

M3xCollisionsWithToolAndToolHolder="V�rifier les collisions";
M3xCollisionsWithToolAndToolHolder.Help="V�rifie les collisions � l'aide de l'outil et du support de l'outil.";

M3xOffsetCollWithToolAndToolHolder="D�calage";
M3xOffsetCollWithToolAndToolHolder.Help="Donne un d�calage au support de l'outil.";

M3xIgnoreHole="Ignorer les trous sur le brut";
M3xIgnoreHole.LongHelp="Permet d'ignorer les petits trous sur le brut. Les trous ne sont plus ignor�s.";

M3xIgnoreHoleLength="Diam�tre";
M3xIgnoreHoleLength.LongHelp="Les trous sont suppos�s �tre des cercles. Cette longueur est le diam�tre du cercle.";

M3xChainingStrategy="Radiale d'abord";
M3xChainingStrategy.LongHelp="Sp�cifie que la premi�re strat�gie est radiale pour r�duire le vide.";

MfgZigZagMode="Mouvement concentrique";
MfgZigZagMode.LongHelp="Sp�cifie comment l'outil se d�place dans un mouvement concentrique:
- Zig-Zag: l'outil se d�place d'abord dans le sens d'usinage s�lectionn�, puis dans le sens inverse.
  Cette option r�duit les coupes hors mati�re.
- Sens unique: l'outil se d�place uniquement dans le sens d'usinage s�lectionn�.";

MfgZigZagRatio="Ratio radial de passe inverse";
MfgZigZagRatio.LongHelp="S'applique au mouvement concentrique Zig-Zag.
D�finit la profondeur radiale maximale de coupe utilis�e lors de passes dans le sens inverse.
Peut diff�rer de celle utilis�e dans le mode d'usinage s�lectionn�.";
