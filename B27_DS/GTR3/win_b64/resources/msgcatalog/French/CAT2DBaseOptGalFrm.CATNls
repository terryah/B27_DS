//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGal
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet General
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   14.01.1999  Ajout du check rotation snap auto
//      03           fgx   20.01.1999  Deplacement des options de la grid de geo a gal
//	    04		     ogk   16.06.1999  Ajout pour le coloring
//	    05		     ogk   29.10.1999  Ajout repere de vue
//=====================================================================================

Title="Informations g�n�rales";

frameRuler.HeaderFrame.Global.Title="R�gle ";
frameRuler.HeaderFrame.Global.LongHelp=
"R�gle
Indique si une r�gle doit �tre affich�e autour de l'espace de travail.";
frameRuler.IconAndOptionsFrame.OptionsFrame.checkRuler.Title="Afficher la r�gle ";
frameRuler.IconAndOptionsFrame.OptionsFrame.checkRuler.LongHelp=
"Afficher la r�gle Affiche ou cache la r�gle.";

frameGrid.HeaderFrame.Global.Title="Grille ";
frameGrid.HeaderFrame.Global.LongHelp=
"Grille
D�finit une grille permettant de dessiner de la g�om�trie ou d'aligner les vues.";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkGrid.Title="Affichage ";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkGrid.LongHelp=
"Afficher la grille.";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkSnap.Title="Points aimant�s ";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkSnap.LongHelp=
"Positionne les points cr��s sur la grille.";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridSpacing.Title="Espacement : ";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridSpacing.LongHelp=
"D�finit l'espacement principal � appliquer � la grille.";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridGraduation.Title="Subdivisions : ";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelGridGraduation.LongHelp=
"Applique un nombre donn� de subdivisions � chaque
espacement principal.";

frameGrid.IconAndOptionsFrame.OptionsFrame.checkAllowDistortions.Title="Disproportions H/V";
frameGrid.IconAndOptionsFrame.OptionsFrame.checkAllowDistortions.LongHelp="Disproportions H/V\nAutorise des subdivisions et des espacements diff�rents suivant H et V.";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelH.Title="H :";
frameGrid.IconAndOptionsFrame.OptionsFrame.labelV.Title="V :";

frameColor.HeaderFrame.Global.Title="Couleurs du fond (pour les dessins ant�rieurs � la version V5R14)";
frameColor.HeaderFrame.Global.LongHelp=
"Couleurs
D�finit les couleurs de fond des diff�rents calques (calques normaux et de d�tail).";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColor.Title="Calque :";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColor.LongHelp=
"Fond de calque
D�finit les couleurs de fond des calques normaux.";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColor.Title="D�grad� de couleur";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColor.LongHelp=
"Couleur d�grad�e
D�finit si la couleur de fond doit �tre d�grad�e ou non.";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColorDD.Title="Calque de d�tail :";
frameColor.IconAndOptionsFrame.OptionsFrame.labelBackgroundColorDD.LongHelp=
"Fond de calque de d�tail
D�finit les couleurs de fond des calques de d�tail.";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColorDD.Title="D�grad� de couleur";
frameColor.IconAndOptionsFrame.OptionsFrame.checkGraduatedColorDD.LongHelp=
"Couleur d�grad�e
D�finit si la couleur de fond doit �tre d�grad�e ou non.";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewer.viewer.Title="Preview:";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewer.viewer.LongHelp=
//"Preview
//Gives the feedback of the user selections.";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewerDD.viewerDD.Title="Preview:";
//frameColor.IconAndOptionsFrame.OptionsFrame.frameViewerDD.viewerDD.LongHelp=
//"Preview
//Gives the feedback of the user selections.";

frameTreeVisu.HeaderFrame.Global.Title="Arborescence";
frameTreeVisu.HeaderFrame.Global.LongHelp="Les options de visualisation de l'arborescence
vous permettent de sp�cifier si les param�tres et/ou les relations
doivent �tre affich�s dans l'arborescence.";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeParameters.Title="Afficher les param�tres";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeParameters.LongHelp=
"Afficher les param�tres
Affiche les param�tres utilis�s dans le dessin courant
dans le graphe.";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeFormulas.Title="Afficher les relations";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame._checkTreeFormulas.LongHelp=
"Afficher les relations
Affiche les relations utilis�es dans le dessin courant
dans le graphe.";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame.checkFeatUnderView.Title="Afficher les �l�ments sous les vues";
frameTreeVisu.IconAndOptionsFrame.OptionsFrame.checkFeatUnderView.LongHelp="Affiche les �l�ments sous les vues.";


frameViewReferential.HeaderFrame.Global.Title="Rep�re de vue";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewReferential.Title="Visualisation dans la vue active";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewReferential.LongHelp="Affiche ou non le rep�re de la vue courante.";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewRefZoom.Title="Zoomable";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.checkViewRefZoom.LongHelp="Zoomable
D�finit si le rep�re de la vue est zoomable comme la g�om�trie.";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.labelViewAxisSize.Title="Taille de r�f�rence :";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.labelViewAxisSize.LongHelp="Taille de r�f�rence
D�finit la taille de r�f�rence utilis�e pour afficher le rep�re de la vue.";
frameViewReferential.IconAndOptionsFrame.OptionsFrame.spinnerViewAxisSize.LongHelp="Taille de r�f�rence
D�finit la taille de r�f�rence utilis�e pour afficher le rep�re de la vue.";


frameAreaFill.HeaderFrame.Global.Title="Hachures";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.labelAreaFillTol.Title="Tol�rance sur l'ouverture des contours � hachurer";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.labelAreaFillTol.LongHelp="D�finit la tol�rance sur l'ouverture des contours � hachurer.";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.spinnerAreaFillTol.Title="Tol�rance sur l'ouverture des contours � hachurer";
frameAreaFill.IconAndOptionsFrame.OptionsFrame.spinnerAreaFillTol.LongHelp="D�finit la tol�rance sur l'ouverture des contours � hachurer.";


frmHideStartWkb.HeaderFrame.Global.Title="D�marrer atelier";
frmHideStartWkb.IconAndOptionsFrame.OptionsFrame.chkHideStartWkb.Title="Cacher la nouvelle bo�te de dialogue en d�marrant l'atelier";
frmHideStartWkb.IconAndOptionsFrame.OptionsFrame.chkHideStartWkb.LongHelp="Cacher la nouvelle bo�te de dialogue en d�marrant l'atelier.";


frmDrwUnits.HeaderFrame.Global.Title="Unit� du papier";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.lblPaperLengthUnit.Title="Longueur";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.lblPaperLengthUnit.LongHelp="Longueur.";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.cmbPaperLengthUnit.Title="Longueur";
frmDrwUnits.IconAndOptionsFrame.OptionsFrame.cmbPaperLengthUnit.LongHelp="Longueur.";
mmUnit="Millim�tre (mm)";
cmUnit="Centim�tre (cm)";
inUnit="Pouce (in)";
