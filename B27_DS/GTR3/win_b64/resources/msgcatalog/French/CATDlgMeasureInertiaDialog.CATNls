CommandModeDialogTitle="Mesure d'inertie";
EditModeDialogTitle="Mesure d'inertie (�dition)";

IndicateInertiaMsg="Indiquer l'entit� � mesurer";

IndicateAxisMsg="Indiquer l'axe � s�lectionner";

IndicatePtMsg="Indiquer le point � s�lectionner";

IndicateAxisSystemMsg="Indiquer le syst�me d'axe � s�lectionner";

SwitchMode="Passer au mode conception ou visualisation pour calculer une mesure d'inertie valide \n et tenir compte de la valeur de densit� correcte";

CancelButton="Fermer";

// Example usage:
//    "Point on Product.1"
//    "Line in Product.2"

OnLabel=" sur ";
InLabel=" dans ";

// Definition

DefFrame.SeparatorDefFrame.LabelDef.Title="D�finition ";
//MainFrame.DefFrame.SeparatorDefFrame.LabelDef.Title = "Definition ";

// Selection

DefFrame.SubDefFrame.SelectionFrame.LabelSelection.Title="S�lection :";
//MainFrame.DefFrame.SubDefFrame.SelectionFrame.LabelSelection.Title = "Selection :";
NoSelection="Pas de s�lection";

// Button

DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.Title="Mesure d'inertie 3D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.ShortHelp="Mesure d'inertie 3D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.Help="Mesure d'inertie d'un objet surfacique ou volumique";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.LongHelp="Mesure d'inertie d'un objet surfacique ou volumique";

DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.Title="Mesure d'inertie 2D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.ShortHelp="Mesure d'inertie 2D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.Help="Mesure d'inertie d'un objet plan";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.LongHelp="Mesure d'inertie d'un objet plan";

// Result

MainFrame.ResultFrame.SeparatorResultFrame.LabelResult.Title="R�sultat ";


MainFrame.ResultFrame.DescriptionFrame.CalculationModeFrame.InertiaCalculationMode.Title="Mode de calcul :  ";
MainFrame.ResultFrame.DescriptionFrame.TypeItemFrame.TypeItemLabel.Title="Type :  ";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.Title="Caract�ristiques";
MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.LongHelp="Donne l'aire, le volume, la densit� et la masse de l'objet s�lectionn�";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.AreaFrame_L.AreaLabel.Title="Aire";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.AreaFrame_L.AreaLabel.LongHelp    = "Area";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.VolumeFrame_L.VolumeLabel.Title="Volume";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.VolumeFrame_L.VolumeLabel.LongHelp  = "Volume";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.DensityFrame_L.DensityLabel.Title="Densit�";
MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.DensityFrame_L.DensityLabel.LongHelp="Donne la densit� ou la masse surfacique de l'�l�ment s�lectionn�. La densit� est celle du mat�riau appliqu� � la pi�ce.\nValeurs par d�faut : 1000 kg/m3 pour les volumes et 10 kg/m2 pour les surfaces.\nNon uniforme : les sous-produits ont des densit�s diff�rentes.";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.MassFrame_L.MassLabel.Title="Masse";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.MassFrame_L.MassLabel.LongHelp    = "Mass";

MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.Title="Centre de gravit� (G)";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.LongHelp="Donne les coordonn�s du centre de gravit�";


MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGxFrame_L.GXLabel.Title="Gx";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGyFrame_L.GYLabel.Title="Gy";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGzFrame_L.GZLabel.Title="Gz";


// CalculationMode

IdCalculationMode="Mode de calcul :  ";
IdCalculationModeResultExactThenApprox="Exact sinon approxim�";
IdCalculationModeResultExact="Exact";
IdCalculationModeResultApprox="Approxim�";

// type

SurfaceType="Chariotage";
VolumeType="Volume";

// density

SurfacicMass="Masse surfacique";
VolumicMass="Densit�";


//
// Item labels
// Enforce same length temporarily
//
Results.Title="R�sultats";

Results.IdentifierFrame.Title="Description";
Results.IdentifierFrame.LongHelp="Identifie l'objet mesur� et indique s'il s'agit d'un volume ou d'une surface";

Results.EquivalentFrame.Title="Equivalent";
Results.EquivalentFrame.LongHelp="D�finit si des �quivalents d'inertie (param�tres utilisateur import�s avec les produits de gestion des connaissances de CATIA) sont pris en compte. \n0 : la mesure s'effectue sur la s�lection, la g�om�trie ou l'assemblage. \n1 ou plus : 1 ou plusieurs �quivalents d'inertie sont pris en compte.";

Results.CharacCoFGFrame.CharacFrame.Title="Caract�ristiques";
Results.CharacCoFGFrame.CharacFrame.LongHelp="Donne l'aire, le volume, la densit� et la masse de l'objet s�lectionn�";

Results.CharacCoFGFrame.CharacFrame.AreaLabel_F.AreaLabel.Title="Aire";
//Results.CharacCoFGFrame.CharacFrame.AreaLabel_F.AreaLabel.LongHelp    = "Area";

Results.CharacCoFGFrame.CharacFrame.VolumeLabel_F.VolumeLabel.Title="Volume";
//Results.CharacCoFGFrame.CharacFrame.VolumeLabel_F.VolumeLabel.LongHelp  = "Volume";

Results.CharacCoFGFrame.CharacFrame.DensityLabel_F.DensityLabel.Title="Densit�";
Results.CharacCoFGFrame.CharacFrame.DensityLabel_F.DensityLabel.LongHelp="Donne la densit� ou la densit� surfacique de l'�l�ment s�lectionn�. La densit� est celle du mat�riau appliqu� � la pi�ce. \nValeurs par d�faut : 1000 kg/m3 pour les volumes et 10 kg/m2 pour les surfaces. \nNon uniforme : les sous-produits ont des densit�s diff�rentes.";

Results.CharacCoFGFrame.CharacFrame.MassLabel_F.MassLabel.Title="Masse";
//Results.CharacCoFGFrame.CharacFrame.MassLabel_F.MassLabel.LongHelp    = "Mass";


Results.CharacCoFGFrame.CenterOfGravityFrame.Title="Centre de gravit� (G)";
Results.CharacCoFGFrame.CenterOfGravityFrame.LongHelp="Donne les coordonn�s du centre de gravit�";

Results.MomentsFrame.Title="Moments principaux / G";
Results.MomentsFrame.LongHelp="Donne les principaux moments d'inertie calcul�s par rapport au centre de gravit�";

Results.AxesFrame.Title="Axes principaux";
Results.AxesFrame.LongHelp="Donne les principaux axes d'inertie";

Results.InertiaFrame.Title="Matrice d'inertie / G";
Results.InertiaFrame.LongHelp="Donne la matrice d'inertie par rapport au centre de gravit�";

Results.AxisFrame.Title="Moment / axe";
Results.AxisFrame.LongHelp="Donne les caract�ristiques et le vecteur directeur de l'axe s�lectionn� ainsi que le moment d'inertie (Ma) et le rayon de giration (R)";

Results.Inertia_O_Frame.Title="Matrice d'inertie / O";
Results.Inertia_O_Frame.LongHelp="Donne la matrice d'inertie par rapport � l'origine du document";

Results.Inertia_P_Frame.Title="Matrice d'inertie / P";
Results.Inertia_P_Frame.LongHelp="Donne la matrice d'inertie par rapport � un point et les coordonn�es du point s�lectionn�";

Results.Inertia_AS_Frame.Title="Matrice d'inertie / syst�me d'axe A";
Results.Inertia_AS_Frame.LongHelp="Donne la matrice d'inertie par rapport au syst�me d'axe, l'origine et les vecteurs du syst�me d'axe s�lectionn�";



// SpacerLabel enables offset equivalent to X or Y or Z
SpacerLabel="   ";


AreaLabel="Aire";
VolumeLabel="Volume";
DensityLabel="Densit�";
MassLabel="Masse";

DensityInfoLabel="  Valeur par d�faut";
DensityInfoLabelEmpty="               ";

MaterialLabel="Mat�riaux";
MaterialLabelDefault="D�faut";


// ----------- Inertia Center

Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GXLabel.Title="Gx";
Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GYLabel.Title="Gy";
Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GZLabel.Title="Gz";

GXLabel="Gx";
GYLabel="Gy";
GZLabel="Gz";


// ----------- Principal Moments

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.Title="Moments principaux / G";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.LongHelp="Donne les principaux moments d'inertie calcul�s par rapport au centre de gravit�";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M1_F.M1Label.Title="M1";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M2_F.M2Label.Title="M2";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M3_F.M3Label.Title="M3";


Results.MomentsFrame.DataFrame.M1Label.Title="M1";
Results.MomentsFrame.DataFrame.M2Label.Title="M2";
Results.MomentsFrame.DataFrame.M3Label.Title="M3";


M1Label="M1";
M2Label="M2";
M3Label="M3";

// ----------- Principals Axes

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.Title="Axes principaux";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.LongHelp="Donne les principaux axes d'inertie";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1X_F.A1XLabel.Title="A1x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1Y_F.A1YLabel.Title="A1y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1Z_F.A1ZLabel.Title="A1z";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2X_F.A2XLabel.Title="A2x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2Y_F.A2YLabel.Title="A2y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2Z_F.A2ZLabel.Title="A2z";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3X_F.A3XLabel.Title="A3x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3Y_F.A3YLabel.Title="A3y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3Z_F.A3ZLabel.Title="A3z";


Results.AxesFrame.DataFrame.A1XLabel.Title="A1x";
Results.AxesFrame.DataFrame.A1YLabel.Title="A1y";
Results.AxesFrame.DataFrame.A1ZLabel.Title="A1z";

Results.AxesFrame.DataFrame.A2XLabel.Title="A2x";
Results.AxesFrame.DataFrame.A2YLabel.Title="A2y";
Results.AxesFrame.DataFrame.A2ZLabel.Title="A2z";

Results.AxesFrame.DataFrame.A3XLabel.Title="A3x";
Results.AxesFrame.DataFrame.A3YLabel.Title="A3y";
Results.AxesFrame.DataFrame.A3ZLabel.Title="A3z";

A1XLabel="A1x";
A1YLabel="A1y";
A1ZLabel="A1z";

A2XLabel="A2x";
A2YLabel="A2y";
A2ZLabel="A2z";

A3XLabel="A3x";
A3YLabel="A3y";
A3ZLabel="A3z";

// ----------- Inertia Matrix

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.Title="Inertie / G";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.LongHelp="Donne la matrice d'inertie par rapport au centre de gravit�";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.Title="Matrice d'inertie / G";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXX_F.IXXLabel.Title="IoxG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IYY_F.IYYLabel.Title="IoyG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IZZ_F.IZZLabel.Title="IozG";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXY_F.IXYLabel.Title="IxyG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXZ_F.IXZLabel.Title="IxzG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IYZ_F.IYZLabel.Title="IyzG";

Results.InertiaFrame.DataFrame.IXXLabel.Title="IoxG";
Results.InertiaFrame.DataFrame.IXYLabel.Title="IxyG";
Results.InertiaFrame.DataFrame.IXZLabel.Title="IxzG";
Results.InertiaFrame.DataFrame.IYXLabel.Title="IyxG";
Results.InertiaFrame.DataFrame.IYYLabel.Title="IoyG";
Results.InertiaFrame.DataFrame.IYZLabel.Title="IyzG";
Results.InertiaFrame.DataFrame.IZXLabel.Title="IzxG";
Results.InertiaFrame.DataFrame.IZYLabel.Title="IzyG";
Results.InertiaFrame.DataFrame.IZZLabel.Title="IozG";

IXXLabel="IoxG";
IYYLabel="IoyG";
IZZLabel="IozG";
IXYLabel="IxyG";
IXZLabel="IxzG";
IYZLabel="IyzG";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.Title="Inertie / O";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.LongHelp="Donne la matrice d'inertie par rapport � l'origine du document";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.Title="Matrice d'inertie / O";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXX_O_F.IXXLabel.Title="IoxO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IYY_O_F.IYYLabel.Title="IoyO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IZZ_O_F.IZZLabel.Title="IozO";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXY_O_F.IXYLabel.Title="IxyO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXZ_O_F.IXZLabel.Title="IxzO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IYZ_O_F.IYZLabel.Title="IyzO";

Results.Inertia_O_Frame.DataFrame.IXXLabel.Title="IoxO";
Results.Inertia_O_Frame.DataFrame.IXYLabel.Title="IxyO";
Results.Inertia_O_Frame.DataFrame.IXZLabel.Title="IxzO";
Results.Inertia_O_Frame.DataFrame.IYXLabel.Title="IyxO";
Results.Inertia_O_Frame.DataFrame.IYYLabel.Title="IoyO";
Results.Inertia_O_Frame.DataFrame.IYZLabel.Title="IyzO";
Results.Inertia_O_Frame.DataFrame.IZXLabel.Title="IzxO";
Results.Inertia_O_Frame.DataFrame.IZYLabel.Title="IzyO";
Results.Inertia_O_Frame.DataFrame.IZZLabel.Title="IozO";

IOXXLabel="IoxO";
IOYYLabel="IoyO";
IOZZLabel="IozO";
IOXYLabel="IxyO";
IOXZLabel="IxzO";
IOYZLabel="IyzO";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.Title="Inertie / P";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.LongHelp="Donne la matrice d'inertie par rapport � un point et les coordonn�es du point s�lectionn�";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.Title="Matrice d'inertie / P";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXX_P_F.IXXLabel.Title="IoxP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IYY_P_F.IYYLabel.Title="IoyP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IZZ_P_F.IZZLabel.Title="IozP";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXY_P_F.IXYLabel.Title="IxyP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXZ_P_F.IXZLabel.Title="IxzP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IYZ_P_F.IYZLabel.Title="IyzP";


Results.Inertia_P_Frame.P_I1_Editor_F.IXXLabel.Title="IoxP";
Results.Inertia_P_Frame.P_I1_Editor_F.IYYLabel.Title="IoyP";
Results.Inertia_P_Frame.P_I1_Editor_F.IZZLabel.Title="IozP";

Results.Inertia_P_Frame.P_I2_Editor_F.IXYLabel.Title="IxyP";
Results.Inertia_P_Frame.P_I2_Editor_F.IXZLabel.Title="IxzP";
Results.Inertia_P_Frame.P_I2_Editor_F.IYZLabel.Title="IyzP";


IPXXLabel="IoxP";
IPYYLabel="IoyP";
IPZZLabel="IozP";
IPXYLabel="IxyP";
IPXZLabel="IxzP";
IPYZLabel="IyzP";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.Title="Inertie / syst�me d'axe";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.LongHelp="Donne la matrice d'inertie par rapport au syst�me d'axe, l'origine et les vecteurs du syst�me d'axe s�lectionn�";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.Title="Matrice d'inertie / syst�me d'axe A";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXX_AS_F.IXXLabel.Title="IoxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IYY_AS_F.IYYLabel.Title="IoyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IZZ_AS_F.IZZLabel.Title="IozA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXY_AS_F.IXYLabel.Title="IxyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXZ_AS_F.IXZLabel.Title="IxzA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IYZ_AS_F.IYZLabel.Title="IyzA";

Results.Inertia_AS_Frame.AS_I1_Editor_F.IXXLabel.Title="IoxA";
Results.Inertia_AS_Frame.AS_I1_Editor_F.IYYLabel.Title="IoyA";
Results.Inertia_AS_Frame.AS_I1_Editor_F.IZZLabel.Title="IozA";

Results.Inertia_AS_Frame.AS_I2_Editor_F.IXYLabel.Title="IxyA";
Results.Inertia_AS_Frame.AS_I2_Editor_F.IXZLabel.Title="IxzA";
Results.Inertia_AS_Frame.AS_I2_Editor_F.IYZLabel.Title="IyzA";


IAXXLabel="IoxA";
IAYYLabel="IoyA";
IAZZLabel="IozA";
IAXYLabel="IxyA";
IAXZLabel="IxzA";
IAYZLabel="IyzA";


// ----------- Axis System

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.Title="Syst�me d'axes";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OX_AS_F.OX_AS_Label.Title="OxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OY_AS_F.OY_AS_Label.Title="OyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OZ_AS_F.OZ_AS_Label.Title="OzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UX_AS_F.UX_AS_Label.Title="UxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UY_AS_F.UY_AS_Label.Title="UyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UZ_AS_F.UZ_AS_Label.Title="UzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VX_AS_F.VX_AS_Label.Title="VxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VY_AS_F.VY_AS_Label.Title="VyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VZ_AS_F.VZ_AS_Label.Title="VzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WX_AS_F.WX_AS_Label.Title="WxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WY_AS_F.WY_AS_Label.Title="WyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WZ_AS_F.WZ_AS_Label.Title="WzA";





Results.Inertia_AS_Frame.AS_O_Editor_F.OX_AS_Label.Title="OxA";
Results.Inertia_AS_Frame.AS_O_Editor_F.OY_AS_Label.Title="OyA";
Results.Inertia_AS_Frame.AS_O_Editor_F.OZ_AS_Label.Title="OzA";

OX_AS_Label="OxA";
OY_AS_Label="OyA";
OZ_AS_Label="OzA";

Results.Inertia_AS_Frame.AS_U_Editor_F.UX_AS_Label.Title="UxA";
Results.Inertia_AS_Frame.AS_U_Editor_F.UY_AS_Label.Title="UyA";
Results.Inertia_AS_Frame.AS_U_Editor_F.UZ_AS_Label.Title="UzA";

UX_AS_Label="UxA";
UY_AS_Label="UyA";
UZ_AS_Label="UzA";

Results.Inertia_AS_Frame.AS_V_Editor_F.VX_AS_Label.Title="VxA";
Results.Inertia_AS_Frame.AS_V_Editor_F.VY_AS_Label.Title="VyA";
Results.Inertia_AS_Frame.AS_V_Editor_F.VZ_AS_Label.Title="VzA";

VX_AS_Label="VxA";
VY_AS_Label="VyA";
VZ_AS_Label="VzA";

Results.Inertia_AS_Frame.AS_W_Editor_F.WX_AS_Label.Title="WxA";
Results.Inertia_AS_Frame.AS_W_Editor_F.WY_AS_Label.Title="WyA";
Results.Inertia_AS_Frame.AS_W_Editor_F.WZ_AS_Label.Title="WzA";

WX_AS_Label="WxA";
WY_AS_Label="WyA";
WZ_AS_Label="WzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.SelectAxisSytemAS.AxisSystem.Title="S�lection syst�me d'axe";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.SelectAxisSytemAS.AxisSystem.LongHelp="Cliquer ici puis s�lectionner un syst�me d'axe dans l'arbre des sp�cifications. \nLes calculs suivants tiennent compte du syst�me d'axe s�lectionn�. \nPour changer de syst�me d'axe, cliquer la case de nouveau et s�lectionner un nouveau syst�me d'axe";

Results.Inertia_AS_Frame.AS_Button_F.AxisSystem.Title="S�lection syst�me d'axe";
Results.Inertia_AS_Frame.AS_Button_F.AxisSystem.LongHelp="Cliquer ici puis s�lectionner un syst�me d'axe dans l'arbre des sp�cifications. \nLes calculs suivants tiennent compte du syst�me d'axe s�lectionn�. \nPour changer de syst�me d'axe, cliquer la case de nouveau et s�lectionner un nouveau syst�me d'axe";

AxisSystem="S�lectionner un syst�me d'axes";
AxisSystemNoSelection="Pas de s�lection";
AxisSystemUnknown="Syst�me d'axe V4";

AxisSystemDeleted="Effac� ou non valu�";

AxisSystemNotValidSelection="Cette s�lection n'est pas valide, car le syst�me d'axe\nne se trouve pas dans la pi�ce activ�e";


// ----------- Axis

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OX_A_F.OXAxisLabel.Title="Ox";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OY_A_F.OYAxisLabel.Title="Oy";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OZ_A_F.OZAxisLabel.Title="Oz";

Results.AxisFrame.A_O_Editor_F.OXAxisLabel.Title="Ox";
Results.AxisFrame.A_O_Editor_F.OYAxisLabel.Title="Oy";
Results.AxisFrame.A_O_Editor_F.OZAxisLabel.Title="Oz";

OXAxisLabel="Ox";
OYAxisLabel="Oy";
OZAxisLabel="Oz";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DX_A_F.DXAxisLabel.Title="Dx";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DY_A_F.DYAxisLabel.Title="Dy";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DZ_A_F.DZAxisLabel.Title="Dz";

Results.AxisFrame.A_D_Editor_F.DXAxisLabel.Title="Dx";
Results.AxisFrame.A_D_Editor_F.DYAxisLabel.Title="Dy";
Results.AxisFrame.A_D_Editor_F.DZAxisLabel.Title="Dz";

DXAxisLabel="Dx";
DYAxisLabel="Dy";
DZAxisLabel="Dz";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.SelectAxisA.ButtonAxisA.Title="S�lectionner l'axe";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.SelectAxisA.ButtonAxisA.LongHelp="Cliquer ici puis s�lectionner un axe dans la zone g�om�trique. \nLes calculs suivants tiennent compte de l'axe s�lectionn�. \nPour changer d'axe, cliquer la case de nouveau et s�lectionner un autre axe";


Results.AxisFrame.A_Button_F.Axis.Title="S�lectionner l'axe";
Results.AxisFrame.A_Button_F.Axis.LongHelp="Cliquer ici puis s�lectionner un axe dans la zone g�om�trique. \nLes calculs suivants tiennent compte de l'axe s�lectionn�. \nPour changer d'axe, cliquer la case de nouveau et s�lectionner un autre axe";

Axis="S�lectionner l'axe";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.Title="Inertie / Axe";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.LongHelp="Donne les caract�ristiques et le vecteur directeur de l'axe s�lectionn� ainsi que le moment d'inertie (Ma) et le rayon de giration (R)";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.Title="Moment / axe";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.MomentAFrame_L.MomentALabel.Title="Ma ";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.RadiusMomentAFrame_L.RadiusMomentALabel.Title="Rayon ";



Results.AxisFrame.A_A_Editor_F.AxisMomentLabel.Title="Ma ";
AxisMomentLabel="Ma ";

Results.AxisFrame.A_A_Editor_F.RadiusMomentLabel.Title="Rayon ";
RadiusMomentLabel="Rayon ";

// ----------- Bounding Box

BBOXLabel="BBOx";
BBOYLabel="BBOy";
BBOZLabel="BBOz";

BBLXLabel="BBLx";
BBLYLabel="BBLy";
BBLZLabel="BBLz";

// -------------
MainFrame.ButtonFrame.CreateGeometry.Title="Cr�er la g�om�trie";
MainFrame.ButtonFrame.Customise.Title="Personnaliser...";
MainFrame.ButtonFrame.KeepState.Title="Garder la mesure";
MainFrame.ButtonFrame.ExportButton.Title="Export";
MainFrame.ButtonFrame.OnlyMainBodies.Title="corps principaux uniquement";
MainFrame.MeasShownElemsFrame.MeasShownElemsCheckButton.Title="Mesurer uniquement les �l�ments visibles";

Results.CustomiseFrame.CreateGeometry.Title="Cr�er la g�om�trie";
Results.CustomiseFrame.Customise.Title="Personnaliser...";
Results.CustomiseFrame.KeepState.Title="Garder la mesure";
Results.CustomiseFrame.ExportButton.Title="Export";
Results.CustomiseFrame.OnlyMainBodies.Title="corps principaux uniquement";
Results.MeasShownElemsFrame.MeasShownElemsCheckButton.Title="Mesurer uniquement les �l�ments visibles";

// ------------- Point

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OXPt_F.OXPtLabel.Title="Px";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OYPt_F.OYPtLabel.Title="Py";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OZPt_F.OZPtLabel.Title="Pz";

Results.Inertia_P_Frame.P_P_Editor_F.OXPtLabel.Title="Px";
Results.Inertia_P_Frame.P_P_Editor_F.OYPtLabel.Title="Py";
Results.Inertia_P_Frame.P_P_Editor_F.OZPtLabel.Title="Pz";

OXPtLabel="Px";
OYPtLabel="Py";
OZPtLabel="Pz";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.SelectPointP.ButtonPointP.Title="S�lection point";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.SelectPointP.ButtonPointP.LongHelp="Cliquer ici puis s�lectionner un point dans la zone g�om�trique. \nLes calculs suivants tiennent compte du point s�lectionn�. \nPour changer de point, cliquer la case de nouveau et s�lectionner un autre point";


Results.Inertia_P_Frame.P_Button_F.Pt.Title="S�lection point";
Results.Inertia_P_Frame.P_Button_F.Pt.LongHelp="Cliquer ici puis s�lectionner un point dans la zone g�om�trique. \nLes calculs suivants tiennent compte du point s�lectionn�. \nPour changer de point, cliquer la case de nouveau et s�lectionner un autre point";
Pt="S�lectionnez un point";

// ---------------- Equivalent

MainFrame.ResultFrame.EquivalentFrame.EquivalentFrame_L.EquivalentLabel.Title="Equivalent";

Results.EquivalentFrame.DataFrame.EQUIVLabel.Title="        ";

Equivalent="Equivalent";


// --------------- Error

PtNotify="Erreur";
AxisNotify="Erreur";
AxisSysNotify="Erreur";

SelectionNotValid="S�lection non valide, s�lectionner un autre composant";


Warning="Avertissement";

NotSelectionAxis="Cette s�lection n'est pas un axe";
NotSelectionPt="Cette s�lection n'est pas un point";
NotSelectionAxisSystem="Cette s�lection n'est pas un syst�me d'axe";

NotValidSelectionNotify="S�lection non valide";



DensityNotValid="Densit� incorrecte, entrer une autre valeur";
DensityTooSmall="Densit� trop faible, elle doit �tre sup�rieure � ";

Information="Informations";
MeasureInertiaDelete="La Mesure d'Inertie n'est plus valide car la s�lection n'existe plus.";
MeasureUpdateNotPossible="La mise � jour de la mesure n'est pas possible car la mesure n'est pas associative.";



//Factory

InertiaVolume="Volume d'inertie";
InertiaSurface="Surface d'inertie";
Inertia2DSurface="Surface 2D d'inertie";

//HIX - Restrict Measure When CATAnalysis is opened with Product as Active.
MeasureInertiaInCATAnalysis="Pour effectuer des mesures dans le produit, ouvrez-le dans une nouvelle fen�tre.";
