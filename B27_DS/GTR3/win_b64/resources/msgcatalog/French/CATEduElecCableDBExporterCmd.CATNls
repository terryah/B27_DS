//=============================================================================
//                                     CNEXT - CXR5
//                          COPYRIGHT DASSAULT SYSTEMES 2004 
//-----------------------------------------------------------------------------
// FILENAME    :    CATEduElecCableDBExporterCmd.CATNls
// FRAMEWORK   :    CATEduUI
// AUTHOR      :    ehh
// DATE        :    September 2004
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Interactive
//                  Plant Ship Commands.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATION     user  date      purpose
//   HISTORY       ----  ----      -------
//------------------------------------------------------------------------------


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// DialogEngine state prompts 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CATEduElecCableDBExporterCmd.SelectWPToExportState.Message = "S�lectionnez les espaces de travail � importer dans la base de donn�es Electrical Cable (sur le panneau)";
CATEduElecCableDBExporterCmd.DisplayImportResultsState.Message = "Cliquez sur FERMER ou sur Visualiser le rapport";
CATEduElecCableDBExporterCmd.ViewReportState.Message = "Cliquez sur FERMER ou sur Enregistrer sous lorsque vous avez termin� la consultation du rapport";
CATEduElecCableDBExporterCmd.SaveReportState.Message = "S�lectionnez le r�pertoire dans lequel enregistrer le rapport";

NoDBConnectionText="Vous avez �t� d�connect� de la base de donn�es. Reconnectez-vous pour pouvoir utiliser cette commande.";
NoElecWPText="Aucun espace de travail Electrical n'a �t� d�tect�.

V�rifiez que l'arborescence PRC est d�velopp�e et affiche les espaces de travail appropri�s.";
CommandTitle="Importation de la base de donn�es Electrical Cable"; 
NoWpSelectedText="S�lectionnez au moins un espace de travail � importer";
ImportingPOPWPsText="Les espaces de travail s�lectionn�s pour l'importation appartiennent tous, ou une partie seulement, � une situation
'Product On Product' (c.-�-d. un PRC imbriqu�). Ces espaces de travail peuvent uniquement �tre import�s
en acc�dant directement aux PRC associ�s. Acc�dez directement aux PRC parent imm�diats
et r�importez les espaces de travail correspondant.";

ImportResultsTitle = "R�capitulatif des r�sultats de l'importation";
ImportResultsText = "L'importation de /P1 espaces de travail sur /P2 a r�ussi.";

ImportReportTitle = "Base de donn�es Electrical Cable : Rapport d'importation";
ImportReportDate = "Date : /P1";
ImportReportTime = "Heure : /P1";
ImportReportBodyPRCTitle = "PRC : /P1";
ImportReportBodyWPTitle = "Espaces de travail trait�s :";
ImportReportSuccessfulWP = "'/P1' - IMPORTATION REUSSIE";
ImportReportFailedWP = "'/P1' - ECHEC DE L'IMPORTATION";
ImportReportObjectLevelError = "Erreur de traitement pour l'objet '/P1' : /P2";
ImportReportWPLevelError = "Erreur relative au traitement des espaces de travail : /P1";
ImportReportWPLevelWarning = "Avertissement relatif au traitement des espaces de travail : /P1";
ImportReportInternalError = "Erreur interne : /P1";

ProgressTaskUI.Title = "Importation des espaces de travail dans la base de donn�es";
//ProgressTaskUI.ObjectName = "Electrical Cable Database";
//ProgressTaskUI.Comment = "Processing work packages";
ProgressTaskUI.CommentRuntime = "Traitement de l'espace de travail /P1 de /P2";

EduExportERR_0001.Request = "Chargement du document";
EduExportERR_0001.Diagnostic = "Impossible de charger le document dans la session";
EduExportERR_0001.Advice = "Veuillez le signaler � l'administrateur de la base de donn�es";
EduExportERR_0002.Request = "Suppression de document";
EduExportERR_0002.Diagnostic = "Impossible de supprimer le document de la session";
EduExportERR_0002.Advice = "Veuillez le signaler � l'administrateur de la base de donn�es";
EduExportERR_0003.Request = "Insertion des donn�es de l'espace de travail";
EduExportERR_0003.Diagnostic = "Impossible de cr�er un enregistrement d'espace de travail dans la base de donn�es";
EduExportERR_0003.Advice = "Veuillez le signaler � l'administrateur de la base de donn�es";
EduExportERR_0004.Request = "Enregistrement des donn�es de l'espace de travail";
EduExportERR_0004.Diagnostic = "Impossible d'enregistrer les donn�es dans la base de donn�es.";
EduExportERR_0004.Advice = "Veuillez le signaler � l'administrateur de la base de donn�es";
EduExportERR_0005.Request = "Enregistrement des donn�es de l'espace de travail";
EduExportERR_0005.Diagnostic = "Impossible d'enregistrer les donn�es dans la base de donn�es : /P1";
EduExportERR_0005.Advice = "Veuillez le signaler � l'administrateur de la base de donn�es";
EduExportERR_0006.Request = "Fin de l'insertion des donn�es de l'espace de travail";
EduExportERR_0006.Diagnostic = "Impossible de terminer le traitement de l'enregistrement d'espace de travail dans la base de donn�es";
EduExportERR_0006.Advice = "Veuillez le signaler � l'administrateur de la base de donn�es";
EduExportERR_0007.Request = "Insertion ou fin de l'insertion des donn�es de l'espace de travail";
EduExportERR_0007.Diagnostic = "Impossible de cr�er ou de terminer le traitement de l'enregistrement d'espace de travail dans la base de donn�es : /P1";
EduExportERR_0007.Advice = "Veuillez le signaler � l'administrateur de la base de donn�es";
EduExportERR_0008.Request = "Importation d'un espace de travail dans la base de donn�es Electrical Cable";
EduExportERR_0008.Diagnostic = "Cet espace de travail est une r�vision de document en double dans un groupe d'espaces de travail non import�s et ne peut pas �tre import� en m�me temps que ces derniers";
EduExportERR_0008.Advice = "S�lectionnez un seul des espaces de travail associ�s � une r�vision de document � importer et proc�dez normalement";
EduExportERR_0009.Request = "Importation d'un espace de travail dans la base de donn�es Electrical Cable";
EduExportERR_0009.Diagnostic = "Cet espace de travail n'est pas configur� ; toutefois, son PRC est configur� mais il ne peut pas �tre import� dans ces conditions ";
EduExportERR_0009.Advice = "Configurez l'espace de travail dans un PRC configur� avant de proc�der � l'importation puis continuez normalement ";
EduExportERR_0010.Request = "Importation d'un espace de travail dans la base de donn�es Electrical Cable";
EduExportERR_0010.Diagnostic = "Cet espace de travail comporte des effectivit�s qui se chevauchent dans sa famille ";
EduExportERR_0010.Advice = "Modifiez les effectivit�s de l'espace de travail de sorte qu'elles ne chevauchent pas les effectivit�s des autres membres de sa famille ";

SecurityCheckPanelTitle = "Valider l'acc�s r�serv�";
SecurityCheckFailure = "Vous ne disposez pas des droits permettant d'effectuer cette op�ration. Prenez contact avec votre administrateur syst�me ENOVIA V5 VPM. Vous devez disposer des droits pour le ou les processus de s�curit� suivants :

  /P1";

InvalidForImportPITitle = "Non valide pour l'importation d'instances de pi�ce";
//InvalidForImportPIText1 = "The following part instances are considered invalid for import into the electrical cable database
//because they contain two or more combinations of CATProduct and/or import XML documents
//(there should only be one of these types of documents per part instance):
//";
InvalidForImportPIText1 = "Les instances de pi�ces suivantes ne seront pas trait�es car elles contiennent deux
CATProduct ou plus et/ou elles importent des documents XML (un seul document de ce type
doit �tre associ� � chaque instance de pi�ce) :
";

InvalidForImportPIText2 = "
     /P1";
InvalidForImportPIText3 = "

Cliquez sur OK pour continuer ou sur Annuler pour quitter cette op�ration";

MultipleDocRevisionPanelTitle = "R�visions de documents multiples";
MultipleDocRevisionPanelText = "Les espaces de travail suivants affich�s dans l'arbre de sp�cifications du navigateur VPM sont associ�s par la r�vision de document :/P1

* - indique le dernier espace de travail import� dans une famille de r�visions de document ";
InvalidToImportByDocRevisionPanelTitle = "R�visions de documents multiples";
InvalidToImportByDocRevisionPanelText = "Les espaces de travail suivants sont associ�s par la r�vision de document et ne peuvent pas �tre import�s au cours d'une m�me op�ration :
/P1";
