//=============================================================================
//                                     CNEXT - CXR2
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwGDTWind
// LOCATION    :    DraftingIntDSA/CNext/resources/msgcatalog/
// AUTHOR      :    pbr
// DATE        :    June 12, 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Interactive GDT
//                  Creation Window.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//   Modified   : Philip Berlioz (pbr)
//   Date       : Jan. 1999
//   Chg id     : RI# 0210215 - CXR2
//   Chg nature : Rework NLS text handling, implement Contextual help
//--------------------------------------------------------------------
//   Modified   : Philip Berlioz (pbr)
//   Date       : January 04, 2000 
//   Chg id     : 0248358 A - CXR3 SP3 & CXR4
//   Chg nature : Update GDT creation panel.
//------------------------------------------------------------------------------

Title = "Tol�rances g�om�triques";

cadre2.Title = "Symbole";
cadre3.Title = "Valeur";
cadre4.Title = "R�f�rences";
cadre35.Title = "Zone de tol�rance projet�e";
cadre36.Title = "Filtrage";

cadre36.radio36.Title = "Oui";
cadre36.radio37.Title = "Non ";

cadre2.label2.Title  = "Spec 1... ";
cadre2.label38.Title = "Spec 2... ";

cadre2.images09.ShortHelp = "Caract�ristique g�om�trique";
cadre2.images09.boutonA0.ShortHelp = "Aucune";
cadre2.images09.boutonA1.ShortHelp = "Angularit�";
cadre2.images09.boutonA2.ShortHelp = "Circularit�";
cadre2.images09.boutonA3.ShortHelp = "Concentricit�";
cadre2.images09.boutonA4.ShortHelp = "Cylindricit�";
cadre2.images09.boutonA6.ShortHelp = "Battement total";
cadre2.images09.boutonA7.ShortHelp = "Rectitude";
cadre2.images09.boutonA8.ShortHelp = "Parall�lisme";
cadre2.images09.boutonA9.ShortHelp = "Perpendicularit�";
cadre2.images09.boutonA10.ShortHelp = "Plan�it�";
cadre2.images09.boutonA11.ShortHelp = "Position";
cadre2.images09.boutonA12.ShortHelp = "Profil de droite";
cadre2.images09.boutonA13.ShortHelp = "Profil de surface";
cadre2.images09.boutonA14.ShortHelp = "Battement simple";
cadre2.images09.boutonA15.ShortHelp = "Sym�trie";

cadre3.images12.ShortHelp = "Diam�tre";
cadre3.images12.boutonB0.ShortHelp = "Aucune";
cadre3.images12.boutonB1.ShortHelp = "Diam�tre";
cadre3.champ15.ShortHelp = "Valeur de la tol�rance";
cadre3.images16.ShortHelp = "Condition d'application";
cadre3.images16.boutonC0.ShortHelp = "Aucune";
cadre3.images16.boutonC1.ShortHelp = "Etat libre";
cadre3.images16.boutonC2.ShortHelp = "Minimum de mati�re";
cadre3.images16.boutonC3.ShortHelp = "Maximum de mati�re";
cadre3.images16.boutonC5.ShortHelp = "Ind�pendant de la dimension";
cadre3.images16.boutonC6.ShortHelp = "Plan tangent";

cadre4.champ19.ShortHelp = "Surface de r�f�rence primaire";
cadre4.images22.ShortHelp = "Condition d'application";
cadre4.images22.boutonD0.ShortHelp = "Aucune";
cadre4.images22.boutonD1.ShortHelp = "Etat libre";
cadre4.images22.boutonD2.ShortHelp = "Minimum de mati�re";
cadre4.images22.boutonD3.ShortHelp = "Maximum de mati�re";
cadre4.images22.boutonD5.ShortHelp = "Ind�pendant de la dimension";
cadre4.images22.boutonD6.ShortHelp = "Plan tangent";
cadre4.champ24.ShortHelp = "Surface de r�f�rence secondaire";
cadre4.images25.ShortHelp = "Condition d'application";
cadre4.images25.boutonE0.ShortHelp = "Aucune";
cadre4.images25.boutonE1.ShortHelp = "Etat libre";
cadre4.images25.boutonE2.ShortHelp = "Minimum de mati�re";
cadre4.images25.boutonE3.ShortHelp = "Maximum de mati�re";
cadre4.images25.boutonE5.ShortHelp = "Ind�pendant de la dimension";
cadre4.images25.boutonE6.ShortHelp = "Plan tangent";
cadre4.champ28.ShortHelp = "Surface de r�f�rence tertiaire";
cadre4.images29.ShortHelp = "Condition d'application";
cadre4.images29.boutonF0.ShortHelp = "Aucune";
cadre4.images29.boutonF1.ShortHelp = "Etat libre";
cadre4.images29.boutonF2.ShortHelp = "Minimum de mati�re";
cadre4.images29.boutonF3.ShortHelp = "Maximum de mati�re";
cadre4.images29.boutonF5.ShortHelp = "Ind�pendant de la dimension";
cadre4.images29.boutonF6.ShortHelp = "Plan tangent";

cadre35.champ35.ShortHelp = "Valeur de la zone de tol�rance projet�e";
cadre35.images35.ShortHelp = "Zone de tol�rance projet�e";
cadre35.images35.boutonM0.ShortHelp = "Aucune";
cadre35.images35.boutonM1.ShortHelp = "Zone de tol�rance projet�e";

cadre36.radio36.ShortHelp = "Filtrage des caract�ristiques g�om�triques";
cadre36.radio37.ShortHelp = "Pas de filtrage des caract�ristiques g�om�triques";

cadre2.images39.ShortHelp = "Caract�ristique g�om�trique secondaire";
cadre2.images39.boutonG0.ShortHelp = "Aucune";
cadre2.images39.boutonG1.ShortHelp = "Angularit�";
cadre2.images39.boutonG2.ShortHelp = "Circularit�";
cadre2.images39.boutonG3.ShortHelp = "Concentricit�";
cadre2.images39.boutonG5.ShortHelp = "Cylindricit�";
cadre2.images39.boutonG6.ShortHelp = "Battement total";
cadre2.images39.boutonG7.ShortHelp = "Rectitude";
cadre2.images39.boutonG8.ShortHelp = "Parall�lisme";
cadre2.images39.boutonG9.ShortHelp = "Perpendicularit�";
cadre2.images39.boutonG10.ShortHelp = "Plan�it�";
cadre2.images39.boutonG11.ShortHelp = "Position";
cadre2.images39.boutonG12.ShortHelp = "Profil de droite";
cadre2.images39.boutonG13.ShortHelp = "Profil de surface";
cadre2.images39.boutonG14.ShortHelp = "Battement simple";
cadre2.images39.boutonG15.ShortHelp = "Sym�trie";

cadre3.images43.ShortHelp = "Diam�tre";
cadre3.images43.boutonH0.ShortHelp = "Aucune";
cadre3.images43.boutonH1.ShortHelp = "Diam�tre";
cadre3.champ46.ShortHelp = "Valeur de la tol�rance";
cadre3.images47.ShortHelp = "Condition d'application";
cadre3.images47.boutonI0.ShortHelp = "Aucune";
cadre3.images47.boutonI1.ShortHelp = "Etat libre";
cadre3.images47.boutonI2.ShortHelp = "Minimum de mati�re";
cadre3.images47.boutonI3.ShortHelp = "Maximum de mati�re";
cadre3.images47.boutonI5.ShortHelp = "Ind�pendant de la dimension";
cadre3.images47.boutonI6.ShortHelp = "Plan tangent";

cadre4.champ51.ShortHelp = "Surface de r�f�rence primaire";
cadre4.images53.ShortHelp = "Condition d'application";
cadre4.images53.boutonJ0.ShortHelp = "Aucune";
cadre4.images53.boutonJ1.ShortHelp = "Etat libre";
cadre4.images53.boutonJ2.ShortHelp = "Minimum de mati�re";
cadre4.images53.boutonJ3.ShortHelp = "Maximum de mati�re";
cadre4.images53.boutonJ5.ShortHelp = "Ind�pendant de la dimension";
cadre4.images53.boutonJ6.ShortHelp = "Plan tangent";
cadre4.champ56.ShortHelp = "Surface de r�f�rence secondaire";
cadre4.images57.ShortHelp = "Condition d'application";
cadre4.images57.boutonK0.ShortHelp = "Aucune";
cadre4.images57.boutonK1.ShortHelp = "Etat libre";
cadre4.images57.boutonK2.ShortHelp = "Minimum de mati�re";
cadre4.images57.boutonK3.ShortHelp = "Maximum de mati�re";
cadre4.images57.boutonK5.ShortHelp = "Ind�pendant de la dimension";
cadre4.images57.boutonK6.ShortHelp = "Plan tangent";
cadre4.champ61.ShortHelp = "Surface de r�f�rence tertiaire";
cadre4.images62.ShortHelp = "Condition d'application";
cadre4.images62.boutonL0.ShortHelp = "Aucune";
cadre4.images62.boutonL1.ShortHelp = "Etat libre";
cadre4.images62.boutonL2.ShortHelp = "Minimum de mati�re";
cadre4.images62.boutonL3.ShortHelp = "Maximum de mati�re";
cadre4.images62.boutonL5.ShortHelp = "Ind�pendant de la dimension";
cadre4.images62.boutonL6.ShortHelp = "Plan tangent";

cadre2.images09.Help = "Diff�rents symboles de caract�ristique g�om�trique";
cadre2.images09.boutonA0.Help = "Aucune caract�ristique g�om�trique";
cadre2.images09.boutonA1.Help = "Ajoute une tol�rance angulaire";
cadre2.images09.boutonA2.Help = "Ajoute une tol�rance circulaire";
cadre2.images09.boutonA3.Help = "Ajoute une tol�rance concentrique";
cadre2.images09.boutonA4.Help = "Ajoute une tol�rance cylindrique";
cadre2.images09.boutonA6.Help = "Ajoute une tol�rance battement total";
cadre2.images09.boutonA7.Help = "Ajoute une tol�rance rectitude";
cadre2.images09.boutonA8.Help = "Ajoute une tol�rance de parall�lisme";
cadre2.images09.boutonA9.Help = "Ajoute une tol�rance de perpendicularit�";
cadre2.images09.boutonA10.Help = "Ajoute une tol�rance de plan�it�";
cadre2.images09.boutonA11.Help = "Ajoute une tol�rance de position";
cadre2.images09.boutonA12.Help = "Ajoute une tol�rance de profil lin�aire";
cadre2.images09.boutonA13.Help = "Ajoute une tol�rance de profil de surface";
cadre2.images09.boutonA14.Help = "Ajoute une tol�rance de battement simple";
cadre2.images09.boutonA15.Help = "Ajoute une tol�rance sym�trique";

cadre3.images12.Help = "Diam�tre";
cadre3.images12.boutonB0.Help = "Pas de tol�rance de diam�tre";
cadre3.images12.boutonB1.Help = "Symbole de diam�tre";
cadre3.champ15.Help = "Editeur de la valeur de la tol�rance";
cadre3.images16.Help = "Condition d'application";
cadre3.images16.boutonC0.Help = "Pas de condition d'application";
cadre3.images16.boutonC1.Help = "Condition d'application Etat Libre";
cadre3.images16.boutonC2.Help = "Condition d'application Minimum de mati�re";
cadre3.images16.boutonC3.Help = "Condition d'application Maximum de mati�re";
cadre3.images16.boutonC5.Help = "Condition d'application Ind�pendant de la dimension";
cadre3.images16.boutonC6.Help = "Condition d'application Plan tangent";

cadre4.champ19.Help = "Editeur de la premi�re valeur de r�f�rence";
cadre4.images22.Help = "Diff�rents symboles";
cadre4.images22.boutonD0.Help = "Pas de condition d'application";
cadre4.images22.boutonD1.Help = "Condition d'application Etat Libre";
cadre4.images22.boutonD2.Help = "Condition d'application Minimum de mati�re";
cadre4.images22.boutonD3.Help = "Condition d'application Maximum de mati�re";
cadre4.images22.boutonD5.Help = "Condition d'application Ind�pendant de la dimension";
cadre4.images22.boutonD6.Help = "Condition d'application Plan tangent";
cadre4.champ24.Help = "Editeur de la deuxi�me valeur de r�f�rence";
cadre4.images25.Help = "Diff�rents symboles";
cadre4.images25.boutonE0.Help = "Pas de condition d'application";
cadre4.images25.boutonE1.Help = "Condition d'application Etat Libre";
cadre4.images25.boutonE2.Help = "Condition d'application Minimum de mati�re";
cadre4.images25.boutonE3.Help = "Condition d'application Maximum de mati�re";
cadre4.images25.boutonE5.Help = "Condition d'application Ind�pendant de la dimension";
cadre4.images25.boutonE6.Help = "Condition d'application Plan tangent";
cadre4.champ28.Help = "Editeur de la troisi�me valeur de r�f�rence";
cadre4.images29.Help = "Diff�rents symboles";
cadre4.images29.boutonF0.Help = "Pas de condition d'application";
cadre4.images29.boutonF1.Help = "Condition d'application Etat Libre";
cadre4.images29.boutonF2.Help = "Condition d'application Minimum de mati�re";
cadre4.images29.boutonF3.Help = "Condition d'application Maximum de mati�re";
cadre4.images29.boutonF5.Help = "Condition d'application Ind�pendant de la dimension";
cadre4.images29.boutonF6.Help = "Condition d'application Plan tangent";

cadre35.champ35.Help = "Valeur de la zone de tol�rance projet�e";
cadre35.images35.Help = "Zone de tol�rance projet�e";
cadre35.images35.boutonM0.Help = "Aucune zone de tol�rance projet�e";
cadre35.images35.boutonM1.Help = "Zone de tol�rance projet�e";

cadre36.radio36.Help = "Filtrage des caract�ristiques g�om�triques";
cadre36.radio37.Help = "Pas de filtrage des caract�ristiques g�om�triques";

cadre2.images39.Help = "Diff�rents symboles de caract�ristique g�om�trique";
cadre2.images39.boutonG0.Help = "Aucune caract�ristique g�om�trique";
cadre2.images39.boutonG1.Help = "Ajoute une tol�rance angulaire";
cadre2.images39.boutonG2.Help = "Ajoute une tol�rance circulaire";
cadre2.images39.boutonG3.Help = "Ajoute une tol�rance concentrique";
cadre2.images39.boutonG5.Help = "Ajoute une tol�rance cylindrique";
cadre2.images39.boutonG6.Help = "Ajoute une tol�rance battement total";
cadre2.images39.boutonG7.Help = "Ajoute une tol�rance rectitude";
cadre2.images39.boutonG8.Help = "Ajoute une tol�rance de parall�lisme";
cadre2.images39.boutonG9.Help = "Ajoute une tol�rance de perpendicularit�";
cadre2.images39.boutonG10.Help = "Ajoute une tol�rance de plan�it�";
cadre2.images39.boutonG11.Help = "Ajoute une tol�rance de position";
cadre2.images39.boutonG12.Help = "Ajoute une tol�rance de profil lin�aire";
cadre2.images39.boutonG13.Help = "Ajoute une tol�rance de profil de surface";
cadre2.images39.boutonG14.Help = "Ajoute une tol�rance de battement simple";
cadre2.images39.boutonG15.Help = "Ajoute une tol�rance sym�trique";

cadre3.images43.Help = "Diam�tre";
cadre3.images43.boutonH0.Help = "Pas de tol�rance de diam�tre";
cadre3.images43.boutonH1.Help = "Symbole de diam�tre";
cadre3.champ46.Help = "Editeur de la valeur de tol�rance";
cadre3.images47.Help = "Condition d'application";
cadre3.images47.boutonI0.Help = "Pas de condition d'application";
cadre3.images47.boutonI1.Help = "Condition d'application Etat Libre";
cadre3.images47.boutonI2.Help = "Condition d'application Minimum de mati�re";
cadre3.images47.boutonI3.Help = "Condition d'application Maximum de mati�re";
cadre3.images47.boutonI5.Help = "Condition d'application Ind�pendant de la dimension";
cadre3.images47.boutonI6.Help = "Condition d'application Plan tangent";

cadre4.champ51.Help = "Editeur de la premi�re valeur de r�f�rence";
cadre4.images53.Help = "Diff�rents symboles";
cadre4.images53.boutonJ0.Help = "Pas de condition d'application";
cadre4.images53.boutonJ1.Help = "Condition d'application Etat Libre";
cadre4.images53.boutonJ2.Help = "Condition d'application Minimum de mati�re";
cadre4.images53.boutonJ3.Help = "Condition d'application Maximum de mati�re";
cadre4.images53.boutonJ5.Help = "Condition d'application Ind�pendant de la dimension";
cadre4.images53.boutonJ6.Help = "Condition d'application Plan tangent";
cadre4.champ56.Help = "Editeur de la deuxi�me valeur de r�f�rence";
cadre4.images57.Help = "Diff�rents symboles";
cadre4.images57.boutonK0.Help = "Pas de condition d'application";
cadre4.images57.boutonK1.Help = "Condition d'application Etat Libre";
cadre4.images57.boutonK2.Help = "Condition d'application Minimum de mati�re";
cadre4.images57.boutonK3.Help = "Condition d'application Maximum de mati�re";
cadre4.images57.boutonK5.Help = "Condition d'application Ind�pendant de la dimension";
cadre4.images57.boutonK6.Help = "Condition d'application Plan tangent";
cadre4.champ61.Help = "Editeur de la troisi�me valeur de r�f�rence";
cadre4.images62.Help = "Diff�rents symboles";
cadre4.images62.boutonL0.Help = "Pas de condition d'application";
cadre4.images62.boutonL1.Help = "Condition d'application Etat Libre";
cadre4.images62.boutonL2.Help = "Condition d'application Minimum de mati�re";
cadre4.images62.boutonL3.Help = "Condition d'application Maximum de mati�re";
cadre4.images62.boutonL5.Help = "Condition d'application Ind�pendant de la dimension";
cadre4.images62.boutonL6.Help = "Condition d'application Plan tangent";

cadre2.images09.LongHelp = "Caract�ristique g�om�trique primaire
D�finit la caract�ristique g�om�trique primaire.";
cadre2.images09.boutonA0.LongHelp = "Aucune
Aucune caract�ristique g�om�trique d�finie.";
cadre2.images09.boutonA1.LongHelp = "Angularit�
Un axe ou une surface forme un angle sp�cifique avec un autre axe ou un autre plan de r�f�rence.";
cadre2.images09.boutonA2.LongHelp = "Circularit�
Tous les points d'une surface de r�volution sont �quidistants a un axe
qui a une intersection avec un plan qui lui est perpendiculaire.";
cadre2.images09.boutonA3.LongHelp = "Concentricit�
L'axe de tous les �l�ments d'une section ont le m�me axe de r�f�rence.";
cadre2.images09.boutonA4.LongHelp = "Cylindricit�
Tous les points d'une surface de r�volution sont �quidistants par rapport � un axe commun.";
cadre2.images09.boutonA6.LongHelp = "Battement Total
Tous les �l�ments d'une surface de r�volution sont associ�s � un axe de r�f�rence.";
cadre2.images09.boutonA7.LongHelp = "Rectitude
L'axe d'un �l�ment de r�volution ou d'un �l�ment d'une surface correspond � une ligne droite.";
cadre2.images09.boutonA8.LongHelp = "Parall�lisme
Une surface plane ou un axe est �quidistant � un plan ou axe de r�f�rence sur toute sa longueur.";
cadre2.images09.boutonA9.LongHelp = "Perpendicularit�
Un axe ou une surface plane pr�sente un angle droit avec un axe ou un plan de r�f�rence.";
cadre2.images09.boutonA10.LongHelp = "Plan�it�
Tous les �l�ments d'une surface se trouvent dans un plan.";
cadre2.images09.boutonA11.LongHelp = "Position
Une zone de tol�rance est associ�e � un axe ou plan central.";
cadre2.images09.boutonA12.LongHelp = "Profil de droite
Un �l�ment 3D est projet� sur un plan.";
cadre2.images09.boutonA13.LongHelp = "Profil d'une surface
Une surface 3D est projet�e sur un plan.";
cadre2.images09.boutonA14.LongHelp = "Battement simple
Les �l�ments circulaires d'une surface de r�volution sont associ�s � un axe de r�f�rence.";
cadre2.images09.boutonA15.LongHelp = "Sym�trie
L'emplacement d'un composant par rapport � un plan m�dian.";

cadre3.images12.LongHelp = "Diam�tre
Identifie que la tol�rance s'applique � un diam�tre.";
cadre3.images12.boutonB0.LongHelp = "Aucun
La tol�rance n'est associ�e � aucun diam�tre.";
cadre3.images12.boutonB1.LongHelp = "Diam�tre
La valeur suivante fait r�f�rence � un diam�tre.";
cadre3.champ15.LongHelp = "Valeur de tol�rance primaire
permet de sp�cifier la valeur de la tol�rance primaire.";
cadre3.images16.LongHelp = "Condition d'application
D�finit la condition d'application de la tol�rance.";
cadre3.images16.boutonC0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance.";
cadre3.images16.boutonC1.LongHelp = "Etat libre
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre3.images16.boutonC2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre3.images16.boutonC3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre3.images16.boutonC5.LongHelp = "Condition d'application Ind�pendant de la dimension. 
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre3.images16.boutonC6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";

cadre4.champ19.LongHelp = "R�f�rence primaire
Nom de la surface de r�f�rence primaire.";
cadre4.images22.LongHelp = "Condition d'application
D�finit la condition d'application de la R�f�rence primaire.";
cadre4.images22.boutonD0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance.";
cadre4.images22.boutonD1.LongHelp = "Etat libre
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre4.images22.boutonD2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images22.boutonD3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images22.boutonD5.LongHelp = "Condition d'application Ind�pendant de la dimension. 
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre4.images22.boutonD6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";
cadre4.champ24.LongHelp = "R�f�rence secondaire
Nom de la surface de r�f�rence secondaire.";
cadre4.images25.LongHelp = "Condition d'application
D�finit la condition d'application de la R�f�rence secondaire.";
cadre4.images25.boutonE0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance.";
cadre4.images25.boutonE1.LongHelp = "Etat libre
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre4.images25.boutonE2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images25.boutonE3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images25.boutonE5.LongHelp = "Condition d'application Ind�pendant de la dimension
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre4.images25.boutonE6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";
cadre4.champ28.LongHelp = "R�f�rence tertiaire
Nom de la surface de r�f�rence tertiaire.";
cadre4.images29.LongHelp = "Condition d'application
D�finit la condition d'application de la R�f�rence tertiaire.";
cadre4.images29.boutonF0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance";
cadre4.images29.boutonF1.LongHelp = "Condition d'application Etat libre. 
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre4.images29.boutonF2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images29.boutonF3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images29.boutonF5.LongHelp = "Condition d'application Ind�pendant de la dimension
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre4.images29.boutonF6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";

cadre35.champ35.LongHelp = "Zone de Tol�rance Projet�e
Valeur de la zone de tol�rance projet�e.";
cadre35.images35.LongHelp = "Zone de tol�rance projet�e
D�finit si une zone de tol�rance projet�e est appliqu�e.";
cadre35.images35.boutonM0.LongHelp = "Aucune
Aucune zone de tol�rance projet�e n'est appliqu�e.";
cadre35.images35.boutonM1.LongHelp = "Zone de tol�rance projet�e
La tol�rance est projet�e suivant la distance sp�cifi�e.";

cadre36.radio36.LongHelp = "Filtrage
Seuls les types de tol�rance admis par l'�l�ment g�om�trique seront propos�s";
cadre36.radio37.LongHelp = "Pas de filtrage
Tous les types de tol�rance seront propos�s";

cadre2.images39.LongHelp = "Caract�ristique g�om�trique secondaire
D�finit la caract�ristique g�om�trique secondaire.";
cadre2.images39.boutonG0.LongHelp = "Aucune
Aucune caract�ristique g�om�trique d�finie.";
cadre2.images39.boutonG1.LongHelp = "Angularit�
Un axe ou une surface forme un angle sp�cifique avec un autre axe ou un autre plan de r�f�rence.";
cadre2.images39.boutonG2.LongHelp = "Circularit�
Tous les points d'une surface de r�volution sont �quidistants a un axe
qui a une intersection avec un plan qui lui est perpendiculaire.";
cadre2.images39.boutonG3.LongHelp = "Concentricit�
L'axe de tous les �l�ments d'une section ont le m�me axe de r�f�rence.";
cadre2.images39.boutonG5.LongHelp = "Cylindricit�
Tous les points d'une surface de r�volution sont �quidistants de l'axe commun.";
cadre2.images39.boutonG6.LongHelp = "Battement Total
Tous les �l�ments d'une surface de r�volution sont associ�s � un axe de r�f�rence.";
cadre2.images39.boutonG7.LongHelp = "Rectitude
L'axe d'un �l�ment de r�volution ou d'un �l�ment d'une surface correspond � une ligne droite.";
cadre2.images39.boutonG8.LongHelp = "Parall�lisme
Une surface plane ou un axe est �quidistant � un plan ou axe de r�f�rence sur toute sa longueur.";
cadre2.images39.boutonG9.LongHelp = "Perpendicularit�
Un axe ou une surface plane pr�sente un angle droit avec un axe ou un plan de r�f�rence.";
cadre2.images39.boutonG10.LongHelp = "Plan�it�
Tous les �l�ments d'une surface se trouvent dans un plan.";
cadre2.images39.boutonG11.LongHelp = "Position
Une zone de tol�rance est associ�e � un axe ou plan central.";
cadre2.images39.boutonG12.LongHelp = "Profil de droite
Un �l�ment 3D est projet� sur un plan.";
cadre2.images39.boutonG13.LongHelp = "Profil d'une surface
Une surface 3D est projet�e sur un plan.";
cadre2.images39.boutonG14.LongHelp = "Battement simple
Les �l�ments circulaires d'une surface de r�volution sont associ�s � un axe de r�f�rence.";
cadre2.images39.boutonG15.LongHelp = "Sym�trie
L'emplacement d'un composant par rapport � un plan m�dian.";

cadre3.images43.LongHelp = "Diam�tre
Identifie que la tol�rance s'applique � un diam�tre.";
cadre3.images43.boutonH0.LongHelp = "Aucun
La tol�rance n'est associ�e � aucun diam�tre.";
cadre3.images43.boutonH1.LongHelp = "Diam�tre
La valeur suivante fait r�f�rence � un diam�tre.";
cadre3.champ46.LongHelp = "Valeur de tol�rance secondaire
permet de sp�cifier la valeur de la tol�rance secondaire.";
cadre3.images47.LongHelp = "Condition d'application
D�finit la condition d'application de la tol�rance.";
cadre3.images47.boutonI0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance.";
cadre3.images47.boutonI1.LongHelp = "Condition d'application Etat libre. 
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre3.images47.boutonI2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre3.images47.boutonI3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre3.images47.boutonI5.LongHelp = "Condition d'application Ind�pendant de la dimension
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre3.images47.boutonI6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";

cadre4.champ51.LongHelp = "R�f�rence primaire
Nom de la surface de r�f�rence primaire.";
cadre4.images53.LongHelp = "Condition d'application
D�finit la condition d'application de la R�f�rence primaire.";
cadre4.images53.boutonJ0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance.";
cadre4.images53.boutonJ1.LongHelp = "Condition d'application Etat libre. 
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre4.images53.boutonJ2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images53.boutonJ3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images53.boutonJ5.LongHelp = "Condition d'application Ind�pendant de la dimension
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre4.images53.boutonJ6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";
cadre4.champ56.LongHelp = "R�f�rence secondaire
Nom de la surface de r�f�rence secondaire.";
cadre4.images57.LongHelp = "Condition d'application
D�finit la condition d'application de la R�f�rence secondaire.";
cadre4.images57.boutonK0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance.";
cadre4.images57.boutonK1.LongHelp = "Condition d'application Etat libre. 
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre4.images57.boutonK2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images57.boutonK3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images57.boutonK5.LongHelp = "Condition d'application Ind�pendant de la dimension
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre4.images57.boutonK6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";
cadre4.champ61.LongHelp = "R�f�rence tertiaire
Nom de la surface de r�f�rence tertiaire.";
cadre4.images62.LongHelp = "Condition d'application
D�finit la condition d'application de la R�f�rence tertiaire.";
cadre4.images62.boutonL0.LongHelp = "Aucun
Aucun modificateur n'est associ� � la tol�rance.";
cadre4.images62.boutonL1.LongHelp = "Condition d'application Etat libre. 
D�formation d'une pi�ce APRES suppression des forces appliqu�es pendant la fabrication.";
cadre4.images62.boutonL2.LongHelp = "Condition d'application Minimum de mati�re
Le composant contient le minimum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images62.boutonL3.LongHelp = "Condition d'application Maximum de mati�re
Le composant contient le maximum de mati�re autoris� par sa cote de tol�rance.";
cadre4.images62.boutonL5.LongHelp = "Condition d'application Ind�pendant de la dimension
La tol�rance de positionnement doit �tre conserv�e ind�pendemment de la taille du composant actuel.";
cadre4.images62.boutonL6.LongHelp = "Plan tangent
Condition d'application Plan tangent.";
