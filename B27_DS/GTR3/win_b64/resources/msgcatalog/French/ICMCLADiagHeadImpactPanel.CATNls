DialogBoxTitle="Analyse de l'impact de la t�te";

//Selection options-------------------------------------------------------------------------------------

Label_Elements.Title="El�ments : ";
Label_Elements.LongHelp="Permet de s�lectionner les surfaces � v�rifier.";
Label_Elements.ShortHelp="S�lection des �l�ments";

//Tesselation frame-------------------------------------------------------------------------------------

Frame_Tessallation.Title="Facettisation";
Frame_Tessallation.LongHelp="L'analyse est bas�e sur des donn�es triangul�es.
Les surfaces sont converties en un mod�le de facette
avant la v�rification.";

CheckButton_Length.Title="Longueur de facette�: ";
CheckButton_Length.LongHelp="D�finit la longueur maximale des facettes, 
quelle que soit la courbure de la surface.
L'analyse est pr�cise avec cette m�thode. 
Cependant, elle n�cessite un temps de calcul
plus long. Plus la valeur de la longueur de facette 
est petite, plus la m�thode est pr�cise ; 
mais le calcul est aussi plus lent.";
CheckButton_Length.ShortHelp="Longueur de facette maximale";

Label_Tolerance.Title="Tol�rance�: ";
Label_Tolerance.LongHelp="D�finit la d�viation maximale du 
mod�le de facette � partir de la surface pour 
une tessellation d�pendante de la courbure.
Les surfaces sont subdivis�es en facettes plus
petites jusqu'� ce que la distance entre les facettes
planes et les surfaces soit inf�rieure � cette valeur. Ainsi,
de grandes facettes sont cr��es sur des zones �
faible courbure et des petites facettes sur celles
� forte courbure. Cette m�thode donne
des r�sultats rapides, mais certains peuvent �tre
difficiles � interpr�ter.";
Label_Tolerance.ShortHelp="D�viation maximale � partir de la surface";

//Options frame-----------------------------------------------------------------------------------------

Frame_Options.Title="Options";
Frame_Options.LongHelp="Offre des options suppl�mentaires.";

//Topology frame----------------------------------------------------------------------------------------

Frame_Topology.Title="Topologie";
Frame_Topology.LongHelp="D�termine les �l�ments � d�tecter.";

CheckButton_SharpBend.Title="Pli aigu";
CheckButton_SharpBend.LongHelp="D�tecte les plis aigus.";
CheckButton_SharpBend.ShortHelp="Plis aigus";

CheckButton_Edge.Title="Ar�te";
CheckButton_Edge.LongHelp="D�tecte des ar�tes de facette.";
CheckButton_Edge.ShortHelp="Ar�tes";

//Display frame-----------------------------------------------------------------------------------------

Frame_Display.Title="Affichage";
Frame_Display.LongHelp="Offre des options d'affichage.";

CheckButton_Lighting.LongHelp="D�finit le mode rendu r�aliste.
- ACTIVE -
  Calcule l'analyse � l'aide des param�tres de source
  de lumi�re courants. En raison des valeurs de luminosit� diff�rentes,
  l'affichage a un effet st�r�oscopique.
- DESACTIVE -
  Affiche l'analyse ind�pendamment des param�tres de source de lumi�re. 
  Les valeurs de luminosit� sont �gales pour chaque pixel.";
CheckButton_Lighting.ShortHelp="Activer l'�clairage";


Label_Radius.Title="Rayon : ";
Label_Radius.LongHelp="D�finit le rayon de la sph�re.
Une 't�te standard' a en principe
un rayon de 80 resp. 85�mm.";
Label_Radius.ShortHelp="Rayon de la sph�re";

Label_CurvatureRadius.Title="Rayon de courbure : ";
Label_CurvatureRadius.LongHelp="Marque en rouge comme critiques toutes les r�gions
de g�om�trie poss�dant des courbures plus faibles
que ce rayon.";
Label_CurvatureRadius.ShortHelp="D�finition du rayon de courbure";


//Parameters frame--------------------------------------------------------------------------------------

Frame_Parameters.Title="Param�tres";
Frame_Parameters.LongHelp="Sp�cifie des rayons de courbure suppl�mentaires.";

CheckButton_Separate.Title="S�par� ";
CheckButton_Separate.LongHelp="Un second rayon de courbure peut �tre d�fini et
une surface peut �tre s�lectionn�e comme surface de s�paration.
Les g�om�tries qui sont dans la direction de la normale � la surface
d'une part et celles oppos�es � cette normale d'autre part
seront donc v�rifi�es avec des rayons diff�rents. Le second
rayon de courbure sp�cifi� ici sera appliqu� sur le c�t� de la
surface de s�paration d�termin� par le vecteur de la normale
� la surface affich�e";
CheckButton_Separate.ShortHelp="Second rayon de courbure :";

CheckButton_Individual.Title="Individuel�: ";
CheckButton_Individual.LongHelp="Des rayons de courbure individuels peuvent �tre
d�finis pour un diagnostic sur des surfaces s�par�es.";
CheckButton_Individual.ShortHelp="Rayons de courbure individuels.";


//------------------------------------------------------------------------------------------------------

RadioButton_Edge.Title="Ar�te";
RadioButton_Edge.LongHelp="Affichage des ar�tes de facette";

RadioButton_Shading.Title="Rendu r�aliste";
RadioButton_Shading.LongHelp="Le mode Rendu r�aliste est uniquement disponible lorsque le rendu r�aliste du mode
d'affichage avec les mati�res est activ�.";

Frame_DisplayOptions.Title="Affichage";
