// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRscLightIlluminationPropertyFrame
//
//===========================================================================

typeSepFrame.typeSepLabel.Title="Source";
typeSepFrame.typeSepLabel.Help="D�finit le type de la source lumineuse";
typeSepFrame.typeSepLabel.ShortHelp="Type de la source lumineuse";
typeSepFrame.typeSepLabel.LongHelp="D�finit le type de la source lumineuse.";

typeLabel.Title="Type";
typeLabel.Help="D�finit le type de la source lumineuse";
typeLabel.ShortHelp="Type de la source lumineuse";
typeLabel.LongHelp="D�finit le type de la source lumineuse.";

typeFrame.typeBorderFrame.typeCombo.Help="D�finit le type de la source lumineuse";
typeFrame.typeBorderFrame.typeCombo.ShortHelp="Type de la source lumineuse";
typeFrame.typeBorderFrame.typeCombo.LongHelp="D�finit le type de la source lumineuse.";

SpotLight="Spot";
PointLight="Ponctuelle";
DirectionalLight="Directionnelle";

angleLabel.Title="Angle";
angleLabel.Help="D�finit l'angle du spot";
angleLabel.ShortHelp="Angle du spot";
angleLabel.LongHelp="D�finit l'angle du spot.";

angleWheel.Title="";
angleWheel.Help="D�finit l'angle du spot";
angleWheel.ShortHelp="Angle du spot";
angleWheel.LongHelp="D�finit l'angle du spot.";

colorSepFrame.colorSepLabel.Title="Couleur";
colorSepFrame.colorSepLabel.Help="D�finit la couleur et l'intensit� de la lumi�re";
colorSepFrame.colorSepLabel.ShortHelp="Couleur et intensit�";
colorSepFrame.colorSepLabel.LongHelp="Change la couleur et l'intensit� de la lumi�re.";

colorLabel.Title="Couleur";
colorLabel.Help="D�finit la couleur de la lumi�re";
colorLabel.ShortHelp="Couleur";
colorLabel.LongHelp="Change la couleur de la lumi�re.";

colorSlider.Title="";
colorSlider.Help="D�finit la couleur de la lumi�re";
colorSlider.ShortHelp="Couleur";
colorSlider.LongHelp="Change la couleur de la lumi�re.";

intensityLabel.Title="Intensit�";
intensityLabel.Help="D�finit l'intensit� de la lumi�re";
intensityLabel.ShortHelp="Intensit� de la lumi�re";
intensityLabel.LongHelp="D�finit l'intensit� de la lumi�re.";

intensitySlider.Help="D�finit l'intensit� de la lumi�re";
intensitySlider.ShortHelp="Intensit� de la lumi�re";
intensitySlider.LongHelp="D�finit l'intensit� de la lumi�re.";

attenuationSepFrame.attenuationSepLabel.Title="Att�nuation";
attenuationSepFrame.attenuationSepLabel.Help="D�finit l'att�nuation de l'�clairage";
attenuationSepFrame.attenuationSepLabel.ShortHelp="Att�nuation de l'�clairage";
attenuationSepFrame.attenuationSepLabel.LongHelp="Change l'att�nuation de l'�clairage.";

falloffLabel.Title="Att�nuation";
falloffLabel.Help="D�finit le type d'att�nuation de l'�clairage";
falloffLabel.ShortHelp="Type d'att�nuation";
falloffLabel.LongHelp="D�finit le type d'att�nuation de l'�clairage.
- Aucune: l'�clairage est constant et infini
- Lin�aire: l'�clairage commence � �tre att�nu� � partir
d'une certaine distance (voir 'Ratio du d�but') et est
nul � la fin de l'att�nuation.
- R�aliste: l'�clairage  diminue avec le carr� de la
distance au centre de la lumi�re.
NB: Pour l'att�nuation r�aliste la lumi�re est infinie
et la distance de fin d'att�nuation repr�sente la limite
ou l'�clairage est n�gligeable.";

falloffFrame.falloffBorderFrame.falloffCombo.Help="D�finit le type d'att�nuation de l'�clairage";
falloffFrame.falloffBorderFrame.falloffCombo.ShortHelp="Type d'att�nuation";
falloffFrame.falloffBorderFrame.falloffCombo.LongHelp="D�finit le type d'att�nuation de l'�clairage.
- Aucune: l'�clairage est constant et infini
- Lin�aire: l'�clairage commence � �tre att�nu� � partir
d'une certaine distance (voir 'Ratio du d�but') et est
nul � la fin de l'att�nuation.
- R�aliste: l'�clairage  diminue avec le carr� de la
distance au centre de la lumi�re.
NB: Pour l'att�nuation r�aliste la lumi�re est infinie
et la distance de fin d'att�nuation repr�sente la limite
ou l'�clairage est n�gligeable.";

FalloffNone="Aucune";
FalloffInverse="Lin�aire";
FalloffInverseSquare="R�aliste";

endLabel.Title="Fin";
endLabel.Help="D�finit la fin de l'att�nuation";
endLabel.ShortHelp="Fin de l'att�nuation";
endLabel.LongHelp="D�finit la distance depuis le centre � partir de
laquelle l'�clairage est nul ou n�gligeable.";

endWheel.Title="";
endWheel.Help="D�finit la fin de l'att�nuation";
endWheel.ShortHelp="Fin de l'att�nuation";
endWheel.LongHelp="D�finit la distance depuis le centre � partir de
laquelle l'�clairage est nul ou n�gligeable.";

attenuationStartLabel.Title="Ratio du d�but";
attenuationStartLabel.Help="D�finit le ratio du d�but de l'att�nuation";
attenuationStartLabel.ShortHelp="Ratio du d�but de l'att�nuation";
attenuationStartLabel.LongHelp="D�finit un ratio entre le centre et la cible, la 
distance � partir de laquelle l'�clairage commence � �tre att�nu�.
ex: 0 : l'att�nuation commence d�s l'origine, 0.5 : l'att�nuation commence au milieu, 1 : l'att�nuation commence � la fin (c-�-d pas d'att�nuation).";

attenuationStartSlider.Title="";
attenuationStartSlider.Help="D�finit le ratio du d�but de l'att�nuation";
attenuationStartSlider.ShortHelp="Ratio du d�but de l'att�nuation";
attenuationStartSlider.LongHelp="D�finit un ratio entre le centre et la cible, la 
distance � partir de laquelle l'�clairage commence � �tre att�nu�.
ex: 0 : l'att�nuation commence d�s l'origine, 0.5 : l'att�nuation commence au milieu, 1 : l'att�nuation commence � la fin (c-�-d pas d'att�nuation).";

attenuationAngleLabel.Title="Ratio de l'angle";
attenuationAngleLabel.Help="D�finit le ratio de l'angle d'att�nuation";
attenuationAngleLabel.ShortHelp="Ratio de l'angle d'att�nuation";
attenuationAngleLabel.LongHelp="D�finit un ratio, l'angle depuis l'axe de la lumi�re � 
partir duquel l'�clairage commence � �tre att�nu�.
ex: 0 : l'att�nuation commence � partir de l'axe, 0.5 : l'att�nuation commence au milieu de l'angle, 1 : l'att�nuation commence � la fin (c-�-d pas d'att�nuation angulaire).";

attenuationAngleSlider.Title="";
attenuationAngleSlider.Help="D�finit le ratio de l'angle d'att�nuation";
attenuationAngleSlider.ShortHelp="Ratio de l'angle d'att�nuation";
attenuationAngleSlider.LongHelp="D�finit un ratio, l'angle depuis l'axe de la lumi�re � 
partir duquel l'�clairage commence � �tre att�nu�.
ex: 0 : l'att�nuation commence � partir de l'axe, 0.5 : l'att�nuation commence au milieu de l'angle, 1 : l'att�nuation commence � la fin (c-�-d pas d'att�nuation angulaire).";


AttenuationExponentLabel.Title="Exposant ";
AttenuationExponentLabel.Help="D�finir l'exposant pour l'att�nuation de l'�clairage directionnel";
AttenuationExponentLabel.ShortHelp="Att�nuation de l'�clairage directionnel";
AttenuationExponentLabel.LongHelp="D�finit l'exposant pour l'att�nuation de l'�clairage directionnel.
Exemple : 0 : valeur minimale,
          2 : valeur maximale.";

AttenuationExponentSlider.Title="";
AttenuationExponentSlider.Help="D�finir l'exposant pour l'att�nuation de l'�clairage directionnel";
AttenuationExponentSlider.ShortHelp="Exposant pour l'att�nuation de l'�clairage directionnel";
AttenuationExponentSlider.LongHelp="D�finit l'exposant pour l'att�nuation de l'�clairage directionnel.
Exemple : 0 : valeur minimale,
128 : valeur maximale.";

shadowSepFrame.shadowSepLabel.Title="Ombres";

shadowSepFrame.shadowSepLabel.Help="Active les ombres";
shadowSepFrame.shadowSepLabel.ShortHelp="Active les ombres";
shadowSepFrame.shadowSepLabel.LongHelp="Active / d�sactive les ombres produites par cette lumi�re.";

shadowCheck.Title="Calcul�es";
shadowCheck.Help="Active les ombres en rendu calcul�";
shadowCheck.ShortHelp="Active les ombres en rendu calcul�";
shadowCheck.LongHelp="Active / d�sactive les ombres produites par cette lumi�re.
(Cette fonctionnalit� n'est utilis�e que par le rendu calcul�).";

hardwareShadowCheck.Title="Temps r�el";
hardwareShadowCheck.Help="Active les ombres temps r�el";
hardwareShadowCheck.ShortHelp="Active les ombres temps r�el";
hardwareShadowCheck.LongHelp="Active / d�sactive les ombres temps r�el produites par cette lumi�re.
(Cette fonctionnalit� n'est utilis�e que par le rendu temps r�el).";

shadowSmoothingLabel.Title="Lissage";
shadowSmoothingLabel.Help="D�finit le lissage aux limites de l'ombre";
shadowSmoothingLabel.ShortHelp="Lissage aux limites de l'ombre";
shadowSmoothingLabel.LongHelp="D�finit le lissage aux limites de l'ombre.";

shadowSmoothingSlider.Title="";
shadowSmoothingSlider.Help="D�finit le lissage aux limites de l'ombre";
shadowSmoothingSlider.ShortHelp="Lissage aux limites de l'ombre";
shadowSmoothingSlider.LongHelp="D�finit le lissage aux limites de l'ombre.";

shadowColorLabel.Title="Couleur";
shadowColorLabel.Help="D�finit la couleur de l'ombre";
shadowColorLabel.ShortHelp="Couleur de l'ombre";
shadowColorLabel.LongHelp="Change la couleur de l'ombre.";

shadowColorSlider.Title="";
shadowColorSlider.Help="D�finit la couleur de l'ombre";
shadowColorSlider.ShortHelp="Couleur de l'ombre";
shadowColorSlider.LongHelp="Change la couleur de l'ombre.";

shadowTransparencyLabel.Title="Transparence";
shadowTransparencyLabel.Help="D�finit la transparence de l'ombre";
shadowTransparencyLabel.ShortHelp="Transparence de l'ombre";
shadowTransparencyLabel.LongHelp="Change la transparence de l'ombre.";

shadowTransparencySlider.Title="";
shadowTransparencySlider.Help="D�finit la transparence de l'ombre";
shadowTransparencySlider.ShortHelp="Transparence de l'ombre";
shadowTransparencySlider.LongHelp="Change la transparence de l'ombre.";

EnvironmentCreationWarning="Il n'y a pas d'environnement actif pour les ombres.\nVoulez-vous en cr�er un transparent?";

