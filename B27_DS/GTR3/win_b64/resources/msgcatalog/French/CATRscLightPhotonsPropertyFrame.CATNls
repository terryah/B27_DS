// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRscLightPhotonsPropertyFrame
//
//===========================================================================

indirectSepFrame.indirectSepLabel.Title = "Photons";
indirectSepFrame.indirectSepLabel.Help = "D�finit les photons �mis par la lumi�re";
indirectSepFrame.indirectSepLabel.ShortHelp = "Photons �mis par la lumi�re";
indirectSepFrame.indirectSepLabel.LongHelp =
"D�finit les photons �mis par la lumi�re.";

indirectCheck.Title = "Emettre des photons";
indirectCheck.Help = "Active l'�mission de photons";
indirectCheck.ShortHelp = "Active l'�mission de photons";
indirectCheck.LongHelp =
"Active / d�sactive l'�mission de photons depuis la lumi�re.";

energyLabel.Title = "Facteur d'intensit�";
energyLabel.Help = "D�finit le facteur d'intensit� d'illumination indirecte";
energyLabel.ShortHelp = "Facteur d'intensit� d'illumination indirecte";
energyLabel.LongHelp =
"D�finit la quantit� totale d'intensit� utilis�e pour
l'illumination indirecte.
Cette intensit� est d�duite de l'intensit� d'�clairage par multiplication avec ce facteur.
Ce facteur peut �tre utilis� pour ajuster s�par�ment
l'illumination directe et indirecte.
La valeur par d�faut est 1 de telle sorte que les deux intensit�s soient �gales.";

energySlider.Help = "D�finit le facteur d'intensit� d'illumination indirecte";
energySlider.ShortHelp = "Facteur d'intensit� d'illumination indirecte";
energySlider.LongHelp =
"D�finit la quantit� totale d'intensit� utilis�e pour
l'illumination indirecte.
Cette intensit� est d�duite de l'intensit� d'�clairage par multiplication avec ce facteur.
Ce facteur peut �tre utilis� pour ajuster s�par�ment
l'illumination directe et indirecte.
La valeur par d�faut est 1 de telle sorte que les deux intensit�s soient �gales.";

globalEmittedLabel.Title = "Photons pour l'illumination globale";
globalEmittedLabel.Help = "D�finit les photons pour l'illumination globale";
globalEmittedLabel.ShortHelp = "Photons pour l'illumination globale";
globalEmittedLabel.LongHelp =
"D�finit la quantit� maximale de photons stock�s dans la table
des photons pour la source lumineuse lors du calcul d'illumination
globale. Cette valeur contr�le la qualit� de rendu de
l'illumination globale. Le nombre de photons est g�n�ralement
situ� entre 10000 et 1000000. Des valeurs trop petites g�n�rent des taches lumineuses floues.
Si le regroupement final est actif, des valeurs plus faibles sont suffisantes.
NB: De trop grandes valeurs peuvent augmenter sensiblement les
temps de calcul.";

globalEmittedSlider.Help = "D�finit les photons pour l'illumination globale";
globalEmittedSlider.ShortHelp = "Photons pour l'illumination globale";
globalEmittedSlider.LongHelp =
"D�finit la quantit� maximale de photons stock�s dans la table
des photons pour la source lumineuse lors du calcul d'illumination
globale. Cette valeur contr�le la qualit� de rendu de
l'illumination globale. Le nombre de photons est g�n�ralement
situ� entre 10000 et 1000000. Des valeurs trop petites g�n�rent des taches lumineuses floues.
Si le regroupement final est actif, des valeurs plus faibles sont suffisantes.
NB: De trop grandes valeurs peuvent augmenter sensiblement les
temps de calcul.";

causticEmittedLabel.Title = "Photons pour les caustics";
causticEmittedLabel.Help = "D�finit les photons pour les caustics";
causticEmittedLabel.ShortHelp = "Photons pour les caustics";
causticEmittedLabel.LongHelp =
"D�finit la quantit� maximale de photons stock�s dans la table
des photons pour la source lumineuse lors du calcul des caustics.
Cette valeur contr�le la qualit� de rendu des caustics.
Le nombre de photons est g�n�ralement situ� entre 10000 et 1000000.
Des valeurs trop petites g�n�rent caustics floues.
NB: De trop grandes valeurs peuvent augmenter sensiblement les temps de calcul.";

causticEmittedSlider.Help = "D�finit les photons pour les caustics";
causticEmittedSlider.ShortHelp = "Photons pour les caustics";
causticEmittedSlider.LongHelp =
"D�finit la quantit� maximale de photons stock�s dans la table
des photons pour la source lumineuse lors du calcul des caustics.
Cette valeur contr�le la qualit� de rendu des caustics.
Le nombre de photons est g�n�ralement situ� entre 10000 et 1000000.
Des valeurs trop petites g�n�rent caustics floues.
NB: De trop grandes valeurs peuvent augmenter sensiblement les temps de calcul.";

