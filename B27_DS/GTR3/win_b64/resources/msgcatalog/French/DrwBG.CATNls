//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-------------------------------------------------------------------------------------
// FILENAME    :    DrwBG
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    Workbench Drafting Background
// DATE        :    17.11.1998
//-------------------------------------------------------------------------------------
// DESCRIPTION :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   ??.05.98    Creation
//      01           fgx   23.11.98    Nouvelle version CXR2
//      02           fgx   10.12.98    Changement de nom des toolbars background pour les records
//      03           fgx   26.02.99    Ajout tb de style
//      04           fgx   29.03.99    Nouvelles commandes sketcher
//      05           fgx   04.08.99    Nouveaux workbenches
//      06           fgx   17.11.99    Ajout des tb measure et browser
//      07           fgx   09.12.99    Ajout de tb analyze + container lines
//	  08	         lgk   15.10.02	   Revision
//=====================================================================================

// Configuration
// -------------
DrwBG.Title="Drafting";
DrwBG.Help = "Atelier de dessin pour le calque du fond";
DrwBG.ShortHelp = "Drafting";

// Toolbars
// --------
// Right
contTbSelectBG.Title="S�lection";
contTbDrawingBG.Title="Dessin";
contTbViewsBG.Title="Vues";
contTbDimensioningBG.Title="Dimensionnement";
contTbGenerationBG.Title="G�n�ration";
contTbAnnotationsBG.Title="Annotations";
contTbDressupBG.Title="Habillage";
contTbGeometryCreationBG.Title="Cr�ation de g�om�trie";
contTbGeometryEditionBG.Title="Modification de g�om�trie";
// Top
contTbStyleBG.Title="Style";
contTbTextPropertiesBG.Title="Propri�t�s des textes";
contTbDimensionPropertiesBG.Title="Propri�t�s des cotes";
CATDrwMumericalPropertiesBGTlb.Title="Propri�t�s num�riques";
contTbGraphicPropertiesBG.Title="Propri�t�s graphiques";
// Bottom
contTbUpdateBG.Title="Active/D�sactive le mode de recalcul automatique";
contTbToolsBG.Title="Outils";
CATDrwVisualizationBckTlb.Title="Visualisation";
contTbMeasureBG.Title="Mesure absolue";
contTbBrowserBG.Title="Visionneur de catalogue";
// UnDock
contTbOrientationBG.Title="Position et orientation";
contTbPositionBG.Title="Positionnement";
contTbAnalyzeBG.Title="Analyser";

// Sub-Toolbars
// ------------
contSheets.Title="Calques";
contProjections.Title="Projections";
contSections.Title="Sections";
contDetails.Title="D�tails";
contClippings.Title="D�coupes";
contBroken.Title="Coupes locales";
contViewWizard.Title="Assistant";
contDimensions.Title="Cotes";
CATDrwFeatDimIcb.Title="Cotation de composant technologique";
CATDrwContInterruptTlb.Title="Edition de cote";
contTolerancing.Title="Tol�rancement";
contGenDim.Title="G�n�ration de cote";
contTexts.Title="Textes";
contSymbols.Title="Symboles";
CATDrwContTableIcb.Title="Table";
contAxis.Title="Axes et filetages";
contPoint.Title="Points";
contLine.Title="Lignes";
contCircle.Title="Cercles et ellipses";
contProfiles.Title="Profils";
contCurves.Title="Courbes";
contRelimit.Title="Relimitations";
contTransfor.Title="Transformations";
contCst.Title="Contraintes";
CATDrwAreaFillIcb.Title="Hachurage";
CATDrwSktAnalyseIcb.Title="Outils d'analyse 2D";
CATDrwBOMIcb.Title="Nomenclature";

// Sub-Menus
// ---------
// First level
contMenuViews.Title="Vues";
contMenuViews.Mnemonic="V";
contMenuDrawing.Title="Dessin";
contMenuDrawing.Mnemonic="s";
contMenuDimensioning.Title="Dimensionnement";
contMenuDimensioning.Mnemonic="D";
contMenuGeneration.Title="G�n�ration";
contMenuGeneration.Mnemonic="G";
contMenuAnnotations.Title="Annotations";
contMenuAnnotations.Mnemonic="A";
contMenuDressup.Title="Habillage";
contMenuDressup.Mnemonic="H";
contMenuGeometryCreation.Title="Cr�ation de g�om�trie";
contMenuGeometryCreation.Mnemonic="C";
contMenuGeometryEdition.Title="Modification de g�om�trie";
contMenuGeometryEdition.Mnemonic="M";
contMenuDrwGenDimTools.Title="G�n�ration de cote";
contMenuDrwGenDimTools.Mnemonic="G";
contMenuDrwPositioning.Title="Positionnement";
contMenuDrwPositioning.Mnemonic="P";
contMenuDrwAnalyze.Title="Analyser";
contMenuDrwAnalyze.Mnemonic="A";
contMenuCst.Title="Contraintes";
contMenuCst.Mnemonic="C";

// Second level
contMenuProjections.Title="Projections";
contMenuProjections.Mnemonic="P";
contMenuSections.Title="Sections";
contMenuSections.Mnemonic="S";
contMenuDetails.Title="D�tails";
contMenuDetails.Mnemonic="D";
contMenuClippings.Title="D�coupes";
contMenuClippings.Mnemonic="c";
contMenuViewWizard.Title="Assistant";
contMenuViewWizard.Mnemonic="A";
// ---
contMenuSheets.Title="Calques";
contMenuSheets.Mnemonic="C";
// ---
contMenuDimensions.Title="Cotes";
contMenuDimensions.Mnemonic="C";
CATDrwFeatDimSnu.Title="Cotation de composant technologique";
CATDrwFeatDimSnu.Mnemonic="c";
contMenuTolerancing.Title="Tol�rancement";
contMenuTolerancing.Mnemonic="T";
// ---
contMenuTexts.Title="Textes";
contMenuTexts.Mnemonic="T";
contMenuSymbols.Title="Symboles";
contMenuSymbols.Mnemonic="S";
CATDrwContTableMenuSnu.Title="Table";
CATDrwContTableMenuSnu.Mnemonic="a";
CATDrwAreaFillSnu.Title="Hachurage";

// ---
contMenuAxis.Title="Axes et filetages";
contMenuAxis.Mnemonic="A";
// ---
contMenuPoint.Title="Points";
contMenuPoint.Mnemonic="P";
contMenuLine.Title="Lignes";
contMenuLine.Mnemonic="L";
contMenuCircle.Title="Cercles et ellipse";
contMenuCircle.Mnemonic="C";
contMenuProfiles.Title="Profils";
contMenuProfiles.Mnemonic="f";
contMenuCurves.Title="Courbes";
contMenuCurves.Mnemonic="u";
// ---
contMenuRelimit.Title="Relimitations";
contMenuRelimit.Mnemonic="R";
contMenuTransfor.Title="Transformations";
contMenuTransfor.Mnemonic="T";
CATDrwBOMSnu.Title="Nomenclature";

// Third level
contMenuInterrupt.Title="Edition de cote";
contMenuInterrupt.Mnemonic="E";

// Contextual Sub-Menus
// --------------------
DrwPositionalLink.Title="Lien de position";
DrwOrientationLink.Title="Lien d'orientation";
DrwViewPositioning.Title="Positionnement des vues";
DrwDimInterruptSubMenu.Title="Interruption";
CATDrwViewPositioningTlb.Title="Positionnement des vues";
CATDrwGVSSnu.Title="Style de vue g�n�rative";
CATDrwDimensionRepresentation.Title="Repr�sentation de la cote";
CATDrwMeasureMode.Title="Mode mesure";
CATDrwRepresentationMode.Title="Mode repr�sentation";
CATDrwAngleSector.Title="Secteur angulaire";
CATDrwClipBoxSnu.Title="D�coupe 3D";
CATDrwPrintAreaSnu.Title="Zone d'impression";
