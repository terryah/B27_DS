DialogBoxTitle="Remplissage";

LabelFillType.Title="Type de remplissage : ";
LabelFillType.LongHelp="- Auto -
  La surface de remplissage est une surface mono-carreau.
- R�f�rence -
  S�lectionne une surface mono-carreau � projeter sur la r�gion s�lectionn�e.
  La r�f�rence d�finit l'ordre et la segmentation du r�sultat.
- Plusieurs surfaces -
  Plusieurs surfaces sont cr��es avec un point �toile
  permettant de modifier le r�sultat.";
LabelFillType.ShortHelp="D�finit le type de surface de remplissage.";

LabelSpace.Title=" ";

ComboFillType.LongHelp="- Auto -
  La surface de remplissage est une surface mono-carreau.
- R�f�rence -
  S�lectionne une surface mono-carreau � projeter sur la r�gion s�lectionn�e.
  La r�f�rence d�finit l'ordre et la segmentation du r�sultat.
- Plusieurs surfaces -
  Plusieurs surfaces sont cr��es avec un point �toile
  permettant de modifier le r�sultat.";
ComboFillType.ShortHelp="D�finit le type de surface de remplissage.";

//combobox options!
Combo.Auto="Automatique";
Combo.Reference="R�f�rence";
Combo.MultipleSurfaces="Surfaces multiples";

LabelCurve.Title="Courbe : ";
LabelCurve.LongHelp="S�lectionne les courbes ou les ar�tes de surface.";
LabelCurve.ShortHelp="S�lectionne les courbes ou les ar�tes de surface.";

//Options tab**************************
TabPageOptions.Title="Options";

//Frame Single *************************
FrameSingle.Title="Unique";
FrameSingle.LongHelp="Solution de surface mon-carreau.
Uniquement disponible pour remplissage de type Auto ou R�f�rence.";

LabelReference.Title="R�f�rence : ";
LabelReference.LongHelp="S�lectionne une surface suppl�mentaire qui est projet�e sur
les courbes ou les ar�tes s�lectionn�es.
La r�f�rence d�finit l'ordre et la segmentation du r�sultat.";
LabelReference.ShortHelp="S�lectionner une surface comme r�f�rence";

CheckButtonTrim.LongHelp="Disponible uniquement avec le remplissage de type R�f�rence.
 La surface de remplissage cr��e peut �tre relimit�e au niveau des fronti�res
 si le r�sultat non relimit� chevauche les courbes d'entr�e.
 non limit� chevauche les courbes en entr�e.";
CheckButtonTrim.ShortHelp="R�sultat de la relimitation";

LabelProjBase.Title="ProjBase : ";
LabelProjBase.LongHelp="S�lectionne les surfaces, les courbes ou les ar�tes adjacentes aux
surfaces de remplissage comme base de projection. Les points auxquels 
la surface de remplissage est approxim�e sont calcul�s dans la g�om�trie adjacente";
LabelProjBase.ShortHelp="S�lectionnez les �l�ments adjacents comme projection de base.";

//Frame Multiple *************************
FrameMultiple.Title="Multiple";
FrameMultiple.LongHelp="Solution de plusieurs surfaces.
Uniquement disponible pour le remplissage de type Plusieurs surfaces";

//System *********************************
LabelSystem.Title="Syst�me : ";
LabelSystem.LongHelp="Ajuste le syst�me de coordonn�es de la direction de mouvement du point �toile : 
 - Afficher-
   D�placement du plan parall�lement � l'�cran.
 - Mod�le -
   D�placement du plan parall�lement au plan de mod�le XY.
 - Plan de la boussole -
   D�placement du plan parall�lement au plan de la boussole XY.
 - Normale � la boussole -
   D�placement dans la direction Z de la boussole.
 - Normale � la surface -
   D�placement dans la direction de la normale � surface de remplissage.";
LabelSystem.ShortHelp="Degr�s de libert� de d�placement du point �toile.";

ComboSystem.ShortHelp="Degr�s de libert� de d�placement du point �toile";
ComboSystem.LongHelp="Ajuste le syst�me de coordonn�es de la direction de mouvement du point �toile : 
 - Afficher-
   D�placement du plan parall�lement � l'�cran.
 - Mod�le -
   D�placement du plan parall�lement au plan de mod�le XY.
 - Plan de la boussole -
   D�placement du plan parall�lement au plan de la boussole XY.
 - Normale � la boussole -
   D�placement dans la direction Z de la boussole.
 - Normale � la surface -
   D�placement dans la direction de la normale � surface de remplissage.";

//combobox options!
Combo.View="Afficher";
Combo.Model="Mod�le";
Combo.CompassPlane="Plan de la boussole";
Combo.CompassNormal="Normale de la boussole";
Combo.NormalToSurface="Normal � la surface";

//Label Order
LabelOrder.Title="Ordre : ";
LabelOrder.LongHelp="D�finit l'ordre minimal des surfaces de remplissage r�sultantes.";
LabelOrder.ShortHelp="Ordre minimal";
SpinnerOrder.LongHelp="D�finit l'ordre minimal des surfaces de remplissage r�sultantes.";
SpinnerOrder.ShortHelp="Ordre minimal";

//Frame Edge Influence
FrameEdgeInfluence.Title="Influence de l'ar�te";
FrameEdgeInfluence.LongHelp="Contr�le l'influence des tangentes d'ar�te s�cantes sur les surfaces
� cr�er. Dans le cas d'une surface cr��e dans un trou rond,
une influence d'ar�te accrue va maintenir la tangence d'ar�te � une �tendue plus large.";
ScrollBarEdgeInfluence.LongHelp="Contr�le l'influence des tangentes d'ar�te s�cantes sur les surfaces
� cr�er. Dans le cas d'une surface cr��e dans un trou rond,
une influence d'ar�te accrue va maintenir la tangence d'ar�te � une �tendue plus large.";
ScrollBarEdgeInfluence.ShortHelp="Influence des tangentes d'ar�te s�cantes";
SpinnerEdgeInfluence.LongHelp="Contr�le l'influence des tangentes d'ar�te s�cantes sur les surfaces
� cr�er. Dans le cas d'une surface cr��e dans un trou rond,
une influence d'ar�te accrue va maintenir la tangence d'ar�te � une �tendue plus large.";
SpinnerEdgeInfluence.ShortHelp="Influence des tangentes d'ar�te s�cantes";

//Frame Flatness
FrameFlatness.Title="Plan�it�";
FrameFlatness.LongHelp="Contr�le l'influence du plan tangent du point �toile.
Les grandes valeurs cr�ent une r�gion grande et tr�s plate autour du 
point �toile, tandis que les petites valeurs cr�ent un pic.";
ScrollBarFlatness.LongHelp="Contr�le l'influence du plan tangent du point �toile.
Les grandes valeurs cr�ent une r�gion grande et tr�s plate autour du 
point �toile, tandis que les petites valeurs cr�ent un pic.";
ScrollBarFlatness.ShortHelp="Influence du plan tangent";
SpinnerFlatness.LongHelp="Contr�le l'influence du plan tangent du point �toile.
Les grandes valeurs cr�ent une r�gion grande et tr�s plate autour du 
point �toile, tandis que les petites valeurs cr�ent un pic.";
SpinnerFlatness.ShortHelp="Influence du plan tangent";

DomainMode.Title="Le mode de domaine a �t� modifi� en Forcer-Domaine-Unique.
Le mode de domaine Utiliser-Premier-Domaine n'est plus pris en charge.";
