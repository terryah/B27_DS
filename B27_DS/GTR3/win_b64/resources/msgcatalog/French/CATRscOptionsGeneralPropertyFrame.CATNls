// COPYRIGHT DASSAULT SYSTEMES 2003
//===========================================================================
//
// CATRscOptionsGeneralPropertyFrame
//
//===========================================================================


//--------------------------------------
// Environment creation options
//--------------------------------------
environmentFrame.HeaderFrame.Global.Title = "Nouvel environnement";

environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.Title = "G�om�trie aimant�e";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.Help = "Place l'environnement sur la g�om�trie.";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.ShortHelp = "Aimanter l'environnement sur la g�om�trie";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.LongHelp =
"Lorsque cette option est coch�e, de nouveaux environnements sont cr��es de sorte que leur paroi inf�rieure (ou leur centre en cas d'environnements sph�riques) soit aimant�e au point g�om�trique le plus bas.";

//--------------------------------------
// Light creation options
//--------------------------------------
creationLightFrame.HeaderFrame.Global.Title = "Positionnement des lumi�res � la cr�ation";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.Title = "Mode par d�faut";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.Help = "Orientation par d�faut dirig�e vers le bas";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.ShortHelp = "Orientation dirig�e vers le bas";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.LongHelp =
"Dans ce mode, la lumi�re est cr��e perpendiculairement au point de vue et
orient�e vers le bas.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.Title = "Identique au point de vue";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.Help = "Orientation de la lumi�re identique au point de vue courant";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.ShortHelp = "Orientation selon le point de vue";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.LongHelp =
"Dans ce mode, la lumi�re est cr��e avec une diffusion identique � celle au point de vue courant, l'origine de la lumi�re �tant situ�e � l'origine du point de vue courant.
La lumi�re d'origine est situ�e � l'origine des yeux de l'utilisateur.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.Title = "Gravitationnel";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.Help = "Orientation de la lumi�re selon l'axe absolu choisi";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.ShortHelp = "Orientation selon l'un des axes absolu";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.LongHelp =
"Dans ce mode, la lumi�re est cr��e avec une direction identique � l'axe absolu choisi (X, Y ou Z)
et orient�e vers le bas.";


creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis1.Title = "X";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.Help = "Axe absolu X";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.ShortHelp = "Axe absolu X";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.LongHelp =
"La direction de la lumi�re cr��e est parall�le � l'axe absolu X.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis2.Title = "Y";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.Help = "Axe absolu Y";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.ShortHelp = "Axe absolu Y";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.LongHelp =
"La direction de la lumi�re cr��e est parall�le � l'axe absolu Y.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis3.Title = "Z";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.Help = "Axe absolu Z";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.ShortHelp = "Axe absolu Z";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.LongHelp =
"La direction de la lumi�re cr��e est parall�le � l'axe absolu Z.";

//-----------------------------------------------------
// Viewpoint Setting Option
//-----------------------------------------------------
viewPointFrame.HeaderFrame.Global.Title = "Mode d'affichage";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.Title = "Activ�e";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.Help = "Active / D�sactive le rapport du point de vue";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.ShortHelp = "Active / D�sactive le rapport du point de vue";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.LongHelp =
"Active / D�sactive le rapport du point de vue lors de l'entr�e dans l'atelier.";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.Title = "Parall�le";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.Help = "Force le point de vue en mode parall�le";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.ShortHelp = "Force le point de vue en mode parall�le";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.LongHelp =
"D�finit le point de vue en mode parall�le lors de l'acc�s � l'atelier.";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.Title = "Perspective";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.Help = "Force le point de vue en mode perspective";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.ShortHelp = "Force le point de vue en mode perspective";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.LongHelp =
"D�finit le pont de vue en mode perspective lors de l'acc�s � l'atelier.";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.Title = "Dernier enregistrement";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.Help = "D�finir le point de vue en mode de dernier enregistrement";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.ShortHelp = "D�finir le point de vue en mode de dernier enregistrement";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.LongHelp =
"D�finit le point de vue en mode de dernier enregistrement lors de l'acc�s � l'atelier.";

//-----------------------------------------------------
// Material View Setting Option
//-----------------------------------------------------
materialViewFrame.HeaderFrame.Global.Title = "Affichage du mat�riau";

materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.Title = "Afficher un mat�riau";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.Help = "Affichage des mat�riaux";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.ShortHelp = "Affichage des mat�riaux";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.LongHelp =
"Active ou d�sactive l'affichage des mat�riaux lors de l'acc�s � l'atelier.";
