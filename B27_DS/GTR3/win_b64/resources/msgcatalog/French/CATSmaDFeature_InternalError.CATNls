// ===============================================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
// ===============================================================================================
// Objectif         : Catalogue des erreurs internes des features sp�cifiques SmaDFeature
// Responsable      : SheetMetal Aero Team
// date de cr�ation : 07/05/2002
// Commentaires     : 
//
// Revue LGK le 09/01/2004
// ===============================================================================================
//-------------------------------------------------------------------
// flangeSurf_support
//-------------------------------------------------------------------
//
// FD
//
CATSmaDFeature_InternalError_ERR_0300.Request    = "";
CATSmaDFeature_InternalError_ERR_0300.Diagnostic = "Une erreur est survenue pendant la cr�ation de la surface.";
CATSmaDFeature_InternalError_ERR_0300.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0301.Request    = "Vous essayez de cr�er un bord tomb� surfacique.";
CATSmaDFeature_InternalError_ERR_0301.Diagnostic = "Une erreur s'est produite pendant le calcul de l'OML, \nou de la relimitation entre la surface de support et le composant de base.";
CATSmaDFeature_InternalError_ERR_0301.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0302.Request    = "Vous essayez de cr�er un bord tomb� surfacique avec un support de type Exact.";
CATSmaDFeature_InternalError_ERR_0302.Diagnostic = "Impossible de calculer le d�calage de la surface de support.";
CATSmaDFeature_InternalError_ERR_0302.Advice     = "V�rifiez la qualit� de la surface de support.";

CATSmaDFeature_InternalError_ERR_0303.Request    = "Vous essayez de cr�er un bord tomb� surfacique avec un support de type Exact.";
CATSmaDFeature_InternalError_ERR_0303.Diagnostic = "La surface de support n'est pas continue en tangence.";
CATSmaDFeature_InternalError_ERR_0303.Advice     = "Changez de surface ou corrigez-la (� l'aide de l'option Extraction ou Ajustement associ�e � une tangence, par exemple) ou utilisez un type de support Approximation.";

CATSmaDFeature_InternalError_ERR_0304.Request    = "Vous essayez de cr�er un bord tomb� surfacique.";
CATSmaDFeature_InternalError_ERR_0304.Diagnostic = "Une erreur s'est produite pendant le calcul de l'OML.";
CATSmaDFeature_InternalError_ERR_0304.Advice     = "Si le composant de base est une �me de surface web, \nv�rifiez que l'intersection entre la surface support et l'�me est connexe.";

CATSmaDFeature_InternalError_ERR_0310.Request    = "Vous essayez d'approcher la surface par une surface r�gl�e.";
CATSmaDFeature_InternalError_ERR_0310.Diagnostic = "Une erreur inattendue est survenue pendant le calcul de la surface.";
CATSmaDFeature_InternalError_ERR_0310.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0311.Request    = "Vous essayez d'approcher la surface par une surface r�gl�e.";
CATSmaDFeature_InternalError_ERR_0311.Diagnostic = "La longueur approch�e sp�cifi�e est trop importante pour d�finir la surface.";
CATSmaDFeature_InternalError_ERR_0311.Advice     = "Diminuez la longueur approch�e.";

CATSmaDFeature_InternalError_ERR_0312.Request    = "Vous essayez d'approcher la surface par une surface r�gl�e.";
CATSmaDFeature_InternalError_ERR_0312.Diagnostic = "Impossible de calculer le d�calage de cette surface.";
CATSmaDFeature_InternalError_ERR_0312.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0320.Request    = "Vous essayez de d�finir une surface avec une courbe et un angle.";
CATSmaDFeature_InternalError_ERR_0320.Diagnostic = "Impossible de calculer le d�calage de cette surface.";
CATSmaDFeature_InternalError_ERR_0320.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0321.Request    = "Vous essayez de d�finir une surface avec une courbe et un angle.";
CATSmaDFeature_InternalError_ERR_0321.Diagnostic = "La surface calcul�e a un point de rebroussement.";
CATSmaDFeature_InternalError_ERR_0321.Advice     = "Diminuez la longueur de la surface.";

CATSmaDFeature_InternalError_ERR_0322.Request    = "Vous essayez de d�finir une surface avec une courbe et un angle.";
CATSmaDFeature_InternalError_ERR_0322.Diagnostic = "La qualit� de la surface est insuffisante ;  impossible d'utiliser la surface calcul�e.";
CATSmaDFeature_InternalError_ERR_0322.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0340.Request    = "Vous essayez de cr�er un bord tomb� surfacique avec un support de type Exact.";
CATSmaDFeature_InternalError_ERR_0340.Diagnostic = "Impossible de calculer le d�calage de la surface de support.  Ce d�calage est n�cessaire pour g�n�rer des courbes caract�ristiques.";
CATSmaDFeature_InternalError_ERR_0340.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0341.Request    = "Vous essayez d'approcher la surface par une surface r�gl�e.";
CATSmaDFeature_InternalError_ERR_0341.Diagnostic = "Le d�calage n�cessaire pour certaines courbes caract�ristiques n'a pas pu �tre calcul� pour cette surface.";
CATSmaDFeature_InternalError_ERR_0341.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0342.Request    = "Vous essayez de d�finir une surface avec une courbe et un angle.";
CATSmaDFeature_InternalError_ERR_0342.Diagnostic = "Le d�calage n�cessaire pour certaines courbes caract�ristiques n'a pas pu �tre calcul� pour cette surface.";
CATSmaDFeature_InternalError_ERR_0342.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0350.Request    = ".";
CATSmaDFeature_InternalError_ERR_0350.Diagnostic = "Le r�sultat du support du bord tomb� repli� n'existe pas.";
CATSmaDFeature_InternalError_ERR_0350.Advice     = ".";

CATSmaDFeature_InternalError_ERR_0351.Request    = ".";
CATSmaDFeature_InternalError_ERR_0351.Diagnostic = "Le r�sultat du support du bord tomb� repli� n'existe pas.";
CATSmaDFeature_InternalError_ERR_0351.Advice     = ".";

CATSmaDFeature_InternalError_ERR_0352.Request    = ".";
CATSmaDFeature_InternalError_ERR_0352.Diagnostic = "La sp�cification du r�sultat du bord tomb� repli� n'existe pas.";
CATSmaDFeature_InternalError_ERR_0352.Advice     = ".";

//
// FP
//
CATSmaDFeature_InternalError_ERR_0380.Request    = "";
CATSmaDFeature_InternalError_ERR_0380.Diagnostic = "Impossible de cr�er le motif plat du support.";
CATSmaDFeature_InternalError_ERR_0380.Advice     = "";

CATSmaDFeature_InternalError_ERR_0390.Request    = ".";
CATSmaDFeature_InternalError_ERR_0390.Diagnostic = "Le r�sultat du support du bord tomb� mis � plat n'existe pas.";
CATSmaDFeature_InternalError_ERR_0390.Advice     = ".";

CATSmaDFeature_InternalError_ERR_0391.Request    = ".";
CATSmaDFeature_InternalError_ERR_0391.Diagnostic = "La sp�cification du r�sultat du bord tomb� mis � plat n'existe pas.";
CATSmaDFeature_InternalError_ERR_0391.Advice     = ".";
//
//-------------------------------------------------------------------
// flangeSurf_EOP_LengthFromOML ou Element
//-------------------------------------------------------------------
//
// FD
//
CATSmaDFeature_InternalError_ERR_0400.Request    = "";
CATSmaDFeature_InternalError_ERR_0400.Diagnostic = "Une erreur inattendue s'est produite pendant le calcul de l'EOP.";
CATSmaDFeature_InternalError_ERR_0400.Advice     = "";

CATSmaDFeature_InternalError_ERR_0401.Request    = "Vous essayez de d�finir l'EOP du bord tomb� avec une longueur.";
CATSmaDFeature_InternalError_ERR_0401.Diagnostic = "La longueur sp�cifi�e est trop importante pour d�finir l'EOP.";
CATSmaDFeature_InternalError_ERR_0401.Advice     = "Diminuez la longueur.";

CATSmaDFeature_InternalError_ERR_0402.Request    = "Vous essayez de d�finir l'EOP du bord tomb� avec un �l�ment en vue pli�e.";
CATSmaDFeature_InternalError_ERR_0402.Diagnostic = "Une erreur s'est produite pendant le calcul de l'EOP dans le r�sultat pli�.";
CATSmaDFeature_InternalError_ERR_0402.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0403.Request    = "Vous essayez de d�finir l'EOP du bord tomb� avec un �l�ment en vue � plat.";
CATSmaDFeature_InternalError_ERR_0403.Diagnostic = "Une erreur s'est produite pendant le calcul de l'EOP dans le r�sultat pli�.";
CATSmaDFeature_InternalError_ERR_0403.Advice     = "Modifiez les param�tres sp�cifi�s.";
//
// FP
//
CATSmaDFeature_InternalError_ERR_0480.Request    = "Calcul du motif plat";
CATSmaDFeature_InternalError_ERR_0480.Diagnostic = "Impossible de cr�er le motif plat de l'EOP.";
CATSmaDFeature_InternalError_ERR_0480.Advice     = "";

CATSmaDFeature_InternalError_ERR_0481.Request    = "Vous essayez de d�finir l'EOP du bord tomb� avec un �l�ment en vue pli�e.";
CATSmaDFeature_InternalError_ERR_0481.Diagnostic = "Une erreur s'est produite pendant le calcul de l'EOP dans le r�sultat � plat.";
CATSmaDFeature_InternalError_ERR_0481.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0482.Request    = "Vous essayez de d�finir l'EOP du bord tomb� avec un �l�ment en vue � plat.";
CATSmaDFeature_InternalError_ERR_0482.Diagnostic = "Une erreur s'est produite pendant le calcul de l'EOP dans le r�sultat � plat.";
CATSmaDFeature_InternalError_ERR_0482.Advice     = "Modifiez les param�tres sp�cifi�s.";

//-------------------------------------------------------------------
// flangeSurf_SideStandard ou element
//-------------------------------------------------------------------
//
// FD
//
CATSmaDFeature_InternalError_ERR_0500.Request    = "";
CATSmaDFeature_InternalError_ERR_0500.Diagnostic = "Une erreur inattendue est survenue pendant le calcul du c�t� 1.";
CATSmaDFeature_InternalError_ERR_0500.Advice     = "";

CATSmaDFeature_InternalError_ERR_0501.Request    = "Vous essayez de d�finir le c�t� 1 du bord tomb� avec l'option standard.";
CATSmaDFeature_InternalError_ERR_0501.Diagnostic = "La g�om�trie rend la d�finition d'un tel c�t� impossible.";
CATSmaDFeature_InternalError_ERR_0501.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0502.Request    = "";
CATSmaDFeature_InternalError_ERR_0502.Diagnostic = "Une erreur inattendue est survenue pendant le calcul du c�t� 2.";
CATSmaDFeature_InternalError_ERR_0502.Advice     = "";

CATSmaDFeature_InternalError_ERR_0503.Request    = "Vous essayez de d�finir le c�t� 2 du bord tomb� avec l'option standard.";
CATSmaDFeature_InternalError_ERR_0503.Diagnostic = "La g�om�trie rend la d�finition d'un tel c�t� impossible.";
CATSmaDFeature_InternalError_ERR_0503.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0504.Request    = "Vous essayez de d�finir le c�t� 1 du bord tomb� avec un �l�ment en vue pli�e.";
CATSmaDFeature_InternalError_ERR_0504.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 1 dans le r�sultat pli�.";
CATSmaDFeature_InternalError_ERR_0504.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0505.Request    = "Vous essayez de d�finir le c�t� 1 du bord tomb� avec un �l�ment en vue � plat.";
CATSmaDFeature_InternalError_ERR_0505.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 1 dans le r�sultat pli�.";
CATSmaDFeature_InternalError_ERR_0505.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0506.Request    = "Vous essayez de d�finir le c�t� 2 du bord tomb� avec un �l�ment en vue pli�e.";
CATSmaDFeature_InternalError_ERR_0506.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 2 dans le r�sultat pli�.";
CATSmaDFeature_InternalError_ERR_0506.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0507.Request    = "Vous essayez de d�finir le c�t� 2 du bord tomb� avec un �l�ment en vue � plat.";
CATSmaDFeature_InternalError_ERR_0507.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 2 dans le r�sultat pli�.";
CATSmaDFeature_InternalError_ERR_0507.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0508.Request    = "Vous essayez de red�finir un des c�t�s du bord tomb� avec un grugeage en coin.";
CATSmaDFeature_InternalError_ERR_0508.Diagnostic = "Une erreur s'est produite pendant le calcul de ce nouveau c�t� dans le r�sultat pli�.";
CATSmaDFeature_InternalError_ERR_0508.Advice     = "Modifiez les param�tres sp�cifi�s.";
//
// FP
//
CATSmaDFeature_InternalError_ERR_0580.Request    = "";
CATSmaDFeature_InternalError_ERR_0580.Diagnostic = "Impossible de cr�er le motif plat du c�t� 1.";
CATSmaDFeature_InternalError_ERR_0580.Advice     = "";

CATSmaDFeature_InternalError_ERR_0581.Request    = "";
CATSmaDFeature_InternalError_ERR_0581.Diagnostic = "Impossible de cr�er le motif plat du c�t� 2.";
CATSmaDFeature_InternalError_ERR_0581.Advice     = "";

CATSmaDFeature_InternalError_ERR_0582.Request    = "Vous essayez de d�finir le c�t� 1 du bord tomb� avec un �l�ment en vue pli�e.";
CATSmaDFeature_InternalError_ERR_0582.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 1 dans le r�sultat � plat.";
CATSmaDFeature_InternalError_ERR_0582.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0583.Request    = "Vous essayez de d�finir le c�t� 1 du bord tomb� avec un �l�ment en vue � plat.";
CATSmaDFeature_InternalError_ERR_0583.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 1 dans le r�sultat � plat.";
CATSmaDFeature_InternalError_ERR_0583.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0584.Request    = "Vous essayez de d�finir le c�t� 2 du bord tomb� avec un �l�ment en vue pli�e.";
CATSmaDFeature_InternalError_ERR_0584.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 2 dans le r�sultat � plat.";
CATSmaDFeature_InternalError_ERR_0584.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0585.Request    = "Vous essayez de d�finir le c�t� 2 du bord tomb� avec un �l�ment en vue � plat.";
CATSmaDFeature_InternalError_ERR_0585.Diagnostic = "Une erreur s'est produite pendant le calcul du c�t� 2 dans le r�sultat � plat.";
CATSmaDFeature_InternalError_ERR_0585.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0586.Request    = "Vous essayez de red�finir un des c�t�s du bord tomb� avec un grugeage en coin.";
CATSmaDFeature_InternalError_ERR_0586.Diagnostic = "Une erreur s'est produite pendant le calcul de ce nouveau c�t� dans le r�sultat � plat.";
CATSmaDFeature_InternalError_ERR_0586.Advice     = "Modifiez les param�tres sp�cifi�s.";

//-------------------------------------------------------------------
// flangeSurf_Relimit 
//-------------------------------------------------------------------
//
// FD
//
CATSmaDFeature_InternalError_ERR_0600.Request    = "Tous les param�tres en entr�e du bord tomb� sont d�finis.";
CATSmaDFeature_InternalError_ERR_0600.Diagnostic = "Les param�tres sp�cifi�s ne permettent pas de relimiter le bord tomb�.";
CATSmaDFeature_InternalError_ERR_0600.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0610.Request    = "Tous les param�tres en entr�e du bord tomb� sont d�finis.";
CATSmaDFeature_InternalError_ERR_0610.Diagnostic = "Les droites tangentes � la courbure appartenant au bord tomb� sont INTROUVABLES.";
CATSmaDFeature_InternalError_ERR_0610.Advice     = "Mettez la pi�ce � jour pour r�soudre cette erreur.";
//
// FP
//
CATSmaDFeature_InternalError_ERR_0680.Request    = "Tous les param�tres en entr�e du bord tomb� sont d�finis.";
CATSmaDFeature_InternalError_ERR_0680.Diagnostic = "Les param�tres en entr�e ne permettent pas de relimiter le bord tomb�.";
CATSmaDFeature_InternalError_ERR_0680.Advice     = "Modifiez les param�tres sp�cifi�s.";

//-------------------------------------------------------------------
// flangeSurf_Fillet 
//-------------------------------------------------------------------
//
// FD
//
// Erreur ind�termin�e
CATSmaDFeature_InternalError_ERR_0700.Request    = "";
CATSmaDFeature_InternalError_ERR_0700.Diagnostic = "Une erreur interne est survenue :  Impossible de calculer le bord tomb� repli�.";
CATSmaDFeature_InternalError_ERR_0700.Advice     = "Modifiez les param�tres sp�cifi�s.";
// Corner
CATSmaDFeature_InternalError_ERR_0710.Request    = "";
CATSmaDFeature_InternalError_ERR_0710.Diagnostic = "Impossible de calculer les coins du bord tomb� repli�.";
CATSmaDFeature_InternalError_ERR_0710.Advice     = "Modifiez les param�tres sp�cifi�s.";
CATSmaDFeature_InternalError_ERR_0711.Request    = "";
CATSmaDFeature_InternalError_ERR_0711.Diagnostic = "Impossible de calculer le coin 1 du bord tomb� repli�.";
CATSmaDFeature_InternalError_ERR_0711.Advice     = "V�rifiez que le bord tomb� repli� sans le coin 1 peut �tre calcul� avec la valeur du rayon sp�cifi� (sur la vue pli�e).";
CATSmaDFeature_InternalError_ERR_0712.Request    = "";
CATSmaDFeature_InternalError_ERR_0712.Diagnostic = "Impossible de calculer le coin 2 du bord tomb� repli�.";
CATSmaDFeature_InternalError_ERR_0712.Advice     = "V�rifiez que le bord tomb� repli� sans le coin 2 peut �tre calcul� avec la valeur du rayon sp�cifi� (sur la vue pli�e).";
// Fillet
CATSmaDFeature_InternalError_ERR_0720.Request    = "";
CATSmaDFeature_InternalError_ERR_0720.Diagnostic = "Impossible de calculer le cong� du bord tomb� repli�.";
CATSmaDFeature_InternalError_ERR_0720.Advice     = "Modifiez les param�tres sp�cifi�s.";
//
CATSmaDFeature_InternalError_ERR_0721.Request    = "";
CATSmaDFeature_InternalError_ERR_0721.Diagnostic = "Le contexte g�om�trique r�sulterait en un cong� � plusieurs rubans.";
CATSmaDFeature_InternalError_ERR_0721.Advice     = "Modifiez les param�tres sp�cifi�s.";
//
// FP
//
CATSmaDFeature_InternalError_ERR_0731.Request    = "Une erreur interne s'est produite";
CATSmaDFeature_InternalError_ERR_0731.Diagnostic = "Le lien des transformations sur les cellules a �chou�.";
CATSmaDFeature_InternalError_ERR_0731.Advice     = "V�rifiez le codage des cellules.";

// Erreur ind�termin�e
CATSmaDFeature_InternalError_ERR_0780.Request    = "";
CATSmaDFeature_InternalError_ERR_0780.Diagnostic = "Impossible de calculer le bord tomb� mis � plat.";
CATSmaDFeature_InternalError_ERR_0780.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaDFeature_InternalError_ERR_0781.Request    = "";
CATSmaDFeature_InternalError_ERR_0781.Diagnostic = "Echec de l'op�ration d'�paississement du bord tomb� mis � plat.";
CATSmaDFeature_InternalError_ERR_0781.Advice     = "Modifiez les param�tres sp�cifi�s.";


// Joggle Compensation
CATSmaDFeature_InternalError_ERR_0783.Request    = "";
CATSmaDFeature_InternalError_ERR_0783.Diagnostic = "La table utilis�e pour d�finir les valeurs de correction du soyage n'est pas valide.";
CATSmaDFeature_InternalError_ERR_0783.Advice     = "V�rifiez cette table et effectuez les corrections n�cessaires.";
CATSmaDFeature_InternalError_ERR_0784.Request    = "";
CATSmaDFeature_InternalError_ERR_0784.Diagnostic = "La correction du soyage n'a pas pu �tre calcul�e.";
CATSmaDFeature_InternalError_ERR_0784.Advice     = "Modifiez les param�tres sp�cifi�s.";
// Corner
CATSmaDFeature_InternalError_ERR_0785.Request    = "";
CATSmaDFeature_InternalError_ERR_0785.Diagnostic = "Impossible de calculer les coins du bord tomb� mis � plat.";
CATSmaDFeature_InternalError_ERR_0785.Advice     = "Modifiez les param�tres sp�cifi�s.";
CATSmaDFeature_InternalError_ERR_0786.Request    = "";
CATSmaDFeature_InternalError_ERR_0786.Diagnostic = "Impossible de calculer le coin 1 du bord tomb� mis � plat.";
CATSmaDFeature_InternalError_ERR_0786.Advice     = "Modifiez les param�tres sp�cifi�s.";
CATSmaDFeature_InternalError_ERR_0787.Request    = "";
CATSmaDFeature_InternalError_ERR_0787.Diagnostic = "Impossible de calculer le coin 2 du bord tomb� mis � plat.";
CATSmaDFeature_InternalError_ERR_0787.Advice     = "Modifiez les param�tres sp�cifi�s.";
// Fillet
CATSmaDFeature_InternalError_ERR_0790.Request    = "";
CATSmaDFeature_InternalError_ERR_0790.Diagnostic = "Impossible de calculer le cong� du bord tomb� repli�.";
CATSmaDFeature_InternalError_ERR_0790.Advice     = "Modifiez les param�tres sp�cifi�s.";

//-------------------------------------------------------------------
// Erreurs diverses 
//-------------------------------------------------------------------

CATSmaDFeature_InternalError_ERR_0900.Request    = ".";
CATSmaDFeature_InternalError_ERR_0900.Diagnostic = "Une erreur interne est survenue :  valeur non valide.";
CATSmaDFeature_InternalError_ERR_0900.Advice     = ".";

CATSmaDFeature_InternalError_ERR_0901.Request    = ".";
CATSmaDFeature_InternalError_ERR_0901.Diagnostic = "Une erreur interne est survenue :  structure du composant du bord tomb� non valide.";
CATSmaDFeature_InternalError_ERR_0901.Advice     = ".";

CATSmaDFeature_InternalError_ERR_0902.Request    = ".";
CATSmaDFeature_InternalError_ERR_0902.Diagnostic = "Une erreur interne est survenue lors du calcul d'un des profils du bord tomb� dans la vue pli�e.";
CATSmaDFeature_InternalError_ERR_0902.Advice     = ".";

// ===============================================================================================
// 1000 -> 1299 : Erreurs r�serv�es au Joggle
// ===============================================================================================

//-------------------------------------------------------------------
// Joggle : Specs d'entr�e incorrectes
//-------------------------------------------------------------------
CATSmaD_Joggle_Error_ERR_0001.Request    = "Impossible de lancer la cr�ation du soyage";
CATSmaD_Joggle_Error_ERR_0001.Diagnostic = "Les sp�cifications de la pi�ce sont incorrectes.";
CATSmaD_Joggle_Error_ERR_0001.Advice     = "Modifiez les sp�cifications de la pi�ce.";

CATSmaD_Joggle_Error_ERR_0002.Request    = "Impossible de lancer la cr�ation du soyage";
CATSmaD_Joggle_Error_ERR_0002.Diagnostic = "Les sp�cifications du soyage sont incorrectes.";
CATSmaD_Joggle_Error_ERR_0002.Advice     = "Modifiez les sp�cifications du soyage pour qu'elles soient compatibles avec cette pi�ce.";

CATSmaD_Joggle_Error_ERR_0010.Request    = "";
CATSmaD_Joggle_Error_ERR_0010.Diagnostic = "Epaisseur introuvable ou incorrecte.";
CATSmaD_Joggle_Error_ERR_0010.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0011.Request    = "";
CATSmaD_Joggle_Error_ERR_0011.Diagnostic = "Rayon de courbure introuvable ou incorrect.";
CATSmaD_Joggle_Error_ERR_0011.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0012.Request    = "";
CATSmaD_Joggle_Error_ERR_0012.Diagnostic = "Composant de base introuvable ou incorrect.";
CATSmaD_Joggle_Error_ERR_0012.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0013.Request    = "";
CATSmaD_Joggle_Error_ERR_0013.Diagnostic = "Plan du soyage introuvable ou incorrect.";
CATSmaD_Joggle_Error_ERR_0013.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0014.Request    = "";
CATSmaD_Joggle_Error_ERR_0014.Diagnostic = "Direction de la transition introuvable ou incorrecte.";
CATSmaD_Joggle_Error_ERR_0014.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0015.Request    = "";
CATSmaD_Joggle_Error_ERR_0015.Diagnostic = "Direction de la profondeur introuvable ou incorrecte.";
CATSmaD_Joggle_Error_ERR_0015.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0016.Request    = "";
CATSmaD_Joggle_Error_ERR_0016.Diagnostic = "Rayon de d�but introuvable ou incorrect.";
CATSmaD_Joggle_Error_ERR_0016.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0017.Request    = "";
CATSmaD_Joggle_Error_ERR_0017.Diagnostic = "Rayon de fin introuvable ou incorrect.";
CATSmaD_Joggle_Error_ERR_0017.Advice     = "Modifiez les param�tres sp�cifi�s.";

//-------------------------------------------------------------------
// Joggle : Build FD
//-------------------------------------------------------------------
//
CATSmaD_Joggle_Error_ERR_0100.Request    = "";
CATSmaD_Joggle_Error_ERR_0100.Diagnostic = "Une erreur s'est produite pendant la cr�ation du soyage repli�.";
CATSmaD_Joggle_Error_ERR_0100.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0101.Request    = "";
CATSmaD_Joggle_Error_ERR_0101.Diagnostic = "Les sp�cifications repli�es du soyage sont introuvables ou incorrectes.";
CATSmaD_Joggle_Error_ERR_0101.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0102.Request    = "";
CATSmaD_Joggle_Error_ERR_0102.Diagnostic = "La g�om�trie repli�e du soyage est introuvable ou incorrecte.";
CATSmaD_Joggle_Error_ERR_0102.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0103.Request    = "";
CATSmaD_Joggle_Error_ERR_0103.Diagnostic = "Une erreur s'est produite pendant le calcul des courbes de la caract�ristique du soyage repli�.";
CATSmaD_Joggle_Error_ERR_0103.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0104.Request    = "";
CATSmaD_Joggle_Error_ERR_0104.Diagnostic = "Un incident est survenu lors du calcul des courbes de la caract�ristique :  la pi�ce est corrompue.";
CATSmaD_Joggle_Error_ERR_0104.Advice     = "Essayez de relancer la cr�ation du soyage.";

//-------------------------------------------------------------------
// Joggle : Build FP
//-------------------------------------------------------------------

CATSmaD_Joggle_Error_ERR_0200.Request    = "";
CATSmaD_Joggle_Error_ERR_0200.Diagnostic = "Une erreur s'est produite pendant la cr�ation du soyage mis � plat.";
CATSmaD_Joggle_Error_ERR_0200.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0201.Request    = "";
CATSmaD_Joggle_Error_ERR_0201.Diagnostic = "Les sp�cifications du contour mis � plat du soyage sont introuvables ou incorrectes.";
CATSmaD_Joggle_Error_ERR_0201.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0202.Request    = "";
CATSmaD_Joggle_Error_ERR_0202.Diagnostic = "La g�om�trie du contour mis � plat du soyage est introuvable ou incorrecte.";
CATSmaD_Joggle_Error_ERR_0202.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0203.Request    = "";
CATSmaD_Joggle_Error_ERR_0203.Diagnostic = "Une erreur s'est produite pendant le calcul des courbes de la caract�ristique du soyage mis � plat.";
CATSmaD_Joggle_Error_ERR_0203.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0204.Request    = "";
CATSmaD_Joggle_Error_ERR_0204.Diagnostic = "Un incident est survenu lors du calcul des courbes de la caract�ristique :  la pi�ce est corrompue.";
CATSmaD_Joggle_Error_ERR_0204.Advice     = "Essayez de relancer la cr�ation du soyage.";

//-------------------------------------------------------------------
// Jogglesupport : Build FD
//-------------------------------------------------------------------
//
CATSmaD_Joggle_Error_ERR_0300.Request    = "";
CATSmaD_Joggle_Error_ERR_0300.Diagnostic = "Une erreur s'est produite pendant la cr�ation du support de soyage repli�.";
CATSmaD_Joggle_Error_ERR_0300.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0301.Request    = "";
CATSmaD_Joggle_Error_ERR_0301.Diagnostic = "Les sp�cifications repli�es du support du soyage sont introuvables ou incorrectes.";
CATSmaD_Joggle_Error_ERR_0301.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0302.Request    = "";
CATSmaD_Joggle_Error_ERR_0302.Diagnostic = "La g�om�trie repli�e du support du soyage est introuvable ou incorrecte.";
CATSmaD_Joggle_Error_ERR_0302.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0303.Request    = "";
CATSmaD_Joggle_Error_ERR_0303.Diagnostic = "Une erreur s'est produite pendant le calcul de l'OML.";
CATSmaD_Joggle_Error_ERR_0303.Advice     = "Si le composant de base est une �me de surface web, \nv�rifiez que l'intersection entre la surface support et l'�me est connexe.";


//-------------------------------------------------------------------
// Jogglesupport : Build FP
//-------------------------------------------------------------------

CATSmaD_Joggle_Error_ERR_0400.Request    = "";
CATSmaD_Joggle_Error_ERR_0400.Diagnostic = "Une erreur s'est produite pendant la cr�ation du support du soyage mis � plat.";
CATSmaD_Joggle_Error_ERR_0400.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0401.Request    = "";
CATSmaD_Joggle_Error_ERR_0401.Diagnostic = "Les sp�cifications de la mise � plat du support du soyage sont introuvables ou incorrectes.";
CATSmaD_Joggle_Error_ERR_0401.Advice     = "Modifiez les param�tres sp�cifi�s.";

CATSmaD_Joggle_Error_ERR_0402.Request    = "";
CATSmaD_Joggle_Error_ERR_0402.Diagnostic = "La g�om�trie de la mise � plat du support du soyage est introuvable ou incorrecte.";
CATSmaD_Joggle_Error_ERR_0402.Advice     = "Modifiez les param�tres sp�cifi�s.";

//-------------------------------------------------------------------
// AeroFlangeCCs : Build FD
//-------------------------------------------------------------------
Flange_CCsNotBuilt = "Les courbes caract�ristiques suivantes ne peuvent pas �tre cr��es :";
Folded_View = "Vue pli�e : ";	
Unfolded_View = "Vue d�pli�e : ";
BTLB = "Composant de base du BTL";
BTLF = "Support BTL";
OML = "OML";
IML = "IML";
OML2 = "Deuxi�me OML";
CLB = "CLB";
