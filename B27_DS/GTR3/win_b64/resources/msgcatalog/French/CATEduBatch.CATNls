//=============================================================================
//                                     CNEXT - CXR17
//                          COPYRIGHT DASSAULT SYSTEMES 2006 
//-----------------------------------------------------------------------------
// FILENAME    :    CATEduBatch.CATNls
// FRAMEWORK   :    CATEduUI
// AUTHOR      :    ehh
// DATE        :    February 2006
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Batch Electrical
//                  Cable Database Applications.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATION     user  date      purpose
//   HISTORY       ----  ----      -------
//------------------------------------------------------------------------------

ParseXMLFileErrorText = "***** Erreur lors de l'analyse du fichier XML : /P1 *****";
InvalidXMLFileErrorText = "***** Fichier XML non valide : /P1 *****";
CATEduBatchEarlyExitText = "Sortie anticip�e /P1, RC = /P2";
SessionNotCreated = "***** Impossible de g�n�rer une session CATIA *****";
BadNumInputParm = "Nombre incorrect de param�tres d'entr�e : /P1";
NULLPLMBatchEngineErrMsg = "***** Impossible de cr�er un moteur PLMBatch *****";
InvalidXMLFileData = "***** Donn�es non valides dans le fichier XML *****";
InvalidUserID = "Aucun utilisateur associ� � ENOVIA V5 VPM";
InvalidPassword = "Aucun mot de passe associ� � ENOVIA V5 VPM";
InvalidHost = "Aucun h�te associ� � ENOVIA V5 VPM";
InvalidPort = "Aucun port associ� � ENOVIA V5 VPM";
InvalidRole = "Aucun r�le associ� � ENOVIA V5 VPM";
RptDirDoesNotExist = "Le chemin du r�pertoire de sortie des rapports '/P1' n'existe pas";
RptDirNotAuthorized = "Le r�pertoire de sortie des rapports existe, mais il n'est pas accessible en �criture";
RptDirAccessProblem = "Probl�me d'acc�s au r�pertoire de sortie des rapports";
RptDirPathUndefined = "Aucun chemin de r�pertoire de sortie des rapports n'est d�fini";
RptPathNotAuthorized = "Le fichier /P1 existe mais n'est pas accessible en �criture ";
RptFilenameUndefined = "Aucun nom de fichier de sortie des rapports n'est d�fini";
CATIEdbWPSessionNotRetrieved = "Impossible d'obtenir CATIEdbWPSession � partir de la session";
SetCurrentDBApplicationIDFailure = "Impossible de d�finir l'application de base de donn�es en cours";
UnsuccessfulConnect = "***** Echec de la connexion � la base de donn�es *****";
NoPRCsInDatabase = "***** Aucun PRC d�tect� dans la base de donn�es *****";
NoPRCsDefined = "Aucun PRC d�fini";
LicensingFailure = "***** Echec de l'octroi de licence - La licence /P1 est n�cessaire pour ex�cuter cette application *****";
BatchHelp = "Syntaxe : /P1 CheminFichierXML
       o� CheminFichierXML correspond au chemin du fichier XML qui d�finit l'entr�e dans cette application
       (voir '/P2' pour une description du format du fichier XML)";
UserID = "ID utilisateur : /P1";

ReportDate = "Date : /P1";
ReportTime = "Heure : /P1";
PRCNotFoundText = "***** PRC '/P1' introuvable dans la base de donn�es *****";

DirDoesNotExist = "Le chemin d'acc�s au r�pertoire '/P1' '/P2' n'existe pas";
DirNotAuthorized = "Le r�pertoire '/P1' '/P2' existe mais n'est pas accessible en �criture ";
DirAccessProblem = "Erreur lors de l'acc�s au r�pertoire '/P1' '/P2'";
DirPathUndefined = "Aucun chemin d'acc�s au r�pertoire '/P1' d�fini ";
FileNotAuthorized = "Le fichier '/P1' '/P2' existe mais n'est pas accessible en �criture ";
FileUndefined = "Aucun nom de fichier '/P1' d�fini ";


//Cable Route Status
CableRouteStatus_0="Non d�fini";                   //CATEdsRouteStatus_Undefined = 0
CableRouteStatus_1="Routage complet";                //CATEdsRouteStatus_Full = 1
CableRouteStatus_2="Routage partiel";            //CATEdsRouteStatus_Partial = 2
CableRouteStatus_3="Validation requise ";                //CATEdsRouteStatus_NeedValidate = 3
CableRouteStatus_4="Routage termin�";              //CATEdsRouteStatus_CompletePartial = 4
CableRouteStatus_5="Non � jour";                 //CATEdsRouteStatus_OutOfDate = 5
CableRouteStatus_6="Non � jour";                 //CATEdsRouteStatus_OutOfDate_Partial = 6
CableRouteStatus_7="Effectivit� incoh�rente ";    //CATEdsRouteStatus_EffectivityInconsistent = 7
CableRouteStatus_8="Effectivit� incoh�rente ";    //CATEdsRouteStatus_EffectivityInconsistent_Partial = 8

//Rolling Report headers
RprtHdrSchematicTitle="Sch�matique ";
RprtHdrCblNameTitle="Nom du c�ble";
RprtHdrRollStatusTitle="Statut d'enroulement ";
RprtHdrCblRouteStatusTitle="Statut du routage de c�ble ";
RprtHdrOverriddenTitle="Remplac�";


//Roll report generation related
RollReportFilePathInfo="Fichier : /P1";
RollSuccessfulStatusInfo="OK : enroulement r�ussi ";
RollStatusLegendInfo="L�gende des statuts d'enroulement ";
OverriddenStatusYes="Oui";
OverriddenStatusNo="Non";
ErrorIndexOK="OK";

RollResultDiagnosticToken="/P1.Diagnostic";
RollResultAdviceToken="/P1.Conseil ";
RollResultRequestToken="/P1.Requ�te ";

