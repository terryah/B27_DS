// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRdgShootingPhotonsFrame
//
//===========================================================================

//--------------------------------------
// Gather frame
//--------------------------------------
gatherFrame.HeaderFrame.Global.Title = "Regroupement final";

gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherCheck.Title = "Actif";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherCheck.Help = "Active le regroupement final";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherCheck.ShortHelp = "Active le regroupement final";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherCheck.LongHelp =
"Active ou d�sactive le regroupement final.
Le regroupement final signifie qu'au niveau de chaque point ombr�,
l'h�misph�re situ� au-dessus est �chantillonn� par l'envoi de rayons
dans les directions appropri�es pour calculer
l'illumination indirecte, outre l'illumination directe.";

gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherAccuracyLabel.Title = "Rayons: ";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherAccuracySpinner.Help = "D�finit le nombre de rayons lanc�s";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherAccuracySpinner.ShortHelp = "Rayons lanc�s";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherAccuracySpinner.LongHelp =
"D�finit le nombre de rayons lanc�s depuis chaque 
pixel pour calculer l'illumination indirecte. 
Une valeur �lev�e augmente la pr�cision mais diminue
les performances. Si la valeur est trop faible de 
petits artefacts lumineux ou sombres apparaissent.";

gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMaxRadiusLabel.Title = "Distance maximum: ";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMaxRadiusSpinner.Help = "D�finit la distance maximum";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMaxRadiusSpinner.ShortHelp = "Distance maximum pour l'interpolation";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMaxRadiusSpinner.LongHelp =
"D�finit la distance maximale � laquelle un autre r�sultat d'�tape de regroupement final
peut �tre utilis� pour l'interpolation.
Des valeurs moins �lev�es augmentent le nombre d'�tapes de regroupement final
et augmentent la pr�cision aux d�pens des
performances. Si cette option prend la valeur 0, la distance est calcul�e
� partir de l'�tendue de la sc�ne.";

gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMinRadiusLabel.Title = "Distance minimum: ";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMinRadiusSpinner.Help = "D�finit le distance minimum";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMinRadiusSpinner.ShortHelp = "Distance minimum pour l'interpolation";
gatherFrame.IconAndOptionsFrame.OptionsFrame.gatherParametersFrame.gatherMinRadiusSpinner.LongHelp =
"Indique qu'un r�sultat de regroupement doit �tre utilis� pour
l'interpolation s'il se trouve � cette distance.
Ce param�tre tend � avoir une influence moindre sur la qualit�
de rendu que la distance maximale. Si cette option prend la valeur 0,
la distance utilis�e est de 10 % du rayon maximal.";

//--------------------------------------
// Global frame
//--------------------------------------
globalFrame.HeaderFrame.Global.Title = "Illumination globale";

globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalCheck.Title = "Actif";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalCheck.Help = "Active l'illumination globale";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalCheck.ShortHelp = "Active l'illumination globale";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalCheck.LongHelp =
"Active ou inactive l'illumination globale.
L'illumination globale est la simulation de toutes les inter-r�flexions lumineuses
dans une sc�ne (� l'exception des caustics).
Ceci inclut les effets tels que les d�bordements de couleurs entre deux
murs adjacents.
Les effets d'illumination globale sont subtils et ajoutent du r�alisme
� une sc�ne.
Le calcul de l'illumination globale n�cessite une table de photons
g�n�r�e lors d'une �tape de pr�-traitement lors de laquelle les photons
sont �mis � partir de sources lumineuses et trac�s dans
la sc�ne � l'aide de la fonction de trace de photons.
Lors de l'ombrage de la surface, la table de photons permet d'ajouter
une illumination indirecte.";

globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalAccuracyLabel.Title = "Photons : ";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalAccuracySpinner.Help = "D�finit le nombre de photons d'illumination globale utilis�s";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalAccuracySpinner.ShortHelp = "Photons d'illumination globale utilis�s";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalAccuracySpinner.LongHelp =
"D�finit le nombre maximum de photons utilis�s pour calculer
l'intensit� locale de l'illumination globale.
La valeur par d�faut est 500. Des valeurs plus �lev�es comme 2000 rendent
l'illumination globale plus douce.";

globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalRadiusLabel.Title = "Distance maximum: ";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalRadiusSpinner.Help = "D�finit la distance maximum des photons";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalRadiusSpinner.ShortHelp = "Distance maximum des photons";
globalFrame.IconAndOptionsFrame.OptionsFrame.globalParametersFrame.globalRadiusSpinner.LongHelp =
"D�finit la distance maximale o� on recherche des 
photons dans la sc�ne afin de calculer l'intensit�
locale d'illumination globale.
Si la valeur est nulle, la distance est calcul�e �
partir de l'�tendue de la sc�ne.";

//--------------------------------------
// Caustic frame
//--------------------------------------
causticFrame.HeaderFrame.Global.Title = "Caustics";

causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticCheck.Title = "Actif";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticCheck.Help = "Active les caustics";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticCheck.ShortHelp = "Active les caustics";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticCheck.LongHelp =
"Active ou d�sactive les caustics.
Les caustics sont des taches lumineuses cr��es lorsque la lumi�re
d'une source �claire une surface diffuse via
une ou plusieurs r�flexions ou transmissions.
Exemples :
- Les taches lumineuses cr��es eu fond d'une
  piscine.
- La lumi�re concentr�e par un verre d'eau.
Le calcul des caustics n�cessite une table de photons
g�n�r�e lors d'une �tape de pr�-traitement au cours de laquelle les photons
sont �mis � partir de sources lumineuses et propag�s dans la
sc�ne par lancer de photons.
Lors de l'ombrage des surfaces, cette table est utilis�e pour ajouter
une illumination indirecte.";

causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticAccuracyLabel.Title = "Photons: ";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticAccuracySpinner.Help = "D�finit le nombre de photons de caustics utilis�s";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticAccuracySpinner.ShortHelp = "Photons de caustics utilis�s";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticAccuracySpinner.LongHelp =
"D�finit le nombre maximal de photons utilis�s pour calculer
l'intensit� locale de l'�clat des caustics.
La valeur par d�faut est 100. Des valeurs plus �lev�es comme 200 rendent les caustics
moins bruit�s mais aussi plus flous et plus longs � calculer.";

causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticRadiusLabel.Title = "Distance maximum: ";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticRadiusSpinner.Help = "D�finit la distance maximum des photons";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticRadiusSpinner.ShortHelp = "Distance maximum des photons";
causticFrame.IconAndOptionsFrame.OptionsFrame.causticParametersFrame.causticRadiusSpinner.LongHelp =
"D�finit la distance maximale o� on recherche des 
photons dans la sc�ne afin de calculer l'intensit�
locale de l'�clat des caustics.
Si la valeur est nulle, la distance est calcul�e �
partir de l'�tendue de la sc�ne.";

//--------------------------------------
// Ambient frame
//--------------------------------------
ambientFrame.HeaderFrame.Global.Title = "Mat�riaux";

ambientFrame.IconAndOptionsFrame.OptionsFrame.ambientParametersFrame.ambientCheck.Title = "Ne pas utiliser l'ambiant";
ambientFrame.IconAndOptionsFrame.OptionsFrame.ambientParametersFrame.ambientCheck.Help = "D�sactive l'utilisation de l'ambiant des mat�riaux";
ambientFrame.IconAndOptionsFrame.OptionsFrame.ambientParametersFrame.ambientCheck.ShortHelp = "D�sactive l'ambiant des mat�riaux";
ambientFrame.IconAndOptionsFrame.OptionsFrame.ambientParametersFrame.ambientCheck.LongHelp =
"Active ou d�sactive l'utilisation du param�tre ambiant des mat�riaux.
Lorsqu'on utilise l'illumination indirecte, le param�tre ambiant
des mat�riaux ne devrait pas �tre utilis�.";
