// COPYRIGHT DASSAULT SYSTEMES 2003
//=============================================================================
//
// DNBAnalysisSettingsFrame
//
//=============================================================================
// Usages Notes:
//  Naming the frame in the property sheet :
//    Tools/Options/DELMIA Infrastructure/Analysis 
//
//=============================================================================
// Mar. 2003  Creation                                                      MMH
//=============================================================================

// Feedback frame
FeedbackFrame.HeaderFrame.Global.Title="Retour d'analyse";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkBeepId.Title="Signal sonore";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkBeepId.ShortHelp="Signal sonore activ�/d�sactiv�";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkBeepId.LongHelp="Cliquez sur la coche pour activer/d�sactiver le son";

FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkSyncAnlSpecsId.Title="Synchroniser les specs d'analyse";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkSyncAnlSpecsId.ShortHelp="Activer/d�sactiver la synchronisation des specs d'analyse";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkSyncAnlSpecsId.LongHelp="Cochez la case pour activer/d�sactiver la synchronisation des specs d'analyse";

// analysis mode activation frame
AnlModeFrame.HeaderFrame.Global.Title="Activation du mode analyse";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkAskAnlModeId.Title="Demander la permission d'activer le mode analyse";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkAskAnlModeId.ShortHelp="Demander la permission d'activer le mode analyse";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkAskAnlModeId.LongHelp="Cliquez sur la coche pour activer/d�sactiver la bo�te de dialogue";

AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkEnableAnlModeId.Title="Activation automatique du mode analyse";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkEnableAnlModeId.ShortHelp="Activer automatiquement le mode analyse";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkEnableAnlModeId.LongHelp="Cliquez sur la coche pour activer/d�sactiver l'activation automatique du mode analyse";

AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkDisplayAnlStatusId.Title="Afficher la fen�tre du statut d'analyse";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkDisplayAnlStatusId.ShortHelp="Afficher automatiquement la fen�tre du statut d'analyse";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkDisplayAnlStatusId.LongHelp="Cliquez sur la coche pour activer/d�sactiver l'affichage de la fen�tre du statut d'analyse.";

// analysis status frame
AnlStatusFrame.HeaderFrame.Global.Title="Niveau d'analyse";
AnlStatusFrame.IconAndOptionsFrame.OptionsFrame.ComboAnlStatusId.ShortHelp="Niveau d'analyse par d�faut";
AnlStatusFrame.IconAndOptionsFrame.OptionsFrame.ComboAnlStatusId.LongHelp="Sp�cifie la valeur par d�faut du niveau d'analyse";

// visualization frame
VisuModeFrame.HeaderFrame.Global.Title="Mode visualisation";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboVisuId.ShortHelp="Mode de visualisation par d�faut";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboVisuId.LongHelp="S�lectionne le mode d'affichage des r�sultats de l'analyse de collision";

VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkColorMode.Title="Mettre en �vidence l'interf�rence et le d�gagement avec changement de couleur";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkColorMode.ShortHelp="Mettre en �vidence l'interf�rence et le d�gagement avec changement de couleur";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkColorMode.LongHelp="Mettre en �vidence l'interf�rence et le d�gagement avec changement de couleur";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkWhiteMode.Title="Pi�ces par d�faut � Blanc";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkWhiteMode.ShortHelp="Pi�ces par d�faut � Blanc";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkWhiteMode.LongHelp="Pi�ces par d�faut � Blanc";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.LabelClashColor.Title="Couleur de collision";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClashColor.ShortHelp="Couleur de collision";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClashColor.LongHelp="Couleur de collision";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.LabelClearanceColor.Title="Couleur de d�gagement";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClearanceColor.ShortHelp="Couleur de d�gagement";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClearanceColor.LongHelp="Couleur de d�gagement";

// Voxel frame
VoxelFrame.HeaderFrame.Global.Title="Analyse statique";
VoxelFrame.IconAndOptionsFrame.OptionsFrame.CkVoxelId.Title="Utiliser des voxels";
VoxelFrame.IconAndOptionsFrame.OptionsFrame.CkVoxelId.ShortHelp="Activer/d�sactiver les voxels pendant l'analyse statique (d�tection des collisions)";
VoxelFrame.IconAndOptionsFrame.OptionsFrame.CkVoxelId.LongHelp="Indiquez si les voxels doivent �tre utilis�s pour la d�tection des collisions pendant l'analyse statique";

// Analysis Type Window

AnlTypeID="Type d'analyse";
AnlActiveID="Active";

AnlIntDistId.ActType="Analyse de distance";
AnlIntfId.ActType="Analyse d'interf�rence";
AnlMeasureId.ActType="Analyse de mesure";
AnlTypeTravelId.ActType="Limite du trajet";
AnlTypeVelocityId.ActType="Limite de v�locit�";
AnlTypeAccelId.ActType="Limite d'acc�l�ration";
AnlTypeCautionId.ActType="Zone � restrictions";
AnlTypeLinSpeedId.ActType="Limite de vitesse lin�aire";
AnlTypeRotSpeedId.ActType="Limite de vitesse angulaire";
AnlTypeLinAccelId.ActType="Limite d'acc�l�ration lin�aire";
AnlTypeRotAccelId.ActType="Limite d'acc�l�ration angulaire";
AnlTypeIOAnalysisId.ActType="En attente du statut de ressource";
