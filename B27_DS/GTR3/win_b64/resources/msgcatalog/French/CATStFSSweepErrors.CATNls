//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES  1999
//==============================================================================
//
// CATStFSSweepErrors: Resource file for NLS purpose related to sweep errors 
//
//==============================================================================
//
// Implementation Notes:
//
//==============================================================================
// Feb 00   Creation                                    F. LETZELTER
//==============================================================================
UpdateError="Une erreur interne s'est produite lors du calcul du balayage de style.
Il n'y a pas de solution pour cette op�ration.";
GeoFactoryError.Message="Impossible de r�cup�rer la factory g�om�trique.";
ProfileRecupError.Message="Impossible de r�cup�rer le profil.";
RefProfilesRecupError.Message="Impossible de r�cup�rer les profils de r�f�rence.";
SpineRecupError.Message="Impossible de r�cup�rer la courbe de contr�le.";
ContProfileRecupError.Message="Impossible de r�cup�rer la continuit� du profil.";
ContSpineRecupError.Message="Impossible de r�cup�rer la continuit� de la courbe de contr�le.";
ContGuideRecupError.Message="Impossible de r�cup�rer la continuit� de la courbe guide.";
MFTypeRecupError.Message="Impossible de r�cup�rer le type de tri�dre de balayage.";
MFLawRecupError.Message="Impossible de r�cup�rer la loi du tri�dre de balayage.";
FirstLPRecupError.Message="Impossible de r�cup�rer le premier point limite.";
SecondLPRecupError.Message="Impossible de r�cup�rer le second point limite.";
MaxOrdersRecupError.Message="Impossible de r�cup�rer les ordres max.";
MaxToleranceRecupError.Message="Impossible de r�cup�rer la tol�rance max�.";
OpCreationError.Message="Une erreur est survenue pendant la cr�ation de l'op�rateur.";
OpAllocationError.Message="Une erreur est survenue pendant l'allocation de l'op�rateur.";
ProfileBodyError.Message="Impossible de r�cup�rer le corps sous le profil.";
SpineBodyError.Message="Impossible de r�cup�rer le corps sous la courbe de contr�le.";
SpineSupportBodyError.Message="Impossible de r�cup�rer le corps de la surface support de la courbe de contr�le.";
GuideBodyError.Message="Impossible de r�cup�rer le corps de la courbe guide.";
GuideSupportBodyError.Message="Impossible de r�cup�rer le corps de la surface support de la courbe guide.";
ParamOpError.Message="Une erreur est survenue pendant l'initialisation des param�tres de l'op�rateur.";
CheckRunErrorM6.Message="Incompatibilit� entre diff�rents coins de la surface.";
CheckRunErrorM5.Message="Aucune solution n'est possible dans cette configuration (v�rifiez les contraintes de continuit�).";
CheckRunErrorM4.Message="Tol�rance non atteinte.";
CheckRunErrorM3.Message="Le calcul s'est arr�t� avant la fin du processus peut-�tre � cause d'une d�g�n�rescence.";
CheckRunErrorM2.Message="Un des profils de r�f�rence n'est connect� ni en tangence ni en courbure avec la surface support de la courbe de contr�le.";
CheckRunErrorM1.Message="Un des profils de r�f�rence n'est connect� ni en tangence ni en courbure avec la surface support du guide.";
CheckRunError1.Message="La partie libre de la courbe de contr�le n'est pas connect�e en tangence avec sa surface de support partielle.";
CheckRunError2.Message="La partie libre du guide n'est connect�e en tangence avec sa surface support partielle.";
CheckRunError3.Message="La partie libre du profil n'est connect�e en tangence avec sa surface support partielle.";
CheckRunError4.Message="Il n'y a pas de connexion entre le profil et la premi�re surface verticale de connexion.";
CheckRunError5.Message="Il n'y a pas de connexion entre le profil et la seconde surface verticale de connexion.";
CheckRunError6.Message="Un des profils (profil ou profil de r�f�rence) n'est pas situ� sur la
surface de support de la courbe de contr�le ou du guide.";
CheckRunError7.Message="La surface verticale de connexion n'est pas une grille.";
CheckRunError8.Message="Les deux points limites ne sont pas de part et d'autre du profil.";
CheckRunError9.Message="Un des contours n'est pas continu en point sur toutes ses courbes internes.";
CheckRunError10.Message="Le profil est r�duit � un point entre la courbe de contr�le et le guide. Le guide et le profil sont peut-�tre confondus.";
CheckRunError11.Message="Donn�es en entr�e erron�es. V�rifiez la taille des entr�es.
Notez que les entr�es doivent se rapporter � un seul domaine. Utilisez la barre d'outils Filtres de s�lection
pour s�lectionner une pi�ce avec des �l�ments multi-domaines.";
CheckRunError12.Message="Le param�tre de direction est incorrect : il est soit nul soit non perpendiculaire � la direction tangente au profil.";
CheckRunError13.Message="Les contraintes g�om�triques sont incompatibles avec les contraintes de connexion requises.";
CheckRunError14.Message="La surface support du profil ou de la courbe de contr�le est en plusieurs morceaux.";
CheckRunError15.Message="La construction est impossible dans cette situation.
Essayez un autre d�placement de cadre et v�rifiez les contraintes sur le syst�me.";
CheckRunError16.Message="Il n'y a pas d'intersection entre la courbe de contr�le et un des profils (profil ou profil de r�f�rence).";
CheckRunError17.Message="Il n'y a pas d'intersection entre le guide et un des profils (profil ou profil de r�f�rence).";
CheckRunError18.Message="Votre guide n'est pas un plan.";
CheckRunError19.Message="Il n'y a pas de surface support soit sous le guide soit sous le profil.";
CheckRunError20.Message="Il est impossible de cr�er le balayage de style. 
Le profil est probablement une droite ou un cercle au niveau de l'intersection avec le guide.
Essayez d'utiliser l'option Balayage ajust� sur guide.";
CheckRunError21.Message="La premi�re surface de connexion n'est ni G1 ni G2 avec la surface support du profil (au point de contact).";
CheckRunError22.Message="La seconde surface de connexion n'est ni G1 ni G2 avec la surface support du profil (au point de contact).";
CheckRunError23.Message="Le profil ou un des profils de r�f�rence n'est connect� ni en tangence ni en courbure avec la surface support de la courbe de contr�le.";
CheckRunError24.Message="Le profil ou un des profils de r�f�rence n'est connect� ni en tangence ni en courbure avec la surface support du guide.";
CheckRunError25.Message="La courbe de contr�le n'est pas connect�e en tangence avec la surface de support du profil.";
CheckRunError26.Message="Le guide n'est pas connect�e G1 avec la surface support du profil.";
