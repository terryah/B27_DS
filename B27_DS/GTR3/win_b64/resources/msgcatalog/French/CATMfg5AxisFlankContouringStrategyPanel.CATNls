SelStrategy="Sp�cifier la strat�gie...";
MfgMachiningTabPage="Usinage";
MfgStepOverTabPage="Multi Passes";
MfgFinishingTabPage="Finition";
MfgToolAxisTabPage="Axe outil";
MfgHSMTabPage="UGV";

MfgToolAxisStrategy="Mode";
MfgToolAxisStrategy.LongHelp="Sp�cifie le mode de guidage de l'axe outil";
MfgContactHeight="Hauteur de contact";
MfgContactHeight.LongHelp="D�termine un point sur la surface de guidage o� le c�t� de l'outil
doit respecter les conditions de tangence.
La hauteur de contact est mesur�e de l'extr�mit� de l'outil
suivant l'axe outil. Le point est tel que sa
projection de la surface de guidage le long de l'axe outil
respecte la hauteur de contact sp�cifi�e.";
Mfg5AxisUserDefinedCuttingHeightFlag="Contr�ler le basculement de l'axe gr�ce � un param�tre outil";
Mfg5AxisUserDefinedCuttingHeightFlag.LongHelp="Cocher la case pour sp�cifier une hauteur de coupe utile sur l'outil
choisi. Autrement, la valeur standard par d�faut (Lc) sera utilis�e.";
Mfg5AxisUserDefinedCuttingHeight="Hauteur de coupe utile";
Mfg5AxisUserDefinedCuttingHeight.LongHelp="Sp�cifie une hauteur de coupe utile pour l'outil choisi. Ce param�tre
peut servir � contr�ler l'inclinaison de l'outil ou la distance de d�calage lors de l'approche
d'une surface en contre-d�pouille, sans modification de l'outil choisi dans le catalogue.
Par exemple, lors de l'usinage d'une surface de guidage, en s'approchant d'une surface de
contr�le en contre-d�pouille, plus la hauteur sp�cifi�e est petite, plus l'outil aura tendance
� s'incliner tard avant se positionner sur la surface de contr�le.";
MfgLeaveDistance="Distance de d�part";
MfgLeaveDistance.LongHelp="Sp�cifie une distance avant le d�but du mouvement
au-dessus de laquelle le basculement de l'outil commence.";
MfgApproachDistance="Distance d'arriv�e";
MfgApproachDistance.LongHelp="Sp�cifie une distance apr�s la fin du mouvement
au-dessus de laquelle le basculement de l'outil commence.";
Mfg5AxisDeActivateFanning="D�sactiver le basculement outil";
Mfg5AxisDeActivateFanning.LongHelp="Sp�cifie le mode de basculement de l'axe outil.";

MfgReUseFirstDrive="R�-utiliser le 1er flanc";
MfgReUseFirstDrive.LongHelp="Pour un contour ferm�, cocher la case pour sp�cifier
que le premier flanc sera aussi utilis� comme dernier flanc.";
MfgManualTMDir="Direction manuelle";
MfgManualTMDir.LongHelp="Sp�cifie la direction sur le flanc en d�but de trajectoire. 'Auto' laisse l'algorithme d�terminer la
direction en fonction du point de r�f�rence.";
Mfg5AxisMachDirMode="Vers";
Mfg5AxisMachDirMode.LongHelp="Permet de sp�cifier manuellement la direction sur le flanc.";

MfgMachToler="Tol�rance d'usinage";
MfgMachToler.LongHelp="Tol�rance d'usinage";
MfgMaxDistSteps="Distance maximum entre positions";
MfgMaxDistSteps.LongHelp="D�finit la distance maximale entre deux positions. En interne, cette valeur est utilis�e en tant qu'ordre de grandeur de la longueur d'un trajet outil le long
d'une surface de guidage. Cela aide l'algorithme � d�tecter l'extr�mit� des �l�ments de guidage. Si une m�me configuration g�om�trique contient des surfaces de guidage de longueurs tr�s
diff�rentes, il est fortement recommand� d'entrer la longueur approximative de la plus grande.";
MfgMaxDiscretizationStep="Pas de discr�tisation maxi";
MfgMaxDiscretizationStep.LongHelp="Sp�cifie le pas maximum de discr�tisation tol�r� utilis� pour assurer la lin�arit� entre positions tr�s espac�es.
Si le calcul de trajectoire th�orique donne des couples de positions adjacentes (x,y,z) dont la distance est sup�rieure au pas tol�r�
par la machine, alors elle ne saura pas effectuer d'interpolation lin�aire. Pour �viter cela, une correction est effectu�e afin d'obtenir
davantage de positions, donc s�par�es par des pas moindres.";
MfgMaxDiscretizationAngle="Angle de discr�tisation maxi";
MfgMaxDiscretizationAngle.LongHelp="Sp�cifie l'angle maximum de discr�tisation tol�r� entre 2 positions cons�cutives pour assurer que la machine saura passer
de l'une � l'autre. Si le calcul de trajectoire th�orique donne des couples de positions adjacentes (i,j,k) formant un angle sup�rieur
� celui tol�r� par la machine, alors elle ne saura pas effectuer d'interpolation. Pour �viter cela, une correction est effectu�e afin d'obtenir
davantage de positions, donc formant des angles moindres.";

MfgDirectionOfCut="Sens d'usinage";
MfgDirectionOfCut.LongHelp="Sp�cifie le sens d'usinage :
- En avalant : la partie avant de l'outil (selon la direction d'usinage) coupe la mati�re en premier
- En opposition : la partie arri�re de l'outil (selon la direction d'usinage) coupe la mati�re en premier ";

MfgMachiningMode="Style d'usinage";
MfgMachiningMode.LongHelp="Indique le style d'usinage de l'op�ration :
- Zig Zag : la direction d'usinage est invers�e
  d'un trajet au suivant
- Aller simple : la direction d'usinage est conserv�e
  d'un trajet au suivant.";
MfgSequencingMode="S�quencement";
MfgSequencingMode.LongHelp="Sp�cifie l'ordre dans lequel l'usinage est effectu� :
- Axial d'abord: l'usinage axial est effectu� d'abord, puis l'usinage radial
- Radial d'abord: l'usinage radial est effectu� d'abord, puis l'usinage axial";

RadialStrategyFrame="Prise de passe radiale";
MfgDistanceBetweenPaths="Distance entre passes";
MfgDistanceBetweenPaths.LongHelp="D�finit la distance maximale entre 
deux trajets cons�cutifs dans une prise de passe radiale.";
MfgNumberOfRadialPaths="Nombre de passes";
MfgNumberOfRadialPaths.LongHelp="D�finit le nombre de trajets outil dans une prise de passe radiale.";

AxialStrategyFrame="Prise de passe axiale";
Mfg5AxisMultiLevelMode="Mode";
Mfg5AxisMultiLevelMode.LongHelp="D�finit la strat�gie de calcul des passes axiales.";
MfgMaxDepthOfCut="Distance entre passes";
MfgMaxDepthOfCut.LongHelp="D�finit la distance maximale entre 
deux trajets cons�cutifs dans une prise de passe axiale.";
MfgNumberOfLevels="Nombre de niveaux";
MfgNumberOfLevels.LongHelp="D�finit le nombre de niveaux � usiner
dans une prise de passe axiale.";

Mfg5AxisOutputType="Sortie compens�e";
Mfg5AxisOutputType.LongHelp="Sp�cifie comment les instructions de compensation outil sont g�n�r�es dans les fichiers de sortie CN : Aucune
La compensation n'est pas calcul�e.

Radial 2D - extr�mit�
La compensation est calcul�e dans un plan normal � l'axe outil, et activ�e par rapport au c�t� droit ou gauche de l'outil.
Le rayon compens� est celui de l'outil.
Le point extr�mit� de l'outil (XT, YT, ZT) est donn� en sortie.

Radial 3D
La compensation est calcul�e selon un vecteur 3D (PQR), normale � la surface de suivi, en contact avec le flanc de l'outil.
Le rayon compens� est celui de l'outil.
Le point extr�mit� de l'outil (XT, YT, ZT) et la normale � la surface de suivi (PQR) sont donn�s en sortie. Le vecteur de l'axe outil (IJK) est sortie en multi-axes.";

MfgLocal4XPlane="Plan 4X optionnel";
SelLocal4XPlane="Cliquer pour s�lectionner une direction 4 axes optionnelle \nutilis�e dans les modifications locales.";

MfgToolSideOnGuideCurve="Position sur contour de guidage";
MfgToolSideOnGuideCurve.LongHelp="Sp�cifie la position de l'outil sur le contour de guidage.";

MfgOffsetOnGuideCurve="Sur�paisseur sur contour de guidage";
MfgOffsetOnGuideCurve.LongHelp="Sp�cifie la sur�paisseur sur le contour de guidage.";

MfgUseGuideCurve="Utilisation du contour de guidage";
MfgUseGuideCurve.LongHelp="Sp�cifie le mode de contact de l'outil avec le contour de guidage: \n- Toujours.\n- Si besoin (uniquement si risque de collision avec les flancs)";

MfgCollisionThkStart="Inclinaison lat�rale";
MfgCollisionThkStart.LongHelp="Sp�cifie l'inclinaison lat�rale";

MfgCollisionThkEnd="Angle de d�talonnage";
MfgCollisionThkEnd.LongHelp="Sp�cifie l'angle de d�talonnage";

MfgAutoContinuing="Nbre d'intersections";
MfgAutoContinuing.LongHelp="Sp�cifier le nombre d'intersections (entre le d�but/la fin et les �l�ments de guidage) qui a �t� pris en compte";

Mfg5XMachineParts="Usiner les �l�ments de guidage && les pi�ces";
Mfg5XMachineParts.LongHelp="Activer ce bouton pour usiner � la fois les �l�ments de guidage et les pi�ces";

MfgTgtParts="Tgt � pi�ces";

