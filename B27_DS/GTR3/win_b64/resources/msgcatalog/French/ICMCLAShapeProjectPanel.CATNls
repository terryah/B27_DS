DialogBoxTitle="Projection de courbe";

CheckButtonCurve.Title="El�ment : ";
CheckButtonCurve.LongHelp="S�lection de la ou des courbes � projeter.
La s�lection multiple et le menu contextuel sont disponibles.";
CheckButtonCurve.ShortHelp="S�lectionnez les courbes � projeter";

CheckButtonPoint.Title="Point : ";
CheckButtonPoint.LongHelp="S�lection des points � projeter.
La s�lection multiple et le menu contextuel sont disponibles.";
CheckButtonPoint.ShortHelp="S�lectionnez les points � projeter";

LabelSupport.Title="Support : ";
LabelSupport.LongHelp="S�lection des �l�ments de support sur lesquels la courbe sera projet�e.
Le support peut �tre un ensemble de surfaces, de plans ou un scan.
La s�lection multiple et le menu contextuel sont disponibles.";
LabelSupport.ShortHelp="S�lectionner un support de projection";

// TabContainer
// . Approx
TabPageApprox.Title="Approximation";
// . Options
TabPageOptionsR19HF10.Title="Options";
TabOutput.Title="Sortie";

FrameExtrapolation.Title="Extrapolation";
FrameExtrapolation.LongHelp="Uniquement disponible si une surface est s�lectionn�e comme support.
Les extr�mit�s de la courbe projet�e sont automatiquement
extrapol�es � l'ar�te la plus proche et relimit�es.
Avec G1-G3, la continuit� de l'extension
peut �tre d�finie pour les extr�mit�s de la courbe.";
Start.LongHelp="Extrapolation de la courbe de projection au
point de d�part (S) avec continuit� G1, G2 ou G3.";
Start.ShortHelp="Extrapolation au point de d�part S";
End.LongHelp="Extrapolation de la courbe de projection au
point d'arriv�e (E) avec continuit� G1, G2 ou G3.";
End.ShortHelp="Extrapolation au point d'arriv�e E";
Continuity_Start.Button_G1.LongHelp="Au point de d�part S, la courbe de projection est
extrapol�e tangentiellement � l'ar�te de la surface.
Pour l'extension, une nouvelle cellule est cr��e.";
Continuity_Start.Button_G1.ShortHelp="Extrapolation G1 au point de d�part S de la courbe";
Continuity_Start.Button_G2.LongHelp="Au point de d�part S, la courbe de projection est extrapol�e
en courbure continue (G2) � l'ar�te de la surface.
Pour l'extension, un nouveau segment est cr��.";
Continuity_Start.Button_G2.ShortHelp="Extrapolation G2 au point de d�part S de la courbe";
Continuity_Start.Button_G3.LongHelp="Plut�t que la courbe de projection, la courbe d'origine est d'abord
extrapol�e en son point de d�part S puis projet�e sur le support.
Dans ce cas, aucun nouveau segment ne doit �tre cr��.";
Continuity_Start.Button_G3.ShortHelp="Extrapolation G3 au point de d�part S de la courbe";
Continuity_End.Button_G1.LongHelp="Au point d'arriv�e E, la courbe de projection est
extrapol�e tangentiellement � l'ar�te de la surface.
Pour l'extension, une nouvelle cellule est cr��e.";
Continuity_End.Button_G1.ShortHelp="Extrapolation G1 au point d'arriv�e E de la courbe";
Continuity_End.Button_G2.LongHelp="Au point d'arriv�e E, la courbe de projection est extrapol�e
en courbure continue (G2) � l'ar�te de la surface.
Pour l'extension, un nouveau segment est cr��.";
Continuity_End.Button_G2.ShortHelp="Extrapolation G2 au point d'arriv�e E de la courbe";
Continuity_End.Button_G3.LongHelp="Plut�t que la courbe de projection, la courbe d'origine est d'abord
extrapol�e en son point d'arriv�e E puis projet�e sur le support.
Dans ce cas, aucun nouveau segment n'est cr��.";
Continuity_End.Button_G3.ShortHelp="Extrapolation G3 au point d'arriv�e E de la courbe";
// .. Switches
// ... Buttons
FrameSwitches.Title="Param�tre";
FrameSwitches.LongHelp="Param�trage de la courbe de projection.";
CheckButtonBasicSurface.Title="Surface de support";
CheckButtonBasicSurface.LongHelp="Si le support est une face, les �l�ments
sont projet�s sur la surface de support.";
CheckButtonBasicSurface.ShortHelp="Projeter sur la surface de support";
CheckButtonTangent.Title="Tangent";
CheckButtonTangent.LongHelp="Si cette option est activ�e, la tangence au point d'extr�mit� de d�but resp. est respect�e.
Dans le cas contraire, la tangence est ignor�e.";
CheckButtonTangent.ShortHelp="Respecte la tangence pendant l'approximation.";
LabelDomain.Title="Domaine : ";
LabelDomain.LongHelp="Les options suivantes sont disponibles :
 - D�sactiv� -
   Le param�trage des courbes � projeter est calcul�
   sur l'ensemble des courbes ind�pendamment des surfaces de support s�lectionn�es.
 - S�par� -
   La courbe est projet�e sur chaque surface d'un groupe de surfaces s�lectionn�.
 - Diviser -
   Le param�trage des courbes est calcul� sur l'ensemble de la courbe,
   mais le r�sultat est divis� � chaque limite de la surface de support.
 - Courbe 2D -
   La courbe 2D est une courbe NURBS param�tr�e.";
LabelDomain.ShortHelp="D�finissez le domaine: D�sactiv�, S�par�, Diviser ou Courbe 2D.";
Combo.Off="D�sactiv�";
Combo.Separate="S�par�";
Combo.Divide="Diviser";
Combo.2DCurve="Courbe 2D";
Combo.Standard="Norme";
Combo.Adapt="Adopter";
ComboDomain.Title="Domaine";
ComboDomain.LongHelp="Les options suivantes sont disponibles :
 - D�sactiv� -
   Le param�trage des courbes � projeter est calcul�
   sur l'ensemble des courbes ind�pendamment des surfaces de support s�lectionn�es.
 - S�par� -
   La courbe est projet�e sur chaque surface d'un groupe de surfaces s�lectionn�.
 - Diviser -
   Le param�trage des courbes est calcul� sur l'ensemble de la courbe,
   mais le r�sultat est divis� � chaque limite de la surface de support.
 - Courbe 2D -
   La courbe 2D est une courbe NURBS param�tr�e.";

ComboStandard.Title="Norme";
ComboStandard.LongHelp="Param�trage du r�sultat de la projection.
- Norme - 
  Le param�trage de la courbe projet�e est 
  calcul� ind�pendamment du support.
 - Adapter -
  Le param�trage de la courbe projet�e
  est calcul� en prenant le support en compte.";

// .. Distance
CheckButtonMaxDist.Title="Dist. max. : ";
CheckButtonMaxDist.LongHelp="Lors de la projection d'une courbe sur une surface, le calcul ne prend en compte que les points
dont la distance � partir de la courbe d'origine est inf�rieure
� la valeur indiqu�e ici.";
CheckButtonMaxDist.ShortHelp="Distance maximale de projection";

// Result
CheckButtonDisplayDeviations.Title="Affichage";
CheckButtonDisplayDeviations.LongHelp="La d�viation maximale entre la courbe projet�e
et le support s'affiche dans la zone graphique.
La valeur s'affiche � la position o� la plus grande d�viation se produit.";
CheckButtonDisplayDeviations.ShortHelp="Afficher la d�viation max. dans la fen�tre graphique";

// . Deviation
FrameDeviation.Title="D�viation";
FrameDeviation.LongHelp="Max est la valeur de la distance maximale entre
le r�sultat de la projection et le support.";
FrameDeviation.ShortHelp="Distance maximale entre le r�sultat et le support";
LabelMaxDeviation.Title="Max. : ";
LabelMaxDeviation.LongHelp="Max est la valeur de la distance maximale entre
le r�sultat de la projection et le support.";
LabelMaxDeviation.ShortHelp="Distance maximale entre le r�sultat et le support";

CheckButtonMultiResultOutput.Title="Multi-R�sultat";
CheckButtonTopology.Title="V�rification topologique";

AdaptModeOld2New.Title="Le mode de param�trage Adapt a �t� transform� en Standard. Le mode bascule automatique sur le mode de param�trisation standard en cas d'�chec d'adaptation n'est plus pris en charge.";

AdaptModeNew2Old.Title="Cette fonctionnalit� a �t� cr��e dans un environnement plus r�cent.
Le r�sultat peut changer dans l'environnement actuel.
Il est conseill� d'utiliser le nouvel environnement pour cette fonctionnalit�.";

