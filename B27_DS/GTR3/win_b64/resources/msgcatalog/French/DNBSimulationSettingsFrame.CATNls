// COPYRIGHT DASSAULT SYSTEMES Delmia 2000 
//=============================================================================
//
// DNBSimulationSettingsFrame
//
//=============================================================================
// Usages Notes:
//  Naming the frame in the property sheet :
//    Tools/Options/Infrastructure/DELMIA Infrastructure/Simulation 
//
//=============================================================================
// Dec. 2000  Creation                                                      MMH
// Mar. 2003  Modify: Remove Beep, add Dynamic clash                        MMH
// Aug. 2003  Modify: Add State Management, Update Cycle Time               MMH
// Sep  2004  Added enable / disable annotation activities                  SMW
// Apr. 2005  Added enable / disable End Condition                          RTL
//=============================================================================

// Build time frame
SimuFrame.HeaderFrame.Global.Title = "Cr�ation de proc�d�";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckStateMgtId.Title     = "Gestion d'�tat";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckStateMgtId.ShortHelp = "Activer/d�sactiver la gestion d'�tat";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckStateMgtId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver la gestion d'�tat";

SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckEndConditionId.Title     = "Appliquer la condition de fin";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckEndConditionId.ShortHelp = "Active/D�sactive la condition de fin";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckEndConditionId.LongHelp  = "Cochez la case pour activer/d�sactiver la condition de fin";

SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckTextId.Title     = "Activit�s de message texte          ";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckTextId.ShortHelp = "Activer/d�sactiver les activit�s de message texte";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckTextId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s de message texte";

SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckViewId.Title     = "Activit�s de point de vue";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckViewId.ShortHelp = "Activer/d�sactiver les activit�s de point de vue";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckViewId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s de point de vue";

SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckHlnkId.Title     = "Activit�s d'hyperlien";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckHlnkId.ShortHelp = "Activer/d�sactiver les activit�s d'hyperlien";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckHlnkId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s d'hyperlien";

SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckAnnotationId.Title     = "Activit�s d'annotation";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckAnnotationId.ShortHelp = "Activer/d�sactiver les activit�s d'annotation";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckAnnotationId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s d'annotation";

SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckDisableId.Title     = "D�sactiver la synchronisation";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckDisableId.ShortHelp = "Activer/d�sactiver la d�sactivation de la synchronisation";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckDisableId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver la synchronisation";

SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckSelAgentId.Title     = "Afficher le dialogue pour l'agent de s�lection d'activit�";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckSelAgentId.ShortHelp = "Activer/d�sactiver l'affichage du dialogue pour l'agent de s�lection d'activit�";
SimuFrame.IconAndOptionsFrame.OptionsFrame.CheckSelAgentId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver l'affichage du dialogue pour l'agent de s�lection d'activit�";

// AutoSync frame
AutoSyncFrame.HeaderFrame.Global.Title = "Synchronisation automatique";
AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.RadioStepId.Title     = "Pas";
AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.RadioStepId.ShortHelp = "Mode pas";
AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.RadioStepId.LongHelp  = "Utiliser le mode pas pendant la synchronisation automatique";

AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.RadioAnimId.Title     = "Animation de bonne qualit�";
AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.RadioAnimId.ShortHelp = "Mode animation de bonne qualit�";
AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.RadioAnimId.LongHelp  = "Utiliser le mode animation de bonne qualit� pendant la synchronisation automatique";

AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.SpinnerStepSizeId.Title     = "Pas :";
AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.SpinnerStepSizeId.ShortHelp = "Pas par d�faut";
AutoSyncFrame.IconAndOptionsFrame.OptionsFrame.SpinnerStepSizeId.LongHelp  = "S�lectionner la valeur de pas par d�faut";

// Run time frame
RunTimeFrame.HeaderFrame.Global.Title = "Options de simulation courantes";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckStateMgtId.Title     = "Gestion d'�tat";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckStateMgtId.ShortHelp = "Activer/d�sactiver la gestion d'�tat";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckStateMgtId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver la gestion d'�tat";

RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckEndConditionId.Title     = "Appliquer la condition de fin";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckEndConditionId.ShortHelp = "Active/D�sactive la condition de fin";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckEndConditionId.LongHelp  = "Cochez la case pour activer/d�sactiver la condition de fin";

RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckTextId.Title     = "Activit�s de message texte";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckTextId.ShortHelp = "Activer/d�sactiver les activit�s de message texte";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckTextId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s de message texte";

RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckViewId.Title     = "Activit�s de point de vue";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckViewId.ShortHelp = "Activer/d�sactiver les activit�s de point de vue";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckViewId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s de point de vue";

RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckHlnkId.Title     = "Activit�s d'hyperlien";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckHlnkId.ShortHelp = "Activer/d�sactiver les activit�s d'hyperlien";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckHlnkId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s d'hyperlien";

RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckAnnotationId.Title     = "Activit�s d'annotation";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckAnnotationId.ShortHelp = "Activer/d�sactiver les activit�s d'annotation";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckAnnotationId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s d'annotation";

RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckPauseId.Title     = "Activit�s de simulation de pause";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckPauseId.ShortHelp = "Activer/d�sactiver les activit�s de simulation de pause";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunCheckPauseId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver les activit�s de simulation de pause";

RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunSpinStepSizeId.Title     = "Pas :";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunSpinStepSizeId.ShortHelp = "Pas par d�faut";
RunTimeFrame.IconAndOptionsFrame.OptionsFrame.RunSpinStepSizeId.LongHelp  = "S�lectionner la valeur de pas par d�faut";
RunStepSizeTitle = "Pas ";

// UI frame
UserInterfaceFrame.HeaderFrame.Global.Title = "Interface Utilisateur";
UserInterfaceFrame.IconAndOptionsFrame.OptionsFrame.UICheckSimTlbId.Title     = "Passer en style de barre d'outils";
UserInterfaceFrame.IconAndOptionsFrame.OptionsFrame.UICheckSimTlbId.ShortHelp = "Cocher pour la barre d'outils, d�cocher pour le dialogue";
UserInterfaceFrame.IconAndOptionsFrame.OptionsFrame.UICheckSimTlbId.LongHelp  = "Cocher pour utiliser le style barre d'outils, d�cocher pour utiliser le style dialogue";

// Dynamic Clash frame
DynClashFrame.HeaderFrame.Global.Title = "Simulation de proc�d�";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.LabelDynClashId.Title = "Collision dynamique ";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.ComboDynClashId.ShortHelp = "Mode de collision automatique";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.ComboDynClashId.LongHelp  = "S�lectionner le mode pour afficher une collision dynamique pendant la simulation";

DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSCheckCycleTimeId.Title     = "Mise � jour du temps de cycle";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSCheckCycleTimeId.ShortHelp = "Activer/d�sactiver la mise � jour du temps de cycle";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSCheckCycleTimeId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver la mise � jour du temps de cycle";

DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSRadioGraphUpdateId.Title     = "Inactif";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSRadioGraphUpdateId.ShortHelp = "D�sactiver les mises � jour graphiques";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSRadioGraphUpdateId.LongHelp  = "D�sactiver les mises � jour graphiques lors de la simulation";

DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSRadioGraphStepsId.Title     = "Etapes de simulation :";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSRadioGraphStepsId.ShortHelp = "Etapes par mise � jour graphique";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSRadioGraphStepsId.LongHelp  = "Etapes par mise � jour graphique lors de la simulation";

DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSEditorGraphStepsId.ShortHelp = "Entrer les �tapes par mise � jour graphique";
DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSEditorGraphStepsId.LongHelp = "Entrez les �tapes par mise � jour graphique lors de la simulation";

DynClashFrame.IconAndOptionsFrame.OptionsFrame.PSLabelGraphUpdateId.Title = "Mises � jour graphiques :";

// Process Verification frame
ProcVerifFrame.HeaderFrame.Global.Title = "V�rification du proc�d�";
ProcVerifFrame.IconAndOptionsFrame.OptionsFrame.PVCheckStateId.Title     = "Gestion d'�tat(local)";
ProcVerifFrame.IconAndOptionsFrame.OptionsFrame.PVCheckStateId.ShortHelp = "Activer/d�sactiver la gestion d'�tat dans PV";
ProcVerifFrame.IconAndOptionsFrame.OptionsFrame.PVCheckStateId.LongHelp  = "Cliquez sur la coche pour activer/d�sactiver la gestion d'�tat dans la v�rification du proc�d�";

ProcVerifFrame.IconAndOptionsFrame.OptionsFrame.PVCheckEndConditionId.Title     = "Appliquer la condition de fin (local)";
ProcVerifFrame.IconAndOptionsFrame.OptionsFrame.PVCheckEndConditionId.ShortHelp = "Active/D�sactive la condition de fin dans PV";
ProcVerifFrame.IconAndOptionsFrame.OptionsFrame.PVCheckEndConditionId.LongHelp  = "Cochez la case pour activer/d�sactiver la condition de fin dans la v�rification du proc�d�";

AuthActTypeID = "Type d'activit�";
AuthEnabledID = "Activ�";

RunActTypeID = "Type d'activit�";
RunEnabledID = "Activ�";
RunBehaviorID = "Comportement";

AuthTextId.ActType = "Activit�s de message texte";
AuthViewId.ActType = "Activit�s de point de vue";
AuthHlnkId.ActType = "Activit�s d'hyperlien";
AuthAnnotId.ActType = "Activit�s d'annotation";

RunTextId.ActType = "Activit�s de message texte";
RunViewId.ActType = "Activit�s de point de vue";
RunHlnkId.ActType = "Activit�s d'hyperlien";
RunAnnotId.ActType = "Activit�s d'annotation";
RunPauseId.ActType = "Activit�s de simulation de pause";
RunVisId.ActType = "Activit�s de visibilit�";
