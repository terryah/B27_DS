// =============================================================================
// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2006
// =============================================================================
// CATCldUIRepositBaseDlg : NLS resource file for Reposit Base Dialog Box
// =============================================================================
// Implementation Notes:
// =============================================================================
// 27-Oct-2006 YSN: Creation
// =============================================================================

// ------
// Titles
// ------
CmdTitle.Compass.Title="Aligner � l'aide de la boussole";
CmdTitle.BestFit.Title="Aligner au plus juste";
CmdTitle.Constraint.Title="Aligner par contraintes";
CmdTitle.RPS.Title="Aligner sur RPS";
CmdTitle.Sphere.Title="Aligner par sph�res";
CmdTitle.Trsf.Title="Aligner avec transformation pr�c�dente";

// ----------
// Base class
// ----------
MainFrame.SourceFrame.SourceLabel.Title="Nuage de points � aligner : ";
MainFrame.SourceFrame.SourceLabel.LongHelp="Affiche le nuage � aligner.";

MoreString="Plus >> ";
LessString=" << Moins  ";
MainFrame.MoreFrame.MorePushButton.LongHelp="Affiche ou cache les statistiques.";
FrameStats.LongHelp="Affiche les statistiques.";
FrameStats.Title="Statistiques";

MoveBtn.Title="Garder l'original";
MoveBtn.LongHelp="Lorsque cette option est s�lectionn�e, cr�e une copie du nuage de points initial � aligner 
et aligne le nuage de points sur les r�f�rences.
Lorsque cette option est d�coch�e, aligne le nuage de points initial sur les r�f�rences sans
conserver une copie de celui-ci.";

// ------------
// With Compass
// ------------
MainFrame.CompassFrame.VisuFrame.Title="Afficher";
MainFrame.CompassFrame.VisuFrame.LongHelp="D�finit l'affichage du r�sultat.";
MainFrame.CompassFrame.VisuFrame.PointsCheck.Title="Points";
MainFrame.CompassFrame.VisuFrame.TrianglesCheck.Title="Triangles";
MainFrame.CompassFrame.VisuFrame.ShadingCheck.Title="Rendu r�aliste";
MainFrame.CompassFrame.VisuFrame.SampleSlider.LongHelp="Fixe le pourcentage de points � afficher.";
MainFrame.CompassFrame.VisuFrame.Percent.Title="%";

MainFrame.CompassFrame.MoveFrame.Title="D�placer";
MainFrame.CompassFrame.MoveFrame.PushReset.Title="RAZ Depl.";
MainFrame.CompassFrame.MoveFrame.PushReset.LongHelp="R�initialise l'alignement.";
MainFrame.CompassFrame.MoveFrame.PushInit.Title="Init.  Depl.";
MainFrame.CompassFrame.MoveFrame.PushInit.LongHelp="Propose un recalage par axes d'inertie.";
MainFrame.CompassFrame.MoveFrame.CompRdio.Title="Boussole active ";
MainFrame.CompassFrame.MoveFrame.CompRdio.LongHelp="Place la boussole sur le centre de gravit� du nuage � d�placer.";

// --------
// Best Fit
// ---------
SelectorMulti.Reference.Panel.Title="R�f�rences";

MainFrame.SourceFrame.SourceSelector.SourceCtxMenu.SourceCtxItem.Title="Activation";

MainFrame.SourceFrame.TargetLabel.Title="R�f�rences : ";
MainFrame.SourceFrame.TargetLabel.ReferenceName="R�f�rence : ";
MainFrame.SourceFrame.TargetLabel.LongHelp="Permet de s�lectionner les r�f�rences. Il peut s'agir :
- d'un ensemble de nuages de points et/ou de surfaces,
- ou d'un ensemble de points.
(Il ne peut s'agir d'un m�lange des deux).";

MainFrame.SourceFrame.TargetSelector.TargetCtxMenu.TargetCtxItem.Title="Activation";

Activate.LongHelp="Active les zones.";
Activate.ShortHelp="Activation";

PushBMulti.LongHelp="Affiche la liste des �l�ments s�lectionn�s.";
PushBMulti.ShortHelp="S�lection multiple";

// ---------------------
// by Constraint and RPS
// ---------------------
MainFrame.ConstraintsList.Title="Liste des contraintes";
MainFrame.RPSList.Title="Liste des contraintes RPS";

MainFrame.ConstraintsFrame.LongHelp="Fournit la liste des contraintes d�finies pour aligner un nuage de points. \nUne contrainte est form�e de deux �l�ments de contrainte (une dans le nuage de points � aligner et une autre dans la r�f�rence)\n et d'une valeur de priorit� pour l'alignement par contraintes, ou des axes fixes pour l'alignement RPS.";
MainFrame.ConstraintsFrame.PushClear.Title="Tout effacer";
MainFrame.ConstraintsFrame.PushClear.LongHelp="Supprime toutes les contraintes de la liste.";
MainFrame.ConstraintsFrame.PushDelete.Title="Supprimer";
MainFrame.ConstraintsFrame.PushDelete.LongHelp="Supprime la contrainte s�lectionn�e de la liste.";
MainFrame.ConstraintsFrame.PushAdd.Title="Ajouter";
MainFrame.ConstraintsFrame.PushAdd.LongHelp="Ajoute une contrainte � la liste : s�lectionnez d'abord un �l�ment de contrainte dans le nuage de points � aligner, \npuis une autre dans la r�f�rence.";
MainFrame.ConstraintsFrame.Input.Title="Dans le nuage de points � aligner";
MainFrame.ConstraintsFrame.Target.Title="Dans la r�f�rence";
MainFrame.ConstraintsFrame.Priority.Title="Priorit�";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel1="Stricte";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel2="Haute";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel3="Au-dessus de la moyenne";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel4="Moyenne";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel5="Au dessous de la moyenne";
MainFrame.ConstraintsFrame.CtxItem.PriorityLabel6="Bas";
MainFrame.ConstraintsFrame.CtxItem.Edit="Modifier la priorit�";
MainFrame.ConstraintsFrame.XYZ.XLabel="X";
MainFrame.ConstraintsFrame.XYZ.YLabel="Y";
MainFrame.ConstraintsFrame.XYZ.ZLabel="Z";
MainFrame.RPSParamFrame.Title="Param�tres RPS it�ratifs";

MainFrame.OrientFrame.Title="Orientation";
MainFrame.OrientFrame.LongHelp="D�finit l'orientation des lignes et des plans. Notez que l'orientation est inutile pour les �l�ments de contrainte de type point.";

MainFrame.OrientFrame.OrientAutoLabel.Title="Automatique";
MainFrame.OrientFrame.OrientAutoLabel.LongHelp="Optimise automatiquement les orientations des plans, des droites et des axes du nuage � aligner (les orientations ne sont pas visualis�es).
Cette option est plus longue que l'option Manuelle.";

MainFrame.OrientFrame.OrientManuLabel.Title="Manuelle";
MainFrame.OrientFrame.OrientManuLabel.LongHelp="Permet de contr�ler les orientations des plans, des droites et des axes du nuage � aligner.
Cliquez sur une droite ou un axe pour en inverser l'orientation.
Utilisez cette option si le r�sultat avec l'option Automatique est insatisfaisant ou si vous devez contr�ler les orientations.";

// ---------
// By Sphere
// ---------
MainFrame.SphereFrame.Title="Rayon des sph�res";
MainFrame.SphereFrame.LongHelp="D�finit le rayon des sph�res.";
MainFrame.SphereFrame.ImposedBut.Title="Contrainte";
MainFrame.SphereFrame.ImposedBut.LongHelp="Lorsque cette option est s�lectionn�e, calcule les sph�res avec le rayon entr�.";

// Icones

MainFrame.SourceFrame.SourceHideShowB.Title="Masquer/afficher le nuage de points � aligner";
MainFrame.SourceFrame.SourceHideShowB.ShortHelp="Masquer/afficher le nuage de points � aligner";
MainFrame.SourceFrame.SourceHideShowB.LongHelp="Masquer/afficher le nuage de points � aligner
Masque ou affiche le nuage de points � aligner.";

MainFrame.SourceFrame.TargetHideShowB.Title="Masquer/afficher les r�f�rences";
MainFrame.SourceFrame.TargetHideShowB.ShortHelp="Masquer/afficher les r�f�rences";
MainFrame.SourceFrame.TargetHideShowB.LongHelp="Masquer/afficher les r�f�rences
Masque ou afficher les r�f�rences.";

MainFrame.SourceFrame.SourceSphereB.Title="Sph�res sur le nuage de points � aligner";
MainFrame.SourceFrame.SourceSphereB.ShortHelp="Sph�res sur le nuage de points � aligner";
MainFrame.SourceFrame.SourceSphereB.LongHelp="Permet de d�finir les sph�res sur le nuage de points � aligner.";

MainFrame.SourceFrame.TargetSphereB.Title="Sph�res sur les r�f�rences";
MainFrame.SourceFrame.TargetSphereB.ShortHelp="Sph�res sur les r�f�rences";
MainFrame.SourceFrame.TargetSphereB.LongHelp="Permet de d�finir les sph�res sur les r�f�rences.";
