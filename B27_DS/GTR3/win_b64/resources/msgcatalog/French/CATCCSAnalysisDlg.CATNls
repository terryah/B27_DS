//-----------------------------------------------------
// Resource file for CATCCSOrientationDlg class
// En_US
//-----------------------------------------------------
//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2003
//=============================================================================
//
//   Resource file for NLS purpose. 
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// 09/12/03 : jcb; creation
//=============================================================================

Title="Analyse de la direction de d�moulage ";

ParmAreaName="Zone projet�e de mod�le";

MenuItemLine="Cr�er une droite";
MenuItemPlane="Cr�er un plan";
MenuItemSystem="Cr�er un syst�me d'axes";
MenuItemEdit="Editer les coordonn�es";
MenuItemXAxis="Axe X";
MenuItemYAxis="Axe Y";
MenuItemZAxis="Axe Z";
MenuItemCompass="Direction de la boussole";

FrmRoot.FrmPullingDirection.Title="Direction de d�moulage";
FrmRoot.FrmPullingDirection.FrmDirection.LbDirEmpty0.Title="  ";
FrmRoot.FrmPullingDirection.FrmDirection.LbDirEmpty1.Title="  ";
FrmRoot.FrmPullingDirection.FrmDirection.LbDirEmpty2.Title="  ";
FrmRoot.FrmPullingDirection.FrmDirection.LbDirEmpty3.Title="  ";

FrmRoot.FrmPullingDirection.FrmDirection.Title="Direction";
FrmRoot.FrmPullingDirection.FrmDirection.LabDirection.Title=" Direction : ";
FrmRoot.FrmPullingDirection.FrmCoordinates.Title="Coordonn�es";
FrmRoot.FrmPullingDirection.FrmCoordinates.DirXFrame.XDirLabel.Title=" DX = ";
FrmRoot.FrmPullingDirection.FrmCoordinates.DirYFrame.YDirLabel.Title=" DY = ";
FrmRoot.FrmPullingDirection.FrmCoordinates.DirZFrame.ZDirLabel.Title=" DZ = ";
FrmRoot.FrmPullingDirection.FrmCoordinates.InverseFrame.LbDirEmpty4.Title="      ";


FrmRoot.FrameModel.Title=" Mod�le  ";
FrmRoot.FrameModel.LabelElement.Title=" El�ment : ";
FrmRoot.FrameModel.LabelArea.Title=" Zone projet�e : ";

FrmRoot.FrmEditDirection.Title="Direction de r�f�rence";
FrmRoot.FrmEditDirection.LabelDirX.Title="         DX        ";
FrmRoot.FrmEditDirection.LabelDirY.Title="         DY        ";
FrmRoot.FrmEditDirection.LabelDirZ.Title="         DZ        ";
FrmRoot.FrmEditDirection.DirOffset.Title="              ";
FrmRoot.FrmEditDirection.FrameReset.PushButtonReset.Title="R�initialiser";

FrmRoot.FrmEditAngle.Title="Intervalles d'angle de d�pouille";

FrmRoot.FrmEditAngle.CheckButtonSimulate.Title="   Direction oppos�e   ";
FrmRoot.FrmEditAngle.CheckButton1.Title="   0 �         ";
FrmRoot.FrmEditAngle.CheckButton2.Title="   �       ";
FrmRoot.FrmEditAngle.CheckButton3.Title="   �       ";
FrmRoot.FrmEditAngle.CheckButton4.Title="   �       ";
FrmRoot.FrmEditAngle.CheckButton5.Title="   �       ";
FrmRoot.FrmEditAngle.CheckButton6.Title="   � 90";
FrmRoot.FrmEditAngle.CheckButtonFly.Title="   Analyse locale";

FrmRoot.FrmEditAngle.LabelEspace.Title="         ";


// Help
// Draft Direction management 
FrmRoot.FrmEditDirection.ShortHelp="Direction";
FrmRoot.FrmEditDirection.LongHelp="Direction de r�f�rence
D�finit la direction de r�f�rence pour l'analyse d'une surface.";

// Color ranges
FrmRoot.FrmEditAngle.ShortHelp="Intervalles d'angle de d�pouille";
FrmRoot.FrmEditAngle.LongHelp="Intervalles d'angle de d�pouille
D�finit les intervalles d'angle de d�pouille et les couleurs associ�es.";

// On the fly
FrmRoot.FrmEditAngle.CheckButtonFly.ShortHelp="Analyse locale";
FrmRoot.FrmEditAngle.CheckButtonFly.LongHelp="Analyse locale
Affiche la direction de d�pouille au point actif.";

// Radio buttons
FrmRoot.FrmPullingDirection.FrmDirection.Radio3D.ShortHelp="S�lection 3D";
FrmRoot.FrmPullingDirection.FrmDirection.Radio3D.LongHelp="S�lection 3D
S�lectionne une droite ou un plan pour d�finir la direction de d�moulage.";

FrmRoot.FrmPullingDirection.FrmDirection.RadioX.ShortHelp="Direction X";
FrmRoot.FrmPullingDirection.FrmDirection.RadioX.LongHelp="Direction X
L'axe X du syst�me d'axes courant d�finit la direction de d�moulage.";

FrmRoot.FrmPullingDirection.FrmDirection.RadioY.ShortHelp="Direction Y";
FrmRoot.FrmPullingDirection.FrmDirection.RadioY.LongHelp="Direction Y
L'axe Y du syst�me d'axes courant d�finit la direction de d�moulage.";

FrmRoot.FrmPullingDirection.FrmDirection.RadioZ.ShortHelp="Direction Z";
FrmRoot.FrmPullingDirection.FrmDirection.RadioZ.LongHelp="Direction Z
L'axe Z du syst�me d'axes courant d�finit la direction de d�moulage.";

FrmRoot.FrmPullingDirection.FrmDirection.RadioCompass.ShortHelp="Direction de la boussole";
FrmRoot.FrmPullingDirection.FrmDirection.RadioCompass.LongHelp="Direction de la boussole
La direction de la boussole d�finit la direction de d�moulage.";


FrmRoot.FrmPullingDirection.FrmDirection.RadioFly.ShortHelp="Analyse locale";
FrmRoot.FrmPullingDirection.FrmDirection.RadioFly.RadioFly.LongHelp="Analyse locale
Affiche la direction de d�pouille au point actif.";

FrmRoot.FrmPullingDirection.FrmDirection.RadioLine.ShortHelp="Droite";
FrmRoot.FrmPullingDirection.FrmDirection.RadioLine.LongHelp="Droite
Cr�ation d'une droite.";

FrmRoot.FrmPullingDirection.FrmDirection.RadioPlane.ShortHelp="Plan";
FrmRoot.FrmPullingDirection.FrmDirection.RadioPlane.LongHelp="Plan
Cr�ation d'un plan.";

FrmRoot.FrmPullingDirection.FrmDirection.RadioEditComponents.ShortHelp="Coordonn�es";
FrmRoot.FrmPullingDirection.FrmDirection.RadioEditComponents.LongHelp="Coordonn�es
Modifie les coordonn�es de la direction de d�moulage.";

// Coordinates
FrmRoot.FrmPullingDirection.FrmCoordinates.ShortHelp="Coordonn�es";
FrmRoot.FrmPullingDirection.FrmCoordinates.LongHelp="Coordonn�es
Affiche les coordonn�es de la direction de d�moulage.";

FrmRoot.FrmPullingDirection.FrmCoordinates.InverseFrame.PushButtonInverse.ShortHelp="Direction inverse";
FrmRoot.FrmPullingDirection.FrmCoordinates.InverseFrame.PushButtonInverse.LongHelp="Direction inverse
Inverse la direction de d�moulage.";

// Model 
FrmRoot.FrameModel.ShapeList.ShortHelp=" Forme";
FrmRoot.FrameModel.ShapeList.LongHelp="Forme
S�lectionne la forme � analyser.";
FrmRoot.FrameModel.LabelElement.ShortHelp=" Forme";
FrmRoot.FrameModel.LabelElement.LongHelp="Forme
S�lectionne la forme � analyser.";

FrmRoot.FrameModel.LabelArea.ShortHelp=" Zone projet�e ";
FrmRoot.FrameModel.LabelArea.LongHelp="Zone projet�e
Affiche la valeur de la zone projet�e.";
FrmRoot.FrameModel.LabelArea.ShortHelp=" Zone projet�e ";
FrmRoot.FrameModel.LabelArea.LongHelp="Zone projet�e
Affiche la valeur de la zone projet�e.";

// Ranges and color editors
FrmRoot.FrmEditAngle.CheckButtonSimulate.ShortHelp="Direction oppos�e";
FrmRoot.FrmEditAngle.CheckButtonSimulate.LongHelp="Direction oppos�e
Affiche les zones devant �tre d�moul�es dans la direction oppos�e.";

FrmRoot.FrmEditAngle.ColorViewer0.ShortHelp="Editeur de couleur";
FrmRoot.FrmEditAngle.ColorViewer0.LongHelp="Editeur de couleur
Cliquez deux fois sur la couleur de la direction oppos�e pour la changer dans l'�diteur de couleur.";

FrmRoot.FrmEditAngle.ColorViewer1.ShortHelp="Editeur de couleur";
FrmRoot.FrmEditAngle.ColorViewer1.LongHelp="Editeur de couleur
Cliquez deux fois sur la couleur pour la changer dans l'�diteur de couleur.";
FrmRoot.FrmEditAngle.ColorViewer2.ShortHelp="Editeur de couleur";
FrmRoot.FrmEditAngle.ColorViewer2.LongHelp="Editeur de couleur
Cliquez deux fois sur la couleur pour la changer dans l'�diteur de couleur.";
FrmRoot.FrmEditAngle.ColorViewer3.ShortHelp="Editeur de couleur";
FrmRoot.FrmEditAngle.ColorViewer3.LongHelp="Editeur de couleur
Cliquez deux fois sur la couleur pour la changer dans l'�diteur de couleur.";
FrmRoot.FrmEditAngle.ColorViewer4.ShortHelp="Editeur de couleur";
FrmRoot.FrmEditAngle.ColorViewer4.LongHelp="Editeur de couleur
Cliquez deux fois sur la couleur pour la changer dans l'�diteur de couleur.";
FrmRoot.FrmEditAngle.ColorViewer5.ShortHelp="Editeur de couleur";
FrmRoot.FrmEditAngle.ColorViewer5.LongHelp="Editeur de couleur
Cliquez deux fois sur la couleur pour la changer dans l'�diteur de couleur.";
FrmRoot.FrmEditAngle.ColorViewer6.ShortHelp="Editeur de couleur";
FrmRoot.FrmEditAngle.ColorViewer6.LongHelp="Editeur de couleur
Cliquez deux fois sur la couleur pour la changer dans l'�diteur de couleur.";

