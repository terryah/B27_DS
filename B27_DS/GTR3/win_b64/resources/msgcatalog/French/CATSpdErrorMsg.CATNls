// COPYRIGHT ImpactXoft 2003
//===================================================================
SpdInternalError="Une erreur inattendue s'est produite.";

SpdProfileInp1="L'esquisse n'est pas sp�cifi�e ou elle est d�sactiv�e.";
SpdProfileInp2="Ce composant implique que l'esquisse associ�e soit ferm�e.";
SpdProfileInp3="Ce composant implique que l'esquisse associ�e ne soit pas vide.";
SpdProfileInp4="Ce composant implique que l'esquisse associ�e soit active.";
SpdProfileInp5="Les profils de balayage doivent �tre plans.";
SpdProfileInp6="L'esquisse s�lectionn�e contient des points de g�om�trie standard. Vous devez convertir ces points en �l�ments de construction.";
SpdProfileInp7="Shell en provenance d'un op�rateur de d�calage ferm� : \nAvertissement : Des auto-intersections existent dans le contour en entr�e.  \nModifiez le contour en entr�e.";
SpdProfileInp8="Le volume form� par les composants sur�paissis ne traverse pas compl�tement le prisme.  \nEssayez de d�placer la g�om�trie, d'�largir le volume sur�paissi ou de r�duire le profil.";
SpdProfileInp9="Au moins une valeur de d�calage doit �tre diff�rente de z�ro.";
SpdProfileInp10="L'interaction entre le prisme et le volume form� par les composants de coque est trop complexe. \nCela est probablement d� au fait qu'une partie seulement du profil du composant est contenue dans le volume de coque.";
SpdProfileInp11="L'interaction entre le prisme et les s�lections de la limite est trop complexe. \nCela est probablement d� au fait que l'esquisse extrud�e n'est pas totalement limit�e par la surface de limite.";
SpdProfileInp12="L'option Jusqu'� la coque n�cessite la pr�sence de composants de coque.";
SpdProfileInp13="Auto-intersection d'un contour s�lectionn�. \nCette configuration ne peut �tre utilis�e dans un op�rateur d'extrusion. \nChoisissez un contour sans auto-intersection.";
SpdProfileInp14="Les sp�cifications des limites du prisme sont trop complexes. \nEssayez de modifier les sp�cifications des limites du prisme.";

SpdDraftTypeInvalid="Les composants obtenus par balayage ne supportent que les angles de d�pouille.";
SpdDraftTypeInvalid2="Si la limite est du type A travers ou Jusqu'� la coque, seuls les angles de d�pouille sont possibles.";
SpdDraftLimit1Invalid="Si la premi�re limite est du type A travers ou Jusqu'� la coque, l'�l�ment neutre de la d�pouille ne peut �tre la premi�re limite.";
SpdDraftLimit2Invalid="Si la seconde limite est du type A travers ou Jusqu'� la coque, l'�l�ment neutre de la d�pouille ne peut �tre la seconde limite.";
SpdDraftAngleInvalid="L'angle de la d�pouille doit �tre compris entre -85. 0 et 85 degr�s.";
SpdDraftDoubleFillet="Il n'est pas possible de cr�er un cong� lat�ral lorsqu'on cr�e une double d�pouille.";
SpdDraftSketchHasNoPlane="Le plan neutre de la d�pouille n'a pu �tre d�termin� car le support de l'esquisse n'est pas valide.";
SpdDraftPropsMissing="Les propri�t�s de la d�pouille ne sont pas disponibles.";
SpdDraftNeutralMissing="L'�l�ment neutre de la d�pouille est manquant.";
SpdDraftNeutralInvalid="L'�l�ment neutre de la d�pouille s�lectionn�e est incorrect.";
SpdDraftPartingUnresolved="L'�l�ment de joint de la d�pouille ne peut pas �tre converti.";
SpdDraftPartingMissing="Lorsque vous utilisez l'�l�ment de joint comme �l�ment neutre, un �l�ment de joint doit �tre s�lectionn�.";
SpdDraftBadPull="La direction de d�moulage du brouillon est parall�le � l'�l�ment neutre.";
SpdDraftBadPartingPull="La direction de d�moulage du brouillon est parall�le l'�l�ment de joint.";
SpdDraftFacesFailed="La direction de d�moulage est perpendiculaire � une face en cours de d�pouille.";
SpdDraftFailed="Echec de l'op�ration de d�pouille. \nEssayez de modifier certains des param�tres de d�pouille de la g�om�trie.";
SpdDraftFailedKindly="Echec de l'op�ration de d�pouille : la topologie ou la g�om�trie locale est trop complexe.";

SpdReferencedPoint="Un sommet n'est plus reconnu.";
SpdReferencedAxis="L'axe n'est plus reconnu.";
SpdReferencedLine="Une ar�te n'est plus reconnue.";
SpdReferencedFace="Une face n'est plus reconnue.";
SpdReferencedCuttingFace="Une face utilis�e comme �l�ment de coupe n'est plus reconnue.";
SpdReferencedPartingFace="Une face utilis�e comme �l�ment de joint n'est plus reconnue.";

SpdEvalDataPath="Ce composant implique qu'un chemin soit sp�cifi�.";
SpdEvalDataPathActive="Ce composant implique que le chemin associ� soit actif.";
SpdEvalDataDirection="La somme des longueurs doit �tre diff�rente de z�ro.";
SpdEvalDataPathInvalid="Chemin non pris en charge de la direction de d�pouille.";
SpdEvalDataPathSupport="Le chemin et le profil utilisent le m�me support d'esquisse.";
SpdEvalDataPathParallel="Les supports du chemin et du profil sont parall�les.";

SpdEvalDataFillet="Les rayons de cong� n�gatifs ne sont pas autoris�s.";
SpdEvalDataThickFillet="Sur un composant sur�paissi, chaque rayon d'arrondi doit �tre inf�rieur ou �gal � la moiti� de l'�paisseur totale.";
SpdEvalDataThickShell="L'�paisseur d'un �l�ment � coque doit �tre sup�rieure � deux fois l'�paisseur de coque.";

SpdEvalDataFillet1Sides="Le premier cong� est trop grand pour les faces lat�rales du composant.";
SpdEvalDataFillet2Sides="Le second cong� est trop grand pour les faces lat�rales du composant.";
SpdEvalDataFillet12Sides="La combinaison des premier et second cong�s est trop grande pour les faces lat�rales du composant.";
SpdEvalDataFillet1End="Le premier cong� est trop grand pour la face limite 1 du composant.";
SpdEvalDataFillet2End="Le second cong� est trop grand pour la face limite 2 du composant.";
SpdFilletFailure="Impossible d'initialiser l'op�ration de cong�s demand�e du dispositif.";

SpdReinforcementFillet="Le cong� lat�ral est trop grand pour les faces lat�rales du composant.";
SpdEvalDraftFillets="L'option du cong� du brouillon n'est pas prise en charge sur ce type de dispositif";
SpdEvalDraftFilletsErr="Echec de l'op�ration de d�pouille sur des cong�s. \nEssayez de modifier certains des param�tres de d�pouille ou de cong� de la g�om�trie.";

SpdEvalDataWall="Epaisseur de paroi non valide.";
SpdEvalDataWallHeight="La longueur du composant doit �tre sup�rieure � l'�paisseur de la paroi.";
SpdEvalDataVolume="Ce composant ne peut constituer un solide � partir d'une esquisse ouverte si l'option Epais n'est pas utilis�e.";
SpdEvalDataThin="Les valeurs choisies cr�eraient un volume vide.";

SpdEvalDataSplitDraft="L'application ne supporte pas une �paisseur sup�rieure � une limite de hauteur pour une d�pouille coup�e.";

SpdAxis="Un axe de r�volution doit �tre sp�cifi�.";
SpdAngle="Les deux angles cr�eraient un volume nul.";

SpdBoundaryInside="La position ou direction du plan du contour du bord incorrect.  \nLe plan ne devrait pas �tre � l'int�rieur de la coque et \n la direction devrait pointer vers l'int�rieur de la coque.";
SpdBoundaryTooLarge="Le contour du bord est trop grand ou ne rencontrerait pas la coque.";
SpdBoundaryTooLoopy="Les esquisses comportant plusieurs contours ne sont pas support�es.";
SpdBoundarySketch="Vous devez sp�cifier le contour du bord.";
SpdBoundaryActive="Le contour du bord doit �tre actif.";
SpdBoundaryEmpty="Le contour du bord ne peut �tre vide.";
SpdBoundaryClosed="Le contour du bord doit �tre ferm�.";
SpdBoundaryPoints="L'esquisse du contour contient des points d�finis comme �l�ments g�om�triques standard.  \nVous devez convertir ces points en �l�ments de construction.";
SpdHeight="La hauteur du bord doit �tre positive.";
SpdWidth="La largeur du bord doit �tre positive.";
SpdSpikeHeight="La hauteur du renfort doit �tre positive.";
SpdSpikeWidth="La largeur du renfort doit �tre positive.";
SpdIslandSketch="L'esquisse de l'�lot doit �tre ferm�e.";
SpdSpikeMustExist="L'�l�ment neutre du dessin s�lectionn� requiert une g�om�trie en renfort.";
SpdRibMustExist="L'�l�ment neutre du dessin s�lectionn� requiert une g�om�trie en bosse.";

SpdSpikePositionRib="Le renfort ne doit pas �tre situ� sous les nervures.";
SpdSpikePositionBdy="Le renfort ne doit pas �tre situ� sous le bord.";
SpdSpikeOffsets="Les d�calages du renfort ne doivent pas absorber le renfort.";
SpdRibHeight="La hauteur de la nervure doit �tre positive.";
SpdRibWidth="La largeur de la nervure doit �tre positive.";


SpdThicknessClosed="L'esquisse doit �tre ferm�e si l'�paisseur d�finie est nulle.";
SpdNoCuttingFaces="Aucun �l�ment coupant n'a �t� s�lectionn�.";
SpdMultipleTargets="Ce composant ne peut avoir plusieurs cibles.";

SpdNoThickenSrf="Les surfaces s�lectionn�es pour �tre sur�paissies n'existent plus.";
SpdUnboundedSrf="La surface s�lectionn�e en vue de l'application d'une sur�paisseur est sans limite.";

SpdUnresolvedDirectionId="La direction de r�f�rence ne peut �tre utilis�e.  \nCATIA va choisir une direction par d�faut.";
SpdUnresolvedPathControlId="La r�f�rence du chemin ne peut �tre utilis�e.  \nCATIA va choisir une r�f�rence par d�faut.";
SpdUnresolvedPathLimitsId="Les points de r�f�rence limitant le chemin ne peuvent �tre utilis�s.  \nCATIA va choisir les limites par d�faut.";
SpdUnresolvedFaceId="Une face ne peut �tre r�solue.  \nCette sp�cification va �tre retir�e.";
SpdUnresolvedFacesId="/p1 faces ne peuvent �tre r�solues.  \nLeurs sp�cifications vont �tre retir�es.";
SpdUnresolvedRevolveAxesId="L'axe de r�f�rence ne peut �tre utilis�.  \nCette sp�cification va �tre retir�e.";
SpdUnresolvedCuttingFaceId="Une face coupante ne peut pas �tre convertie.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedCuttingFacesId="Les faces coupantes /p1 ne peuvent pas �tre converties.  \nLeurs sp�cifications vont �tre supprim�es.";
SpdUnresolvedPartingFaceId="La face de l'�l�ment de joint ne peut pas �tre convertie.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedPullingDirection="La direction de d�pouille ne peut pas �tre convertie.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedLimitsId="Une face de limite ne peut pas �tre supprim�e.";
SpdUnresolvedPathEdgeId="Un bord de trajet ne peut pas �tre converti.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedPathEdgesId="Les bords du trajet /p1 ne peuvent pas �tre convertis.  \nLeurs sp�cifications vont �tre supprim�es.";
SpdUnresolvedDraftProperties="Les propri�t�s ne peuvent pas �tre converties.  \nLe comportement de d�pouille va �tre red�fini sur Aucun et la fonction ne sera pas d�pouill�e.";
SpdUnresolvedDraftPartingFace="La face de l'�l�ment de joint (dans les propri�t�s de d�pouille) ne peut pas �tre convertie.";
SpdUnresolvedDraftPullingDir="La direction de d�pouille (dans les propri�t�s de d�pouille) ne peut pas �tre convertie.";
SpdUnresolvedDraftNeutralElement="L'�l�ment neutre ne peut pas �tre converti.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedLipPullingDirection="La direction de d�pouille ne peut pas �tre convertie.  \nLa normale de l'�l�ment de division est utilis�e.";
SpdUnresolvedPatternDir1="La 1�re direction de mod�le ne peut pas �tre convertie.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedPatternDir2="La 2�me direction de mod�le ne peut pas �tre convertie.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedMirrorElement="L'�l�ment miroir ne peut pas �tre converti.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedSymmetryRef="L'�l�ment de r�f�rence � la transformation par sym�trie ne peut pas �tre converti.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedScalingRef="L'�l�ment de r�f�rence � la transformation par mise � l'�chelle ne peut pas �tre converti.  \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedStartPt="Le point de d�part de la transformation ne peut pas �tre converti.  \n La sp�cification va �tre supprim�e.";
SpdUnresolvedEndPt="Le point de fin de la transformation ne peut pas �tre converti.  \n La sp�cification va �tre supprim�e.";
SpdUnresolvedAxisOrigin="L'origine l'axe de transformation ne peut pas �tre convertie.  \n La sp�cification va �tre supprim�e.";
SpdUnresolvedXYPlane="Le plan XY de l'axe de transformation ne peut pas �tre converti.  \n La sp�cification va �tre supprim�e.";
SpdUnresolvedXAxis="L'axe X de transformation ne peut pas �tre converti.  \n La sp�cification va �tre supprim�e.";
SpdUnresolvedRefAxis="Le rep�re de r�f�rence de la transformation ne peut pas �tre converti.  \n La sp�cification va �tre supprim�e.";
SpdUnresolvedTgtAxis="La cible de la transformation ne peut pas �tre convertie.  \n La sp�cification va �tre supprim�e.";
SpdUnresolvedElementId="Impossible de r�soudre un �l�ment de profil. \nLa sp�cification va �tre supprim�e.";
SpdUnresolvedElementsId="/p1 �l�ments de profil ne peuvent �tre r�solus. \nLeurs sp�cifications vont �tre supprim�es.";
SpdUnresolvedBrepEdgeFilletId="Une ar�te du cong� ne peut pas �tre trait�e. \nSa sp�cification va �tre supprim�e.";
SpdUnresolvedBrepEdgesFilletId="/p1 ar�tes ne peuvent pas �tre trait�es. \nLes sp�cifications correspondantes vont �tre supprim�es.";

SpdWallInterference="L'�paisseur doit �tre sup�rieure ou �gale � l'�paisseur du corps, sinon l'int�rieur de la plate-forme sera r�duit.";

SpdInvalidThickness="Les �paisseurs ne peuvent avoir une valeur n�gative.";

SpdBadDirection="La direction s�lectionn�e est parall�le au plan de l'esquisse.";
SpdBadRevolveAxis="Vous devez sp�cifier un axe appartenant au plan de l'esquisse.";
SpdBadPath="Le chemin s�lectionn� est parall�le au plan de l'esquisse.";

SpdPathLimitPtsAndMulti="Les points de limite de chemin ne sont pas autoris�s sur un chemin comportant plusieurs contours.";
SpdPathMoveToNeeded="Si le chemin comporte plusieurs contours, vous devez s�lectionner l'option de d�placement du profil sur la Trajectoire.";

SpdMissingLimitSurface="Le plan ou la surface utilis�e comme limite n'est pas trouv�e.";
SpdLimitSketchBadNormal="La normale au plan de l'esquisse utilis�e comme limite ne peut pas �tre parall�le � la direction d'extrusion.";

SpdShellDeleteErr="Vous ne pouvez pas d�truire cet �l�ment car il fait partie des sp�cifications obligatoires du corps fonctionnel.";
SpdShellDeleteErr2="Vous ne pouvez pas supprimer cet �l�ment, car des sp�cifications bas�es sur sa paroi existent dans le corps fonctionnel.";
SpdShellCopyErr="Vous ne pouvez pas copier cet �l�ment car il fait partie des sp�cifications obligatoires du corps fonctionnel.";
SpdShellNoOutput="Propri�t�s de la coque : toutes les faces de la pi�ce ont �t� s�lectionn�es comme faces d'ouverture. \nLe r�sultat de l'op�ration sera vide.  Cela peut �tre d� � la propagation automatique en tangence.";

SpdFeatureCopyErr="Vous ne pouvez pas copier cet �l�ment.";

SpdBodyRefNoBody="Le r�sultat n'a pu �tre recalcul� pour le corps r�f�renc�.";
SpdCloseOpError="Une erreur inattendue s'est produite.";

SpdTwistError="Le contour et la courbe des centres s�lectionn�s g�n�rent une configuration vrill�e.  \nUtilisez une courbe des centres avec une courbure plus faible.";

SpdPatNYI="La fonction de structuration n'est pas encore mise en oeuvre.";
SpdPatKey1="La position de r�f�rence dans la premi�re direction doit �tre sup�rieure � z�ro.";
SpdPatKey2="Lors de l'utilisation de l'option <Espacement&Longueur>, la longueur doit �tre sup�rieure ou �gale � l'espacement.";
SpdPatSpaceTotalAng="Lors de l'utilisation de l'option <Espacement angulaire&Angle total>, l'angle total doit �tre sup�rieur ou �gal � l'espacement angulaire.";
SpdPatSpaceTotalLen="Lors de l'utilisation de l'option <Espacement&Longueur>, la longueur doit �tre sup�rieure ou �gale � l'espacement.";
SpdPatCountNeg="Le nombre d'instances doit �tre sup�rieur � z�ro.";
SpdPatCountTooBig="Le nombre d'instances doit �tre inf�rieur ou �gal � 10 000.";
SpdPatSpaceLen="L'espacement ne peut pas �tre nul.";
SpdPatTotalLen="La longueur ne peut pas �tre nulle.";
SpdPatSpaceAng="L'espacement angulaire ne peut pas �tre nul.";
SpdPatTotalAng="L'angle total ne peut pas �tre nul.";
SpdPatTotalAng360="L'angle total doit �tre � moins de 360 degr�s.  Utilisez l'option <Couronne enti�re> si n�cessaire.";
SpdPatKeySuppress="L'objet de structure r�f�renc� ne peut pas �tre supprim�.";
SpdPatUnresolvedDir1="La 1�re direction de mod�le ne peut pas �tre convertie.";
SpdPatUnresolvedDir2="La 2�me direction de mod�le ne peut pas �tre convertie.";
SpdPatNoAxes="Le mod�le n�cessite au moins une direction.";
SpdPatParallelAxes="Les 1�re et 2�me directions du mod�le ne peuvent pas �tre parall�les.";
SpdPatNoSketch="Le mod�le d�fini par l'utilisateur n�cessite une esquisse.";
SpdPatSketchEmpty="L'esquisse correspondant au mod�le d�fini par l'utilisateur doit contenir des points de non construction.";
SpdPatKeepSpec="L'option Conserver les sp�cifications n'est pas autoris�e lorsque le composant � r�p�ter est un modificateur.";

SpdJoinSketchEmpty="L'esquisse correspondant � la jointure doit contenir des points de non construction.";
SpdJoinProfilePosition="Echec de g�n�ration d'un assemblage correct avec l'emplacement de profil sp�cifi�.";
SpdJoinGuideHeight="La hauteur du guide doit �tre sup�rieure ou �gale � z�ro.";
SpdJoinDraftAngle="Tous les angles de d�pouille de l'assemblage doivent �tre sup�rieurs ou �gaux � z�ro.";
SpdJoinClearance="Outre le d�gagement de taraudage, tous les d�gagements de l'assemblage doivent �tre \nsup�rieurs ou �gaux � z�ro.";
SpdScrewHeight="La hauteur de vis doit �tre sup�rieure ou �gale � z�ro.";
SpdScrewHeight2="Les hauteurs de vis doivent �tre sup�rieures ou �gales � z�ro.";
SpdScrewDiameter="Tous les diam�tres de vis doivent �tre sup�rieurs ou �gaux � z�ro.";
SpdThreadDiameter="Aucune vis valide n'est d�finie.\nLe diam�tre de taraudage doit �tre sup�rieur � z�ro.";
SpdThreadHeight="Aucune vis valide n'est d�finie.\nLa hauteur de taraudage doit �tre sup�rieure � z�ro.";
SpdJoinNoEVT="Impossible de g�n�rer un assemblage valide. L'assemblage utilise la g�om�trie des\n corps dans laquelle il se trouve pour d�terminer sa propre g�om�trie.";
SpdJoinLump0="Emplacements des vis situ�s dans le mat�riau de la pi�ce.";
SpdJoinLump1="Les emplacements des vis ou les valeurs du param�tre de t�te/filetage  poussent le corps de l'assemblage hors du mat�riau de la pi�ce.";
SpdThreadWall="La paroi du filetage va entrecouper le guide du collier serrage.  Pour rem�dier � ce probl�me, \n augmentez l'angle de d�pouille du collier de serrage, le d�gagement du c�t� de l'encastrement ou \nle d�gagement de la hauteur du collier de serrage, ou diminuez l'angle de d�pouille externe du filetage.";
SpdJoinSiblingAbsent="Le remplacement de l'esquisse est impossible � ce moment l� �tant donn� que le joint apparent� n'est pas accessible.";
SpdUnderCutShank="La combinaison des param�tres risque de provoquer le d�coupage d'une zone du corps dans les deux directions.";
SpdShankDraftLarge="La t�te est r�duite car l'angle de d�pouille du corps est trop important.";
SpdHeadOuterDraft="Le guide du collier est coup� car l'angle de d�pouille externe de la t�te ou l'angle de d�pouille interne du collier est \ntrop important.";
SpdThreadTooLarge="Le filetage ne correspond pas au guide du collier.  Retirez le guide ou diminuez la paroi de \nfiletage, le diam�tre de taraudage, le d�gagement du c�t� du filetage ou le d�gagement du c�t� du collier.";
SpdInvalidScrewDia="Des cotes de diam�tre de vis incorrectes ont �t� pr�cis�es.";
SpdInvalidHeadDraft="L'angle de d�pouille externe de la t�te doit �tre sup�rieur ou �gal � son angle de d�pouille interne.";
SpdMissingTrim="Il est impossible d'obtenir les volumes positifs utilis�s pour limiter partiellement les positions de la vis externe.";
SpdJoinSSCTooSmall="L'ensemble rayon du corps/d�gagement de la hauteur du corps est inf�rieur au rayon du filetage.";
SpdNoOSVDCC="L'assemblage cr�e des structures con�ues pour connecter deux corps avec parois.\n Si l'un des corps est un solide, utilisez plut�t un trou.";

SpdJoinGussetTopOffset="Le d�calage sup�rieur du gousset ne peut pas �tre n�gatif.";
SpdJoinGussetHeadWidth="Impossible de cr�er le gousset de t�te, essayez d'augmenter la largeur du c�ne-outil.";
SpdJoinGussetThreadWidth="Impossible de cr�er le gousset du filetage, essayez d'augmenter la largeur du c�ne-outil.";
SpdJoinGussetThickness="L'�paisseur de la paroi du gousset ne peut pas �tre n�gative.";
SpdJoinGussetFilletRad1="Le rayon d'arrondi du gousset ne peut pas �tre n�gatif.";
SpdJoinGussetFilletRad2="Le rayon d'arrondi du gousset est trop important pour l'�paisseur demand�e.";
SpdJoinGussetBlendRad="Le rayon de la surface de raccord du gousset ne peut pas �tre n�gatif.";
SpdJoinGussetLimit1="Impossible de cr�er le gousset.  Il est probable que les param�tres soient � l'origine du croisement entre le gousset et la face supprim�e des propri�t�s de la coque.";

SpdScrewDescNoRead="Echec de lecture des descriptions de vis dans le catalogue.";

SpdDivideCutFailure="Echec de g�n�ration d'une division correcte avec l'�l�ment de division comme �l�ment coupant.";
SpdCutCutFailure="Echec de g�n�ration d'une d�coupe correcte avec l'�l�ment coupant.";
SpdCutTooSmall="L'�l�ment coupant n'est pas compl�tement conforme aux fonctions s�lectionn�es.  Essayez d'augmenter la surface de d�coupe.";
SpdCutSurfaceParallelToExtrusion="Impossible de cr�er l'�l�ment coupant. La direction de l'extrusion de la surface de d�coupe ne peut pas �tre parall�le � la surface de d�coupe. Modifiez la surface de d�coupe.";

SpdDraftPropsPartingFaceId="La face de l'�l�ment de joint est manquante.";
SpdDraftPropsPullingDirection="La direction de d�pouille est manquante.";

SpdPPFClearance1="La distance de l'ouverture doit �tre sup�rieure ou �gale � z�ro.";
SpdPPFClearance2="La distance de l'ouverture ou des autres faces d'ouverture doit �tre sup�rieure � z�ro.";

SpdLipSimpleAllLimitation="Les boucles de toutes les l�vres simples n'ont pas pu �tre extraites en raison des limitations actuelles.";
SpdLipSimpleHeight="La hauteur de la l�vre doit �tre positive.";
SpdLipSimpleThickness="L'�paisseur de la l�vre doit �tre positive.";
SpdLipSimpleAngle="L'angle de l�vre doit �tre compris entre -85 et 85 degr�s.";
SpdLipSimpleInnerAngle="L'angle de l�vre interne doit �tre compris entre -85 et 85 degr�s.";
SpdLipSimpleClearance="Le jeu de la l�vre doit �tre positif ou �gal � z�ro.";
SpdLipPullParallel="Le sens de traction de la l�vre ne doit pas �tre parall�le � la trajectoire.";
SpdLipProfilePerp="Le sens de traction de la l�vre ne doit pas se trouver dans le plan du profil.";
SpdLipSimpleSweep="Les conditions de la l�vre sont trop complexes.  \nEssayez une trajectoire plus simple, une �paisseur plus faible ou une hauteur moins �lev�e.";
SpdLipPullingDirectionUnr="La direction de d�pouille ne peut pas �tre convertie.";
SpdLipProfileClosed="Le profil doit �tre ouvert et plan.";
SpdLipUpperProfileClosed="Le profil sup�rieur doit �tre ouvert et plan.";
SpdLipLowerProfileClosed="Le profil inf�rieur doit �tre ouvert et plat.";
SpdLipCutWithDivElemFail="Echec de l'intersection face-face. \nRecherchez un profil de l�vre pratiquement tangent � un �l�ment de division.";
SpdLipIntxWithDivBodyFail="Echec de l'intersection face-face. \nRecherchez un profil de l�vre pratiquement tangent au corps divis�.";
SpdLipMultiContourProfile="Un profil de l�vre ne peut contenir plusieurs contours.";
SpdLipNoContourProfile="Le profil de l�vre ne contient aucun contour.";

SpdMirrorElementRequired="El�ment de sym�trie obligatoire.";
SpdMirrorElementInActive="L'�l�ment de sym�trie doit �tre actif.";

SpdReinforcementThickness="La combinaison des param�tres entra�nerait l'ajout d'une pi�ce enti�re d'un mat�riau.\n Diminuez les valeurs d'�paisseur";

SpdAffinityOrigin="L'origine de l'axe d'affinit� doit �tre un point.";
SpdAffinityOriginSolve="L'origine de l'axe d'affinit� s�lectionn� ne peut �tre r�solue.";
SpdAffinityMissingSelec="Etant donn� qu'un axe X, une origine ou un plan XY a �t� s�lectionn� pour le syst�me d'axes d'affinit�, tous doivent �tre s�lectionn�s.";
SpdAffinityPlane="Le plan XY de l'axe d'affinit� doit �tre un plan.";
SpdAffinityPlaneSolve="Le plan XY de l'axe de l'affinit� s�lectionn�e ne peut �tre r�solu.";
SpdAffinityXScale="Le coefficient de l'axe X de l'affinit� doit �tre sup�rieur � z�ro.";
SpdAffinityYScale="Le coefficient de l'axe Y de l'affinit� doit �tre sup�rieur � z�ro.";
SpdAffinityZScale="Le coefficient de l'axe Z de l'affinit� doit �tre sup�rieur � z�ro.";
SpdAffinityXAxis="L'axe X de l'affinit� doit �tre une ligne.";
SpdAffinityXAxisSolve="L'axe X de l'affinit� s�lectionn�e ne peut �tre r�solu.";
SpdAffinityXAxisInvalid="L'axe X s�lectionn� projette vers un point du plan XY s�lectionn�.";

SpdSweepPullParallel="La direction de d�pouille ne doit pas �tre parall�le � la trajectoire.";
SpdSweepRefSurface="Un contour ne repose pas sur la surface. Projetez-le sur la surface support et utilisez cette projection.";
SpdSweepDistanceError="Le point de d�part et le point final du balayage sont identiques. Le balayage ne peut pas �tre cr��";

SpdExtractBadVolType="Type de volume d�fini non valide, seuls les �l�ments ajout�s et prot�g�s sont pris en charge.";
SpdExtractNoSource="Un corps source est obligatoire.";
SpdExtractOldGrill="Le composant de grille s�lectionn� a �t� cr�� dans une version ant�rieure du logiciel et ne peut pas �tre extrait.";
SpdExtractMissingGrill="Un composant de grille doit �tre s�lectionn�.";
SpdExtractMissingFeatures="Un ou plusieurs composants de grille doivent �tre s�lectionn�s.";

SpdExtBehaviorOverrideConflict="Le composant cible n'autorise pas de modification de son comportement.";
SpdExtBehaviorInvalidTarget="Extraction impossible du composant cible.  Le comportement des composants suivants ne peut pas �tre extrait : \n   Composants de propri�t� \n   Composants � plusieurs corps \n   Modificateur de composant avec plusieurs cibles";

SpdPocketBadWallVersion="Les poches cr��es avant la version 13 ne prennent pas en charge la paroi de d�gagement.";
SpdPocketOOnlyNoCWall="La valeur de la paroi de d�gagement des poches ext�rieures uniquement doit �tre nulle.";
SpdPocketCWallRequiresPWall="Une paroi de d�gagement n�cessite une paroi de poche.";

SpdImportWallDirMismatch="Non-concordance entre la direction de la paroi de la coque et la direction de paroi de l'import.";
SpdImportWallDirMismatch2="Non-concordance entre la direction de la paroi de la coque source et la direction de la paroi de la coque cible.\n Modifiez la direction de la paroi de la coque dans l'ensemble fonctionnel solide cible";
SpdImportWallDirMismatch3="Non-concordance entre la direction de la paroi de la coque source et la direction de la paroi de la coque cible.\n Cr�ez les propri�t�s de la coque et modifiez la direction de la paroi de la coque dans l'ensemble fonctionnel solide cible";
SpdImportWallDirMismatch4="Non-concordance entre la direction de la paroi de la coque source et la direction de la paroi de la coque cible.\n Modifiez la direction de la paroi de la coque dans l'ensemble fonctionnel solide cible, synchronisez et mettez � jour l'importation";

SpdImportWallTypeUseBodyTh="Le r�sultat du collage peut �tre diff�rent du r�sultat escompt� : v�rifiez que l'option de la paroi du composant de r�f�rence n'a pas la valeur 'Epaisseur du corps'.";
SpdImportExtAcrRemdFaces="Le r�sultat du collage peut �tre diff�rent du r�sultat escompt� : v�rifiez que l'option du composant de r�f�rence 'Etendre aux faces supprim�es' n'est pas d�finie.";
SpdImportLimitUpToShell="Le r�sultat du collage peut �tre diff�rent du r�sultat escompt� : v�rifiez que les limites du composant de r�f�rence ne sont pas configur�es jusqu'� la paroi.";

SpdImportWithTRPropagation="Echec de la propagation des r�sultats technologiques par le biais de l'importation.";

SpdFlangeTargetNeeded="Le bord bomb� peut uniquement �tre de cr�er sur une pi�ce avec une division.";
SpdFlangeTargetData1="Il manque des donn�es dans la d�coupe du bord bomb�.";
SpdFlangeTargetData2="Le r�sultat de la d�coupe du bord bomb� ne supportera pas un bord bomb�.";
SpdFlangeTargetData3="La projection des ar�tes de d�coupe le long de la direction de d�moulage ne produit pas un r�sultat valide.  \nNotez que la surface de d�coupe doit �tre suffisamment large pour maintenir le bord bomb�.";
SpdFlangeTargetData4="La projection des ar�tes de d�coupe n'a pas pu �tre calcul�e en raison d'une limitation en cours.";
SpdFlangeTargetData5="La pi�ce partag�e provoque des ar�tes � bord bomb�. \nIl est probable que la limite externe de la pi�ce soit d�finie avec un volume prot�g�.";
SpdFlangeWidth="La largeur du bord bomb� doit �tre sup�rieure � z�ro.";
SpdFlangeThickness="L'�paisseur du bord bomb� doit �tre sup�rieure � z�ro.";
SpdFlangeTopRadius="Le rayon sup�rieur du bord bomb� doit �tre une valeur positive.";
SpdFlangeTopRadius2Big="Le rayon sup�rieur du bord bomb� est trop important.";
SpdFlangeBotRadius="Le rayon inf�rieur du bord bomb� doit �tre une valeur positive.";
SpdFlangeBotRadius2Big="Le rayon inf�rieur du bord bomb� est trop important.";
SpdFlangeSidRadius="Le rayon lat�ral du bord bomb� doit �tre positif.";
SpdFlangeBothRadius2Big="La combinaison des rayons sup�rieur et inf�rieur du bord bomb� est trop importante.";
SpdFlangePullDirUnr="Impossible de convertir la direction de d�moulage du bord bomb�.";
SpdFlangeBoundaryProblem="Les boucles du contour de division n'ont pas pu �tre �tendues � la largeur requise. \nDiminuez la largeur.";
SpdFlangePullBad="\nEssayez de modifier la direction de d�moulage du bord tomb� de sorte que les boucles de l'ar�te de la d�coupe puissent �tre proprement projet�es.";

SpdSnapLockBadLength="Les longueurs de verrou doivent �tre sup�rieures � z�ro.";
SpdSnapLockRetAngle="L'angle de r�tention doit �tre sup�rieur � 0 et inf�rieur � 180 degr�s.";
SpdSnapLockRetAngle2="L'angle de r�tention doit �tre sup�rieur � 0 et inf�rieur ou �gal � 90 degr�s.";
SpdSnapLockInsertAngle="L'angle de r�tention doit �tre sup�rieur ou �gal � 0 et inf�rieur � 90 degr�s.";
SpdSnapLockInsertAngle2="L'angle d'insertion doit �tre sup�rieur � 0 et inf�rieur ou �gal � 90 degr�s.";
SpdSnapLockWidth="La largeur au niveau de la paroi doit �tre sup�rieure ou �gale � la largeur au niveau de la r�tention.";
SpdSnapLockThick="L'�paisseur au niveau de la paroi doit �tre sup�rieure ou �gale � l'�paisseur au niveau de la r�tention.";
SpdSnapLockRetLength="La longueur de r�tention doit �tre sup�rieure ou �gale � la longueur de l'extr�mit� du crochet.";
SpdSnapLockTolerance="Les tol�rances doivent �tre sup�rieures ou �gales � z�ro.";
SpdSnapLockWidth2="(Largeur - Epaisseur C�t�1 - Epaisseur C�t�2) doit �tre sup�rieur � z�ro.";
SpdSnapLockBadLength2="(Longueur - Epaisseur sup�rieure - Largeur de boucle) doit �tre sup�rieur � z�ro.";
SpdSnapLockWidth3="La partie du cliquet se trouvant dans le trou de r�tention doit �tre inf�rieure ou �gale � la largeur de boucle.";
SpdSnapLockWidth4="(Largeur - Epaisseur C�t�1 - Epaisseur C�t�2 - (2*Tol�rance de largeur du cliquet)) doit �tre sup�rieure � z�ro.";
SpdSnapLockBadLength3="Les angles sp�cifi�s et la largeur de l'extr�mit� du crochet obligent la longueur de r�tention � d�passer la limite sp�cifi�e.  \n Essayez d'augmenter les angles ou la longueur de r�tention, ou de diminuer la longueur sup�rieure du crochet.";
SpdSnapLockBadHeight1="Les angles sp�cifi�s obligent les courbes de r�tention produites � s'entrecouper.  \nEssayez d'�largir les angles.";
SpdSnapLockBadAngle="Les angles sp�cifi�s obligent la longueur de la base du cliquet � �tre inf�rieure � z�ro. \n Essayez de modifier les angles ou de diminuer la longueur et le d�calage du cliquet.";
SpdSnapLockNoPos="Aucune position de d�coupe s�lectionn�e.";
SpdSnapLockNoEngage="Le d�calage du cliquet est sup�rieur ou �gal sa hauteur et le cliquet\n ne s'engage pas dans la boucle.";

SpdCFSBooleanNVStar="Une erreur s'est produite lors de la suppression des volumes internes des volumes centraux.\n";
SpdCFSBooleanSVC="Une erreur s'est produite lors de la suppression des volumes centraux du corps.\n";
SpdCFSBooleanFNV="Une erreur s'est produite lors de la suppression des volumes prot�g�s du corps.\n";
SpdCFSBooleanENV="Une erreur s'est produite lors de la suppression des volumes n�gatifs du corps.\n";

SpdEdgeFilletPartial="R�sultat partiel : impossible de cr�er des cong�s sur certains �l�ments.";
SpdEdgeFilletUseLess="Avec la modification des sp�cifications, la fonction peut �tre inutile.";

SpdThickenFailed="Echec de l'ajout de sur�paisseur. Une ou plusieurs faces ne peuvent pas �tre �paissies. Essayez de modifier les entr�es.";
SpdThickenUnresolved="L'ajout de sur�paisseur a �chou�. Certaines faces n'ont pas pu �tre r�solues.";

SpdUpdateCycleError="Cette s�lection provoque un cycle de mises � jour.";
SpdShellPropsError="Aucun composant de propri�t� de coque disponible dans l'ensemble fonctionnel solide d�pendant des propri�t�s de coque.";
SpdShellPropsError01="Un ensemble fonctionnel solide ne peut pas contenir plus d'un �l�ment de propri�t� de coque.";

SPDCFSCompatibleFeature="Le composant est incompatible dans l'environnement en cours. Sa s�lection est interdite.";

SpdLLMCopyError="Vous ne pouvez pas copier/couper un modificateur local r�el si sa cible est un modificateur local.";
SpdLLMCopyError1="Interdiction de couper un modificateur direct local.";

