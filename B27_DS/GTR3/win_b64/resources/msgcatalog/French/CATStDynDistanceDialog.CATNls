//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//=============================================================================
//
// Distance Analysis Command :
//   Resource file for NLS purpose. 
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// Oct. 02   Creation                                          Minh-Duc TRUONG
//=============================================================================

Title="Distance";

FrmLeft.FrmSelection.Title="S�lection";
FrmLeft.FrmType.Title="Direction De Mesure"; // R12 Modification
FrmLeft.FrmProjection.Title="Espace de Projection "; // R12 Modification

FrmLeft.FrmSelection.SourcesRadB.Title="Premier ensemble";
FrmLeft.FrmSelection.SourcesRadB.LongHelp="
Permet la s�lection du
premier ensemble d'�l�ments.";

FrmLeft.FrmSelection.TargetsRadB.Title="Second ensemble";
FrmLeft.FrmSelection.TargetsRadB.LongHelp="
Permet la s�lection du
second ensemble d'�l�ments.";

FrmLeft.FrmInversion.InvertAnalysisPush.Title="Inverser l'analyse";
FrmLeft.FrmInversion.InvertAnalysisPush.ShortHelp="Inverse le sens du calcul de l'analyse";
FrmLeft.FrmInversion.InvertAnalysisPush.LongHelp="Inverse le sens du calcul de l'analyse";

FrmLeft.FrmSelection.RunningPtRadio.Title="Point courant";
FrmLeft.FrmSelection.RunningPtRadio.ShortHelp="Active le point courant";
FrmLeft.FrmSelection.RunningPtRadio.LongHelp="
Permet de calculer la distance
entre le point situ� sous la souris
et les �l�ments source.";

FrmLeft.FrmCenterBis.FrmDispIcons.DiagramPush.ShortHelp="Diagramme 2D";
FrmLeft.FrmCenterBis.FrmDispIcons.DiagramPush.LongHelp="Affiche le diagramme 2D (pour les �l�ments mono-dim mono-cellule seulement).";

FrmLeft.FrmCenterBis.FrmDispIcons.FullRadio.ShortHelp="Echelle de couleurs compl�te";
FrmLeft.FrmCenterBis.FrmDispIcons.FullRadio.LongHelp="Affiche l'analyse avec une �chelle de couleurs compl�te.";

FrmLeft.FrmCenterBis.FrmDispIcons.QuickRadio.ShortHelp="Echelle de couleurs restreinte";
FrmLeft.FrmCenterBis.FrmDispIcons.QuickRadio.LongHelp="Affiche l'analyse avec une �chelle de
couleurs restreinte pour une analyse rapide.";

FrmLeft.FrmCenterBis.FrmDispIcons.QuickCheck.ShortHelp="Echelle de couleurs restreinte"; // Light Mode
FrmLeft.FrmCenterBis.FrmDispIcons.QuickCheck.LongHelp="Affiche l'analyse avec une �chelle de
couleurs restreinte pour une analyse rapide.";

FrmLeft.FrmProjection.FrmProjMode.Radio3D.ShortHelp="Pas de projection des �l�ments";
FrmLeft.FrmProjection.FrmProjMode.Radio3D.LongHelp="Pas de projection des �l�ments";
FrmLeft.FrmProjection.FrmProjMode.RadioX.ShortHelp="Projection suivant la direction X";
FrmLeft.FrmProjection.FrmProjMode.RadioX.LongHelp="Calcule la distance entre les projections des �l�ments suivant la direction X.";
FrmLeft.FrmProjection.FrmProjMode.RadioY.ShortHelp="Projection suivant la direction Y";
FrmLeft.FrmProjection.FrmProjMode.RadioY.LongHelp="Calcule la distance entre les projections des �l�ments suivant la direction Y.";
FrmLeft.FrmProjection.FrmProjMode.RadioZ.ShortHelp="Projection suivant la direction Z";
FrmLeft.FrmProjection.FrmProjMode.RadioZ.LongHelp="Calcule la distance entre les projections des �l�ments suivant la direction Z.";
FrmLeft.FrmProjection.FrmProjMode.RadioCompass.ShortHelp="Projection suivant la direction de la boussole";
FrmLeft.FrmProjection.FrmProjMode.RadioCompass.LongHelp="Calcule la distance entre les projections des �l�ments suivant la direction de la boussole";
FrmLeft.FrmProjection.FrmProjMode.PlanarDistRadio.ShortHelp="Distance plane";
FrmLeft.FrmProjection.FrmProjMode.PlanarDistRadio.LongHelp="Calcule la distance entre un �l�ment mono-dim
et l'intersection du plan contenant cet �l�ment
et les �l�ments de l'autre ensemble.";

FrmLeft.FrmType.FrmTypeMode.NormalRadio.ShortHelp="Distance normale";
FrmLeft.FrmType.FrmTypeMode.NormalRadio.LongHelp="Calcule la plus petite distance normale.";

FrmLeft.FrmType.FrmTypeMode.RadioXMeasureDir.ShortHelp="Distance suivant la direction X"; // r12
FrmLeft.FrmType.FrmTypeMode.RadioXMeasureDir.LongHelp="Calcule la distance suivant la direction X.";

FrmLeft.FrmType.FrmTypeMode.RadioYMeasureDir.ShortHelp="Distance suivant la direction Y"; // r12
FrmLeft.FrmType.FrmTypeMode.RadioYMeasureDir.LongHelp="Calcule la distance suivant la direction Y.";

FrmLeft.FrmType.FrmTypeMode.RadioZMeasureDir.ShortHelp="Distance suivant la direction Z"; //r12
FrmLeft.FrmType.FrmTypeMode.RadioZMeasureDir.LongHelp="Calcule la distance suivant la direction Z.";

FrmLeft.FrmType.FrmTypeMode.RadioCompassMeasureDir.ShortHelp="Distance suivant la direction de la boussole"; // r12
FrmLeft.FrmType.FrmTypeMode.RadioCompassMeasureDir.LongHelp="Calcule la distance suivant la direction de la boussole.";

FrmLeft.FrmCenterBis.Title="Options d'affichage";
FrmLeft.FrmCenterBis.Espace1.Title=" ";
FrmLeft.FrmCenterBis.Espace2.Title="   ";
FrmLeft.FrmCenterBis.Espace3.Title="   ";
FrmLeft.FrmCenterBis.Espace4.Title="   ";

FrmLeft.FrmCenterBis.MorePsh.Title="Plus...";
FrmLeft.FrmCenterBis.LessPsh.Title="Moins...";

FrmRight.FrmDisplay.PointsChk.Title="Points";
FrmRight.FrmDisplay.PointsChk.LongHelp="Affiche les points color�s.";
FrmRight.FrmDisplay.MinMaxChk.Title="Valeurs Min/Max";
FrmRight.FrmDisplay.MinMaxChk.LongHelp="Affiche les valeurs minimum et maximum.";
FrmRight.FrmDisplay.ExactMaxChk.Title="Valeur max exacte";
FrmRight.FrmDisplay.ExactMaxChk.LongHelp="Affiche la valeur maximum exacte.";

FrmRight.FrmDisplay.ColorScaleChk.Title="Echelle de couleurs";
FrmRight.FrmDisplay.ColorScaleChk.LongHelp="Affiche/cache l'�chelle de couleurs.";

FrmRight.FrmDisplay.StatChk.Title="R�partition statistique";
FrmRight.FrmDisplay.StatChk.LongHelp="Affiche/cache la r�partition statistique.";

FrmRight.FrmDisplay.CombChk.Title="Epis";
FrmRight.FrmDisplay.CombChk.LongHelp="Affiche les �pis de distance.";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.Title="Epis";

FrmRight.FrmDisplay.FrmCombOptions.DecalLbl.Title="     ";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.EnvelopChk.Title="Enveloppe";
FrmRight.FrmDisplay.FrmCombOptions.FrmComb.EnvelopChk.LongHelp="Affiche la ligne bris�e reliant les extr�mit�s des �pis.";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.InvertChk.Title="Invers�s";
FrmRight.FrmDisplay.FrmCombOptions.FrmComb.InvertChk.LongHelp="Inverse les �pis.";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.AutoScaleChk.Title="Auto scale";
FrmRight.FrmDisplay.FrmCombOptions.FrmComb.AutoScaleChk.LongHelp="Affiche les �pis avec une longueur ind�pendante du zoom.";

FrmRight.FrmDisplay.Mapping.Title="Texture";
FrmRight.FrmDisplay.Mapping.LongHelp="Affiche le placage de texture.";


FrmRight.FrmDisplay.LimitsPoints.Title="Limites de courbes";
FrmRight.FrmDisplay.LimitsPoints.LongHelp="Relimite la courbe discr�tis�e.";

FrmRight.FrmDisplay.MaxDist.Title="Distance Max.";
FrmRight.FrmDisplay.MaxDist.LongHelp="D�finit la distance maximum.";

FrmLeft.FrmCenterBis.FrmDispIcons.MaxDist.Title="Distance Max."; // Light Mode 
FrmLeft.FrmCenterBis.FrmDispIcons.MaxDist.LongHelp="D�finit la distance maximum.";




FrmRight.FrmDiscretisation.Title="Discr�tisation";

FrmRight.FrmDiscretisation.AutoTrapChk.Title="Trappe automatique";
FrmRight.FrmDiscretisation.AutoTrapChk.ShortHelp="Suppression automatique de points";
FrmRight.FrmDiscretisation.AutoTrapChk.LongHelp="
Autorise l'�limination automatique des points cible
non concern�s pour chacune des
surfaces source.";
