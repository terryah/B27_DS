//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStCCKAnalysisDialog: Resource file for NLS purposes: 
// Connect Checker Dialog Box
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// Nov 2005						   HRY
//=============================================================================

Title="Connect Checker";
CCKTypeFrame.Title="Type";
GapFrame.Title="Gap";
GapFrame.Connection.Title="Connection";
TabContainer.Search.FullTabPage.Title="Search";
TabContainer.QuickTabPage.Title="Quick";
TabContainer.FullTabPage.Title="Full";
TabContainer.FullTabPage.ContinuityFrame.Title="Continuity";
TabContainer.FullTabPage.DisplayFrame.Title="Display";
TabContainer.FullTabPage.ScalingFrame.Title="Amplitude";

TabContainer.QuickTabPage.QuickModeFrame.G0Label = "            ";
TabContainer.QuickTabPage.QuickModeFrame.G1Label = "            ";
TabContainer.QuickTabPage.QuickModeFrame.G2Label = "            ";
TabContainer.QuickTabPage.QuickModeFrame.G3Label = "            ";
TabContainer.QuickTabPage.QuickModeFrame.G4Label = "            ";

CCKTypeFrame.SurSurCCKRadio.ShortHelp = "Surface-Surface";
CCKTypeFrame.SurSurCCKRadio.LongHelp = "Checks connection between two or more surfaces";
CCKTypeFrame.CurCurCCKRadio.ShortHelp = "Curve-Curve";
CCKTypeFrame.CurCurCCKRadio.LongHelp = "Checks connection between two or more curves";
CCKTypeFrame.SurCurCCKRadio.ShortHelp = "Surface-Curve";
CCKTypeFrame.SurCurCCKRadio.LongHelp = "Checks connection between surfaces and curves";

GapFrame.Connection.LongHelp = "Sets the minimum gap below which all gaps are analysed as connected";
GapFrame.UpperLimit.LongHelp = "Sets the maximum gap above which no analysis will be performed";
GapFrame.GapRadio.ShortHelp = "Gap";
GapFrame.GapRadio.LongHelp = "Allows user to select gap definition";
GapFrame.InternalEdgesRadio.ShortHelp = "Internal Edge";
GapFrame.InternalEdgesRadio.LongHelp = "The internal edges of the joint elements are taken into account to perform the analysis";

TabContainer.FullTabPage.ContinuityFrame.G0Radio.ShortHelp = "G0";
TabContainer.FullTabPage.ContinuityFrame.G0Radio.LongHelp = "Checks for G0 type continuity";
TabContainer.FullTabPage.ContinuityFrame.G1Radio.ShortHelp = "G1";
TabContainer.FullTabPage.ContinuityFrame.G1Radio.LongHelp = "Checks for G1 type continuity";
TabContainer.FullTabPage.ContinuityFrame.G2Radio.ShortHelp = "G2";
TabContainer.FullTabPage.ContinuityFrame.G2Radio.LongHelp = "Checks for G2 type continuity";
TabContainer.FullTabPage.ContinuityFrame.FullG2Concavity.ShortHelp = "Concavity Defect";
TabContainer.FullTabPage.ContinuityFrame.FullG2Concavity.LongHelp = "Checks for Concavity in G2 mode";
TabContainer.FullTabPage.ContinuityFrame.G3Radio.ShortHelp = "G3";
TabContainer.FullTabPage.ContinuityFrame.G3Radio.LongHelp = "Checks for G3 type continuity";

TabContainer.FullTabPage.DisplayFrame.Comb.ShortHelp = "Comb";
TabContainer.FullTabPage.DisplayFrame.Comb.LongHelp = "Displays Comb";
TabContainer.FullTabPage.DisplayFrame.Envelop.ShortHelp = "Envelop";
TabContainer.FullTabPage.DisplayFrame.Envelop.LongHelp = "Displays Envelop";
TabContainer.FullTabPage.DisplayFrame.MinInfo.ShortHelp = "MinInfo";
TabContainer.FullTabPage.DisplayFrame.MinInfo.LongHelp = "Displays minimum values";
TabContainer.FullTabPage.DisplayFrame.MaxInfo.ShortHelp = "MaxInfo";
TabContainer.FullTabPage.DisplayFrame.MaxInfo.LongHelp = "Displays maximum values";
TabContainer.FullTabPage.DisplayFrame.Discretization.ShortHelp = "Discretization";
TabContainer.FullTabPage.DisplayFrame.Discretization.LongHelp = "Allows the user to switch between the four predefined discretization coefficients";
TabContainer.FullTabPage.DisplayFrame.ColorScale.ShortHelp = "ColorScale";
TabContainer.FullTabPage.DisplayFrame.ColorScale.LongHelp = "Displays the analysis with full color range";

TabContainer.FullTabPage.ScalingFrame.AutoScaling.ShortHelp = "Auto Scaling";
TabContainer.FullTabPage.ScalingFrame.AutoScaling.LongHelp = "Enables auto scaling";
TabContainer.FullTabPage.ScalingFrame.MultiplyTwo.ShortHelp = "MultiplyTwo";
TabContainer.FullTabPage.ScalingFrame.MultiplyTwo.LongHelp = "Multiplies scale factor by 2";
TabContainer.FullTabPage.ScalingFrame.DivideTwo.ShortHelp = "DivideTwo";
TabContainer.FullTabPage.ScalingFrame.DivideTwo.LongHelp = "Divides scale factor by 2";

TabContainer.QuickTabPage.QuickModeFrame.G0Radio.ShortHelp = "G0";
TabContainer.QuickTabPage.QuickModeFrame.G0Radio.LongHelp = "Checks for G0 type continuity";
TabContainer.QuickTabPage.QuickModeFrame.G1Radio.ShortHelp = "G1";
TabContainer.QuickTabPage.QuickModeFrame.G1Radio.LongHelp = "Checks for G1 type continuity";
TabContainer.QuickTabPage.QuickModeFrame.G2Radio.ShortHelp = "G2";
TabContainer.QuickTabPage.QuickModeFrame.G2Radio.LongHelp = "Checks for G2 type continuity";
TabContainer.QuickTabPage.QuickModeFrame.G3Radio.ShortHelp = "G2";
TabContainer.QuickTabPage.QuickModeFrame.G3Radio.LongHelp = "Checks for G3 type continuity";
TabContainer.QuickTabPage.QuickModeFrame.OverlapRadio.ShortHelp = "Overlap";
TabContainer.QuickTabPage.QuickModeFrame.OverlapRadio.LongHelp = "Checks for overlap type continuity";

