Help = "Defines options for export";
//ExportMode.HeaderFrame.Global.Title = "Export Mode";


//------------------------------------------------------------------------------------------------------------------------------------
// DXF 
//------------------------------------------------------------------------------------------------------------------------------------

Type.DXF.Title="DXF"; 


// File Name 
Type.DXF.FileNamePattern1.HeaderFrame.Global.Title = "File Name Pattern";
Type.DXF.FileNamePattern1.HeaderFrame.Global.LongHelp = "Defines file name pattern, 
Usable keywords to generate the name of the export files are: 
%PARTFILENAME% %PARTNUMBER% %PLYGROUP% %SEQUENCE% %PLY% %CUTPIECE%  %PLYORCUTPIECE% %MATERIALNAME% %MATERIALID% %DIRECTIONNAME% %DIRECTIONVALUE%
You can add other constant characters supported by Windows."; 

Type.DXF.FileNamePattern1.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock1.Title = " ";
Type.DXF.FileNamePattern1.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock1.Help = "Define administration for File name pattern.";
Type.DXF.FileNamePattern1.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock1.LongHelp = "Define administration for File name pattern.";

Type.DXF.FileNamePattern1.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt1.Title = "File name definition for exported DXF";
Type.DXF.FileNamePattern1.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt1.Help    = "Allow to define exported file name.";
Type.DXF.FileNamePattern1.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt1.LongHelp ="Allow to define exported file name.  
the file name can contain keyword for naming  , 
for example: CompositesExportOf_%PARTFILENAME%_%PARTNUMBER%_%PLYGROUP%_%SEQUENCE%_%PLYORCUTPIECE%_%MATERIALNAME%_%DIRECTIONNAME%"; 


//Export Mode 
Type.DXF.ExportMode1.HeaderFrame.Global.Title = "Export Mode";
Type.DXF.ExportMode1.HeaderFrame.Global.LongHelp = "Defines the content of the export files.";


Type.DXF.ExportMode1.IconAndOptionsFrame.OptionsFrame.ExportModeLock1.Title = " ";
Type.DXF.ExportMode1.IconAndOptionsFrame.OptionsFrame.ExportModeLock1.Help = "Define administration for entities included in ply export.";
Type.DXF.ExportMode1.IconAndOptionsFrame.OptionsFrame.ExportModeLock1.LongHelp = "Define administration for entities included in ply export.";

Type.DXF.ExportMode1.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.Title = "One file per entity (ply or sub-ply)";
Type.DXF.ExportModel1.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.Help = "One file per entity (ply or sub-ply)";
Type.DXF.ExportModel1.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.LongHelp = "One file per entity (ply or sub-ply)";

Type.DXF.ExportMode1.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.Title = "One file per material";
Type.DXF.ExportModel1.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.Help = "One file per material";
Type.DXF.ExportModel1.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.LongHelp = "One file per material";

Type.DXF.ExportMode1.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.Title = "One file per ply (all sub-plies)";
Type.DXF.ExportModel1.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.Help = "One file per ply (all sub-plies)";
Type.DXF.ExportModel1.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.LongHelp = "One file per ply (all sub-plies)";


//Includes 

Type.DXF.IncludeDXF.HeaderFrame.Global.Title = "Entities to Export";
Type.DXF.IncludeDXF.HeaderFrame.Global.LongHelp = "Specifies the entities to export:
- Strategy point (as a 3D point added to the flatten shape).
- Rosette (as 2 segments in the flatten shape).
- Annotation (added in the flatten shape). When this check box is selected, Annotation Pattern, Text Orientation and Font options become available."; 


Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.IncludeLock1.Title = " ";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.IncludeLock1.Help = "Define administration for entities included in ply export.";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.IncludeLock1.LongHelp = "Define administration for entities included in ply export.";

Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.StrategyPt1.Title = "Strategy Point";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.StrategyPt1.Help = "Includes Strategy Point";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.StrategyPt1.LongHelp = "Includes Strategy Point(Seed Point)";

Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.Rosette1.Title = "Rosette";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.Rosette1.Help = "Includes Rosette";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.Rosette1.LongHelp = "Includes Rosette";

Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.Annotation1.Title = "Annotation";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.Annotation1.Help = "Includes Annotation";
Type.DXF.IncludeDXF.IconAndOptionsFrame.OptionsFrame.Annotation1.LongHelp = "Includes Annotation";


//Annotation Pattern 

Type.DXF.AnnotationPattern1.HeaderFrame.Global.Title = "Annotation Pattern";
Type.DXF.AnnotationPattern1.HeaderFrame.Global.LongHelp = "Defines the annotation pattern using the same keywords as the File Name Pattern.
 %PARTFILENAME%
 %PARTNUMBER% 
 %PLYGROUP% 
 %SEQUENCE% 
 %PLY% 
 %CUTPIECE%
 %PLYORCUTPIECE% 
 %MATERIALNAME% 
 %MATERIALID% 
 %DIRECTIONNAME% 
 %DIRECTIONVALUE%";

Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternLock1.Title = " ";
Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternLock1.Help = "Defines administration for entities included in ply export.";
Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternLock1.LongHelp = "Defines administration for entities included in ply export.";

Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternChk1.Title = "Annotation definition";
Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternChk1.Help = "Annotation definition";
Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternChk1.LongHelp = "Annotation definition"; 

Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternEdt1.Title = "Annotation editor";
Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternEdt1.Help    = "Allows to define annotation associated to flatten data exported";
Type.DXF.AnnotationPattern1.IconAndOptionsFrame.OptionsFrame.AnnotationPatternEdt1.LongHelp ="Allows to define annotation associated to flatten data exported
for example: 
Part        : %PARTFILENAME% 
PlyGroup    : %PLYGROUP%
Sequence    : %SEQUENCE%
Ply&Cutpiece: %PLY% %CUTPIECE%
material    : %MATERIALID%
Direction   : %DIRECTIONNAME%";

//Text Font 
Type.DXF.TextFont1.HeaderFrame.Global.Title = "Text Font for Stacking Preview ";
Type.DXF.TextFont1.HeaderFrame.Global.LongHelp = "Defines the text font type for the stacking preview.
Important: This font applies exclusively to the preview.
When exporting to DXF/DWG, the font defined as default in the Drafting standard applies.
Contact your administrator to learn which default font is used, and possibly change it in the standard.";


//TextOrientation  
Type.DXF.TextOrientation1.HeaderFrame.Global.Title = "Text Position";
Type.DXF.TextOrientation1.HeaderFrame.Global.LongHelp = "Defines the text position strategy, 
Is active if Annotation is selected under Entities to Export.
By default
- The top left corner of the text bounding box is coincident with the flatten seed point.
- The text direction is the 0� direction of the flatten rosette.
If the text does not lie within  the boundaries of the flatten ply or cut-piece, and if  Compute text position is selected,
the text is automatically positioned to fit the boundaries (when possible)
-	Either on a computed point (Anchor)
-	Or on a computed  point and with an optimized direction in the flatten plane (Anchor and direction).
Positioning by hand remains possible."; 

Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.TextOrientationLock1.Title = " ";
Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.TextOrientationLock1.Help = "Defines administration for entities included in ply export.";
Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.TextOrientationLock1.LongHelp = "Defines administration for entities included in ply export.";

Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.TextOrientationChk1.Title = "Compute text position: ";
Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.TextOrientationChk1.Help = "Computes text position: reposition text in flatten shape";
Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.TextOrientationChk1.LongHelp = "Computes  text position: reposition text in flatten shape";

Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.ModeDirection1.Title = "Anchor";
Type.DXF.TextOrientation1.IconAndOptionsFrame.OptionsFrame.ModeAutomatic1.Title = "Anchor and direction ";


// Layer 
Type.DXF.LayerOptions.HeaderFrame.Global.Title = "Layer Options ";
Type.DXF.LayerOptions.HeaderFrame.Global.LongHelp = "Specifies how categories are exported to layers, either in separate layers or not.
If this check box is not selected, several categories can be exported to the same layer."; 


Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerActivationLock1.Title = "";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerActivationLock1.Help = "Define administration for entities included in ply export.";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerActivationLock1.LongHelp = "Define administration for entities included in ply export.";


Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerActivationChk1.Title = "Export each category in a separate DXF layer"; 
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerActivationChk1.Help = "Exports each Category in a separate DXF layer";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerActivationChk1.LongHelp ="Exports each Category in a separate DXF layer";

Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerMode.Title        = "Keep in export: ";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerMode.LongHelp     = "Defines how the layers are driven, by their index or by their name.
The color are used in the Stacking Management preview."; 


Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerNumber.Title      = "Number"; 
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerName.Title        = "Name";
//Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColor.Title       = "Color";

Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.ExternalContours.Title  = "Outer contour";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.InternalContours.Title  = "Inner contours"; 
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.Rosette.Title           = "Rosette";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.Strategy.Title          = "Strategy point";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.Markers.Title           = "Markers";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.CutElements.Title       = "Cuts";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.TextElements.Title      = "Text elements";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerDefinition.NoCategory.Title        = "No category";

Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorConservationLock1.Title = " ";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorConservationLock1.Help = "Define administration for color inheritance in ply export.";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorConservationLock1.LongHelp = "Define administration for color inheritance in ply export.";

Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorconservation.LayerColorMode.Title = "Colors in DXF Export: ";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorconservation.LayerColorMode.LongHelp = "Defines in which color flatten geometry is exported to DXF.
- 3D colors: Colors used to display the flatten geometry in the graphic area. They are set by the material orientation in the ply or cut-piece. 
- Layer colors: Colors used to categorize flatten body elements.
- Black and white: The flatten geometry is exported in black."; 

Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorconservation.3DColor.Title = "3D colors";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorconservation.LayerColor.Title = "Layer colors";
Type.DXF.LayerOptions.IconAndOptionsFrame.OptionsFrame.LayerColorconservation.BlackWhite.Title = "Black and white";

//force use of DXF_ARC_EXPORT=2 variable
Type.DXF.ForceARC_EXPORTframe.HeaderFrame.Global.Title = "DXF Generation Preference";
Type.DXF.ForceARC_EXPORTframe.HeaderFrame.Global.LongHelp = "Defines DXF generation preference";

Type.DXF.ForceARC_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceARC_EXPORTLock1.Title = " ";
Type.DXF.ForceARC_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceARC_EXPORTLock1.Help = "Define administration for arc export.";
Type.DXF.ForceARC_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceARC_EXPORTLock1.LongHelp = "Define administration for arc export.";

Type.DXF.ForceARC_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceARC_EXPORTChk.Title = "Use polyline instead of arc when connectivity issue may occur";
Type.DXF.ForceARC_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceARC_EXPORTChk.Help = "Arc with very large radius reach DXF precision limit and could cause connectivity issue.";
Type.DXF.ForceARC_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceARC_EXPORTChk.LongHelp = "Arc with very large radius reach DXF precision limit. Arc precision is quasi perfect but it extremities may have a nanometer wide connectivity issue. Thus, Cutting machine cannot handle this. Replacing by polyline ensure connectivity at cost of lack of precision on rest of the curve. Precision still remain accurate for cutting machine. (Effect is similar to DXF_ARC_EXPORT=2 variable but it only apply on ply export)";

//force use of DXF_SPLINE_EXPORT=0 variable
Type.DXF.ForceDXF_SPLINE_EXPORTframe.HeaderFrame.Global.Title = "DXF generation preference";

Type.DXF.ForceDXF_SPLINE_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceDXF_SPLINE_EXPORTLock1.Title = " ";
Type.DXF.ForceDXF_SPLINE_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceDXF_SPLINE_EXPORTLock1.Help = "Define administration for spline export.";
Type.DXF.ForceDXF_SPLINE_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceDXF_SPLINE_EXPORTLock1.LongHelp = "Define administration for spline export.";

Type.DXF.ForceDXF_SPLINE_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceDXF_SPLINE_EXPORTChk.Title = "Do not create spline in DXF export ";
Type.DXF.ForceDXF_SPLINE_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceDXF_SPLINE_EXPORTChk.Help = "No spline will be generated in DXF file.";
Type.DXF.ForceDXF_SPLINE_EXPORTframe.IconAndOptionsFrame.OptionsFrame.ForceDXF_SPLINE_EXPORTChk.LongHelp = "No spline will be generated in DXF file.";

//------------------------------------------------------------------------------------------------------------------------------------
//IGES2D
//------------------------------------------------------------------------------------------------------------------------------------

//
Type.IGES2D.Title="IGES2D"; 

// File Name 
Type.IGES2D.FileNamePattern2.HeaderFrame.Global.Title = "File Name Pattern";
Type.IGES2D.FileNamePattern2.HeaderFrame.Global.LongHelp = "Defines file name pattern, 
Usable keywords to generate the name of the export files are: 
%PARTFILENAME% %PARTNUMBER% %PLYGROUP% %SEQUENCE% %PLY% %CUTPIECE%  %PLYORCUTPIECE% %MATERIALNAME% %MATERIALID% %DIRECTIONNAME% %DIRECTIONVALUE%
You can add other constant characters supported by Windows."; 

Type.IGES2D.FileNamePattern2.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock2.Title = " ";
Type.IGES2D.FileNamePattern2.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock2.Help = "Define administration for File name pattern.";
Type.IGES2D.FileNamePattern2.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock2.LongHelp = "Define administration for File name pattern.";

Type.IGES2D.FileNamePattern2.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt2.Title = "File name deniition for exported IGES2D";
Type.IGES2D.FileNamePattern2.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt2.Help    = "Allow to define exported file name.";
Type.IGES2D.FileNamePattern2.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt2.LongHelp ="Allow to define exported file name.  
the file name can contain keyword for naming  , 
for example: CompositesExportOf_%PARTFILENAME%_%PARTNUMBER%_%PLYGROUP%_%SEQUENCE%_%PLYORCUTPIECE%_%MATERIALNAME%_%DIRECTIONNAME%"; 

Type.IGES2D.ExportMode2.HeaderFrame.Global.Title = "Export Mode";
Type.IGES2D.ExportMode2.HeaderFrame.Global.LongHelp = "Defines the content of the export files."; 


Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ExportModeLock2.Title = " ";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ExportModeLock2.Help = "Define administration for entities included in ply export.";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ExportModeLock2.LongHelp = "Define administration for entities included in ply export.";

Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.Title = "One file per entity (ply or sub-ply)";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.Help = "One file per entity (ply or sub-ply)";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.LongHelp = "One file per entity (ply or sub-ply)";

Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.Title = "One file per material";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.Help = "One file per material";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.LongHelp = "One file per material";

Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.Title = "One file per ply (all sub-plies)";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.Help = "One file per ply (all sub-plies)";
Type.IGES2D.ExportMode2.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.LongHelp = "One file per ply (all sub-plies)";

Type.IGES2D.IncludeIGES2D.HeaderFrame.Global.Title = "Entities to Export";
Type.IGES2D.IncludeIGES2D.HeaderFrame.Global.LongHelp  = "Specifies the entities to export:
- Strategy point (as a 3D point added to the flatten shape).
- Rosette (as 2 segments in the flatten shape).
- Annotation (added in the flatten shape). When this check box is selected, Annotation Pattern, Text Orientation and Font options become available."; 


Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.IncludeLock2.Title = " ";
Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.IncludeLock2.Help = "Define administration for entities included in ply export.";
Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.IncludeLock2.LongHelp = "Define administration for entities included in ply export.";

Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.StrategyPt2.Title = "Strategy Point";
Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.StrategyPt2.Help = "Includes Strategy Point";
Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.StrategyPt2.LongHelp = "Includes Strategy Point(Seed Point)";

Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.Rosette2.Title = "Rosette";
Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.Rosette2.Help = "Includes Rosette";
Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.Rosette2.LongHelp = "Includes Rosette";

Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.Annotation2.Title = "Annotation";
Type.IGES2D.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.Annotation2.Help = "Includes Annotation";
Type.IGES2D.IncludeIGES2D.IconAndOptionsFrame.OptionsFrame.Annotation2.LongHelp = "Includes Annotation";

//Annotation Pattern 
Type.IGES2D.AnnotationPattern2.HeaderFrame.Global.Title = "Annotation Pattern";
Type.IGES2D.AnnotationPattern2.HeaderFrame.Global.LongHelp = "Defines the annotation pattern using the same keywords as the File Name Pattern.
 %PARTFILENAME%
 %PARTNUMBER% 
 %PLYGROUP% 
 %SEQUENCE% 
 %PLY% 
 %CUTPIECE%
 %PLYORCUTPIECE% 
 %MATERIALNAME% 
 %MATERIALID% 
 %DIRECTIONNAME% 
 %DIRECTIONVALUE%";

Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternLock2.Title = " ";
Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternLock2.Help = "Define administration for entities included in ply export.";
Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternLock2.LongHelp = "Define administration for entities included in ply export.";

Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternChk2.Title = "Annotation definition";
Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternChk2.Help = "Annotation definition";
Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternChk2.LongHelp = "Annotation definition"; 

Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternEdt2.Title = "Annotation editor";
Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternEdt2.Help = "Allows to define annotation associated to flatten data exported";
Type.IGES2D.AnnotationPattern2.IconAndOptionsFrame.OptionsFrame.AnnotationPatternEdt2.LongHelp ="Allows to define annotation associated to flatten data exported
for example: 
Part        : %PARTFILENAME% 
PlyGroup    : %PLYGROUP%
Sequence    : %SEQUENCE%
Ply&Cutpiece: %PLY% %CUTPIECE%
material    : %MATERIALID%
Direction   : %DIRECTIONNAME%";

//Text Font 
Type.IGES2D.TextFont2.HeaderFrame.Global.Title = "Text Font for Stacking Preview ";
Type.IGES2D.TextFont2.HeaderFrame.Global.LongHelp = "Defines the text font type for the stacking preview.
Important: This font applies exclusively to the preview.
When exporting to IGES2D, the font defined as default in the Drafting standard applies.
Contact your administrator to learn which default font is used, and possibly change it in the standard."; 


Type.IGES2D.TextFont2.IconAndOptionsFrame.OptionsFrame.TextFontLock2.Title = " ";
Type.IGES2D.TextFont2.IconAndOptionsFrame.OptionsFrame.TextFontLock2.Help = "Define administration for entities included in ply export.";
Type.IGES2D.TextFont2.IconAndOptionsFrame.OptionsFrame.TextFontLock2.LongHelp = "Define administration for entities included in ply export.";

//TextOrientation  
Type.IGES2D.TextOrientation2.HeaderFrame.Global.Title = "Text Position";
Type.IGES2D.TextOrientation2.HeaderFrame.Global.LongHelp ="Defines the text position strategy, 
Is active if Annotation is selected under Entities to Export.
By default
- The top left corner of the text bounding box is coincident with the flatten seed point.
- The text direction is the 0� direction of the flatten rosette.
If the text does not lie within  the boundaries of the flatten ply or cut-piece, and if  Compute text position is selected,
the text is automatically positioned to fit the boundaries (when possible)
-	Either on a computed point (Anchor)
-	Or on a computed  point and with an optimized direction in the flatten plane (Anchor and direction).
Positioning by hand remains possible."; 

Type.IGES2D.TextOrientation2.IconAndOptionsFrame.OptionsFrame.TextOrientationLock2.Title = " ";
Type.IGES2D.TextOrientation2.IconAndOptionsFrame.OptionsFrame.TextOrientationLock2.Help = "Define administration for entities included in ply export.";
Type.IGES2D.TextOrientation2.IconAndOptionsFrame.OptionsFrame.TextOrientationLock2.LongHelp = "Defines administration for entities included in ply export.";

Type.IGES2D.TextOrientation2.IconAndOptionsFrame.OptionsFrame.TextOrientationChk2.Title = "Compute text position: ";
Type.IGES2D.TextOrientation2.IconAndOptionsFrame.OptionsFrame.TextOrientationChk2.LongHelp = "Computes text position: reposition text in flatten shape";

Type.IGES2D.TextOrientation2.IconAndOptionsFrame.OptionsFrame.ModeDirection2.Title = "Anchor";
Type.IGES2D.TextOrientation2.IconAndOptionsFrame.OptionsFrame.ModeAutomatic2.Title = "Anchor and Direction";

//------------------------------------------------------------------------------------------------------------------------------------
//IGES3D
//------------------------------------------------------------------------------------------------------------------------------------
Type.IGES3D.Title="IGES3D"; 

// File Name 
Type.IGES3D.FileNamePattern3.HeaderFrame.Global.Title = "File Name Pattern";
Type.IGES3D.FileNamePattern3.HeaderFrame.Global.LongHelp = "Defines file name pattern, 
Usable keywords to generate the name of the export files are: 
%PARTFILENAME% %PARTNUMBER% %PLYGROUP% %SEQUENCE% %PLY% %CUTPIECE%  %PLYORCUTPIECE% %MATERIALNAME% %MATERIALID% %DIRECTIONNAME% %DIRECTIONVALUE%
You can add other constant characters supported by Windows."; 


Type.IGES3D.FileNamePattern3.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock3.Title = " ";
Type.IGES3D.FileNamePattern3.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock3.Help = "Define administration for File name pattern.";
Type.IGES3D.FileNamePattern3.IconAndOptionsFrame.OptionsFrame.FileNamePatternLock3.LongHelp = "Define administration for File name pattern.";

Type.IGES3D.FileNamePattern3.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt3.Title = "File name deniition for exported IGES3D";
Type.IGES3D.FileNamePattern3.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt3.Help    = "Allow to define exported file name.";
Type.IGES3D.FileNamePattern3.IconAndOptionsFrame.OptionsFrame.FileNamePatternEdt3.LongHelp ="Allow to define exported file name.  
the file name can contain keyword for naming  , 
for example: CompositesExportOf_%PARTFILENAME%_%PARTNUMBER%_%PLYGROUP%_%SEQUENCE%_%PLYORCUTPIECE%_%MATERIALNAME%_%DIRECTIONNAME%"; 

//IGES3D

Type.IGES3D.ExportMode3.HeaderFrame.Global.Title = "Export Mode";
Type.IGES3D.ExportMode3.HeaderFrame.Global.LongHelp = "Defines the content of the export files."; 

Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ExportModeLock3.Title = " ";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ExportModeLock3.Help = "Define administration for entities included in ply export.";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ExportModeLock3.LongHelp = "Define administration for entities included in ply export.";

Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.Title = "One file per entity (ply or sub-ply)";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.Help = "One file per entity (ply or sub-ply)";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERENTITY.LongHelp = "One file per entity (ply or sub-ply)";

Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.Title = "One file per material";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.Help = "One file per material";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERMAT.LongHelp = "One file per material";

Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.Title = "One file per ply (all sub-plies)";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.Help = "One file per ply (all sub-plies)";
Type.IGES3D.ExportMode3.IconAndOptionsFrame.OptionsFrame.ONEPERPLY.LongHelp = "One file per ply (all sub-plies)";


Type.IGES3D.IGES3DContourOption.HeaderFrame.Global.Title = "IGES 3D Contour Options";
Type.IGES3D.IGES3DContourOption.HeaderFrame.Global.LonHelp = "Defines IGES3D contour options";

Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.TakeThicknessUpdateIntoAccountForIGES3D.Title    = "Take thickness update into account";
Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.TakeThicknessUpdateIntoAccountForIGES3D.Help     = "Take thickness update into account";
Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.TakeThicknessUpdateIntoAccountForIGES3D.LongHelp = "Take thickness update into account";

Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.FullStacking.Title = "On full stacking";
Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.FullStacking.Help = "On full stacking";
Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.FullStacking.LongHelp = "On full stacking";

Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.PlyGroupOnly.Title = "On ply group only";
Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.PlyGroupOnly.Help = "On ply group only";
Type.IGES3D.IGES3DContourOption.IconAndOptionsFrame.OptionsFrame.PlyGroupOnly.LongHelp = "On ply group only";


