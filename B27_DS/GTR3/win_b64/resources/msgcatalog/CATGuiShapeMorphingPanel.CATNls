//----------------------------------------------
// Resource file for CATGuiShapeMorphingPanel class
// En_US
//----------------------------------------------
Title="Shape Morphing Deformation Definition";

FraShapeMorphing.LabToDeform.Title="Element to deform:";
FraShapeMorphing.LabToDeform.LongHelp="Specifies the element to be deformed.";
FraShapeMorphing.FraToDeform.LongHelp="Specifies the element to be deformed.";

FraShapeMorphing.Propagation.Title="Constraint propagation:";
FraShapeMorphing.Propagation.LongHelp="Specifies the attenuation of the constraint propagation.";
FraShapeMorphing.Energy.LongHelp="Specifies the attenuation of the constraint propagation.";

TabContainer.DeformTabPage.Title="Deformation Elements";

TabContainer.DeformTabPage.LabDeformList.Title=" ";
TabContainer.DeformTabPage.LabDeformList.LongHelp="Specifies the deformation elements.";
TabContainer.DeformTabPage.DeformList.LongHelp="Specifies the deformation elements.";

// number of selected elements
NumberColumnTitle = "No.";

//Deformation elements
ReferencesColumn = "References";
DefTypesColumn = "Def. Types";
TargetsColumn = "Targets";
ConstraintsColumn = "Constraints";
SupportsColumn = "Supports";
AutoCouplingPointsColumn = "Auto. Coupling Points";

//Limit elements
LimitCurvesColumn = "Limit Curves";
LimitCurvesLabel = "Limit curve";
ContinuitiesColumn = "Continuities";
ContinuitiesLabel = "Continuity";
DirectionsColumn = "Directions";
DirectionsLabel = "Direction";
LeftType = "Left";
RightType = "Right";

//Coupling elements
CouplingReferencesColumn = "Coupling References";
CouplingReferencesLabel = "Coupling reference";
CouplingTargetsColumn = "Coupling Targets";
CouplingTargetsLabel = "Coupling target";

//Rigid zones
RigidZonesColumn = "Rigid Zones";
RigidZonesLabel = "Rigid zone";

TabContainer.DeformTabPage.FraDeformButton.AddDeformButton.Title="Add";
TabContainer.DeformTabPage.FraDeformButton.AddDeformButton.LongHelp="Enables you to add a new deformation element to the list.";
TabContainer.DeformTabPage.FraDeformButton.RemoveDeformButton.Title="Remove";
TabContainer.DeformTabPage.FraDeformButton.RemoveDeformButton.LongHelp="Enables you to remove the selected deformation element from the list.";

TabContainer.DeformTabPage.FraDeformElem.LabReference.Title="Reference:";
TabContainer.DeformTabPage.FraDeformElem.LabReference.LongHelp="Specifies the reference elements.";
TabContainer.DeformTabPage.FraDeformElem.FraReference.LongHelp="Specifies the reference elements.";

TabContainer.DeformTabPage.FraDeformElem.LabDefType.Title="Type:";
TabContainer.DeformTabPage.FraDeformElem.LabDefType.LongHelp="Specifies the type of the deformation applied.";
TabContainer.DeformTabPage.FraDeformElem.DefType.LongHelp="Specifies the type of the deformation applied.";

TabContainer.DeformTabPage.FraDeformElem.LabTarget.Title="Target:";
TabContainer.DeformTabPage.FraDeformElem.LabTarget.LongHelp="Specifies the target elements.";
TabContainer.DeformTabPage.FraDeformElem.FraTarget.LongHelp="Specifies the target elements.";

TabContainer.DeformTabPage.FraDeformElem.LabContinuityType.Title="Constraint:";
TabContainer.DeformTabPage.FraDeformElem.CmbTargetContType.LongHelp="Lists the available continuity types on 
the target curve with the associated support surface.";
TabContainer.DeformTabPage.FraDeformElem.LabContinuityType.LongHelp="Lists the available continuity types on 
the target curve with the associated support surface.";

TabContainer.DeformTabPage.FraDeformElem.LabTargetCont.Title="Support:";
TabContainer.DeformTabPage.FraDeformElem.LabTargetCont.LongHelp="Specifies the support surface.";
TabContainer.DeformTabPage.FraDeformElem.FraTargetCont.LongHelp="Specifies the support surface.";

TabContainer.DeformTabPage.FraDeformElem.LabTypeOfAutoCouplingPoints.Title="Automatic coupling points:";
TabContainer.DeformTabPage.FraDeformElem.LabTypeOfAutoCouplingPoints.LongHelp="Specifies the way to automatically compute coupling points.";
TabContainer.DeformTabPage.FraDeformElem.CmbTypeOfAutoCouplingPoints.LongHelp="Specifies the way to automatically compute coupling points.";

TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.LabRefsOrigin.Title="Reference's origin:";
TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.LabRefsOrigin.LongHelp="Specifies the first vertex of the reference element.";
TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.FraRefsOrigin.LongHelp="Specifies the first vertex of the reference element.";

TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.LabTgtsOrigin.Title="Target's origin:";
TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.LabTgtsOrigin.LongHelp="Specifies the first vertex of the target element.";
TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.FraTgtsOrigin.LongHelp="Specifies the first vertex of the target element.";

TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.PshDirACPV.Title="Reverse Direction";
TabContainer.DeformTabPage.InfoForAutoCouplingPointsInVetices.PshDirACPV.LongHelp="Specifying the origins enables to couple them.
Reversing the direction enables to couple the reference's second vertex with the target's second or second last vertex.";

TabContainer.LimitTabPage.Title="Limit Elements";

TabContainer.LimitTabPage.FraList.LongHelp="Curves that limit the area of deformation.";

TabContainer.LimitTabPage.FraLimitButton.AddLimitButton.Title="Add";
TabContainer.LimitTabPage.FraLimitButton.AddLimitButton.LongHelp="Enables you to add a new limit element to the list.";
TabContainer.LimitTabPage.FraLimitButton.RemoveLimitButton.Title="Remove";
TabContainer.LimitTabPage.FraLimitButton.RemoveLimitButton.LongHelp="Enables you to remove the selected limit element from the list.";

TabContainer.LimitTabPage.FraLimitSupContDir.LabLimitSupport.Title="Support surface:";
TabContainer.LimitTabPage.FraLimitSupContDir.LabLimitSupport.LongHelp="Surface on which the wire to deform and all the limit curves lay.";
TabContainer.LimitTabPage.FraLimitSupContDir.FraLimitSupport.LongHelp="Surface on which the wire to deform and all the limit curves lay.";

TabContainer.LimitTabPage.FraLimitSupContDir.LabContinuityType.Title="Continuity:";
TabContainer.LimitTabPage.FraLimitSupContDir.LabContinuityType.LongHelp="Lists the available continuity types on
the limit curve.";
TabContainer.LimitTabPage.FraLimitSupContDir.CmbLimitContType.LongHelp="Lists the available continuity types on
the limit curve.";

TabContainer.LimitTabPage.FraLimitSupContDir.PshDir.Title="Reverse Direction";
TabContainer.LimitTabPage.FraLimitSupContDir.PshDir.LongHelp="Deforms the element to deform on the other side 
of the limit curve.
The arrow indicates the side of the deformation area.";

TabContainer.CouplingTabPage.Title="Manual Coupling Points";

TabContainer.CouplingTabPage.LabCouplingList.Title=" ";
TabContainer.CouplingTabPage.LabCouplingList.LongHelp="Specifies the coupling points.";
TabContainer.CouplingTabPage.CouplingList.LongHelp="Specifies the coupling points.";

CouplingColumn = "Coupling Points";

TabContainer.CouplingTabPage.LabCoupling.Title="Coupling Point:";
TabContainer.CouplingTabPage.LabCoupling.LongHelp="Specifies the coupling points.";
TabContainer.CouplingTabPage.FraCoupling.LongHelp="Specifies the coupling points.";

TabContainer.CouplingTabPage.FraCouplingButton.RemoveCouplingButton.Title="Remove";
TabContainer.CouplingTabPage.FraCouplingButton.RemoveCouplingButton.LongHelp="Enables you to remove the selected coupling pair from the list.";
TabContainer.CouplingTabPage.FraCouplingButton.AddCouplingButton.Title="Add";
TabContainer.CouplingTabPage.FraCouplingButton.AddCouplingButton.LongHelp="Enables you to add a new coupling pair to the list.";
TabContainer.CouplingTabPage.FraCouplingPair.LabCouplingReference.Title="Coupling reference:";
TabContainer.CouplingTabPage.FraCouplingPair.LabCouplingTarget.Title="Coupling target:";

TabContainer.RigidTabPage.Title="Rigid Zones";

TabContainer.RigidTabPage.LabRigidList.Title=" ";
TabContainer.RigidTabPage.LabRigidList.LongHelp="Specifies the rigid zones.";
TabContainer.RigidTabPage.RigidList.LongHelp="Specifies the rigid zones.";

RigidColumn = "Rigid Zones";

TabContainer.RigidTabPage.LabIndustryType.Title="Recognition context: ";

TabContainer.RigidTabPage.LabIndustryType.LongHelp=
"Specifies the recognition context. 
If rigid zones share constant fillets (resp. chamfers) 
with the surface to deform whose radii (resp. lengths) 
belong to the selected context, the rigid zones will be 
automatically sewn to the deformed surface.";

TabContainer.RigidTabPage.CmbIndustryType.LongHelp=
"Specifies the recognition context. 
If rigid zones share constant fillets (resp. chamfers) 
with the surface to deform whose radii (resp. lengths) 
belong to the selected context, the rigid zones will be 
automatically sewn to the deformed surface.";

TabContainer.RigidTabPage.LabRigid.Title="Rigid Zone:";
TabContainer.RigidTabPage.LabRigid.LongHelp="Specifies the rigid zones.";
TabContainer.RigidTabPage.FraRigid.LongHelp="Specifies the rigid zones.";

TabContainer.RigidTabPage.FraRigidPropagation.DistancePropagation.Title="Distance propagation";
TabContainer.RigidTabPage.FraRigidPropagation.DistancePropagation.LongHelp="Adds all faces which can be reached by going from face to face without gaps starting from the selected zone.";
TabContainer.RigidTabPage.FraRigidPropagation.AngularPropagation.Title="Angular propagation";
TabContainer.RigidTabPage.FraRigidPropagation.AngularPropagation.LongHelp="Adds all faces which can be reached by going from face to face without sharpnesses starting from the selected zone.";
TabContainer.RigidTabPage.FraRigidPropagation.ComplementaryAngularPropagation.Title="Complementary angular propagation";
TabContainer.RigidTabPage.FraRigidPropagation.ComplementaryAngularPropagation.LongHelp="Adds all faces but those which can be reached by going from face to face without sharpnesses starting from the selected zone; removes the selected zone.";

TabContainer.RigidTabPage.BanMultidomain.Title="Ban multidomain results and throw an update error in case of failure";
TabContainer.RigidTabPage.BanMultidomain.LongHelp="Prevents the feature from having a multidomain result. Requests to throw an update error when rigid zones fail to be sewn to the deformed surface.";

FreezeReference="Reference frozen";
PointCurve="Reference point/curve";
ReferenceTranslation="Reference translation";
ReferenceIsometry="Reference isometry";
ReferenceSimilarity="Reference similarity";
ReferenceLinear="Reference linear transformation";
FreezeReferencesh="Frozen";
PointCurvesh="Point/Curve";
ReferenceTranslationsh="Translation";
ReferenceIsometrysh="Isometry";
ReferenceSimilaritysh="Similarity";
ReferenceLinearsh="Linear";

PointType="Point";
TangentType="Tangent";
CurvatureType="Curvature";
NoneType="None (G0)";
RigidTangencyType="Rigid tangency";
RigidSpaceType="Rigid space";

NoneAutoCouplingPoints = "None";
TangencyAutoCouplingPoints = "Tangency";
VerticesAutoCouplingPoints = "Vertices";

Lowattenuation="Low attenuation";
Mediumattenuation="Medium attenuation";
Strongattenuation="Strong attenuation";
Verystrongattenuation="Very strong attenuation";

IndustryTypePowerTrain="Power Train (0mm - 15mm)";
IndustryTypeBiW="BiW (1mm - 100mm)";
IndustryTypeConsumerGoods="Consumer Goods (0mm - 50mm)";
IndustryTypeShipBuilding="Ship Building (0mm - infinity)";
IndustryTypeHighTech="High Tech (0mm - 1.5mm)";
IndustryTypeBuilding="Building (1mm - 100mm)";
IndustryTypeMachineDesign="Machine Design (see User Assistance)";
