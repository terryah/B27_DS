// =============================================================================
// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2006
// =============================================================================
// CATCldUIRepositCmd : NLS resource file for Reposit Base Command
// =============================================================================
// Implementation Notes:
// =============================================================================
// 27-Oct-2006 YSN: Creation
// =============================================================================

// ----------------
// Base class
// ----------------
Base.Warning.Title = "Information about Alignment" ;
Base.Warning.Text1 ="No Keep option is not allowed and unchecked, because the cloud to align is a feature.";
Base.Warning.Text2 ="No Keep option is not allowed and unchecked, because the set of points of the cloud to align is shared by ";

CATCldUIRepositCmd.initialState.Message ="Select the cloud to align";

// ----------------
// Compass
// ----------------
CATCldUIRepositCmd.CompassState2.Message ="Select the first reference";
CATCldUIRepositCmd.CompassState3a.Message ="Select the next reference (cloud type)";
CATCldUIRepositCmd.CompassState3b.Message ="Select the next reference (face type)";
CATCldUIRepositCmd.CompassState3c.Message ="Select the next reference (point type)";
CATCldUIRepositCmd.CompassState4.Message ="Use the compass to move the cloud";

// ----------------
// Best Fit
// ----------------
CATCldUIRepositCmd.targetSelState.Message ="Select references which can be one or several clouds and surfaces, or a set of points";
CATCldUIRepositCmd.targetSelPoints.Message ="Select points as references";
CATCldUIRepositCmd.targetSelCldSrf.Message ="Select clouds or surfaces as references";

BestFit.Warning.Text1 ="The selected cloud has always been used as reference. It can't be used as cloud to align."; 
BestFit.Warning.Text2 ="The selected cloud has always been used as cloud to align. It can't be used as reference."; 
BestFit.Warning.Text3 ="References must be either a point, or a surface, or a cloud.";
BestFit.Warning.Text4 ="References must be either a set of points, or a set of clouds and/or surfaces. They can't be mixed.";
BestFit.Warning.Text5 ="Best fit alignment fails. First make a rough alignment using Alignment by Compass, then try best fit alignment again.";
BestFit.Warning.Text6 ="You can't change the selection of reference because local selection has been applied on it. You must first un-select the local selection before.";
BestFit.Warning.Text7 ="You can't change the selection of cloud to align because local selection has been applied on it. You must first un-select the local selection.";
BestFit.Stat.AlignNb ="Alignment #";
BestFit.Stat.NbOfIter ="Number of iterations: ";
BestFit.Stat.NbOfPoints ="Number of points used in statistic calculation: ";
BestFit.Stat.MinDev ="Min deviation: ";
BestFit.Stat.AveDev ="Mean deviation: ";
BestFit.Stat.MaxDev ="Max deviation: ";
BestFit.Stat.StdDev ="Standard deviation: ";
BestFit.Stat.UnderTol ="Percentage of points under tolerance: ";

// ---------------------
// By Constraint and RPS
// ---------------------
CATCldUIRepositCmd.CtrNeutralState.Message ="Use Add to add a constraint then Apply to update";
CATCldUIRepositCmd.CtrAddPushedState.Message ="Select a first constraint element in the cloud to align";
CATCldUIRepositCmd.Ctr1SelectedState.Message ="Select a second constraint element in the reference";

Warning.Title = "Align With Constraints" ;
Warning.Text1 = "No solution is found. Check the consistency of input constraints and the priority values.";
Warning.Text3 = "Error during result update.";
Warning.Text4 = "Internal error.";
Warning.Text5 = "This element has been used as constraint element in the cloud to align. It can not be used as constraint element in the reference at the same time.";
Warning.Text6 = "This element has been used as constraint element in the reference. It can not be used as constraint element in the cloud to align at the same time.";
Warning.Text7 = "At least one fixed constraint condition is not satisfied. Check the consistency of input constraints and the priority values, and make sure that the input order (element in cloud to align/element in reference) is not inversed.";
Warning.Text8 = "A constraint with the same constraint element in the cloud to align and the same constraint element in the reference already exists. Select another constraint element in the reference.";
Warning.Text9 = "At least one constraint condition is not satisfied. Cf. the statistic window for details. Check the consistency of input constraints, especially make sure that the input order (element in cloud to align/element in reference) is not inversed. Free some constraints and try the alignment again.";

Warning.Text301 = " Selected reference geometry is invalid. \n Selection is made of an unrecognized geometrical element. \n Fix the geometry or choose another one and try again.";
Warning.Text302 = " Selected reference geometry is invalid. \n Selection is made of multiple geometrical elements. \n Fix the geometry or choose another one and try again.";

Ctr.NumRecal      = "Alignment #";
Ctr.NumCtr        = "Constraint #";
Ctr.Type          = ", type: ";
Ctr.Point         = "point";
Ctr.Line          = "line";		
Ctr.Plane         = "plane";
Ctr.Dist          = "  dist.= "; 
Ctr.Angl          = "  angl.= "; 
Ctr.TolLin        = "Linear tolerance = ";
Ctr.TolAng        = "Angular tolerance = ";
Ctr.NotCalculated = " not calculated";
Ctr.RPSLabel           = "RPS";
Ctr.NotFrozenLabel     = " not frozen";

// ----------------
// Sphere
// ----------------
CATCldUIRepositCmd.SphereTargetSelState.Message = "Select a reference cloud of point";
CATCldUIRepositCmd.SphereRunningPtState.Message = "Pick a point on the cloud to align";
CATCldUIRepositCmd.SphereRunningPtState.Target  = "Pick a point on the reference";
