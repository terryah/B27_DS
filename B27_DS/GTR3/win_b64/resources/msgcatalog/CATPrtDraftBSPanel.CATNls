DraftBSPanelTitle = "Draft Both Sides";


FrmRoot.FrmMain.UseCaseFrame.ModeLabel.Title = "Mode:";
FrmRoot.FrmMain.UseCaseFrame.ModeCombo.LongHelp = "Specifies the modes for draft both sides";
FrmRoot.FrmMain.UseCaseFrame.ModeLabel.LongHelp = "Sets the use case";
FrmRoot.FrmMain.UseCaseFrame.MethodLabel.Title = "Computation method:";
FrmRoot.FrmMain.UseCaseFrame.MethodCombo.LongHelp = 
"Specifies the methods for draft both sides, 
independent if the two sides are considered independently 
(draft parts unadjusted in most cases), driving/driven if a
 side is driving the draft angle of the other one, or fitted 
 if the draft angles of the two sides are optimized.";
FrmRoot.FrmMain.UseCaseFrame.MethodLabel.LongHelp = "Sets the computation method";

FrmRoot.FrmMain.TabContainer.MainTabPage.Title = "Main";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.MainBasicAngleLabel.Title = "Angle:";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.MainPartingLabel.Title = "Parting Element:";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.MainPartingSel.LongHelp = "Specifies if the draft will have a parting element or not";

FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.MainPullDirLabel.Title = "Pulling Direction:";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.MainPullDirSel.LongHelp = "Specifies the directon in which the mold will be removed. It may be indicated by the direction of a line or an edge, or by the direction normal to a plane or a face";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.MainByRefCheck.Title = "Controlled by reference";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.MainByRefCheck.LongHelp = "Any modification to the reference used to define the pulling direction will affect the draft";

FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.Neutrals1Label.Title = "Neutral Element(s) 1st Side:";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.Neutrals1SelectorList.LongHelp = "Specifies the face(s) or plane defining the neutral curve, on which the drafted face(s) will lie at 1st side";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.NeutralProp1Check.Title = "Propagation by tangency";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.NeutralProp1Check.LongHelp = "Enables tangency proragation for the neutral selected face(s)";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.1stSideAngleLabel.Title = "1st Side Angle:";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.1stSideCheck.Title = "1st Side Driving";

FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.Neutrals2Label.Title = "Neutral Element(s) 2nd Side:";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.Neutrals2SelectorList.LongHelp = "Specifies the face(s) or plane defining the neutral curve, on which the drafted face(s) will lie at 2nd side";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.NeutralProp2Check.Title = "Propagation by tangency";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.NeutralProp2Check.LongHelp = "Enables tangency proragation for the neutral selected face(s)";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.2ndSideAngleLabel.Title = "2nd Side Angle :";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.2ndSideCheck.Title = "2nd Side Driving";

FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.FacesLabel.Title = "Faces to Draft:";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.FacesSelectorList.LongHelp = "Specifies the face(s) to be drafted. The face(s) propagated in tangency are taken in account";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.ByNeutralCheck.Title = "Selection of Faces to Draft by neutral faces";
FrmRoot.FrmMain.TabContainer.MainTabPage.MainTabFrame.ByNeutralCheck.LongHelp = "If acticvated, it selects the faces to be drafted by neutral faces";

FrmRoot.FrmMain.TabContainer.MoreTabPage.Title = "More";
FrmRoot.FrmMain.TabContainer.MoreTabPage.LimitingLabel.Title = "Limiting Element(s):";
FrmRoot.FrmMain.TabContainer.MoreTabPage.LimitingSelectorList.LongHelp = "Specifies the elements to limit the faces to draft";
FrmRoot.FrmMain.TabContainer.MoreTabPage.AdjustmentLabel.Title = "Parting Line Adjustment:";
FrmRoot.FrmMain.TabContainer.MoreTabPage.PartingAdjustmentLabel.Title = "Parting Element Adjustment:";
FrmRoot.FrmMain.TabContainer.MoreTabPage.PartingAdjustmentLabel.LongHelp = "Specifies the tolerance for Parting Element Adjustment:";

FrmRoot.FrmMore.BSMore.Title = "Both Sides More Frame";
FrmRoot.FrmMore.BSMore.Help = "Helping Picture";
FrmRoot.FrmMore.BSMore.ShortHelp = "Helping Picture";
FrmRoot.FrmMore.BSMore.LongHelp = "Helping picture showing the input parameters ans the effect of the computation methods";
FrmRoot.FrmMore.BSMore.Helper.Title = "Helper";
FrmRoot.FrmMore.BSMore.Helper.Help = "Helper"; 
FrmRoot.FrmMore.BSMore.Helper.ShortHelp = "Helper"; 
FrmRoot.FrmMore.BSMore.Helper.LongHelp = "Helper"; 
//FrmRoot.

DraftReflectReflectMode = "Reflect / Reflect";
DraftNeutralNeutralMode = "Neutral / Neutral";

DraftFittedMethod =  "Fitted";
DraftDrivingDrivenMethod = "Driving / Driven";
DraftIndependentMethod = "Independent";

DraftBSPanelPullingDirectionText = "Pulling Direction";

//===================================================================
// Warning Messages
//===================================================================
NlsTitleExtractWarning = "Extract Geometry";
NlsTextExtractWarning = "Deactivate Draft and Extract Geometry ?";

//===================================================================
// 3D Indicators Texts
//===================================================================
NlsTextPartingElemIndicator = "Parting Element";

//===================================================================
// Helper Texts
//===================================================================
NlsTextPartingElemId = "Parting Element";
NlsTextPullingDirId = "Pulling Direction";
NlsTextAngleId = "Draft Angle";
NlsTextNeutral1Id = "Neutral 1st side";
NlsTextNeutral2Id = "Neutral 2nd side";
NlsTextAngle1Id = "Draft Angle 1st side";
NlsTextAngle2Id = "Draft Angle 2nd side";
NlsTextLimitingId = "Limiting Elements";
NlsTextPartingLineAdjId = "Parting Line Adjustment";
NlsTextPartingElement1AdjId = "Parting";
NlsTextPartingElement2AdjId = "Element";
NlsTextPartingElement3AdjId = "Adjustment";

//===================================================================
// Element list titles
//===================================================================
LimitingElements="Limiting elements";
NeutralElements="Neutral elements";
DraftFaces="Draft faces";

