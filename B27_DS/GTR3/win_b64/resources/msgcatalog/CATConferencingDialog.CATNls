Title="Conferencing";
_topFrm._hostLab.Title="Host  ";
_topFrm._hostEdt.LongHelp="Identifies the conference host.";
_topFrm._confStatusLab.Title="  Status  ";
_topFrm._confStatusEdt.LongHelp="Displays the visual conference status";

// Conference tab page controls
_tabCont._confTabp.Title="Setup";
_tabCont._confTabp.LongHelp="Calls the conference participants and identifies them.";
_tabCont._confTabp._userInfoBtn.Title="Who I am";
_tabCont._confTabp._userInfoBtn.LongHelp="Displays and edits personal information.";
_tabCont._confTabp._selfEdt.LongHelp="Displays user name and machine name and/or address.";
_tabCont._confTabp._confMembersLab.Title="Other participants";
_tabCont._confTabp._confMembersList.LongHelp=
"Identifies the other conference participants.
Double-click name in list to display information about a participant.";
_tabCont._confTabp._callCmb.LongHelp=
"Displays a list of the most recent called machines.
Select or type in a machine name or address.";
_tabCont._confTabp._callBtn.Title="Call";
_tabCont._confTabp._callBtn.LongHelp="Calls the selected machine.";

// Control tab page controls
_tabCont._controlTabp.Title="Visual control";
_tabCont._controlTabp.LongHelp="Lets you control the visual conference.";
_tabCont._controlTabp._driverLab.Title="Speaker";
_tabCont._controlTabp._driverEdt.LongHelp="Identifies the speaker.";
_tabCont._controlTabp._drivingFrm.Title="Visual conference mode";
_tabCont._controlTabp._drivingFrm._directedDriverBtn.Title="Directed";
_tabCont._controlTabp._drivingFrm._directedDriverBtn.LongHelp="The host member decides who takes the floor";
_tabCont._controlTabp._drivingFrm._freeDriverBtn.Title="Free";
_tabCont._controlTabp._drivingFrm._freeDriverBtn.LongHelp="Any member can take the floor";
_tabCont._controlTabp._behaviorFrm.Title="Participant mode";
_tabCont._controlTabp._behaviorFrm._listenBtn.Title="Record off";
_tabCont._controlTabp._behaviorFrm._listenBtn.LongHelp="The session is replaying the conference events as soons as received.";
_tabCont._controlTabp._behaviorFrm._recordBtn.Title="Record on";
_tabCont._controlTabp._behaviorFrm._recordBtn.LongHelp="The session is recording the conference events in order to replay them later.";
_tabCont._controlTabp._behaviorFrm._updateBtn.Title="Update";
_tabCont._controlTabp._behaviorFrm._updateBtn.LongHelp="Lets you replay the recorded events.";
_tabCont._controlTabp._behaviorFrm._activityFrm._idleBtn.Title="Listen";
_tabCont._controlTabp._behaviorFrm._activityFrm._idleBtn.LongHelp="The participant cannot interact with his/her session.";
_tabCont._controlTabp._behaviorFrm._activityFrm._workBtn.Title="Work";
_tabCont._controlTabp._behaviorFrm._activityFrm._workBtn.LongHelp="The participant can interact with his/her session.";
_tabCont._controlTabp._behaviorFrm._activityFrm._speakBtn.Title="Speak";
_tabCont._controlTabp._behaviorFrm._activityFrm._speakBtn.LongHelp="The session is sending events to the other conference sessions.";
_tabCont._controlTabp._behaviorFrm._activityFrm._controlRequestedLab.Title="requested";
_tabCont._controlTabp._driverSelectionFrm.Title="Speaking requests";
_tabCont._controlTabp._driverSelectionFrm._controlReqMembersLab.Title="Control requesters  ";
_tabCont._controlTabp._driverSelectionFrm._controlReqMembersList.LongHelp="Identifies the participants requesting the control.";
_tabCont._controlTabp._driverSelectionFrm._controlOthMembersLab.Title="Other participants  ";
_tabCont._controlTabp._driverSelectionFrm._controlOthMembersList.LongHelp="Identifies the participants not requesting the control.";

// Options tab page controls
_tabCont._optionsTabp.Title="Options";
_tabCont._optionsTabp.LongHelp="Displays and edits conferencing settings.";
_tabCont._optionsTabp._tlpFrm.Title="Mouse pointer";
_tabCont._optionsTabp._tlpFrm._tlpExportBtn.Title="Enable other participants to see your mouse pointer";
_tabCont._optionsTabp._tlpFrm._tlpExportBtn.LongHelp="Lets you enable other participants to see your mouse pointer when you drive.";
_tabCont._optionsTabp._tlpFrm._tlpImportBtn.Title="Display the speaker's mouse pointer";
_tabCont._optionsTabp._tlpFrm._tlpImportBtn.LongHelp="Lets you display the speaker's mouse pointer.";
_tabCont._optionsTabp._tlpFrm._tlpStepLabel.Title="Motion step of the mouse pointer";
_tabCont._optionsTabp._tlpFrm._tlpStepEditor.LongHelp="Lets you change the speed of the speaker's mouse pointer.";
_tabCont._optionsTabp._tlpFrm._tlpStepButton.Title="OK";
_tabCont._optionsTabp._tlpFrm._tlpDriverNameBtn.Title="Display the speaker's name with the mouse pointer";
_tabCont._optionsTabp._tlpFrm._tlpDriverNameBtn.LongHelp="Lets you display the speaker's name with his mouse pointer.";

// Chat tab page controls
_tabCont._chatTabp.Title="Chat";
_tabCont._chatTabp.LongHelp="Lets you exchange messages with other participants.";
_tabCont._chatTabp._chatLogLab.Title="Log  ";
_tabCont._chatTabp._chatLog.LongHelp="Displays incoming and outcoming messages.";
_tabCont._chatTabp._chatWhoLab.Title="To  ";
_tabCont._chatTabp._chatWhoCmb.LongHelp="Lets you select the message addressee.";
_tabCont._chatTabp._chatMsgLab.Title="Message  ";
_tabCont._chatTabp._chatMsgEdt.LongHelp="Lets you type in the text to send.";
_tabCont._chatTabp._sendBtn.Title="Send";
_tabCont._chatTabp._sendBtn.LongHelp="Lets you send the message.";

// History tab page controls
_tabCont._historyTabp.Title="History";
_tabCont._historyTabp.LongHelp="Displays events occurred and actions performed during the conference.";

// Documents tab page controls (GRM)
_tabCont._docTabp.Title="Document";
_tabCont._docTabp.LongHelp="Lets you manage the components of the current document";
_tabCont._docTabp._documentLabel.Title="Current document";
_tabCont._docTabp._visibleLabel.Title="Set selected components ";
_tabCont._docTabp._visibleForLabel.Title=" for ";
_tabCont._docTabp._shareButton.Title="Share";
_tabCont._docTabp._insertLabel.Title="Set selected components ";
_tabCont._docTabp._insertButton.Title="Load";

//File transfert (GRM)
_tabCont._fileXTabp.Title="File transfer";
_tabCont._fileXTabp._fileXSendFrm.Title="Send";
_tabCont._fileXTabp._fileXSendFrm._fileXToLbl.Title="To ";
_tabCont._fileXTabp._fileXSendFrm._fileXSendPathLbl.Title="File ";
_tabCont._fileXTabp._fileXSendFrm._fileXSendPathBtn.Title="Browse";
_tabCont._fileXTabp._fileXSendFrm._fileXSendBtn.Title="Send";
_tabCont._fileXTabp._fileXRecvFrm.Title="Receive";
_tabCont._fileXTabp._fileXRecvFrm._fileXDLDirLbl.Title="Directory ";
_tabCont._fileXTabp._fileXRecvFrm._fileXDLDirBtn.Title="Browse";
_tabCont._fileXTabp._fileXRecvFrm._fileXFromLbl.Title="From ";
_tabCont._fileXTabp._fileXRecvFrm._fileXRecvPathLbl.Title="File ";
_tabCont._fileXTabp._fileXRecvFrm._fileXAcceptBtn.Title="Accept";

// Miscellaneous text
waitingText="Waiting for a call...";
myselfText="Myself";
startText="Start the visual conference";
suspendText="Suspend the visual conference";
stateStartedText="In progress";
stateSuspendedText="Suspended";
everybodyText="Everybody";

componentTitle="Component";
ownerTitle="Owner";
everyText="Everybody";
saveBeforeShareTitle="Save Document";
saveBeforeShareText = "Your current Document has been modified. Do you want to save it before sharing it ?";
alreadyOpenDocTitle = "Already open Document";
alreadyOpenDocText = "The Host wants to conference with a Document which is already open. Do you want to keep the local Document (Cancel) or to open a temporary Document downloaded from the Host (OK).";
activeWndTitle = "Warning";
activeWndText = "The active window is changing but only the components of the document '/P' will continue to be managed.";
ownText="Own";
invisibleText="Invisible";
visibleText="Visible";
noLoadText="Not to load";
loadText="To load";
noLoadedText="No loaded";
loadedText="Loaded";

cantopenfileTitle="Failure";
cantopenfileText="Can't open file (no access)";
cantreadfileText="Can't read file (no access)";
cantwritefileText="Can't write file (no access)";
