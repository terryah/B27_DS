//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2006
//=============================================================================
//
// CATStConnectCheckerAnalysisDialog.CATNls : Resource file for NLS purposes: 
// R18 Connect Checker Dialog Box
//
//=============================================================================
// Dec. 2006                                          Shilpa HAWALDAR - 3DPLM
//=============================================================================

DialogBoxTitle="Controllo connessione";

ResultOverlapping.Title="Sovrapposizione";
ResultNoOverlapping.Title="Nessuna sovrapposizione";
SurfaceSelected.Message=" superficie(i)";
CurveSelected.Message=" Curva(e)";
ConnectionsDetected.Message=" Connessione(i)";
SingularCurve.Title="CurvaSingolare";

MainFrame.InputElementsFrame.Title="Elementi";

MainFrame.InputElementsFrame.InputElementsLable.Title=" Origine: ";
MainFrame.InputElementsFrame.InputElementsSelector.ShortHelp="Mostra il numero di elementi selezionati";
MainFrame.InputElementsFrame.InputElementsSelector.LongHelp="Mostra il numero totale di elementi selezionati per l'analisi di connessione";
InputElementsListTitle.Message="Elenco elementi di origine";

MainFrame.InputElementsFrame.TargetElementsLable.Title=" Destinazione: ";
MainFrame.InputElementsFrame.TargetElementsSelector.ShortHelp="Mostra il numero di elementi di destinazione selezionati";
MainFrame.InputElementsFrame.TargetElementsSelector.LongHelp="Mostra il numero totale di elementi di destinazione selezionati per l'analisi di connessione";
TargetElementsListTitle.Message="Elenco elementi di destinazione";

MainFrame.SecondRowDummyFrame.CCKTypeFrame.Title="Tipo";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.LongHelp="Diversi tipi di connessione da rilevare";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurSurCCKChkButton.ShortHelp="Connessione superficie-superficie";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurSurCCKChkButton.LongHelp="Verifica connessioni tra due o pi� superfici";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.CurCurCCKChkButton.ShortHelp="Connessione curva-curva";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.CurCurCCKChkButton.LongHelp="Verifica connessioni tra due o pi� curve";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurCurCCKChkButton.ShortHelp="Connessione superficie-curva";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.DummyCCKTypeFrame.SurCurCCKChkButton.LongHelp="Verifica connessione tra superfici e curve";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CrvExtremProjectionCCKChkButton.ShortHelp="Connessione tra l'estremit� della curva e la proiezione sulla destinazione";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CrvExtremProjectionCCKChkButton.LongHelp="Verifica la connessione tra l'estremit� della curva della curva di origine e la proiezione sulla curva di destinazione";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurBorderProjectionCCKChkButton.ShortHelp="Connessione tra il bordo di una superficie e la proiezione sulla destinazione";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurBorderProjectionCCKChkButton.LongHelp="Verifica la connessione tra il bordo di una superficie di origine e la proiezione sulla superficie di destinazione";

MainFrame.SecondRowDummyFrame.CCKTypeFrame.ConnectionWayFrame.BoundaryRadButton.Title="Bordo";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.ConnectionWayFrame.BoundaryRadButton.LongHelp="Verifica la connessione bordi ed elementi di immissione";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.ConnectionWayFrame.ProjectionRadButton.Title="Proiezione";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurBorderProjectionCCKChkButton.LongHelp="Verifica la connessione tra il bordo di un elemento di origine e la proiezione sull'elemento di destinazione";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.Title="Veloce";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.ShortHelp="Difetto di sovrapposizione";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.LongHelp="Verifica la continuit� di tipo sovrapposizione";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.ShortHelp="Continuit� G0";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.LongHelp="Verifica la continuit� di tipo G0";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.ShortHelp="Continuit� G1";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.LongHelp="Verifica la continuit� di tipo G1";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.ShortHelp="Continuit� G2";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.LongHelp="Verifica la continuit� di tipo G2";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.ShortHelp="Continuit� G3";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.LongHelp="Verifica la continuit� di tipo G3";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Text.Title=">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Text.Title=">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Text.Title=">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Text.Title=">";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.Title="Completa";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.ShortHelp="Difetto di sovrapposizione";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.LongHelp="Verifica la continuit� del tipo sovrapposizione";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.ShortHelp="Continuit� G0";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.LongHelp="Verifica la continuit� di tipo G0";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.ShortHelp="Continuit� G1";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.LongHelp="Verifica la continuit� di tipo G1";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.ShortHelp="Continuit� G2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.LongHelp="Verifica la continuit� di tipo G2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.ShortHelp="Continuit� G3";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.LongHelp="Verifica la continuit� di tipo G3";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Title="Visualizza";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.LongHelp="Varie opzioni di visualizzazione per le connessioni rilevate";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.LimitedColorScale.ShortHelp="Scala di colori limitata";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.LimitedColorScale.LongHelp="Visualizza l'analisi con gamma di colori limitata";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.FullColorScale.ShortHelp="Scala di colori completa";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.FullColorScale.LongHelp="Visualizza l'analisi con gamma di colori completa";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Comb.ShortHelp="Pettine";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Comb.LongHelp="Visualizza pettine";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Envelop.ShortHelp="Inviluppo";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDisplayOptionsFrame.Envelop.LongHelp="Visualizza Inviluppo";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.Title="Ampiezza";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.LongHelp="Definisce l'ampiezza del pettine";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.AutoScaling.ShortHelp="Scala automatica";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.AutoScaling.LongHelp="Consente la scala automatica del pettine";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.MultiplyTwo.ShortHelp="Moltiplica per due";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.MultiplyTwo.LongHelp="Moltiplica fattore di scala per 2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.DivideTwo.ShortHelp="Dividi per due";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.AmplitudeFrame.DivideTwo.LongHelp="Divide fattore di scala per 2";

MainFrame.ConnectionFrame.Title="Connessione";
MainFrame.ConnectionFrame.LongHelp="Definisce i parametri per ricercare le connessioni tra gli elementi di immissione";
MainFrame.ConnectionFrame.MinGapLabel.Title="Distanza minima";
MainFrame.ConnectionFrame.MaxGapLabel.Title="Distanza massima";
MainFrame.ConnectionFrame.InternalEdgeChkButton.ShortHelp="Spigolo interno";
MainFrame.ConnectionFrame.InternalEdgeChkButton.LongHelp="Gli spigoli interni degli elementi di giunto sono presi in considerazione per eseguire l'analisi";
//Start AV7 11:05:03
MainFrame.ConnectionFrame.IgnoreFreeEdgesChkButton.ShortHelp="Ignora spigoli liberi piccoli";
MainFrame.ConnectionFrame.IgnoreFreeEdgesChkButton.LongHelp="Ignora gli spigoli liberi la cui dimensione � inferiore alla distanza massima specificata";
//End AV7 11:05:03

MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.Title="Informazioni";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.LongHelp="Mostra tutti i valori minimi e massimi per ogni connessione rilevata";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MinInfo.ShortHelp="Minimi";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MinInfo.LongHelp="Visualizza valori minimi";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MaxInfo.ShortHelp="Massimi";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.MaxInfo.LongHelp="Visualizza valori massimi";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.G1ReturnedValue.ShortHelp="Valori G1 nell'intervallo compreso tra 0 e 90 gradi";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.G1ReturnedValue.LongHelp="Visualizza solo i valori G1 compresi nell'intervallo tra 0 e 90 gradi";
MainFrame.FifthRowDummyFrame.MinMaxInfoDisplayFrame.FullG2Concavity.LongHelp="Verifica il difetto di Concavit� per Superficie-Superficie e Superficie-Curva o Angolo tra piani osculatori per Curva-Curva in modalit� G2";

MainFrame.FifthRowDummyFrame.DiscretizationFrame.Title="Discretizzazione";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.LongHelp="Consente all'utente di passare tra le quattro fasi di discretizzazione predefinite";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.ShortHelp="Discretizzazione leggera";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.LongHelp="Imposta il tipo di discretizzazione leggera";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.ShortHelp="Discretizzazione grossolana";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.LongHelp="Imposta il tipo di discretizzazione grossolana";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.ShortHelp="Discretizzazione media";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.LongHelp="Imposta il tipo di discretizzazione media";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.ShortHelp="Discretizzazione fine";
MainFrame.FifthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.LongHelp="Imposta il tipo di discretizzazione fine";

MainFrame.MaxValuesDisplayFrame.Title="Deviazione massima";
MainFrame.MaxValuesDisplayFrame.LongHelp="Mostra il valore massimo per ogni continuit� dalle connessioni correnti";
MainFrame.MaxValuesDisplayFrame.G0TitleLabel.Title="G0:";
MainFrame.MaxValuesDisplayFrame.G0Label="----  ";
MainFrame.MaxValuesDisplayFrame.G1TitleLabel.Title="G1:";
MainFrame.MaxValuesDisplayFrame.G2TitleLabel.Title="G2:";
MainFrame.MaxValuesDisplayFrame.G3TitleLabel.Title="G3:";

MinGapCkeFrame.StatusBarHelp="Modificare il valore del parametro: � richiesta la distanza minima.";
MinGapCkeFrame.LongHelp="Definisce il valore di minima distanza per tutte le deviazioni da visualizzare.";
MaxGapCkeFrame.StatusBarHelp="Modificare il valore del parametro: � richiesta la distanza massima.";
MaxGapCkeFrame.LongHelp="Definisce il valore di distanza massima fino al quale l'analisi di connessione verr� eseguita  tra gli elementi di immissione.";
G0Cke.StatusBarHelp="Modificare il valore del parametro: � richiesto il valore di soglia G0.";
G0Cke.LongHelp="Definisce il valore di soglia G0 per l'analisi rapida.";
G1Cke.StatusBarHelp="Modificare il valore del parametro: � richiesto il valore di soglia G1.";
G1Cke.LongHelp="Definisce il valore di soglia G1 per l'analisi rapida.";
G2Cke.StatusBarHelp="Modificare il valore del parametro: � richiesto il valore di soglia G2.";
G2Cke.LongHelp="Definisce il valore di soglia G2 per l'analisi rapida.";
G3Cke.StatusBarHelp="Modificare il valore del parametro: � richiesto il valore di soglia G3.";
G3Cke.LongHelp="Definisce il valore di soglia G3 per l'analisi rapida.";

SurfaceSurfaceCCKConcavity.ShortHelp="Difetto di concavit�";
CurveCurveCCKConcavity.ShortHelp="Angolo tra i piani osculatori";
CCKConcavity.ShortHelp="Difetto di Concavit� o Angolo tra i piani osculatori";


