// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRmaPlasticDialog (English)
//
//===========================================================================

NoneShader   = "Nessuna";
ImagesShader = "Immagine";
MarbleShader = "Marmo";
VeinShader   = "Venatura";
AlternateVeinShader = "Venatura alternativa";
RockShader   = "Roccia";
ChessboardShader = "Scacchiera";
ClearCoatShader = "ClearCoat 360"; 
CarPaintShader = "Vernice per auto";
ISLShader = "Shader esterno";

tabContainer.basicPage.Title = "Illuminazione";
tabContainer.basicPage.Help = "Parametri di illuminazione del materiale";
tabContainer.basicPage.LongHelp =
"Parametri utilizzati per definire l'illuminazione del materiale.";


tabContainer.texturePage.Title = "Tessitura";
tabContainer.texturePage.Help = "Parametri di tessitura del materiale";
tabContainer.texturePage.LongHelp =
"Parametri utilizzati per definire la tessitura del materiale (facoltativo).";


tabContainer.basicPage.colorLabel.Title = "Colore";
tabContainer.basicPage.colorLabel.Help = "Colore del materiale";
tabContainer.basicPage.colorLabel.ShortHelp = "Modifica il colore";
tabContainer.basicPage.colorLabel.LongHelp =
"Definisce il colore del materiale.";


tabContainer.basicPage.colorValue.Title = "Colore";
tabContainer.basicPage.colorValue.Help = "Colore del materiale";
tabContainer.basicPage.colorValue.ShortHelp = "Modifica il colore";
tabContainer.basicPage.colorValue.LongHelp =
"Definisce il colore del materiale.";

tabContainer.basicPage.filterLabel.Title = "(*)";

tabContainer.basicPage.luminosityLabel.Title = "Ambiente";
tabContainer.basicPage.luminosityLabel.Help = "Luminosit� del materiale";
tabContainer.basicPage.luminosityLabel.ShortHelp = "Modifica il coefficiente di ambiente";
tabContainer.basicPage.luminosityLabel.LongHelp =
"Indica se il materiale � globalmente luminoso o scuro.";


tabContainer.basicPage.luminositySlider.Title = "Ambiente";
tabContainer.basicPage.luminositySlider.Help = "Luminosit� del materiale";
tabContainer.basicPage.luminositySlider.ShortHelp = "Modifica il coefficiente di ambiente";
tabContainer.basicPage.luminositySlider.LongHelp =
"Indica se il materiale � globalmente luminoso o scuro.";


tabContainer.basicPage.ambientLinkButton.Help = "Collega i colori ambiente e diffusi";
tabContainer.basicPage.ambientLinkButton.ShortHelp = "Collega i colori ambiente e diffusi";
tabContainer.basicPage.ambientLinkButton.LongHelp =
"Collega la modifica dei colori ambiente e diffusi.";


tabContainer.basicPage.contrastLabel.Title = "Diffuso";
tabContainer.basicPage.contrastLabel.Help = "Contrasto del materiale";
tabContainer.basicPage.contrastLabel.ShortHelp = "Modifica il coefficiente di diffusione";
tabContainer.basicPage.contrastLabel.LongHelp =
"Accentua la differenza tra aree chiare e scure.";


tabContainer.basicPage.contrastSlider.Title = "Diffuso";
tabContainer.basicPage.contrastSlider.Help = "Contrasto del materiale";
tabContainer.basicPage.contrastSlider.ShortHelp = "Modifica il coefficiente di diffusione";
tabContainer.basicPage.contrastSlider.LongHelp =
"Accentua la differenza tra aree chiare e scure.";


tabContainer.basicPage.diffuseLinkButton.Help = "Collega i colori diffuso e speculare";
tabContainer.basicPage.diffuseLinkButton.ShortHelp = "Collega i colori diffuso e speculare";
tabContainer.basicPage.diffuseLinkButton.LongHelp =
"Collega la modifica dei colori diffusi e speculari.";


tabContainer.basicPage.shininessLabel.Title = "Speculare";
tabContainer.basicPage.shininessLabel.Help = "Luminosit� del materiale";
tabContainer.basicPage.shininessLabel.ShortHelp = "Modifica il coefficiente di speculare";
tabContainer.basicPage.shininessLabel.LongHelp =
"Modula la luminosit� del materiale.";


tabContainer.basicPage.shininessSlider.Title = "Speculare";
tabContainer.basicPage.shininessSlider.Help = "Luminosit� del materiale";
tabContainer.basicPage.shininessSlider.ShortHelp = "Modifica il coefficiente di speculare";
tabContainer.basicPage.shininessSlider.LongHelp =
"Modula la luminosit� del materiale.";


tabContainer.basicPage.shininessExpLabel.Title = "Rugosit�";
tabContainer.basicPage.shininessExpLabel.Help = "Opacit� del materiale";
tabContainer.basicPage.shininessExpLabel.ShortHelp = "Modifica l'esponente speculare";
tabContainer.basicPage.shininessExpLabel.LongHelp =
"Modula la dimensione dell'area di luminosit� del materiale.";


tabContainer.basicPage.shininessExpSlider.Title = "Rugosit�";
tabContainer.basicPage.shininessExpSlider.Help = "Opacit� del materiale";
tabContainer.basicPage.shininessExpSlider.ShortHelp = "Modifica l'esponente speculare";
tabContainer.basicPage.shininessExpSlider.LongHelp =
"Modula la dimensione dell'area di luminosit� del materiale.";


tabContainer.basicPage.transparencyLabel.Title = "Trasparenza";
tabContainer.basicPage.transparencyLabel.Help = "Trasparenza del materiale";
tabContainer.basicPage.transparencyLabel.ShortHelp = "Modifica il coefficiente di trasparenza";
tabContainer.basicPage.transparencyLabel.LongHelp =
"Modula la trasparenza del materiale.";


tabContainer.basicPage.transparencySlider.Title = "Trasparenza";
tabContainer.basicPage.transparencySlider.Help = "Trasparenza del materiale";
tabContainer.basicPage.transparencySlider.ShortHelp = "Modifica il coefficiente di trasparenza";
tabContainer.basicPage.transparencySlider.LongHelp =
"Modula la trasparenza del materiale.";


tabContainer.basicPage.reflectivityLabel.Title = "Fattore di riflessione";
tabContainer.basicPage.reflectivityLabel.Help = "Fattore di riflessione del materiale";
tabContainer.basicPage.reflectivityLabel.ShortHelp = "Modifica il coefficiente di riflessione";
tabContainer.basicPage.reflectivityLabel.LongHelp =
"Modula il fattore di riflessione del materiale.
Nota: Se il materiale ha una tessitura, sar� visibile
solo se il materiale non � riflettente.";


tabContainer.basicPage.reflectivitySlider.Title = "Fattore di riflessione";
tabContainer.basicPage.reflectivitySlider.Help = "Fattore di riflessione del materiale";
tabContainer.basicPage.reflectivitySlider.ShortHelp = "Modifica il coefficiente di riflessione";
tabContainer.basicPage.reflectivitySlider.LongHelp =
"Modula il fattore di riflessione del materiale.
Nota: Se il materiale ha una tessitura, sar� visibile
solo se il materiale non � riflettente.";

tabContainer.basicPage.reflectionCustom.Help = "Impostazioni avanzate di riflessione";
tabContainer.basicPage.reflectionCustom.ShortHelp = "Impostazioni avanzate di riflessione";
tabContainer.basicPage.reflectionCustom.LongHelp =
"Impostazioni avanzate di riflessione.";

tabContainer.basicPage.refractionLabel.Title = "Fattore di rifrazione(*)";
tabContainer.basicPage.refractionLabel.Help = "Fattore di rifrazione del materiale";
tabContainer.basicPage.refractionLabel.ShortHelp = "Modifica il coefficiente del fattore di rifrazione
(*)Viene utilizzato solo per la resa del software.";
tabContainer.basicPage.refractionLabel.LongHelp =
"Modula il fattore di rifrazione del materiale.
(*)Viene utilizzato solo per la resa del software.";


tabContainer.basicPage.refractionSlider.Title = "Fattore di rifrazione";
tabContainer.basicPage.refractionSlider.Help = "Fattore di rifrazione del materiale";
tabContainer.basicPage.refractionSlider.ShortHelp = "Modifica il coefficiente di rifrazione";
tabContainer.basicPage.refractionSlider.LongHelp =
"Modula il fattore di rifrazione del materiale.";

tabContainer.basicPage.warningLabel.Title = "(*) Parametri solo per la resa tramite software";

tabContainer.texturePage.textTypeFrm.textTypeLabel.Title = "Tipo ";
tabContainer.texturePage.textTypeFrm.textTypeCmbLabel.Help = "Tipo di tessitura";
tabContainer.texturePage.textTypeFrm.textTypeCmbLabel.ShortHelp = "Modifica il tipo di tessitura";
tabContainer.texturePage.textTypeFrm.textTypeCmbLabel.LongHelp =
"Definisce il tipo di tessitura applicato al materiale.";

tabContainer.texturePage.textTypeFrm.mappingFrame.mappingTypeLabel.Title = "Mappatura ";
tabContainer.texturePage.textTypeFrm.mappingFrame.mappingTypeLabel.Help = "Tipi di mappatura della tessitura";
tabContainer.texturePage.textTypeFrm.mappingFrame.mappingTypeLabel.ShortHelp = "Modifica il tipo di mappatura";
tabContainer.texturePage.textTypeFrm.mappingFrame.mappingTypeLabel.LongHelp =
"Definisce il tipo di mappatura utilizzato per il materiale con tessitura.";

tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.Title = "Cambia il tipo di mappatura";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.Help = "Cambia il tipo di mappatura";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.ShortHelp = "Cambia il tipo di mappatura";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.LongHelp =
"Cambia il tipo di mappatura utilizzato per la tessitura.";

tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.planRadioButton.Title = "Corrispondenza planare";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.planRadioButton.Help = "Il tipo di corrispondenza � planare";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.planRadioButton.ShortHelp = "Corrispondenza planare";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.planRadioButton.LongHelp =
"Seleziona il tipo di corrispondenza planare da applicare ad un materiale su un riferimento.";

tabContainer.texturePage.textTypeFrm.mappingFrame.buttonsFrame.iconBox2.sphereRadioButton.Title = "Corrispondenza sferica";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.sphereRadioButton.Help = "Il tipo di corrispondenza � sferico";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.sphereRadioButton.ShortHelp = "Corrispondenza sferica";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.sphereRadioButton.LongHelp =
"Seleziona il tipo di corrispondenza sferico da applicare ad un materiale su un riferimento.";

tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cylinderRadioButton.Title = "Corrispondenza cilindrica";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cylinderRadioButton.Help = "Il tipo di corrispondenza � cilindrico";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cylinderRadioButton.ShortHelp = "Corrispondenza cilindrica";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cylinderRadioButton.LongHelp =
"Seleziona il tipo di corrispondenza cilindrico da applicare ad un materiale su un riferimento.";

tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cubeRadioButton.Title = "Anteprima del riquadro";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cubeRadioButton.Help = "Il tipo di corrispondenza � cubico";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cubeRadioButton.ShortHelp = "Corrispondenza cubica";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.cubeRadioButton.LongHelp =
"Seleziona il tipo di corrispondenza cubico da applicare ad un materiale su un riferimento.";

tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.automaticRadioButton.Title = "Corrispondenza auto-adattativa";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.automaticRadioButton.Help = "Il tipo di corrispondenza � auto-adattativo";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.automaticRadioButton.ShortHelp = "Corrispondenza auto-adattativa";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.automaticRadioButton.LongHelp =
"Seleziona il tipo di corrispondenza auto-adattativa per applicare un materiale su un riferimento.";

tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.manualRadioButton.Title = "Corrispondenza adattativa manuale";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.manualRadioButton.Help = "Il tipo di corrispondenza � adattativo manuale";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.manualRadioButton.ShortHelp = "Corrispondenza adattativa manuale";
tabContainer.texturePage.textTypeFrm.mappingFrame.iconBox2.manualRadioButton.LongHelp =
"Seleziona il tipo di corrispondenza adattativo manuale per applicare un materiale su un riferimento.";
