//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameDimensionLineFormat
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//------------------------------------------------------------------------------

ComboRepresentation0="Regolare" ;
ComboRepresentation1="Due parti";
ComboRepresentation2="Con guida";
ComboRepresentation3="Guida una parte";
ComboRepresentation4="Guida due parti";

ComboReferenceLine0="Schermo";
ComboReferenceLine1="Vista";
ComboReferenceLine2="Linea di quota";

ComboOrientationLine0="Parallelo";
ComboOrientationLine1="Perpendicolare";
ComboOrientationLine2="Angolo fisso";

ComboOrientationLine00="Orizzontale";
ComboOrientationLine01="Verticale";
ComboOrientationLine02="Angolo fisso";

NO_SYMBOL="Nessun simbolo";
ARROW="Freccia aperta";
CLOSED_ARROW="Freccia con contorno";
FILLED_ARROW="Freccia piena";
SYMETRIC_ARROW="Freccia trasparente";
SLASH="Barra";
CIRCLE="Cerchio con contorno";
FILLED_CIRCLE="Cerchio pieno";
SCORED_CIRCLE="Cerchio trasparente";
CROSSED_CIRCLE="Cerchio con croce";
TRIANGLE="Triangolo con contorno";
FILLED_TRIANGLE="Triangolo pieno";
PLUS="Segno +";
CROSS="Croce";
DOUBLE_FILLED_ARROW="Freccia piena doppia";

ComboSymbolModeLine0="Automatica";
ComboSymbolModeLine1="Interna";
ComboSymbolModeLine2="Esterna";
ComboSymbolModeLine3="Simbolo 1 interno / Simbolo 2 esterno";
ComboSymbolModeLine4="Simbolo 1 esterno / Simbolo 2 interno";

ComboExtendToCenterMode0="Da standard";
ComboExtendToCenterMode1="Fino al centro";
ComboExtendToCenterMode2="Non fino al centro";

FrameSymbolTitle.LabelSymbolTitle.Title = "Simboli ";

FrameDimensionLine.SubFrameDimLineMain.LabelRepresentation.Title = "Rappresentazione: ";
FrameDimensionLine.SubFrameDimLineMain.LabelColorLine.Title = "Colore: ";
FrameDimensionLine.SubFrameDimLineMain.LabelThicknessLine.Title = "Spessore: ";


SubFrameDimLeader.SubFrameDimLineLeaderAngle.Title = "Guida";
SubFrameDimLeader.SubFrameDimLineLeaderAngle.LabelLeaderAngle.Title = "Angolo: ";
SubFrameDimLeader.SubFrameDimLineLeaderAngle.LabelLeaderLength.Title = "Lunghezza: ";

SubFrameDimLeader.SubFrameDimLine2Part.Title = "Seconda parte";
SubFrameDimLeader.SubFrameDimLine2Part.LabelReference.Title = "Riferimento: ";
SubFrameDimLeader.SubFrameDimLine2Part.LabelOrientation.Title = "Orientamento: ";
SubFrameDimLeader.SubFrameDimLine2Part.LabelAngle.Title = "Angolo: ";

FrameDimensionLine.SubFrameDimLineMain.LabelExtendToCenter.Title = "Estensione:";


FrameSymbol.SubFrameSymbol.LabelSymbol.Title = "Simbolo 1";
FrameSymbol.SubFrameSymbol.LabelShape.Title = "Forma: ";
FrameSymbol.SubFrameSymbol.LabelThicknessSymbol.Title = "Spessore: ";
FrameSymbol.SubFrameSymbol.LabelColorSymbol.Title = "Colore: ";
FrameSymbol.SubFrameSymbol.LabelSymbol2.Title = "Simbolo 2";
FrameSymbol.SubFrameSymbol.LabelSymbol3.Title = "Simbolo della guida";

FrameSymbol.SubFrameSymbolMore.LabelSymbolMode.Title = "Inversione: ";
FrameSymbol.SubFrameSymbolMore.CheckButtonDisplaySymb2.Title = "Simbolo di visualizzazione 2";

FrameSymbol.LabelSymbol.Title = "Simbolo 1";
FrameSymbol.LabelShape.Title = "Forma: ";
FrameSymbol.LabelThicknessSymbol.Title = "Spessore: ";
FrameSymbol.LabelColorSymbol.Title = "Colore: ";
FrameSymbol.LabelSymbol3.Title = "Simbolo della guida";

FrameSymbol.LabelSymbolMode.Title = "Inversione: ";
FrameSymbol.CheckButtonDisplaySymb2.Title = "Simbolo 2";

FrameDimensionLine.SubFrameDimLineMain.ComboRepresentation.LongHelp=
"Fornisce una rappresentazione per la linea di quota.";
FrameDimensionLine.SubFrameDimLineMain.ComboColorLine.LongHelp=
"Definisce il colore della linea di quota.";
FrameDimensionLine.SubFrameDimLineMain.ComboThicknessLine.LongHelp=
"Definisce lo spessore della linea di quota.";

FrameDimensionLine.SubFrameDimLine2Part.ComboReference.LongHelp=
"Fornisce un riferimento per il posizionamento delle parti secondarie della linea di quota.";
FrameDimensionLine.SubFrameDimLine2Part.ComboOrientation.LongHelp=
"Definisce un orientamento per le parti secondarie della linea di quota in relazione al suo \"Riferimento\".";
FrameDimensionLine.SubFrameDimLine2Part.SpinnerAngle.LongHelp=
"Definisce un angolo per le parti secondarie della linea di quota in relazione al suo \"Riferimento\".";

FrameSymbol.SubFrameSymbol.ComboSymbol.LongHelp=
"Definisce la forma dei simboli.";
FrameSymbol.SubFrameSymbol.ComboSymbol2.LongHelp=
"Definisce la forma dei simboli.";
FrameSymbol.SubFrameSymbol.ComboSymbol3.LongHelp=
"Definisce la forma dei simboli.";

FrameSymbol.SubFrameSymbol.ComboThicknessSymbol.LongHelp=
"Definisce lo spessore per i simboli.";
FrameSymbol.SubFrameSymbol.ComboThicknessSymbol2.LongHelp=
"Definisce lo spessore per i simboli.";
FrameSymbol.SubFrameSymbol.ComboThicknessSymbol3.LongHelp=
"Definisce lo spessore per i simboli.";

FrameSymbol.SubFrameSymbol.ComboColorSymbol.LongHelp=
"Definisce un colore per i simboli.";
FrameSymbol.SubFrameSymbol.ComboColorSymbol2.LongHelp=
"Definisce un colore per i simboli.";
FrameSymbol.SubFrameSymbol.ComboColorSymbol3.LongHelp=
"Definisce un colore per i simboli.";

FrameSymbol.SubFrameSymbolMore.ComboSymbolMode.LongHelp=
"Inverte la posizione del simbolo in relazione alle linee di estensione.";
