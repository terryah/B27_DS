//---------------------------------------------------------------
// Resource file for CATGSMUILinePanel class
// US
//---------------------------------------------------------------

Title="Definizione di linea";

LineTypePtPt="Punto-Punto";
LineTypePtDir="Punto-Direzione";
LineTypeAngle="Angolo / normale alla curva";
LineTypeTgt="Tangente alla curva";
LineTypeNrm="Normale alla superficie";
LineTypeNrmC="Normale alla curva";
LineTypeBisecting="Bisecante";
LineTypeExpl="Esplicito";

frame1.LabList.Title="Tipo di linea:";
frame1.CmbList.LongHelp="Elenca i metodi disponibili per la creazione di linee.";

PtPtFrame.FraGeo.LabelPtPtPointO.Title="Punto 1: ";
PtPtFrame.FraGeo.LabelPtPtPointE.Title="Punto 2: ";
PtPtFrame.FraGeo.LabelPtPtSupp.Title="Riferimento: ";
PtPtFrame.FraGeo.LabelPtPtStart.Title="Inizio: ";
PtPtFrame.FraGeo.LabelPtPtEnd.Title="Fine: ";
PtPtFrame.FraGeo.FraElem1.LongHelp="Specifica il punto utilizzato come prima estremit� della linea.";
PtPtFrame.FraGeo.FraElem2.LongHelp="Specifica il punto utilizzato come seconda estremit� della linea.";
PtPtFrame.FraGeo.FraGeod2.LongHelp="Specifica la superficie di riferimento su cui\nla linea geometrica deve essere creata.\nFacoltativo.";
PtPtFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Inizio";
PtPtFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Fine";
PtPtFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Specifica l'estensione della linea dal punto di inizio.\n� inoltre possibile utilizzare il menu contestuale per specificare una formula";
PtPtFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Specifica l'estensione della linea dal punto di fine.\n� inoltre possibile utilizzare il menu contestuale per specificare una formula";
PtPtFrame.ChkMirror.Title="Estensione specchiata";
PtPtFrame.ChkMirror.LongHelp="Specchia l'estensione di fine";
PtPtFrame.ChkRepeat.Title="Riseleziona il secondo punto all'avvio successivo";
PtPtFrame.ChkRepeat.LongHelp="Al successivo avvio del comando, il primo punto diverr� il secondo punto corrente selezionato.";
PtPtFrame.FraRadio.LabelRadio.Title="Tipo lunghezza";
PtPtFrame.FraRadio.RadioLength.Title="Lunghezza";
PtPtFrame.FraRadio.RadioInfinite.Title="Infinito";
PtPtFrame.FraRadio.RadioInfStart.Title="Punto iniziale infinito";
PtPtFrame.FraRadio.RadioInfEnd.Title="Punto finale infinito";
PtPtFrame.FraGeo.LabelPtPtUpto1.Title="Fino a 1: ";
PtPtFrame.FraGeo.LabelPtPtUpto2.Title="Fino a 2: ";
PtPtFrame.FraGeo.FraUpto1.LongHelp="Specifica il primo elemento Fino a per la creazione della linea.";
PtPtFrame.FraGeo.FraUpto2.LongHelp="Specifica il secondo elemento Fino a per la creazione della linea.";

PtPtFrame.FraRadio.RadioLength.LongHelp="Crea una linea con le lunghezze specificate.";
PtPtFrame.FraRadio.RadioInfinite.LongHelp="Crea una linea infinita.";
PtPtFrame.FraRadio.RadioInfStart.LongHelp="Crea una linea con un punto iniziale infinito e una lunghezza finale specificata.";
PtPtFrame.FraRadio.RadioInfEnd.LongHelp="Crea una linea con una lunghezza iniziale specificata e un punto finale infinito.";

PtDirFrame.FraGeo.LabelPtDirPoint.Title="Punto: ";
PtDirFrame.FraGeo.LabelPtDirDirection.Title="Direzione: ";
PtDirFrame.FraGeo.LabelPtDirStart.Title="Inizio: ";
PtDirFrame.FraGeo.LabelPtDirEnd.Title="Fine: ";
PtDirFrame.FraGeo.LabelPtDirSupp.Title="Riferimento: ";
PtDirFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Inizio";
PtDirFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Fine";
PtDirFrame.PushRev.Title="Inverti direzione";
PtDirFrame.PushRev.LongHelp="Specifica che la linea deve essere creata\nsull'altro lato del punto.";
PtDirFrame.FraGeo.FraElem1.LongHelp="Specifica il punto di riferimento per la creazione della linea.";
PtDirFrame.FraGeo.FraDirection.LongHelp="Specifica la linea con l'orientamento che determina la direzione\no il piano, di cui la normale determina la direzione.\n� anche possibile utilizzare il menu contestuale per specificare i componenti\nX, Y, Z della direzione.";
PtDirFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
PtDirFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
PtDirFrame.FraGeo.FraGeod2.LongHelp="Specifica la superficie di riferimento su cui deve trovarsi la linea.\nQuesto dato � opzionale.";
PtDirFrame.ChkMirror.Title="Estensione specchiata";
PtDirFrame.ChkMirror.LongHelp="Specchia l'offset finale";
PtDirFrame.FraRadio.LabelRadio.Title="Tipo lunghezza";
PtDirFrame.FraRadio.RadioLength.Title="Lunghezza";
PtDirFrame.FraRadio.RadioInfinite.Title="Infinito";
PtDirFrame.FraRadio.RadioInfStart.Title="Punto iniziale infinito";
PtDirFrame.FraRadio.RadioInfEnd.Title="Punto finale infinito";

PtDirFrame.FraRadio.RadioLength.LongHelp="Crea una linea con le lunghezze specificate.";
PtDirFrame.FraRadio.RadioInfinite.LongHelp="Crea una linea infinita.";
PtDirFrame.FraRadio.RadioInfStart.LongHelp="Crea una linea con un punto iniziale infinito e una lunghezza finale specificata.";
PtDirFrame.FraRadio.RadioInfEnd.LongHelp="Crea una linea con una lunghezza iniziale specificata e un punto finale infinito.";

PtDirFrame.FraGeo.LabelPtDirUpto1.Title="Fino a 1: ";
PtDirFrame.FraGeo.LabelPtDirUpto2.Title="Fino a 2: ";
PtDirFrame.FraGeo.FraUpto1.LongHelp="Specifica il primo elemento Fino a per la creazione della linea.";
PtDirFrame.FraGeo.FraUpto2.LongHelp="Specifica il secondo elemento Fino a per la creazione della linea.";

AngFrame.FraGeo.LabelAngCurve.Title="Curva: ";
AngFrame.FraGeo.LabelAngSupp.Title="Riferimento: ";
AngFrame.FraGeo.LabelAngPoint.Title="Punto: ";
AngFrame.FraGeo.LabelAngAngle.Title="Angolo: ";
AngFrame.FraGeo.LabelAngStart.Title="Inizio: ";
AngFrame.FraGeo.LabelAngEnd.Title="Fine: ";
AngFrame.PushNrm.Title="Normale alla curva";
AngFrame.PushRev.Title="Inverti direzione";
AngFrame.PushRev.LongHelp="Specifica che la linea deve essere creata\nsull'altro lato del punto.";
AngFrame.ChkGeod.Title="Geometria su riferimento";
AngFrame.FraGeo.FraAngTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Angolo";
AngFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Inizio";
AngFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Fine";
AngFrame.FraGeo.FraElem1.LongHelp="Specifica la curva di riferimento che contiene la tangente\nutilizzata per definire l'angolo della nuova linea.";
AngFrame.FraGeo.FraElem2.LongHelp="Specifica la superficie di riferimento su cui si trova la curva di riferimento.";
AngFrame.FraGeo.FraElem3.LongHelp="Specifica il punto in cui la tangente della curva\ndeve essere utilizzata per definire la nuova linea.";
AngFrame.FraGeo.FraAngTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
AngFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
AngFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
AngFrame.ChkGeod.LongHelp="Consente di creare la linea in\nmodalit� geodesica su una superficie di riferimento.";
AngFrame.PushNrm.LongHelp="Consente di specificare che � necessario utilizzare un angolo di 90 gradi\nper definire una nuova linea.";
AngFrame.ChkMirror.Title="Estensione specchiata";
AngFrame.ChkMirror.LongHelp="Specchia l'offset finale";
// for multiple solution..
AngFrame.PushNext.Title="Soluzione successiva";
AngFrame.PushNext.LongHelp="Consente di selezionare la successiva linea selezionata.";

AngFrame.FraRadio.LabelRadio.Title="Tipo lunghezza";
AngFrame.FraRadio.RadioLength.Title="Lunghezza";
AngFrame.FraRadio.RadioInfinite.Title="Infinito";
AngFrame.FraRadio.RadioInfStart.Title="Punto iniziale infinito";
AngFrame.FraRadio.RadioInfEnd.Title="Punto finale infinito";

AngFrame.FraRadio.RadioLength.LongHelp="Crea una linea con le lunghezze specificate.";
AngFrame.FraRadio.RadioInfinite.LongHelp="Crea una linea infinita.";
AngFrame.FraRadio.RadioInfStart.LongHelp="Crea una linea con un punto iniziale infinito e una lunghezza finale specificata.";
AngFrame.FraRadio.RadioInfEnd.LongHelp="Crea una linea con una lunghezza iniziale specificata e un punto finale infinito.";

AngFrame.FraGeo.LabelAngUpto1.Title="Fino a 1: ";
AngFrame.FraGeo.LabelAngUpto2.Title="Fino a 2: ";
AngFrame.FraGeo.FraUpto1.LongHelp="Specifica il primo elemento Fino a per la creazione della linea.";
AngFrame.FraGeo.FraUpto2.LongHelp="Specifica il secondo elemento Fino a per la creazione della linea.";

TgtFrame.FraGeo.LabelTgtCurve.Title="Curva: ";
TgtFrame.FraGeo.LabelTgtPoint.Title="Elemento 2: ";
TgtFrame.FraGeo.LabelTgtSupport.Title="Riferimento: ";
TgtFrame.FraGeo.FraGeod2.LongHelp="Specifica la superficie di riferimento su cui deve trovarsi la linea.\nIn modalit� bitangente, il riferimento � obbligatorio.";
TgtFrame.ChkBiTangent.Title="Bitangente";
TgtFrame.FraGeod.Title="Opzioni di tangenza";
TgtFrame.FraGeod.FraLong.LabelTgtStart.Title="Inizio: ";
TgtFrame.FraGeod.FraLong.LabelTgtEnd.Title="Fine: ";
TgtFrame.FraGeod.ChkGeod.Title="Geometria su riferimento";
TgtFrame.FraGeod.PushRev.Title="Inverti direzione";
TgtFrame.FraGeod.PushRev.LongHelp="Specifica che la linea deve essere creata\nsull'altro lato del punto.";
TgtFrame.FraGeod.FraLong.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Inizio";
TgtFrame.FraGeod.FraLong.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Fine";
TgtFrame.FraGeod.FraLong.LabelTgtStart.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
TgtFrame.FraGeod.FraLong.LabelTgtEnd.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
TgtFrame.FraGeo.FraElem1.LongHelp="Specifica la curva su cui\ndeve trovarsi la linea.";
TgtFrame.FraGeo.FraElem2.LongHelp="Specifica il punto di riferimento per la creazione della linea per la modalit� Tangente singola \noppure specifica la curva o il punto a cui la linea deve essere tangente per la modalit� bitangente.";
TgtFrame.FraGeod.ChkGeod.LongHelp="Consente di creare la linea in\nmodalit� geodesica su una superficie di riferimento.";
TgtFrame.FraGeod.frameTgt.ComboTangent.LongHelp="Definisce il tipo di tangenza:\n -Tangente singola: la linea creata � tangente alla curva\ne passa attraverso la proiezione del punto sulla curva.\n -Bitangente: la linea creata � tangente ad entrambi gli elementi.";
TextM="Tangente singola";
TextB="Bitangente";
TgtFrame.FraGeod.frameTgt.LabTgt.Title="Tipo: ";
TgtFrame.FraGeod.PushNext.Title="Soluzione successiva";
TgtFrame.FraGeod.PushNext.LongHelp="Consente di selezionare la successiva linea selezionata.";
TgtFrame.FraGeod.ChkMirror.Title="Estensione specchiata";
TgtFrame.FraGeod.ChkMirror.LongHelp="Specchia l'offset finale";
TgtFrame.FraGeod.FraRadio.LabelRadio.Title="Tipo lunghezza";
TgtFrame.FraGeod.FraRadio.RadioLength.Title="Lunghezza";
TgtFrame.FraGeod.FraRadio.RadioInfinite.Title="Infinito";
TgtFrame.FraGeod.FraRadio.RadioInfStart.Title="Punto iniziale infinito";
TgtFrame.FraGeod.FraRadio.RadioInfEnd.Title="Punto finale infinito";
TgtFrame.FraGeod.FraLong.LabelTgtUpto1.Title="Fino a 1: ";
TgtFrame.FraGeod.FraLong.LabelTgtUpto2.Title="Fino a 2: ";

TgtFrame.FraGeod.FraRadio.RadioLength.LongHelp="Crea una linea con le lunghezze specificate.";
TgtFrame.FraGeod.FraRadio.RadioInfinite.LongHelp="Crea una linea infinita.";
TgtFrame.FraGeod.FraRadio.RadioInfStart.LongHelp="Crea una linea con un punto iniziale infinito e una lunghezza finale specificata.";
TgtFrame.FraGeod.FraRadio.RadioInfEnd.LongHelp="Crea una linea con una lunghezza iniziale specificata e un punto finale infinito.";
TgtFrame.FraGeod.FraLong.FraUpto1.LongHelp="Specifica il primo elemento Fino a per la creazione della linea.";
TgtFrame.FraGeod.FraLong.FraUpto2.LongHelp="Specifica il secondo elemento Fino a per la creazione della linea.";

NrmFrame.FraGeo.LabelNrmSkin.Title="Superficie: ";
NrmFrame.FraGeo.LabelNrmPoint.Title="Punto: ";
NrmFrame.FraGeo.LabelNrmStart.Title="Inizio: ";
NrmFrame.FraGeo.LabelNrmEnd.Title="Fine: ";
NrmFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Inizio";
NrmFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Fine";
NrmFrame.FraGeo.FraElem1.LongHelp="Specifica la superficie di riferimento la cui normale\nviene utilizzata per definire la nuova linea.";
NrmFrame.FraGeo.FraElem2.LongHelp="Specifica il punto sulla superficie in cui la normale\ndeve essere utilizzata per definire la nuova linea.";
NrmFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
NrmFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="� anche possibile utilizzare il menu contestuale per specificare le formule";
NrmFrame.PushRev.Title="Inverti direzione";
NrmFrame.PushRev.LongHelp="Specifica che la linea deve essere creata\nsull'altro lato del punto.";
NrmFrame.ChkMirror.Title="Estensione specchiata";
NrmFrame.ChkMirror.LongHelp="Specchia l'offset finale";
NrmFrame.FraRadio.LabelRadio.Title="Tipo lunghezza";
NrmFrame.FraRadio.RadioLength.Title="Lunghezza";
NrmFrame.FraRadio.RadioInfinite.Title="Infinito";
NrmFrame.FraRadio.RadioInfStart.Title="Punto iniziale infinito";
NrmFrame.FraRadio.RadioInfEnd.Title="Punto finale infinito";
NrmFrame.FraGeo.LabelNrmUpto1.Title="Fino a 1: ";
NrmFrame.FraGeo.LabelNrmUpto2.Title="Fino a 2: ";
NrmFrame.FraGeo.FraUpto1.LongHelp="Specifica il primo elemento Fino a per la creazione della linea.";
NrmFrame.FraGeo.FraUpto2.LongHelp="Specifica il secondo elemento Fino a per la creazione della linea.";

NrmFrame.FraRadio.RadioLength.LongHelp="Crea una linea con le lunghezze specificate.";
NrmFrame.FraRadio.RadioInfinite.LongHelp="Crea una linea infinita.";
NrmFrame.FraRadio.RadioInfStart.LongHelp="Crea una linea con un punto iniziale infinito e una lunghezza finale specificata.";
NrmFrame.FraRadio.RadioInfEnd.LongHelp="Crea una linea con una lunghezza iniziale specificata e un punto finale infinito.";

BisectingFrame.FraGeo.LabelBisectLine1.Title="Linea 1: ";
BisectingFrame.FraGeo.LabelBisectLine2.Title="Linea 2: ";
BisectingFrame.FraGeo.LabelBisectPoint.Title="Punto: ";
BisectingFrame.FraGeo.LabelBisectSupp.Title="Riferimento: ";
BisectingFrame.FraGeo.LabelBisectStart.Title="Inizio: ";
BisectingFrame.FraGeo.LabelBisectEnd.Title="Fine: ";
BisectingFrame.FraGeo.FraElem1.LongHelp="Specifica la prima linea.";
BisectingFrame.FraGeo.FraElem2.LongHelp="Specifica la seconda linea.";
BisectingFrame.FraGeo.FraElem4.LongHelp="Specifica il punto iniziale.\nFacoltativo.";
BisectingFrame.FraGeo.FraGeod2.LongHelp="Specifica la superficie di riferimento su cui\nla linea deve essere proiettata.\nFacoltativo.";
BisectingFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.Title="Inizio";
BisectingFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.Title="Fine";
BisectingFrame.FraGeo.FraLengTang.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Specifica l'estensione della linea dal punto di inizio.\n� inoltre possibile utilizzare il menu contestuale per specificare una formula";
BisectingFrame.FraGeo.FraLengTang2.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Specifica l'estensione della linea dal punto di fine.\n� inoltre possibile utilizzare il menu contestuale per specificare una formula";
BisectingFrame.ChkMirror.Title="Estensione specchiata";
BisectingFrame.ChkMirror.LongHelp="Specchia l'estensione di fine";
BisectingFrame.PushRev.Title="Inverti direzione";
BisectingFrame.PushRev.LongHelp="Specifica che la linea deve essere creata\nsull'altro lato del punto.";
BisectingFrame.PushNext.Title="Soluzione successiva";
BisectingFrame.PushNext.LongHelp="Consente di selezionare la successiva linea selezionata.";
BisectingFrame.FraRadio.LabelRadio.Title="Tipo lunghezza";
BisectingFrame.FraRadio.RadioLength.Title="Lunghezza";
BisectingFrame.FraRadio.RadioInfinite.Title="Infinito";
BisectingFrame.FraRadio.RadioInfStart.Title="Punto iniziale infinito";
BisectingFrame.FraRadio.RadioInfEnd.Title="Punto finale infinito";

BisectingFrame.FraRadio.RadioLength.LongHelp="Crea una linea con le lunghezze specificate.";
BisectingFrame.FraRadio.RadioInfinite.LongHelp="Crea una linea infinita.";
BisectingFrame.FraRadio.RadioInfStart.LongHelp="Crea una linea con un punto iniziale infinito e una lunghezza finale specificata.";
BisectingFrame.FraRadio.RadioInfEnd.LongHelp="Crea una linea con una lunghezza iniziale specificata e un punto finale infinito.";

BisectingFrame.FraGeo.LabelBisectUpto1.Title="Fino a 1: ";
BisectingFrame.FraGeo.LabelBisectUpto2.Title="Fino a 2: ";
BisectingFrame.FraGeo.FraUpto1.LongHelp="Specifica il primo elemento Fino a per la creazione della linea.";
BisectingFrame.FraGeo.FraUpto2.LongHelp="Specifica il secondo elemento Fino a per la creazione della linea.";

ExpDirFrame.FrameDir.Title="Direzione: ";
ExpDirFrame.FrameDir.LabelExpDirX.Title="X: ";
ExpDirFrame.FrameDir.LabelExpDirY.Title="Y: ";
ExpDirFrame.FrameDir.LabelExpDirZ.Title="Z: ";
ExpDirFrame.No_information_available.Title="Nessuna informazione disponibile";
ExpDirFrame.UxValue.LongHelp="Specifica il componente X della direzione della linea.";
ExpDirFrame.UyValue.LongHelp="Specifica il componente Y della direzione della linea.";
ExpDirFrame.UzValue.LongHelp="Specifica il componente Z della direzione della linea.";

// start code 3dplm
//Combo lock help
LineLockShort="Fare clic per abilitare la modifica del tipo automatica.";
LineUnlockShort="Fare clic per disabilitare la modifica del tipo automatica.";
LineLockLong="Facendo clic su questo pulsante verr� abilitata la modifica del tipo automatica.";
LineUnlockLong="Facendo clic su questo pulsante verr� disabilitata la modifica del tipo automatica.";
// end code 3dplm
