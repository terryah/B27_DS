// =====================================================================
//
// Erreurs Framework 'CATLatheMach'
// =================================
//
// Deux manieres de formater vos messages : 
//
// a- Specifier en delimitant avec le caractere '\n' :
//       Request      (ce que l'on tentait de faire)
//       Diagnostic   (ce qui est arrive)
//       Advice       (...ce qu'il pourrait faire si possible)
//
//     Exemple :
//       ERR_3000 = "Initializing '/p1' attribute in '/p2'.\nAttribute does not exist.\nContact your local support.";  
//       
//               NOTE : Le parametrage des messages se fait via SetNLSParameter
//
// ou bien
//
// b- Specifier vos messages avec la cle + les 3 facettes Request/Diagnostic/Advice
//
//     Exemple :
//       ERR_3000.Request    "Initializing '/p1' attribute in '/p2'." 
//       ERR_3000.Diagnostic "Attribute does not exist."
//       ERR_3000.Advice     "\nContact your local support."
// 
//               NOTE : Le parametrage de chaque facette se fait par :
//                         Request    : SetNLSRequestParams
//                         Diagnostic : SetNLSDiagnosticParams
//                         Advice     : SetNLSAdviceParams
//
//
//
// NOTE : de 8670 � 8699 = WARNING
// 
// V5R13 
// Creneaux libres erreurs severes
//          8728 a 8791
//          8800 a 8819
//          8823 a infini
// V5R14 
// 10000 19999 ERREURS 
// 20000 29999 WARNINGS
// =====================================================================


//====================================================================================================
// Erreurs Turning Operations
//====================================================================================================
ERR_1.Diagnostic="Si � verificato un errore.";

ERR_2.Diagnostic="Si � verificato un errore.";

//------------------------
// Sequential operation 
//------------------------
ERR_21.Diagnostic="Potenziale collisione nel punto iniziale.";

ERR_23.Diagnostic="Impossibile determinare la posizione dell'utensile.";
ERR_23.Advice="\nVerificare le geometrie selezionate e i parametri delle traiettorie dell'utensile.";

ERR_27.Diagnostic="Impossibile determinare la posizione dell'utensile con il limite 'Effettivo'.
L'utensile � stato posizione all'estremit� pi� vicina del profilo.";

ERR_196.Request="Calcolo traiettoria utensile num. /p1.";
ERR_196.Diagnostic="La definizione della traiettoria dell'utensile d� luogo ad una posizione dell'utensile non modificata.";
ERR_196.Advice="\nVerificare che la traiettoria dell'utensile sia definita correttamente.";

ERR_199.Diagnostic="Si � verificato un errore.";

ERR_1000.Diagnostic="Si � verificato un errore.";
ERR_1001.Advice="Calcolo del Percorso utensile elementi limite non compatibile per utensili di due raggi diversi.";
ERR_1002.Diagnostic="Errore nel calcolo del comando '/p1'.";

ERR_1003.Request="Calcolo del percorso utensile.";
ERR_1003.Diagnostic="Esiste un solo elenco di comandi NC in questa operazione.";
ERR_1003.Advice="\nIl codice NC (Punto, Avanzamento e Velocit�) potrebbe essere errato.\nInserire almeno un punto o utilizzare l'attivit� di comando NC.";

ERR_1004.Request="";
ERR_1004.Diagnostic="La direzione di rotazione del mandrino selezionata dall'utente non � compatibile con i parametri dell'utensile. ";
ERR_1004.Advice="\nModificare i parametri dell'utensile oppure invertire la direzione di rotazione del mandrino. ";


//====================================================================================================
// Erreurs Turning Operations
//====================================================================================================

ERR_8600.Diagnostic="La geometria della parte proiettata sul piano di tornitura fornisce un profilo incongruente.";
ERR_8600.Advice="\nVerificare se la geometria della parte � congruente con il piano di tornitura.";

ERR_8601.Diagnostic="La geometria del grezzo proiettata sul piano di tornitura fornisce un profilo incongruente.";
ERR_8601.Advice="\nVerificare se la geometria del grezzo � congruente con il piano di tornitura.";

ERR_8647.Diagnostic="L'operazione e i parametri dell'utensile sono incompatibili con la parte.";
ERR_8647.Advice="\nVerificare i parametri dell'operazione e dell'utensile.";

ERR_8648.Diagnostic="L'operazione e i parametri dell'utensile sono incompatibili con il grezzo.";
ERR_8648.Advice="\nVerificare i parametri dell'operazione e dell'utensile.";

ERR_8649.Diagnostic="La geometria selezionata e i parametri dell'operazione conducono ad un'area non lavorabile.";
ERR_8649.Advice="\nVerificare quanto segue:
\na- La compatibilit� del grezzo e della parte.
\nb- I parametri principali che influenzano la relimitazione dell'area da lavorare:
\t- Orientamento e posizione nell'operazione.
\t- Gli angoli di attacco e di uscita nell'operazione aggiunti a quelli definiti nell'inserto-codolo.
\t- L'angolo di impostazione dell'assieme di lavorazione.";

ERR_8650.Diagnostic="Collisione rilevata nella traiettoria di alzata.";
ERR_8650.Advice="\nVerificare i parametri di alzata.";

ERR_8651.Diagnostic="Collisione rilevata nella traiettoria di attacco.";
ERR_8651.Advice="\nVerificare i parametri di attacco.";

ERR_8652.Diagnostic="La profondit� assiale e radiale dei valori di taglio non possono essere entrambe uguali a zero.";
ERR_8652.Advice="\nModificare uno dei valori.";

ERR_8653.Diagnostic="Il profilo della parte � incongruente quando vi vengono applicati gli offset.";
ERR_8653.Advice="\nAssegnare un offset su una parte pi� piccola.";

ERR_8654.Request="Verifica dell'elemento iniziale.";
ERR_8654.Diagnostic="La posizione sul profilo della parte non pu� essere determinata se l'operazione � un'operazione di finitura.
La posizione sul profilo del grezzo non pu� essere determinata se l'operazione � un'operazione di tornitura di sgrossatura";
ERR_8654.Advice="\nSelezionare un altro elemento iniziale.";

ERR_8655.Request="Verifica dell'elemento finale.";
ERR_8655.Diagnostic="Impossibile determinare la posizione del profilo sulla parte";
ERR_8655.Advice="\nSelezionare un altro elemento finale.";

ERR_8656.Request="Verifica della direzione di lavorazione.";
ERR_8656.Diagnostic="L'elemento iniziale e l'elemento finale non sono compatibili con la direzione di lavorazione definita.";
ERR_8656.Advice="\nModificare uno dei parametri.";

ERR_8657.Diagnostic="L'inserto non � compatibile con il profilo della parte da lavorare.";
ERR_8657.Advice="\nVerificare i parametri dell'operazione e dell'inserto.";

ERR_8658.Diagnostic="La durata massima specificata nell'inserto-codolo � stata raggiunta. Non � impossibile lavorare tutti i percorsi.";
ERR_8658.Advice="\nModificare i parametri dell'operazione o l'inserto-codolo.
Se � attiva la modalit� 'inizio del percorso', verificare che la durata dell'interruzione sia maggiore della durata del primo percorso.";

ERR_8659.Diagnostic="L'offset sugli elementi lavorati con la direzione invertita � troppo ampio.";
ERR_8659.Advice="\nModificare il valore di offset.";

ERR_8671.Diagnostic="Raggio di attacco troppo piccolo. ";
ERR_8671.Advice="\nIl raggio di attacco deve essere maggiore del raggio della punta dell'inserto.";

ERR_8672.Diagnostic="Raggio di alzata troppo piccolo.";
ERR_8672.Advice="\nIl raggio di alzata deve essere maggiore del raggio della punta dell'inserto.";

ERR_8673.Diagnostic="Ultimo raggio di attacco troppo piccolo.";
ERR_8673.Advice="\nL'ultimo raggio di attacco deve essere maggiore del raggio della punta dell'inserto.";

ERR_8674.Diagnostic="Altro raggio di attacco troppo piccolo.";
ERR_8674.Advice="\nL'altro raggio di attacco deve essere maggiore del raggio della punta dell'inserto.";

ERR_8675.Diagnostic="Numero di correttore appropriato non definito sull'inserto-codolo.";
ERR_8675.Advice="\nDefinire il correttore appropriato sull'inserto-codolo in relazione ai parametri dell'operazione 'Orientamento' e 'Posizione'.";

ERR_8676.Diagnostic="� stata rilevata una collisione tra l'inserto ed il profilo della parte durante una traiettoria rapida.";
ERR_8676.Advice="\nVerificare i parametri dell'operazione.";

ERR_8677.Diagnostic="� stata rilevata una collisione tra l'inserto ed il profilo della parte durante una traiettoria di alzata.";
ERR_8677.Advice="\nVerificare i parametri di alzata.";

ERR_8678.Diagnostic="L'opzione 'Cambia il punto di uscita' non verr� presa in considerazione.\nLe correzioni appropriate non sono definite su inserto-codolo";
ERR_8678.Advice="\nDisattivare l'opzione o definire le correzioni appropriate su inserto-codolo rispetto all'orientamento dell'utensile.";

ERR_8679.Diagnostic="Ampiezza della parte inferiore della scanalatura troppo piccola.\nIl disimpegno sul fianco successivo e la sovrapposizione sono troppo grandi rispetto all'ampiezza della parte inferiore della scanalatura.";
ERR_8679.Advice="\nModificare i parametri di lavorazione o l'inserto.";

ERR_8680.Diagnostic="Ultima profondit� di taglio troppo ampia. Le ultime passate lavorano completamente la filettatura.";
ERR_8680.Advice="\nModificare i parametri di lavorazione.";

ERR_8681.Diagnostic="Prima sezione di taglio troppo ampia. Le prime passate lavorano completamente la filettatura.";
ERR_8681.Advice="\nModificare i parametri di lavorazione.";

ERR_8682.Diagnostic="Disimpegno troppo ampio. La direzione di lavorazione su un elemento non pu� essere invertita.";
ERR_8682.Advice="\nModificare i parametri di lavorazione.";

ERR_8683.Diagnostic="Sovrapposizione troppo ampia. La sovrapposizione � maggiore di uno degli elementi lavorati nella direzione invertita.";
ERR_8683.Advice="\nModificare i parametri di lavorazione.";

ERR_8684.Diagnostic="L'area di lavorazione � ridotta a causa del valore 'Profondit� max barenatura' definito su inserto-codolo.";
ERR_8684.Advice="\nQuesto valore pu� essere ignorato utilizzando l'opzione 'Vincoli Inserto-codolo' a livello di lavorazione.";

ERR_8685.Diagnostic="L'area di lavorazione � ridotta a causa del valore 'Profondit� di taglio max' definito su inserto-codolo.";
ERR_8685.Advice="\nQuesto valore pu� essere ignorato utilizzando l'opzione 'Vincoli Inserto-codolo' a livello di lavorazione.";

ERR_8686.Diagnostic="Il valore specificato 'Raggio di lavorazione min.' non influisce sull'area da lavorare.";

ERR_8687.Diagnostic="Il valore specificato 'Raggio di lavorazione max' non influisce sull'area da lavorare.";

ERR_8688.Diagnostic="Il limite assiale per le griffe del mandrino non � compatibile con la parte da lavorare.";
ERR_8688.Advice="\nVerificare il valore del parametro.";

ERR_8689.Diagnostic="Le distanze di fresatura a tuffo sono troppo ampie in confronto alla profondit� di taglio.";

ERR_8690.Diagnostic="Il valore 'Profondit� max livello' � ridotta a causa del valore 'Profondit� di taglio max' specificato su inserto-codolo.";
ERR_8690.Advice="\nQuesto valore pu� essere ignorato utilizzando l'opzione 'Vincoli Inserto-codolo' a livello di lavorazione.";

ERR_8691.Diagnostic="La 'larghezza di inserimento' � minore di o uguale alla 'massima profondit� di taglio'.\nLa 'massima profondit� di taglio' viene ridotta per creare una seconda passata per lavorare completamente la linguetta.";
ERR_8691.Advice="Se non si desidera la seconda passata, impostare 'Massima profondit� di taglio' su un valore maggiore di 'Larghezza di inserimento'.";

ERR_8692.Diagnostic="'Massima profondit� di taglio' � maggiore di due volta rispetto al valore di 'Larghezza di inserimento'. Il materiale rimane dopo la lavorazione della linguetta.";
ERR_8692.Advice="Per lavorare completamente la linguetta, � possibile ridurre il valore di 'Massima profondit� di taglio' o scegliere un inserimento con una 'Larghezza di inserimento' maggiore.";

ERR_8693.Diagnostic="Il valore 'Larghezza di taglio max' sull'inserto-codolo � minore del valore 'Massima profondit� di taglio'.\nIl valore 'Massima profondit� di taglio' viene ridotto rispetto a 'Larghezza di taglio max'.";

ERR_8694.Diagnostic="Raggio di alzata inversa troppo piccolo.";
ERR_8694.Advice="\nIl raggio di alzata inversa locale deve essere maggiore del 'Raggio della punta' dell'utensile.";

ERR_8695.Diagnostic="Le profondit� di taglio assiale e radiale non sono compatibili con la parte da lavorare.";
ERR_8695.Advice="\nModificare i valori dei parametri.";

ERR_8696.Diagnostic="Errore durante l'elaborazione delle informazioni sull'elemento iniziale.";

ERR_8697.Diagnostic="Errore durante l'elaborazione delle informazioni sull'elemento finale.";

ERR_8698.Diagnostic="Non � possibile inviare il comando CUTCOM automaticamente prima dell'attacco.";
ERR_8698.Advice="\nInserire manualmente il comando CUTCOM nella macro di avvicinamento";

ERR_8699.Diagnostic="La compensazione dell'utensile non � attivata poich� la macchina specificata non � una macchina tornio.";
ERR_8699.Advice="\nSelezionare una macchina tornio.";

ERR_8700.Diagnostic="La compensazione dell'utensile selezionata non � definita nell'inserto-codolo '/p1'.\nPer impostazione predefinita, viene preso in considerazione il tipo 'P9'.";
ERR_8700.Advice="\nSe necessario, selezionare un altro tipo di compensazione dell'utensile definito nell'inserto-codolo.";

ERR_8701.Diagnostic="Il profilo non pu� essere lavorato dall'operazione.";
ERR_8701.Advice="\nDividere il profilo e lavorare i nuovi profili combinando le operazioni Tornitura della cava su rampa e Tornitura di sgrossatura della rampa.";

ERR_INTERRUPT_TIM_0.Diagnostic="La durata dell'interruzione deve essere maggiore di 0.";
ERR_INTERRUPT_TIM_0.Advice="\nSe � richiesta l'opzione 'A tempo', specificare un valore corretto.";

ERR_COMPUTED_STOCK_NOT_UP_TO_DATE.Diagnostic="Il grezzo di immissione calcolato precedentemente per l'operazione '/p1' non � pi� aggiornato.";
ERR_COMPUTED_STOCK_NOT_UP_TO_DATE.Advice="\nPer ottenere il percorso utensile aggiornato, rimuovere la selezione manuale del grezzo. � possibile aggiornare manualmente il grezzo o fare in modo che venga calcolato e selezionato automaticamente durante il calcolo del percorso utensile.";

ERR_30003.Diagnostic="Errore durante l'elaborazione delle informazioni sull'elemento finale nella sgrossatura linguetta.
\nLa geometria della linguetta non presenta fianchi per l'impostazione della prima fresatura o per il parametro della strategia della posizione della fresatura a tuffo successiva.";
ERR_30003.Advice="\nLa linguetta lavorata con l'elemento iniziale deve avere un fianco. Definire un fianco per
\nil parametro della strategia menzionato.";

//====================================================================================================
// erreurs preparation geometrie 
//====================================================================================================

ERR_8703.Diagnostic="Il profilo della linguetta � incompatibile con l'utensile.";
ERR_8703.Advice="\nSelezionare un utensile appropriato.";

ERR_8704.Diagnostic="Impossibile calcolare la profondit� del taglio risultante.";
ERR_8704.Advice="\nVerificare che l'orientamento della geometria sia compatibile con l'operazione.";

ERR_8705.Diagnostic="Impossibile calcolare il grezzo di immissione.";
ERR_8705.Advice="\nSelezionare manualmente un altro profilo del grezzo.";

ERR_8706.Diagnostic="La continuit� del profilo del grezzo non � rispettata.";
ERR_8706.Advice="\nSelezionare manualmente un altro profilo del grezzo.";

ERR_8707.Diagnostic="L'orientamento del profilo del grezzo non � congruente.";
ERR_8707.Advice="\nSelezionare manualmente un altro profilo del grezzo.";

ERR_8708.Diagnostic="Il profilo della parte non pu� essere elaborato correttamente.";
ERR_8708.Advice="\nVerificare il profilo della parte e gli offset globali e locali sulla parte.";

ERR_8709.Diagnostic="L'estensione dell'area lavorata porta ad un'incongruenza.";
ERR_8709.Advice="\nVerificare la posizione relativa del profilo della parte e del profilo del grezzo.";

ERR_8710.Diagnostic="Area da lavorare non valida.";

ERR_8711.Diagnostic="Nessuna passata calcolata per questa rampa.";
ERR_8711.Advice="\nVerificare la posizione relative del grezzo e della parte e la profondit� di taglio.";

ERR_8712.Diagnostic="Nessuna passata calcolata per questa linguetta.";
ERR_8712.Advice="\nVerificare la posizione relative del grezzo e della parte e la profondit� di taglio.";

ERR_8713.Diagnostic="Nessuna passata calcolata per questa lavorazione a zig-zag della cava.";
ERR_8713.Advice="\nVerificare la posizione relative del grezzo e della parte e la profondit� di taglio.";

ERR_8714.Diagnostic="Nessuna passata calcolata per questa lavorazione della cava della rampa.";
ERR_8714.Advice="\nVerificare la posizione relative del grezzo e della parte e la profondit� di taglio.";

ERR_8715.Diagnostic="Nessun profilo risultante da lavorare.";
ERR_8715.Advice="\n1- Verificare se la geometria giace sul piano di tornitura.
\n2- Verificare gli offset globali e locali applicati alla parte, se presenti.
\n3- Verificare l'offset globale applicato al grezzo, se presente.
\n4- Verificare i parametri dell'utensile.";

ERR_8716.Diagnostic="Modalit� esterna non compatibile con la geometria.";
ERR_8716.Advice="\nSelezionare una geometria congruente con la modalit� esterna.";

ERR_8717.Diagnostic="Modalit� interna non compatibile con la geometria.";
ERR_8717.Advice="\nSelezionare una geometria congruente con la modalit� interna.";

ERR_8718.Diagnostic="Modalit� frontale non compatibile con la geometria.";
ERR_8718.Advice="\nSelezionare una geometria congruente con la modalit� frontale.";

ERR_8719.Diagnostic="Modalit� di orientamento 'Altro' non compatibile con la geometria.";
ERR_8719.Advice="\nSelezionare una modalit� compatibile con il profilo.";

ERR_8720.Diagnostic="Impossibile elaborare la proiezione del grezzo sul piano di tornitura.";
ERR_8720.Advice="\nVerificare la congruenza tra il grezzo e il piano di tornitura.";


//====================================================================================================
// erreurs op�ration sequential
//====================================================================================================

ERR_8620.Diagnostic="Il punto iniziale � sul contorno e non pu� essere calcolato.";
ERR_8620.Advice="\nSelezionare un altro elemento.";

ERR_8623.Diagnostic="Impossibile determinare la posizione dell'utensile.";
ERR_8623.Advice="\nVerificare la validit� della prima traiettoria dell'utensile con stato 'NC' (Non calcolabile).";

ERR_8624.Diagnostic="Problema relativo all'elemento iniziale.";
ERR_8624.Advice="\nVerificare la geometria selezionata e i parametri delle traiettorie dell'utensile.";

ERR_8625.Diagnostic="Loop dell'elemento finale.";
ERR_8625.Advice="\nVerificare la geometria selezionata e i parametri delle traiettorie dell'utensile.";

ERR_8727.Diagnostic="Calcolo della geometria della traiettoria dell'utensile non eseguito.";

ERR_8792.Diagnostic="Conflitto tra l'inserto e il lato della geometria da lavorare.";
ERR_8792.Advice="\nVerificare la compatibilit� tra la posizione precedente dell'utensile e la geometria da lavorare.";

ERR_8793.Diagnostic="Collisione rilevata tra lo spigolo sinistro dell'inserto e la geometria.";

ERR_8794.Diagnostic="Collisione rilevata tra lo spigolo destro dell'inserto e la geometria.";

ERR_8795.Diagnostic="Conflitto inserto/geometria in modalit� prevenzione delle collisioni.";

ERR_8796.Diagnostic="Posizione dell'utensile non modificata.";

ERR_8797.Diagnostic="Impossibile raggiungere la posizione dell'utensile.";

ERR_8798.Diagnostic="Nessuna guida corrente definita sulle traiettorie precedenti.";

ERR_8799.Diagnostic="La geometria per definire la traiettoria dell'utensile non � completamente specificata.";
ERR_8799.Advice="\nVerificare la geometria della traiettoria corrente dell'utensile.";

ERR_8820.Diagnostic="Modalit� 'Limite di verifica estesa' e 'Evita collisioni' non compatibile.
Causa un conflitto sullo spigolo sinistro dell'inserto.";
ERR_8820.Advice="\nModificare uno dei parametri.";

ERR_8821.Diagnostic="Modalit� 'Limite di verifica estesa' e 'Evita collisioni' non compatibile.
Causa un conflitto sullo spigolo destro dell'inserto.";
ERR_8821.Advice="\nModificare uno dei parametri.";

ERR_8822.Diagnostic="La modalit� 'Evita collisione' o 'Posizione della verifica' causa una posizione non determinata.";
ERR_8822.Advice="\nModificare uno dei parametri.";

//====================================================================================================
// warning sortie profil 8830 a 8834
// erreurs infrastructure nc
//====================================================================================================
ERR_8830.Request="L'opzione Profilo risultante non viene preso in considerazione per questa operazione.";
ERR_8830.Diagnostic="\nL'opzione � disponibile per la modalit� Tornitura con finitura del profilo e la modalit� longitudinale/faccia della tornitura di sgrossatura.";
ERR_8830.Advice="\nDisattivare l'opzione Profilo risultante.";

ERR_8831.Request="Profilo risultante non compatibile con lo smusso o la contornatura.";
ERR_8831.Diagnostic="\nIl profilo risultante non pu� essere elaborato con lo smusso o la contornatura.";
ERR_8831.Advice="\nDisattivare lo smusso o la contornatura.";

ERR_8832.Request="Cava in sgrossatura non � lavorato con l'opzione 'Profilo risultante'.";
ERR_8832.Diagnostic="\nL'opzione Cava viene ignorata perch� viene eseguito il 'Profilo risultante'.";
ERR_8832.Advice="\nDisattivare la lavorazione della cava.";

ERR_8833.Request="Elemento finale nella sgrossatura ignorato nel profilo risultante.";
ERR_8833.Diagnostic="\nL'opzione Elemento finale viene ignorata perch� viene eseguito il profilo risultante.";
ERR_8833.Advice="\nSelezionare l'opzione Nessuno per l'elemento finale nel profilo risultante.";

ERR_8834.Request="Interruzione nella sgrossatura ignorata nel profilo risultante.";
ERR_8834.Diagnostic="\nL'interruzione viene ignorata perch� viene eseguito il profilo risultante.";
ERR_8834.Advice="\nSelezionare l'opzione Nessuna per la modalit� di interruzione in Profilo risultante.";

ERR_8835.Request="Il raggio degli elementi del profilo risultante deve essere maggiore di quello dell'inserto.";
ERR_8835.Diagnostic="\nUno dei raggi degli elementi del profilo risultante � minore di quello dell'inserto.";
ERR_8835.Advice="\nModificare il raggio dell'elemento del profilo/inserto per ottenere risultati migliori.";

ERR_8836.Request="Profilo risultante non compatibile con smusso/contornatura.";
ERR_8836.Diagnostic="\nIl profilo risultante non pu� essere elaborato con lo smusso o la contornatura.";
ERR_8836.Advice="\nDisattivare o non selezionare alcuna opzione di smusso/contornatura.";

ERR_9701.Diagnostic="Errore durante la lettura dei parametri dell'operazione.";

ERR_9702.Diagnostic="Errore durante la lettura della geometria da lavorare.";

ERR_9703.Diagnostic="Errore durante la lettura dei parametri utensile.";

ERR_9710.Diagnostic="Errore durante l'elaborazione del contorno da lavorare.";

ERR_9735.Diagnostic="Memoria non sufficiente per il calcolo o errore.";

ERR_9736.Diagnostic="Memoria non sufficiente per il calcolo.";

ERR_9737.Diagnostic="Si � verificato un errore.";

//====================================================================================================
// Geometry management
//====================================================================================================
ERR_256.Diagnostic="Errore durante l'elaborazione dell'area da lavorare.";

ERR_10000="Errore durante l'elaborazione della geometria della parte.";
ERR_10001="Errore durante l'elaborazione della geometria del grezzo.";
ERR_10002="Errore durante l'elaborazione della geometria dell'elemento di relimitazione finale.";
ERR_10003="Errore durante l'elaborazione della geometria dell'elemento di relimitazione iniziale.";
ERR_10004="Errore durante l'elaborazione della geometria da lavorare.";

//====================================================================================================
// Warnings about Multi Slide
//====================================================================================================
ERR_20000="La configurazione dell'utensile e/o la definizione dell'asse della torretta non consentono la lavorazione sul lato richiesto del mandrino.
Il percorso utensile verr� visualizzato sul lato opposto del mandrino.
\nPer lavorare la parte richiesta, verificare quanto segue:
a- La configurazione dell'utensile, ad esempio l'angolo di attrezzatura e lo stile (mano).
   La configurazione deve essere coerente con la geometria da lavorare.
b- La definizione di direzione radiale nel sistema di assi dell'utensile per tornitura.
   La direzione radiale deve partire dal mandrino verso la parte esterna della parte.";

ERR_20001="La configurazione dell'utensile e/o la definizione dell'asse della torretta non consentono la lavorazione sul lato richiesto del mandrino.
Il percorso utensile verr� visualizzato sul lato opposto del mandrino.
\nPer lavorare la parte richiesta, verificare quanto segue:
a- La configurazione dell'utensile, ad esempio l'angolo di attrezzatura e lo stile (mano).
   La configurazione deve essere coerente con la geometria da lavorare.
b- La definizione di direzione radiale nel sistema di assi dell'utensile per tornitura.
   La direzione radiale deve partire dal mandrino verso la parte esterna della parte.";

//====================================================================================================
// More Warnings about Grooving
//====================================================================================================
ERR_21001.Request="La Prima posizione di fresatura a tuffo deve essere Sinistra / Basso invece di Destra / Alto.";
ERR_21001.Diagnostic="\nLa linguetta ha un fianco compatibile con Sinistra / Basso o Centro per la Prima posizione di fresatura a tuffo.
\n Per il valore Destra / Alto esiste il rischio che il materiale lungo il fianco non venga completamente rimosso.";
ERR_21001.Advice="\n1 - Verificare la Prima posizione di fresatura a tuffo.
\n2-  Se � corretta, disattivare la Contornatura della parte.";

ERR_21002.Request="La Prima posizione di fresatura a tuffo deve essere Destra / Alto invece di Sinistra / Basso.";
ERR_21002.Diagnostic="\nLa linguetta ha un fianco compatibile con Destra / Alto o Centro per la Prima posizione di fresatura a tuffo.
\n Per il valore Sinistra / Basso esiste il rischio che il materiale lungo il fianco non venga rimosso completamente.";
ERR_21002.Advice="\n1 - Verificare la Prima posizione di fresatura a tuffo.
\n2-  Se � corretta, disattivare la Contornatura della parte.";

//====================================================================================================
// MultiTurret MultiSpindle at operation level
//====================================================================================================
ERR_TURRET_AXIS_ON_SPINDLE.Request="Il sistema di assi per l'utensile di tornitura '/p1' e il mandrino non sono compatibili.";
ERR_TURRET_AXIS_ON_SPINDLE.Diagnostic="L'origine di '/p1' giace sull'asse del mandrino del sistema di assi della parte.";
ERR_TURRET_AXIS_ON_SPINDLE.Advice="\nSpostare l'origine al di fuori dell'asse del mandrino.";

ERR_SPINDLE_AXIS_NOT_COLINEAR.Request="Il sistema di assi della parte '/p1' non � compatibile con il sistema di assi della parte principale.";
ERR_SPINDLE_AXIS_NOT_COLINEAR.Diagnostic="La direzione assiale di '/p1' non � colineare al mandrino.";
ERR_SPINDLE_AXIS_NOT_COLINEAR.Advice="\nModificare il sistema di assi della parte.";

ERR_SPINDLE_AXIS_NO_SPINDLE.Request="Nessun mandrino definito nella lavorazione '/p1'. Viene utilizzato il mandrino principale.";
ERR_SPINDLE_AXIS_NO_SPINDLE.Diagnostic="Nessun mandrino definito nella lavorazione '/p1'. Per impostazione predefinita, viene utilizzato il mandrino principale.";
ERR_SPINDLE_AXIS_NO_SPINDLE.Advice="\nScegliere un mandrino nell'elenco di mandrini disponibili per questa lavorazione.";


ERR_NO_SPINDLE_SELECTED.Diagnostic="La riproduzione non � possibile perch� nessun mandrino � assegnato all'operazione.";
ERR_NO_SPINDLE_SELECTED.Advice="Selezionare un mandrino a livello di operazione.";

ERR_NO_TURRET_SELECTED.Diagnostic="La riproduzione non � possibile perch� nessuna torretta � assegnata all'operazione.";
ERR_NO_TURRET_SELECTED.Advice="Selezionare una torretta a livello di programma.";

//====================================================================================================
// Plane management
//====================================================================================================
ERR_PART_NOT_ON_LOCAL_TURNING_PLANE.Diagnostic="La geometria della parte selezionata non giace sul piano di tornitura corrente.";
ERR_PART_NOT_ON_LOCAL_TURNING_PLANE.Advice="\nSelezionare la geometria della parte che giace su questo piano.";

ERR_30000.Diagnostic="La geometria della parte selezionata non giace sul piano di tornitura corrente.";
ERR_30000.Advice="\nSelezionare la geometria della parte che giace su questo piano.";

ERR_STOCK_NOT_ON_LOCAL_TURNING_PLANE.Diagnostic="La geometria del grezzo selezionata non giace sul piano di tornitura corrente.";
ERR_STOCK_NOT_ON_LOCAL_TURNING_PLANE.Advice="\nSelezionare la geometria del grezzo che giace su questo piano.";

ERR_30001.Diagnostic="La geometria del grezzo selezionata non giace sul piano di tornitura corrente.";
ERR_30001.Advice="\nSelezionare la geometria del grezzo che giace su questo piano.";
