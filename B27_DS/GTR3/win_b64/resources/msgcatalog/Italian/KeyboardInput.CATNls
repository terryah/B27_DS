//
// ------------------------------------------------------------
// |
// |  NLS Message Catalog
// |  Framework: CATIAApplicationFrame
// |  Module : Search.m
// |
// ------------------------------------------------------------
//

// ------------------------------------------------------------
// | Special characters for Keyboard Input                  
// ------------------------------------------------------------
//
//  character to surround special characters
Quote                  = "'";


// ------------------------------------------------------------
// | Special names for Keyboard Input                  
// ------------------------------------------------------------
// For the following translations, the names of the commands, as well as the name for 
// the shortcuts MUST BE distinctive from each other. 
//
// In case of a clash, choose the name of the shortcut the shortest possible to be 
// differentiable
// Priority rule: The shortcut for Command has the highest priority level:
//   command -> c
//   color   -> col
// Priority rule: The shortcut for Search is independant from others:
//   search -> s
//   set    -> s
//
// Example: If 2 commands' names are plane and planification
//          choose plane and plani for the 2 associate shortcuts

//  no default prefix
None                  = "nessuno";
//  last entered prefix
Last                  = "ultima immissione";

//  name to launch a command
Command               = "comando";
//  shortcut for this name
CommandShortcut       = "c";

//  name to launch a search through a favorite query
Favorite              = "preferiti";
//  shortcut for this name
FavoriteShortcut      = "f";

//  string to characterize the name of an object 
Name                  = "Nome";
//  shortcut for this string
NameShortcut          = "n";

//  string to characterize the name of an object as shown in the specification tree (graph)
NameInGraph           = "Nome nel grafo";
//  shortcut for this string
NameInGraphShortcut   = "q";

//  string to characterize the type of an object 
Type                  = "tipo";
//  shortcut for this string
TypeShortcut         = "t";

//  string to characterize the selection set of an object 
Set              = "Serie";
//  shortcut for this string
SetShortcut      = "r";

//  string to characterize the color of an object 
Color              = "Colore";
//  shortcut for this string
ColorShortcut      = "col";
//
ProblemInColorQuery = "Il valore </P1> non � corretto per l'attributo Colore.";

//  string to characterize the layer of an object 
Layer              = "Layer";
//  shortcut for this string
LayerShortcut      = "l";

//  string to characterize the dashed type of an object (line)
Dashed              = "Tratteggio";
//  shortcut for this string
DashedShortcut      = "g";
//
ProblemInDashedQuery = "Il valore </P1> non � corretto per l'attributo Tratteggiato.";

//  string to characterize the thickness of an object (line)
Weight              = "Peso";
//  shortcut for this string
WeightShortcut      = "p";
//
ProblemInWeightQuery = "Il valore </P1> non � corretto per l'attributo Peso.";

//  string to characterize the symbol type of an object (point)
Symbol              = "Simbolo";
//  shortcut for this string
SymbolShortcut      = "symb";
//
ProblemInWeightQuery = "Il valore </P1> non � corretto per l'attributo Peso.";

// string to characterize the visibility of an object
Visibility             = "Visibilit�";
//  shortcut for this string
VisibilityShortcut     = "vis";

// Note for translation:
// The following couple of values is related to the visibility attribute of an object (from a data point of view).
// An object which has the "Visible" attribute may be visible but will not if one of its "parents" object is not visible.
// In other terms, Visible means "visualizable" and Hidden means "not visualizable",
// but users can have recorded theses values in favorite queries so one cannot change them.
Visible                = "Visibile";
Hidden                 = "Nascosto";

// Note for translation:
// The following couple of values is related to the visibility of an object (from the display point of view).
// An object is told "Shown" if it is shown on the screen: the object as well as each of its parents have the "Visible" attribute.
// An object is told "Invisible" if it is not shown on the screen: the object or any of its parents have the "Hidden" attribute.
Shown                  = "Visualizzato";
Invisible              = "Invisibili";
//
ProblemInVisibilityQuery = "Il valore </P1> non � corretto per l'attributo Visibilit�.";


// ------------------------------------------------------------
// | Special names for the definition of the context,
// | for a Search carried out from the Keyboard Input                  
// ------------------------------------------------------------
// Rules: They are the same as previously, but the shortcuts can be a little longer
// ------   (ie, bot for to bottom)

//  string to characterize a Search context being everywhere 
Everywhere              = "Ovunque";
//  shortcut for this Search context - Everywhere
EverywhereShortcut      = "tutto";

//  string to characterize a Search context in the element 
InElement               = "In /P1";
DefaultInElement        = "Nell'elemento";
//  shortcut for this Search context - In the element
InElementShortcut       = "in";

//  string to characterize a Search context from the element to the bottom
FromElement             = "Da /P1 verso il basso";
DefaultFromElement      = "Dall'elemento verso il basso";
//  shortcut for this Search context - From Element
FromElementShortcut     = "da";

//  string to characterize a Search context being from the last scope 
FromLastScope           = "Dall'ultimo ambito";
//  shortcut for this Search context - From Last Scope
FromLastScopeShortcut   = "conserva";

//  string to characterize a Search context being from the selection 
FromSelection           = "Dalla selezione corrente";
//  shortcut for this Search context - From Selection
FromSelectionShortcut   = "sel";

//  string to characterize a Search context from the screen 
VisibleOnScreen         = "Visibile sullo schermo";
//  shortcut for this Search context - Visible on screen
VisibleOnScreenShortcut = "scr";

//  string to characterize a Search context in the elements already found
InList             = "Dai risultati della ricerca";


// ------------------------------------------------------------
// | Special names for context options,
// | for a Search carried out from the Keyboard Input                  
// ------------------------------------------------------------

//  string to characterize the "Include Topology" context option 
TopologyShortcut      = "topo";
//  string to characterize the "Published elements only" context option 
PublicationsShortcut  = "pub";
//  string to characterize the "Deep Search" context option 
DeepSearchShortcut    = "deep";


// --------------------------------------------------------------------
// | Names of boolean for a Search carried out from the Keyboard Input                  
// --------------------------------------------------------------------
True  = "VERO";
False = "FALSO";

//
// ------------------------------------------------------------
// |  Error Messages for  Power Input and Search command
// ------------------------------------------------------------
//
UnknownCommand     = "Comando sconosciuto: ";
//
UnavailableCommand = "Comando non disponibile: ";
//
UnknownWorkbench   = "Workbench sconosciuto: ";
//
UnknownType        = "Tipo sconosciuto: ";
//
UnknownAttribute   = "Attributo sconosciuto: ";
//
UnknownColor       = "Colore sconosciuto: ";
//
WrongColorNumber   = "Numero di colore errato: ";
//
WrongLayerNumber   = "Numero di layer errato: ";
//
SyntaxError        = "Errore di sintassi";
//
OddNumberOf        = "Numero dispari di: ";
//
MissingClosingParenthesis = "Una o pi� parentesi di chiusura mancanti.";
//
MissingOpeningParenthesis = "Una o pi� parentesi di chiusura mancanti.";
//
BeforeOpeningParenthesis = "Dopo una (, pu� esserci solo:\n\t\t ( oppure \n\t\tun operatore.";
//
AfterOpeningParenthesis = "Dopo una (, non pu� esserci:\n\t\tuna ), n� \n\t\tun operatore.";
//
BeforeClosingParenthesis = "Dopo una ), non pu� esserci una (. \nPrima di una ), non pu� esserci un operatore.";
//
AfterClosingParenthesis = "Dopo una ), pu� esserci solo:\n\t\tuna ), \n\t\tun non operatore oppure \n\t\tun separatore."; 
//
OperatorsPosition = "Gli operatori non possono trovarsi all'inizio o alla fine di una stringa.";
//
DoubleOperator = "Due operatori non possono essere affiancati.";
//
MissingOperator = "Questo tipo di interrogazione deve contenere un operatore.";
//
RefPRT = "L'elemento di riferimento deve essere in una parte meccanica.";
//
RefPBD = "L'elemento di riferimento deve essere in un Part body o in un body ibrido.";
//
RefGST = "L'elemento di riferimento deve essere in un gruppo geometrico.";
//
RefOGS = "L'elemento di riferimento deve essere in un gruppo geometrico ordinato.";

//
// Error Panel Title
PowerInputMessage  = "Messaggio di riga comandi";
//
NoAttributeOnType = "Il tipo </P1> non ha attributi specifici.";
//
NoSuchAttributeOnType = "L'attributo </P1> non esiste su oggetti di tipo </P2>.";
//
ErrorIn = "La seguente stringa non � corretta: </P1>";
//
IntegerAttribute = 
"L'attributo � di tipo intero. Rimuovere le specifiche
di unit� o di decimali nella stringa </P1>.";
//
RealAttribute = 
"L'attributo � di tipo reale. Rimuovere le specifiche di
unit� nella stringa </P1>.";
//
BooleanAttribute = 
"L'attributo � di tipo booleano.
I valori consentiti sono: /P1, /P2, /P3, /P4.";
//
DimensionedAttribute = 
"L'attributo � di tipo quotato.
� necessario specificare un'unit�.";
//
WrongToleranceRange = 
"L'intervallo di tolleranza non � preciso.
Il primo valore deve essere rigorosamente pi� basso del secondo.";
//
WrongToleranceOperator = 
"Nella ricerca con tolleranza,
i soli operatori validi sono = e !=.";
//
CharacterString = "stringa di caratteri";
BooleanType     = "booleana";
EnumerationType = "numerazione";
WrongOperatorForAttribute = 
"L'attributo </P1> � di tipo /P2. \nper cui gli unici operatori validi sono = e !=.";
//
UserAttributeWarning = 
"A meno che l'attributo </P1> non sia definito dall'utente,
l'interrogazione non � corretta.";
//
NoListOfPossibleValuesForEnumeration = "Impossibile richiamare l'elenco dei valori possibili \nper l'attributo di tipo numerazione </P1>.";
InvalidValueForEnumeration = "Il valore </P1> immesso per l'attributo di tipo \nnumerazione </P2> non � valido.";


// ------------------------------------------------------------
// | Special names for Tools/Options,
// ------------------------------------------------------------

//  example : "c: as command:"
As                    = "come";
