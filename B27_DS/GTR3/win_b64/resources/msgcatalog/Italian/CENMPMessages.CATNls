// =====================================================================
//
// MIK common NLS-File
//
// Syntax:
//   <ModuleId>_<MsgId>.<..> = "Text";
//   where 
//     ModuleId: MIK     -> common MIK Messages (MIK1 + MIK CE)
//               MIK1    -> only MIK1 Messages
//               MIKCE   -> only MIK CE Messages
//     MsgId   : PB      -> Progressbar Text
//               EMxxxx  -> Error/Message + Number
//
// Limitations:
//    # max. nb. of parameters supported: /P1 .. /P25
//
// =====================================================================

ERR_39999.Request    = "Creazione dei dati di simulazione";
ERR_39999.Diagnostic = "/p1";
ERR_39999.Advice     = "Verificare il processo. ";

// ==================================================================
// Common Messages
// ==================================================================

MIK1_Version = "Versione (MIK1):";

MIKCE_Version = "Versione (MIK CE):";

// ==================================================================
// Progressbar
// ==================================================================

MIK_PB0000 = "Inizializzazione...";

// ==================================================================
// Information messages (10000..19999)
// ==================================================================

MIK1_EM10000.iSev = "1";
MIK1_EM10000.sMsg = "Informazioni sull'applicazione";
MIK1_EM10000.lMsg = "Esito positivo: MIK1 non ha restituito errori o avvertenze.";

MIKCE_EM10000.iSev = "1";
MIKCE_EM10000.sMsg = "Informazioni sull'applicazione";
MIKCE_EM10000.lMsg = "Esito positivo: MIK CE non ha restituito errori o avvertenze.";

// ==================================================================
// Warning messages (30000..39999)
// ==================================================================

MIK_EM30100.iSev = "3";
MIK_EM30100.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30100.lMsg = "La definizione del nome del modulo nel file di parametri (/P1) non � supportata per MIK.\nLa definizione viene ignorata.";

MIK_EM30101.iSev = "3";
MIK_EM30101.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30101.lMsg = "La definizione del nome della voce cPost nel file di parametri (/P1) non � supportata per MIK.\nLa definizione viene ignorata.";

MIK_EM30102.iSev = "3";
MIK_EM30102.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30102.lMsg = "Opzione non valida per #MIK_DUMP_MODE nel file di parametri: \n/P1\nRiga: /P2.";

MIK_EM30103.iSev = "3";
MIK_EM30103.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30103.lMsg = "*********************************************************\n
                     Versione cPost non valida:\n
                     /P1\n
                     ...Il programma potrebbe bloccarsi\n
                     *********************************************************";

MIK_EM30104.iSev = "3";
MIK_EM30104.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30104.lMsg = "Avvio a caldo non disponibile per mce.";

MIK_EM30105.iSev = "3";
MIK_EM30105.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30105.lMsg = "Tentativo di ottenere la voce cPostGetMAPtable dalla libreria cPost (<V1R6 SP2).";

MIK_EM30106.iSev = "3";
MIK_EM30106.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30106.lMsg = "Libreria mce non valida, i dati della simulazione verranno creati da cPost.";

MIK_EM30107.iSev = "3";
MIK_EM30107.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30107.lMsg = "Impossibile leggere le impostazioni predefinite dal file di parametri.\n -> Impostazione dei valori predefiniti.";

MIK_EM30108.iSev = "3";
MIK_EM30108.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30108.lMsg = "Impossibile inizializzare la gestione degli errori cPost.\n-> Consultare il file di prospetti di cPost per i messaggi runtime di cPost.";

MIK_EM30109.iSev = "3";
MIK_EM30109.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30109.lMsg = "Dati dell'avvio a caldo non memorizzati per l'attivit� /P1.\n";

MIK_EM30110.iSev = "3";
MIK_EM30110.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30110.lMsg = "Gestione degli errori non disponibile per mce.";

MIK_EM30111.iSev = "3";
MIK_EM30111.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30111.lMsg = "Impossibile ottenere Zero Tool Mount Location da DNBIVncMachineKinDetails (SETUPD=0/0/0).";

MIK_EM30112.iSev = "3";
MIK_EM30112.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30112.lMsg = "Impossibile ottenere DNBIVncMachineKinDetails da NCMachine (SETUPD=0/0/0).";

MIK_EM30113.iSev = "3";
MIK_EM30113.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30113.lMsg = "Prove di interruzione per chiudere la libreria /P1 dopo 21266 errori.";

MIK_EM30114.iSev = "3";
MIK_EM30114.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30114.lMsg = "Impossibile creare il file ISO. Verificare le impostazioni per la directory NCCode.\n Directory: /P1";

MIK_EM30115.iSev = "3";
MIK_EM30115.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30115.lMsg = "Impossibile creare il collegamento al file ISO sull'attivit� /P1.";

MIK_EM30116.iSev = "3";
MIK_EM30116.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30116.lMsg = "Impossibile creare il file ISO. Verificare le impostazioni per la directory NCCode.";

MIK_EM30117.iSev = "3";
MIK_EM30117.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30117.lMsg = "Impossibile aprire il file di parametri: /P1.\nCaricamento file dalla directory di avvio di lavorazione.";

MIK_EM30118.iSev = "3";
MIK_EM30118.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30118.lMsg = "Impossibile ottenere Current Tool Mount Location da DNBIVncMachineKinDetails (set=0/0/0).";


// MIK CE
MIK_EM30201.iSev = "3";
MIK_EM30201.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30201.lMsg = "ToolNumber/CorrectorRegister non validi (/P1,/P2) restituiti da mce. AssociateToolToDOFTargets() ignorato.";

MIK_EM30202.iSev = "3";
MIK_EM30202.sMsg = "Avvertenza relativa all'applicazione";
MIK_EM30202.lMsg = "Avvio non valido-, Endnumber (/P1,/P2) restituito da mce. AssociateToolToDOFTargets() ignorato.";

// ==================================================================
// Error messages (50000..59999)
// 
// ==================================================================
//MIK_EM50000.iSev = "5";
//MIK_EM50000.sMsg = "Internal Error";
//MIK_EM50000.lMsg = "RESERVED FOR INTERNAL ERROR MESSAGES";

MIK_EM50100.iSev = "5";
MIK_EM50100.sMsg = "Errore interno";
MIK_EM50100.lMsg = "MIKInit richiamato in modalit� non supportata (=/P1). ";

MIK_EM50101.iSev = "5";
MIK_EM50101.sMsg = "Errore interno";
MIK_EM50101.lMsg = "mparams non inizializzato.";

MIK_EM50102.iSev = "5";
MIK_EM50102.sMsg = "Errore interno della libreria cPost";
MIK_EM50102.lMsg = "Punto di accesso /P1 non trovato nella libreria cPost /P2.";

MIK_EM50103.iSev = "5";
MIK_EM50103.sMsg = "Errore interno della libreria mce";
MIK_EM50103.lMsg = "Punto di accesso /P1 non trovato nella libreria mce /P2.";

MIK_EM50104.iSev = "5";
MIK_EM50104.sMsg = "Errore interno";
MIK_EM50104.lMsg = "File di parametri non valido per l'uso con MIK:\n/P1\nDefinizione mancante di: /P2";

MIK_EM50105.iSev = "5";
MIK_EM50105.sMsg = "Errore dell'applicazione";
MIK_EM50105.lMsg = "Impossibile aprire il file di parametri: /P1.\nCaricamento file dalla directory di avvio di lavorazione.";

MIK_EM50106.iSev = "5";
MIK_EM50106.sMsg = "Errore dell'applicazione";
MIK_EM50106.lMsg = "La voce cPost /P2 � stata restituita con codice di errore = /P1.";

MIK_EM50107.iSev = "5";
MIK_EM50107.sMsg = "Errore dell'applicazione";
MIK_EM50107.lMsg = "La voce mce /P2 � stata restituita con codice di errore = /P1.";

MIK_EM50108.iSev = "5";
MIK_EM50108.sMsg = "Errore dell'applicazione";
MIK_EM50108.lMsg = "I dati della simulazione non sono memorizzati per l'attivit� /P1.";

MIK_EM50109.iSev = "5";
MIK_EM50109.sMsg = "Errore dell'applicazione";
MIK_EM50109.lMsg = "La creazione dei dati della simulazione di cPost per l'attivit� /P2 ha restituito il codice di errore = /P1.";

MIK_EM50110.iSev = "5";
MIK_EM50110.sMsg = "Errore dell'applicazione";
MIK_EM50110.lMsg = "La creazione dei dati della simulazione di mce per l'attivit� /P2 ha restituito il codice di errore = /P1.";

MIK_EM50111.iSev = "5";
MIK_EM50111.sMsg = "Errore dell'applicazione";
MIK_EM50111.lMsg = "Sezione mce mancante nel file di parametri.";

MIK_EM50112.iSev = "5";
MIK_EM50112.sMsg = "Errore dell'applicazione";
MIK_EM50112.lMsg = "Impossibile leggere l'unit� corrente (mm/pollici).";

MIK_EM50113.iSev = "5";
MIK_EM50113.sMsg = "Errore dell'applicazione";
MIK_EM50113.lMsg = "Inizializzazione di mce non riuscita per MIK.";

MIK_EM50114.iSev = "5";
MIK_EM50114.sMsg = "Errore dell'applicazione";
MIK_EM50114.lMsg = "Impossibile aprire il file di parametri dalla directory di avvio di lavorazione: /P1.";

MIK_EM50115.iSev = "5";
MIK_EM50115.sMsg = "Errore dell'applicazione";
MIK_EM50115.lMsg = "MIK1 non � riuscito ad ottenere il valore per la parola chiave del file MfgBatch FT05 /P1.";

MIK_EM50116.iSev = "5";
MIK_EM50116.sMsg = "Errore dell'applicazione";
MIK_EM50116.lMsg = "La chiamata di creazione MIK APT di LaunchCATMFG non � riuscito con rc = /P1.";

MIK_EM50117.iSev = "5";
MIK_EM50117.sMsg = "Errore dell'applicazione";
MIK_EM50117.lMsg = "Impossibile ottenere MOAPTIndexes per l'operazione: /P1.";

MIK_EM50118.iSev = "5";
MIK_EM50118.sMsg = "Errore dell'applicazione";
MIK_EM50118.lMsg = "Impossibile aprire il file MfgBatch APT per la lettura: /P1.";

MIK_EM50119.iSev = "5";
MIK_EM50119.sMsg = "Errore dell'applicazione";
MIK_EM50119.lMsg = "Modalit� simulazione non valida: /P1.";


// MIK CE
MIK_EM50200.iSev = "5";
MIK_EM50200.sMsg = "Errore dell'applicazione";
MIK_EM50200.lMsg = "Nessun file Post & Controller selezionato.";

MIK_EM50201.iSev = "5";
MIK_EM50201.sMsg = "Errore dell'applicazione";
MIK_EM50201.lMsg = "Nessun Machine Controller Emulator definito nel file di definizione del controllore.";

MIK_EM50202.iSev = "5";
MIK_EM50202.sMsg = "Errore interno della libreria mce";
MIK_EM50202.lMsg = "mce non valida per la simulazione.";

MIK_EM50203.iSev = "5";
MIK_EM50203.sMsg = "Errore dell'applicazione";
MIK_EM50203.lMsg = "Chiamata di MIKCreateDOFTable() senza chiamata positiva di MIKInit().";

MIK_EM50204.iSev = "5";
MIK_EM50204.sMsg = "Errore dell'applicazione";
MIK_EM50204.lMsg = "Inizializzazione sessione non riuscita per: /P1.";

// common
MIK_EM59100.iSev = "5";
MIK_EM59100.sMsg = "Errore dell'applicazione";
MIK_EM59100.lMsg = "Eccezione manipolatore NULL per: /P1 (in: /P2).";

MIK_EM59998.iSev = "5";
MIK_EM59998.sMsg = "Errore interno";
MIK_EM59998.lMsg = "Memoria insufficiente: assegnazione della memoria non riuscita.";

MIK_EM59999.iSev = "5";
MIK_EM59999.sMsg = "Errore interno";
MIK_EM59999.lMsg = "Errore interno, il programma verr� interrotto.";

