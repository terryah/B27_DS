// COPYRIGHT DASSAULT SYSTEMES 2001
//===========================================================================
//
// CATRmaClearCoatGeneratorDlg (English)
//
//===========================================================================
// 
//
Title = "Generatore di tessitura ClearCoat 360";

small  = "Piccola (128 x 128 pixel)";
medium = "Media (256 x 256 pixel)";
large  = "Grande (512 x 512 pixel)"; 

sphere         = "Sfera";
roundplatelet  = "Placchetta circolare";
squareplatelet = "Placchetta quadrata";

paint   = "Verniciatura a pi� strati";
mirror  = "Specchio";
normal  = "Specchio sferico";
phong   = "Effetto Phong";

one = "1";
two = "2";
four = "4";
eight = "8";

upPicture    = "upEng.bmp";
downPicture  = "downEng.bmp";
frontPicture = "frontEng.bmp";
backPicture  = "backEng.bmp";
leftPicture  = "leftEng.bmp";
rightPicture = "rightEng.bmp";

frameOption.fileLabel.Title = "Salva con nome ";

framePicture.frontViewer.Help = "Fare clic per fissare l'immagine frontale";
framePicture.frontViewer.ShortHelp = "Fare clic per fissare l'immagine frontale";
framePicture.frontViewer.LongHelp = "Fare clic per fissare l'immagine frontale";

framePicture.backViewer.Help = "Fare clic per fissare l'immagine di sfondo";
framePicture.backViewer.ShortHelp = "Fare clic per fissare l'immagine di sfondo";
framePicture.backViewer.LongHelp = "Fare clic per fissare l'immagine di sfondo";

framePicture.leftViewer.Help = "Fare clic per fissare l'immagine di sinistra";
framePicture.leftViewer.ShortHelp = "Fare clic per fissare l'immagine di sinistra";
framePicture.leftViewer.LongHelp = "Fare clic per fissare l'immagine di sinistra";

framePicture.rightViewer.Help = "Fare clic per fissare l'immagine di destra";
framePicture.rightViewer.ShortHelp = "Fare clic per fissare l'immagine di destra";
framePicture.rightViewer.LongHelp = "Fare clic per fissare l'immagine di destra";

framePicture.upViewer.Help = "Fare clic per fissare l'immagine in alto";
framePicture.upViewer.ShortHelp = "Fare clic per fissare l'immagine in alto";
framePicture.upViewer.LongHelp = "Fare clic per fissare l'immagine in alto";

framePicture.downViewer.Help = "Fare clic per fissare l'immagine in basso";
framePicture.downViewer.ShortHelp = "Fare clic per fissare l'immagine in basso";
framePicture.downViewer.LongHelp = "Fare clic per fissare l'immagine in basso";

frameOption.fileButton.Help = "Sfoglia directory";
frameOption.fileButton.ShortHelp = "Sfoglia directory";
frameOption.fileButton.LongHelp =
"Definisce il nome file dell'immagine salvata
sfogliando le directory.";

// Tabs
tabContainer.qualityTabPage.Title = "Qualit�";
tabContainer.layer0TabPage.Title = "Layer ClearCoat";
tabContainer.layer1TabPage.Title = "Layer substrato";
tabContainer.phongTabPage.Title = "Parametri Phong";

// Type
frameType.typeLabel.Title = "Tipo tessitura ";

// Quality
tabContainer.qualityTabPage.qualityFrame.qualitySepFrame.qualitySepLabel.Title = "Qualit� dell'emissione";

tabContainer.qualityTabPage.qualityFrame.samplingLabel.Title = "Supercampione ";
tabContainer.qualityTabPage.qualityFrame.samplingCombo.Help = "Supercampione";
tabContainer.qualityTabPage.qualityFrame.samplingCombo.ShortHelp = "Supercampione";
tabContainer.qualityTabPage.qualityFrame.samplingCombo.LongHelp =
"Modula il supercampione.";

tabContainer.qualityTabPage.qualityFrame.nbRaysLabel.Title = "Numero di raggi ";
tabContainer.qualityTabPage.qualityFrame.nbRaysSpinner.Help = "Numero di raggi";
tabContainer.qualityTabPage.qualityFrame.nbRaysSpinner.ShortHelp = "Numero di raggi";
tabContainer.qualityTabPage.qualityFrame.nbRaysSpinner.LongHelp =
"Definisce la massima profondit� dell'albero logico dei raggi.";

tabContainer.qualityTabPage.qualityFrame.accuracySepFrame.accuracySepLabel.Title = "Precisione del punto di vista";

tabContainer.qualityTabPage.qualityFrame.accTopLabel.Title = "Superiore ";
tabContainer.qualityTabPage.qualityFrame.accTopSpinner.Help = "Precisione del punto di vista superiore";
tabContainer.qualityTabPage.qualityFrame.accTopSpinner.ShortHelp = "Precisione del punto di vista superiore";
tabContainer.qualityTabPage.qualityFrame.accTopSpinner.LongHelp =
"Modula la precisione del punto di vista superiore.";

tabContainer.qualityTabPage.qualityFrame.accMiddleLabel.Title = "Centrale ";
tabContainer.qualityTabPage.qualityFrame.accMiddleSpinner.Help = "Precisione del punto di vista centrale";
tabContainer.qualityTabPage.qualityFrame.accMiddleSpinner.ShortHelp = "Precisione del punto di vista centrale";
tabContainer.qualityTabPage.qualityFrame.accMiddleSpinner.LongHelp =
"Modula la precisione del punto di vista centrale.";

tabContainer.qualityTabPage.qualityFrame.accBottomLabel.Title = "Inferiore ";
tabContainer.qualityTabPage.qualityFrame.accBottomSpinner.Help = "Precisione del punto di vista inferiore";
tabContainer.qualityTabPage.qualityFrame.accBottomSpinner.ShortHelp = "Precisione del punto di vista inferiore";
tabContainer.qualityTabPage.qualityFrame.accBottomSpinner.LongHelp =
"Modula la precisione del punto di vista inferiore.";

tabContainer.qualityTabPage.qualityFrame.labelImageSize.Title = "Dimensione immagine ";

// Layer0
tabContainer.layer0TabPage.layer0Frame.layer0ShapeLabel.Title = "Forma delle particelle ";
tabContainer.layer0TabPage.layer0Frame.layer0ShapeCombo.Help = "Forma delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0ShapeCombo.ShortHelp = "Forma delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0ShapeCombo.LongHelp =
"Definisce la forma delle particelle.";

tabContainer.layer0TabPage.layer0Frame.layer0SizeLabel.Title = "Dimensione delle particelle ";
tabContainer.layer0TabPage.layer0Frame.layer0SizeSpinner.Help = "Dimensione delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0SizeSpinner.ShortHelp = "Dimensione delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0SizeSpinner.LongHelp =
"Modula la dimensione delle particelle.";

tabContainer.layer0TabPage.layer0Frame.layer0DensityLabel.Title = "Densit� delle particelle ";
tabContainer.layer0TabPage.layer0Frame.layer0DensitySpinner.Help = "Densit� delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0DensitySpinner.ShortHelp = "Densit� delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0DensitySpinner.LongHelp =
"Modula la densit� delle particelle.";

tabContainer.layer0TabPage.layer0Frame.layer0DepthLabel.Title = "Profondit� del layer ";
tabContainer.layer0TabPage.layer0Frame.layer0DepthSpinner.Help = "Profondit� del layer";
tabContainer.layer0TabPage.layer0Frame.layer0DepthSpinner.ShortHelp = "Profondit� del layer";
tabContainer.layer0TabPage.layer0Frame.layer0DepthSpinner.LongHelp =
"Modula la profondit� del layer.";

tabContainer.layer0TabPage.layer0Frame.layer0ColorLabel.Title = "Colore delle particelle ";
tabContainer.layer0TabPage.layer0Frame.layer0ColorSpinner.Help = "Colore delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0ColorSpinner.ShortHelp = "Colore delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0ColorSpinner.LongHelp =
"Modula il colore delle particelle.";

tabContainer.layer0TabPage.layer0Frame.layer0RefractionLabel.Title = "Rifrazione delle particelle ";
tabContainer.layer0TabPage.layer0Frame.layer0RefractionSpinner.Help = "Rifrazione delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0RefractionSpinner.ShortHelp = "Rifrazione delle particelle";
tabContainer.layer0TabPage.layer0Frame.layer0RefractionSpinner.LongHelp =
"Modula la rifrazione delle particelle.";

tabContainer.layer0TabPage.layer0Frame.layer0Refraction2Label.Title = "Rifrazione del layer ";
tabContainer.layer0TabPage.layer0Frame.layer0Refraction2Spinner.Help = "Rifrazione del layer";
tabContainer.layer0TabPage.layer0Frame.layer0Refraction2Spinner.ShortHelp = "Rifrazione del layer";
tabContainer.layer0TabPage.layer0Frame.layer0Refraction2Spinner.LongHelp =
"Modula la rifrazione del layer.";

//Layer1
tabContainer.layer1TabPage.layer1Frame.layer1ShapeLabel.Title = "Forma delle particelle ";
tabContainer.layer1TabPage.layer1Frame.layer1ShapeCombo.Help = "Forma delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1ShapeCombo.ShortHelp = "Forma delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1ShapeCombo.LongHelp =
"Definisce la forma delle particelle.";

tabContainer.layer1TabPage.layer1Frame.layer1SizeLabel.Title = "Dimensione delle particelle ";
tabContainer.layer1TabPage.layer1Frame.layer1SizeSpinner.Help = "Dimensione delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1SizeSpinner.ShortHelp = "Dimensione delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1SizeSpinner.LongHelp =
"Modula la dimensione delle particelle.";

tabContainer.layer1TabPage.layer1Frame.layer1DensityLabel.Title = "Densit� delle particelle ";
tabContainer.layer1TabPage.layer1Frame.layer1DensitySpinner.Help = "Densit� delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1DensitySpinner.ShortHelp = "Densit� delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1DensitySpinner.LongHelp =
"Modula la densit� delle particelle.";

tabContainer.layer1TabPage.layer1Frame.layer1DepthLabel.Title = "Profondit� del layer ";
tabContainer.layer1TabPage.layer1Frame.layer1DepthSpinner.Help = "Profondit� del layer";
tabContainer.layer1TabPage.layer1Frame.layer1DepthSpinner.ShortHelp = "Profondit� del layer";
tabContainer.layer1TabPage.layer1Frame.layer1DepthSpinner.LongHelp =
"Modula la profondit� del layer.";

tabContainer.layer1TabPage.layer1Frame.layer1ColorLabel.Title = "Colore delle particelle ";
tabContainer.layer1TabPage.layer1Frame.layer1ColorSpinner.Help = "Colore delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1ColorSpinner.ShortHelp = "Colore delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1ColorSpinner.LongHelp =
"Modula il colore delle particelle.";

tabContainer.layer1TabPage.layer1Frame.layer1RefractionLabel.Title = "Rifrazione delle particelle ";
tabContainer.layer1TabPage.layer1Frame.layer1RefractionSpinner.Help = "Rifrazione delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1RefractionSpinner.ShortHelp = "Rifrazione delle particelle";
tabContainer.layer1TabPage.layer1Frame.layer1RefractionSpinner.LongHelp =
"Modula la rifrazione delle particelle.";

tabContainer.layer1TabPage.layer1Frame.layer1Refraction2Label.Title = "Rifrazione del layer ";
tabContainer.layer1TabPage.layer1Frame.layer1Refraction2Spinner.Help = "Rifrazione del layer";
tabContainer.layer1TabPage.layer1Frame.layer1Refraction2Spinner.ShortHelp = "Rifrazione del layer";
tabContainer.layer1TabPage.layer1Frame.layer1Refraction2Spinner.LongHelp =
"Modula la rifrazione del layer.";

//Phong
tabContainer.phongTabPage.phongFrame.phongPLabel.Title = "Rugosit� ";
tabContainer.phongTabPage.phongFrame.phongPSlider.Help = "Rugosit�";
tabContainer.phongTabPage.phongFrame.phongPSlider.ShortHelp = "Modifica l'esponente speculare";
tabContainer.phongTabPage.phongFrame.phongPSlider.LongHelp =
"Modula la dimensione dell'area di luminosit� del materiale.";

tabContainer.phongTabPage.phongFrame.phongRefractionLabel.Title = "Fattore di rifrazione ";
tabContainer.phongTabPage.phongFrame.phongRefractionSlider.Help = "Fattore di rifrazione";
tabContainer.phongTabPage.phongFrame.phongRefractionSlider.ShortHelp = "Fattore di rifrazione";
tabContainer.phongTabPage.phongFrame.phongRefractionSlider.LongHelp =
"Modula la rifrazione.";

tabContainer.phongTabPage.phongFrame.phongLLabel.Title = "Direzione della luce ";

tabContainer.phongTabPage.phongFrame.phongLXLabel.Title = " X ";
tabContainer.phongTabPage.phongFrame.phongLXSpinner.Help = "Coordinata X";
tabContainer.phongTabPage.phongFrame.phongLXSpinner.ShortHelp = "Coordinata X";
tabContainer.phongTabPage.phongFrame.phongLXSpinner.LongHelp =
"Modifica la direzione della luce lungo l'asse X.";

tabContainer.phongTabPage.phongFrame.phongLYLabel.Title = " Y ";
tabContainer.phongTabPage.phongFrame.phongLYSpinner.Help = "Coordinata Y";
tabContainer.phongTabPage.phongFrame.phongLYSpinner.ShortHelp = "Coordinata Y";
tabContainer.phongTabPage.phongFrame.phongLYSpinner.LongHelp =
"Modifica la direzione della luce lungo l'asse Y.";

tabContainer.phongTabPage.phongFrame.phongLZLabel.Title = " Z ";
tabContainer.phongTabPage.phongFrame.phongLZSpinner.Help = "Coordinata Z";
tabContainer.phongTabPage.phongFrame.phongLZSpinner.ShortHelp = "Coordinata Z";
tabContainer.phongTabPage.phongFrame.phongLZSpinner.LongHelp =
"Modifica la direzione della luce lungo l'asse Z.";

tabContainer.phongTabPage.phongFrame.phongKALabel.Title = "Ambiente ";
tabContainer.phongTabPage.phongFrame.phongKASlider.Help = "Colore ambiente";
tabContainer.phongTabPage.phongFrame.phongKASlider.ShortHelp = "Ambiente";
tabContainer.phongTabPage.phongFrame.phongKASlider.LongHelp =
"Modula il colore dell'ambiente.";

tabContainer.phongTabPage.phongFrame.phongKDLabel.Title = "Diffuso ";
tabContainer.phongTabPage.phongFrame.phongKDSlider.Help = "Colore diffuso";
tabContainer.phongTabPage.phongFrame.phongKDSlider.ShortHelp = "Diffuso";
tabContainer.phongTabPage.phongFrame.phongKDSlider.LongHelp =
"Modula il colore diffuso.";

tabContainer.phongTabPage.phongFrame.phongKSLabel.Title = "Speculare ";
tabContainer.phongTabPage.phongFrame.phongKSSlider.Help = "Colore speculare";
tabContainer.phongTabPage.phongFrame.phongKSSlider.ShortHelp = "Speculare";
tabContainer.phongTabPage.phongFrame.phongKSSlider.LongHelp =
"Modula il colore speculare.";

tabContainer.phongTabPage.phongFrame.phongColorLabel.Title = "Colore della luce ";
tabContainer.phongTabPage.phongFrame.phongColorSlider.Help = "Colore della luce";
tabContainer.phongTabPage.phongFrame.phongColorSlider.ShortHelp = "Colore della luce";
tabContainer.phongTabPage.phongFrame.phongColorSlider.LongHelp =
"Modula il colore della luce.";

