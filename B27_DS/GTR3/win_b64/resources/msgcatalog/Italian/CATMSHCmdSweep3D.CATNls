//
// ---------------------------
// Sweep 3D command
// ---------------------------
//
CATMSHCmdSweep3D.UndoTitle="3D sweep";
CATMSHCmdSweep3D.Sweep3DDlg.Title="3D sweep";

CATMSHCmdSweep3D.Sweep3DDlg.BClose="Annulla";

CATMSHCmdSweep3D.SelectVolume.Message="Selezionare il volume per la mesh.";
CATMSHCmdSweep3D.SelectGeometry.Message="Selezionare i limiti di sweep del volume.";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.Title="Geometria";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.LimitsFrame.Title="Limiti di sweep ";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.LimitsFrame.BottomLbl.Title="Basso: ";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.LimitsFrame.TopLbl.Title="Alto: ";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.Title="Parametri guida";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.AngleEditorLbl.Title="Angolo: ";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.GuidesLbl.Title="Guide: ";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.ComputeGuides.Title="Calcola guide";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.Title="Mesh";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.Title="Cattura";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.CaptureLbl.Title="Tolleranza: ";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.CondensationCheck.Title="Condensazione";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.Title="Distribuzione";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.DistribLbl.Title="Tipo: ";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.NbLayersLbl.Title="Numero di layer: ";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.RatioLbl.Title="Rapporto di dimensione: ";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.SymmetryCheck.Title="Simmetria";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.ElementFrame.Title="Tipo di elemento";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.ElementFrame.RBLinear.Title="Lineare";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.ElementFrame.RBParabolic.Title="Parabolico";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.SmoothFrame.Title="Lisciatura";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.SmoothFrame.SmoothCheck.Title="Mesh interna e superiore";

CATMSHCmdSweep3D.Sweep3DDlg.NoSelection="Nessuna faccia selezionata";
CATMSHCmdSweep3D.Sweep3DDlg.NoGuides="Nessuna guida calcolata";

CATMSHCmdSweep3D.Sweep3DDlg.Uniform="Uniforme";
CATMSHCmdSweep3D.Sweep3DDlg.Arithmetic="Aritmetica";
CATMSHCmdSweep3D.Sweep3DDlg.Geometric="Geometria";

CATMSHCmdSweep3D.Sweep3DDlg.ErrorCoherence1.Title="Avvertenza";
CATMSHCmdSweep3D.Sweep3DDlg.ErrorCoherence1.Text="Il numero di layer deve essere positivo.";

CATMSHCmdSweep3D.Sweep3DDlg.ErrorCoherence2.Title="Avvertenza";
CATMSHCmdSweep3D.Sweep3DDlg.ErrorCoherence2.Text="Il rapporto della dimensione deve essere un valore positivo.";

CATMSHCmdSweep3D.Geometry.UndoTitle="Seleziona geometria";
CATMSHCmdSweep3D.BottomSel.UndoTitle="Seleziona il limite di sweep (basso)";
CATMSHCmdSweep3D.TopSel.UndoTitle="Seleziona il limite di sweep (alto)";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.Remove.Title="Rimuovi";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.Remove.ShortHelp="Rimuovi";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.Remove.LongHelp="Rimuove le guide calcolate.";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.Compute.Title="Calcola";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.Compute.ShortHelp="Calcola";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.Compute.LongHelp="Calcola le guide.";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.GuideButton.Title="Imponi guida";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.GuideButton.ShortHelp="Imponi guida";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.GuideButton.LongHelp="Crea una specifica locale per imporre gli spigoli selezionati nel calcolo della guida.";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.GuideButton.Help="Crea una specifica locale per imporre gli spigoli selezionati nel calcolo della guida";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.NoGuideButton.Title="Escludi guida";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.NoGuideButton.ShortHelp="Escludi guida";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.NoGuideButton.LongHelp="Crea una specifica locale per escludere gli spigoli selezionati nel calcolo della guida.";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.GuidesFrame.NoGuideButton.Help="Crea una specifica locale per escludere gli spigoli selezionati nel calcolo della guida";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.Check.Title="Anteprima";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.Check.ShortHelp="Anteprima";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.Check.LongHelp="Anteprima della cattura della mesh sweep";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.Check.Help="Anteprima della cattura della mesh sweep";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.InitToleranceBtn.Title="Inizializza";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.InitToleranceBtn.ShortHelp="Inizializza la tolleranza di condensazione con la dimensione dello spigolo pi� piccolo";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.InitToleranceBtn.LongHelp="Inizializza la tolleranza di condensazione con la dimensione dello spigolo pi� piccolo";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.CaptureFrame.InitToleranceBtn.Help="Inizializza la tolleranza di condensazione con la dimensione dello spigolo pi� piccolo.";

nbfaces = "/P1 Facce";
oneface = "/P1 Faccia";

nbguides = "/P1 Guide";
oneguide = "/P1 Guida";


Geometries2D	= "/P1 Facce";
Geometry2D		= "/P1 Faccia";
Feature			= "/P1 Feature";
Features		= "/P1 Feature";
Set				= "/P1 Gruppo geometrico";
Sets			= "/P1 Gruppi geometrici";
NoSelection		= "Nessuna selezione";

CaptureCountTitle	="Avvertenza";
CaptureNoCount		="Non � stata trovata nessuna cattura della mesh laterale.";
CaptureManyCount	="� stata trovata pi� di una cattura della mesh laterale: \n /P1.";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.LimitsFrame.GeomLabel.Title="Basso";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.GeometryTab.GeometryFrame.LimitsFrame.GeomLabel1.Title="Alto";
CATMSHCmdSweep3D.GeometrySelDlg.Title="Selettore di facce";

CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.Count.Title="Numero di catture";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.Count.Help="Imposta il numero di spigoli catturati.";
CATMSHCmdSweep3D.Sweep3DDlg.SweepContainer.MeshTab.MeshFrame.DistribFrame.Count.LongHelp="Imposta il numero di spigoli catturati.";


