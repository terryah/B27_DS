//
// ---------------------------
// Maillauto global parameters dialog
// ---------------------------
//

Title="Parametri globali";
Help="Imposta i parametri globali";
ShortHelp="Imposta i parametri globali";
LongHelp="Imposta i parametri globali della parte mesh";

TriaFrame.TriaTabContainer.GeomTabTria.Title="Geometria";
TriaFrame.TriaTabContainer.MeshTabTria.Title="Mesh";

QuadFrame.QuadTabContainer.GeomTabQuad.Title="Geometria";
QuadFrame.QuadTabContainer.MeshTabQuad.Title="Mesh";

MethodFrame.MethodLabel.Title = "Tipo di mesh  ";
MethodFrame.MethodLabel.LongHelp = "Imposta il tipo di mesh su superficie.";

MethodFrame.Button1.Title = "Triangolo frontale";
MethodFrame.Button1.Help = "Imposta il metodo del triangolo frontale";
MethodFrame.Button1.ShortHelp = "Imposta il metodo del triangolo frontale";
MethodFrame.Button1.LongHelp = "Imposta il metodo del triangolo frontale.";

MethodFrame.Button2.Title = "Quadrangolo frontale";
MethodFrame.Button2.Help = "Imposta il metodo del quadrangolo frontale";
MethodFrame.Button2.ShortHelp = "Imposta il metodo del quadrangolo frontale";
MethodFrame.Button2.LongHelp = "Imposta il metodo del triangolo frontale.";

elementFrame.elementLabel.Title = "Tipo di elemento  ";
elementFrame.elementLabel.LongHelp = "Imposta il tipo di elemento per la mesh su superficie.";

elementFrame.RBLinear.Title="Lineare";
elementFrame.RBLinear.Help="Utilizza il tipo di elemento lineare";
elementFrame.RBLinear.ShortHelp="Utilizza il tipo di elemento lineare";
elementFrame.RBLinear.RBLinear.LongHelp="Utilizza il tipo di elemento lineare.";

elementFrame.RBParabolic.Title="Parabolico";
elementFrame.RBParabolic.Help="Utilizza il tipo di elemento parabolico";
elementFrame.RBParabolic.ShortHelp="Utilizza il tipo di elemento parabolico";
elementFrame.RBParabolic.LongHelp="Utilizza il tipo di elemento parabolico.";

GeometrySelDlg.Title="Selezione della geometria";
CATMSHDlgMtoParams.GeometrySelDlg.Title="Selezione della geometria";

TriaFrame.TriaTabContainer.MeshTabTria.MaxSizeLbl.Title="Dimensione della mesh";
TriaFrame.TriaTabContainer.MeshTabTria.MaxSizeLbl.LongHelp="Imposta la dimensione della mesh globale della parte mesh.";

TriaFrame.TriaTabContainer.MeshTabTria.OffsetLbl.Title="Offset ";
TriaFrame.TriaTabContainer.MeshTabTria.OffsetLbl.LongHelp="Imposta l'offset globale della parte mesh.";

TriaFrame.TriaTabContainer.GeomTabTria.CleanSizeLbl.Title="Dimensione minima dei fori: ";
TriaFrame.TriaTabContainer.GeomTabTria.CleanSizeLbl.LongHelp="Imposta la dimensione minima dei fori da considerare.";

TriaFrame.TriaTabContainer.GeomTabTria.MinSizeLbl.Title="Dimensione minima   ";
TriaFrame.TriaTabContainer.GeomTabTria.MinSizeLbl.LongHelp="Imposta la dimensione minima degli elementi";

TriaFrame.TriaTabContainer.GeomTabTria.SagLbl.Title="Sagitta del vincolo";
TriaFrame.TriaTabContainer.GeomTabTria.SagLbl.LongHelp="Imposta la sagitta del vincolo della parte mesh.";

TriaFrame.TriaTabContainer.GeomTabTria.TopologySizeLbl.Title="Dimensione di riferimento del vincolo";
TriaFrame.TriaTabContainer.GeomTabTria.TopologySizeLbl.LongHelp="Imposta la dimensione di riferimento del vincolo.";

TriaFrame.TriaTabContainer.GeomTabTria.ModifyTopologySize.Title="Dimensione di riferimento del vincolo indipendente dalla dimensione della mesh";
TriaFrame.TriaTabContainer.GeomTabTria.ModifyTopologySize.LongHelp="Dimensione di riferimento del vincolo indipendente dalla dimensione della mesh";

TriaFrame.TriaTabContainer.MeshTabTria.MeshSagValueLbl.Title="Sagitta assoluta";

TriaFrame.TriaTabContainer.MeshTabTria.MeshAbsSag.Title="Sagitta assoluta";
TriaFrame.TriaTabContainer.MeshTabTria.MeshAbsSag.LongHelp="Se l'opzione � selezionata, la sagitta assoluta verr� presa in considerazione.";

TriaFrame.TriaTabContainer.MeshTabTria.MeshAbsSagValueLbl.Title="Valore sagitta assoluta";

TriaFrame.TriaTabContainer.MeshTabTria.MeshRelSag.Title="Sagitta relativa";
TriaFrame.TriaTabContainer.MeshTabTria.MeshRelSag.LongHelp="Se l'opzione � selezionata, la sagitta relativa verr� presa in considerazione.";

TriaFrame.TriaTabContainer.MeshTabTria.MeshRelSagValueLbl.Title="Valore sagitta relativa";

TriaFrame.TriaTabContainer.MeshTabTria.SizeProgression.Title="Progressione della dimensione";
TriaFrame.TriaTabContainer.MeshTabTria.SizeProgression.LongHelp="Se selezionata, la Progressione della dimensione verr� presa in considerazione.";

TriaFrame.TriaTabContainer.GeomTabTria.TolCaptureLbl.Title="Tolleranza";
TriaFrame.TriaTabContainer.GeomTabTria.TolCaptureLbl.LongHelp="Imposta la tolleranza per la cattura automatica delle curve.";

TriaFrame.TriaTabContainer.MeshTabTria.MeshCaptureToleranceLbl.Title="Tolleranza";
TriaFrame.TriaTabContainer.MeshTabTria.MeshCaptureToleranceLbl.LongHelp="Imposta la tolleranza per la cattura automatica della mesh";

TriaFrame.TriaTabContainer.MeshTabTria.CaptureFilter.Title="Filtro";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureFilter.ShortHelp="Filtro di condensazione";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureFilter.LongHelp="Consente di selezionare le mesh da catturare.";

TriaFrame.TriaTabContainer.MeshTabTria.MinSizeForSagLbl.Title="Dimensione minima";
TriaFrame.TriaTabContainer.MeshTabTria.MinSizeForSagLbl.LongHelp="Imposta la dimensione minima degli elementi per la mesh con sagitta.";

TriaFrame.TriaTabContainer.MeshTabTria.CaptureAuto.Title="Cattura automatica della mesh";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureAuto.Help="Attiva o disattiva la cattura automatica della mesh";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureAuto.LongHelp="Attiva o disattiva la cattura automatica della mesh.";

TriaFrame.TriaTabContainer.GeomTabTria.CurveAuto.Title="Cattura automatica della curva";
TriaFrame.TriaTabContainer.GeomTabTria.CurveAuto.Help="Attiva o disattiva la cattura automatica della curva";
TriaFrame.TriaTabContainer.GeomTabTria.CurveAuto.LongHelp="Attiva o disattiva la cattura automatica della curva.";

TriaFrame.TriaTabContainer.GeomTabTria.DetailSimplification.Title="Unione durante la semplificazione";
TriaFrame.TriaTabContainer.GeomTabTria.DetailSimplification.Help="Attiva o disattiva l'unione durante la semplificazione";
TriaFrame.TriaTabContainer.GeomTabTria.DetailSimplification.LongHelp="Attiva o disattiva l'unione durante la semplificazione.";

TriaFrame.TriaTabContainer.MeshTabTria.StripOptimization.Title="Ottimizzazione della striscia";
TriaFrame.TriaTabContainer.MeshTabTria.StripOptimization.Help="Attiva o disattiva l'ottimizzazione della posizione del nodo lungo le strisce";
TriaFrame.TriaTabContainer.MeshTabTria.StripOptimization.LongHelp="Attiva o disattiva l'ottimizzazione della posizione del nodo lungo le strisce.";

TriaFrame.TriaTabContainer.GeomTabTria.FaceAngleLabel.Title="Angolo tra le facce";
TriaFrame.TriaTabContainer.GeomTabTria.FaceAngleLabel.LongHelp="Imposta l'angolo tra le facce.";

TriaFrame.TriaTabContainer.GeomTabTria.CurveAngleLabel.Title="Angolo tra le curve";
TriaFrame.TriaTabContainer.GeomTabTria.CurveAngleLabel.LongHelp="Imposta l'angolo tra le curve.";

QuadFrame.QuadTabContainer.MeshTabQuad.MaxSize2Lbl.Title="Dimensione della mesh";
QuadFrame.QuadTabContainer.MeshTabQuad.MaxSize2Lbl.LongHelp="Imposta la dimensione della mesh globale della parte mesh.";

QuadFrame.QuadTabContainer.MeshTabQuad.Offset2Lbl.Title="Offset ";
QuadFrame.QuadTabContainer.MeshTabQuad.Offset2Lbl.LongHelp="Imposta l'offset globale della parte mesh.";

QuadFrame.QuadTabContainer.GeomTabQuad.CleanSize2Lbl.Title="Dimensione minima dei fori: ";
QuadFrame.QuadTabContainer.GeomTabQuad.CleanSize2Lbl.LongHelp="Imposta la dimensione minima dei fori da considerare.";

QuadFrame.QuadTabContainer.GeomTabQuad.MinSize2Lbl.Title="Dimensione minima   ";
QuadFrame.QuadTabContainer.GeomTabQuad.MinSize2Lbl.LongHelp="Imposta la dimensione minima degli elementi";

QuadFrame.QuadTabContainer.GeomTabQuad.Sag2Lbl.Title="Sagitta del vincolo ";
QuadFrame.QuadTabContainer.GeomTabQuad.Sag2Lbl.LongHelp="Imposta la sagitta del vincolo della parte mesh";

QuadFrame.QuadTabContainer.GeomTabQuad.TopologySize2Lbl.Title="Dimensione di riferimento del vincolo ";
QuadFrame.QuadTabContainer.GeomTabQuad.TopologySize2Lbl.LongHelp="Imposta la dimensione di riferimento del vincolo.";

QuadFrame.QuadTabContainer.GeomTabQuad.ModifyTopologySize2.Title="Dimensione di riferimento del vincolo indipendente dalla dimensione della mesh";
QuadFrame.QuadTabContainer.GeomTabQuad.ModifyTopologySize2.LongHelp="Dimensione di riferimento del vincolo indipendente dalla dimensione della mesh";

QuadFrame.QuadTabContainer.MeshTabQuad.QuadsOnly.Title="Solo quadrangoli";
QuadFrame.QuadTabContainer.MeshTabQuad.QuadsOnly.LongHelp="Se l'opzione � selezionata e se � possibile, verranno creati solo i quadrangoli.";

QuadFrame.QuadTabContainer.MeshTabQuad.MinimizeTriangles.Title="Riduci al minimo i triangoli";
QuadFrame.QuadTabContainer.MeshTabQuad.MinimizeTriangles.LongHelp="Se l'opzione � selezionata, il numero dei triangoli viene ridotto al minimo.";

QuadFrame.QuadTabContainer.MeshTabQuad.OptimizeRegularity.Title="Mesh direzionale";
QuadFrame.QuadTabContainer.MeshTabQuad.OptimizeRegularity.LongHelp="Se l'opzione � selezionata e se � possibile, la mesh sar� direzionale.";

QuadFrame.QuadTabContainer.GeomTabQuad.TolCapture2Lbl.Title="Tolleranza";
QuadFrame.QuadTabContainer.GeomTabQuad.TolCapture2Lbl.LongHelp="Imposta la tolleranza per la cattura automatica delle curve.";

QuadFrame.QuadTabContainer.MeshTabQuad.MeshCaptureTolerance2Lbl.Title="Tolleranza";
QuadFrame.QuadTabContainer.MeshTabQuad.MeshCaptureTolerance2Lbl.LongHelp="Imposta la tolleranza per la cattura automatica della mesh";

QuadFrame.QuadTabContainer.MeshTabQuad.CaptureAuto2.Title="Cattura automatica della mesh";
QuadFrame.QuadTabContainer.MeshTabQuad.CaptureAuto2.Help="Attiva o disattiva la cattura automatica della mesh.";
QuadFrame.QuadTabContainer.MeshTabQuad.CaptureAuto2.LongHelp="Attiva o disattiva la cattura automatica della mesh";

QuadFrame.QuadTabContainer.GeomTabQuad.CurveAuto2.Title="Cattura automatica della curva";
QuadFrame.QuadTabContainer.GeomTabQuad.CurveAuto2.Help="Attiva o disattiva la cattura automatica della curva";
QuadFrame.QuadTabContainer.GeomTabQuad.CurveAuto2.LongHelp="Attiva o disattiva la cattura automatica della curva.";

QuadFrame.QuadTabContainer.GeomTabQuad.DetailSimplification2.Title="Unione durante la semplificazione";
QuadFrame.QuadTabContainer.GeomTabQuad.DetailSimplification2.Help="Attiva o disattiva l'unione durante la semplificazione";
QuadFrame.QuadTabContainer.GeomTabQuad.DetailSimplification2.LongHelp="Attiva o disattiva l'unione durante la semplificazione.";

QuadFrame.QuadTabContainer.MeshTabQuad.StripOptimization.Title="Ottimizzazione della striscia";
QuadFrame.QuadTabContainer.MeshTabQuad.StripOptimization.Help="Attiva o disattiva l'ottimizzazione della posizione del nodo lungo le strisce";
QuadFrame.QuadTabContainer.MeshTabQuad.StripOptimization.LongHelp="Attiva o disattiva l'ottimizzazione della posizione del nodo lungo le strisce.";

QuadFrame.QuadTabContainer.GeomTabQuad.FaceAngleLabel2.Title="Angolo tra le facce";
QuadFrame.QuadTabContainer.GeomTabQuad.FaceAngleLabel2.LongHelp="Imposta l'angolo tra le facce.";

QuadFrame.QuadTabContainer.GeomTabQuad.CurveAngleLabel2.Title="Angolo tra le curve";
QuadFrame.QuadTabContainer.GeomTabQuad.CurveAngleLabel2.LongHelp="Imposta l'angolo tra le curve.";

//
// Error Panel.
//

CATMSHCmdMtoParams.ErrorCoherence0.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence0.Text="La dimensione della mesh deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence1.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence1.Text="La dimensione minima deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence2.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence2.Text="La dimensione minima della sagitta deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence3.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence3.Text="La sagitta del vincolo deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence4.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence4.Text="La dimensione minima del foro deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence5.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence5.Text="La tolleranza di cattura della mesh deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence6.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence6.Text="La tolleranza di cattura della curva deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence7.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence7.Text="La dimensione della sagitta assoluta deve essere maggiore di 0.";

CATMSHCmdMtoParams.ErrorCoherence8.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence8.Text="La dimensione minima deve essere maggiore. (dim.min. > 0.001 * dimensione della mesh)";

CATMSHCmdMtoParams.ErrorCoherence9.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence9.Text="La dimensione della mesh deve essere maggiore della dimensione minima.";

CATMSHCmdMtoParams.ErrorCoherence10.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence10.Text="La sagitta deve essere minore. (Sag < 0.2* dimensione della mesh)";

CATMSHCmdMtoParams.ErrorCoherence11.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence11.Text="La sagitta deve essere maggiore. (Sag > 0.05 * dimensione della mesh)";

CATMSHCmdMtoParams.ErrorCoherence12.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence12.Text="L'offset deve essere minore";

CATMSHCmdMtoParams.ErrorCoherence13.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence13.Text="La sagitta relativa deve essere positiva.";

CATMSHCmdMtoParams.ErrorCoherence14.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence14.Text="La dimensione minima deve essere minore della dimensione della mesh.";

CATMSHCmdMtoParams.ErrorCoherence15.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence15.Text="La sagitta deve essere minore. (Sag < 0.2* dimensione di riferimento del vincolo)";

CATMSHCmdMtoParams.ErrorCoherence16.Title="Errore";
CATMSHCmdMtoParams.ErrorCoherence16.Text="La sagitta deve essere maggiore. (Sag > 0.05 * dimensione di riferimento del vincolo)";

CATMSHCmdMtoParams.ParamsTopoChanged.Title="Avvertenza";
CATMSHCmdMtoParams.ParamsTopoChanged.Text="Le modifiche rendono non valida la semplificazione della geometria.";

CATMSHCmdMtoParams.ParamsMeshChanged.Title="Avvertenza";
CATMSHCmdMtoParams.ParamsMeshChanged.Text="Le modifiche rendono non valida la mesh.";

//
// Ajout pour le mailleur associatif
//

TabContainer.GeomTab.Title="Geometria";
TabContainer.MeshTab.Title="Mesh";

TabContainer.MeshTab.MethodLabel.Title = "Forma: ";
TabContainer.MeshTab.MethodLabel.LongHelp = "Imposta la forma degli elementi mesh su superficie.";

TabContainer.MeshTab.ShapeFrame.Button1.Title = "Triangolo frontale";
TabContainer.MeshTab.ShapeFrame.Button1.Help = "Imposta il metodo del triangolo frontale";
TabContainer.MeshTab.ShapeFrame.Button1.ShortHelp = "Imposta il metodo del triangolo frontale";
TabContainer.MeshTab.ShapeFrame.Button1.LongHelp = "Imposta il metodo del triangolo frontale.";

TabContainer.MeshTab.ShapeFrame.Button2.Title = "Quadrangolo frontale";
TabContainer.MeshTab.ShapeFrame.Button2.Help = "Imposta il metodo del quadrangolo frontale";
TabContainer.MeshTab.ShapeFrame.Button2.ShortHelp = "Imposta il metodo del quadrangolo frontale";
TabContainer.MeshTab.ShapeFrame.Button2.LongHelp = "Imposta il metodo del triangolo frontale.";

TabContainer.MeshTab.elementLabel.Title = "Tipo: ";
TabContainer.MeshTab.elementLabel.LongHelp = "Imposta il tipo di elemento per la mesh su superficie.";

TabContainer.MeshTab.ElementTypeFrame.RBLinear.Title="Lineare";
TabContainer.MeshTab.ElementTypeFrame.RBLinear.Help="Utilizza il tipo di elemento lineare";
TabContainer.MeshTab.ElementTypeFrame.RBLinear.ShortHelp="Utilizza il tipo di elemento lineare";
TabContainer.MeshTab.ElementTypeFrame.RBLinear.RBLinear.LongHelp="Utilizza il tipo di elemento lineare.";

TabContainer.MeshTab.ElementTypeFrame.RBParabolic.Title="Parabolico";
TabContainer.MeshTab.ElementTypeFrame.RBParabolic.Help="Utilizza il tipo di elemento parabolico";
TabContainer.MeshTab.ElementTypeFrame.RBParabolic.ShortHelp="Utilizza il tipo di elemento parabolico";
TabContainer.MeshTab.ElementTypeFrame.RBParabolic.LongHelp="Utilizza il tipo di elemento parabolico.";

TabContainer.MeshTab.Frame.TriaFrame.MaxSizeLbl.Title="Dimensione della mesh";
TabContainer.MeshTab.Frame.TriaFrame.MaxSizeLbl.LongHelp="Imposta la dimensione della mesh globale della parte mesh.";

TabContainer.MeshTab.Frame.QuadFrame.MaxSizeLbl.Title="Dimensione della mesh";
TabContainer.MeshTab.Frame.QuadFrame.MaxSizeLbl.LongHelp="Imposta la dimensione della mesh globale della parte mesh.";

TabContainer.MeshTab.Frame.QuadFrame.QuadsOnly.Title="Solo quadrangoli";
TabContainer.MeshTab.Frame.QuadFrame.QuadsOnly.LongHelp="Se l'opzione � selezionata e se � possibile, verranno creati solo i quadrangoli.";

TabContainer.MeshTab.Frame.TriaFrame.MeshAbsSag.Title="Sagitta assoluta";
TabContainer.MeshTab.Frame.TriaFrame.MeshAbsSag.LongHelp="Se l'opzione � selezionata, la sagitta assoluta verr� presa in considerazione.";

TabContainer.MeshTab.Frame.TriaFrame.MeshAbsSagValueLbl.Title="Valore sagitta assoluta";

TabContainer.MeshTab.Frame.TriaFrame.MeshRelSag.Title="Sagitta relativa";
TabContainer.MeshTab.Frame.TriaFrame.MeshRelSag.LongHelp="Se l'opzione � selezionata, la sagitta relativa verr� presa in considerazione.";

TabContainer.MeshTab.Frame.TriaFrame.MeshRelSagValueLbl.Title="Valore sagitta relativa";

TabContainer.MeshTab.Frame.TriaFrame.MinSizeForSagLbl.Title="Dimensione minima";
TabContainer.MeshTab.Frame.TriaFrame.MinSizeForSagLbl.LongHelp="Imposta la dimensione minima degli elementi per la mesh con sagitta.";

TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.Title="Cattura automatica della mesh";
TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.Help="Attiva o disattiva la cattura automatica della mesh";
TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.LongHelp="Attiva o disattiva la cattura automatica della mesh.";

TabContainer.MeshTab.Frame.TriaFrame.CaptureAuto.Title="Cattura automatica della mesh";
TabContainer.MeshTab.Frame.TriaFrame.CaptureAuto.Help="Attiva o disattiva la cattura automatica della mesh";
TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.LongHelp="Attiva o disattiva la cattura automatica della mesh.";

TabContainer.MeshTab.Frame.QuadFrame.MeshCaptureToleranceLbl.Title="Tolleranza";
TabContainer.MeshTab.Frame.QuadFrame.MeshCaptureToleranceLbl.LongHelp="Imposta la tolleranza per la cattura automatica della mesh";

TabContainer.MeshTab.Frame.TriaFrame.MeshCaptureToleranceLbl.Title="Tolleranza";
TabContainer.MeshTab.Frame.TriaFrame.MeshCaptureToleranceLbl.LongHelp="Imposta la tolleranza per la cattura automatica della mesh";

TabContainer.MeshTab.CaptureFilter.Title="Filtro";
TabContainer.MeshTab.CaptureFilter.ShortHelp="Filtro di condensazione";
TabContainer.MeshTab.CaptureFilter.LongHelp="Consente di selezionare le mesh da catturare.";

TabContainer.MeshTab.Frame.QuadFrame.DefaultMesh.Title = "Metodo predefinito";
TabContainer.MeshTab.Frame.QuadFrame.DefaultMesh.LongHelp = "Imposta il tipo di mesh predefinito.";

Mapped = "Mesh libera mappata";
MinimalMesh = "Mesh minima";

TabContainer.GeomTab.TopologySizeLbl.Title="Dimensione di riferimento del vincolo";
TabContainer.GeomTab.TopologySizeLbl.LongHelp="Imposta la dimensione di riferimento del vincolo";

TabContainer.GeomTab.ModifyTopologySize.Title="Vincoli indipendenti dalla dimensione della mesh";
TabContainer.GeomTab.ModifyTopologySize.LongHelp="Vincoli indipendenti dalla dimensione della mesh";

TabContainer.GeomTab.TopologySagLbl.Title="Sagitta del vincolo";
TabContainer.GeomTab.TopologySagLbl.LongHelp="Imposta la sagitta del vincolo per l'inizializzazione.";

TabContainer.GeomTab.SharpEdges.Title="Aggiungi spigoli vivi";
TabContainer.GeomTab.SharpEdges.LongHelp="Se l'opzione � selezionata, gli spigoli vivi verranno presi in considerazione.";

TabContainer.GeomTab.FaceAngleLbl.Title="Angolo degli spigoli vivi";
TabContainer.GeomTab.FaceAngleLbl.LongHelp="Imposta l'angolo degli spigoli vivi";

TabContainer.GeomTab.CleanSizeLbl.Title="Dimensione minima dei fori: ";
TabContainer.GeomTab.CleanSizeLbl.LongHelp="Imposta la dimensione minima dei fori da considerare.";

TabContainer.GeomTab.OffsetLbl.Title="Offset ";
TabContainer.GeomTab.OffsetLbl.LongHelp="Imposta l'offset globale della parte mesh.";

TabContainer.GeomTab.CurveAuto.Title="Cattura automatica della curva";
TabContainer.GeomTab.CurveAuto.Help="Attiva o disattiva la cattura automatica della curva";
TabContainer.GeomTab.CurveAuto.LongHelp="Attiva o disattiva la cattura automatica della curva.";

TabContainer.GeomTab.TolCaptureLbl.Title="Tolleranza";
TabContainer.GeomTab.TolCaptureLbl.LongHelp="Imposta la tolleranza per la cattura automatica delle curve.";
