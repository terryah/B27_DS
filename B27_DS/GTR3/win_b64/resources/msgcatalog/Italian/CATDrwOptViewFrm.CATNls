//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 2000 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptManip
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    30.10.2000
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//	01			lgk		17.12.02	Revision
//=====================================================================================

Title="Visualizza";


frameGenElem.HeaderFrame.Global.Title="Creazione di geometrie / Dettagliatura";
frameGenElem.HeaderFrame.Global.LongHelp=
"Creazione di geometrie
Definisce quali elementi geometrici
vengono creati.";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenAxis.Title="Crea assi";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenAxis.LongHelp="Crea linee di assi";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenCenter.Title="Crea linee di centro";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenCenter.LongHelp="Crea linee di centro";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenThread.Title="Crea filettature";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenThread.LongHelp="Crea le filettature";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenHiddenLines.Title="Crea linee nascoste";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenHiddenLines.LongHelp="Crea linee nascoste";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil._checkGenFil.Title="Crea raccordo";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil._checkGenFil.LongHelp="Crea raccordo";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil.pushGenFil.Title="Configura";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil.pushGenFil.LongHelp="Configura la proiezione dei raccordi";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGen3DColors.Title="Eredita colori 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGen3DColors.LongHelp="Eredita colori 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.checkGenWireFrame.Title="Proietta wireframe 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.checkGenWireFrame.LongHelp="Proietta wireframe 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.pushGenWireFrameMod.Title="Configura";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.pushGenWireFrameMod.LongHelp="Configura la proiezione del wireframe";
frameGenElem.IconAndOptionsFrame.OptionsFrame.checkGenApplyUncut.Title="Applica specifiche 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame.checkGenApplyUncut.LongHelp="Applica specifiche 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.labelLinetype.Title="Visualizza tipo di linea";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.labelLinetype.LongHelp="Visualizza tipo di linea";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.linetypePushButton.Title="Configura";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.linetypePushButton.LongHelp="Configura tipo di linea e spessore";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.checkProject3DPts.Title="Proietta punti 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.checkProject3DPts.LongHelp="Proietta punti 3D";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.project3DPtsPushButton.Title="Configura";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.project3DPtsPushButton.LongHelp="Configura la proiezione dei punti 3D";

genGeomMngt.HeaderFrame.Global.Title="Geometria generata";
genGeomMngt.HeaderFrame.Global.LongHelp="Geometria generata";
genGeomMngt.IconAndOptionsFrame.OptionsFrame.chkGMECreation.Title="Conserva dettagliatura grafica manualmente impostata sulla geometria";
genGeomMngt.IconAndOptionsFrame.OptionsFrame.chkGMECreation.LongHelp="Conserva dettagliatura grafica manualmente impostata sulla geometria.";


frameGenView.HeaderFrame.Global.Title="Creazione della vista";
frameGenView.HeaderFrame.Global.LongHelp="Definisce i parametri per la creazione della vista generativa.";        
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.lbl.Title="Modalit� di creazione della vista";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.lbl.LongHelp="Definisce le modalit� di creazione della vista";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.combViewMod.LongHelp="Definisce le modalit� di creazione della vista";
ExactView="Vista esatta";
CGRView="CGR";
PictureView="Raster";
HRV="Approssimato";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.pushRasterOptions.Title="Configura";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.pushRasterOptions.LongHelp="Configura le opzioni raster";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.chkAutoLOD.Title="Livello di dettaglio automatico";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.chkAutoLOD.LongHelp="I DPI per la visualizzazione e la stampa vengono
adattati automaticamente.";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl1.Title="DPI per visualizzazione";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl1.LongHelp="DPI per visualizzazione";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl2.Title="DPI per stampa";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl2.LongHelp="DPI per stampa";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkExactPreview.Title="Anteprima esatta per la creazione della vista";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkExactPreview.LongHelp="Consente di eseguire un'anteprima delle viste in modalit� Progettazione
anche se si sta lavorando in modalit� Visualizzazione.
Lasciando questa opzione non selezionata utilizza la visualizzazione
dei dati inserita nel documento 3D per l'anteprima.";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmGenBox.checkGenBox.Title="Genera solo parti pi� grandi di";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmGenBox.checkGenBox.LongHelp="Genera solo parti pi� grandi della dimensione specificata.";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkOcclusionCulling.Title="Abilita l'eliminazione delle parti non in vista";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkOcclusionCulling.LongHelp="Attiva o disattiva l'eliminazione delle parti non in vista.
Questa impostazione consente di migliorare la visualizzazione
non mostrando nuovamente gli elementi nascosti.
E' particolarmente utile durante la visualizzazione di scenari
con molti compartimenti come impianti e costruzioni.";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkSelBodyInAss.Title="Seleziona body nell'assieme";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkSelBodyInAss.LongHelp="Attiva o disattiva la selezione del body nell'assieme.";

frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._labelViewStatus.Title="Stato di aggiornamento delle viste approssimate/raster/CGR";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._labelViewStatus.LongHelp="Definisce le informazioni sul prodotto da utilizzare per determinare se � necessario aggiornare una vista";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataVisu.Title="Basata sui dati di visualizzazione ";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataVisu.LongHelp="Utilizza le registrazioni data/ora dei dati di visualizzazione delle parti (dati cgr)";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataDesign.Title="Basata su dati del disegno ";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataDesign.LongHelp="Utilizza le registrazioni data/ora dei dati del disegno delle parti ";


frameClipping.HeaderFrame.Global.Title="Mascheratura a cerchio";
frameClipping.HeaderFrame.Global.LongHelp="Definisce i parametri utilizzati nelle mascherature a cerchio.";        
frameClipping.IconAndOptionsFrame.OptionsFrame.chkDimClipNoShow.Title="Nascondi quote collegate alla geometria non visibile";
frameClipping.IconAndOptionsFrame.OptionsFrame.chkDimClipNoShow.LongHelp="Nascondi quote collegate alla geometria non visibile e le annotazioni associate";

frameView3D.HeaderFrame.Global.Title="Vista da 3D";
frameView3D.HeaderFrame.Global.LongHelp="Definisce i parametri utilizzati nelle viste estratte da 3D.";        
frameView3D.IconAndOptionsFrame.OptionsFrame.chkFTAAnnotLay.Title="Conserva il layout e la dettagliatura delle annotazioni 2D";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkFTAAnnotLay.LongHelp="Conserva il layout e la dettagliatura delle annotazioni 2D estratte.";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGeomOf2DLView.Title="Genera geometria 2D";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGeomOf2DLView.LongHelp="Genera la geometria 2D dalle viste di layout.";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGenRedCross.Title="Genera una croce rossa sull'annotazione";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGenRedCross.LongHelp="Le annotazioni estratte da 3D sono contrassegnate da una croce rossa,
se la loro geometria non viene estratta o
se la loro geometria � nascosta nella vista corrente.";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkSyncUpdate.Title="Sincronizza durante l'aggiornamento";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkSyncUpdate.LongHelp="Sincronizza tutte le viste ed i fogli nel disegno durante l'aggiornamento.";


viewGenFillet.Title="Creazione dei raccordi";
viewGenFillet.frameButtonGenFil.HeaderFrame.Global.Title="Configura la creazione dei raccordi";
viewGenFillet.frameButtonGenFil.HeaderFrame.Global.LongHelp="Configura la creazione dei raccordi
Consente di configurare la creazione dei raccordi.";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioBoundaries.Title="Bordi";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioBoundaries.LongHelp="Bordi
Indica se \"i bordi dei raccordi\" devono essere visualizzati nelle viste.";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioSymbo.Title="Simbolico";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioSymbo.LongHelp="Simbolico
Indica se i \"raccordi simbolici\" devono essere visualizzati nelle viste.";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioProjEd.Title="Spigoli originali proiettati";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioProjEd.LongHelp="Spigoli originali proiettati
Indica se gli \"spigoli originali proiettati\" devono essere visualizzati nelle viste.";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioOrigEd.Title="Spigoli originali approssimati";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioOrigEd.LongHelp="Spigoli originali approssimati
Indica se gli \"spigoli originali\" devono essere visualizzati nelle viste.";


viewGenWireFrame.Title="Modalit� Proiezione wireframe 3D";
viewGenWireFrame.frmWireFrameMod.HeaderFrame.Global.Title="Wireframe 3D proiettato";
viewGenWireFrame.frmWireFrameMod.HeaderFrame.Global.LongHelp="Wireframe 3D proiettato\nIndica se il wireframe 3D proiettato pu� essere nascosto\no deve essere sempre visibile nelle viste selezionate.";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioHidden.Title="Pu� essere nascosto";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioHidden.LongHelp="Pu� essere nascosto\nIndica se il wireframe 3D proiettato pu� essere nascosto.";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioVisible.Title="Sempre visibile";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioVisible.LongHelp="Sempre visibile\nIndica se il wireframe 3D proiettato deve essere sempre visibile.";


viewGen3DPoints.Title="Proietta Punti 3D";
viewGen3DPoints.frameButtonProject3DPts.HeaderFrame.Global.Title="Proietta punti 3D";
viewGen3DPoints.frameButtonProject3DPts.HeaderFrame.Global.LongHelp="Proietta punti 3D
Indica se i punti 3D proiettati visualizzano il simbolo
ereditato dal 3D o il simbolo selezionato.";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbolInherit.Title="Eredita simboli 3D";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbolInherit.LongHelp="Eredita simboli 3D
Visualizza il simbolo ereditato dal 3D.";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbol.Title="Simbolo";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbol.LongHelp="Simbolo
Visualizza il simbolo selezionato.";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.comboSymbol.Title="Simbolo";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.comboSymbol.LongHelp="Simbolo
Visualizza il simbolo selezionato.";


viewLineType.Title="Tipo di linea e spessore";
viewLineType.frameButtonLinetype.HeaderFrame.Global.Title="Tipo di linea e spessore";
viewLineType.frameButtonLinetype.HeaderFrame.Global.LongHelp="Tipo di linea e spessore";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSection.Title="Vista in sezione";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSection.LongHelp="Vista in sezione";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblDetail.Title="Vista di dettaglio";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblDetail.LongHelp="Vista di dettaglio";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBroken.Title="Vista spezzata";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBroken.LongHelp="Vista spezzata";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBreakout.Title="Vista maschera";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBreakout.LongHelp="Vista maschera";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSkin.Title="Vista di sezione skin";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSkin.LongHelp="Vista di sezione skin";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSectionLinetype.LongHelp="Seleziona un tipo di linea";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboDetailLinetype.LongHelp="Seleziona un tipo di linea";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBrokenLinetype.LongHelp="Seleziona un tipo di linea";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBreakoutLinetype.LongHelp="Seleziona un tipo di linea";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSkinLinetype.LongHelp="Seleziona un tipo di linea";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSectionThickness.LongHelp="Seleziona uno spessore";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboDetailThickness.LongHelp="Seleziona uno spessore";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBrokenThickness.LongHelp="Seleziona uno spessore";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBreakoutThickness.LongHelp="Seleziona uno spessore";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSkinThickness.LongHelp="Seleziona uno spessore";


viewGenModApproximate.Title="Modalit� approssimata";
viewGenModApproximate.frmHRVOptions.HeaderFrame.Global.Title="Opzione modalit� approssimata";
viewGenModApproximate.frmHRVOptions.HeaderFrame.Global.LongHelp="Configura l'opzione della modalit� approssimata";
viewGenModApproximate.frmHRVOptions.IconAndOptionsFrame.OptionsFrame.lblHRVLOD.Title = "Livello di dettaglio:";
viewGenModApproximate.frmHRVOptions.IconAndOptionsFrame.OptionsFrame.lblHRVLOD.LongHelp = "Livello di dettaglio:";
XGraph = "LdD";
YGraph = "Durata";

viewGenModRaster.Title="Opzioni del modalit� Raster";
viewGenModRaster.frmRasterOptions.HeaderFrame.Global.Title="Opzioni del modalit� Raster";
viewGenModRaster.frmRasterOptions.HeaderFrame.Global.LongHelp="Configura le opzioni per le viste raster";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblRasterMode.Title="Modalit�";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblRasterMode.LongHelp="Modalit�";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLOD.Title="Livello di dettaglio:";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLOD.LongHelp="Livello di dettaglio:";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLODVisu.Title="Per la visualizzazione";
viewGenModRaster.viewGenModRaster.IconAndOptionsFrame.OptionsFrame.lblLODVisu.LongHelp="Per la visualizzazione";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLODPrint.Title="Per la stampa";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLODPrint.LongHelp="Per la stampa";

HRD = "Rimozione dinamica di linee nascoste";
Shading = "Ombreggiatura";
ShadingWithEdges = "Ombreggiatura con spigoli";
ShadingNoLight = "Ombreggiatura, nessuna fonte di luce";
ShadingWithEdgesNoLight = "Ombreggiatura con spigoli, nessuna fonte di luce";

LowQualityMode       = "Qualit� bassa";
NormalQualityMode    = "Qualit� normale";
HighQualityMode      = "Qualit� elevata";
CustomizeMode        = "Personalizza";
