//--------------------------------------------------------
// NODES

Nodes_Text="Testo nodi";

//--------------------------------------------------------
// ELEMENTS

Elements_Text="Testo elementi";

//--------------------------------------------------------
// USER_NUMBER

UserNodesNumber_Text="Testo nodi utente";
UserElementsNumber_Text="Testo elementi utente";

//--------------------------------------------------------
// PHYSICAL_TYPE

PhysicalType_Text="Testo tipo fisico";
PhysicalType_Fringe="Frangiatura tipo fisico";

//--------------------------------------------------------
// MATERIALS

Material_Text="Testo materiale";
Material_Fringe="Frangiatura materiale";

//--------------------------------------------------------
// THICKNESS

Thickness_Text="Testo spessore";
Thickness_Fringe="Frangiatura spessore";
Lamina_Thickness_Fringe="Frangiatura spessore (lamina)";
Lamina_Thickness_Text="Testo spessore (lamina)";

//--------------------------------------------------------
// CROSS SECTION

CrossSectionalArea_Symbol="Simbolo area sezione retta";
CrossSectionalArea_Text="Testo area sezione retta";
CrossSectionalArea_Fringe="Frangiatura area sezione retta";

//--------------------------------------------------------
// STRESS

Stress_Iso_VonMises="Sforzi alla Von Mises (valori nodali)";
Stress_Iso_VonMises_Disc="Sforzi alla Von Mises (valori nodali sull'elemento)";
Stress_Text_VonMises="Testo Sforzi alla Von Mises";
StressVonMises_Fringe="Sforzi alla Von Mises (centro dei valori sull'elemento)";

Stress_Iso_PpalValue="Componente tensore principale degli sforzi (valori nodali)";
Stress_Iso_PpalValue_Abs="Componente tensore principale degli sforzi (valori nodali assoluti)";
Stress_Iso_PpalValue_Disc_Abs="Componente tensore principale degli sforzi (valori nodali assoluti sull'elemento)";
Stress_Text_PpalTensor="Testo tensore principale degli sforzi";
Stress_Text_PpalTensor_Abs="Testo tensore principale degli sforzi (assoluto)";

Stress_Iso_Tensor_Component="Componente tensore completo degli sforzi (valori nodali)";
Stress_Iso_Tensor_Component_Disc="Componente tensore completo degli sforzi (valori nodali sull'elemento)";
Stress_Text_SymTensor="Testo tensore completo degli sforzi";

Stress_Iso_PpalShearing="Tensore taglio massimo (valori nodali)";
Stress_Text_PpalShearing="Testo tensore taglio massimo";

Surface_Stress_Symbol_PpalTensor="Simbolo tensore principale di sforzo superficiale";
Surface_Stress_Iso_PpalValue_Disc="Componente tensore principale di sforzo superficiale (valori nodali sull'elemento)";
Surface_Stress_Iso_PpalValue="Componente tensore principale di sforzo superficiale (valori nodali)";
Surface_Stress_Text_PpalTensor="Testo tensore principale di sforzo superficiale";
Surface_Stress_Iso_Tensor_Component="Componente tensore completo di sforzo superficiale (valori nodali)";
Surface_Stress_Iso_Tensor_Component_Disc="Componente tensore completo di sforzo superficiale (valori nodali sull'elemento)";
Surface_Stress_Text_SymTensor="Testo tensore completo di sforzo superficiale";

ShearStress_Disc="Sforzo pannello di taglio (valori nodali sull'elemento)";
ShearStress_Text="Testo sforzo pannello di taglio";

Instantaneous_Stress_VonMises_Fringe="Sforzi alla Von Mises istantanei (centro dei valori dell'elemento)";
Instantaneous_Stress_VonMises_Disc="Sforzi alla Von Mises istantanei (valori nodali dell'elemento)";
Instantaneous_Stress_VonMises_Iso="Sforzi alla Von Mises istantanei (valori nodali)";

//--------------------------------------------------------
// STRAIN

Strain_Iso_PpalValue="Componente tensore principale delle deformazioni (valori nodali)";
Strain_Symbol_PpalTensor="Simbolo tensore principale delle deformazioni";
Strain_Text_PpalTensor="Testo tensore principale delle deformazioni";
Strain_Iso_Tensor_Component="Componente tensore completo delle deformazioni (valori nodali)";
Strain_Text_SymTensor="Testo tensore completo delle deformazioni";

//--------------------------------------------------------
// 3D NODAL DISPLACEMENTS

Disp_Text="Testo traslazione";

//--------------------------------------------------------
// 3D NODAL ROTATIONS

Rotation_Iso="Ampiezza rotazione";
Rotation_Iso_Component="Componente rotazione";
Rotation_Symbol="Vettore rotazione";
Rotation_text="Testo rotazione";

//--------------------------------------------------------
// TRANSLATION SubType=SNG

Local_Singularity_In_Translation="Singolarit� locale in traslazione";

//--------------------------------------------------------
// ROTATION SubType=SNG

Local_Singularity_In_Rotation="Singolarit� locale in rotazione";

//--------------------------------------------------------
// NODAL FORCES

Force_Iso="Valore della forza concentrata";
Force_Iso_Component="Componente della forza concentrata";
Force_Symbol="Vettore forza concentrata";
Force_Text="Testo della forza concentrata";

//--------------------------------------------------------
// FREEBODY FORCES

Freebody_Force_Iso="Valore della forza body libero";
Freebody_Force_Iso_Component="Componente della forza body libero";
Freebody_Force_Symbol="Vettore della forza body libero";
Freebody_Force_Text="Testo della forza body libero";


//--------------------------------------------------------
// LINE FORCES

Lineic_Force_Symbol="Vettore di carico lineare";
Lineic_Force_Text="Testo di carico lineare";

//--------------------------------------------------------
// SURFACE FORCES

Surfacic_Force_Symbol="Vettore forza di superficie";
Surfacic_Force_Text="Testo forza di superficie";
Surfacic_Force_Fringe="Frangiatura forza di superficie";

//--------------------------------------------------------
// VOLUME FORCES

Volumic_Force_Symbol="Vettore di forza volumica";
Volumic_Force_Text="Testo di forza volumica";
Volumic_Force_Iso="Forza volumica";
Volumic_Force_Fringe="Frangiatura di forza volumica";

//--------------------------------------------------------
// ANGULAR ACCELERATION

AngularAcc_Symbol="Vettore accelerazione angolare";
AngularAcc_Text="Testo accelerazione angolare";
AngularAcc_Fringe="Frangiatura accelerazione angolare";

//--------------------------------------------------------
// ANGULAR VELOCITY

AngularVelocity_Symbol="Vettore velocit� angolare";
AngularVelocity_Text="Testo velocit� angolare";
AngularVelocity_Fringe="Frangiatura velocit� angolare";

//--------------------------------------------------------
// ROTATIONAL_ACCELERATION

Rotational_Acceleration_Symbol="Vettore accelerazione rotazionale";
Rotational_Acceleration_Text="Testo accelerazione rotazionale";
Rotational_Acceleration_Iso="Accelerazione rotazionale";

//--------------------------------------------------------
// ROTATIONAL_VELOCITY

Rotational_Velocity_Symbol="Vettore velocit� rotazionale";
Rotational_Velocity_Text="Testo velocit� rotazionale";
Rotational_Velocity_Iso="Velocit� rotazionale";

//--------------------------------------------------------
// ACCELERATION

Acceleration_Symbol="Vettore accelerazione";
Acceleration_Text="Testo accelerazione";
Acceleration_Iso="Accelerazione";
Acceleration_Fringe="Frangiatura accelerazione";
Relative_Acceleration_Symbol="Vettore accelerazione relativa";

//--------------------------------------------------------
// VELOCITY

Velocity_Symbol="Vettore velocit�";
Velocity_Text="Testo velocit�";
Velocity_Iso="Velocit�";
Relative_Velocity_Symbol="Vettore velocit� relativa";

//--------------------------------------------------------
// MASS

Point_Mass_Symbol="Simbolo massa concentrata";
Point_Mass_Text="Testo massa concentrata";
Point_Mass_Iso="Massa concentrata";

Lineic_Mass_Symbol="Simbolo massa lineare";
Lineic_Mass_Text="Testo massa lineare";

Surfacic_Mass_Symbol="Simbolo massa superficiale";
Surfacic_Mass_Text="Testo massa superficiale";
Surfacic_Mass_Fringe="Frangiatura massa superficiale";

Volumic_Mass_Symbol="Simbolo massa volumica";
Volumic_Mass_Text="Testo massa volumica";
Volumic_Mass_Iso="Massa volumica";
Volumic_Mass_Fringe="Frangiatura massa volumica";

//--------------------------------------------------------
// PRESSURE

Pressure_Symbol="Vettore pressione";
Pressure_Text="Testo pressione";
Pressure_Fringe="Frangiatura pressione";
Pressure_Iso="Pressione (valori nodali)";

//--------------------------------------------------------
// MOMENT

Moment_Symbol="Vettore momento localizzato";
Moment_Text="Testo momento localizzato";
Moment_Iso="Misura momento localizzato";
Moment_Iso_Component="Componente momento localizzato";

//--------------------------------------------------------
// FREEBODY MOMENT

Freebody_Moment_Symbol="Vettore momento body libero";
Freebody_Moment_Text="Testo momento body libero";
Freebody_Moment_Iso="Valore del momento body libero";
Freebody_Moment_Iso_Component="Componente del momento body libero";

//--------------------------------------------------------
// VON MISES

StressVonMises_Iso_Disc="Sforzi alla Von Mises (valori nodali sull'elemento)";
StressVonMises_Text="Testo Sforzi alla Von Mises";
StressVonMises_Symbol="Sforzi alla Von Mises (simbolo)";

//--------------------------------------------------------
// STRAIN ENERGY

ElasticEnergy_Text="Testo energia di deformazione locale";
ElasticEnergy_Fringe="Energia di deformazione locale";
ElasticEnergy_Symbol="Simbolo energia di deformazione locale";

ElasticEnergyDensity_Text="Testo densit� energia di deformazione locale";
ElasticEnergyDensity_Fringe="Densit� energia di deformazione locale";
ElasticEnergyDensity_Symbol="Simbolo densit� energia di deformazione locale";

//--------------------------------------------------------
// ERROR

EstimatedError_Text="Testo errore locale stimato";
EstimatedError_Symbol="Simbolo errore locale stimato";

//--------------------------------------------------------
// DEGREES_OF_FREEDOM

Restraint="Simbolo gradi di libert�";

//--------------------------------------------------------
// LOCAL AXIS

LocalAxis="Simbolo asse locale";

//--------------------------------------------------------
// CLEARANCE

Display_clearance_symbol="Simbolo luce minima";
Display_clearance_text="Testo luce minima";
Display_clearance_iso="Luce minima";

//--------------------------------------------------------
// TEMPERATURE

Temperature_Field_Symbol="Simbolo campo di temperatura";

//--------------------------------------------------------
// MASS_INERTIA

MassInertia_Text="Massa inerziale (testo)";

//--------------------------------------------------------
// MASS_MOMENT_OF_INERTIA

MassMomentOfInertia_Text="Momento di inerzia di massa (testo)";

//--------------------------------------------------------
// AREA_MOMENT_OF_INERTIA

AreaMomentOfInertia_Text="Momento di inerzia dell'area (testo)";

//--------------------------------------------------------
// AREA_SHEAR_RATIO_XY

AreaShearRatioInXYPlane_Text="Rapporto taglio area nel piano XY (testo)";

//--------------------------------------------------------
// AREA_SHEAR_RATIO_XZ

AreaShearRatioInXZPlane_Text="Rapporto taglio area nel piano XZ (testo)";

//--------------------------------------------------------
// SHEAR_CENTER

ShearCenter_Text="Centro di taglio (testo)";

//--------------------------------------------------------
// TRANSLATIONAL_STIFFNESS

Translational_Stiffness_Symbol="Rigidezza di traslazione (simbolo)";
Translational_Stiffness_Text="Rigidezza di traslazione (testo)";

//--------------------------------------------------------
// ROTATIONAL_STIFFNESS

Rotational_Stiffness_Symbol="Rigidezza di rotazione (simbolo)";
Rotational_Stiffness_Text="Rigidezza di rotazione (testo)";

//--------------------------------------------------------
// ORIENTATION_VECTOR

Orientation_Vector_Symbol="Vettore di orientamento (simbolo)";
Orientation_Vector_Text="Vettore di orientamento (testo)";

//--------------------------------------------------------
// FRICTION_RATIO

Display_FrictionRatio_Symbol="Simbolo del rapporto di forza di frizione";
Display_FrictionRatio_Text="Testo del rapporto di forza di frizione";
Display_FrictionRatio_Iso="Iso del rapporto di forza di frizione";

//--------------------------------------------------------
// 3D_COORDINATES

Coordinates_Symbol="Simbolo coordinate nodali";
Coordinates_Text="Testo coordinate nodali";

//--------------------------------------------------------
//Element Centroid

Element_Centroid_Text="Testo del baricentro dell'elemento";
Element_Centroid_Symbol="Simbolo del baricentro dell'elemento";

//--------------------------------------------------------
// CURVATURE

Curvature_Text="Testo curvatura";

//--------------------------------------------------------
// TRANSVERSE_SHEAR_STRAIN

Transverse_Shear_Strain_Text="Testo deformazione taglio trasversale";
Transverse_Shear_Strain_Iso="Deformazione taglio trasversale";

//--------------------------------------------------------
// TRANSVERSE_SHEAR_STRESS

Transverse_Shear_Stress_Text="Testo sforzo taglio trasversale";
Transverse_Shear_Stress_Iso="Sforzo taglio trasversale";

//--------------------------------------------------------
// AVG-SHEAR_FLOW

AvgShear_Flow_Text="Testo flusso di taglio medio";
AvgShear_Flow_Fringe="Frangiatura flusso di taglio medio";

//--------------------------------------------------------
// FORCE_FLOW

Force_Flow_Text="Testo flusso forza";
Force_Flow_Text_3D="Testo 3� flusso forza";
Force_Flow_Text_2D="Testo 2� flusso forza";

//--------------------------------------------------------
// MOMENT_FLOW

Moment_Flow_Text="Testo flusso momento";

//--------------------------------------------------------
// NB_LAMINATES

Nb_laminates="Testo numero laminato";
Nb_laminates_Fringe="Frangiatura numero laminato";

//--------------------------------------------------------
// PLY_ID

Ply_id="Testo ID strato";
Ply_id_Fringe="Frangiatura ID strato";

//--------------------------------------------------------
// ANGLE

Composite_Angle="Simbolo angolo composito";

//--------------------------------------------------------
// OFFSET

Offset_Symbol="Offset (simbolo)";
Offset_Text="Offset (testo)";

//--------------------------------------------------------
// SURFACE_OFFSET

Surface_Offset_Fringe="Frangiatura dell'offset di superficie";

//--------------------------------------------------------
// ENVELOP

Values_Symbol="Simboli di valori";
Values_Text="Testo valori";
Values_Average_Iso="Iso medio valori";
Values_Discontinuous_Iso="Iso discontinuo valori";
Values_Fringe="Frangiatura valori";
Criticals_Symbol="Simbolo di gruppi critici";
Criticals_Text="Testo di gruppi critici";
Criticals_Average_Iso="ISO medio di gruppi critici";
Criticals_Discontinuous_Iso="ISO discontinuit� di gruppi critici";
Criticals_Fringe="Frangiatura di gruppi critici";

//--------------------------------------------------------
// FAILURE CRITERION
Criterion_TsailHill_DiscIso="Iso discontinuo criterio Tsai-Hill";
Criterion_TsailHill_Text="Testo criterio Tsai-Hill";
Criterion_TsailWu_DiscIso="Iso discontinuo criterio Tsai-Wu";
Criterion_TsailWu_Text="Testo criterio Tsai-Wu";
Criterion_MaxFailure_DiscIso="Iso discontinuo criterio errore max";
Criterion_MaxFailure_Text="Testo criterio errore max";
Criterion_Hoffman_DiscIso="Iso discontinuo criterio Hoffman";
Criterion_Hoffman_Text="Testo criterio Hoffman";
Criterion_Tresca_DiscIso="Iso discontinuo criterio Tresca";
Criterion_Tresca_Text="Testo criterio Tresca";
Criterion_VonMises_DiscIso="Iso discontinuo criterio Von Mises";
Criterion_VonMises_Text="Testo criterio Von Mises";

Stress_Tresca_Iso="Iso media tensione Tresca";
Stress_Tresca_DiscIso="Iso discontinuo tensione Tresca";
Stress_Tresca_Text="Testo tensione Tresca";
Stress_Tresca_Symbol="Simbolo tensione Tresca";

//--------------------------------------------------------
// Criteria names used in SPM xml file

SCALAR="Scalare";
PRINCIPAL_SHEARING="Taglio principale";
PRINCIPAL_VALUE="Valore principale";
NORMALIZE="Risultante";
VECTOR="Vettoriale";
VECTOR_COMPONENT="Componente vettore";
VON_MISES="Von Mises";
ABS_PRINCIPAL_VALUE="Valore principale (valore assoluto)";
TENSOR_COMPONENT="Componente tensore";
SYMMETRICAL_TENSOR="Tensore simmetrico";
MASS="Massa";
COMPONENTS="Componenti";
Envelop="Inviluppo";
SUM="Somma";
INSTANTANEOUS_VON_MISES="Von Mises istantaneo";

//
//--------------------------------------------------------
// SubTypes names used in GPS xml file

RELATIVE="Relativo";
SURFACE="Superficie";
SHEAR="Taglio";
ENVELOP_VALUES="Valori di inviluppo";
ENVELOP_CRITICALS="Valori critici di inviluppo";
SNG="Singolarit�";
VonMises="Von Mises";
2D="2D";
3D="3D";
Max_Failure="Errore max";
Tsai_Hill="Tsai-Hill";
Tsai_Wu="Tsai-Wu";
Hoffman="Hoffman";
Tresca="Tresca";
