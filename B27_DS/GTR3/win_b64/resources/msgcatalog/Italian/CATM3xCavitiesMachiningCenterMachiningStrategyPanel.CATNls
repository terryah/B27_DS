Title="Lavorazione";

M3xMultiPocketsSweepingMode="Stile percorso utensile";
M3xPocketProgressionMode="Traiettoria elicoidale";
M3xStayOnBottom="Sempre su parte inferiore";
M3xFMManagementMode="Gestione dell'utensile interamente innestato";
M3xMaxFMDepthOfCut="Profondit� max taglio dell'intero materiale";
M3xFMTrochoidMinRadDist="Raggio trochoid minimo";


M3xStockContouringHelical="Modalit� Taglio forzata sul contorno della parte";
M3xStockContouringHelical.LongHelp="Forza l'utensile ad eseguire il taglio rispettando i contorni della parte.";


M3xMultiPocketsSweepingMode.LongHelp="Specifica le traiettorie dell'utensile. Lo stile del percorso utensile pu� essere:
- Elicoidale: il percorso utensile � composto da parallele successive verso l'interno o l'esterno.
- Concentrico: l'utensile segue archi concentrici e paralleli; la distanza
   tra le passate � costante.
- Avanti e indietro: il percorso utensile alterna le direzioni durante le passate.";

M3xPocketProgressionMode.LongHelp="Indica la traiettoria dell'utensile in una tasca o in un'area esterna.
Pu� essere:
- Verso l'interno: l'utensile inizia dal punto all'interno dell'area e
segue percorsi verso l'interno paralleli al bordo.
- Verso l'esterno: l'utensile inizia dal punto all'interno dell'area e
segue percorsi verso l'esterno paralleli al bordo.
- Entrambe: per le tasche, l'utensile inizia da un punto all'interno della tasca e
segue percorsi verso l'esterno paralleli al bordo;
per le aree esterne, l'utensile parte da un punto sul bordo del grezzo e
segue percorsi verso l'interno paralleli al bordo.";

M3xStayOnBottom.LongHelp="Forza il contatto dell'utensile con la parte
inferiore della tasca nello spostamento da un dominio all'altro.";


M3xStockContouringPosition="Passata di contornatura";
M3xStockContouringPosition.LongHelp="Specifica la posizione della passata di contornatura";

M3xStockContouringRatio="Percentuale della passata di contornatura";
M3xStockContouringRatio.LongHelp="Regola la posizione della passata di contornatura per ottimizzare la rimozione delle creste (% del diametro dell'utensile)";

M3xStockContouringNumberOfLevel="Numero di contorni";
M3xStockContouringNumberOfLevel.LongHelp="Specifica il numero di passate di contornatura per ottimizzare il carico di lavoro dell'utensile";

M3xFMManagementMode.LongHelp="Specifica il modo di gestire l'utensile interamente innestato.
- Nessuna: nessuna azione specifica.
- Trochoid: sostituire il percorso corrente con un percorso trochoid.
- Passate multiple: aggiungere i piani di lavorazione per ridurre il sovraccarico di taglio.";

M3xMaxFMDepthOfCut.LongHelp="Specifica la distanza tra i piani di lavorazione nelle aree in cui l'utensile � interamente innestato.";
M3xFMTrochoidMinRadDist.LongHelp="Specifica il raggio minimo delle passate trochoid nelle aree in cui l'utensile � interamente innestato.";

MfgZigZagMode="Moto concentrico";
MfgZigZagMode.LongHelp="Specifica come si muove l'utensile in modo concentrico:
- Zig-Zag: l'utensile si sposta prima nella modalit� Taglio selezionata, quindi nella direzione opposta.
  Questa opzione riduce i tagli ad aria compressa.
- Direzione unica: l'utensile si sposta solo nella modalit� Taglio selezionata.";

MfgZigZagRatio="Profondit� radiale passate inverse";
MfgZigZagRatio.LongHelp="Si applica al moto concentrico Zig-Zag.
Definisce la profondit� di taglio radiale massima utilizzata per le passate nella direzione inversa.
Pu� essere diversa da quella utilizzata nella modalit� di taglio selezionata.";

MfgContouringPass="Passata di contornatura";
MfgContouringPass.LongHelp="Effettua un passaggio finale lungo la parte per rimuovere le creste.";
