//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    DrwDeleteWarnings
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog
// AUTHOR      :    mmr
// DATE        :    Jul. 05 1999
//------------------------------------------------------------------------------
// DESCRIPTION :    Resource File for NLS purpose related to Drafting Delete cmd
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//------------------------------------------------------------------------------

DefaultDeleteTitle="Conferma eliminazione";
DeleteInvalid="Eliminazione non valida";
CutInvalid="Taglio non valido";

//Drawing & Layout
//-------
DeleteDrawing="Non � possibile rimuovere un disegno";
DeleteLayout="Impossibile eliminare un layout durante la modifica.\nChiudere tutte le finestre di modifica del layout.";
CutLayout="Impossibile tagliare un layout";

//Sheet messages
//------------
DeleteSheet="Non � possibile rimuovere l'ultimo foglio";

SheetDeleteUndoNotAvailableWarningTitle="Conferma eliminazione fogli";
SheetDeleteUndoNotAvailableWarning="Impossibile annullare l'operazione di eliminazione di un foglio.
Continuare?";

DetailSheetsDeleteWarning="I fogli selezionati contengono dei dettagli utilizzati come ricorrenze in almeno una vista o un dettaglio.
Eliminandoli, verranno eliminate anche tutte le ricorrenze.
Continuare?";

DetailSheetsNoUndoDeleteWarning="I fogli selezionati contengono dei dettagli utilizzati come ricorrenze in almeno una vista o un dettaglio.
Eliminandoli, verranno eliminate anche tutte le ricorrenze. Questa operazione non � reversibile.
Continuare?";

//View
//------
ViewWithCalloutDeleteWarning="La vista selezionata contiene almeno un delimitatore utilizzato per definire un'altra vista.
Continuare?";

MainBackgroundViewDelete="Non � possibile rimuovere una vista di sfondo o principale";

//Details messages
//--------------
DetailDeleteTitle="Conferma eliminazione dettagli";
DetailDeleteWarning=" viene utilizzato come ricorrenza in almeno una vista o un dettaglio.
Eliminandolo, verranno eliminate anche tutte le ricorrenze.
Continuare?";

DetailsDeleteWarning="I dettagli selezionati sono utilizzati come ricorrenze in almeno una vista o un dettaglio.
Eliminandoli, verranno eliminate anche tutte le ricorrenze.
Continuare?";

//Datum curves
//-----------
DatumFeatureDeleteWarning="Taglia/Incolla non � un'operazione consigliata.\nPer nurbs o spline
congelate, copiarle ed incollarle prima di eliminare l'oggetto copiato 
originariamente.\nContinuare?";

//DrwDressup
//-----------
DrwDressupDelete="Non � possibile rimuovere una dettagliatura";
DrwAnndDecodeLeafDelete="Eliminazione oggetto interna non consentita";

//AreaFill
//-----------
AreaFillSupportGeometryDelete="Si desidera eliminare la geometria di supporto\ntratteggio?";
