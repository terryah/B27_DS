// COPYRIGHT ImpactXoft 2003
//===================================================================
//===========================================================================
// Feature spec-tree names
//===========================================================================

// All_CATISpd all features need a name
SpdBodyRef="Body";
SpdBoss="Borchia";
SpdCut="Taglio";
SpdCutout="Sagomatura";
SpdDraftProperties="Proprietą dello svuotamento";
SpdDivide="Dividi";
SpdEdgeFillet="raccordo su spigolo precedente";
SpdEdgeFilletR19="Raccordo su spigolo";
SpdExtract="Estrazione";
SpdExtractBehavior="Funzionamento dell'estrazione";
SpdExtractionProperties="Proprietą dell'estrazione";
SpdExtrude="Estrusione";
SpdFlange="Flangia";
SpdGrill="Griglia";
SpdImport="Importa";
SpdJoin="Unisci";
SpdLip="Orlo";
SpdMirror="Specchio";
SpdOffset="Offset";
SpdPattern="Matrice";
SpdPocket="Tasca";
SpdPPF="PPF";
SpdReinforcement="Rinforzo";
SpdRest="Riposo";
SpdRevolve="Rivoluzione";
SpdRib="Costa";
SpdShell="Proprietą del guscio";
SpdSnapLock="Blocco snap";
SpdThicken="Superficie spessa";
SpdXForm="Trasformazione";
SpdThicknessMod="Spessore";
SpdLLM="Modificatore locale live";

SpdXfs="Specifica funzionale esterna";

SpdDivideNLS="Dividi";
SpdExtrudeNLS="Sweep del prisma";
SpdPPFNLS="Montaggio bidirezionale";
//
SpdIntersecFilletInputHandle="Raccordo di intersezione";
SpdFilletInputHandleR19="Lembo";


// old Boolean Remove 
SpdCFSBoolean="Rimuovi CFS";

//===========================================================================
// CXR20/R208  New Generic Boolean (FastRun Compatibility)
//===========================================================================
SpdCFSGenericBoolean="Booleano CFS";
SpdCFSGenericBooleanCoreCutter="Taglio piastra portacavitą booleano CFS";
SpdCFSGenericBooleanSVCCutter="Taglio SVC booleano CFS";
SpdCFSGenericBooleanENVCutter="Taglio ENV booleano CFS";
SpdCFSGenericBooleanFNVCutter="Taglio FNV booleano CFS";
SpdCFSGenericBooleanStdRemove="Rimuovi booleano CFS";
SpdCFSGenericBooleanStdAdd="Aggiungi booleano CFS";
//===========================================================================

SpdJoinData="Dati unione";
SpdScrew="Vite";

SpdCustomScrew="Vite personalizzata";

//===========================================================================
// Feature alias names
//===========================================================================

SpdBodyRefSV="Body per svuotamento";
SpdBodyRefPV="Body esterno";
SpdBodyRefOSV="Body per corpo";
SpdBodyRefNVStar="Body interno";
SpdBodyRefFPV="Body aggiunto";
SpdBodyRefFNV="Body protetto";
SpdBodyRefRemove="Rimuovi body";
SpdBodyRefIntersect="Interseca body";

SpdDivideUpper="Dividi";
SpdDivideLower="Dividi";
SpdDivideBody="Dividi body";

SpdExtractCoreBody="Body per corpo";
SpdExtractCavityBody="Body per cavitą";
SpdExtractProtectedBody="Body protetto";
SpdExtractGrillBody="Body della griglia";
SpdExtractBody="Body di estrazione";

SpdExtractCoreFPV="Corpo estratto aggiunto";
SpdExtractCoreFNV="Corpo estratto aggiunto";

SpdExtractCavityFPV="Cavitą estratta aggiunta";
SpdExtractCavityFNV="Cavitą estratta protetta";

SpdExtractGrillFPV="Griglia estratta aggiunta";
SpdExtractGrillFNV="Griglia estratta protetta";

SpdExtractProtectedFPV="Aggiunta Estrazione Protezione";
SpdExtractProtectedFNV="Protezione Estrazione Protezione";

SpdExtractBehaviorSV="Funzionamento estrazione per svuotamento";
SpdExtractBehaviorPV="Funzionamento estrazione esterna";
SpdExtractBehaviorDCC="Funzionamento estrazione corpo";
SpdExtractBehaviorNVS="Funzionamento estrazione interno";
SpdExtractBehaviorFPV="Funzionamento estrazione aggiunto";
SpdExtractBehaviorFNV="Funzionamento estrazione protetto";

SpdExtrudeDistSV="Prima per svuotamento";
SpdExtrudeDistPV="Prisma esterno";
SpdExtrudeDistOSV="Prisma per corpo";
SpdExtrudeDistNVStar="Prisma interno";
SpdExtrudeDistFPV="Prisma aggiunto";
SpdExtrudeDistFNV="Prisma protetto";
SpdExtrudeDistRemove="Rimuovi prisma";
SpdExtrudeDistIntersect="Interseca prisma";

SpdExtrudePathSV="Sweep per svuotamento";
SpdExtrudePathPV="Sweep esterno";
SpdExtrudePathOSV="Sweep per corpo";
SpdExtrudePathNVStar="Sweep interno";
SpdExtrudePathFPV="Sweep aggiunto";
SpdExtrudePathFNV="Sweep protetto";
SpdExtrudePathRemove="Rimuovi sweep";
SpdExtrudePathIntersect="Interseca sweep";

SpdJoinHead="Testa";
SpdJoinThread="Filettatura";

SpdLipUpper="Orlo superiore";
SpdLipLower="Orlo inferiore";

SpdPatternRectangular="Matrice rettangolare";
SpdPatternCircular="Matrice circolare";
SpdPatternUserDefined="Matrice personalizzata";

SpdPPFPush="Spinta";
SpdPPFPull="Trazione";
SpdPPFFitting="Montaggio";

SpdRevolveSV="Rivoluzione per svuotamento";
SpdRevolvePV="Rivoluzione esterna";
SpdRevolveOSV="Rivoluzione per corpo";
SpdRevolveNVStar="Rivoluzione interna";
SpdRevolveFPV="Rivoluzione aggiunta";
SpdRevolveFNV="Rivoluzione protetta";
SpdRevolveRemove="Rimuovi rivoluzione";
SpdRevolveIntersect="Interseca rivoluzione";

SpdCantileverHookClip="Supporto per trave a sbalzo con gancio";
SpdCantileverLoopClip="Supporto per trave a sbalzo con loop";
SpdCantileverHookTrap="Blocco per trave a sbalzo con gancio";
SpdCantileverLoopCatch="Fermo per trave a sbalzo con loop";

SpdThickSurfaceSV="Superficie spessa per svuotamento";
SpdThickSurfacePV="Superficie spessa esterna";
SpdThickSurfaceOSV="Superficie spessa per corpo";
SpdThickSurfaceNVStar="Superficie spessa interna";
SpdThickSurfaceFPV="Superficie spessa aggiunta";
SpdThickSurfaceFNV="Superficie spessa protetta";
SpdThickSurfaceRemove="Rimuovi superficie spessa";
SpdThickSurfaceIntersect="Interseca superficie spessa";

SpdXFormRotate="Rotazione";
SpdXFormTranslate="Traslazione";
SpdXFormScaling="Scala";
SpdXFormSymmetry="Simmetria";
SpdXFormAxis="Da asse a asse";
SpdXFormAffinity="Affinitą";

//===========================================================================
// Feature parameter names
// All_CATISpd - All attributes that end up in Fx or Parent/Child
//               should have a name mapping
//===========================================================================

// Direction interface
SpdToDistanceLimitLength="Primo limite";
SpdFromDistanceLimitLength="Secondo limite";
SpdDirToSelectionOffset="OffsetPrimoLimite";
SpdDirFromSelectionOffset="OffsetSecondoLimite";
SpdMirrorExtent="SecondoLimiteSpecchiaPrimo";
SpdDirectionSymmetrical="RelazioneSimmetricaSuLimiti";

// Fillet interface
SpdFilletLateralRadius="RaggioLateraleRaccordo";
SpdFilletBottomRadius="PrimoRaggioRaccordo";
SpdFilletTopRadius="SecondoRaggioRaccordo";

// Draft interface
SpdDraft1stAngleLimit="AngoloPrimoLimiteSformo";
SpdDraft2ndAngleLimit="AngoloSecondoLimiteSformo";
SpdDraftSplitEdgeFilletRad="RaggioRaccordoSuSpigoliSformo";

SpdThickness="Spessore 1";
SpdThickness2="Spessore 2";
SpdSplitThickness="DividiSpessore";

// Wall interface
SpdWallThickness="SpessoreParete";
SpdWallBodyThickness="RelazioneSpessorePareteBody";

// 2ndWall interface
Spd2ndWallThickness="SpessorePareteLuceMinima";

// Revolve
SpdAngle="PrimoAngolo";
SpdAntiAngle="SecondoAngolo";
SpdAngleMirrored="SecondoAngoloSpecchiaPrimo";

// Shell
SpdShellBodyThickness="SpessoreBodySvuotamento";

// Grill feature
SpdHeight="Altezza";
SpdWidth="Larghezza";
SpdRibsHeight="AltezzaCoste";
SpdRibsWidth="LarghezzaCoste";
SpdRibsTopOffset="OggsetSuperioreCoste";
SpdSpikesHeight="AltezzaPunte";
SpdSpikesWidth="LarghezzaPunte";
SpdSpikesTopOffset="OffsetSuperiorePunte";
SpdSpikesBottomOffset="OffsetInferiorePunte";
SpdIslandThickness="SpessoreIsola";

// Reinforcement feature
SpdReinforcementHeight="Altezza";
SpdReinforcementWidth="Larghezza";

// Cut feature
SpdFilletCutRadius="RaggioRaccordoDiIntersezione";
SpdCutterOffset="OffsetElementoDiTaglio";

// Offset feature
SpdOffsetValue="ValoreDiOffset";

// Rib feature
SpdRibPostTaperAngle="ConicitąPosteriore";
SpdRibPostDiameter="DiametroPosteriore";

// Lip
SpdLipUpperClearance="LuceMinimaSuperiore";
SpdLipLowerClearance="LuceMinimaInferiore";
SpdLipHeight="Altezza";
SpdLipThickness="Spessore";
SpdLipAngle="Angolo";
SpdLipInnerAngle="AngoloInterno";
SpdLipEndExtension="Lunghezza";

// Pattern
SpdPatternFirstDirInstanceCount="NumeroDiRicorrenzeNellaPrimaDirezione";
SpdPatternFirstDirSpacingLength="DistanzaNellaPrimaDirezione";
SpdPatternFirstDirTotalLength="LunghezzaNellaPrimaDirezione";
SpdPatternFirstDirSpacingAngle="DistanzaAngolareNellaPrimaDirezione";
SpdPatternFirstDirTotalAngle="AngoloTotaleNellaPrimaDirezione";

SpdPatternSecondDirInstanceCount="NumeroDiRicorrenzeNellaSecondaDirezione";
SpdPatternSecondDirSpacingLength="DistanzaNellaSecondaDirezione";
SpdPatternSecondDirTotalLength="LunghezzaNellaSecondaDirezione";

SpdPatternKeyIndexInFirstDir="IndiceRicorrenzeChiaveNellaPrimaDirezione";
SpdPatternKeyIndexInSecondDir="IndiceRicorrenzeChiaveNellaSecondaDirezione";

SpdPatternKeyRotationAngle="RotazioneIntornoAllaRicorrenzaChiave";

// PPF
SpdClearanceThickness="SpessoreLuceMinima";

// Rest
SpdClearanceDraftAngle="AngoloDiDformoLuceMinima";

// Snap Lock
SpdSLPositionOffset="RitagliaOffsetDiPosizioni";
SpdSLCatchToleranceWidth="RilevaLarghezzaDiTolleranza";
SpdSLCatchToleranceEngage="RilevaContattoDiTolleranza";
SpdSLToleranceWidth="LarghezzaDiTolleranza";
SpdSLToleranceEngage="ContattoDiTolleranza";
SpdSLToleranceLength="LunghezzaTolleranza";
SpdSLToleranceOffset="OffsetTolleranza";
SpdSLToleranceDepth="ProfonditąTolleranza";
SpdSLUnderCutDepth="ProfonditąSottosquadraRitenzione";
SpdSLLength="Lunghezza";
SpdSLLoopLength="LunghezzaLoop";
SpdSLCatchLength="LunghezzaRilevamento";
SpdSLWidth="Larghezza";
SpdSLBeamThickAtWall="SpessoreSuParete";
SpdSLLoopBeamThickAtWall="LoopSpessoreSuParete";
SpdSLBeamAtTop="SpessoreDellaTraveInAlto";
SpdSLBeamThickAtRetention="SpessoreSuRitenzione";
SpdSLBeamWidthAtWall="LarghezzaSuParete";
SpdSLThickAtTop="SpessoreDellaTraveInAlto";
SpdSLLoopThickAtTop="SpessoreLoopInAlto";
SpdSLBeamWidthAtRetention="LarghezzaSuRitenzione";
SpdSLOffset="Offset";
SpdSLHeight="Altezza";
SpdSLThickAtBottom="LarghezzaLoop";
SpdSLThickAtSide1="SpessoreLoopSulLato1";
SpdSLThickAtSide2="SpessoreLoopSulLato2";
SpdSLRetentionLength="LunghezzaRitenzione";
SpdSLHookTipLength="LunghezzaPuntaDelGancio";
SpdSLRetentionAngle="AngoloDiRitenzione";
SpdSLInsertionAngle="AngoloDiInserimento";
SpdSLLoopRetentionAngle="AngoloDiRitenzioneLoop";
SpdSLLoopInsertionAngle="AngoloDiInserimentoLoop";
SpdSLLoopWidth="LarghezzaLoop";

// XForm
SpdXFormDistance="Distanza";
SpdXFormX="X";
SpdXFormY="Y";
SpdXFormZ="Z";
SpdXFormRotationAngle="Angolo";
SpdXFormScaleScale="Scala";
SpdXFormYScale="Scala Y";
SpdXFormZScale="Scala Z";

// Screw
SpdScrewHeadDiameter="DiametroDellaTesta";
SpdScrewShankDiameter="DiametroDelGambo";
SpdScrewThreadDiameter="DiametroDellaFilettatura";
SpdScrewHeadHeight="SpessoreDellaTesta";
SpdScrewShankHeight="SpessoreDelGambo";
SpdScrewThreadHeight="SpessoreDellaFilettatura";

// JoinData
SpdJoinHeadClearance="LuceMinimaDellaTesta";
SpdJoinHeadInnerDraft="AngoloDiSformoInternoDellaTesta";
SpdJoinHeadOuterDraft="AngoloDiSformoEsternoDellaTesta";

SpdJoinThreadClearance="LuceMinimaDellaFilettatura";
SpdJoinThreadInnerDraft="AngoloDiSformoInternoDellaFilettatura";
SpdJoinThreadOuterDraft="AngoloDiSformoEsternoDellaFilettatura";
SpdThreadWallThickness="SpessorePareteDellaFilettatura";
SpdJoinThreadDepth="LuceMinimaSpessoreDellaFilettatura";

SpdJoinClampGuideHeight="SpessoreDellaGuidaIncastro";
SpdJoinClampInnerDraft="AngoloDiSformoInternoIncastro";
SpdJoinClampSideClearance="LuceMinimaLateraleIncastro";
SpdJoinClampHeightClearance="LuceMinimaSpessoreIncastro";

SpdJoinShankSideClearance="LuceMinimaLateraleGambo";
SpdJoinShankInnerDraft="AngoloDiSformoInternoGambo";
SpdJoinShankHeightClearance="LuceMinimaSpessoreGambo";

// Join gusset added in R16
SpdJoinGussetExistOn="SoffiettoEsisteSu";
SpdJoinGussetInitialAngle="AngoloInizialeSoffietto";
SpdJoinGussetNumberOf="NumeroDiSoffietti";
SpdJoinGussetHeadOffset="OffsetTestaSoffietto";
SpdJoinGussetThreadOffset="OffsetFilettaturaSoffietto";
SpdJoinGussetShoulderWidth="LarghezzaSpallaSoffietto";
SpdJoinGussetThickness="SpessorePareteSoffietto";
SpdJoinGussetDraftAngle="AngoloDiSformoSoffietto";
SpdJoinGussetFlareAngle="AngoloAloneSoffietto";
SpdJoinGussetFilletRadius="RaggioRaccordoSoffietto";
SpdJoinGussetBlendRadius="RaggioDiPiegaSoffietto";

// Flange added in R16
SpdFlangeWidth="LarghezzaFlangia";
SpdFlangeThickness="SpessoreFlangia";
SpdFlangeTopRadius="RaggioSuperioreFlangia";
SpdFlangeBottomRadius="RaggioInferioreFlangia";
SpdFlangeSideRadius="RaggioLateraleFlangia";
SpdFlangeDraftAngle="AngoloDiSformoFlangia";

// Offset interface
SpdFaceCoreOffset="Offset corpo lunghezza";
SpdFaceBodyOffset="Offset body lunghezza";

// Intersection Fillet interface
SpdFilletRadius="Raggio del raccordo";
SpdRoundRadius="Raggio arrotondato";
SpdFilletIntersecMode="Tipo di raccordo di intersezione";
SpdUseFilletRad="IsFilletApplied";
SpdUseRoundRad="IsRoundApplied";
SpdPreserveThk="IsThicknessKept";

// Thickness Feature
SpdThicknessValue="Valore dello spessore";
SpdMaxDeviation="Deviazione massima";



SpdExtendAcressRemovedfaces="Estendi su facce rimosse";
SpdTrimToShell="Accorcia a svuotamento";


SpdDraftPartingEqNeutral="Partizione = Neutra";
SpdDraftBothSides="Sformo su entrambi i lati";
SpdDraftNeutralElement="Elemento neutro";
SpdDraftReverseAngle="Inverti angolo";
SpdDraftFilletBeforeDraft="Raccordi di sformo";
SpdWallConstantWallThickness="Spessore parete costante";
SpdReversed="Inverti direzione";
SpdDirectionMethod="Normale al profilo";
SpdFilletUseLateralRad="ConRaggioLateraleApplicato";
SpdFilletUseBottomRad="ConRaggioSoffittoApplicato";
SpdWallDirection="Direzione parete";
SpdConstantThickness="Spessore costante";
SpdReference="Riferimento";
SpdShellRemoveFaceList="Facce da rimuovere";
SpdShellThickFaceList="Facce spessore";
SpdShellThicknessList="Elenco spessori";
SpdCoreType="Tipo corpo";
SpdCoreShellReferencedBody="Body per corpo";
SpdCoreWallDirectionRef="Direzione parete corpo";
SpdCoreOffsetFaceList="Facce offset";
SpdCoreOffsetValueList="Elenco valori offset";
SpdExtendAcrossRemovedFaces="Estendi su facce rimosse";
SpdFilletProfileEnds="Estremitą profilo raccordo";
SpdDraftTypeAngleApplyFrom="Elemento neutro";
SpdComplement="Complemento";
SpdNonProtectedVolume="VolumeNonProtetto";
SpdFilletUseTopRad="ConSecondoRaggioApplicato";
SpdDivideReverseExtrude="InvertiDirezioneEstrusione";
SpdOnlyOutside="Solo esterno";
SpdClearanceVolume="Volume luce";
SpdProtectedVolume="Volume protetto";
SpdPathMoveProfileTo="Sposta profilo su percorso";
SpdClearanceList="Distanza";
SpdLimitPathWithPoints="Tra i punti";
SpdDraftIsOpenEndDrafted="Escludi sformo a estremitą profilo";
SpdFilletSkipOpenEndFaces="Escludi raccordo a estremitą profilo";
SpdRestRemoveBottom="Escludi con fondo";
SpdReverseClearanceAngle="Inverti angolo luce sformo";
SpdPatLimitsMaintained="Conserva le specifiche";
SpdPatternFirstDirReversed="Inverti 1a direzione";
SpdPatternSecondDirReversed="Inverti 2a direzione";
SpdXFormVectorDefType="Definizione vettore";
SpdWallType="TipoParete";
SpdDivideReversed="InvertiDirezione";
SpdDivideProtected="Protetto";
SpdDivideCore="Corpo";
SpdLipProtected="Volume protetto";
SpdFlangePullDirIsSplitter="NormaleAElementoDivisorio";
SpdLipPullDirectionIsSplitter="NormaleAElementoDivisorio";
SpdLipUseAllEdges="TuttoEsternoBordiDivisori";
SpdLipArrowReversed="Inverti direzione";
SpdLipAutoPosition="Sposta profilo su percorso";
SpdRibDoNotSplitThickness="Escludi fibra neutra";
SpdRibPostCreate="CreaPostAiPunti";
SpdExtensionType="Tipo estensione";
SpdDraftUse1stLimit="Comportamento sformo";
SpdLipPathControlType="Comparsa controllo profilo";
SpdLipClearanceLocation="Posizione luce";
SpdLipEdgeType="Tipo percorso";
SpdHole="Escludi con pavimento";
SpdFilletUseCutRad="Raggio intersezione";
SpdMethod="Metodo griglia";
SpdPathControlType="Tipo controllo profilo";
SpdToDistanceLimitType="Tipo primo limite";
SpdFromDistanceLimitType="Tipo secondo limite";
Spd2ndWallType="Tipo seconda parete";
SpdPPFnonProtectedVolume="Escludi volume protetto";
SpdLipMethodType="Tipo metodo linguetta";
SpdPatternFirstDirParametersUsed="Parametri 1a direzione";
SpdPatternSecondDirParametersUsed="Parametri 2a direzione";
SpdPatternRotationInstances="AllineamentoRadialeDelleRicorrenze";
SpdRibPostNeutralElement="Riferimento";
SpdFilletUseTopTad="IsTopRadApplied";

