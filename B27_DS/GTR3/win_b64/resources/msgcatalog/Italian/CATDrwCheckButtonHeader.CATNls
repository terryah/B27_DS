//=============================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwCheckButtonHeader
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// DATE        :    10.02.99
//------------------------------------------------------------------------------
// DESCRIPTION :    
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//	 01		  fgx	  01.03.99  Ajout de DrwStyle
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Bold
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwBold.Title="Grassetto" ;
CATDrwCheckButtonHeader.DrwBold.ShortHelp="Grassetto";
CATDrwCheckButtonHeader.DrwBold.Help="Applica l'attributo Grassetto ad un blocco di testo" ;
CATDrwCheckButtonHeader.DrwBold.LongHelp =
"Grassetto
Applica l'attributo Grassetto ad un blocco di testo.";

//------------------------------------------------------------------------------
// Italic
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwItalic.Title="Corsivo" ;
CATDrwCheckButtonHeader.DrwItalic.ShortHelp="Corsivo";
CATDrwCheckButtonHeader.DrwItalic.Help="Applica l'attributo Corsivo ad un blocco di testo" ;
CATDrwCheckButtonHeader.DrwItalic.LongHelp =
"Corsivo
Applica l'attributo Corsivo ad un blocco di testo.";

//------------------------------------------------------------------------------
// Underline
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwUnderline.Title="Sottolineato" ;
CATDrwCheckButtonHeader.DrwUnderline.ShortHelp="Sottolineato";
CATDrwCheckButtonHeader.DrwUnderline.Help="Sottolinea un blocco di testo" ;
CATDrwCheckButtonHeader.DrwUnderline.LongHelp =
"Sottolineato
Sottolinea un blocco di testo.";

//------------------------------------------------------------------------------
// Overline
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwOverline.Title="Linea sopra" ;
CATDrwCheckButtonHeader.DrwOverline.ShortHelp="Linea sopra";
CATDrwCheckButtonHeader.DrwOverline.Help="Inserisce una linea su un blocco di testo" ;
CATDrwCheckButtonHeader.DrwOverline.LongHelp =
"Linea sopra
Inserisce una linea su un blocco di testo.";

//------------------------------------------------------------------------------
// Strikethrough
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwStrikeThru.Title="Barrato" ;
CATDrwCheckButtonHeader.DrwStrikeThru.ShortHelp="Barrato";
CATDrwCheckButtonHeader.DrwStrikeThru.Help="Applica l'attributo Barrato ad un blocco di testo" ;
CATDrwCheckButtonHeader.DrwStrikeThru.LongHelp =
"Barrato
Applica l'attributo Barrato ad un blocco di testo.";

//------------------------------------------------------------------------------
// Superscript
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwSuperScript.Title="Apice" ;
CATDrwCheckButtonHeader.DrwSuperScript.ShortHelp="Apice";
CATDrwCheckButtonHeader.DrwSuperScript.Help="Applica l'attributo Apice ad un blocco di testo" ;
CATDrwCheckButtonHeader.DrwSuperScript.LongHelp =
"Apice
Applica l'attributo Apice ad un blocco di testo.";

//------------------------------------------------------------------------------
// Subscript
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwSubScript.Title="Pedice" ;
CATDrwCheckButtonHeader.DrwSubScript.ShortHelp="Pedice";
CATDrwCheckButtonHeader.DrwSubScript.Help="Applica l'attributo Pedice ad un blocco di testo" ;
CATDrwCheckButtonHeader.DrwSubScript.LongHelp =
"Pedice
Applica l'attributo Pedice ad un blocco di testo.";

//------------------------------------------------------------------------------
// Left Justification
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwJustifLeft.Title="Giustificazione a sinistra" ;
CATDrwCheckButtonHeader.DrwJustifLeft.ShortHelp="Giustificazione a sinistra";
CATDrwCheckButtonHeader.DrwJustifLeft.Help="Allinea a sinistra un blocco di testo" ;
CATDrwCheckButtonHeader.DrwJustifLeft.LongHelp =
"Giustificazione a sinistra
Allinea a sinistra un blocco di testo.";

//------------------------------------------------------------------------------
// Center Justification
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwJustifCenter.Title="Giustificazione al centro" ;
CATDrwCheckButtonHeader.DrwJustifCenter.ShortHelp="Giustificazione al centro";
CATDrwCheckButtonHeader.DrwJustifCenter.Help="Allinea al centro un blocco di testo" ;
CATDrwCheckButtonHeader.DrwJustifCenter.LongHelp =
"Giustificazione al centro
Allinea al centro un blocco di testo.";

//------------------------------------------------------------------------------
// Right Justification
//------------------------------------------------------------------------------
CATDrwCheckButtonHeader.DrwJustifRight.Title="Giustificazione a destra" ;
CATDrwCheckButtonHeader.DrwJustifRight.ShortHelp="Giustificazione a destra";
CATDrwCheckButtonHeader.DrwJustifRight.Help="Allinea a destra un blocco di testo" ;
CATDrwCheckButtonHeader.DrwJustifRight.LongHelp =
"Giustificazione a destra
Allinea a destra un blocco di testo.";



