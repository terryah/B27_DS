// COPYRIGHT DASSAULT SYSTEMES 2003
//=============================================================================
//
// DNBAnalysisSettingsFrame
//
//=============================================================================
// Usages Notes:
//  Naming the frame in the property sheet :
//    Tools/Options/DELMIA Infrastructure/Analysis 
//
//=============================================================================
// Mar. 2003  Creation                                                      MMH
//=============================================================================

// Feedback frame
FeedbackFrame.HeaderFrame.Global.Title="Risultato dell'analisi";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkBeepId.Title="Segnalazione acustica";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkBeepId.ShortHelp="Attiva/Disattiva segnalazione acustica";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkBeepId.LongHelp="Fare clic sull'opzione per abilitare/disabilitare la segnalazione acustica";

FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkSyncAnlSpecsId.Title="Sincronizza specifiche analisi";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkSyncAnlSpecsId.ShortHelp="Attiva/disattiva Sincronizza specifiche analisi";
FeedbackFrame.IconAndOptionsFrame.OptionsFrame.CkSyncAnlSpecsId.LongHelp="Fare clic sulla casella per abilitare/disabilitare la sincronizzazione delle specifiche di analisi";

// analysis mode activation frame
AnlModeFrame.HeaderFrame.Global.Title="Attivazione modalit� di analisi:";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkAskAnlModeId.Title="Chiedi autorizzazione per attivare modalit� di analisi";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkAskAnlModeId.ShortHelp="Chiedi autorizzazione per attivare modalit� di analisi";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkAskAnlModeId.LongHelp="Fare clic per abilitare/disabilitare la finestra";

AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkEnableAnlModeId.Title="Attivazione automatica modalit� di analisi";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkEnableAnlModeId.ShortHelp="Attiva automaticamente la modalit� di analisi";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkEnableAnlModeId.LongHelp="Fare clic sull'opzione per abilitare/disabilitare l'attivazione automatica della modalit� di analisi";

AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkDisplayAnlStatusId.Title="Finestra Visualizza stato dell'analisi";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkDisplayAnlStatusId.ShortHelp="Finestra Visualizza automaticamente stato dell'analisi";
AnlModeFrame.IconAndOptionsFrame.OptionsFrame.CkDisplayAnlStatusId.LongHelp="Fare clic per abilitare/disabilitare la finestra Visualizza stato dell'analisi";

// analysis status frame
AnlStatusFrame.HeaderFrame.Global.Title="Livello di analisi";
AnlStatusFrame.IconAndOptionsFrame.OptionsFrame.ComboAnlStatusId.ShortHelp="Livello di analisi predefinito";
AnlStatusFrame.IconAndOptionsFrame.OptionsFrame.ComboAnlStatusId.LongHelp="Specifica il valore predefinito per il livello di analisi";

// visualization frame
VisuModeFrame.HeaderFrame.Global.Title="Modalit� visualizzazione";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboVisuId.ShortHelp="Modalit� visualizzazione predefinita";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboVisuId.LongHelp="Selezionare la modalit� di visualizzazione dei risultati dell'analisi delle interferenze";

VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkColorMode.Title="Evidenzia interferenze e gioco con colori diversi";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkColorMode.ShortHelp="Evidenzia interferenze e gioco con colori diversi";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkColorMode.LongHelp="Evidenzia interferenze e gioco con colori diversi";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkWhiteMode.Title="Parti predefinite in bianco";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkWhiteMode.ShortHelp="Parti predefinite in bianco";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.CkWhiteMode.LongHelp="Parti predefinite in bianco";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.LabelClashColor.Title="Colore interferenza";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClashColor.ShortHelp="Colore interferenza";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClashColor.LongHelp="Colore interferenza";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.LabelClearanceColor.Title="Colore gioco";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClearanceColor.ShortHelp="Colore gioco";
VisuModeFrame.IconAndOptionsFrame.OptionsFrame.ComboClearanceColor.LongHelp="Colore gioco";

// Voxel frame
VoxelFrame.HeaderFrame.Global.Title="Analisi statica";
VoxelFrame.IconAndOptionsFrame.OptionsFrame.CkVoxelId.Title="Utilizza voxel";
VoxelFrame.IconAndOptionsFrame.OptionsFrame.CkVoxelId.ShortHelp="Abilita o disabilita i voxel durante l'analisi statica (rilevazione delle collisioni)";
VoxelFrame.IconAndOptionsFrame.OptionsFrame.CkVoxelId.LongHelp="Specifica se utilizzare i voxel per la determinazione delle collisioni durante l'analisi statica";

// Analysis Type Window

AnlTypeID="Tipo di analisi";
AnlActiveID="Attivo";

AnlIntDistId.ActType="Distanza analisi";
AnlIntfId.ActType="Interfaccia analisi";
AnlMeasureId.ActType="Misura analisi";
AnlTypeTravelId.ActType="Limite percorso";
AnlTypeVelocityId.ActType="Limite velocit�";
AnlTypeAccelId.ActType="Limite di accelerazione";
AnlTypeCautionId.ActType="Area di attenzione";
AnlTypeLinSpeedId.ActType="Limite velocit� lineare";
AnlTypeRotSpeedId.ActType="Limite velocit� di rotazione";
AnlTypeLinAccelId.ActType="Limite accelerazione lineare";
AnlTypeRotAccelId.ActType="Limite accelerazione di rotazione";
AnlTypeIOAnalysisId.ActType="Stato risorse in attesa";
