// ==============================================================================================
// Messages STEP
// 
// 16/10/2002: JHH : Creation
// 25/06/2003: ARH : InvalidPositionOfComponent
// 08/09/2003: ARH : OldStyleAssemblyStructure et DuplicatedProduct
// 05/01/2004: ARH : DuplicatedProduct a l'export
// 08/04/2004: ARH : RI446208: message
// 04/06/2004: ARH : RI453883: message AssemblyCycle et AssemblyAsPart
// 08/06/2004: ARH : RI455372: message WrongHeader
// 20/07/2004: JHH : Modification message OldStyleAssemblyStructure (devient une information)
// 22/10/2004: ARH : RI470187: message ErrorWriting
// 16/05/2006: ARH : RI534401: message GVPofPart
// 17/01/2007: ARH : messages Version
// 14/09/2007: ARH : RI592111: message DuplicateProductKO
// 21/09/2007: ARH : message LightProduct
// 22/01/2008: ARH : messages import des FTA
// 22/04/2008: YFZ : Traduction pour le HEADER STEP
// 16/09/2008: ARH : Messages pour FTA
// 25/08/2009: ARH : RI11316: message pour FTA invalide sans vue
// 09/09/2009: ARH : message FTA_ValidationPropertiesKO
// 23/02/2010: ARH : IR-040076: Ajout message WarningNoLicense
// 29/12/2010: ARH : messages UDA_VP
// 20/07/2012: YFZ: Message pour avertire que la convertion de la geometries step ne sera pas faite. IR-169572V5-6R2013WIM
// 02/10/2012: ARH: Message CATStepMsg.Exp.FTA_EmptyView
// 09/07/2013: ARH: Message CATStepMsg.Imp.FTA_ValidationProperties3
// 15/11/2013: ARH: Messages CATStepMsg.Imp.Tessellation_TolerancesVP*
// 17/12/2013: ARH: Message CATStepMsg.ProcessingDate
// 23/07/2015: ACS1: Message Export STEP XML
// ==============================================================================================

CATStepMsg.Exp.Version="Versione preprocessore: ";
CATStepMsg.Imp.Version="Versione postprocessore: ";

CATStepMsg.ProcessingDate="Data elaborazione: ";

CATStepMsg.Imp.InvalidStepFile="<E> Errore rilevato durante la lettura del file STEP: ";

CATStepMsg.Imp.WrongHeader="<E> File STEP non valido: intestazione incompleta";

CATStepMsg.Exp.ErrorWriting="<E> Errore rilevato durante la scrittura del file STEP";

CATStepMsg.Imp.TooManySDAIerrors="<E> Pi� di 50 errori rilevati nel file STEP. Ulteriori errori non verranno riportati.";

CATStepMsg.SimpleMsg="/p1";

CATStepMsg.Exp.InvalidPositionOfComponent="<E> La ricorrenza '/p1' del prodotto '/p2' � stata posizionata in modo non corretto da /p3";

CATStepMsg.Exp.InvalidPositionOfComponentTess="<E> Entit� posizionate in modo errato da /p1";

CATStepMsg.Imp.OldStyleAssemblyStructure="<I> Struttura dell'assieme obsoleta.";

CATStepMsg.Imp.DuplicatedProduct="<E> Esistono diversi prodotto denominati '/p1' nella struttura dell'assieme";

CATStepMsg.Exp.DuplicatedProduct="<W> Il prodotto duplicato denominato '/p1' � stato rinominato come '/p2'";

CATStepMsg.Exp.DuplicatedProductKO="<E> Il prodotto denominato '/p1' � definito due volte. Verr� esportata solo la prima definizione";

CATStepMsg.Exp.FlexibleProduct="<W> Il prodotto flessibile denominato '/p1' viene esportato come nuovo prodotto";

CATStepMsg.Exp.LightProduct="<W> Il prodotto light denominato '/p1' viene esportato come parte";

CATStepMsg.Exp.MissingDocument="<E> Manca un documento associato al numero parte '/p1'";

CATStepMsg.Exp.WrongWire="<E> Figura '/p1' non esportata";

CATStepMsg.Imp.AssemblyCycle="<E> Si � verificato un ciclo nella struttura dell'assieme tra i prodotti /p1 e /p2";

CATStepMsg.Imp.AssemblyAsPart="<E> La struttura dell'assieme � errata. Viene trascritta solo la geometria.";

CATStepMsg.Imp.MatrixInvalide="<W> Una ricorrenza del prodotto '/p1' presenta una ubicazione non corretta. La matrice non � valida";

CATStepMsg.Exp.GVPofPart="Propriet� di convalida geometrica a livello di parte:";

CATStepMsg.Exp.TVPofPart="Propriet� di convalida verifica dati tessellati 3D a livello di parte:";

CATStepMsg.Imp.FTA_Unknown="<E> L'annotazione 3D /p1 dispone di un tipo STEP sconosciuto /p1";

CATStepMsg.Imp.FTA_Empty="<E> L'annotazione 3D /p1 non dispone di una rappresentazione grafica";

CATStepMsg.Exp.FTA_NoView="<E> Nessuna vista di annotazione 3D trovata per l'annotazione 3D dell'intervallo /p2 in /p1. Eliminare CATPart mediante CATDUA con la regola FTA_12 prima dell'esportazione";

CATStepMsg.Exp.FTA_EmptyView="<W> La vista Annotazione 3D /p1 � vuota. Non � supportata da STEP.";

CATStepMsg.Imp.FTA_NoLinks="<W> L'annotazione 3D /p1 non dispone di collegamenti geometrici";

CATStepMsg.Imp.FTA_ValidationPropertiesKO="<E> Errore delle propriet� di convalida per l'annotazione 3D /p1";

CATStepMsg.Imp.FTA_ValidationProperties1="Numero previsto di annotazioni 3D: /p1 , trovato: /p2";

CATStepMsg.Imp.FTA_ValidationProperties2="Numero previsto di catture: /p1 , trovato: /p2";

CATStepMsg.Imp.FTA_ValidationProperties="Verifica delle propriet� di convalida dell'annotazione 3D:";

CATStepMsg.Imp.FTA_ValidationProperties3="Cattura: /p1 - Numero previsto di annotazioni 3D: /p2, trovate: /p3";

CATStepMsg.Imp.FTA_TolerancesVP="Lunghezza curve annotazione 3D e deviazione massima centroide: /p1 mm";

CATStepMsg.Imp.Tessellation_TolerancesVP1="Deviazione massima area superficie tessellata: /p1% o /p2 mm2";

CATStepMsg.Imp.Tessellation_TolerancesVP2="Deviazione massima lunghezza curva tessellata: /p1% o /p2 mm";

CATStepMsg.Imp.Tessellation_TolerancesVP3="Deviazione massima centroide curva o superficie tessellata: /p1 mm";

CATStepMsg.Imp.AVP_Tolerance="Deviazione massima baricentro teorico: /p1 mm";

// Traduction pour le HEADER STEP

CATStepMsg.Imp.OriginatingSystem="Sistema di origine                               : ";

CATStepMsg.Imp.PreprocessorVersion="Versione preprocessore                             : ";

CATStepMsg.Imp.FileSchema="Schema di file                                      : ";

CATStepMsg.Exp.WarningNoLicense="Licenza STEP non garantita. Confermare l'esportazione STEP solo della struttura di prodotto";

CATStepMsg.Imp.WarningNoLicense="Licenza STEP non concessa. STEP di geometrie non convertito.";

CATStepMsg.Exp.WarningTitle="Rivolgersi all'amministratore.";

CATStepMsg.Imp.UDA_ValidationProperties="Controllo delle propriet� di verifica UDA per il prodotto /p1";

CATStepMsg.Imp.UDA_ComputedReadProperties="/p1 - Propriet� calcolata: /p2 - Propriet� di lettura: /p3";

CATStepMsg.Imp.UDA_StatusOK="/p1 - Stato UDA: OK";

CATStepMsg.Imp.UDA_StatusKO="/p1 - Stato UDA: KO";

CATStepMsg.Imp.UDA_Part="Parte";

CATStepMsg.Imp.UDA_Point="Punto";

CATStepMsg.Imp.UDA_Wire="Figura";

CATStepMsg.Imp.UDA_Shell="Svuotamento";

CATStepMsg.Imp.UDA_Solid="Solido";

CATStepMsg.Imp.UDA_String="Stringa";

CATStepMsg.Imp.UDA_Integer="Intero";

CATStepMsg.Imp.UDA_Real="Reale";

CATStepMsg.Imp.UDA_Boolean="Booleano";

CATStepMsg.Imp.Warning_UDA_unsupported="AVVERTENZA: /p1 UDA importati come stringa anzich� come misura con unit�";

CATStepMsg.Exp.WrongUnitForStpx="<W> L'unit� utilizzata nell'assieme � mm, non pollici";

ExportOneFile_stpx.Request="Impossibile esportare";
ExportOneFile_stpx.Diagnostic="L'esportazione del file STEP in formato XML non � compatibile con l'opzione Un file STEP";
ExportOneFile_stpx.Advice="Nel pannello dell'opzione STEP, selezionare il protocollo di applicazione 242 e un tipo diverso da \"Un file STEP\" per l'assieme esportato";

ExportWrongAP_stpx.Request="Impossibile esportare";
ExportWrongAP_stpx.Diagnostic="L'esportazione del file STEP in formato XML � disponibile solo con il protocollo di applicazione 242 (AP242)";
ExportWrongAP_stpx.Advice="Scegliere AP242 nel pannello dell'opzione STEP";


