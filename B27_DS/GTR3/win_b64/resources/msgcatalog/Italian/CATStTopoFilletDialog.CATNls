//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStTopoFilletDialog: Resource file for NLS purpose related to TopoFillet command
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// March 05   Creation                                    Ritesh Kanetkar
//=============================================================================
DialogBoxTitle="Raccordo di stile";
TabCont.GeneralTab.Title="Immissioni di raccordo";
TabCont.ApproxTab.Title="Approssima";

TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="Tolleranza";
TabCont.GeneralTab.MainFrame.RadiusFrame.Title="Raggio";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.Title="Tipo di arco";
TabCont.GeneralTab.MainFrame.ParameterFrame.Title="Parametro";
TabCont.GeneralTab.MainFrame.ResultFrame.Title="Risultato";
TabCont.GeneralTab.MainFrame.OptionFrame.Title="Opzione";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PDefault.Title="Predefinito";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch1.Title="Patch 1";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch2.Title="Patch 2";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PAverage.Title="Medio";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PBlend.Title="Connessione";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PChordal.Title="Cordale";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimFace.Title="Faccia di relimitazione";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimApprox.Title="Approssimato";

TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSStitch.Title="Cucitura";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSRip.Title="Scucitura";

TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="Continuit� G0 tra gli elementi di immissione e del raccordo";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="Continuit� G1 tra gli elementi di immissione e del raccordo";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="Continuit� G2 tra gli elementi di immissione e del raccordo";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="Continuit� G3 tra gli elementi di immissione e del raccordo";


TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="Raggio costante";
TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="Raggio costante del raccordo";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="Raggio minimo";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="Raggio minimo ON/OFF";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="Valore raggio minimo";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="Valore raggio minimo";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="Raggio relativo";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="Raggio relativo ON/OFF";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.ShortHelp="Raccordo variabile/parziale";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.LongHelp="Visualizza il manipolatore per il raccordo variabile/parziale";

TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.ShortHelp="Connessione";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.LongHelp="Crea una superficie di connessione tra gli elementi di immissione";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.ShortHelp="Approssimato";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.LongHelp="Crea un'approssimazione Bezier circolare tra gli elementi di immissione";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.ShortHelp="Esatta";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.LongHelp="Crea una superficie razionale con le sezioni circolari vere";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.ShortHelp="Parametro";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.LongHelp=" Opzioni diverse per la parametrizzazione della superficie di raccordo";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.ShortHelp="Risultato della relimitazione";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.LongHelp="Opzione del risultato della relimitazione ON/OFF";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.ShortHelp="Relimita faccia / Approssimazione della relimitazione";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.LongHelp="Tipo dell'opzione del risultato di relimitazione Faccia di relimitazione - Il risultato � una faccia
                                                               Approssimazione della relimitazione - Il risultato non � una faccia";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.ShortHelp="Estrapola";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.LongHelp="Estrapola la superficie di raccordo risultante";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.ShortHelp="Superficie piccola";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.LongHelp="Se i gruppi di immissione hanno pi� superfici e i bordi delle superfici non sono sulla
                                                              stessa posizione, il risultato � una superficie (molto) piccola. Modalit� ON/OFF";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.ShortHelp="Cucitura/Scucitura";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.LongHelp="Cucitura: la superficie piccola pu� essere cucita con una delle superfici adiacenti
                                                                   Scucitura: la superficie piccola � divisa (dalla sua diagonale) in due superfici";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.ShortHelp="Tolleranza piccola superficie";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.LongHelp="Tolleranza per creare la piccola superficie";

TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.ShortHelp="Relimita immissione";
TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.LongHelp="Relimita gli elementi di immissione del raccordo";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.ShortHelp="Raccordo cordale";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.LongHelp="La distanza tra i bordi del raccordo in direzione della rolling ball � la distanza cordale";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.ShortHelp="Punti di controllo";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.LongHelp="Abilita le modifiche alla shape della superficie di raccordo";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.ShortHelp="Minimo reale";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.LongHelp="Il raggio minimo � controllato";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.ShortHelp="Approssimazione indipendente";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.LongHelp="Approssimazione interna indipendente";
// New UI

MainOuterFrameTabs.TabCont.GeneralTab.Title="Opzioni";
// MainOuterFrameTabs.TabCont.ApproxTab.Title = "Advanced"; // WAP 08:08:26 R19SP4 HL2 - Nls entry commented.


// Continuity Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="Continuit�";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="Continuit� G0 tra gli elementi di immissione e del raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="Continuit� G1 tra gli elementi di immissione e del raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="Continuit� G2 tra gli elementi di immissione e del raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="Continuit� G3 tra gli elementi di immissione e del raccordo";

// Radius Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Title="Parametri del raggio";
//MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.Title = "Radius :"; // WAP 08:11:25 R19SP3 HL
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.ShortHelp="Raggio costante";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.LongHelp="Raggio costante del raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="Raggio costante";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="Raggio costante del raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.Title="Raggio min.:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="Raggio minimo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="Raggio minimo ON/OFF";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="Valore raggio minimo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="Valore raggio minimo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="Raggio relativo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="Raggio relativo ON/OFF";

// Arc Type 
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.Title="Tipo di arco";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.ShortHelp="Connessione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.LongHelp="Crea una superficie di connessione tra gli elementi di immissione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.ShortHelp="Approssimato";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.LongHelp="Crea un'approssimazione Bezier circolare tra gli elementi di immissione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.ShortHelp="Esatta";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.LongHelp="Crea una superficie razionale con le sezioni circolari vere";


MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.Title="Risultato";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Title="Semplificazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.Title="Cucitura";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.ShortHelp="Superficie piccola - Cucitura";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.LongHelp="Cucitura: la superficie piccola pu� essere cucita con una delle superfici adiacenti";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.Title="Scucitura";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.ShortHelp="Superficie piccola - Scucitura";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.LongHelp="Scucitura: la superficie piccola � divisa (con la sua diagonale) in due superfici";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.ShortHelp="Tolleranza piccola superficie";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.LongHelp="Tolleranza per creare la piccola superficie";

// Relimitation Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.Title="Relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.Title="Faccia di relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.ShortHelp="Faccia di relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.LongHelp="Il risultato � una faccia";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.Title="Approssimazione della relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.ShortHelp="Approssimazione della relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.LongHelp="Il risultato non � una faccia";
// For R18SP3 UI
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Title="Geometria";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.ShortHelp="Estrapola su lato 1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.LongHelp="Il risultato viene estrapolato sul lato 1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.ShortHelp="Estrapola su lato 2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.LongHelp="Il risultato viene estrapolato sul lato 2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.ShortHelp="Relimita su lato 1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.LongHelp="Il risultato viene relimitato sul lato 1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.ShortHelp="Relimita su lato 2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.LongHelp="Il risultato viene relimitato sul lato 2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.Title="Faccia di relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.ShortHelp="Faccia di relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.LongHelp="Il risultato � una faccia";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.Title="Approssimazione della relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.ShortHelp="Approssimazione della relimitazione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.LongHelp="Il risultato non � una faccia";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.ShortHelp="Concatena";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.LongHelp="Concatena il lembo di raccordo a celle multiple in un risultato a cella singola (solo possibile in caso di continuit� G2 tra le celle)";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Title="Geometria";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.Title="Estrapola";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.ShortHelp="Estrapola";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.LongHelp="Estrapola la superficie di raccordo risultante";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.ShortHelp="Concatena";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.LongHelp="Concatena il lembo di raccordo a celle multiple in un risultato a cella singola (solo possibile in caso di continuit� G2 tra le celle)";
//--------------------------------------

// Options Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.Title="Tipo di raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.ShortHelp="Raccordo variabile/parziale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.LongHelp="Visualizza il manipolatore per il raccordo variabile/parziale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.ShortHelp="Relimita immissione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.LongHelp="Relimita gli elementi di immissione del raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.ShortHelp="Raccordo cordale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.LongHelp="La distanza tra i bordi del raccordo in direzione della rolling ball � la distanza cordale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.ShortHelp="Punti di controllo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.LongHelp="Abilita le modifiche alla shape della superficie di raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.ShortHelp="Minimo reale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.LongHelp="Il raggio minimo � controllato";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.ShortHelp="Approssimazione indipendente";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.LongHelp="Approssimazione interna indipendente";


// Surface-1 Curv-1
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.Title="Curva:";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.ShortHelp="Seleziona gruppo 1 curve";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.LongHelp="Seleziona il primo gruppo di curve per l'operazione di raccordo";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.ShortHelp="Elemento di relimitazione 1";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.LongHelp="Relimita gli elementi di immissione (superfici) nel gruppo Riferimento 1 del raccordo.";

// Surface-2 Curv-2
MainOuterFrame.Surface2Curve2Frame.Title="Riferimento 2";
MainOuterFrame.Surface2Curve2Frame.ShortHelp="Seleziona gruppo 2 superfici e curve";
MainOuterFrame.Surface2Curve2Frame.LongHelp="Seleziona il secondo gruppo di superfici e curve per l'operazione di raccordo";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.Title="Superficie:";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.ShortHelp="Seleziona gruppo 2 superfici";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.LongHelp="Seleziona il secondo gruppo di superfici per l'operazione di raccordo";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.Title="Curva:";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.ShortHelp="Seleziona gruppo 2 curve";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.LongHelp="Seleziona il secondo gruppo di curve per l'operazione di raccordo";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.ShortHelp="Relimita riferimento 2";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.LongHelp="Relimita gli elementi di immissione (superfici) nel gruppo Riferimento 2 del raccordo.";

// Point-Spine
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.ShortHelp="Seleziona gruppo di dorsale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.LongHelp="Seleziona la dorsale per l'operazione di raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.ShortHelp="Seleziona punto sulla dorsale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.LongHelp="Seleziona un punto sulla dorsale per l'operazione di raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.Title="Dorsale:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.ShortHelp="Seleziona gruppo di dorsale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.LongHelp="Seleziona la dorsale per l'operazione di raccordo";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.Title="Punto:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.ShortHelp="Seleziona punto sulla dorsale";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.LongHelp="Seleziona un punto sulla dorsale per l'operazione di raccordo";

// Parameters
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.Title="Parametro";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.ShortHelp="Parametro";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.LongHelp=" Opzioni diverse per la parametrizzazione della superficie di raccordo";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PDefault.Title="Predefinito";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch1.Title="Patch 1";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch2.Title="Patch 2";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PAverage.Title="Medio";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PBlend.Title="Connessione";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PChordal.Title="Cordale";

// Independent Approximation
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.Title="Approssimazione indipendente";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.ShortHelp="Approssimazione indipendente";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.LongHelp="Approssimazione interna indipendente";

Surface1BagTitle.Message="Selezionare la superficie 1";
Curve1BagTitle.Message="Selezionare la curva 1";
Surface2BagTitle.Message="Selezionare la superficie 2";
Curve2BagTitle.Message="Selezionare la curva 2";
SpineBagTitle.Message="Selezionare la dorsale";
PointOnSpineBagTitle.Message="Selezionare un punto sulla dorsale";

//Propagate contextual menu
Propagate.Message="Propagazione tangenziale"; //AV7 11:01:17

MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.Title="Visualizzazione della deviazione";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.ShortHelp="Connessione tra le celle del raccordo";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.LongHelp="Visualizza la connessione solo tra le celle del lembo di raccordo";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.ShortHelp="Connessione tra il lembo di raccordo e il riferimento";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.LongHelp="Visualizza la connessione tra il lembo di raccordo e il riferimento";
MaxDevOptionsFrame.MaxDevValueFrame.Title="Deviazione massima";
MaxDevOptionsFrame.MaxDevValueFrame.LongHelp="Visualizza il massimo del valore massimo per ciascuna continuit�";
MaxDevOptionsFrame.MaxDevValueFrame.G0TitleLabel.Title="G0:";
MaxDevOptionsFrame.MaxDevValueFrame.G0Label="----   ";
MaxDevOptionsFrame.MaxDevValueFrame.G1TitleLabel.Title="G1:";
MaxDevOptionsFrame.MaxDevValueFrame.G2TitleLabel.Title="G2:";
MaxDevOptionsFrame.MaxDevValueFrame.G3TitleLabel.Title="G3:";

// Start WAP 08:08:26 R19SP4 HL1 AND HL2

NewApproxTab.Message="Approssima";
ApproxTab.Message="Avanzato";
OutputResultFrame.Title="Risultato emissione";
OutputResultFrame.MaxOrderTitleLabel.Title="Grado massimo:";
OutputResultFrame.MaxSegTitleLabel.Title="Numero massimo di segmenti per cella:";
OutputResultFrame.OrdUTitleLabel.Title="U:";
OutputResultFrame.OrdVTitleLabel.Title="V:";
OutputResultFrame.SegUTitleLabel.Title="U:";
OutputResultFrame.SegVTitleLabel.Title="V:";
//OutputResultFrame.NbOfCellsTitleLabel.LongHelp = "Displays the Number of cells in entire fillet result created";
OutputResultFrame.MaxOrderTitleLabel.ShortHelp="Visualizza il grado massimo nelle direzioni U e V";
//OutputResultFrame.MaxOrderTitleLabel.LongHelp = "Displays the maximum order of the created entire fillet result in both U and V directions"; 
OutputResultFrame.MaxSegTitleLabel.ShortHelp="Visualizza il numero massimo di segmenti per cella nelle direzioni U e V";
//OutputResultFrame.MaxSegTitleLabel.LongHelp = "Displays the maximum number of segments per cell of the created entire fillet result in both U and V directions"; 
// End WAP 08:08:26	

// Start WAP 08:11:25 R19SP3 HL
LengthLabel.Message="Lunghezza:";
RadiusLabel.Message="Raggio:";
// End WAP 08:11:25

// Start WAP 08:12:02 R19SP4 HL6
//Spine - Point Frame	
MainOuterFrame.SpinePointFrame.LabelSpine.Title="Dorsale:";
MainOuterFrame.SpinePointFrame.LabelPoint.Title="Punto:";
MainOuterFrame.SpinePointFrame.SpineSelector.ShortHelp="Seleziona gruppo di dorsale";
MainOuterFrame.SpinePointFrame.SpineSelector.LongHelp="Seleziona la dorsale per l'operazione di raccordo";
MainOuterFrame.SpinePointFrame.PointSelector.ShortHelp="Seleziona punto sulla dorsale";
MainOuterFrame.SpinePointFrame.PointSelector.LongHelp="Seleziona un punto sulla dorsale per l'operazione di raccordo";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.Title="Estrapola/Relimita su lato 1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.ShortHelp="Estrapola/Relimita su lato 1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.LongHelp="Il risultato � estrapolato/relimitato sul lato 1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.Title="Estrapola/Relimita su lato 2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.ShortHelp="Estrapola/Relimita su lato 2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.LongHelp="Il risultato � estrapolato/relimitato sul lato 2";

// Result Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.Title="Unione logica";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.ShortHelp="Unione logica";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.LongHelp="Assembla il risultato (lembo di raccordo, superfici di immissione relimitate) con tolleranza fissa 0,1 mm";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.Title="Concatena";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.ShortHelp="Concatena";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.LongHelp="Concatena il lembo di raccordo a celle multiple in un risultato a cella singola (solo possibile in caso di continuit� G2 tra le celle)";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.Title="Unione logica";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.ShortHelp="Unione logica";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.LongHelp="Assembla il risultato (lembo di raccordo, superfici di immissione relimitate) con tolleranza fissa 0,1 mm";
// End WAP 08:12:02

// Start WAP 09:01:23 R19SP4 HL5
Curve1SelectorShortHelp.Message="Seleziona gruppo 1 curve";
Curve1SelectorLongHelp.Message="Seleziona il primo gruppo di curve per l'operazione di raccordo";
Curve1SelStateShortHelp.Message="Abilita gruppo 1 curve";
Curve1SelStateLongHelp.Message="Abilita la selezione nel gruppo 1 di curve.";

Curve2SelectorShortHelp.Message="Seleziona gruppo 2 curve";
Curve2SelectorLongHelp.Message="Seleziona il secondo gruppo di curve per l'operazione di raccordo";
Curve2SelStateShortHelp.Message="Abilita gruppo 2 curve";
Curve2SelStateLongHelp.Message="Abilita la selezione nel gruppo 2 di curve.";


EdgesToExtrapol1SelectorShortHelp.Message="Seleziona spigolo 1 per estrapolazione";
EdgesToExtrapol1SelectorLongHelp.Message="Seleziona il primo gruppo di spigoli da estrapolare per l'operazione di raccordo variabile/parziale";
EdgesToExtrapol1SelStateShortHelp.Message="Abilita spigolo 1 per estrapolazione";
EdgesToExtrapol1SelStateLongHelp.Message="Abilita la selezione del primo gruppo di spigoli da estrapolare per l'operazione di raccordo variabile/parziale";
EdgesToExtrapol2SelectorShortHelp.Message="Seleziona spigolo 2 per estrapolazione";
EdgesToExtrapol2SelectorLongHelp.Message="Seleziona il secondo gruppo di spigoli da estrapolare per l'operazione di raccordo variabile/parziale";
EdgesToExtrapol2SelStateShortHelp.Message="Abilita spigolo 2 per estrapolazione";
EdgesToExtrapol2SelStateLongHelp.Message="Abilita la selezione del secondo gruppo di spigoli da estrapolare per l'operazione di raccordo variabile/parziale";

// End WAP 09:01:23

// Start WAP 08:12:02 R19SP5
// Input Parameter Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.Title="Parametri di immissione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.LabelMergeDist.Title="Tolleranza di unione: ";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.ShortHelp="Tolleranza di unione";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.LongHelp="Imposta la tolleranza di unione per il raccordo a celle multiple";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.ShortHelp="Tolleranza curva su supporto";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.LongHelp="Utilizza Tolleranza curva su supporto";
TolerantLaydown.Title="Tolleranza curva su supporto";
// End WAP 08:12:02					   

MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.Title="Tipo di raccordo";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.ShortHelp="Tipo di raccordo";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.LongHelp="Seleziona il tipo di operazioni di raccordo. Raccordo di riferimento / Raccordo su spigolo";

EdgeFilletModeLabel.Message="Oggetti per il raccordo";
SupportFilletMode.Message="Riferimento 1";

Surface1Curve1FrameShortHelp.Message="Seleziona gruppo 1 superfici e curve";
Surface1Curve1FrameLongHelp.Message="Seleziona il primo gruppo di superfici e curve per l'operazione di raccordo";
ObjectsToFilletFrameShortHelp.Message="Seleziona oggetti per il raccordo";
ObjectsToFilletFrameLongHelp.Message="Seleziona spigoli vivi / facce con spigoli vivi per l'operazione di raccordo";

Surface1Title.Message="Superficie:";
Surface1ShortHelp.Message="Seleziona gruppo 1 superfici";
Surface1LongHelp.Message="Seleziona il primo gruppo di superfici per l'operazione di raccordo";

ObjectToFilletTitle.Message="Oggetti per il raccordo:";
ObjectToFilletShortHelp.Message="Seleziona oggetti per il raccordo";
ObjectToFilletLongHelp.Message="Seleziona spigoli vivi / facce con spigoli vivi per l'operazione di raccordo";

Surface1SelectorShortHelp.Message="Seleziona gruppo 1 superfici";
Surface1SelectorLongHelp.Message="Seleziona il primo gruppo di superfici per l'operazione di raccordo";
ObjectsToFilletSelectorShortHelp.Message="Seleziona oggetti per il raccordo";
ObjectsToFilletSelectorLongHelp.Message="Seleziona spigoli vivi / facce con spigoli vivi per l'operazione di raccordo";

// Start WAP 10:06:09 - R19SP3HF
OutputResultFrame.NewNbOfCellsTitleLabel.Title="N. di celle       :";
OutputResultFrame.NewNbOfCellsTitleLabel.ShortHelp="Visualizza il numero di celle";
OutputResultFrame.NbOfDomainsTitleLabel.Title="N. di domini:";
OutputResultFrame.NbOfDomainsTitleLabel.ShortHelp="Visualizza il numero di domini";
OutputResultFrame.NbOfDomainsTitleLabel.LongHelp="Visualizza il numero di domini nel lembo di raccordo creato";
// End WAP 10:06:09

// Start WAP 10:12:21
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.Title="Tolleranze continuit�";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG0Tol.Title="G0:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG1Tol.Title="G1:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG2Tol.Title="G2:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG3Tol.Title="G3:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG0TolFrame.ShortHelp="Modifica il valore del parametro: � richiesto il valore di tolleranza G0.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG0TolFrame.LongHelp="Definisce il valore di tolleranza G0";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG1TolFrame.ShortHelp="Modifica il valore del parametro: � richiesto il valore di tolleranza G1.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG1TolFrame.LongHelp="Definisce il valore di tolleranza G1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG2TolFrame.ShortHelp="Modifica il valore del parametro: � richiesto il valore di tolleranza G2.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG2TolFrame.LongHelp="Definisce il valore di tolleranza G2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG3TolFrame.ShortHelp="Modifica il valore del parametro: � richiesto il valore di tolleranza G3.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG3TolFrame.LongHelp="Definisce il valore di tolleranza G3";
// End WAP 10:12:21

// Start WAP 11:06:27
NbOfCellsOnFilletRibbonLongHelp.Message="Visualizza il numero di celle nel lembo di raccordo creato.";
MaxOrderOnFilletRibbonLongHelp.Message="Visualizza il grado massimo del lembo di raccordo creato in entrambe le direzioni U e V.";
MaxSegOnFilletRibbonLongHelp.Message="Visualizza il numero massimo di segmenti per cella del lembo di raccordo creato in entrambe le direzioni U e V.";
NbOfCellsOnEntireFilletLongHelp.Message="Visualizza il numero di celle nell'intero risultato del raccordo creato.";
MaxOrderOnEntireFilletLongHelp.Message="Visualizza il grado massimo nell'intero risultato del raccordo creato in entrambe le direzioni U e V.";
MaxSegOnEntireFilletLongHelp.Message="Visualizza il numero massimo di segmenti per cella nell'intero risultato del raccordo creato in entrambe le direzioni U e V.";
// End WAP 11:06:27

