// ==============================================================================================
// Valeurs de Combo d'Import
// ==============================================================================================

// Unit
Import.ComboUnit.item_0="Millimetri (mm)";
Import.ComboUnit.item_1="Centimetri (cm)";
Import.ComboUnit.item_2="Metri (m)";
Import.ComboUnit.item_3="Pollici (in)";
Import.ComboUnit.item_4="Piedi (ft)";
Import.ComboUnit.item_5="Fattore di scala";
Import.ComboUnit.item_6="Automatico";

// ==============================================================================================
// Valeurs de Combo d'Export
// ==============================================================================================

// Version
Export.ComboVersion.item_0="DXF/DWG R12";
Export.ComboVersion.item_1="DXF/DWG R13";
Export.ComboVersion.item_2="DXF/DWG R14";
Export.ComboVersion.item_3="DXF/DWG 2000";
Export.ComboVersion.item_4="DXF/DWG 2004";
Export.ComboVersion.item_5="DXF/DWG 2007";

// --------------------
// >>>>>>>IMPORT<<<<<<<
// --------------------
_ToolsImport.HeaderFrame.Global.Title=" Importa";

// --- Standard ---
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameStandard.Title="Standard";

_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameStandard._LabelDftStd.Title="Drafting:     ";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameStandard._LabelDxfStd.Title="DXF:     ";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameStandard._ComboDftStd.ShortHelp = "Select the 'CATIA Drafting' standard of the DXF/DWG file to be imported.\nThe Import External Format mode will automatically choose the standard from the existing drawing.";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameStandard._ComboDftStd.LongHelp="Seleziona lo standard di Drafting per il file DXF/DWG da importare.\nLa modalita Importa formato esterno selezionera automaticamente lo standard dal disegno esistente.";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameStandard._ComboDxfStd.ShortHelp = "Select the 'DXF' standard of the DXF/DWG file to be imported.\nThis option will be used both for 'Open File' and for 'Import External Format' modes.";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameStandard._ComboDxfStd.LongHelp="Seleziona lo standard di mappatura DXF per le modalita Apri file e Importa formato esterno. \nQuesta opzione viene utilizzata per le modalita Apri file e Importa formato esterno.";

// --- Unit ---
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameUnit._LabelUnit.Title=" Unita del file:  ";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameUnit._ComboUnit.ShortHelp = "Select the unit of the DXF/DWG file to be imported";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameUnit._ComboUnit.LongHelp="Seleziona l'unita del file DXF/DWG da importare.\nIl file DXF/DWG non contiene queste informazioni.";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameUnit._ScaleFactorNum.ShortHelp = "Numerator of the scale factor";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameUnit._ScaleFactorNum.LongHelp="Definisce il numeratore del fattore di scala\nper importare i dati in millimetri.";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameUnit._ScaleFactorDen.ShortHelp = "Denominator of the scale factor";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameUnit._ScaleFactorDen.LongHelp="Definisce il denominatore del fattore di scala\nper importare i dati in millimetri.";

// --- View	---
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameView._BackgroundCheckButton.Title="Spazio carta sullo sfondo";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameView._BackgroundCheckButton.ShortHelp = "To put Paper Spaces geometry in Backgound";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameView._BackgroundCheckButton.LongHelp="Consente di posizionare la geometria Spazio carta (cartuccia e cosi via) sullo sfondo.\nLe viste congelate verranno create nelle viste di lavoro.";

// --- Space ---
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameSpace._KeepModelSpaceCheckButton.Title="Conserva spazio modello";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameSpace._KeepModelSpaceCheckButton.ShortHelp = "To keep the whole Model Space";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameSpace._KeepModelSpaceCheckButton.LongHelp="Conservare l'intero Spazio modello nel proprio foglio anche se e completamente ridistribuito nelle viste congelate.";

// --- EndPoints ---
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._LabelEndPoints.Title=" Crea punti estremi:  ";

_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioNeverCreateEndPoints.Title="Mai";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioNeverCreateEndPoints.ShortHelp = "The end points of segments will not be created for any entities";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioNeverCreateEndPoints.LongHelp="Impedisce la creazione degli estremi dei segmenti per tutte le entita.";

_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioSometimesCreateEndPoints.Title="Per alcune entita";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioSometimesCreateEndPoints.ShortHelp = "The end points of segments will be created only for hatch boundaries and mixed polylines";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioSometimesCreateEndPoints.LongHelp="Crea gli estremi dei segmenti solo per i bordi e le poli-linee miste.";

_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioAlwaysCreateEndPoints.Title="Sempre";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioAlwaysCreateEndPoints.ShortHelp = "The end points of segments will be created for all entities";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameEndPoints._RadioAlwaysCreateEndPoints.LongHelp="Crea gli estremi dei segmenti per tutte le entita.";

// --- Dimension ---
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._LabelDimension.Title=" Converti quote come:  ";

_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioRealDimension.Title="Quote";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioRealDimension.ShortHelp = "Select dimensions import format as Dimensions";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioRealDimension.LongHelp="Converte le entita delle quote come quote V5.\nSe non e possibile, esse vengono convertite come Dettagli.";

_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioDetailDimension.Title="Dettagli";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioDetailDimension.ShortHelp = "Select dimensions import format as Details";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioDetailDimension.LongHelp="Converte le entita delle quote come Dettagli.";

_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioExplodedDimension.Title="Geometria";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioExplodedDimension.ShortHelp = "Select dimensions import format as geometry elements";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameDimension._RadioExplodedDimension.LongHelp="Converte le entita delle quote come geometria esplosa.";

// --- Import Mapping ---
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping.Title = "Mapping";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._LabelLineType.Title = "      Line type:  ";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._LineTypePushButton.Title = "Define...";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._LineTypePushButton.ShortHelp = "Define the line type mapping from DXF/DWG line types to CATIA V5 line types";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._LineTypePushButton.LongHelp = "Defines the line type mapping from DXF/DWG line types to CATIA V5 line types.";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._LabelTextFont.Title = "      Text font:  ";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._TextFontPushButton.Title = "Define...";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._TextFontPushButton.ShortHelp = "Define the text font mapping from DXF/DWG text fonts to CATIA V5 text fonts";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._TextFontPushButton.LongHelp = "Defines the text font mapping from DXF/DWG text fonts to CATIA V5 text fonts.";

//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._ColorThicknessCheckButton.Title = "Color to thickness:  ";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._ColorThicknessCheckButton.ShortHelp = "To enable color / thickness mapping mode";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._ColorThicknessCheckButton.LongHelp = "This option enables import mapping from DXF/DXG colors to CATIA thicknesses and allows customizing it.\nOtherwise, for AutoCAD 2000 format DXF/DWG file with line weight attributes,\na default mapping is made from the DXF/DWG line weight to the nearest available CATIA line thickness.";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._ColorThicknessPushButton.Title = "Define...";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._ColorThicknessPushButton.ShortHelp = "Define the mapping from DXF/DWG colors to CATIA V5 line thicknesses";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameImportMapping._ColorThicknessPushButton.LongHelp = "Defines the mapping from DXF/DWG colors to CATIA V5 line thicknesses.";

// --- MapLayerOn2DL ---
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameMapLayer._MapLayerOn2DLCheckButton.Title="Mappa layer DXF con layout 2D per fogli di disegno 3D";
//_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameMapLayer._MapLayerOn2DLCheckButton.ShortHelp = "Map DXF Layers with 2D Layout for 3D Design Sheets";
_ToolsImport.IconAndOptionsFrame.OptionsFrame._FrameMapLayer._MapLayerOn2DLCheckButton.LongHelp="Mappa layer DXF con layout 2D per fogli di disegno 3D";


// --------------------
// >>>>>>>EXPORT<<<<<<<
// --------------------
_ToolsExport.HeaderFrame.Global.Title=" Esporta";

// --- Sheets ---
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameSheets._LabelAllSheets.Title="Fogli esportati:  ";

_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameSheets._RadioAllSheets.Title="Tutti";
//_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameSheets._RadioAllSheets.ShortHelp = "Export all sheet under distinct file";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameSheets._RadioAllSheets.LongHelp="Esporta tutti i fogli in file distinti.\nAi file creati verra assegnato il nome selezionato dall'utente e il nome del foglio esportato.";

_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameSheets._RadioOnlyCurrentSheet.Title="Solo corrente";
//_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameSheets._RadioOnlyCurrentSheet.ShortHelp = "Export only current sheet";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameSheets._RadioOnlyCurrentSheet.LongHelp="Esporta solo il foglio corrente.";

// --- Version AutoCAD ---
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameVersion._LabelVersion.Title="Versione:  ";
//_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameVersion._ComboVersion.ShortHelp = "Select the release for exported DXF/DWG files";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameVersion._ComboVersion.LongHelp="Seleziona il rilascio DXF/DWG per esportare i file.";

// --- Export Mode ---
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportMode._LabelExportMode.Title="Modalita esportazione:  ";

_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportMode._RadioSemanticMode.Title="Semantico";
//_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportMode._RadioSemanticMode.ShortHelp = "Easier edition result";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportMode._RadioSemanticMode.LongHelp="Offre la possibilita di scegliere tra due modalita di esportazione:\n- La modalita semantica produce un risultato con funzionalita di modifica piu semplici:\ngeometria di gruppi in blocchi, \nesportazioni di layer e testi. \n- La modalita grafica garantisce un'esportazione conforme alla visualizzazione originale.\n\nNota: per un'esportazione piu precisa della geometria, utilizzare l'opzione semantica.";

_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportMode._RadioGraphicMode.Title="Grafica";
//_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportMode._RadioGraphicMode.ShortHelp = "Faithful graphic export";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportMode._RadioGraphicMode.LongHelp="Offre la possibilita di scegliere tra due modalita di esportazione:\n- La modalita semantica produce un risultato con funzionalita di modifica piu semplici:\ngeometria di gruppi in blocchi, \nesportazioni di layer e testi. \n- La modalita grafica garantisce un'esportazione conforme alla visualizzazione originale.\n\nNota: per un'esportazione piu precisa della geometria, utilizzare l'opzione semantica.";

_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions.Title="Opzioni Semantica";

// --- Export Dimension as dimension ---
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDimension._CheckButtonExportDimension.Title="Esporta quote come Quote";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDimension._CheckButtonExportDimension.LongHelp="Esporta quote come tali.\nOgni quota non ancora supportata viene esportata come blocco grafico.\nQuesta opzione e disponibile solo mediante l'esportazione della modalita semantica.";

// --- Export Detail ---
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._LabelExportDetail.Title="Esporta blocchi:  ";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._LabelExportDetail.LongHelp="Esporta i blocchi.\nQuesta opzione e disponibile solo mediante l'esportazione della modalita semantica.";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._RadioExportNoDetail.Title="Nessuna";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._RadioExportNoDetail.LongHelp="Non esporta alcun blocco.\nQuesta opzione e disponibile solo mediante l'esportazione della modalita semantica.";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._RadioExportOneLevelDetail.Title="Un livello";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._RadioExportOneLevelDetail.LongHelp="Esporta solo un livello di blocco automatico per fornire una struttura all'interno del file esportato.\nQuesta opzione e disponibile solo mediante l'esportazione della modalita semantica.";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._RadioExportFullDetail.Title="Completa";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportDetail._RadioExportFullDetail.LongHelp="Esportazione completa del componente 2D come blocco in aggiunta al blocco automatico creato per la struttura.\nQuesta opzione e disponibile solo mediante l'esportazione della modalita semantica.";

// --- Export Layer Name ---
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportLayer._RadioExportLayerNumber.Title="Esporta numero di layer";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportLayer._RadioExportLayerNumber.LongHelp="Esporta numero di layer.\nQuesta opzione e disponibile solo mediante l'esportazione della modalita semantica.";

_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportLayer._RadioExportLayerName.Title="Esporta nome layer               :";
_ToolsExport.IconAndOptionsFrame.OptionsFrame._FrameExportSemanticOptions._FrameExportLayer._RadioExportLayerName.LongHelp="Esporta nome layer.\nQuesta opzione e disponibile solo mediante l'esportazione della modalita semantica.";
