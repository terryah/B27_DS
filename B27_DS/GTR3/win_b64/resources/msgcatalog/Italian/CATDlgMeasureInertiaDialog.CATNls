CommandModeDialogTitle="Misura inerzia";
EditModeDialogTitle="Misura inerzia (modifica)";

IndicateInertiaMsg="Indicare l'elemento da misurare";

IndicateAxisMsg="Indicare l'asse da selezionare";

IndicatePtMsg="Indicare il punto da selezionare";

IndicateAxisSystemMsg="Indicare il sistema di assi da selezionare";

SwitchMode="Passare alla modalit� progettazione o alla modalit� visualizzazione per calcolare una misurazione di inerzia valida \n e considerare il valore di densit� corretto";

CancelButton="Chiudi";

// Example usage:
//    "Point on Product.1"
//    "Line in Product.2"

OnLabel=" attivata ";
InLabel=" in ";

// Definition

DefFrame.SeparatorDefFrame.LabelDef.Title="Definizione ";
//MainFrame.DefFrame.SeparatorDefFrame.LabelDef.Title = "Definition ";

// Selection

DefFrame.SubDefFrame.SelectionFrame.LabelSelection.Title="Selezione:";
//MainFrame.DefFrame.SubDefFrame.SelectionFrame.LabelSelection.Title = "Selection :";
NoSelection="Nessuna selezione";

// Button

DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.Title="Misura inerzia 3D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.ShortHelp="Misura inerzia 3D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.Help="Misura l'inerzia dell'elemento superficiale o volumetrico";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton3D.LongHelp="Misura l'inerzia dell'elemento superficiale o volumetrico";

DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.Title="Misura inerzia 2D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.ShortHelp="Misura inerzia 2D";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.Help="Misura l'inerzia dell'elemento planare";
DefFrame.SubDefFrame.RadioButtonFrame.RadioButton2D.LongHelp="Misura l'inerzia dell'elemento planare";

// Result

MainFrame.ResultFrame.SeparatorResultFrame.LabelResult.Title="Risultato ";


MainFrame.ResultFrame.DescriptionFrame.CalculationModeFrame.InertiaCalculationMode.Title="Modalit� di calcolo:  ";
MainFrame.ResultFrame.DescriptionFrame.TypeItemFrame.TypeItemLabel.Title="Tipo:  ";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.Title="Caratteristiche";
MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.LongHelp="Fornisce l'area, il volume, la densit� e la massa dell'elemento selezionato";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.AreaFrame_L.AreaLabel.Title="Area";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.AreaFrame_L.AreaLabel.LongHelp    = "Area";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.VolumeFrame_L.VolumeLabel.Title="Volume";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.VolumeFrame_L.VolumeLabel.LongHelp  = "Volume";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.DensityFrame_L.DensityLabel.Title="Densit�";
MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.DensityFrame_L.DensityLabel.LongHelp="Fornisce la densit� o la massa superficiale dell'elemento selezionato. La densit� � quella del materiale applicato alla parte.\nValori predefiniti: 1000 kg/m3 per i volumi e 10 kg/m2 per le superfici.\nNon uniforme: i prodotti secondari hanno densit� differenti.";

MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.MassFrame_L.MassLabel.Title="Massa";
//MainFrame.ResultFrame.CharacAndCoFGFrame.CharacFrame.MassFrame_L.MassLabel.LongHelp    = "Mass";

MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.Title="Baricentro (G)";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.LongHelp="Fornisce le coordinate del baricentro";


MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGxFrame_L.GXLabel.Title="Gx";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGyFrame_L.GYLabel.Title="Gy";
MainFrame.ResultFrame.CharacAndCoFGFrame.CenterOfGravityFrame.CoFGzFrame_L.GZLabel.Title="Gz";


// CalculationMode

IdCalculationMode="Modalit� di calcolo:  ";
IdCalculationModeResultExactThenApprox="Esatto o approssimato";
IdCalculationModeResultExact="Esatta";
IdCalculationModeResultApprox="Approssimato";

// type

SurfaceType="Superficie";
VolumeType="Volume";

// density

SurfacicMass="Densit� massa superficiale";
VolumicMass="Densit�";


//
// Item labels
// Enforce same length temporarily
//
Results.Title="Risultati";

Results.IdentifierFrame.Title="Descrizione";
Results.IdentifierFrame.LongHelp="Identifica l'elemento misurato ed indica se � un volume o una superficie";

Results.EquivalentFrame.Title="Equivalente";
Results.EquivalentFrame.LongHelp="Indica se gli equivalenti di inerzia (parametri utente importati tramite CATIA Knowledgeware) vengono presi in considerazione\n0: la misura � ricavata sulla selezione, sulla geometria o sull'assieme.\n1 o pi�: vengono presi in considerazione 1 o pi� equivalenti di inerzia";

Results.CharacCoFGFrame.CharacFrame.Title="Caratteristiche";
Results.CharacCoFGFrame.CharacFrame.LongHelp="Fornisce l'area, il volume, la densit� e la massa dell'elemento selezionato";

Results.CharacCoFGFrame.CharacFrame.AreaLabel_F.AreaLabel.Title="Area";
//Results.CharacCoFGFrame.CharacFrame.AreaLabel_F.AreaLabel.LongHelp    = "Area";

Results.CharacCoFGFrame.CharacFrame.VolumeLabel_F.VolumeLabel.Title="Volume";
//Results.CharacCoFGFrame.CharacFrame.VolumeLabel_F.VolumeLabel.LongHelp  = "Volume";

Results.CharacCoFGFrame.CharacFrame.DensityLabel_F.DensityLabel.Title="Densit�";
Results.CharacCoFGFrame.CharacFrame.DensityLabel_F.DensityLabel.LongHelp="Fornisce la densit� o la densit� superficiale dell'elemento selezionato. La densit� � quella del materiale applicato alla parte.\nValori predefiniti: 1000 kg/m3 per i volumi e 10 kg/m2 per le superfici.\nNon uniforme: i prodotti secondari hanno densit� differenti.";

Results.CharacCoFGFrame.CharacFrame.MassLabel_F.MassLabel.Title="Massa";
//Results.CharacCoFGFrame.CharacFrame.MassLabel_F.MassLabel.LongHelp    = "Mass";


Results.CharacCoFGFrame.CenterOfGravityFrame.Title="Baricentro (G)";
Results.CharacCoFGFrame.CenterOfGravityFrame.LongHelp="Fornisce le coordinate del baricentro";

Results.MomentsFrame.Title="Momenti principali / G";
Results.MomentsFrame.LongHelp="Fornisce i momenti di inerzia principali M calcolati rispetto al baricentro";

Results.AxesFrame.Title="Assi principali";
Results.AxesFrame.LongHelp="Fornisce gli assi principali A in base ai quali viene calcolata l'inerzia";

Results.InertiaFrame.Title="Matrice di inerzia / G";
Results.InertiaFrame.LongHelp="Fornisce la matrice di inerzia I calcolata rispetto al baricentro G";

Results.AxisFrame.Title="Momento / Asse";
Results.AxisFrame.LongHelp="Fornisce l'equazione O ed il vettore di direzione D dell'asse selezionato ed il momento di inerzia Ma rispetto all'asse ed il raggio di rotazione R";

Results.Inertia_O_Frame.Title="Matrice di inerzia / O";
Results.Inertia_O_Frame.LongHelp="Fornisce la matrice di inerzia I calcolata rispetto all'origine O del documento";

Results.Inertia_P_Frame.Title="Matrice di inerzia / P";
Results.Inertia_P_Frame.LongHelp="Fornisce la matrice di inerzia I rispetto al punto selezionato P e le coordinate del punto";

Results.Inertia_AS_Frame.Title="Matrice di inerzia / sistema di assi A";
Results.Inertia_AS_Frame.LongHelp="Fornisce la matrice di inerzia I rispetto al sistema di assi selezionato A e l'origine O ed i vettori (U, V, W) del sistema di assi";



// SpacerLabel enables offset equivalent to X or Y or Z
SpacerLabel="   ";


AreaLabel="Area";
VolumeLabel="Volume";
DensityLabel="Densit�";
MassLabel="Massa";

DensityInfoLabel="  Valore predefinito";
DensityInfoLabelEmpty="               ";

MaterialLabel="Materiale";
MaterialLabelDefault="Predefinito";


// ----------- Inertia Center

Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GXLabel.Title="Gx";
Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GYLabel.Title="Gy";
Results.CharacCoFGFrame.CenterOfGravityFrame.DataFrame.GZLabel.Title="Gz";

GXLabel="Gx";
GYLabel="Gy";
GZLabel="Gz";


// ----------- Principal Moments

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.Title="Momenti principali / G";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.LongHelp="Fornisce i momenti di inerzia principali M calcolati rispetto al baricentro";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M1_F.M1Label.Title="M1";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M2_F.M2Label.Title="M2";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalMomentG.M3_F.M3Label.Title="M3";


Results.MomentsFrame.DataFrame.M1Label.Title="M1";
Results.MomentsFrame.DataFrame.M2Label.Title="M2";
Results.MomentsFrame.DataFrame.M3Label.Title="M3";


M1Label="M1";
M2Label="M2";
M3Label="M3";

// ----------- Principals Axes

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.Title="Assi principali";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.LongHelp="Fornisce gli assi principali A in base ai quali viene calcolata l'inerzia";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1X_F.A1XLabel.Title="A1x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1Y_F.A1YLabel.Title="A1y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A1Z_F.A1ZLabel.Title="A1z";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2X_F.A2XLabel.Title="A2x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2Y_F.A2YLabel.Title="A2y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A2Z_F.A2ZLabel.Title="A2z";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3X_F.A3XLabel.Title="A3x";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3Y_F.A3YLabel.Title="A3y";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.PrincipalAxisG.A3Z_F.A3ZLabel.Title="A3z";


Results.AxesFrame.DataFrame.A1XLabel.Title="A1x";
Results.AxesFrame.DataFrame.A1YLabel.Title="A1y";
Results.AxesFrame.DataFrame.A1ZLabel.Title="A1z";

Results.AxesFrame.DataFrame.A2XLabel.Title="A2x";
Results.AxesFrame.DataFrame.A2YLabel.Title="A2y";
Results.AxesFrame.DataFrame.A2ZLabel.Title="A2z";

Results.AxesFrame.DataFrame.A3XLabel.Title="A3x";
Results.AxesFrame.DataFrame.A3YLabel.Title="A3y";
Results.AxesFrame.DataFrame.A3ZLabel.Title="A3z";

A1XLabel="A1x";
A1YLabel="A1y";
A1ZLabel="A1z";

A2XLabel="A2x";
A2YLabel="A2y";
A2ZLabel="A2z";

A3XLabel="A3x";
A3YLabel="A3y";
A3ZLabel="A3z";

// ----------- Inertia Matrix

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.Title="Inerzia / G";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.LongHelp="Fornisce la matrice di inerzia I calcolata rispetto al baricentro G";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.Title="Matrice di inerzia / G";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXX_F.IXXLabel.Title="IoxG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IYY_F.IYYLabel.Title="IoyG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IZZ_F.IZZLabel.Title="IozG";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXY_F.IXYLabel.Title="IxyG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IXZ_F.IXZLabel.Title="IxzG";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaG.FrameInertiaG.InertiaMatrixG.IYZ_F.IYZLabel.Title="IyzG";

Results.InertiaFrame.DataFrame.IXXLabel.Title="IoxG";
Results.InertiaFrame.DataFrame.IXYLabel.Title="IxyG";
Results.InertiaFrame.DataFrame.IXZLabel.Title="IxzG";
Results.InertiaFrame.DataFrame.IYXLabel.Title="IyxG";
Results.InertiaFrame.DataFrame.IYYLabel.Title="IoyG";
Results.InertiaFrame.DataFrame.IYZLabel.Title="IyzG";
Results.InertiaFrame.DataFrame.IZXLabel.Title="IzxG";
Results.InertiaFrame.DataFrame.IZYLabel.Title="IzyG";
Results.InertiaFrame.DataFrame.IZZLabel.Title="IozG";

IXXLabel="IoxG";
IYYLabel="IoyG";
IZZLabel="IozG";
IXYLabel="IxyG";
IXZLabel="IxzG";
IYZLabel="IyzG";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.Title="Inerzia / O";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.LongHelp="Fornisce la matrice di inerzia I calcolata rispetto all'origine O del documento";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.Title="Matrice di inerzia / O";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXX_O_F.IXXLabel.Title="IoxO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IYY_O_F.IYYLabel.Title="IoyO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IZZ_O_F.IZZLabel.Title="IozO";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXY_O_F.IXYLabel.Title="IxyO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IXZ_O_F.IXZLabel.Title="IxzO";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaO.FrameInertiaO.InertiaMatrixO.IYZ_O_F.IYZLabel.Title="IyzO";

Results.Inertia_O_Frame.DataFrame.IXXLabel.Title="IoxO";
Results.Inertia_O_Frame.DataFrame.IXYLabel.Title="IxyO";
Results.Inertia_O_Frame.DataFrame.IXZLabel.Title="IxzO";
Results.Inertia_O_Frame.DataFrame.IYXLabel.Title="IyxO";
Results.Inertia_O_Frame.DataFrame.IYYLabel.Title="IoyO";
Results.Inertia_O_Frame.DataFrame.IYZLabel.Title="IyzO";
Results.Inertia_O_Frame.DataFrame.IZXLabel.Title="IzxO";
Results.Inertia_O_Frame.DataFrame.IZYLabel.Title="IzyO";
Results.Inertia_O_Frame.DataFrame.IZZLabel.Title="IozO";

IOXXLabel="IoxO";
IOYYLabel="IoyO";
IOZZLabel="IozO";
IOXYLabel="IxyO";
IOXZLabel="IxzO";
IOYZLabel="IyzO";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.Title="Inerzia / P";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.LongHelp="Fornisce la matrice di inerzia I rispetto al punto selezionato P e le coordinate del punto";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.Title="Matrice di inerzia / P";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXX_P_F.IXXLabel.Title="IoxP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IYY_P_F.IYYLabel.Title="IoyP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IZZ_P_F.IZZLabel.Title="IozP";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXY_P_F.IXYLabel.Title="IxyP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IXZ_P_F.IXZLabel.Title="IxzP";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.InertiaMatrixP.IYZ_P_F.IYZLabel.Title="IyzP";


Results.Inertia_P_Frame.P_I1_Editor_F.IXXLabel.Title="IoxP";
Results.Inertia_P_Frame.P_I1_Editor_F.IYYLabel.Title="IoyP";
Results.Inertia_P_Frame.P_I1_Editor_F.IZZLabel.Title="IozP";

Results.Inertia_P_Frame.P_I2_Editor_F.IXYLabel.Title="IxyP";
Results.Inertia_P_Frame.P_I2_Editor_F.IXZLabel.Title="IxzP";
Results.Inertia_P_Frame.P_I2_Editor_F.IYZLabel.Title="IyzP";


IPXXLabel="IoxP";
IPYYLabel="IoyP";
IPZZLabel="IozP";
IPXYLabel="IxyP";
IPXZLabel="IxzP";
IPYZLabel="IyzP";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.Title="Inerzia / Sistema di assi";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.LongHelp="Fornisce la matrice di inerzia I rispetto al sistema di assi selezionato A e l'origine O ed i vettori (U, V, W) del sistema di assi";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.Title="Matrice di inerzia / sistema di assi A";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXX_AS_F.IXXLabel.Title="IoxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IYY_AS_F.IYYLabel.Title="IoyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IZZ_AS_F.IZZLabel.Title="IozA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXY_AS_F.IXYLabel.Title="IxyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IXZ_AS_F.IXZLabel.Title="IxzA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.InertiaMatrixAS.IYZ_AS_F.IYZLabel.Title="IyzA";

Results.Inertia_AS_Frame.AS_I1_Editor_F.IXXLabel.Title="IoxA";
Results.Inertia_AS_Frame.AS_I1_Editor_F.IYYLabel.Title="IoyA";
Results.Inertia_AS_Frame.AS_I1_Editor_F.IZZLabel.Title="IozA";

Results.Inertia_AS_Frame.AS_I2_Editor_F.IXYLabel.Title="IxyA";
Results.Inertia_AS_Frame.AS_I2_Editor_F.IXZLabel.Title="IxzA";
Results.Inertia_AS_Frame.AS_I2_Editor_F.IYZLabel.Title="IyzA";


IAXXLabel="IoxA";
IAYYLabel="IoyA";
IAZZLabel="IozA";
IAXYLabel="IxyA";
IAXZLabel="IxzA";
IAYZLabel="IyzA";


// ----------- Axis System

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.Title="Sistema di assi";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OX_AS_F.OX_AS_Label.Title="OxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OY_AS_F.OY_AS_Label.Title="OyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.OZ_AS_F.OZ_AS_Label.Title="OzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UX_AS_F.UX_AS_Label.Title="UxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UY_AS_F.UY_AS_Label.Title="UyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.UZ_AS_F.UZ_AS_Label.Title="UzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VX_AS_F.VX_AS_Label.Title="VxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VY_AS_F.VY_AS_Label.Title="VyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.VZ_AS_F.VZ_AS_Label.Title="VzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WX_AS_F.WX_AS_Label.Title="WxA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WY_AS_F.WY_AS_Label.Title="WyA";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.AxisSytemAS.WZ_AS_F.WZ_AS_Label.Title="WzA";





Results.Inertia_AS_Frame.AS_O_Editor_F.OX_AS_Label.Title="OxA";
Results.Inertia_AS_Frame.AS_O_Editor_F.OY_AS_Label.Title="OyA";
Results.Inertia_AS_Frame.AS_O_Editor_F.OZ_AS_Label.Title="OzA";

OX_AS_Label="OxA";
OY_AS_Label="OyA";
OZ_AS_Label="OzA";

Results.Inertia_AS_Frame.AS_U_Editor_F.UX_AS_Label.Title="UxA";
Results.Inertia_AS_Frame.AS_U_Editor_F.UY_AS_Label.Title="UyA";
Results.Inertia_AS_Frame.AS_U_Editor_F.UZ_AS_Label.Title="UzA";

UX_AS_Label="UxA";
UY_AS_Label="UyA";
UZ_AS_Label="UzA";

Results.Inertia_AS_Frame.AS_V_Editor_F.VX_AS_Label.Title="VxA";
Results.Inertia_AS_Frame.AS_V_Editor_F.VY_AS_Label.Title="VyA";
Results.Inertia_AS_Frame.AS_V_Editor_F.VZ_AS_Label.Title="VzA";

VX_AS_Label="VxA";
VY_AS_Label="VyA";
VZ_AS_Label="VzA";

Results.Inertia_AS_Frame.AS_W_Editor_F.WX_AS_Label.Title="WxA";
Results.Inertia_AS_Frame.AS_W_Editor_F.WY_AS_Label.Title="WyA";
Results.Inertia_AS_Frame.AS_W_Editor_F.WZ_AS_Label.Title="WzA";

WX_AS_Label="WxA";
WY_AS_Label="WyA";
WZ_AS_Label="WzA";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.SelectAxisSytemAS.AxisSystem.Title="Seleziona sistema di assi";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaAS.FrameInertiaAS.SelectAxisSytemAS.AxisSystem.LongHelp="Fare clic qui e selezionare un sistema di assi nell'albero logico delle specifiche.\nI calcoli successivi vengono eseguiti rispetto allo stesso sistema di assi.\nPer passare a un altro sistema di assi, fare clic di nuovo sulla casella di controllo e scegliere un sistema di assi diverso";

Results.Inertia_AS_Frame.AS_Button_F.AxisSystem.Title="Seleziona sistema di assi";
Results.Inertia_AS_Frame.AS_Button_F.AxisSystem.LongHelp="Fare clic qui e selezionare un sistema di assi nell'albero logico delle specifiche.\nI calcoli successivi vengono eseguiti rispetto allo stesso sistema di assi.\nPer passare a un altro sistema di assi, fare clic di nuovo sulla casella di controllo e scegliere un sistema di assi diverso";

AxisSystem="Seleziona sistema assi";
AxisSystemNoSelection="Nessuna selezione";
AxisSystemUnknown="Sistema di assi V4";

AxisSystemDeleted="Eliminato o non impostato";

AxisSystemNotValidSelection="Questa selezione non � valida perch� il sistema di assi\nnon � nella parte attivata";


// ----------- Axis

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OX_A_F.OXAxisLabel.Title="Ox";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OY_A_F.OYAxisLabel.Title="Oy";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.OZ_A_F.OZAxisLabel.Title="Oz";

Results.AxisFrame.A_O_Editor_F.OXAxisLabel.Title="Ox";
Results.AxisFrame.A_O_Editor_F.OYAxisLabel.Title="Oy";
Results.AxisFrame.A_O_Editor_F.OZAxisLabel.Title="Oz";

OXAxisLabel="Ox";
OYAxisLabel="Oy";
OZAxisLabel="Oz";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DX_A_F.DXAxisLabel.Title="Dx";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DY_A_F.DYAxisLabel.Title="Dy";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.AxisA.DZ_A_F.DZAxisLabel.Title="Dz";

Results.AxisFrame.A_D_Editor_F.DXAxisLabel.Title="Dx";
Results.AxisFrame.A_D_Editor_F.DYAxisLabel.Title="Dy";
Results.AxisFrame.A_D_Editor_F.DZAxisLabel.Title="Dz";

DXAxisLabel="Dx";
DYAxisLabel="Dy";
DZAxisLabel="Dz";


MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.SelectAxisA.ButtonAxisA.Title="Seleziona l'asse";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.SelectAxisA.ButtonAxisA.LongHelp="Fare clic qui e selezionare un asse nell'area geometrica.\nI calcoli successivi vengono eseguiti rispetto allo stesso asse.\nPer passare a un altro asse, fare clic di nuovo sulla casella di controllo e scegliere un asse diverso";


Results.AxisFrame.A_Button_F.Axis.Title="Seleziona l'asse";
Results.AxisFrame.A_Button_F.Axis.LongHelp="Fare clic qui e selezionare un asse nell'area geometrica.\nI calcoli successivi vengono eseguiti rispetto allo stesso asse.\nPer passare a un altro asse, fare clic di nuovo sulla casella di controllo e scegliere un asse diverso";

Axis="Seleziona l'asse";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.Title="Inerzia / Asse";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.LongHelp="Fornisce l'equazione O ed il vettore di direzione D dell'asse selezionato ed il momento di inerzia Ma rispetto all'asse ed il raggio di rotazione R";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.Title="Momento / Asse";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.MomentAFrame_L.MomentALabel.Title="Ma ";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaA.FrameInertiaA.InertiaMomentA.RadiusMomentAFrame_L.RadiusMomentALabel.Title="Raggio ";



Results.AxisFrame.A_A_Editor_F.AxisMomentLabel.Title="Ma ";
AxisMomentLabel="Ma ";

Results.AxisFrame.A_A_Editor_F.RadiusMomentLabel.Title="Raggio ";
RadiusMomentLabel="Raggio ";

// ----------- Bounding Box

BBOXLabel="BBOx";
BBOYLabel="BBOy";
BBOZLabel="BBOz";

BBLXLabel="BBLx";
BBLYLabel="BBLy";
BBLZLabel="BBLz";

// -------------
MainFrame.ButtonFrame.CreateGeometry.Title="Crea geometria";
MainFrame.ButtonFrame.Customise.Title="Personalizza...";
MainFrame.ButtonFrame.KeepState.Title="Conserva misura";
MainFrame.ButtonFrame.ExportButton.Title="Esporta";
MainFrame.ButtonFrame.OnlyMainBodies.Title="solo body principali";
MainFrame.MeasShownElemsFrame.MeasShownElemsCheckButton.Title="Misura solo gli elementi visualizzati";

Results.CustomiseFrame.CreateGeometry.Title="Crea geometria";
Results.CustomiseFrame.Customise.Title="Personalizza...";
Results.CustomiseFrame.KeepState.Title="Conserva misura";
Results.CustomiseFrame.ExportButton.Title="Esporta";
Results.CustomiseFrame.OnlyMainBodies.Title="solo body principali";
Results.MeasShownElemsFrame.MeasShownElemsCheckButton.Title="Misura solo gli elementi visualizzati";

// ------------- Point

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OXPt_F.OXPtLabel.Title="Px";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OYPt_F.OYPtLabel.Title="Py";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.PointP.OZPt_F.OZPtLabel.Title="Pz";

Results.Inertia_P_Frame.P_P_Editor_F.OXPtLabel.Title="Px";
Results.Inertia_P_Frame.P_P_Editor_F.OYPtLabel.Title="Py";
Results.Inertia_P_Frame.P_P_Editor_F.OZPtLabel.Title="Pz";

OXPtLabel="Px";
OYPtLabel="Py";
OZPtLabel="Pz";

MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.SelectPointP.ButtonPointP.Title="Seleziona un punto";
MainFrame.TabResultFrame.TabResultCont.TabPageInertiaP.FrameInertiaP.SelectPointP.ButtonPointP.LongHelp="Fare clic qui e selezionare un punto nell'area geometrica.\nI calcoli successivi vengono eseguiti rispetto allo stesso punto.\nPer passare a un altro punto, fare clic di nuovo sulla casella di controllo e scegliere un punto diverso";


Results.Inertia_P_Frame.P_Button_F.Pt.Title="Seleziona un punto";
Results.Inertia_P_Frame.P_Button_F.Pt.LongHelp="Fare clic qui e selezionare un punto nell'area geometrica.\nI calcoli successivi vengono eseguiti rispetto allo stesso punto.\nPer passare a un altro punto, fare clic di nuovo sulla casella di controllo e scegliere un punto diverso";
Pt="Selezionare un punto";

// ---------------- Equivalent

MainFrame.ResultFrame.EquivalentFrame.EquivalentFrame_L.EquivalentLabel.Title="Equivalente";

Results.EquivalentFrame.DataFrame.EQUIVLabel.Title="        ";

Equivalent="Equivalente";


// --------------- Error

PtNotify="Errore";
AxisNotify="Errore";
AxisSysNotify="Errore";

SelectionNotValid="Selezione non valida, selezionare un componente diverso";


Warning="Avvertenza";

NotSelectionAxis="Questa selezione non � un asse";
NotSelectionPt="Questa selezione non � un punto";
NotSelectionAxisSystem="Questa selezione non � un sistema di assi";

NotValidSelectionNotify="Selezione non valida";



DensityNotValid="Densit� non valida. Immettere un valore diverso";
DensityTooSmall="Densit� troppo piccola. Deve essere maggiore di ";

Information="Informazioni";
MeasureInertiaDelete="Misura inerzia non � pi� valido perch� l'elemento misurato non esiste pi�.";
MeasureUpdateNotPossible="Impossibile aggiornare la misura perch� questa non � associativa.";



//Factory

InertiaVolume="Volume di inerzia";
InertiaSurface="Superficie di inerzia";
Inertia2DSurface="Superficie di inerzia 2D";

//HIX - Restrict Measure When CATAnalysis is opened with Product as Active.
MeasureInertiaInCATAnalysis="Per la misurazione nel prodotto, aprire questo prodotto in una nuova finestra.";
