// COPYRIGHT DASSAULT SYSTEMES 2002
//===========================================================================
//
// CATRmaText2DFrm (English)
//
//===========================================================================
fileLabel.Title = "Nome dell'immagine";
fileLabel.Help = "File di immagine della tessitura";
fileLabel.ShortHelp = "File di immagine della tessitura";
fileLabel.LongHelp =
"Definisce il nome file della tessitura da applicare.
L'assenza di nome indica assenza di tessitura.
Nota: La tessitura non � visibile se il materiale
� riflettente.";


fileSelector.Title = "Selettore di immagine";
fileSelector.Help = "File di immagine della tessitura";
fileSelector.ShortHelp = "File di immagine della tessitura";
fileSelector.LongHelp =
"Definisce il nome file della tessitura da applicare.
L'assenza di nome indica assenza di tessitura.
Nota: La tessitura non � visibile se il materiale
� riflettente.";


repeatLabel.Title = "Ripeti";
repeatLabel.Help = "Ripetizione della tessitura";
repeatLabel.ShortHelp = "Ripeti tessitura";
repeatLabel.LongHelp =
"Indica se la tessitura deve essere ripetuta.";


repeatFrame.repeatUCheck.Title = "U";
repeatFrame.repeatUCheck.Help = "Ripetizione della tessitura lungo l'asse U delle superfici";
repeatFrame.repeatUCheck.ShortHelp = "Ripeti tessitura lungo U";
repeatFrame.repeatUCheck.LongHelp =
"Indica se la tessitura deve essere ripetuta
lungo l'asse U delle superfici.";


repeatFrame.repeatVCheck.Title = "V";
repeatFrame.repeatVCheck.Help = "Ripetizione della tessitura lungo l'asse V delle superfici";
repeatFrame.repeatVCheck.ShortHelp = "Ripeti tessitura lungo V";
repeatFrame.repeatVCheck.LongHelp =
"Indica se la tessitura deve essere ripetuta
lungo l'asse V delle superfici.";


repeatFrame.flipLabel.Title = "Ribalta";
repeatFrame.flipLabel.Help = "Ribalta la tessitura";
repeatFrame.flipLabel.ShortHelp = "Ribalta la tessitura";
repeatFrame.flipLabel.LongHelp =
"Indica se la tessitura deve essere ribaltata.";


repeatFrame.flipUCheck.Title = "U";
repeatFrame.flipUCheck.Help = "Ribaltamento della tessitura lungo l'asse U delle superfici";
repeatFrame.flipUCheck.ShortHelp = "Ribalta la tessitura lungo U";
repeatFrame.flipUCheck.LongHelp =
"Indica se la tessitura deve essere ribaltata
lungo l'asse U delle superfici.";


repeatFrame.flipVCheck.Title = "V";
repeatFrame.flipVCheck.Help = "Ribalta la tessitura lungo l'asse V delle superfici";
repeatFrame.flipVCheck.ShortHelp = "Ribalta la tessitura lungo V";
repeatFrame.flipVCheck.LongHelp =
"Indica se la tessitura deve essere ribaltata
lungo l'asse V delle superfici.";


scaleULabel.Title = "Scala U";
scaleULabel.Help = "Scala della tessitura lungo l'asse U delle superfici";
scaleULabel.ShortHelp = "Scala della tessitura lungo U";
scaleULabel.LongHelp =
"Definisce la scala della tessitura lungo
l'asse U delle superfici.";


scaleUWheel.Title = "Scala U";
scaleUWheel.Help = "Scala della tessitura lungo l'asse U delle superfici";
scaleUWheel.ShortHelp = "Scala della tessitura lungo U";
scaleUWheel.LongHelp =
"Definisce la scala della tessitura lungo
l'asse U delle superfici.";


scaleVLabel.Title = "Scala V";
scaleVLabel.Help = "Scala della tessitura lungo l'asse V delle superfici";
scaleVLabel.ShortHelp = "Scala della tessitura lungo V";
scaleVLabel.LongHelp =
"Definisce la scala della tessitura lungo
l'asse V delle superfici.";


scaleVWheel.Title = "Scala V";
scaleVWheel.Help = "Scala della tessitura lungo l'asse V delle superfici";
scaleVWheel.ShortHelp = "Scala della tessitura lungo V";
scaleVWheel.LongHelp =
"Definisce la scala della tessitura lungo
l'asse V delle superfici.";


scaleLinkButton.Help = "Collega le scale U e V";
scaleLinkButton.ShortHelp = "Collega le scale U e V";
scaleLinkButton.LongHelp =
"Blocca la proporzione delle scale U e V.";


translateULabel.Title = "Posizione U";
translateULabel.Help = "Posizione della tessitura lungo l'asse U delle superfici";
translateULabel.ShortHelp = "Posizione della tessitura lungo U";
translateULabel.LongHelp =
"Definisce la posizione della tessitura lungo
l'asse U delle superfici.";


translateUWheel.Title = "Posizione U";
translateUWheel.Help = "Posizione della tessitura lungo l'asse U delle superfici";
translateUWheel.ShortHelp = "Posizione della tessitura lungo U";
translateUWheel.LongHelp =
"Definisce la posizione della tessitura lungo
l'asse U delle superfici.";


translateVLabel.Title = "Posizione V";
translateVLabel.Help = "Posizione della tessitura lungo l'asse V delle superfici";
translateVLabel.ShortHelp = "Posizione della tessitura lungo V";
translateVLabel.LongHelp =
"Definisce la posizione della tessitura lungo
l'asse V delle superfici.";


translateVWheel.Title = "Posizione V";
translateVWheel.Help = "Posizione della tessitura lungo l'asse V delle superfici";
translateVWheel.ShortHelp = "Posizione della tessitura lungo V";
translateVWheel.LongHelp =
"Definisce la posizione della tessitura lungo
l'asse V delle superfici.";


rotateLabel.Title = "Orientamento";
rotateLabel.Help = "Orientamento della tessitura sulle superfici";
rotateLabel.ShortHelp = "Orientamento della tessitura (da -360 a 360 gradi)";
rotateLabel.LongHelp =
"Definisce l'orientamento della tessitura sulle superfici.";


rotateWheel.Title = "Orientamento";
rotateWheel.Help = "Orientamento della tessitura sulle superfici";
rotateWheel.ShortHelp = "Orientamento della tessitura (da -360 a 360 gradi)";
rotateWheel.LongHelp =
"Definisce l'orientamento della tessitura sulle superfici.";


bumpLab.Title = "Imbozzatura(*)";
bumpLab.Help = "Ampiezza dell'imbozzatura da applicare alla tessitura";
bumpLab.ShortHelp = "Ampiezza dell'imbozzatura (da -1 a 1)
(*)Viene utilizzata solo per la resa tramite software o lo shader in tempo reale";
bumpLab.LongHelp =
"Definisce l'ampiezza dell'imbozzatura da applicare alla tessitura.
(*)Viene utilizzata solo per la resa tramite software o lo shader in tempo reale";


bumpWheel.Title = "Imbozzatura";
bumpWheel.Help = "Ampiezza dell'imbozzatura da applicare alla tessitura";
bumpWheel.ShortHelp = "Ampiezza dell'imbozzatura (da -1 a 1)";
bumpWheel.LongHelp =
"Definisce l'ampiezza dell'imbozzatura da applicare alla tessitura.";

warningLabel.Title = "(*) Parametri solo per la resa tramite software o lo shader in tempo reale";



