//---------------------------------------------------
// Ressource File for CATHybridPartCommandHeader Class
// En_US
//---------------------------------------------------
//
// ========================================================================================
// ========================================================================================
//							Commandes Wireframe
// ========================================================================================
// ========================================================================================
// == Point== 
CATHybridPartCommandHeader.CATHybridPartPoint.Title             = "Point...";
CATHybridPartCommandHeader.CATHybridPartPoint.Help              = "Creates one or more points";
CATHybridPartCommandHeader.CATHybridPartPoint.ShortHelp         = "Point";
CATHybridPartCommandHeader.CATHybridPartPoint.LongHelp          = 
"Point
Creates one or more points.";
// == Line== 
CATHybridPartCommandHeader.CATHybridPartLine.Title              = "Line...";
CATHybridPartCommandHeader.CATHybridPartLine.Help               = "Creates a line";
CATHybridPartCommandHeader.CATHybridPartLine.ShortHelp          = "Line";
CATHybridPartCommandHeader.CATHybridPartLine.LongHelp           = 
"Line
Creates a line.";
// == Axis Line == 
CATHybridPartCommandHeader.CATHpwAxisLineHdr.Title           = "Axis...";
CATHybridPartCommandHeader.CATHpwAxisLineHdr.Help            = "Creates an axis";
CATHybridPartCommandHeader.CATHpwAxisLineHdr.ShortHelp       = "Axis";
CATHybridPartCommandHeader.CATHpwAxisLineHdr.LongHelp        = 
"Axis
Creates an axis.";
// == Plane== 
CATHybridPartCommandHeader.CATHybridPartPlane.Title             = "Plane...";
CATHybridPartCommandHeader.CATHybridPartPlane.Help              = "Creates a plane";
CATHybridPartCommandHeader.CATHybridPartPlane.ShortHelp         = "Plane";
CATHybridPartCommandHeader.CATHybridPartPlane.LongHelp          = 
"Plane
Creates a plane.";
// == Curve== 
CATHybridPartCommandHeader.CATHybridPartCurve.Title             = "Spline...";
CATHybridPartCommandHeader.CATHybridPartCurve.Help              = "Creates a spline curve";
CATHybridPartCommandHeader.CATHybridPartCurve.ShortHelp         = "Spline";
CATHybridPartCommandHeader.CATHybridPartCurve.LongHelp          = 
"Spline
Creates a spline curve, possibly on a support element
and with tangency conditions.";
// == Helix == 
CATHybridPartCommandHeader.CATHybridPartHelix.Title             = "Helix...";
CATHybridPartCommandHeader.CATHybridPartHelix.Help              = "Creates an helix";
CATHybridPartCommandHeader.CATHybridPartHelix.ShortHelp         = "Helix";
CATHybridPartCommandHeader.CATHybridPartHelix.LongHelp          = 
"Helix
Creates an helical curve.";
// == Circle ==
CATHybridPartCommandHeader.CATHybridPartCircle.Title            = "Circle...";
CATHybridPartCommandHeader.CATHybridPartCircle.Help             = "Creates a circle or circular arc";
CATHybridPartCommandHeader.CATHybridPartCircle.ShortHelp        = "Circle";
CATHybridPartCommandHeader.CATHybridPartCircle.LongHelp         = 
"Circle
Creates a circle or circular arc.";
// == Connect == 
CATHybridPartCommandHeader.CATHybridPartConnect.Title           = "Connect Curve...";
CATHybridPartCommandHeader.CATHybridPartConnect.Help            = "Creates a connect curve between two curves";
CATHybridPartCommandHeader.CATHybridPartConnect.ShortHelp       = "Connect Curve";
CATHybridPartCommandHeader.CATHybridPartConnect.LongHelp        = 
"Connect Curve
Creates a connecting curve between two curves
taking continuity constraints into account.";
// == Corner ==
CATHybridPartCommandHeader.CATHybridPartCorner.Title            = "Corner...";
CATHybridPartCommandHeader.CATHybridPartCorner.Help             = "Creates a corner between two curves";
CATHybridPartCommandHeader.CATHybridPartCorner.ShortHelp        = "Corner";
CATHybridPartCommandHeader.CATHybridPartCorner.LongHelp         = 
"Corner
Creates a corner between two curves
or between a point and a curve.";
// == Project == 
CATHybridPartCommandHeader.CATHybridPartProject.Title           = "Projection...";
CATHybridPartCommandHeader.CATHybridPartProject.Help            = "Projects a point or curve onto a support element";
CATHybridPartCommandHeader.CATHybridPartProject.ShortHelp       = "Projection";
CATHybridPartCommandHeader.CATHybridPartProject.LongHelp        = 
"Projection
Projects a point or curve onto a support element.";
// == Intersect == 
CATHybridPartCommandHeader.CATHybridPartIntersect.Title         = "Intersection...";
CATHybridPartCommandHeader.CATHybridPartIntersect.Help          = "Creates geometry by intersecting two geometric elements";
CATHybridPartCommandHeader.CATHybridPartIntersect.ShortHelp     = "Intersection";
CATHybridPartCommandHeader.CATHybridPartIntersect.LongHelp      = 
"Intersection
Creates geometry by intersecting
two geometric elements.";
// == Boundary ==
CATHybridPartCommandHeader.CATHybridPartBoundary.Title          = "Boundary...";
CATHybridPartCommandHeader.CATHybridPartBoundary.Help           = "Creates a boundary from a surface";
CATHybridPartCommandHeader.CATHybridPartBoundary.ShortHelp      = "Boundary";
CATHybridPartCommandHeader.CATHybridPartBoundary.LongHelp       = 
"Boundary
Creates a boundary from
the edge of a surface.";
// == Polyline == 
CATHybridPartCommandHeader.CATHpwLineCornerHdr.Title                     = "Polyline...";
CATHybridPartCommandHeader.CATHpwLineCornerHdr.Help                      = "Creates a polyline";
CATHybridPartCommandHeader.CATHpwLineCornerHdr.ShortHelp                 = "Polyline";
CATHybridPartCommandHeader.CATHpwLineCornerHdr.LongHelp                  = 
"Polyline
Creates a broken line.";
//
// ========================================================================================
// ========================================================================================
//							Commandes Surface
// ========================================================================================
// ========================================================================================
// == Extrude== 
CATHybridPartCommandHeader.CATHybridPartExtrude.Title           = "Extrude...";
CATHybridPartCommandHeader.CATHybridPartExtrude.Help            = "Creates an extruded surface";
CATHybridPartCommandHeader.CATHybridPartExtrude.ShortHelp       = "Extrude";
CATHybridPartCommandHeader.CATHybridPartExtrude.LongHelp        = 
"Extrude
Creates a surface by extruding a profile.";

// == Cylinder== 
CATHybridPartCommandHeader.CATHpwCylinderHdr.Title               = "Cylinder...";
CATHybridPartCommandHeader.CATHpwCylinderHdr.Help                = "Creates an Cylinder";
CATHybridPartCommandHeader.CATHpwCylinderHdr.ShortHelp           = "Cylinder";
CATHybridPartCommandHeader.CATHpwCylinderHdr.LongHelp            = 
"Cylinder
Creates a cylinder with center, radius and two lengths as input.";

// == Offset== 
CATHybridPartCommandHeader.CATHybridPartOffset.Title            = "Offset...";
CATHybridPartCommandHeader.CATHybridPartOffset.Help             = "Creates a surface that is offset from a reference surface";
CATHybridPartCommandHeader.CATHybridPartOffset.ShortHelp        = "Offset";
CATHybridPartCommandHeader.CATHybridPartOffset.LongHelp         = 
"Offset
Creates a surface that is offset from a reference surface.";
// == Fill == 
CATHybridPartCommandHeader.CATHybridPartFill.Title              = "Fill...";
CATHybridPartCommandHeader.CATHybridPartFill.Help               = "Creates a fill surface inside a closed boundary";
CATHybridPartCommandHeader.CATHybridPartFill.ShortHelp          = "Fill";
CATHybridPartCommandHeader.CATHybridPartFill.LongHelp           = 
"Fill
Creates a fill surface inside a closed boundary.";
// == Sweep == 
CATHybridPartCommandHeader.CATHybridPartSweep.Title             = "Sweep...";
CATHybridPartCommandHeader.CATHybridPartSweep.Help              = "Creates a swept surface";
CATHybridPartCommandHeader.CATHybridPartSweep.ShortHelp         = "Sweep";
CATHybridPartCommandHeader.CATHybridPartSweep.LongHelp          = 
"Sweep
Creates a surface by sweeping a profile
along a center curve.";
// == Revol ==
CATHybridPartCommandHeader.CATHybridPartRevol.Title             = "Revolve...";
CATHybridPartCommandHeader.CATHybridPartRevol.Help              = "Creates a surface by revolving a profile about an axis";
CATHybridPartCommandHeader.CATHybridPartRevol.ShortHelp         = "Revolve";
CATHybridPartCommandHeader.CATHybridPartRevol.LongHelp          = 
"Revolve
Creates a surface by revolving a profile about an axis.";
// == Sphere ==
CATHybridPartCommandHeader.CATHpwSphereHdr.Title                = "Sphere...";
CATHybridPartCommandHeader.CATHpwSphereHdr.Help                 = "Creates a sphere";
CATHybridPartCommandHeader.CATHpwSphereHdr.ShortHelp            = "Sphere";
CATHybridPartCommandHeader.CATHpwSphereHdr.LongHelp             =
"Sphere
Creates a sphere.";
// == Loft ==
CATHybridPartCommandHeader.CATHybridPartLoft.Title              = "Multi-Sections Surface...";
CATHybridPartCommandHeader.CATHybridPartLoft.Help               = "Creates a multi-sections surface";
CATHybridPartCommandHeader.CATHybridPartLoft.ShortHelp          = "Multi-Sections Surface";
CATHybridPartCommandHeader.CATHybridPartLoft.LongHelp           = 
"Multi-Sections Surface
Creates a multi-sections surface based on section curves,
and possibly guide curves, along a center curve.";
// == Blend ==
CATHybridPartCommandHeader.CATHybridPartBlend.Title             = "Blend...";
CATHybridPartCommandHeader.CATHybridPartBlend.Help              = "Creates a blend surface";
CATHybridPartCommandHeader.CATHybridPartBlend.ShortHelp         = "Blend";
CATHybridPartCommandHeader.CATHybridPartBlend.LongHelp          = 
"Blend
Creates a blended surface between two surfaces.";
// == Extract Solide == 
CATHybridPartCommandHeader.CATHybridPartExtractSolide.Title     = "Extract...";
CATHybridPartCommandHeader.CATHybridPartExtractSolide.Help      = "Extracts a face or a surface edge";
CATHybridPartCommandHeader.CATHybridPartExtractSolide.ShortHelp = "Extract";
CATHybridPartCommandHeader.CATHybridPartExtractSolide.LongHelp  = 
"Extract
Extracts a face or a surface edge.";

//
// ========================================================================================
// ========================================================================================
//							Commandes Modification
// ========================================================================================
// ========================================================================================
// == Assemble == 
CATHybridPartCommandHeader.CATHybridPartAssemble.Title          = "Join...";
CATHybridPartCommandHeader.CATHybridPartAssemble.Help           = "Joins curves or surfaces";
CATHybridPartCommandHeader.CATHybridPartAssemble.ShortHelp      = "Join";
CATHybridPartCommandHeader.CATHybridPartAssemble.LongHelp       = 
"Join
Joins curves or surfaces.";
// == Healing ==
CATHybridPartCommandHeader.CATHybridPartHealing.Title           = "Healing...";
CATHybridPartCommandHeader.CATHybridPartHealing.Help            = "Heals surfaces";
CATHybridPartCommandHeader.CATHybridPartHealing.ShortHelp       = "Healing";
CATHybridPartCommandHeader.CATHybridPartHealing.LongHelp        = 
"Healing
Heals surfaces by filling in small gaps between the surfaces.";
// == Untrim ==
CATHybridPartCommandHeader.CATHybridPartUntrim.Title            = "Untrim...";
CATHybridPartCommandHeader.CATHybridPartUntrim.Help             = "Untrims previously trimmed surfaces and curves";
CATHybridPartCommandHeader.CATHybridPartUntrim.ShortHelp        = "Untrim";
CATHybridPartCommandHeader.CATHybridPartUntrim.LongHelp         = 
"Untrim Surface or Curve
Untrims previously trimmed
surfaces and curves.";
// == Disassemble ==
CATHybridPartCommandHeader.CATHybridPartDisassemble.Title       = "Disassemble...";
CATHybridPartCommandHeader.CATHybridPartDisassemble.Help        = "Disassembles multi-cell bodies into mono-cell bodies";
CATHybridPartCommandHeader.CATHybridPartDisassemble.ShortHelp   = "Disassemble";
CATHybridPartCommandHeader.CATHybridPartDisassemble.LongHelp    = 
"Disassemble
Disassembles multi-cell bodies
into mono-cell bodies.";
// == Split == 
CATHybridPartCommandHeader.CATHybridPartSplit.Title             = "Split...";
CATHybridPartCommandHeader.CATHybridPartSplit.Help              = "Cuts and relimits an element using a cutting element";
CATHybridPartCommandHeader.CATHybridPartSplit.ShortHelp         = "Split";
CATHybridPartCommandHeader.CATHybridPartSplit.LongHelp          = 
"Split
Cuts and relimits an element using a cutting element.";
// == Trim == 
CATHybridPartCommandHeader.CATHybridPartTrim.Title              = "Trim...";
CATHybridPartCommandHeader.CATHybridPartTrim.Help               = "Cuts and assembles two elements";
CATHybridPartCommandHeader.CATHybridPartTrim.ShortHelp          = "Trim";
CATHybridPartCommandHeader.CATHybridPartTrim.LongHelp           = 
"Cuts and assembles two elements.";
// == Near == 
CATHybridPartCommandHeader.CATHybridPartNear.Title              = "Near / Far ...";
CATHybridPartCommandHeader.CATHybridPartNear.Help               = "Extracts the nearest or the farthest entity from the reference element";
CATHybridPartCommandHeader.CATHybridPartNear.ShortHelp          = "Near / Far";
CATHybridPartCommandHeader.CATHybridPartNear.LongHelp           = 
"Near,
Creates, from the multi-element,
the element nearest or farthest to the reference element.";
// == Extrapol == 
CATHybridPartCommandHeader.CATHybridPartExtrapol.Title          = "Extrapolate...";
CATHybridPartCommandHeader.CATHybridPartExtrapol.Help           = "Creates a surface or curve by extrapolation";
CATHybridPartCommandHeader.CATHybridPartExtrapol.ShortHelp      = "Extrapolate";
CATHybridPartCommandHeader.CATHybridPartExtrapol.LongHelp       = 
"Extrapolate
Creates a surface by extrapolation of one of its boundaries,
or a curve at one of its endpoints.";
// == Inverse == 
CATHybridPartCommandHeader.CATHpwInverseHdr.Title               = "Invert Orientation...";
CATHybridPartCommandHeader.CATHpwInverseHdr.Help                = "Inverts an element's orientation";
CATHybridPartCommandHeader.CATHpwInverseHdr.ShortHelp           = "Invert Orientation";
CATHybridPartCommandHeader.CATHpwInverseHdr.LongHelp            = 
"Invert Orientation
Inverts an element's orientation.";
//
// ========================================================================================
// ========================================================================================
//							Commandes Fillet
// ========================================================================================
// ========================================================================================
// == Fillet == 
CATHybridPartCommandHeader.CATHpwFilletHdr.Title                = "Shape Fillet...";
CATHybridPartCommandHeader.CATHpwFilletHdr.Help                 = "Creates a fillet between two surfaces";
CATHybridPartCommandHeader.CATHpwFilletHdr.ShortHelp            = "Shape Fillet";
CATHybridPartCommandHeader.CATHpwFilletHdr.LongHelp             = 
"Shape Fillet
Creates a fillet between two surfaces.";
// == FilletEdge == 
CATHybridPartCommandHeader.CATHpwFilletEdgeHdr.Title            = "Edge Fillet...";
CATHybridPartCommandHeader.CATHpwFilletEdgeHdr.Help             = "Creates an edge fillet";
CATHybridPartCommandHeader.CATHpwFilletEdgeHdr.ShortHelp        = "Edge Fillet";
CATHybridPartCommandHeader.CATHpwFilletEdgeHdr.LongHelp         = 
"Edge Fillet
Creates a fillet on a surface's edge.";
// == FilletVariable =
CATHybridPartCommandHeader.CATHpwFilletVarHdr.Title             = "Variable Fillet...";
CATHybridPartCommandHeader.CATHpwFilletVarHdr.Help              = "Creates a variable radius fillet";
CATHybridPartCommandHeader.CATHpwFilletVarHdr.ShortHelp         = "Variable Fillet";
CATHybridPartCommandHeader.CATHpwFilletVarHdr.LongHelp          = 
"Variable Radius Fillet
Creates a fillet whose radius varies at each selected point.";
// == Fillet Face == 
CATHybridPartCommandHeader.CATHpwFilletFaceHdr.Title            = "Face-Face Fillet...";
CATHybridPartCommandHeader.CATHpwFilletFaceHdr.Help             = "Creates a face-face fillet by selecting two faces";
CATHybridPartCommandHeader.CATHpwFilletFaceHdr.ShortHelp        = "Face-Face Fillet";
CATHybridPartCommandHeader.CATHpwFilletFaceHdr.LongHelp         = 
"Face-Face Fillet
Creates a fillet between two non-intersecting faces.";
// == Fillet Tritangent == 
CATHybridPartCommandHeader.CATHpwTriFilletHdr.Title             = "Tritangent Fillet...";
CATHybridPartCommandHeader.CATHpwTriFilletHdr.Help              = "Creates a fillet by removing the face you specify";
CATHybridPartCommandHeader.CATHpwTriFilletHdr.ShortHelp         = "Tritangent Fillet";
CATHybridPartCommandHeader.CATHpwTriFilletHdr.LongHelp          = 
"Tritangent Fillet
Creates a tritangent fillet from three
faces you specify, by removing one.";
//
// ========================================================================================
// ========================================================================================
//							Commandes Transformation
// ========================================================================================
// ========================================================================================
// == Affinity == 
CATHybridPartCommandHeader.CATHybridPartAffinity.Title          = "Affinity...";
CATHybridPartCommandHeader.CATHybridPartAffinity.Help           = "Transforms an element by affinity";
CATHybridPartCommandHeader.CATHybridPartAffinity.ShortHelp      = "Affinity";
CATHybridPartCommandHeader.CATHybridPartAffinity.LongHelp       = 
"Affinity
Transforms an element by affinity.";
// == Rotate == 
CATHybridPartCommandHeader.CATHybridPartRotate.Title            = "Rotate...";
CATHybridPartCommandHeader.CATHybridPartRotate.Help             = "Rotates an element about an axis";
CATHybridPartCommandHeader.CATHybridPartRotate.ShortHelp        = "Rotate";
CATHybridPartCommandHeader.CATHybridPartRotate.LongHelp         = 
"Rotate
Rotates an element about an axis.";
// == Symmetry == 
CATHybridPartCommandHeader.CATHybridPartSymmetry.Title          = "Symmetry...";
CATHybridPartCommandHeader.CATHybridPartSymmetry.Help           = "Transforms an element by symmetry";
CATHybridPartCommandHeader.CATHybridPartSymmetry.ShortHelp      = "Symmetry";
CATHybridPartCommandHeader.CATHybridPartSymmetry.LongHelp       = 
"Symmetry
Creates a symmetrical element
in relation to a reference element.";
// == AxisToAxis == 
CATHybridPartCommandHeader.CATHpwAxisToAxisHdr.Title          = "Axis To Axis...";
CATHybridPartCommandHeader.CATHpwAxisToAxisHdr.Help           = "Transforms an element from an axis system to another";
CATHybridPartCommandHeader.CATHpwAxisToAxisHdr.ShortHelp      = "Axis To Axis";
CATHybridPartCommandHeader.CATHpwAxisToAxisHdr.LongHelp       = 
"Axis To Axis
Transforms an element from an axis system to another.";
// == Scaling == 
CATHybridPartCommandHeader.CATHybridPartScaling.Title           = "Scaling...";
CATHybridPartCommandHeader.CATHybridPartScaling.Help            = "Transforms an element by scaling";
CATHybridPartCommandHeader.CATHybridPartScaling.ShortHelp       = "Scaling";
CATHybridPartCommandHeader.CATHybridPartScaling.LongHelp        = 
"Scaling
Transforms an element by scaling.";
// == Translate == 
CATHybridPartCommandHeader.CATHybridPartTranslate.Title         = "Translate...";
CATHybridPartCommandHeader.CATHybridPartTranslate.Help          = "Translates an element along a direction";
CATHybridPartCommandHeader.CATHybridPartTranslate.ShortHelp     = "Translate";
CATHybridPartCommandHeader.CATHybridPartTranslate.LongHelp      = 
"Translates an element along a direction.";
//
// ========================================================================================
// ========================================================================================
//							Commandes Multiple
// ========================================================================================
// ========================================================================================
// == Commande multiple ==
CATHybridPartCommandHeader.CATHybridPartMultiple.Title          = "Object Repetition...";
CATHybridPartCommandHeader.CATHybridPartMultiple.Help           = "Repetes several times the creation of an object";
CATHybridPartCommandHeader.CATHybridPartMultiple.ShortHelp      = "Object Repetition";
CATHybridPartCommandHeader.CATHybridPartMultiple.LongHelp       = 
"Object Repetition
Creates several instances of an object at a time.";
// == Commande multiple de point
CATHybridPartCommandHeader.CATHybridPartMultiplePoint.Title     = "Points Repetition...";
CATHybridPartCommandHeader.CATHybridPartMultiplePoint.Help      = "Repeats several times the creation of points or planes or axis systems";
CATHybridPartCommandHeader.CATHybridPartMultiplePoint.ShortHelp = "Points Repetition";
CATHybridPartCommandHeader.CATHybridPartMultiplePoint.LongHelp  = 
"Points Repetition :
Creates several instances of points , planes and Axis Systems at a time.";
// == Commande multiple de planes
CATHybridPartCommandHeader.CATHpwMultiplePlaneHdr.Title         = "Planes Between...";
CATHybridPartCommandHeader.CATHpwMultiplePlaneHdr.Help          = "Repeats several times the creation of planes between two others";
CATHybridPartCommandHeader.CATHpwMultiplePlaneHdr.ShortHelp     = "Planes Between";
CATHybridPartCommandHeader.CATHpwMultiplePlaneHdr.LongHelp      = 
"Planes Repetition :
Creates several instances of equidistant planes 
between two others at a time.";
//
// ========================================================================================
// ========================================================================================
//							Commandes d'analyse de FreeStyle
// ========================================================================================
// ========================================================================================
// == Connect Checker ==
CATHybridPartCommandHeader.CATHpwConnectCheckerHdr.Title        = "Connect Checker Analysis...";
CATHybridPartCommandHeader.CATHpwConnectCheckerHdr.Help         = "Performs a connection analysis";
CATHybridPartCommandHeader.CATHpwConnectCheckerHdr.ShortHelp    = "Connect Checker Analysis";
CATHybridPartCommandHeader.CATHpwConnectCheckerHdr.LongHelp     = 
"Connect Checker Analysis
checks connections between surfaces/curves
and measures G0, G1, G2 and G3 deviations.";
// == Analyse Gaussienne ==
CATHybridPartCommandHeader.CATHpwGaussCurvatureHdr.Title        = "Surfacic Curvature Analysis";
CATHybridPartCommandHeader.CATHpwGaussCurvatureHdr.Help         = "Analyzes the Gaussian curvature on shape";
CATHybridPartCommandHeader.CATHpwGaussCurvatureHdr.ShortHelp    = "Surfacic Curvature Analysis";
CATHybridPartCommandHeader.CATHpwGaussCurvatureHdr.LongHelp     = 
"Gaussian Curvature
Maps a color texture on a surface
according to the curvature in each point.";
// == Draft Analyse ==
CATHybridPartCommandHeader.CATHpwDraftAnalysisHdr.Title         = "Feature Draft Analysis";
CATHybridPartCommandHeader.CATHpwDraftAnalysisHdr.Help          = "Analyses the draft direction on selected surfaces";
CATHybridPartCommandHeader.CATHpwDraftAnalysisHdr.ShortHelp     = "Feature Draft Analysis";
CATHybridPartCommandHeader.CATHpwDraftAnalysisHdr.LongHelp      = 
"Feature Draft Analysis
Maps a color texture on surfaces according to
the delta angle value between the normal vector
and the draft direction on each surface point.";
// == Porcupine Analysis ==
CATHybridPartCommandHeader.CATHpwPorcAnalysisHdr.Title               = "Porcupine Curvature Analysis";
CATHybridPartCommandHeader.CATHpwPorcAnalysisHdr.Help                = "Performs a porcupine curvature analysis on any curve";
CATHybridPartCommandHeader.CATHpwPorcAnalysisHdr.ShortHelp           = "Porcupine Curvature Analysis";
CATHybridPartCommandHeader.CATHpwPorcAnalysisHdr.LongHelp            =
"Porcupine Curvature Analysis 
Analyzes the curvature of
a set of curves using
a porcupine representation.";
// == Distance Analysis ==
CATHybridPartCommandHeader.CATHpwDynDistanceHdr.Title               = "Distance Analysis";
CATHybridPartCommandHeader.CATHpwDynDistanceHdr.Help                = "Analyzes distances between two sets of elements";
CATHybridPartCommandHeader.CATHpwDynDistanceHdr.ShortHelp           = "Distance Analysis";
CATHybridPartCommandHeader.CATHpwDynDistanceHdr.LongHelp            =
"Distance Analysis 
Analyzes the distance between
a set of curves or surfaces 
and a set of any geometric elements.";

// == IsoParametric Curve ==
CATHybridPartCommandHeader.CATHpwIsoparamCrvHdr.Title = "Isoparametric Curve";
CATHybridPartCommandHeader.CATHpwIsoparamCrvHdr.ShortHelp = "Isoparametric Curve";
CATHybridPartCommandHeader.CATHpwIsoparamCrvHdr.Help = "Creates isoparametric curves on a support";
CATHybridPartCommandHeader.CATHpwIsoparamCrvHdr.LongHelp =
"Isoparametric Curve
Creates isoparametric curves on a support";
//
// ========================================================================================
// ========================================================================================
//			      		Commandes relatives au mode Work on Support
// ========================================================================================
// ========================================================================================
CATHybridPartCommandHeader.CATHybridPartSetCurrentWS.Title            = "Set As Current";
CATHybridPartCommandHeader.CATHybridPartSetCurrentWS.Help             = "Sets the working support as current";
CATHybridPartCommandHeader.CATHybridPartSetCurrentWS.ShortHelp        = "Sets the working support as current";
CATHybridPartCommandHeader.CATHybridPartSetCurrentWS.LongHelp         = "Sets the working support as current.";
//
CATHybridPartCommandHeader.CATHybridPartSetNotCurrentWS.Title         = "Set As Not Current";
CATHybridPartCommandHeader.CATHybridPartSetNotCurrentWS.Help          = "Sets the working support as not current";
CATHybridPartCommandHeader.CATHybridPartSetNotCurrentWS.ShortHelp     = "Sets the working support as not current";
CATHybridPartCommandHeader.CATHybridPartSetNotCurrentWS.LongHelp      = "Sets the working support as not current.";
// == OnSupport== 
CATHybridPartCommandHeader.CATHybridPartShowGrid.Title                = "Work on Support";
CATHybridPartCommandHeader.CATHybridPartShowGrid.Help                 = "Allows working on support";
CATHybridPartCommandHeader.CATHybridPartShowGrid.ShortHelp            = "Work on Support";
CATHybridPartCommandHeader.CATHybridPartShowGrid.LongHelp             = 
"Work on Support
Allows working on a support plane or surface.";
// == OnSupport Activity ==
CATHybridPartCommandHeader.CATHybridPartChangeActivityWS.Title        = "Working Supports Activity";
CATHybridPartCommandHeader.CATHybridPartChangeActivityWS.Help         = "Allows activating/deactivating working supports";
CATHybridPartCommandHeader.CATHybridPartChangeActivityWS.ShortHelp    = "Working Supports Activity";
CATHybridPartCommandHeader.CATHybridPartChangeActivityWS.LongHelp     = 
"Working Supports Activity
Allows deactivating current working support when nothing is selected,
or activating/deactivating selected working support";
//
// ========================================================================================
// ========================================================================================
//							Commandes relatives au mode Datum
// ========================================================================================
// ========================================================================================
// == DatumCreationMode== 
CATHybridPartCommandHeader.CATHybridPartDatumMode.Title         = "Create Datum";
CATHybridPartCommandHeader.CATHybridPartDatumMode.Help          = "Creates a datum feature";
CATHybridPartCommandHeader.CATHybridPartDatumMode.ShortHelp     = "Create Datum";
CATHybridPartCommandHeader.CATHybridPartDatumMode.LongHelp      = 
"Create Datum
Creates a datum feature (with no history).";
//
// ========================================================================================
// ========================================================================================
//							Commandes Developed Shapes
// ========================================================================================
// ========================================================================================
// == Develop == 
CATHybridPartCommandHeader.CATHybridPartDevelopHdr.Title                    = "Develop...";
CATHybridPartCommandHeader.CATHybridPartDevelopHdr.Help                     = "Creates a developed curve on a revolution surface";
CATHybridPartCommandHeader.CATHybridPartDevelopHdr.ShortHelp                = "Develop";
CATHybridPartCommandHeader.CATHybridPartDevelopHdr.LongHelp                 = "Develop Creates a developed curve on a revolution surface";
// == Unfold == 
CATHybridPartCommandHeader.CATHybridPartUnfoldHdr.Title                    = "Unfold...";
CATHybridPartCommandHeader.CATHybridPartUnfoldHdr.Help                     = "Creates a unfolded surface";
CATHybridPartCommandHeader.CATHybridPartUnfoldHdr.ShortHelp                = "Unfold";
CATHybridPartCommandHeader.CATHybridPartUnfoldHdr.LongHelp                 = "Unfold Creates a Unfolded surface";
// == Transfer == 
CATShapeDesignCommandHeader.CATHybridPartTransferHdr.Title                    = "Transfer...";
CATShapeDesignCommandHeader.CATHybridPartTransferHdr.Help                     = "Creates transfered elements";
CATShapeDesignCommandHeader.CATHybridPartTransferHdr.ShortHelp                = "Transfer";
CATShapeDesignCommandHeader.CATHybridPartTransferHdr.LongHelp                 = "Transfer Creates a transfered elements";

