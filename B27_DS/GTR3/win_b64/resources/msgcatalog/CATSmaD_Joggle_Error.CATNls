// ===============================================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
// ===============================================================================================
// Objectif         : Catalogue des erreurs du Joggle 
// Responsable      : MDD
// date de cr�ation : 10/04/2002
// Commentaires     : 
//
// Revue LGK le 09/01/2004
// ===============================================================================================

//-------------------------------------------------------------------
// JoggleCompensation : Specs d'entr�e incorrectes
//-------------------------------------------------------------------
CATSmaD_JoggleCompensation_Error_ERR_0001.Request    = "Unable to launch joggle compensation";
CATSmaD_JoggleCompensation_Error_ERR_0001.Diagnostic = "The part's specifications are incorrect.";
CATSmaD_JoggleCompensation_Error_ERR_0001.Advice     = "Change the part's specifications.";

CATSmaD_JoggleCompensation_Error_ERR_0010.Request    = "Unable to launch joggle compensation";
CATSmaD_JoggleCompensation_Error_ERR_0010.Diagnostic = "The Path to the Method 1 design table has changed.";
CATSmaD_JoggleCompensation_Error_ERR_0010.Advice     = "Edit the FlangeSurf to solve this problem.";

CATSmaD_JoggleCompensation_Error_ERR_0020.Request    = "Unable to launch joggle compensation";
CATSmaD_JoggleCompensation_Error_ERR_0020.Diagnostic = "A Path to the Method 2 design table has changed.";
CATSmaD_JoggleCompensation_Error_ERR_0020.Advice     = "Edit the FlangeSurf to solve this problem.";

//-------------------------------------------------------------------
// Joggle : Specs d'entr�e incorrectes
//-------------------------------------------------------------------
CATSmaD_Joggle_Error_ERR_0001.Request    = "Unable to launch joggle creation";
CATSmaD_Joggle_Error_ERR_0001.Diagnostic = "The part's specifications are incorrect.";
CATSmaD_Joggle_Error_ERR_0001.Advice     = "Change the part's specifications.";

CATSmaD_Joggle_Error_ERR_0002.Request    = "Unable to launch joggle creation";
CATSmaD_Joggle_Error_ERR_0002.Diagnostic = "The joggle's specifications are incorrect.";
CATSmaD_Joggle_Error_ERR_0002.Advice     = "Change the joggle specifications so as to make them appropriate for this part.";

CATSmaD_Joggle_Error_ERR_0010.Request    = "";
CATSmaD_Joggle_Error_ERR_0010.Diagnostic = "Thickness not found or not valid.";
CATSmaD_Joggle_Error_ERR_0010.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0011.Request    = "";
CATSmaD_Joggle_Error_ERR_0011.Diagnostic = "Bend radius not found or not valid.";
CATSmaD_Joggle_Error_ERR_0011.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0012.Request    = "";
CATSmaD_Joggle_Error_ERR_0012.Diagnostic = "Base feature not found or not valid.";
CATSmaD_Joggle_Error_ERR_0012.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0013.Request    = "";
CATSmaD_Joggle_Error_ERR_0013.Diagnostic = "Joggle plane not found or not valid.";
CATSmaD_Joggle_Error_ERR_0013.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0014.Request    = "";
CATSmaD_Joggle_Error_ERR_0014.Diagnostic = "Runout direction not found or not valid.";
CATSmaD_Joggle_Error_ERR_0014.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0015.Request    = "";
CATSmaD_Joggle_Error_ERR_0015.Diagnostic = "Depth direction not found or not valid.";
CATSmaD_Joggle_Error_ERR_0015.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0016.Request    = "";
CATSmaD_Joggle_Error_ERR_0016.Diagnostic = "Start radius not found or not valid.";
CATSmaD_Joggle_Error_ERR_0016.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0017.Request    = "";
CATSmaD_Joggle_Error_ERR_0017.Diagnostic = "End radius not found or not valid.";
CATSmaD_Joggle_Error_ERR_0017.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0018.Request    = "";
CATSmaD_Joggle_Error_ERR_0018.Diagnostic = "Joggle plane is ambiguous.";
CATSmaD_Joggle_Error_ERR_0018.Advice     = "Select a plane near the solution you want to keep.";

CATSmaD_Joggle_Error_ERR_0070.Request    = "";
CATSmaD_Joggle_Error_ERR_0070.Diagnostic = "The offset surface is invalid.";
CATSmaD_Joggle_Error_ERR_0070.Advice     = "Modify the offset surface or select another one.";

CATSmaD_Joggle_Error_ERR_0071.Request    = "";
CATSmaD_Joggle_Error_ERR_0071.Diagnostic = "The intersection of the joggle base feature and the offset surface can not be computed.";
CATSmaD_Joggle_Error_ERR_0071.Advice     = "Modify the offset surface or select another one.";

CATSmaD_Joggle_Error_ERR_0072.Request    = "";
CATSmaD_Joggle_Error_ERR_0072.Diagnostic = "The projection of the EOP of the surfacic flange on the offset surface can not be computed.";
CATSmaD_Joggle_Error_ERR_0072.Advice     = "Modify the offset surface or select another one.";

//-------------------------------------------------------------------
// Joggle : Build FD
//-------------------------------------------------------------------
//
CATSmaD_Joggle_Warning_0100 = "Impossible to compute the impact of this Joggle on the folded characteristic curves";

CATSmaD_Joggle_Error_ERR_0100.Request    = "";
CATSmaD_Joggle_Error_ERR_0100.Diagnostic = "An error occurred while creating the folded joggle.";
CATSmaD_Joggle_Error_ERR_0100.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0101.Request    = "";
CATSmaD_Joggle_Error_ERR_0101.Diagnostic = "The folded specifications of the joggle could not be found or are incorrect.";
CATSmaD_Joggle_Error_ERR_0101.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0102.Request    = "";
CATSmaD_Joggle_Error_ERR_0102.Diagnostic = "The folded geometry of the joggle could not be found or is incorrect.";
CATSmaD_Joggle_Error_ERR_0102.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0130.Request    = "";
CATSmaD_Joggle_Error_ERR_0130.Diagnostic = "The runout geometry can not be computed.";
CATSmaD_Joggle_Error_ERR_0130.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0140.Request    = "";
CATSmaD_Joggle_Error_ERR_0140.Diagnostic = "Intersection between a joggle plane and the OML of its base feature not found.";
CATSmaD_Joggle_Error_ERR_0140.Advice     = "Modify the parameters of the joggle's base feature.";

CATSmaD_Joggle_Error_ERR_0141.Request    = "";
CATSmaD_Joggle_Error_ERR_0141.Diagnostic = "Intersection between the joggle START plane and the OML of its base feature not found.";
CATSmaD_Joggle_Error_ERR_0141.Advice     = "Modify the parameters of the joggle's base feature.";

CATSmaD_Joggle_Error_ERR_0142.Request    = "";
CATSmaD_Joggle_Error_ERR_0142.Diagnostic = "Intersection between the joggle END plane and the OML of its base feature not found.";
CATSmaD_Joggle_Error_ERR_0142.Advice     = "Modify the parameters of the joggle's base feature.";

CATSmaD_Joggle_Error_ERR_0150.Request    = "";
CATSmaD_Joggle_Error_ERR_0150.Diagnostic = "At least one of the joggle planes can not be computed.";
CATSmaD_Joggle_Error_ERR_0150.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0160.Request    = "";
CATSmaD_Joggle_Error_ERR_0160.Diagnostic = "Previous support surface not found.";
CATSmaD_Joggle_Error_ERR_0160.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0170.Request    = "";
CATSmaD_Joggle_Error_ERR_0170.Diagnostic = "Previous relimited surface not found.";
CATSmaD_Joggle_Error_ERR_0170.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0180.Request    = "";
CATSmaD_Joggle_Error_ERR_0180.Diagnostic = "Offset support surface not found.";
CATSmaD_Joggle_Error_ERR_0180.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0190.Request    = "";
CATSmaD_Joggle_Error_ERR_0190.Diagnostic = "Offset relimited surface not found.";
CATSmaD_Joggle_Error_ERR_0190.Advice     = "Modify the parameters you entered.";

//-------------------------------------------------------------------
// Joggle : Build FP
//-------------------------------------------------------------------
CATSmaD_Joggle_Warning_0200 = "Impossible to compute the impact of this Joggle on the flat characteristic curves";

CATSmaD_Joggle_Error_ERR_0200.Request    = "";
CATSmaD_Joggle_Error_ERR_0200.Diagnostic = "An error occurred while creating the flattened joggle.";
CATSmaD_Joggle_Error_ERR_0200.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0201.Request    = "";
CATSmaD_Joggle_Error_ERR_0201.Diagnostic = "The flattened Runout Face of the joggle is twisted.";
CATSmaD_Joggle_Error_ERR_0201.Advice     = "Decrease the EOP Length of the Surfacic Flange.";

//-------------------------------------------------------------------
// JoggleSupport : Build FD
//-------------------------------------------------------------------
//
CATSmaD_Joggle_Error_ERR_0300.Request    = "";
CATSmaD_Joggle_Error_ERR_0300.Diagnostic = "An error occurred while creating the folded joggle support.";
CATSmaD_Joggle_Error_ERR_0300.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0301.Request    = "";
CATSmaD_Joggle_Error_ERR_0301.Diagnostic = "The folded specifications of the joggle support could not be found or are incorrect.";
CATSmaD_Joggle_Error_ERR_0301.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0302.Request    = "";
CATSmaD_Joggle_Error_ERR_0302.Diagnostic = "The folded geometry of the joggle support could not be found or is incorrect.";
CATSmaD_Joggle_Error_ERR_0302.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0310.Request    = "";
CATSmaD_Joggle_Error_ERR_0310.Diagnostic = "The selected Runout Formula can not be applied on this kind of Base Feature.";
CATSmaD_Joggle_Error_ERR_0310.Advice     = "Select another Runout Formula.";

//-------------------------------------------------------------------
// JoggleSupport : Build FP
//-------------------------------------------------------------------

CATSmaD_Joggle_Error_ERR_0400.Request    = "";
CATSmaD_Joggle_Error_ERR_0400.Diagnostic = "An error occurred while creating the flattened joggle support.";
CATSmaD_Joggle_Error_ERR_0400.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0401.Request    = "";
CATSmaD_Joggle_Error_ERR_0401.Diagnostic = "The flattened specifications of the joggle support could not be found or are incorrect.";
CATSmaD_Joggle_Error_ERR_0401.Advice     = "Modify the parameters you entered.";

CATSmaD_Joggle_Error_ERR_0402.Request    = "";
CATSmaD_Joggle_Error_ERR_0402.Diagnostic = "The flattened geometry of the joggle support could not be found or is incorrect.";
CATSmaD_Joggle_Error_ERR_0402.Advice     = "Modify the parameters you entered.";









