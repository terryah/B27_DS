DialogBoxTitle                                   = "Curve Conversion";

LabelElement.Title                               = "Elements: ";
LabelElement.LongHelp                            = "Specifies the curves to be processed.";
LabelElement.ShortHelp                           = "Select curves";

TabOptions.Title                                 = "Options";

CheckButtonApproximation.Title                   = "Approximation";
CheckButtonApproximation.LongHelp                = "Activates/deactivates the Approximation tab. This way, you can
e. g. create curves from surface edges without approximation.";
CheckButtonApproximation.ShortHelp               = "Approximation tab ON/OFF";

LabelCellStructure.Title                         = "Cell structure: ";
LabelCellStructure.LongHelp                      = "- Use Input Cell Structure -
  Approximates each cell of a domain separately.
  The Cell Fusion options are not available.
- Enable Cell Fusion -
  Approximates all adjacent cells of a domain as one entity
  whose transition quality meets at least the value specified
  in Continuity (segmentation of the domain in continuity ranges).
  The segmentation will be applied to each continuity range.
  The transition parameters will be used as preferential parameters
  for further processing with the settings on the Approximation tab.
- Manage Cell Boundaries -
  To enable this option, Single Result on the Output tab must
  be selected.
  Moves, inserts, and deletes cell boundaries.
  For each input cell boundary a manipulator is displayed.
  The manipulator color indicates the current status of the
  corresponding cell boundary:
  The Cell Fusion options are not available.
- Add Cell Boundaries -
  Adds cell boundaries to the curve. Existing cell boundaries
  are displayed as grey squares. To add a new cell boundary,
  click the wanted boundary position on the curve.
- Remove Cell Boundaries - 
  The cell boundaries are displayed as cyan squares. Moving
  the mouse over a cell boundary will highlight it in orange.
  Clicking the highlighted cell boundary deletes it.
- Add / Remove Cell Boundaries -
  This mode is a combination between the Add Cell Boundaries
  and the Remove Cell Boundaries mode, i. e. you can add or
  delete cell boundaries without switching the mode.";
LabelCellStructure.ShortHelp                     = "Cell structure options";

ComboCellStructure.LongHelp                      = "- Use Input Cell Structure -
  Approximates each cell of a domain separately.
  The Cell Fusion options are not available.
- Enable Cell Fusion -
  Approximates all adjacent cells of a domain as one entity
  whose transition quality meets at least the value specified
  in Continuity (segmentation of the domain in continuity ranges).
  The segmentation will be applied to each continuity range.
  The transition parameters will be used as preferential parameters
  for further processing with the settings on the Approximation tab.
- Manage Cell Boundaries -
  To enable this option, Single Result on the Output tab must
  be selected.
  Moves, inserts, and deletes cell boundaries.
  For each input cell boundary a manipulator is displayed.
  The manipulator color indicates the current status of the
  corresponding cell boundary:
  The Cell Fusion options are not available.
- Add Cell Boundaries -
  Adds cell boundaries to the curve. Existing cell boundaries
  are displayed as grey squares. To add a new cell boundary,
  click the wanted boundary position on the curve.
- Remove Cell Boundaries - 
  The cell boundaries are displayed as cyan squares. Moving
  the mouse over a cell boundary will highlight it in orange.
  Clicking the highlighted cell boundary deletes it.
- Add / Remove Cell Boundaries -
  This mode is a combination between the Add Cell Boundaries
  and the Remove Cell Boundaries mode, i. e. you can add or
  delete cell boundaries without switching the mode.";
ComboCellStructure.ShortHelp                     = "Cell structure options";

//combo box options!
CellStructure.UseInputCellStructure              = "Use Input Cell Structure   ";
CellStructure.EnableCellFusion                   = "Enable Cell Fusion   ";
CellStructure.ManageCellBoundaries               = "Manage Cell Boundaries   ";
CellStructure.AddCellBoundaries                  = "Add Cell Boundaries";
CellStructure.RemoveCellBoundaries               = "Remove Cell Boundaries";
CellStructure.AddAndRemoveCellBoundaries         = "Add / Remove Cell Boundaries";

FrameCellFusion.Title                            = "Cell Fusion";
FrameCellFusion.LongHelp                         = "The Cell Fusion options are only available if
Enable Cell Fusion is selected.";

LabelContinuity.Title                            = "Continuity: ";
LabelContinuity.LongHelp                         = "The cells are only merged if at least
the specified continuity is kept.";
LabelContinuity.ShortHelp                        = "Set continuity";

Continuity_CellFusion.Button_G0.LongHelp         = "The cells are only merged if G0 continuity is kept.
To keep the number of segments as low as possible, the tolerance
is not considered independently from the specified approximation
parameters.";
Continuity_CellFusion.Button_G0.ShortHelp        = "Merge if G0 is kept";
Continuity_CellFusion.Button_G1.LongHelp         = "The cells are only merged if G1 continuity is kept.";
Continuity_CellFusion.Button_G1.ShortHelp        = "Merge if G1 is kept";
Continuity_CellFusion.Button_G2.LongHelp         = "The cells are only merged if G2 continuity is kept.";
Continuity_CellFusion.Button_G2.ShortHelp        = "Merge if G2 is kept";
Continuity_CellFusion.Button_G3.LongHelp         = "Curves with G3-continuity are 
joined in the same cell.";
Continuity_CellFusion.Button_G3.ShortHelp        = "Merge if G3 is kept";

CheckButtonKeepStructure.Title                   = "Keep input cell structure";
CheckButtonKeepStructure.LongHelp                = "After the approximation operation, the resulting geometry
is re-broken at the transitions.";
CheckButtonKeepStructure.ShortHelp               = "Re-break result at transition";


TabApproximation.Title                           = "Approximation";
TabOutput.Title                                  = "Output";


//cannot be assigned
CheckButtonEnableCellFusion.LongHelp = "Works only if Approximation is ON and Output 
Single-Result with Topology Check is selected.";
CheckButtonEnableCellFusion.ShortHelp = "Option: Enable Cell Fusion.";
CheckButtonEnableCellFusion.Title = "Enable Cell Fusion";
CheckButtonMultiResultOutput.Title = "Multi-Result";
CheckButtonParameterizationOn.Title = "On";
FrameParameterization.Title = "Parametrization";
FramePriority.Title = "Priority";
FrameType.Title = "Type";
LabelType.Title = "Type: ";
RadioButtonArcLength.Title = "Arc Length";
RadioButtonChordal.Title = "Chordal";
RadioButtonEquidistant.Title = "Equidistant";
RadioButtonShape.Title = "Shape";
RadioButtonVariation.Title = "Variation";

MultipleCellWarning.Title = "Boundary management does not work on multiple cells input.";

