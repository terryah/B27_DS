// COPYRIGHT ICEM Technologies 2005

//Frame Geometry to be imported
ImportOptions.HeaderFrame.Global.Title                                                 = "Geometry to be imported";
ImportOptions.HeaderFrame.Global.LongHelp                                              = "Select the ICEM DB element types to be imported  and
determine in which Part element types they shall be converted.";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportPoints.Title                      = "Points";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportPoints.ShortHelp                  = "Converts ICEM DB points to Part datum points.";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedPoints.Title                      = "Datum Point";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedPoints.ShortHelp                  = "Converts ICEM DB points to Part datum points.";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportRawData.Title                     = "Raw Data";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportRawData.ShortHelp                 = "Converts ICEM DB raw data to Part datum point clouds.
or datum wires of lines";
Combo.MappedRawData.DatumPoints                                                        = "Datum Point Cloud";
Combo.MappedRawData.DatumSingle                                                        = "Datum Wire of Lines";
//ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedRawData.Title    = "Datum Line Wire";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportCurves.Title                      = "Curves";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportCurves.ShortHelp                  = "Converts ICEM DB curves to Part datum curves.";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedCurves.Title                      = "Datum Curve";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedCurves.ShortHelp                  = "Converts ICEM DB curves to Part datum curves.";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportScans.Title                       = "Scans";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportScans.ShortHelp                   = "Converts ICEM DB scans to Part datum meshes or datum point clouds.";
Combo.MappedScans.DatumMesh                                                            = "Datum Mesh";
Combo.MappedScans.DatumPoints                                                          = "Datum Point Cloud";
//ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedScans.Title      = "Datum CloudData";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportSurfaces.Title                    = "Surfaces";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportSurfaces.ShortHelp                = "Converts ICEM DB surfaces to Part datum surfaces.";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedSurfaces.Title                    = "Datum Surface";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedSurfaces.ShortHelp                = "Converts ICEM DB surfaces to Part datum surfaces.";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportFaces.Title                       = "Faces";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportFaces.ShortHelp                   = "Converts ICEM DB faces to Part datum surfaces.";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedFaces.Title                       = "Datum Surface";
ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedFaces.ShortHelp                   = "Converts ICEM DB faces to Part datum surfaces.";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWires.Title                       = "Curves Composite";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWires.ShortHelp                   = "Converts ICEM DB Curves Composite to Part curve joins, datum wires, or datum curves.";
Combo.MappedWires.Join                                                                 = "Curve Join";
Combo.MappedWires.DatumSingle                                                          = "Datum Wire";
Combo.MappedWires.Split                                                                = "Datum Curves";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportShells.Title                      = "Shells";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportShells.ShortHelp                  = "Converts ICEM DB shells to Part datum surface joins, datum shells, or datum surfaces.";
Combo.MappedShells.Join                                                                = "Surface Join";
Combo.MappedShells.DatumSingle                                                         = "Datum Shell";
Combo.MappedShells.Split                                                               = "Datum Surfaces";
//ImportOptions.IconAndOptionsFrame.OptionsFrame.MappedShells.Title     = "Shells";

ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWorkplanes.Title                  = "Workplanes";
ImportOptions.IconAndOptionsFrame.OptionsFrame.ImportWorkplanes.ShortHelp              = "Converts ICEM DB surfaces to Part planes or axis systems.";
Combo.MappedWorkplanes.Plane                                                           = "Plane";
Combo.MappedWorkplanes.Axis                                                            = "Axis System";

// Frame Continuity Options
ContinuityOptions.HeaderFrame.Global.Title                                             = "Continuity Optimization of Edge Curves and Independent Curves";
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.NoOptimize.Title                    = "No Optimization";                          
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.NoOptimize.ShortHelp                = "Adapts elements to modeler by
cutting them at discontinuity points.";                         
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.AutoOptimize.Title                  = "Automatic Optimization";                          
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.AutoOptimize.ShortHelp              = "Adapts elements to modeler by
automatically controlled geometric deformation.";                         
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.ToleranceOptimize.Title             = "Tolerance";                          
ContinuityOptions.IconAndOptionsFrame.OptionsFrame.ToleranceOptimize.ShortHelp         = "Allows maximum deformation (in millimeter)
in the optimization of curves and surfaces.";                         

// Frame Attributes to be imported
ImportAttributes.HeaderFrame.Global.Title                                              = "Attributes to be imported";
ImportAttributes.HeaderFrame.Global.LongHelp                                           = "Imports colors, layers, lists, selection sets, lights,
and materials defined in the imported ICEM databases.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportColors.Title                   = "Colors";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportColors.ShortHelp               = "Imports ICEM DB colors.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLayers.Title                   = "Layers";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLayers.ShortHelp               = "Imports ICEM DB layers.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLists.Title                    = "Lists";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLists.ShortHelp                = "Imports ICEM DB lists and converts them to selection sets.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportSelectionSets.Title            = "Selection Sets";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportSelectionSets.ShortHelp        = "Imports ICEM DB selection sets.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLights.Title                   = "Lights";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportLights.ShortHelp               = "Imports ICEM DB lights.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportMaterials.Title                = "Materials";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportMaterials.ShortHelp            = "Imports ICEM DB materials.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.TexturePathes.Title                  = "Texture Paths";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.TexturePathes.ShortHelp              = "Opens the Texture Paths dialog box where
you can add and remove texture paths.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.AllowGeomSetMaterial.Title           = "Allow Material in Geometrical Set";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.AllowGeomSetMaterial.ShortHelp       = "Assigns a material to a geometrical set with which all
materials belonging to the set are displayed.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.AllowGeomSetMaterial.LongHelp        = "You can assign another material to child sets to display
them with this material.
If all two dimensional elements of a group have individual materials
the most used material is attached to the corresponding geometrical set.
No material will be attached to the group if it includes a two
dimensional geometrical object with no individual material assigned.
A material will be attached to a geometrical element only
if it is different from the geometrical set's material.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportAnnotations.Title              = "Texts";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportAnnotations.ShortHelp          = "Imports ICEM DB texts with leader.";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportViews.Title                    = "Views";
ImportAttributes.IconAndOptionsFrame.OptionsFrame.ImportViews.ShortHelp                = "Imports ICEM DB views as cameras.";

//Frame Additional Options
ImportAddOptions.HeaderFrame.Global.Title                                              = "Additional Options";
ImportAddOptions.IconAndOptionsFrame.OptionsFrame.SkipIcemdbRootNode.Title             = "Skip root node of the ICEM DB for import";
ImportAddOptions.IconAndOptionsFrame.OptionsFrame.Use2DCurveForC2TrimmedSurfaces.Title = "Try to use 2D curves for import of C2 continuous trimmed surfaces";
ImportAddOptions.IconAndOptionsFrame.OptionsFrame.ImportSurfaceAsPlane.Title           = "Import planar surface as plane if possible";

//Frame Geometry to be exported
ExportGeometry.HeaderFrame.Global.Title                                                = "Geometry to be exported";
ExportGeometry.HeaderFrame.Global.LongHelp                                             = "All selected CATPart element types are saved in the ICEM DB.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPoints.Title                     = "Points";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPoints.ShortHelp                 = "Converts Part points to ICEM DB points.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurves.Title                     = "Curves";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurves.ShortHelp                 = "Converts Part curves to ICEM DB curves.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaces.Title                   = "Surfaces";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaces.ShortHelp               = "Converts Part surfaces to ICEM DB faces.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPointClouds.Title                = "Point Clouds";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPointClouds.ShortHelp            = "Converts Part point clouds to ICEM DB raw data.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportMeshes.Title                     = "Meshes";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportMeshes.ShortHelp                 = "Converts Part meshes to ICEM DB scans.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaceJoins.Title               = "Surface Joins";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportSurfaceJoins.ShortHelp           = "Converts Part surface joins to ICEM DB shells.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurveJoins.Title                 = "Curve Joins";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportCurveJoins.ShortHelp             = "Converts Part curve joins to ICEM DB curves composite.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportAxisSystems.Title                = "Axis Systems";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportAxisSystems.ShortHelp            = "Converts Part axis systems to ICEM DB work planes.";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPlanes.Title                     = "Planes";
ExportGeometry.IconAndOptionsFrame.OptionsFrame.ExportPlanes.ShortHelp                 = "Converts Part planes to ICEM DB work planes.";

//Frame Attributes to be exported
ExportAttributes.HeaderFrame.Global.Title                                              = "Attributes to be exported";
ExportAttributes.HeaderFrame.Global.LongHelp                                           = "The selected attributes defined in the Part are saved in the ICEM DB.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportColors.Title                   = "Colors";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportColors.ShortHelp               = "Exports Part colors.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLayers.Title                   = "Layers";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLayers.ShortHelp               = "Exports Part layers.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportSelectionSets.Title            = "Selection Sets";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportSelectionSets.ShortHelp        = "Exports Part selection sets and converts them to ICEM DB lists.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportDisplaySets.Title              = "Display Sets";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportDisplaySets.ShortHelp          = "Exports Part display sets and converts them to ICEM DB lists.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLights.Title                   = "Lights";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportLights.ShortHelp               = "Exports Part lights.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportMaterials.Title                = "Materials";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportMaterials.LongHelp             = "Materials can be exported together with texture data.
The export of materials and texture data is only possible with ICEM DB version 4.12.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportMaterials.ShortHelp            = "Exports Part materials.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.TextureFrame.ExportBrowse.Title      = "Texture Path";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.TextureFrame.ExportBrowse.LongHelp   = "Opens the file browser for selecting a texture path where the texture
images shall be exported. The path is displayed in the text field.
If the text field is empty, no texture data are exported.
The export of materials and texture data is only possible with ICEM DB version 4.12.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.TextureFrame.ExportBrowse.ShortHelp  = "Select texture path";
ExportFolderChooser.Title                                                              = "Select Texture Path";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportAnnotations.Title              = "Text with Leader";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportAnnotations.ShortHelp          = "Exports texts with leader.";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportViews.Title                    = "Views";
ExportAttributes.IconAndOptionsFrame.OptionsFrame.ExportViews.ShortHelp                = "Exports views.";

//Frame Additional Export Options
ExportAddOptions.HeaderFrame.Global.Title                                              = "Additional Export Options";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.IcemdbVersion.Title                  = "ICEM DB Version";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.IcemdbVersion.LongHelp               = "Selects the ICEM DB version for the export.
Materials and textures can only be exported to ICEM DB version 4.12.
If 4.11 is selected, the Materials check box and Texture Path
button in the 'Attributes to be exported' area are disabled
and no material will be exported.";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.IcemdbVersion.ShortHelp              = "Select ICEM DB version for the export";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.ExportHidden.Title                   = "Export Hidden Elements";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.ExportHidden.ShortHelp               = "Exports also elements in Hidden mode into the ICEM DB.";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.TruncateUnderlyingSurface.Title      = "Crop underlying Surface";
ExportAddOptions.IconAndOptionsFrame.OptionsFrame.TruncateUnderlyingSurface.ShortHelp  = "Crops underlying surfaces at face boundaries.";
