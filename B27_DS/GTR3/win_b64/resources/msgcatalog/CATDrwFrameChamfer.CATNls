//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 2000 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwFrameChamfer
// LOCATION    :    DraftingIntUI
// AUTHOR      :    fgx
// BUT         :    Properties : Tab "Chamfer" 
// DATE        :    07.03.2000
//-------------------------------------------------------------------------------------
// DESCRIPTION :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS    user    date        purpose
//    HISTORY       ----    ----        -------
//=====================================================================================


_titleValFormat.Title="Value Format";
_titleValFormat.LongHelp=
"Sets the display format of the value.";

_frameValFormat._radioValFormatDistDist.Title="Distance x Distance";
_frameValFormat._radioValFormatDistDist.LongHelp=
"Sets a value format 
of type \"distance x distance\".";

_frameValFormat._radioValFormatDistAngle.Title="Distance x Angle";
_frameValFormat._radioValFormatDistAngle.LongHelp=
"Sets a value format 
of type \"distance x angle\".";

_frameValFormat._radioValFormatAngleDist.Title="Angle x Distance";
_frameValFormat._radioValFormatAngleDist.LongHelp=
"Sets a value format 
of type \"angle x distance\".";

_frameValFormat._radioValFormatDistance.Title="Distance";
_frameValFormat._radioValFormatDistance.LongHelp=
"Sets a value format 
of type \"distance\".";

_titleDescription.Title="Numerical Display";
_titleDescription.LongHelp=
"Sets the description of numerical display of the value.";

_frameDescription._frameMoreInfoMain1._pushMoreInfoMain1.ShortHelp="Browse main description";
_frameDescription._frameMoreInfoMain2._pushMoreInfoMain2.ShortHelp="Browse main description";
_frameDescription._frameMoreInfoDual1._pushMoreInfoDual1.ShortHelp="Browse dual description";
_frameDescription._frameMoreInfoDual2._pushMoreInfoDual2.ShortHelp="Browse dual description";

_frameDescription._labelDescriptionMain1.Title="Main description";
_frameDescription._labelUnitOrderMain.Title="Display: ";
_frameDescription._labelDescriptionDual1.Title="Dual description";
_frameDescription._labelUnitOrderDual.Title="Display: ";

_frameDescription._comboDescriptionMain1.LongHelp=
"Sets the first main value description of numerical display.";
_frameDescription._comboDescriptionMain2.LongHelp=
"Sets the second main value description of numerical display.";

_frameDescription._comboUnitOrderMain1.LongHelp=
"Sets the number of unit factors to display for first main value.";
_frameDescription._comboUnitOrderMain2.LongHelp=
"Sets the number of unit factors to display for second main value.";

_frameDescription._comboDescriptionDual1.LongHelp=
"Sets the first dual value description of numerical display.";
_frameDescription._comboDescriptionDual2.LongHelp=
"Sets the second dual value description of numerical display.";

_frameDescription._comboUnitOrderDual1.LongHelp=
"Sets the number of unit factors to display for first dual value.";
_frameDescription._comboUnitOrderDual2.LongHelp=
"Sets the number of unit factors to display for second dual value.";

_titleRepType.Title="Representation Type";
_titleRepType.LongHelp=
"Sets the type of representation of the dimension.";

_frameRepType._radioRepTypeDistance.Title="Two Symbols";
_frameRepType._radioRepTypeDistance.LongHelp=
"Sets a representation 
of type two symbols.";

_frameRepType._radioRepTypeRadius.Title="One Symbol";
_frameRepType._radioRepTypeRadius.LongHelp=
"Sets a representation 
of type one symbol.";

ONE_FACTOR="1 factor";
TWO_FACTORS="2 factors";
THREE_FACTORS="3 factors";

ComboValueFormatLine0="Decimal";
ComboValueFormatLine1="Fractional";

_frameDescription.LabelFormatMain.Title="Format: ";
_frameDescription.LabelFormatDual.Title="Format: ";
_frameDescription.LabelPrecisionMain.Title="Precision: ";
_frameDescription.LabelPrecisionDual.Title="Precision: ";


_frameDescription.ComboValuePrecisionMain1.LongHelp="Sets the main value precision.";
_frameDescription.ComboValueFormatMain1.LongHelp="Sets the main  value format.";
_frameDescription.ComboValuePrecisionMain2.LongHelp="Sets the main value secondary precision.";
_frameDescription.ComboValueFormatMain2.LongHelp="Sets the main value secondary format.";
_frameDescription.ComboValuePrecisionDual1.LongHelp="Sets the dual value precision.";
_frameDescription.ComboValueFormatDual1.LongHelp="Sets the dual value format.";
_frameDescription.ComboValuePrecisionDual2.LongHelp="Sets the dual value secondary precision.";
_frameDescription.ComboValueFormatDual2.LongHelp="Sets the dual value secondary format.";

_frameDescription.ComboValuePrecisionMain1.ShortHelp="Insert main value precision";
_frameDescription.ComboValueFormatMain1.ShortHelp="Insert main value format";
_frameDescription.ComboValuePrecisionMain2.ShortHelp="Insert main value secondary precision";
_frameDescription.ComboValueFormatMain2.ShortHelp="Insert main value secondary format";
_frameDescription.ComboValuePrecisionDual1.ShortHelp="Insert dual value precision";
_frameDescription.ComboValueFormatDual1.ShortHelp="Insert dual value format";
_frameDescription.ComboValuePrecisionDual2.ShortHelp="Insert dual value secondary precision";
_frameDescription.ComboValueFormatDual2.ShortHelp="Insert dual value secondary format";

