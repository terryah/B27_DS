// COPYRIGHT DASSAULT SYSTEMES  1999
//=============================================================================
//
// CATStCmdBreak:  	Resource file for NLS purpose related to
//                      Break surface/curve
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================

CATStCmdBreak.State1.Message="Select the cut elements";
CATStCmdBreak.State2.Message="Select the cutting elements";
CATStCmdBreak.State3.Message="Manipulation state";

CATStCmdBreak.Title = "Break";

CATStCmdBreak.UndoTitle = "Break";
CATStCmdBreak.RedoTitle = "Break";

BreakCurve.Message="Break Curve";
BreakSurface.Message="Break Surface";

NoResult.Message="No result was found in this configuration.
See the Report for break operations for more information.";
// A19 11:11:25
NoProjection.Message="Break creation failed.
Try to create break using Projection option.
See the Report of Break operations for more information.";

ProjectionFail.Message="Break creation failed due to failure in Projection of input element.
Try using another Projection option.
See the Report of Break operations for more information.";

NoExtrapolation.Message="Break creation failed.
Try to create break using Extrapolation option.
See the Report of Break operations for more information.";

ExtrapolationFail.Message="Break creation failed due to failure in Extrapolation of input element.
Try using another Extrapolation option.
See the Report of Break operations for more information.";

DisconnectResultFail.Message="Break creation failed for these inputs.
Try changing input for break.
See the Report for break operations for more information.";

RelimitationFail.Message="Break creation failed due to failure in Relimitation of input element.
Try to create break using another trim option.
See the Report of Break operations for more information.";
// A19 11:11:25

NoProjOrExtrapol.Message="Break creation failed.
Try to create break using Projection or Extrapolation Option.
See the Report of Break operations for more information."; // A19 12:07:17

ProjOrExtrapolFail.Message="Break creation failed due to failure in Projection or Extrapolation of input element. 
Try to create break using Projection or Extrapolation option.
See the Report of Break operations for more information."; // A19 12:07:17

FragmentError.Message = "Cannot fragment all the selected elements. 
Check whether they are either multi-cell or trimmed or non-NUPBS elements.";

BreakIntoeqNoError.Message = "Cannot break into equal parts all the selected elements. 
Check whether they are multi-cell elements.";

BreakMultiDomainError.Message = "Multi domain support selection is not allowed for quick isoparam curve creation. 
Please select a valid single domain support.";

BreakMaxDeviation.Message="Max Dev: ";
BreakEdgeDeviation.Message="Edge Dev: ";

//Start AV7 11:06:14
BreakAutoJoin.Assemble ="Assemble";
BreakAutoJoin.Propagate ="Propagate Isoparametric Curve";
//End AV7 11:06:14

//Start H67 11:12:02

CATStCmdBreak.State1SelectionAgent.UndoTitle ="Selection of Element(s) ";
CATStCmdBreak.State1SelectionAgent.RedoTitle ="Selection of Element(s) ";

CATStCmdBreak.State2SelectionAgent.UndoTitle ="Selection of Limitation Element(s)";
CATStCmdBreak.State2SelectionAgent.RedoTitle ="Selection of Limitation Element(s)";

CATStCmdBreak.ChangeBreakType.UndoTitle ="Change Break type";
CATStCmdBreak.ChangeBreakType.RedoTitle ="Change Break type";
 
CATStCmdBreak.ChangeBreakBothType.UndoTitle = "Break both ";
CATStCmdBreak.ChangeBreakBothType.RedoTitle = "Break both ";

CATStCmdBreak.ChangeDisassembleResultType.UndoTitle = "Disassemble"; 
CATStCmdBreak.ChangeDisassembleResultType.RedoTitle = "Disassemble"; 
  
CATStCmdBreak.ChangeFragmentType.UndoTitle ="Fragment "; 
CATStCmdBreak.ChangeFragmentType.RedoTitle ="Fragment "; 

CATStCmdBreak.ChangeInvertSelType.UndoTitle = "Invert selection";
CATStCmdBreak.ChangeInvertSelType.RedoTitle = "Invert selection";
  
CATStCmdBreak.ChangeBreakIntoEqNoType.UndoTitle ="Break into equal number "; 
CATStCmdBreak.ChangeBreakIntoEqNoType.RedoTitle ="Break into equal number "; 
  
CATStCmdBreak.ChangeBreakIntoEqNoValType.UndoTitle ="Modification of Break into equal number value";
CATStCmdBreak.ChangeBreakIntoEqNoValType.RedoTitle ="Modification of Break into equal number value";
  
CATStCmdBreak.ChangeDeviationModeType.UndoTitle ="Deviation mode "; 
CATStCmdBreak.ChangeDeviationModeType.RedoTitle ="Deviation mode "; 
  
CATStCmdBreak.ChangeProjection.UndoTitle ="Change Projection type ";
CATStCmdBreak.ChangeProjection.RedoTitle ="Change Projection type ";
  
CATStCmdBreak.ChangeExtrapolation.UndoTitle ="Change Extrapolation type ";
CATStCmdBreak.ChangeExtrapolation.RedoTitle ="Change Extrapolation type ";
  
CATStCmdBreak.ChangeRelimitation.UndoTitle ="Change Trim Type ";
CATStCmdBreak.ChangeRelimitation.RedoTitle ="Change Trim Type "; 

CATStCmdBreak.PointAcquisition.UndoTitle ="Creation of Isoparam curve";
CATStCmdBreak.PointAcquisition.RedoTitle ="Creation of Isoparam curve";
//End H67 11:12:02

//Start IF8 14:02:13
CATStCmdBreak.ManipulatorModified.UndoTitle ="Manipulator Type Modified";
CATStCmdBreak.ManipulatorModified.RedoTitle ="Manipulator Type Modified";
//End IF8 14:02:13

CATStCmdBreak.ChangeSelection.UndoTitle ="Switch input selection";
CATStCmdBreak.ChangeSelection.RedoTitle ="Switch input selection";

BreakError.Connexity ="Connexity Error";
BreakError.Overlap ="Overlap Error";

//Start SDE2 14:09:29
CATStCmdBreak.ArrowManipulatorChange.UndoTitle ="Arrow Manipulator Modified";
CATStCmdBreak.ArrowManipulatorChange.RedoTitle ="Arrow Manipulator Modified";
//End SDE2 14:09:26


