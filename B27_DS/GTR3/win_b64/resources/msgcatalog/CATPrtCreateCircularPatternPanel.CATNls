LongHelp = "Defines the circular pattern.";
 
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2n.Title = "Circle(s) :";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1n.Title= "Instance(s) :";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2s.Title= "Circle  spacing :"; 
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1s.Title= "Angular spacing :";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2l.Title= "Crown thickness :";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1l.Title= "Total angle :";
FrmMore.PositionningDefinition.CkeLabelV.Title = "Row in radial direction :";
FrmMore.PositionningDefinition.CkeLabelU.Title = "Row in angular direction :";
FrmMore.PositionningDefinition.CkeLabelXY.Title= "Rotation angle :";
  
InstanceAndAngularSpacing      = "Instance(s) & angular spacing";
AngularSpacingAndTotalAngle    = "Angular spacing & total angle";
CompleteCrown                  = "Complete crown"; 
UnequalAngularSpacing  = "Instance(s) & unequal angular spacing";
 
CircleAndCrownThickness        = "Circle(s) & crown thickness";
CircleAndCircleSpacing         = "Circle(s) & circle spacing";
CircleSpacingAndCrownThickness = "Circle spacing & crown thickness";
InstanceAndTotalAngle          = "Instance(s) & total angle";
 
CircularPatternPanelTitle  = "Circular Pattern Definition";

FrmRoot.FrmMain.EulerianPattern.AxialReference.Title = "Axial Reference";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.FirstDrivingType.Title = "Parameters:";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.FirstDrivingType.LongHelp =
"Specifies the parameter required to define
the pattern : Instance(s) and total angle or 
Instance(s) and angular spacing or Angular 
spacing and total angle or Complete crown or
Instance(s) and unequal angular spacing.";

FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.FirstDrivingby.LongHelp =
"Specifies the parameter required to define
the pattern : Instance(s) and total angle or 
Instance(s) and angular spacing or Angular 
spacing and total angle or Complete crown or
Instance(s) and unequal angular spacing.";

FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.Title = "Reference Direction";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.LongHelp =
"Reference Direction\nDefines the pattern axis.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.FraRefDir.FraEdt.Lab.Title = "Reference element:";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.FraRefDir.LongHelp = 
"Specifies the face, cylindrical face, 
edge, axis, line, plane or point for 
defining pattern direction.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.FraRefDir.FraEdt.Lab.LongHelp = 
"Specifies the face, cylindrical face, 
edge, axis, line, plane or point for 
defining pattern direction.";

FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.Reverse.Title = "Reverse";
FrmRoot.FrmMain.EulerianPattern.AxialReference.SecondFrame.Reverse.LongHelp =
"Reverses the pattern axis.";
  
FrmRoot.FrmMain.EulerianPattern.Crown.Title = "Crown Definition";
  
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.SecondDrivingType.Title = "Parameters:";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.SecondDrivingType.LongHelp = 
"Specifies the parameter required to define the pattern : 
Circle(s) and crown thickness or Circle(s) and circle spacing 
or Circle spacing and crown thickness";

FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.SecondDrivingBy.LongHelp = 
"Specifies the parameter required to define the pattern : 
Circle(s) and crown thickness or Circle(s) and circle spacing 
or Circle spacing and crown thickness";

FrmMore.PositionningDefinition.Title = "Position of Object in Pattern";
FrmMore.PositionningDefinition.LongHelp =
"Defines the original object position.";

FrmMore.SimpleGeomDefinition.Title = "Pattern Representation";
FrmMore.SimpleGeomDefinition.LongHelp =
"Defines the representation mode to keep part geometry simple for the purpose of good performance."; 

FrmMore.SimpleGeomDefinition.CheckSimpleGeom.Title = "Simplified representation";
FrmMore.SimpleGeomDefinition.CheckSimpleGeom.LongHelp =
"If activated, only the first four and the last four instances in each direction 
of patterning will be computed and displayed.";


FrmMore.RotationInstanceDefinition.Title = "Rotation of Instance(s)";
FrmMore.RotationInstanceDefinition.LongHelp =
"Rotation of Instances\nDefines the orientation of each instance.";

FrmMore.RotationInstanceDefinition.RotationStatus.Title = "Radial alignment of instance(s) ";
FrmMore.RotationInstanceDefinition.RotationStatus.LongHelp =
"Checking this option makes all instances remain		
normal to lines tangent to circles. 
Unchecking this option makes all instances keep			
the same orientation as the original object's.";		// @validatedUsage OZL 2013/11/19 option


FrmMore.StaggeredPattern.Title = "Staggered Pattern Definition";
FrmMore.StaggeredPattern.LongHelp =
"Defines the inputs for Staggered Pattern.";						// @validatedUsage OZL 2013/10/30 tag
FrmMore.StaggeredPattern.CkeLabelSS.Title = "Stagger Angle :";		// @validatedUsage OZL 2013/10/30 option
FrmMore.StaggeredPattern.CkeLabelSS.LongHelp =
"Defines value of Stagger Angle. This value should be less than angle between instances in axial direction.";	// @validatedUsage OZL 2013/10/30 tag
FrmMore.StaggeredPattern.StaggeredPattern.Title = "Staggered";		// @validatedUsage OZL 2013/10/30 tag
FrmMore.StaggeredPattern.StaggeredPattern.LongHelp =
"Activate this option to create instances of alternate crown-circles of circular pattern Staggered by certain angle in axial direction.";	// @validatedUsage OZL 2013/10/30 option tag
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1n.LongHelp =
"Specifies the number of instances for
the angular direction. This number includes
the original object.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.Instance.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the number of instances for
the angular direction. This number includes
the original object.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2n.LongHelp =
"Specifies the number of instances for
the radial direction. This number includes
the original object.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.Circle.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the number of instances for
the radial direction. This number includes
the original object.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1s.LongHelp =
"Specifies the angular spacing between two instances.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.AngularSpacing.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the angular spacing between two instances.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2s.LongHelp =
"Specifies the radial spacing between two instances.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CircleSpacing.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the radial spacing between two instances.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.CkeLabel1l.LongHelp = 
"Specifies the total angle to be used to place
all instances for the angular direction.
Total angle is the sum of angular spacing between individual instances,\nstarting from first instance upto the last instance.";
FrmRoot.FrmMain.EulerianPattern.AxialReference.FirstFrame.TotalAngle.EnglobingFrame.IntermediateFrame.LongHelp = 
"Specifies the total angle to be used to place
all instances for the angular direction.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CkeLabel2l.LongHelp =
"Specifies the total length to be used to place
all instances for the radial direction.";
FrmRoot.FrmMain.EulerianPattern.Crown.CrownFrame.CrownThickness.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the total length to be used to place
all instances for the radial direction.";
FrmMore.PositionningDefinition.CkeLabelU.LongHelp =
"Specifies the original object position in the
angular direction.";
FrmMore.PositionningDefinition.RowinAngularDirection.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the original object position in the
angular direction.";
FrmMore.PositionningDefinition.CkeLabelV.LongHelp =
"Specifies the original object position in the
radial direction.";
FrmMore.PositionningDefinition.RowinRadialDirection.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the original object position in the
radial direction.";
FrmMore.PositionningDefinition.CkeLabelXY.LongHelp =
"Specifies the original object orientation
according to the pattern directions.";  
FrmMore.PositionningDefinition.RotationAngle.EnglobingFrame.IntermediateFrame.LongHelp =
"Specifies the original object orientation
according to the pattern directions.";  

FrmMore.StaggeredPattern.CheckHalfStagStep.Title = "Set Half Of Spacing";
FrmMore.StaggeredPattern.CheckHalfStagStep.LongHelp =
"If activated, this will set the stagger angle equal to half the angular spacing.";		// @validatedUsage OZL 2013/10/30 tag

