// NLS Resource
//-------------------------
// CATVisuCstSettingFrame
//-------------------------
// English Version
//-------------------------
// tbu, 11 juin 1998, pii

Title = "Constraints and Dimensions";

// ==============================================================================================
// Frame de Constraint Style
// ==============================================================================================

General.HeaderFrame.Global.Title = "Constraint Style"; // Titre

General.Help = "Defines the constraint Style"; // apparait en bas a gauche de l'ecran
General.ShortHelp = "Constraint Style"; // info bulle
General.LongHelp = "Constraint Style\nDefines the constraint Style"; // apparait quand clic ? + sur le label ou le combo ou autre



// ==============================================================================================
// Visualisation des couleurs de status ou non
// ==============================================================================================

General.IconAndOptionsFrame.OptionsFrame.Show_Color_Status.Title = "Status Color Visualization";

General.IconAndOptionsFrame.OptionsFrame.Show_Color_Status.Help = "Display the status color of the constraint";
General.IconAndOptionsFrame.OptionsFrame.Show_Color_Status.ShortHelp="Display the status color of the constraint";
General.IconAndOptionsFrame.OptionsFrame.Show_Color_Status.LongHelp="Status Color Visualization\nDisplay the status color of the constraint";


// ==============================================================================================
// Angle
// ==============================================================================================

General.IconAndOptionsFrame.OptionsFrame.Angle.Title = "View angle";

General.IconAndOptionsFrame.OptionsFrame.Angle.Help = "Defines the angle between the plane containing a constraint and the normal to the visualization plane";
General.IconAndOptionsFrame.OptionsFrame.Angle.ShortHelp="Angle between the plane containing the constraint and the normal of the visualization plane.";
General.IconAndOptionsFrame.OptionsFrame.Angle.LongHelp = "View angle\nDefines the angle between the plane containing a constraint\nand the normal to the visualization plane from which the\nconstraint is displayed";

General.IconAndOptionsFrame.OptionsFrame.Angleeditor.Help = "Defines the angle between the plane containing a constraint and the normal to the visualization plane";
General.IconAndOptionsFrame.OptionsFrame.Angleeditor.ShortHelp="Angle between the plane containing the constraint and the normal of the visualization plane.";
General.IconAndOptionsFrame.OptionsFrame.Angleeditor.LongHelp = "View angle\nDefines the angle between the plane containing a constraint\nand the normal to the visualization plane from which the\nconstraint is displayed";


// ==============================================================================================
// Status_ok
// ==============================================================================================

General.IconAndOptionsFrame.OptionsFrame.Status_ok.Title = "Resolved constraint";

General.IconAndOptionsFrame.OptionsFrame.Status_ok.Help = "Defines the resolved constraint color";
General.IconAndOptionsFrame.OptionsFrame.Status_ok.ShortHelp = "Resolved constraint";
General.IconAndOptionsFrame.OptionsFrame.Status_ok.LongHelp = "Resolved constraint\nDefines the resolved constraint color";

General.IconAndOptionsFrame.OptionsFrame.Status_ok_combo.Help = "Defines the resolved constraint color";
General.IconAndOptionsFrame.OptionsFrame.Status_ok_combo.ShortHelp = "Resolved constraint";
General.IconAndOptionsFrame.OptionsFrame.Status_ok_combo.LongHelp = "Resolved constraint\nDefines the resolved constraint color";


// ==============================================================================================
// Status_ko
// ==============================================================================================

General.IconAndOptionsFrame.OptionsFrame.Status_ko.Title = "Unresolved constraint";

General.IconAndOptionsFrame.OptionsFrame.Status_ko.Help = "Defines the unresolved constraint color";
General.IconAndOptionsFrame.OptionsFrame.Status_ko.ShortHelp = "Unresolved constraint";
General.IconAndOptionsFrame.OptionsFrame.Status_ko.LongHelp = "Unresolved constraint\nDefines the unresolved constraint color";

General.IconAndOptionsFrame.OptionsFrame.Status_ko_combo.Help = "Defines the unresolved constraint color";
General.IconAndOptionsFrame.OptionsFrame.Status_ko_combo.ShortHelp = "Unresolved constraint";
General.IconAndOptionsFrame.OptionsFrame.Status_ko_combo.LongHelp = "Unresolved constraint\nDefines the unresolved constraint color";


// ==============================================================================================
// Status_overcst
// ==============================================================================================

General.IconAndOptionsFrame.OptionsFrame.Status_overcst.Title = "Overconstrained ";

General.IconAndOptionsFrame.OptionsFrame.Status_overcst.Help = "Defines the overconstrained color";
General.IconAndOptionsFrame.OptionsFrame.Status_overcst.ShortHelp = "Overconstrained";
General.IconAndOptionsFrame.OptionsFrame.Status_overcst.LongHelp = "Overconstrained\nDefines the overconstrained color";

General.IconAndOptionsFrame.OptionsFrame.Status_overcst_combo.Help = "Defines the overconstrained color";
General.IconAndOptionsFrame.OptionsFrame.Status_overcst_combo.ShortHelp = "Overconstrained";
General.IconAndOptionsFrame.OptionsFrame.Status_overcst_combo.LongHelp = "Overconstrained\nDefines the overconstrained color";


// ==============================================================================================
// Status_geoinv
// ==============================================================================================

General.IconAndOptionsFrame.OptionsFrame.Status_geoinv.Title = "Invalid geometry ";

General.IconAndOptionsFrame.OptionsFrame.Status_geoinv.Help = "Defines the invalid geometry color";
General.IconAndOptionsFrame.OptionsFrame.Status_geoinv.ShortHelp = "Invalid geometry";
General.IconAndOptionsFrame.OptionsFrame.Status_geoinv.LongHelp = "Invalid geometry\nDefines the invalid geometry color";

General.IconAndOptionsFrame.OptionsFrame.Status_geoinv_combo.Help = "Defines the invalid geometry color";
General.IconAndOptionsFrame.OptionsFrame.Status_geoinv_combo.ShortHelp = "Invalid geometry";
General.IconAndOptionsFrame.OptionsFrame.Status_geoinv_combo.LongHelp = "Invalid geometry\nDefines the invalid geometry color";


// ==============================================================================================
// Frame de Dimension Style
// ==============================================================================================

Dimension.HeaderFrame.Global.Title = "Dimension Style";

Dimension.Help = "Defines the dimension style";
Dimension.ShortHelp = "Dimension Style";
Dimension.LongHelp = "Dimension Style\nDefines the dimension style";


// ==============================================================================================
// Scale
// ==============================================================================================

Dimension.IconAndOptionsFrame.OptionsFrame.Scale.Title = "Scale";

Dimension.IconAndOptionsFrame.OptionsFrame.Scale.Help= "Defines size of non zoomable items";
Dimension.IconAndOptionsFrame.OptionsFrame.Scale.ShortHelp = "Modifies the size of non zoomables items";
Dimension.IconAndOptionsFrame.OptionsFrame.Scale.LongHelp = "Scale\nDefines size of non zoomable items";

Dimension.IconAndOptionsFrame.OptionsFrame.Scale_combo.item_0 = "Small";
Dimension.IconAndOptionsFrame.OptionsFrame.Scale_combo.item_1 = "Medium";
Dimension.IconAndOptionsFrame.OptionsFrame.Scale_combo.item_2 = "Large";

Dimension.IconAndOptionsFrame.OptionsFrame.Scale_combo.Help = "Defines size of non zoomable items";
Dimension.IconAndOptionsFrame.OptionsFrame.Scale_combo.ShortHelp = "Modifies the size of non zoomables items";
Dimension.IconAndOptionsFrame.OptionsFrame.Scale_combo.LongHelp = "Scale\nDefines size of non zoomable items";


// ==============================================================================================
// Gap
// ==============================================================================================

Dimension.IconAndOptionsFrame.OptionsFrame.Gap.Title = "Gap";

Dimension.IconAndOptionsFrame.OptionsFrame.Gap.Help= "Defines the extension lines gap";
Dimension.IconAndOptionsFrame.OptionsFrame.Gap.ShortHelp = "Gap between the extension lines of the constraint and the constrained element";
Dimension.IconAndOptionsFrame.OptionsFrame.Gap.LongHelp = "Gap\nDefines the extension lines gap";

Dimension.IconAndOptionsFrame.OptionsFrame.Gapeditor.Help = "Defines the extension lines gap";
Dimension.IconAndOptionsFrame.OptionsFrame.Gapeditor.ShortHelp = "Gap between the extension lines of the constraint and the constrained element";
Dimension.IconAndOptionsFrame.OptionsFrame.Gapeditor.LongHelp = "Gap\nDefines the extension lines gap";


// ==============================================================================================
// Overrun
// ==============================================================================================

Dimension.IconAndOptionsFrame.OptionsFrame.Overrun.Title= "Overrun";

Dimension.IconAndOptionsFrame.OptionsFrame.Overrun.Help= "Defines the extension lines overrun";
Dimension.IconAndOptionsFrame.OptionsFrame.Overrun.ShortHelp = "Gap between the extension lines of the constraint and its extremities";
Dimension.IconAndOptionsFrame.OptionsFrame.Overrun.LongHelp = "Overrun\nDefines the extension lines overrun";

Dimension.IconAndOptionsFrame.OptionsFrame.Overruneditor.Help = "Defines the extension lines overrun";
Dimension.IconAndOptionsFrame.OptionsFrame.Overruneditor.ShortHelp = "Gap between the extension lines of the constraint and its extremities";
Dimension.IconAndOptionsFrame.OptionsFrame.Overruneditor.LongHelp = "Overrun\nDefines the extension lines overrun";


// ==============================================================================================
// Other Style
// ==============================================================================================

Dimension.IconAndOptionsFrame.OptionsFrame.Reference_ratio.Title = "Anchor position";

Dimension.IconAndOptionsFrame.OptionsFrame.Reference_ratio.Help = "Defines the position of anchor symbol";
Dimension.IconAndOptionsFrame.OptionsFrame.Reference_ratio.ShortHelp = "Anchor position";
Dimension.IconAndOptionsFrame.OptionsFrame.Reference_ratio.LongHelp = "Anchor position\nDefines the position of anchor symbol";

Dimension.IconAndOptionsFrame.OptionsFrame.Reference_ratioeditor.Help = "Defines the position of anchor symbol";
Dimension.IconAndOptionsFrame.OptionsFrame.Reference_ratioeditor.ShortHelp = "Anchor position";
Dimension.IconAndOptionsFrame.OptionsFrame.Reference_ratioeditor.LongHelp = "Anchor position\nDefines the position of anchor symbol";


// ==============================================================================================
// Show_iconic_line
// ==============================================================================================

Dimension.IconAndOptionsFrame.OptionsFrame.Show_iconic_line.Title = "Displays iconified constraint elements";

Dimension.IconAndOptionsFrame.OptionsFrame.Show_iconic_line.Help = "Displays iconified constraint elements in the geometry or not";
Dimension.IconAndOptionsFrame.OptionsFrame.Show_iconic_line.ShortHelp = "Displays iconified constraint elements";
Dimension.IconAndOptionsFrame.OptionsFrame.Show_iconic_line.LongHelp = "Displays iconified constraint elements\nDisplays iconified constraint elements in the geometry or not";


// ==============================================================================================
// Hilite_pointed_elements
// ==============================================================================================

Dimension.IconAndOptionsFrame.OptionsFrame.Hilite_pointed_elements.Title = "Highlight constrained elements";

Dimension.IconAndOptionsFrame.OptionsFrame.Hilite_pointed_elements.Help = "Highlights the constrained element(s)";
Dimension.IconAndOptionsFrame.OptionsFrame.Hilite_pointed_elements.ShortHelp = "Highlight constrained elements";
Dimension.IconAndOptionsFrame.OptionsFrame.Hilite_pointed_elements.LongHelp = "Highlight constrained elements\nHighlights the constrained element(s)";


// ==============================================================================================
// Frame de Preview
// ==============================================================================================

PreView.HeaderFrame.Global.Title = "Preview";

PreView.Help = "Previews the defined styles";
PreView.ShortHelp = "Preview";
PreView.LongHelp = "Preview\nPreviews the defined styles";

PreView.IconAndOptionsFrame.OptionsFrame.View.Title ="Preview";

PreView.IconAndOptionsFrame.OptionsFrame.View.Help = "Previews the defined styles";
PreView.IconAndOptionsFrame.OptionsFrame.View.LongHelp = "Preview\nPreviews the defined styles";


// ===================================================================================
// Frame de Display
// ===================================================================================

Constraint_Display.HeaderFrame.Global.Title = "Constraint Display";

Constraint_Display.Help = "Defines the display of constraints";
Constraint_Display.ShortHelp = "Display of constraints";
Constraint_Display.LongHelp = "Display of constraints\nDefines the display of constraints";


// ===================================================================================
// Mode de Display
// ===================================================================================

Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode.Title = "Display Mode  ";

Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode.Help = "Defines the display mode of the constraint at its creation";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode.ShortHelp = "Display Mode at creation";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode.LongHelp = "Display Mode\nDefines the display mode of the constraint at its creation";

Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode_combo.item_0 = "Value";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode_combo.item_1 = "Name";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode_combo.item_2 = "Name+Value";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode_combo.item_3 = "Name+Value+Formula";

Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode_combo.Help = "Defines the display mode of the constraint at its creation";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode_combo.ShortHelp = "Display Mode at creation";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Mode_combo.LongHelp = "Display Mode\nDefines the display mode of the constraint at its creation";

Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Param_Mode.Title = "Display Mode available as Knowledge parameter";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Param_Mode.Help = "Makes available the Display Mode for Sketcher dimensional constraints as Knowledge parameter";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Param_Mode.ShortHelp = "Display Mode as Knowledge parameter";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Display_Param_Mode.LongHelp = "Display Mode as Knowledge parameter\nMakes available the Display Mode for Sketcher dimensional contsraints as Knowledge parameter.";

// ==============================================================================================
// Filter
// ==============================================================================================

Constraint_Display.IconAndOptionsFrame.OptionsFrame.Filter.Title = "Filter...";

Constraint_Display.IconAndOptionsFrame.OptionsFrame.Filter.Help = "Defines constraint filters";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Filter.ShortHelp="Constraint Filter";
Constraint_Display.IconAndOptionsFrame.OptionsFrame.Filter.LongHelp="Constraint Filter\nDefines constraint filters";
