//
// ---------------------------
// Maillauto global parameters dialog
// ---------------------------
//

Title="Global Parameters";
Help="Set Global parameters";
ShortHelp="Set global parameters";
LongHelp="Set Mesh Part global parameters";

TriaFrame.TriaTabContainer.GeomTabTria.Title="Geometry";
TriaFrame.TriaTabContainer.MeshTabTria.Title="Mesh";

QuadFrame.QuadTabContainer.GeomTabQuad.Title="Geometry";
QuadFrame.QuadTabContainer.MeshTabQuad.Title="Mesh";

MethodFrame.MethodLabel.Title = "Mesh Type  ";
MethodFrame.MethodLabel.LongHelp = "Set type for surface mesh.";

MethodFrame.Button1.Title = "Frontal triangle";
MethodFrame.Button1.Help = "Set frontal triangle method";
MethodFrame.Button1.ShortHelp = "Set frontal triangle method";
MethodFrame.Button1.LongHelp = "Set frontal triangle method.";

MethodFrame.Button2.Title = "Frontal quadrangle";
MethodFrame.Button2.Help = "Set frontal quadrangle method";
MethodFrame.Button2.ShortHelp = "Set frontal quadrangle method";
MethodFrame.Button2.LongHelp = "Set frontal quadrangle method.";

elementFrame.elementLabel.Title = "Element type  ";
elementFrame.elementLabel.LongHelp = "Set element type for surface mesh.";

elementFrame.RBLinear.Title="Linear";
elementFrame.RBLinear.Help="Use linear element type";
elementFrame.RBLinear.ShortHelp="Use linear element type";
elementFrame.RBLinear.RBLinear.LongHelp="Use linear element type.";

elementFrame.RBParabolic.Title="Parabolic";
elementFrame.RBParabolic.Help="Use parabolic element type";
elementFrame.RBParabolic.ShortHelp="Use parabolic element type";
elementFrame.RBParabolic.LongHelp="Use parabolic element type.";

GeometrySelDlg.Title="Geometry Selector";
CATMSHDlgMtoParams.GeometrySelDlg.Title="Geometry Selector";

TriaFrame.TriaTabContainer.MeshTabTria.MaxSizeLbl.Title="Mesh size";
TriaFrame.TriaTabContainer.MeshTabTria.MaxSizeLbl.LongHelp="Set mesh part global mesh size.";

TriaFrame.TriaTabContainer.MeshTabTria.OffsetLbl.Title="Offset ";
TriaFrame.TriaTabContainer.MeshTabTria.OffsetLbl.LongHelp="Set mesh part global offset.";

TriaFrame.TriaTabContainer.GeomTabTria.CleanSizeLbl.Title="Min holes size ";
TriaFrame.TriaTabContainer.GeomTabTria.CleanSizeLbl.LongHelp="Set minimum holes size to be taken into account.";

TriaFrame.TriaTabContainer.GeomTabTria.MinSizeLbl.Title="Min size   ";
TriaFrame.TriaTabContainer.GeomTabTria.MinSizeLbl.LongHelp="Set minimum elements size.";

TriaFrame.TriaTabContainer.GeomTabTria.SagLbl.Title="Constraint Sag";
TriaFrame.TriaTabContainer.GeomTabTria.SagLbl.LongHelp="Set mesh part constraint sag.";

TriaFrame.TriaTabContainer.GeomTabTria.TopologySizeLbl.Title="Constraint ref size";
TriaFrame.TriaTabContainer.GeomTabTria.TopologySizeLbl.LongHelp="Set constraint reference size.";

TriaFrame.TriaTabContainer.GeomTabTria.ModifyTopologySize.Title="Constraint ref size independant from mesh size";
TriaFrame.TriaTabContainer.GeomTabTria.ModifyTopologySize.LongHelp="Constraint ref size independant from mesh size";

TriaFrame.TriaTabContainer.MeshTabTria.MeshSagValueLbl.Title="Absolute sag";

TriaFrame.TriaTabContainer.MeshTabTria.MeshAbsSag.Title="Absolute sag";
TriaFrame.TriaTabContainer.MeshTabTria.MeshAbsSag.LongHelp="If checked, absolute sag will be taken into account.";

TriaFrame.TriaTabContainer.MeshTabTria.MeshAbsSagValueLbl.Title="Absolute Sag Value";

TriaFrame.TriaTabContainer.MeshTabTria.MeshRelSag.Title="Relative  sag";
TriaFrame.TriaTabContainer.MeshTabTria.MeshRelSag.LongHelp="If checked, relative sag will be taken into account.";

TriaFrame.TriaTabContainer.MeshTabTria.MeshRelSagValueLbl.Title="Relative Sag Value";

TriaFrame.TriaTabContainer.MeshTabTria.SizeProgression.Title="Size progression";
TriaFrame.TriaTabContainer.MeshTabTria.SizeProgression.LongHelp="If checked, Size progression will be taken into account.";

TriaFrame.TriaTabContainer.GeomTabTria.TolCaptureLbl.Title="Tolerance";
TriaFrame.TriaTabContainer.GeomTabTria.TolCaptureLbl.LongHelp="Set tolerance for automatic curve capture.";

TriaFrame.TriaTabContainer.MeshTabTria.MeshCaptureToleranceLbl.Title="Tolerance";
TriaFrame.TriaTabContainer.MeshTabTria.MeshCaptureToleranceLbl.LongHelp="Set tolerance for automatic mesh capture.";

TriaFrame.TriaTabContainer.MeshTabTria.CaptureFilter.Title="Filter";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureFilter.ShortHelp="Condensation Filter";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureFilter.LongHelp="Allows to select meshes to capture.";

TriaFrame.TriaTabContainer.MeshTabTria.MinSizeForSagLbl.Title="Min Size";
TriaFrame.TriaTabContainer.MeshTabTria.MinSizeForSagLbl.LongHelp="Set minimum elements size for mesh with sag.";

TriaFrame.TriaTabContainer.MeshTabTria.CaptureAuto.Title="Automatic mesh capture";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureAuto.Help="Activate/unactivate automatic mesh capture";
TriaFrame.TriaTabContainer.MeshTabTria.CaptureAuto.LongHelp="Activate/unactivate automatic mesh capture.";

TriaFrame.TriaTabContainer.GeomTabTria.CurveAuto.Title="Automatic curve capture";
TriaFrame.TriaTabContainer.GeomTabTria.CurveAuto.Help="Activate/unactivate automatic curve capture";
TriaFrame.TriaTabContainer.GeomTabTria.CurveAuto.LongHelp="Activate/unactivate automatic curve capture.";

TriaFrame.TriaTabContainer.GeomTabTria.DetailSimplification.Title="Merge during simplification";
TriaFrame.TriaTabContainer.GeomTabTria.DetailSimplification.Help="Activate/unactivate merge during simplification";
TriaFrame.TriaTabContainer.GeomTabTria.DetailSimplification.LongHelp="Activate/unactivate merge during simplification.";

TriaFrame.TriaTabContainer.MeshTabTria.StripOptimization.Title="Strip optimization";
TriaFrame.TriaTabContainer.MeshTabTria.StripOptimization.Help="Activate/unactivate node position optimisation along strips";
TriaFrame.TriaTabContainer.MeshTabTria.StripOptimization.LongHelp="Activate/unactivate node position optimisation along strips.";

TriaFrame.TriaTabContainer.GeomTabTria.FaceAngleLabel.Title="Angle between faces";
TriaFrame.TriaTabContainer.GeomTabTria.FaceAngleLabel.LongHelp="Set angle between faces.";

TriaFrame.TriaTabContainer.GeomTabTria.CurveAngleLabel.Title="Angle between curves";
TriaFrame.TriaTabContainer.GeomTabTria.CurveAngleLabel.LongHelp="Set angle between curves.";

QuadFrame.QuadTabContainer.MeshTabQuad.MaxSize2Lbl.Title="Mesh size";
QuadFrame.QuadTabContainer.MeshTabQuad.MaxSize2Lbl.LongHelp="Set mesh part global mesh size.";

QuadFrame.QuadTabContainer.MeshTabQuad.Offset2Lbl.Title="Offset ";
QuadFrame.QuadTabContainer.MeshTabQuad.Offset2Lbl.LongHelp="Set mesh part global offset.";

QuadFrame.QuadTabContainer.GeomTabQuad.CleanSize2Lbl.Title="Min holes size ";
QuadFrame.QuadTabContainer.GeomTabQuad.CleanSize2Lbl.LongHelp="Set minimum holes size to be taken into account.";

QuadFrame.QuadTabContainer.GeomTabQuad.MinSize2Lbl.Title="Min size   ";
QuadFrame.QuadTabContainer.GeomTabQuad.MinSize2Lbl.LongHelp="Set minimum elements size.";

QuadFrame.QuadTabContainer.GeomTabQuad.Sag2Lbl.Title="Constraint sag ";
QuadFrame.QuadTabContainer.GeomTabQuad.Sag2Lbl.LongHelp="Set mesh part constraint sag";

QuadFrame.QuadTabContainer.GeomTabQuad.TopologySize2Lbl.Title="Constraint ref size ";
QuadFrame.QuadTabContainer.GeomTabQuad.TopologySize2Lbl.LongHelp="Set constraint reference size.";

QuadFrame.QuadTabContainer.GeomTabQuad.ModifyTopologySize2.Title="Constraint ref size independant from mesh size";
QuadFrame.QuadTabContainer.GeomTabQuad.ModifyTopologySize2.LongHelp="Constraint ref size independant from mesh size";

QuadFrame.QuadTabContainer.MeshTabQuad.QuadsOnly.Title="Quads only";
QuadFrame.QuadTabContainer.MeshTabQuad.QuadsOnly.LongHelp="If checked and if possible, only quadrangles will be created.";

QuadFrame.QuadTabContainer.MeshTabQuad.MinimizeTriangles.Title="Minimize triangles";
QuadFrame.QuadTabContainer.MeshTabQuad.MinimizeTriangles.LongHelp="If checked, number of triangles will be minimized.";

QuadFrame.QuadTabContainer.MeshTabQuad.OptimizeRegularity.Title="Directional mesh";
QuadFrame.QuadTabContainer.MeshTabQuad.OptimizeRegularity.LongHelp="If checked and if possible, mesh will be directional.";

QuadFrame.QuadTabContainer.GeomTabQuad.TolCapture2Lbl.Title="Tolerance";
QuadFrame.QuadTabContainer.GeomTabQuad.TolCapture2Lbl.LongHelp="Set tolerance for automatic curve capture.";

QuadFrame.QuadTabContainer.MeshTabQuad.MeshCaptureTolerance2Lbl.Title="Tolerance";
QuadFrame.QuadTabContainer.MeshTabQuad.MeshCaptureTolerance2Lbl.LongHelp="Set tolerance for automatic mesh capture.";

QuadFrame.QuadTabContainer.MeshTabQuad.CaptureAuto2.Title="Automatic mesh capture";
QuadFrame.QuadTabContainer.MeshTabQuad.CaptureAuto2.Help="Activate/unactivate automatic mesh capture.";
QuadFrame.QuadTabContainer.MeshTabQuad.CaptureAuto2.LongHelp="Activate/unactivate automatic mesh capture";

QuadFrame.QuadTabContainer.GeomTabQuad.CurveAuto2.Title="Automatic curve capture";
QuadFrame.QuadTabContainer.GeomTabQuad.CurveAuto2.Help="Activate/unactivate automatic curve capture";
QuadFrame.QuadTabContainer.GeomTabQuad.CurveAuto2.LongHelp="Activate/unactivate automatic curve capture.";

QuadFrame.QuadTabContainer.GeomTabQuad.DetailSimplification2.Title="Merge during simplification";
QuadFrame.QuadTabContainer.GeomTabQuad.DetailSimplification2.Help="Activate/unactivate merge during simplification";
QuadFrame.QuadTabContainer.GeomTabQuad.DetailSimplification2.LongHelp="Activate/unactivate merge during simplification.";

QuadFrame.QuadTabContainer.MeshTabQuad.StripOptimization.Title="Strip optimization";
QuadFrame.QuadTabContainer.MeshTabQuad.StripOptimization.Help="Activate/unactivate node position optimisation along strips";
QuadFrame.QuadTabContainer.MeshTabQuad.StripOptimization.LongHelp="Activate/unactivate node position optimisation along strips.";

QuadFrame.QuadTabContainer.GeomTabQuad.FaceAngleLabel2.Title="Angle between faces";
QuadFrame.QuadTabContainer.GeomTabQuad.FaceAngleLabel2.LongHelp="Set angle between faces.";

QuadFrame.QuadTabContainer.GeomTabQuad.CurveAngleLabel2.Title="Angle between curves";
QuadFrame.QuadTabContainer.GeomTabQuad.CurveAngleLabel2.LongHelp="Set angle between curves.";

//
// Error Panel.
//

CATMSHCmdMtoParams.ErrorCoherence0.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence0.Text="Mesh Size should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence1.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence1.Text="Minimum Size should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence2.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence2.Text="Minimum Sag Size should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence3.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence3.Text="Constraint Sag should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence4.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence4.Text="Min Hole Size should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence5.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence5.Text="Mesh Capture Tolerance should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence6.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence6.Text="Curve Capture Tolerance should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence7.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence7.Text="Absolute Sag Size should be larger than 0.";

CATMSHCmdMtoParams.ErrorCoherence8.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence8.Text="Minimum Size should be larger.( MinSize > 0.001 * Mesh Size )";

CATMSHCmdMtoParams.ErrorCoherence9.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence9.Text="Mesh size should be larger than Minimum Size.";

CATMSHCmdMtoParams.ErrorCoherence10.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence10.Text="Sag should be smaller. ( Sag < 0.2* Mesh Size )";

CATMSHCmdMtoParams.ErrorCoherence11.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence11.Text="Sag should be larger. ( Sag > 0.05 * Mesh Size )";

CATMSHCmdMtoParams.ErrorCoherence12.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence12.Text="Offset should be smaller";

CATMSHCmdMtoParams.ErrorCoherence13.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence13.Text="Relative sag should be strictly positive.";

CATMSHCmdMtoParams.ErrorCoherence14.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence14.Text="Minimum size should be smaller than mesh size.";

CATMSHCmdMtoParams.ErrorCoherence15.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence15.Text="Sag should be smaller. ( Sag < 0.2* Constraint ref size )";

CATMSHCmdMtoParams.ErrorCoherence16.Title="Error";
CATMSHCmdMtoParams.ErrorCoherence16.Text="Sag should be larger. ( Sag > 0.05 * Constraint ref size )";

CATMSHCmdMtoParams.ParamsTopoChanged.Title="Warning";
CATMSHCmdMtoParams.ParamsTopoChanged.Text="These modifications invalidate the geometry simplification.";

CATMSHCmdMtoParams.ParamsMeshChanged.Title="Warning";
CATMSHCmdMtoParams.ParamsMeshChanged.Text="These modifications invalidate the mesh.";

//
// Ajout pour le mailleur associatif
//

TabContainer.GeomTab.Title="Geometry";
TabContainer.MeshTab.Title="Mesh";

TabContainer.MeshTab.MethodLabel.Title = "Shape: ";
TabContainer.MeshTab.MethodLabel.LongHelp = "Set shape for surface mesh elements.";

TabContainer.MeshTab.ShapeFrame.Button1.Title = "Frontal triangle";
TabContainer.MeshTab.ShapeFrame.Button1.Help = "Set frontal triangle method";
TabContainer.MeshTab.ShapeFrame.Button1.ShortHelp = "Set frontal triangle method";
TabContainer.MeshTab.ShapeFrame.Button1.LongHelp = "Set frontal triangle method.";

TabContainer.MeshTab.ShapeFrame.Button2.Title = "Frontal quadrangle";
TabContainer.MeshTab.ShapeFrame.Button2.Help = "Set frontal quadrangle method";
TabContainer.MeshTab.ShapeFrame.Button2.ShortHelp = "Set frontal quadrangle method";
TabContainer.MeshTab.ShapeFrame.Button2.LongHelp = "Set frontal quadrangle method.";

TabContainer.MeshTab.elementLabel.Title = "Type: ";
TabContainer.MeshTab.elementLabel.LongHelp = "Set element type for surface mesh.";

TabContainer.MeshTab.ElementTypeFrame.RBLinear.Title="Linear";
TabContainer.MeshTab.ElementTypeFrame.RBLinear.Help="Use linear element type";
TabContainer.MeshTab.ElementTypeFrame.RBLinear.ShortHelp="Use linear element type";
TabContainer.MeshTab.ElementTypeFrame.RBLinear.RBLinear.LongHelp="Use linear element type.";

TabContainer.MeshTab.ElementTypeFrame.RBParabolic.Title="Parabolic";
TabContainer.MeshTab.ElementTypeFrame.RBParabolic.Help="Use parabolic element type";
TabContainer.MeshTab.ElementTypeFrame.RBParabolic.ShortHelp="Use parabolic element type";
TabContainer.MeshTab.ElementTypeFrame.RBParabolic.LongHelp="Use parabolic element type.";

TabContainer.MeshTab.Frame.TriaFrame.MaxSizeLbl.Title="Mesh size";
TabContainer.MeshTab.Frame.TriaFrame.MaxSizeLbl.LongHelp="Set mesh part global mesh size.";

TabContainer.MeshTab.Frame.QuadFrame.MaxSizeLbl.Title="Mesh size";
TabContainer.MeshTab.Frame.QuadFrame.MaxSizeLbl.LongHelp="Set mesh part global mesh size.";

TabContainer.MeshTab.Frame.QuadFrame.QuadsOnly.Title="Quads only";
TabContainer.MeshTab.Frame.QuadFrame.QuadsOnly.LongHelp="If checked and possible, only quadrangles will be created.";

TabContainer.MeshTab.Frame.TriaFrame.MeshAbsSag.Title="Absolute sag";
TabContainer.MeshTab.Frame.TriaFrame.MeshAbsSag.LongHelp="If checked, absolute sag will be taken into account.";

TabContainer.MeshTab.Frame.TriaFrame.MeshAbsSagValueLbl.Title="Absolute Sag Value";

TabContainer.MeshTab.Frame.TriaFrame.MeshRelSag.Title="Relative  sag";
TabContainer.MeshTab.Frame.TriaFrame.MeshRelSag.LongHelp="If checked, relative sag will be taken into account.";

TabContainer.MeshTab.Frame.TriaFrame.MeshRelSagValueLbl.Title="Relative Sag Value";

TabContainer.MeshTab.Frame.TriaFrame.MinSizeForSagLbl.Title="Min size";
TabContainer.MeshTab.Frame.TriaFrame.MinSizeForSagLbl.LongHelp="Set minimum elements size for mesh with sag.";

TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.Title="Automatic mesh capture";
TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.Help="Activate/unactivate automatic mesh capture";
TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.LongHelp="Activate/unactivate automatic mesh capture.";

TabContainer.MeshTab.Frame.TriaFrame.CaptureAuto.Title="Automatic mesh capture";
TabContainer.MeshTab.Frame.TriaFrame.CaptureAuto.Help="Activate/unactivate automatic mesh capture";
TabContainer.MeshTab.Frame.QuadFrame.CaptureAuto.LongHelp="Activate/unactivate automatic mesh capture.";

TabContainer.MeshTab.Frame.QuadFrame.MeshCaptureToleranceLbl.Title="Tolerance";
TabContainer.MeshTab.Frame.QuadFrame.MeshCaptureToleranceLbl.LongHelp="Set tolerance for automatic mesh capture.";

TabContainer.MeshTab.Frame.TriaFrame.MeshCaptureToleranceLbl.Title="Tolerance";
TabContainer.MeshTab.Frame.TriaFrame.MeshCaptureToleranceLbl.LongHelp="Set tolerance for automatic mesh capture.";

TabContainer.MeshTab.CaptureFilter.Title="Filter";
TabContainer.MeshTab.CaptureFilter.ShortHelp="Condensation Filter";
TabContainer.MeshTab.CaptureFilter.LongHelp="Allows to select meshes to capture.";

TabContainer.MeshTab.Frame.QuadFrame.DefaultMesh.Title = "Default method";
TabContainer.MeshTab.Frame.QuadFrame.DefaultMesh.LongHelp = "Set type for default mesh.";

Mapped = "Mapped free mesh";
MinimalMesh = "Minimal mesh";

TabContainer.GeomTab.TopologySizeLbl.Title="Constraint ref size";
TabContainer.GeomTab.TopologySizeLbl.LongHelp="Set constraint ref size";

TabContainer.GeomTab.ModifyTopologySize.Title="Constraints independant from mesh size";
TabContainer.GeomTab.ModifyTopologySize.LongHelp="Constraints independant from mesh size";

TabContainer.GeomTab.TopologySagLbl.Title="Constraint sag";
TabContainer.GeomTab.TopologySagLbl.LongHelp="Set constraint sag for initialization.";

TabContainer.GeomTab.SharpEdges.Title="Add sharp edges";
TabContainer.GeomTab.SharpEdges.LongHelp="If checked, sharp edges will be taken into account.";

TabContainer.GeomTab.FaceAngleLbl.Title="Sharp edges angle";
TabContainer.GeomTab.FaceAngleLbl.LongHelp="Set sharp edges angle";

TabContainer.GeomTab.CleanSizeLbl.Title="Min holes size ";
TabContainer.GeomTab.CleanSizeLbl.LongHelp="Set minimum holes size to be taken into account.";

TabContainer.GeomTab.OffsetLbl.Title="Offset ";
TabContainer.GeomTab.OffsetLbl.LongHelp="Set mesh part global offset.";

TabContainer.GeomTab.CurveAuto.Title="Automatic curve capture";
TabContainer.GeomTab.CurveAuto.Help="Activate/unactivate automatic curve capture";
TabContainer.GeomTab.CurveAuto.LongHelp="Activate/unactivate automatic curve capture.";

TabContainer.GeomTab.TolCaptureLbl.Title="Tolerance";
TabContainer.GeomTab.TolCaptureLbl.LongHelp="Set tolerance for automatic curve capture.";
