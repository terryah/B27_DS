//-----------------------------------------------
// Resource file for CATGuiCurveSmoothPanel class
// En_US
//-----------------------------------------------

Title="Curve Smooth Definition";

CurveFrame.LabelCurve.Title="Curve to smooth: ";
CurveFrame.LabelCurve.LongHelp="Specifies the curve to be smoothed.";
CurveFrame.SelCurve.LongHelp="Specifies the curve to be smoothed.";
ElementsListTitle = "Elements";
ListeOnglets.OngletParam.Title="Parameters";

ListeOnglets.OngletParam.FrameParam.LabelLittTangence.Title="Tangency threshold: ";
ListeOnglets.OngletParam.FrameParam.LabelLittTangence.LongHelp="Specifies the tangency discontinuity under which the curve is smoothed.";
ListeOnglets.OngletParam.FrameParam.FraLittTangence.LongHelp="Specifies the tangency discontinuity under which the curve is smoothed.";
ListeOnglets.OngletParam.FrameParam.FraLittTangence.EnglobingFrame.IntermediateFrame.Spinner.Title="Tangency";

ListeOnglets.OngletParam.FrameParam.SelCourbure.Title="Curvature threshold: ";
ListeOnglets.OngletParam.FrameParam.SelCourbure.LongHelp="Specifies the curvature discontinuity above which the curve is smoothed.";
ListeOnglets.OngletParam.FrameParam.FraLittCourbure.LongHelp="Specifies the curvature discontinuity above which the curve is smoothed.";
ListeOnglets.OngletParam.FrameParam.FraLittCourbure.EnglobingFrame.IntermediateFrame.Spinner.Title="Curvature";

ListeOnglets.OngletParam.FrameParam.SelDeviation.Title="Maximum deviation: ";
ListeOnglets.OngletParam.FrameParam.SelDeviation.LongHelp="Specifies the radius of the pipe in which the result has to be contained.";
ListeOnglets.OngletParam.FrameParam.FraLittDeviation.LongHelp="Specifies the radius of the pipe in which the result has to be contained.";
ListeOnglets.OngletParam.FrameParam.FraLittDeviation.EnglobingFrame.IntermediateFrame.Spinner.Title="Deviation";

ListeOnglets.OngletParam.FrameContinuity.LabelContinuity.Title="Continuity: ";
ListeOnglets.OngletParam.FrameContinuity.LabelContinuity.LongHelp="";
ListeOnglets.OngletParam.FrameContinuity.ThresholdRB.Title="Threshold";
ListeOnglets.OngletParam.FrameContinuity.ThresholdRB.LongHelp="";
ListeOnglets.OngletParam.FrameContinuity.PointRB.Title="Point";
ListeOnglets.OngletParam.FrameContinuity.PointRB.LongHelp="";
ListeOnglets.OngletParam.FrameContinuity.TangentRB.Title="Tangent";
ListeOnglets.OngletParam.FrameContinuity.TangentRB.LongHelp="";
ListeOnglets.OngletParam.FrameContinuity.CurvatureRB.Title="Curvature";
ListeOnglets.OngletParam.FrameContinuity.CurvatureRB.LongHelp="";


ListeOnglets.OngletFrozen.Title="Freeze";
ListeOnglets.OngletFrozen.FraList.LongHelp="Specifies a list of sub-elements (vertex or edge) of the curve to be smoothed
which must remain unchanged during smoothing operation.";


ListeOnglets.OngletExtrem.Title="Extremities";
ListeOnglets.OngletExtrem.StartExtremLabel.Title="Start: ";
ListeOnglets.OngletExtrem.StartExtremLabel.LongHelp="Allows to specify start extremity continuity condition:
same curvature, tangency or point as the input curve.";
ListeOnglets.OngletExtrem.StartExtremCombo.LongHelp="Allows to specify start extremity continuity condition:
same curvature, tangency or point as the input curve.";
ListeOnglets.OngletExtrem.EndExtremLabel.Title="End: ";
ListeOnglets.OngletExtrem.EndExtremLabel.LongHelp="Allows to specify end extremity continuity condition:
same curvature, tangency or point as the input curve.";
ListeOnglets.OngletExtrem.EndExtremCombo.LongHelp="Allows to specify end extremity continuity condition:
same curvature, tangency or point as the input curve.";
CurvatureKey="Curvature";
TangencyKey="Tangency";
PointKey="Point";

ListeOnglets.OngletVisu.Title="Visualization";

ListeOnglets.OngletVisu.LabelVisuRadio.Title="Shown solution(s): ";
ListeOnglets.OngletVisu.LabelVisuRadio.LongHelp="";
ListeOnglets.OngletVisu.VisuRadioFrame.SelVisuAll.Title="All";
ListeOnglets.OngletVisu.VisuRadioFrame.SelVisuAll.LongHelp="";
ListeOnglets.OngletVisu.VisuRadioFrame.SelVisuNotCorrected.Title="Not corrected";
ListeOnglets.OngletVisu.VisuRadioFrame.SelVisuNotCorrected.LongHelp="";
ListeOnglets.OngletVisu.VisuRadioFrame.SelVisuNone.Title="None";
ListeOnglets.OngletVisu.VisuRadioFrame.SelVisuNone.LongHelp="";

ListeOnglets.OngletVisu.SelVisuInteractive.Title="Display information interactively";
ListeOnglets.OngletVisu.SelVisuInteractive.LongHelp="";

ListeOnglets.OngletVisu.SelVisuSequential.Title="Display information sequentially";
ListeOnglets.OngletVisu.SelVisuSequential.LongHelp="";

SupportFrame.LabelSupport.Title="Support surface: ";
SupportFrame.LabelSupport.LongHelp="Specifies the optional support surface of the curve to be smoothed,
surface on which the result curve shall lie.";
SupportFrame.SelSupport.LongHelp="Specifies the optional support surface of the curve to be smoothed,
surface on which the result curve shall lie.";

SelSimplif.Title="Topology simplification";
SelSimplif.LongHelp="Allows to simplify output topology by erasing continuous vertices.";


// Gestion de l'ancienne mise en forme
IntituleShowParam="Show parameter >>";
IntituleHideParam="Hide parameter <<";

OptionFrame.LabelLittTangence.Title="Tangency threshold: ";
OptionFrame.LabelLittTangence.LongHelp="Specifies the tangency discontinuity under which the curve is smoothed.";
OptionFrame.FraLittTangence.EnglobingFrame.IntermediateFrame.Spinner.Title="Tangency";

//continuity diagnosis reps
PtDiscontinuityLabel = "point discontinuous";
TgDiscontinuityLabel = "tangency discontinuous";
CrvDiscontinuityLabel = "curvature discontinuous";

SuppressionLabel = "vertex erased";
C2Label = "vertex corrected";

