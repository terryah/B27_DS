Title1  = "Main Pulling Direction Definition ";
Title2  = "Slider Lifter Pulling Direction Definition " ;

// Titre de la barre de progression
ProgressBar	=	"Please wait...";

Repere.MainDirection   = "Main Pulling Direction";
Repere.SliderDirection = "Slider/Lifter Direction";
Body.Core              = "Core";
Body.Cavity            = "Cavity";
Body.Other             = "Other";
Body.Slider            = "Slider/Lifter";
Body.NoDraft           = "NoDraft";
Join.Core              = "Core";
Join.Cavity            = "Cavity";
Join.Other             = "Other";
Join.Slider            = "Slider";
OpenRemove			     = "Removed surfaces";
OpenAdd				     = "Added surfaces";
 
// Type de propagation
PropagationC0		     = "Point continuity";
PropagationC1		     = "By area";
PropagationNone		  = "No propagation";
PropagationNoDraft	  = "No draft faces";

AngularToleranceCore   = "Core Angular Tolerance";
AngularToleranceCavity = "Cavity Angular Tolerance";
AngularToleranceOther  = "Other Angular Tolerance";
AngularToleranceSlider = "Slider/Lifter Angular Tolerance";
MergeDistanceCore      = "Core Merge Distance";
MergeDistanceCavity    = "Cavity Merge Distance";
MergeDistanceOther     = "Other Merge Distance";
MergeDistanceSlider    = "Slider/Lifter Merge Distance";

CATCCSDraftDlg.InitialState.Message   = "Select an object for draft analysis";
CATCCSDraftDlg.State1.Message         = "Select a pulling direction";
CATCCSDraftDlg.State2.Message         = "Select a join";
CATCCSDraftDlg.UndoTitle			  = "Pulling Direction Definition";

FrameGeneral.Title = "Definition";

FrameGeneral.FrameSysteme.Title = " Pulling Axis System";
FrameGeneral.FrameSysteme.LongHelp = 
	"Defines the pulling axis system";

FrameGeneral.FrameSysteme.FrameDirectionDemoulage.Title = "Direction";
FrameGeneral.FrameSysteme.FrameDirectionDemoulage.LongHelp = 
	"Defines the pulling direction axis either by its coordinates or using the compass.";

FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameEditDirection.LabelDirX.Title = " DX  ";

FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameDirection2Inv.CheckButtonLock.Title = " Locked ";
FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameDirection2Inv.CheckButtonLock.LongHelp = 
	"Locks the direction to avoid unintentional modifications.
The direction must be locked before using Local Transfer, Fly Analysis or Explode.";

FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameDirection2Inv.CheckButtonFly.Title  = " Fly Analysis";
FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameDirection2Inv.CheckButtonFly.LongHelp  = 
	"Displays the normal to the face when you move the mouse pointer over the surface";

FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameEditDirection.LabelDirY.Title = " DY  ";
FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameEditDirection.LabelDirZ.Title = " DZ  ";
FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameDirection2Inv.PushButtonInvert.Title = "Reverse";
FrameGeneral.FrameSysteme.FrameDirectionDemoulage.FrameDirection2Inv.PushButtonInvert.LongHelp = 
	"Reverses the puling direction";

FrameGeneral.FrameSysteme.FrameParameterInv.FrameParameter.Title = " Draft angle ";
FrameGeneral.FrameSysteme.FrameParameterInv.FrameParameter.LongHelp = 
	"Defines the minimum un-molding angle.";

FrameGeneral.FrameSysteme.FrameParameterInv.FrameParameter.LabelDraftAngle.Title = "Draft angle ";
FrameGeneral.FrameSysteme.FrameParameterInv.FrameParameter.LabelErrCorde.Title = "Chord error ";

FrameGeneral.FrameSysteme.FrameParameterInv.FrameReset.PushButtonReset.Title = "Reset";
FrameGeneral.FrameSysteme.FrameParameterInv.FrameReset.PushButtonReset.LongHelp = 
	"Resets Pulling Axis System";

FrameGeneral.FrameBasInv.Title = "Mold Areas";
FrameGeneral.FrameBasInv.LongHelp = "Extracts the mold areas";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.Title = "Areas to Extract";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.LongHelp = 
	"Assigns a color to the surfaces found according to the area to extract they belong to.
Displays the size of each area to extract found.";

FrameGeneral.FrameBasInv.FrameExtract.CheckButtonCore.Title1   = "Core   ";
FrameGeneral.FrameBasInv.FrameExtract.CheckButtonCore.Title2   = "Slider/Lifter";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonCore.Title1   = "Core   ";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonCore.Title2   = "Slider/Lifter";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.LabelCouleurCore.Title   = "Green";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonCavity.Title  = "Cavity ";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.LabelCouleurCavity.Title = "Red";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonSlider.Title  = "Other ";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonNoDraft.Title = "No Draft ";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonShadow.Title  = "Undercut";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonShadow.LongHelp  = 
	"Takes account of hidden faces for a given pulling direction.";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonConnex.Title    = "Connected area";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.CheckButtonConnex.LongHelp = 
	"Uses all of the faces that are connected to the face you have selected (when active).";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.LabelCouleurSlider.Title = "Blue";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.LabelCouleurNoDraft.Title= "Pink";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameExtract.LabelZoneConnexe.Title   = "Connected Zones";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameSwitch.PushButtonSwitch.Title   = "Switch Core/Cavity ";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameSwitch.LongHelp= 
	"Switchs the core and cavity areas.";

FrameGeneral.FrameSysteme.FrameIgnore.FacetsToIgnore.Title = "Facets to ignore";
FrameGeneral.FrameSysteme.FrameIgnore.LongHelp= 
"Sets the acceptable percentage of facets in a surface that have a different orientation from the rest.";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameTransfer.Title     = "Local Transfer";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameTransfer.LongHelp  = 
	"No propagation: select the faces to transfer one by one.
Point continuity: select a reference face. All the faces contiguous to the reference face are transferred.
By area: select a face of a given color isolated among faces of another color. It will be transferred to the area of the surrounding color.";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameTransfer.CheckButtonTransfer.Title     = "Target ";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameTransfer.CheckButtonTransfer.LongHelp  = 
	"Transfers one face of the part to one of the areas to extract";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameVisu.Title   = "Visualization";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameVisu.LongHelp= 
	"Displays the surfaces as faces or as facets.
Use the facets display to check the orientation of the faces";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameVisu.DisplayFace.Title       = " Faces display  ";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameVisu.DisplayFacette.Title    = " Facets display ";

FrameGeneral.FrameBasInv.FrameMoldAreas.FrameVisu.DisplayExplode.Title    = " Explode";
FrameGeneral.FrameBasInv.FrameMoldAreas.FrameVisu.DisplayExplode.LongHelp = 
	"Explodes the mold areas along the axis of the current pulling direction";

// Transfer
FrameGeneral.FrameBasInv.FrameTransfer0.Title  = "Local Transfer";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.NoDraftToOther.Title     = "NoDraft->Other";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.NoDraftToOther.ShortHelp = "NoDraft->Other";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.NoDraftToOther.LongHelp  = 
"Transfers all NoDraft faces of the part to the Other area.";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OtherToCore.Title     = "Other->Core";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OtherToCore.ShortHelp = "Other->Core";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OtherToCore.LongHelp  = 
"Transfers all Other faces of the part to the Core area.";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OtherToCavity.Title      = "Other->Cavity";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OtherToCavity.ShortHelp  = "Other->Cavity";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OtherToCavity.LongHelp   = 
"Transfers all Other faces of the part to the Cavity area.";

FrameGeneral.FrameBasInv.FrameBtns.Title  = "Separation";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.ReduceTransitions.Title     = "Reduce Transitions";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.ReduceTransitions.ShortHelp = "Reduce Transitions";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.ReduceTransitions.LongHelp  = 
"NoDraft faces and Other Faces are checked one by one. 
When a face is checked if all adjacent faces are in the same area (Core or Cavity), 
the checked face is transferred in the area (Core or Cavity)";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OptimizeSplit.Title        = "Optimize Split";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OptimizeSplit.ShortHelp    = "Optimize Split";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.OptimizeSplit.LongHelp     = 
"The small core and cavity areas are transferred in Other area.";

FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.QuickSplit.Title        = "Quick Dispatch";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.QuickSplit.ShortHelp    = "Quick Dispatch";
FrameGeneral.FrameBasInv.FrameTransfer0.FrameBtn0.QuickSplit.LongHelp     = 
"Quick Dispatch
This action computes an automatic separation.";

