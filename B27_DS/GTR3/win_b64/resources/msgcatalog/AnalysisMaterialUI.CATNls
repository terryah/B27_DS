// COPYRIGHT DASSAULT SYSTEMES 2000
//===========================================================================
//
// CATSamMaterialFrame (English)
//
//===========================================================================
Analysis = "Analysis";

INF_PoissonRatioModifed="Poisson's ratio cannot exceed 0.5.It has been set to 0.4999.";
INF_PoissonRatioExceed="Poisson's ratio is greater than the maximum allowed value. It has been set to the maximum allowed value.";
INF_YoungsModulusRatio="Young�s modulus ratio is greater than the maximum allowed value. Young�s modulus and Poisson�s ratio values have been set to 0.";
INF_PoissonRatioOrthotropic3D="Values of Poisson�s ratios are greater than the maximum allowed value. They have been set to the maximum allowed value.";
INF_YoungsModulusRatioOrthotropic3D="Values of Young�s modulus ratios are greater than the maximum allowed value. Values of Young�s modulus and Poisson�s ratios have been set to 0.";
INF_CombinedOrthotropic3DExceed="Combination of Young's modulus and Poisson's ratios values is greater than the allowed value of 1. Values of Young's Modulus and Poisson's ratios have been set to 0.";
MaterialInfoUITitle = "Information";

analysisFrame.Title = "Structural Properties";

SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Title= "Young Modulus";
SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Young Modulus of the material";
SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp =
"Young Modulus to describe the material stifness";

SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Title = "Poisson Ratio";
SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Poisson Ratio of the material";
SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp =
"Poisson Ratio of the material 
( mechanical dilatation , value between 0 and 0.5 )";

SAMDensity.EnglobingFrame.IntermediateFrame.Label.Title = "Density";
SAMDensity.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Density of the material";
SAMDensity.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp =
"Density of the material ( used for modal computation )";

SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Title = "Thermal Expansion";
SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Thermal expansion of the material";
SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp =
"Thermal expansion ( thermal dilatation effect ) of the material";

SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Title = "Yield Strength";
SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Yield Strength of the material";
SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp =
"Yield Strength of the material. 
To be compared with max VON MISES value to see if the part fails or not";

// SAMOrthotropic
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Title = "Longitudinal Young Modulus";
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Longitudinal Young Modulus of the material";
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Longitudinal Young Modulus";

SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Title ="Transverse Young Modulus";
SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.Help ="Transverse Young Modulus of the material"; 
SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Transverse Young Modulus";

SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Title = "Poisson Ratio in XY Plane";
SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Poisson Ratio of the material in XY Plane";
SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Poisson Ratio in XY Plane";

SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Title = "Poisson Ratio in XZ Plane";
SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Poisson Ratio of the material in XZ Plane";
SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Poisson Ratio in XZ Plane";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Title = "Poisson Ratio in YZ Plane";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Poisson Ratio of the material in YZ Plane";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Poisson Ratio in YZ Plane";

SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Modulus in XY Plane";
SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Shear Modulus in XY Plane of the material";
SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Value.longHelp = "Shear Modulus in XY Plane";

SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Modulus in XZ Plane";
SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Value.Help= "Shear Modulus in XZ Plane of the material"; 
SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Modulus in XZ Plane";

SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Modulus in YZ Plane";
SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Shear Modulus in YZ Plane of the material";
SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Modulus in YZ Plane";

SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Modulus in XZ Plane";
SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Value.Help= "Shear Modulus in XZ Plane of the material"; 
SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Modulus in XZ Plane";

SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Modulus in YZ Plane";
SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Shear Modulus in YZ Plane of the material";
SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Modulus in YZ Plane";

SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Title = "Longitudinal Tensile Stress";
SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Longitudinal Tensile Stress of the material";
SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Longitudinal Tensile Stress";

SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Title = "Longitudinal Compressive Stress";
SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Longitudinal Compressive Stress of the material";
SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Longitudinal Compressive Stress";

SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title = "Transverse Tensile Stress";
SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Transverse Tensile Stress of the material";
SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Transverse Tensile Stress";

SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title = "Transverse Compressive Stress";
SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Transverse Compressive Stress of the material";
SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Transverse Compressive Stress";

SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Title = "Longitudinal Tensile Strain";
SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Longitudinal Tensile Strain of the material";
SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Longitudinal Tensile Strain";

SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Title = "Longitudinal Compressive Strain";
SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Longitudinal Compressive Strain of the material";
SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Longitudinal Compressive Strain";

SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title = "Transverse Tensile Strain";
SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Transverse Tensile Strain of the material";
SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Transverse Tensile Strain";

SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title = "Transverse Compressive Strain";
SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Transverse Compressive Strain of the material";
SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Transverse Compressive Strain";

SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Title ="Longitudinal Thermal Expansion";
SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Value.Help ="Longitudinal Thermal Expansion of the material";
SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp ="Longitudinal Thermal Expansion";

SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Title = "Transverse Thermal Expansion";
SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Transverse Thermal Expansion of the material";
SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Transverse Thermal Expansion";

SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Title = "Normal Thermal Expansion";
SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Normal Thermal Expansion of the material";
SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Normal Thermal Expansion";

analysisFrame.stressFrame.Title = "Stress Limits";
analysisFrame.strainFrame.Title = "Strain Limits";


// combo de selection de type de materaiux
MaterialType.Help="Define the Kind of Material";
MaterialType.MaterialTypeLabel.Title="Material";
MaterialType.MaterialTypeCombo.SAMIsotropicMaterial.Title="Isotropic Material";
MaterialType.MaterialTypeCombo.SAMOrthotropicMaterial.Title="Orthotropic Material 2D";
MaterialType.MaterialTypeCombo.SAMFiberMaterial.Title="Fiber Material";
MaterialType.MaterialTypeCombo.SAMHoneyCombMaterial.Title="HoneyComb Material";
MaterialType.MaterialTypeCombo.SAMOrthotropic3DMaterial.Title="Orthotropic Material 3D";
MaterialType.MaterialTypeCombo.SAMAnisotropicMaterial.Title="Anisotropic Material";


// Material HONEYCOMB
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Title ="Normal Young Modulus";
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.Help ="Normal Young Modulus of the material"; 
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Normal Young Modulus";

SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Stress Limit in XZ Plane";
SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Shear Stress Limit in XZ Plane of the material";
SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Stress Limit in XZ Plane";

SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Stress Limit in YZ Plane";
SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Shear Stress Limit in YZ Plane of the material";
SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Stress Limit in YZ Plane";

// Material FIBER
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Stress Limit in XY Plane";
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Shear Stress Limit in XY Plane of the material";
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Stress Limit in XY Plane";

// Material ANISOTROPE
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Title = "Normal Thermal Expansion";
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Normal Thermal Expansion of the material";
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Normal Thermal Expansion";

SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Title = "Longitudinal Shear Modulus";
SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Longitudinal Shear Modulus of the material";
SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Longitudinal Shear Modulus";

SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Title = "Transverse Shear Modulus";
SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Transverse Shear Modulus of the material";
SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Transverse  Shear Modulus";

SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Title = "Normal Shear Modulus";
SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Normal Shear Modulus of the material";
SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Normal Shear Modulus";

SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Title = "Tensile Stress";
SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Tensile Stress of the Material";
SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Tensile Stress";

SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Title = "Compressive Stress";
SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Compressive Stress of the Material";
SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Compressive Stress";

SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Title = "Shear Stress";
SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help = "Shear Stress of the Material";
SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp = "Shear Stress";

