<?xml version="1.0"?>
<!-- Release Table Entries Display -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/ReleaseTable">
		<html>
			<body>
				<h3 align="center">Release Table Entries : JOB - <xsl:value-of select="JobName"/> Version # <xsl:value-of select="Version"/>
				</h3>
        <TABLE align="center" border='1' style='table-layout:fixed' width='600'>
          <TR bgcolor='#FFFF00'>
            <TD>Line Number</TD>
            <TD>SOI Statuts</TD>
            <TD>PD XML Flag</TD>
          </TR>
          <xsl:for-each select="RowElement">
            <TR>
              <TD>
                <xsl:value-of select='LineNumber'/>
              </TD>
              <TD>
                <xsl:value-of select='SOIState'/>
              </TD>
              <TD>
                <xsl:value-of select='PDXMLFlag' disable-output-escaping="yes"/>
              </TD>
            </TR>
          </xsl:for-each>
        </TABLE>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
