<?xml version="1.0" encoding="utf-8"?>
<!-- COPYRIGHT DASSAULT SYSTEMES 2007 -->
<!-- 07 Jan 2007 creation -->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" />
  <xsl:template match="/">
    <html>
      <head>
        <title>Analyze Welds For Robot - Results</title>
        <link href="./WeldAnalysis.css" media="screen" rel="StyleSheet" type="text/css" />
        <script src="./excanvas.js" type="text/javascript" />
        <script src="./DrawCanvas.js" type="text/javascript" />
      </head>
      <body onLoad="LoadCanvasData();">
        <!-- This table outputs the main Data manager properties to XHTML file-->
        <center>
          <table class="properties" >
            <tr>
              <td class="propertiesHeader" colspan="2" >Analysis Settings</td>
            </tr>
            <tr>
              <td class="propertyName" >Check for collision</td>
              <td class="propertyName" >
                <xsl:call-template name="getBooleanString" >
                  <xsl:with-param name="inputString" select="number(/AnalyseWeldForRobot/@CheckClash)" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="propertyName" >Analyze All</td>
              <td class="propertyName" >
                <xsl:call-template name="getBooleanString" >
                  <xsl:with-param name="inputString" select="number(/AnalyseWeldForRobot/@CheckAll)" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="propertyName" >Show Graphic Update</td>
              <td class="propertyName" >
                <xsl:call-template name="getBooleanString" >
                  <xsl:with-param name="inputString" select="number(/AnalyseWeldForRobot/@ShowGraphic)" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="propertyName" >Step Angle</td>
              <td class="propertyName" >
                <xsl:value-of select="/AnalyseWeldForRobot/@StepAngle" /> deg
              </td>
            </tr>
            <tr>
              <td class="propertyName" >Collision Tolerance</td>
              <td class="propertyName" >
                <xsl:value-of select="/AnalyseWeldForRobot/@ClashTolerance" /> mm
              </td>
            </tr>
          </table>
          <table class="resultsTable" >
            <xsl:for-each select="/AnalyseWeldForRobot/ResourceResult" >
              <xsl:call-template name="BuildResourceResult" >
                <xsl:with-param name="resResultNode" select="." />
              </xsl:call-template>
            </xsl:for-each>
          </table>
        </center>
      </body>
    </html>
  </xsl:template>
  <!--This template creates a tabled output for resource result node-->
  <xsl:template name="BuildResourceResult" >
    <xsl:param name="resResultNode" />
    <xsl:variable name="clashObjNb" select="count($resResultNode/ClashList/Product)" />
    <xsl:variable name="excludedPartNb" select="count($resResultNode/ExcludedRobotParts/Product)" />
    <tr>
      <td class="robotName" colspan="2" >
        <xsl:value-of select="$resResultNode/@Name" />
      </td>
    </tr>
        <tr class="productListRow">
          <xsl:element name="td">
            <xsl:attribute name="class">productListName</xsl:attribute>
            Clash List
          </xsl:element>
          <td class="productList">
            <xsl:call-template name="buildProductList" >
              <xsl:with-param name="productList" select="$resResultNode/ClashList" />
            </xsl:call-template>
          </td>
        </tr>
        <tr class="productListRow">
          <xsl:element name="td">
            <xsl:attribute name="class">productListName</xsl:attribute>
            Excluded Robot Parts
          </xsl:element>
          <td class="productList">
            <xsl:call-template name="buildProductList" >
              <xsl:with-param name="productList" select="$resResultNode/ExcludedRobotParts" />
            </xsl:call-template>
          </td>
        </tr>
    <tr>
      <td colspan="2">
      <xsl:call-template name="buildWeldResults" >
        <xsl:with-param name="resourceResNode" select="." />
      </xsl:call-template>
      </td>
    </tr>
  </xsl:template>

  <!--This template appends a string that represents true or false-->
  <xsl:template name="getBooleanString" >
    <xsl:param name="inputString" />
    <xsl:if test="boolean($inputString)" >Yes</xsl:if>
    <xsl:if test="not(boolean($inputString))" >No</xsl:if>
  </xsl:template>

  <!--This template builds a simple product list by outputing-->
  <xsl:template name="buildProductList" >
    <xsl:param name="productList" />
    <div>
      <ul>
        <xsl:for-each select="$productList/Product" >
          <li>
            <xsl:value-of select="@ProductName" />
          </li>
        </xsl:for-each>
        <xsl:if test="count($productList/Product) = 0" >
          <li>None</li>
        </xsl:if>
      </ul>
    </div>
  </xsl:template>

  <!--This template will format and output all the weld results-->
  <xsl:template name="buildWeldResults" >
    <xsl:param name="resourceResNode" />
    <!--To do the magic here-->
    <div class="weldResultsFrame" >
    <table class="weldResults" >
      <thead>
        <tr>
          <th class="weldResultHead">Weld Name</th>
          <th class="weldResultHead" >Tool Profile</th>
          <th class="weldResultHead">Robot Config</th>
          <th class="weldResultHead">No Flip</th>
          <th class="weldResultHead">Flip</th>
          <th class="weldResultHead">Pie Diagram</th>
        </tr>
      </thead>
      <tbody>
        <xsl:variable name="configNb" select="count(./SelectedConfigList/ConfigName)" />
        <xsl:variable name="profileNb" select="count(./SelectedToolProfiles/ToolProfile)" />
        <xsl:for-each select="$resourceResNode/WeldResult" >
          <xsl:if test="@EndAngle > @StartAngle" >
            <xsl:variable name="tempVar" select="@EndAngle - @StartAngle" />
            <xsl:call-template name="buildWeld" >
              <xsl:with-param name="resResultNode" select="$resourceResNode" />
              <xsl:with-param name="numberOfConfigs" select="$configNb" />
              <xsl:with-param name="numberOfProfiles" select="$profileNb" />
              <xsl:with-param name="weldAngleRange" select="$tempVar" />
            </xsl:call-template>
          </xsl:if>
          <xsl:if test="@StartAngle > @EndAngle" >
            <xsl:variable name="tempVar" select="@StartAngle - @EndAngle" />
            <xsl:call-template name="buildWeld" >
              <xsl:with-param name="resResultNode" select="$resourceResNode" />
              <xsl:with-param name="numberOfConfigs" select="$configNb" />
              <xsl:with-param name="numberOfProfiles" select="$profileNb" />
              <xsl:with-param name="weldAngleRange" select="$tempVar" />
            </xsl:call-template>
          </xsl:if>
        </xsl:for-each>
      </tbody>
    </table>
    </div>
  </xsl:template>

  <!--Build the multiple rows needed for the result display-->
  <xsl:template name="buildWeld" >
    <xsl:param name="resResultNode" />
    <xsl:param name="numberOfConfigs" />
    <xsl:param name="numberOfProfiles" />
    <xsl:param name="weldAngleRange" select="360"/>
    <xsl:variable name="stepAngle" select="/AnalyseWeldForRobot/@StepAngle" />
    <xsl:variable name="startAngle" select="@StartAngle" />
    <xsl:variable name="transformResNb" select="$weldAngleRange div $stepAngle + 1" />
    <xsl:variable name="currentWeldResult" select="." />
    <tr>
      <xsl:element name="td" >
        <xsl:attribute name="class" >weldName</xsl:attribute>
        <xsl:attribute name="rowspan" >
          <xsl:value-of select="$numberOfConfigs*$numberOfProfiles" />
        </xsl:attribute>
        <xsl:value-of select="@Name" />
      </xsl:element>
      <xsl:element name="td" >
        <xsl:attribute name="class" >weldResult</xsl:attribute>
        <xsl:attribute name="rowspan" >
          <xsl:value-of select="$numberOfConfigs" />
        </xsl:attribute>
        <xsl:value-of select="$resResultNode/SelectedToolProfiles/ToolProfile[position() = 1]/@Name"/>
      </xsl:element>
      <xsl:element name="td" >
        <xsl:attribute name="class">weldResult</xsl:attribute>
        <xsl:value-of select="$resResultNode/SelectedConfigList/ConfigName[position() = 1]/@Name" />
      </xsl:element>
      <!--No Flip Result-->
      <xsl:element name="td" >
        <xsl:attribute name="class" >weldResult</xsl:attribute>
        <!--Cumulative Weld Result-->
        <xsl:call-template name="decodeWeldResults" >
          <xsl:with-param name="weldResults" select="./TransformResults/Result[position() > 0 and position() &lt;= $transformResNb]" />
          <xsl:with-param name="mode" select="string('NoFlip')" />
        </xsl:call-template>
      </xsl:element>
      <!--Flip Result-->
      <xsl:element name="td" >
        <xsl:attribute name="class" >weldResult</xsl:attribute>
        <!--Cumulative Weld Result-->
        <xsl:call-template name="decodeWeldResults" >
          <xsl:with-param name="weldResults" select="./TransformResults/Result[position() > 0 and position() &lt;= $transformResNb]" />
          <xsl:with-param name="mode" select="string('Flip')" />
        </xsl:call-template>
      </xsl:element>
      <xsl:element name="td" >
        <xsl:attribute name="class" >weldResult</xsl:attribute>
        <div class="canvasParent" >
          <xsl:element name="table">
            <xsl:attribute name="class" >canvasDataTable</xsl:attribute>
            <tbody>
              <!--Properties Output row-->
              <tr>
                <td>
                  <xsl:value-of select="$startAngle"/>
                </td>
                <td>
                  <xsl:value-of select="$stepAngle"/>
                </td>
                <td>
                  <xsl:value-of select="$weldAngleRange"/>
                </td>
              </tr>
              <!--Weld results Output row-->
              <tr>
                <xsl:for-each select="./TransformResults/Result[position() > 0 and position() &lt;= $transformResNb]" >
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:for-each>
              </tr>
            </tbody>
          </xsl:element>
          <canvas width="250" height="200" class="canvasElement" ></canvas>
        </div>
      </xsl:element>
    </tr>
    <xsl:variable name="transformResults" select="$resResultNode/TransformResults/Result" />
    <xsl:for-each select="$resResultNode/SelectedToolProfiles/ToolProfile" >
      <xsl:variable name="toolProfileNode" select="." />
      <xsl:variable name="currentToolProfile" select="position()"/>
      <xsl:if test="position() != 1" >
        <tr>
          <xsl:element name="td" >
            <xsl:attribute name="class" >weldResult</xsl:attribute>
            <xsl:attribute name="rowspan" >
              <xsl:value-of select="$numberOfConfigs" />
            </xsl:attribute>
            <xsl:value-of select="$resResultNode/SelectedToolProfiles/ToolProfile[position() = $currentToolProfile]/@Name" />
          </xsl:element>
          <xsl:element name="td" >
            <xsl:attribute name="class" >weldResult</xsl:attribute>
            <xsl:value-of select="$resResultNode/SelectedConfigList/ConfigName[position() = 1]/@Name" />
          </xsl:element>
          <xsl:variable name="weldResultIndex" select="$currentToolProfile - 1" />
          <!--No Flip Result-->
          <xsl:element name="td" >
            <xsl:attribute name="class" >weldResult</xsl:attribute>
            <!--Cumulative Weld Result-->
            <xsl:call-template name="decodeWeldResults" >
              <xsl:with-param name="weldResults" select="$currentWeldResult/TransformResults/Result[position() > $weldResultIndex*$transformResNb and position() &lt;= $weldResultIndex*$transformResNb + $transformResNb]" />
              <xsl:with-param name="mode" select="string('NoFlip')" />
            </xsl:call-template>
          </xsl:element>
          <!--Flip Result-->
          <xsl:element name="td" >
            <xsl:attribute name="class" >weldResult</xsl:attribute>
            <!--Cumulative Weld Result-->
            <xsl:call-template name="decodeWeldResults" >
              <xsl:with-param name="weldResults" select="$currentWeldResult/TransformResults/Result[position() > $weldResultIndex*$transformResNb and position() &lt;= $weldResultIndex*$transformResNb + $transformResNb]" />
              <xsl:with-param name="mode" select="string('Flip')" />
            </xsl:call-template>
          </xsl:element>
          <xsl:element name="td" >
            <xsl:attribute name="class" >weldResult</xsl:attribute>
            <div class="canvasParent" >
              <xsl:element name="table">
                <xsl:attribute name="class" >
                  canvasDataTable
                </xsl:attribute>
                <tbody>
                  <!--Properties Output row-->
                  <tr>
                    <td>
                      <xsl:value-of select="$startAngle"/>
                    </td>
                    <td>
                      <xsl:value-of select="$stepAngle"/>
                    </td>
                    <td>
                      <xsl:value-of select="$weldAngleRange"/>
                    </td>
                  </tr>
                  <!--Weld results Output row-->
                  <tr>
                    <xsl:for-each select="$currentWeldResult/TransformResults/Result[position() > $weldResultIndex*$transformResNb and position() &lt;= $weldResultIndex*$transformResNb + $transformResNb]" >
                      <td>
                        <xsl:value-of select="."/>
                      </td>
                    </xsl:for-each>
                  </tr>
                </tbody>
              </xsl:element>
              <canvas width="250" height="200" class="canvasElement" ></canvas>
            </div>
          </xsl:element>
        </tr>
      </xsl:if>
      <xsl:for-each select="$resResultNode/SelectedConfigList/ConfigName" >
        <xsl:variable name="currentConfig" select="position()" />
        <xsl:if test="$toolProfileNode[position() != 1] or position() != 1" >
          <tr>
            <xsl:element name="td" >
              <xsl:attribute name="class" >weldResult</xsl:attribute>
              <xsl:value-of select="$resResultNode/SelectedConfigList/ConfigName[position() = $currentConfig]/@Name" />
            </xsl:element>
            <xsl:variable name="weldResultIndex" select="$currentToolProfile*$currentConfig - 1" />
            <!--No Flip Result-->
            <xsl:element name="td" >
              <xsl:attribute name="class" >weldResult</xsl:attribute>
              <!--Cumulative Weld Result-->
              <xsl:call-template name="decodeWeldResults" >
                <xsl:with-param name="weldResults" select="$currentWeldResult/TransformResults/Result[position() > $weldResultIndex*$transformResNb and position() &lt;= $weldResultIndex*$transformResNb + $transformResNb]" />
                <xsl:with-param name="mode" select="string('NoFlip')" />
              </xsl:call-template>
            </xsl:element>
            <!--Flip Result-->
            <xsl:element name="td" >
              <xsl:attribute name="class" >weldResult</xsl:attribute>
              <!--Cumulative Weld Result-->
              <xsl:call-template name="decodeWeldResults" >
                <xsl:with-param name="weldResults" select="$currentWeldResult/TransformResults/Result[position() > $weldResultIndex*$transformResNb and position() &lt;= $weldResultIndex*$transformResNb + $transformResNb]" />
                <xsl:with-param name="mode" select="string('Flip')" />
              </xsl:call-template>
            </xsl:element>
            <xsl:element name="td" >
              <xsl:attribute name="class" >weldResult</xsl:attribute>
              <div class="canvasParent" >
                <xsl:element name="table">
                  <xsl:attribute name="class" >
                    canvasDataTable
                  </xsl:attribute>
                  <tbody>
                    <!--Properties Output row-->
                    <tr>
                      <td>
                        <xsl:value-of select="$startAngle"/>
                      </td>
                      <td>
                        <xsl:value-of select="$stepAngle"/>
                      </td>
                      <td>
                        <xsl:value-of select="$weldAngleRange"/>
                      </td>
                    </tr>
                    <!--Weld results Output row-->
                    <tr>
                      <xsl:for-each select="$currentWeldResult/TransformResults/Result[position() > $weldResultIndex*$transformResNb and position() &lt;= $weldResultIndex*$transformResNb + $transformResNb]" >
                        <td>
                          <xsl:value-of select="."/>
                        </td>
                      </xsl:for-each>
                    </tr>
                  </tbody>
                </xsl:element>
                <canvas width="250" height="200" class="canvasElement" ></canvas>
              </div>
            </xsl:element>
          </tr>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="decodeWeldResults" >
    <xsl:param name="weldResults" />
    <xsl:param name="mode" select="string('NoFlip')" />
    <xsl:variable name="numericResultUnion" >
      <xsl:call-template name="processWeldResults" >
        <xsl:with-param name="weldResults" select="$weldResults" />
        <xsl:with-param name="mode" select="$mode" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$numericResultUnion = 3">OK</xsl:when>
      <xsl:when test="$numericResultUnion = 2">NF</xsl:when>
      <xsl:when test="$numericResultUnion = 1">NR</xsl:when>
      <xsl:otherwise>NC</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--Processes weld results and generates the magic string-->
  <xsl:template name="processWeldResults" >
    <xsl:param name="weldResults" />
    <xsl:param name="mode" select="NoFlip" />
    <xsl:choose>
      <xsl:when test="$mode = 'NoFlip'">
        <!--Any time we encounter OK result recursion stopsotherwise, we keep returing 
        the higher result because higher result has bigger priority-->
        <!--NoFlip result analysis-->
        <!--Recursion Logic:
        If, result modulus 3 is zero and quotient when divided by 3 is non-zero then, result is OK(numerically 3)
        Else, we directly take result modulus 3 as numerical result.
        
        This logic is applied on results recursively-->
        <xsl:variable name="tempVar" select="number($weldResults[1])" />
        <xsl:variable name="firstResult" >
          <xsl:choose>
            <xsl:when test="$tempVar mod 3 = 0 and $tempVar div 3 != 0">
              <xsl:number value="number('3')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:number value="number($tempVar mod 3)"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$firstResult = 3">3</xsl:when>
          <xsl:otherwise>
            <xsl:variable name="restResultUnion" >
              <xsl:choose>
                <xsl:when test="count($weldResults[position() != 1]) > 0" >
                  <xsl:call-template name="processWeldResults" >
                    <xsl:with-param name="weldResults" select="$weldResults[position() != 1]"/>
                    <xsl:with-param name="mode" select="string('NoFlip')" />
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="$firstResult > $restResultUnion">
                <xsl:value-of select="$firstResult"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$restResultUnion"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <!--Flip result analysis-->
        <xsl:variable name="tempVar" select="number($weldResults[1])" />
        <xsl:variable name="firstResult" >
          <xsl:choose>
            <xsl:when test="$tempVar mod 3 = 0 and $tempVar div 3 != 0">
              <xsl:number value="number(floor($tempVar div 3 - 1))"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:number value="number(floor($tempVar div 3))"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$firstResult = 3">3</xsl:when>
          <xsl:otherwise>
            <xsl:variable name="restResultUnion" >
              <xsl:choose>
                <xsl:when test="count($weldResults[position() != 1]) > 0" >
                  <xsl:call-template name="processWeldResults" >
                    <xsl:with-param name="weldResults" select="$weldResults[position() != 1]" />
                    <xsl:with-param name="mode" select="string('Flip')" />
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="$firstResult > $restResultUnion" >
                <xsl:value-of select="$firstResult"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$restResultUnion"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
