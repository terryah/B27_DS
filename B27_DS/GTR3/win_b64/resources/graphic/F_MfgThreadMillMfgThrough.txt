# bitmap Int Threaded Hole feature 

# Anchor Point
"Anchor"
2
5
107 232
107 82
110 82
110 232
107 232
5
161 232
161 82
164 82
164 232
161 232

# Diameter
"Diameter"
1
4
166 248
230 248
104 248
166 248

# THDiameter
"THDiameter"
1
4
103 70
166 70
166 70
168 70

# Depth
"Depth"
1
4
20 227
20 89
20 89
20 87

# Bottom plane
"Bottom"
1
5
108 232
162 232
162 229
107 229
107 232

# Check elements
"Checks"
2
6
8 62
48 62
67 67
67 80
8 80
8 62
4
68 67
76 69
76 80
68 80

# Info design
"Info"
0

