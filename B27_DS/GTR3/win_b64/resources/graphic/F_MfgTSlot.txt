# bitmap TSLOT feature 

# Anchor Point
"Anchor"
2
5
107 82
107 146
110 146
110 82
107 82
5
161 82
161 146
164 146
164 82
161 82

# Depth
"Depth"
1
4
20 182
20 86
20 86
20 88

# Diameter
"Diameter"
1
4
107 70
162 70
162 70
164 70

# SlotDiameter
"SlotDiameter"
1
4
94 269
176 269
176 269
178 269

# SlotDepth
"SlotDepth"
1
4
42 148
42 92
42 187
42 148

# Bottom plane
"Bottom"
1
5
94 187
177 187
177 184
94 184
94 187

# Check elements
"Checks"
2
6
8 62
48 62
67 67
67 80
8 80
8 62
4
68 67
76 69
76 80
68 80

#Info design
"Info"
0
