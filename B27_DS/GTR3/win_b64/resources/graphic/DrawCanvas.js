// COPYRIGHT 2007 COPYRIGHT DASSAULT SYSTEMES
//
// Custom JS to read the table data and load canvas
// in html generated from XML reports for Analyze welds
// for robot
//
//

function LoadCanvasData()
{
    var canvasElementList = document.getElementsByTagName("canvas");
    if( canvasElementList.length != 0 )
    {
        var canvasElementNb = canvasElementList.length;
        for( var ii = 0; ii < canvasElementNb; ++ii )
        {
            var canvasElement = canvasElementList[ii];
            
            // Extracting properties for Pie representation
            var propertyNodeList = canvasElement.previousSibling.childNodes[0].childNodes[0].childNodes;
            var startAngle = -180;
            var stepAngle = 5;
            var angleRange = 360;
            if( propertyNodeList.length == 3 )
            {
                startAngle = parseFloat(propertyNodeList[0].innerHTML);
                stepAngle = parseFloat(propertyNodeList[1].innerHTML);
                angleRange = parseFloat(propertyNodeList[2].innerHTML);
            }
            // Extracting data for canvas
            // Access table/tbody/tr
            var dataNodeList = canvasElement.previousSibling.childNodes[0].childNodes[1].childNodes;
            if( dataNodeList.length == (angleRange/stepAngle) + 1 )
            {
                drawPie( canvasElement, dataNodeList, startAngle, stepAngle, angleRange );
            }
        }
    }
};

function drawPie( canvasElement, dataNodes, startAngle, stepAngle, angleRange )
{
    if( dataNodes.length )
    {
        var dataNodesNb = dataNodes.length;
        var sectorStartAngle = startAngle;
        var sectorEndAngle = startAngle;
        var currentStatus = 0;
        for( var ii = 0; ii < dataNodesNb; ++ii )
        {
            var parsedResult = {noFlip: 0, flip: 0 };
            parseResult( dataNodes[ii].innerHTML, parsedResult );
            var status = 0;
            if( parsedResult.noFlip == 3 )
                status = 4;
            else if( parsedResult.flip == 3 )
                status = 3;
            else if( parsedResult.noFlip == 2 || parsedResult.flip == 2 )
                status = 2;
            else if( parsedResult.noFlip == 1 || parsedResult.flip == 1 )
                status = 1;
            else
                status = 0;
            if( ii == 0 )
            {
                currentStatus = status;
                sectorEndAngle = sectorStartAngle + stepAngle/2;
            }
            else if( currentStatus == status )
            {
                if( ii == dataNodesNb - 1 )
                {
                    sectorEndAngle = sectorEndAngle + stepAngle/2;
                    drawSector( canvasElement, sectorStartAngle, sectorEndAngle, currentStatus );
                    drawSectorEdge( canvasElement, sectorStartAngle, sectorStartAngle + 'deg' );
                }
                else
                    sectorEndAngle = sectorEndAngle + stepAngle;
            }
            else
            {
                if( sectorStartAngle == sectorEndAngle )
                {
                    if( ii == dataNodesNb - 1 )
                        sectorEndAngle = sectorStartAngle + stepAngle/2;
                    else
                        sectorEndAngle = sectorEndAngle + stepAngle;
                }
                drawSector( canvasElement, sectorStartAngle, sectorEndAngle, currentStatus );
                drawSectorEdge( canvasElement, sectorStartAngle, sectorStartAngle + 'deg' );
                sectorStartAngle = sectorEndAngle;
                if( ii == dataNodesNb - 1 )
                    sectorEndAngle = sectorStartAngle + stepAngle/2;
                else
                    sectorEndAngle = sectorEndAngle + stepAngle;
                currentStatus = status;
            }
        }
        if( angleRange != 360 )
            drawSectorEdge( canvasElement, sectorEndAngle, sectorEndAngle + 'deg' );
    }
};

function drawSector( canvasElement, startAngle, endAngle, status )
{
    if( canvasElement.getContext )
    {
        // IE Hack - To make it work in IE
        // with VML based excanvas.js
        if( endAngle - startAngle == 360 )
        {
            drawSector( canvasElement, startAngle, startAngle + 180, status );
            drawSector( canvasElement, endAngle - 180, endAngle, status );
        }
        var colorArray = [ "white", "red", "blue", "yellow", "green" ];
        var startAngleInRadian = startAngle * Math.PI/180;
        var endAngleInRadian = endAngle * Math.PI/180;
        var canvasCtx = canvasElement.getContext('2d');
        canvasCtx.strokeStyle = "black";
        canvasCtx.fillStyle = colorArray[ status ];
        var centerX = 125;
        var centerY = 100;
        var medianAngle = endAngleInRadian - startAngleInRadian;
        canvasCtx.moveTo( centerX, centerY );
        canvasCtx.beginPath();
        canvasCtx.moveTo( centerX, centerY );
        canvasCtx.arc( centerX, centerY, 50, startAngleInRadian, endAngleInRadian, false );
        canvasCtx.closePath();
        canvasCtx.fill();
    }
};

function drawSectorEdge( canvasElement, angle, string )
{
    var canvasParent = canvasElement.parentNode;
    var angleInRadian = angle * Math.PI/180;
    var radius = 80;
    if( ( angle > 135 && angle < 180 ) || (angle < -135 && angle > -180 ) )
        radius = 110;
    else if( ( angle > 90 && angle <= 135 ) || ( angle >= -135 && angle < -90 ) )
        radius = 75;
    else if( (angle <= 90 && angle > 0 ) || ( angle >= -90 && angle < 0 ) )
        radius = 65;
    var left = 125 + radius*Math.cos( angleInRadian );
    var top = 100 + radius*Math.sin( angleInRadian );
    if( canvasElement.getContext )
    {
        var canvasCtx = canvasElement.getContext('2d');
        canvasCtx.strokeStyle = "black";
        canvasCtx.lineWidth = 2;
        var lineEndPtX = 125 + 50*Math.cos( angleInRadian );
        var lineEndPtY = 100 + 50*Math.sin( angleInRadian );
        canvasCtx.beginPath();
        canvasCtx.moveTo( 125, 100 );
        canvasCtx.lineTo( lineEndPtX, lineEndPtY );
        canvasCtx.stroke();
    }
    var labelElement = document.createElement("div");
    labelElement.innerHTML = string;
    labelElement.setAttribute( "id", "pieLabelClass" );
    var styleLeft = left + 'px';
    var styleTop = top + 'px';
    var styleObbj = labelElement.style;
    labelElement.style.left = styleLeft;
    labelElement.style.top = styleTop;
    canvasParent.appendChild( labelElement );
};

function parseResult( resultUnion, parsedResult )
{
    if( resultUnion % 3 == 0 && resultUnion / 3 != 0 )
    {
        parsedResult.noFlip = 3;
    }
    else
    {
        parsedResult.noFlip = resultUnion % 3;
    }
    
    if( resultUnion % 3 == 0 && resultUnion / 3 != 0 )
    {
        parsedResult.flip = resultUnion / 3 - 1;
    }
    else
    {
        parsedResult.flip = Math.floor(resultUnion / 3);
    }
};

