For HF-077965V5R20SP4WIM,following resources are added in the FW CATStrDetail
1.CATStructureDetailDesignSample.CATfct and 
2.CATStructureDetailDesign.feat

These resources were present initially pre R14. 
Due to data model change these were removed post R14.
Now since customer is migrating from R9 to R20SP04, these files have to be restored, so as to solve link errors in customer data.
