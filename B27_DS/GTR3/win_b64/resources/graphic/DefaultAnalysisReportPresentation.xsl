<?xml version = "1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<HTML>
		<HEAD>
			<TITLE>Gun Search Result Analysis Report</TITLE>
			<BODY>
			<p><b><font size="5" color="#E88B00"><img border="0" src="DSLogo.gif" align="right" width="86" height="56">Gun Search Result Analysis Report</img></font></b></p>
			<p>COPYRIGHT Dassault-Systemes 2003.</p>
			<p><b><font size="5">Summary</font></b></p>
			<p><i><b><font size="4"> Description</font></b></i></p>
			<p> Analysis Report for results provided by Gun search in TSA.</p>
			<p> <font size="4"><b><i>Model Information</i></b></font></p>
			<p><font size="4"><b><i> </i></b></font>
				<i><b><font size="4">Comments</font></b></i>
				<p><xsl:value-of select="//UserComment"/></p>
			</p>
			<p><b><font size="5">Analysis Parameter Information</font></b></p>
			
			<p><b><font size="5">Weld Gun and Welds from Result Files</font></b></p>
				<table border="1" width="100%">
				<tr>
			    <td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Weld Gun used</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Weld Point</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">No Flip Result</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Flip Result</font></b></td>
				</tr>
				<xsl:for-each select="//AnalysisResult">
					<xsl:sort select="WeldGun"/>
					<tr>
						<td width="25%" align="center"><xsl:value-of select="WeldGun"/></td>
						<td width="25%" align="center"><xsl:value-of select="WeldPoint"/></td>
						<xsl:apply-templates select="Result" />
					</tr>
				</xsl:for-each>
				</table>
			<p><b><font size="5">Mandatory Weld guns and Relevant Welds</font></b></p>
				<table border="1" width="100%">
				<tr>
			    <td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Mandatory Weld Gun</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Weld Point</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">No Flip Result</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Flip Result</font></b></td>
				</tr>
				<xsl:for-each select="//MandatoryGunsWeldResult">
					<xsl:sort select="WeldGun"/>
					<tr>
						<td width="25%" align="center"><xsl:value-of select="WeldGun"/></td>
						<td width="25%" align="center"><xsl:value-of select="WeldPoint"/></td>
						<xsl:apply-templates select="Result" />
					</tr>
				</xsl:for-each>
				</table>
			<p><b><font size="5">Welds Accessible by Only One Gun</font></b></p>
				<table border="1" width="100%">
				<tr>
			    <td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Weld Point</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Weld Gun</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">No Flip Result</font></b></td>
				<td width="25%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Flip Result</font></b></td>
				</tr>
				<xsl:for-each select="//WeldByOnlyGunResult">
					<xsl:sort select="WeldPoint"/>
					<tr>
						<td width="25%" align="center"><xsl:value-of select="WeldPoint"/></td>
						<td width="25%" align="center"><xsl:value-of select="WeldGun"/></td>
						<xsl:apply-templates select="Result" />
					</tr>
				</xsl:for-each>
				</table>
			<p><b><font size="5">Welds Not Accessible by any Gun</font></b></p>
				<table border="1" width="30%">
				<tr>
			    <td width="30%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Inaccessible Weld Points</font></b></td>				
				</tr>
				<xsl:for-each select="//InaccessibleWeld">					
					<tr>
						<td width="30%" align="center"><xsl:value-of select="."/></td>						
					</tr>
				</xsl:for-each>
				</table>
			<p><b><font size="5">Welds Not Computed</font></b></p>
				<table border="1" width="30%">
				<tr>
			    <td width="30%" align="center" bgcolor="#0000FF"><b><font color="#FFFFFF">Not Computed Weld Points</font></b></td>				
				</tr>
				<xsl:for-each select="//NotComputedWeld">			
					<tr>
						<td width="30%" align="center"><xsl:value-of select="."/></td>						
					</tr>
				</xsl:for-each>
				</table>
			</BODY>
		</HEAD>
		</HTML>
	</xsl:template>

<xsl:template match="Result">
	<xsl:if test = ". = 'NN'">
		<td width="25%" align="center"><font color="#000080"><b>Not Computed</b></font></td>
		<td width="25%" align="center"><font color="#000080"><b>Not Computed</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'NO'">
		<td width="25%" align="center"><font color="#000080"><b>Not Computed</b></font></td>
		<td width="25%" align="center"><font color="#008000"><b>Accessible</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'NX'">
		<td width="25%" align="center"><font color="#000080"><b>Not Computed</b></font></td>
		<td width="25%" align="center"><font color="#CC0000"><b>Not Accessible</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'ON'">
		<td width="25%" align="center"><font color="#008000"><b>Accessible</b></font></td>
		<td width="25%" align="center"><font color="#000080"><b>Not Computed</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'OO'">
		<td width="25%" align="center"><font color="#008000"><b>Accessible</b></font></td>
		<td width="25%" align="center"><font color="#008000"><b>Accessible</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'OX'">
		<td width="25%" align="center"><font color="#008000"><b>Accessible</b></font></td>
		<td width="25%" align="center"><font color="#CC0000"><b>Not Accessible</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'XN'">
		<td width="25%" align="center"><font color="#CC0000"><b>Not Accessible</b></font></td>
		<td width="25%" align="center"><font color="#000080"><b>Not Computed</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'XO'">
		<td width="25%" align="center"><font color="#CC0000"><b>Not Accessible</b></font></td>
		<td width="25%" align="center"><font color="#008000"><b>Accessible</b></font></td>
	</xsl:if>
	<xsl:if test = ". = 'XX'">
		<td width="25%" align="center"><font color="#CC0000"><b>Not Accessible</b></font></td>
		<td width="25%" align="center"><font color="#CC0000"><b>Not Accessible</b></font></td>
	</xsl:if>
</xsl:template>
</xsl:stylesheet>

