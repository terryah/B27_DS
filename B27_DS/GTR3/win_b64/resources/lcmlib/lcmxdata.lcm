
module LcmxData = 
struct 
  open @@Pervasives

  type AUTLcmxDataPtr = "AUTLcmxDataPtr"

  type AUTLcmxRecordPtr = "AUTLcmxRecordPtr"
  type AUTLcmxTuplePtr = "AUTLcmxTuplePtr"
  type AUTLcmxScalarPtr = "AUTLcmxScalarPtr"
  type AUTLcmxArrayPtr = "AUTLcmxArrayPtr"

  type voidPtr = "voidPtr"
  type int32Ptr = "LCMInt32Ptr"

  extern AUTLCMX_KIND_ARRAY : int32 = "AUTLCMX_KIND_ARRAY"
  extern AUTLCMX_KIND_RECORD : int32 = "AUTLCMX_KIND_RECORD"
  extern AUTLCMX_KIND_TUPLE : int32 = "AUTLCMX_KIND_TUPLE"
  extern AUTLCMX_KIND_SCALAR : int32 = "AUTLCMX_KIND_SCALAR"

  extern AUTLcmxDataPtrCopy : AUTLcmxDataPtr * voidPtr -> pure = "AUTLcmxDataPtrCopy"
  extern AUTLcmxDataPtrCopyArrayData : AUTLcmxDataPtr * voidPtr -> pure = "AUTLcmxDataPtrCopyArrayData"

  extern nullAdr : voidPtr = "nullAdr"
  extern getAdr : voidPtr -> voidPtr = "getAdr"
  extern getData : AUTLcmxDataPtr -> voidPtr = "AUTLcmxGetData"
  extern AUTLcmxScalarData : voidPtr * AUTLcmxScalarPtr -> AUTLcmxDataPtr = "AUTLcmxScalarData"

  extern AUTLcmxCreateDim : int32 -> int32Ptr = "AUTLcmxCreateDim"
  extern AUTLcmxSetDimSize : int32Ptr * int32 * int32 -> int32Ptr = "AUTLcmxSetDimSize"

  extern AUTLcmxCreateArray : int32 * int32Ptr * int32 * AUTLcmxScalarPtr -> AUTLcmxDataPtr = "AUTLcmxCreateArray"
  extern AUTLcmxCreateVector : int32 * int32 * AUTLcmxScalarPtr -> AUTLcmxDataPtr = "AUTLcmxCreateVector"
  extern AUTLcmxCreateMatrix : int32 * int32 * int32 * AUTLcmxScalarPtr -> AUTLcmxDataPtr = "AUTLcmxCreateMatrix"
  extern AUTLcmxCreateTuple : int32 -> AUTLcmxDataPtr = "AUTLcmxCreateTuple"
  extern AUTLcmxCreateRecord : int32 -> AUTLcmxDataPtr = "AUTLcmxCreateRecord"
  extern AUTLcmxDataDeleteOneLevel : AUTLcmxDataPtr -> int32 = "AUTLcmxDataDeleteOneLevel"
  extern AUTLcmxSetRecordFieldName : AUTLcmxDataPtr * string8 * int32 -> AUTLcmxDataPtr = "AUTLcmxSetRecordFieldName"
  extern AUTLcmxCreateScalar : AUTLcmxScalarPtr -> AUTLcmxDataPtr = "AUTLcmxCreateScalar"
  extern AUTLcmxFillArray : AUTLcmxDataPtr * voidPtr -> pure = "AUTLcmxFillArray"
  extern AUTLcmxFillArrayWithClones: AUTLcmxDataPtr * AUTLcmxDataPtr -> AUTLcmxDataPtr = "AUTLcmxFillArrayWithClones"

  extern AUTLcmxSetArrayElem : AUTLcmxDataPtr * voidPtr * int32 * int32Ptr -> pure = "AUTLcmxSetArrayElem"
  extern AUTLcmxSetVectorElem : AUTLcmxDataPtr * voidPtr * int32 -> pure = "AUTLcmxSetVectorElem"
  extern AUTLcmxSetMatrixElem : AUTLcmxDataPtr * voidPtr * int32 * int32 -> pure = "AUTLcmxSetMatrixElem"
  extern AUTLcmxSetTupleField : AUTLcmxDataPtr * AUTLcmxDataPtr * int32 -> AUTLcmxDataPtr = "AUTLcmxSetTupleField"
  extern AUTLcmxSetRecordField : AUTLcmxDataPtr * AUTLcmxDataPtr * string8 -> AUTLcmxDataPtr = "AUTLcmxSetRecordField"

  extern AUTLcmxCopyVectorElem : AUTLcmxDataPtr * voidPtr * int32 -> pure = "AUTLcmxCopyVectorElem"

  extern AUTLcmxGetArrayElem : AUTLcmxDataPtr * int32 * int32Ptr -> voidPtr = "AUTLcmxGetArrayElem"
  extern AUTLcmxGetVectorElem : AUTLcmxDataPtr * int32 -> voidPtr = "AUTLcmxGetVectorElem"
  extern AUTLcmxGetMatrixElem : AUTLcmxDataPtr * int32 * int32 -> voidPtr = "AUTLcmxGetMatrixElem"
  extern AUTLcmxGetTupleField : AUTLcmxDataPtr * int32 -> AUTLcmxDataPtr = "AUTLcmxGetTupleField"
  extern AUTLcmxGetrecordField : AUTLcmxDataPtr * string8 -> AUTLcmxDataPtr = "AUTLcmxGetRecordField"

end
