<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="REPLAY_COMPARISON">
  <html>
    <head>
      <title>Replay results comparison</title>
      <link href="css/TAReportDefaultStyle.css" rel="stylesheet" type="text/css" />
    </head>
    <table id="top">
      <tr>
        <td>
          <div class="logo-3ds"></div>
		  <div class="compass-ct"><div class="compass"></div></div>
        </td>
        <td class="title">
          <span>TACreateImpactFileByODT | </span>
        </td>
        <td class="menuHorizontal">
          <table cellspacing="10" >
            <tr><td><a class="menuHorizontalLinkSelected" href="TAODTComputeReg.xml" >Results</a></td>    </tr>
          </table>
        </td>
        <td class="home"><a id="linkHome" style="display:block;width:100%;height:100%;" href="TAODTComputeReg.xml" /></td>
      </tr>
    </table>
    <body>
      <span class="dataTitle">Replay results comparison for <xsl:value-of select="@currentReplayResult" /></span>
      <span class="dataSubTitle">Report computed on release of <xsl:value-of select="@currentDate" />
      </span>
      <p class="dataGrid">
        <xsl:apply-templates/>
      </p>
    </body>
  </html>
</xsl:template >

<xsl:template match="CONTEXT">
      <table width="100%"><xsl:attribute name="class"><xsl:value-of select="'tableWithoutBorder'" /></xsl:attribute>
        <tr><td width="80%">
            <table>
              <tr><td>Reference : </td><td>
                  <xsl:call-template name="splitString">
                    <xsl:with-param name="input" select="@referenceReplayResult"/>
                  </xsl:call-template>
                </td><td>
                <xsl:call-template name="splitString">
                  <xsl:with-param name="input" select="@referenceDate"/>
                </xsl:call-template>
              </td></tr>
            </table>
          </td>
          <td align="right">
            <table><xsl:attribute name="class"><xsl:value-of select="'tableWithBorder'" /></xsl:attribute>
              <tr class="Regression"><td>Regression</td><td><xsl:value-of select="count(..//ODT[@status = 'Regression'])" /></td></tr>
              <tr class="NotRegKO"><td>KO in both workspaces with different RC</td><td><xsl:value-of select="count(..//ODT[@status = 'KO'])" /></td></tr>
              <tr class="NotReplayedInRef"><td>Not found in reference workspace</td><td><xsl:value-of select="count(..//ODT[@status = 'Not replayed in reference workspace'])" /></td></tr>
            </table>
          </td>
        </tr>
      </table>
</xsl:template>

<xsl:template match="REPLAYED_ODTS">
<br/>
<table>
<tr><xsl:attribute name="class"><xsl:value-of select="'dataGridHeaderStyle'" /></xsl:attribute>
<td>Framework</td><td>Owner</td><td>Folder</td><td>ODT</td><td>Ref RC</td><td>RC</td><td>LastOK date</td></tr>

<xsl:for-each select="FRAMEWORK">
	<xsl:sort select="@name" order="ascending"/>

	<xsl:variable name="fwName" select="@name"/>
  <xsl:variable name="fwOwner" select="@owner"/>

	<xsl:for-each select="ODT">
	
		<xsl:variable name="odtName" select="@name"/>
		<xsl:variable name="folder" select="@folder"/>
		<xsl:variable name="status" select="@status"/>
		<xsl:variable name="returnCode" select="@returnCode"/>
		<xsl:variable name="refReturnCode" select="@returnCodeRef"/>
		<xsl:variable name="trace" select="@output"/>
		<xsl:variable name="traceRef" select="@outputRef"/>
		
		<xsl:variable name="lastOKDate" select="@lastOKDate"/>
		
		<tr>
    <xsl:choose>
        <xsl:when test="(position() mod 2) = 0"><xsl:attribute name="class"><xsl:value-of select="'grise1'" /></xsl:attribute></xsl:when>
        <xsl:otherwise><xsl:attribute name="class"><xsl:value-of select="'grise0'" /></xsl:attribute></xsl:otherwise>
    </xsl:choose>

		<td><xsl:value-of select="$fwName" /></td>
        <td><xsl:value-of select="$fwOwner" /></td>
		<td><xsl:value-of select="$folder" /></td>
		<td><xsl:value-of select="$odtName" /></td>
		
		<xsl:choose>
		<xsl:when test="$status = 'Regression' or $status = 'KO'">
    <td align="right"><a><xsl:attribute name="href"><xsl:value-of select="$traceRef" /></xsl:attribute><xsl:value-of select="$refReturnCode" /></a></td>
		</xsl:when>
		<xsl:otherwise><td></td></xsl:otherwise>
		</xsl:choose>	
		
		<td align="right">
		<xsl:choose>
		<xsl:when test="$status = 'Regression'"><xsl:attribute name="class"><xsl:value-of select="'Regression'" /></xsl:attribute></xsl:when>
		<xsl:when test="$status = 'KO'"><xsl:attribute name="class"><xsl:value-of select="'NotRegKO'" /></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="class"><xsl:value-of select="'NotReplayedInRef'" /></xsl:attribute></xsl:otherwise>
		</xsl:choose>	
		<a><xsl:attribute name="href"><xsl:value-of select="$trace" /></xsl:attribute><xsl:value-of select="$returnCode" /></a></td>
		<td><xsl:value-of select="$lastOKDate" /></td>
		</tr>

	</xsl:for-each>
		
</xsl:for-each>

</table>

</xsl:template>

<xsl:template name="splitString">
    <xsl:param name="input" />
    <xsl:param name="value" select="substring-before($input,';')"/>
    <xsl:param name="next-string" select="substring-after($input,';')"/>

	<xsl:choose>
	<xsl:when test="$value"><xsl:value-of select="$value" /><br/></xsl:when>
	<xsl:otherwise><xsl:value-of select="$input" /></xsl:otherwise>
	</xsl:choose>	
	
    <xsl:if test="$next-string">
        <xsl:call-template name="splitString">
            <xsl:with-param name="input" select="$next-string"/>
        </xsl:call-template>
    </xsl:if>
	
</xsl:template>

</xsl:stylesheet>


