<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" language="JavaScript">

<xsl:template match="PROFILE">
  <xsl:apply-templates select="PROFILENode"/>
</xsl:template>

<xsl:template match="PROFILENode">
  <div onclick="window.event.cancelBubble = true;clickOnEntity(this);" onselectstart="return false" ondragstart="return false">
  <xsl:attribute name="image">images/ProductList.gif</xsl:attribute>
  <xsl:attribute name="imageOpen">images/ProductList.gif</xsl:attribute>
  <xsl:attribute name="open">false</xsl:attribute>
  <xsl:attribute name="id">f<xsl:value-of select="@DisplayName"/></xsl:attribute>
  <xsl:attribute name="open">false</xsl:attribute>
  <xsl:attribute name="STYLE">
    padding-left: 20px;
    cursor: hand;
    <xsl:if expr="depth(this) > 2">
      display: none;
    </xsl:if>
  </xsl:attribute>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="middle">
          <img border="0" id="image">
            <xsl:attribute name="SRC">
              images/ProductList.gif
            </xsl:attribute>
          </img>
        </td>
        <td valign="middle" nowrap="true">
        <xsl:attribute name="STYLE">
          padding-left: 7px;
          font-family: Verdana;
          font-size: 11px;
          font-color: black;
        </xsl:attribute>
        <xsl:value-of select="@DisplayName"/></td>
      </tr>
    </table>
    <xsl:apply-templates select="PROFILEPart"/>
  <xsl:apply-templates select="PROFILENode"/>
  </div>
</xsl:template>

<xsl:template match="PROFILEPart">  
  <div onclick="window.event.cancelBubble = true;clickOnEntity(this);" onselectstart="return false" ondragstart="return false">
  <xsl:attribute name="image">images/Product.gif</xsl:attribute>
  <xsl:attribute name="imageOpen">images/Product.gif</xsl:attribute>
  <xsl:attribute name="open">false</xsl:attribute>
  <xsl:attribute name="id">f<xsl:value-of select="@DisplayName"/></xsl:attribute>
  <xsl:attribute name="open">false</xsl:attribute>
  <xsl:attribute name="STYLE">
    padding-left: 20px;
    cursor: hand;
    <xsl:if expr="depth(this) > 2">
      display: none;
    </xsl:if>
  </xsl:attribute>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="middle">
          <img border="0" id="image">
            <xsl:attribute name="SRC">
              images/Product.gif
            </xsl:attribute>
          </img>
        </td>
        <td valign="middle" nowrap="true">
        <xsl:attribute name="STYLE">
          padding-left: 7px;
          font-family: Verdana;
          font-size: 11px;
          font-color: black;
        </xsl:attribute>
        <xsl:value-of select="@DisplayName"/></td>
      </tr>
    </table>
  </div>
</xsl:template>
</xsl:stylesheet>
