<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <body>
    <table align="center">
      <tr>
        <th colspan="30" align="center">OS</th>
        <th colspan="30" align="center">UserName</th>
        <th colspan="30" align="center">Date</th>
      </tr>
      <xsl:for-each select="IBCData/Info">
      <tr>
        <td width="192" colspan="30" align="center"><xsl:value-of select="OS-Name"/></td>
        <td width="192" colspan="30" align="center"><xsl:value-of select="UserName"/></td>
        <td width="192" colspan="30" align="center"><xsl:value-of select="Date"/></td>
      </tr>
      </xsl:for-each>
    </table>
	<table align="center">
      <tr>
        <th align="center">Interim_Product</th>
        <th align="center">Activity_Name</th>
        <th align="center">Activity_Type</th>
      </tr>
      <xsl:for-each select="IBCData/Info">
      <tr>
        <td width="192" align="center"><xsl:value-of select="Interim_Product"/></td>
        <td width="192" align="center"><xsl:value-of select="Activity_Name"/></td>
        <td width="192" align="center"><xsl:value-of select="Activity_Type"/></td>
      </tr>
      </xsl:for-each>
    </table>
    <h2 align="center">Inverse Bending Curve Data Table</h2>

	<h3 align="center"><font color="#3333FF">Profile Maximum Sag  =  
	<xsl:for-each select="IBCData/MaxSag">
    <td width="192" align="center"><xsl:value-of select="MaximumSag"/></td>
	</xsl:for-each>
	</font></h3>

    <table align="center" border="1" cellspacing="0" cellpadding="4">
      <tr bgcolor="#9acd32">
        <th align="center">--------CurveID--------</th>
        <th align="center">-----MaxSagPointID-----</th>
        <th align="center">-------MaxSag-X--------</th>
        <th align="center">-------MaxSag-Y--------</th>
      </tr>
      <xsl:for-each select="IBCData/MaxSag">
      <tr>
        <td align="center"><xsl:value-of select="CurveID"/></td>
        <td align="center"><xsl:value-of select="MaxSagPointID"/></td>
        <td align="center"><xsl:value-of select="MaxSag-X"/></td>
        <td align="center"><xsl:value-of select="MaxSag-Y"/></td>
      </tr>
      </xsl:for-each>
    </table>
    <table align="center" border="1" cellspacing="0" cellpadding="4">
      <tr bgcolor="#9acd32">
        <th align="center">----------CurveID------</th>
        <th align="center">-----------PointID----------</th>
        <th align="center">---------X-Coord----------</th>
        <th align="center">---------Y-Coord---------</th>
      </tr>
      <xsl:for-each select="IBCData/Row">
      <tr>
        <td align="center"><xsl:value-of select="CurveID"/></td>
        <td align="center"><xsl:value-of select="PointID"/></td>
        <td align="center"><xsl:value-of select="X-Coord"/></td>
        <td align="center"><xsl:value-of select="Y-Coord"/></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>