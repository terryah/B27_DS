Option Explicit

'---------------------------------------------------------------------------
'  This Script is supposed to do the import of an XML file and populate the
'  database.
'
'     Script Requires: (none)
'
'     Author: Dassault-Systemes
'
'     Notes: (none)
'
' (C) Copyright Dassault-Systemes 2004   All Rights Reserved
'---------------------------------------------------------------------------
'  THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED
'  WARRANTY.  ALL IMPLIED WARRANTIES OF FITNESS FOR ANY PARTICULAR
'  PURPOSE AND OF MERCHANTABILITY ARE HEREBY DISCLAIMED.
'---------------------------------------------------------------------------

Function ImportSpaceReservationData(ApplHndl As Long,Sstr As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long) As Integer
	On Error GoTo AssignErrorCode

	Dim CATIA As Object

	Dim XMLFilename As String
	XMLFilename = InputBox("Please give the Space Reservation data filename to import:", "XML filename", "e:\tmp\ExportSR.xml")
	If XMLFilename <> "" Then

		Dim simulation As Integer
		simulation = MsgBox("Do you want to simulate the import process ?", 3)
		If simulation <> 2 Then

			Set CATIA = CreateObject("CATIA.Application")
			CATIA.RefreshDisplay = True

			Dim CATSpaceReservation As Object
			Set CATSpaceReservation = CATIA.GetItem("CATSpaceReservationExchange")
			If(IsEmpty(CATSpaceReservation) = False) Then

				Dim SmarTeamSession As Object
				Set SmarTeamSession = SCREXT_ObjectForInterface(ApplHndl)

				If simulation = 6 Then
					CATSpaceReservation.ImportSpaceReservationDataInSmarTeam XMLFilename, SmarTeamSession, 1
				Else
					CATSpaceReservation.ImportSpaceReservationDataInSmarTeam XMLFilename, SmarTeamSession, 0
				End If

				MsgBox "Space reservation data import done", 64
			End If

			If CATIA.Visible = False Then
				CATIA.Quit
			End If
		End If
	End If

   	ImportSpaceReservationData = Err_None
    Exit Function

  AssignErrorCode : 
	If CATIA.Visible = False Then
		CATIA.Quit
	End If

	MsgBox "Following Error has occured : " + Err.Description, 16
    ImportSpaceReservationData = Err_Gen  
End Function
