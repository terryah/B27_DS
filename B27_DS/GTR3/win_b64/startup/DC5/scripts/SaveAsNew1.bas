Attribute VB_Name = "SaveAsNew1"
Sub CATMain()

'Get V6 Engine
Set V6Engine = CATIA.GetItem("V6Engine")

    'Check if connected to server
If Not V6Engine Is Nothing Then
    If V6Engine.IsConnected() = "FALSE" Then
        MsgBox "Not connected to server. Please connect and try again"
        Exit Sub
    End If
Else
    Exit Sub
End If

Dim CD5_Save_Scope_CD5_ActiveDocument As Integer
CD5_Save_Scope_CD5_ActiveDocument = 0
Dim SaveOperation As V6SaveOperation

If Not V6Engine Is Nothing Then
    Set SaveOperation = V6Engine.CreateSaveOperation(CD5_Save_Scope_CD5_ActiveDocument)
    If SaveOperation Is Nothing Then
        MsgBox "No active document present"
        Exit Sub
    End If
Else
    Exit Sub
End If

Dim SaveItems As V6SaveItems
If Not SaveOperation Is Nothing Then
    Set SaveItems = SaveOperation.Items
    If SaveItems Is Nothing Then
        MsgBox "No Save Items present"
        Exit Sub
    End If
Else
    Exit Sub
End If

Dim SaveItem As V6SaveItem
NbItems = SaveItems.Count

For i = 1 To NbItems
    Set SaveItem = SaveItems.Item(i)
    If SaveItem Is Nothing Then
        MsgBox "SaveItem Error"
        Exit Sub
    Else
        Dim Properties As V6Properties
        Set Properties = SaveItem.Properties
        If Properties Is Nothing Then
            MsgBox "No Properties present"
            Exit Sub
        End If
    End If
    
    If Properties.Count > 0 Then
        For NewCount = 1 To Properties.Count
            If Properties.Item(NewCount).PropertyName = "Owner" Then
                ItemOwner = Properties.Item(NewCount).PropertyValue
            End If
            If Properties.Item(NewCount).PropertyName = "State" Then
                ItemState = Properties.Item(NewCount).PropertyValue
            End If
            Next
    Else
        Exit Sub
    End If

    If Not ItemOwner = V6Engine.CurrentUser And ItemState = "Design Frozen" Then
        SaveItem.SetSaveAsNew (True)
    End If
    
Next

If Not SaveOperation Is Nothing Then
SaveOperation.ShowPanel
End If
End Sub

