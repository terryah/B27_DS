@echo off
setlocal

rem # ------------------------------------------------------------
rem # Run this script using command line arguments to avoid having
rem # to edit CCWAY_PATH/CCWWriteps.defs
rem # ------------------------------------------------------------

rem # ------------------------------------------------------------
rem # ENVIRONMENT VARIABLES  Set these to match your installation.
rem # CATIA_REL_SDIR is the subdirectory that specifies the
rem #                the CATIA release
rem # PATH_TO_CATIA  is the path to where CATIA is installed.
rem #                CATIA_REL_SDIR will be appended to this.
rem # CATIA_ENV      is the name that was assigned to CCWAY when
rem #                CCWAY was defined to CATIA in the CATIA
rem #                Environment Editor when CCWAY was installed.
rem # ------------------------------------------------------------
set CATIA_REL_SDIR=B21
set PATH_TO_CATIA=c:\Program Files\Dassault Systemes\%CATIA_REL_SDIR%
set CATIA_ENV=CCWAY-2.21

rem # ------------------------------------------------------------
rem # You can override the default setting of the CCWAY environment
rem # variables by setting them in this script.                       
rem # CCWAY_PATH     is the path to where CCWAY looks for its .defs
rem #                and message files.
rem # CCWAY_TEMP     is where CCWAY writes its log files and any
rem #                temporary files used during a translation.
rem # ------------------------------------------------------------
rem set CCWAY_PATH=C:\home\dan\ccwayv2
rem set CCWAY_TEMP=C:\home\dan\ccwayv2\tmp                            

rem # ------------------------------------------------------------
rem * No need to modify anything after this point.
rem # ------------------------------------------------------------
set MKMK_PROGNAME=CCWWriteps.exe

echo PATH_TO_CATIA=%PATH_TO_CATIA%
echo MKMK_PROGNAME=%MKMK_PROGNAME%
echo CATIA_ENV=%CATIA_ENV%

rem # ------------------------------------------------------------
rem # Set OS_DIR to the CATIA operating system naming convention
rem # for the platform CCWAY is installed on
rem # ------------------------------------------------------------

if (%PROCESSOR_ARCHITECTURE%) == (x86) (
  set OS_SDIR=intel_a
)
if (%PROCESSOR_ARCHITECTURE%) == (AMD64) (
  set OS_SDIR=win_b64
)

set path="%PATH_TO_CATIA%\%OS_SDIR%\code\bin";%PATH%
echo ** Starting %MKMK_PROGNAME% with CATIA environment %CATIA_ENV% **
catstart -env "%CATIA_ENV%" -run "%MKMK_PROGNAME% %*"
set RC=%ERRORLEVEL%
echo -- Program finished with return code %RC%.

REM Let the user close the dialog when they are done reading any messages.
pause
