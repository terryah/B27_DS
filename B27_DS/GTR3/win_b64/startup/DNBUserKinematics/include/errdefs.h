
/*
 * Shared Library     Error Definitions
 */

#define ER_OKAY			0
#define ER_BADHIT		11
#define ER_BADPOPNM		16
#define ER_ABORT		17
#define ER_FAILURE		27
#define ER_COLLISION		32
#define ER_BAD_CONFIG		34
#define ER_IO_CONNECTION        48
#define ER_BAD_NAME		59
#define ER_NOT_SUB_PROG		74  /** no such sub_prog active in prog **/
#define ER_NO_PROG		75  /** no proper prog loaded for dev **/
#define ER_NOT_VAR		93  /** no such variable in prog/sub-prog **/
#define ER_PART_NOT_FOUND	100
#define ER_PATH_NOT_FOUND	103
#define ER_TAG_NOT_FOUND	105
#define ER_PATH_EXIST		109
#define ER_TAG_EXIST		110
#define ER_BAD_INDEX		111
#define ER_BAD_XFORM		112
#define ER_BAD_VALUE		113
#define ER_ERANGE               362
#define ER_AUX_TYPE		450
#define ER_AUX_NAME		451
#define ER_AUX_LEN		452
#define ER_MAX_VPOP_ITEMS	551

