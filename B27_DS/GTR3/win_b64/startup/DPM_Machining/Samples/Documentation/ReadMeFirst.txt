
 ------------------------------------------------------------------
|==================================================================|
|Generation of documentation with drawings for machining operations|
|								   |
|******************************************************************|
 ------------------------------------------------------------------
ReadMe file created by VZR - Dassault Systemes - oct 2007

' V5 Version:    V5R18 sp2


1 - Launch DELMIA V5

2 - Go to the Workbench : Digital Process For Manufacturing - Machining Process Planner

3 - Open the Process document 

4 - Verify links to drawings in machining operations and part operations
	(the macro works if there are drawings associated with machining operations or part operations) 

5 - Go to "Tools" menu, then select "Macro" and "Macros..." (Alt+F8)

6 - In Macro dialogbox, click on "Macro Libraries" 

7 - In Macro Libraries window, select "VBA projects" in Library type

8 - Then click on "Add existing library..."

9 - Choose "ProcessDocumentation.catvba" in Delmia installation path &"startup\DPM_Machining\Samples\documentation" directory
	 and open it

10- Close "Macros Libraries" window

11- In Macro dialogbox, click on Run ("ProcessDocumentation" is selected in Available macros box)

12- A message box window appears "New documentation created : C:\Documentation\P_MM_DD_YYYY_hh_mm_ss_ProcessName.doc" Click OK
	=> the directory "c:\documentation" is used by default, it can be changed in the macro "ProcessDocumentation.catvba"

13- Open the created Word document to verify layout and included information (choose landscape position as default in properties)




NB : The macro can be launched directly if "ExtractInfosFromXMLforDocumentation.dot" and "SheetForProcessDocumentation.doc" files 
     are in their default installation path

     If the macro doesn't find ExtractInfosFromXMLforDocumentation.dot file, a file selection box appears
	=> Choose the right directory where the file is located,
	=> Make sure that "SheetForProcessDocumentation.doc" and "ExtractInfosFromXMLforDocumentation.dot" files
	   are in the same folder

    Drawing captures needed for process documentation are stored in the same path as drawings. 

