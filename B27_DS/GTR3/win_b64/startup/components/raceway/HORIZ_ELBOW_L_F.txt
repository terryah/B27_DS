CATRouPartNumber	CATRouSize	CATRouOutsideHeight	CATRouOutsideWidth	CATRouAngle	CATRouTurnRadius	InsideWidth	LoadingDepth	TangentLength	RungSpacing	RungHeight	RungLength	RailFlangeWidth	RailFlangeThickness	RailWebThickness
B34AL-06-30HB12	B34x6"	4.2in	7.5in	30deg	12in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-30HB12	B34x9"	4.2in	10.5in	30deg	12in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-30HB12	B34x12"	4.2in	13.5in	30deg	12in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-30HB12	B34x18"	4.2in	19.5in	30deg	12in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-30HB12	B34x24"	4.2in	25.5in	30deg	12in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-30HB12	B34x30"	4.2in	31.5in	30deg	12in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-30HB12	B34x36"	4.2in	37.5in	30deg	12in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-30HB12	B34x42"	4.2in	43.5in	30deg	12in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-06-30HB24	B34x6"	4.2in	7.5in	30deg	24in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-30HB24	B34x9"	4.2in	10.5in	30deg	24in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-30HB24	B34x12"	4.2in	13.5in	30deg	24in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-30HB24	B34x18"	4.2in	19.5in	30deg	24in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-30HB24	B34x24"	4.2in	25.5in	30deg	24in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-30HB24	B34x30"	4.2in	31.5in	30deg	24in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-30HB24	B34x36"	4.2in	37.5in	30deg	24in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-30HB24	B34x42"	4.2in	43.5in	30deg	24in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-06-45HB12	B34x6"	4.2in	7.5in	45deg	12in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-45HB12	B34x9"	4.2in	10.5in	45deg	12in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-45HB12	B34x12"	4.2in	13.5in	45deg	12in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-45HB12	B34x18"	4.2in	19.5in	45deg	12in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-45HB12	B34x24"	4.2in	25.5in	45deg	12in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-45HB12	B34x30"	4.2in	31.5in	45deg	12in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-45HB12	B34x36"	4.2in	37.5in	45deg	12in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-45HB12	B34x42"	4.2in	43.5in	45deg	12in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-06-45HB24	B34x6"	4.2in	7.5in	45deg	24in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-45HB24	B34x9"	4.2in	10.5in	45deg	24in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-45HB24	B34x12"	4.2in	13.5in	45deg	24in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-45HB24	B34x18"	4.2in	19.5in	45deg	24in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-45HB24	B34x24"	4.2in	25.5in	45deg	24in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-45HB24	B34x30"	4.2in	31.5in	45deg	24in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-45HB24	B34x36"	4.2in	37.5in	45deg	24in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-45HB24	B34x42"	4.2in	43.5in	45deg	24in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-06-60HB12	B34x6"	4.2in	7.5in	60deg	12in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-60HB12	B34x9"	4.2in	10.5in	60deg	12in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-60HB12	B34x12"	4.2in	13.5in	60deg	12in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-60HB12	B34x18"	4.2in	19.5in	60deg	12in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-60HB12	B34x24"	4.2in	25.5in	60deg	12in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-60HB12	B34x30"	4.2in	31.5in	60deg	12in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-60HB12	B34x36"	4.2in	37.5in	60deg	12in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-60HB12	B34x42"	4.2in	43.5in	60deg	12in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-06-60HB24	B34x6"	4.2in	7.5in	60deg	24in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-60HB24	B34x9"	4.2in	10.5in	60deg	24in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-60HB24	B34x12"	4.2in	13.5in	60deg	24in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-60HB24	B34x18"	4.2in	19.5in	60deg	24in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-60HB24	B34x24"	4.2in	25.5in	60deg	24in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-60HB24	B34x30"	4.2in	31.5in	60deg	24in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-60HB24	B34x36"	4.2in	37.5in	60deg	24in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-60HB24	B34x42"	4.2in	43.5in	60deg	24in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-06-90HB12	B34x6"	4.2in	7.5in	90deg	12in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-90HB12	B34x9"	4.2in	10.5in	90deg	12in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-90HB12	B34x12"	4.2in	13.5in	90deg	12in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-90HB12	B34x18"	4.2in	19.5in	90deg	12in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-90HB12	B34x24"	4.2in	25.5in	90deg	12in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-90HB12	B34x30"	4.2in	31.5in	90deg	12in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-90HB12	B34x36"	4.2in	37.5in	90deg	12in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-90HB12	B34x42"	4.2in	43.5in	90deg	12in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-06-90HB24	B34x6"	4.2in	7.5in	90deg	24in	6in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-09-90HB24	B34x9"	4.2in	10.5in	90deg	24in	9in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-12-90HB24	B34x12"	4.2in	13.5in	90deg	24in	12in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-18-90HB24	B34x18"	4.2in	19.5in	90deg	24in	18in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-24-90HB24	B34x24"	4.2in	25.5in	90deg	24in	24in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-30-90HB24	B34x30"	4.2in	31.5in	90deg	24in	30in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-36-90HB24	B34x36"	4.2in	37.5in	90deg	24in	36in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B34AL-42-90HB24	B34x42"	4.2in	43.5in	90deg	24in	42in	3.08in	3in	9in	1in	1in	1.75in	.12in	.1in
B35AL-06-30HB12	B35x6"	5.06in	7.49in	30deg	12in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-30HB12	B35x9"	5.06in	10.49in	30deg	12in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-30HB12	B35x12"	5.06in	13.49in	30deg	12in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-30HB12	B35x18"	5.06in	19.49in	30deg	12in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-30HB12	B35x24"	5.06in	25.49in	30deg	12in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-30HB12	B35x30"	5.06in	31.49in	30deg	12in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-30HB12	B35x36"	5.06in	37.49in	30deg	12in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-30HB12	B35x42"	5.06in	43.49in	30deg	12in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-06-30HB24	B35x6"	5.06in	7.49in	30deg	24in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-30HB24	B35x9"	5.06in	10.49in	30deg	24in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-30HB24	B35x12"	5.06in	13.49in	30deg	24in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-30HB24	B35x18"	5.06in	19.49in	30deg	24in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-30HB24	B35x24"	5.06in	25.49in	30deg	24in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-30HB24	B35x30"	5.06in	31.49in	30deg	24in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-30HB24	B35x36"	5.06in	37.49in	30deg	24in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-30HB24	B35x42"	5.06in	43.49in	30deg	24in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-06-45HB12	B35x6"	5.06in	7.49in	45deg	12in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-45HB12	B35x9"	5.06in	10.49in	45deg	12in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-45HB12	B35x12"	5.06in	13.49in	45deg	12in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-45HB12	B35x18"	5.06in	19.49in	45deg	12in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-45HB12	B35x24"	5.06in	25.49in	45deg	12in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-45HB12	B35x30"	5.06in	31.49in	45deg	12in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-45HB12	B35x36"	5.06in	37.49in	45deg	12in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-45HB12	B35x42"	5.06in	43.49in	45deg	12in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-06-45HB24	B35x6"	5.06in	7.49in	45deg	24in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-45HB24	B35x9"	5.06in	10.49in	45deg	24in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-45HB24	B35x12"	5.06in	13.49in	45deg	24in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-45HB24	B35x18"	5.06in	19.49in	45deg	24in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-45HB24	B35x24"	5.06in	25.49in	45deg	24in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-45HB24	B35x30"	5.06in	31.49in	45deg	24in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-45HB24	B35x36"	5.06in	37.49in	45deg	24in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-45HB24	B35x42"	5.06in	43.49in	45deg	24in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-06-60HB12	B35x6"	5.06in	7.49in	60deg	12in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-60HB12	B35x9"	5.06in	10.49in	60deg	12in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-60HB12	B35x12"	5.06in	13.49in	60deg	12in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-60HB12	B35x18"	5.06in	19.49in	60deg	12in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-60HB12	B35x24"	5.06in	25.49in	60deg	12in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-60HB12	B35x30"	5.06in	31.49in	60deg	12in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-60HB12	B35x36"	5.06in	37.49in	60deg	12in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-60HB12	B35x42"	5.06in	43.49in	60deg	12in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-06-60HB24	B35x6"	5.06in	7.49in	60deg	24in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-60HB24	B35x9"	5.06in	10.49in	60deg	24in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-60HB24	B35x12"	5.06in	13.49in	60deg	24in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-60HB24	B35x18"	5.06in	19.49in	60deg	24in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-60HB24	B35x24"	5.06in	25.49in	60deg	24in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-60HB24	B35x30"	5.06in	31.49in	60deg	24in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-60HB24	B35x36"	5.06in	37.49in	60deg	24in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-60HB24	B35x42"	5.06in	43.49in	60deg	24in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-06-90HB12	B35x6"	5.06in	7.49in	90deg	12in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-90HB12	B35x9"	5.06in	10.49in	90deg	12in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-90HB12	B35x12"	5.06in	13.49in	90deg	12in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-90HB12	B35x18"	5.06in	19.49in	90deg	12in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-90HB12	B35x24"	5.06in	25.49in	90deg	12in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-90HB12	B35x30"	5.06in	31.49in	90deg	12in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-90HB12	B35x36"	5.06in	37.49in	90deg	12in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-90HB12	B35x42"	5.06in	43.49in	90deg	12in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-06-90HB24	B35x6"	5.06in	7.49in	90deg	24in	6in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-09-90HB24	B35x9"	5.06in	10.49in	90deg	24in	9in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-12-90HB24	B35x12"	5.06in	13.49in	90deg	24in	12in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-18-90HB24	B35x18"	5.06in	19.49in	90deg	24in	18in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-24-90HB24	B35x24"	5.06in	25.49in	90deg	24in	24in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-30-90HB24	B35x30"	5.06in	31.49in	90deg	24in	30in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-36-90HB24	B35x36"	5.06in	37.49in	90deg	24in	36in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B35AL-42-90HB24	B35x42"	5.06in	43.49in	90deg	24in	42in	3.96in	3in	9in	1in	1in	1.75in	.1in	.09in
B36AL-06-30HB12	B36x6"	6.17in	7.45in	30deg	12in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-30HB12	B36x9"	6.17in	10.45in	30deg	12in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-30HB12	B36x12"	6.17in	13.45in	30deg	12in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-30HB12	B36x18"	6.17in	19.45in	30deg	12in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-30HB12	B36x24"	6.17in	25.45in	30deg	12in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-30HB12	B36x30"	6.17in	31.45in	30deg	12in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-30HB12	B36x36"	6.17in	37.45in	30deg	12in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-30HB12	B36x42"	6.17in	43.45in	30deg	12in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-06-30HB24	B36x6"	6.17in	7.45in	30deg	24in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-30HB24	B36x9"	6.17in	10.45in	30deg	24in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-30HB24	B36x12"	6.17in	13.45in	30deg	24in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-30HB24	B36x18"	6.17in	19.45in	30deg	24in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-30HB24	B36x24"	6.17in	25.45in	30deg	24in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-30HB24	B36x30"	6.17in	31.45in	30deg	24in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-30HB24	B36x36"	6.17in	37.45in	30deg	24in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-30HB24	B36x42"	6.17in	43.45in	30deg	24in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-06-45HB12	B36x6"	6.17in	7.45in	45deg	12in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-45HB12	B36x9"	6.17in	10.45in	45deg	12in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-45HB12	B36x12"	6.17in	13.45in	45deg	12in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-45HB12	B36x18"	6.17in	19.45in	45deg	12in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-45HB12	B36x24"	6.17in	25.45in	45deg	12in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-45HB12	B36x30"	6.17in	31.45in	45deg	12in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-45HB12	B36x36"	6.17in	37.45in	45deg	12in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-45HB12	B36x42"	6.17in	43.45in	45deg	12in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-06-45HB24	B36x6"	6.17in	7.45in	45deg	24in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-45HB24	B36x9"	6.17in	10.45in	45deg	24in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-45HB24	B36x12"	6.17in	13.45in	45deg	24in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-45HB24	B36x18"	6.17in	19.45in	45deg	24in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-45HB24	B36x24"	6.17in	25.45in	45deg	24in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-45HB24	B36x30"	6.17in	31.45in	45deg	24in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-45HB24	B36x36"	6.17in	37.45in	45deg	24in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-45HB24	B36x42"	6.17in	43.45in	45deg	24in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-06-60HB12	B36x6"	6.17in	7.45in	60deg	12in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-60HB12	B36x9"	6.17in	10.45in	60deg	12in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-60HB12	B36x12"	6.17in	13.45in	60deg	12in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-60HB12	B36x18"	6.17in	19.45in	60deg	12in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-60HB12	B36x24"	6.17in	25.45in	60deg	12in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-60HB12	B36x30"	6.17in	31.45in	60deg	12in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-60HB12	B36x36"	6.17in	37.45in	60deg	12in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-60HB12	B36x42"	6.17in	43.45in	60deg	12in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-06-60HB24	B36x6"	6.17in	7.45in	60deg	24in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-60HB24	B36x9"	6.17in	10.45in	60deg	24in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-60HB24	B36x12"	6.17in	13.45in	60deg	24in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-60HB24	B36x18"	6.17in	19.45in	60deg	24in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-60HB24	B36x24"	6.17in	25.45in	60deg	24in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-60HB24	B36x30"	6.17in	31.45in	60deg	24in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-60HB24	B36x36"	6.17in	37.45in	60deg	24in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-60HB24	B36x42"	6.17in	43.45in	60deg	24in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-06-90HB12	B36x6"	6.17in	7.45in	90deg	12in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-90HB12	B36x9"	6.17in	10.45in	90deg	12in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-90HB12	B36x12"	6.17in	13.45in	90deg	12in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-90HB12	B36x18"	6.17in	19.45in	90deg	12in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-90HB12	B36x24"	6.17in	25.45in	90deg	12in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-90HB12	B36x30"	6.17in	31.45in	90deg	12in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-90HB12	B36x36"	6.17in	37.45in	90deg	12in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-90HB12	B36x42"	6.17in	43.45in	90deg	12in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-06-90HB24	B36x6"	6.17in	7.45in	90deg	24in	6in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-09-90HB24	B36x9"	6.17in	10.45in	90deg	24in	9in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-12-90HB24	B36x12"	6.17in	13.45in	90deg	24in	12in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-18-90HB24	B36x18"	6.17in	19.45in	90deg	24in	18in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-24-90HB24	B36x24"	6.17in	25.45in	90deg	24in	24in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-30-90HB24	B36x30"	6.17in	31.45in	90deg	24in	30in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-36-90HB24	B36x36"	6.17in	37.45in	90deg	24in	36in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B36AL-42-90HB24	B36x42"	6.17in	43.45in	90deg	24in	42in	5.06in	3in	9in	1in	1in	2in	.11in	.075in
B37AL-06-30HB12	B37x6"	7.14in	7.5in	30deg	12in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-30HB12	B37x9"	7.14in	10.5in	30deg	12in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-30HB12	B37x12"	7.14in	13.5in	30deg	12in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-30HB12	B37x18"	7.14in	19.5in	30deg	12in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-30HB12	B37x24"	7.14in	25.5in	30deg	12in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-30HB12	B37x30"	7.14in	31.5in	30deg	12in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-30HB12	B37x36"	7.14in	37.5in	30deg	12in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-30HB12	B37x42"	7.14in	43.5in	30deg	12in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-06-30HB24	B37x6"	7.14in	7.5in	30deg	24in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-30HB24	B37x9"	7.14in	10.5in	30deg	24in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-30HB24	B37x12"	7.14in	13.5in	30deg	24in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-30HB24	B37x18"	7.14in	19.5in	30deg	24in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-30HB24	B37x24"	7.14in	25.5in	30deg	24in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-30HB24	B37x30"	7.14in	31.5in	30deg	24in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-30HB24	B37x36"	7.14in	37.5in	30deg	24in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-30HB24	B37x42"	7.14in	43.5in	30deg	24in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-06-45HB12	B37x6"	7.14in	7.5in	45deg	12in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-45HB12	B37x9"	7.14in	10.5in	45deg	12in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-45HB12	B37x12"	7.14in	13.5in	45deg	12in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-45HB12	B37x18"	7.14in	19.5in	45deg	12in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-45HB12	B37x24"	7.14in	25.5in	45deg	12in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-45HB12	B37x30"	7.14in	31.5in	45deg	12in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-45HB12	B37x36"	7.14in	37.5in	45deg	12in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-45HB12	B37x42"	7.14in	43.5in	45deg	12in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-06-45HB24	B37x6"	7.14in	7.5in	45deg	24in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-45HB24	B37x9"	7.14in	10.5in	45deg	24in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-45HB24	B37x12"	7.14in	13.5in	45deg	24in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-45HB24	B37x18"	7.14in	19.5in	45deg	24in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-45HB24	B37x24"	7.14in	25.5in	45deg	24in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-45HB24	B37x30"	7.14in	31.5in	45deg	24in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-45HB24	B37x36"	7.14in	37.5in	45deg	24in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-45HB24	B37x42"	7.14in	43.5in	45deg	24in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-06-60HB12	B37x6"	7.14in	7.5in	60deg	12in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-60HB12	B37x9"	7.14in	10.5in	60deg	12in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-60HB12	B37x12"	7.14in	13.5in	60deg	12in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-60HB12	B37x18"	7.14in	19.5in	60deg	12in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-60HB12	B37x24"	7.14in	25.5in	60deg	12in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-60HB12	B37x30"	7.14in	31.5in	60deg	12in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-60HB12	B37x36"	7.14in	37.5in	60deg	12in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-60HB12	B37x42"	7.14in	43.5in	60deg	12in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-06-60HB24	B37x6"	7.14in	7.5in	60deg	24in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-60HB24	B37x9"	7.14in	10.5in	60deg	24in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-60HB24	B37x12"	7.14in	13.5in	60deg	24in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-60HB24	B37x18"	7.14in	19.5in	60deg	24in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-60HB24	B37x24"	7.14in	25.5in	60deg	24in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-60HB24	B37x30"	7.14in	31.5in	60deg	24in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-60HB24	B37x36"	7.14in	37.5in	60deg	24in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-60HB24	B37x42"	7.14in	43.5in	60deg	24in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-06-90HB12	B37x6"	7.14in	7.5in	90deg	12in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-90HB12	B37x9"	7.14in	10.5in	90deg	12in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-90HB12	B37x12"	7.14in	13.5in	90deg	12in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-90HB12	B37x18"	7.14in	19.5in	90deg	12in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-90HB12	B37x24"	7.14in	25.5in	90deg	12in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-90HB12	B37x30"	7.14in	31.5in	90deg	12in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-90HB12	B37x36"	7.14in	37.5in	90deg	12in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-90HB12	B37x42"	7.14in	43.5in	90deg	12in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-06-90HB24	B37x6"	7.14in	7.5in	90deg	24in	6in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-09-90HB24	B37x9"	7.14in	10.5in	90deg	24in	9in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-12-90HB24	B37x12"	7.14in	13.5in	90deg	24in	12in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-18-90HB24	B37x18"	7.14in	19.5in	90deg	24in	18in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-24-90HB24	B37x24"	7.14in	25.5in	90deg	24in	24in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-30-90HB24	B37x30"	7.14in	31.5in	90deg	24in	30in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-36-90HB24	B37x36"	7.14in	37.5in	90deg	24in	36in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
B37AL-42-90HB24	B37x42"	7.14in	43.5in	90deg	24in	42in	6.05in	3in	9in	1in	1in	2in	.09in	.075in
