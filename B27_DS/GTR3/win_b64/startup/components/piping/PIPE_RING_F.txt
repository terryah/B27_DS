CATRouPartNumber	CATRouSize	CATRouOutsideDiameter	RodTakeOut	BoltSize	PipeRingGap	PipeRingWidth	PipeRingThickness	BoltHeadDiameter	BoltHeadHeight
PIPE-RING-00.375in	3/8"	0.375in	0.75in	0.19in	0.75in	0.5in	0.125in	0.359in	0.137in
PIPE-RING-00.50in	1/2"	0.5in	0.9375in	0.25in	0.75in	0.5in	0.125in	0.505in	0.21875in
PIPE-RING-00.75in	3/4"	0.75in	1.125in	0.25in	0.75in	0.5in	0.125in	0.505in	0.21875in
PIPE-RING-01.00in	1"	1.0in	1.25in	0.25in	0.75in	0.5in	0.125in	0.505in	0.21875in
PIPE-RING-01.25in	1 1/4"	1.25in	1.5625in	0.25in	0.75in	0.5in	0.125in	0.505in	0.21875in
PIPE-RING-01.50in	1 1/2"	1.5in	1.6875in	0.25in	0.75in	0.5in	0.125in	0.505in	0.21875in
PIPE-RING-02.00in	2"	2.0in	2.0625in	0.25in	0.75in	0.625in	0.25in	0.505in	0.21875in
PIPE-RING-02.50in	2 1/ 2"	2.5in	2.25in	0.25in	0.75in	0.625in	0.25in	0.505in	0.21875in
PIPE-RING-03.00in	3"	3.0in	2.75in	0.25in	0.75in	0.625in	0.25in	0.505in	0.21875in
PIPE-RING-03.50in	3 1/2"	3.5in	3.125in	0.25in	0.75in	0.625in	0.25in	0.505in	0.21875in
PIPE-RING-04.00in	4"	4.0in	3.625in	0.375in	0.75in	0.75in	0.25in	0.650in	0.328125in
PIPE-RING-05.00in	5"	5.0in	4.5in	0.375in	0.75in	0.75in	0.25in	0.650in	0.328125in
PIPE-RING-06.00in	6"	6.0in	5.4375in	0.5in	1.0in	0.875in	0.375in	0.866in	0.4375in
PIPE-RING-08.00in	8"	8.0in	6.375in	0.5in	1.0in	0.875in	0.375in	0.866in	0.4375in
