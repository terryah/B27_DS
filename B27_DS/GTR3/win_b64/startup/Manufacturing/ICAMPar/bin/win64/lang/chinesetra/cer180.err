!   CAM-POST Version 16.0: Error Specification File
!   (c) copyright 2005,...,2010
!   ICAM Technologies Corporation
!
!	This file is a text oriented file which can be modified with an editor.
!	It contains all error messages recognized by the Controller Emulator.
!	Modifications to this file are allowed. To make the modifications known
!	to the Controller Emulator, the following steps are necessary.
!
!	1. Copy the original text file to a safe place and rename it if you like.
!	2. Modify the errors inside that new text file as you wish.
!	3. Set "erfile" in your default file to that new modified error text file.
!
!	Within this file any line starting with an "!" is a comment. Lines
!	starting with a "\" define an error message. Error message definitions
!	can be continued from one line to the next. Extra spacing characters
!	will be ignored. An error message consists of three fields, separated
!	by blanks or tabs:
!
!	1. Error number
!	2. Severity
!	3. Message
!
!	The error number field should not be modified as only these listed
!	numbers are known to the Controller Emulator. The severity field specifies
!	the severity of the error message. A negative severity disables the output
!	of that particular error message. The message field provides the text 
!	associated with the error. There is no length limitation on the message length.
!
!
! Error messages that that are shared by CAM-POST and Control Emulator
!
\ 01353002 8	Missing matrix information. Command rejected.
\ 01353003 8	Matrix cannot be inverted. This can be caused by specifying
		"0,0,0" for a rotation component, or by using the same rotation
		component for 2 or more axes. Command rejected.
\ 01353005 8	Origin rotation angle missing. Command rejected.
\ 01317003 8	$CPPANM or $CPPADS macro variable index out of range.
\ 01312022 8	!(*) macro variable index exceeds the maximum allowed
		(!(*),!(*)).
\ 01312023 8	!(*) macro variable index must be a positive, nonzero number.
!
! Simulation and collision detection
!
\ 01409008 8	Simulation reported collision between "!(*)" and "!(*)".
\ 01409009 8	Simulation reported overtravel: "!(*)".
\ 01409010 8	Simulation is not available. Syntax checking performed.
		Command ignored.
\ 01409016 8	Tool "!(*)" rapids into in-process stock "!(*)".
\ 01409017 8	Tool "!(*)" is not turning while cutting in-process stock "!(*)".
\ 01409018 4	Tool "!(*)" is turning in the reverse direction while cutting in-process stock "!(*)".
\ 01409019 8	Simulation reported collision between "!(*)" and in-process stock "!(*)".
!
! Debugger
!
\ 01512009 8	Cannot find database variable.
\ 01512010 8	Database variable is not an array.
\ 01512011 8	Database variable needs a subscript.
\ 01512012 8	Database variable improperly defined.
\ 01512013 8	Database variable subscript out of range.
\ 01512019 8	Cannot find tooling table variable.

!
! Controller Emulator (Main parser messages)
!
\ 02001001 8	The register !(A) is not used and will be discarded
\ 02001002 4	!(A) replaces !(A) in !(A) at line !(*)
\ 02001003 8	!(A) unused in !(A) at line !(*)
\ 02001004 4	'!(A)' not recognized in '!(A)' at line !(*)
\ 02001005 4	!(A) not recognized in '!(A)' at line !(*)
\ 02001006 4	!(A) value !(*) is out of range ( Min: !(*) Max: !(*) )
\ 02001007 4	Modal DATA !(A) value !(*) replaces unused last value !(*) from line !(*)
\ 02001008 8	Non-modal DATA identifier !(A) with value !(*) unused at line !(*)
!
! Controller Emulator (Cycles error messages)
!
\ 02002001 8	Cycle type not defined before activator code encountered
\ 02002002 8	Cycle axis depth register is missing
\ 02002003 8	Both !(A) and !(A) axes coded on cycle block, can not determine the cycle axis
\ 02002004 8	Cycle axis register can not be determined or is missing
\ 02002005 8	Cycle tap feed register is missing
\ 02002006 8	Cycle uPM feed register is missing
\ 02002007 8	Cycle uPR feed register is missing
\ 02002008 8	Cycle uPM or uPR feed register is missing
\ 02002009 8	Cycle depth register is missing
\ 02002010 8	Cycle clearance register is missing
\ 02002011 8	Cycle retract register is missing
\ 02002012 8	Cycle step register is missing
\ 02002013 8	Cycle step decrement register is missing
\ 02002014 8	Cycle back-step register is missing
\ 02002015 8	Cycle dwell register is missing
\ 02002016 8	Cycle spindle orient register is missing
\ 02002017 8	Cycle oriented-bore jog register is missing
\ 02002018 8	Cycle clearance position could not be calculated. Check questionnaire consistency.
\ 02002019 8	Cycle depth position could not be calculated. Check questionnaire consistency.
\ 02002020 8	Cycle retract position could not be calculated. Check questionnaire consistency.
\ 02002021 8	Cycle feed value is negative or zero.
\ 02002022 8	Cycle uPM feed value is negative or zero.
\ 02002023 8	Cycle uPR feed value is negative or zero.
\ 02002024 8	Cycle tap feed value is negative or zero.
!
! Controller Emulator (Pre-Processor error messages)
!
\ 02003001 16	The pre-processor could not be loaded because the function
		requirement were not met for that particular pre-processor version.
\ 02003002 16	The pre-processor DLL was not found: !(A)
\ 02003003 8	Pre-Processor: Wrong message ID in call of SendCEMessage
\ 02003004 8	Invalid value: !(A)
\ 02003005 8	No pre-processor loaded
\ 02003006 8	The requested variable is not defined: !(A)
\ 02003007 16	The pre-processor failed to initialize
\ 02003008 16	You are not licensed to use the Siemens VNCK. Contact
		your ICAM sales representative.
\ 02003009 8	$P1 was set to re-evaluate without modifying the $P2 value "!(A)" from the identification macro
!
! Controller Emulator (Initialization messages)
!
\ 02004001 16	Unable to open database file: !(A)
\ 02004002 16	Unable to load specified object: object name is invalid.
\ 02004003 16	Unable to load specified object: object type is unknown.
\ 02004004 16	Unable to load CE from database. Make sure object exist in the database
\ 02004005 16	Unable to load CE associated post from database. Make sure object exist in the database
\ 02004006 16	Failed to load CE or one of it's components because a license for this object could not be found.
\ 02004007 16	Failed to initialize simulation
\ 02004008 16	Unable to acquire the source MCD data
\ 02004009 16	Unable to load the input MCD file: !(A)
\ 02004010 16	Unable to load the error file: !(A)
\ 02004011 16	Unable to input the !(A) language file
\ 02004012 16	The !(A) locale is not supported by the system
\ 02004013 16	The post you are trying to import is using a deprecated format of the
		associated tables. Please re-generate it.
\ 02004014 16	Error opening listing file: !(A).
\ 02004015 16	Unable to open virtual machine model database file: !(A)
\ 02004016 16	Unable to load VM model from database. Make sure object exist in the database
\ 02004017 16	Unable to initialize Delmia axes.
\ 02004018 8	Unable to initialize the virtual machine model.
\ 02004019 16	Unable to create the virtual machine dynamic model.
\ 02004020 16	Failed to load VM model due to missing license.
\ 02004021 8	Unable to load the input MCD file: !(A)
\ 02004022 8	(Words)Invalid word definition on line !(*) of words file '!(A)'.
!
! Security license messages
!
\ 02005001 16	Not licensed to run dedicated Post-processor or Controller Emulator
\ 02005002 16	Not licensed for composite post
\ 02005003 16	Not licensed for multi-axes EDM
\ 02005004 16	Not licensed for EDM
\ 02005005 16	Not licensed for Punch
\ 02005006 16	Not licensed for multi-axes lathe
\ 02005007 16	Not licensed for merging lathe
\ 02005008 16	Not licensed for lathe
\ 02005009 16	Not licensed for Contour
\ 02005010 16	Not licensed for Multi-axis Mill
\ 02005011 16	Not licensed for Mill
\ 02005012 16	Cannot find a valid license key
\ 02005013 16	Not licensed for simulation
\ 02005014 16	Not licensed for material removal
\ 02005015 16	Not licensed to run dedicated Virtual Machine model
!
! Calsub error messages
!
\ 02006001 8	Subprogram calling is not active.
\ 02006002 8	Missing starting sequence number register.
\ 02006003 8	Missing ending sequence number register.
\ 02006004 8	Missing subprogram ID register.
\ 02006005 8	Missing required subprogram repeat count register.
\ 02006006 8	Subprogram ID number is out of range.
\ 02006007 8	Maximum subprogram nesting limit has been reached.
\ 02006008 8	Subprogram file "!(A)" not found.
\ 02006009 8	Subprogram starting sequence number !(*) not found.
\ 02006010 8	Subprogram ending sequence number !(*) not found.
\ 02006011 4	Using an end of subprogram code in the main program.
\ 02006012 8	Subprogram ending sequence number !(*) is lower than the starting number !(*).
\ 02006013 8	Subprogram "!(A)" not found.
!
! Controller Emulator (Circular interpolation error messages)
!
\ 02007001 8	Unknown circular interpolation plane. !(A) plane assumed.
\ 02007002 8	Circular interpolation has ambiguous choice of !(A) and !(A)
		axes motion.
\ 02007003 8	Circular interpolation block references one or more axes that
		cannot be circularly interpolated.
\ 02007010 8	Circular interpolation arc center is missing. Arc not
		generated.
\ 02007011 8	Circular interpolation radius is missing. Arc not generated.
\ 02007012 8	Circular interpolation arc center and radius are missing. Arc
		not generated.
\ 02007013 8	Circular interpolation radius specified without axes data. Arc
		not generated.
\ 02007014 8	Circular interpolation function coded without circle data or
		endpoint. Arc not generated.
\ 02007015 8	Circular interpolation center specified without axes data. Arc
		not generated.
\ 02007016 8	Circular interpolation !(A) axis center offset is missing. Zero
		assumed.
\ 02007017 8	Circular interpolation !(A) axis center offset should be
		unsigned or positive. Sign ignored.
\ 02007018 8	Circular interpolation !(A) axis center coordinate is missing.
		Current position assumed.
\ 02007019 8	Circular interpolation radius !(*), as defined by center
		coordinates, exceeds maximum permitted radius !(*).
\ 02007020 8	Circular interpolation radius !(*), as defined by radius
		register, exceeds maximum permitted radius !(*).
\ 02007021 8	Circular interpolation start and end radii difference !(*), as
		defined by center coordinates, is larger than the allowable
		tolerance of 1.414 times the offset register resolution !(*).
\ 02007022 8	Circular interpolation negative radius not permitted when
		maximum arc span is 180 degrees or less. Sign of radius
		ignored.
\ 02007023 8	Circular interpolation specified radius !(*) is smaller than
		start->end span minimum calculated radius !(*). Radius adjusted
		to fit span.
\ 02007024 8	Circular interpolation specified radius differs from center
		calculated radius by !(*), which is larger than the allowable
		tolerance of 1.414 times the radius register resolution !(*).
\ 02007025 8	Circular interpolation arc span !(*) exceeds maximum permitted
		arc span !(*).
\ 02007026 8	Circular interpolation arc crosses quadrant boundary.
\ 02007027 8	Circular interpolation !(A) axis helical rise/radian register
		is missing. Calculated rise used.
\ 02007028 8	Circular interpolation !(A) axis helical rise/radian value
		specified with a zero-degree span arc. Rise/radian value
		ignored.
\ 02007029 8	Circular interpolation !(A) axis helical rise/radian value !(*)
		differs from the calculated rise !(*) by more than the
		rise/radian register resolution !(*). Calculated rise used.
\ 02007030 8	Circular interpolation !(A) axis helical rise/radian value
		should be unsigned. Sign ignored.
\ 02007031 8	Circular interpolation !(A) axis helical rise/radian value sign
		does not match the direction of helical motion. Sign adjusted.
\ 02007032 8	Circular interpolation block should not include both arc center
		and radius data. Processing continues.
\ 02007033 8	Circular interpolation block should include both arc center and
		radius data. Processing continues.
\ 02007034 8	Circular interpolation !(A) axis helical pitch register
		is missing. Calculated pitch used.
\ 02007035 8	Circular interpolation !(A) axis helical pitch value
		specified with a zero-degree span arc. Pitch value
		ignored.
\ 02007036 8	Circular interpolation !(A) axis helical pitch value !(*)
		differs from the calculated pitch !(*) by more than the
		helical pitch register resolution !(*). Calculated pitch used.
\ 02007037 8	Circular interpolation !(A) axis helical pitch value
		should be unsigned. Sign ignored.
\ 02007038 8	Circular interpolation !(A) axis helical pitch value sign
		does not match the direction of helical motion. Sign adjusted.
\ 02007040 8	3D circular interpolation coded without coordinate data.
		Expected midpoint position is missing. Processing continues.
\ 02007041 8	3D circular interpolation coded without coordinate data.
		Expected endpoint position is missing. Processing continues.
\ 02007042 8	3D circular interpolation coded without coordinate data.
		Expected midpoint and endpoint positions are missing.
		Processing continues.
\ 02007043 8	3D circular interpolation midpoint position is missing.
		Arc not generated.
\ 02007044 8	3D circular interpolation endpoint position is missing.
		Arc not generated.
\ 02007045 8	3D circular interpolation failed. An arc cannot be constructed
		through the start (!(*),!(*),!(*)), mid (!(*),!(*),!(*)) and
		end (!(*),!(*),!(*)) points. Arc not generated.
\ 02007090 8	CE has not defined circular interpolation center or radius
		registers. Check questionnaire consistency.
!
! Controller Emulator (Other error messages)
!
\ 02008001 8	Combined !(A) axis linear and associated !(A) axis colinear
		axes must not be coded together on the same motion block.
		!(A) axis motion ignored.
\ 02008002 8	A maximum of 2 rotary axes can be coded on a motion block.
		!(A) axis motion ignored.
\ 02008003 4	Rewind-Stop code signaling the start of program was not found; program
		will be processed from the start ignoring the Rewind-Stop code at startup requirement
\ 02008004 4	End-of-Block code signaling the start of program was not found; program
		will be processed from the start ignoring the End-of-Block code at startup requirement
\ 02008005 4	Rewind-Stop / End-of-Block code combination signaling the start of program was not found; program
		will be processed from the start ignoring the Rewind-Stop code at startup requirement
\ 02008006 4	Unexpected data was found and skipped between the Rewind-Stop and End-of-Block 
		characters. Processing continues
\ 02008007 4	Rewind-Stop (RWS) code encountered in program section. RWS code ignored.
\ 02008008 4	'!(A)' ignored following End-of-Block (EOB) code
\ 02008009 8	Can not find CE matching designator #!(*)
\ 02008010 4	$FCESEEK: Requested DATA identifier "!(A)" is not modal. MODAL and LAST keyword can not be used.
\ 02008011 4	$FCESEEK: MODAL and LAST keyword can only be used when requesting a DATA identifier (not a CODE).
\ 02008012 8	$FCESEEK: Expecting MODAL or LAST keyword as the second argument.
\ 02008013 4	Requested DATA identifier "!(A)" is not defined.
\ 02008014 4	Requested CODE identifier "!(A)" is not defined.
\ 02008015 4	!(A) can only be used during block processing.
\ 02008016 4	$FCEADD: Value ignored when adding CODE identifiers.
\ 02008017 8	System variable !(A) is not assignable. Assignment ignored.
\ 02008018 8	$FCERST: Input program file "!(A)" not found or not readable.
\ 02008019 8	$FCERST: Provided program file offset must be positive or 0.
\ 02008020 8	$FCERST: This function can only be called from the program shutdown macro.
!
! Controller Emulator (DELAY)
!
\ 02009001 4	Dwell in revolutions per minute can not be performed when the
		spindle is stopped.
\ 02009002 4	Dwell value is negative.
\ 02009003 4	Missing register defining dwell time in seconds.
\ 02009004 4	Missing register defining dwell in revolutions.
!
! CE Macro Operation Errors
!
\ 02010001 8	Cannot remove the currently active CODE from the block
\ 02010002 8	LPRINT: First argument must be ON, OFF, a positive integer specifying
		the number of lines to skip or zero to cancel the previously
		set line skipping.
\ 02010003 8	LPRINT: Too many arguments specified.
\ 02010004 4	OUTPUT command ignored for this macro type.
!
! CE OPSKIP errors
!
\ 02011001 8	Sequence element must be of type REAL
\ 02011002 8	OPSKIP level !(*) outside of min/max limits
!
! CE EXEC Macro Command Errors
!
\ 02012001 8	EXEC command must contain at least one argument.
\ 02012002 8	EXEC argument must evaluate to a string, code
		or register name. Command ignored.
\ 02012003 4	EXEC string is blank or code/register sequence is empty.
		Command ignored.
\ 02012004 8	Invalid EXEC code/register list. Specify one or more
		code or register names, each optionally followed by a
		numeric code or register value. Command ignored.
\ 02012005 8	Cannot defer EXEC command when using VNCK.
\ 02012006 8	Cannot use EXEC with code/reg list when using VNCK.
!
! IXMP Macro Processor
!
\ 01110001 16	Internal error: !(A) Contact your ICAM representative.
\ 01111001 4	Macro: !(A)
\ 01111002 8	Macro: !(A)
\ 01111003 16	Macro: !(A)
\ 01111004 0	TERMAC generated due to error(s).
\ 01111005 0	TERMAC generated due to error(s) during look-ahead.
\ 01112001 4	Write to STDOUT requires verbose mode. WRITE command ignored.
\ 01112002 8	Read from STDIN requires verbose mode. READ command ignored.
\ 01112003 4	Read from STDIN requires verbose mode. READ command ignored.
\ 01117001 4	String format error: Expecting string for first argument.
\ 01117002 4	String format error: Argument stack overflow.
\ 01117003 4	String format error: Expecting format statement or another "!!".
\ 01117004 4	String format error: Not enough arguments specified.
\ 01117005 4	String format error: Argument !(s4) should be a numeric
		argument.
\ 01117006 4	String format error: Too many arguments specified. Extra
		ignored.
\ 01117007 4	String format error: Unsupported register letter.
\ 01117008 4	String format error: Not enough arguments specified.
\ 01117009 4	String format error: Missing register letter.
\ 01117010 4	String format error: Missing or invalid register letter.
\ 01117011 4	String format error: Invalid or unsupported register letter.
\ 01117012 4	String format error: Incomplete format statement.
\ 01117013 4	String format error: Argument !(s4) too large for format.
\ 01117014 4	String format error: Argument !(s4) should be a MINOR word
		argument.
\ 01117015 4	String format error: Argument !(s4) should be a text argument.
\ 01117016 4	String format error: Argument !(s4) should be a logical
		argument.
\ 01117017 4	String format error: Invalid register number.
\ 01117018 4	Internal error: Insufficient text space: Increase TXTMAX.
		Contact your ICAM representative.
\ 01117019 4	String format error: Invalid format specification.
\ 01117020 4	String format error: Argument !(s4) should be a RECORD
		argument.
\ 01117021 4	String format error: Invalid argument value.
