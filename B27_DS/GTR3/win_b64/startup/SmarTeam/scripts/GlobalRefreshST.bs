Option Explicit
'-----------------------------------------------------------------------------
' Script launch the Global Refresh function from Smarteam 
'
'
' Author: Dassault-Systemes
' Called By: User defined tool
'
' (C) Copyright Dassault Systemes 2009   All Rights Reserved
'-----------------------------------------------------------------------------
' History:
' 
' 2009/01/14 KXI : Created
' 2009/07/16 KXI : Modified
' 2009/09/16 WVM : Modified
' 2009/10/16 KXI : Modified
' 2010/03/22 KXI : Modified
' 2010/10/20 KXI : Modified
' 2010/11/23 KXI : Modified
' 2010/11/29 KXI : Modified
' 2011/02/04 KXI : Modified
' ----------------------------------------------------------------------------
' GLOBAL VARIABLES
' ----------------------------------------------------------------------------
' These variables can be used anywhere in this script.
' All the corresponding values are set at the beginning of the script
' and won't be modified during the process of the script.
' ----------------------------------------------------------------------------

Const ERROR_MESSAGE = 2
Const WARNING_MESSAGE = 1
Const INFO_MESSAGE = 0

Sub ShowMessage(SmSession As ISmSession, MessageCode As Integer, Params As ISmStrings , MessageType As Integer)
	
	Dim GUIServices As ISmGUIServices
	Dim SmButtons As Object
	Dim MESSCOMMON
	Dim Result

	MESSCOMMON = 1

	Set SmButtons = SmSession.NewVariantList()
	SmButtons.Value("0") = tmbOk
	Set GUIServices = SmSession.GetService("SmGUISrv.SmGUIServices")

	Result = GUIServices.GUIStore.DisplayMessage(MESSCOMMON, MessageCode, MessageType, 0, SmButtons, Params)	
End Sub

Function Extract_FileType(FileName As String) As String
	
	Dim UserString As String

    UserString = FileName
    Dim Increment1 As Integer
    Dim Temp_String As String
    Dim Length As Integer
    Length = Len(UserString)
    Increment1 = InStr(UserString, "\")
    While Increment1 > 0
        UserString = Right(UserString, Length - Increment1)
        Length = Len(UserString)
        Increment1 = InStr(UserString, "\")
    Wend
    Temp_String = UserString
    Length = Len(Temp_String)
    Increment1 = InStr(Temp_String, ".")
    While Increment1 <> 0
        UserString = Right(UserString, Length - Increment1)
        Length = Len(UserString)
        Increment1 = InStr(UserString, ".")
    Wend
    Increment1 = Len(Temp_String) - Len(UserString) - 1
    Extract_FileType = UserString

End Function


Function GetAllChildren(SmSession As ISmSession, SmObject As ISmObject, SmAllChildren As ISmObjects, AttributeNames As String , AllChildrenLatest As Boolean) As Boolean
'-------------------------------------------------------------------------
'
' Function
' Name        : GetAllChildren
' Description : To Retireve All Children of an Object 
'
' Input       : SmSession      : Current SmSession
'               SmObject       : Object whose children to be calculated
'               SmAllChildren  : Collection od All Children
'
' Output      : GetAllChildren : True  -> Successfully retrieved Children
'                                False ->Failed to Retrieve All Children
'
'-------------------------------------------------------------------------

    Dim SmObjectsToExpand As ISmObjects
    Dim SmTempObject      As ISmObject
    Dim SmChildren        As ISmObjects
    Dim SmChild           As ISmObject
    Dim SmQueryDefinition As ISmQueryDefinition
    Dim i                 As Long
    Dim Count             As Long
    Dim SmPrimaryIdenfier As ISmClassAttributes
    Dim SmClassAttribute  As ISmClassAttribute
    Dim Params 		  As ISmStrings    

    On Error GoTo ErrorExit
    
    GetAllChildren = False
    AllChildrenLatest = True    
    
    Set SmAllChildren = SmSession.ObjectStore.NewObjects
    Set SmObjectsToExpand = SmSession.ObjectStore.NewObjects
    SmObjectsToExpand.Add SmObject
    
    Set SmPrimaryIdenfier = SmObject.SmClass.PrimaryIdentifier
    Set SmQueryDefinition = SmSession.ObjectStore.NewQueryDefinition
    SmQueryDefinition.Roles.Add SmObject.SmClass.SuperClassId, "F"
    
    For i = 0 To SmPrimaryIdenfier.Count - 1
	  
        Set SmClassAttribute = SmPrimaryIdenfier.Item(i)
        SmQueryDefinition.Select.Add SmClassAttribute.Name, "F", False
        
    Next i    
    
    While SmObjectsToExpand.Count > 0
        
            Set SmTempObject = SmObjectsToExpand.Item(0)	        
            Set SmChildren = SmTempObject.RetrieveChildren(SmQueryDefinition)        
                
            Count = SmChildren.Count - 1
            For i = 0 To Count
            
            	If AllChildrenLatest = True Then
					If SmChildren.Item(i).ObjectID <> SmChildren.Item(i).RetrieveLastRevisionObject(rfLatest).ObjectID Then			    
						AllChildrenLatest = False
					End If
				End If
				
				Set SmChild = SmChildren.Item(i).RetrieveLastRevisionObject(rfLatest)
                SmAllChildren.Add SmChild
                SmObjectsToExpand.Add SmChild
                
            Next i
        	SmObjectsToExpand.Remove 0
        
    Wend
    
    If SmAllChildren.Count > 0 Then	      
    	If AttributeNames <> "" Then
    	    SmAllChildren.AddAttributes AttributeNames
            SmAllChildren.Refresh
    	End If
    End If       
            
    GetAllChildren = True

    Exit Function

ErrorExit:
	Set Params = SmSession.NewSmStrings()
    Params.Add "GetAllChildren"
    Params.Add Err.Description
	ShowMessage SmSession, 28503, Params, ERROR_MESSAGE 'ERR_IN_FUNCTION = 28503   

End Function
	
	
Function IsGRAllowed(SmSession As ISmSession,WorkObjects As SmApplic.ISmObjects) As Boolean
    
'-------------------------------------------------------------------------
'
' Function
' Name          : IsGRAllowed
' Description   : Return true if one object the list is not in "CheckIn"
'                 or "Release" state. This function dertermines if the
'                 GlobalRefresh function can be performed
'
' Input         : WorkObjects : List of objects from Smarteam
'
' Output        : True => can perform the GlobalRefresh
'
'-------------------------------------------------------------------------
    On Error GoTo ErrorTrt
    
    Dim i As Long
    Dim State As Integer
    Dim CurrentObject As ISmObject
	Dim Params 	 	  As ISmStrings

    
    IsGRAllowed = True

    For i = 0 To WorkObjects.Count - 1
    
        Set CurrentObject = WorkObjects.Item(i)

        If CurrentObject Is Nothing Then
            IsGRAllowed = False
            Exit Function
        End If
        
        State = CurrentObject.Data.Value(NM_STATE)
        If State <> 1 And State <> 3 Then
            IsGRAllowed = False
            Exit Function
        End If
        
    Next i
    
    Set CurrentObject = Nothing
    
    Exit Function

ErrorTrt:
	Set Params = SmSession.NewSmStrings()
    Params.Add "IsGRAllowed"   	
    Params.Add Err.Description
	ShowMessage SmSession, 28503, Params,ERROR_MESSAGE 'ERR_IN_FUNCTION = 28503  

End Function

Function CheckOutRootObject(SmSession As ISmSession,WorkObject As SmApplic.ISmObject, State As Integer)	As Boolean

	On Error GoTo ErrorTrt

    CheckOutRootObject = False
	Dim Operation As ISmOperation  
	Dim Metainfo As SmMetaInfo 
    Dim SessionUtil	As SmUtil.SmSessionUtil 
    Dim TaskRecord As Object 
	Dim OperName As String 
	Dim Result As Long 
	Dim DefaultTask	As Object 
	Dim Propogate As Boolean
	Dim Params As ISmStrings	 

	Set SessionUtil = SmSession.GetService("SmUtil.SmSessionUtil")	
	Set Metainfo = SmSession.Metainfo	
	If Not (WorkObject Is Nothing) Then

      If State = 1 Then	  
	      OperName = NM_OPER_CHECKOUT
	  Else
		  OperName = NM_OPER_NEWRELEASE
	  End If

	  Set Operation = Metainfo.OperationsForClass(WorkObject.ClassId, False).ItemByName(OperName)

	  ' Check if operation allowed for object
	  If SessionUtil.OperationAllowedOnObject(WorkObject, Operation, False) Then	

		 ' Perform operation on object using SmSessionUtil method 
		 Set DefaultTask = Nothing
		  Set TaskRecord = Nothing			 
		 Propogate = False		 		 
		 SessionUtil.ExecuteOperationOnObjectTree WorkObject, Operation, Propogate, TaskRecord, DefaultTask
		 CheckOutRootObject = True

	  End If
	End If
	Exit Function

ErrorTrt:
	Set Params = SmSession.NewSmStrings()
    Params.Add "CheckOutRootObject"   	
    Params.Add Err.Description
	ShowMessage SmSession, 28503, Params,ERROR_MESSAGE 'ERR_IN_FUNCTION = 28503  

End Function


 		
Function GlobalRefreshST(ApplHndl As Long, Sstr As String, FirstPar As Long, SecondPar As Long, ThirdPar As Long) As Integer
'-------------------------------------------------------------------------
'
' Function
' Name          : GlobalRefreshST
' Description   : This function performs Global Refresh on Selected Object in Smarteame.
'
' Input         : FirstPar : RecordList of object Selected
'
' Output        : 
'-------------------------------------------------------------------------   
    On Error GoTo ErrorTreatment	
		
	Dim FirstRec 					As Object	 	
	' LifeCycle state of selected Smarteam Object
    Dim State                       As Integer
    ' Selected Smarteam object
    Dim SmObject                   As ISmObject
    ' Class and Object ID of selected Smarteam obejct
    Dim ObjID                       As Long
    Dim ClassID                     As Integer

	Dim CurrentSession 				As ISmSession
	Dim CurrentCATIAApp             As Object
	Dim Integration                 As Object 
	Dim ListObjects                 As ISmObjects
    Dim FileName                    As String
	Dim ShortName                    As String
	Dim FileType 					As String
	Dim CATRel  					As Long
    Dim bCATIADisplayMessages       As Boolean
    
    Dim SwitchToLatestForceCheckOut As String
    Dim ReplacingObjects            As String
    Dim UseLightLifeCycleScreen     As String
    Dim CatiaRefreshScreen          As String
    Dim LatestAvailableOnCheckOut   As String
    Dim OverwriteSilenceMode        As String
    Dim EvaluationReport            As String
    Dim SwitchToLatestForceNR       As String
    Dim CheckOutOverwrite           As String
    Dim CompareRevisionBeforeOverwriteFile As String	
    Dim CopyOverwrite               As String
	Dim IsConnected 				As Boolean
	Dim Params 						As ISmStrings  ' for errors  
	Dim SwitchToLatestCopyFile      As String
	Dim CheckoutObject	 			As Boolean
	Dim CatiaDocuments 				As Object
	Dim count1 						As Integer

	
	Set Params = Nothing

	Set CurrentSession = SCREXT_ObjectForInterface(ApplHndl)	

	Set CurrentCATIAApp = GetObject(,"CATIA.Application")   	
	If CurrentCATIAApp Is Nothing Then
		ShowMessage CurrentSession,07888, Params, ERROR_MESSAGE
		'"CATIA is Not Launched"		
        GoTo FreeObject
    End If

	
	Set Integration = CurrentCATIAApp.GetItem("CAIEngine")	

	IsConnected = Integration.IsConnected()	
	If IsConnected = False Then
		ShowMessage CurrentSession,07889, Params,ERROR_MESSAGE
		'"SmarTeam is not Connected from CATIA. Please connect and then run the command"
        GoTo FreeObject
    End If
	
	CATRel = CurrentCATIAApp.SystemConfiguration.Release

	' Get The correct session associated with CAI
	Dim VbObj As Object
	Dim SessionName
	 	 
	Set VbObj = CurrentCATIAApp.GetItem("CATStiVB")
    If Not (VbObj Is Nothing) Then	    
		SessionName = VbObj.GetCurrentSmSessionName 	 
	End If 	
	
     ' Check Whether we are in secured connection

	'Dim ProtectedSession As String   
	'Dim Password As String  
    
	'ProtectedSession = CurrentSession.Config.Value("$User\EnableProtectedSessionMode")    
   
	'If ProtectedSession = "True" Then          		
	    'Dim Message As String    
	    'Dim NLSMessage As String 	    
	    'Dim MsgTitle As String    
	    'Dim NLSMsgTitle As String 
	
		'Message = "Please Enter Session Password"
	    'MsgTitle = "Session Password"	 	 

		'If Not (VbObj Is Nothing) Then
			'NLSMessage = VbObj.GetNLSMessage("CATSmarTeamIntegration", "LAGR.SecurePassword",	Message)
			'NLSMsgTitle = VbObj.GetNLSMessage("CATSmarTeamIntegration", "LAGR.SecurePassword.Title",	MsgTitle)
		'End If 		
	
		'Password = InputBox(NLSMessage,NLSMsgTitle)
	'End If 	   
     
	Dim objSmarteamApplication As Object
	Dim NewSession As Object 
	Set objSmarteamApplication = GetObject(,"Smarteam.SmApplication")
    If Not (objSmarteamApplication Is Nothing) Then
	    Dim SessionCount As Integer
		Dim i As Integer
		SessionCount = objSmarteamApplication.Engine.SessionsCount		
		For i = 0 To SessionCount - 1
        	If CATRel >= 21 Then
        		VbObj.ProvideSecuredSessionForGR
        	End If			
		   	Set NewSession = objSmarteamApplication.Engine.Sessions(i)	
		    If SessionName = NewSession.SessionName Then	 		  	 
				Set CurrentSession = NewSession	
				Exit For
			End If
		Next i 	
	End If
	
	' Check whether we found correct session
	If (CurrentSession Is Nothing) Then
	    Set CurrentSession = NewSession
		GoTo FreeObject
	End If
		 		        
	' convert input data	
    CONV_RecListToComRecordList FirstPar, FirstRec   
	
	' Check that record list contains only one item
    If FirstRec.RecordCount <> 1 Then
		ShowMessage CurrentSession,07890, Params,ERROR_MESSAGE
		'"More Than one object Selected Please select only one SmarTeam object"        
        GoTo FreeObject
    End If    	
	
				
	' Get Smarteam selected SmObject
    Set SmObject = CurrentSession.ObjectStore.ObjectFromData(FirstRec.GetRecord(0), True).Clone
    SmObject.Retrieve

	' Check Whether we are dealing with CATIA Product only.
	ShortName = SmObject.Data.ValueAsString(NM_CAD_REF_FILE_NAME)
	FileType = Extract_FileType(ShortName)
	If FileType <> "CATProduct"	Then
         'This command is available only for CATIA Products.
		ShowMessage CurrentSession, 07896, Params,ERROR_MESSAGE
        GoTo FreeObject
	End If
    
    ' Get classID and object ID
    ObjID = SmObject.ObjectID
    ClassID = SmObject.ClassID	
  
	State = SmObject.RetrieveLastRevisionObject(rfLatest).Data.ValueAsInteger(NM_STATE)
    If State <> 1 And State <> 3 Then	    
		Set Params = CurrentSession.NewSmStrings()		
		Params.Add ShortName & " "
        '"Selected Object or its Latest Revision is not in Correct State. Please select Checked in or Released Object"
		ShowMessage CurrentSession, 07891, Params,ERROR_MESSAGE
		Set Params = Nothing 
        GoTo FreeObject
    End If
    
	' Get All Children and check their states.  

	Dim AllChildren As Boolean
    Dim AllChildrenLatest As Boolean

	AllChildren = GetAllChildren(CurrentSession, SmObject, ListObjects, NM_CAD_REF_FILE_NAME , AllChildrenLatest)
	
	If AllChildren = False Then		
        '"Could not get All children"        
        GoTo FreeObject
    End If   

	If AllChildrenLatest = True Then
		ShowMessage CurrentSession, 07906, Params ,WARNING_MESSAGE
		'"The assembly may be up-to-date.There is no need to run Global Refresh."        
        GoTo FreeObject
    End If   

	
	Dim GRAllowed As Boolean
	GRAllowed = IsGRAllowed(CurrentSession,ListObjects)

	If GRAllowed = False Then
		ShowMessage CurrentSession, 07892, Params ,ERROR_MESSAGE
        '"Check That all children are in CI or Released State"        
        GoTo FreeObject
    End If   

	 ' All Checks are completed now. We can start actual Global Refresh Now.
        
    Dim dbitem As Object

    FileName = CurrentSession.Config.Value(CFGUSERWORKINGDIRECTORY) & "\" & SmObject.Data.ValueAsString(NM_CAD_REF_FILE_NAME)
	    	 
		If Not IsEmpty(FileName) And Not IsNull(FileName) Then
			If Not CurrentCATIAApp Is Nothing Then
                CurrentCATIAApp.Visible = False
            End If
            
            Set SmObject = SmObject.RetrieveLastRevisionObject(rfLatest)
            
            ' Get Smarteam Property
            On Error GoTo ErrorSmarteamProperty
            LatestAvailableOnCheckOut = CurrentSession.Config.Value("$User\LifeCycleSetUp\LatestAvailableOnCheckOut")
            SwitchToLatestForceCheckOut = CurrentSession.Config.Value("$User\LIFE_CYCLE\SwitchToLatestForceCheckOut")		
            ReplacingObjects = CurrentSession.Config.Value("$User\LifeCycleSetUp\ReplacingObjectsReportOnCheckOut")
            UseLightLifeCycleScreen = CurrentSession.Config.Value("$User\LifeCycleSetUp\UseLightLifeCycleScreen")
            SwitchToLatestCopyFile = CurrentSession.Config.Value("$User\LIFE_CYCLE\LatestAvailableOnCopyFile")
            CatiaRefreshScreen = CurrentSession.Config.Value("$User\CATIA\Refresh_Screen")	            
			OverwriteSilenceMode = CurrentSession.Config.Value("$User\LIFE_CYCLE\FileOverwriteSilienceMode")
            EvaluationReport = CurrentSession.Config.Value("$User\LIFE_CYCLE\LifeCycleEvaluationReport")
            SwitchToLatestForceNR = CurrentSession.Config.Value("$User\LIFE_CYCLE\SwitchToLatestForceNewRelease")
            CheckOutOverwrite = CurrentSession.Config.Value("$User\LifeCycleSetUp\CheckOutOverwrite")
            CompareRevisionBeforeOverwriteFile = CurrentSession.Config.Value("$User\LifeCycleSetUp\CompareRevisionBeforeOverwriteFile")
            CopyOverwrite = CurrentSession.Config.Value("$User\LifeCycleSetUp\CopyOverwrite")
				  
            CurrentSession.Config.Value("$User\LIFE_CYCLE\SwitchToLatestForceCheckOut") = "TRUE"
            CurrentSession.Config.Value("$User\LifeCycleSetUp\ReplacingObjectsReportOnCheckOut") = "NO"
            CurrentSession.Config.Value("$User\LifeCycleSetUp\UseLightLifeCycleScreen") = "NODIALOG"
            CurrentSession.Config.Value("$User\CATIA\Refresh_Screen") = "FALSE"
            CurrentSession.Config.Value("$User\LifeCycleSetUp\LatestAvailableOnCheckOut") = "YES"
            CurrentSession.Config.Value("$User\LIFE_CYCLE\FileOverwriteSilienceMode") = "TRUE"
            CurrentSession.Config.Value("$User\LIFE_CYCLE\LifeCycleEvaluationReport") = "FALSE"
            CurrentSession.Config.Value("$User\LIFE_CYCLE\SwitchToLatestForceNewRelease") = "TRUE"
            CurrentSession.Config.Value("$User\LifeCycleSetUp\CheckOutOverwrite") = "YES"
            CurrentSession.Config.Value("$User\LifeCycleSetUp\CompareRevisionBeforeOverwriteFile") = "NO"
            CurrentSession.Config.Value("$User\LifeCycleSetUp\CopyOverwrite") = "YES"		
 	        CurrentSession.Config.Value("$User\LIFE_CYCLE\LatestAvailableOnCopyFile") = "TRUE"			
			 				
			' Create new ST session to reflect config settings
            Dim ApplicationName As String
            Dim SmDB As Object
            Dim SmDBConnection As Object
            Dim ConnectToNewSession As Boolean					 
            Dim NewEditorSession As Object			   
			
            Set SmDB = CurrentSession.Database

            If CATRel >= 21 Then	    
                VbObj.ProvideSecuredSessionForGR
            End If			
		    		
            Set NewEditorSession = objSmarteamApplication.Engine.CreateSession(ApplicationName,	CurrentSession.ConfigurationName)
			
            If Not (NewEditorSession Is Nothing) Then						
                 Set SmDBConnection = NewEditorSession.OpenDatabaseConnection(SmDB.Alias,SmDB.Password,TRUE)
                 If Not (SmDBConnection Is Nothing) Then			  			
                     If(SmDBConnection.Connected = TRUE) Then
                        ConnectToNewSession = NewEditorSession.UserLogin(CurrentSession.UserMetaInfo.UserLogin, CurrentSession.UserMetaInfo.UserPassword)				  
                     End If
                  End If
             End If

			
			'Not display CATIA messages
            If Not CurrentCATIAApp Is Nothing Then
                bCATIADisplayMessages = CurrentCATIAApp.DisplayFileAlerts
                CurrentCATIAApp.DisplayFileAlerts = False
            End If
			
            Dim prdDoc As Object

			Set CatiaDocuments = CurrentCATIAApp.Documents
    		Count1 = CatiaDocuments.Count				
		    If Count1 > 0 Then
				On Error Resume Next				
		        Set prdDoc = CatiaDocuments.Item(ShortName)						
			End If
			
            If Not(prdDoc Is Nothing) Then			    
       			prdDoc.close
			End If	  			            
		            
            Set dbitem = Integration.BuildDocDBItemFromSmarTeamID(ObjID, ClassID)			
			
			If(ConnectToNewSession = TRUE) Then
				CheckoutObject = CheckOutRootObject (NewEditorSession, SmObject , State)
			Else
			    CheckoutObject = CheckOutRootObject (CurrentSession, SmObject , State)
			End If 

			If( Not CheckoutObject) Then			   
 			  	GoTo ErrorSmarteamProperty
            End If          
            
            CurrentSession.Config.Value("$User\LIFE_CYCLE\SwitchToLatestForceCheckOut") = SwitchToLatestForceCheckOut
            CurrentSession.Config.Value("$User\LifeCycleSetUp\LatestAvailableOnCheckOut") = LatestAvailableOnCheckOut
            CurrentSession.Config.Value("$User\LifeCycleSetUp\ReplacingObjectsReportOnCheckOut") = ReplacingObjects
			CurrentSession.Config.Value("$User\LIFE_CYCLE\SwitchToLatestForceNewRelease") = SwitchToLatestForceNR
			CurrentSession.Config.Value("$User\LIFE_CYCLE\LatestAvailableOnCopyFile") = SwitchToLatestCopyFile					             	
            

			Set prdDoc = CurrentCATIAApp.Documents.Open(FileName)
			
		    Set dbitem = Integration.GetStiDBItemFromAnyObject(prdDoc)		
            Integration.Save dbitem

            Integration.LifeCycleCheckIn dbitem

            CurrentSession.Config.Value("$User\LifeCycleSetUp\UseLightLifeCycleScreen") = UseLightLifeCycleScreen
            CurrentSession.Config.Value("$User\CATIA\Refresh_Screen") = CatiaRefreshScreen
            CurrentSession.Config.Value("$User\LIFE_CYCLE\FileOverwriteSilienceMode") = OverwriteSilenceMode
            CurrentSession.Config.Value("$User\LIFE_CYCLE\LifeCycleEvaluationReport") = EvaluationReport
            CurrentSession.Config.Value("$User\LifeCycleSetUp\CheckOutOverwrite") = CheckOutOverwrite
            CurrentSession.Config.Value("$User\LifeCycleSetUp\CompareRevisionBeforeOverwriteFile") = CompareRevisionBeforeOverwriteFile
            CurrentSession.Config.Value("$User\LifeCycleSetUp\CopyOverwrite") = CopyOverwrite

            'Set back the messages in CATIA
            If Not CurrentCATIAApp Is Nothing Then
                CurrentCATIAApp.DisplayFileAlerts = bCATIADisplayMessages
            End If											                        
            
			DoEvents

        End If
		       
    If Not CurrentCATIAApp Is Nothing Then
        CurrentCATIAApp.Visible = True
    End If	 
    
	If(ConnectToNewSession) Then
		NewEditorSession.UserLogOff
	End If

	If Not(SmDBConnection Is Nothing) Then
		SmDBConnection.CloseDatabaseConnection	
	End If

	If Not(NewEditorSession Is Nothing)	Then
		NewEditorSession.Close
	End If

    ' Refresh windows in all the session
	Dim GUIServ As Object
    Dim smWindows As Object
    ListObjects.Add SmObject ' Add root To All Children

    SessionCount = objSmarteamApplication.Engine.SessionsCount                                          
    
    For i = 0 To SessionCount - 1  
        If CATRel >= 21 Then	    
            VbObj.ProvideSecuredSessionForGR
        End If								

        Set NewSession = objSmarteamApplication.Engine.Sessions(i) 
        If Not(NewSession Is Nothing) Then	                                                  
            Set GUIServ = NewSession.GetService("SmGUISrv.SmCommonGUI")
            If Not(GUIServ Is Nothing) Then
                Set smWindows = GUIServ.Views.Windows                                    
          	    If smWindows.Count > 0 Then
	                 smWindows.Item(0).SmView.Refresh 1,ListObjects,TRUE
                End If
             End If
       	End If
    Next i          
     

	ShowMessage CurrentSession, 07894, Params,INFO_MESSAGE
    Exit Function
    
ErrorSmarteamProperty:
    CurrentSession.Config.Value("$User\LIFE CYCLE\SwitchToLatestForceCheckOut") = SwitchToLatestForceCheckOut
    CurrentSession.Config.Value("$User\LifeCycleSetup\ReplacingObjectsReportOnCheckOut") = ReplacingObjects
    CurrentSession.Config.Value("$User\LifeCycleSetup\UseLightLifeCycleScreen") = UseLightLifeCycleScreen
    CurrentSession.Config.Value("$User\CATIA\Refresh_Screen") = CatiaRefreshScreen
    CurrentSession.Config.Value("$User\LifeCycleSetUp\LatestAvailableOnCheckOut") = LatestAvailableOnCheckOut
    CurrentSession.Config.Value("$User\LIFE_CYCLE\FileOverwriteSilienceMode") = OverwriteSilenceMode
    CurrentSession.Config.Value("$User\LIFE_CYCLE\LifeCycleEvaluationReport") = EvaluationReport
    CurrentSession.Config.Value("$User\LIFE_CYCLE\SwitchToLatestForceNewRelease") = SwitchToLatestForceNR
    CurrentSession.Config.Value("$User\LifeCycleSetUp\CheckOutOverwrite") = CheckOutOverwrite
    CurrentSession.Config.Value("$User\LifeCycleSetUp\CompareRevisionBeforeOverwriteFile") = CompareRevisionBeforeOverwriteFile
    CurrentSession.Config.Value("$User\LifeCycleSetUp\CopyOverwrite") = CopyOverwrite
	CurrentSession.Config.Value("$User\LIFE_CYCLE\LatestAvailableOnCopyFile") = SwitchToLatestCopyFile            
    
ErrorTreatment:
    If CurrentCATIAApp Is Nothing Then
        ShowMessage CurrentSession,07888, Params,ERROR_MESSAGE
    Else
	Set Params = CurrentSession.NewSmStrings()
	Params.Add	Err.Description   
    ShowMessage CurrentSession, 07893, Params,ERROR_MESSAGE
    End If	     
    DoEvents

FreeObject:    
    'Free Objects

    Set Integration = Nothing
    Set FirstRec = Nothing
    Set SmObject = Nothing
    Set ListObjects = Nothing
    
    If Not CurrentCATIAApp Is Nothing Then
        CurrentCATIAApp.Visible = True
    End If

	If(ConnectToNewSession) Then
		NewEditorSession.UserLogOff
	End If

	If Not(SmDBConnection Is Nothing) Then
		SmDBConnection.CloseDatabaseConnection	
	End If

	If Not(NewEditorSession Is Nothing)	Then
		NewEditorSession.Close
	End If

	'Set Params = CurrentSession.NewSmStrings()
	'Params.Add	Err.Description   
    'ShowMessage CurrentSession, 07893, Params,ERROR_MESSAGE
	     
End Function



