Option Explicit
'---------------------------------------------------------------------------
'  This Script displays attributes of linked documents:
'  Displayed attributes are:
'  CN_PART_NUMBER
'
'     Script Requires that current object has also a CN_PART_NUMBER attribute
'
'     Author: Dassault-Systemes
'
'     Notes: (none)
'
' (C) Copyright Dassault-Systemes 2000   All Rights Reserved
'---------------------------------------------------------------------------
'  THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED
'  WARRANTY.  ALL IMPLIED WARRANTIES OF FITNESS FOR ANY PARTICULAR
'  PURPOSE AND OF MERCHANTABILITY ARE HEREBY DISCLAIMED.
'	07/16/2004 Modification By Dassault Systemes 
'	in order to manage Downstream application linked objects
'---------------------------------------------------------------------------
'
' Attribute to be retrieved on the linked document
'
Dim LinkedAttributeName As String
'
' Attribute where to store the retrieved attribute on the main document
'
Dim MainAttributeName As String


'
' Retrieve attributes from linked document
' Update current document profile-card
'
Sub RetrieveAttributesOfLinked(SmSession As Object, SmObject As Object)
    Dim DocumentsClass As ISmClass
	Dim LinkClasses As ISmClasses

	SmObject.Retrieve	'
	' Retrieve links to Documents from the selected object
	' input : SmObject
	' output: Children
	'
	Dim QueryDefinition As ISmQueryDefinition
	Dim LinkedDocs As ISmObjects
	Set QueryDefinition = SmSession.ObjectStore.NewQueryDefinition

  	Set DocumentsClass = SmSession.MetaInfo.SmClassByName("Documents")	
	If Not DocumentsClass Is Nothing Then
		QueryDefinition.Roles.Add DocumentsClass.ClassId, "S"
	    Set LinkClasses = SmObject.SmClass.GeneralLinks.GetLinkClasses(DocumentsClass.ClassId)
    End If
	
	'	07/16/2004
	'	Modification By Dassault Systemes		 
    Dim LinkClass As IsmClass
    Set LinkClass = SmSession.Metainfo.SmClassByName("CATIA Downstream Application")
	QueryDefinition.Roles.Add LinkClass.ClassId , "L"
 
	Set LinkedDocs = SmObject.RetrieveRelations(QueryDefinition)

	'
	' Display informations about the children
	' input : SmObject
	' output: nothing
	'
	Dim NewPartNumber As String
	NewPartNumber = ""
	Dim nbLinked As Integer
	Dim i As Integer
	nbLinked = LinkedDocs.Count
	Print "Nb children:" & nbLinked 
	For i = 0 To nbLinked-1
		Dim oneLinked As ISmObject
		Set OneLinked = LinkedDocs.Item(i).Clone
		OneLinked.Retrieve
		Dim PartNumber As String
		' Check that required attribute is present
		If ( OneLinked.Data.Headers.HeaderExists(LinkedAttributeName) ) Then
			PartNumber = OneLinked.Data.ValueAsString(LinkedAttributeName)
			Print "Retrieved PartNumber:" + PartNumber
		End If
		If ( NewPartNumber = "" ) Then
			NewPartNumber = PartNumber
		Else
			NewPartNumber = NewPartNumber + chr$(13) + chr$(10) + PartNumber
		End If
	Next
	Dim Behavior
	Set Behavior = SmObject.ObjectStore.DefaultBehavior.Clone
	Behavior.InvokeScripts = False
	If ( SmObject.Data.Headers.HeaderExists(MainAttributeName) ) Then
		If SmObject.SmClass.AttributeExists("TDM_ID") Then 
			Print "Updating main object:" + SmObject.Data.ValueAsString("TDM_ID")
		Else 
			Print "Updating main object:" + SmObject.Data.ValueAsString("CN_ID")
		End If
		SmObject.Data.ValueAsString(MainAttributeName) = NewPartNumber
    	SmObject.UpdateEx Behavior
	End If

	'
	' Now, repeat operation for children of document
	' (usefull when mapping properties on the drawing level and on the sheets level)
	'
	Dim Children As ISmObjects
	Dim QueryDef As ISmQueryDefinition
	Set QueryDef = Nothing
	Set Children = SmObject.RetrieveChildren(QueryDef)
	Dim nbChildren As Integer
	nbChildren = Children.Count
	For i = 0 To nbChildren-1
		Dim OneChild As ISmObject
		Set OneChild = Children.Item(i).Clone
		Call RetrieveAttributesOfLinked (SmSession, OneChild)
	Next
End Sub

Sub ShowSmErrorMessage(SmSession As ISmSession, MessageCode As Integer, Params As ISmStrings)
	Dim GUIServices As ISmGUIServices
	Dim SmButtons As Object
	Dim ErrERROR
	Dim MESSCOMMON

	ErrERROR = 2
	MESSCOMMON = 1

	Set SmButtons = SmSession.NewVariantList()
	SmButtons.Value("0") = tmbOk

	Set GUIServices = SmSession.GetService("SmGUISrv.SmGUIServices")

	Dim Result
	Result = GUIServices.GUIStore.DisplayMessage(MESSCOMMON, MessageCode, ErrERROR, 0, SmButtons, Params)	
End Sub

Function AttributesOfLinked(ApplHndl As Long,Sstr As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long ) As Integer
	Dim SmSession As ISmSession
	Dim FirstRec  As Object
	Dim SecondRec As Object
	Dim ThirdRec  As Object
	Dim SmObject As ISmObject
	Dim Params As ISmStrings

	On Error GoTo AssignErrorCode 
	'ViewPort.Open "Debug window",3000,3000,500,500
	'ViewPort.Clear

	LinkedAttributeName = "CN_PART_NUMBER"
	MainAttributeName = "CN_PART_NUMBER"

 '================================Converting procedural script arguments into COM ones===============================

    'Converting ApplHndl to SmSession
    Set SmSession = SCREXT_ObjectForInterface(ApplHndl) 
    'Converting three record lists into COM SmRecordList objects
    CONV_RecListToComRecordList FirstPar,FirstRec 
    CONV_RecListToComRecordList SecondPar,SecondRec
    CONV_RecListToComRecordList ThirdPar,ThirdRec
    
 '================================The Code============================================================================

	'
	' Retrieve selected object
	' input : SmSession, FirstRec
	' output: SmObject
	'
	If FirstRec.RecordCount = 0 Then	
		Set Params = Nothing
		ShowSmErrorMessage SmSession, 28500, Params 'ERR_NO_OBJECT_SELECTED_OR_ERROR
		
		GoTo EndFunc
	End If

	Set SmObject = SmSession.ObjectStore.ObjectFromData(FirstRec.GetRecord(0),True)
	SmObject.Retrieve

	'
	' Retrieve attribute from linked document
	'
	Call RetrieveAttributesOfLinked (SmSession, SmObject)

 '================================Converting COM Variable into procedural script output===============================
    CONV_ComRecListToRecordList ThirdRec,ThirdPar 
   	AttributesOfLinked = Err_None
    Exit Function
  AssignErrorCode : 
	Set Params = SmSession.NewSmStrings()

    Params.Add "AttributesOfLinked"
   	Params.Add "AttributesOfLinked"
    Params.Add Err.Description
	ShowSmErrorMessage SmSession, 28503, Params 'ERR_IN_FUNCTION = 28503
    AttributesOfLinked = Err_Gen  
  EndFunc:
End Function



