Option Explicit
'---------------------------------------------------------------------------
'  This Script demonstrates the usage of a scripting macro language from
'  within SmarTeam .
'
'     Script Requires: (none)
'
'     Author: Smart Solutions Ltd.
'
'     Notes: (none)
'
' (C) Copyright Smart Solutions Ltd. 1995   All Rights Reserved
'---------------------------------------------------------------------------

'Const Err_None = 0, Err_Gen = 1
'Const Err_NotFound = 3
'Const TDMT_CHAR = 1
'------------------------------------------------------------------------------------
Function StripFileExtension(FileName As String) As String
  Dim i As Integer
  Dim DotPos As Integer

  StripFileExtension = FileName
  DotPos = 0
  For i = Len(FileName) To 1 Step -1
    If (Mid(FileName,i,1) = ".") Then
      DotPos = i
	  Print "DotPos = " + cstr(DotPos)
	  Exit For
    End If
  Next i
  If (DotPos <> 0) Then
    StripFileExtension = left(FileName, DotPos-1)
	Print "StripFileExtension = " + left(FileName, DotPos-1)
  End If
End Function

'------------------------------------------------------------------------------------
Function GetFileExtension(FileName As String) As String
  Dim i As Integer
  Dim DotPos As Integer

  GetFileExtension = FileName
  DotPos = 0
  For i = Len(FileName) To 1 Step -1
    If (Mid(FileName,i,1) = ".") Then
      DotPos = i
	  Print "DotPos = " + cstr(DotPos)
	  Exit For
    End If
  Next i
  If (DotPos <> 0) Then
    GetFileExtension = Mid(FileName, DotPos+1, Len(FileName)-DotPos)
	Print "GetFileExtension = " + Mid(FileName, DotPos+1, Len(FileName)-DotPos)
  Else
	GetFileExtension = ""
  End If
End Function
'------------------------------------------------------------------------------------
Sub ShowSmErrorMessage(SmSession As ISmSession, MessageCode As Integer, Params As ISmStrings)
	Dim GUIServices As ISmGUIServices
	Dim SmButtons As Object
	Dim ErrERROR
	Dim MESSCOMMON
	Dim Result

	ErrERROR = 2
	MESSCOMMON = 1

	Set SmButtons = SmSession.NewVariantList()
	SmButtons.Value("0") = tmbOk
	Set GUIServices = SmSession.GetService("SmGUISrv.SmGUIServices")

	Result = GUIServices.GUIStore.DisplayMessage(MESSCOMMON, MessageCode, ErrERROR, 0, SmButtons, Params)	
End Sub

Function SetDescription(ApplHndl As Long,SelectOp As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long ) As Integer
' FirstPar  - Record List, containing all attributes of the current object
' SecondPar - nil
' ThirdPar  - nothing
Dim RetCode As Integer
Dim file_name  As String
Dim file_ext As String
Dim tmp_file_name  As String
Dim description  As String, cn_id As String
Dim StrValue  As String
Dim ValueSize As Integer,ValueType As Integer
Dim ClassId As Integer
Dim Before_Value As String, After_Value As String
Dim CompNameAttr As String
Dim CompTypeAttr As String
Dim CompName As String
Dim CompType As String
Dim CompTypeStr As String
Dim IsExternalMDT As Boolean
Dim AssyTypeStr As String
Dim PartTypeStr As String
Dim DrawTypeStr As String
Dim FileTypeStr As String
Dim LngPtr As Long
Dim File_Type As Long
Dim RefClassId As Integer
Dim strDescriptionField As String , strIDField As String
Dim WorkingDir As String
Dim Directory  As String
Dim SmSession As SmApplic.SmSession  
Dim Record1  As Object
Dim Record2 As Object
Dim Record3  As Object
Dim SmLookUp As Object
Dim Attr As Object
Dim SmConfig As Object
Dim ValueYN As String
Dim Key As String
Dim DefaultValue
Dim SmClassAttribute As ISmClassAttribute
Dim SmSequence As ISmSequence
Dim SmClass As ISmClass
Dim IsCFO As Boolean
'------------------------------------
'Add for Toolbox integration 
Dim VaultLngPtr As Long
Dim VaultId As Long
'------------------------------------

On Error GoTo ErrHandler

'------------------------------------------------------
'Declare SmSession
Set SmSession = SCREXT_ObjectForInterface(ApplHndl)
CONV_RecListToComRecordList FirstPar,Record1 
CONV_RecListToComRecordList SecondPar,Record2
CONV_RecListToComRecordList ThirdPar,Record3

'----------------------------------------------------------------------------------
'Add for Toolbox integration 
' Get the Vault id (Toolbox integration)
VaultId = Record1.ValueAsInteger("VAULT_OBJECT_ID", 0)
Print "VaultId=" + Str(VaultId)
'-----------------------------------------------------------------------------------

'Retrieve information from GetPrivateProfileString
'Information read in SmTeam32.ini
Set SmConfig = SmSession.Config
SmConfig.IniFileName = "SMTEAM32.INI"
Key = "$Admin\CATIA\SaveWithUniqueFileName"
DefaultValue = "NO"
ValueYN = SmConfig.ReadValueAsString(Key,DefaultValue)
'------------------------------------------------------

'ViewPort.Open "Debug window",3000,3000,500,500
'ViewPort.Clear
Print "Script Started"
Print "The Operation (SelectOp) is: "+SelectOp

strDescriptionField="TDM_DESCRIPTION"
Print "strDescriptionField: "+ strDescriptionField
strIDField="TDM_ID"
Print "strIDField: "+ strIDField

' Set the Class ID
ClassId = -32767
If Record1.HeaderIndex("CLASS_ID") <> -1 Then 
	ClassId = Record1.ValueAsSmallInt("CLASS_ID",0)
	If ClassId = -32767 Then GoTo ErrHandler
End If

' Set the ID attribute
cn_id = ""
If Record1.HeaderIndex(strIDField) = -1 Then 
	strIDField = "CN_ID"
End If

If Record1.HeaderIndex(strIDField) <> -1 Then 
	cn_id = Record1.ValueAsString(strIDField,0)
End If

If ClassId = -32767 Or Len(cn_id) = 0 Then	  
	Set SmClassAttribute = SmSession.MetaInfo.SmClass(ClassId).Attributes.ItemByName(strIDField)
	If Not SmClassAttribute Is Nothing Then 
		Set SmSequence = SmSession.ObjectStore.Sequences.ItemByAttribute(SmClassAttribute)
	End If
	If Not SmSequence Is Nothing Then
		'Print "GetNextMask RetCode: "+cstr(RetCode)
		'Print "Before_Value: "+Before_Value
		'Print "After_Value: "+After_Value
		cn_id = SmSequence.IncrementGlobal
		Record3.AddRecord
		Record3.AddHeader strIDField, 255, TDMT_CHAR
		Record3.Value(strIDField, 0) = cn_id
		'Print "RECLST_AddElValAsPChar RetCode: "+cstr(RetCode)
	End If
End If

'Modified for Toolbox integration
' Set Directory attribute
' Chchange for Toolbox integration 
'from
'If RetCode = Err_None Then
'to
If VaultId = NULL_OBJ_ID Then
 	Directory = Record1.Value(NM_DIRECTORY, 0)
  	'Directory = SCREXT_StringAlloc(ApplHndl,255)
	'RetCode = RECLST_GetValueByNameAsPchar(ApplHndl,FirstPar,NM_DIRECTORY,0,Directory,255,"")
	If Trim(Directory) = "" Then
    	WorkingDir = SmSession.Config.Value(CFGUSERWORKINGDIRECTORY)
		Record3.AddHeader NM_DIRECTORY, 255, TDMT_CHAR
		Record3.Value(NM_DIRECTORY, 0) = WorkingDir
  End If
End If
Print strIDField+": " + After_Value

' Check if the FileName is specified
file_name =""
If Record1.HeaderIndex("FILE_NAME") <> -1 Then 
	file_name =	Record1.Value("FILE_NAME",0)
End If

Print "file_name: "+file_name
If Len(file_name) = 0 Then
	File_Name = cn_id
	Print "No File Name specified !"
	Print "File Name set to ID: "+cn_id
End If

file_ext = GetFileExtension(File_Name)
Print "file_ext: "+ file_ext

' Fix the file's extension
tmp_file_name = StripFileExtension(File_Name)
Print "tmp_file_name: "+ tmp_file_name

' Get the file type
File_Type = Record1.ValueAsInteger(NM_FILE_TYPE,0)
Print "FileType=" + Str(File_Type)

IsCFO = Record1.ValueAsBoolean("TDM_CFO_FLAG", 0)

' Get the object's file type
Set Attr = SmSession.MetaInfo.SmClass(classId).Attributes.ItemByName("FILE_TYPE") 
If Not Attr Is Nothing Then
   	RefClassId = Attr.ReferencedClassId
    Set SmLookUp = SmSession.ObjectStore.GetSmLookUp(RefClassId, file_type)
	If Not SmLookUp Is Nothing  Then
 		FileTypeStr = SmLookUp.Name
 		' Get the SolidWorks file types
  		If FileTypeStr = "SolidWorks Assembly" Then
			If lcase(file_ext) <> "asm" Then
				file_name = tmp_file_name + ".sldasm"
			End If
  		ElseIf FileTypeStr = "SolidWorks Part" Then
			If lcase(file_ext) <> "prt" Then
				file_name = tmp_file_name + ".sldprt"
			End If
  		ElseIf FileTypeStr = "SolidWorks Drawing" Then
			If lcase(file_ext) <> "drw" Then
				file_name = tmp_file_name + ".slddrw"
			End If
  		' Get the Solid Edge file types
  		ElseIf FileTypeStr = "Solid Edge Part" Then
			If lcase(file_ext) <> "par" Then
				file_name = tmp_file_name + ".par"
			End If
  		ElseIf FileTypeStr = "Solid Edge Assembly" Then
			If lcase(file_ext) <> "asm" Then
				file_name = tmp_file_name + ".asm"
			End If
  		ElseIf FileTypeStr = "Solid Edge SheetMetal" Then
			If lcase(file_ext) <> "psm" Then
 				file_name = tmp_file_name + ".psm"
 			End If
  		ElseIf FileTypeStr = "Solid Edge Draft" Then
  			If lcase(file_ext) <> "dft" Then
  				file_name = tmp_file_name + ".dft"
  			End If
  		ElseIf FileTypeStr = "Solid Edge Weldment" Then
  			If lcase(file_ext) <> "pwd" Then
  				file_name = tmp_file_name + ".pwd"
  			End If
		' Get the AutoCad file types
 		ElseIf FileTypeStr = "MCAD 4 Assembly" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dwg"
			End If
		ElseIf FileTypeStr = "MCAD 4 Part" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dwg"
			End If
 		ElseIf FileTypeStr = "MCAD Assembly" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dwg"
			End If
		ElseIf FileTypeStr = "MCAD Part" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dwg"
			End If
		ElseIf FileTypeStr = "AutoCAD" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dwg"
			End If
		ElseIf FileTypeStr = "AutoCAD 2000" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dwg"
			End If
		' Get the Inventor file types
		ElseIf FileTypeStr = "Inventor Part" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".ipt"
			End If
  		ElseIf FileTypeStr = "Inventor Assembly" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".iam"
			End If
		ElseIf FileTypeStr = "Inventor Drawing" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".idw"
			End If
		ElseIf FileTypeStr = "Inventor Presentation" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".ipn"
			End If
		' Get the MicroStation file types
		ElseIf FileTypeStr = "MicroStation Part" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dgn"
			End If
		ElseIf FileTypeStr = "MicroStation Assembly" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dgn"
			End If
		ElseIf FileTypeStr = "MicroStation Drawing" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".dgn"
			End If
		' Get the Microsoft office file types
		ElseIf FileTypeStr = "Microsoft Word" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".doc"
			End If
		ElseIf FileTypeStr = "Microsoft Excel" Then
			If file_ext = "" Then
				file_name = tmp_file_name + ".xls"
			End If
		End If
		' Get the CATIA file types
		If FileTypeStr = "CATPart" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATPart"
		ElseIf FileTypeStr = "CATProduct" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes" And Not IsCFO) Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATProduct"
		ElseIf FileTypeStr = "CATDrawing" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes" And Not IsCFO) Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATDrawing"
		ElseIf FileTypeStr = "CATProcess" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATProcess"
		ElseIf FileTypeStr = "model" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".model"
		ElseIf FileTypeStr = "CATMaterial" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATMaterial"
		ElseIf FileTypeStr = "CATAnalysis" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATAnalysis"
		ElseIf FileTypeStr = "catalog" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".catalog"
		ElseIf FileTypeStr = "cgr" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".cgr"
		ElseIf FileTypeStr = "CATNCCode" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".NCCode"
		ElseIf FileTypeStr = "CATShape" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATShape"
		ElseIf FileTypeStr = "CATAnalysisResults" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATAnalysisResults"
		ElseIf FileTypeStr = "CATAnalysisInput" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATAnalysisInput"	
		ElseIf FileTypeStr = "CATAnalysisComputations" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATAnalysisComputations"	
		ElseIf FileTypeStr = "CATScript" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATScript"	
		ElseIf FileTypeStr = "CATRaster" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATRaster"
		ElseIf FileTypeStr = "CATDrawing_Sheet" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes" And Not IsCFO) Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATDrawing"				
		ElseIf FileTypeStr = "CATFct" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".CATFct"				
		ElseIf FileTypeStr = "act" Then
				If (lcase(tmp_file_name) <> lcase(cn_id) And lcase(ValueYN) = "yes") Then
					tmp_file_name = cn_id
				End If
				file_name = tmp_file_name + ".act"
		ElseIf FileTypeStr = "tlp" Then
				file_name = tmp_file_name + ".tlp"
		ElseIf FileTypeStr = "html" Then
				file_name = tmp_file_name + ".htm"
		ElseIf FileTypeStr = "aptsource" Then
				file_name = tmp_file_name + ".aptsource"
		End If
		Print "file_name: "+file_name
	Else	
		SetDescription = RetCode
		Exit Function
	End If
End If
Record3.AddHeader "FILE_NAME", 255, TDMT_CHAR
Record3.Value("FILE_NAME", 0) = File_Name

' Check if Description is already specified
If Record1.HeaderIndex(strDescriptionField) = -1 Then 
	strDescriptionField = "CN_DESCRIPTION"
End If

If Record1.HeaderIndex(strDescriptionField) <> -1 Then 
	description	= Record1.ValueAsString(strDescriptionField, 0)
	If (description = "" Or description = "-") Then
		' Set the Description attribute
		Record3.AddHeader strDescriptionField, 255, TDMT_CHAR
		Record3.ValueAsString(strDescriptionField, 0) = tmp_file_name
	Else
		SetDescription = RetCode
	End If
End If

Print "description: "+ description

' Set the component name attribute
CompNameAttr = "TDM_COMPONENT_NAME"
CompTypeAttr = "CN_COMPONENT_TYPE"
'Print "MTINFO_IsAttrInClass RetCode: "+cstr(RetCode)

'If RetCode = Err_None Then
Print "ClassId = " & ClassId
Set SmClass = SmSession.MetaInfo.SmClass(ClassId)

If Not SmClass.AttributeExists(CompNameAttr) Then 
	CompNameAttr = "CN_COMPONENT_NAME"
End If 

If SmClass.AttributeExists(CompNameAttr) Then 
	CompName = Record1.ValueAsString(CompNameAttr, 0)
	Print "CompName = " & CompName
	' Check the component type attribute
	IsExternalMDT = False
	If SmClass.AttributeExists(CompTypeAttr) Then 
		CompType = Record1.ValueAsString(CompTypeAttr, 0)
		Print "CompType = " & CompType
		Set Attr = SmClass.Attributes.ItemByName(CompTypeAttr)
		If Not Attr Is Nothing Then
	        RefClassId = Attr.ReferencedClassId
			If (RefClassId > 0 And CompType <> "") Then  
				Set SmLookup = SmSession.ObjectStore.GetSmLookUp(RefClassId, CompType)
				If Not SmLookup Is Nothing Then
					CompTypeStr = SmLookup.Name
			   		If (CompTypeStr = "Part") Or (CompTypeStr = "Assembly") Then
			   			IsExternalMDT = True
			   		End If
				End If
				If (IsExternalMDT = True ) Then	'And Len(CompName) > 0 And CompType = ""
 					Record3.AddHeader CompNameAttr, 255, TDMT_CHAR
					Record3.Value(CompNameAttr, 0) = UCase$(tmp_file_name)
				Else
					CompName = ""	'non-MDT CFO FType mapped to MDT class
					SetDescription = Err_None
				End If
			End If
		End If
	End If
End If

RetCode = Err_None

CONV_ComRecListToRecordList Record3,ThirdPar 

Print "SetDescription: "+cstr(RetCode)

SetDescription = RetCode

Exit Function

ErrHandler:
	Dim Params As ISmStrings
	Set Params = SmSession.NewSmStrings()

    Params.Add "SetDescription"
   	Params.Add "SetDescription."
    Params.Add Err.Description
	ShowSmErrorMessage SmSession, 28503, Params 'ERR_IN_FUNCTION = 28503

End Function





