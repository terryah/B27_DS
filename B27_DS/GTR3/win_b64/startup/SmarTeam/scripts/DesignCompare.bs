Option Explicit
'****************************************************************************************** *
' Script launch the Design Compare function from Smarteam 									*	
'																							*
' Author: Dassault-Systemes																	*
' Called By: User defined tool																*
'																							*
' (C) Copyright Dassault Systemes 2009   All Rights Reserved								*
'****************************************************************************************** *
'*  Script Purpose:                                                                         *
'*                                                                                          *
'*  1.  Allow to open at the same moment different versions of CATIA Parts/ CATIA Products  *
'*  2.  Two vizualisations: Two revisons are loaded seperately 								*
'*           				Two revisions are loaded under new product                      *
'*  3.  Allows modifications on the Checked-Out Version                                     *
'*                                                                                          *
'****************************************************************************************** *
'-----------------------------------------------------------------------------
' History:
' 
' 2009/02/04 KXI : Created
' 2009/04/06 KXI : Modified
' 2009/07/08 KXI : Modified
' 2009/08/31 KXI : Modified
' 2010/03/22 KXI : Modified
' 2010/04/19 KXI : Modified
' 2011/01/19 KXI : Modified
' 
' ----------------------------------------------------------------------------
' This Script contains
' 	 Common Functions for Part and Product : Show Error Message, PopUpDialog , Extract_FileName , Extract_FileType
'	 Functions for Product:	Retrieve_Children ,CopyFileForProduct
'    Functions for Part   :	CopyFileForPart					

' *************************************************** Common Functions Starts *********************************************

' Global Variables
  
  ' DeleteTempFolder: This variable will decide whether to delete temperary folders created for Design Compare.
  '  				: 1 = Delete Temp folder
  '					: 0 = Do not delete Temp Folder
      
 ' Dim DeleteTempFolder As Integer
   Const DeleteTempFolder = 1
Sub ShowSmErrorMessage(SmSession As ISmSession, MessageCode As Integer, Params As ISmStrings)
	
	Dim GUIServices As ISmGUIServices
	Dim SmButtons As Object
	Dim MESSCOMMON
	Dim Result

	MESSCOMMON = 1

	Set SmButtons = SmSession.NewVariantList()
	SmButtons.Value("0") = tmbOk
	Set GUIServices = SmSession.GetService("SmGUISrv.SmGUIServices")

	Result = GUIServices.GUIStore.DisplayMessage(MESSCOMMON, MessageCode, 2, 0, SmButtons, Params)	
End Sub

Function PopUpDialog(SmSession As ISmSession, MessageCode As Integer)As Integer
	
	Dim GUIServices As ISmGUIServices
	Dim SmButtons As Object
	Dim MESSCOMMON
	Dim Result
	Dim ErrERROR As Integer
	Dim Params As ISmStrings

	ErrERROR = 0
	MESSCOMMON = 1

	Set SmButtons = SmSession.NewVariantList()
	SmButtons.Value("0") = tmbyes
	SmButtons.Value("1") = tmbno
	Set GUIServices = SmSession.GetService("SmGUISrv.SmGUIServices")

	Result = GUIServices.GUIStore.DisplayMessage(MESSCOMMON, MessageCode, ErrERROR, 0, SmButtons, Params)
	
	PopUpDialog = Result		
End Function



'-------------------------------------------------------------------------
' Function
' Name        : Extract_FileName
' Description : To Retireve FileName from Fullpath. 
'
' Input       : Fullpath      : Full file path of document
'            
' Output      : Extract_FileName :  File Name.
'
'-------------------------------------------------------------------------


Function Extract_FileName(Fullpath As String) As String
	
	Dim UserString As String
	
	UserString = FullPath
    Dim Increment1 As Integer
    Dim Temp_String As String
    Dim Length As Integer
    Length = Len(UserString)
    Increment1 = InStr(UserString, "\")
    While Increment1 <> 0
        UserString = Right(UserString, Length - Increment1)
        Length = Len(UserString)
        Increment1 = InStr(UserString, "\")
    Wend
    Temp_String = UserString
    Length = Len(Temp_String)
    Increment1 = InStr(Temp_String, ".")
    While Increment1 > 0
        UserString = Right(UserString, Length - Increment1)
        Length = Len(UserString)
        Increment1 = InStr(UserString, ".")
    Wend
    Increment1 = Len(Temp_String) - Len(UserString) - 1
    If Increment1 > 0 Then UserString = Left(Temp_String, Increment1)
    Extract_FileName = UserString

End Function

'-------------------------------------------------------------------------
' Function
' Name        : Extract_FileType
' Description : To Retireve File Extension from Fullpath. 
'
' Input       : Fullpath      : Full file path of document
'            
' Output      : Extract_FileType :  File Extension.
'
'-------------------------------------------------------------------------

Function Extract_FileType(Fullpath As String) As String
	
	Dim UserString As String

    UserString = Fullpath
    Dim Increment1 As Integer
    Dim Temp_String As String
    Dim Length As Integer
    Length = Len(UserString)
    Increment1 = InStr(UserString, "\")
    While Increment1 > 0
        UserString = Right(UserString, Length - Increment1)
        Length = Len(UserString)
        Increment1 = InStr(UserString, "\")
    Wend
    Temp_String = UserString
    Length = Len(Temp_String)
    Increment1 = InStr(Temp_String, ".")
    While Increment1 <> 0
        UserString = Right(UserString, Length - Increment1)
        Length = Len(UserString)
        Increment1 = InStr(UserString, ".")
    Wend
    Increment1 = Len(Temp_String) - Len(UserString) - 1
    Extract_FileType = UserString

End Function

' *************************************************** Common Function Ends *********************************************

' *************************************************** Functions for Product Starts *********************************************

'-------------------------------------------------------------------------
' Function
' Name        : Retrieve_Children
' Description : To Retireve All Children of an Object and copies to temp 
'
' Input       : SmSession      : Current SmSession
'               SmObject       : Object whose children to be calculated
'            
' Output      : Retrieve_Children : 0  -> Children don't exist
'                                	Other than 0 -> Children successfully copied
'
'-------------------------------------------------------------------------

Function Retrieve_Children(CATIA As Object ,Root As ISmObject, SmSession As SmApplic.ISmSession, _
                           TempDir As String) As Integer

	Dim Children As Object
	Dim QueryDefinition As SmApplic.ISmQueryDefinition
	Dim Child1() As SmApplic.ISmObject
	Dim i As Integer
	Dim Increment2 As Integer
	Dim fso As Object
	Dim Fullpath As String

   	Increment2 = 0
	Set fso = CreateObject("Scripting.FileSystemObject")
    
	Set QueryDefinition = Nothing
    Set Children = Root.RetrieveChildren(QueryDefinition)	
    If Children.Count < 1 Then Exit Function
 
    ReDim Child1(1 To Children.Count)

    For i = 1 To Children.Count	    
        Set Child1(i) = SmSession.ObjectStore.RetrieveObject(Children.Item(i - 1).Data.Value("CLASS_ID"), Children.Item(i - 1).Data.Value("OBJECT_ID"))		
        Fullpath =  TempDir & "\" & Child1(i).Data.ValueAsString("CAD_REF_FILE_NAME")
        
		If fso.FileExists(Fullpath) Then		
            Kill Fullpath
        End If
   
        Child1(i).CopyFileFromVault Child1(i).Data.ValueAsString("CAD_REF_FILE_NAME"), TempDir & "\"	

        Increment2 = Retrieve_Children(CATIA ,Child1(i), SmSession,TempDir )        
    Next

    Increment2 = Increment2 + Children.Count
    Retrieve_Children = Increment2

End Function

'-------------------------------------------------------------------------
' Function
' Name        : CopyFileForProduct
' Description : This function performs Sendto on selected Product from temp to Work 
'
' Input       : SmSession      : Current SmSession
'               CATIA          : CATIA Object
' 				COMFirstList   : RecordList containg all selected Object
'
' Output      : CopyFileForProduct : Empty  -> Copy File failed
'                                	Other than Empty-> Copy File Succeeded

'-------------------------------------------------------------------------
Function CopyFileForProduct(CATIA As Object, SmSession As SmApplic.ISmSession, _
		                        COMFirstList As SmRecList.ISmRecordList, level As Integer , TempDir As String) As String
	Dim ChildrenCount As Integer
	Dim Increment As Integer
	Dim RevisionNum As String
	Dim FirstObject As SmApplic.ISmObject
	Dim tempstring As String
	Dim Send As Object
	Dim Product1 As Object
	Dim oWillBeCopied()
	Dim i As Integer
	Dim j As Integer
	Dim FileName As String
	Dim ProductDoc As Object
	Dim FullPath As String
	Dim FSO As Object
	
	 On Error GoTo ERR_CANCEL	

    Set FirstObject = SmSession.ObjectStore.ObjectFromData(COMFirstList.GetRecord(level), True).Clone
    Set FirstObject = SmSession.ObjectStore.RetrieveObject(FirstObject.Data.ValueAsString("CLASS_ID"), FirstObject.Data.ValueAsString("OBJECT_ID"))

	' Tests if CO or New Released
	' If Document is already CheckedOut,return its full path

    If FirstObject.Data.Value("STATE") = 2 Then
        CopyFileForProduct = FirstObject.Data.ValueAsString("DIRECTORY") & FirstObject.Data.ValueAsString("FILE_NAME")
        Exit Function
    Else	  	
	  	On Error Resume Next         

	  	RevisionNum = FirstObject.Data.ValueAsString("REVISION")       
        ChildrenCount = Retrieve_Children(CATIA ,FirstObject, SmSession, TempDir)
		FileName = FirstObject.Data.ValueAsString("CAD_REF_FILE_NAME")

		'Copy the File From the Vault to a temporary folder
         FirstObject.CopyFileFromVault FileName,TempDir & "\"  
	                
		 ' SendTo from temp To work folder  

		Set Send = CATIA.CreateSendTo()
        Send.SetInitialFile TempDir & "\" & FileName
        ReDim oWillBeCopied(0)
		
		'Move all the files And To change their names.
        Send.GetListOfDependantFile oWillBeCopied
        Increment = UBound(oWillBeCopied)		
        ReDim tempchar(Increment)
        
		For i = LBound(oWillBeCopied) To Increment
        	tempstring = oWillBeCopied(i)	    							
			tempchar(i) = Extract_FileName(tempstring) & "_" & RevisionNum					
			Send.SetRenameFile oWillBeCopied(i), tempchar(i)
        Next              
	
	    Send.SetDirectoryFile TempDir 
	 	Send.Run	 		   
        Set Send = Nothing

    	Set FSO = CreateObject("Scripting.FileSystemObject")    					        
		For j = LBound(oWillBeCopied) To Increment
			tempstring = oWillBeCopied(j)
			FullPath = TempDir & "\" & tempchar(j) & "." & Extract_FileType(tempstring)
			'Set Part Number On each Document
			If FSO.FileExists(FullPath) Then
 				Set ProductDoc = CATIA.Documents.Read(FullPath)
				If ProductDoc Then
					Set Product1 = ProductDoc.Product
					Product1.PartNumber = Product1.PartNumber & "_" & RevisionNum
					ProductDoc.SaveAs FullPath
					ProductDoc.Close
				End If
			End If
		Next

        On Error Resume Next               
        
		' Return Correct FilePath
		tempstring = oWillBeCopied(0)		
	   	CopyFileForProduct = TempDir & "\" & tempchar(0) & "." & Extract_FileType(tempstring)
        Set FirstObject = Nothing

        Exit Function
    End If

ERR_CANCEL:    
    CopyFileForProduct = Empty	

End Function

' *************************************************** Functions for Product Ends *********************************************

' *************************************************** Functions for Part Starts **********************************************

'-------------------------------------------------------------------------
' Function
' Name        : CopyFileForPart
' Description : This function copies selected Parts from vault to Work 
'
' Input       : SmSession      : Current SmSession
'               CATIA          : CATIA Object
' 				COMFirstList   : RecordList containg all selected Object

' Output      : CopyFileForPart : Empty  -> Copy File failed
'                                Other than Empty-> Copy File Succeeded
'
'-------------------------------------------------------------------------


Public Function CopyFileForPart(CATIA As Object, SmSession As SmApplic.ISmSession, _
		                               COMFirstList As SmRecList.ISmRecordList,level As Integer , TempDir As String) As String
	Dim RevisionNum As String
	Dim FileName As String
	Dim FullPath As String	
	Dim FirstObject As SmApplic.ISmObject	
	Dim CatiaDoc As Object
	Dim Product As Object
	Dim FSO As Object

    On Error GoTo ERR_CANCEL
    Set FirstObject = SmSession.ObjectStore.ObjectFromData(COMFirstList.GetRecord(level), True).Clone
    Set FirstObject = SmSession.ObjectStore.RetrieveObject(FirstObject.Data.ValueAsString("CLASS_ID"), FirstObject.Data.ValueAsString("OBJECT_ID"))	
	
	
	'Tests if CO
	' If Document is already CheckedOut or New Released , return its full path
    If FirstObject.Data.Value("STATE") = 2	Then
        CopyFileForPart = FirstObject.Data.ValueAsString("DIRECTORY") & FirstObject.Data.ValueAsString("FILE_NAME")
        Exit Function
    Else	
		
		RevisionNum = FirstObject.Data.ValueAsString("REVISION")
    	FileName = Extract_FileName(FirstObject.Data.ValueAsString("CAD_REF_FILE_NAME")) & "_" & RevisionNum & "." & Extract_FileType(FirstObject.Data.ValueAsString("CAD_REF_FILE_NAME"))		
		
	    'Check if file already exists
    	Set FSO = CreateObject("Scripting.FileSystemObject")
    	FullPath =  TempDir & "\" & FileName
    	If FSO.FileExists(FullPath) Then
        	FSO.DeleteFile FullPath, True
    	End If 
		    
   		FirstObject.CopyFileFromVault FileName, TempDir		
		
    	' Modify Part Number for each Doc
		If FSO.FileExists(FullPath) Then					
			Set CatiaDoc = CATIA.Documents.Read(FullPath)			
			Set Product = CatiaDoc.Product
			Product.PartNumber = Product.PartNumber & "_" & RevisionNum
			Product.Update	
			CatiaDoc.SaveAs FullPath
   			CatiaDoc.Close			
		    CopyFileForPart = FullPath
		End If
    	Exit Function
	End If	
	

ERR_CANCEL:    
    CopyFileForPart = Empty

End Function

' *************************************************** Functions for Part Ends **********************************************

'-------------------------------------------------------------------------
' Function
' Name        : DesignCompare (Main Function)
' Description : This function Opens slected Documents in CATIA ( As per comparison choice selected by user)
'
' Input       : ApplHndl   : Current SmSession'               
' 				FirstPar   : RecordList containg all selected Object
'
' Output      : DesignCompare : 1  -> DesignCompare failed
'                               0  -> DesignCompare Succeeded
'
'-------------------------------------------------------------------------


Function DesignCompare(ApplHndl As Long, OpStr As String, FirstPar As Long, SecondPar As Long, ThirdPar As Long) As Integer

    Dim SmSession As ISmSession
	Dim RecListIn As Object    
    Dim FirstObject As SmApplic.ISmObject
    Dim Increment As Integer
    Dim CatiaDocument As Object
    Dim CatiaDocuments As Object
    Dim ObjId As Long
	Dim ClassId As Integer
	Dim ClassName As String	
	Dim arrayOfVariantOfBSTR1(0)
    Dim RevisedPart_Path() As String
    Dim Object1 As SmApplic.ISmObject
    Dim Object2 As SmApplic.ISmObject
    Dim products1 As Object
    Dim products1Variant As Object
    Dim Doc As Object
    Dim Status As Integer
    Dim Count1 As Integer
    Dim Count2 As Integer
    Dim FileName As String
    Dim CATIA As Object
    Dim FileName_Obj As String
    Dim Product As Object    
    Dim viewer3D1 As Object
    Dim specsAndGeomWindow1 As Object    
    Dim TempDoc As Object 
    Dim FileType As String
	Dim Result As Object
	Dim VbObj As Object
	Dim CATRel As Integer
	Dim CATSP As Integer
	Dim Params As ISmStrings
	Dim TypeLib As Object
	Dim NewGUID As String
	Dim DCDir As String
	Dim GUIDDir As String
	Dim fso As Object 
    
    On Error Resume Next 
	
	Set Params = Nothing	
    DesignCompare = 1
	    
	Set SmSession = SCREXT_ObjectForInterface(ApplHndl)	  
    Set CATIA = GetObject(, "CATIA.Application")
    '===========================================
    'If CATIA is not launched, the user is advised of it
    If CATIA Is Nothing Then
	    ShowSmErrorMessage SmSession,07888, Params
        'Please Launch Catia First
        GoTo Err_Fail     
    End If
    '===========================================

	' Arrange windows
	'CATIA.Windows.Arrange 2
	CATIA.DisplayFileAlerts = False	
	
	   		                      

   	'Initialize objects
	Set VbObj = CATIA.GetItem("CATStiVB")
	Set CatiaDocuments = CATIA.Documents   

	'Convert RecordList parameters to SmRecordLists
	CONV_RecListToComRecordList FirstPar, RecListIn 				           	    	
	
	On Error GoTo Err_Fail
    Set Object1 = SmSession.ObjectStore.ObjectFromData(RecListIn.GetRecord(0), True).Clone
   	Set Object1 = SmSession.ObjectStore.RetrieveObject(Object1.Data.ValueAsString("CLASS_ID"), Object1.Data.ValueAsString("OBJECT_ID"))

    'Test If the Document selected is Obsolete, we Do Not support Design Compare for them.	
	If Object1.Data.Value("STATE") = 4 Then
	   ShowSmErrorMessage SmSession,07905, Params	 
	   GoTo Err_Fail
    End If


    FileName_Obj = Object1.Data.ValueAsString("CAD_REF_FILE_NAME")
	FileType = Extract_FileType(FileName_Obj)
	If (FileType <> "CATPart" And FileType <> "CATProduct") Then
		'This command Is available only For CATIA Products And CATIA Parts only
		ShowSmErrorMessage SmSession,07897, Params
		GoTo Err_Fail
	End If 	  

	' We are having 1st selected Object.
	' We will check If any revision of that object is already open in CATIA Editor.
    Count1 = CatiaDocuments.Count	
    If Count1 > 0 Then	   
        On Error Resume Next		
        Set Doc = CatiaDocuments.Item(FileName_Obj)		
	End If 

   	'Checks If there are at least two objects in the selection
	'If only 1 Object is selected from ST, there can be two cases
	'  	1. Other revision of selected Object is already Open in CATIA
	'  	2. No Revision is Open in CATIA, Wrongly selected one object => We exit for this case   

	If (RecListIn.RecordCount < 2) And (Doc Is Nothing) Then
       	 'Please Select at least two SmarTeam Object.
		 ShowSmErrorMessage SmSession,07898, Params
		 GoTo Err_Fail   
   	End If

   	'Checks if all the selected objects have the same TDM_ORIGIN_ID	  
	'===========================================
	'If 1 Revision is Open in CATIA Editor,we get its Object ID and check whether user has selected same revision for comparison.
    ObjId = 0
    If Not(Doc Is Nothing) Then 
		ObjId = VbObj.GetObjectID(Doc)
    End If
    
    If RecListIn.RecordCount < 2 And Not(Doc Is Nothing) Then
		If Object1.Data.ValueAsString("OBJECT_ID") = ObjId Then 
			'Same revisions are selected For comparison.
		 	ShowSmErrorMessage SmSession,07899, Params
		 	GoTo Err_Fail   			
		Else
			ClassId = Object1.Data.ValueAsString("CLASS_ID")	
		    Set Object2 = SmSession.ObjectStore.RetrieveObject(ClassId,ObjId)
        	If Object1.Data.ValueAsString("TDM_ORIGIN_ID") <> Object2.Data.ValueAsString("TDM_ORIGIN_ID") Then
            	'Please Select two revisions of Same Object
				ShowSmErrorMessage SmSession,07900, Params
            	GoTo Err_Fail
        	End If

			'Test If the Document selected is Obsolete, we Do Not support Design Compare for them.
			If Object2.Data.Value("STATE") = 4 Then
	        	ShowSmErrorMessage SmSession,07900, Params
				GoTo Err_Fail
			End If				    
		End If
	Else
	  	For Increment = 0 To RecListIn.RecordCount - 1			
        	Set Object2 = SmSession.ObjectStore.ObjectFromData(RecListIn.GetRecord(Increment), True).Clone
			Set Object2 = SmSession.ObjectStore.RetrieveObject(Object2.Data.ValueAsString("CLASS_ID"), Object2.Data.ValueAsString("OBJECT_ID"))

			If Object2.Data.ValueAsString("OBJECT_ID") = ObjId Then 
				'Same revisions are selected For comparison.
			 	ShowSmErrorMessage SmSession,07899, Params
			 	GoTo Err_Fail
			End If   			


        	If Object1.Data.ValueAsString("TDM_ORIGIN_ID") <> Object2.Data.ValueAsString("TDM_ORIGIN_ID") Then
            	'Please Select two revisions of Same Object
				ShowSmErrorMessage SmSession,07900, Params
				GoTo Err_Fail
        	End If
			
		    'Test If the Document selected is Obsolete, we Do Not support Design Compare for them.			
			If Object2.Data.Value("STATE") = 4 Then
	        	ShowSmErrorMessage SmSession,07905, Params
				GoTo Err_Fail
			End If 					 
    	Next
	End If

	' All Checks Are over
    '===========================================          
    
    'Perform all the operations and returns the FilePath
    '===========================================    

	DCDir =  SmSession.Config.Value("$Admin\Directory Structure\ReadOnlyDir") & "\" & "DC"
	
    Set fso = CreateObject("Scripting.FileSystemObject")		
	If fso.FolderExists(DCDir) Then
       If (DeleteTempFolder = 1) Then	  
	          fso.DeleteFolder DCDir
    	      fso.CreateFolder DCDir
	   End If
    Else 
        fso.CreateFolder DCDir  
	End If
	 	  	 
	ReDim RevisedPart_Path(RecListIn.RecordCount - 1)   
   	For Increment = 0 To RecListIn.RecordCount - 1
	    ' Create GUID and append DCDir with GUID.  
 	    Set TypeLib = CreateObject("Scriptlet.TypeLib")	   	    
 		NewGUID = TypeLib.Guid
		Set TypeLib = Nothing   		
		GUIDDir =  DCDir & "\" & (left(NewGUID, Len(NewGUID)-2))		        
		fso.CreateFolder GUIDDir
	    If FileType = "CATPart" Then 						        	
			RevisedPart_Path(Increment) = CopyFileForPart(CATIA, SmSession, RecListIn, Increment,GUIDDir)
        	If RevisedPart_Path(Increment) = Empty Then Count2 = Count2 + 1    	
    	Else		    
            RevisedPart_Path(Increment) = CopyFileForProduct(CATIA, SmSession, RecListIn, Increment,GUIDDir)
            If RevisedPart_Path(Increment) = Empty Then Count2 = Count2 + 1
		End If
    Next
    '===========================================
    If RecListIn.RecordCount = Count2 Then	' All file paths are empty
        GoTo Err_Fail
    End If

	On Error GoTo Err_Fail
    If (RecListIn.RecordCount - Count2) > 1 Then
        Status = PopUpDialog(SmSession,07902)
		'Status = MsgBox("Would you like to load all the revisions under a new product? If you click 'No' the revisions will be loaded in several separate windows!",4 , "For Comparison")        
    Else
        Status = 5
    End If
    CATIA.DisplayFileAlerts = False
		  
    'For Viewing
    '===========================================
    If Status = 5 Then
        For Increment = 0 To RecListIn.RecordCount - 1
            FileName = RevisedPart_Path(RecListIn.RecordCount - 1 - Increment)
            If FileName <> Empty Then 
				Set CatiaDocument = CatiaDocuments.Open(FileName)
			Else
    			'Selected file not found.
				ShowSmErrorMessage SmSession,07901, Params
				GoTo Err_Fail 
			End If
			'Set the Design Compare Flag			 
			Result = VbObj.SetDesignCompareFlag(CatiaDocument,True)	

            FileName = Empty
            Set specsAndGeomWindow1 = CATIA.Application.ActiveWindow
    	    Set viewer3D1 = specsAndGeomWindow1.ActiveViewer
        	viewer3D1.Reframe            
        Next
    End If
    '===========================================
    'For Using comparison tools
    '===========================================
    If Status = 4 Then
        CATRel = CATIA.SystemConfiguration.Release
        CATSP = CATIA.SystemConfiguration.ServicePack	

        If (CATRel = 20 And CATSP > 2) Or ( CATRel = 19 And CATSP > 7 ) Or CATRel > 20	Then
            Set TempDoc = VbObj.CreateNewDocumentForDesignCompare
        Else
            Set TempDoc = CatiaDocuments.Add("Product")
        End If 		   
 		   
        Set Product = TempDoc.Product
        Set products1 = Product.Products
        Set products1Variant = products1
        For Increment = 0 To RecListIn.RecordCount - 1
            arrayOfVariantOfBSTR1(0) = RevisedPart_Path(Increment)
            FileName = arrayOfVariantOfBSTR1(0)
            If FileName <> Empty Then				
				products1Variant.AddComponentsFromFiles arrayOfVariantOfBSTR1, "All"
			Else 
    			'Selected file not found.
				ShowSmErrorMessage SmSession,07901, Params
				GoTo Err_Fail 				
			End If
            FileName = Empty                      
        Next
	
		'Set the Design Compare Flag To whole assembly.	
		Result = VbObj.SetDesignCompareFlag(TempDoc,True) 
		        
        On Error Resume Next
        
        Set specsAndGeomWindow1 = CATIA.Application.ActiveWindow
	    On Error GoTo Err_Fail
        If Not specsAndGeomWindow1 Is Nothing Then			    
           	Set viewer3D1 = specsAndGeomWindow1.ActiveViewer
           	viewer3D1.Reframe
        End If
                          
    End If
    '===========================================
    DesignCompare = 0
	
	' Arrange windows
	CATIA.Windows.Arrange 2 	
    
    CATIA.Application.Visible = False
    CATIA.Application.Visible = True

	'ShowSmErrorMessage SmSession,07911, Params,2    
    'Free objects
    Set SmSession = Nothing
    Exit Function

Err_Fail:
    CATIA.Visible = True
    Set CATIA = Nothing
   	ShowSmErrorMessage SmSession,07904, Params
		    
    'Free objects
    Set SmSession = Nothing
    Exit Function

End Function
