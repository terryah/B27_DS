'--------------------------------------------------------------------------
' Valuation of the mass attribute
'--------------------------------------------------------------------------
'
' Usage note :
' Before using this scirpt, be sure that CN_MASS attribute is defined
' on Classes containing Product and Part documents
'
'
' Script relevant if following conditions are met :
'   - Just one document selected
'   - Only on a Product or Part document
'   - Attribute CN_MASS should be defined inside the database
'   - Any state
'
' Simplified algorithm :
'   - Launch or retrieve a CATIA session
'   - Open the selected document in the CATIA session
'   - Retrieve the mass value by a CATIA API
'   - Valuate the mass attribute
'
' Technical notes :
'   If the selected document is located in a vault directory
'   a copy is performed before
'
'     Author: Dassault-Systemes
'
'     Notes: (none)
'
' (C) Copyright Dassault Systemes 2002   All Rights Reserved
'---------------------------------------------------------------------------

Declare Sub CONV_RecListToComRecordList Lib "SmTdm32" (ByVal RecList As Long, ByRef COMRecList As ISmRecordList)
Declare Sub CONV_ComRecListToRecordList Lib "SmTdm32" (ByVal ComRecList As ISmRecordList, ByRef RecList As Long)

Function MassCalculation(ApplHndl As Long,Sstr As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long ) As Integer

  	'Declaration of variable
	Dim COMFirstSmObject As SmApplic.ISmObject
	Dim COMFirstList As Object
	Dim	FileNamePDM As String
	Dim	DirectoryPDM As String
	Dim	FullPathPDM As String
 	Dim Extension As String
	Dim TheSPAWorkbench As Object
	Dim TheInertias As Object
	Dim ProductInertia1 As Object
	Dim product1 As Object
	Dim products1 As Object

	On Error GoTo AssignErrorCode


	MassAttributeName = "CN_MASS"

	'================================Converting procedural script arguments into COM ones================================
		'Get Session from Application handle
   		Set SmSession = SCREXT_ObjectForInterface(ApplHndl)
   		'Convert Record lists to COM Representation
		CONV_RecListToComRecordList	FirstPar,COMFirstList
 '		CONV_RecListToComRecordList	SecondPar,COMSecondList
 '		CONV_RecListToComRecordList	ThirdPar,COMThirdList

	'================================================Get selected object=================================================
		'Impossible case.
		If COMFirstList.RecordCount <= 0 Then
			GoTo NoSelectedObject
		End If

	'===========================Retrieving in the CATIA session the drawing active document==========================
		On Error Resume Next
		Set CATIA = GetObject(,"CATIA.Application")
		'switch-on the default system error
		On Error GoTo 0
 		If (IsEmpty(CATIA) = False) Then  
 			Set CATIA = CreateObject("CATIA.Application")   
		End If
	

	' Retrieving FullPath of the selected object. 
		Set COMFirstSmObject = SmSession.ObjectStore.ObjectFromData(COMFirstList.GetRecord(0),True).Clone
		COMFirstSmObject.Retrieve
		
		FileNamePDM = COMFirstSmObject.Data.ValueAsString("CAD_REF_FILE_NAME")
		DirectoryPDM = COMFirstSmObject.Data.ValueAsString("DIRECTORY")

		' Wrong Type
	  	Extension = GetFileExtension(FileNamePDM)

  		If ( (UCase(Extension) <> UCase("CATProduct")) And (UCase(Extension) <> UCase("CATPart")) )Then
			MsgBox "Wrong File Type"
			Exit Function
		End If

		' State
		Set DocClass = SmSession.MetaInfo.SmClass(COMFirstSmObject.ClassID)
		LookUpclassId = DocClass.Attributes.Item("STATE").ReferencedClassId
		LookUPObjID = COMFirstSmObject.Data.ValueAsInteger("STATE")
		Set LookUPObj = Smsession.ObjectStore.GetSmLookUp(LookUpclassId, LookUPObjID)
		State = LookUPObj.Id
		
		' Retrieve FullPathPDM 
		If ((State = 1) Or (State = 3)) Then
			WorkingDir = SmSession.Config.Value(CFGUSERWORKINGDIRECTORY)
			Set toto = COMFirstSmObject.CopyFileFromVault(FileNamePDM, WorkingDir)
			FullPathPDM = WorkingDir + "\" +	FileNamePDM
		Else
			FullPathPDM = DirectoryPDM + "\" + FileNamePDM
		End If

		' Open doc
		If( UCase(Extension) = UCase("CATProduct") ) Then
	    	Set ProdDoc = CATIA.Documents.Open(FullPathPDM)
		Else
			Set ProdDoc = CATIA.Documents.Add("Product")
	    	Set PartDoc = CATIA.Documents.Open(FullPathPDM)

			Set product1 = ProdDoc.Product

			Set products1 = product1.Products

			Set titi = products1.AddExternalComponent(PartDoc)
		End If

		MsgBox "Mass Calculation..."

		
		Set TheSPAWorkbench = ProdDoc.GetWorkbench("SPAWorkbench")
 		Set TheInertias = TheSPAWorkbench.Inertias
 		Set ProductInertia1 = TheInertias.Add(ProdDoc.Product)

 		COMFirstSmObject.Data.ValueAsString(MassAttributeName) = ProductInertia1.Mass
    	COMFirstSmObject.Update

		' Close documents
		ProdDoc.Close
		If( UCase(Extension) = UCase("CATPart") ) Then
			PartDoc.Close
		End If

	'================================Converting COM Variable into procedural script output===============================
	ExitFunction:
'		CONV_ComRecListToRecordList	COMThirdList,ThirdPar

		MsgBox "The mass is now calculated"
		MassCalculation = Err_None ' the function should return a value
			              	  ' Err_None=0
		Exit Function

 	'==================================================Error management==================================================
	NoSelectedObject:
		MsgBox "No selected object in the CATIA session."
		MassCalculation = Err_Gen
		Exit Function

	AssignErrorCode:
		MsgBox "Error Code on a SmarTeam API. The Script MassCalculation ended."
		MassCalculation = Err_Gen

End Function





'-----------------------------------------------------------------------------
' Get File Extension
'	This function retrieves the Extension of a File Name.
'	If the document has not dot, we retrieve "".
'	If the document has two dots or more we retrieve the Extension
'	from the last one.
' eg : MyExcel.xls1.xls2 => The Extension = xls2
'
' In : FileName As String = Given File Name
' Output : the Extension of the given File Name  
'-----------------------------------------------------------------------------
Function GetFileExtension(FileName As String) As String
	'Declaration of variables.
  	Dim i As Integer
  	Dim DotPos As Integer

  	GetFileExtension = FileName
  	DotPos = 0
  	'Retrieve after the last "." .
  	For i = Len(FileName) To 1 Step -1
    	If (Mid(FileName,i,1) = ".") Then
      		DotPos = i
	  		Print "DotPos = " + cstr(DotPos)
	  		Exit For
    	End If
  	Next i
  	If (DotPos <> 0) Then
    	GetFileExtension = Mid(FileName, DotPos+1, Len(FileName)-DotPos)
		Print "GetFileExtension = " + Mid(FileName, DotPos+1, Len(FileName)-DotPos)
  	Else
		GetFileExtension = ""
  	End If
End Function
