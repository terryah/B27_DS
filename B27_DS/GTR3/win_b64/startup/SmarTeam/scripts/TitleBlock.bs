'---------------------------------------------------------------------------
' Script updating the attributes for the Title Block.
' Only the revision attribute is updated in the title block.
' This step applies to a state "Check out" on Drawing Document on before 
' "Check-In" operation.
' At this time, the drawing document is in a state "Check out"
'
'     Script Requires:  SmarTeam Record List Library
'						CATIA V5 Drafting Interfaces Object Library
'						CATIA V5 Knowledge Interfaces Object Library
'
'	  Attributes required:	REVISION
'							STATE
'							CAD_REF_FILE_NAME
'
'     Author: Dassault-Systemes
'
'     Notes: (none)
'
' (C) Copyright Dassault Systemes 2002   All Rights Reserved
'---------------------------------------------------------------------------


'-----------------------------------------------------------------------------
' MoveIntheVault
' 	Used to move file in the vault
'
'	In : SmSession As Object = The SmarTeam Session
'		 SmObject As Object = the object to return in the vault
'		 SourceDirectory As	String = 
'		 SourceFileName As String = 
'-----------------------------------------------------------------------------
Sub MoveIntheVault(SmSession As Object, SmObject As Object, SourceDirectory As String, SourceFileName As String)
	'--------------------------------------------------	
	'Declaration of variables.
	Dim DestinationVault As Integer
	Dim DestinationDirectory As String
	Dim DestinationFileName As String 
	Dim SmSessionUtil As Object
	Dim KeepLocalCopy As Boolean
	
	KeepLocalCopy = False
	DestinationVault = 0
	On Error Resume Next
	DestinationVault = SmObject.Data.ValueAsInteger("VAULT_OBJECT_ID")
	On Error GoTo 0
	DestinationDirectory = SmObject.Data.ValueAsString("DIRECTORY")
	DestinationFileName = SmObject.Data.ValueAsString("FILE_NAME")	

	Set SmSessionUtil = SmSession.GetService("SmUtil.SmSessionUtil")
	SmSessionUtil.MoveFiletoVault SourceDirectory,SourceFileName,DestinationVault,DestinationDirectory,DestinationFileName,KeepLocalCopy
	
End Sub

Function TitleBlock(ApplHndl As Long,Sstr As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long ) As Integer

  	'Declaration of variable
	Dim COMFirstSmObject As SmApplic.ISmObject
	Dim COMSecondSmObject As SmApplic.ISmObject
	Dim SmSession As SmApplic.SmSession
	Dim COMFirstList As Object
	Dim COMSecondList As Object
	Dim COMThirdList As Object
	Dim State As Integer
	Dim LookUpclassId As Long
	Dim LookUPObj As SmApplic.ISmLookUpObject
	Dim LookUPObjID As Integer
	Dim DocClass As SmApplic.ISmClass
	Dim SmCadInterface As SmCADInterface
	Dim SmIntegration As ISmIntegrationStore
	Dim SmPropertyGroupTypes As SmIntegrationTool.ISmPropertyGroupTypes
	Dim SmPropertyGroupType As SmIntegrationTool.ISmPropertyGroupType
	Dim SmPropertyGroups As SmIntegrationTool.ISmPropertyGroups
	Dim SmPropertyGroup As SmIntegrationTool.ISmPropertyGroup
	Dim SmGroupProperties As SmIntegrationTool.ISmGroupProperties
	Dim SmGroupProperty As SmIntegrationTool.ISmGroupProperty
	Dim SmClassesMappings As SmIntegrationTool.ISmClassesMappings
	Dim SmClassMapping As SmIntegrationTool.ISmClassMapping
	Dim SmClassAttribute As SmApplic.ISmClassAttribute
	Dim NbGroupProperties As Integer
	Dim NbGroupTypes As Integer
	Dim GroupsTypeName As String
	Dim NbGroups As Integer
	Dim GroupProperty As String
	Dim FileNamePDM As String
	Dim DirectoryPDM As String
	Dim FullPathPDM As String
	Dim AttribName As String
	Dim NbClassesMapping As Integer
	Dim myDoc As Object
 	Dim CATIA As Object
	Dim myParamCol As Object 'CATIA V5 parameters
	Dim NbParameters As Integer
	Dim Description As String
	Dim myDocDirty As Boolean
	Dim i As Integer
	Dim j As Integer
	Dim k As Integer
	Dim l As Integer
	Dim Revision As String
	Dim MyPath As String
	Dim DocPath As String
	Dim MyName As String
	Dim NameWithoutExtension As String
	Dim ExtensionPreview As String
	Dim FullPathPreview As String 
	Dim ValueCATIA As String
	Dim f As Object
	Dim fs As Object
	Dim CATIADrawingClass As String
	Dim CN_REVISION As String
	Dim ApplNameCATIA As String

	'==========================================Hard Coded Argument check before Use======================================	
	'Application Name
	ApplNameCATIA = "CATIA"
	'For Title Block
	CATIADrawingClass = "CATIA Drawing"
	CN_REVISION = "REVISION"
	'====================================================================================================================

	On Error GoTo AssignErrorCode
	'================================Converting procedural script arguments into COM ones================================
		'Get Session from Application handle
   		Set SmSession = SCREXT_ObjectForInterface(ApplHndl)
   		'Convert Record lists to COM Representation
		CONV_RecListToComRecordList	FirstPar,COMFirstList
		CONV_RecListToComRecordList	SecondPar,COMSecondList
		CONV_RecListToComRecordList	ThirdPar,COMThirdList

	'================================================Get selected object=================================================
		'Impossible case.
		If COMFirstList.RecordCount <= 0 Then
			GoTo NoSelectedObject
		End If

	'===========================Retrieving in the CATIA session the drawing active document==============================
		'Launch the CATIA session if necessary
		'Switch-off the default system error 
		On Error Resume Next
		Set CATIA = GetObject(,"CATIA.Application")
		'switch-on the default system error
		On Error GoTo 0
		If (IsEmpty(CATIA) = False) Then  
			Set CATIA = CreateObject("CATIA.Application")   
		End If

	'=========================================Retrieving the state of my document========================================
		'State of my document
		'	-2 : no current document
		'	-1 : the document doesn't exist in the Database
		'	 0 : the document is in state: New
		'	 1 : the document is in state: Check In
		'	 2 : the document is in state: Check out
		'	 3 : the document is in state: Approve or Released
		'	 4 : the document is in state: Obsolete
		
		'Retrieving the attributes from first record list
		Set COMFirstSmObject = SmSession.ObjectStore.ObjectFromData(COMFirstList.GetRecord(0),True).Clone
		COMFirstSmObject.Retrieve	

		Set DocClass = SmSession.MetaInfo.SmClass(COMFirstSmObject.ClassID)
		LookUpclassId = DocClass.Attributes.Item("STATE").ReferencedClassId
		LookUPObjID = COMFirstSmObject.Data.ValueAsInteger("STATE")
		Set LookUPObj = Smsession.ObjectStore.GetSmLookUp(LookUpclassId, LookUPObjID)
		State = LookUPObj.Id   
		
		'This step applies on a Check out drawing document
		If ( State <> 2 ) Then
			GoTo ExitFunction	
		End If

		'Retrieving the CAD ref file name
		FileNamePDM = COMFirstSmObject.Data.ValueAsString("CAD_REF_FILE_NAME")
		Set SmCadInterface = SmSession.GetService("SmCad.SmCADInterface")
		SmCadInterface.FindFile LookUPObjID,LookUpclassId,DirectoryPDM
		FullPathPDM = DirectoryPDM+"\"+FileNamePDM

		'Retrieving the attributes from second record list as the value of REVISION attribute 
		On Error Resume Next
		Set COMSecondSmObject = SmSession.ObjectStore.ObjectFromData(COMSecondList.GetRecord(0),True)
		On Error GoTo 0	   
		Revision = COMSecondSmObject.Data.ValueAsString(CN_REVISION)

	'=========================================Valuating the revision in the drawing======================================
		'Retrieve the Revison name
		Set SmIntegration = SmSession.GetService("SmIntegrationTool.SmIntegrationStore")
		'Get CATIA application group types 
		Set SmPropertyGroupTypes = SmIntegration.GetGroupTypesForApplication(ApplNameCATIA)
		NbGroupTypes = SmPropertyGroupTypes.Count
		For i = 0 To NbGroupTypes-1 Step 1
			Set SmPropertyGroupType = SmPropertyGroupTypes.Item(i)
			GroupsTypeName = SmPropertyGroupType.Name
			'GroupsTypeName: CATIA Drawing
			If GroupsTypeName = CATIADrawingClass Then		
				Set SmPropertyGroups = SmPropertyGroupType.Groups
				NbGroups = SmPropertyGroups.Count
				'Groups = 1
				For j = 0 To NbGroups-1 Step 1
					Set SmPropertyGroup = SmPropertyGroups.Item(j)
					'GroupName = Drawing Information
					Set SmGroupProperties = SmPropertyGroup.Properties
					NbGroupProperties = SmGroupProperties.Count
				 	'GroupProperties: number of columns
			  		For k = 0 To NbGroupProperties-1 Step 1
						Set SmGroupProperty = SmGroupProperties.Item(k)
					   	GroupProperty = SmGroupProperty.Name
						Set SmClassesMappings = SmGroupProperty.Mappings
						NbClassesMapping = SmClassesMappings.Count
						For l = 0 To NbClassesMapping-1 Step 1
							Set SmClassMapping = SmClassesMappings.Item(l)
							Set SmClassAttribute = SmClassMapping.Attribute
							'Team PDM Attribut Name: REVISION
							AttribName = SmClassAttribute.Name
							If AttribName = CN_REVISION Then
								'F(X) Properties Name: ValueCATIA
								ValueCATIA = GroupProperty
								GoTo InsertedRevisionValue									
							End If
						Next l
					Next k
				Next j
			End If	
		Next i
		'If we are here we have encountered a problem like no groups type
		GoTo NoRevisionInformation
		
		InsertedRevisionValue:

		'Active the Drawing Document
		Set myDoc = CATIA.ActiveDocument
		'First, we check if the given CATIA document correspond to the selected
		'document, copied somewhere out of the vault.
		For i = 1 To ResultOfQuery.RecordCount
			FullPathPDM = ResultOfQuery.ValueAsString("DIRECTORY",i-1)+"\"+FileNamePDM
			If (UCase(myDoc.FullName) = UCase(FullPathPDM)) Then
				DirectoryPDM = ResultOfQuery.ValueAsString("DIRECTORY",i-1)
				Set myParamCol = myDoc.Parameters
				NbParameters = myParamCol.Count
				GoTo ActivatedDrawing
			End If
		Next i		

		'Retrieve the number of Window(s) in the CATIA Session.
		NumberOfWindowsInCATIA = CATIA.Windows.Count
		For i = 1 To NumberOfWindowsInCATIA 
			Set Window = CATIA.Windows.Item(i)
			Window.Activate
			Set myDoc = CATIA.ActiveDocument
			'First, we check if the given CATIA document correspond to the selected
			'document, copied somewhere out of the vault.
			For j = 1 To ResultOfQuery.RecordCount
				FullPathPDM = ResultOfQuery.ValueAsString("DIRECTORY",j-1)+"\"+FileNamePDM
				If (UCase(myDoc.FullName) = UCase(FullPathPDM)) Then
					DirectoryPDM = ResultOfQuery.ValueAsString("DIRECTORY",j-1)
					Set myParamCol = myDoc.Parameters
					NbParameters = myParamCol.Count
					GoTo ActivatedDrawing
				End If
			Next j
		Next i
		GoTo NoDrawingOpenInSession	
	
	ActivatedDrawing:
		For j = 1 To NbParameters Step 1	
			Description = myParamCol.Item(j).Name
			If Description = ValueCATIA Then
				MsgBox Revision	
				'Insert the revision information in the Drawing
				myParamCol.Item(j).ValuateFromString Revision
				GenerateANewPreview = True	
			End If
		Next j	

	'====================================Save the document and generate a new preview====================================
		'Save the document in the CATIA session and generate a preview
		If GenerateANewPreview = True Then
			myDoc.Save
			'Retrieve all the objects inside the directory where the drawing is
			DocPath = myDoc.FullName
			MyPath = GetDirectory(DocPath)   'Set the path.
			vbNormal = 0
			MyName = Dir(MyPath, vbNormal)  'Retrieve the first entry.  
			Do While MyName <> ""   'Start the loop.   
         		'Test to retrieve the extension:
				' ie FileNamePDM = "Test.CATDrawing"
				'    MyName = "Test.CATDrawing.cgm"
				'	 NameWithoutExtension = "Test.CATDrawing"
				'	 ExtensionPreview = "cgm"
				NameWithoutExtension = StripFileExtension(MyName)
				If (UCase(FileNamePDM) = UCase(NameWithoutExtension)) Then
					ExtensionPreview = GetFileExtension(MyName)
					FullPathPreview = MyPath+MyName 
					'Delete the preview file because we have a message when
					'we do a save as on an existing document
					Set fs = CreateObject("Scripting.FileSystemObject")
    				Set f = fs.GetFile(FullPathPreview)
					f.Delete True		 
					myDoc.ExportData FullPathPreview, ExtensionPreview
					GoTo MoveToVault
				End If
   				MyName = Dir()   'Get next entry.
			Loop
		End If

	MoveToVault:
		'MoveIntheVault SmSession, COMFirstSmObject, DirectoryPDM, FileNamePDM

	NoRevisionInformation:

	'================================Converting COM Variable into procedural script output===============================
	ExitFunction:
		CONV_ComRecListToRecordList	COMThirdList,ThirdPar

		TitleBlock = Err_None ' the function should return a value
			              	  ' Err_None=0
		Exit Function

 	'==================================================Error management==================================================
	NoSelectedObject:
		MsgBox "No selected object in the CATIA session."
		RevisionBlock = Err_Gen
		Exit Function

	NoDrawingOpenInSession:
		MsgBox "The Drawing should not be updated, because it is not open in the CATIA Session."
		RevisionBlock = Err_Gen
		Exit Function	

	AssignErrorCode:
		MsgBox "Error Code on a SmarTeam API. The Script TitleBlock ended."
		RevisionBlock = Err_Gen

End Function

'------------------------------------------------------------------------------------
Function GetDirectory(FullPath As String) As String
	
	Dim i As Integer
  	Dim DotPos As Integer

  	GetDirectory = FullPath
  	DotPos = 0
  	For i = Len(FullPath) To 1 Step -1
    	If (Mid(FullPath,i,1) = "\") Then
      	DotPos = i
	  	Exit For
    	End If
  	Next i
  	If (DotPos <> 0) Then
		GetDirectory = left(FullPath, DotPos)
  	Else
		GetDirectory = ""
  	End If
  
End Function
'------------------------------------------------------------------------------------
Function StripFileExtension(FileName As String) As String
  Dim i As Integer
  Dim DotPos As Integer

  StripFileExtension = FileName
  DotPos = 0
  For i = Len(FileName) To 1 Step -1
    If (Mid(FileName,i,1) = ".") Then
      DotPos = i
	  Print "DotPos = " + cstr(DotPos)
	  Exit For
    End If
  Next i
  If (DotPos <> 0) Then
    StripFileExtension = left(FileName, DotPos-1)
	Print "StripFileExtension = " + left(FileName, DotPos-1)
  End If
End Function
'------------------------------------------------------------------------------------
Function GetFileExtension(FileName As String) As String
  Dim i As Integer
  Dim DotPos As Integer

  GetFileExtension = FileName
  DotPos = 0
  For i = Len(FileName) To 1 Step -1
    If (Mid(FileName,i,1) = ".") Then
      DotPos = i
	  Print "DotPos = " + cstr(DotPos)
	  Exit For
    End If
  Next i
  If (DotPos <> 0) Then
    GetFileExtension = Mid(FileName, DotPos+1, Len(FileName)-DotPos)
	Print "GetFileExtension = " + Mid(FileName, DotPos+1, Len(FileName)-DotPos)
  Else
	GetFileExtension = ""
  End If
End Function
'------------------------------------------------------------------------------------

