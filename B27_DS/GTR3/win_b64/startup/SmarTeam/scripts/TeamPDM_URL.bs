Option Explicit
' ----------------------------------------------------------------------------
' Function Name:   TeamPDM_URL
' Purpose:         Generate a TeamPDM URL
'
' Note:            Version is hard-coded
' ----------------------------------------------------------------------------


Function TeamPDM_URL(ApplHndl As Long,Sstr As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long ) As Integer
' FirstPar  - Record List, containing all attributes of the current object
' SecondPar - nil
' ThirdPar  - nothing

	Dim SmSession As ISmSession
	Dim Record1  As Object
	Dim Record2 As Object
	Dim Record3  As Object
	Dim ClassId As Integer
	Dim ObjectId As Long
	Dim Vers As String
	Dim URL As String

	Set SmSession = SCREXT_ObjectForInterface(ApplHndl)
	CONV_RecListToComRecordList FirstPar,Record1 
	CONV_RecListToComRecordList SecondPar,Record2
	CONV_RecListToComRecordList ThirdPar,Record3

	' Get class identifier
	ClassId = Record1.ValueAsInteger("CLASS_ID", 0)

	'Get object identifier of the root object
	ObjectId = Record1.ValueAsInteger("OBJECT_ID", 0)

	'Hard-coded version
	Vers="1"
	URL = "TeamPDM://DBExtractor?CLASSID.EQ." + Str(ClassId) + ".AND.OBJECTID.EQ." + Str(ObjectId) + ".AND.Vers.EQ." + Vers

	Clipboard.Clear
	Clipboard.SetText URL

	MsgBox "The Following URL has been copied to clipboard: " & Chr(13) & Chr(10) & URL


End Function

