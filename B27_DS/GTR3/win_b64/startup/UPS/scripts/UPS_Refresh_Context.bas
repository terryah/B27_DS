Attribute VB_Name = "Refresh_Context1"
'This script intends to provide a functionality to designers to replace the context used in their
'design by any of the revisions of the Source Context.
'For this to work properly, a designer must select a context in his design (which has some root context
'from which it has been duplicated) and run the Refresh Context script. A dialog will pop up showing
'all the available revisions of the source Context. Designer can then select any of the source from
'list and run "Refresh". This will replace the context from Designer's assembly by the one selected from
'list.

'Declaration of global variables
Public SourceID
Public SourceRevisions
Public SourceRevisionsCount
Public SourceName
Public SE As SelectedElement
Public Revs()
Public SourceType
Public SelectedElementType
Public SourceRevision
Public SelectedElementRevision
Public SelectedElementName
Public SourceTitles()
Public V6Engine
Public CS As Selection
Public ProductsCounter As Long
Public ProductsArray() As Products


Sub CATMain()

'Get V6 Engine
    Set V6Engine = CATIA.GetItem("V6Engine")

    'Check if connected to server
    If Not V6Engine Is Nothing Then
        If V6Engine.IsConnected() = "FALSE" Then
            MsgBox "Not connected to server. Please connect and try again"
            Exit Sub
        End If
    Else
        Exit Sub
    End If

    'Check if there is an Active Document
    On Error Resume Next
    Dim ActiveDocument As Document
    Call GetActiveDocument(ActiveDocument)
    If Err Then
        MsgBox "There is no Active Document!!!"
        Exit Sub
    End If


    'Check if document to replace is selected in CATIA tree
    'Get Selected Element in the CATIA Tree
    SE = Nothing
    Call GetSelectedElement(SE)
    If CS.Count > 1 Then
        Exit Sub
    End If

    If SE Is Nothing Then
        Exit Sub
    End If


    'Get V6ID for Selected Element
    Dim ID
    If Not V6Engine Is Nothing Then
        ID = V6Engine.GetIDFromFileName(SE.LeafProduct.ReferenceProduct.Parent.FullName)
        SelectedElementName = ID.N
        If ID Is Nothing Then
            MsgBox "ID for selected element cannot be created"
            Exit Sub
        End If
    Else
        Exit Sub
    End If

    'Get Type and revision of selected Element
    If Not ID Is Nothing Then
        SelectedElementType = ID.T
        SelectedElementRevision = ID.RI
    Else
        Exit Sub
    End If

    'Get SourceID of Selected Element
    If Not V6Engine Is Nothing Then
        SourceID = V6Engine.GetSource(ID)
        If SourceID Is Nothing Then
            MsgBox("There is no Source Context available in 3DEXPERIENCE for the selected context. Please revise your selection.", , "Warning!!!")
            Exit Sub
        End If
    Else
        Exit Sub
    End If

    'Get Source Name
    If Not SourceID Is Nothing Then
        SourceName = SourceID.N
        If SourceName = " " Then
            MsgBox "No Source"
            Exit Sub
        End If
    Else
        Exit Sub
    End If

    'Get Source Type and Revision
    If Not SourceID Is Nothing Then
        SourceType = SourceID.T
        SourceRevision = SourceID.RI
    Else
        Exit Sub
    End If


    'Get SourceRevisions
    Dim SourceRevisions
    If Not V6Engine Is Nothing Then
        SourceRevisions = V6Engine.GetRevisions(SourceID)
        If Not SourceRevisions Is Nothing Then
            SourceRevisionsCount = SourceRevisions.Count
        End If
    Else
        Exit Sub
    End If

    'Get Title for Source Revisions
    If SourceRevisionsCount > 0 Then
        ReDim Revs(0 To (SourceRevisionsCount - 1))
        ReDim SourceTitles(0 To (SourceRevisionsCount - 1))
        For Count = 0 To (SourceRevisionsCount - 1)
            If Not SourceRevisions Is Nothing Then
                Item = SourceRevisions.Item(Count + 1)
            Else
                Exit Sub
            End If

            'Get Properties
            If Not V6Engine Is Nothing Then
                Properties = V6Engine.GetProperties(Item)
            Else
                Exit Sub
            End If

            If Properties.Count > 0 Then
                For NewCount = 0 To (Properties.Count - 1)
                    If Properties.Item(NewCount + 1).PropertyName = "Title" Then
                        SourceTitles(Count) = Properties.Item(NewCount + 1).PropertyValue
                    End If
                Next
            Else
                Exit Sub
            End If

            If Not Item Is Nothing Then
                Revs(Count) = Item.RI
            Else
                Exit Sub
            End If
        Next
    Else
        Exit Sub
    End If

    Call Show_Dialog()
End Sub


Public Sub Show_Dialog()
    UserForm1.Show vbModal
End Sub

Public Function GetActiveDocument(ActiveDoc As Document)
    On Error Resume Next
    ActiveDoc = CATIA.ActiveDocument
    If Err Then
        MsgBox "ERROR: " & Err.Description
        Exit Function
    End If
End Function

Public Function Getproduct(product1 As Product)
    Dim AD As Document
    Call GetActiveDocument(AD)
    If Not AD Is Nothing Then
        product1 = AD.Product
        If Err Then
            MsgBox "ERROR: " & Err.Description
            Exit Function
        End If
    Else
        Exit Function
    End If
End Function

Public Function GetProducts(products1 As Products)
    Dim Product As Product
    Call Getproduct(Product)
    If Not Product Is Nothing Then
        products1 = Product.Products
        If Err Then
            MsgBox "ERROR: " & Err.Description
            Exit Function
        End If
    Else
        Exit Function
    End If
End Function

Public Function GetCurrentSelection(CurSelection As Selection)
    Dim AD As Document
    Call GetActiveDocument(AD)
    If Not AD Is Nothing Then
        CurSelection = AD.Selection
        If Err Then
            MsgBox "ERROR: " & Err.Description
            Exit Function
        End If
    Else
        Exit Function
    End If
End Function

Public Function GetSelectedElement(ByRef SelDoc As SelectedElement)

    Call GetCurrentSelection(CS)

    If CS.Count = 0 Then
        MsgBox "Context to replace is not selected"
        Exit Function
    End If

    'Check that multiple selection has not been done
    If CS.Count2 > 1 Then
        MsgBox("Refresh Context is not allowed on multiple selection. Please revise your selection to select one.", , "Warning!!!")
        Exit Function
    End If

    If Not CS Is Nothing Then
        SelDoc = CS.Item2(1)
        If Err Then
            MsgBox "ERROR: " & Err.Description
            Exit Function
        End If
    Else
        Exit Function
    End If



End Function

Sub GetAllProducts(CurrentProduct As Product)
    Dim i As Integer
    Dim CurrentTreeNode As Product

    ' Loop through every tree node for the current product
    For i = 1 To CurrentProduct.Products.Count

        Dim occurence As Long
        occurence = 1

        'If there are more than one instances of any particular item within a Products group, Products counter will increase for
        'only one of these
        If i > 1 Then
            For k = 1 To (i - 1)
                If CurrentProduct.Products.Item(i).PartNumber = CurrentProduct.Products.Item(i - 1).PartNumber Then
                    occurence = occurence + 1
                End If
            Next
        End If

        If occurence = 1 Then
            CurrentTreeNode = CurrentProduct.Products.Item(i)

            'If the current node name is similar to the one as selected element to be replaced, we add its parent(products group) in the
            'ProductsArray
            If CurrentTreeNode.PartNumber = SE.LeafProduct.ReferenceProduct.PartNumber Then
                ProductsCounter = ProductsCounter + 1
                ReDim Preserve ProductsArray(1 To ProductsCounter)
                ProductsArray(ProductsCounter) = CurrentProduct.Products
            End If
        End If


        ' if sub-nodes exist below the current tree node, call the sub recursively
        If CurrentTreeNode.Products.Count > 0 Then
            Call GetAllProducts(CurrentTreeNode)
        End If
    Next
End Sub
