<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:lxslt="http://xml.apache.org/xslt" 
xmlns:java="http://xml.apache.org/xslt/java" 
xmlns:NumberIncrement="NumberIncrement" 
xmlns:MatrixUtils="MatrixUtils" 
xmlns:MotomanUtils="MotomanUtils" 
xmlns:FileUtils="FileUtils" 
xmlns:BitMaskUtils="BitMaskUtils" 
xmlns:KIRStrUtils="KIRStrUtils" 
extension-element-prefixes="NumberIncrement MotomanUtils MatrixUtils FileUtils BitMaskUtils KIRStrUtils">

	<lxslt:component prefix="NumberIncrement" functions="next" elements="startnum increment reset counternum">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MotomanUtils" functions="JointToEncoderConversion SetParameterData GetParameterData" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils" functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"/>
	</lxslt:component>
	<lxslt:component prefix="BitMaskUtils" functions="bitAND bitOR bitShiftLeft bitShiftRight" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.BitMaskUtils"/>
	</lxslt:component>
	<lxslt:component prefix="FileUtils" functions="fileOpen fileWrite fileClose" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.FileUtils"/>
	</lxslt:component>
	<lxslt:component prefix="KIRStrUtils" functions="cat get set kir setLastProfile getLastProfile readConfigDat readMachineDat getRailConfig getZOffset getBaseName getBaseIndex addDAT getDATIndex clearDAT" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.KIRStrUtils"/>
	</lxslt:component>

	<xsl:strip-space elements="*"/>
	<xsl:output method="text"/>
	<xsl:variable name="debug" select="0"/>
	<xsl:variable name="version" select="'DELMIA CORP. KUKA KRC DOWNLOADER VERSION 5 RELEASE 26 SP3'"/>
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 1986-2016, ALL RIGHTS RESERVED'"/>
	<xsl:variable name="rbttype" select="Robot"/>
	<xsl:variable name="auxGrp1" select="'RailTrackGantry'"/>
	<xsl:variable name="auxGrp2" select="'EndOfArmTooling'"/>
	<xsl:variable name="auxGrp3" select="'WorkpiecePositioner'"/>
	<xsl:variable name="cr" select="'&#xa;'"/>
	<xsl:variable name="openCurlyBrace"  select="'&#x7B;'"/>
	<xsl:variable name="closeCurlyBrace" select="'&#x7D;'"/>
	<xsl:variable name="kirFrame"  select="'_kuka_kir_ref_frame'"/>
	<xsl:variable name="kirFrameX" select="'_kuka_kir_ref_frame_to_part_xform_x'"/>
	<xsl:variable name="kirFrameY" select="'_kuka_kir_ref_frame_to_part_xform_y'"/>
	<xsl:variable name="kirFrameZ" select="'_kuka_kir_ref_frame_to_part_xform_z'"/>
	<xsl:variable name="kirFrameW" select="'_kuka_kir_ref_frame_to_part_xform_w'"/>
	<xsl:variable name="kirFrameP" select="'_kuka_kir_ref_frame_to_part_xform_p'"/>
	<xsl:variable name="kirFrameR" select="'_kuka_kir_ref_frame_to_part_xform_r'"/>
	<xsl:variable name="kirRobotX" select="'_robot_base_to_ref_robot_base_x'"/>
	<xsl:variable name="kirRobotY" select="'_robot_base_to_ref_robot_base_y'"/>
	<xsl:variable name="kirRobotZ" select="'_robot_base_to_ref_robot_base_z'"/>
	<xsl:variable name="kirRobotW" select="'_robot_base_to_ref_robot_base_w'"/>
	<xsl:variable name="kirRobotP" select="'_robot_base_to_ref_robot_base_p'"/>
	<xsl:variable name="kirRobotR" select="'_robot_base_to_ref_robot_base_r'"/>
	<xsl:variable name="keywordSufix" select="'_FLANGE'"/>
	<xsl:variable name="sufixLen" select="string-length($keywordSufix)"/>
	<xsl:variable name="pat" select="'0'"/>
	<xsl:variable name="robotName" select="/OLPData/Resource[@ResourceType='Robot']/GeneralInfo/ResourceName"/>
	<xsl:variable name="NumBaseAuxAxes"    select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<xsl:variable name="NumToolAuxAxes"    select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<xsl:variable name="NumPositionerAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="numpat" select="'0'"/>
	<xsl:variable name="posnum" select="0"/>
	<xsl:variable name="targetUnits" select="1000.0"/>
	<xsl:variable name="jointPositionPattern" select="'#'"/>
	<xsl:variable name="cartesianPositionPattern" select="'#.#######'"/>
	<xsl:variable name="delayTimePattern" select="'#.#####'"/>
	<xsl:variable name="toolPattern" select="'0.0##'"/>
	<xsl:variable name="templateFile" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='TemplateFile']/ParameterValue"/>
	<xsl:variable name="configFile"   select="/OLPData/Resource/ParameterList/Parameter[ParameterName='ConfigFile']/ParameterValue"/>
	<xsl:variable name="machineFile"   select="/OLPData/Resource/ParameterList/Parameter[ParameterName='MachineFile']/ParameterValue"/>
	<xsl:variable name="pointFormat"  select="/OLPData/Resource/ParameterList/Parameter[ParameterName='PointFormat']/ParameterValue"/>
	<xsl:variable name="tagRootName"  select="/OLPData/Resource/ParameterList/Parameter[ParameterName='TagRootName']/ParameterValue"/>
	<xsl:variable name="toolOutput"   select="/OLPData/Resource/ParameterList/Parameter[ParameterName='ToolOutput']/ParameterValue"/>
	<xsl:variable name="baseOutput"   select="/OLPData/Resource/ParameterList/Parameter[ParameterName='BaseOutput']/ParameterValue"/>
	<xsl:variable name="auxAxisOrder" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='KukaAuxAxisOrder']/ParameterValue"/>
	<xsl:variable name="softwareV0"   select="/OLPData/Resource/ParameterList/Parameter[ParameterName='SoftwareVersion']/ParameterValue"/>
	<xsl:variable name="softwareV">
		<xsl:choose>
			<xsl:when test="$softwareV0">
				<xsl:value-of select="$softwareV0"/>
			</xsl:when>
			<xsl:otherwise>4.1.5</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="maingroup"    select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Maingroup']/ParameterValue"/>
	<xsl:variable name="toolgroup"    select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Toolgroup']/ParameterValue"/>
	<xsl:variable name="railgroup"    select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Railgroup']/ParameterValue"/>
	<xsl:variable name="intaxes"      select="/OLPData/Resource/ParameterList/Parameter[ParameterName='IntegratedAxes']/ParameterValue"/>
	<xsl:variable name="maxdigital"   select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Digital_IO']/ParameterValue"/>
	<xsl:variable name="maxrobot"     select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Robot_IO']/ParameterValue"/>
	<xsl:variable name="maxweld"      select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Weld_IO']/ParameterValue"/>
	<xsl:variable name="tooluserfile" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='UtoolUframeFile']/ParameterValue"/>
	<xsl:variable name="maxSpeedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
	<xsl:variable name="jointSpdPat"  select="'0'"/>
	<xsl:variable name="linearSpdPat" select="'0.#####'"/>
	<xsl:variable name="pi" select="3.141592653589"/>
	<xsl:variable name="toDeg" select="180 div $pi"/>
	<xsl:variable name="downloadStyle">
		<xsl:choose>
			<xsl:when test="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBIgpCallRobotTask']/DownloadStyle='File'">
				<xsl:text>File</xsl:text>
			</xsl:when>
			<xsl:when test="/OLPData/Resource/ProgramLogic/Jobs/Job/CallTask/@DownloadStyle='File'">
				<xsl:text>File</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Subroutine</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template name="IOSignal"/>

	<xsl:template name="getNumIncNext">
		<xsl:choose>
			<xsl:when test="$debug">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NumberIncrement:next()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="getNumIncCurrent">
		<xsl:choose>
			<xsl:when test="$debug">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NumberIncrement:current()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="/">
		<!-- counter number 2: Tag number -->
		<NumberIncrement:counternum counter="2"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<!-- counter number 3: SDAT(Spot weld data) -->
		<NumberIncrement:counternum counter="3"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<!-- counter 4: string attribute "_kuka_kir_ref_frame" counter -->
		<NumberIncrement:counternum counter="4"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<!-- counter 5: external axis counter -->
		<NumberIncrement:counternum counter="5"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>

		<xsl:call-template name="HeaderInfo"/>

		<xsl:variable name="downloadCoord" select="OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/DownloadCoordinates"/>

		<xsl:apply-templates select="OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity" mode="KIR"/>
		<NumberIncrement:counternum counter="4"/>
		<xsl:variable name="kirDownload">
			<xsl:call-template name="getNumIncCurrent"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$downloadCoord != 'Part' and $kirDownload > 0">
				<xsl:value-of select="$cr"/>
				<xsl:text>KUKA KIR download must be performed in Part Coordinates</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>Please re-download and select Part Coordinates</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<!-- xsl:if test="$kirDownload > 0" -->
					<xsl:variable name="status" select="KIRStrUtils:readConfigDat($configFile)"/>
				<!-- /xsl:if -->
				<xsl:if test="$machineFile">
					<xsl:variable name="rbt_act" select="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
					<xsl:variable name="isRail" select="$rbt_act/Target/JointTarget/AuxJoint/@Type=$auxGrp1"/>
					<xsl:if test="$isRail = 'true'">
						<xsl:variable name="status" select="KIRStrUtils:readMachineDat($machineFile)"/>
						<xsl:text>VERSION INFO START</xsl:text><xsl:value-of select="$cr"/>
						<xsl:value-of select="$cr"/>
						<xsl:choose>
							<xsl:when test="$status = 'OK'">
								<xsl:variable name="railConfig" select="KIRStrUtils:getRailConfig()"/>
								<xsl:text>Robot-Rail Configuration: </xsl:text>
								<xsl:choose>
									<xsl:when test="$railConfig = 'A'">90-degree A</xsl:when>
									<xsl:when test="$railConfig = 'B'">90-degree B</xsl:when>
									<xsl:when test="$railConfig = 'C'">90-degree C</xsl:when>
									<xsl:when test="$railConfig = 'D'">90-degree D</xsl:when>
									<xsl:when test="$railConfig = 'H'">45-degree D(A-45-B)</xsl:when>
									<xsl:when test="$railConfig = 'I'">45-degree A(B-45-C)</xsl:when>
									<xsl:when test="$railConfig = 'J'">45-degree B(C-45-D)</xsl:when>
									<xsl:when test="$railConfig = 'K'">45-degree C(D-45-A)</xsl:when>
									<xsl:otherwise>Wrong Settings</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="$status = 'KO'">
								<xsl:text>ERROR: Rail settings in the Machine Data file are wrong.</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>ERROR: </xsl:text>
								<xsl:value-of select="substring(substring-before($status,'('), 3)"/>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR: (</xsl:text>
								<xsl:value-of select="substring-after($status, '(')"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$cr"/>
						<xsl:text>VERSION INFO END</xsl:text><xsl:value-of select="$cr"/>
					</xsl:if>
				</xsl:if>
				<xsl:call-template name="InitializeParameters"/>
				<xsl:apply-templates select="OLPData"/>
				<xsl:if test="$kirDownload > 0">
					<xsl:value-of select="$cr"/>
					<xsl:text>DATA FILE START Message.txt</xsl:text>
					<xsl:call-template name="configDAT"/>
					<xsl:value-of select="$cr"/>
					<xsl:text>DATA FILE END</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="HeaderInfo">
		<xsl:text>VERSION INFO START</xsl:text><xsl:value-of select="$cr"/>
		<xsl:value-of select="$version"/><xsl:value-of select="$cr"/>
		<xsl:value-of select="$copyright"/><xsl:value-of select="$cr"/>
		<xsl:text>VERSION INFO END</xsl:text>	<xsl:value-of select="$cr"/>
	</xsl:template>

	<xsl:template name="InitializeParameters">
	</xsl:template>

	<xsl:template match="OLPData">
		<xsl:call-template name="programLogic">
			<xsl:with-param name="plm" select="Resource[GeneralInfo/ResourceName=$robotName]/ProgramLogic"/>
		</xsl:call-template>
		<xsl:choose>
			<xsl:when test="$downloadStyle = 'File'">
				<xsl:for-each select="Resource/ActivityList">
					<xsl:call-template name="activityListDAT"/>
					<xsl:call-template name="activityListSRC"/>
					<xsl:value-of select="KIRStrUtils:clearDAT('TagNameTable')"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="Resource/ActivityList">
					<xsl:call-template name="activityListDAT"/>
				</xsl:for-each>
				<xsl:for-each select="Resource/ActivityList">
					<xsl:call-template name="activityListSRC"/>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Program Logic section -->
	<xsl:template name="programLogic">
		<xsl:param name="plm"/>

		<xsl:if test="$plm">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START </xsl:text>
			<xsl:value-of select="translate($plm/Jobs/@Name, '.', '')"/>
			<xsl:text>.src</xsl:text>
	
			<xsl:variable name="header" select="AttributeList/Attribute[AttributeName = 'Header1']/AttributeValue"/>
			<xsl:choose>
				<xsl:when test="$header != ''">
					<xsl:apply-templates select="AttributeList/Attribute" mode="STD"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="kukaHeader0"/>
				</xsl:otherwise>
			</xsl:choose>
	
			<xsl:value-of select="$cr"/>
			<xsl:text>DEF </xsl:text>
			<xsl:value-of select="translate($plm/Jobs/@Name, '.', '')"/>
			<xsl:text>( )</xsl:text>
	
			<xsl:if test="boolean(normalize-space($templateFile))">
				<xsl:choose>
					<xsl:when test="$debug!=0">
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="kukaSrcHeader">
							<xsl:with-param name="fileName" select="$templateFile"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>

			<xsl:value-of select="$cr"/>
			<xsl:text>WHILE TRUE</xsl:text>
			<xsl:apply-templates select="$plm/Jobs/Job"/>
			<xsl:value-of select="$cr"/>
			<xsl:text>ENDWHILE</xsl:text>

			<xsl:value-of select="$cr"/>
			<xsl:text>END</xsl:text>
			<xsl:if test="$downloadStyle = 'File'">
				<xsl:value-of select="$cr"/>
				<xsl:text>DATA FILE END</xsl:text>
			</xsl:if>
		</xsl:if>
		<!-- program logic exists -->

	</xsl:template>
	<!-- end of template programLogic -->

	<xsl:template match="Job">
		<xsl:variable name="numStartExpr" select="count(StartCondition/Expression)"/>
		<xsl:choose>
			<xsl:when test="$numStartExpr=1 and StartCondition/Expression='true'">
				<xsl:apply-templates select="SetOutput|WaitExpression|CallTask"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$cr"/>
				<xsl:text>IF </xsl:text>
				<xsl:apply-templates select="StartCondition/Expression"/>
				<xsl:apply-templates select="SetOutput|WaitExpression|CallTask"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>ENDIF</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Job -->

	<xsl:template match="Expression">
		<xsl:choose>
			<xsl:when test="local-name(following-sibling::node()[1])='Operator'">(</xsl:when>
			<xsl:when test="local-name(preceding-sibling::node()[1])='Operator'">(</xsl:when>
		</xsl:choose>

		<xsl:variable name="ioName" select="normalize-space(substring-before(., '='))"/>
		<xsl:variable name="ioVal"  select="normalize-space(substring-after(., '='))"/>
		<xsl:variable name="ioNum"  select="../../../../Inputs/Input[@InputName=$ioName]/@InputNumber"/>
		<xsl:call-template name="singleExpr">
			<xsl:with-param name="ioName" select="$ioName"/>
			<xsl:with-param name="ioVal" select="$ioVal"/>
			<xsl:with-param name="ioNum" select="$ioNum"/>
		</xsl:call-template>

		<xsl:choose>
			<xsl:when test="local-name(following-sibling::node()[1])='Operator'">
				<xsl:text>) </xsl:text>
				<!-- output the next logical operator (and|or) -->
				<xsl:value-of select="following-sibling::node()[1]"/>
				<xsl:text> </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(preceding-sibling::node()[1])='Operator'">)</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Expression -->

	<xsl:template name="singleExpr">
		<xsl:param name="ioName"/>
		<xsl:param name="ioVal"/>
		<xsl:param name="ioNum"/>

		<xsl:choose>
			<xsl:when test="number(substring-before($softwareV, '.')) >= 5">
				<xsl:choose>
					<xsl:when test="contains($ioVal, '=')">
						<xsl:if test="substring-before($ioVal, ' ')='false'">NOT ( </xsl:if>
						<xsl:text>$IN[</xsl:text>
						<xsl:value-of select="$ioNum"/>
						<xsl:text>]</xsl:text>
						<xsl:if test="substring-before($ioVal, ' ')='false'"> )</xsl:if>

						<xsl:variable name="subExpr0" select="normalize-space(substring-after($ioVal, ' '))"/>
						<!-- logic operator -->
						<xsl:text> </xsl:text>
						<xsl:value-of select="substring-before($subExpr0, ' ')"/>
						<xsl:text> </xsl:text>
		
						<xsl:variable name="subExpr" select="normalize-space(substring-after($subExpr0, ' '))"/>
						<xsl:variable name="ioName1" select="normalize-space(substring-before($subExpr, '='))"/>
						<xsl:variable name="ioVal1"  select="normalize-space(substring-after($subExpr, '='))"/>
						<xsl:variable name="ioNum1"  select="../../../../Inputs/Input[@InputName=$ioName1]/@InputNumber"/>
						<xsl:call-template name="singleExpr">
							<xsl:with-param name="ioName" select="$ioName1"/>
							<xsl:with-param name="ioVal"  select="$ioVal1"/>
							<xsl:with-param name="ioNum"  select="$ioNum1"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$ioVal='false'">NOT ( </xsl:if>
						<xsl:text>$IN[</xsl:text>
						<xsl:value-of select="$ioNum"/>
						<xsl:text>]</xsl:text>
						<xsl:if test="$ioVal='false'"> )</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>$IN[</xsl:text>
				<xsl:value-of select="$ioNum"/>
				<xsl:text>]==</xsl:text>
		
				<xsl:choose>
					<xsl:when test="contains($ioVal, '=')">
						<xsl:value-of select="substring-before($ioVal, ' ')"/>
						<xsl:variable name="subExpr0" select="normalize-space(substring-after($ioVal, ' '))"/>
						<!-- logic operator -->
						<xsl:text> </xsl:text>
						<xsl:value-of select="substring-before($subExpr0, ' ')"/>
						<xsl:text> </xsl:text>
		
						<xsl:variable name="subExpr" select="normalize-space(substring-after($subExpr0, ' '))"/>
						<xsl:variable name="ioName1" select="normalize-space(substring-before($subExpr, '='))"/>
						<xsl:variable name="ioVal1"  select="normalize-space(substring-after($subExpr, '='))"/>
						<xsl:variable name="ioNum1"  select="../../../../Inputs/Input[@InputName=$ioName1]/@InputNumber"/>
						<xsl:call-template name="singleExpr">
							<xsl:with-param name="ioName" select="$ioName1"/>
							<xsl:with-param name="ioVal"  select="$ioVal1"/>
							<xsl:with-param name="ioNum"  select="$ioNum1"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$ioVal"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template singleExpr -->

	<xsl:template match="SetOutput">
		<xsl:variable name="ioName" select="normalize-space(substring-before(., '='))"/>
		<xsl:variable name="ioVal"  select="normalize-space(substring-after(., '='))"/>
		<xsl:variable name="ioNum"  select="../../../Outputs/Output[@OutputName=$ioName]/@OutputNumber"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>$OUT[</xsl:text>
		<xsl:value-of select="$ioNum"/>
		<xsl:text>]=</xsl:text>
		<xsl:value-of select="$ioVal"/>
	</xsl:template>

	<xsl:template match="WaitExpression">
		<xsl:value-of select="$cr"/>
		<xsl:text>WAIT FOR </xsl:text>
		<xsl:apply-templates select="Expression"/>
	</xsl:template>

	<xsl:template match="CallTask">
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="translate(., '.', '')"/>
		<xsl:text>()</xsl:text>
	</xsl:template>
	<!-- end Program Logic section -->

	<xsl:template name="activityListDAT">
		<xsl:variable name="templateFileAttr" select="AttributeList/Attribute[AttributeName='TemplateFile']/AttributeValue"/>
		<!-- create DAT file -->

		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[1]">
			<NumberIncrement:counternum counter="2"/>
			<NumberIncrement:reset/>
			<NumberIncrement:counternum counter="3"/>
			<NumberIncrement:reset/>
			<xsl:value-of select="KIRStrUtils:clearDAT('ggdat')"/>
			<xsl:value-of select="KIRStrUtils:clearDAT('ppdat')"/>
			<xsl:value-of select="KIRStrUtils:clearDAT('lcpdat')"/>
		</xsl:if>

		<xsl:if test="$downloadStyle = 'File' or (boolean(/OLPData/Resource/ProgramLogic)=false and .=/OLPData/Resource/ActivityList[1])">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START </xsl:text>
			<xsl:value-of select="translate(@Task, '.', '')"/>
			<xsl:text>.dat</xsl:text>

			<xsl:variable name="header" select="AttributeList/Attribute[AttributeName = 'Header1']/AttributeValue"/>
			<xsl:choose>
				<xsl:when test="$header != ''">
					<xsl:apply-templates select="AttributeList/Attribute" mode="STD"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="kukaHeader0"/>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:value-of select="$cr"/>
			<xsl:text>DEFDAT </xsl:text>
			<xsl:value-of select="translate(@Task, '.', '')"/>

			<xsl:call-template name="processCommentsDAT">
				<xsl:with-param name="prefix" select="'Pre'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>

			<xsl:if test="boolean(normalize-space($templateFile)) or boolean(normalize-space($templateFileAttr))">
				<xsl:choose>
					<xsl:when test="$debug!=0">
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="boolean(normalize-space($templateFileAttr))">
								<xsl:call-template name="kukaDatHeader">
									<xsl:with-param name="fileName" select="$templateFileAttr"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="kukaDatHeader">
									<xsl:with-param name="fileName" select="$templateFile"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>

		<xsl:variable name="group1">
			<xsl:choose>
				<xsl:when test="$auxAxisOrder">
					<xsl:choose>
						<xsl:when test="contains($auxAxisOrder, ',')">
							<xsl:value-of select="substring-before($auxAxisOrder, ',')"/>
						</xsl:when>
						<xsl:otherwise>
							<!-- parameter AuxAxisOrder contains no comma(",") -->
							<xsl:choose>
								<xsl:when test="$auxAxisOrder=$auxGrp1"/>
								<xsl:when test="$auxAxisOrder=$auxGrp2"/>
								<xsl:when test="$auxAxisOrder=$auxGrp3"/>
								<xsl:otherwise>
									<xsl:message terminate="yes">Aux Axis Type Unknown.</xsl:message>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="$auxAxisOrder"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<!-- parameter AuxAxisOrder not set -->
					<xsl:value-of select="$auxGrp1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$auxAxisOrder">
				<xsl:choose>
					<xsl:when test="contains($auxAxisOrder, ',')">
						<xsl:variable name="group1" select="substring-before($auxAxisOrder, ',')"/>
						<xsl:variable name="auxAxisOrder2" select="substring-after($auxAxisOrder, ',')"/>
						<xsl:choose>
							<xsl:when test="contains($auxAxisOrder2, ',')">
								<xsl:variable name="group2" select="substring-before($auxAxisOrder2, ',')"/>
								<xsl:variable name="group3" select="substring-after($auxAxisOrder2, ',')"/>

								<xsl:apply-templates select="Activity | Action" mode="DAT">
									<xsl:with-param name="group1" select="$group1"/>
									<xsl:with-param name="group2" select="$group2"/>
									<xsl:with-param name="group3" select="$group3"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:otherwise>
								<!-- parameter AuxAxisOrder contains one comma(",") -->
								<xsl:variable name="group2" select="$auxAxisOrder2"/>

								<xsl:apply-templates select="Activity | Action" mode="DAT">
									<xsl:with-param name="group1" select="$group1"/>
									<xsl:with-param name="group2" select="$group2"/>
									<xsl:with-param name="group3" select="''"/>
								</xsl:apply-templates>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<!-- parameter AuxAxisOrder contains no comma(",") -->
						<xsl:variable name="group1" select="$auxAxisOrder"/>
						<xsl:apply-templates select="Activity | Action" mode="DAT">
							<xsl:with-param name="group1" select="$group1"/>
							<xsl:with-param name="group2" select="''"/>
							<xsl:with-param name="group3" select="''"/>
						</xsl:apply-templates>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- parameter AuxAxisOrder not set -->
				<xsl:apply-templates select="Activity | Action" mode="DAT"/>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:call-template name="processCommentsDAT">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[last()]">
			<xsl:value-of select="$cr"/>
			<xsl:text>ENDDAT</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE END</xsl:text>
		</xsl:if>

	</xsl:template>
	<!-- end of template activityListDAT -->

	<xsl:template name="activityListSRC">
		<xsl:variable name="templateFileAttr" select="AttributeList/Attribute[AttributeName='TemplateFile']/AttributeValue"/>
		<!-- create SRC file-->
		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[1]">
			<NumberIncrement:counternum counter="2"/>
			<NumberIncrement:reset/>
			<NumberIncrement:counternum counter="3"/>
			<NumberIncrement:reset/>
			<xsl:value-of select="KIRStrUtils:clearDAT('ggdat')"/>
			<xsl:value-of select="KIRStrUtils:clearDAT('ppdat')"/>
			<xsl:value-of select="KIRStrUtils:clearDAT('lcpdat')"/>
		</xsl:if>

		<xsl:if test="$downloadStyle = 'File' or (boolean(/OLPData/Resource/ProgramLogic)=false and .=/OLPData/Resource/ActivityList[1])">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START </xsl:text>
			<xsl:value-of select="translate(@Task, '.', '')"/>
			<xsl:text>.src</xsl:text>

			<xsl:variable name="header" select="AttributeList/Attribute[AttributeName = 'Header1']/AttributeValue"/>
			<xsl:choose>
				<xsl:when test="$header != ''">
					<xsl:apply-templates select="AttributeList/Attribute" mode="STD"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="kukaHeader0"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<xsl:value-of select="$cr"/>
		<xsl:text>DEF </xsl:text>
		<xsl:value-of select="translate(@Task, '.', '')"/>
		<xsl:text>( )</xsl:text>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

		<xsl:if test="$downloadStyle = 'File' or (boolean(/OLPData/Resource/ProgramLogic)=false and .=/OLPData/Resource/ActivityList[1])">
			<xsl:if test="boolean(normalize-space($templateFile)) or boolean(normalize-space($templateFileAttr))">
				<xsl:choose>
					<xsl:when test="$debug!=0">
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="boolean(normalize-space($templateFileAttr))">
								<xsl:call-template name="kukaSrcHeader">
									<xsl:with-param name="fileName" select="$templateFileAttr"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="kukaSrcHeader">
									<xsl:with-param name="fileName" select="$templateFile"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>

		<NumberIncrement:counternum counter="4"/>
		<NumberIncrement:reset/>
		<xsl:apply-templates select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity" mode="KIR"/>
		<NumberIncrement:counternum counter="4"/>
		<xsl:variable name="kirDownload">
			<xsl:call-template name="getNumIncCurrent"/>
		</xsl:variable>
		<xsl:if test="$kirDownload=0">
			<xsl:if test="$toolOutput='true'">
				<xsl:call-template name="toolDataOutput"/>
			</xsl:if>
			<xsl:if test="$baseOutput='true'">
				<xsl:call-template name="baseDataOutput"/>
			</xsl:if>
		</xsl:if>

		<xsl:apply-templates select="Activity | Action" mode="SRC"/>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

		<xsl:value-of select="$cr"/>
		<xsl:text>END</xsl:text>
		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[last()]">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE END</xsl:text>
		</xsl:if>
	</xsl:template>
	<!-- end of template activityListSRC -->

	<xsl:template match="Attribute" mode="STD">
		<xsl:variable name="attrName" select="AttributeName"/>
		<xsl:variable name="attrValue" select="AttributeValue"/>

		<xsl:choose>
			<xsl:when test="substring($attrName,1,7) = 'Comment'">
				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD ; </xsl:text>
				<xsl:value-of select="$attrValue"/>
				<xsl:text>;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPBASIS,%CCOMMENT,%VNORMAL,%P 2:</xsl:text>
				<xsl:value-of select="$attrValue"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with($attrName, 'Robot Language')">
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$attrValue"/>
			</xsl:when>

			<xsl:when test="substring($attrName,1,6) = 'Header'">
				<xsl:value-of select="$cr"/>
				<xsl:text>&amp;</xsl:text>
				<xsl:value-of select="$attrValue"/>
			</xsl:when>

			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Attribute mode:STD -->

	<xsl:template match="Attribute" mode="KIR">
		<xsl:variable name="attrName" select="AttributeName"/>
		<xsl:variable name="attrValue" select="AttributeValue"/>
		<xsl:variable name="attrValueLen" select="string-length($attrValue)"/>

		<xsl:if test="$attrName = $kirFrame">
			<xsl:if test="substring($attrValue, number($attrValueLen)-$sufixLen+1) = $keywordSufix">
				<NumberIncrement:counternum counter="4"/>
				<xsl:variable name="junkVar">
					<xsl:call-template name="getNumIncNext"/>
				</xsl:variable>
				
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Activity" mode="KIR">
		<xsl:apply-templates select="AttributeList/Attribute" mode="KIR"/>
	</xsl:template>

	<!-- Header -->
	<xsl:template name="kukaHeader0">
		<xsl:value-of select="$cr"/>
		<xsl:text>&amp;ACCESS RVP</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>&amp;REL 1</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>&amp;PARAM EDITMASK = *</xsl:text>
	</xsl:template>

	<xsl:template name="kukaDatHeader">
		<xsl:param name="fileName"/>
		<xsl:variable name="openRead" select="FileUtils:fileOpenRead(concat($fileName,'.dat'))"/>
		<xsl:choose>
			<xsl:when test="$openRead='Okay'">
				<xsl:call-template name="readTemplate">
					<xsl:with-param name="line" select="FileUtils:fileLineRead()"/>
				</xsl:call-template>
				<xsl:variable name="closeFile" select="FileUtils:fileCloseRead()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>Error: Template file NOT found</xsl:text><xsl:value-of select="$cr"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="kukaSrcHeader">
		<xsl:param name="fileName"/>
		<xsl:variable name="openRead" select="FileUtils:fileOpenRead(concat($fileName,'.src'))"/>
		<xsl:choose>
			<xsl:when test="$openRead='Okay'">
				<xsl:call-template name="readTemplate">
					<xsl:with-param name="line" select="FileUtils:fileLineRead()"/>
				</xsl:call-template>
				<xsl:variable name="closeFile" select="FileUtils:fileCloseRead()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>Error: Template file NOT found</xsl:text><xsl:value-of select="$cr"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="readTemplate">
		<xsl:param name="line"/>
		<xsl:if test="$line">
			<xsl:value-of select="$cr"/>
			<xsl:value-of select="$line"/>
			<xsl:call-template name="readTemplate">
				<xsl:with-param name="line" select="FileUtils:fileLineRead()"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="configDAT">
		<!-- set BASE_DATA first, then set TOOL_DATA as it will overwrite coresponding BASE_DATA if it's Stationary) -->
		<xsl:apply-templates select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile" mode="CFG"/>
		<xsl:apply-templates select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile" mode="CFG"/>
		<xsl:apply-templates select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity" mode="CFG"/>

		<xsl:choose>
			<xsl:when test="$configFile">
				<xsl:variable name="cfgFileName" select="KIRStrUtils:kir($configFile)"/>
				<xsl:value-of select="$cr"/>
				<xsl:choose>
					<xsl:when test="starts-with($cfgFileName, 'OK')">
						<xsl:text>Config file </xsl:text>
						<xsl:value-of select="$configFile"/>
						<xsl:text> renamed to: </xsl:text>
						<xsl:value-of select="substring($cfgFileName,3)"/>
					</xsl:when>
					<xsl:when test="starts-with($cfgFileName, 'KO')">
						<xsl:value-of select="$cr"/>
						<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
						<xsl:text>Error: </xsl:text>
						<xsl:value-of select="substring($cfgFileName,3)"/>
						<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
						<xsl:text>ERROR INFO END</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$cfgFileName"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<NumberIncrement:counternum counter="4"/>
				<NumberIncrement:reset/>
				<xsl:apply-templates select="OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity" mode="KIR"/>
				<NumberIncrement:counternum counter="4"/>
				<xsl:variable name="kirDownload">
					<xsl:call-template name="getNumIncCurrent"/>
				</xsl:variable>

				<xsl:if test="$kirDownload > 0">
					<xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
					<xsl:text>Error: Parameter ConfigFile NOT specified</xsl:text>
					<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO END</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="ToolProfile" mode="CFG">
		<xsl:variable name="toolIndex">
			<xsl:apply-templates select="." mode="NUM"/>
		</xsl:variable>
		<xsl:variable name="x" select="format-number(TCPOffset/TCPPosition/@X * $targetUnits, $toolPattern)"/>
		<xsl:variable name="y" select="format-number(TCPOffset/TCPPosition/@Y * $targetUnits, $toolPattern)"/>
		<xsl:variable name="z" select="format-number(TCPOffset/TCPPosition/@Z * $targetUnits, $toolPattern)"/>
		<xsl:variable name="a" select="format-number(TCPOffset/TCPOrientation/@Roll, $toolPattern)"/>
		<xsl:variable name="b" select="format-number(TCPOffset/TCPOrientation/@Pitch, $toolPattern)"/>
		<xsl:variable name="c" select="format-number(TCPOffset/TCPOrientation/@Yaw, $toolPattern)"/>
		<xsl:choose>
			<xsl:when test="./ToolType='Stationary'">
				<!-- set coresponding BASE -->
				<xsl:variable name="arg1" select="concat('BASE_DATA;', $toolIndex, ';&#x7B;x ', $x, ',y ', $y, ',z ', $z)"/>
				<xsl:variable name="arg2" select="concat($arg1, ',a ', $a, ',b ', $b, ',c ', $c, '&#x7D;')"/>
				<xsl:variable name="result1" select="KIRStrUtils:set($arg2)"/>
				<xsl:variable name="toolName" select="Name"/>
				<xsl:variable name="arg3" select="concat('BASE_NAME;', $toolIndex, ';', $toolName)"/>
				<xsl:variable name="result2" select="KIRStrUtils:set($arg3)"/>
				<xsl:variable name="arg4" select="concat('BASE_TYPE;', $toolIndex, ';#BASE')"/>
				<xsl:variable name="result3" select="KIRStrUtils:set($arg4)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="arg1" select="concat('TOOL_DATA;', $toolIndex, ';&#x7B;x ', $x, ',y ', $y, ',z ', $z)"/>
				<xsl:variable name="arg2" select="concat($arg1, ',a ', $a, ',b ', $b, ',c ', $c, '&#x7D;')"/>
				<xsl:variable name="result1" select="KIRStrUtils:set($arg2)"/>
				<xsl:variable name="toolName" select="Name"/>
				<xsl:variable name="arg3" select="concat('TOOL_NAME;', $toolIndex, ';', $toolName)"/>
				<xsl:variable name="result2" select="KIRStrUtils:set($arg3)"/>
				<xsl:variable name="arg4" select="concat('TOOL_TYPE;', $toolIndex, ';#BASE')"/>
				<xsl:variable name="result3" select="KIRStrUtils:set($arg4)"/>
				<xsl:variable name="arg" select="concat('MACHINE_TOOL_DAT;', $toolIndex, ';&#x7B;MACH_DEF_INDEX 1')"/>
				<xsl:variable name="arg" select="concat($arg, ',PARENT[] &#x22;', $robotName,'&#x22;,GEOMETRY[] &#x22; &#x22;&#x7D;')"/>
				<xsl:variable name="result" select="KIRStrUtils:set($arg)"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$toolIndex=0">
			<xsl:variable name="arg" select="concat('MACHINE_DEF;1;&#x7B;NAME[] &#x22;', $robotName, '&#x22;,COOP_KRC_INDEX 1')"/>
			<xsl:variable name="result" select="KIRStrUtils:set($arg)"/>
			<xsl:variable name="arg" select="concat('MACHINE_DEF;1;,PARENT[] &#x22;WORLD&#x22;,ROOT &#x7B;', 'x 0.0,y 0.0,z 0.0')"/>
			<xsl:variable name="arg" select="concat($arg, ',a 0.0,b 0.0,c 0.0&#x7D;,MECH_TYPE #ROBOT,GEOMETRY[] &#x22; &#x22;&#x7D;')"/>
			<xsl:variable name="result" select="KIRStrUtils:cat($arg)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template name="FindRobotBase">
		<xsl:value-of select="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]/Target/BaseWRTWorld"/>
	</xsl:template>

	<xsl:template match="ObjectFrameProfile" mode="CFG">
		<xsl:variable name="baseName">
			<xsl:call-template name="getBaseName">
				<xsl:with-param name="partName" select="Name"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="baseIndex">
			<xsl:apply-templates select="." mode="NUM"/>
		</xsl:variable>
		<xsl:if test="$baseIndex > 32 ">
			<xsl:value-of select="$cr"/>
			<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
			<xsl:text>Error: More than 32 Object Profiles</xsl:text>
			<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
			<xsl:text>ERROR INFO END</xsl:text>
		</xsl:if>
		<xsl:if test="$baseIndex &lt; 33 ">
			<!-- prepare World WRT Robot xform -->
			<xsl:variable name="robotBase" select="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]/Target/BaseWRTWorld"/>
			<!-- Robot Base WRT World -->
			<xsl:variable name="baseX" select="$robotBase/Position/@X * $targetUnits"/>
			<xsl:variable name="baseY" select="$robotBase/Position/@Y * $targetUnits"/>
			<xsl:variable name="baseZ" select="$robotBase/Position/@Z * $targetUnits"/>
			<xsl:variable name="baseW" select="$robotBase/Orientation/@Yaw"/>
			<xsl:variable name="baseP" select="$robotBase/Orientation/@Pitch"/>
			<xsl:variable name="baseR" select="$robotBase/Orientation/@Roll"/>
			<xsl:variable name="xyzwpr" select="concat($baseX, ',', $baseY, ',', $baseZ, ',', $baseW, ',', $baseP, ',', $baseR)"/>
			<!-- set mat RobotBase/World -->
			<xsl:variable name="matxyzwpr" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
			<!-- invert to get mat World/RobotBase -->
			<xsl:variable name="invxyzwpr" select="MatrixUtils:dgInvert()"/>
			<!-- ObjectFrame WRT World -->
			<xsl:variable name="x" select="ObjectFrame/ObjectFramePosition/@X * $targetUnits"/>
			<xsl:variable name="y" select="ObjectFrame/ObjectFramePosition/@Y * $targetUnits"/>
			<xsl:variable name="z" select="ObjectFrame/ObjectFramePosition/@Z * $targetUnits"/>
			<xsl:variable name="a" select="ObjectFrame/ObjectFrameOrientation/@Roll"/>
			<xsl:variable name="b" select="ObjectFrame/ObjectFrameOrientation/@Pitch"/>
			<xsl:variable name="c" select="ObjectFrame/ObjectFrameOrientation/@Yaw"/>
			<!-- cat mat ObjectFrame/World to get mat ObjectFrame/RobotBase -->
			<xsl:variable name="xyzwpr" select="concat($x, ',', $y, ',', $z, ',', $c, ',', $b, ',', $a)"/>
			<xsl:variable name="matxyzwpr" select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr)"/>
			<!-- ObjectFrame WRT Robot Base -->
			<xsl:variable name="x" select="format-number(MatrixUtils:dgGetX(), $cartesianPositionPattern)"/>
			<xsl:variable name="y" select="format-number(MatrixUtils:dgGetY(), $cartesianPositionPattern)"/>
			<xsl:variable name="z" select="format-number(MatrixUtils:dgGetZ(), $cartesianPositionPattern)"/>
			<xsl:variable name="c" select="format-number(MatrixUtils:dgGetYaw(), $cartesianPositionPattern)"/>
			<xsl:variable name="b" select="format-number(MatrixUtils:dgGetPitch(), $cartesianPositionPattern)"/>
			<xsl:variable name="a" select="format-number(MatrixUtils:dgGetRoll(), $cartesianPositionPattern)"/>
			<xsl:variable name="arg1" select="concat('BASE_DATA;', $baseIndex, ';&#x7B;x ', $x)"/>
			<xsl:variable name="arg2" select="concat($arg1, ',y ', $y, ',z ', $z, ',a ', $a, ',b ', $b, ',c ', $c, '&#x7D;')"/>
			<xsl:variable name="result1" select="KIRStrUtils:set($arg2)"/>
			<!-- set BASE_NAME -->
			<xsl:variable name="arg3" select="concat('BASE_NAME;', $baseIndex, ';', $baseName)"/>
			<xsl:variable name="result2" select="KIRStrUtils:set($arg3)"/>
			<!-- set BASE_TYPE -->
			<xsl:variable name="arg4" select="concat('BASE_TYPE;', $baseIndex, ';#TCP')"/>
			<xsl:variable name="result3" select="KIRStrUtils:set($arg4)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Activity" mode="CFG">
		<xsl:variable name="objFrameName" select="MotionAttributes/ObjectFrameProfile"/>
		<xsl:variable name="objFrameNum">
			<xsl:apply-templates select="../../Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objFrameName]" mode="NUM"/>
		</xsl:variable>

		<xsl:if test="$objFrameNum > 32 ">
			<xsl:value-of select="$cr"/>
			<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
			<xsl:text>Error: More than 32 Object Profiles</xsl:text>
			<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
			<xsl:text>ERROR INFO END</xsl:text>
		</xsl:if>
		<xsl:if test="$objFrameNum &lt; 33 ">
			<xsl:apply-templates select="AttributeList/Attribute" mode="CFG">
				<xsl:with-param name="objFrameNum" select="$objFrameNum"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Attribute" mode="CFG">
		<xsl:param name="objFrameNum"/>
		<xsl:variable name="attrName" select="AttributeName"/>
		<xsl:choose>
			<xsl:when test="$attrName = $kirFrame">
				<xsl:variable name="attrValue" select="AttributeValue"/>
				<xsl:variable name="attrValueLen" select="string-length($attrValue)"/>
				<xsl:choose>
					<xsl:when test="substring($attrValue, number($attrValueLen)-$sufixLen+1) = $keywordSufix">
						<xsl:variable name="x">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirFrameX]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="y">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirFrameY]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="z">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirFrameZ]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="a">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirFrameR]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="b">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirFrameP]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="c">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirFrameW]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="x" select="format-number($x * $targetUnits, $toolPattern)"/>
						<xsl:variable name="y" select="format-number($y * $targetUnits, $toolPattern)"/>
						<xsl:variable name="z" select="format-number($z * $targetUnits, $toolPattern)"/>
						<xsl:variable name="a" select="format-number($a * $toDeg, $toolPattern)"/>
						<xsl:variable name="b" select="format-number($b * $toDeg, $toolPattern)"/>
						<xsl:variable name="c" select="format-number($c * $toDeg, $toolPattern)"/>

						<xsl:variable name="rx">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirRobotX]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="ry">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirRobotY]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="rz">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirRobotZ]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="ra">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirRobotR]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="rb">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirRobotP]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="rc">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="../Attribute[AttributeName=$kirRobotW]/AttributeValue"/>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="rx" select="format-number($rx * $targetUnits, $toolPattern)"/>
						<xsl:variable name="ry" select="format-number($ry * $targetUnits, $toolPattern)"/>
						<xsl:variable name="rz" select="format-number($rz * $targetUnits, $toolPattern)"/>
						<xsl:variable name="ra" select="format-number($ra * $toDeg, $toolPattern)"/>
						<xsl:variable name="rb" select="format-number($rb * $toDeg, $toolPattern)"/>
						<xsl:variable name="rc" select="format-number($rc * $toDeg, $toolPattern)"/>

						<xsl:variable name="arrayName" select="'BASE_DATA;'"/>
						<xsl:variable name="arg1" select="concat($arrayName, $objFrameNum, ';&#x7B;x ', $x)"/>
						<xsl:variable name="result1" select="KIRStrUtils:set($arg1)"/>
						<xsl:variable name="arg2" select="concat($arrayName, $objFrameNum, ';,y ', $y, ',z ', $z)"/>
						<xsl:variable name="result2" select="KIRStrUtils:cat($arg2)"/>
						<xsl:variable name="arg3" select="concat($arrayName, $objFrameNum, ';,a ', $a, ',b ', $b)"/>
						<xsl:variable name="result3" select="KIRStrUtils:cat($arg3)"/>
						<xsl:variable name="arg4" select="concat($arrayName, $objFrameNum, ';,c ', $c, '&#x7D;')"/>
						<xsl:variable name="result4" select="KIRStrUtils:cat($arg4)"/>

						<xsl:variable name="otherRobot" select="substring($attrValue, 1, number($attrValueLen)-$sufixLen)"/>
						<xsl:variable name="arg" select="concat('MACHINE_FRAME_DAT;', $objFrameNum, ';&#x7B;MACH_DEF_INDEX 2,PARENT[] &#x22;')"/>
						<xsl:variable name="result" select="KIRStrUtils:set($arg)"/>
						<xsl:variable name="arg" select="concat('MACHINE_FRAME_DAT;', $objFrameNum, ';', $otherRobot, '&#x22;')"/>
						<xsl:variable name="result" select="KIRStrUtils:cat($arg)"/>
						<xsl:variable name="arg" select="concat('MACHINE_FRAME_DAT;', $objFrameNum, ';,GEOMETRY[] &#x22; &#x22;&#x7D;')"/>
						<xsl:variable name="result" select="KIRStrUtils:cat($arg)"/>

						<xsl:variable name="arg" select="concat('MACHINE_DEF;2;&#x7B;NAME[] &#x22;', $otherRobot, '&#x22;,COOP_KRC_INDEX 1')"/>
						<xsl:variable name="result" select="KIRStrUtils:set($arg)"/>
						<xsl:variable name="arg" select="'MACHINE_DEF;2;,PARENT[] &#x22;WORLD&#x22;,ROOT &#x7B;'"/>
						<xsl:variable name="arg" select="concat($arg, 'x ', $rx, ',y ', $ry, ',z ', $rz, ',a ', $ra, ',b ', $rb, ',c ', $rc)"/>
						<xsl:variable name="arg" select="concat($arg, '&#x7D;,MECH_TYPE #ROBOT,GEOMETRY[] &#x22; &#x22;&#x7D;')"/>
						<xsl:variable name="result" select="KIRStrUtils:cat($arg)"/>

					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Attribute mode CFG -->

	<!-- Motion Attributes -->
	<xsl:template name="GetAccuracy">
		<xsl:param name="flyByMode" select="Off"/>
		<xsl:param name="moType" select="Joint"/>
		<xsl:param name="accuracyType" select="Speed"/>

		<xsl:if test="$flyByMode='On'">
			<xsl:if test="$moType='Joint'">C_PTP </xsl:if>
			<xsl:choose>
				<xsl:when test="$accuracyType='Speed'">C_VEL</xsl:when>
				<xsl:otherwise>C_DIS</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<!-- end of template GetAccuracy  -->

	<xsl:template name="GetSpeed">
		<xsl:param name="moType" select="Joint"/>
		<xsl:param name="speedValue" select="100"/>
		<xsl:param name="speedUnits" select="'%'"/>
		<xsl:choose>
			<xsl:when test="$moType='Joint'">
				<xsl:choose>
					<xsl:when test="$speedUnits = '%'">
						<xsl:value-of select="format-number($speedValue, $jointSpdPat)"/>
					</xsl:when>
					<xsl:when test="$speedUnits = 'm/s'">
						<xsl:value-of select="format-number($speedValue div $maxSpeedValue * 100, $jointSpdPat)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>50</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$speedUnits = '%'">
						<xsl:value-of select="format-number($speedValue * $maxSpeedValue div 100, $linearSpdPat)"/>
					</xsl:when>
					<xsl:when test="$speedUnits = 'm/s'">
						<xsl:value-of select="format-number($speedValue, $linearSpdPat)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="1.75"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template GetSpeed -->

	<xsl:template name="getTagNum">
		<xsl:param name="activity"/>
		<xsl:variable name="cartTgt" select="$activity/Target/CartesianTarget"/>
		<xsl:choose>
			<xsl:when test="$cartTgt">
				<xsl:call-template name="stripStr4num">
					<xsl:with-param name="str" select="$cartTgt/Tag"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template getTagNum -->

	<xsl:template name="stripStr4num">
		<xsl:param name="str"/>
		<xsl:choose>
			<xsl:when test="string-length($str)=0">0</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="string(number($str))='NaN'">
						<xsl:call-template name="stripStr4num">
							<xsl:with-param name="str" select="substring($str, 2)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="number($str)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template stripStr4num -->

	<xsl:template name="outputToolBase">
		<xsl:param name="activity"/>
		<xsl:variable name="toolName" select="$activity/MotionAttributes/ToolProfile"/>
		<xsl:variable name="baseName" select="$activity/MotionAttributes/ObjectFrameProfile"/>
		<xsl:variable name="toolProfile" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name=$toolName]"/>
		<xsl:variable name="objProfileList" select="/OLPData/Resource/Controller/ObjectFrameProfileList"/>
		<xsl:variable name="baseProfile" select="$objProfileList/ObjectFrameProfile[Name=$baseName]"/>
		<xsl:variable name="toolType" select="$toolProfile/ToolType"/>
		<xsl:variable name="toolNum">
			<xsl:apply-templates select="$toolProfile" mode="NUM"/>
		</xsl:variable>
		<xsl:variable name="baseNum">
			<xsl:variable name="baseSuffix" select="concat('_Tool_', $toolNum+1, '_FixedTCP')"/>
			<xsl:variable name="obj" select="$objProfileList/ObjectFrameProfile[contains(Name, $baseSuffix)]"/>
			<xsl:choose>
				<xsl:when test="$obj">
					<xsl:value-of select="$toolNum+1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="$baseProfile" mode="NUM"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
			
		<xsl:variable name="baseNameMapped">
			<xsl:call-template name="getBaseName">
				<xsl:with-param name="partName" select="$baseName"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:text> Tool[</xsl:text>
		<xsl:choose>
			<xsl:when test="$toolType = 'Stationary'">
				<xsl:value-of select="$baseNum"/>
				<xsl:text>]</xsl:text>
				<xsl:if test="$baseNum!=0 and $baseName!='Default'">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$baseNameMapped"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$toolNum"/>
				<xsl:text>]</xsl:text>
				<xsl:if test="$toolNum!=0 and $toolName!='Default' and starts-with($toolName, 'Tool[')=false">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$toolName"/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text> Base[</xsl:text>
		<xsl:choose>
			<xsl:when test="$toolType = 'Stationary'">
				<xsl:value-of select="$toolNum"/>
				<xsl:text>]</xsl:text>
				<xsl:if test="$toolNum!=0 and $toolName!='Default'">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$toolName"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$baseNum"/>
				<xsl:text>]</xsl:text>
				<xsl:if test="$baseNum!=0 and $baseName!='Default' and starts-with($baseName, 'Base[')=false">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$baseNameMapped"/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template outputToolBase -->

	<!-- Create DAT -->

	<xsl:template match="Activity" mode="DAT">
		<xsl:param name="group1" select="$auxGrp1"/>
		<xsl:param name="group2" select="$auxGrp2"/>
		<xsl:param name="group3" select="$auxGrp3"/>
		<xsl:variable name="tagRootNameAttr" select="../AttributeList/Attribute[AttributeName='TagRootName']/AttributeValue"/>

		<xsl:call-template name="processCommentsDAT">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

		<xsl:variable name="targetType" select="Target/@Default"/>
		<xsl:if test="@ActivityType='DNBRobotMotionActivity' and $targetType='Cartesian'">

			<NumberIncrement:counternum counter="2"/>
			<xsl:variable name="tagNum">
				<xsl:call-template name="getNumIncNext"/>
			</xsl:variable>
			<!-- KUKA tag names are limited to 23 characters, '.' is not allowed -->
			<xsl:variable name="tagName" select="Target/CartesianTarget/Tag"/>
			<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
			<xsl:variable name="tagPart" select="Target/CartesianTarget/Tag/@AttachedToPart"/>
			<xsl:variable name="tagComboKIR">
				<xsl:value-of select="$tagName"/><xsl:text>_kukataggroup_</xsl:text>
				<xsl:value-of select="$tagGroup"/>	<xsl:text>_kukaattachedtopart_</xsl:text>
				<xsl:value-of select="$tagPart"/><xsl:text>_</xsl:text>
				<xsl:value-of select="$tagNum"/>
			</xsl:variable>
			<xsl:variable name="tagCombo">
				<xsl:choose>
					<xsl:when test="$NumBaseAuxAxes + $NumToolAuxAxes + $NumPositionerAxes &gt; 0">
						<!-- tag may be used with different aux axis values, output them all -->
						<xsl:choose>
							<xsl:when test="$tagRootNameAttr">
								<xsl:value-of select="$tagRootNameAttr"/>
							</xsl:when>
							<xsl:when test="$tagRootName">
								<xsl:value-of select="$tagRootName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>P</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$tagNum"/>
					</xsl:when>
					<xsl:otherwise>
						<!-- no auxiliary axis, no need to output re-used tags -->
						<xsl:choose>
							<xsl:when test="$tagRootNameAttr">
								<xsl:value-of select="$tagRootNameAttr"/>
								<xsl:call-template name="getTagNum">
									<xsl:with-param name="activity" select="."/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="$tagRootName">
								<xsl:value-of select="$tagRootName"/>
								<xsl:call-template name="getTagNum">
									<xsl:with-param name="activity" select="."/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$tagName"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="moType" select="MotionAttributes/MotionType"/>
			<xsl:variable name="pgTagName" select="concat($tagPart,';',$tagGroup,';',$tagCombo)"/>
			<xsl:variable name="tagNameUnique" select="KIRStrUtils:getTagName($pgTagName, 'DAT')"/>
			<xsl:if test="$tagNameUnique">
				<!-- Position Line  -->
				<xsl:value-of select="$cr"/>
				<xsl:text>DECL </xsl:text>
				<xsl:choose>
					<xsl:when test="$pointFormat = 'POS'">
						<xsl:text>POS</xsl:text>
					</xsl:when>
					<xsl:when test="$pointFormat = 'E3POS'">
						<xsl:text>E3POS</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>E6POS</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:variable name="toolName" select="MotionAttributes/ToolProfile"/>
				<xsl:variable name="baseName" select="MotionAttributes/ObjectFrameProfile"/>
				<xsl:variable name="toolProfile" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name=$toolName]"/>
				<xsl:variable name="toolNum">
					<xsl:apply-templates select="$toolProfile" mode="NUM"/>
				</xsl:variable>
				<xsl:if test="$toolNum > 16 ">
					<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
					<xsl:text>Error: KUKA Tool Data can not be more than 16.</xsl:text>
					<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO END</xsl:text>
				</xsl:if>
				<xsl:variable name="objProfile" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$baseName]"/>
				<xsl:variable name="baseNum">
					<xsl:apply-templates select="$objProfile" mode="NUM"/>
				</xsl:variable>
				<xsl:if test="$baseNum > 32 ">
					<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
					<xsl:text>Error: KUKA Base Data can not be more than 32.</xsl:text>
					<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO END</xsl:text>
				</xsl:if>
				
				<xsl:variable name="zeroBase">
					<xsl:call-template name="checkZeroBase">
						<xsl:with-param name="baseName" select="$baseName"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="x">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@X"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="y">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@Y"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="z">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@Z"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="a">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Orientation/@Roll"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Orientation/@Pitch"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="c">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Orientation/@Yaw"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- for moves using non-zero object frame profiles -->
				<xsl:if test="$zeroBase = 0">
					<xsl:choose>
						<xsl:when test="$toolProfile/ToolType = 'Stationary' and $objProfile/@ApplyOffsetToTags = 'On'">
							<!-- Should multiply tool inverse to tag to get tag/tool.
							     For fixed tcp tags used with non-zero object profile, the position output to XML
								 is WRT a frame which is transformed by the object profile from the robot mount plate.
							     Therefore use the object profile here.
							-->
							<xsl:variable name="objX" select="$objProfile/ObjectFrame/ObjectFramePosition/@X"/>
							<xsl:variable name="objY" select="$objProfile/ObjectFrame/ObjectFramePosition/@Y"/>
							<xsl:variable name="objZ" select="$objProfile/ObjectFrame/ObjectFramePosition/@Z"/>
							<xsl:variable name="objW" select="$objProfile/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
							<xsl:variable name="objP" select="$objProfile/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
							<xsl:variable name="objR" select="$objProfile/ObjectFrame/ObjectFrameOrientation/@Roll"/>
							<xsl:variable name="objxyzwpr" select="concat($objX, ',', $objY, ',', $objZ, ',', $objW, ',', $objP, ',', $objR)"/>
							<xsl:variable name="objmat" select="MatrixUtils:dgXyzyprToMatrix($objxyzwpr)"/>
							<xsl:variable name="objinv" select="MatrixUtils:dgInvert()"/>
							<xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix(concat($x, ',', $y, ',', $z, ',', $c, ',', $b, ',', $a))"/>
							
							<xsl:variable name="x2">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="y2">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="z2">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="a2">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="MatrixUtils:dgGetRoll()"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="b2">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="MatrixUtils:dgGetPitch()"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="c2">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="MatrixUtils:dgGetYaw()"/>
								</xsl:call-template>
							</xsl:variable>
							
							<xsl:text> X</xsl:text>
							<xsl:value-of select="$tagNameUnique"/>
							<xsl:text>={x </xsl:text>
							<xsl:value-of select="format-number($x2 * $targetUnits, $cartesianPositionPattern)"/>
							<xsl:text>,y </xsl:text>
							<xsl:value-of select="format-number($y2 * $targetUnits, $cartesianPositionPattern)"/>
							<xsl:text>,z </xsl:text>
							<xsl:value-of select="format-number($z2 * $targetUnits, $cartesianPositionPattern)"/>
							<xsl:text>,a </xsl:text>
							<xsl:value-of select="format-number($a2, $cartesianPositionPattern)"/>
							<xsl:text>,b </xsl:text>
							<xsl:value-of select="format-number($b2, $cartesianPositionPattern)"/>
							<xsl:text>,c </xsl:text>
							<xsl:value-of select="format-number($c2, $cartesianPositionPattern)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text> X</xsl:text>
							<xsl:value-of select="$tagNameUnique"/>
							<xsl:text>={x </xsl:text>
							<xsl:value-of select="format-number($x * $targetUnits, $cartesianPositionPattern)"/>
							<xsl:text>,y </xsl:text>
							<xsl:value-of select="format-number($y * $targetUnits, $cartesianPositionPattern)"/>
							<xsl:text>,z </xsl:text>
							<xsl:value-of select="format-number($z * $targetUnits, $cartesianPositionPattern)"/>
							<xsl:text>,a </xsl:text>
							<xsl:value-of select="format-number($a, $cartesianPositionPattern)"/>
							<xsl:text>,b </xsl:text>
							<xsl:value-of select="format-number($b, $cartesianPositionPattern)"/>
							<xsl:text>,c </xsl:text>
							<xsl:value-of select="format-number($c, $cartesianPositionPattern)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- for moves using zero object frame profiles -->
				<xsl:if test="$zeroBase = 1">
					<xsl:if test="$toolProfile/ToolType = 'Stationary'">
						<xsl:text> X</xsl:text>
						<xsl:value-of select="$tagNameUnique"/>
						<xsl:text>={x </xsl:text>
						<xsl:value-of select="format-number($x * $targetUnits, $cartesianPositionPattern)"/>
						<xsl:text>,y </xsl:text>
						<xsl:value-of select="format-number($y * $targetUnits, $cartesianPositionPattern)"/>
						<xsl:text>,z </xsl:text>
						<xsl:value-of select="format-number($z * $targetUnits, $cartesianPositionPattern)"/>
						<xsl:text>,a </xsl:text>
						<xsl:value-of select="format-number($a, $cartesianPositionPattern)"/>
						<xsl:text>,b </xsl:text>
						<xsl:value-of select="format-number($b, $cartesianPositionPattern)"/>
						<xsl:text>,c </xsl:text>
						<xsl:value-of select="format-number($c, $cartesianPositionPattern)"/>
					</xsl:if>
					<xsl:if test="$toolProfile/ToolType = 'OnRobot'">
					<xsl:variable name="isRail" select="Target/JointTarget/AuxJoint/@Type=$auxGrp1"/>
					<xsl:variable name="railConfig">
						<xsl:choose>
							<xsl:when test="$isRail = 'true'">
								<xsl:value-of select="KIRStrUtils:getRailConfig()"/>
							</xsl:when>
							<xsl:otherwise>NONE</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<!-- robot WRT Kuka rail base -->
					<xsl:variable name="roll">
						<xsl:choose>
							<xsl:when test="$railConfig = 'A'">0</xsl:when>
							<xsl:when test="$railConfig = 'B'">90</xsl:when>
							<xsl:when test="$railConfig = 'C'">180</xsl:when>
							<xsl:when test="$railConfig = 'D'">270</xsl:when>
							<xsl:when test="$railConfig = 'H'">45</xsl:when>
							<xsl:when test="$railConfig = 'I'">135</xsl:when>
							<xsl:when test="$railConfig = 'J'">225</xsl:when>
							<xsl:when test="$railConfig = 'K'">315</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="matwpr" select="MatrixUtils:dgXyzyprToMatrix(concat('0,0,0,0,0,', $roll))"/>
					<xsl:variable name="bx">
						<xsl:choose>
							<xsl:when test="$isRail = 'true'">
								<xsl:value-of select="Target/JointTarget/AuxJoint[@Type=$auxGrp1]/JointValue"/>
							</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="by" select="0"/>
					<xsl:variable name="bz">
						<xsl:choose>
							<xsl:when test="$isRail = 'true' and $toolProfile/ToolType != 'Stationary'">
								<xsl:value-of select="number(KIRStrUtils:getZOffset()) div 1000"/>
							</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="xyz" select="concat($bx, ',', $by, ',', $bz, ',0,0,0')"/>
					<xsl:variable name="matxyz" select="MatrixUtils:dgCatXyzyprMatrix($xyz)"/>
					<xsl:variable name="dX">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="dY">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="dZ">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
						</xsl:call-template>
					</xsl:variable>
					
					<xsl:variable name="x1" select="$x + $dX"/>
					<xsl:variable name="y1" select="$y + $dY"/>
					<xsl:variable name="z1" select="$z + $dZ"/>
					<!-- for V5 tags using zero base, tag positions downloaded are WRT robot base(X along robot arm) -->
					<!-- for 45-degree robot-rail configurations, KUKA robot base X is normal to rail -->
					<!-- and in between the two neighboring configurations(config B or D) -->
					<!-- thus rotate the position from V5 robot base to KUKA robot base -->
					<xsl:variable name="roll45">
						<xsl:choose>
							<xsl:when test="$railConfig = 'H'">45</xsl:when>
							<xsl:when test="$railConfig = 'I'">-45</xsl:when>
							<xsl:when test="$railConfig = 'J'">45</xsl:when>
							<xsl:when test="$railConfig = 'K'">-45</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="mat45" select="MatrixUtils:dgXyzyprToMatrix(concat('0,0,0,0,0,', $roll45))"/>
					<xsl:variable name="result45" select="MatrixUtils:dgCatXyzyprMatrix(concat($x1, ',', $y1, ',', $z1, ',', $c, ',', $b, ',', $a))"/>
					
					<xsl:variable name="x2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="y2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="z2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="a2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetRoll()"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="b2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetPitch()"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="c2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetYaw()"/>
						</xsl:call-template>
					</xsl:variable>
					
					<xsl:text> X</xsl:text>
					<xsl:value-of select="$tagNameUnique"/>
					<xsl:text>={x </xsl:text>
					<xsl:value-of select="format-number($x2 * $targetUnits, $cartesianPositionPattern)"/>
					<xsl:text>,y </xsl:text>
					<xsl:value-of select="format-number($y2 * $targetUnits, $cartesianPositionPattern)"/>
					<xsl:text>,z </xsl:text>
					<xsl:value-of select="format-number($z2 * $targetUnits, $cartesianPositionPattern)"/>
					<xsl:text>,a </xsl:text>
					<xsl:value-of select="format-number($a2, $cartesianPositionPattern)"/>
					<xsl:text>,b </xsl:text>
					<xsl:value-of select="format-number($b2, $cartesianPositionPattern)"/>
					<xsl:text>,c </xsl:text>
					<xsl:value-of select="format-number($c2, $cartesianPositionPattern)"/>
				</xsl:if>
					<!-- end ToolType = 'OnRobot' -->
				</xsl:if>
				<!-- end $zeroBase = 1 -->
				<xsl:text>,s </xsl:text>
				<xsl:value-of select="substring-after(Target/CartesianTarget/Config/@Name, 'S')"/>
				<xsl:text>,t </xsl:text>
				<xsl:variable name="jv1">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 1' or @DOFNumber='1']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="jv2">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 2' or @DOFNumber='2']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="jv3">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 3' or @DOFNumber='3']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="jv4">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 4' or @DOFNumber='4']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="jv5">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 5' or @DOFNumber='5']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="jv6">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 6' or @DOFNumber='6']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="turnBit1" select="$jv1 &lt; 0"/>
				<xsl:variable name="turnBit2" select="$jv2 &lt; 0"/>
				<xsl:variable name="turnBit3" select="$jv3 &lt; 0"/>
				<xsl:variable name="turnBit4" select="$jv4 &lt; 0"/>
				<xsl:variable name="turnBit5" select="$jv5 &lt; 0"/>
				<xsl:variable name="turnBit6" select="$jv6 &lt; 0"/>
				<xsl:value-of select="$turnBit6*32 + $turnBit5*16 + $turnBit4*8 + $turnBit3*4 + $turnBit2*2 + $turnBit1"/>
				<xsl:choose>
					<xsl:when test="$pointFormat = 'POS'">
						<xsl:text>}</xsl:text>
					</xsl:when>
					<xsl:when test="$pointFormat = 'E3POS'">
						<NumberIncrement:counternum counter="5"/>
						<NumberIncrement:reset/>
						<xsl:choose>
							<xsl:when test="$NumBaseAuxAxes + $NumToolAuxAxes + $NumPositionerAxes = 0">
								<xsl:text>,e1 0.0,e2 0.0,e3 0.0}</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="processAuxAxes">
									<xsl:with-param name="jTarget" select="Target/JointTarget"/>
									<xsl:with-param name="auxType" select="$group1"/>
								</xsl:call-template>
								<xsl:call-template name="processAuxAxes">
									<xsl:with-param name="jTarget" select="Target/JointTarget"/>
									<xsl:with-param name="auxType" select="$group2"/>
								</xsl:call-template>
								<xsl:call-template name="processAuxAxes">
									<xsl:with-param name="jTarget" select="Target/JointTarget"/>
									<xsl:with-param name="auxType" select="$group3"/>
								</xsl:call-template>
								<xsl:variable name="extAxisNum">
									<xsl:call-template name="getNumIncCurrent"/>
								</xsl:variable>

								<xsl:choose>
									<xsl:when test="$extAxisNum=1">
										<xsl:text>,e2 0.0,e3 0.0}</xsl:text>
									</xsl:when>
									<xsl:when test="$extAxisNum=2">
										<xsl:text>,e3 0.0}</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>}</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<!-- E6POS -->
						<NumberIncrement:counternum counter="5"/>
						<NumberIncrement:reset/>
						<xsl:choose>
							<xsl:when test="$NumBaseAuxAxes + $NumToolAuxAxes + $NumPositionerAxes = 0">
								<xsl:text>,e1 0.0,e2 0.0,e3 0.0,e4 0.0,e5 0.0,e6 0.0}</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="processAuxAxes">
									<xsl:with-param name="jTarget" select="Target/JointTarget"/>
									<xsl:with-param name="auxType" select="$group1"/>
								</xsl:call-template>
								<xsl:call-template name="processAuxAxes">
									<xsl:with-param name="jTarget" select="Target/JointTarget"/>
									<xsl:with-param name="auxType" select="$group2"/>
								</xsl:call-template>
								<xsl:call-template name="processAuxAxes">
									<xsl:with-param name="jTarget" select="Target/JointTarget"/>
									<xsl:with-param name="auxType" select="$group3"/>
								</xsl:call-template>
								<xsl:variable name="extAxisNum">
									<xsl:call-template name="getNumIncCurrent"/>
								</xsl:variable>

								<xsl:choose>
									<xsl:when test="$extAxisNum=1">
										<xsl:text>,e2 0.0,e3 0.0,e4 0.0,e5 0.0,e6 0.0}</xsl:text>
									</xsl:when>
									<xsl:when test="$extAxisNum=2">
										<xsl:text>,e3 0.0,e4 0.0,e5 0.0,e6 0.0}</xsl:text>
									</xsl:when>
									<xsl:when test="$extAxisNum=3">
										<xsl:text>,e4 0.0,e5 0.0,e6 0.0}</xsl:text>
									</xsl:when>
									<xsl:when test="$extAxisNum=4">
										<xsl:text>,e5 0.0,e6 0.0}</xsl:text>
									</xsl:when>
									<xsl:when test="$extAxisNum=5">
										<xsl:text>,e6 0.0}</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>}</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						<!-- end E6POS -->
					</xsl:otherwise>
				</xsl:choose>

				<xsl:if test="$moType!='CircularVia'">
					<!-- FDAT Line -->
					<xsl:value-of select="$cr"/>
					<xsl:text>DECL FDAT F</xsl:text>
					<xsl:value-of select="$tagNameUnique"/>
					<!-- TOOL_NO -->
					<xsl:text>={TOOL_NO </xsl:text>
					<xsl:choose>
						<xsl:when test="$toolProfile/ToolType = 'Stationary'">
							<xsl:apply-templates select="$objProfile" mode="NUM"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="$toolProfile" mode="NUM"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- BASE_NO -->
					<xsl:text>,BASE_NO </xsl:text>
					<xsl:choose>
						<xsl:when test="$toolProfile/ToolType = 'Stationary'">
							<xsl:apply-templates select="$toolProfile" mode="NUM"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="$objProfile" mode="NUM"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- IPO_FRAME -->
					<xsl:choose>
						<xsl:when test="$toolProfile/ToolType = 'Stationary'">
							<xsl:text>,IPO_FRAME #TCP}</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>,IPO_FRAME #BASE}</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:if><!-- $tagNameUnique -->

			<xsl:if test="$moType!='CircularVia'">
				<!-- PDAT/LCPDAT Line -->
				<xsl:variable name="motionProfile" select="MotionAttributes/MotionProfile"/>
				<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
				<xsl:variable name="datName">
					<xsl:choose>
						<xsl:when test="$moType='Joint'">ppdat</xsl:when>
						<xsl:otherwise>lcpdat</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="pdatCombo" select="concat($motionProfile,';',$accuracyProfile)"/>
				<xsl:variable name="pdatIdx" select="KIRStrUtils:getDATIndex($datName, $pdatCombo)"/>
				<xsl:if test="$pdatIdx='-1'">
					<xsl:call-template name="outputPDAT">
						<xsl:with-param name="moType" select="$moType"/>
						<xsl:with-param name="motionProfile" select="$motionProfile"/>
						<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:if><!-- END xsl:if test="$moType!='CircularVia'" -->

		</xsl:if><!-- END xsl:if test="@ActivityType='DNBRobotMotionActivity'" -->

		<xsl:call-template name="processCommentsDAT">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

	</xsl:template>
	<!-- end of template Activity mode="DAT" -->

	<xsl:template name="outputPDAT">
		<xsl:param name="moType"/>
		<xsl:param name="motionProfile"/>
		<xsl:param name="accuracyProfile"/>

		<xsl:variable name="datName">
			<xsl:choose>
				<xsl:when test="$moType='Joint'">ppdat</xsl:when>
				<xsl:otherwise>lcpdat</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:value-of select="$cr"/>
		<xsl:variable name="speedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Value"/>
		<xsl:variable name="speedUnits" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Units"/>
		<xsl:variable name="accelValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Accel/@Value"/>
		<xsl:choose>
			<xsl:when test="$moType='Joint'">
				<xsl:text>DECL PDAT PPDAT</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>DECL LDAT LCPDAT</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="KIRStrUtils:addDAT($datName, concat($motionProfile,';',$accuracyProfile))"/>
		<xsl:text>={VEL </xsl:text>
		<xsl:call-template name="GetSpeed">
			<xsl:with-param name="moType" select="$moType"/>
			<xsl:with-param name="speedValue" select="$speedValue"/>
			<xsl:with-param name="speedUnits" select="$speedUnits"/>
		</xsl:call-template>
		<xsl:text>,ACC </xsl:text>
		<xsl:value-of select="$accelValue"/>
		<xsl:text>,APO_DIST </xsl:text>
		<xsl:variable name="accuracyNode" select="../../Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]"/>
		<xsl:choose>
			<xsl:when test="$accuracyNode/FlyByMode='Off'">0</xsl:when>
			<xsl:otherwise> <!-- FlyByMode = 'On' -->
				<xsl:choose>
					<xsl:when test="$accuracyNode/AccuracyType='Speed'">
						<xsl:value-of select="$accuracyNode/AccuracyValue/@Value"/>
					</xsl:when>
					<xsl:otherwise> <!-- Distance -->
						<xsl:value-of select="$accuracyNode/AccuracyValue/@Value * 1000"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:if test="$moType='Linear' or $moType='Circular'">
			<xsl:text>,APO_FAC 50.0</xsl:text>
			<xsl:if test="number(substring-before($softwareV, '.')) >= 5">
				<xsl:text>,ORI_TYP #VAR</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:text>}</xsl:text>
	</xsl:template>

	<xsl:template match="Action" mode="DAT">
		<xsl:choose>
			<xsl:when test="@ActionType='KukaSpotWeld'">
				<xsl:value-of select="$cr"/>
				<xsl:text>DECL SPOT_TYPE SSDAT</xsl:text>
				<NumberIncrement:counternum counter="3"/>
				<xsl:call-template name="getNumIncNext"/>

				<xsl:text>={GUN </xsl:text>
				<xsl:variable name="gunNum" select="AttributeList/Attribute[AttributeName='Gun']/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="$gunNum != ''">
						<xsl:value-of select="$gunNum"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:text>,PAIR #FIRST</xsl:text>

				<xsl:text>,RETR #</xsl:text>
				<xsl:variable name="retractDAT" select="AttributeList/Attribute[AttributeName='Retract']/AttributeValue"/>
				<xsl:call-template name="outputRetract">
					<xsl:with-param name="retractState" select="$retractDAT"/>
				</xsl:call-template>

				<xsl:text>,CLO_TM </xsl:text>
				<xsl:variable name="earlyCloseTime" select="AttributeList/Attribute[AttributeName='Early Closing Time']/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="$earlyCloseTime != '' and $earlyCloseTime &lt; '0'">
						<xsl:value-of select="$earlyCloseTime"/>
					</xsl:when>
					<xsl:when test="$earlyCloseTime != '' and $earlyCloseTime &gt; '0'">
						<xsl:text>-</xsl:text>
						<xsl:value-of select="$earlyCloseTime"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:text>,PGNO1 </xsl:text>
				<xsl:variable name="timerProgramNum" select="AttributeList/Attribute[AttributeName='Timer Program Number']/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="$timerProgramNum != ''">
						<xsl:value-of select="$timerProgramNum"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:text>,PRESS1 </xsl:text>
				<xsl:variable name="press1" select="AttributeList/Attribute[AttributeName='Pressure']/AttributeValue"/>
				<xsl:variable name="press1output" select="format-number($press1, '0.0')"/>
				<xsl:choose>
					<xsl:when test="$press1 != ''">
						<xsl:value-of select="$press1output"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0.0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:text>,PGNO2 0,PRESS2 0.0}</xsl:text>
			</xsl:when>
			<xsl:when test="@ActionType='KukaSpotRetract'">
			</xsl:when>
			<xsl:when test="@ActionType='KukaGripperOpen' or @ActionType='KukaGripperClose'">
				<xsl:variable name="grpCont"     select="AttributeList/Attribute[AttributeName='Cont']/AttributeValue"/>
				<xsl:variable name="grpAPO"      select="AttributeList/Attribute[AttributeName='G_APO']/AttributeValue"/>
				<xsl:variable name="grpWaitTime" select="AttributeList/Attribute[AttributeName='WaitTime']/AttributeValue"/>
				<xsl:variable name="grpCTRL"     select="AttributeList/Attribute[AttributeName='CTRL']/AttributeValue"/>
				<xsl:variable name="grpDLY"      select="AttributeList/Attribute[AttributeName='DLY']/AttributeValue"/>
				<xsl:variable name="grpDST"      select="AttributeList/Attribute[AttributeName='DST']/AttributeValue"/>
				<xsl:variable name="gdatCombo"   select="concat($grpAPO,';',$grpWaitTime,';',$grpCTRL,';',$grpDLY,';',$grpDST)"/>
				<xsl:variable name="datName" select="'ggdat'"/>
				<xsl:choose>
					<xsl:when test="$grpCont='1'">
						<xsl:variable name="gdatNumCurrent" select="KIRStrUtils:getGDATIndexCurr()"/>
						<xsl:if test="$gdatNumCurrent &lt; '1'">
							<xsl:call-template name="outputGDAT"/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="gdatIdx" select="KIRStrUtils:getDATIndex($datName, $gdatCombo)"/>
						<xsl:if test="$gdatIdx='-1'">
							<xsl:call-template name="outputGDAT"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Action mode="DAT" -->

	<xsl:template name="outputGDAT">
		<xsl:variable name="grpAPO"      select="AttributeList/Attribute[AttributeName='G_APO']/AttributeValue"/>
		<xsl:variable name="grpWaitTime" select="AttributeList/Attribute[AttributeName='WaitTime']/AttributeValue"/>
		<xsl:variable name="grpCTRL"     select="AttributeList/Attribute[AttributeName='CTRL']/AttributeValue"/>
		<xsl:variable name="grpDLY"      select="AttributeList/Attribute[AttributeName='DLY']/AttributeValue"/>
		<xsl:variable name="grpDST"      select="AttributeList/Attribute[AttributeName='DST']/AttributeValue"/>
		<xsl:variable name="gdatCombo"   select="concat($grpAPO,';',$grpWaitTime,';',$grpCTRL,';',$grpDLY,';',$grpDST)"/>

		<xsl:value-of select="$cr"/>
		<xsl:text>DECL GRP_TYP GGDAT</xsl:text>
		<xsl:value-of select="KIRStrUtils:addDAT('ggdat', $gdatCombo)"/>
		<xsl:text>={G_APO #NO,TIME </xsl:text>
		<xsl:value-of select="$grpWaitTime"/>
		<xsl:text>,CTRL #</xsl:text>
		<xsl:choose>
			<xsl:when test="$grpCTRL='0'">OFF</xsl:when>
			<xsl:otherwise>ON</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,DLY 0,DST 1}</xsl:text>
	</xsl:template>
	<!-- end of template outputGDAT -->

	<xsl:template name="outputRetract">
		<xsl:param name="retractState"/>

		<xsl:choose>
			<xsl:when test="$retractState = 'Close'">
				<xsl:text>CLO</xsl:text>
			</xsl:when>
			<xsl:when test="$retractState = 'Open'">
				<xsl:text>OPN</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>CLO</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="ToolProfile" mode="NUM">
		<xsl:value-of select="count(preceding-sibling::ToolProfile)"/>
	</xsl:template>

	<xsl:template match="ObjectFrameProfile" mode="NUM">
		<!-- ordinal number in profile list -->
		<xsl:variable name="baseNum" select="count(preceding-sibling::ObjectFrameProfile)"/>

		<xsl:variable name="baseName">
			<xsl:call-template name="getBaseName">
				<xsl:with-param name="partName" select="Name"/>
			</xsl:call-template>
		</xsl:variable>

		<!-- get base index from the mapping, return same number if not in the mapping -->
		<xsl:value-of select="KIRStrUtils:getBaseIndex($baseName, $baseNum)"/>
	</xsl:template>

	<xsl:template name="getBaseName">
		<xsl:param name="partName"/>

		<!-- strip sufix '.ObjectFrameProfile' if exists -->
		<xsl:variable name="baseName">
			<xsl:choose>
				<xsl:when test="substring-before($partName, '.ObjectFrameProfile')">
					<xsl:value-of select="substring-before($partName, '.ObjectFrameProfile')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$partName"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!-- get mapped base name -->
		<xsl:value-of select="KIRStrUtils:getBaseName($baseName)"/>
	</xsl:template>

	<!-- output gripper Start or End field -->
	<xsl:template name="grpStartEnd">
		<xsl:param name="startEnd"/>

		<xsl:choose>
			<xsl:when test="$startEnd='0'">Start</xsl:when>
			<xsl:when test="$startEnd='1'">End</xsl:when>
			<xsl:otherwise>Wrong attribute value</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Create SRC -->
	<xsl:template match="Action" mode="SRC">
		<xsl:variable name="grpNum"      select="AttributeList/Attribute[AttributeName='GripperNumber']/AttributeValue"/>
		<xsl:variable name="grpStartEnd" select="AttributeList/Attribute[AttributeName='StartEnd']/AttributeValue"/>
		<xsl:variable name="grpDelay"    select="AttributeList/Attribute[AttributeName='Delay']/AttributeValue"/>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

		<xsl:choose>
			<xsl:when test="@ActionType='KukaSpotWeld'"></xsl:when>
			<xsl:when test="@ActionType='KukaSpotRetract'"></xsl:when>
			<xsl:when test="@ActionType='KukaServoGunWeld'"></xsl:when>
			<xsl:when test="@ActionType='KukaServoGunTipDress'"></xsl:when>
			<!-- Gripper Check -->
			<xsl:when test="@ActionType='KukaGripperCheck'">
				<xsl:variable name="grpSt" select="AttributeList/Attribute[AttributeName='GripperState']/AttributeValue"/>

				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD CHECK GRP </xsl:text>
				<xsl:value-of select="$grpNum"/>
				<xsl:text> State= </xsl:text>
				<xsl:value-of select="ActivityList/Activity/Target/JointTarget/HomeName"/>
				<xsl:text> at </xsl:text>
				<xsl:call-template name="grpStartEnd">
					<xsl:with-param name="startEnd" select="$grpStartEnd"/>
				</xsl:call-template>
				<xsl:text> Delay= </xsl:text>
				<xsl:value-of select="$grpDelay"/>
				<xsl:text> ms;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPGRP,%CCHK_GRP,%VCHK_GRP,%P 2:</xsl:text>
				<xsl:value-of select="$grpNum"/>
				<xsl:text>, 4:</xsl:text>
				<xsl:value-of select="$grpSt"/>
				<xsl:text>, 6:</xsl:text>
				<xsl:value-of select="$grpStartEnd"/>
				<xsl:text>, 8:</xsl:text>
				<xsl:value-of select="$grpDelay"/>

				<xsl:value-of select="$cr"/>
				<xsl:text>TRIGGER WHEN DISTANCE=</xsl:text>
				<xsl:value-of select="$grpStartEnd"/>
				<xsl:text> DELAY=</xsl:text>
				<xsl:value-of select="$grpDelay"/>
				<xsl:text> DO H50(CHK_APO_UP,</xsl:text>
				<xsl:value-of select="$grpNum"/>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$grpSt"/>
				<xsl:text>,GCONT) PRIO=-1</xsl:text>

				<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<!-- Gripper Close/Open -->
			<xsl:when test="@ActionType='KukaGripperOpen' or @ActionType='KukaGripperClose'">
				<xsl:variable name="grpCont"     select="AttributeList/Attribute[AttributeName='Cont']/AttributeValue"/>
				<xsl:variable name="grpAPO"      select="AttributeList/Attribute[AttributeName='G_APO']/AttributeValue"/>
				<xsl:variable name="grpWaitTime" select="AttributeList/Attribute[AttributeName='WaitTime']/AttributeValue"/>
				<xsl:variable name="grpCTRL"     select="AttributeList/Attribute[AttributeName='CTRL']/AttributeValue"/>
				<xsl:variable name="grpDLY"      select="AttributeList/Attribute[AttributeName='DLY']/AttributeValue"/>
				<xsl:variable name="grpDST"      select="AttributeList/Attribute[AttributeName='DST']/AttributeValue"/>
				<xsl:variable name="gdatCombo"   select="concat($grpAPO,';',$grpWaitTime,';',$grpCTRL,';',$grpDLY,';',$grpDST)"/>
				<xsl:variable name="grpSt">
					<xsl:choose>
						<xsl:when test="@ActionType='KukaGripperOpen'">1</xsl:when>
						<xsl:otherwise>2</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<xsl:variable name="gdatNum">
					<xsl:call-template name="getGDATnum">
						<xsl:with-param name="cont" select="$grpCont"/>
						<xsl:with-param name="gdat" select="$gdatCombo"/>
					</xsl:call-template>
				</xsl:variable>

				<xsl:if test="@ActionType='KukaGripperClose'">
					<xsl:value-of select="$cr"/>
					<xsl:text>;DELMIA Process Info:grabbingObj:</xsl:text>
					<xsl:variable name="grabAct" select="ActivityList/Activity[@ActivityType='GrabActivity']"/>
					<xsl:value-of select="$grabAct/GrabbingObject"/>
					<xsl:text>;grabbedObj:</xsl:text>
					<xsl:value-of select="$grabAct/GrabbedObjectList/GrabbedObject"/>
				</xsl:if>

				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD SET GRP </xsl:text>
				<xsl:value-of select="$grpNum"/>
				<xsl:text> State= </xsl:text>
				<xsl:value-of select="ActivityList/Activity/Target/JointTarget/HomeName"/>
				<xsl:choose>
					<xsl:when test="$grpCont='1'">
						<xsl:text> CONT at </xsl:text>
						<xsl:call-template name="grpStartEnd">
							<xsl:with-param name="startEnd" select="$grpStartEnd"/>
						</xsl:call-template>
						<xsl:text> Delay= </xsl:text>
						<xsl:value-of select="$grpDelay"/>
						<xsl:text> ms</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> GDAT</xsl:text>
						<xsl:value-of select="$gdatNum"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPGRP,%CGRP,%VGRP,%P 2:</xsl:text>
				<xsl:value-of select="$grpNum"/>
				<xsl:text>, 4:</xsl:text>
				<xsl:value-of select="$grpSt"/>
				<xsl:text>, 5:</xsl:text>
				<xsl:choose>
					<xsl:when test="$grpCont='1'">#YES</xsl:when>
					<xsl:otherwise>#NO</xsl:otherwise>
				</xsl:choose>
				<xsl:text>, 6:GDAT</xsl:text>
				<xsl:value-of select="$gdatNum"/>
				<xsl:text>, 8:</xsl:text>
				<xsl:value-of select="$grpStartEnd"/>
				<xsl:text>, 10:</xsl:text>
				<xsl:value-of select="$grpDelay"/>

				<xsl:value-of select="$cr"/>
				<xsl:choose>
					<xsl:when test="$grpCont='1'">
						<xsl:text>TRIGGER WHEN DISTANCE=</xsl:text>
						<xsl:value-of select="$grpStartEnd"/>
						<xsl:text> DELAY=</xsl:text>
						<xsl:value-of select="$grpDelay"/>
						<xsl:text> DO H50(GRP_APO,</xsl:text>
						<xsl:value-of select="$grpNum"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of select="$grpSt"/>
						<xsl:text>,GCONT) PRIO=-1</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>H50(GRP,</xsl:text>
						<xsl:value-of select="$grpNum"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of select="$grpSt"/>
						<xsl:text>,GGDAT</xsl:text>
						<xsl:value-of select="$gdatNum"/>
						<xsl:text>)</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<!-- ServoGun Init -->
			<xsl:when test="@ActionType='KukaServoGunInit'">
				<xsl:variable name="gunNumber" select="AttributeList/Attribute[AttributeName='GunNumber']/AttributeValue"/>
				<xsl:variable name="newSame"   select="AttributeList/Attribute[AttributeName='NewSame']/AttributeValue"/>

				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD INIT ServoGun= </xsl:text>
				<xsl:value-of select="$gunNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$newSame"/>
				<xsl:text>;%{PE}%R 1.0.11,%MKUKATPSERVOTECH,%CInitGun,%VIniG,%P 2:</xsl:text>
				<xsl:value-of select="$gunNumber"/>
				<xsl:text>, 3:INIT_</xsl:text>
				<xsl:choose>
					<xsl:when test="$newSame='New'">FIRST</xsl:when>
					<xsl:otherwise>CYC</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
				<xsl:text>$BWDSTART=FALSE</xsl:text>			<xsl:value-of select="$cr"/>
				<xsl:text>EG_EXTAX_ACTIVE=EG_SERVOGUN_EXAXIS[</xsl:text>
				<xsl:value-of select="$gunNumber"/>
				<xsl:text>]</xsl:text>								<xsl:value-of select="$cr"/>
				<xsl:text>BAS(#FRAME)</xsl:text>				<xsl:value-of select="$cr"/>
				<xsl:text>P_INIT=$POS_ACT</xsl:text>			<xsl:value-of select="$cr"/>
				<xsl:text>PTP P_INIT</xsl:text>					<xsl:value-of select="$cr"/>
				<xsl:text>CMD=#INIT_</xsl:text>
				<xsl:choose>
					<xsl:when test="$newSame='New'">FIRST</xsl:when>
					<xsl:otherwise>CYC</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
				<xsl:text>EG_SPOT_POINT=P_INIT</xsl:text>	<xsl:value-of select="$cr"/>
				<xsl:text>EG_SPOT( )</xsl:text>					<xsl:value-of select="$cr"/>
				<xsl:text>PTP P_INIT</xsl:text>					<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<!-- ServoGun Couple -->
			<xsl:when test="@ActionType='KukaServoGunCouple'">
				<xsl:variable name="gunNumber" select="AttributeList/Attribute[AttributeName='GunNumber']/AttributeValue"/>

				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD ServoGun Couple= </xsl:text>
				<xsl:value-of select="$gunNumber"/>
				<xsl:text>;%{PE}%R 1.0.11,%MKUKATPSERVOTECH,%CGun,%Vcouple,%P 2:</xsl:text>
				<xsl:value-of select="$gunNumber"/>			<xsl:value-of select="$cr"/>
				<xsl:text>EG_CHANGING(#COUPLE,</xsl:text>
				<xsl:value-of select="$gunNumber"/><xsl:text>)</xsl:text>	<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<!-- ServoGun Decouple -->
			<xsl:when test="@ActionType='KukaServoGunDecouple'">
				<xsl:variable name="gunNumber" select="AttributeList/Attribute[AttributeName='GunNumber']/AttributeValue"/>

				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD ServoGun Decouple= </xsl:text>
				<xsl:value-of select="$gunNumber"/>
				<xsl:text>;%{PE}%R 1.0.11,%MKUKATPSERVOTECH,%CGun,%Vdecouple,%P 2:</xsl:text>
				<xsl:value-of select="$gunNumber"/>			<xsl:value-of select="$cr"/>
				<xsl:text>EG_CHANGING(#DECOUPLE,</xsl:text>
				<xsl:value-of select="$gunNumber"/><xsl:text>)</xsl:text>	<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$cr"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="ActionName"/>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

	</xsl:template>
	<!-- end of template Action mode="SRC" -->

	<xsl:template match="Activity" mode="SRC">
		<xsl:variable name="setSignalValue"   select="SetIOAttributes/IOSetSignal/@SignalValue"/>
		<xsl:variable name="setSignalName"    select="SetIOAttributes/IOSetSignal/@SignalName"/>
		<xsl:variable name="signalDuration"   select="SetIOAttributes/IOSetSignal/@SignalDuration"/>
		<xsl:variable name="outputPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
		<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
		<xsl:variable name="waitSignalName"  select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
		<xsl:variable name="inputPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
		<xsl:variable name="setSignalState">
			<xsl:choose>
				<xsl:when test="$setSignalValue='On'">TRUE</xsl:when>
				<xsl:otherwise>FALSE</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="waitSignalState">
			<xsl:choose>
				<xsl:when test="$waitSignalValue='On'">TRUE</xsl:when>
				<xsl:otherwise>FALSE</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<xsl:apply-templates select="AttributeList/Attribute" mode="STD"/>
		
		<xsl:choose>
			<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
				<xsl:choose>
					<xsl:when test="number(substring-before($softwareV, '.')) >= 5">
						<!-- Line 1 -->
						<xsl:value-of select="$cr"/>
						<xsl:text>;FOLD WAIT FOR </xsl:text>
						<xsl:if test="$waitSignalValue='Off'">NOT </xsl:if>
						<xsl:text>( IN </xsl:text>
						<xsl:value-of select="$inputPortNumber"/>
						<xsl:text> '</xsl:text>
						<xsl:value-of select="$waitSignalName"/>
						<xsl:text>' );%{PE}%R </xsl:text>
						<xsl:value-of select="$softwareV"/>
						<xsl:text>,%MKUKATPBASIS,%CEXT_WAIT_FOR,%VEXT_WAIT_FOR,%P 2:</xsl:text>
						<xsl:if test="$waitSignalValue='Off'">NOT</xsl:if>
						<xsl:text>, 4:, 5:$IN, 6:</xsl:text>
						<xsl:value-of select="$inputPortNumber"/>
						<xsl:text>, 7:</xsl:text><xsl:value-of select="$waitSignalName"/>
						<xsl:text>, 9:</xsl:text>
						<!-- Line 2 -->
						<xsl:value-of select="$cr"/>
						<xsl:text>WAIT FOR </xsl:text>
						<xsl:if test="$waitSignalValue='Off'">NOT </xsl:if>
						<xsl:text>( $IN[</xsl:text>
						<xsl:value-of select="$inputPortNumber"/>
						<xsl:text>] )</xsl:text>
						<!-- Line 3 -->
						<xsl:value-of select="$cr"/>
						<xsl:text>;ENDFOLD</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<!-- Line 1 -->
						<xsl:value-of select="$cr"/>
						<xsl:text>;FOLD WAIT FOR IN </xsl:text>
						<xsl:value-of select="$inputPortNumber"/>
						<xsl:text> '</xsl:text>
						<xsl:value-of select="$waitSignalName"/>
						<xsl:text>'  State= </xsl:text>
						<xsl:value-of select="$waitSignalState"/>
						<xsl:text> ;%{PE}%R 4.1.5,%MKUKATPBASIS,%CWAIT_FOR,%VWAIT_FOR,%P 1:0, 2:</xsl:text>
						<xsl:value-of select="$inputPortNumber"/>
						<xsl:text>, 3:'</xsl:text>
						<xsl:value-of select="$waitSignalName"/>
						<xsl:text>', 5:</xsl:text>
						<xsl:value-of select="$waitSignalState"/>
						<xsl:text>, 6:</xsl:text>
						<!-- Line 2 -->
						<xsl:value-of select="$cr"/>
						<xsl:text>WAIT FOR $IN[</xsl:text>
						<xsl:value-of select="$inputPortNumber"/>
						<xsl:text>]==</xsl:text>
						<xsl:value-of select="$waitSignalState"/>
						<!-- Line 3 -->
						<xsl:value-of select="$cr"/>
						<xsl:text>;ENDFOLD</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBSetSignalActivity' and $signalDuration=0">
				<!-- Line 1 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD OUT </xsl:text>
				<xsl:value-of select="$outputPortNumber"/>
				<xsl:text> '</xsl:text>
				<xsl:value-of select="$setSignalName"/>
				<xsl:text>'  State= </xsl:text>
				<xsl:value-of select="$setSignalState"/>
				<xsl:text> CONT;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPBASIS,%COUT,%VOUTX,%P 2:</xsl:text>
				<xsl:value-of select="$outputPortNumber"/>
				<xsl:text>, 3:</xsl:text>
				<xsl:if test="number(substring-before($softwareV, '.')) &lt; 5">
					<xsl:text>'</xsl:text>
				</xsl:if>
				<xsl:value-of select="$setSignalName"/>
				<xsl:if test="number(substring-before($softwareV, '.')) &lt; 5">
					<xsl:text>'</xsl:text>
				</xsl:if>
				<xsl:text>, 5:</xsl:text>
				<xsl:value-of select="$setSignalState"/>
				<xsl:text>, 6:CONTINUE</xsl:text>
				<!-- Line 2 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>CONTINUE</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>$OUT[</xsl:text>
				<xsl:value-of select="$outputPortNumber"/>
				<xsl:text>]=</xsl:text>
				<xsl:value-of select="$setSignalState"/>
				<!-- Line 3 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBSetSignalActivity' and $signalDuration!=0">
				<!-- Line 1 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD PULSE </xsl:text>
				<xsl:value-of select="$outputPortNumber"/>
				<xsl:text> '</xsl:text>
				<xsl:value-of select="$setSignalName"/>
				<xsl:text>'  State= </xsl:text>
				<xsl:value-of select="$setSignalState"/>
				<xsl:text> CONT Time= </xsl:text>
				<xsl:value-of select="$signalDuration"/>
				<xsl:text> sec;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPBASIS,%COUT,%VPULSE,%P 2:</xsl:text>
				<xsl:value-of select="$outputPortNumber"/>
				<xsl:text>, 3:</xsl:text>
				<xsl:if test="number(substring-before($softwareV, '.')) &lt; 5">
					<xsl:text>'</xsl:text>
				</xsl:if>
				<xsl:value-of select="$setSignalName"/>
				<xsl:if test="number(substring-before($softwareV, '.')) &lt; 5">
					<xsl:text>'</xsl:text>
				</xsl:if>
				<xsl:text>, 5:</xsl:text>
				<xsl:value-of select="$setSignalState"/>
				<xsl:text>, 6:CONTINUE, 8:</xsl:text>
				<xsl:value-of select="$signalDuration"/>
				<!-- Line 2 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>CONTINUE</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>PULSE($OUT[</xsl:text>
				<xsl:value-of select="$outputPortNumber"/>
				<xsl:text>], </xsl:text>
				<xsl:value-of select="$setSignalState"/>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$signalDuration"/>
				<xsl:text>)</xsl:text>
				<!-- Line 3 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DelayActivity'">
				<xsl:variable name="start" select="ActivityTime/StartTime"/>
				<xsl:variable name="end" select="ActivityTime/EndTime"/>
				<xsl:variable name="delay" select="format-number($end - $start, $delayTimePattern)"/>
				<!-- Line 1 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>;FOLD WAIT Time= </xsl:text>
				<xsl:value-of select="$delay"/>
				<xsl:text> sec;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPBASIS,%CWAIT,%VWAIT,%P 2:</xsl:text>
				<xsl:value-of select="$delay"/>
				<!-- Line 2 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>WAIT SEC </xsl:text>
				<xsl:value-of select="$delay"/>
				<!-- Line 3 -->
				<xsl:value-of select="$cr"/>
				<xsl:text>;ENDFOLD</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
				<xsl:value-of select="$cr"/>
				<xsl:variable name="callname" select="CallName"/>
				<xsl:value-of select="translate($callname, '.', '')"/>
				<xsl:text>()</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
				<xsl:variable name="moType" select="MotionAttributes/MotionType"/>
				<xsl:choose>
					<xsl:when test="$moType='CircularVia'">
						<!-- make sure next node is 'Circular', or give message(no output for CircularVia) -->
						<xsl:variable name="nextNode" select="following-sibling::node()[1]"/>
						<xsl:choose>
							<xsl:when test="local-name($nextNode)='Activity' and
													$nextNode/@ActivityType='DNBRobotMotionActivity' and
													$nextNode/MotionAttributes/MotionType='Circular'">
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
								<xsl:text>Error: CircularVia must be followed by Circular.</xsl:text>
								<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO END</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="prevNode" select="preceding-sibling::node()[1]"/>
						<!-- make sure prev node is 'CircularVia', or give message(continue output anyway) -->
						<xsl:if test="$moType='Circular'">
							<xsl:choose>
								<xsl:when test="local-name($prevNode)='Activity' and
														$prevNode/@ActivityType='DNBRobotMotionActivity' and
														$prevNode/MotionAttributes/MotionType='CircularVia'">
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$cr"/>
									<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
									<xsl:text>Error: Circular has no preceding CircularVia.</xsl:text>
									<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
									<xsl:text>ERROR INFO END</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>

						<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
						<xsl:variable name="tagPart" select="Target/CartesianTarget/Tag/@AttachedToPart"/>
						<xsl:variable name="tagName">
							<xsl:choose>
								<xsl:when test="Target/@Default = 'Joint'">HOME</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="Target/CartesianTarget/Tag"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="tagGroupVia" select="$prevNode/Target/CartesianTarget/Tag/@TagGroup"/>
						<xsl:variable name="tagPartVia" select="$prevNode/Target/CartesianTarget/Tag/@AttachedToPart"/>
						<xsl:variable name="tagNameVia" select="$prevNode/Target/CartesianTarget/Tag"/>

						<xsl:variable name="tagRootNameAttr" select="../AttributeList/Attribute[AttributeName='TagRootName']/AttributeValue"/>
						<xsl:variable name="motionProfile"   select="MotionAttributes/MotionProfile"/>
						<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
						<xsl:variable name="accuracyValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyValue/@Value"/>
						<xsl:variable name="accuracyType"  select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyType"/>
						<xsl:variable name="flyByMode"     select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/FlyByMode"/>
						<xsl:variable name="speedUnits"    select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Units"/>
						<xsl:variable name="speedValue"    select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Value"/>
						<xsl:variable name="spd">
							<xsl:call-template name="GetSpeed">
								<xsl:with-param name="moType" select="$moType"/>
								<xsl:with-param name="speedValue" select="$speedValue"/>
								<xsl:with-param name="speedUnits" select="$speedUnits"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="accuracy">
							<xsl:call-template name="GetAccuracy">
								<xsl:with-param name="flyByMode" select="$flyByMode"/>
								<xsl:with-param name="moType" select="$moType"/>
								<xsl:with-param name="accuracyType" select="$accuracyType"/>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="datName">
							<xsl:choose>
								<xsl:when test="$moType='Joint'">ppdat</xsl:when>
								<xsl:otherwise>lcpdat</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="pdatCombo" select="concat($motionProfile,';',$accuracyProfile)"/>
						<xsl:variable name="pdatIdx" select="KIRStrUtils:getDATIndex($datName, $pdatCombo)"/>
						<xsl:variable name="pdatNum">
							<xsl:choose>
								<xsl:when test="$pdatIdx='-1'">
									<xsl:value-of select="KIRStrUtils:addDAT($datName, $pdatCombo)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$pdatIdx"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<NumberIncrement:counternum counter="2"/>
						<xsl:variable name="tagNumVia">
							<xsl:choose>
								<xsl:when test="$moType='Circular'">
									<xsl:call-template name="getNumIncNext"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="tagNum">
							<xsl:choose>
								<xsl:when test="Target/@Default = 'Joint'">0</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="getNumIncNext"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="tagCombo">
							<xsl:choose>
								<xsl:when test="Target/@Default = 'Joint'">HOME</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="$NumBaseAuxAxes + $NumToolAuxAxes + $NumPositionerAxes &gt; 0">
											<!-- tag may be used with different aux axis values, use the increment number -->
											<xsl:choose>
												<xsl:when test="$tagRootNameAttr">
													<xsl:value-of select="$tagRootNameAttr"/>
												</xsl:when>
												<xsl:when test="$tagRootName">
													<xsl:value-of select="$tagRootName"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text>P</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
											<xsl:value-of select="$tagNum"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="$tagRootNameAttr">
													<xsl:value-of select="$tagRootNameAttr"/>
													<xsl:call-template name="getTagNum">
														<xsl:with-param name="activity" select="."/>
													</xsl:call-template>
												</xsl:when>
												<xsl:when test="$tagRootName">
													<xsl:value-of select="$tagRootName"/>
													<xsl:call-template name="getTagNum">
														<xsl:with-param name="activity" select="."/>
													</xsl:call-template>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="$tagName"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable><!-- tagCombo -->
						<xsl:variable name="tagComboVia">
							<xsl:choose>
								<xsl:when test="$NumBaseAuxAxes + $NumToolAuxAxes + $NumPositionerAxes &gt; 0">
									<!-- tag may be used with different aux axis values, use the increment number -->
									<xsl:choose>
										<xsl:when test="$tagRootNameAttr">
											<xsl:value-of select="$tagRootNameAttr"/>
										</xsl:when>
										<xsl:when test="$tagRootName">
											<xsl:value-of select="$tagRootName"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>P</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:value-of select="$tagNumVia"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="$tagRootNameAttr">
											<xsl:value-of select="$tagRootNameAttr"/>
											<xsl:call-template name="getTagNum">
												<xsl:with-param name="activity" select="$prevNode"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:when test="$tagRootName">
											<xsl:value-of select="$tagRootName"/>
											<xsl:call-template name="getTagNum">
												<xsl:with-param name="activity" select="$prevNode"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$tagNameVia"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable><!-- tagComboVia -->

						<xsl:variable name="pgTagName" select="concat($tagPart,';',$tagGroup,';',$tagCombo)"/>
						<xsl:variable name="tagNameUnique" select="KIRStrUtils:getTagName($pgTagName, 'SRC')"/>
						<xsl:variable name="pgTagNameVia" select="concat($tagPartVia,';',$tagGroupVia,';',$tagComboVia)"/>
						<xsl:variable name="tagNameViaUnique" select="KIRStrUtils:getTagName($pgTagNameVia, 'SRC')"/>

						<xsl:value-of select="$cr"/>
						<xsl:variable name="nextNode" select="following-sibling::node()[1]"/>
						<xsl:choose>
							<xsl:when test="local-name($nextNode) = 'Action' and
													($nextNode/@ActionType='KukaSpotRetract' or
													 $nextNode/@ActionType='KukaSpotWeld')">
								<xsl:call-template name="TechSpot">
									<xsl:with-param name="activity"   select="."/>
									<xsl:with-param name="tagName"    select="$tagNameUnique"/>
									<xsl:with-param name="tagNameVia" select="$tagNameViaUnique"/>
									<xsl:with-param name="spd"        select="$spd"/>
									<xsl:with-param name="pdatNum"    select="$pdatNum"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="local-name($nextNode) = 'Action' and
													($nextNode/@ActionType='KukaServoGunWeld' or
													 $nextNode/@ActionType='KukaServoGunTipDress')">
								<xsl:call-template name="TechServoGun">
									<xsl:with-param name="activity"   select="."/>
									<xsl:with-param name="tagName"    select="$tagNameUnique"/>
									<xsl:with-param name="tagNameVia" select="$tagNameViaUnique"/>
									<xsl:with-param name="spd"        select="$spd"/>
									<xsl:with-param name="pdatNum"    select="$pdatNum"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="TechBasis">
									<xsl:with-param name="activity"   select="."/>
									<xsl:with-param name="tagName"    select="$tagNameUnique"/>
									<xsl:with-param name="tagNameVia" select="$tagNameViaUnique"/>
									<xsl:with-param name="spd"        select="$spd"/>
									<xsl:with-param name="accuracy"   select="$accuracy"/>
									<xsl:with-param name="pdatNum"    select="$pdatNum"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise><!-- non-CircularVia DNBRobotMotionActivity -->
				</xsl:choose>

			</xsl:when>
		</xsl:choose>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
	</xsl:template>
	<!-- end of template Activity mode="SRC" -->

	<xsl:template name="TechBasis">
		<xsl:param name="activity"/>
		<xsl:param name="tagName"/>
		<xsl:param name="tagNameVia" select="''"/>
		<xsl:param name="spd"/>
		<xsl:param name="accuracy"/>
		<xsl:param name="pdatNum"/>

		<!-- activity var -->
		<xsl:variable name="moType" select="MotionAttributes/MotionType"/>

		<!-- Line 1 -->
		<xsl:text>;FOLD </xsl:text>
		<xsl:choose>
			<xsl:when test="$moType='Joint'">PTP </xsl:when>
			<xsl:when test="$moType='Linear'">LIN </xsl:when>
			<xsl:when test="$moType='Circular'">
				<xsl:text>CIRC </xsl:text>
				<xsl:value-of select="$tagNameVia"/>
				<xsl:text> </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:value-of select="$tagName"/>
		<xsl:if test="string-length($accuracy) > 0"> CONT</xsl:if>
		<xsl:text> Vel= </xsl:text><xsl:value-of select="$spd"/>
		<xsl:choose>
			<xsl:when test="$tagName='HOME'"> % DEFAULT;%{PE}</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$moType='Joint'"> % PDAT</xsl:when>
					<xsl:otherwise> m/s CPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<!-- Tool & Base Number & Name -->
				<xsl:call-template name="outputToolBase">
					<xsl:with-param name="activity" select="."/>
				</xsl:call-template>

				<xsl:text>;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/><xsl:text>,</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>%MKUKATPBASIS,%CMOVE,%V</xsl:text>
		<xsl:choose>
			<xsl:when test="$moType='Joint'">PTP,%P 1:PTP, 2:</xsl:when>
			<xsl:when test="$moType='Linear'">LIN,%P 1:LIN, 2:</xsl:when>
			<xsl:when test="$moType='Circular'">
				<xsl:text>CIRC,%P 1:CIRC, 2:</xsl:text>
				<xsl:value-of select="$tagNameVia"/>
				<xsl:text>, 3:</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:value-of select="$tagName"/>

		<!-- counter number 0: fold fields -->
		<NumberIncrement:counternum counter="0"/>
		<xsl:choose>
			<xsl:when test="$moType='Circular'">
				<NumberIncrement:startnum start="2"/>
			</xsl:when>
			<xsl:otherwise>
				<NumberIncrement:startnum start="1"/>
			</xsl:otherwise>
		</xsl:choose>
		<NumberIncrement:reset/>
		<NumberIncrement:increment inc="2"/>
		<xsl:call-template name="foldField"/><!-- 3/4 -->
		<xsl:value-of select="$accuracy"/>
		<xsl:call-template name="foldField"/><!-- 5/6 -->
		<xsl:value-of select="$spd"/>
		<xsl:call-template name="foldField"/><!-- 7/8 -->
		<xsl:choose>
			<xsl:when test="$tagName='HOME'">DEFAULT</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT</xsl:when>
					<xsl:otherwise>CPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
		<!-- Line 2 -->
		<xsl:text>$BWDSTART=FALSE</xsl:text><xsl:value-of select="$cr"/>
		<xsl:if test="$tagName='HOME'">
			<xsl:text>$H_POS=XHOME</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<!-- Line 3 -->
		<xsl:choose>
			<xsl:when test="$tagName='HOME'">PDAT_ACT=PDEFAULT</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT_ACT=PPDAT</xsl:when>
					<xsl:otherwise>LDAT_ACT=LCPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
		<!-- Line 4 -->
		<xsl:if test="substring-before($softwareV, '.')='4' or $tagName='HOME'">
			<xsl:choose>
				<xsl:when test="$moType='Joint'">BAS(#PTP_DAT)</xsl:when>
				<xsl:otherwise>BAS(#CP_DAT)</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<!-- Line 5 -->
		<xsl:text>FDAT_ACT=F</xsl:text>
		<xsl:value-of select="$tagName"/><xsl:value-of select="$cr"/>
		<!-- Line 6 -->
		<xsl:if test="substring-before($softwareV, '.')='4' or $tagName='HOME'">
			<xsl:text>BAS(#FRAMES)</xsl:text><xsl:value-of select="$cr"/>
		</xsl:if>
		<!-- Line 7 -->
		<xsl:choose>
			<xsl:when test="number(substring-before($softwareV, '.')) >= 5 and $tagName!='HOME'">
				<xsl:choose>
					<xsl:when test="$moType='Joint'">BAS(#PTP_PARAMS,</xsl:when>
					<xsl:otherwise>BAS(#CP_PARAMS,</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">BAS(#VEL_PTP,</xsl:when>
					<xsl:otherwise>BAS(#VEL_CP,</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$spd"/>)<xsl:value-of select="$cr"/>
		<!-- Line 8 -->
		<xsl:choose>
			<xsl:when test="$moType='Joint'">PTP X</xsl:when>
			<xsl:when test="$moType='Linear'">LIN X</xsl:when>
			<xsl:when test="$moType='Circular'">
				<xsl:text>CIRC X</xsl:text><xsl:value-of select="$tagNameVia"/>
				<xsl:text>, X</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:value-of select="$tagName"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$accuracy"/><xsl:value-of select="$cr"/>
		<xsl:text>;ENDFOLD</xsl:text>
	</xsl:template>
	<!-- end of template TechBasis -->

	<xsl:template name="TechSpot">
		<xsl:param name="activity"/>
		<xsl:param name="tagName"/>
		<xsl:param name="tagNameVia" select="''"/>
		<xsl:param name="spd"/>
		<xsl:param name="pdatNum"/>

		<!-- activity var -->
		<xsl:variable name="moType" select="MotionAttributes/MotionType"/>

		<!-- action var -->
		<xsl:variable name="action" select="following-sibling::node()[1]"/>
		<xsl:variable name="gunNumSRC" select="$action/AttributeList/Attribute[AttributeName='Gun']/AttributeValue"/>
		<xsl:variable name="retractSRC" select="$action/AttributeList/Attribute[AttributeName='Retract']/AttributeValue"/>
		<xsl:variable name="retractName">
			<xsl:call-template name="outputRetract">
				<xsl:with-param name="retractState" select="$retractSRC"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:text>;FOLD </xsl:text>
		<xsl:choose>
		   <xsl:when test="$moType='Joint'">PTP </xsl:when>
		   <xsl:when test="$moType='Linear'">LIN </xsl:when>
		   <xsl:when test="$moType='Circular'">
		      <xsl:text>CIRC </xsl:text>
		      <xsl:value-of select="$tagNameVia"/>
				<xsl:text> </xsl:text>
		   </xsl:when>
		</xsl:choose>
		<xsl:value-of select="$tagName"/>
		<xsl:choose>
		   <xsl:when test="$moType='Joint'"> PDAT</xsl:when>
		   <xsl:otherwise> CPDAT</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$pdatNum"/>

		<xsl:choose>
			<xsl:when test="$action/@ActionType='KukaSpotRetract'">
            <!-- SpotRetract Line 1 Continue -->
				<xsl:text> RETR </xsl:text>
				<xsl:value-of select="$retractName"/>
				<xsl:text> Gun= </xsl:text>
				<xsl:value-of select="$gunNumSRC"/>
				<!-- Tool & Base Numbers & Names -->
				<xsl:call-template name="outputToolBase">
					<xsl:with-param name="activity" select="."/>
				</xsl:call-template>
				<xsl:text>;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPSPOT,%CRETR,%V</xsl:text>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PTP,%P 1:PTP, 2:</xsl:when>
					<xsl:when test="$moType='Linear'">LIN,%P 1:LIN, 2:</xsl:when>
					<xsl:when test="$moType='Circular'">
						<xsl:text>CIRC,%P 1:CIRC, 2:</xsl:text>
						<xsl:value-of select="$tagNameVia"/>
						<xsl:text>, 3:</xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$tagName"/>

				<!-- counter number 0: fold fields -->
				<NumberIncrement:counternum counter="0"/>
				<xsl:choose>
					<xsl:when test="$moType='Circular'">
						<NumberIncrement:startnum start="2"/>
					</xsl:when>
					<xsl:otherwise>
						<NumberIncrement:startnum start="1"/>
					</xsl:otherwise>
				</xsl:choose>
				<NumberIncrement:reset/>
				<NumberIncrement:increment inc="2"/>
				<xsl:call-template name="foldField"/><!-- 3/4 -->
				<xsl:call-template name="foldField"/><!-- 5/6 -->
				<xsl:value-of select="$spd"/>
				<xsl:call-template name="foldField"/><!-- 7/8 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT</xsl:when>
					<xsl:otherwise>CPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<xsl:call-template name="foldField"/><!-- 9/10 -->
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$retractName"/>
				<xsl:call-template name="foldField"/><!-- 11/12 -->
				<xsl:value-of select="$gunNumSRC"/>
				<xsl:call-template name="foldField"/><!-- 13/14 -->
				<xsl:text>#FIRST</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>$BWDSTART=FALSE</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 2 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT_ACT=PPDAT</xsl:when>
					<xsl:otherwise>LDAT_ACT=LCPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/><xsl:value-of select="$cr"/>
				<!-- Line 3 -->
				<xsl:if test="substring-before($softwareV, '.')='4'">
					<xsl:choose>
						<xsl:when test="$moType='Joint'">BAS(#PTP_DAT)</xsl:when>
						<xsl:otherwise>BAS(#CP_DAT)</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<!-- Line 4 -->
				<xsl:text>FDAT_ACT=F</xsl:text>
				<xsl:value-of select="$tagName"/><xsl:value-of select="$cr"/>
				<!-- Line 5 -->
				<xsl:if test="substring-before($softwareV, '.')='4'">
					<xsl:text>BAS(#FRAMES)</xsl:text><xsl:value-of select="$cr"/>
				</xsl:if>
				<!-- Line 6 -->
				<xsl:choose>
					<xsl:when test="number(substring-before($softwareV, '.')) >= 5">
						<xsl:choose>
							<xsl:when test="$moType='Joint'">BAS(#PTP_PARAMS,PPDAT</xsl:when>
							<xsl:otherwise>BAS(#CP_PARAMS,LCPDAT</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$moType='Joint'">BAS(#VEL_PTP,PPDAT</xsl:when>
							<xsl:otherwise>BAS(#VEL_CP,LCPDAT</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<xsl:text>.VEL)</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 7 -->
				<xsl:text>S_ACT.GUN=</xsl:text><xsl:value-of select="$gunNumSRC"/><xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.PAIR=#FIRST</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.RETR=#</xsl:text><xsl:value-of select="$retractName"/><xsl:value-of select="$cr"/>
				<xsl:text>S_READY=FALSE</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>TRIGGER WHEN DISTANCE=1 DELAY=0.0 DO USERSPOT(#RETR, S_ACT) PRIO=-1</xsl:text><xsl:value-of select="$cr"/>
			</xsl:when>
			<xsl:when test="$action/@ActionType='KukaSpotWeld'">
            <!-- SpotWeld Line 1 Continue -->
            <xsl:text> SPOT </xsl:text>
            <xsl:text> Gun= </xsl:text>
            <xsl:value-of select="$gunNumSRC"/>
            <xsl:text> RETR </xsl:text>
            <xsl:value-of select="$retractName"/>
            <xsl:text> SDAT</xsl:text>
				<NumberIncrement:counternum counter="3"/>
				<xsl:variable name="sdatNum">
					<xsl:call-template name="getNumIncNext"/>
				</xsl:variable>
				<xsl:value-of select="$sdatNum"/>
				<!-- Tool & Base Numbers & Names -->
				<xsl:call-template name="outputToolBase">
					<xsl:with-param name="activity" select="."/>
				</xsl:call-template>
				<xsl:text>;%{PE}%R </xsl:text>
				<xsl:value-of select="$softwareV"/>
				<xsl:text>,%MKUKATPSPOT,%CSPOT,%V</xsl:text>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PTP,%P 1:PTP, 2:</xsl:when>
					<xsl:when test="$moType='Linear'">LIN,%P 1:LIN, 2:</xsl:when>
					<xsl:when test="$moType='Circular'">
						<xsl:text>CIRC,%P 1:CIRC, 2:</xsl:text>
						<xsl:value-of select="$tagNameVia"/>
						<xsl:text>, 3:</xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$tagName"/>

				<!-- counter number 0: fold fields -->
				<NumberIncrement:counternum counter="0"/>
				<xsl:choose>
					<xsl:when test="$moType='Circular'">
						<NumberIncrement:startnum start="2"/>
					</xsl:when>
					<xsl:otherwise>
						<NumberIncrement:startnum start="1"/>
					</xsl:otherwise>
				</xsl:choose>
				<NumberIncrement:reset/>
				<NumberIncrement:increment inc="2"/>
				<xsl:call-template name="foldField"/><!-- 3/4 -->
				<xsl:call-template name="foldField"/><!-- 5/6 -->
				<xsl:value-of select="$spd"/>
				<xsl:call-template name="foldField"/><!-- 7/8 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT</xsl:when>
					<xsl:otherwise>CPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<!-- skip 1 field -->
				<NumberIncrement:increment inc="1"/>
				<xsl:variable name="junkVar">
					<xsl:call-template name="getNumIncNext"/>
				</xsl:variable>
				<NumberIncrement:increment inc="2"/>
				<xsl:call-template name="foldField"/><!-- 10/11 -->
						<xsl:value-of select="$gunNumSRC"/>
				<xsl:call-template name="foldField"/><!-- 12/13 -->
						<xsl:text>#FIRST</xsl:text>
				<xsl:call-template name="foldField"/><!-- 14/15 -->
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$retractName"/>
				<xsl:choose>
					<xsl:when test="$moType='Joint' or $moType='Linear'">
						<xsl:text>, 16:1, 18:1, 20:0, 22:0, 24:0, 25:SDAT</xsl:text>
					</xsl:when>
					<xsl:when test="$moType='Circular'">
						<xsl:text>, 17:1, 19:1, 21:0, 23:0, 25:0, 26:SDAT</xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$sdatNum"/><xsl:value-of select="$cr"/>
				<xsl:text>$BWDSTART=FALSE</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 2 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT_ACT=PPDAT</xsl:when>
					<xsl:otherwise>LDAT_ACT=LCPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/><xsl:value-of select="$cr"/>
				<!-- Line 3 -->
				<xsl:if test="substring-before($softwareV, '.')='4'">
				<xsl:choose>
					<xsl:when test="$moType='Joint'">BAS(#PTP_DAT)</xsl:when>
					<xsl:otherwise>BAS(#CP_DAT)</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
				</xsl:if>
				<!-- Line 4 -->
				<xsl:text>FDAT_ACT=F</xsl:text>
				<xsl:value-of select="$tagName"/><xsl:value-of select="$cr"/>
				<!-- Line 5 -->
				<xsl:if test="substring-before($softwareV, '.')='4'">
					<xsl:text>BAS(#FRAMES)</xsl:text><xsl:value-of select="$cr"/>
				</xsl:if>
				<!-- Line 6 -->
				<xsl:choose>
					<xsl:when test="number(substring-before($softwareV, '.')) >= 5">
						<xsl:choose>
							<xsl:when test="$moType='Joint'">BAS(#PTP_PARAMS,PPDAT</xsl:when>
							<xsl:otherwise>BAS(#CP_PARAMS,LCPDAT</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$moType='Joint'">BAS(#VEL_PTP,PPDAT</xsl:when>
							<xsl:otherwise>BAS(#VEL_CP,LCPDAT</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<xsl:text>.VEL)</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 7 -->
				<xsl:text>S_ACT.GUN=</xsl:text><xsl:value-of select="$gunNumSRC"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.PAIR=SDEFAULT.PAIR</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.RETR=#</xsl:text>
				<xsl:value-of select="$retractName"/><xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.CLO_TM=SSDAT</xsl:text><xsl:value-of select="$sdatNum"/>
				<xsl:text>.CLO_TM</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.PGNO1=SSDAT</xsl:text><xsl:value-of select="$sdatNum"/>
				<xsl:text>.PGNO1</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.PGNO2=SDEFAULT.PGNO2</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.PRESS1=SSDAT</xsl:text><xsl:value-of select="$sdatNum"/>
				<xsl:text>.PRESS1</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>S_ACT.PRESS2=SDEFAULT.PRESS2</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>S_READY=FALSE</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>USERSPOT(#ADVSPOT, S_ACT)</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>TRIGGER WHEN DISTANCE=1 DELAY=SSDAT</xsl:text><xsl:value-of select="$sdatNum"/>
				<xsl:text>.CLO_TM DO USERSPOT(#PRESPOT, S_ACT) PRIO=-1</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>TRIGGER WHEN DISTANCE=1 DELAY=0.0 DO USERSPOT(#SPOT, S_ACT) PRIO=-1</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:when>
		</xsl:choose>

		<!-- common lines -->
		<xsl:choose>
			<xsl:when test="$moType='Joint'">PTP X</xsl:when>
			<xsl:when test="$moType='Linear'">LIN X</xsl:when>
			<xsl:when test="$moType='Circular'">
				<xsl:text>CIRC X</xsl:text>
				<xsl:value-of select="$tagNameVia"/>
				<xsl:text>, X</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:value-of select="$tagName"/><xsl:value-of select="$cr"/>
		<xsl:text>WAIT FOR S_READY</xsl:text><xsl:value-of select="$cr"/>
		<xsl:text>;ENDFOLD</xsl:text>
	</xsl:template>
	<!-- end of template TechSpot -->

	<xsl:template name="TechServoGun">
		<xsl:param name="activity"/>
		<xsl:param name="tagName"/>
		<xsl:param name="tagNameVia" select="''"/>
		<xsl:param name="spd"/>
		<xsl:param name="pdatNum"/>

		<!-- activity var -->
		<xsl:variable name="moType" select="MotionAttributes/MotionType"/>

		<!-- action var -->
		<xsl:variable name="action" select="following-sibling::node()[1]"/>
		<xsl:variable name="gunNumber" select="$action/AttributeList/Attribute[AttributeName='GunNumber']/AttributeValue"/>
		<xsl:variable name="part" select="$action/AttributeList/Attribute[AttributeName='Part']/AttributeValue"/>
		<xsl:variable name="force" select="$action/AttributeList/Attribute[AttributeName='Force']/AttributeValue"/>
		<xsl:variable name="comp" select="$action/AttributeList/Attribute[AttributeName='Comp']/AttributeValue"/>
		<xsl:variable name="trigger" select="$action/AttributeList/Attribute[AttributeName='Trigger']/AttributeValue"/>

		<xsl:choose>
			<xsl:when test="$action/@ActionType='KukaServoGunWeld'">
				<xsl:variable name="cont" select="$action/AttributeList/Attribute[AttributeName='Cont']/AttributeValue"/>
				<!-- Line 1 -->
				<xsl:text>;FOLD </xsl:text>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PTP </xsl:when>
					<xsl:when test="$moType='Linear'">LIN </xsl:when>
					<xsl:when test="$moType='Circular'">
						<xsl:text>CIRC </xsl:text>
						<xsl:value-of select="$tagNameVia"/>
						<xsl:text> </xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$tagName"/>
				<xsl:text> Vel= </xsl:text><xsl:value-of select="$spd"/>
				<xsl:choose>
					<xsl:when test="$moType='Joint'"> % PDAT</xsl:when>
					<xsl:otherwise> m/s CPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<xsl:text> ServoGun= </xsl:text><xsl:value-of select="$gunNumber"/>
				<xsl:text> Cont= </xsl:text><xsl:value-of select="$cont"/>
				<xsl:text> Part= </xsl:text><xsl:value-of select="$part"/><xsl:text> mm</xsl:text>
				<xsl:text> Force= </xsl:text><xsl:value-of select="$force"/><xsl:text> KN</xsl:text>
				<xsl:text> Comp.= </xsl:text><xsl:value-of select="$comp"/>
				<xsl:text> Trigger= </xsl:text><xsl:value-of select="$trigger"/><xsl:text> mm</xsl:text>
				<!-- Tool & Base Numbers & Names -->
				<xsl:call-template name="outputToolBase">
					<xsl:with-param name="activity" select="."/>
				</xsl:call-template>
				<xsl:text>;%{PE}%R 1.0.16,%MKUKATPSERVOTECH,%CSpotP,%V</xsl:text>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PTP,%P 1:PTP, 2:</xsl:when>
					<xsl:when test="$moType='Linear'">LIN,%P 1:LIN, 2:</xsl:when>
					<xsl:when test="$moType='Circular'">
						<xsl:text>CIRC,%P 1:CIRC, 2:</xsl:text>
						<xsl:value-of select="$tagNameVia"/>
						<xsl:text>, 3:</xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$tagName"/>

				<!-- counter number 0: fold fields -->
				<NumberIncrement:counternum counter="0"/>
				<xsl:choose>
					<xsl:when test="$moType='Circular'">
						<NumberIncrement:startnum start="3"/>
					</xsl:when>
					<xsl:otherwise>
						<NumberIncrement:startnum start="2"/>
					</xsl:otherwise>
				</xsl:choose>
				<NumberIncrement:reset/>
				<NumberIncrement:increment inc="2"/>
				<xsl:call-template name="foldField"/><!-- 4/5 -->
				<xsl:value-of select="$spd"/>
				<xsl:call-template name="foldField"/><!-- 6/7 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT</xsl:when>
					<xsl:otherwise>CPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<xsl:call-template name="foldField"/><!-- 8/9 -->
				<xsl:value-of select="$gunNumber"/>
				<xsl:call-template name="foldField"/><!-- 10/11 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">
						<xsl:if test="$cont='CLS OPN'">12C_PTP</xsl:if>
						<xsl:if test="$cont='CLS'">1C_PTP</xsl:if>
						<xsl:if test="$cont='OPN'">2C_PTP</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$cont='CLS OPN'">C_DIS C_PTP</xsl:if>
						<xsl:if test="$cont='CLS'">C_DIS</xsl:if>
						<xsl:if test="$cont='OPN'">C_PTP</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:call-template name="foldField"/><!-- 12/13 -->
				<xsl:value-of select="$part"/>
				<xsl:call-template name="foldField"/><!-- 14/15 -->
				<xsl:value-of select="$force"/>
				<!-- skip 1 field -->
				<NumberIncrement:increment inc="1"/>
				<xsl:variable name="junkVar">
					<xsl:call-template name="getNumIncNext"/>
				</xsl:variable>
				<NumberIncrement:increment inc="2"/>
				<xsl:call-template name="foldField"/><!-- 17/18 -->
				<xsl:value-of select="$comp"/>
				<xsl:call-template name="foldField"/><!-- 19/20 -->
				<xsl:value-of select="$trigger"/><xsl:value-of select="$cr"/>
				<!-- Line 2 -->
				<xsl:text>$BWDSTART=FALSE</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 3 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT_ACT=PPDAT</xsl:when>
					<xsl:otherwise>LDAT_ACT=LCPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/><xsl:value-of select="$cr"/>
				<!-- Line 4 -->
				<xsl:text>FDAT_ACT=F</xsl:text><xsl:value-of select="$tagName"/><xsl:value-of select="$cr"/>
				<!-- Line 5 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">BAS(#PTP_PARAMS,</xsl:when>
					<xsl:otherwise>BAS(#CP_PARAMS,</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$spd"/>)<xsl:value-of select="$cr"/>
				<!-- Line 6 -->
				<xsl:text>EG_EXTAX_ACTIVE=EG_SERVOGUN_EXAXIS[</xsl:text>
				<xsl:value-of select="$gunNumber"/>]<xsl:value-of select="$cr"/>
				<!-- Line 7 -->
				<xsl:text>EG_THICKNESS=</xsl:text><xsl:value-of select="$part"/><xsl:value-of select="$cr"/>
				<!-- Line 8 -->
				<xsl:text>EG_GUN_FORCE=</xsl:text><xsl:value-of select="$force"/><xsl:value-of select="$cr"/>
				<!-- Line 9 -->
				<xsl:text>EG_COMP_HELP=</xsl:text><xsl:value-of select="$comp"/><xsl:value-of select="$cr"/>
				<!-- Line 10 -->
				<xsl:text>EG_COMPENSATE_PATH=</xsl:text><xsl:value-of select="$trigger"/><xsl:value-of select="$cr"/>
				<!-- Line 11 -->
				<xsl:if test="$moType='Circular'">
					<xsl:text>EG_HPPOINT=EG_CHTIP(</xsl:text>
					<xsl:value-of select="$tagNameVia"/>)<xsl:value-of select="$cr"/>
				</xsl:if>
				<xsl:text>EG_HELPPOINT=EG_CHTIP(</xsl:text>
				<xsl:value-of select="$tagName"/>)<xsl:value-of select="$cr"/>
				<!-- Line 12 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">
						<xsl:text>PTP EG_HELPPOINT </xsl:text>
						<xsl:if test="$cont='CLS OPN' or $cont='CLS'">C_PTP</xsl:if>
					</xsl:when>
					<xsl:when test="$moType='Linear'">
						<xsl:text>LIN EG_HELPPOINT </xsl:text>
						<xsl:if test="$cont='CLS OPN' or $cont='CLS'">C_DIS</xsl:if>
					</xsl:when>
					<xsl:when test="$moType='Circular'">
						<xsl:text>CIRC EG_HPPOINT EG_HELPPOINT </xsl:text>
						<xsl:if test="$cont='CLS OPN' or $cont='CLS'">C_DIS</xsl:if>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
				<!-- Line 13 -->
				<xsl:text>EG_APPROX_OPEN=</xsl:text>
				<xsl:choose>
					<xsl:when test="$cont='CLS OPN' or $cont='OPN'">TRUE</xsl:when>
					<xsl:otherwise>FALSE</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
				<!-- Line 14 -->
				<xsl:text>EG_PROGRAM_NUMBER=</xsl:text>
				<xsl:call-template name="stripStr4num">
					<xsl:with-param name="str" select="$tagName"/>
				</xsl:call-template>
				<xsl:value-of select="$cr"/>
				<!-- Line 15 -->
				<xsl:text>CMD=#SPOT</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 16 -->
				<xsl:text>EG_SPOT_POINT=EG_HELPPOINT</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 17 -->
				<xsl:text>EG_SPOT( )</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 18 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">
						<xsl:text>PTP EG_HELPPOINT </xsl:text>
						<xsl:if test="$cont='CLS OPN' or $cont='OPN'">C_PTP</xsl:if>
					</xsl:when>
					<xsl:otherwise>EG_Check_Coop( )</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$action/@ActionType='KukaServoGunTipDress'">
				<!-- Line 1 -->
				<xsl:text>;FOLD Tippdress </xsl:text>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PTP </xsl:when>
					<xsl:when test="$moType='Linear'">LIN </xsl:when>
				</xsl:choose>
				<xsl:value-of select="$tagName"/>
				<xsl:text> Vel= </xsl:text><xsl:value-of select="$spd"/>
				<xsl:choose>
					<xsl:when test="$moType='Joint'"> % PDAT</xsl:when>
					<xsl:when test="$moType='Linear'"> m/s CPDAT</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<xsl:text> ServoGun= </xsl:text><xsl:value-of select="$gunNumber"/>
				<xsl:text> Part= </xsl:text><xsl:value-of select="$part"/><xsl:text> mm</xsl:text>
				<xsl:text> Force= </xsl:text><xsl:value-of select="$force"/><xsl:text> KN</xsl:text>
				<xsl:text> Comp.= </xsl:text><xsl:value-of select="$comp"/>
				<xsl:text> Trigger= </xsl:text><xsl:value-of select="$trigger"/><xsl:text> mm</xsl:text>
				<!-- Tool & Base Numbers & Names -->
				<xsl:call-template name="outputToolBase">
					<xsl:with-param name="activity" select="."/>
				</xsl:call-template>
				<xsl:text>;%{PE}%R 1.0.16,%MKUKATPSERVOTECH,%CTippDress,%VNormalSG</xsl:text>
				<xsl:if test="$moType='Linear'">LIN</xsl:if>
				<xsl:text>,%P 2:NormalSG</xsl:text>
				<xsl:if test="$moType='Linear'">LIN</xsl:if>
				<xsl:text>, 3:</xsl:text>
				<xsl:value-of select="$tagName"/>
				<xsl:text>, 5:</xsl:text>
				<xsl:value-of select="$spd"/>
				<xsl:text>, 7:</xsl:text>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT</xsl:when>
					<xsl:when test="$moType='Linear'">CPDAT</xsl:when>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/>
				<xsl:text>, 9:</xsl:text>
				<xsl:value-of select="$gunNumber"/>
				<xsl:text>, 12:</xsl:text>
				<xsl:value-of select="$part"/>
				<xsl:text>, 14:</xsl:text>
				<xsl:value-of select="$force"/>
				<xsl:text>, 17:</xsl:text>
				<xsl:value-of select="$comp"/>
				<xsl:text>, 19:</xsl:text>
				<xsl:value-of select="$trigger"/><xsl:value-of select="$cr"/>
				<!-- Line 2 -->
				<xsl:text>$BWDSTART=FALSE</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 3 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PDAT_ACT=PPDAT</xsl:when>
					<xsl:otherwise>LDAT_ACT=LCPDAT</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$pdatNum"/><xsl:value-of select="$cr"/>
				<!-- Line 4 -->
				<xsl:text>FDAT_ACT=F</xsl:text><xsl:value-of select="$tagName"/><xsl:value-of select="$cr"/>
				<!-- Line 5 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">BAS(#PTP_PARAMS,</xsl:when>
					<xsl:otherwise>BAS(#CP_PARAMS,</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$spd"/>)<xsl:value-of select="$cr"/>
				<!-- Line 6 -->
				<xsl:text>EG_EXTAX_ACTIVE=EG_SERVOGUN_EXAXIS[</xsl:text>
				<xsl:value-of select="$gunNumber"/>]<xsl:value-of select="$cr"/>
				<!-- Line 7 -->
				<xsl:text>EG_THICKNESS=</xsl:text><xsl:value-of select="$part"/><xsl:value-of select="$cr"/>
				<!-- Line 8 -->
				<xsl:text>EG_GUN_FORCE=</xsl:text><xsl:value-of select="$force"/><xsl:value-of select="$cr"/>
				<!-- Line 9 -->
				<xsl:text>EG_COMP_HELP=</xsl:text><xsl:value-of select="$comp"/><xsl:value-of select="$cr"/>
				<!-- Line 10 -->
				<xsl:text>EG_COMPENSATE_PATH=</xsl:text><xsl:value-of select="$trigger"/><xsl:value-of select="$cr"/>
				<!-- Line 11 -->
				<xsl:text>EG_HELPPOINT=EG_CHTIP(</xsl:text>
				<xsl:value-of select="$tagName"/>)<xsl:value-of select="$cr"/>
				<!-- Line 12 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PTP</xsl:when>
					<xsl:when test="$moType='Linear'">LIN</xsl:when>
				</xsl:choose>
				<xsl:text> EG_HELPPOINT</xsl:text>	<xsl:value-of select="$cr"/>
				<!-- Line 13 not for TipDress -->
				<!-- Line 14 -->
				<xsl:text>EG_PROGRAM_NUMBER=</xsl:text>
				<xsl:call-template name="stripStr4num">
					<xsl:with-param name="str" select="$tagName"/>
				</xsl:call-template>
				<xsl:value-of select="$cr"/>
				<!-- Line 15 -->
				<xsl:text>CMD=#TIP_DRESSING</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 16 -->
				<xsl:text>EG_SPOT_POINT=EG_HELPPOINT</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 17 -->
				<xsl:text>EG_SPOT( )</xsl:text><xsl:value-of select="$cr"/>
				<!-- Line 18 -->
				<xsl:choose>
					<xsl:when test="$moType='Joint'">PTP</xsl:when>
					<xsl:when test="$moType='Linear'">LIN</xsl:when>
				</xsl:choose>
				<xsl:text> EG_HELPPOINT</xsl:text>
			</xsl:when>
		</xsl:choose>

		<xsl:value-of select="$cr"/>
		<!-- Line 19 -->
		<xsl:text>CMD=#STOP_POINTEND</xsl:text><xsl:value-of select="$cr"/>
		<!-- Line 20(16) -->
		<xsl:text>EG_SPOT_POINT=EG_HELPPOINT</xsl:text><xsl:value-of select="$cr"/>
		<!-- Line 21(17) -->
		<xsl:text>EG_SPOT( )</xsl:text><xsl:value-of select="$cr"/>
		<!-- Line 22 -->
		<xsl:text>;ENDFOLD</xsl:text>
	</xsl:template>
	<!-- end of template TechServoGun -->

	<xsl:template name="foldField">
		<xsl:text>, </xsl:text>
		<xsl:value-of select="NumberIncrement:next()"/>
		<xsl:text>:</xsl:text>
	</xsl:template>

	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNodeSet"/>

		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<xsl:if test="($prefix = 'Pre'  and starts-with(AttributeName, 'PreComment')) or
			              ($prefix = 'Post' and starts-with(AttributeName, 'PostComment'))">
				<xsl:choose>
					<xsl:when test="starts-with(AttributeValue, 'DAT:')">
					</xsl:when>
					<xsl:when test="starts-with(AttributeValue, 'InLineComment:')">
						<xsl:value-of select="substring-after(AttributeValue, ':')"/>
					</xsl:when>
					<xsl:when test="starts-with(AttributeValue, 'Robot Language:')">
						<xsl:value-of select="$cr"/>
						<xsl:value-of select="substring-after(AttributeValue, ':')"/>
					</xsl:when>
					<xsl:when test="starts-with(AttributeValue, 'StampComment:')">
						<xsl:variable name="c246" select="substring-after(AttributeValue, ':')"/>
						<xsl:variable name="c46"  select="substring-after($c246, ',')"/>
						<xsl:variable name="c2"   select="substring-before($c246, ',')"/>
						<xsl:variable name="c4"   select="substring-before($c46, ',')"/>
						<xsl:variable name="c6"   select="substring-after($c46, ',')"/>

						<xsl:value-of select="$cr"/>
						<xsl:text>;FOLD ; </xsl:text>
						<xsl:value-of select="$c2"/><xsl:text>  NAME: </xsl:text>
						<xsl:value-of select="$c4"/><xsl:text> CHANGES: </xsl:text>
						<xsl:value-of select="$c6"/>
						<xsl:text>;%{PE}%R </xsl:text>
						<xsl:value-of select="$softwareV"/>
						<xsl:text>,%MKUKATPBASIS,%CCOMMENT,%VSTAMP,%P 1:;, 2:</xsl:text>
						<xsl:value-of select="$c2"/><xsl:text>, 3: NAME:, 4:</xsl:text>
						<xsl:value-of select="$c4"/><xsl:text>, 5:CHANGES:, 6:</xsl:text>
						<xsl:value-of select="$c6"/>
						<xsl:value-of select="$cr"/>
						<xsl:text>;ENDFOLD</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>;FOLD ; </xsl:text>
						<xsl:value-of select="AttributeValue"/>
						<xsl:text>;%{PE}%R </xsl:text>
						<xsl:value-of select="$softwareV"/>
						<xsl:text>,%MKUKATPBASIS,%CCOMMENT,%VNORMAL,%P 2:</xsl:text>
						<xsl:value-of select="AttributeValue"/>
						<xsl:value-of select="$cr"/>
						<xsl:text>;ENDFOLD</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end of template processComments -->

	<xsl:template name="processCommentsDAT">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNodeSet"/>

		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<xsl:if test="($prefix = 'Pre'  and starts-with(AttributeName, 'PreComment')) or
			              ($prefix = 'Post' and starts-with(AttributeName, 'PostComment'))">
				<xsl:choose>
					<xsl:when test="starts-with(AttributeValue, 'DAT:Robot Language:')">
						<xsl:value-of select="$cr"/>
						<xsl:value-of select="substring(AttributeValue, 20)"/>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end of template processCommentsDAT -->

	<xsl:template name="toolDataOutput">
		<xsl:for-each select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile">
			<xsl:variable name="toolIndex">
				<xsl:apply-templates select="." mode="NUM"/>
			</xsl:variable>
			
			<xsl:if test="$toolIndex &gt; 0">
				<xsl:value-of select="$cr"/>
				<xsl:text>TOOL_DATA[</xsl:text>
				<xsl:value-of select="$toolIndex"/>
				<xsl:text>]=</xsl:text>
				
				<xsl:variable name="numBases" select="count(/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile)"/>
				<xsl:variable name="baseIndex">
					<xsl:choose>
						<xsl:when test="$toolIndex &lt; $numBases">
							<xsl:value-of select="$toolIndex"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="objProfileList" select="/OLPData/Resource/Controller/ObjectFrameProfileList"/>
				<xsl:variable name="objProfileOfToolIndex" select="$objProfileList/ObjectFrameProfile[$baseIndex+1]"/>
				<xsl:choose>
					<xsl:when test="ToolType = 'Stationary'">
						<!-- look for an obj profile named "*_Tool_num_FixedTCP" -->
						<!-- if not exist, output obj profile with the same sequence number as the tool -->
						<xsl:variable name="baseSuffix" select="concat('_Tool_', $toolIndex+1, '_FixedTCP')"/>
						<xsl:variable name="obj" select="$objProfileList/ObjectFrameProfile[contains(Name, $baseSuffix)]"/>
						<xsl:choose>
							<xsl:when test="$obj">
								<xsl:variable name="x" select="$obj/ObjectFrame/ObjectFramePosition/@X * $targetUnits"/>
								<xsl:variable name="y" select="$obj/ObjectFrame/ObjectFramePosition/@Y * $targetUnits"/>
								<xsl:variable name="z" select="$obj/ObjectFrame/ObjectFramePosition/@Z * $targetUnits"/>
								<xsl:variable name="w" select="$obj/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
								<xsl:variable name="p" select="$obj/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
								<xsl:variable name="r" select="$obj/ObjectFrame/ObjectFrameOrientation/@Roll"/>
								<xsl:text>{x </xsl:text>
								<xsl:value-of select="format-number($x, $toolPattern)"/>
								<xsl:text>,y </xsl:text>
								<xsl:value-of select="format-number($y, $toolPattern)"/>
								<xsl:text>,z </xsl:text>
								<xsl:value-of select="format-number($z, $toolPattern)"/>
								<xsl:text>,a </xsl:text>
								<xsl:value-of select="format-number($r, $toolPattern)"/>
								<xsl:text>,b </xsl:text>
								<xsl:value-of select="format-number($p, $toolPattern)"/>
								<xsl:text>,c </xsl:text>
								<xsl:value-of select="format-number($w, $toolPattern)"/>
								<xsl:text>}</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="x" select="$objProfileOfToolIndex/ObjectFrame/ObjectFramePosition/@X * $targetUnits"/>
								<xsl:variable name="y" select="$objProfileOfToolIndex/ObjectFrame/ObjectFramePosition/@Y * $targetUnits"/>
								<xsl:variable name="z" select="$objProfileOfToolIndex/ObjectFrame/ObjectFramePosition/@Z * $targetUnits"/>
								<xsl:variable name="w" select="$objProfileOfToolIndex/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
								<xsl:variable name="p" select="$objProfileOfToolIndex/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
								<xsl:variable name="r" select="$objProfileOfToolIndex/ObjectFrame/ObjectFrameOrientation/@Roll"/>
								<xsl:text>{x </xsl:text>
								<xsl:value-of select="format-number($x, $toolPattern)"/>
								<xsl:text>,y </xsl:text>
								<xsl:value-of select="format-number($y, $toolPattern)"/>
								<xsl:text>,z </xsl:text>
								<xsl:value-of select="format-number($z, $toolPattern)"/>
								<xsl:text>,a </xsl:text>
								<xsl:value-of select="format-number($r, $toolPattern)"/>
								<xsl:text>,b </xsl:text>
								<xsl:value-of select="format-number($p, $toolPattern)"/>
								<xsl:text>,c </xsl:text>
								<xsl:value-of select="format-number($w, $toolPattern)"/>
								<xsl:text>}</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<!--
						<xsl:variable name="xyzwpr" select="concat($x, ',', $y, ',', $z, ',', $w, ',', $p, ',', $r)"/>
						<xsl:variable name="matxyzwpr" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
						<xsl:variable name="invxyzwpr" select="MatrixUtils:dgInvert()"/>
						<xsl:text>{x </xsl:text>
						<xsl:value-of select="format-number(MatrixUtils:dgGetX(), $toolPattern)"/>
						<xsl:text>,y </xsl:text>
						<xsl:value-of select="format-number(MatrixUtils:dgGetY(), $toolPattern)"/>
						<xsl:text>,z </xsl:text>
						<xsl:value-of select="format-number(MatrixUtils:dgGetZ(), $toolPattern)"/>
						<xsl:text>,a </xsl:text>
						<xsl:value-of select="format-number(MatrixUtils:dgGetRoll(), $toolPattern)"/>
						<xsl:text>,b </xsl:text>
						<xsl:value-of select="format-number(MatrixUtils:dgGetPitch(), $toolPattern)"/>
						<xsl:text>,c </xsl:text>
						<xsl:value-of select="format-number(MatrixUtils:dgGetYaw(), $toolPattern)"/>
						<xsl:text>}</xsl:text>
						-->
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>{x </xsl:text>
						<xsl:value-of select="format-number(TCPOffset/TCPPosition/@X * $targetUnits, $toolPattern)"/>
						<xsl:text>,y </xsl:text>
						<xsl:value-of select="format-number(TCPOffset/TCPPosition/@Y * $targetUnits, $toolPattern)"/>
						<xsl:text>,z </xsl:text>
						<xsl:value-of select="format-number(TCPOffset/TCPPosition/@Z * $targetUnits, $toolPattern)"/>
						<xsl:text>,a </xsl:text>
						<xsl:value-of select="format-number(TCPOffset/TCPOrientation/@Roll, $toolPattern)"/>
						<xsl:text>,b </xsl:text>
						<xsl:value-of select="format-number(TCPOffset/TCPOrientation/@Pitch, $toolPattern)"/>
						<xsl:text>,c </xsl:text>
						<xsl:value-of select="format-number(TCPOffset/TCPOrientation/@Yaw, $toolPattern)"/>
						<xsl:text>}</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end of template toolDataOutput -->
	
	<xsl:template name="checkZeroBase">
		<xsl:param name="baseName"/>
		<xsl:variable name="base" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$baseName]"/>
		<xsl:variable name="x" select="$base/ObjectFrame/ObjectFramePosition/@X"/>
		<xsl:variable name="y" select="$base/ObjectFrame/ObjectFramePosition/@Y"/>
		<xsl:variable name="z" select="$base/ObjectFrame/ObjectFramePosition/@Z"/>
		<xsl:variable name="w" select="$base/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
		<xsl:variable name="p" select="$base/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
		<xsl:variable name="r" select="$base/ObjectFrame/ObjectFrameOrientation/@Roll"/>
		<xsl:choose>
			<!-- non-zero base -->
			<xsl:when test="$x*$x + $y*$y + $z*$z + $w*$w + $p*$p + $r*$r &gt; 0.00001">0</xsl:when>
			<!-- zero base -->
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="baseDataOutput">
		<xsl:for-each select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile">
			<xsl:variable name="baseName" select="Name"/>
			<xsl:variable name="zeroBase">
				<xsl:call-template name="checkZeroBase">
					<xsl:with-param name="baseName" select="$baseName"/>
				</xsl:call-template>
			</xsl:variable>
			
			<xsl:variable name="baseIndex">
				<xsl:apply-templates select="." mode="NUM"/>
			</xsl:variable>
			<xsl:variable name="toolProfileOfBaseIndex" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[$baseIndex+1]"/>
			
			<xsl:if test="$baseIndex &gt; 0">
				<xsl:if test="$toolProfileOfBaseIndex/ToolType='Stationary' or (contains($baseName,'_FixedTCP')=false and $zeroBase=0)">
					<xsl:value-of select="$cr"/>
					<xsl:text>BASE_DATA[</xsl:text>
					<xsl:value-of select="$baseIndex"/>
					<xsl:text>]=</xsl:text>
					
					<!-- first robot motion activity -->
					<xsl:variable name="rbt_act" select="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
					<xsl:variable name="isRail" select="$rbt_act/Target/JointTarget/AuxJoint/@Type=$auxGrp1"/>
					<xsl:variable name="railPos">
						<xsl:choose>
							<xsl:when test="$isRail = 'true'">
								<xsl:value-of select="$rbt_act/Target/JointTarget/AuxJoint[@Type=$auxGrp1]/JointValue * $targetUnits"/>
							</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					
					<xsl:variable name="robotBase" select="$rbt_act/Target/BaseWRTWorld"/>
					<!-- Robot Base WRT World -->
					<xsl:variable name="baseX" select="$robotBase/Position/@X * $targetUnits"/>
					<xsl:variable name="baseY" select="$robotBase/Position/@Y * $targetUnits"/>
					<xsl:variable name="baseZ" select="$robotBase/Position/@Z * $targetUnits - KIRStrUtils:getZOffset()"/>
					<xsl:variable name="baseW" select="$robotBase/Orientation/@Yaw"/>
					<xsl:variable name="baseP" select="$robotBase/Orientation/@Pitch"/>
					<xsl:variable name="baseR" select="$robotBase/Orientation/@Roll"/>
					<xsl:variable name="xyzwpr" select="concat($baseX, ',', $baseY, ',', $baseZ, ',', $baseW, ',', $baseP, ',', $baseR)"/>
					<!-- set mat RobotBase/World -->
					<xsl:variable name="matxyzwpr" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
					
					<xsl:variable name="railConfig">
						<xsl:choose>
							<xsl:when test="$isRail = 'true'">
								<xsl:value-of select="KIRStrUtils:getRailConfig()"/>
							</xsl:when>
							<xsl:otherwise>NONE</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="roll">
						<xsl:choose>
							<xsl:when test="$railConfig = 'A'">0</xsl:when>
							<xsl:when test="$railConfig = 'B'">90</xsl:when>
							<xsl:when test="$railConfig = 'C'">180</xsl:when>
							<xsl:when test="$railConfig = 'D'">270</xsl:when>
							<xsl:when test="$railConfig = 'H'">45</xsl:when>
							<xsl:when test="$railConfig = 'I'">135</xsl:when>
							<xsl:when test="$railConfig = 'J'">225</xsl:when>
							<xsl:when test="$railConfig = 'K'">315</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="matRoll" select="MatrixUtils:dgCatXyzyprMatrix(concat('0,0,0,0,0,', $roll))"/>
					<xsl:variable name="matRail" select="MatrixUtils:dgCatXyzyprMatrix(concat(-$railPos, ',0,0,0,0,0'))"/>
					<!-- for 45-degree robot-rail configurations, KUKA robot base X is normal to rail -->
					<!-- and in between the two neighboring configurations -->
					<xsl:variable name="rollBack">
						<xsl:choose>
							<xsl:when test="$railConfig = 'A'">0</xsl:when>
							<xsl:when test="$railConfig = 'B'">90</xsl:when>
							<xsl:when test="$railConfig = 'C'">180</xsl:when>
							<xsl:when test="$railConfig = 'D'">270</xsl:when>
							<xsl:when test="$railConfig = 'H'">90</xsl:when>  <!-- same as config B -->
							<xsl:when test="$railConfig = 'I'">90</xsl:when>  <!-- same as config B -->
							<xsl:when test="$railConfig = 'J'">270</xsl:when> <!-- same as config D -->
							<xsl:when test="$railConfig = 'K'">270</xsl:when> <!-- same as config D -->
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="matRoll" select="MatrixUtils:dgCatXyzyprMatrix(concat('0,0,0,0,0,', -$rollBack))"/>
					
					<!-- invert to get mat World/RobotBase -->
					<xsl:variable name="invxyzwpr" select="MatrixUtils:dgInvert()"/>
					
					<xsl:choose>
						<xsl:when test="$toolProfileOfBaseIndex/ToolType='Stationary'">
							<!-- FixedTCP WRT World -->
							<xsl:variable name="x" select="$toolProfileOfBaseIndex/TCPOffset/TCPPosition/@X * $targetUnits"/>
							<xsl:variable name="y" select="$toolProfileOfBaseIndex/TCPOffset/TCPPosition/@Y * $targetUnits"/>
							<xsl:variable name="z" select="$toolProfileOfBaseIndex/TCPOffset/TCPPosition/@Z * $targetUnits"/>
							<xsl:variable name="a" select="$toolProfileOfBaseIndex/TCPOffset/TCPOrientation/@Roll"/>
							<xsl:variable name="b" select="$toolProfileOfBaseIndex/TCPOffset/TCPOrientation/@Pitch"/>
							<xsl:variable name="c" select="$toolProfileOfBaseIndex/TCPOffset/TCPOrientation/@Yaw"/>
							<!-- cat mat FixedTCP/World to get mat FixedTCP/RobotBase -->
							<xsl:variable name="xyzwpr" select="concat($x, ',', $y, ',', $z, ',', $c, ',', $b, ',', $a)"/>
							<xsl:variable name="matxyzwpr" select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr)"/>
						</xsl:when>
						<xsl:otherwise>
							<!-- object frame WRT World -->
							<xsl:variable name="x" select="ObjectFrame/ObjectFramePosition/@X * $targetUnits"/>
							<xsl:variable name="y" select="ObjectFrame/ObjectFramePosition/@Y * $targetUnits"/>
							<xsl:variable name="z" select="ObjectFrame/ObjectFramePosition/@Z * $targetUnits"/>
							<xsl:variable name="a" select="ObjectFrame/ObjectFrameOrientation/@Roll"/>
							<xsl:variable name="b" select="ObjectFrame/ObjectFrameOrientation/@Pitch"/>
							<xsl:variable name="c" select="ObjectFrame/ObjectFrameOrientation/@Yaw"/>
							<!-- cat mat ObjectFrame/World to get mat ObjectFrame/RobotBase -->
							<xsl:variable name="xyzwpr" select="concat($x, ',', $y, ',', $z, ',', $c, ',', $b, ',', $a)"/>
							<xsl:variable name="matxyzwpr" select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr)"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- FixedTCP or ObjectFrame WRT Robot Base -->
					<xsl:text>{x </xsl:text>
					<xsl:value-of select="format-number(MatrixUtils:dgGetX(), $toolPattern)"/>
					<xsl:text>,y </xsl:text>
					<xsl:value-of select="format-number(MatrixUtils:dgGetY(), $toolPattern)"/>
					<xsl:text>,z </xsl:text>
					<xsl:value-of select="format-number(MatrixUtils:dgGetZ(), $toolPattern)"/>
					<xsl:text>,a </xsl:text>
					<xsl:value-of select="format-number(MatrixUtils:dgGetRoll(), $toolPattern)"/>
					<xsl:text>,b </xsl:text>
					<xsl:value-of select="format-number(MatrixUtils:dgGetPitch(), $toolPattern)"/>
					<xsl:text>,c </xsl:text>
					<xsl:value-of select="format-number(MatrixUtils:dgGetYaw(), $toolPattern)"/>
					<xsl:text>}</xsl:text>
				</xsl:if>
			</xsl:if> <!-- $baseIndex &gt; 0 -->
		</xsl:for-each>
	</xsl:template>
	<!-- end of template baseDataOutput -->

	<xsl:template name="processAuxAxes">
		<xsl:param name="jTarget"/>
		<xsl:param name="auxType"/>

		<xsl:for-each select="$jTarget/AuxJoint">
			<xsl:if test="@Type=$auxType">
				<xsl:text>,E</xsl:text>
				<xsl:variable name="extAxisNum">
					<xsl:call-template name="getNumIncNext"/>
				</xsl:variable>
				<xsl:value-of select="$extAxisNum"/>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="@JointType='Translational' and @Units='m'">
						<xsl:value-of select="format-number(JointValue * $targetUnits, $cartesianPositionPattern)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number(JointValue, $cartesianPositionPattern)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'e')"/>
					<xsl:with-param name="e" select="substring-after($Num,'e')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'E')"/>
					<xsl:with-param name="e" select="substring-after($Num,'E')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
			<xsl:value-of select="$Num"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="getGDATnum">
		<xsl:param name="cont"/>
		<xsl:param name="gdat"/>

		<xsl:variable name="datName" select="'ggdat'"/>
		<xsl:choose>
			<xsl:when test="$cont='1'">
				<xsl:variable name="gdatNumCurrent" select="KIRStrUtils:getGDATIndexCurr()"/>
				<xsl:choose>
					<xsl:when test="$gdatNumCurrent &gt; '0'">
						<xsl:value-of select="$gdatNumCurrent"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="KIRStrUtils:addDAT($datName, $gdat)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="gdatIdx" select="KIRStrUtils:getDATIndex($datName, $gdat)"/>
				<xsl:choose>
					<xsl:when test="$gdatIdx='-1'">
						<xsl:value-of select="KIRStrUtils:addDAT($datName, $gdat)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$gdatIdx"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
