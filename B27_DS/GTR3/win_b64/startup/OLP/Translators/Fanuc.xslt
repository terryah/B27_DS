<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lxslt="http://xml.apache.org/xslt" xmlns:java="http://xml.apache.org/xslt/java"
	xmlns:math="http://exslt.org/math" xmlns:NumberIncrement="NumberIncrement"
	xmlns:MatrixUtils="MatrixUtils" xmlns:MotomanUtils="MotomanUtils" xmlns:FileUtils="FileUtils"
	xmlns:BitMaskUtils="BitMaskUtils" xmlns:TrigUtils="TrigUtils"
	extension-element-prefixes="NumberIncrement  MotomanUtils MatrixUtils FileUtils BitMaskUtils TrigUtils">
	<lxslt:component prefix="NumberIncrement" functions="next current"
		elements="startnum increment reset counternum">
		<lxslt:script lang="javaclass"
			src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MotomanUtils"
		functions="JointToEncoderConversion SetParameterData GetParameterData" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"
		/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils"
		functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll"
		elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"
		/>
	</lxslt:component>
	<lxslt:component prefix="BitMaskUtils" functions="bitAND bitOR bitShiftLeft bitShiftRight"
		elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.BitMaskUtils"
		/>
	</lxslt:component>
	<lxslt:component prefix="FileUtils" functions="fileOpen fileWrite fileClose" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.FileUtils"/>
	</lxslt:component>
	<lxslt:component prefix="TrigUtils" functions="SASS SASA SSSA" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.TrigUtils"/>
	</lxslt:component>
	<xsl:strip-space elements="*"/>
	<xsl:output method="text"/>
	<xsl:variable name="version"
		select="'DELMIA CORP. FANUC RJ TPE  DOWNLOADER VERSION 5 RELEASE 26 SP3'"/>
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 2002-2015 ALL RIGHTS RESERVED'"/>
	<xsl:variable name="saveInfo"
		select="'If you decide to save this file, VERSION INFO header will not be taken into account.'"/>
	<xsl:variable name="rbttype" select="Robot"/>
	<xsl:variable name="cr" select="'&#xa;'"/>
	<xsl:variable name="pat" select="'0'"/>
	<xsl:variable name="robotName"
		select="/OLPData/Resource[@ResourceType='Robot']/GeneralInfo/ResourceName"/>
	<xsl:variable name="NumRobotAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<xsl:variable name="NumBaseAuxAxes" select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<xsl:variable name="NumToolAuxAxes" select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<xsl:variable name="NumWorkAuxAxes"
		select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="numpat" select="'0000'"/>
	<xsl:variable name="posnum" select="0"/>
	<xsl:variable name="jointPositionPattern" select="'#'"/>
	<xsl:variable name="xyzPattern" select="'0.000'"/>
	<xsl:variable name="tagPrefix"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='TagPrefix']/ParameterValue"/>
	<xsl:variable name="maingroup0"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Maingroup' or ParameterName='MainGroup']/ParameterValue"/>
	<xsl:variable name="renumberPositions"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='RenumberPositions']/ParameterValue"/>
	<xsl:variable name="maingroup">
		<xsl:choose>
			<xsl:when test="string($maingroup0) != ''">
				<xsl:value-of select="$maingroup0"/>
			</xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="railgroup0">
		<xsl:if test="$NumBaseAuxAxes &gt; 0">
			<xsl:value-of
				select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Railgroup' or ParameterName='RailGroup']/ParameterValue"
			/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="railgroup">
		<xsl:choose>
			<xsl:when test="string($railgroup0) != ''">
				<xsl:value-of select="$railgroup0"/>
			</xsl:when>
			<xsl:when test="$NumBaseAuxAxes &gt; 0">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="toolgroup0">
		<xsl:if test="$NumToolAuxAxes &gt; 0">
			<xsl:value-of
				select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Toolgroup' or ParameterName='ToolGroup']/ParameterValue"
			/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="toolgroup">
		<xsl:choose>
			<xsl:when test="string($toolgroup0) != ''">
				<xsl:value-of select="$toolgroup0"/>
			</xsl:when>
			<xsl:when test="$NumToolAuxAxes &gt; 0">2</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="workgroup0">
		<xsl:if test="$NumWorkAuxAxes &gt; 0">
			<xsl:value-of
				select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Workgroup' or ParameterName='WorkGroup']/ParameterValue"
			/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="workgroup">
		<xsl:choose>
			<xsl:when test="string($workgroup0) != ''">
				<xsl:value-of select="$workgroup0"/>
			</xsl:when>
			<xsl:when test="$NumWorkAuxAxes &gt; 0">3</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="intaxes"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='intaxes']/ParameterValue"/>
	<xsl:variable name="worldCoords"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='WorldCoords']/ParameterValue"/>
	<xsl:variable name="result"
		select="MotomanUtils:SetParameterData('MAXIMUM DIGITAL SIGNAL', '100')"/>
	<xsl:variable name="tempmaxdigital"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Digital_IO']/ParameterValue"/>
	<xsl:variable name="maxdigital">
		<xsl:choose>
			<xsl:when test="string($tempmaxdigital) != ''">
				<xsl:value-of select="$tempmaxdigital"/>
			</xsl:when>
			<xsl:otherwise>100</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="tempmaxrobot"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Robot_IO']/ParameterValue"/>
	<xsl:variable name="maxrobot">
		<xsl:choose>
			<xsl:when test="string($tempmaxrobot) != ''">
				<xsl:value-of select="$tempmaxrobot"/>
			</xsl:when>
			<xsl:otherwise>200</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="tempmaxweld"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Weld_IO']/ParameterValue"/>
	<xsl:variable name="maxweld">
		<xsl:choose>
			<xsl:when test="string($tempmaxweld) != ''">
				<xsl:value-of select="$tempmaxweld"/>
			</xsl:when>
			<xsl:otherwise>300</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="controlname"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Controller']/ParameterValue"/>
	<xsl:template name="GetAccuracy">
		<xsl:param name="flyByMode" select="Off"/>
		<xsl:param name="accuracyValue" select="0"/>
		<xsl:param name="accuracyType" select="Speed"/>
		<xsl:choose>
			<xsl:when test="$flyByMode='On'">
				<xsl:choose>
					<xsl:when test="$accuracyType='Speed'">
						<xsl:text>CNT</xsl:text>
						<xsl:value-of select="$accuracyValue"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>CD</xsl:text>
						<xsl:value-of select="floor($accuracyValue*1000)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>FINE</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="IOSignal"/>
	<xsl:template match="/">
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:reset/>
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:reset/>
		<NumberIncrement:counternum counter="2"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:reset/>
		<NumberIncrement:counternum counter="0"/>
		<xsl:if
			test="contains($railgroup0, ',') or contains($toolgroup0, ',') or contains($workgroup0, ',')">
			<xsl:call-template name="setGroupAxes"/>
		</xsl:if>
		<xsl:call-template name="HeaderInfo"/>
		<xsl:apply-templates select="OLPData"/>
		<xsl:variable name="sysvarOutput"
			select="/OLPData/Resource/ParameterList/Parameter[ParameterName='SysvarOutput']/ParameterValue"/>
		<xsl:variable name="tooluserfile">
			<xsl:choose>
				<xsl:when
					test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='ToolUserFile']/ParameterValue) != ''">
					<xsl:value-of
						select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='ToolUserFile']/ParameterValue)"
					/>
				</xsl:when>
				<xsl:otherwise>tooluser</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='ToolUserFile']/ParameterValue"/>
			<xsl:if test="string($sysvarOutput) != 'false'">
			<!-- added output of sysvars.ls with Uframe, Utool and PR information -->
			<xsl:apply-templates
				select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile"
				mode="fileMode"/>
			<xsl:apply-templates
				select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile"
				mode="fileMode"/>
			<xsl:text>DATA FILE END</xsl:text>
			<xsl:if test="string($tooluserfile) != ''">
				<NumberIncrement:counternum counter="3"/>
				<NumberIncrement:startnum start="0"/>
				<NumberIncrement:increment inc="1"/>
				<NumberIncrement:reset/>
				<!-- added output of <tooluser>.ls file for setting Utool/Uframe values at the robot -->
				<xsl:call-template name="progHeader">
					<xsl:with-param name="attributeList"
						select="/OLPData/Resource/ActivityList/AttributeList[1]"/>
					<xsl:with-param name="taskName" select="$tooluserfile"/>
				</xsl:call-template>
				<xsl:value-of select="$cr"/>
				<xsl:call-template name="lineNumber">
					<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
				</xsl:call-template>
				<xsl:text>  ! WARNING  THIS PROGRAM USES POSITION REGISTER 1 TO SET UFRAME AND UTOOL VALUES AT THE ROBOT</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:call-template name="lineNumber">
					<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
				</xsl:call-template>
				<xsl:text>  ! WARNING  ONLY LOAD AND RUN THIS PROGRAM AT THE ROBOT IF THIS IS THE DESIRED RESULT</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:call-template name="lineNumber">
					<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
				</xsl:call-template>
				<xsl:text>  ! WARNING  BE SURE TO SAVE SYSTEM VARIABLES FOLLOWING USE OF THIS PROGRAM OR VALUES WILL BE LOST</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:apply-templates
					select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile"
					mode="setMode"/>
				<xsl:apply-templates
					select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile"
					mode="setMode"/>
				<xsl:text>/POS</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>/END</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>DATA FILE END</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	<xsl:template name="HeaderInfo">
		<xsl:text>VERSION INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$saveInfo"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>VERSION INFO END</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
	</xsl:template>
	<xsl:template match="OLPData">
		<xsl:apply-templates
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList"/>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	<xsl:template match="ActivityList">
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('LASTUTOOLNUMBER', '20')"/>
		<xsl:variable name="hr2" select="MotomanUtils:SetParameterData('LASTUFRAMENUMBER', '20')"/>
		<xsl:variable name="taskName">
			<xsl:choose>
				<xsl:when test="contains(string(@Task), '.')">
					<xsl:value-of
						select="concat(substring-before(string(@Task),'.'), substring-after(string(@Task),'.'))"
					/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@Task"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="hr3"
			select="MotomanUtils:SetParameterData('CURRENT_ACTIVITY_LIST_TASK', $taskName)"/>
		<xsl:value-of select="$cr"/>
		<xsl:call-template name="progHeader">
			<xsl:with-param name="attributeList" select="AttributeList"/>
			<xsl:with-param name="taskName" select="$taskName"/>
		</xsl:call-template>
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:reset/>
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>
		<xsl:if test="$renumberPositions='false' or $renumberPositions='0'">
			<xsl:for-each
				select="Activity[@ActivityType='DNBRobotMotionActivity' and string(substring-after(Target/CartesianTarget/Tag,']')) = '']">
				<xsl:call-template name="SetHighestPositionNumber"/>
			</xsl:for-each>
			<xsl:variable name="highReg"
				select="MotomanUtils:GetParameterData('HighestPositionNumber')"/>
			<xsl:variable name="hr2"
				select="MotomanUtils:SetParameterData('OriginalHighestPositionNumber',$highReg)"/>
		</xsl:if>
		<xsl:variable name="id" select="@id"/>
		<xsl:apply-templates select="Activity | Action"/>
		<xsl:variable name="postcomment"
			select="AttributeList/Attribute[substring(AttributeName,1,11) =  'PostComment']/AttributeValue"/>
		<xsl:variable name="curnum" select="NumberIncrement:current()"/>
		<xsl:if test="string($postcomment) != ''">
			<xsl:variable name="hr"
				select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Post'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
				<xsl:with-param name="linenum" select="$curnum"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="lastlinecomment"
			select="MotomanUtils:GetParameterData('LastLineComment')"/>
		<xsl:if test="$lastlinecomment='FALSE'">
			<xsl:text> ;</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:text>/POS </xsl:text>
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:reset/>
		<xsl:if test="string($renumberPositions) ='0' or string($renumberPositions) = 'false'">
			<xsl:variable name="originalHighPos"
				select="MotomanUtils:GetParameterData('OriginalHighestPositionNumber')"/>
			<xsl:variable name="hr"
				select="MotomanUtils:SetParameterData('HighestPositionNumber',$originalHighPos)"/>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="string($renumberPositions) ='0' or string($renumberPositions) = 'false'">
				<xsl:for-each
					select="Activity[@ActivityType='DNBRobotMotionActivity' and string(substring-after(Target/CartesianTarget/Tag,']')) = '' and (substring-before(substring-after(Target/CartesianTarget/Tag,'['),']') != '' or string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue) != '')]">
					<xsl:sort
						select="concat(substring-before(substring-after(Target/CartesianTarget/Tag,'['),']'),string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue))"
						data-type="number" order="ascending"/>
					<xsl:call-template name="DoPosition">
						<xsl:with-param name="positionRegister">
							<xsl:value-of select="'false'"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:for-each
					select="Activity[@ActivityType='DNBRobotMotionActivity' and (string(substring-after(Target/CartesianTarget/Tag,']')) != '' or (substring-before(substring-after(Target/CartesianTarget/Tag,'['),']') = '' and string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue) = ''))]">
					<xsl:sort
						select="substring-after(ActivityName,'.')" data-type="number" order="ascending"/>					
					<xsl:call-template name="DoPosition">
						<xsl:with-param name="positionRegister">
							<xsl:value-of select="'false'"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="Activity[@ActivityType='DNBRobotMotionActivity']">
					<xsl:call-template name="DoPosition">
						<xsl:with-param name="positionRegister">
							<xsl:value-of select="'false'"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
		<xsl:for-each
			select="/OLPData/Resource/ActivityList/Activity[@ActivityType = 'FollowPathActivity'] ">
			<!-- Call template TransformFollowPathActivityData with mode parameter set to 'TargetDeclarationSection' -->
			<xsl:if test="@ActivityType = 'FollowPathActivity' ">
				<xsl:call-template name="TransformFollowPathActivityData">
					<!-- Send the currect node, as the first parameter -->
					<xsl:with-param name="followPathActivityNode" select="."/>
					<!-- Send the mode string, as the second parameter -->
					<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
				</xsl:call-template>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>/END</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:variable name="result"
			select="MotomanUtils:SetParameterData('CURRENTGROUPNUMBER', '')"
		/>						
	</xsl:template>
	<!-- end of ActivityList -->
	<xsl:template name="DoPosition">
		<xsl:param name="positionRegister"/>
		<xsl:variable name="prefix"
		select="AttributeList/Attribute[AttributeName='PosType']/AttributeValue"/>
		<xsl:variable name="localtagname" select="Target/CartesianTarget/Tag"/>		
		<xsl:variable name="reg">
			<xsl:choose>
				<xsl:when
					test="string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue) != ''">
					<xsl:value-of
						select="AttributeList/Attribute[AttributeName='PosReg']/AttributeValue"/>
				</xsl:when>
				<xsl:when test="string(Target/CartesianTarget/Tag) != '' and substring-after($localtagname,'[') != ''">
					<xsl:variable name="localtagSuffix" select="substring-after($localtagname,'[')"/>
					<xsl:value-of select="substring-before($localtagSuffix,']')"/>					
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="substring-after(ActivityName,'.')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="tagname">
			<xsl:choose>
				<xsl:when test="string(Target/CartesianTarget/Tag) != '' and substring-after($localtagname,'[') != ''">
					<xsl:value-of select="Target/CartesianTarget/Tag"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($prefix,'[',$reg,']')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="posregtagprefix"
			select="substring($tagname,1,3+string-length($tagPrefix))"/>
		<xsl:variable name="tagSuffix" select="substring-after($tagname,']')"/>
		<xsl:variable name="curcomment"
			select="AttributeList/Attribute[AttributeName='Pos Comment']/AttributeValue"/>
		<xsl:if test="substring($tagname,1,17) != 'VISION_SHIFT_FIND'">
			<xsl:choose>
				<xsl:when test="Target/@Default = 'Joint' or Target/@Default = 'Cartesian'">
					<xsl:variable name="motop"
						select="AttributeList/Attribute[AttributeName='Motion Option']/AttributeValue"/>
					<xsl:variable name="toolName" select="MotionAttributes/ToolProfile"/>
					<xsl:variable name="objectFrameName"
						select="MotionAttributes/ObjectFrameProfile"/>
					<xsl:variable name="objectFrameReplaced"
						select="MotionAttributes/ObjectFrameProfile/@Replaced"/>
					<xsl:choose>
						<xsl:when test="string($objectFrameReplaced) = ''">
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameName)"
							/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameReplaced)"
							/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:variable name="objectName"
						select="MotomanUtils:GetParameterData('OBJECTPROFILENAME')"/>
					<xsl:variable name="configname" select="Target/CartesianTarget/Config/@Name"/>
					<xsl:variable name="charconfig1" select="substring($configname,1,1)"/>
					<xsl:variable name="charconfig2" select="substring($configname,2,1)"/>
					<xsl:variable name="charconfig3" select="substring($configname,3,1)"/>
					<xsl:variable name="joint1">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/JointTarget/Joint[@DOFNumber='1']/JointValue"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="joint4">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/JointTarget/Joint[@DOFNumber='4']/JointValue"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="joint5">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/JointTarget/Joint[@DOFNumber='5']/JointValue"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="joint6">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/JointTarget/Joint[@DOFNumber='6']/JointValue"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="turn1" select="floor(($joint1 + 180) div 360)"/>
					<xsl:variable name="turn4" select="floor(($joint4 + 180) div 360)"/>
					<xsl:variable name="turn5" select="floor(($joint5 + 180) div 360)"/>
					<xsl:variable name="turn6" select="floor(($joint6 + 180) div 360)"/>
					<xsl:variable name="targetunits" select="1000.0"/>
					<xsl:variable name="objframeunits" select="1000.0"/>
					<xsl:variable name="toolunits" select="1000.0"/>
					<xsl:variable name="rottargetunits" select="1.0"/>
					<xsl:variable name="rotobjframeunits" select="1.0"/>
					<xsl:variable name="rottoolunits" select="1.0"/>
					<xsl:variable name="xxx">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="yyy">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="zzz">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="roll">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/CartesianTarget/Orientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="pitch">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/CartesianTarget/Orientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="yaw">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/CartesianTarget/Orientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="basex">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="Target/BaseWRTWorld/Position/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="basey">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="Target/BaseWRTWorld/Position/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="basez">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="Target/BaseWRTWorld/Position/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="baser">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/BaseWRTWorld/Orientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="basep">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="Target/BaseWRTWorld/Orientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="basew">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="Target/BaseWRTWorld/Orientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframex">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFramePosition/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframey">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFramePosition/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframez">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFramePosition/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframer">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframep">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframew">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframeTotal"
						select="number($objframex) + number($objframey) + number($objframez) + number($objframep) + number($objframew) + number($objframer)"/>
					<xsl:variable name="toolx">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPPosition/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tooly">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPPosition/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolz">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPPosition/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolr">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPOrientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolp">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPOrientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolw">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPOrientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tooltype"
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/ToolType"/>
					<xsl:variable name="tagNumStr" select="substring-after($tagname,'[')"/>
					<xsl:variable name="tagNum" select="substring-before($tagNumStr,']')"/>
					<xsl:variable name="posTaskName"
						select="MotomanUtils:GetParameterData('CURRENT_ACTIVITY_LIST_TASK')"/>
					<xsl:variable name="posName" select="concat($posTaskName,$tagname)"/>
					<xsl:variable name="posDone" select="MotomanUtils:GetParameterData($posName)"/>
					<xsl:if test="Target/@Default = 'Cartesian' and string($posDone) != 'TRUE'">
						<xsl:choose>
							<xsl:when
								test="$posregtagprefix=concat($tagPrefix,'PR[') and $positionRegister='false'">
								<xsl:if
									test="string($renumberPositions)='0' or string($renumberPositions) = 'false'">
									<NumberIncrement:reset/>
								</xsl:if>
							</xsl:when>
							<xsl:when
								test="($posregtagprefix!=concat($tagPrefix,'PR[') and $positionRegister='false') or ($posregtagprefix=concat($tagPrefix,'PR[') and $positionRegister='true')">
								<xsl:choose>
									<xsl:when test="$positionRegister='true'">
										<xsl:value-of select="$cr"/>
										<xsl:text>    [1,</xsl:text>
										<xsl:value-of select="$reg"/>
										<xsl:text>] =   '</xsl:text>
										<xsl:if test="string($curcomment) != ''">
											<xsl:value-of select="$curcomment"/>
										</xsl:if>
										<xsl:text>'   Group: 1</xsl:text>
										<xsl:value-of select="$cr"/>
										<xsl:variable name="tagResult"
											select="MotomanUtils:SetParameterData($posName,'TRUE')"
										/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable name="posnum" select="NumberIncrement:next()"/>
										<xsl:value-of select="$cr"/>
										<xsl:text>P[</xsl:text>
										<xsl:choose>
											<xsl:when
												test="(string($renumberPositions)='0' or string($renumberPositions) = 'false') and string($tagSuffix)='' and string($tagNum)!=''">
												<xsl:value-of select="$tagNum"/>
												<xsl:variable name="tagResult"
												select="MotomanUtils:SetParameterData($posName,'TRUE')"
												/>
											</xsl:when>
											<xsl:when
												test="(string($renumberPositions)='0' or string($renumberPositions) = 'false') and string($tagSuffix)!=''">
												<xsl:variable name="highPos"
												select="number(MotomanUtils:GetParameterData('HighestPositionNumber')) + 1"/>
												<xsl:value-of select="number($highPos)"/>
												<xsl:variable name="hr"
												select="MotomanUtils:SetParameterData('HighestPositionNumber',$highPos)"
												/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$posnum"/>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:if test="string($curcomment) != ''">
											<xsl:text> : </xsl:text>
											<xsl:value-of select="$curcomment"/>
										</xsl:if>
										<xsl:text>]{</xsl:text>
										<xsl:call-template name="GroupOutput">
											<xsl:with-param name="groupnum">
												<xsl:value-of select="$maingroup"/>
											</xsl:with-param>
											<xsl:with-param name="axestype">
												<xsl:value-of select="'Main'"/>
											</xsl:with-param>
											<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
										</xsl:call-template>
										<xsl:value-of select="$cr"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:variable name="tltype"
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/ToolType"/>
								<xsl:choose>
									<xsl:when test="$tltype = 'Stationary'">
										<xsl:text>  UF : </xsl:text>
										<xsl:apply-templates
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
											mode="codeMode"/>
										<xsl:text>, UT : </xsl:text>
										<xsl:apply-templates
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
											mode="codeMode"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>  UF : </xsl:text>
										<xsl:apply-templates
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
											mode="codeMode"/>
										<xsl:text>, UT : </xsl:text>
										<xsl:apply-templates
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
											mode="codeMode"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>,           CONFIG : '</xsl:text>
								<xsl:if test="$charconfig1 != ''">
									<xsl:value-of select="$charconfig1"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:if test="$charconfig2 != ''">
									<xsl:value-of select="$charconfig2"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:if test="$charconfig3 != ''">
									<xsl:value-of select="$charconfig3"/>
								</xsl:if>
								<xsl:choose>
									<xsl:when
										test="/OLPData/Resource/ParameterList/Parameter[ParameterName='Turn1']/ParameterValue = 'false'"/>
									<xsl:otherwise>
										<xsl:text>, </xsl:text>
										<xsl:value-of select="$turn1"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>, </xsl:text>
								<xsl:value-of select="$turn4"/>
								<xsl:if
									test="/OLPData/Resource/ParameterList/Parameter[ParameterName='Turn1']/ParameterValue = 'false' and (string(/OLPData/Resource/ParameterList/Parameter[ParameterName='Turn5']/ParameterValue) != 'true' or $NumRobotAxes &lt; 6)">
									<xsl:text>, </xsl:text>
								</xsl:if>
								<xsl:choose>
									<xsl:when
										test="/OLPData/Resource/ParameterList/Parameter[ParameterName='Turn5']/ParameterValue = 'true'">
										<xsl:text>, </xsl:text>
										<xsl:value-of select="$turn5"/>
									</xsl:when>
									<xsl:otherwise/>
								</xsl:choose>
								<xsl:choose>
									<xsl:when
										test="/OLPData/Resource/ParameterList/Parameter[ParameterName='Turn6']/ParameterValue = 'false' or $NumRobotAxes &lt; 6"/>
									<xsl:otherwise>
										<xsl:text>, </xsl:text>
										<xsl:value-of select="$turn6"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>',</xsl:text>
								<xsl:choose>
									<xsl:when test="$tooltype != 'OnRobot'">
										<xsl:variable name="xyzwpr"
											select="concat($xxx, ',',  $yyy,  ',' , $zzz , ',' , $yaw , ',' , $pitch , ',' , $roll )"/>
										<xsl:variable name="matresult"
											select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
										<xsl:variable name="invresult"
											select="MatrixUtils:dgInvert()"/>
										<xsl:variable name="oxxx" select="MatrixUtils:dgGetX()"/>
										<xsl:variable name="oyyy" select="MatrixUtils:dgGetY()"/>
										<xsl:variable name="ozzz" select="MatrixUtils:dgGetZ()"/>
										<xsl:variable name="oyaw" select="MatrixUtils:dgGetYaw()"/>
										<xsl:variable name="opitch"
											select="MatrixUtils:dgGetPitch()"/>
										<xsl:variable name="oroll" select="MatrixUtils:dgGetRoll()"/>
										<xsl:value-of select="$cr"/>
										<xsl:text>  X = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($oxxx,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text>  mm,  Y = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($oyyy,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text>  mm,  Z = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($ozzz,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text>  mm,</xsl:text>
										<xsl:value-of select="$cr"/>
										<xsl:text>  W = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($oyaw,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text> deg,   P = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($opitch,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text> deg,   R = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($oroll,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text> deg</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<!-- 2009/04/30 - take out concatenation to get target with respect to base...>
										                  this should not be needed since the UFRAME is converted to be 
										                  with respect to the base  TARGET/UFRAME_NUM * UFRAME_NUM/BASE-->
										<!-- output Target/ObjectFrame or Base -->
										<xsl:value-of select="$cr"/>
										<xsl:text>  X = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($xxx,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text>  mm,  Y = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($yyy,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text>  mm,  Z = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($zzz,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text>  mm,</xsl:text>
										<xsl:value-of select="$cr"/>
										<xsl:text>  W = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($yaw,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text> deg,   P = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($pitch,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text> deg,   R = </xsl:text>
										<xsl:call-template name="posValueOutput">
											<xsl:with-param name="value"
												select="format-number($roll,$xyzPattern)"/>
										</xsl:call-template>
										<xsl:text> deg</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:variable name="grp1Axes"
									select="string(MotomanUtils:GetParameterData('grp1Axes'))"/>
								<xsl:variable name="grp2Axes"
									select="string(MotomanUtils:GetParameterData('grp2Axes'))"/>
								<xsl:variable name="grp3Axes"
									select="string(MotomanUtils:GetParameterData('grp3Axes'))"/>
								<xsl:variable name="grp4Axes"
									select="string(MotomanUtils:GetParameterData('grp4Axes'))"/>
								<xsl:variable name="grp5Axes"
									select="string(MotomanUtils:GetParameterData('grp5Axes'))"/>
								<!--
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp1Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp2Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp3Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp4Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp5Axes"/>
-->
								<xsl:choose>
									<xsl:when
										test="$grp1Axes != '' or $grp2Axes != '' or $grp3Axes != '' or $grp4Axes != '' or $grp5Axes != ''">
										<xsl:choose>
											<xsl:when
												test="$grp2Axes = '' and $grp3Axes = '' and $grp4Axes = '' and $grp5Axes = ''">
												<xsl:call-template name="Bothmain">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="outputGroupsR17">
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:if
											test="$railgroup = $maingroup and $toolgroup = $maingroup and $workgroup = $maingroup and $railgroup != ''">
											<xsl:call-template name="Bothmain">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test="$railgroup = $maingroup">
											<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test="$toolgroup = $maingroup">
											<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test="$workgroup = $maingroup">
											<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'WorkpiecePositioner'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:choose>
											<xsl:when
												test="$toolgroup != '' or $railgroup != '' or $workgroup != ''"/>
											<xsl:otherwise>
												<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:if
											test="$railgroup != $maingroup and  $railgroup = $toolgroup and $railgroup = $workgroup and $railgroup != '' and $toolgroup != ''">
											<xsl:call-template name="Bothgroup">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &lt; $toolgroup and $railgroup &lt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$toolgroup != $maingroup and $toolgroup != '' and $toolgroup &lt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$toolgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &lt; $workgroup and $railgroup &gt; $toolgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test="$workgroup != $maingroup and $workgroup != ''">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$workgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'WorkpiecePositioner'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &gt; $workgroup and $railgroup &lt; $toolgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$toolgroup != $maingroup and $toolgroup != '' and $toolgroup &gt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$toolgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &gt; $toolgroup and $railgroup &gt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:choose>
											<xsl:when
												test="$railgroup != '' or $toolgroup != '' or $workgroup != ''"/>
											<xsl:otherwise>
												<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$toolgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if test="$positionRegister!='true'">
									<xsl:value-of select="$cr"/>
									<xsl:text>}; </xsl:text>
								</xsl:if>
							</xsl:when>
						</xsl:choose>
					</xsl:if>
					<!-- End Cartesian Target Type !-->
					<!-- Joint Target Type !-->
					<xsl:if test="Target/@Default = 'Joint' and string($posDone) != 'TRUE'">
						<xsl:variable name="joint1Type"
							select="Target/JointTarget/Joint[@DOFNumber='1']/@JointType"/>
						<xsl:variable name="joint1">
							<xsl:call-template name="GetJointVal">
								<xsl:with-param name="jointType" select="$joint1Type"/>
								<xsl:with-param name="jointNum" select="1"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="joint2Type"
							select="Target/JointTarget/Joint[@DOFNumber='2']/@JointType"/>
						<xsl:variable name="joint2">
							<xsl:call-template name="GetJointVal">
								<xsl:with-param name="jointType" select="$joint2Type"/>
								<xsl:with-param name="jointNum" select="2"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="joint3Type"
							select="Target/JointTarget/Joint[@DOFNumber='3']/@JointType"/>
						<xsl:variable name="joint3">
							<xsl:call-template name="GetJointVal">
								<xsl:with-param name="jointType" select="$joint3Type"/>
								<xsl:with-param name="jointNum" select="3"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="joint4Type"
							select="Target/JointTarget/Joint[@DOFNumber='4']/@JointType"/>
						<xsl:variable name="joint4">
							<xsl:call-template name="GetJointVal">
								<xsl:with-param name="jointType" select="$joint4Type"/>
								<xsl:with-param name="jointNum" select="4"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="joint5Type"
							select="Target/JointTarget/Joint[@DOFNumber='5']/@JointType"/>
						<xsl:variable name="joint5">
							<xsl:call-template name="GetJointVal">
								<xsl:with-param name="jointType" select="$joint5Type"/>
								<xsl:with-param name="jointNum" select="5"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="joint5str"
							select="string(Target/JointTarget/Joint[@DOFNumber='5']/JointValue)"/>
						<xsl:variable name="joint6Type"
							select="Target/JointTarget/Joint[@DOFNumber='6']/@JointType"/>
						<xsl:variable name="joint6">
							<xsl:call-template name="GetJointVal">
								<xsl:with-param name="jointType" select="$joint6Type"/>
								<xsl:with-param name="jointNum" select="6"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="joint6str"
							select="string(Target/JointTarget/Joint[@DOFNumber='6']/JointValue)"/>
						<xsl:choose>
							<xsl:when
								test="$posregtagprefix=concat($tagPrefix,'PR[') and $positionRegister='false'">
								<xsl:if
									test="string($renumberPositions)='0' or string($renumberPositions) = 'false'">
									<NumberIncrement:reset/>
								</xsl:if>
							</xsl:when>
							<xsl:when
								test="($posregtagprefix!=concat($tagPrefix,'PR[') and $positionRegister='false') or ($posregtagprefix=concat($tagPrefix,'PR[') and $positionRegister='true')">
								<xsl:choose>
									<xsl:when test="$positionRegister='true'">
										<xsl:value-of select="$cr"/>
										<xsl:text>    [1,</xsl:text>
										<xsl:value-of select="$reg"/>
										<xsl:text>] =   '</xsl:text>
										<xsl:if test="string($curcomment) != ''">
											<xsl:value-of select="$curcomment"/>
										</xsl:if>
										<xsl:text>'   Group: 1</xsl:text>
										<xsl:value-of select="$cr"/>
										<xsl:variable name="tagResult"
											select="MotomanUtils:SetParameterData($posName,'TRUE')"
										/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable name="posnum" select="NumberIncrement:next()"/>
										<xsl:value-of select="$cr"/>
										<xsl:text>P[</xsl:text>
										<xsl:choose>
											<xsl:when
												test="(string($renumberPositions)='0' or string($renumberPositions) = 'false') and string($tagNum)!=''">
												<xsl:value-of select="$tagNum"/>
												<xsl:variable name="tagResult"
												select="MotomanUtils:SetParameterData($posName,'TRUE')"
												/>
											</xsl:when>
											<xsl:when
												test="(string($renumberPositions)='0' or string($renumberPositions) = 'false') and string($tagNum)=''">
												<xsl:variable name="highPos"
												select="number(MotomanUtils:GetParameterData('HighestPositionNumber')) + 1"/>
												<xsl:value-of select="number($highPos)"/>
												<xsl:variable name="hr"
												select="MotomanUtils:SetParameterData('HighestPositionNumber',$highPos)"
												/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$posnum"/>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:if test="string($curcomment) != ''">
											<xsl:text> : </xsl:text>
											<xsl:value-of select="$curcomment"/>
										</xsl:if>
										<xsl:text>]{</xsl:text>
										<xsl:call-template name="GroupOutput">
											<xsl:with-param name="groupnum">
												<xsl:value-of select="$maingroup"/>
											</xsl:with-param>
											<xsl:with-param name="axestype">
												<xsl:value-of select="'Main'"/>
											</xsl:with-param>
											<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
										</xsl:call-template>
										<xsl:value-of select="$cr"/>
										<xsl:variable name="tltype"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/ToolType"/>
										<xsl:choose>
											<xsl:when test="$tltype = 'Stationary'">
												<xsl:text>  UF : </xsl:text>
												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
												mode="codeMode"/>
												<xsl:text>, UT : </xsl:text>
												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
												mode="codeMode"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>  UF : </xsl:text>
												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
												mode="codeMode"/>
												<xsl:text>, UT : </xsl:text>
												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
												mode="codeMode"/>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:text>,</xsl:text>
										<xsl:value-of select="$cr"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if
									test="string($renumberPositions)='0' or string($renumberPositions) = 'false'">
									<xsl:variable name="tagResult"
										select="MotomanUtils:SetParameterData($posName,'TRUE')"/>
								</xsl:if>
								<xsl:text>  J1= </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($joint1,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:call-template name="posTypeOutput">
									<xsl:with-param name="jointType" select="$joint1Type"/>
									<xsl:with-param name="nextjointNum" select="2"/>
								</xsl:call-template>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($joint2,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:call-template name="posTypeOutput">
									<xsl:with-param name="jointType" select="$joint2Type"/>
									<xsl:with-param name="nextjointNum" select="3"/>
								</xsl:call-template>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($joint3,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:call-template name="posTypeOutput">
									<xsl:with-param name="jointType" select="$joint3Type"/>
									<xsl:with-param name="nextjointNum" select="4"/>
								</xsl:call-template>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($joint4,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:call-template name="posTypeOutput">
									<xsl:with-param name="jointType" select="$joint4Type"/>
									<xsl:with-param name="nextjointNum" select="5"/>
								</xsl:call-template>
								<xsl:if test="$joint5str!=''">
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($joint5,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:call-template name="posTypeOutput">
										<xsl:with-param name="jointType" select="$joint5Type"/>
										<xsl:with-param name="nextjointNum" select="6"/>
									</xsl:call-template>
								</xsl:if>
								<xsl:if test="$joint6str!=''">
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($joint6,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:call-template name="posTypeOutput">
										<xsl:with-param name="jointType" select="$joint6Type"/>
										<xsl:with-param name="nextjointNum" select="7"/>
									</xsl:call-template>
								</xsl:if>
								<xsl:variable name="grp1Axes"
									select="string(MotomanUtils:GetParameterData('grp1Axes'))"/>
								<xsl:variable name="grp2Axes"
									select="string(MotomanUtils:GetParameterData('grp2Axes'))"/>
								<xsl:variable name="grp3Axes"
									select="string(MotomanUtils:GetParameterData('grp3Axes'))"/>
								<xsl:variable name="grp4Axes"
									select="string(MotomanUtils:GetParameterData('grp4Axes'))"/>
								<xsl:variable name="grp5Axes"
									select="string(MotomanUtils:GetParameterData('grp5Axes'))"/>
								<!--
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp1Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp2Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp3Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp4Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp5Axes"/>
-->
								<xsl:choose>
									<xsl:when
										test="$grp1Axes != '' or $grp2Axes != '' or $grp3Axes != '' or $grp4Axes != '' or $grp5Axes != ''">
										<xsl:choose>
											<xsl:when
												test="$grp2Axes = '' and $grp3Axes = '' and $grp4Axes = '' and $grp5Axes = ''">
												<xsl:call-template name="Bothmain">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="outputGroupsR17">
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:if
											test="$railgroup = $maingroup and $toolgroup = $maingroup and $workgroup = $maingroup and $railgroup != ''">
											<xsl:call-template name="Bothmain">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test=" $railgroup = $maingroup">
											<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test=" $toolgroup = $maingroup">
											<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test=" $workgroup = $maingroup">
											<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'WorkpiecePositioner'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:choose>
											<xsl:when test="$railgroup != '' or $toolgroup != ''"/>
											<xsl:otherwise>
												<xsl:call-template name="Main">
												<xsl:with-param name="axesletter">
												<xsl:value-of select="'E'"/>
												</xsl:with-param>
												<xsl:with-param name="axesnum">
												<xsl:value-of select="1"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:if
											test="$railgroup != $maingroup and  $railgroup = $toolgroup and $railgroup = $workgroup and $railgroup != '' and $toolgroup != ''">
											<xsl:call-template name="Bothgroup">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &lt; $toolgroup and $railgroup &lt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$toolgroup != $maingroup and $toolgroup != '' and $toolgroup &lt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$toolgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &lt; $workgroup and $railgroup &gt; $toolgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if test="$workgroup != $maingroup and $workgroup != ''">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$workgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'WorkpiecePositioner'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &gt; $workgroup and $railgroup &lt; $toolgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$toolgroup != $maingroup and $toolgroup != '' and $toolgroup &gt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$toolgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:if
											test="$railgroup != $maingroup and $railgroup != '' and $railgroup &gt; $toolgroup and $railgroup &gt; $workgroup">
											<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$railgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'RailTrackGantry'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
											</xsl:call-template>
										</xsl:if>
										<xsl:choose>
											<xsl:when
												test="$railgroup != '' or $toolgroup != '' or $workgroup != ''"/>
											<xsl:otherwise>
												<xsl:call-template name="Group">
												<xsl:with-param name="groupnum">
												<xsl:value-of select="$toolgroup"/>
												</xsl:with-param>
												<xsl:with-param name="axestype">
												<xsl:value-of select="'EndOfArmTooling'"/>
												</xsl:with-param>
												<xsl:with-param name="objnam">
												<xsl:value-of select="$objectName"/>
												</xsl:with-param>
												<xsl:with-param name="tlnam">
												<xsl:value-of select="$toolName"/>
												</xsl:with-param>
												<xsl:with-param name="positionRegister"
												select="$positionRegister"/>
												<xsl:with-param name="regval" select="$reg"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if test="$positionRegister!='true'">
									<xsl:value-of select="$cr"/>
									<xsl:text>}; </xsl:text>
								</xsl:if>
							</xsl:when>
						</xsl:choose>
					</xsl:if>
					<!-- End Joint Target Type !-->
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<!-- end of DoPosition -->
	<xsl:template name="GetJointVal">
		<xsl:param name="jointType"/>
		<xsl:param name="jointNum"/>
		<xsl:choose>
			<xsl:when test="$jointType='Translational'">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num"
						select="Target/JointTarget/Joint[@DOFNumber=$jointNum]/JointValue"/>
					<xsl:with-param name="Units" select="1000.0"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num"
						select="Target/JointTarget/Joint[@DOFNumber=$jointNum]/JointValue"/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="Action">
		<NumberIncrement:counternum counter="0"/>
		<xsl:if test="@ActionType='TPESpotRetract'">
			<xsl:variable name="curnum" select="NumberIncrement:next()"/>
			<xsl:variable name="lastlinecomment"
				select="MotomanUtils:GetParameterData('LastLineComment')"/>
			<xsl:if test="$lastlinecomment='FALSE'">
				<xsl:if test="$curnum != 1">
					<xsl:text> ;</xsl:text>
				</xsl:if>
				<xsl:value-of select="$cr"/>
			</xsl:if>
			<xsl:variable name="precomment"
				select="AttributeList/Attribute[substring(AttributeName,1,10) =  'PreComment']/AttributeValue"/>
			<xsl:variable name="hr"
				select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
			<xsl:if test="$precomment != ''">
				<xsl:call-template name="processComments">
					<xsl:with-param name="prefix" select="'Pre'"/>
					<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
					<xsl:with-param name="linenum">
						<xsl:value-of select="$curnum"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<xsl:variable name="home1">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.1']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>CLOSE</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="home2">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.2']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>OPEN</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="home3">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.3']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>CLOSE</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="home4">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.4']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>OPEN</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="home5">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.5']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>CLOSE</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="home6">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.6']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>OPEN</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="home7">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.7']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>CLOSE</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="home8">
				<xsl:variable name="homeParam"
					select="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OpenHome.8']/ParameterValue)"/>
				<xsl:choose>
					<xsl:when test="$homeParam != ''">
						<xsl:value-of select="$homeParam"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>OPEN</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="homename" select="ActivityList/Activity/Target/JointTarget/HomeName"/>
			<xsl:variable name="curnum2" select="NumberIncrement:current()"/>
			<xsl:call-template name="lineNumber">
				<xsl:with-param name="linenum" select="$curnum2"/>
			</xsl:call-template>
			<xsl:text>  BACKUP[</xsl:text>
			<xsl:choose>
				<xsl:when test="substring($homename,string-length($homename),1) = '1'">
					<xsl:value-of select="$home1"/>
				</xsl:when>
				<xsl:when test="substring($homename,string-length($homename),1) = '2'">
					<xsl:value-of select="$home2"/>
				</xsl:when>
				<xsl:when test="substring($homename,string-length($homename),1) = '3'">
					<xsl:value-of select="$home3"/>
				</xsl:when>
				<xsl:when test="substring($homename,string-length($homename),1) = '4'">
					<xsl:value-of select="$home4"/>
				</xsl:when>
				<xsl:when test="substring($homename,string-length($homename),1) = '5'">
					<xsl:value-of select="$home5"/>
				</xsl:when>
				<xsl:when test="substring($homename,string-length($homename),1) = '6'">
					<xsl:value-of select="$home6"/>
				</xsl:when>
				<xsl:when test="substring($homename,string-length($homename),1) = '7'">
					<xsl:value-of select="$home7"/>
				</xsl:when>
				<xsl:when test="substring($homename,string-length($homename),1) = '8'">
					<xsl:value-of select="$home8"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>OPEN</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>]</xsl:text>
		</xsl:if>
		<xsl:if test="@ActionType='TPESpotWeld'">
			<xsl:variable name="weld_sched"
				select="string(AttributeList/Attribute[AttributeName='Weld_sched']/AttributeValue)"/>
			<xsl:variable name="equipment"
				select="string(AttributeList/Attribute[AttributeName='Equipment']/AttributeValue)"/>
			<xsl:variable name="pressure_lmh"
				select="string(AttributeList/Attribute[AttributeName='Pressure_lmh']/AttributeValue)"/>
			<xsl:variable name="backup1"
				select="string(AttributeList/Attribute[AttributeName='Backup1']/AttributeValue)"/>
			<xsl:variable name="backup2"
				select="string(AttributeList/Attribute[AttributeName='Backup2']/AttributeValue)"/>
			<xsl:variable name="motop"
				select="string(AttributeList/Attribute[AttributeName='Motion Option']/AttributeValue)"/>
			<xsl:variable name="macro"
				select="string(AttributeList/Attribute[AttributeName='Macro']/AttributeValue)"/>
			<xsl:variable name="weld_condition" select="@TimerTableName"/>
			<xsl:variable name="spotValue">
				<xsl:call-template name="createSpotValue">
					<xsl:with-param name="backup1" select="$backup1"/>
					<xsl:with-param name="pressure_lmh" select="$pressure_lmh"/>
					<xsl:with-param name="weld_sched" select="$weld_sched"/>
					<xsl:with-param name="equipment" select="$equipment"/>
					<xsl:with-param name="backup2" select="$backup2"/>
					<xsl:with-param name="weld_condition" select="$weld_condition"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="srvgun_eq_wld"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity' and position()=1]/AttributeList/Attribute[AttributeName='_servo_spot_schedule\srvgun_eq_wld']/AttributeValue)"/>
			<xsl:variable name="srvgun_eq_aftwld"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_servo_spot_schedule\srvgun_eq_aftwld']/AttributeValue)"/>
			<xsl:variable name="srvgun_wldsch_gn1"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_servo_spot_schedule\srvgun_wldsch_gn1']/AttributeValue)"/>
			<xsl:variable name="srvgun_wldsch_gn2"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_servo_spot_schedule\srvgun_wldsch_gn2']/AttributeValue)"/>
			<xsl:variable name="srvgun_prs"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_servo_spot_schedule\srvgun_prs']/AttributeValue)"/>
			<xsl:variable name="srvgun_bu"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_servo_spot_schedule\srvgun_bu']/AttributeValue)"/>
			<xsl:variable name="dospotweld"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_do_spot_weld']/AttributeValue)"/>
			<xsl:variable name="gn1_start_dist_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn1_start_dist_sch']/AttributeValue)"/>
			<xsl:variable name="gn2_start_dist_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn2_start_dist_sch']/AttributeValue)"/>
			<xsl:variable name="gn1_pressure_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn1_pressure_sch']/AttributeValue)"/>
			<xsl:variable name="gn2_pressure_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn2_pressure_sch']/AttributeValue)"/>
			<xsl:variable name="gn1_weld_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn1_weld_sch']/AttributeValue)"/>
			<xsl:variable name="gn2_weld_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn2_weld_sch']/AttributeValue)"/>
			<xsl:variable name="gn1_end_dist_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn1_end_dist_sch']/AttributeValue)"/>
			<xsl:variable name="gn2_end_dist_sch"
				select="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'  and position()=1]/AttributeList/Attribute[AttributeName='_v7_servo_spot_schedule\gn2_end_dist_sch']/AttributeValue)"/>
			<xsl:if test="$dospotweld = '1'">
				<xsl:variable name="rrsSpot">
					<xsl:text> SPOT[</xsl:text>
					<xsl:if test="$srvgun_eq_wld != '' and $srvgun_eq_wld != '0'">
						<xsl:text>EQ=</xsl:text>
						<xsl:value-of select="$srvgun_eq_wld"/>
						<xsl:text>,</xsl:text>
					</xsl:if>
					<xsl:if test="$gn1_start_dist_sch != '' and $gn1_start_dist_sch != '0'">
						<xsl:text>SD=</xsl:text>
						<xsl:choose>
							<xsl:when
								test="$gn2_start_dist_sch != '' and $gn2_start_dist_sch != '0'">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="$gn1_start_dist_sch"/>
								<xsl:text>,</xsl:text>
								<xsl:value-of select="$gn2_start_dist_sch"/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$gn1_start_dist_sch"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
					</xsl:if>
					<xsl:if test="$srvgun_prs != '' and $srvgun_prs != '0'">
						<xsl:text>P=</xsl:text>
						<xsl:value-of select="$srvgun_prs"/>
						<xsl:text>,</xsl:text>
					</xsl:if>
					<xsl:if test="$gn1_pressure_sch != '' and $gn1_pressure_sch != '0'">
						<xsl:text>P=</xsl:text>
						<xsl:choose>
							<xsl:when test="$gn2_pressure_sch != '' and $gn2_pressure_sch != '0'">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="$gn1_pressure_sch"/>
								<xsl:text>,</xsl:text>
								<xsl:value-of select="$gn2_pressure_sch"/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$gn1_pressure_sch"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
					</xsl:if>
					<xsl:if test="$srvgun_wldsch_gn1 != '' and $srvgun_wldsch_gn1 != '0'">
						<xsl:text>S=</xsl:text>
						<xsl:choose>
							<xsl:when test="$srvgun_wldsch_gn2 != '' and $srvgun_wldsch_gn2 != '0'">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="$srvgun_wldsch_gn1"/>
								<xsl:text>,</xsl:text>
								<xsl:value-of select="$srvgun_wldsch_gn2"/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$srvgun_wldsch_gn1"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$gn1_weld_sch != '' and $gn1_weld_sch != '0'">
						<xsl:text>S=</xsl:text>
						<xsl:choose>
							<xsl:when test="$gn2_weld_sch != '' and $gn2_weld_sch != '0'">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="$gn1_weld_sch"/>
								<xsl:text>,</xsl:text>
								<xsl:value-of select="$gn2_weld_sch"/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$gn1_weld_sch"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$srvgun_eq_aftwld != '' and $srvgun_eq_aftwld != '0'">
						<xsl:text>,EQ=</xsl:text>
						<xsl:value-of select="$srvgun_eq_aftwld"/>
					</xsl:if>
					<xsl:if test="$srvgun_bu != '' and $srvgun_bu != '0'">
						<xsl:text>,BU=</xsl:text>
						<xsl:value-of select="$srvgun_bu"/>
					</xsl:if>
					<xsl:if test="$gn1_end_dist_sch != '' and $gn1_end_dist_sch != '0'">
						<xsl:text>,ED=</xsl:text>
						<xsl:choose>
							<xsl:when test="$gn2_end_dist_sch != '' and $gn2_end_dist_sch != '0'">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="$gn1_end_dist_sch"/>
								<xsl:text>,</xsl:text>
								<xsl:value-of select="$gn2_end_dist_sch"/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$gn1_end_dist_sch"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:text>]</xsl:text>
				</xsl:variable>
				<xsl:if test="$spotValue != $rrsSpot">
					<xsl:variable name="spotError">
						<xsl:text>WARNING:  activity '</xsl:text>
						<xsl:value-of select="ActivityName"/>
						<xsl:text>' RRS contains '</xsl:text>
						<xsl:value-of select="$rrsSpot"/>
						<xsl:text>' action '</xsl:text>
						<xsl:value-of select="following-sibling::Action/ActionName"/>
						<xsl:text>' contains '</xsl:text>
						<xsl:value-of select="$spotValue"/>
						<xsl:text>' Using action value.</xsl:text>
					</xsl:variable>
					<xsl:call-template name="displayError">
						<xsl:with-param name="errorStr" select="$spotError"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:value-of select="$spotValue"/>
			</xsl:if>
			<xsl:if test="$weld_sched != '' and $dospotweld != '1'">
				<!-- comment out carriage return for now since both Nissan and Honda don't like it 
				<xsl:if test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OLPStyle']/ParameterValue) != 'Honda'">
					<xsl:value-of select="$cr"/>
					<xsl:text> :</xsl:text>
				</xsl:if>
				-->
				<!-- DLY Start 2006/05/20 for simple SPOT[1] command always output a new line with line number per DJ request for Nissan -->
				<!-- DLY Start 2007/04/21 for air gun output on a separate axes regardless of the attributes -->
				<xsl:if test="$NumToolAuxAxes = 0">
					<xsl:variable name="curnum" select="NumberIncrement:next()"/>
					<xsl:variable name="lastlinecomment"
						select="MotomanUtils:GetParameterData('LastLineComment')"/>
					<xsl:if test="$lastlinecomment='FALSE'">
						<xsl:if test="$curnum != 1">
							<xsl:text> ;</xsl:text>
						</xsl:if>
						<xsl:value-of select="$cr"/>
					</xsl:if>
					<xsl:variable name="curnum2" select="NumberIncrement:current()"/>
					<xsl:call-template name="lineNumber">
						<xsl:with-param name="linenum" select="$curnum2"/>
					</xsl:call-template>
					<xsl:text>  </xsl:text>
				</xsl:if>
				<!-- DLY End -->
				<xsl:value-of select="$spotValue"/>
			</xsl:if>
			<xsl:if test="$weld_condition != ''">
				<!-- use carriage return and a new line number for now since both Nissan and Honda seem to want it this way
				<xsl:if test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OLPStyle']/ParameterValue) != 'Honda'">
					<xsl:value-of select="$cr"/>
					<xsl:text> :</xsl:text>
				</xsl:if>
				-->
				<xsl:variable name="curnum" select="NumberIncrement:next()"/>
				<xsl:variable name="lastlinecomment"
					select="MotomanUtils:GetParameterData('LastLineComment')"/>
				<xsl:if test="$lastlinecomment='FALSE'">
					<xsl:if test="$curnum != 1">
						<xsl:text> ;</xsl:text>
					</xsl:if>
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<xsl:variable name="curnum2" select="NumberIncrement:current()"/>
				<xsl:call-template name="lineNumber">
					<xsl:with-param name="linenum" select="$curnum2"/>
				</xsl:call-template>
				<xsl:text>  </xsl:text>
				<xsl:value-of select="$spotValue"/>
			</xsl:if>
			<xsl:if test="$motop != ''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$motop"/>
			</xsl:if>
		</xsl:if>
		<xsl:variable name="macro"
			select="AttributeList/Attribute[AttributeName='Macro']/AttributeValue"/>
		<xsl:if test="$macro != ''">
			<xsl:variable name="lastlinecomment"
				select="MotomanUtils:GetParameterData('LastLineComment')"/>
			<xsl:if test="$lastlinecomment='FALSE'">
				<xsl:text> ;</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:if>
			<xsl:variable name="curnum3" select="NumberIncrement:next()"/>
			<xsl:call-template name="lineNumber">
				<xsl:with-param name="linenum" select="$curnum3"/>
			</xsl:call-template>
			<xsl:value-of select="$macro"/>
		</xsl:if>
		<xsl:variable name="lastlinecomment"
			select="MotomanUtils:GetParameterData('LastLineComment')"/>
		<xsl:variable name="postcomment"
			select="AttributeList/Attribute[substring(AttributeName,1,11) = 'PostComment']/AttributeValue"/>
		<xsl:if
			test="position()=last() and string($lastlinecomment) != 'TRUE' and $postcomment = ''">
			<xsl:text> ;</xsl:text>
		</xsl:if>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
		<xsl:if test="$postcomment != ''">
			<xsl:variable name="curnum4" select="NumberIncrement:current()"/>
			<xsl:text> ;</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Post'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
				<xsl:with-param name="linenum">
					<xsl:value-of select="$curnum4"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- end of Action -->
	<xsl:template match="Activity">
		<xsl:variable name="robotLanguage"
			select="AttributeList/Attribute[AttributeName='Robot Language']/AttributeValue"/>
		<xsl:variable name="comment"
			select="AttributeList/Attribute[AttributeName='Comment']/AttributeValue"/>
		<xsl:variable name="speedProfile" select="MotionAttributes/MotionProfile"/>
		<xsl:variable name="speedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$speedProfile]/Speed/@Value"/>
		<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
		<xsl:variable name="accuracyValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyValue/@Value"/>
		<xsl:variable name="accuracyType"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyType"/>
		<xsl:variable name="flyByMode"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/FlyByMode"/>
		<xsl:variable name="speedUnits"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$speedProfile]/Speed/@Units "/>
		<xsl:variable name="maxSpeedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
		<xsl:variable name="arcStartName" select="MotionAttributes/UserProfile[@Type='TPEArcStart']"/>
		<xsl:variable name="arcEndName" select="MotionAttributes/UserProfile[@Type='TPEArcEnd']"/>
		<xsl:variable name="weldspeed"
			select="AttributeList/Attribute[AttributeName='WELD_SPEED']/AttributeValue"/>
		<xsl:variable name="angSpeedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$speedProfile]/AngularSpeedValue/@Value"/>
		<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
		<xsl:variable name="waitSignalName" select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
		<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
		<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
		<xsl:variable name="signalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>
		<xsl:variable name="outputPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
		<xsl:variable name="inputPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
		<xsl:variable name="maxWaitTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>
		<xsl:variable name="signalComment"
		select="AttributeList/Attribute[AttributeName='SignalComment']/AttributeValue"/>
		<xsl:variable name="localtagname" select="Target/CartesianTarget/Tag"/>				
		<xsl:variable name="tagname">
			<xsl:choose>
				<xsl:when test="string(Target/CartesianTarget/Tag) != '' and substring-after($localtagname,'[') != ''">
					<xsl:value-of select="Target/CartesianTarget/Tag"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="prefix"
						select="AttributeList/Attribute[AttributeName='PosType']/AttributeValue"/>
					<xsl:variable name="reg">
						<xsl:choose>
							<xsl:when test="string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue) != ''">
								<xsl:value-of select="AttributeList/Attribute[AttributeName='PosReg']/AttributeValue"/>								
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="substring-after(ActivityName,'.')"/>																
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:value-of select="concat($prefix,'[',$reg,']')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if
			test="@ActivityType='DNBRobotMotionActivity' and substring($tagname,1,17) = 'VISION_SHIFT_FIND'">
			<NumberIncrement:counternum counter="0"/>
		</xsl:if>
		<xsl:if test="substring($tagname,1,17) != 'VISION_SHIFT_FIND'">
			<xsl:choose>
				<xsl:when test="MotionAttributes/MotionType">
					<xsl:if test="MotionAttributes/MotionType != 'Circular'">
						<xsl:variable name="curnum" select="NumberIncrement:next()"/>
						<xsl:if test="@ActivityType != 'FollowPathActivity'">
							<xsl:variable name="lastlinecomment"
								select="MotomanUtils:GetParameterData('LastLineComment')"/>
							<xsl:if test="$lastlinecomment='FALSE'">
								<xsl:if test="$curnum != 1">
									<xsl:text> ;</xsl:text>
								</xsl:if>
								<xsl:value-of select="$cr"/>
							</xsl:if>
							<xsl:variable name="precomment"
								select="AttributeList/Attribute[substring(AttributeName,1,10) =  'PreComment']/AttributeValue"/>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
							<xsl:if test="$precomment != ''">
								<xsl:call-template name="processComments">
									<xsl:with-param name="prefix" select="'Pre'"/>
									<xsl:with-param name="attributeListNodeSet"
										select="AttributeList"/>
									<xsl:with-param name="linenum">
										<xsl:value-of select="$curnum"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</xsl:if>
						<xsl:variable name="curnum5" select="NumberIncrement:current()"/>
						<xsl:if test="@ActivityType != 'FollowPathActivity'">
							<xsl:call-template name="lineNumber">
								<xsl:with-param name="linenum" select="$curnum5"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="curnum" select="NumberIncrement:next()"/>
					<xsl:if test="@ActivityType != 'FollowPathActivity'">
						<xsl:variable name="lastlinecomment"
							select="MotomanUtils:GetParameterData('LastLineComment')"/>
						<xsl:if test="$lastlinecomment='FALSE'">
							<xsl:if test="$curnum != 1">
								<xsl:text> ;</xsl:text>
							</xsl:if>
							<xsl:value-of select="$cr"/>
						</xsl:if>
						<xsl:variable name="precomment"
							select="AttributeList/Attribute[substring(AttributeName,1,10) =  'PreComment']/AttributeValue"/>
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
						<xsl:if test="$precomment != ''">
							<xsl:call-template name="processComments">
								<xsl:with-param name="prefix" select="'Pre'"/>
								<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
								<xsl:with-param name="linenum">
									<xsl:value-of select="$curnum"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:if>
					</xsl:if>
					<xsl:variable name="curnum2" select="NumberIncrement:current()"/>
					<xsl:if test="@ActivityType != 'FollowPathActivity'">
						<xsl:call-template name="lineNumber">
							<xsl:with-param name="linenum" select="$curnum2"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@ActivityType='FollowPathActivity'">
					<xsl:call-template name="TransformFollowPathActivityData">
						<!-- Send the currect node, as the first parameter -->
						<xsl:with-param name="followPathActivityNode" select="."/>
						<!-- Send the mode string, as the second parameter -->
						<xsl:with-param name="mode" select=" 'MoveAlongStatement' "/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
					<NumberIncrement:counternum counter="0"/>
					<xsl:variable name="toolName" select="MotionAttributes/ToolProfile"/>
					<xsl:variable name="objectFrameName"
						select="MotionAttributes/ObjectFrameProfile"/>
					<xsl:variable name="objectFrameReplaced"
						select="MotionAttributes/ObjectFrameProfile/@Replaced"/>
					<xsl:choose>
						<xsl:when test="string($objectFrameReplaced) = ''">
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameName)"
							/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameReplaced)"
							/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:variable name="objectName"
						select="MotomanUtils:GetParameterData('OBJECTPROFILENAME')"/>
					<xsl:variable name="posvalue" select="position() - 1"/>
					<xsl:variable name="tltype"
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/ToolType"/>
					<xsl:variable name="motop">
						<xsl:variable name="mtnop"
							select="AttributeList/Attribute[AttributeName='Motion Option']/AttributeValue"/>
						<xsl:choose>
							<xsl:when test="string($mtnop) != ''">
								<xsl:value-of select="$mtnop"/>
							</xsl:when>
							<xsl:when test="string($tltype) = 'Stationary'">
								<xsl:text>RTCP</xsl:text>
							</xsl:when>
						</xsl:choose>
					</xsl:variable>
					<xsl:if test="$toolName and MotionAttributes/MotionType != 'Circular'">
						<xsl:if
							test="not(string(preceding-sibling::Activity[@ActivityType = 'DNBRobotMotionActivity'][1]/MotionAttributes/ToolProfile) = $toolName)">
							<xsl:variable name="toolNumHex">
								<xsl:apply-templates
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
									mode="codeMode">
									<xsl:with-param name="store" select="'false'"/>
								</xsl:apply-templates>
							</xsl:variable>
							<xsl:variable name="toolNum">
								<xsl:choose>
									<xsl:when test="$toolNumHex = 'A'">
										<xsl:text>10</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$toolNumHex"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:variable name="toolCheck">
								<xsl:variable name="tln" select="substring-after($toolName, ' = ')"/>
								<xsl:choose>
									<xsl:when test="$tln != ''">
										<xsl:value-of select="$tln"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$toolName"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:variable name="lastTool"
								select="MotomanUtils:GetParameterData('LASTUTOOLNUMBER')"/>
							<!-- <xsl:text>LASTTOOL:</xsl:text>
							<xsl:value-of select="$lastTool"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>LASTTOOLNUMBER:</xsl:text>
							<xsl:value-of select="number($lastTool)"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>TOOLCHECK:</xsl:text>
							<xsl:value-of select="$toolCheck"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>TOOLCHECKNUMBER:</xsl:text>
							<xsl:value-of select="number($toolCheck)"/>
							<xsl:value-of select="$cr"/>	-->
							<xsl:if
								test="(number($toolCheck) != NaN and number($toolCheck) != number($lastTool)) or $toolNum != number($lastTool)">
								<xsl:choose>
									<xsl:when test="substring($toolName,1,12) = 'UTOOL_NUM = '">
										<xsl:variable name="toolNum"
											select="number(substring-after($toolName,'UTOOL_NUM = '))"/>
										<xsl:choose>
											<xsl:when test="$toolNum &gt; 10">
												<xsl:call-template name="displayError">
												<xsl:with-param name="errorStr"
												select="'UTOOL NUMBER GREATER THAN 10 IS INVALID FOR FANUC TPE LANGUAGE - MAXIMUM 10 WILL BE USED'"
												/>
												</xsl:call-template>
												<xsl:text>  UTOOL_NUM = 10</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
												<xsl:when test="$tltype='Stationary'">
												<xsl:text>  UFRAME_NUM = </xsl:text>
												</xsl:when>
												<xsl:otherwise>
												<xsl:text>  UTOOL_NUM = </xsl:text>
												</xsl:otherwise>
												</xsl:choose>

												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
												mode="codeMode"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:when test="substring($toolName,1,12) = 'UTOOL_NUM[GP'">
										<xsl:variable name="toolNum"
											select="number(substring-after($toolName,']='))"/>
										<xsl:variable name="groupToolOutput"
											select="substring($toolName,1,14)"/>
										<xsl:choose>
											<xsl:when test="$toolNum &gt; 10">
												<xsl:call-template name="displayError">
												<xsl:with-param name="errorStr"
												select="'UTOOL NUMBER GREATER THAN 10 IS INVALID FOR FANUC TPE LANGUAGE - MAXIMUM 10 WILL BE USED'"
												/>
												</xsl:call-template>
												<xsl:text>  UTOOL_NUM = 10</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
												<xsl:when test="$tltype='Stationary'">
												<xsl:text>  UFRAME_NUM = </xsl:text>
												</xsl:when>
												<xsl:otherwise>
												<xsl:text>  </xsl:text>
												<xsl:value-of select="$groupToolOutput"/>
												</xsl:otherwise>
												</xsl:choose>

												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
												mode="codeMode"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="$tltype='Stationary'">
												<xsl:text>  UFRAME_NUM = </xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>  UTOOL_NUM = </xsl:text>
											</xsl:otherwise>
										</xsl:choose>

										<xsl:apply-templates
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
											mode="codeMode"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:variable name="curnum" select="NumberIncrement:next()"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="$curnum"/>
								</xsl:call-template>
							</xsl:if>
						</xsl:if>
					</xsl:if>
					<xsl:if test="$objectName and MotionAttributes/MotionType != 'Circular'">
						<xsl:if
							test="not(string(preceding-sibling::Activity[@ActivityType = 'DNBRobotMotionActivity'][1]/MotionAttributes/ObjectFrameProfile) = $objectName) and not(string(preceding-sibling::Activity[@ActivityType = 'DNBRobotMotionActivity'][1]/MotionAttributes/ObjectFrameProfile/@Replaced) = $objectName)">
							<xsl:variable name="frameNum">
								<xsl:apply-templates
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
									mode="codeMode">
									<xsl:with-param name="toolType" select="string($tltype)"/>
									<xsl:with-param name="toolName" select="string($toolName)"/>
									<xsl:with-param name="store" select="false"/>
								</xsl:apply-templates>

							</xsl:variable>
							<xsl:variable name="frameCheck">
								<xsl:variable name="frn" select="substring-after($objectName,' = ')"/>
								<xsl:choose>
									<xsl:when test="$frn != ''">
										<xsl:value-of select="$frn"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$objectName"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:variable name="lastFrame"
								select="MotomanUtils:GetParameterData('LASTUFRAMENUMBER')"/>
							<!-- <xsl:text>LASTFRAME:</xsl:text>
								<xsl:value-of select="$lastFrame"/>
								<xsl:value-of select="$cr"/>
								<xsl:text>LASTUFRAMENUMBER:</xsl:text>
								<xsl:value-of select="number($lastFrame)"/>
								<xsl:value-of select="$cr"/>
								<xsl:text>FRAMECHECK:</xsl:text>
								<xsl:value-of select="$frameCheck"/>
								<xsl:value-of select="$cr"/>
								<xsl:text>FRAMECHECKNUMBER:</xsl:text>
								<xsl:value-of select="number($frameCheck)"/>
								<xsl:value-of select="$cr"/> -->
							<xsl:if
								test="(number($frameCheck) != NaN and number($frameCheck) != number($lastFrame)) or $frameNum != number($lastFrame)">
								<xsl:choose>
									<xsl:when test="substring($objectName,1,13) = 'UFRAME_NUM = '">
										<xsl:variable name="frameNum"
											select="number(substring-after($objectName,'UFRAME_NUM = '))"/>
										<xsl:choose>
											<xsl:when test="$frameNum &gt; 9">
												<xsl:call-template name="displayError">
												<xsl:with-param name="errorStr"
												select="'UFRAME NUMBER GREATER THAN 9 IS INVALID FOR FANUC TPE LANGUAGE - MAXIMUM 9 WILL BE USED'"
												/>
												</xsl:call-template>
												<xsl:text>  UFRAME_NUM = 9</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
												<xsl:when test="$tltype='Stationary'">
												<xsl:text>  UTOOL_NUM = </xsl:text>
												</xsl:when>
												<xsl:otherwise>
												<xsl:text>  UFRAME_NUM = </xsl:text>
												</xsl:otherwise>
												</xsl:choose>

												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
												mode="codeMode">
												<xsl:with-param name="toolType"
												select="string($tltype)"/>
												<xsl:with-param name="toolName"
												select="string($toolName)"/>
												</xsl:apply-templates>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:when test="substring($objectName,1,13) = 'UFRAME_NUM[GP'">
										<xsl:variable name="frameNum"
											select="number(substring-after($objectName,']='))"/>
										<xsl:variable name="groupObjectOutput"
											select="substring($objectName,1,15)"/>
										<xsl:choose>
											<xsl:when test="$frameNum &gt; 9">
												<xsl:call-template name="displayError">
												<xsl:with-param name="errorStr"
												select="'UFRAME NUMBER GREATER THAN 9 IS INVALID FOR FANUC TPE LANGUAGE - MAXIMUM 9 WILL BE USED'"
												/>
												</xsl:call-template>
												<xsl:text>  UFRAME_NUM = 9</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
												<xsl:when test="$tltype='Stationary'">
												<xsl:text>  UTOOL_NUM = </xsl:text>
												</xsl:when>
												<xsl:otherwise>
												<xsl:text>  </xsl:text>
												<xsl:value-of select="$groupObjectOutput"/>
												</xsl:otherwise>
												</xsl:choose>

												<xsl:apply-templates
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
												mode="codeMode">
												<xsl:with-param name="toolType"
												select="string($tltype)"/>
												<xsl:with-param name="toolName"
												select="string($toolName)"/>
												</xsl:apply-templates>

											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="$tltype='Stationary'">
												<xsl:text>  UTOOL_NUM = </xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>  UFRAME_NUM = </xsl:text>
											</xsl:otherwise>
										</xsl:choose>

										<xsl:apply-templates
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
											mode="codeMode">
											<xsl:with-param name="toolType" select="string($tltype)"/>
											<xsl:with-param name="toolName"
												select="string($toolName)"/>
										</xsl:apply-templates>

									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:variable name="curnum" select="NumberIncrement:next()"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="$curnum"/>
								</xsl:call-template>
							</xsl:if>
						</xsl:if>
					</xsl:if>
					<xsl:if test="MotionAttributes/MotionType='Joint'">
						<xsl:text>J </xsl:text>
					</xsl:if>
					<xsl:if test="MotionAttributes/MotionType='Linear'">
						<xsl:text>L </xsl:text>
					</xsl:if>
					<xsl:if test="MotionAttributes/MotionType='CircularVia'">
						<xsl:text>C </xsl:text>
					</xsl:if>
					<xsl:if test="MotionAttributes/MotionType='Circular'">
						<xsl:value-of select="$cr"/>
						<xsl:text>	</xsl:text>
					</xsl:if>
					<xsl:variable name="localtagname" select="Target/CartesianTarget/Tag"/>									
					<xsl:variable name="tagname">
						<xsl:choose>
							<xsl:when test="string(Target/CartesianTarget/Tag) != '' and substring-after($localtagname,'[') != ''">
								<xsl:value-of select="Target/CartesianTarget/Tag"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="prefix"
									select="AttributeList/Attribute[AttributeName='PosType']/AttributeValue"/>
								<xsl:variable name="reg">
									<xsl:choose>
										<xsl:when test="string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue) != ''">
											<xsl:value-of select="AttributeList/Attribute[AttributeName='PosReg']/AttributeValue"/>								
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="substring-after(ActivityName,'.')"/>																
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:value-of select="concat($prefix,'[',$reg,']')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="posregtagprefix"
						select="substring($tagname,1,3+string-length($tagPrefix))"/>
					<xsl:variable name="tagsuffix"
						select="substring-after($tagname,$posregtagprefix)"/>
					<xsl:variable name="posregnum" select="substring-before($tagsuffix,']')"/>
					<xsl:choose>
						<xsl:when test="$posregtagprefix = concat($tagPrefix, 'PR[')">
							<xsl:text>PR[</xsl:text>
							<xsl:value-of select="$posregnum"/>
							<xsl:if
								test="string($renumberPositions)='0' or string($renumberPositions) = 'false'">
								<NumberIncrement:counternum counter="1"/>
								<NumberIncrement:reset/>
								<NumberIncrement:counternum counter="0"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>P[</xsl:text>
							<xsl:variable name="tagSuffix" select="substring-after($tagname,']')"/>
							<xsl:variable name="tagNumStr" select="substring-after($tagname,'[')"/>
							<xsl:variable name="tagNum" select="substring-before($tagNumStr,']')"/>
							<xsl:choose>
								<xsl:when
									test="string($renumberPositions)='false' and string($tagSuffix)='' and string($tagNum) != ''">
									<xsl:value-of select="$tagNum"/>
								</xsl:when>
								<xsl:when
									test="string($renumberPositions)='0' and string($tagSuffix)='' and string($tagNum) != ''">
									<xsl:value-of select="$tagNum"/>
								</xsl:when>
								<xsl:when
									test="string($renumberPositions)='0' or string($renumberPositions) = 'false'">
									<xsl:variable name="highPos"
										select="number(MotomanUtils:GetParameterData('HighestPositionNumber')) + 1"/>
									<xsl:value-of select="number($highPos)"/>
									<xsl:variable name="hr"
										select="MotomanUtils:SetParameterData('HighestPositionNumber',$highPos)"
									/>
								</xsl:when>
								<xsl:otherwise>
									<NumberIncrement:counternum counter="1"/>
									<xsl:variable name="posnum" select="NumberIncrement:next()"/>
									<NumberIncrement:counternum counter="0"/>
									<xsl:value-of select="$posnum"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:variable name="siblingcomment"
						select="following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='Pos Comment']/AttributeValue"/>
					<xsl:variable name="siblingacttype"
						select="following-sibling::*[position() = 1]/@ActivityType"/>
					<xsl:variable name="currcomment"
						select="AttributeList/Attribute[AttributeName='Pos Comment']/AttributeValue"/>
					<xsl:variable name="sComment" select="string($currcomment)"/>
					<xsl:choose>
						<xsl:when
							test="string($currcomment) != '' and string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OLPStyle']/ParameterValue) != 'Honda'">
							<xsl:text> : </xsl:text>
							<xsl:choose>
								<xsl:when test="starts-with(string($currcomment),'&quot;')">
									<xsl:variable name="afterQuoteComment"
										select="substring(string($currcomment),2)"/>
									<xsl:choose>
										<xsl:when test="contains($afterQuoteComment,'&quot;')">
											<xsl:value-of
												select="substring-before($afterQuoteComment,'&quot;')"
											/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$afterQuoteComment"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$currcomment"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$siblingacttype = 'TPESpotWeld'">
								<xsl:if test="string($siblingcomment) != ''">
									<xsl:text> : </xsl:text>
									<xsl:value-of select="$siblingcomment"/>
								</xsl:if>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>] </xsl:text>
					<xsl:if test="MotionAttributes/MotionType!='CircularVia'">
						<xsl:choose>
							<xsl:when test="$weldspeed = 'TRUE'">
								<xsl:text>WELD_SPEED </xsl:text>
							</xsl:when>
							<xsl:when test="contains($speedProfile,'/')">
								<xsl:value-of select="$speedProfile"/>
								<xsl:text> </xsl:text>
							</xsl:when>
							<xsl:when test="$speedUnits = '%'">
								<xsl:if test="MotionAttributes/MotionType='Joint'">
									<xsl:value-of
										select="format-number($speedValue, $jointPositionPattern)"/>
									<xsl:text>% </xsl:text>
								</xsl:if>
								<xsl:if
									test="MotionAttributes/MotionType='Linear' or MotionAttributes/MotionType='Circular' ">
									<xsl:if test="$angSpeedValue='100'">
										<xsl:value-of
											select="format-number($maxSpeedValue * $speedValue * 10, $jointPositionPattern)"/>
										<xsl:text>mm/sec </xsl:text>
									</xsl:if>
									<xsl:if test="$angSpeedValue != '100'">
										<xsl:value-of
											select="format-number(360 * $angSpeedValue div 100, $jointPositionPattern)"/>
										<xsl:text>deg/sec </xsl:text>
									</xsl:if>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$speedUnits = 'm/s'">
								<xsl:if
									test="MotionAttributes/MotionType='Linear' or MotionAttributes/MotionType='Circular'">
									<xsl:if test="$angSpeedValue='100'">
										<xsl:value-of
											select="format-number($speedValue * 1000, $jointPositionPattern)"/>
										<xsl:text>mm/sec </xsl:text>
									</xsl:if>
									<xsl:if test="$angSpeedValue != '100'">
										<xsl:value-of
											select="format-number(360 * $angSpeedValue div 100, $jointPositionPattern)"/>
										<xsl:text>deg/sec </xsl:text>
									</xsl:if>
								</xsl:if>
								<xsl:if test="MotionAttributes/MotionType='Joint'">
									<xsl:value-of
										select="format-number($speedValue * 100 div $maxSpeedValue, $jointPositionPattern)"/>
									<xsl:text>% </xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="format-number($speedValue, $xyzPattern)"/>
								<xsl:text>sec </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:call-template name="GetAccuracy">
							<xsl:with-param name="flyByMode">
								<xsl:value-of select="$flyByMode"/>
							</xsl:with-param>
							<xsl:with-param name="accuracyValue">
								<xsl:value-of select="$accuracyValue"/>
							</xsl:with-param>
							<xsl:with-param name="accuracyType">
								<xsl:value-of select="$accuracyType"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:if test="MotionAttributes/OrientMode = 'Wrist'">
							<xsl:text> WJNT</xsl:text>
						</xsl:if>
						<xsl:if test="$motop != ''">
							<xsl:text> </xsl:text>
							<xsl:value-of select="$motop"/>
						</xsl:if>
						<xsl:if test="string($arcStartName)!='' or string($arcEndName) != ''">
							<xsl:variable name="curnum" select="NumberIncrement:next()"/>
							<xsl:variable name="lastlinecomment"
								select="MotomanUtils:GetParameterData('LastLineComment')"/>
							<xsl:if test="$lastlinecomment='FALSE'">
								<xsl:if test="$curnum != 1">
									<xsl:text> ;</xsl:text>
								</xsl:if>
								<xsl:value-of select="$cr"/>
							</xsl:if>
							<xsl:variable name="curnum2" select="NumberIncrement:current()"/>
							<xsl:call-template name="lineNumber">
								<xsl:with-param name="linenum" select="$curnum2"/>
							</xsl:call-template>
							<xsl:text>  </xsl:text>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="contains($arcStartName,'.')">
								<xsl:call-template name="outputArcProfile">
									<xsl:with-param name="arcName" select="$arcStartName"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="string($arcStartName)!=''">
								<xsl:text> </xsl:text>
								<xsl:value-of select="$arcStartName"/>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
						<xsl:choose>
							<xsl:when test="contains($arcEndName,'.')">
								<xsl:call-template name="outputArcProfile">
									<xsl:with-param name="arcName" select="$arcEndName"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="string($arcEndName)!=''">
								<xsl:text> </xsl:text>
								<xsl:value-of select="$arcEndName"/>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
						<xsl:text> </xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="@ActivityType='DNBSetSignalActivity'">
					<xsl:choose>
						<xsl:when test="$signalDuration != 0 and $setSignalValue='On'">
							<xsl:call-template name="mapio">
								<xsl:with-param name="iotype" select="'O'"/>
								<xsl:with-param name="iovalue" select="string($outputPortNumber)"/>
								<xsl:with-param name="comment" select="string($signalComment)"/>
								<xsl:with-param name="ioname" select="string($setSignalName)"/>
							</xsl:call-template>
							<xsl:text>] =  PULSE,</xsl:text>
							<xsl:value-of select="format-number($signalDuration, $xyzPattern)"/>
							<xsl:text>sec </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="mapio">
								<xsl:with-param name="iotype" select="'O'"/>
								<xsl:with-param name="iovalue" select="string($outputPortNumber)"/>
								<xsl:with-param name="comment" select="string($signalComment)"/>
								<xsl:with-param name="ioname" select="string($setSignalName)"/>
							</xsl:call-template>
							<xsl:text>] = </xsl:text>
							<xsl:variable name="groupValue"
								select="AttributeList/Attribute[AttributeName='GroupValue']/AttributeValue"/>
							<xsl:choose>
								<xsl:when test="$setSignalValue='Off' and string($groupValue)=''">
									<xsl:text>OFF</xsl:text>
								</xsl:when>
								<xsl:when test="$setSignalValue='On' and string($groupValue)=''">
									<xsl:text>ON</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$groupValue"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
					<xsl:call-template name="mapio">
						<xsl:with-param name="iotype" select="'I'"/>
						<xsl:with-param name="iovalue" select="string($inputPortNumber)"/>
						<xsl:with-param name="comment" select="string($signalComment)"/>
						<xsl:with-param name="ioname" select="string($waitSignalName)"/>
					</xsl:call-template>
					<xsl:text>] = </xsl:text>
					<xsl:variable name="groupValue"
						select="AttributeList/Attribute[AttributeName='GroupValue']/AttributeValue"/>
					<xsl:variable name="timeoutLabel"
						select="string(AttributeList/Attribute[AttributeName='TimeoutLabel']/AttributeValue)"/>
					<xsl:choose>
						<xsl:when test="$waitSignalValue='Off' and string($groupValue)=''">
							<xsl:text>OFF</xsl:text>
						</xsl:when>
						<xsl:when test="$waitSignalValue='On' and string($groupValue)=''">
							<xsl:text>ON</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$groupValue"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$maxWaitTime != 0 or contains($timeoutLabel, 'TIMEOUT')">
						<NumberIncrement:counternum counter="2"/>
						<xsl:variable name="labelname"
							select="concat('LBL[',string(NumberIncrement:next()),']')"/>
						<xsl:text> </xsl:text>
						<xsl:if test="not(contains($timeoutLabel, 'TIMEOUT'))">
							<xsl:value-of select="$maxWaitTime"/>
							<xsl:text>(sec),</xsl:text>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="string($timeoutLabel)=''">
								<xsl:value-of select="string($labelname)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="string($timeoutLabel)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:variable name="waitlabel">
							<xsl:choose>
								<xsl:when test="string($timeoutLabel)=''">
									<xsl:value-of select="string($labelname)"/>
								</xsl:when>
								<xsl:when test="contains($timeoutLabel, 'TIMEOUT')">
									<xsl:value-of select="substring-after($timeoutLabel,',')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string($timeoutLabel)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData('OutputWaitLabel','TRUE')"/>
						<xsl:call-template name="DoCheckLabel">
							<xsl:with-param name="currentactivity" select="."/>
							<xsl:with-param name="waitlabel" select="$waitlabel"/>
						</xsl:call-template>
						<xsl:for-each select="preceding-sibling::Activity">
							<xsl:call-template name="DoCheckLabel">
								<xsl:with-param name="currentactivity" select="."/>
								<xsl:with-param name="waitlabel" select="$waitlabel"/>
							</xsl:call-template>
						</xsl:for-each>
						<xsl:for-each select="following-sibling::Activity">
							<xsl:call-template name="DoCheckLabel">
								<xsl:with-param name="currentactivity" select="."/>
								<xsl:with-param name="waitlabel" select="$waitlabel"/>
							</xsl:call-template>
						</xsl:for-each>
						<xsl:variable name="outputWaitLabel"
							select="MotomanUtils:GetParameterData('OutputWaitLabel')"/>
						<NumberIncrement:counternum counter="0"/>
						<xsl:if test="$outputWaitLabel='TRUE'">
							<xsl:value-of select="$cr"/>
							<xsl:variable name="curnum" select="NumberIncrement:next()"/>
							<xsl:call-template name="lineNumber">
								<xsl:with-param name="linenum" select="$curnum"/>
							</xsl:call-template>
							<xsl:text>  </xsl:text>
							<xsl:value-of select="string($labelname)"/>
						</xsl:if>
					</xsl:if>
				</xsl:when>
				<xsl:when test="@ActivityType='DelayActivity'">
					<xsl:text>   WAIT </xsl:text>
					<xsl:variable name="start" select="ActivityTime/StartTime"/>
					<xsl:variable name="end" select="ActivityTime/EndTime"/>
					<xsl:value-of select="format-number($end - $start, $xyzPattern)"/>
					<xsl:text>(sec)</xsl:text>
				</xsl:when>
				<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
					<xsl:text>  CALL </xsl:text>
					<xsl:variable name="callname">
						<xsl:choose>
							<xsl:when test="contains(string(CallName), '.')">
								<xsl:value-of
									select="concat(substring-before(string(CallName),'.'), substring-after(string(CallName),'.'))"
								/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="CallName"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:value-of select="$callname"/>
					<!-- IR 0671018V5R21 uploader now creates a task name with arguments included....there callname already has the
						arguments...therefore don't use the arguments attribute
					<xsl:variable name="arguments"
						select="AttributeList/Attribute[AttributeName='CallArguments']/AttributeValue"/>
					<xsl:if test="string($arguments) != ''">
						<xsl:value-of select="$arguments"/>
					</xsl:if>
					-->
				</xsl:when>
				<xsl:when test="string($robotLanguage) != ''">
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData('LastLineComment','TRUE')"/>
					<!-- DLY Start 2007/04/25 take out spaces since the parser now includes everything beyond the : for robot language commands <xsl:text>  </xsl:text> -->
					<xsl:value-of select="$robotLanguage"/>
					<xsl:if test="starts-with(normalize-space($robotLanguage), 'UTOOL_NUM = ')">
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData('LASTUTOOLNUMBER',substring-before(substring-after($robotLanguage,' = '),';'))"
						/>
					</xsl:if>
					<xsl:if test="starts-with(normalize-space($robotLanguage), 'UFRAME_NUM = ')">
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData('LASTUFRAMENUMBER',substring-before(substring-after($robotLanguage,' = '),';'))"
						/>
					</xsl:if>
					<xsl:if test="starts-with(normalize-space($robotLanguage), 'UTOOL_NUM[GP')">
						<xsl:variable name="groupNum"
							select="substring-before(substring-after($robotLanguage,'[GP'),']')"/>
						<xsl:if test="$groupNum='1'">
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData('LASTUTOOLNUMBER',substring-before(substring-after($robotLanguage,'='),';'))"
							/>
						</xsl:if>
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData(concat('UTOOLGROUPNUMBER',$groupNum),substring-before(substring-after($robotLanguage,'='),';'))"
						/>
					</xsl:if>
					<xsl:if test="starts-with(normalize-space($robotLanguage), 'UFRAME_NUM[GP')">
						<xsl:variable name="groupNum"
							select="substring-before(substring-after($robotLanguage,'[GP'),']')"/>
						<xsl:if test="$groupNum='1'">
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData('LASTUFRAMENUMBER',substring-before(substring-after($robotLanguage,'='),';'))"
							/>
						</xsl:if>
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData(concat('UFRAMEGROUPNUMBER',$groupNum),substring-before(substring-after($robotLanguage,'='),';'))"
						/>
					</xsl:if>
					<xsl:value-of select="$cr"/>
				</xsl:when>
				<xsl:when test="string($comment) != ''">
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData('LastLineComment','TRUE')"/>
					<xsl:text>  !</xsl:text>
					<xsl:value-of select="$comment"/>
					<xsl:value-of select="$cr"/>
				</xsl:when>
			</xsl:choose>
			<xsl:variable name="macro2"
				select="AttributeList/Attribute[AttributeName='Macro']/AttributeValue"/>
			<xsl:if test="$macro2 != ''">
				<xsl:if
					test="@ActivityType != 'DNBEnterZoneActivity' and @ActivityType != 'DNBClearZoneActivity' ">
					<xsl:text> ;</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:variable name="curnum" select="NumberIncrement:next()"/>
					<xsl:call-template name="lineNumber">
						<xsl:with-param name="linenum" select="$curnum"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:value-of select="$macro2"/>
			</xsl:if>
			<xsl:variable name="lastlinecomment"
				select="MotomanUtils:GetParameterData('LastLineComment')"/>
			<xsl:variable name="postcomment"
				select="AttributeList/Attribute[substring(AttributeName,1,11) = 'PostComment']/AttributeValue"/>
			<xsl:if
				test="position()=last() and string($lastlinecomment) != 'TRUE' and $postcomment=''">
				<xsl:text> ;</xsl:text>
			</xsl:if>
			<xsl:if test="@ActivityType != 'Operation'">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
			</xsl:if>
			<xsl:if test="$postcomment != ''">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
				<xsl:variable name="curnum" select="NumberIncrement:current()"/>
				<xsl:text> ;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:call-template name="processComments">
					<xsl:with-param name="prefix" select="'Post'"/>
					<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
					<xsl:with-param name="linenum">
						<xsl:value-of select="$curnum"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!-- end of Activity -->
	<xsl:template match="ToolProfile" mode="codeMode">
		<xsl:param name="store" select="'true'"/>
		<xsl:variable name="toolNamePrefix" select="substring(Name,1,10)"/>
		<xsl:variable name="toolNamePrefix2" select="substring(Name,1,12)"/>
		<xsl:variable name="toolType" select="ToolType"/>
		<xsl:variable name="currentGroupNumber"
			select="MotomanUtils:GetParameterData('CURRENTGROUPNUMBER')"/>
		<xsl:variable name="groupTool">
			<xsl:if test="string($currentGroupNumber)!=''">
				<xsl:value-of
					select="MotomanUtils:GetParameterData(concat('UTOOLGROUPNUMBER',$currentGroupNumber))"
				/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="toolNumber">
			<xsl:choose>
				<xsl:when test="string($groupTool)!=''">
					<xsl:value-of select="$groupTool"/>
				</xsl:when>
				<xsl:when test="$toolNamePrefix2='UTOOL_NUM = '">
					<xsl:value-of select="substring-after(Name,'= ')"/>
				</xsl:when>
				<xsl:when test="$toolNamePrefix2='UTOOL_NUM[GP'">
					<xsl:value-of select="substring-after(Name,'=')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="count(preceding-sibling::ToolProfile)+1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- store non-stationary tool name to be used with the next Fanuc RTCP move -->
		<xsl:if test="string($toolType)!='Stationary'">
			<xsl:variable name="storeStatToolResult"
				select="MotomanUtils:SetParameterData('RTCPTOOLNAME',string(Name))"/>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$toolNumber &gt; 10">
				<xsl:call-template name="displayError">
					<xsl:with-param name="errorStr"
						select="'UTOOL NUMBER GREATER THAN 10 IS INVALID FOR FANUC TPE LANGUAGE - MAXIMUM 10 WILL BE USED'"
					/>
				</xsl:call-template>
				<xsl:choose>
					<xsl:when test="string($controlname) = 'RJ3-iB'">
						<xsl:text>A</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>10</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="string($controlname) = 'RJ3-iB' and $toolNumber = '10'">
						<xsl:text>A</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$toolNumber"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:variable name="toolName" select="concat(Name,'TOOLUSED')"/>
				<xsl:variable name="toolResult"
					select="MotomanUtils:SetParameterData($toolName, 'true')"/>
				<xsl:if test="$store='true'">
					<xsl:variable name="lastToolResult"
						select="MotomanUtils:SetParameterData('LASTUTOOLNUMBER', string($toolNumber))"
					/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="ToolProfile" mode="fileMode">
		<xsl:param name="uframeDef" select="'false'"/>
		<xsl:variable name="tlName" select="string(Name)"/>
		<xsl:if test="position()=1 and $uframeDef='false'">
			<xsl:text>[*SYSTEM*] $MNUTOOL  Storage: CMOS  Access: RW  : ARRAY[1,9] OF POSITION =</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:variable name="toolType" select="string(ToolType)"/>
		<xsl:variable name="toolName" select="concat(Name,'TOOLUSED')"/>
		<xsl:variable name="toolUsed" select="MotomanUtils:GetParameterData($toolName)"/>
		<xsl:variable name="objectType"
			select="string(MotomanUtils:GetParameterData(concat($tlName,'ObjectType')))"/>
		<xsl:variable name="objectName"
			select="string(MotomanUtils:GetParameterData(concat($tlName,'ObjectName')))"/>
		<xsl:if
			test="(string($toolUsed) = 'true' and string($toolType) = 'OnRobot') or ($uframeDef='true' and string($toolType)='Stationary') or ($uframeDef='false' and string($objectType)='Stationary')">
			<xsl:variable name="firstTool" select="MotomanUtils:GetParameterData('FIRSTTOOLDONE')"/>
			<xsl:choose>
				<xsl:when test="$objectType='Stationary' and $uframeDef='false'">
					<xsl:apply-templates
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile"
						mode="fileMode">
						<xsl:with-param name="utoolDef" select="true"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="toolDone"
						select="MotomanUtils:GetParameterData(concat($tlName,'FILEDONE'))"/>
					<xsl:if test="string($toolDone)=''">
						<xsl:text>[1,</xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlName]"
							mode="codeMode"/>
						<xsl:text>]    Group: 1   Config: F R U T, 0, 0, 0</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:variable name="toolx">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@X"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="tooly">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Y"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolz">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Z"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolr">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Roll"/>
								<xsl:with-param name="Units" select="1.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolp">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Pitch"/>
								<xsl:with-param name="Units" select="1.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolw">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Yaw"/>
								<xsl:with-param name="Units" select="1.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when
								test="string($worldCoords) != 'true' and string($toolType) != 'OnRobot'">
								<xsl:variable name="basex">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@X"/>
										<xsl:with-param name="Units" select="1000.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basey">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Y"/>
										<xsl:with-param name="Units" select="1000.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basez">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Z"/>
										<xsl:with-param name="Units" select="1000.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="baser">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Roll"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basep">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Pitch"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basew">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Yaw"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="xyzwpr"
									select="concat($basex, ',',  $basey,  ',' , $basez , ',' , $basew , ',' , $basep , ',' , $baser )"/>
								<!-->Set Base/World /!-->
								<xsl:variable name="matresult"
									select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
								<!--  <xsl:value-of select="$cr"/><xsl:value-of select="$matresult"/ !-->
								<!-- Invert to World/Base !-->
								<xsl:variable name="invresult" select="MatrixUtils:dgInvert()"/>
								<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$invresult"/ !-->
								<xsl:variable name="xyzwpr4"
									select="concat($toolx, ',',  $tooly,  ',' , $toolz , ',' , $toolw , ',' , $toolp , ',' , $toolr )"/>
								<!-- Cat Target/World !-->
								<xsl:variable name="catresult2"
									select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr4)"/>
								<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$catresult2"/ !-->
								<!-- Get Target/Base !-->
								<xsl:variable name="rxxx" select="MatrixUtils:dgGetX()"/>
								<xsl:variable name="ryyy" select="MatrixUtils:dgGetY()"/>
								<xsl:variable name="rzzz" select="MatrixUtils:dgGetZ()"/>
								<xsl:variable name="ryaw" select="MatrixUtils:dgGetYaw()"/>
								<xsl:variable name="rpitch" select="MatrixUtils:dgGetPitch()"/>
								<xsl:variable name="rroll" select="MatrixUtils:dgGetRoll()"/>
								<xsl:text>X:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rxxx,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>   Y:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($ryyy,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>   Z:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rzzz,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:value-of select="$cr"/>
								<xsl:text>W:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($ryaw,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>   P:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rpitch,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>   R:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rroll,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:value-of select="$cr"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>X:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolx,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>    Y:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($tooly,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>    Z:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolz,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:value-of select="$cr"/>
								<xsl:text>W:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolw,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>   P:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolp,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text>   R:    </xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolr,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:value-of select="$cr"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:variable name="toolResult"
							select="MotomanUtils:SetParameterData(concat($tlName,'FILEDONE'),'true')"
						/>
					</xsl:if>

				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="position()=last() and $uframeDef='false'">
			<xsl:text>[*POSREG*] $POSREG  Storage: CMOS  Access: RW  : ARRAY[1,100] OF Position Reg =</xsl:text>
			<xsl:for-each
				select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']">
				<xsl:call-template name="DoPosition">
					<xsl:with-param name="positionRegister">
						<xsl:value-of select="'true'"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:for-each>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="ToolProfile" mode="setMode">
		<xsl:param name="uframeDef" select="'false'"/>
		<xsl:variable name="tlName" select="string(Name)"/>
		<xsl:variable name="toolType" select="string(ToolType)"/>
		<xsl:variable name="toolName" select="concat(Name,'TOOLUSED')"/>
		<xsl:variable name="toolUsed" select="MotomanUtils:GetParameterData($toolName)"/>
		<xsl:variable name="objectType"
			select="MotomanUtils:GetParameterData(concat($tlName,'ObjectType'))"/>
		<xsl:variable name="objectName"
			select="MotomanUtils:GetParameterData(concat($tlName,'ObjectName'))"/>
		<xsl:if
			test="(string($toolUsed) = 'true' and string($toolType) = 'OnRobot') or ($uframeDef='true' and string($toolType)='Stationary') or ($uframeDef='false' and string($objectType)='Stationary')">
			<xsl:choose>
				<xsl:when test="$objectType='Stationary' and $uframeDef = 'false'">
					<xsl:apply-templates
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile"
						mode="setMode">
						<xsl:with-param name="utoolDef" select="'true'"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="toolDone"
						select="MotomanUtils:GetParameterData(concat($tlName,'SETDONE'))"/>
					<xsl:if test="string($toolDone)=''">
						<xsl:variable name="toolx">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@X"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="tooly">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Y"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolz">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Z"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolr">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Roll"/>
								<xsl:with-param name="Units" select="1.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolp">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Pitch"/>
								<xsl:with-param name="Units" select="1.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="toolw">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Yaw"/>
								<xsl:with-param name="Units" select="1.0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when
								test="string($worldCoords) != 'true' and string($toolType) != 'OnRobot'">
								<xsl:variable name="basex">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@X"/>
										<xsl:with-param name="Units" select="1000.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basey">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Y"/>
										<xsl:with-param name="Units" select="1000.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basez">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Z"/>
										<xsl:with-param name="Units" select="1000.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="baser">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Roll"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basep">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Pitch"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="basew">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num"
											select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Yaw"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="xyzwpr"
									select="concat($basex, ',',  $basey,  ',' , $basez , ',' , $basew , ',' , $basep , ',' , $baser )"/>
								<!-->Set Base/World /!-->
								<xsl:variable name="matresult"
									select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
								<!--  <xsl:value-of select="$cr"/><xsl:value-of select="$matresult"/ !-->
								<!-- Invert to World/Base !-->
								<xsl:variable name="invresult" select="MatrixUtils:dgInvert()"/>
								<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$invresult"/ !-->
								<xsl:variable name="xyzwpr4"
									select="concat($toolx, ',',  $tooly,  ',' , $toolz , ',' , $toolw , ',' , $toolp , ',' , $toolr )"/>
								<!-- Cat Target/World !-->
								<xsl:variable name="catresult2"
									select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr4)"/>
								<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$catresult2"/ !-->
								<!-- Get Target/Base !-->
								<xsl:variable name="rxxx" select="MatrixUtils:dgGetX()"/>
								<xsl:variable name="ryyy" select="MatrixUtils:dgGetY()"/>
								<xsl:variable name="rzzz" select="MatrixUtils:dgGetZ()"/>
								<xsl:variable name="ryaw" select="MatrixUtils:dgGetYaw()"/>
								<xsl:variable name="rpitch" select="MatrixUtils:dgGetPitch()"/>
								<xsl:variable name="rroll" select="MatrixUtils:dgGetRoll()"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,1]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rxxx,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,2]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($ryyy,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,3]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rzzz,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,4]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($ryaw,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,5]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rpitch,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,6]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($rroll,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,1]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolx,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,2]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($tooly,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,3]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolz,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,4]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolw,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,5]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolp,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:call-template name="lineNumber">
									<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
								</xsl:call-template>
								<xsl:text>  PR[1,6]=</xsl:text>
								<xsl:call-template name="posValueOutput">
									<xsl:with-param name="value"
										select="format-number($toolr,$xyzPattern)"/>
								</xsl:call-template>
								<xsl:text> ;</xsl:text>
								<xsl:value-of select="$cr"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:call-template name="lineNumber">
							<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
						</xsl:call-template>
						<xsl:choose>
							<xsl:when test="$uframeDef='false'">
								<xsl:text>  UTOOL[</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>  UFRAME[</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlName]"
							mode="codeMode"/>
						<xsl:text>]=PR[1] ;</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:variable name="toolResult"
							select="MotomanUtils:SetParameterData(concat($tlName,'SETDONE'),'true')"
						/>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template match="ObjectFrameProfile" mode="codeMode">
		<xsl:param name="toolType" select="'OnRobot'"/>
		<xsl:param name="toolName"/>
		<xsl:param name="store" select="'true'"/>
		<xsl:variable name="objNamePrefix" select="substring(Name,1,11)"/>
		<xsl:variable name="objNamePrefix2" select="substring(Name,1,13)"/>
		<xsl:variable name="currentGroupNumber"
			select="MotomanUtils:GetParameterData('CURRENTGROUPNUMBER')"/>
		<xsl:variable name="groupObject">
			<xsl:if test="string($currentGroupNumber)!=''">
				<xsl:value-of
					select="MotomanUtils:GetParameterData(concat('UFRAMEGROUPNUMBER',$currentGroupNumber))"
				/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="objectNumber">
			<xsl:choose>
				<xsl:when test="string($groupObject)!=''">
					<xsl:value-of select="$groupObject"/>
				</xsl:when>
				<xsl:when test="$objNamePrefix2='UFRAME_NUM = '">
					<xsl:value-of select="substring-after(Name,'= ')"/>
				</xsl:when>
				<xsl:when test="$objNamePrefix2='UFRAME_NUM[GP'">
					<xsl:value-of select="substring-after(Name,'=')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="count(preceding-sibling::ObjectFrameProfile)+1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$objectNumber &gt; 9">
				<xsl:call-template name="displayError">
					<xsl:with-param name="errorStr"
						select="'UFRAME NUMBER GREATER THAN 9 IS INVALID FOR FANUC TPE LANGUAGE - MAXIMUM 9 WILL BE USED'"
					/>
				</xsl:call-template>
				<xsl:text>9</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$objectNumber"/>
				<xsl:variable name="objectName" select="concat(Name,'OBJECTUSED')"/>
				<xsl:variable name="objectResult"
					select="MotomanUtils:SetParameterData($objectName,'true')"/>
				<xsl:if test="string($toolType)='Stationary'">
					<xsl:variable name="objectToolResult"
						select="MotomanUtils:SetParameterData(concat(Name,'ToolType'),string($toolType))"/>
					<xsl:variable name="objectToolNameResult"
						select="MotomanUtils:SetParameterData(concat(Name,'ToolName'),string($toolName))"/>
					<xsl:variable name="toolObjectResult"
						select="MotomanUtils:SetParameterData(concat($toolName,'ObjectType'),string($toolType))"/>
					<xsl:variable name="toolObjectNameResult"
						select="MotomanUtils:SetParameterData(concat($toolName,'ObjectName'),Name)"/>
					<xsl:if test="$store='true'">
						<xsl:variable name="lastFrameResult"
							select="MotomanUtils:SetParameterData('LASTUFRAMENUMBER', string($objectNumber))"
						/>
					</xsl:if>
					<xsl:variable name="stationaryTool"
						select="MotomanUtils:GetParameterData('RTCPTOOLNAME')"/>
					<xsl:variable name="objframex">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframey">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframez">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframer">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="ObjectFrame/ObjectFrameOrientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframep">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="ObjectFrame/ObjectFrameOrientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframew">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="ObjectFrame/ObjectFrameOrientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframeTotal"
						select="number($objframex) + number($objframey) + number($objframez) + number($objframep) + number($objframew) + number($objframer)"/>
					<xsl:variable name="toolframex">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$stationaryTool]/TCPOffset/TCPPosition/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolframey">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$stationaryTool]/TCPOffset/TCPPosition/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolframez">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$stationaryTool]/TCPOffset/TCPPosition/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolframer">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$stationaryTool]/TCPOffset/TCPOrientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolframep">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$stationaryTool]/TCPOffset/TCPOrientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolframew">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$stationaryTool]/TCPOffset/TCPOrientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolframeTotal"
						select="number($toolframex) + number($toolframey) + number($toolframez) + number($toolframep) + number($toolframew) + number($toolframer)"/>
					<!-- xsl:if
						test="math:abs(number($toolframeTotal)-number($objframeTotal)) &gt; .1">
						<xsl:variable name="errStr"
							select="concat('UFRAME ', string(Name),' FOR FIXED TCP MOVE SHOULD MATCH PREVIOUS MOVE TOOL ', string($stationaryTool),'')"/>
						<xsl:call-template name="displayError">
							<xsl:with-param name="errorStr" select="$errStr"/>
						</xsl:call-template>
					</xsl:if -->
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="ObjectFrameProfile" mode="fileMode">
		<xsl:param name="utoolDef" select="'false'"/>
		<xsl:variable name="objName" select="string(Name)"/>
		<xsl:if test="string(@Replaced) = '' and $objName != 'UFRAME_NUM = 0'">
			<xsl:variable name="firstObjectDone"
				select="MotomanUtils:GetParameterData('FIRSTOBJECTFRAMEOUTPUT')"/>
			<xsl:if test="string($firstObjectDone) != 'true'">
				<xsl:text>DATA FILE START sysvarsoutput.ls</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>[*SYSTEM*] $MNUFRAME  Storage: CMOS  Access: RW  : ARRAY[1,9] OF POSITION =</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:variable name="firstObjectResult"
					select="MotomanUtils:SetParameterData('FIRSTOBJECTFRAMEOUTPUT','true')"/>
			</xsl:if>
			<xsl:variable name="objectName" select="concat(Name,'OBJECTUSED')"/>
			<xsl:variable name="objectUsed" select="MotomanUtils:GetParameterData($objectName)"/>
			<xsl:variable name="toolType"
				select="MotomanUtils:GetParameterData(concat(Name,'ToolType'))"/>
			<xsl:variable name="toolName"
				select="MotomanUtils:GetParameterData(concat(Name,'ToolName'))"/>
			<xsl:if test="string($objectUsed) = 'true'">
				<xsl:choose>
					<xsl:when test="$toolType='Stationary' and $utoolDef='false'">
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[ToolType='Stationary']"
							mode="fileMode">
							<xsl:with-param name="uframeDef" select="'true'"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="objectDone"
							select="MotomanUtils:GetParameterData(concat($objName,'FILEDONE'))"/>
						<xsl:if test="string($objectDone)=''">
							<xsl:text>[1,</xsl:text>
							<xsl:apply-templates
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objName]"
								mode="codeMode"/>
							<xsl:if test="@ApplyOffsetToTags='On'">
								<xsl:text>:ApplyOffsetToTags</xsl:text>
							</xsl:if>
							<xsl:text>]    Group: 1   Config: F R U T, 0, 0, 0</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:variable name="objframex">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFramePosition/@X"/>
									<xsl:with-param name="Units" select="1000.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframey">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFramePosition/@Y"/>
									<xsl:with-param name="Units" select="1000.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframez">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFramePosition/@Z"/>
									<xsl:with-param name="Units" select="1000.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframer">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFrameOrientation/@Roll"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframep">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFrameOrientation/@Pitch"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframew">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFrameOrientation/@Yaw"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframeTotal"
								select="number($objframex) + number($objframey) + number($objframez) + number($objframep) + number($objframew) + number($objframer)"/>
							<xsl:choose>
								<xsl:when
									test="string($worldCoords) !='true' and math:abs($objframeTotal) &gt; .001 and $utoolDef='false'">
									<xsl:variable name="basex">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@X"/>
											<xsl:with-param name="Units" select="1000.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basey">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Y"/>
											<xsl:with-param name="Units" select="1000.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basez">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Z"/>
											<xsl:with-param name="Units" select="1000.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="baser">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Roll"/>
											<xsl:with-param name="Units" select="1.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basep">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Pitch"/>
											<xsl:with-param name="Units" select="1.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basew">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Yaw"/>
											<xsl:with-param name="Units" select="1.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="xyzwpr"
										select="concat($basex, ',',  $basey,  ',' , $basez , ',' , $basew , ',' , $basep , ',' , $baser )"/>
									<!-->Set Base/World /!-->
									<xsl:variable name="matresult"
										select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
									<!--  <xsl:value-of select="$cr"/><xsl:value-of select="$matresult"/ !-->
									<!-- Invert to World/Base !-->
									<xsl:variable name="invresult" select="MatrixUtils:dgInvert()"/>
									<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$invresult"/ !-->
									<xsl:variable name="xyzwpr4"
										select="concat($objframex, ',',  $objframey,  ',' , $objframez , ',' , $objframew , ',' , $objframep , ',' , $objframer )"/>
									<!-- Cat Target/World !-->
									<xsl:variable name="catresult2"
										select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr4)"/>
									<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$catresult2"/ !-->
									<!-- Get Target/Base !-->
									<xsl:variable name="rxxx" select="MatrixUtils:dgGetX()"/>
									<xsl:variable name="ryyy" select="MatrixUtils:dgGetY()"/>
									<xsl:variable name="rzzz" select="MatrixUtils:dgGetZ()"/>
									<xsl:variable name="ryaw" select="MatrixUtils:dgGetYaw()"/>
									<xsl:variable name="rpitch" select="MatrixUtils:dgGetPitch()"/>
									<xsl:variable name="rroll" select="MatrixUtils:dgGetRoll()"/>
									<xsl:text>X:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rxxx,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   Y:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($ryyy,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   Z:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rzzz,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:value-of select="$cr"/>
									<xsl:text>W:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($ryaw,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   P:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rpitch,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   R:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rroll,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:value-of select="$cr"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>X:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframex,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   Y:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframey,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   Z:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframez,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:value-of select="$cr"/>
									<xsl:text>W:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframew,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   P:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframep,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text>   R:    </xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframer,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:value-of select="$cr"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:variable name="objectResult"
								select="MotomanUtils:SetParameterData(concat($objName,'FILEDONE'),'true')"
							/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template match="ObjectFrameProfile" mode="setMode">
		<xsl:param name="utoolDef" select="'false'"/>
		<xsl:variable name="objName" select="string(Name)"/>
		<xsl:if test="string(@Replaced) = '' and $objName != 'UFRAME_NUM = 0'">
			<xsl:variable name="objectName" select="concat(Name,'OBJECTUSED')"/>
			<xsl:variable name="objectUsed" select="MotomanUtils:GetParameterData($objectName)"/>
			<xsl:variable name="toolType"
				select="MotomanUtils:GetParameterData(concat($objName,'ToolType'))"/>
			<xsl:variable name="toolName"
				select="MotomanUtils:GetParameterData(concat($objName,'ToolName'))"/>
			<xsl:if test="$objectUsed='true'">
				<xsl:choose>
					<xsl:when test="$toolType='Stationary' and $utoolDef = 'false'">
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[ToolType='Stationary']"
							mode="setMode">
							<xsl:with-param name="uframeDef" select="'true'"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="objectDone"
							select="MotomanUtils:GetParameterData(concat($objName,'SETDONE'))"/>
						<xsl:if test="string($objectDone)=''">
							<xsl:variable name="objframex">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFramePosition/@X"/>
									<xsl:with-param name="Units" select="1000.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframey">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFramePosition/@Y"/>
									<xsl:with-param name="Units" select="1000.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframez">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFramePosition/@Z"/>
									<xsl:with-param name="Units" select="1000.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframer">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFrameOrientation/@Roll"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframep">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFrameOrientation/@Pitch"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframew">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num"
										select="ObjectFrame/ObjectFrameOrientation/@Yaw"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="objframeTotal"
								select="number($objframex) + number($objframey) + number($objframez) + number($objframep) + number($objframew) + number($objframer)"/>
							<xsl:choose>
								<xsl:when
									test="string($worldCoords) !='true' and math:abs($objframeTotal) &gt; .001 and $utoolDef = 'false'">
									<xsl:variable name="basex">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@X"/>
											<xsl:with-param name="Units" select="1000.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basey">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Y"/>
											<xsl:with-param name="Units" select="1000.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basez">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Position/@Z"/>
											<xsl:with-param name="Units" select="1000.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="baser">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Roll"/>
											<xsl:with-param name="Units" select="1.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basep">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Pitch"/>
											<xsl:with-param name="Units" select="1.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="basew">
										<xsl:call-template name="Scientific">
											<xsl:with-param name="Num"
												select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']/Target/BaseWRTWorld/Orientation/@Yaw"/>
											<xsl:with-param name="Units" select="1.0"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="xyzwpr"
										select="concat($basex, ',',  $basey,  ',' , $basez , ',' , $basew , ',' , $basep , ',' , $baser )"/>
									<!-->Set Base/World /!-->
									<xsl:variable name="matresult"
										select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
									<!--  <xsl:value-of select="$cr"/><xsl:value-of select="$matresult"/ !-->
									<!-- Invert to World/Base !-->
									<xsl:variable name="invresult" select="MatrixUtils:dgInvert()"/>
									<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$invresult"/ !-->
									<xsl:variable name="xyzwpr4"
										select="concat($objframex, ',',  $objframey,  ',' , $objframez , ',' , $objframew , ',' , $objframep , ',' , $objframer )"/>
									<!-- Cat Target/World !-->
									<xsl:variable name="catresult2"
										select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr4)"/>
									<!-- <xsl:value-of select="$cr"/><xsl:value-of select="$catresult2"/ !-->
									<!-- Get Target/Base !-->
									<xsl:variable name="rxxx" select="MatrixUtils:dgGetX()"/>
									<xsl:variable name="ryyy" select="MatrixUtils:dgGetY()"/>
									<xsl:variable name="rzzz" select="MatrixUtils:dgGetZ()"/>
									<xsl:variable name="ryaw" select="MatrixUtils:dgGetYaw()"/>
									<xsl:variable name="rpitch" select="MatrixUtils:dgGetPitch()"/>
									<xsl:variable name="rroll" select="MatrixUtils:dgGetRoll()"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,1]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rxxx,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,2]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($ryyy,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,3]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rzzz,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,4]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($ryaw,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,5]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rpitch,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,6]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($rroll,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,1]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframex,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,2]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframey,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,3]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframez,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,4]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframew,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,5]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframep,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:call-template name="lineNumber">
										<xsl:with-param name="linenum"
											select="NumberIncrement:next()"/>
									</xsl:call-template>
									<xsl:text>  PR[1,6]=</xsl:text>
									<xsl:call-template name="posValueOutput">
										<xsl:with-param name="value"
											select="format-number($objframer,$xyzPattern)"/>
									</xsl:call-template>
									<xsl:text> ;</xsl:text>
									<xsl:value-of select="$cr"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:call-template name="lineNumber">
								<xsl:with-param name="linenum" select="NumberIncrement:next()"/>
							</xsl:call-template>
							<xsl:choose>
								<xsl:when test="$utoolDef='false'">
									<xsl:text>  UFRAME[</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>  UTOOL[</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:apply-templates
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objName]"
								mode="codeMode"/>
							<xsl:text>]=PR[1] ;</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:variable name="objectResult"
								select="MotomanUtils:SetParameterData(concat($objName,'SETDONE'),'true')"
							/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template name="Bothmain">
		<xsl:param name="axesletter"/>
		<xsl:param name="axesnum"/>
		<xsl:for-each select="Target/JointTarget/AuxJoint/JointValue">
			<xsl:variable name="jointOutput">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="."/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="position() = 1">
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>  </xsl:text>
				<xsl:value-of select="$axesletter"/>
				<xsl:value-of select="$axesnum"/>
				<xsl:text> =    </xsl:text>
			</xsl:if>
			<xsl:if test="position() != 1">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="$axesletter"/>
				<xsl:value-of select="$axesnum + position() - 1"/>
				<xsl:text> = </xsl:text>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="parent::node()/@Units = 'deg' ">
					<xsl:call-template name="DoRotationalJoint">
						<xsl:with-param name="axesType">
							<xsl:value-of select="parent::node()/@Type"/>
						</xsl:with-param>
						<xsl:with-param name="jointValue">
							<xsl:value-of select="$jointOutput"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="posValueOutput">
						<xsl:with-param name="value"
							select="format-number($jointOutput * 1000,$xyzPattern)"/>
					</xsl:call-template>
					<xsl:text> mm</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="Main">
		<xsl:param name="axesletter"/>
		<xsl:param name="axesnum"/>
		<xsl:param name="axestype"/>
		<xsl:for-each select="Target/JointTarget/AuxJoint[@Type = $axestype]//JointValue">
			<xsl:variable name="jointOutput">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="."/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="position() = 1">
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>  </xsl:text>
				<xsl:value-of select="$axesletter"/>
				<xsl:value-of select="$axesnum"/>
				<xsl:text> =    </xsl:text>
			</xsl:if>
			<xsl:if test="position() != 1">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="$axesletter"/>
				<xsl:value-of select="$axesnum + position() - 1"/>
				<xsl:text> = </xsl:text>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="parent::node()/@Units = 'deg' ">
					<xsl:call-template name="DoRotationalJoint">
						<xsl:with-param name="axesType">
							<xsl:value-of select="$axestype"/>
						</xsl:with-param>
						<xsl:with-param name="jointValue">
							<xsl:value-of select="$jointOutput"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="posValueOutput">
						<xsl:with-param name="value"
							select="format-number($jointOutput * 1000,$xyzPattern)"/>
					</xsl:call-template>
					<xsl:text> mm</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="Group">
		<xsl:param name="groupnum"/>
		<xsl:param name="axestype"/>
		<xsl:param name="objnam"/>
		<xsl:param name="tlnam"/>
		<xsl:param name="positionRegister"/>
		<xsl:param name="regval"/>

		<xsl:variable name="tltype"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]/ToolType"/>

		<xsl:for-each select="Target/JointTarget/AuxJoint[@Type = $axestype]/JointValue">
			<xsl:variable name="jointOutput">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="."/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="position() = 1">
				<xsl:call-template name="GroupOutput">
					<xsl:with-param name="groupnum">
						<xsl:value-of select="$groupnum"/>
					</xsl:with-param>
					<xsl:with-param name="axestype">
						<xsl:value-of select="$axestype"/>
					</xsl:with-param>
					<xsl:with-param name="positionRegister" select="$positionRegister"/>
					<xsl:with-param name="regval" select="$regval"/>
				</xsl:call-template>
				<xsl:value-of select="$cr"/>
				<xsl:choose>
					<xsl:when test="$tltype = 'Stationary'">
						<xsl:text>  UF : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]"
							mode="codeMode"/>
						<xsl:text>, UT : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objnam]"
							mode="codeMode"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>  UF : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objnam]"
							mode="codeMode"/>
						<xsl:text>, UT : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]"
							mode="codeMode"/>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:text>,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>  J1= </xsl:text>
			</xsl:if>
			<xsl:if test="position() != 1">
				<xsl:if test="position() = 4">
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<xsl:text>  J</xsl:text>
				<xsl:value-of select="position()"/>
				<xsl:text>= </xsl:text>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="parent::node()/@Units = 'deg'">
					<!--
<xsl:value-of select="$cr"/>
<xsl:text>RotationalJoint</xsl:text>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$axestype"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$jointOutput"/>
-->
					<xsl:call-template name="DoRotationalJoint">
						<xsl:with-param name="axesType">
							<xsl:value-of select="$axestype"/>
						</xsl:with-param>
						<xsl:with-param name="jointValue">
							<xsl:value-of select="$jointOutput"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="posValueOutput">
						<xsl:with-param name="value"
							select="format-number($jointOutput * 1000,$xyzPattern)"/>
					</xsl:call-template>
					<xsl:text> mm</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="Bothgroup">
		<xsl:param name="groupnum"/>
		<xsl:param name="objnam"/>
		<xsl:param name="tlnam"/>
		<xsl:param name="positionRegister"/>
		<xsl:param name="regval"/>


		<xsl:variable name="tltype"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]/ToolType"/>

		<xsl:for-each select="Target/JointTarget/AuxJoint/JointValue">
			<xsl:variable name="jointOutput">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="."/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="position() = 1">
				<xsl:call-template name="GroupOutput">
					<xsl:with-param name="groupnum">
						<xsl:value-of select="$groupnum"/>
					</xsl:with-param>
					<xsl:with-param name="axestype">
						<xsl:value-of select="'EndOfArmTooling'"/>
					</xsl:with-param>
					<xsl:with-param name="positionRegister" select="$positionRegister"/>
					<xsl:with-param name="regval" select="$regval"/>
				</xsl:call-template>
				<xsl:value-of select="$cr"/>

				<xsl:choose>
					<xsl:when test="$tltype = 'Stationary'">
						<xsl:text>  UF : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]"
							mode="codeMode"/>
						<xsl:text>, UT : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objnam]"
							mode="codeMode"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>  UF : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objnam]"
							mode="codeMode"/>
						<xsl:text>, UT : </xsl:text>
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]"
							mode="codeMode"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>  J1= </xsl:text>
			</xsl:if>
			<xsl:if test="position() != 1">
				<xsl:if test="position() = 4">
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<xsl:text>  J</xsl:text>
				<xsl:value-of select="position()"/>
				<xsl:text>= </xsl:text>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="parent::node()/@Units = 'deg' ">
					<xsl:call-template name="posValueOutput">
						<xsl:with-param name="value"
							select="format-number($jointOutput,$xyzPattern)"/>
					</xsl:call-template>
					<xsl:text> deg</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="posValueOutput">
						<xsl:with-param name="value"
							select="format-number($jointOutput * 1000,$xyzPattern)"/>
					</xsl:call-template>
					<xsl:text> mm</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="GroupOutput">
		<xsl:param name="groupnum"/>
		<xsl:param name="axestype"/>
		<xsl:param name="positionRegister"/>
		<xsl:param name="regval"/>
		<xsl:value-of select="$cr"/>
		<xsl:choose>
			<xsl:when test="$positionRegister='true'">
				<xsl:text>    [</xsl:text>
				<xsl:choose>
					<xsl:when test="$groupnum != ''">
						<xsl:value-of select="$groupnum"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$axestype='Main'">
								<xsl:text>1</xsl:text>
							</xsl:when>
							<xsl:when test="$axestype='RailTrackGantry'">
								<xsl:text>1</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>2</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$regval"/>
				<xsl:text>] =   ''   Group: </xsl:text>
				<xsl:choose>
					<xsl:when test="$groupnum != ''">
						<xsl:value-of select="$groupnum"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$axestype='Main'">
								<xsl:text>1</xsl:text>
							</xsl:when>
							<xsl:when test="$axestype='RailTrackGantry'">
								<xsl:text>1</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>2</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>   GP</xsl:text>
				<xsl:choose>
					<xsl:when test="$groupnum != ''">
						<xsl:value-of select="$groupnum"/>
						<xsl:variable name="result"
							select="MotomanUtils:SetParameterData('CURRENTGROUPNUMBER', $groupnum)"
						/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$axestype='Main'">
								<xsl:text>1</xsl:text>
							</xsl:when>
							<xsl:when test="$axestype='RailTrackGantry'">
								<xsl:text>1</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>2</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>:</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="GroupValue">
		<xsl:param name="groupnum"/>
		<xsl:param name="groupbits"/>
		<xsl:variable name="bitandresult"
			select="BitMaskUtils:bitAND(number($groupbits),  number($groupnum))"/>
		<xsl:choose>
			<xsl:when test="number($bitandresult) = number($groupnum)">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>*</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="number($groupnum) != 16">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:if test="number($groupnum) = 16">
			<xsl:text>;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template name="mapio">
		<xsl:param name="iotype"/>
		<xsl:param name="iovalue"/>
		<xsl:param name="comment"/>
		<xsl:param name="ioname"/>
		<xsl:choose>
			<xsl:when test="$iotype = 'O'">
				<xsl:text>  </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>  WAIT </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when
				test="(number($maxweld) &gt; 0 and number($iovalue) &gt; number($maxweld))">
				<xsl:text>G</xsl:text>
			</xsl:when>
			<xsl:when
				test="(number($maxrobot) &gt; 0 and number($iovalue) &gt; number($maxrobot))">
				<xsl:text>W</xsl:text>
			</xsl:when>
			<xsl:when
				test="(number($maxdigital) &gt; 0 and number($iovalue) &gt; number($maxdigital))">
				<xsl:text>R</xsl:text>
			</xsl:when>
			<xsl:when
				test="(contains($ioname,concat('G',$iotype,'[')) or contains($iovalue,concat('G',$iotype,'[')))">
				<xsl:text>G</xsl:text>
			</xsl:when>
			<xsl:when
				test="(contains($ioname,concat('W',$iotype,'[')) or contains($iovalue,concat('W',$iotype,'[')))">
				<xsl:text>W</xsl:text>
			</xsl:when>
			<xsl:when
				test="(contains($ioname,concat('R',$iotype,'[')) or contains($iovalue,concat('R',$iotype,'[')))">
				<xsl:text>R</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>D</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$iotype"/>
		<xsl:text>[</xsl:text>
		<xsl:choose>
			<xsl:when
				test="(number($maxweld) &gt; 0) and (number($iovalue) &gt; number($maxweld))">
				<xsl:value-of select="number($iovalue)-number($maxweld)"/>
				<xsl:if
					test="string($comment) != '' and string($comment) != concat('G', $iotype, number($iovalue)-number($maxweld))">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$comment"/>
				</xsl:if>
			</xsl:when>
			<xsl:when
				test="(number($maxrobot) &gt; 0) and (number($iovalue) &gt; number($maxrobot))">
				<xsl:value-of select="number($iovalue)-number($maxrobot)"/>
				<xsl:if
					test="string($comment) != '' and string($comment) != concat('W', $iotype, number($iovalue)-number($maxrobot))">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$comment"/>
				</xsl:if>
			</xsl:when>
			<xsl:when
				test="(number($maxdigital) &gt; 0) and (number($iovalue) &gt; number($maxdigital))">
				<xsl:value-of select="number($iovalue)-number($maxdigital)"/>
				<xsl:if
					test="string($comment) != '' and string($comment) != concat('R', $iotype, number($iovalue)-number($maxdigital))">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$comment"/>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$iovalue='undefined' or $iovalue='NaN'">
				<xsl:variable name="localPort"
					select="substring-before(substring-after($ioname,'['),']')"/>
				<xsl:choose>
					<xsl:when test="string($localPort) != '' and number($localPort) != 'NaN'">
						<xsl:value-of select="$localPort"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="number($iovalue)"/>
						<xsl:if
							test="string($comment) != '' and string($comment) != concat('D', $iotype, $iovalue)">
							<xsl:text>:</xsl:text>
							<xsl:value-of select="$comment"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="number($iovalue)"/>
				<xsl:if
					test="string($comment) != '' and string($comment) != concat('D', $iotype, $iovalue)">
					<xsl:text>:</xsl:text>
					<xsl:value-of select="$comment"/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="DoRotationalJoint">
		<xsl:param name="axesType"/>
		<xsl:param name="jointValue"/>
		<xsl:variable name="tipDistanceOffset"
			select="/OLPData/Resource/ParameterList/Parameter[ParameterName='TipDistanceOffset']/ParameterValue"/>
		<xsl:variable name="tipDistanceScale"
			select="/OLPData/Resource/ParameterList/Parameter[ParameterName='TipDistanceScale']/ParameterValue"/>
		<xsl:variable name="arm1Length"
			select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Arm1Length']/ParameterValue"/>
		<xsl:variable name="arm2Length"
			select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Arm2Length']/ParameterValue"/>
		<xsl:choose>
			<xsl:when
				test="$axesType='EndOfArmTooling' and number($arm1Length) &gt; 0.0 and number($arm2Length) &gt; 0.0 and (number($tipDistanceOffset) != 0.0 or number($tipDistanceScale) != 1.0)">
				<xsl:variable name="sassJointValue"
					select="TrigUtils:SASS(number($arm1Length),number($jointValue),number($arm2Length))"/>
				<xsl:variable name="scaledsassJointValue"
					select="number($tipDistanceOffset) + number($sassJointValue)*number($tipDistanceScale)"/>
				<xsl:call-template name="posValueOutput">
					<xsl:with-param name="value"
						select="format-number(number($scaledsassJointValue),$xyzPattern)"/>
				</xsl:call-template>
				<xsl:text> mm</xsl:text>
			</xsl:when>
			<xsl:when
				test="$axesType='EndOfArmTooling' and number($arm1Length) &gt; 0.0 and number($arm2Length) &gt; 0.0">
				<xsl:variable name="sassJointValue"
					select="TrigUtils:SASS(number($arm1Length),number($jointValue),number($arm2Length))"/>
				<xsl:call-template name="posValueOutput">
					<xsl:with-param name="value"
						select="format-number(number($sassJointValue),$xyzPattern)"/>
				</xsl:call-template>
				<xsl:text>  mm</xsl:text>
			</xsl:when>
			<xsl:when
				test="(number($tipDistanceOffset) != NaN and number($tipDistanceOffset) != 0.0) or (number($tipDistanceScale) != NaN and number($tipDistanceScale) != 1.0)">
				<xsl:variable name="scaledJointValue"
					select="number($tipDistanceOffset) + number($jointValue)*number($tipDistanceScale)"/>
				<xsl:call-template name="posValueOutput">
					<xsl:with-param name="value"
						select="format-number(number($scaledJointValue),$xyzPattern)"/>
				</xsl:call-template>
				<xsl:text> mm</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="posValueOutput">
					<xsl:with-param name="value" select="format-number($jointValue,$xyzPattern)"/>
				</xsl:call-template>
				<xsl:text> deg</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--~~~~~FOLLOW PATH ACTIVITY MULTI-PURPOSE TEMPLATE~~~~~-->
	<xsl:template name="TransformFollowPathActivityData">
		<!-- Nodeset that contains all the descendants of follow path activity element -->
		<xsl:param name="followPathActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveAlongStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveAlongStatement' "> 
		element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$followPathActivityNode/@id"/>
		<!-- Name of the part that contains geometric feature along which the path is defined -->
		<xsl:variable name="pathOnPart" select="$followPathActivityNode/PathOnPart"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile"
			select="$followPathActivityNode/PathMotionAttributes/PathMotionProfile"/>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile"
			select="$followPathActivityNode/PathMotionAttributes/PathAccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile"
			select="$followPathActivityNode/PathMotionAttributes/PathToolProfile"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<xsl:variable name="objectFrameProfile"
			select="$followPathActivityNode/PathMotionAttributes/PathObjectFrameProfile"/>
		<!-- Motion profile values used by this activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speedValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<xsl:variable name="maxSpeedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Accuracy profile values used by this activity -->
		<!-- Variable that determines whether rounding is turned on or off. Possible values: 'On' or 'Off' -->
		<xsl:variable name="flyByMode"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/FlyByMode"/>
		<!-- Rounding criterium: 'Speed' or 'Distance' -->
		<xsl:variable name="accuracyType"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyType"/>
		<!-- Rounding precision value -->
		<xsl:variable name="accuracyValue"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyValue/@Value"/>
		<!-- Tool profile values used by this activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType"
			select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX"
			select="number(/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X)"/>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY"
			select="number(/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y)"/>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ"
			select="number(/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z)"/>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw"
			select="number(/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw)"/>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch"
			select="number(/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch)"/>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll"
			select="number(/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll)"/>
		<!-- ObjectFrame profile values used by this activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame"
			select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<xsl:variable name="ofPositionX"
			select="number(/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X)"/>
		<!-- Object frame's location offset in Y direction -->
		<xsl:variable name="ofPositionY"
			select="number(/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y)"/>
		<!-- Object frame's location offset in Z direction -->
		<xsl:variable name="ofPositionZ"
			select="number(/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z)"/>
		<!-- Object frame's orientation offset in Yaw direction -->
		<xsl:variable name="ofOrientationYaw"
			select="number(/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw)"/>
		<!-- Object frame's orientation offset in Pitch direction -->
		<xsl:variable name="ofOrientationPitch"
			select="number(/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch)"/>
		<!-- Object frame's orientation offset in Roll direction -->
		<xsl:variable name="ofOrientationRoll"
			select="number(/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll)"/>
		<!-- Motion type of this follow path activity: Always 'Linear' -->
		<xsl:variable name="motionType"
			select="$followPathActivityNode/PathMotionAttributes/PathMotionType"/>
		<xsl:variable name="orientMode"
			select="$followPathActivityNode/PathMotionAttributes/PathOrientMode"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this path -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basex"
			select="number($followPathActivityNode/BaseWRTWorld/Position/@X)*1000.0"/>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basey"
			select="number($followPathActivityNode/BaseWRTWorld/Position/@Y)*1000.0"/>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basez"
			select="number($followPathActivityNode/BaseWRTWorld/Position/@Z)*1000.0"/>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="basew"
			select="number($followPathActivityNode/BaseWRTWorld/Orientation/@Yaw)"/>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="basep"
			select="number($followPathActivityNode/BaseWRTWorld/Orientation/@Pitch)"/>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baser"
			select="number($followPathActivityNode/BaseWRTWorld/Orientation/@Roll)"/>
		<!-- Configuration string name. Config string is the same for all the nodes in the path. -->
		<xsl:variable name="configname" select="$followPathActivityNode/PathConfig/@Name"/>
		<!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models.
			Turn numbers do not change between the nodes in the path.  -->
		<!-- If turn numbers have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn number value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turn1"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 1]/@Value"/>
		<!-- Turn number value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turn4"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 4]/@Value"/>
		<!-- Turn number value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnNumber5"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 5]/@Value"/>
		<!-- Turn number value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turn6"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 6]/@Value"/>
		<!-- Get all  turn signs. If the robot uses turn signs, there will be 4 joints that will always have turn signs enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does for all robot models.
			Turn signs do not change between the nodes in the path.  -->
		<!-- If turn signs have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn sign output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn sign value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turnSign1"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 1]/@Value"/>
		<!-- Turn sign value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turnSign4"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 4]/@Value"/>
		<!-- Turn sign value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnSign5"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 5]/@Value"/>
		<!-- Turn sign value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turnSign6"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 6]/@Value"/>
		<!-- Get node numbers, positions and orientations -->
		<xsl:if test="count($followPathActivityNode/PathNodeList/Node) > 0">
			<!-- For each node in the path, get -->
			<xsl:for-each select="$followPathActivityNode/PathNodeList/Node">
				<!-- Get node number -->
				<xsl:variable name="nodeNumber" select="@Number"/>
				<!-- Node position and orientation values. Node offset is always defined with respect to workcell's World coordinate system -->
				<!-- Node location offset in X direction -->
				<xsl:variable name="xxx"
					select="number($followPathActivityNode/PathNodeList/Node/NodePosition/@X)"/>
				<!-- Node location offset in Y direction -->
				<xsl:variable name="yyy"
					select="number($followPathActivityNode/PathNodeList/Node/NodePosition/@Y)"/>
				<!-- Node location offset in Z direction -->
				<xsl:variable name="zzz"
					select="number($followPathActivityNode/PathNodeList/Node/NodePosition/@Z)"/>
				<!-- Node orientation offset in Yaw direction -->
				<xsl:variable name="yaw"
					select="number($followPathActivityNode/PathNodeList/Node/NodeOrientation/@Yaw)"/>
				<!-- Node orientation offset in Pitch direction -->
				<xsl:variable name="pitch"
					select="number($followPathActivityNode/PathNodeList/Node/NodeOrientation/@Pitch)"/>
				<!-- Node orientation offset in Roll direction -->
				<xsl:variable name="roll"
					select="number($followPathActivityNode/PathNodeList/Node/NodeOrientation/@Roll)"/>
				<!-- Creation of this node target declaration -->
				<xsl:if test=" $mode = 'TargetDeclarationSection' ">
					<!-- Create node target declaration output -->
					<xsl:variable name="posnum" select="NumberIncrement:next()"/>
					<xsl:value-of select="$cr"/>
					<xsl:text>P[</xsl:text>
					<xsl:value-of select="$posnum"/>
					<xsl:variable name="motop"
						select="AttributeList/Attribute[AttributeName='Motion Option']/AttributeValue"/>
					<xsl:variable name="toolName" select="MotionAttributes/ToolProfile"/>
					<xsl:variable name="objectName" select="MotionAttributes/ObjectFrameProfile"/>
					<xsl:variable name="charconfig1" select="substring($configname,1,1)"/>
					<xsl:variable name="charconfig2" select="substring($configname,2,1)"/>
					<xsl:variable name="charconfig3" select="substring($configname,3,1)"/>
					<xsl:variable name="targetunits" select="1000.0"/>
					<xsl:variable name="objframeunits" select="1000.0"/>
					<xsl:variable name="toolunits" select="1000.0"/>
					<xsl:variable name="rottargetunits" select="1.0"/>
					<xsl:variable name="rotobjframeunits" select="1.0"/>
					<xsl:variable name="rottoolunits" select="1.0"/>
					<xsl:variable name="objframex">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFramePosition/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframey">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFramePosition/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframez">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFramePosition/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframer">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframep">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframew">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objframeTotal"
						select="number($objframex) + number($objframey) + number($objframez) + number($objframep) + number($objframew) + number($objframer)"/>
					<xsl:variable name="toolx">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPPosition/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tooly">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPPosition/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolz">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPPosition/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolr">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPOrientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolp">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPOrientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="toolw">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset/TCPOrientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tooltype"
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/ToolType"/>
					<xsl:text>]{</xsl:text>
					<xsl:call-template name="GroupOutput">
						<xsl:with-param name="groupnum">
							<xsl:value-of select="$maingroup"/>
						</xsl:with-param>
						<xsl:with-param name="axestype">
							<xsl:value-of select="'Main'"/>
						</xsl:with-param>
						<xsl:with-param name="positionRegister" select="'false'"/>
					</xsl:call-template>
					<xsl:value-of select="$cr"/>
					<xsl:text>  UF : </xsl:text>
					<xsl:apply-templates
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"
						mode="codeMode"/>
					<xsl:text>, UT : </xsl:text>
					<xsl:apply-templates
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]"
						mode="codeMode"/>
					<xsl:text>,           CONFIG : '</xsl:text>
					<xsl:if test="$charconfig1 != ''">
						<xsl:value-of select="$charconfig1"/>
						<xsl:text> </xsl:text>
					</xsl:if>
					<xsl:if test="$charconfig2 != ''">
						<xsl:value-of select="$charconfig2"/>
						<xsl:text> </xsl:text>
					</xsl:if>
					<xsl:if test="$charconfig3 != ''">
						<xsl:value-of select="$charconfig3"/>
					</xsl:if>
					<xsl:choose>
						<xsl:when
							test="/OLPData/Resource/ParameterList/Parameter[ParameterName='Turn1']/ParameterValue = 'false'"/>
						<xsl:otherwise>
							<xsl:text>, </xsl:text>
							<xsl:value-of select="$turn1"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>, </xsl:text>
					<xsl:value-of select="$turn4"/>
					<xsl:if
						test="/OLPData/Resource/ParameterList/Parameter[ParameterName='Turn1']/ParameterValue = 'false'">
						<xsl:text>, </xsl:text>
					</xsl:if>
					<xsl:text>, </xsl:text>
					<xsl:value-of select="$turn6"/>
					<xsl:text>',</xsl:text>
					<xsl:choose>
						<xsl:when test="$tooltype != 'OnRobot'">
							<xsl:variable name="xyzwpr"
								select="concat($xxx, ',',  $yyy,  ',' , $zzz , ',' , $yaw , ',' , $pitch , ',' , $roll )"/>
							<xsl:variable name="matresult"
								select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
							<xsl:variable name="invresult" select="MatrixUtils:dgInvert()"/>
							<xsl:variable name="oxxx" select="MatrixUtils:dgGetX()"/>
							<xsl:variable name="oyyy" select="MatrixUtils:dgGetY()"/>
							<xsl:variable name="ozzz" select="MatrixUtils:dgGetZ()"/>
							<xsl:variable name="oyaw" select="MatrixUtils:dgGetYaw()"/>
							<xsl:variable name="opitch" select="MatrixUtils:dgGetPitch()"/>
							<xsl:variable name="oroll" select="MatrixUtils:dgGetRoll()"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>  X =  </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($oxxx,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text>  mm,	Y = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($oyyy,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text>  mm,	Z = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($ozzz,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text>  mm,</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>  W =  </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($oyaw,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text> deg,	P = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($opitch,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text> deg,	R = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($oroll,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text> deg</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<!-- 2009/04/30 - take out concatenation to get target with respect to base...>
								this should not be needed since the UFRAME is converted to be 
								with respect to the base  TARGET/UFRAME_NUM * UFRAME_NUM/BASE-->
							<!-- output Target/ObjectFrame or Base -->
							<xsl:value-of select="$cr"/>
							<xsl:text>  X =  </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($xxx,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text>  mm,	Y = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($yyy,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text>  mm,	Z = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($zzz,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text>  mm,</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>  W =  </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($yaw,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text> deg,	P = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($pitch,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text> deg,	R = </xsl:text>
							<xsl:call-template name="posValueOutput">
								<xsl:with-param name="value"
									select="format-number($roll,$xyzPattern)"/>
							</xsl:call-template>
							<xsl:text> deg</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if
						test="$railgroup = $maingroup and $toolgroup = $maingroup and $workgroup = $maingroup and $railgroup != ''">
						<xsl:call-template name="Bothmain">
							<xsl:with-param name="axesletter">
								<xsl:value-of select="'E'"/>
							</xsl:with-param>
							<xsl:with-param name="axesnum">
								<xsl:value-of select="1"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$railgroup = $maingroup">
						<xsl:call-template name="Main">
							<xsl:with-param name="axesletter">
								<xsl:value-of select="'E'"/>
							</xsl:with-param>
							<xsl:with-param name="axesnum">
								<xsl:value-of select="1"/>
							</xsl:with-param>
							<xsl:with-param name="axestype">
								<xsl:value-of select="'RailTrackGantry'"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$toolgroup = $maingroup">
						<xsl:call-template name="Main">
							<xsl:with-param name="axesletter">
								<xsl:value-of select="'E'"/>
							</xsl:with-param>
							<xsl:with-param name="axesnum">
								<xsl:value-of select="1"/>
							</xsl:with-param>
							<xsl:with-param name="axestype">
								<xsl:value-of select="'EndOfArmTooling'"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$workgroup = $maingroup">
						<xsl:call-template name="Main">
							<xsl:with-param name="axesletter">
								<xsl:value-of select="'E'"/>
							</xsl:with-param>
							<xsl:with-param name="axesnum">
								<xsl:value-of select="1"/>
							</xsl:with-param>
							<xsl:with-param name="axestype">
								<xsl:value-of select="'WorkpiecePositioner'"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$toolgroup != '' or $railgroup != '' or $workgroup != ''"/>
						<xsl:otherwise>
							<xsl:call-template name="Main">
								<xsl:with-param name="axesletter">
									<xsl:value-of select="'E'"/>
								</xsl:with-param>
								<xsl:with-param name="axesnum">
									<xsl:value-of select="1"/>
								</xsl:with-param>
								<xsl:with-param name="axestype">
									<xsl:value-of select="'RailTrackGantry'"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if
						test="$railgroup != $maingroup and  $railgroup = $toolgroup and $railgroup = $workgroup and $railgroup != '' and $toolgroup != ''">
						<xsl:call-template name="Bothgroup">
							<xsl:with-param name="groupnum">
								<xsl:value-of select="$railgroup"/>
							</xsl:with-param>
							<xsl:with-param name="objnam">
								<xsl:value-of select="$objectName"/>
							</xsl:with-param>
							<xsl:with-param name="tlnam">
								<xsl:value-of select="$toolName"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$railgroup != $maingroup and $railgroup != ''">
						<xsl:call-template name="Group">
							<xsl:with-param name="groupnum">
								<xsl:value-of select="$railgroup"/>
							</xsl:with-param>
							<xsl:with-param name="axestype">
								<xsl:value-of select="'RailTrackGantry'"/>
							</xsl:with-param>
							<xsl:with-param name="objnam">
								<xsl:value-of select="$objectName"/>
							</xsl:with-param>
							<xsl:with-param name="tlnam">
								<xsl:value-of select="$toolName"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$toolgroup != $maingroup and $toolgroup != ''">
						<xsl:call-template name="Group">
							<xsl:with-param name="groupnum">
								<xsl:value-of select="$toolgroup"/>
							</xsl:with-param>
							<xsl:with-param name="axestype">
								<xsl:value-of select="'EndOfArmTooling'"/>
							</xsl:with-param>
							<xsl:with-param name="objnam">
								<xsl:value-of select="$objectName"/>
							</xsl:with-param>
							<xsl:with-param name="tlnam">
								<xsl:value-of select="$toolName"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$workgroup != $maingroup and $workgroup != ''">
						<xsl:call-template name="Group">
							<xsl:with-param name="groupnum">
								<xsl:value-of select="$workgroup"/>
							</xsl:with-param>
							<xsl:with-param name="axestype">
								<xsl:value-of select="'WorkpiecePositioner'"/>
							</xsl:with-param>
							<xsl:with-param name="objnam">
								<xsl:value-of select="$objectName"/>
							</xsl:with-param>
							<xsl:with-param name="tlnam">
								<xsl:value-of select="$toolName"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$railgroup != '' or $toolgroup != '' or $workgroup != ''"/>
						<xsl:otherwise>
							<xsl:call-template name="Group">
								<xsl:with-param name="groupnum">
									<xsl:value-of select="$toolgroup"/>
								</xsl:with-param>
								<xsl:with-param name="axestype">
									<xsl:value-of select="'EndOfArmTooling'"/>
								</xsl:with-param>
								<xsl:with-param name="objnam">
									<xsl:value-of select="$objectName"/>
								</xsl:with-param>
								<xsl:with-param name="tlnam">
									<xsl:value-of select="$toolName"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$cr"/>
					<xsl:text>}; </xsl:text>
				</xsl:if>
				<!-- Create move along the path statement-->
				<xsl:if test=" $mode = 'MoveAlongStatement' ">
					<!-- Create move along statement output -->
					<NumberIncrement:counternum counter="1"/>
					<xsl:variable name="posnum" select="NumberIncrement:next()"/>
					<NumberIncrement:counternum counter="0"/>
					<xsl:variable name="curnum" select="NumberIncrement:current()"/>
					<xsl:if test="$curnum != 1">
						<xsl:text> ;</xsl:text>
					</xsl:if>
					<xsl:value-of select="$cr"/>
					<xsl:call-template name="lineNumber">
						<xsl:with-param name="linenum" select="$curnum"/>
					</xsl:call-template>
					<xsl:variable name="curnum2" select="NumberIncrement:next()"/>
					<xsl:if test="$motionType='Joint'">
						<xsl:text>J P[</xsl:text>
						<xsl:value-of select="$posnum"/>
					</xsl:if>
					<xsl:if test="$motionType='Linear'">
						<xsl:text>L P[</xsl:text>
						<xsl:value-of select="$posnum"/>
					</xsl:if>
					<xsl:variable name="currcomment"
						select="AttributeList/Attribute[AttributeName='Pos Comment']/AttributeValue"/>
					<xsl:if test="string($currcomment) != ''">
						<xsl:text> : </xsl:text>
						<xsl:value-of select="$currcomment"/>
					</xsl:if>
					<xsl:text>] </xsl:text>
					<xsl:choose>
						<xsl:when test="$motionBasis = 'Percent'">
							<xsl:if test="$motionType='Joint'">
								<xsl:value-of
									select="format-number($speedValue, $jointPositionPattern)"/>
								<xsl:text>% </xsl:text>
							</xsl:if>
							<xsl:if test="$motionType='Linear'">
								<xsl:if test="$angularSpeedValue='100'">
									<xsl:value-of
										select="format-number($maxSpeedValue * $speedValue * 10, $jointPositionPattern)"/>
									<xsl:text>mm/sec </xsl:text>
								</xsl:if>
								<xsl:if test="$angularSpeedValue != '100'">
									<xsl:value-of
										select="format-number(360 * $angularSpeedValue div 100, $jointPositionPattern)"/>
									<xsl:text>deg/sec </xsl:text>
								</xsl:if>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$motionBasis = 'Absolute'">
							<xsl:if test="$motionType='Linear'">
								<xsl:if test="$angularSpeedValue='100'">
									<xsl:value-of
										select="format-number($speedValue * 1000, $jointPositionPattern)"/>
									<xsl:text>mm/sec </xsl:text>
								</xsl:if>
								<xsl:if test="$angularSpeedValue != '100'">
									<xsl:value-of
										select="format-number(360 * $angularSpeedValue div 100, $jointPositionPattern)"/>
									<xsl:text>deg/sec </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:if test="$motionType='Joint'">
								<xsl:value-of
									select="format-number($speedValue * 100 div $maxSpeedValue, $jointPositionPattern)"/>
								<xsl:text>% </xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number($speedValue, $xyzPattern)"/>
							<xsl:text>sec </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:call-template name="GetAccuracy">
						<xsl:with-param name="flyByMode">
							<xsl:value-of select="$flyByMode"/>
						</xsl:with-param>
						<xsl:with-param name="accuracyValue">
							<xsl:value-of select="$accuracyValue"/>
						</xsl:with-param>
						<xsl:with-param name="accuracyType">
							<xsl:value-of select="$accuracyType"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:if test="$orientMode = 'Wrist'">
						<xsl:text> WJNT</xsl:text>
					</xsl:if>
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNodeSet"/>
		<xsl:param name="linenum"/>
		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
			<xsl:variable name="postcomchk" select="substring($attrname,1,11)"/>
			<xsl:variable name="roblangchk" select="substring($attrvalue,1,15)"/>
			<xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
			<!-- <xsl:value-of select="$cr"/>
			<xsl:text>ATTR: </xsl:text>
			<xsl:value-of select="$attrname"/>
	             <xsl:value-of select="$cr"/>
			<xsl:text>ATTRVALUE: </xsl:text>
			<xsl:value-of select="$attrvalue"/>
	             <xsl:value-of select="$cr"/>
			<xsl:text>PRECOM: </xsl:text>
			<xsl:value-of select="$precomchk"/>
	             <xsl:value-of select="$cr"/>
			<xsl:text>POSTCOM: </xsl:text>
			<xsl:value-of select="$postcomchk"/>
	             <xsl:value-of select="$cr"/>
			<xsl:text>ROBLANG: </xsl:text>
			<xsl:value-of select="$roblangchk"/>
	             <xsl:value-of select="$cr"/>
			<xsl:text>STRIPROBLANG: </xsl:text>
			<xsl:value-of select="$striprobotlang"/>
	             <xsl:value-of select="$cr"/> -->
			<xsl:if
				test="($precomchk = 'PreComment' and $prefix = 'Pre') or ($postcomchk = 'PostComment' and 	$prefix = 'Post')">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('LastLineComment','TRUE')"/>
				<xsl:if test="$linenum != '0'">
					<xsl:if test="($postcomchk = 'PostComment' and 	$prefix = 'Post')">
						<xsl:variable name="linenum" select="NumberIncrement:next()"/>
					</xsl:if>
					<xsl:variable name="linenum" select="NumberIncrement:current()"/>
					<xsl:call-template name="lineNumber">
						<xsl:with-param name="linenum" select="$linenum"/>
					</xsl:call-template>
					<xsl:if test="($precomchk = 'PreComment' and $prefix = 'Pre')">
						<xsl:variable name="linenum" select="NumberIncrement:next()"/>
					</xsl:if>
					<xsl:if test="$roblangchk != 'Robot Language:'">
						<xsl:text>!</xsl:text>
					</xsl:if>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$roblangchk = 'Robot Language:'">
						<!-- DLY 2007/04/25 remove spaces because now unparsed lines uploaded as they are with spaces <xsl:text>  </xsl:text> -->
						<xsl:value-of select="$striprobotlang"/>
						<xsl:value-of select="$cr"/>
						<!-- set last utool/uframe number -->
						<xsl:if test="starts-with(normalize-space($striprobotlang), 'UTOOL_NUM = ')">
							<xsl:variable name="utn"
								select="substring-before(substring-after($striprobotlang,' = '),';')"/>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData('LASTUTOOLNUMBER', $utn)"/>
						</xsl:if>
						<xsl:if test="starts-with(normalize-space($striprobotlang), 'UTOOL_NUM[GP')">
							<xsl:variable name="utn"
								select="substring-before(substring-after($striprobotlang,'='),';')"/>
							<xsl:variable name="groupNum"
								select="substring-before(substring-after($striprobotlang,'GP'),']')"/>
							<xsl:if test="$groupNum='1'">
								<xsl:variable name="hr"
									select="MotomanUtils:SetParameterData('LASTUTOOLNUMBER', $utn)"
								/>
							</xsl:if>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(concat('UTOOLGROUPNUMBER',$groupNum),$utn)"
							/>							
						</xsl:if>
						<xsl:if
							test="starts-with(normalize-space($striprobotlang), 'UFRAME_NUM = ')">
							<xsl:variable name="ufn"
								select="substring-before(substring-after($striprobotlang,' = '),';')"/>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData('LASTUFRAMENUMBER', $ufn)"/>
						</xsl:if>
						<xsl:if
							test="starts-with(normalize-space($striprobotlang), 'UFRAME_NUM[GP')">
							<xsl:variable name="ufn"
								select="substring-before(substring-after($striprobotlang,'='),';')"/>
							<xsl:variable name="groupNum"
								select="substring-before(substring-after($striprobotlang,'GP'),']')"/>
							<xsl:if test="$groupNum='1'">
								<xsl:variable name="hr"
									select="MotomanUtils:SetParameterData('LASTUFRAMENUMBER', $ufn)"
								/>
							</xsl:if>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(concat('UFRAMEGROUPNUMBER',$groupNum),$ufn)"
							/>														
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$linenum != '0'">
							<xsl:text>  </xsl:text>
						</xsl:if>
						<xsl:value-of select="$attrvalue"/>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end of processComments -->
	<xsl:template name="replace-string">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="with"/>
		<xsl:choose>
			<xsl:when test="contains($text,$replace)">
				<xsl:variable name="currentReplaceStr"
					select="MotomanUtils:GetParameterData('REPLACEMENTSTRING')"/>
				<xsl:variable name="newReplaceStr"
					select="concat($currentReplaceStr,substring-before($text,$replace),$with)"/>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('REPLACEMENTSTRING',$newReplaceStr)"/>
				<xsl:call-template name="replace-string">
					<xsl:with-param name="text" select="substring-after($text,$replace)"/>
					<xsl:with-param name="replace" select="$replace"/>
					<xsl:with-param name="with" select="$with"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="currentReplaceStr"
					select="MotomanUtils:GetParameterData('REPLACEMENTSTRING')"/>
				<xsl:variable name="newReplaceStr" select="concat($currentReplaceStr,$text)"/>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('REPLACEMENTSTRING',$newReplaceStr)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:param name="Units"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'e')"/>
						<xsl:with-param name="e" select="substring-after($Num,'e')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'E')"/>
						<xsl:with-param name="e" select="substring-after($Num,'E')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num*$Units"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="lineNumber">
		<xsl:param name="linenum"/>
		<xsl:choose>
			<xsl:when test="number($linenum) &gt; 99">
				<xsl:text> </xsl:text>
			</xsl:when>
			<xsl:when test="number($linenum) &lt; 100 and number($linenum) &gt; 9">
				<xsl:text>  </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>   </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$linenum"/>
		<xsl:text>:</xsl:text>
	</xsl:template>
	<xsl:template name="setGroupAxes">
		<!-- group parameter in the form of (*,*,*,*,*) -->
		<xsl:variable name="grp1RailAxis" select="substring-before($railgroup0, ',')"/>
		<xsl:variable name="grp1ToolAxis" select="substring-before($toolgroup0, ',')"/>
		<xsl:variable name="grp1WorkAxis" select="substring-before($workgroup0, ',')"/>
		<xsl:variable name="grp1Axes">
			<xsl:if test="$grp1RailAxis != '' and $grp1RailAxis != '*'">
				<xsl:value-of select="$grp1RailAxis"/>
			</xsl:if>
			<xsl:if test="$grp1ToolAxis != '' and $grp1ToolAxis != '*'">
				<xsl:if test="$grp1RailAxis != '' and $grp1RailAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp1ToolAxis"/>
			</xsl:if>
			<xsl:if test="$grp1WorkAxis != '' and $grp1WorkAxis != '*'">
				<xsl:if test="$grp1ToolAxis != '' and $grp1ToolAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp1WorkAxis"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="railgroup2" select="substring-after($railgroup0, ',')"/>
		<xsl:variable name="toolgroup2" select="substring-after($toolgroup0, ',')"/>
		<xsl:variable name="workgroup2" select="substring-after($workgroup0, ',')"/>
		<xsl:variable name="grp2RailAxis" select="substring-before($railgroup2, ',')"/>
		<xsl:variable name="grp2ToolAxis" select="substring-before($toolgroup2, ',')"/>
		<xsl:variable name="grp2WorkAxis" select="substring-before($workgroup2, ',')"/>
		<xsl:variable name="grp2Axes">
			<xsl:if test="$grp2RailAxis != '' and $grp2RailAxis != '*'">
				<xsl:value-of select="$grp2RailAxis"/>
			</xsl:if>
			<xsl:if test="$grp2ToolAxis != '' and $grp2ToolAxis != '*'">
				<xsl:if test="$grp2RailAxis != '' and $grp2RailAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp2ToolAxis"/>
			</xsl:if>
			<xsl:if test="$grp2WorkAxis != '' and $grp2WorkAxis != '*'">
				<xsl:if test="$grp2ToolAxis != '' and $grp2ToolAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp2WorkAxis"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="railgroup3" select="substring-after($railgroup2, ',')"/>
		<xsl:variable name="toolgroup3" select="substring-after($toolgroup2, ',')"/>
		<xsl:variable name="workgroup3" select="substring-after($workgroup2, ',')"/>
		<xsl:variable name="grp3RailAxis" select="substring-before($railgroup3, ',')"/>
		<xsl:variable name="grp3ToolAxis" select="substring-before($toolgroup3, ',')"/>
		<xsl:variable name="grp3WorkAxis" select="substring-before($workgroup3, ',')"/>
		<xsl:variable name="grp3Axes">
			<xsl:if test="$grp3RailAxis != '' and $grp3RailAxis != '*'">
				<xsl:value-of select="$grp3RailAxis"/>
			</xsl:if>
			<xsl:if test="$grp3ToolAxis != '' and $grp3ToolAxis != '*'">
				<xsl:if test="$grp3RailAxis != '' and $grp3RailAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp3ToolAxis"/>
			</xsl:if>
			<xsl:if test="$grp3WorkAxis != '' and $grp3WorkAxis != '*'">
				<xsl:if test="$grp3ToolAxis != '' and $grp3ToolAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp3WorkAxis"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="railgroup4" select="substring-after($railgroup3, ',')"/>
		<xsl:variable name="toolgroup4" select="substring-after($toolgroup3, ',')"/>
		<xsl:variable name="workgroup4" select="substring-after($workgroup3, ',')"/>
		<xsl:variable name="grp4RailAxis" select="substring-before($railgroup4, ',')"/>
		<xsl:variable name="grp4ToolAxis" select="substring-before($toolgroup4, ',')"/>
		<xsl:variable name="grp4WorkAxis" select="substring-before($workgroup4, ',')"/>
		<xsl:variable name="grp4Axes">
			<xsl:if test="$grp4RailAxis != '' and $grp4RailAxis != '*'">
				<xsl:value-of select="$grp4RailAxis"/>
			</xsl:if>
			<xsl:if test="$grp4ToolAxis != '' and $grp4ToolAxis != '*'">
				<xsl:if test="$grp4RailAxis != '' and $grp4RailAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp4ToolAxis"/>
			</xsl:if>
			<xsl:if test="$grp4WorkAxis != '' and $grp4WorkAxis != '*'">
				<xsl:if test="$grp4ToolAxis != '' and $grp4ToolAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp4WorkAxis"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="grp5RailAxis" select="substring-after($railgroup4, ',')"/>
		<xsl:variable name="grp5ToolAxis" select="substring-after($toolgroup4, ',')"/>
		<xsl:variable name="grp5WorkAxis" select="substring-after($workgroup4, ',')"/>
		<xsl:variable name="grp5Axes">
			<xsl:if test="$grp5RailAxis != '' and $grp5RailAxis != '*'">
				<xsl:value-of select="$grp5RailAxis"/>
			</xsl:if>
			<xsl:if test="$grp5ToolAxis != '' and $grp5ToolAxis != '*'">
				<xsl:if test="$grp5RailAxis != '' and $grp5RailAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp5ToolAxis"/>
			</xsl:if>
			<xsl:if test="$grp5WorkAxis != '' and $grp5WorkAxis != '*'">
				<xsl:if test="$grp5ToolAxis != '' and $grp5ToolAxis != '*'">;</xsl:if>
				<xsl:value-of select="$grp5WorkAxis"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('grp1Axes', $grp1Axes)"/>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('grp2Axes', $grp2Axes)"/>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('grp3Axes', $grp3Axes)"/>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('grp4Axes', $grp4Axes)"/>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('grp5Axes', $grp5Axes)"/>
	</xsl:template>
	<xsl:template name="outputDefaultGroupR17">
		<xsl:text>DEFAULT_GROUP   = 1,</xsl:text>
		<xsl:variable name="grp2Axes" select="MotomanUtils:GetParameterData('grp2Axes')"/>
		<xsl:variable name="grp3Axes" select="MotomanUtils:GetParameterData('grp3Axes')"/>
		<xsl:variable name="grp4Axes" select="MotomanUtils:GetParameterData('grp4Axes')"/>
		<xsl:variable name="grp5Axes" select="MotomanUtils:GetParameterData('grp5Axes')"/>
		<xsl:choose>
			<xsl:when test="string($grp2Axes) != ''">1,</xsl:when>
			<xsl:otherwise>*,</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="string($grp3Axes) != ''">1,</xsl:when>
			<xsl:otherwise>*,</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="string($grp4Axes) != ''">1,</xsl:when>
			<xsl:otherwise>*,</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="string($grp5Axes) != ''">1;</xsl:when>
			<xsl:otherwise>*;</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="outputGroupsR17">
		<xsl:param name="positionRegister"/>
		<xsl:param name="regval"/>
		<xsl:variable name="grp1Axes" select="string(MotomanUtils:GetParameterData('grp1Axes'))"/>
		<xsl:variable name="grp2Axes" select="string(MotomanUtils:GetParameterData('grp2Axes'))"/>
		<xsl:variable name="grp3Axes" select="string(MotomanUtils:GetParameterData('grp3Axes'))"/>
		<xsl:variable name="grp4Axes" select="string(MotomanUtils:GetParameterData('grp4Axes'))"/>
		<xsl:variable name="grp5Axes" select="string(MotomanUtils:GetParameterData('grp5Axes'))"/>
		<!--
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp2Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp3Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp4Axes"/>
<xsl:value-of select="$cr"/>
<xsl:value-of select="$grp5Axes"/>
-->
		<xsl:if test="$grp1Axes != ''">
			<xsl:call-template name="output1GroupR17">
				<xsl:with-param name="grpNum" select="1"/>
				<xsl:with-param name="axisNum" select="$grp1Axes"/>
				<xsl:with-param name="positionRegister" select="$positionRegister"/>
				<xsl:with-param name="regval" select="$regval"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="$grp2Axes != ''">
			<xsl:choose>
				<xsl:when test="contains($grp2Axes, ';')">
					<xsl:variable name="a1" select="substring-before($grp2Axes, ';')"/>
					<xsl:call-template name="output1GroupR17">
						<xsl:with-param name="grpNum" select="2"/>
						<xsl:with-param name="axisNum" select="$a1"/>
						<xsl:with-param name="positionRegister" select="$positionRegister"/>
						<xsl:with-param name="regval" select="$regval"/>
					</xsl:call-template>
					<xsl:variable name="a2a3" select="substring-after($grp2Axes, ';')"/>
					<xsl:choose>
						<xsl:when test="contains($a2a3, ';')">
							<xsl:variable name="a2" select="substring-before($a2a3, ';')"/>
							<xsl:variable name="a3" select="substring-after($a2a3, ';')"/>
							<xsl:call-template name="output1GroupR17">
								<xsl:with-param name="grpNum" select="2"/>
								<xsl:with-param name="axisNum" select="$a2"/>
								<xsl:with-param name="positionRegister" select="$positionRegister"/>
								<xsl:with-param name="regval" select="$regval"/>
							</xsl:call-template>
							<xsl:call-template name="output1GroupR17">
								<xsl:with-param name="grpNum" select="2"/>
								<xsl:with-param name="axisNum" select="$a3"/>
								<xsl:with-param name="positionRegister" select="$positionRegister"/>
								<xsl:with-param name="regval" select="$regval"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="output1GroupR17">
								<xsl:with-param name="grpNum" select="2"/>
								<xsl:with-param name="axisNum" select="$a2a3"/>
								<xsl:with-param name="positionRegister" select="$positionRegister"/>
								<xsl:with-param name="regval" select="$regval"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="output1GroupR17">
						<xsl:with-param name="grpNum" select="2"/>
						<xsl:with-param name="axisNum" select="$grp2Axes"/>
						<xsl:with-param name="positionRegister" select="$positionRegister"/>
						<xsl:with-param name="regval" select="$regval"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$grp3Axes != ''">
			<xsl:call-template name="output1GroupR17">
				<xsl:with-param name="grpNum" select="3"/>
				<xsl:with-param name="axisNum" select="$grp3Axes"/>
				<xsl:with-param name="positionRegister" select="$positionRegister"/>
				<xsl:with-param name="regval" select="$regval"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="$grp4Axes != ''">
			<xsl:call-template name="output1GroupR17">
				<xsl:with-param name="grpNum" select="4"/>
				<xsl:with-param name="axisNum" select="$grp4Axes"/>
				<xsl:with-param name="positionRegister" select="$positionRegister"/>
				<xsl:with-param name="regval" select="$regval"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="$grp5Axes != ''">
			<xsl:call-template name="output1GroupR17">
				<xsl:with-param name="grpNum" select="5"/>
				<xsl:with-param name="axisNum" select="$grp5Axes"/>
				<xsl:with-param name="positionRegister" select="$positionRegister"/>
				<xsl:with-param name="regval" select="$regval"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- end of outputGroupsR17 -->
	<xsl:template name="output1GroupR17">
		<xsl:param name="grpNum"/>
		<xsl:param name="axisNum"/>
		<xsl:param name="positionRegister"/>
		<xsl:param name="regval"/>
		<xsl:choose>
			<xsl:when test="$positionRegister = 'true'">
				<xsl:text>    [</xsl:text>
				<xsl:value-of select="$grpNum"/>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$regval"/>
				<xsl:text>] =   ''   Group: </xsl:text>
				<xsl:value-of select="$grpNum"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$grpNum = 1">
						<xsl:text>,</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>   GP</xsl:text>
						<xsl:value-of select="$grpNum"/>
						<xsl:text>:</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:variable name="objnam" select="MotionAttributes/ObjectFrameProfile"/>
						<xsl:variable name="tlnam" select="MotionAttributes/ToolProfile"/>
						<xsl:variable name="tltype"
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]/ToolType"/>
						<xsl:choose>
							<xsl:when test="$tltype = 'Stationary'">
								<xsl:text>  UF : </xsl:text>
								<xsl:apply-templates
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]"
									mode="codeMode"/>
								<xsl:text>, UT : </xsl:text>
								<xsl:apply-templates
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objnam]"
									mode="codeMode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>  UF : </xsl:text>
								<xsl:apply-templates
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objnam]"
									mode="codeMode"/>
								<xsl:text>, UT : </xsl:text>
								<xsl:apply-templates
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$tlnam]"
									mode="codeMode"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="jointOutput">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="Target/JointTarget/AuxJoint[@DOFNumber = $axisNum]/JointValue"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="$cr"/>
		<xsl:choose>
			<xsl:when test="$grpNum = 1">
				<xsl:text>  E1= </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>  J1= </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="Target/JointTarget/AuxJoint/@Units = 'deg'">
				<xsl:call-template name="DoRotationalJoint">
					<xsl:with-param name="axesType">
						<xsl:value-of select="Target/JointTarget/@Type"/>
					</xsl:with-param>
					<xsl:with-param name="jointValue">
						<xsl:value-of select="$jointOutput"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="posValueOutput">
					<xsl:with-param name="value"
						select="format-number($jointOutput * 1000,$xyzPattern)"/>
				</xsl:call-template>
				<xsl:text> mm</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of output1GroupR17 -->
	<xsl:template name="posValueOutput">
		<xsl:param name="value"/>
		<xsl:choose>
			<xsl:when test="string-length($value) = '5'">
				<xsl:text>   </xsl:text>
				<xsl:value-of select="$value"/>
			</xsl:when>
			<xsl:when test="string-length($value) = '6'">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="$value"/>
			</xsl:when>
			<xsl:when test="string-length($value) = '7'">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$value"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="posTypeOutput">
		<xsl:param name="jointType"/>
		<xsl:param name="nextjointNum"/>
		<xsl:variable name="currJointNum" select="number($nextjointNum)-1"/>
		<xsl:choose>
			<xsl:when test="$jointType='Translational'">
				<xsl:text> mm</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> deg</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$NumRobotAxes &gt; $currJointNum">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:if test="$currJointNum='3'">
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if test="$NumRobotAxes &gt; $currJointNum">
			<xsl:text>  J</xsl:text>
			<xsl:value-of select="$nextjointNum"/>
			<xsl:text>= </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template name="DoCheckLabel">
		<xsl:param name="currentactivity"/>
		<xsl:param name="waitlabel"/>
		<xsl:for-each
			select="$currentactivity/descendant::Attribute[contains(AttributeName,'Comment')]">
			<xsl:if
				test="contains(AttributeValue,'Robot Language') and contains(AttributeValue,string($waitlabel))">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('OutputWaitLabel','FALSE')"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each
			select="$currentactivity/descendant::Attribute[contains(AttributeName,'Robot Language')]">
			<xsl:if test="contains(AttributeValue,string($waitlabel))">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('OutputWaitLabel','FALSE')"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="SetHighestPositionNumber">
		<xsl:variable name="prefix"
		select="AttributeList/Attribute[AttributeName='PosType']/AttributeValue"/>
		<xsl:variable name="localtagname" select="Target/CartesianTarget/Tag"/>		
		<xsl:variable name="reg">
			<xsl:choose>
				<xsl:when
					test="string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue) != ''">
					<xsl:value-of
						select="AttributeList/Attribute[AttributeName='PosReg']/AttributeValue"/>
				</xsl:when>
				<xsl:when test="string(Target/CartesianTarget/Tag) != '' and substring-after($localtagname,'[') != ''">
					<xsl:variable name="localtagSuffix" select="substring-after($localtagname,'[')"/>
					<xsl:value-of select="substring-before($localtagSuffix,']')"/>										
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="substring-after(ActivityName,'.')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="currentHigh"
			select="MotomanUtils:GetParameterData('HighestPositionNumber')"/>
		<xsl:if test="number($reg) &gt; number($currentHigh)">
			<xsl:variable name="hr"
				select="MotomanUtils:SetParameterData('HighestPositionNumber',$reg)"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="displayError">
		<xsl:param name="errorStr"/>
		<xsl:text>ERROR INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$errorStr"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>ERROR INFO END</xsl:text>
	</xsl:template>
	<xsl:template name="progHeader">
		<xsl:param name="attributeList"/>
		<xsl:param name="taskName"/>
		<xsl:variable name="extension"
			select="/OLPData/Resource/ParameterList/Parameter[ParameterName='FileExtension']/ParameterValue"/>
		<xsl:text>DATA FILE START </xsl:text>
		<xsl:value-of select="$taskName"/>
		<xsl:text>.</xsl:text>
		<xsl:choose>
			<xsl:when test="string($extension) != ''">
				<xsl:value-of select="$extension"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>ls</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
		<xsl:text>/PROG  </xsl:text>
		<xsl:call-template name="replace-string">
			<xsl:with-param name="text" select="$taskName"/>
			<xsl:with-param name="replace" select="'.'"/>
			<xsl:with-param name="with" select="'_'"/>
		</xsl:call-template>
		<xsl:variable name="currentReplaceStr"
			select="MotomanUtils:GetParameterData('REPLACEMENTSTRING')"/>
		<xsl:value-of select="$currentReplaceStr"/>
		<xsl:value-of select="$cr"/>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('REPLACEMENTSTRING','')"/>
		<xsl:variable name="precomment"
			select="$attributeList/Attribute[substring(AttributeName,1,10) = 'PreComment']/AttributeValue"/>
		<xsl:choose>
			<xsl:when test="$precomment != ''">
				<xsl:call-template name="processComments">
					<xsl:with-param name="prefix" select="'Pre'"/>
					<xsl:with-param name="attributeListNodeSet" select="$attributeList"/>
					<xsl:with-param name="linenum" select="'0'"/>
				</xsl:call-template>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('LastLineComment','FALSE')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>/ATTR</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>OWNER           = MNEDITOR;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>COMMENT         = "DELMIA OFFLINE PROGRAM - </xsl:text>
				<xsl:value-of select="GeneralInfo/ResourceName"/> Resource Id <xsl:value-of
					select="@id"/>";<xsl:value-of select="$cr"/>
				<xsl:text>PROG_SIZE       = 0;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:variable name="formatter"
					select="java:java.text.SimpleDateFormat.new('yy-	MM-	dd')"/>
				<xsl:variable name="c" select="java:java.util.Calendar.getInstance()"/>
				<xsl:variable name="c2" select="java:getTime($c)"/>
				<xsl:text>CREATE          = DATE </xsl:text>
				<xsl:value-of select="java:format($formatter, $c2)"/>
				<xsl:variable name="formatter2"
					select="java:java.text.SimpleDateFormat.new		('hh:mm:ss')"/>
				<xsl:variable name="c3" select="java:java.util.Calendar.getInstance()"/>
				<xsl:variable name="c4" select="java:getTime($c3)"/>
				<xsl:text> TIME  </xsl:text>
				<xsl:value-of select="java:format($formatter2, $c4)"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:variable name="formatter3"
					select="java:java.text.SimpleDateFormat.new('yy-	MM-	dd')"/>
				<xsl:variable name="c5" select="java:java.util.Calendar.getInstance()"/>
				<xsl:variable name="c6" select="java:getTime($c5)"/>
				<xsl:text>MODIFIED        = DATE </xsl:text>
				<xsl:value-of select="java:format($formatter3, $c6)"/>
				<xsl:variable name="formatter4"
					select="java:java.text.SimpleDateFormat.new		('hh:mm:ss')"/>
				<xsl:variable name="c7" select="java:java.util.Calendar.getInstance()"/>
				<xsl:variable name="c8" select="java:getTime($c7)"/>
				<xsl:text> TIME  </xsl:text>
				<xsl:value-of select="java:format($formatter4, $c8)"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>FILE_NAME       = ;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>VERSION         = 0;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>LINE_COUNT      = 0;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>MEMORY_SIZE     = 0;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>PROTECT         = READ_WRITE;</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>TCD:  STACK_SIZE        = 0,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>      TASK_PRIORITY     = 50,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>     TIME_SLICE        = 0,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>     BUSY_LAMP_OFF     = 0,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>     ABORT_REQUEST     = 0,</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>     PAUSE_REQUEST     = 0;</xsl:text>
				<xsl:value-of select="$cr"/>
				<!-- <xsl:variable name="openresult" select="FileUtils:fileOpen('D:/TEMP/test.txt')"/>
					<xsl:variable name="writeresult" select="FileUtils:fileWrite('BSBSBSBS')"/> 
					<xsl:variable name="closeresult" select="FileUtils:fileClose()"/> !-->
				<xsl:variable name="grp1Axes"
					select="string(MotomanUtils:GetParameterData('grp1Axes'))"/>
				<xsl:variable name="grp2Axes"
					select="string(MotomanUtils:GetParameterData('grp2Axes'))"/>
				<xsl:variable name="grp3Axes"
					select="string(MotomanUtils:GetParameterData('grp3Axes'))"/>
				<xsl:variable name="grp4Axes"
					select="string(MotomanUtils:GetParameterData('grp4Axes'))"/>
				<xsl:variable name="grp5Axes"
					select="string(MotomanUtils:GetParameterData('grp5Axes'))"/>
				<xsl:choose>
					<xsl:when
						test="$grp1Axes != '' or $grp2Axes != '' or $grp3Axes != '' or $grp4Axes != '' or $grp5Axes != ''">
						<!--
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$grp1Axes"/>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$grp2Axes"/>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$grp3Axes"/>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$grp4Axes"/>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$grp5Axes"/>
						-->
						<xsl:call-template name="outputDefaultGroupR17"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when
								test="$maingroup != '' or $railgroup != '' or $toolgroup != '' or $workgroup != ''">
								<xsl:variable name="shiftmain"
									select="BitMaskUtils:bitShiftLeft( 1, number		($maingroup) - 1)"/>
								<xsl:variable name="shiftrail"
									select="BitMaskUtils:bitShiftLeft( 1, number		($railgroup) - 1)"/>
								<xsl:variable name="shifttool"
									select="BitMaskUtils:bitShiftLeft( 1, number		($toolgroup) - 1)"/>
								<xsl:variable name="shiftwork"
									select="BitMaskUtils:bitShiftLeft( 1, number		($workgroup) - 1)"/>
								<xsl:variable name="mainandrail"
									select="BitMaskUtils:bitOR(number($shiftmain),  number($shiftrail))"/>
								<xsl:variable name="mainandrailandtool"
									select="BitMaskUtils:bitOR(number($mainandrail),  number($shifttool))"/>
								<xsl:variable name="groupbits"
									select="BitMaskUtils:bitOR(number($mainandrailandtool), number($shiftwork))"/>
								<xsl:text>DEFAULT_GROUP   = </xsl:text>
								<xsl:call-template name="GroupValue">
									<xsl:with-param name="groupnum">
										<xsl:value-of select="'1'"/>
									</xsl:with-param>
									<xsl:with-param name="groupbits">
										<xsl:value-of select="$groupbits"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="GroupValue">
									<xsl:with-param name="groupnum">
										<xsl:value-of select="'2'"/>
									</xsl:with-param>
									<xsl:with-param name="groupbits">
										<xsl:value-of select="$groupbits"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="GroupValue">
									<xsl:with-param name="groupnum">
										<xsl:value-of select="'4'"/>
									</xsl:with-param>
									<xsl:with-param name="groupbits">
										<xsl:value-of select="$groupbits"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="GroupValue">
									<xsl:with-param name="groupnum">
										<xsl:value-of select="'8'"/>
									</xsl:with-param>
									<xsl:with-param name="groupbits">
										<xsl:value-of select="$groupbits"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="GroupValue">
									<xsl:with-param name="groupnum">
										<xsl:value-of select="'16'"/>
									</xsl:with-param>
									<xsl:with-param name="groupbits">
										<xsl:value-of select="$groupbits"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="$NumToolAuxAxes &gt; 0 ">
									<xsl:text>DEFAULT_GROUP   = 1,1,*,*,*;</xsl:text>
								</xsl:if>
								<xsl:if test="$NumToolAuxAxes=0">
									<xsl:text>DEFAULT_GROUP   = 1,*,*,*,*;</xsl:text>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<!-- '*,*,*,*,*' or 'num' format group -->
				<xsl:value-of select="$cr"/>
				<xsl:text>CONTROL_CODE    = 00000000 00000000;</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>/MN</xsl:text>
	</xsl:template>
	<xsl:template name="createSpotValue">
		<xsl:param name="backup1"/>
		<xsl:param name="pressure_lmh"/>
		<xsl:param name="weld_sched"/>
		<xsl:param name="equipment"/>
		<xsl:param name="backup2"/>
		<xsl:param name="weld_condition"/>
		<xsl:text>SPOT[</xsl:text>
		<xsl:if
			test="$backup1 != '' and starts-with($backup1,'BU=') != 'true' and starts-with($backup1,'E=') != 'true' and starts-with($backup1,'EQ=') != 'true' and starts-with($backup1,'SD=') != 'true'">
			<xsl:text>BU=</xsl:text>
		</xsl:if>
		<xsl:if test="$backup1 != ''">
			<xsl:value-of select="$backup1"/>
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:if test="$pressure_lmh != '' and starts-with($pressure_lmh,'P=') != 'true'">
			<xsl:text>P=</xsl:text>
		</xsl:if>
		<xsl:if test="$pressure_lmh != ''">
			<xsl:value-of select="$pressure_lmh"/>
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:if test="$weld_sched != '' and starts-with($weld_sched,'S=') != 'true'">
			<xsl:text>S=</xsl:text>
		</xsl:if>
		<xsl:value-of select="$weld_sched"/>
		<xsl:if test="$equipment != '' or $backup2 != ''">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:if test="$equipment != '' and starts-with($equipment,'EQ=') != 'true'">
			<xsl:text>EQ=</xsl:text>
		</xsl:if>
		<xsl:if test="$equipment != ''">
			<xsl:value-of select="$equipment"/>
		</xsl:if>
		<xsl:if test="$backup2 != '' and $equipment != ''">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:if
			test="$backup2 != '' and starts-with($backup2,'BU=') != 'true' and starts-with($backup2,'ED=') != 'true'">
			<xsl:text>BU=</xsl:text>
		</xsl:if>
		<xsl:if test="$backup2 != ''">
			<xsl:value-of select="$backup2"/>
		</xsl:if>
		<xsl:if test="$weld_condition != ''">
			<xsl:value-of select="substring-after($weld_condition,'.')"/>
		</xsl:if>
		<xsl:text>]</xsl:text>
	</xsl:template>
	<xsl:template name="outputArcProfile">
		<xsl:param name="arcName"/>
		<xsl:choose>
			<xsl:when test="substring-before($arcName, '.') = 'TPEArcStart'">
				<xsl:text>Arc Start[</xsl:text>
				<xsl:value-of select="substring-after($arcName,'.')"/>
				<xsl:text>]</xsl:text>
			</xsl:when>
			<xsl:when test="substring-before($arcName, '.') = 'TPEArcEnd'">
				<xsl:text>Arc End[</xsl:text>
				<xsl:value-of select="substring-after($arcName,'.')"/>
				<xsl:text>]</xsl:text>
			</xsl:when>
			<xsl:when test="contains(substring-after($arcName,'.'),'.')">
				<xsl:value-of select="substring-before($arcName, '.')"/>
				<xsl:text>.</xsl:text>
				<xsl:call-template name="outputArcProfile">
					<xsl:with-param name="arcName" select="substring-after($arcName,'.')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="contains(substring-after($arcName,'.'),']')">
				<xsl:value-of select="$arcName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring-before($arcName, '.')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
