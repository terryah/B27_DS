<?xml version="1.0" encoding="UTF-8"?>
<!-- COPYRIGHT DASSAULT SYSTEMES 2003 -->
<!-- 13 Nov 2003 creation -->
<!-- IMPORTANT USAGE INFORMATION, PLEASE READ:
	This xslt stylesheet should be used as a starting point to create Delmia V5 OLP downloaders. This stylesheet is not a translator,
	since it doesn't output any of the values retrieved from processed XML elements. The task of a developer who will be using this 
	stylesheet is to create the output based on the syntax of a target robot language. For that purpose, xsl:text and xsl:value-of elements
	can be used, as well as the functions accessible through java xslt extensions. 
	Every line of source code has its matching comment. Please read the comments, since they often contain very important implementation
	information. 
	Some xsl variables have been set to contain default values. Comments above those variables will inform the developer that those 
	variable contents are expected to be changed. The effort has been made to create a very generic stylesheet that would require just 
	additional creation of output statements, while the structure of the file would stay unchanged. In case of translators that would need 
	development of additional templates, possibly to deal with issues not addressed here, it is developer's responsibility to provide them. 
	Also, if the content of this stylesheet needs to be changed, beyond the point of just adding output statements, it is up to the developer 
	to provide that modified xslt support. 
	This stylesheet can be extended either by declaring and referencing built-in java extensions in Xalan or by declaring and referencing 
	custom made java extension through their elements and functions. XSLT extension related literature is quite abundant and easy to find, primarily 
	through various www resources, and some of those resources are referenced in this stylesheet. -->
<!-- To see how to create and use java extensions within an xslt file, read in full the following references:
	http://xml.apache.org/xalan-j/extensions.html
	http://xml.apache.org/xalan-j/extensionslib.html
	http://xml.apache.org/xalan-j/extensions_xsltc.html -->
<!-- Declare the extensions within stylesheet element -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lxslt="http://xml.apache.org/xslt" xmlns:java="http://xml.apache.org/xslt/java" xmlns:dnbigpolp="http://www.dassault_systemes.com/V5/Delmia/Igrip/Olp/DNBIgpOLPXSLExtension" xmlns:NumberIncrement="NumberIncrement" xmlns:MatrixUtils="MatrixUtils" xmlns:MotomanUtils="MotomanUtils" xmlns:FileUtils="FileUtils" xmlns:BitMaskUtils="BitMaskUtils" xmlns:EncoderUtils="EncoderUtils" extension-element-prefixes="NumberIncrement  MotomanUtils MatrixUtils FileUtils BitMaskUtils EncoderUtils">
	<lxslt:component prefix="NumberIncrement" functions="next current" elements="startnum increment reset counternum getProgramNum">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<!--Declare the prefix and the functions to be accessed by using that prefix.
	     Here we just call the functons to set and get parameter and attribute data -->
	<lxslt:component prefix="dnbigpolp" functions="SetParameterData GetParameterData SetAttributeData GetAttributeData">
		<!-- Declare that java extensions were written in form of java class, and give the full class location -->
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils" functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll dgMatrixToQuaternions dgGetQ1 dgGetQ2 dgGetQ3 dgGetQ4" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"/>
	</lxslt:component>
	<lxslt:component prefix="EncoderUtils" functions="JointCalc DecimalToHex" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.EncoderUtils"/>
	</lxslt:component>
	<!-- Do not preserve whitespace-only text nodes for all the elements of the source XML document -->
	<xsl:strip-space elements="*"/>
	<!-- Indicates the output method for the result document. Text output method simply outputs all the result document's
		text nodes without modification -->
	<xsl:output method="text"/>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!-- HEADER INFORMATION -->
	<!-- You may modify the content of the select attribute, and type in the stylesheet version information relevant for your company -->
	<xsl:variable name="version" select="'DELMIA CORP. HYUNDAI DOWNLOADER VERSION 5 RELEASE 19'"/>
	<!-- You may modify the content of the select attribute, and type in the copyright information relevant for your company -->
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 1986-2007, ALL RIGHTS RESERVED'"/>
	<!-- In general this shouldn't be modified, but if the meaning will stay the same, the wording can change -->
	<xsl:variable name="saveInfo" select="'If you decide to save this file, VERSION INFO header will not be taken into account.'"/>
	<!-- PRESENTATION PATTERNS-->
	<!-- Decimal number format. For more information refer to java.text.DecimalFormat class documentation, in Java API Specification -->
	<xsl:variable name="programNumberPattern" select="'000'"/>
	<xsl:variable name="decimalNumberPattern" select="'#.###'"/>
	<xsl:variable name="targetNumberPattern" select="'#.#####'"/>
	<xsl:variable name="toolPattern" select="'0.000'"/>
	<!-- GENERAL INFORMATION ABOUT THE ROBOT AND DOWNLOADING TECHNIQUE -->
	<!-- Name of the robot which owns the downloaded task -->
	<xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<!-- Robot's maximum allowed speed -->
	<xsl:variable name="maxSpeed" select="/OLPData/Resource/GeneralInfo/MaxSpeed"/>
	<!-- Number of robot's base axes, excluding any type of external axes that can be detached from the robot -->
	<xsl:variable name="numberOfRobotAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<!-- Number of robot's rail and/or track axes -->
	<xsl:variable name="numberOfAuxiliaryAxes" select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<!-- Number of robot's end of arm tooling axes -->
	<xsl:variable name="numberOfExternalAxes" select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<!-- Number of workpiece positioner axes, that are controlled by the robot controller -->
	<xsl:variable name="numberOfPositionerAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<!-- Variable which value defines whether tag targets are generated with respect to the robot's base or with resptect to 
		the bases of the parts to which the tags are attached to -->
	<xsl:variable name="downloadCoordinates" select="/OLPData/Resource/GeneralInfo/DownloadCoordinates"/>
	<!-- Carriage Return character -->
	<xsl:variable name="platform" select="/OLPData/Resource/GeneralInfo/Platform"/>
	<xsl:variable name="cr">
		<xsl:choose>
			<xsl:when test="$platform='Windows'">
				<xsl:text>&#xa;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>&#xd;&#xa;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Main task name is the name of the task that has been selected for download -->
	<xsl:variable name="mainTaskName" select="/OLPData/Resource/ActivityList/@Task"/>
	<xsl:variable name="printlinenum" select="dnbigpolp:GetParameterData(string('PrintLineNumbers'))"/>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES END~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TEMPLATE DEFINITIONS START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!--~~~~~ROOT TEMPLATE~~~~~ -->
	<!-- This template is called first, automatically, by XSLT processor -->
	<xsl:template match="/">
		<!-- Create header information -->
		<xsl:call-template name="HeaderInfo"/>
		<!-- Initialize parameter data -->
		<xsl:call-template name="InitializeParameters"/>
		<!-- This is optional. Call this template if you need to create target declaration section for downloaded program -->
		<!--<xsl:call-template name="CreateTargetDeclarationSection"/>-->
		<!-- Process all the ActivityList nodes in order they appear in the source XML file -->
		<!--<xsl:variable name="tmpbss" select="/OLPData/Resource/ProgramLogic"/>-->
		<!--<xsl:if test="string($tmpbss) = '' or string($tmpbss) = 'NaN'">-->
		<xsl:apply-templates select="OLPData/Resource/ActivityList"/>
		<!--</xsl:if>-->
		<!--<xsl:if test="$tmpbss != ''">-->
		<xsl:apply-templates select="OLPData/Resource/ProgramLogic"/>
		<!--<xsl:apply-templates select="OLPData/Resource/ActivityList"/>-->
		<!--</xsl:if>-->
		<!--<xsl:text>END</xsl:text>-->
	</xsl:template>
	<!--~~~~~HEADER CREATOR TEMPLATE~~~~~ -->
	<!-- This template will not be called automatically by the processor. It is explicitly called from the root template.
		It creates version and copyright information blocks in-between VERSION INFO START and VERSION INFO END
		statements -->
	<xsl:template name="HeaderInfo">
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO START</xsl:text>
		<!-- Print carriage-return character to the result document -->
		<xsl:value-of select="$cr"/>
		<!-- Prints the version information to the result document -->
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the copyright information to the result document -->
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the save information to the result document -->
		<xsl:value-of select="$saveInfo"/>
		<xsl:value-of select="$cr"/>
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO END</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	<!-- ~~~~~PARAMETER DATA INITIALIZER TEMPLATE~~~~~ -->
	<!-- In order to make all the parameter data global, this template first stores all the parameter name / value pairs in java.
		After this template is processed all the parameter values will be accessible throughout this stylesheet. This template
		will not be called automatically by the processor. It is explicitly called from the root template. -->
	<xsl:template name="InitializeParameters">
		<!-- For each resource parameter defined in V5, get it's name and value, and store it in java extension -->
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<!-- Store parameter name -->
			<xsl:variable name="paramName" select="ParameterName"/>
			<!-- Store parameter value -->
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<!-- Store all the parameter name/value pairs in java -->
			<xsl:variable name="hr" select="dnbigpolp:SetParameterData( string($paramName), string($paramValue) )"/>
			<!-- IMPORTANT: In order to retrieve parameter value, from anywhere in the stylesheet use:
				String dnbigpolp:GetParameterData(String) function. For example, this will work:
				<xsl:variable name="result" select="dnbigpolp:GetParameterData('MyParameter1')"/>
				Pass the parameter name as a string, and the return value will be either 
				the parameter value (result, in the example above) returned as a string, or an empty string if the parameter
				 name does not exist in V5 for the downloading resource -->
		</xsl:for-each>
	</xsl:template>
	<!-- ~~~~~TARGET DECLARATION SECTION CREATOR TEMPLATE~~~~~ -->
	<!-- This template should only be called if target declaration section needs to be created in the output document. By default, in this 
		stylesheet it is always called from the root template. You can comment out that call-template element if you do not need to 
		create target declaration section in the output document -->
	<xsl:template name="CreateTargetDeclarationSection">
		<!-- For each robot motion and follow path activity in the source XML file -->
		<xsl:variable name="curnum" select="NumberIncrement:current()"/>
		<xsl:for-each select="/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity'] | 
							 /OLPData/Resource/ActivityList/Activity[@ActivityType = 'FollowPathActivity'] ">
			<!-- Call template TransformRobotMotionActivityData with mode parameter set to 'TargetDeclarationSection' -->
			<xsl:if test="@ActivityType = 'DNBRobotMotionActivity' ">
				<xsl:call-template name="TransformRobotMotionActivityData">
					<!-- Send the currect node, as the first parameter -->
					<xsl:with-param name="motionActivityNode" select="."/>
					<!-- Send the mode string, as the second parameter -->
					<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
				</xsl:call-template>
			</xsl:if>
			<!-- Call template TransformFollowPathActivityData with mode parameter set to 'TargetDeclarationSection' -->
			<xsl:if test="@ActivityType = 'FollowPathActivity' ">
				<xsl:call-template name="TransformFollowPathActivityData">
					<!-- Send the currect node, as the first parameter -->
					<xsl:with-param name="followPathActivityNode" select="."/>
					<!-- Send the mode string, as the second parameter -->
					<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--~~~~~ATTRIBUTE DATA INITIALIZER TEMPLATE~~~~~ -->
	<!-- In order to make all the attribute data global, this template stores all the passed attribute name / value pairs in java.
	Afeter being stored, those attribute values will be accessible throughout this stylesheet. This template
	will not be called automatically by the processor. It is explicitly called from the ActivityList, Activity, and Action templates -->
	<xsl:template name="SetAttributes">
		<!-- Value of the id attribute of the XML element, for which attributes are to be set -->
		<xsl:param name="id"/>
		<!-- Valid AttributeList XML element that contains Attribute sub-elements -->
		<xsl:param name="attributeListNodeSet"/>
		<!-- For each Attribute element in supplied AttributeList element -->
		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<!-- Check if AttributeName sub-element exist -->
			<xsl:if test="AttributeName">
				<!-- Store the value of the AttributeName element -->
				<xsl:variable name="attribName" select="AttributeName"/>
				<!-- Check if AttributeValue sub-element exist -->
				<xsl:if test="AttributeValue">
					<!-- Store the value of the AttributeValue element -->
					<xsl:variable name="attribValue" select="AttributeValue"/>
					<!-- Call Java extension to store the attribute name/value pairs -->
					<xsl:variable name="hr" select="dnbigpolp:SetAttributeData( string($id), string($attribName), string($attribValue) )"/>
					<!-- IMPORTANT: In order to retrieve attribute value, from anywhere in the stylesheet use:
					String dnbigpolp:GetAttributeData(String, String) function. For example, this will work:
					<xsl:variable name="result" select="dnbigpolp:GetAttributeData('_123456', 'MyAttrib1')"/>
					Pass XML element's id attribute and an attribute name, both as strings, and the return value 
					(result, in the example above) will either be the attribute value (result, in the example above) 
					returned as a string, or an empty string if the attribute name does not exist in V5 for the XML 
					element which id attribute has been passed to the function -->
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--
	<xsl:template match="ProgramLogic">
		<xsl:variable name="printlinenum" select="dnbigpolp:GetParameterData(string('PrintLineNumbers'))"/>
		<xsl:variable name="jobsname" select="/OLPData/Resource/ProgramLogic/Jobs/@Name"/>
		<xsl:text>DATA FILE START </xsl:text>
		<xsl:value-of select="$jobsname"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
		<xsl:for-each select="/OLPData/Resource/ProgramLogic/Jobs/Job">
			<xsl:variable name="jobname" select="@Name"/>
			<xsl:for-each select="child::node()">
				<xsl:variable name="currentItem" select="local-name()"/>
				<xsl:if test="$currentItem='StartCondition'">
					<xsl:variable name="curnum" select="NumberIncrement:next()"/>
					<xsl:if test="$printlinenum != 'false'">
						<xsl:value-of select="$curnum"/>
					</xsl:if>
					<xsl:text> IF(</xsl:text>
					<xsl:for-each select="Expression">
						<xsl:variable name="expression" select="."/>
						<xsl:text>(</xsl:text>
						<xsl:variable name="hr" select="HyundaiUtils:setStartExpr($expression)"/>
						<xsl:variable name="startex" select="HyundaiUtils:getStartExpr()"/>
						<xsl:value-of select="$startex"/>
						<xsl:text>)</xsl:text>
						<xsl:variable name="andOrNext" select="following-sibling::*[1]/."/>
						<xsl:if test="$andOrNext='or' or $andOrNext='and'">
							<xsl:text> </xsl:text>
							<xsl:value-of select="$andOrNext"/>
							<xsl:text> </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>) THEN</xsl:text>
				</xsl:if>
				<xsl:if test="$currentItem='CallTask'">
					<xsl:text>  *</xsl:text>
					<xsl:value-of select="."/>
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<xsl:if test="$currentItem='SetOutput'">
					<xsl:variable name="outputStr" select="."/>
					<xsl:variable name="curnum" select="NumberIncrement:next()"/>
					<xsl:value-of select="$curnum"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="signalname" select="/OLPData/Resource/ProgramLogic/Outputs/Output/@OutputName"/>
					<xsl:choose>
						<xsl:when test="contains($outputStr,'= false') or contains($outputStr, '=false') ">
							<xsl:variable name="outputSignal" select="substring-before($outputStr,'=false')"/>
							<xsl:value-of select="$signalname"/>
							<xsl:text>  0</xsl:text>
							<xsl:value-of select="$cr"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="outputSignal" select="substring-before($outputStr,'=true')"/>
							<xsl:value-of select="$signalname"/>
							<xsl:text>  1</xsl:text>
							<xsl:value-of select="$cr"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$currentItem='WaitExpression'">
					<xsl:variable name="waitexp" select="."/>
					<xsl:variable name="waitex" select="HyundaiUtils:setWaitExprStmt($waitexp)"/>
					<xsl:variable name="curnum" select="NumberIncrement:next()"/>
					<xsl:value-of select="$curnum"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="waitexstr" select="HyundaiUtils:getWaitExprStmt()"/>
					<xsl:text>WAITI </xsl:text>
					<xsl:value-of select="$waitexstr"/>
					<xsl:value-of select="$cr"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:apply-templates select="/OLPData/Resource/ActivityList">
			<xsl:with-param name="mode" select=" 'fromPLC' "/>
		</xsl:apply-templates>
		<xsl:variable name="curnum" select="NumberIncrement:next()"/>
		<xsl:value-of select="$curnum"/>
		<xsl:text> </xsl:text>
		<xsl:text>END</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END </xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	-->
	<!--~~~~~MULTI FILE OUTPUT TEMPLATE~~~~~-->
	<!-- This template creates a new DATA FILE START / END block for each ActivityList element in the source XML file.
		The contents of each of these blocks will be saved in separate files, when users decide to save the downloaded
		 content to disk. Equally effective (if not more effective) alternative is to save files from an XSLT stylesheet directly. For
		 that purpose org.apache.xalan.lib.Redirect java extension should be used. To get more information on how to activate
		 this extension, check the following url: http://xml.apache.org/xalan-j/apidocs/org/apache/xalan/lib/Redirect.html.
		 This template is called automatically by XSLT processor. -->
	<xsl:template match="ActivityList">
		<xsl:param name="mode"/>
		<!-- Store the robot task name which content is represented by this ActivityList XML element -->
		<xsl:variable name="taskName" select="@Task"/>
		<!-- If there is an attribute list associated with the robot task, store its attributes in java -->
		<xsl:if test="AttributeList">
			<!-- Call SetAttributes template to store attribute values for subsequent retrievals -->
			<xsl:call-template name="SetAttributes">
				<!-- Task name is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="@Task"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="$mode != 'fromPLC'">
			<xsl:text>DATA FILE START </xsl:text>
			<xsl:call-template name="getProgramName"/>
			<xsl:variable name="progNum" select="NumberIncrement:getProgramNum($taskName)"/>
			<xsl:value-of select="format-number($progNum, $programNumberPattern)"/>
		</xsl:if>
		<xsl:value-of select="$cr"/>
		<xsl:text>Program File Format Version : 1.5  TotalAxis: </xsl:text>
		<xsl:variable name="numberOfAllAxes" select="number($numberOfRobotAxes)+number($numberOfAuxiliaryAxes)+number($numberOfExternalAxes)+number($numberOfPositionerAxes)"/>
		<xsl:value-of select="$numberOfAllAxes"/>
		<xsl:variable name="numberOfAuxAxes" select="number($numberOfAuxiliaryAxes)+number($numberOfExternalAxes)+number($numberOfPositionerAxes)"/>
		<xsl:text>  AuxAxis: </xsl:text>
		<xsl:value-of select="$numberOfAuxAxes"/>
		<xsl:value-of select="$cr"/>
		<xsl:variable name="precomment" select="AttributeList/Attribute[AttributeName =  'PreComment1']/AttributeValue"/>
		<xsl:variable name="postcomment" select="AttributeList/Attribute[AttributeName =  'PostComment1']/AttributeValue"/>
		<xsl:variable name="curnum" select="NumberIncrement:current()"/>
		<xsl:if test="$precomment != ''">
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Pre'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
				<xsl:with-param name="linenum" select="'0'"/>
			</xsl:call-template>
		</xsl:if>
		<!-- If each ActivityList element needs to be saved in a new file, DATA FILE START/END block needs to be added and
			file extension specified below needs to changed from .txt to appropriate file extension for the chosen robot language -->
		<xsl:choose>
			<!-- If Call task activities exist and if they require creation of a new file for each task, output DATA FILE START/END block -->
			<xsl:when test="count(/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBIgpCallRobotTask']) > 0 and
							/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBIgpCallRobotTask'][1]/DownloadStyle = 'File'">
				<!-- Start the block which contents will be saved in a file -->
				<xsl:value-of select="$cr"/>
				<!-- This string must not be changed, since it is internally used by V5 as output file delimiter syntax -->
				<!--<xsl:if test="$mode != 'fromPLC'">
					<xsl:text>DATA FILE START </xsl:text>
					<xsl:value-of select="$taskName"/>
					<xsl:text>.prg</xsl:text>
				</xsl:if>-->
				<!-- Call the templates that will process all the Action and Activity nodes of this activity list -->
				<xsl:apply-templates select="Activity | Action"/>
				<!-- End the block which contents will be saved in a file -->
				<!--<xsl:value-of select="$cr"/>-->
				<!-- This string must not be changed, since it is internally used by V5 as output file delimiter syntax -->
				<!--<xsl:if test="$mode != 'fromPLC'">
				<xsl:if test="$postcomment != ''">
					<xsl:call-template name="processComments">
						<xsl:with-param name="prefix" select="'Post'"/>
						<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
					</xsl:call-template>
				</xsl:if>
					<xsl:text> END</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>DATA FILE END </xsl:text>
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<xsl:value-of select="$cr"/>-->
			</xsl:when>
			<!-- If there are no Call task activities or if they require subroutine output, begin activity/action translation -->
			<xsl:otherwise>
				<!-- Call the templates that will process all the Action and Activity nodes of this activity list -->
				<xsl:apply-templates select="Activity | Action"/>
				<xsl:if test="$postcomment != ''">
					<xsl:call-template name="processComments">
						<xsl:with-param name="prefix" select="'Post'"/>
						<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>     END</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END</xsl:text>
		<xsl:value-of select="$cr"/>
		<!--<xsl:value-of select="$cr"/>-->
		<NumberIncrement:reset/>
		<!--<xsl:value-of select="$cr"/>-->
		<xsl:if test="$postcomment != ''">
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Post'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
				<xsl:with-param name="linenum" select="$curnum"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!--~~~~~ACTION PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Action elements which are composed of one or more Activity elements. Only the 
		atomic activity types listed below are supported to be part of actions. This template is called automatically by
		XSLT processor. -->
	<xsl:template match="Action">
		<!-- All actions, regardless of their type, have these XML attributes and elkements -->
		<!-- Store this action's id number -->
		<xsl:variable name="actionID" select="@id"/>
		<!-- Store this action's type -->
		<xsl:variable name="actionType" select="@ActionType"/>
		<!-- Store this action's name -->
		<xsl:variable name="actionName" select="ActionName"/>
		<!-- Store name of the resource which performs this action-->
		<xsl:variable name="toolInActionName" select="ToolResource/ResourceName"/>
		<!--<xsl:text>SPOT </xsl:text>-->
		<!-- Set the attributes associated with this action. Refer to the comment marked as IMPORTANT in
			SetAttributes template -->
		<xsl:if test="AttributeList">
			<xsl:call-template name="SetAttributes">
				<!-- Action id attribute value is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="$actionID"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<!-- Based on action type, and the number and type of activities it contains, pick relevant portions of code contained below
		      in xsl:if element and create appropriate output. Delete redundant poritions of the code, and just keep what is needed.
		      All blocks of code below, by default, reference just the first activity of a specified type within an action. If there is more
		      than one activity of the same type within the action, each activity needs to be referenced based on its order of appearance
		      (of all the activities of the same type) within the action. 
		      For example, in case of Delay-Release-Delay combination of activities in one action:
		      
		      The first activity will be referenced, like:
		      
		      	<xsl:variable name="delayActivity" select="ActivityList/Activity[$activityType = 'DelayActivity' ][1]"/>
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			
			The second activity will be referenced, like:
			
			<xsl:variable name="releaseActivity" select="ActivityList/Activity[$activityType =  'ReleaseActivity' ][1]"/>
			<xsl:variable name="releaseActivityID" select="$releaseActivity/@id"/>
			<xsl:variable name="releaseActivityName" select="$releaseActivity/ActivityName"/>
			<xsl:if test="$releaseActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$releaseActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$releaseActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:variable name="releasedObject" select="$releaseActivity/ReleasedObject"/>
			<xsl:variable name="releasedDeviceParent" select="$releaseActivity/ReleasedObject/@ParentName"/>

			While the thrid activity will be referenced like:
			
		      	<xsl:variable name="delayActivity" select="ActivityList/Activity[$activityType = 'DelayActivity' ][2]"/>
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
		-->
		<xsl:if test="$actionType = 'Change this and specify action type relavant for the target robot language' ">
			<!-- DELAY ACTIVITY IN ACTION -->
			<!-- The first, by default, delay activity within the action -->
			<xsl:variable name="delayActivity" select="ActivityList/Activity[@ActivityType = 'DelayActivity' ][1]"/>
			<!-- Delay activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<!-- Delay activity's name, as specified in the source XML file -->
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<!-- Delay activity's begin time, as specified in the source XML file -->
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<!-- Delay activity's finish time, as specified in the source XML file -->
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<!-- Set the attributes associated with this activity. Refer to the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- ROBOT MOTION ACTIVITY IN ACTION -->
			<!-- The first, by default, delay robot motion within the action -->
			<xsl:variable name="robotMotionActivity" select="ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity'][1]"/>
			<!-- Robot Motion activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="robotMotionActivityID" select="$robotMotionActivity/@id"/>
			<!-- Robot Motion activity's name, as specified in the source XML file -->
			<xsl:variable name="robotMotionActivityName" select="$robotMotionActivity/ActivityName"/>
			<!-- Robot Motion activity's begin time, as specified in the source XML file -->
			<xsl:variable name="robotMotionActivityStartTime" select="$robotMotionActivity/ActivityTime/StartTime"/>
			<!-- Robot Motion activity's finish time, as specified in the source XML file -->
			<xsl:variable name="robotMotionActivityEndTime" select="$robotMotionActivity/ActivityTime/EndTime"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$robotMotionActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$robotMotionActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$robotMotionActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Motion type of this motion activity -->
			<xsl:variable name="motionType" select="$robotMotionActivity/MotionAttributes/MotionType"/>
			<!-- Target type of this motion activity -->
			<xsl:variable name="targetType" select="$robotMotionActivity/Target/@Default"/>
			<!-- Home position name for the tool in action -->
			<xsl:variable name="homeName" select="$robotMotionActivity/Target/JointTarget/HomeName"/>
			<!-- GRAB ACTIVITY IN ACTION -->
			<!-- The first, by default, grab activity within the action -->
			<xsl:variable name="grabActivity" select="ActivityList/Activity[@ActivityType =  'GrabActivity' ][1]"/>
			<!-- Grab activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="grabActivityID" select="$grabActivity/@id"/>
			<!-- Grab activity's name, as specified in the source XML file -->
			<xsl:variable name="grabActivityName" select="$grabActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$grabActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$grabActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$grabActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the grabbing device name -->
			<xsl:variable name="grabbingObject" select="$grabActivity/GrabbingObject"/>
			<!-- Get the grabbing device's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="grabbingDeviceParent" select="$grabActivity/GrabbingObject/@ParentName"/>
			<!-- Get the name of device which is to be grabbed-->
			<xsl:variable name="grabbedObject" select="$grabActivity/GrabbedObject"/>
			<!-- Get the grabbed device's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="grabbedDeviceParent" select="$grabActivity/GrabbedObject/@ParentName"/>
			<!-- RELEASE ACTIVITY IN ACTION -->
			<!-- The first, by default, release activity within the action -->
			<xsl:variable name="releaseActivity" select="ActivityList/Activity[@ActivityType =  'ReleaseActivity' ][1]"/>
			<!-- Release activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="releaseActivityID" select="$releaseActivity/@id"/>
			<!-- Release activity's name, as specified in the source XML file -->
			<xsl:variable name="releaseActivityName" select="$releaseActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$releaseActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$releaseActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$releaseActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the name of the device which is to be released -->
			<xsl:variable name="releasedObject" select="$releaseActivity/ReleasedObject"/>
			<!-- Get the released device's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="releasedDeviceParent" select="$releaseActivity/ReleasedObject/@ParentName"/>
			<!-- MOUNT ACTIVITY IN ACTION -->
			<!-- The first, by default, mount activity within the action -->
			<xsl:variable name="mountActivity" select="ActivityList/Activity[@ActivityType =  'DNBIgpMountActivity' ][1]"/>
			<!-- Mount activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="mountActivityID" select="$mountActivity/@id"/>
			<!-- Mount activity's name, as specified in the source XML file -->
			<xsl:variable name="mountActivityName" select="$mountActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$mountActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$mountActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$mountActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the tool name to be mounted -->
			<xsl:variable name="mountedTool" select="$mountActivity/MountTool"/>
			<!-- Get the tool's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="mountedToolParentName" select="$mountActivity/MountTool/@ParentName"/>
			<!-- Get the name of the tool profile which is to be set -->
			<xsl:variable name="setMountedToolProfile" select="$mountActivity/SetToolProfile"/>
			<!-- UNMOUNT ACTIVITY IN ACTION -->
			<!-- The first, by default, unmount activity within the action -->
			<xsl:variable name="unmountActivity" select="ActivityList/Activity[@ActivityType =  'DNBIgpUnMountActivity' ][1]"/>
			<!-- Unmount activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="unmountActivityID" select="$unmountActivity/@id"/>
			<!-- Unmount activity's name, as specified in the source XML file -->
			<xsl:variable name="unmountActivityName" select="$unmountActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$unmountActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$unmountActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$unmountActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the tool name to be unmounted -->
			<xsl:variable name="unmountedTool" select="$unmountActivity/UnmountTool"/>
			<!-- Get the tool's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="unmountedToolParentName" select="$unmountActivity/UnmountTool/@ParentName"/>
			<!-- Get the name of the tool profile which is to be set -->
			<xsl:variable name="setUnmountedToolProfile" select="$unmountActivity/UnsetToolProfile"/>
			<xsl:value-of select="$cr"/>
			<!-- CREATE THE OUTPUT -->
			<!-- Create the output based on the variable values and attribute values generated above -->
		</xsl:if>
		<!--<xsl:value-of select="$cr"/>-->
		<!-- Repeate xsl:if elements for as many action types which are relevant for the target robot language -->
	</xsl:template>
	<!--~~~~~ACTIVITY PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Activity elements in their order of appearance within the XML source file. Only the top level 
		Activity elements (the ones whose anscestors are OLPData, Resource, and AttributeList elements) will be transformed 
		by this template. Activity elements which are descendants of Action element, will be processed by Actions template above.
		This template is called automatically by XSLT processor. -->
	<xsl:template match="Activity">
		<!-- Global variables for all activities -->
		<!-- Get activity id number. It is used to set and get activity attributes. -->
		<xsl:variable name="activityID" select="@id"/>
		<!-- Get the activity type. Only the activity types specified below are supported. -->
		<xsl:variable name="activityType" select="@ActivityType"/>
		<!-- Get the activityName -->
		<xsl:variable name="activityName" select="ActivityName"/>
		<!-- Get the activity begin time -->
		<xsl:variable name="startTime" select="ActivityTime/StartTime"/>
		<!-- Get the activity finish time -->
		<xsl:variable name="endTime" select="ActivityTime/EndTime"/>
		<xsl:variable name="shortHand" select="dnbigpolp:GetParameterData(string('Shorthand'))"/>
		<!--<xsl:variable name="printlinenum" select="dnbigpolp:GetParameterData(string('PrintLineNumbers'))"/>-->
		<!-- Set the attributes associated with this activity. Refer to the comment marked as IMPORTANT in
			SetAttributes template -->
		<xsl:if test="AttributeList">
			<xsl:call-template name="SetAttributes">
				<!-- Activity id attribute value is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="$activityID"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<!--<xsl:variable name="curnum" select="NumberIncrement:next()"/>-->
		<!--<xsl:value-of select="$curnum"/>
		<xsl:text> </xsl:text>-->
		<!-- Based on activity type (the value of ActivityType XML element), process all the supported activities -->
		<xsl:choose>
			<!-- ROBOT MOTION ACTIVITY -->
			<xsl:when test="$activityType = 'DNBRobotMotionActivity'">
				<!-- Call the template which will output a move statement -->
				<xsl:call-template name="TransformRobotMotionActivityData">
					<!-- Send this robot motion activity Nodeset as parameter -->
					<xsl:with-param name="motionActivityNode" select="."/>
					<!-- This parameter determines that called templete needs to create a move statement
						in designated robot language -->
					<xsl:with-param name="mode" select=" 'MoveStatement' "/>
				</xsl:call-template>
			</xsl:when>
			<!-- DELAY ACTIVITY -->
			<xsl:when test="$activityType = 'DelayActivity' ">
				<!-- Output formatted delay statement in chosen robot language -->
				<!--<xsl:value-of select="$cr"/>-->
				<xsl:text>     DELAY </xsl:text>
				<xsl:variable name="start" select="ActivityTime/StartTime"/>
				<xsl:variable name="end" select="ActivityTime/EndTime"/>
				<xsl:value-of select="format-number($endTime - $startTime,$decimalNumberPattern)"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- SET IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBSetSignalActivity' ">
				<!-- Get signal name -->
				<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="setPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
				<!-- Get the signal pulse time -->
				<xsl:variable name="setSignalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>
				<!-- Output formatted set IO signal statement in chosen robot language -->
				<xsl:choose>
					<xsl:when test="$setSignalDuration != 0">
						<xsl:text>     DO</xsl:text>
						<xsl:value-of select="$setPortNumber"/>
						<xsl:text>=</xsl:text>
						<xsl:choose>
							<xsl:when test="$setSignalValue='Off'">
								<xsl:text>0</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>1,0,</xsl:text>
								<xsl:value-of select="$setSignalDuration"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="port" select="number($setPortNumber)"/>
						<xsl:text>     DO</xsl:text>
						<xsl:value-of select="$setPortNumber"/>
						<xsl:text>=</xsl:text>
						<xsl:choose>
							<xsl:when test="$setSignalValue='Off'">
								<xsl:text>0</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>1</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- WAIT FOR IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBWaitSignalActivity' ">
				<!-- Get signal name -->
				<xsl:variable name="waitSignalName" select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="waitPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
				<!-- Get maximum amount of time a robot will wait to receive a signal -->
				<xsl:variable name="waitMaxTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>
				<!-- Output formatted wait IO signal statement in chosen robot language -->
				<xsl:choose>
					<xsl:when test="$waitMaxTime != 0">
						<xsl:text>     WAIT DI</xsl:text>
						<xsl:value-of select="$waitPortNumber"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of select="$waitMaxTime"/>
						<xsl:text/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="port" select="number($waitPortNumber)"/>
						<xsl:choose>
							<xsl:when test="$waitSignalValue = 'Off'"> WAITJ</xsl:when>
							<xsl:when test="$shortHand = 'true' and ($port &gt; 0 and $port &lt; 25)"/>
							<xsl:otherwise>     WAIT DI</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$waitPortNumber"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- CALL ROBOT TASK ACTIVITY -->
			<xsl:when test="$activityType = 'DNBIgpCallRobotTask' ">
				<!-- Sub-program or sub-routine call. That depemds on the value of DownloadStyle element (below) -->
				<xsl:variable name="robotTaskToCall" select="CallName"/>
				<!-- This defines whether a subprogram - "File" or a subroutine - "Subroutine" will be called -->
				<xsl:variable name="downloadCalledTaskStyle" select="DownloadStyle"/>
				<xsl:text>      CALL </xsl:text>
				<xsl:value-of select="NumberIncrement:getProgramNum($robotTaskToCall)"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!--<xsl:when test="$activityType = 'DNBIgpCallRobotTask' ">
				<xsl:variable name="curnum" select="NumberIncrement:next()"/>
				<xsl:value-of select="$curnum"/>
				<xsl:variable name="robotTaskToCall" select="CallName"/>
				<xsl:variable name="downloadCalledTaskStyle" select="DownloadStyle"/>
				<xsl:text>   CALLP </xsl:text>
				<xsl:value-of select="$robotTaskToCall"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>-->
			<!-- ENTER INTERFERENCE ZONE ACTIVITY -->
			<xsl:when test="$activityType = 'DNBEnterZoneActivity' ">
				<!-- By default only zone names are relevant for the robot program creation. If the names of resources
					that are monitored for this zone are important for the robot language, this variable can be changed to
					'true' -->
				<xsl:variable name="areResourceNamesImportant" select=" 'false' "/>
				<!-- Get interference zone name -->
				<xsl:variable name="zoneName" select="ZoneData/ZoneName"/>
				<!-- Based on the value of areResourceNamesImportant variable, create the output-->
				<xsl:choose>
					<!-- Resource names are important for the output robot program -->
					<xsl:when test="$areResourceNamesImportant">
						<!-- For each resource in interference zone -->
						<xsl:for-each select="ZoneData/ZoneResourceList/ZoneResource">
							<!-- Get the resource name -->
							<xsl:variable name="resourceName" select="@Name"/>
							<!-- Get resource parent's name, as it appears in the PPR tree. 
								This may be important when workcell was imported from PPR Hub. -->
							<xsl:variable name="parentName" select="@ParentName"/>
							<!-- Output formatted enter interference zone statement and define the resources involved, in 
								a chosen robot language -->
						</xsl:for-each>
					</xsl:when>
					<!-- Resource names are not important for the output robot program -->
					<xsl:otherwise>
						<!-- Output formatted enter interference zone statement in a chosen robot language -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- CLEAR INTERFERENCE ZONE ACTIVITY -->
			<xsl:when test="$activityType = 'DNBClearZoneActivity' ">
				<!-- By default only zone names are relevant for the robot program creation. If the names of resources
					that are monitored for this zone are important for the robot language, this variable can be changed to
					'true' -->
				<xsl:variable name="areResourceNamesImportant" select=" 'false' "/>
				<!-- Get interference zone name -->
				<xsl:variable name="zoneName" select="ZoneData/ZoneName"/>
				<!-- Based on the value of areResourceNamesImportant variable, create the output-->
				<xsl:choose>
					<!-- Resource names are important for the output robot program -->
					<xsl:when test="$areResourceNamesImportant">
						<!-- For each resource in interference zone -->
						<xsl:for-each select="ZoneData/ZoneResourceList/ZoneResource">
							<!-- Get the resource name -->
							<xsl:variable name="resourceName" select="@Name"/>
							<!-- Get resource parent's name, as it appears in the PPR tree. 
								This may be important when workcell was imported from PPR Hub. -->
							<xsl:variable name="parentName" select="@ParentName"/>
							<!-- Output formatted clear interference zone statement and define the resources involved, in 
								a chosen robot language -->
						</xsl:for-each>
					</xsl:when>
					<!-- Resource names are not important for the output robot program -->
					<xsl:otherwise>
						<!-- Output formatted clear interference zone statement in a chosen robot language -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- FOLLOW PATH ACTIVITY -->
			<xsl:when test="$activityType = 'FollowPathActivity'">
				<!--<xsl:variable name="curnum" select="NumberIncrement:next()"/>-->
				<!-- Call the template which will output MoveAlong statement -->
				<xsl:call-template name="TransformFollowPathActivityData">
					<!-- Send this follow path activity Nodeset as parameter -->
					<xsl:with-param name="followPathActivityNode" select="."/>
					<!-- This parameter determines that called templete needs to create MoveAlong statement
						in designated robot language -->
					<xsl:with-param name="mode" select=" 'MoveAlongStatement' "/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$activityType = 'Operation'">
				<xsl:apply-templates select="AttributeList/Attribute">
					<xsl:with-param name="linenum" select="0"/>
				</xsl:apply-templates>
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
	</xsl:template>
	<!-- end template match Activity -->
	<!--~~~~~ROBOT MOTION ACTIVITY MULTI-PURPOSE TEMPLATE~~~~~-->
	<xsl:template name="TransformRobotMotionActivityData">
		<!-- Nodeset that contains all the descendants of robot motion activity element -->
		<xsl:param name="motionActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveStatement' "> element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<xsl:variable name="activityName" select="ActivityName"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$motionActivityNode/@id"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile" select="$motionActivityNode/MotionAttributes/MotionProfile"/>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile" select="$motionActivityNode/MotionAttributes/AccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile" select="$motionActivityNode/MotionAttributes/ToolProfile"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<xsl:variable name="objectFrameProfile" select="$motionActivityNode/MotionAttributes/ObjectFrameProfile"/>
		<!-- Motion profile values used by this motion activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speed" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Tool profile values used by this motion activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X"/>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y"/>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z"/>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw"/>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch"/>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll"/>
		<!-- ObjectFrame profile values used by this motion activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<xsl:variable name="ofPosX" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X"/>
		<!-- Object frame's location offset in Y direction -->
		<xsl:variable name="ofPosY" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y"/>
		<!-- Object frame's location offset in Z direction -->
		<xsl:variable name="ofPosZ" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z"/>
		<!-- Object frame's orientation offset in Yaw direction -->
		<xsl:variable name="ofOriYaw" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
		<!-- Object frame's orientation offset in Pitch direction -->
		<xsl:variable name="ofOriPitch" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
		<!-- Object frame's orientation offset in Roll direction -->
		<xsl:variable name="ofOriRoll" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
		<!-- Motion type of this motion activity: 'Joint' or 'Linear' -->
		<xsl:variable name="motionType" select="$motionActivityNode/MotionAttributes/MotionType"/>
		<!-- Orient mode of this motion activity: 'Wrist', '1_Axis', '2_Axis', or '3_Axis' -->
		<xsl:variable name="orientMode" select="$motionActivityNode/MotionAttributes/OrientMode"/>
		<!-- Target type of this motion activity: 'Joint' or 'Cartesian' -->
		<xsl:variable name="targetType" select="$motionActivityNode/Target/@Default"/>
		<xsl:variable name="tagName" select="$motionActivityNode/Target/CartesianTarget/Tag"/>
		<xsl:if test="$targetType = 'Cartesian'">
			<xsl:variable name="sumSquare1" select="$ofPosX * $ofPosX + $ofPosY * $ofPosY + $ofPosZ * $ofPosZ"/>
			<xsl:variable name="sumSquare2" select="$ofOriYaw * $ofOriYaw + $ofOriPitch * $ofOriPitch + $ofOriRoll * $ofOriRoll"/>
			<xsl:if test="$sumSquare1 + $sumSquare2 > 0">
				<xsl:text>ERROR INFO START</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>Error: Cartesian target "</xsl:text>
				<xsl:value-of select="$tagName"/>
				<xsl:text>" can NOT be used with a non-zero ObjectFrameProfile.</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
				<xsl:text># ERROR !!!</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text># Cartesian target "</xsl:text>
				<xsl:value-of select="$tagName"/>
				<xsl:text>" can NOT be used with a non-zero ObjectFrameProfile.</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text># ERROR !!!</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:if>
		<!-- Boolean that specifies whether this target is a via point or not: 'true' or 'false' -->
		<xsl:variable name="isViaPoint" select="$motionActivityNode/Target/@ViaPoint"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this move -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basePositionX" select="$motionActivityNode/Target/BaseWRTWorld/Position/@X"/>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY" select="$motionActivityNode/Target/BaseWRTWorld/Position/@Y"/>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ" select="$motionActivityNode/Target/BaseWRTWorld/Position/@Z"/>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="baseOrientationYaw" select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Yaw"/>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="baseOrientationPitch" select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Pitch"/>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baseOrientationRoll" select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Roll"/>
		<!-- BSS -->
		<xsl:if test="$mode = 'MoveStatement'">
			<xsl:variable name="curnum" select="NumberIncrement:next()"/>
			<xsl:call-template name="outputMove">
				<xsl:with-param name="targetType" select="$targetType"/>
				<xsl:with-param name="lineNum" select="$curnum"/>
			</xsl:call-template>
			<xsl:call-template name="moType">
				<xsl:with-param name="motionType" select="$motionType"/>
			</xsl:call-template>
			<xsl:call-template name="outputSpeedTime">
				<xsl:with-param name="speed" select="$speed"/>
				<xsl:with-param name="motionBasis" select="$motionBasis"/>
			</xsl:call-template>
			<xsl:call-template name="outputAccuracy">
				<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
			</xsl:call-template>
			<xsl:call-template name="outputTool">
				<xsl:with-param name="toolProfile" select="$toolProfile"/>
			</xsl:call-template>
			<xsl:variable name="retractnode" select="preceding-sibling::*[1]"/>
			<xsl:variable name="retractActionType" select="$retractnode/@ActionType"/>
			<xsl:if test="substring($retractActionType,1,17) = 'DNBIgpSpotRetract'">
				<xsl:variable name="retractValue" select="$retractnode/AttributeList/Attribute[AttributeName='Retract']/AttributeValue"/>
				<xsl:text>,</xsl:text>
				<xsl:choose>
					<xsl:when test="string($retractValue) != ''">
						<xsl:value-of select="$retractValue"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>MX</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:variable name="spotnode" select="following-sibling::*[1]"/>
			<xsl:variable name="spotActionType" select="$spotnode/@ActionType"/>
			<xsl:if test="substring($spotActionType,1,14) = 'DNBIgpSpotWeld'">
				<xsl:variable name="gunValue" select="$spotnode/AttributeList/Attribute[AttributeName='Gun']/AttributeValue"/>
				<xsl:text>,</xsl:text>
				<xsl:choose>
					<xsl:when test="string($gunValue) != ''">
						<xsl:value-of select="$gunValue"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>G1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:call-template name="outputPose">
				<xsl:with-param name="motionActivityNode" select="$motionActivityNode"/>
			</xsl:call-template>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if test="$activityName='ARCON'">
			<xsl:text> ARCON </xsl:text>
			<xsl:apply-templates select="MotionAttributes/UserProfile"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if test="$activityName='ARCOFF'">
			<xsl:text> ARCOFF </xsl:text>
			<xsl:apply-templates select="MotionAttributes/UserProfile"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	<!-- end of TransformRobotMotionActivityData -->
	<!--~~~~~FOLLOW PATH ACTIVITY MULTI-PURPOSE TEMPLATE~~~~~-->
	<xsl:template name="TransformFollowPathActivityData">
		<!-- Nodeset that contains all the descendants of follow path activity element -->
		<xsl:param name="followPathActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveAlongStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveAlongStatement' "> 
		element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$followPathActivityNode/@id"/>
		<!-- Name of the part that contains geometric feature along which the path is defined -->
		<xsl:variable name="pathOnPart" select="$followPathActivityNode/PathOnPart"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile" select="$followPathActivityNode/PathMotionAttributes/PathMotionProfile"/>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile" select="$followPathActivityNode/PathMotionAttributes/PathAccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile" select="$followPathActivityNode/PathMotionAttributes/PathToolProfile"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<xsl:variable name="objectFrameProfile" select="$followPathActivityNode/PathMotionAttributes/PathObjectFrameProfile"/>
		<!-- Motion profile values used by this activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speed" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Tool profile values used by this activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X"/>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y"/>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z"/>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw"/>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch"/>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll"/>
		<!-- ObjectFrame profile values used by this activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<xsl:variable name="ofPositionX" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X"/>
		<!-- Object frame's location offset in Y direction -->
		<xsl:variable name="ofPositionY" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y"/>
		<!-- Object frame's location offset in Z direction -->
		<xsl:variable name="ofPositionZ" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z"/>
		<!-- Object frame's orientation offset in Yaw direction -->
		<xsl:variable name="ofOrientationYaw" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
		<!-- Object frame's orientation offset in Pitch direction -->
		<xsl:variable name="ofOrientationPitch" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
		<!-- Object frame's orientation offset in Roll direction -->
		<xsl:variable name="ofOrientationRoll" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
		<!-- Motion type of this follow path activity: Always 'Linear' -->
		<xsl:variable name="motionType" select="$followPathActivityNode/PathMotionAttributes/MotionType"/>
		<xsl:variable name="targetType" select="$followPathActivityNode/PathMotionAttributes/TargetType"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this path -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basePositionX" select="$followPathActivityNode/BaseWRTWorld/Position/@X"/>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY" select="$followPathActivityNode/BaseWRTWorld/Position/@Y"/>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ" select="$followPathActivityNode/BaseWRTWorld/Position/@Z"/>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="baseOrientationYaw" select="$followPathActivityNode/BaseWRTWorld/Orientation/@Yaw"/>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="baseOrientationPitch" select="$followPathActivityNode/BaseWRTWorld/Orientation/@Pitch"/>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baseOrientationRoll" select="$followPathActivityNode/BaseWRTWorld/Orientation/@Roll"/>
		<!-- Configuration string name. Config string is the same for all the nodes in the path. -->
		<xsl:variable name="configurationName" select="$followPathActivityNode/PathConfig/@Name"/>
		<!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models.
			Turn numbers do not change between the nodes in the path.  -->
		<!-- If turn numbers have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn number value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turnNumber1" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 1]/@Value"/>
		<!-- Turn number value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turnNumber4" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 4]/@Value"/>
		<!-- Turn number value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnNumber5" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 5]/@Value"/>
		<!-- Turn number value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turnNumber6" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 6]/@Value"/>
		<!-- Get all  turn signs. If the robot uses turn signs, there will be 4 joints that will always have turn signs enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does for all robot models.
			Turn signs do not change between the nodes in the path.  -->
		<!-- If turn signs have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn sign output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn sign value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turnSign1" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 1]/@Value"/>
		<!-- Turn sign value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turnSign4" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 4]/@Value"/>
		<!-- Turn sign value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnSign5" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 5]/@Value"/>
		<!-- Turn sign value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turnSign6" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 6]/@Value"/>
		<!--<xsl:variable name="printlinenum" select="dnbigpolp:GetParameterData(string('PrintLineNumbers'))"/>-->
		<!-- Get node numbers, positions and orientations -->
		<xsl:if test="count($followPathActivityNode/PathNodeList/Node) > 0">
			<!-- For each node in the path, get -->
			<xsl:for-each select="$followPathActivityNode/PathNodeList/Node">
				<!-- Get node number -->
				<xsl:variable name="nodeNumber" select="@Number"/>
				<!-- Node position and orientation values. Node offset is always defined with respect to workcell's World coordinate system -->
				<!-- Node location offset in X direction -->
				<!--<xsl:variable name="nodePositionX" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@X"/>-->
				<xsl:variable name="nodePositionX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@X"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node location offset in Y direction -->
				<!--<xsl:variable name="nodePositionY" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Y"/>-->
				<xsl:variable name="nodePositionY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Y"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node location offset in Z direction -->
				<!--<xsl:variable name="nodePositionZ" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Z"/>-->
				<xsl:variable name="nodePositionZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Z"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node orientation offset in Yaw direction -->
				<!--<xsl:variable name="nodeOrientationYaw" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Yaw"/>-->
				<xsl:variable name="nodeOrientationYaw">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Yaw"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node orientation offset in Pitch direction -->
				<!--<xsl:variable name="nodeOrientationPitch" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Pitch"/>-->
				<xsl:variable name="nodeOrientationPitch">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Pitch"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node orientation offset in Roll direction -->
				<!--<xsl:variable name="nodeOrientationRoll" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Roll"/>-->
				<xsl:variable name="nodeOrientationRoll">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Roll"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test=" $mode = 'MoveAlongStatement' ">
					<xsl:variable name="curnum" select="NumberIncrement:next()"/>
					<xsl:call-template name="outputMove">
						<xsl:with-param name="targetType" select="$targetType"/>
						<xsl:with-param name="lineNum" select="$curnum"/>
					</xsl:call-template>
					<xsl:call-template name="moType">
						<xsl:with-param name="motionType" select="$motionType"/>
					</xsl:call-template>
					<xsl:text>(</xsl:text>
					<xsl:value-of select="format-number($nodePositionX,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodePositionY,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodePositionZ,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodeOrientationYaw,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodeOrientationPitch,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodeOrientationRoll,$targetNumberPattern)"/>
					<!-- Handle aux axes here -->
					<!-- end of aux axes handling -->
					<xsl:text>), </xsl:text>
					<!-- Speed Profile related -->
					<xsl:call-template name="outputSpeedTime">
						<xsl:with-param name="speed" select="$speed"/>
						<xsl:with-param name="motionBasis" select="$motionBasis"/>
					</xsl:call-template>
					<xsl:call-template name="outputAccuracy">
						<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
					</xsl:call-template>
					<xsl:call-template name="outputTool">
						<xsl:with-param name="toolProfile" select="$toolProfile"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:value-of select="$cr"/>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<!-- end of TransformFollowPathActivityData -->
	<xsl:template name="outputMove">
		<xsl:param name="targetType"/>
		<xsl:param name="lineNum"/>
		<xsl:variable name="printlinenum" select="dnbigpolp:GetParameterData(string('PrintLineNumbers'))"/>
		<xsl:if test="string($printlinenum) != 'false'">
			<xsl:text>S</xsl:text>
			<xsl:value-of select="$lineNum"/>
			<xsl:choose>
				<xsl:when test="$lineNum &lt; 10">
					<xsl:text>   </xsl:text>
				</xsl:when>
				<xsl:when test="$lineNum &lt; 100">
					<xsl:text>  </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="string($printlinenum) = 'false'">
			<xsl:text>     </xsl:text>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$targetType='Joint'">
				<xsl:text>MOVE </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>MOVE </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of outputMove -->
	<xsl:template name="moType">
		<xsl:param name="motionType"/>
		<xsl:choose>
			<xsl:when test="$motionType = 'Joint'">
				<xsl:text>P</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'Linear'">
				<xsl:text>L</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'Circular'">
				<xsl:text>C</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'CircularVia'">
				<xsl:text>C</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>L</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
	</xsl:template>
	<!-- end of moType -->
	<xsl:template name="outputPose">
		<xsl:param name="motionActivityNode"/>
		<xsl:text>  (</xsl:text>
		<xsl:variable name="railJoint1" select="dnbigpolp:GetParameterData('RailAxisIsJoint1')"/>
		<xsl:variable name="railNodes" select="$motionActivityNode/Target/JointTarget/AuxJoint[@Type='RailTrackGantry']"/>
		<xsl:if test="$railJoint1 = 'true' and boolean($railNodes) = 'true'">
			<xsl:for-each select="$railNodes">
				<xsl:variable name="auxJointValue">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="JointValue"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="jointType" select="@JointType"/>
				<xsl:choose>
					<xsl:when test="$jointType='Translational'">
						<xsl:value-of select="format-number($auxJointValue*1000.0, $targetNumberPattern)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number($auxJointValue, $targetNumberPattern)"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>,</xsl:text>
			</xsl:for-each>
		</xsl:if>
		<xsl:variable name="targetType" select="$motionActivityNode/Target/@Default"/>
		<!-- Cartesian target components -->
		<xsl:if test="$targetType = 'Cartesian'">
			<!-- Cartesian target position and orientation values. Target offset is defined with respect to object frame or if object frame
				offset is zero (x, y, z, roll, pitch, and yaw values are all set to 0) with respect to robot's base frame -->
			<!-- Target location offset in X direction -->
			<xsl:variable name="targetPositionX">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$motionActivityNode/Target/CartesianTarget/Position/@X"/>
					<xsl:with-param name="Units" select="1000.0"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target location offset in Y direction -->
			<xsl:variable name="targetPositionY">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$motionActivityNode/Target/CartesianTarget/Position/@Y"/>
					<xsl:with-param name="Units" select="1000.0"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target location offset in Z direction -->
			<xsl:variable name="targetPositionZ">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$motionActivityNode/Target/CartesianTarget/Position/@Z"/>
					<xsl:with-param name="Units" select="1000.0"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target orientation offset in Yaw direction -->
			<xsl:variable name="targetOrientationYaw">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$motionActivityNode/Target/CartesianTarget/Orientation/@Yaw"/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target orientation offset in Pitch direction -->
			<xsl:variable name="targetOrientationPitch">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$motionActivityNode/Target/CartesianTarget/Orientation/@Pitch"/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target orientation offset in Roll direction -->
			<xsl:variable name="targetOrientationRoll">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$motionActivityNode/Target/CartesianTarget/Orientation/@Roll"/>
					<xsl:with-param name="Units" select="1.0"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Retrieve tag name, and possibly, tag group name and the name of the part to which that tag group is attached to -->
			<!-- If this Cartesian target has a Tag element defined, tagName variable will not be empty, and if the tag is contained in a tag group
				which is attached to a part, tagGroupName and partOwnerName variables will not be empty.  Before creating tag output
				 in the result document, variables below need to be tested for validity in xsl:if element -->
			<!-- Tag name, empty if doesn't exist -->
			<!-- Tag group name that contains the above tag, empty if doesn't exist -->
			<xsl:variable name="tagGroupName" select="$motionActivityNode/Target/CartesianTarget/Tag/@TagGroup"/>
			<!-- Part name that has the above tag group attached, empty if doesn't exist -->
			<xsl:variable name="partOwnerName" select="$motionActivityNode/Target/CartesianTarget/Tag/@AttachedToPart"/>
			<xsl:value-of select="format-number($targetPositionX,$targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetPositionY,$targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetPositionZ,$targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetOrientationYaw,$targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetOrientationPitch,$targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetOrientationRoll,$targetNumberPattern)"/>
		</xsl:if>
		<!-- end targetType 'Cartesian' -->
		<xsl:variable name="BCoR2" select="dnbigpolp:GetParameterData(string('BCoR2'))"/>
		<xsl:variable name="R1CoR2" select="dnbigpolp:GetParameterData(string('R1CoR2'))"/>
		<xsl:variable name="R1CoB" select="dnbigpolp:GetParameterData(string('R1CoB'))"/>
		<!-- If this target has been defined in joint coordinates -->
		<xsl:if test="$targetType = 'Joint'">
			<!-- Get joint names, values, types and dof numbers-->
			<xsl:if test="count($motionActivityNode/Target/JointTarget/Joint) > 0">
				<!-- For each base robot joint (non-auxiliary joint), get -->
				<xsl:for-each select="$motionActivityNode/Target/JointTarget/Joint">
					<!-- Joint name -->
					<xsl:variable name="jointName" select="@JointName"/>
					<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
					<xsl:variable name="jointType" select="@JointType"/>
					<!-- Degree of freedom number of this joint -->
					<xsl:variable name="dofNumber" select="@DOFNumber"/>
					<!-- Joint value -->
					<!--<xsl:variable name="jointValue" select="JointValue"/>-->
					<xsl:variable name="jointValue">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="JointValue"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="JointHome" select="dnbigpolp:GetParameterData(concat('JointHome',string($dofNumber)))"/>
					<xsl:variable name="EncoderOffset" select="dnbigpolp:GetParameterData(concat('EncoderOffset',string($dofNumber)))"/>
					<xsl:variable name="EncoderScaleValue" select="dnbigpolp:GetParameterData(concat('EncoderScale',string($dofNumber)))"/>
					<xsl:variable name="EncoderScale">
						<xsl:choose>
							<xsl:when test="contains($EncoderScaleValue,'=')">
								<xsl:variable name="rest" select="substring-after($EncoderScaleValue, '=')"/>
								<xsl:value-of select="$rest"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$EncoderScaleValue"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<!-- Output joint value to the result document -->
					<xsl:variable name="jointCalcArg" select="concat(string($dofNumber),',',string($jointValue),',',$JointHome,',',$EncoderOffset,',',$EncoderScale,',',$BCoR2,',',$R1CoR2,',',$R1CoB)"/>
					<xsl:variable name="encoderValue" select="EncoderUtils:JointCalc($jointCalcArg)"/>
					<xsl:value-of select="$encoderValue"/>
					<xsl:if test="$dofNumber != count($motionActivityNode/Target/JointTarget/Joint)">
						<xsl:text>,</xsl:text>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:if>
		<!-- end targetType 'Joint' -->
		<!-- auxiliary joints -->
		<xsl:for-each select="$motionActivityNode/Target/JointTarget/AuxJoint">
			<xsl:choose>
				<xsl:when test="$railJoint1 = 'true' and @Type = 'RailTrackGantry'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>,</xsl:text>
					<xsl:variable name="auxJointValue">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="JointValue"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="$targetType = 'Joint'">
							<xsl:variable name="dofNumber" select="@DOFNumber"/>
							<xsl:variable name="JointHome" select="dnbigpolp:GetParameterData(concat('JointHome',string($dofNumber)))"/>
							<xsl:variable name="EncoderOffset" select="dnbigpolp:GetParameterData(concat('EncoderOffset',string($dofNumber)))"/>
							<xsl:variable name="EncoderScale" select="dnbigpolp:GetParameterData(concat('EncoderScale',string($dofNumber)))"/>
							<xsl:variable name="jointCalcArg" select="concat(string($dofNumber),',',string($auxJointValue),',',$JointHome,',',$EncoderOffset,',',$EncoderScale,',',$BCoR2,',',$R1CoR2,',',$R1CoB)"/>
							<xsl:variable name="encoderValue" select="EncoderUtils:JointCalc($jointCalcArg)"/>
							<xsl:value-of select="$encoderValue"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="@JointType='Translational'">
									<xsl:value-of select="format-number($auxJointValue*1000.0, $targetNumberPattern)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number($auxJointValue, $targetNumberPattern)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<xsl:if test="$targetType = 'Cartesian'">
			<!-- Configuration string name -->
			<xsl:variable name="configurationName" select="$motionActivityNode/Target/CartesianTarget/Config/@Name"/>
			<!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
				1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models. -->
			<!-- If turn numbers have been defined for this target, return value, stored in variables below, will not be empty. 
				Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
			<!-- Turn number value for joint 1, empty if doesn't exist -->
			<xsl:variable name="turnNumber1" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 1]/@Value"/>
			<!-- Turn number value for joint 4, empty if doesn't exist -->
			<xsl:variable name="turnNumber4" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 4]/@Value"/>
			<!-- Turn number value for joint 5, empty if doesn't exist -->
			<xsl:variable name="turnNumber5" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 5]/@Value"/>
			<!-- Turn number value for joint 6, empty if doesn't exist -->
			<xsl:variable name="turnNumber6" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 6]/@Value"/>
			<xsl:variable name="decConfig">
				<xsl:choose>
					<xsl:when test="$configurationName='Posture_1'">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:when test="$configurationName='Posture_2'">
						<xsl:text>8</xsl:text>
					</xsl:when>
					<xsl:when test="$configurationName='Posture_3'">
						<xsl:text>4</xsl:text>
					</xsl:when>
					<xsl:when test="$configurationName='Posture_4'">
						<xsl:text>12</xsl:text>
					</xsl:when>
					<xsl:when test="$configurationName='Posture_5'">
						<xsl:text>6</xsl:text>
					</xsl:when>
					<xsl:when test="$configurationName='Posture_6'">
						<xsl:text>14</xsl:text>
					</xsl:when>
					<xsl:when test="$configurationName='Posture_7'">
						<xsl:text>2</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>10</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="decTurn1">
				<xsl:choose>
					<xsl:when test="$turnNumber1 != '0'">
						<xsl:text>16</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="decTurn4">
				<xsl:choose>
					<xsl:when test="$turnNumber4 != '0'">
						<xsl:text>32</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="decTurn6">
				<xsl:choose>
					<xsl:when test="$turnNumber6 != '0'">
						<xsl:text>64</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="totalDecimal" select="number($decConfig) + number($decTurn1) + number($decTurn4) +number($decTurn6)"/>
			<xsl:variable name="totalHexidecimal" select="EncoderUtils:DecimalToHex($totalDecimal,string('true'))"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$totalHexidecimal"/>
		</xsl:if>
		<xsl:text>)</xsl:text>
		<xsl:if test="$targetType='Joint'">
			<xsl:text>E</xsl:text>
		</xsl:if>
	</xsl:template>
	<!-- end of outputPose -->
	<xsl:template name="outputSpeedTime">
		<xsl:param name="speed"/>
		<xsl:param name="motionBasis"/>
		<xsl:choose>
			<xsl:when test="$motionBasis='Percent'">
				<xsl:text>S=</xsl:text>
				<xsl:value-of select="format-number($speed,$decimalNumberPattern)"/>
				<xsl:text>%</xsl:text>
			</xsl:when>
			<xsl:when test="$motionBasis='Absolute'">
				<xsl:text>S=</xsl:text>
				<xsl:value-of select="format-number($speed * 1000, $decimalNumberPattern)"/>
				<xsl:text>mm/sec</xsl:text>
			</xsl:when>
			<xsl:when test="$motionBasis='Time'">
				<xsl:text>S=</xsl:text>
				<xsl:value-of select="format-number($speed, $decimalNumberPattern)"/>
				<xsl:text>sec</xsl:text>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
		<xsl:text>,</xsl:text>
	</xsl:template>
	<!-- end of outputSpeedTime -->
	<xsl:template name="outputAccuracy">
		<xsl:param name="accuracyProfile"/>
		<xsl:variable name="flyByMode" select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/FlyByMode"/>
		<xsl:variable name="accuracyType" select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyType"/>
		<xsl:variable name="accuracyValue" select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyValue/@Value"/>
		<xsl:text>A=</xsl:text>
		<xsl:call-template name="GetAccuracy">
			<xsl:with-param name="accuracyValue" select="$accuracyValue"/>
			<xsl:with-param name="accuracyType" select="$accuracyType"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
	</xsl:template>
	<xsl:template name="GetAccuracy">
		<xsl:param name="accuracyValue" select="0"/>
		<xsl:param name="accuracyType" select="Speed"/>
		<xsl:variable name="accuracyValueFlt">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$accuracyValue"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$accuracyType='Speed'">
				<xsl:choose>
					<xsl:when test="$accuracyValueFlt &lt;= 0.001">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:when test="$accuracyValueFlt &gt;= 100.0">
						<xsl:text>5</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="floor($accuracyValueFlt div (100 div 5.1))"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$accuracyType='Distance'">
				<xsl:choose>
					<xsl:when test="$accuracyValueFlt &lt;= 0.00001">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:when test="$accuracyValueFlt &gt;= 1">
						<xsl:text>5</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="floor($accuracyValueFlt *5)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$accuracyValueFlt &lt;= 0.00001">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:when test="$accuracyValueFlt &gt;= 1">
						<xsl:text>5</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="floor($accuracyValueFlt *5)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end template GetAccuracy -->
	<xsl:template name="outputTool">
		<xsl:param name="toolProfile"/>
		<xsl:choose>
			<xsl:when test="starts-with($toolProfile, 'T=')">
				<xsl:value-of select="$toolProfile"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>T=</xsl:text>
				<xsl:apply-templates select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolProfile]" mode="codeMode"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="ToolProfileList">
		<xsl:apply-templates select="ToolProfile" mode="fileMode"/>
	</xsl:template>
	<!-- ToolProfileList Template End-->
	<xsl:template match="ToolProfile" mode="codeMode">
		<xsl:value-of select="count(preceding-sibling::ToolProfile)"/>
	</xsl:template>
	<!-- ToolProfile Template End -->
	<xsl:template match="ToolProfile" mode="fileMode">
		<xsl:value-of select="$cr"/>'TOOL <xsl:value-of select="position()-1"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="format-number(TCPOffset/TCPPosition/@X, $toolPattern)"/>, <xsl:value-of select="format-number(TCPOffset/TCPPosition/@Y, $toolPattern)"/>, <xsl:value-of select="format-number(TCPOffset/TCPPosition/@Z, $toolPattern)"/>
	</xsl:template>
	<!-- ToolProfile Template End -->
	<xsl:template match="Attribute">
		<xsl:param name="linenum" select="0"/>
		<xsl:variable name="attrname" select="AttributeName"/>
		<xsl:variable name="attrvalue" select="AttributeValue"/>
		<xsl:if test="substring($attrname,1,7) = 'Comment'">
			<xsl:if test="$linenum != '0'">
				<xsl:value-of select="$linenum"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:text>'</xsl:text>
			<xsl:value-of select="$attrvalue"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if test="substring($attrname,1,14) = 'Robot Language'">
			<xsl:if test="$linenum != '0'">
				<xsl:value-of select="$linenum"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:value-of select="$attrvalue"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNodeSet"/>
		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
			<xsl:variable name="postcomchk" select="substring($attrname,1,11)"/>
			<xsl:variable name="roblangchk" select="substring($attrvalue,1,15)"/>
			<xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
			<xsl:if test="($precomchk = 'PreComment' and $prefix = 'Pre') or ($postcomchk = 'PostComment' and $prefix = 'Post')">
				<xsl:if test="$roblangchk != 'Robot Language:'">
					<xsl:text>' </xsl:text>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$roblangchk = 'Robot Language:'">
						<xsl:value-of select="$striprobotlang"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$attrvalue"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="UserProfile">
		<xsl:variable name="name" select="."/>
		<xsl:variable name="type" select="./@Type"/>
		<xsl:variable name="userProfile" select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type=$type]"/>
		<xsl:choose>
			<xsl:when test="contains($type,'NAWArcONTable')">
				<xsl:variable name="rest" select="substring-after($type, 'NAWArcONTable.')"/>
				<xsl:value-of select="$rest"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="rest" select="substring-after($type, 'NAWArcOFFTable.')"/>
				<xsl:value-of select="$rest"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:param name="Units" select="1"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'e')"/>
						<xsl:with-param name="e" select="substring-after($Num,'e')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'E')"/>
						<xsl:with-param name="e" select="substring-after($Num,'E')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num*$Units"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<!-- program name is composed of <resource name>.<program number> -->
	<xsl:template name="getProgramName">
		<xsl:variable name="resourceName" select="dnbigpolp:GetParameterData(string('ResourceName'))"/>
		<xsl:choose>
			<xsl:when test="string($resourceName) != '' and $resourceName != 'No Value'">
				<!-- parameter ResourceName is defined and non-blank -->
				<xsl:value-of select="$resourceName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring-before($robotName, '.')"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>.</xsl:text>
	</xsl:template>
</xsl:stylesheet>
