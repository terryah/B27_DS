<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:lxslt="http://xml.apache.org/xslt" 
xmlns:java="http://xml.apache.org/xslt/java" 
xmlns:NumberIncrement="NumberIncrement" 
xmlns:MotomanUtils="MotomanUtils" 
xmlns:KIRStrUtils="KIRStrUtils" 
xmlns:MatrixUtils="MatrixUtils" 
extension-element-prefixes="NumberIncrement MotomanUtils KIRStrUtils MatrixUtils">
	<lxslt:component prefix="NumberIncrement" functions="next current" elements="startnum increment reset">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MotomanUtils" functions="JointToEncoderConversion SetParameterData GetParameterData" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"/>
	</lxslt:component>
	<lxslt:component prefix="KIRStrUtils" functions="storeToolNum retrieveToolNum" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.KIRStrUtils"/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils" functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll dgMatrixToQuaternions dgGetQ1 dgGetQ2 dgGetQ3 dgGetQ4" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"/>
	</lxslt:component>
	<xsl:strip-space elements="*"/>
	<xsl:output method="text"/>
	<xsl:variable name="debug" select="0"/>
	<xsl:variable name="version" select="'DELMIA CORP. DENSO DOWNLOADER VERSION 5 RELEASE 25 SP6'"/>
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 2016, ALL RIGHTS RESERVED'"/>
	<xsl:variable name="cr" select="'&#xa;'"/>
	<xsl:variable name="RecursiveSplit" select="1000"/>
	<xsl:variable name="patInteger" select="'#'"/>
	<xsl:variable name="patD1" select="'0.0'"/>
	<xsl:variable name="patD2" select="'0.00'"/>
	<xsl:variable name="patD3" select="'0.000'"/>
	<xsl:variable name="patUptoD1" select="'#.#'"/>
	<xsl:variable name="patUptoD2" select="'#.##'"/>
	<xsl:variable name="patUptoD3" select="'#.###'"/>
	<xsl:variable name="patUptoD4" select="'#.####'"/>
	<xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<xsl:variable name="rbtAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<xsl:variable name="auxAxes" select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<xsl:variable name="extAxes" select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<xsl:variable name="posAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="dof" select="$rbtAxes+$auxAxes+$extAxes+$posAxes"/>
	<xsl:variable name="dofAux" select="$auxAxes+$extAxes+$posAxes"/>
	<xsl:variable name="maxSpeedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed"/>

	<xsl:template match="/">
		<xsl:call-template name="HeaderInfo"/>
		<xsl:call-template name="chkAccuracyProfiles"/>
		<xsl:variable name="BadAccuracyProfileNames" select="MotomanUtils:GetParameterData('BadAccuracyProfileNames')"/>
		<xsl:choose>
			<xsl:when test="string-length($BadAccuracyProfileNames)=0">
				<xsl:call-template name="InitializeParameters"/>
				<xsl:apply-templates select="OLPData"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>DATA FILE START ERROR</xsl:text>
				<xsl:value-of select="$cr"/>
				
				<xsl:choose>
					<xsl:when test="contains($BadAccuracyProfileNames,',')">
						<xsl:text>Accuracy Profiles &lt;</xsl:text>
						<xsl:value-of select="$BadAccuracyProfileNames"/>
						<xsl:text>&gt; have a problem.</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Accuracy Profile &lt;</xsl:text>
						<xsl:value-of select="$BadAccuracyProfileNames"/>
						<xsl:text>&gt; has a problem.</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
				<xsl:text>For a Speed type Accuracy Profile with FlyBy Mode 'On',</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>the Accuracy Value needs to be 100% for Denso Pass Motion '@P'.</xsl:text>
				
				<xsl:value-of select="$cr"/>
				<xsl:text>DATA FILE END</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="chkAccuracyProfiles">
		<!-- check if there is an Accuracy Profile of type Speed, with FlyBy Mode 'On' but accuracy value is NOT 100% -->
		<xsl:variable name="AccuracyProfiles" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile"/>
		<xsl:variable name="MotionActivities" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity']"/>
		<xsl:for-each select="$AccuracyProfiles">
			<xsl:if test="AccuracyType = 'Speed' and FlyByMode = 'On' and AccuracyValue/@Value &lt; 99.99">
				<xsl:variable name="badName" select="Name"/>
				<xsl:for-each select="$MotionActivities">
					<xsl:if test="MotionAttributes/AccuracyProfile = $badName">
						<xsl:variable name="existNames" select="MotomanUtils:GetParameterData('BadAccuracyProfileNames')"/>
						<xsl:choose>
							<xsl:when test="string-length($existNames)=0">
								<xsl:variable name="hr" select="MotomanUtils:SetParameterData('BadAccuracyProfileNames', $badName)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="hr" select="MotomanUtils:SetParameterData('BadAccuracyProfileNames', concat($existNames,', ',$badName))"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="HeaderInfo">
		<xsl:call-template name="WriteWarningMessages">
			<xsl:with-param name="line1" select="$version"/>
			<xsl:with-param name="line2" select="$copyright"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="InitializeParameters">
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<xsl:variable name="paramName" select="ParameterName"/>
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<xsl:variable name="hr"
				select="MotomanUtils:SetParameterData( string($paramName), string($paramValue) )"/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="OLPData">
		<!-- Tag target counter -->
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>
		<!-- Joint target counter -->
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:reset/>
		
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ProgramNumbersUsed', '')"/>
		<xsl:call-template name="storeMatchingProgramNumber"/>
		<xsl:call-template name="storeDotNumProgramNumber"/>
		
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('LargestPositionNumber', 0)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('SmallestPositionNumber', 1000000000)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('LargestJointNumber', 0)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('SmallestJointNumber', 1000000000)"/>
		<xsl:apply-templates select="Resource[GeneralInfo/ResourceName=$robotName]/ActivityList" mode="PAC"/>
		
		<!-- Cartesian pose data file Var_P.csv -->
		<xsl:call-template name="VarFileHeader">
			<xsl:with-param name="TargetType" select="'P'"/>
		</xsl:call-template>
		<xsl:apply-templates select="Resource[GeneralInfo/ResourceName=$robotName]/ActivityList" mode="CSV">
			<xsl:with-param name="TargetType" select="'P'"/>
		</xsl:apply-templates>
		<!-- close file Var_P.csv -->
		<xsl:text>DATA FILE END</xsl:text>

		<!-- Joint pose data file Var_J.csv -->
		<xsl:call-template name="VarFileHeader">
			<xsl:with-param name="TargetType" select="'J'"/>
		</xsl:call-template>
		<xsl:apply-templates select="Resource[GeneralInfo/ResourceName=$robotName]/ActivityList" mode="CSV">
			<xsl:with-param name="TargetType" select="'J'"/>
		</xsl:apply-templates>
		<!-- close file Var_J.csv -->
		<xsl:text>DATA FILE END</xsl:text>
	</xsl:template>
	
	<xsl:template name="VarFileHeader">
		<xsl:param name="TargetType"/>
		<!-- file name: Var_J|P.csv -->
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE START Var_</xsl:text>
		<xsl:value-of select="$TargetType"/>
		<xsl:text>.csv</xsl:text>
		<xsl:value-of select="$cr"/>
		<!-- Line 1: Type J|P Variables -->
		<xsl:text>===== Type </xsl:text>
		<xsl:value-of select="$TargetType"/>
		<xsl:text> Variables =====</xsl:text>
		<xsl:value-of select="$cr"/>
		<!-- Line 2: RobotTypeName : ,<RobotName> -->
		<xsl:text>RobotTypeName : ,</xsl:text>
		<xsl:variable name="robotTypeName">
			<xsl:value-of select="substring-before($robotName, '.')"/>
		</xsl:variable>
		<xsl:value-of select="$robotTypeName"/>
		<!-- Append 'I' if $dofAux > 0 -->
		<xsl:if test="$dofAux &gt; 0">I</xsl:if>
		<xsl:value-of select="$cr"/>
		<!-- Line 3: RobotTypeID : ,<RobotID> -->
		<xsl:text>RobotTypeID : ,</xsl:text>
		<xsl:variable name="nameLength">
			<xsl:value-of select="string-length($robotTypeName)"/>
		</xsl:variable>
		<xsl:variable name="robotTypeId">
			<xsl:choose>
				<xsl:when test="starts-with($robotName, 'HM-4060') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65744</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HM-4070') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65745</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HM-4080') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65746</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HM-40A0') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65747</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HMS-4070') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65768</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HMS-4085') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65769</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HS-4535') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65780</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HS-4545') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65781</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HS-4555') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65782</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HSS-4545') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65789</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'HSS-4555') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65790</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VM-6083G') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>62</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VM-60B1G') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>63</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VP-5243F') and substring($robotTypeName,$nameLength)='F'">
					<xsl:text>59</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VP-5243G') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>65</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VP-6242G') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>64</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VS-6556G') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>54</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VS-6556G-B')">
					<xsl:text>56</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VS-6556G-B-UL')">
					<xsl:text>537133112</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VS-6577G') and substring($robotTypeName,$nameLength)='G'">
					<xsl:text>55</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VS-6577G-B')">
					<xsl:text>57</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($robotName, 'VS-6577G-B-UL')">
					<xsl:text>537133113</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>-1</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="$robotTypeId &lt; 0 and $TargetType='J'">
			<xsl:call-template name="WriteWarningMessages">
				<xsl:with-param name="warning" select="'WARNING: '"/>
				<xsl:with-param name="line1" select="'The RobotTypeId is not available for this robot.'"/>
				<xsl:with-param name="line2" select="'Please find it out from the robot controller files Var_P.csv or'"/>
				<xsl:with-param name="line3" select="'Var_J.csv and edit these files generated from DELMIA download.'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="robotTypeId_Aux">
			<xsl:if test="$robotTypeId &gt; 0">
				<xsl:choose>
					<xsl:when test="$dofAux &gt; 0">
						<xsl:value-of select="number(16*16*16*16*16 + $robotTypeId)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$robotTypeId"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:value-of select="$robotTypeId_Aux"/>
		<!-- HM-40A0?G: 65747 -->
		<!-- HM-4060?G: 65744; HM-4060?GI: 1114320 = 16^5 + 65744 -->
		<!-- HM-4070?G: 65745 -->
		<!-- HM-4085?G: 65746 -->
		<!-- HM-40601G-P: 8454360 = &H008100D8; HM-40601G-PI: 9502936 = &H009100D8 -->
		<!-- HMS-4070?G: 65768 -->
		<!-- HMS-4085?G: 65769 -->
		<!-- HS-4535?G: 65780 -->
		<!-- HS-4545?G: 65781 -->
		<!-- HS-4555?G: 65782; HS-45553GI: 1114358 = 16^5 + 65782 -->
		<!-- HSS-4545?G: 65789 -->
		<!-- HSS-4555?G: 65790 -->
		<!-- VM-60B1G: 63 = &H3F -->
		<!-- VM-60B1GI: 1048639 = &H10003F = 16^5 + 63 = 1048576 + 63 -->
		<!-- VM-6083G: &H3E = 62 -->
		<!-- VP-5243F: 59 -->
		<!-- VP-5243G: 65; VP-6242G: 64 -->
		<!-- VS-6556G: 54; VS-6556G-B: 56; VS-6556G-B-UL: 537133112 -->
		<!-- VS-6577G: 55; VS-6577G-B: 57; VS-6577G-B-UL: 537133113 -->
		<xsl:value-of select="$cr"/>
		<!-- Line 4: -->
		<xsl:choose>
			<xsl:when test="$TargetType='J'">
				<xsl:choose>
					<xsl:when test="$rbtAxes = 4">
						<xsl:text>[No.],[J1],[J2],[J3],[J4],[Usage],[Macro]</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>[No.],[J1],[J2],[J3],[J4],[J5],[J6],[Usage],[Macro]</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$TargetType='P'">
				<xsl:choose>
					<xsl:when test="$rbtAxes = 4">
						<xsl:text>[No.],[X],[Y],[Z],[T],[FIG],[Usage],[Macro]</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>[No.],[X],[Y],[Z],[RX],[RY],[RZ],[FIG],[Usage],[Macro]</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
	</xsl:template>

	<!-- This template is called before processing ActivityList
		 to store program numbers for DELMIA task names in the format PRO<number>.
	  -->
	<xsl:template name="storeMatchingProgramNumber">
		<xsl:for-each select="Resource/ActivityList">
			<!-- to UPPER case -->
			<xsl:variable name="taskName0">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="@Task"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- strip trailing '_1' appended for duplicate names from upload -->
			<xsl:variable name="taskName">
				<xsl:call-template name="TrimTrailing_1">
					<xsl:with-param name="instring" select="$taskName0"/>
				</xsl:call-template>
			</xsl:variable>

			<xsl:if test="starts-with($taskName, 'PRO') and string(number(substring-after($taskName, 'PRO')))!='NaN'">
				<xsl:variable name="numSuffix" select="substring-after($taskName, 'PRO')"/>
				<xsl:variable name="dummy" select="NumberIncrement:setProgramNum($taskName, $numSuffix)"/>
				<!-- store numSuffix to parameter data ProgramNumbersUsed -->
				<xsl:variable name="prgNumsUsed" select="MotomanUtils:GetParameterData('ProgramNumbersUsed')"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ProgramNumbersUsed', concat($prgNumsUsed, ':', $numSuffix, ';'))"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template> <!-- storeMatchingProgramNumber -->
	
	<!-- This template stores program numbers for DELMIA task names in the format *.<number> -->
	<xsl:template name="storeDotNumProgramNumber">
		<xsl:for-each select="Resource/ActivityList">
			<!-- to UPPER case -->
			<xsl:variable name="taskName0">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="@Task"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- strip trailing '_1' appended for duplicate names from upload -->
			<xsl:variable name="taskName">
				<xsl:call-template name="TrimTrailing_1">
					<xsl:with-param name="instring" select="$taskName0"/>
				</xsl:call-template>
			</xsl:variable>
			
			<xsl:variable name="suffix">
				<xsl:call-template name="GetSuffix">
					<xsl:with-param name="instring" select="$taskName"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="string(number($suffix)) != 'NaN'">
				<xsl:variable name="prgNumsUsed" select="MotomanUtils:GetParameterData('ProgramNumbersUsed')"/>
				<xsl:if test="contains($prgNumsUsed, concat(':', $suffix, ';')) = false">
					<xsl:variable name="dummy" select="NumberIncrement:setProgramNum($taskName, $suffix)"/>
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ProgramNumbersUsed', concat($prgNumsUsed, ':', $suffix, ';'))"/>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="ActivityList" mode="CSV">
		<xsl:param name="TargetType"/>
		
		<!-- SmallestPositionNumber or SmallestJointNumber -->
		<xsl:variable name="startnum">
			<xsl:choose>
				<xsl:when test="$TargetType='P'">
					<xsl:value-of select="MotomanUtils:GetParameterData('SmallestPositionNumber')"/>
				</xsl:when>
				<xsl:when test="$TargetType='J'">
					<xsl:value-of select="MotomanUtils:GetParameterData('SmallestJointNumber')"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<!-- LargestPositionNumber or LargestJointNumber -->
		<xsl:variable name="endnum">
			<xsl:choose>
				<xsl:when test="$TargetType='P'">
					<xsl:value-of select="MotomanUtils:GetParameterData('LargestPositionNumber')"/>
				</xsl:when>
				<xsl:when test="$TargetType='J'">
					<xsl:value-of select="MotomanUtils:GetParameterData('LargestJointNumber')"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:if test="$endnum &gt; $startnum - 1">
			<xsl:call-template name="PrepCallOutputTarget">
				<xsl:with-param name="TargetType" select="$TargetType"/>
				<xsl:with-param name="StartNum" select="$startnum"/>
				<xsl:with-param name="EndNum" select="$endnum"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="PrepCallOutputTarget">
		<xsl:param name="TargetType"/>
		<xsl:param name="StartNum"/>
		<xsl:param name="EndNum"/>
		
		<xsl:choose>
			<xsl:when test="$EndNum - $StartNum &gt; $RecursiveSplit">
				<!-- In order to output sequentially P & J var, it's necessary to call outputTarget recursively.
					 The DNBRobotMotionActivity id is stored in ArrayList pVarActID & jVarActID.
					 The ArrayList index is the order to output P & J var.
					 Calling recursively a template over 1,000 times will get StackOverflowError.
					 Here we divide the target numbers in 1,000 and call recursively PrepCallOutputTarget
					 which in turn calls outputTarget.
					 This lets the target numbers to span over 800,000
				-->
				<xsl:variable name="NewEndNum" select="$StartNum + $RecursiveSplit - 1"/>
				<!-- do output targets from StartNum to StartNum + RecursiveSplit -->
				<xsl:call-template name="PrepCallOutputTarget">
					<xsl:with-param name="TargetType" select="$TargetType"/>
					<xsl:with-param name="StartNum" select="$StartNum"/>
					<xsl:with-param name="EndNum" select="$NewEndNum"/>
				</xsl:call-template>
				<!-- do output targets from StartNum + RecursiveSplit + 1 to EndNum -->
				<xsl:call-template name="PrepCallOutputTarget">
					<xsl:with-param name="TargetType" select="$TargetType"/>
					<xsl:with-param name="StartNum" select="$NewEndNum+1"/>
					<xsl:with-param name="EndNum" select="$EndNum"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputTarget">
					<xsl:with-param name="TargetType" select="$TargetType"/>
					<xsl:with-param name="targetNum" select="$StartNum"/>
					<xsl:with-param name="EndNum" select="$EndNum"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputTarget">
		<xsl:param name="TargetType"/>
		<xsl:param name="targetNum"/>
		<xsl:param name="EndNum"/>

		<!-- Get motion activity id -->
		<xsl:variable name="actID">
			<xsl:choose>
				<xsl:when test="$TargetType='P'">
					<xsl:value-of select="KIRStrUtils:getPVarActID($targetNum)"/>
				</xsl:when>
				<xsl:when test="$TargetType='J'">
					<xsl:value-of select="KIRStrUtils:getJVarActID($targetNum)"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<!-- output target data -->
		<xsl:if test="string-length($actID) &gt; 0">
			<xsl:apply-templates select="Activity[@id=$actID]" mode="CSV">
				<xsl:with-param name="TargetType" select="$TargetType"/>
			</xsl:apply-templates>
		</xsl:if>
		
		<xsl:if test="$targetNum &lt; $EndNum">
			<xsl:call-template name="outputTarget">
				<xsl:with-param name="TargetType" select="$TargetType"/>
				<xsl:with-param name="targetNum" select="$targetNum+1"/>
				<xsl:with-param name="EndNum" select="$EndNum"/>
			</xsl:call-template>
		</xsl:if>

	</xsl:template>
	
	<xsl:template match="Activity" mode="CSV">
		<xsl:param name="TargetType"/>
		
		<!-- For APPROACH command, get the offset -->
		<xsl:variable name="DensoApproach" select="AttributeList/Attribute[AttributeName='DensoApproach']"/>
		<xsl:variable name="approach">
			<xsl:choose>
				<xsl:when test="$DensoApproach">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$DensoApproach/AttributeValue"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- DEPART command -->
		<xsl:variable name="DensoDepart" select="AttributeList/Attribute[AttributeName='DensoDepart']"/>
		<xsl:variable name="precedingMove" select="preceding-sibling::node()[@ActivityType='DNBRobotMotionActivity'][1]"/>
		<xsl:variable name="followingMove" select="following-sibling::node()[@ActivityType='DNBRobotMotionActivity'][1]"/>
		<xsl:variable name="depart">
			<xsl:call-template name="CheckDepartMove">
				<xsl:with-param name="currentMove" select="."/>
				<xsl:with-param name="compareMove" select="$precedingMove"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="departFollowing">
			<xsl:call-template name="CheckDepartMove">
				<xsl:with-param name="currentMove" select="."/>
				<xsl:with-param name="compareMove" select="$followingMove"/>
				<xsl:with-param name="following" select="1"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$DensoDepart and $depart &gt; 0">
				<!-- no target output if the move is translated to DEPART -->
			</xsl:when>
			<xsl:when test="$TargetType='P' and Target/@Default='Cartesian' and Target/CartesianTarget/Tag">
				
				<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
				<xsl:variable name="tagName" select="Target/CartesianTarget/Tag"/>
				<xsl:variable name="TAGNAME">
					<xsl:call-template name="toupper">
						<xsl:with-param name="instring" select="$tagName"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$TAGNAME='HOME'"/> <!-- defined in each PAC file, each program can have a HOME -->
					<xsl:otherwise>
						<xsl:variable name="tagGroupTagName" select="concat($tagGroup,$tagName)"/>
						<xsl:if test="KIRStrUtils:tagUsed($tagGroupTagName)=false">
							<xsl:variable name="dummy" select="KIRStrUtils:addTag($tagGroupTagName)"/>
							<xsl:variable name="index" select="KIRStrUtils:getTagName($tagGroupTagName, '', '')"/>
							<xsl:value-of select="$index"/>
							<xsl:text>,</xsl:text>
							<xsl:call-template name="outputPosData">
								<xsl:with-param name="pos" select="Target/CartesianTarget/Position"/>
								<xsl:with-param name="ori" select="Target/CartesianTarget/Orientation"/>
								<xsl:with-param name="coordinate" select="concat('Tag Target:',$tagGroup,'|',$tagName)"/>
								<xsl:with-param name="activity" select="."/>
								<xsl:with-param name="approach" select="$approach"/>
							</xsl:call-template>
							<xsl:text>,</xsl:text>
							<xsl:call-template name="outputFigure"/>
							<xsl:text>,"",</xsl:text>
							<xsl:value-of select="$cr"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$TargetType='P' and Target/@Default='Joint'">
				<xsl:if test="$rbtAxes &gt; 4">
					<!-- NOT 4-axis robot; Joint target, move is APPROACH or
						 following move is DEPART, write to P.csv
					  -->
					<xsl:if test="$DensoApproach or $departFollowing &gt; 0">
						<xsl:variable name="index" select="KIRStrUtils:getTagName(@id, '', '')"/>
						<xsl:value-of select="$index"/>
						<xsl:text>,</xsl:text>
						<xsl:call-template name="outputPosData">
							<xsl:with-param name="pos" select="Target/CartesianTarget/Position"/>
							<xsl:with-param name="ori" select="Target/CartesianTarget/Orientation"/>
							<xsl:with-param name="coordinate" select="concat('Joint Target:',@Operation,'|Joint')"/>
							<xsl:with-param name="activity" select="."/>
							<xsl:with-param name="approach" select="$approach"/>
						</xsl:call-template>
						<xsl:text>,</xsl:text>
						<xsl:call-template name="outputFigure"/>
						<xsl:text>,"",</xsl:text>
						<xsl:value-of select="$cr"/>
					</xsl:if>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$TargetType='J' and Target/@Default='Joint'">
				<xsl:choose>
					<xsl:when test="$rbtAxes &gt; 4 and ($DensoApproach or $departFollowing &gt; 0)">
						<!-- NOT 4-axis robot; Joint target, move is APPROACH or
							 following move is DEPART, write to P.csv
						  -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="index" select="KIRStrUtils:getTagName(@id, '', '')"/>
						<xsl:value-of select="$index"/>
						<xsl:text>,</xsl:text>
						<xsl:call-template name="outputJntData">
							<xsl:with-param name="approach" select="$approach"/>
							<xsl:with-param name="coordinate" select="concat('TARGET:',@Operation,'|Joint')"/>
						</xsl:call-template>
						<xsl:text>,"",</xsl:text>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- Activity mode='CSV' -->
	
	<xsl:template match="ActivityList" mode="PAC">
		<xsl:variable name="taskName0">
			<xsl:call-template name="toupper">
				<xsl:with-param name="instring" select="@Task"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name="taskName">
			<xsl:call-template name="TrimTrailing_1">
				<xsl:with-param name="instring" select="$taskName0"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE START </xsl:text>
		<xsl:text>PRO</xsl:text>
		<xsl:variable name="progNum" select="NumberIncrement:getProgramNum($taskName)"/>
		<xsl:value-of select="$progNum"/>
		<xsl:text>.PAC</xsl:text>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="title" select="AttributeList/Attribute[AttributeName='TITLE']/AttributeValue"/>
		<xsl:if test="boolean($title)">
			<xsl:text>'!TITLE "</xsl:text>
			<xsl:value-of select="$title"/>
			<xsl:text>"</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:variable name="author" select="AttributeList/Attribute[AttributeName='AUTHOR']/AttributeValue"/>
		<xsl:if test="boolean($author)">
			<xsl:text>'!AUTHOR "</xsl:text>
			<xsl:value-of select="$author"/>
			<xsl:text>"</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>

		<xsl:call-template name="outputDateTime"/>

		<xsl:text>PROGRAM PRO</xsl:text>
		<xsl:value-of select="$progNum"/>
		<xsl:value-of select="$cr"/>

		<!-- check if TAKEARM is there as ATTRIBUTE, from list beginning to the first motion -->
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('TakeArmOutput', 0)"/>
		<xsl:variable name="firstMotionActivity" select="Activity[@ActivityType='DNBRobotMotionActivity'][1]"/>
		<xsl:variable name="uptoFirstMotionActivities" select="$firstMotionActivity | $firstMotionActivity/preceding-sibling::node()"/>
		<xsl:for-each select="$uptoFirstMotionActivities">
			<xsl:for-each select="AttributeList/Attribute">
				<xsl:variable name="attrValUpper">
					<xsl:call-template name="toupper">
						<xsl:with-param name="instring" select="AttributeValue"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- activity attribute -->
				<xsl:if test="starts-with($attrValUpper, 'ROBOT LANGUAGE:TAKEARM')">
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('TakeArmOutput', 1)"/>
					<xsl:value-of select="substring-after(AttributeValue, 'Robot Language:')"/>
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<!-- OperationComments -->
				<xsl:if test="AttributeName='Robot Language' and starts-with($attrValUpper,'TAKEARM')">
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('TakeArmOutput', 1)"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:variable name="takeArmOutput" select="MotomanUtils:GetParameterData('TakeArmOutput')"/>
		<xsl:if test="$takeArmOutput = 0">
			<xsl:choose>
				<xsl:when test="$dofAux &gt; 0">
					<xsl:text>TAKEARM 2</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>TAKEARM 0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$cr"/>
		</xsl:if>

		<!-- TAKEARM sets speed and accel to 100% -->
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Speed', 100)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', 100)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Decel', 100)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Tool', 'Default')"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ToolsUsed', '')"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_ObjectFrame', 'DensoWorkZero')"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ObjectFramesUsed', 'DensoWorkZero')"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('WorkShift', '0')"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('HomePosOutput', 0)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('IOBlockOn', 0)"/>
		<!-- Do not clear tag name table, since there is only one position data file and one joint data file -->
		<!--
		<xsl:value-of select="KIRStrUtils:clearDAT('TagNameTable')"/>
		<xsl:value-of select="KIRStrUtils:clearDAT('TagNameList')"/>
		-->
		<xsl:call-template name="setTargetStartNumber"/>
		<xsl:apply-templates select="Activity[@ActivityType='DNBRobotMotionActivity']" mode="SetPrgTargetName"/>

		<xsl:variable name="largestPnum" select="MotomanUtils:GetParameterData('LargestPositionNumber')"/>
		<NumberIncrement:counternum counter="0"/>
		<xsl:variable name="pNum" select="NumberIncrement:current()"/>
		<xsl:if test="not($largestPnum) or ($largestPnum &lt; $pNum)">
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('LargestPositionNumber', $pNum)"/>
		</xsl:if>
		<xsl:variable name="largestJnum" select="MotomanUtils:GetParameterData('LargestJointNumber')"/>
		<NumberIncrement:counternum counter="1"/>
		<xsl:variable name="jNum" select="NumberIncrement:current()"/>
		<xsl:if test="not($largestJnum) or ($largestJnum &lt; $jNum)">
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('LargestJointNumber', $jNum)"/>
		</xsl:if>
		
		<xsl:apply-templates select="Activity" mode="Instruction"/>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="actOrActListNode" select="."/>
		</xsl:call-template>

		<xsl:text>END</xsl:text>

		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END</xsl:text>
	</xsl:template>
	<!-- end of template ActivityList mode='PAC' -->
	
	<xsl:template name="setTargetStartNumber">
		<!-- Motion activities with Tag targets -->
		<xsl:variable name="tagAct" select="Activity[@ActivityType='DNBRobotMotionActivity' and Target[@Default='Cartesian']/CartesianTarget/Tag]"/>
		<xsl:variable name="tagTgtNum" select="$tagAct/AttributeList/Attribute[AttributeName='TargetNum']/AttributeValue"/>
		<xsl:for-each select="$tagTgtNum">
			<xsl:variable name="num" select="MotomanUtils:GetParameterData('LargestPositionNumber')"/>
			<xsl:if test="number(.) &gt; number($num)">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('LargestPositionNumber', .)"/>
			</xsl:if>
			<xsl:variable name="smallestNum" select="MotomanUtils:GetParameterData('SmallestPositionNumber')"/>
			<xsl:if test="number(.) &lt; number($smallestNum)">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('SmallestPositionNumber', .)"/>
			</xsl:if>
		</xsl:for-each>
		<!-- Tag target counter -->
		<xsl:variable name="largestTagTgtNum" select="MotomanUtils:GetParameterData('LargestPositionNumber')"/>
		<NumberIncrement:counternum counter="0"/>
		<!-- This does NOT work 
		<NumberIncrement:startnum start="string($largestTagTgtNum)"/>
		<NumberIncrement:startnum start="$largestTagTgtNum"/>
		-->
		<!-- This works -->
		<xsl:variable name="dummy" select="NumberIncrement:startnum(number($largestTagTgtNum))"/>
		<NumberIncrement:reset/>

		<!-- Motion activities with Joint targets -->
		<xsl:variable name="jntAct" select="Activity[@ActivityType='DNBRobotMotionActivity' and Target[@Default='Joint']]"/>
		<xsl:variable name="jntTgtNum" select="$jntAct/AttributeList/Attribute[AttributeName='TargetNum']/AttributeValue"/>
		<xsl:for-each select="$jntTgtNum">
			<!-- 'sort' does NOT work
			<xsl:sort data-type="number"/>
			-->
			<xsl:variable name="num" select="MotomanUtils:GetParameterData('LargestJointNumber')"/>
			<xsl:if test="number(.) &gt; number($num)">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('LargestJointNumber', .)"/>
			</xsl:if>
			<xsl:variable name="smallestNum" select="MotomanUtils:GetParameterData('SmallestJointNumber')"/>
			<xsl:if test="number(.) &lt; number($smallestNum)">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('SmallestJointNumber', .)"/>
			</xsl:if>
		</xsl:for-each>
		<!-- 'sort' does NOT work 
		<xsl:variable name="largestJntTgtNum" select="$jntTgtNum[last()]"/>
		-->
		<!-- Joint target counter -->
		<xsl:variable name="largestJntTgtNum" select="MotomanUtils:GetParameterData('LargestJointNumber')"/>
		<NumberIncrement:counternum counter="1"/>
		<xsl:variable name="dummy" select="NumberIncrement:startnum(number($largestJntTgtNum))"/>
		<NumberIncrement:reset/>
	</xsl:template>
	<!-- end of template setTargetStartNumber -->
	
	<xsl:template match="Activity" mode="SetPrgTargetName">
		<!-- For KIRStrUtils:getTagName, first call with parameters (DELMIA_tagName, 'DAT', Denso_targetName)
			 to set table; then call with (DELMIA_tagName, '', '') to get Denso_targetName.
		 -->
		<!-- for DEPART command, skip variable counting -->
		<xsl:variable name="DensoDepart" select="AttributeList/Attribute[AttributeName='DensoDepart']"/>
		<xsl:variable name="targetNum" select="AttributeList/Attribute[AttributeName='TargetNum']/AttributeValue"/>
		<xsl:variable name="precedingMove" select="preceding-sibling::node()[@ActivityType='DNBRobotMotionActivity'][1]"/>
		<xsl:variable name="followingMove" select="following-sibling::node()[@ActivityType='DNBRobotMotionActivity'][1]"/>
		<xsl:variable name="depart">
			<xsl:call-template name="CheckDepartMove">
				<xsl:with-param name="currentMove" select="."/>
				<xsl:with-param name="compareMove" select="$precedingMove"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="departFollowing">
			<xsl:call-template name="CheckDepartMove">
				<xsl:with-param name="currentMove" select="."/>
				<xsl:with-param name="compareMove" select="$followingMove"/>
				<xsl:with-param name="following" select="1"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:if test="Target/CartesianTarget/@Status = 'Unreachable'">
			<xsl:call-template name="WriteWarningMessages">
				<xsl:with-param name="warning" select="'WARNING: '"/>
				<xsl:with-param name="line1" select="concat('Activity &lt;', ActivityName, '&gt; target is Unreachable.')"/>
				<xsl:with-param name="line2" select="'Target position downloaded may not be correct.'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$DensoDepart and $depart &gt; 0">
				<!-- skip variable counting -->
			</xsl:when>
			<xsl:when test="Target/@Default='Cartesian' and Target/CartesianTarget/Tag">
				<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
				<xsl:variable name="tagName" select="Target/CartesianTarget/Tag"/>
				<xsl:variable name="TAGNAME">
					<xsl:call-template name="toupper">
						<xsl:with-param name="instring" select="$tagName"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$TAGNAME='HOME'">
					</xsl:when>
					<xsl:when test="$targetNum">
						<xsl:variable name="dummy" select="KIRStrUtils:getTagName(concat($tagGroup,$tagName), 'DAT', $targetNum)"/>
						<xsl:variable name="actId" select="KIRStrUtils:setPVarActID($targetNum, @id)"/>
						<xsl:if test="$actId != ''">
							<xsl:variable name="pActName" select="preceding-sibling::node()[@id=$actId]/ActivityName"/>
							<xsl:call-template name="WriteWarningMessages">
								<xsl:with-param name="warning" select="'WARNING: '"/>
								<xsl:with-param name="line1" select="concat('Activity &lt;', ActivityName, '&gt;:')"/>
								<xsl:with-param name="line2" select="concat('Attribute TargetNum &lt;', $targetNum, '&gt; is previously set to Activity &lt;', $pActName, '&gt;.')"/>
								<xsl:with-param name="line3" select="concat('Position data of Activity &lt;', $pActName, '&gt; is output.')"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<NumberIncrement:counternum counter="0"/>
						<xsl:variable name="curNum" select="NumberIncrement:current()"/>
						<xsl:variable name="prgPosIndex" select="$curNum+1"/>
						<xsl:variable name="tagNameUnique" select="KIRStrUtils:getTagName(concat($tagGroup,$tagName), 'DAT', $prgPosIndex)"/>
						<xsl:if test="$tagNameUnique">
							<xsl:variable name="dummy" select="NumberIncrement:next()"/>
							<xsl:variable name="dummy" select="KIRStrUtils:setPVarActID($prgPosIndex, @id)"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="Target/@Default='Joint'">
				<xsl:variable name="DensoApproach" select="AttributeList/Attribute[AttributeName='DensoApproach']"/>
				<xsl:variable name="changeToPosVar">
					<xsl:choose>
						<xsl:when test="$rbtAxes &gt; 4 and ($DensoApproach or $departFollowing &gt; 0)">
							<!-- position variable counter -->
							<NumberIncrement:counternum counter="0"/>
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:otherwise>
							<!-- joint variable counter -->
							<NumberIncrement:counternum counter="1"/>
							<xsl:value-of select="0"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$targetNum and $changeToPosVar = 0">
						<xsl:variable name="dummy" select="KIRStrUtils:getTagName(@id, 'DAT', $targetNum)"/>
						<xsl:variable name="actId" select="KIRStrUtils:setJVarActID($targetNum, @id)"/>
						<xsl:if test="$actId != ''">
							<xsl:variable name="pActName" select="preceding-sibling::node()[@id=$actId]/ActivityName"/>
							<xsl:call-template name="WriteWarningMessages">
								<xsl:with-param name="warning" select="'WARNING: '"/>
								<xsl:with-param name="line1" select="concat('Activity &lt;', ActivityName, '&gt;:')"/>
								<xsl:with-param name="line2" select="concat('Attribute TargetNum &lt;', $targetNum, '&gt; is previously set to Activity &lt;', $pActName, '&gt;.')"/>
								<xsl:with-param name="line3" select="concat('Joint data of Activity &lt;', $pActName, '&gt; is output.')"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="curNum" select="NumberIncrement:current()"/>
						<xsl:variable name="prgJntIndex" select="$curNum+1"/>
						<xsl:variable name="tagNameUnique" select="KIRStrUtils:getTagName(@id, 'DAT', $prgJntIndex)"/>
						<xsl:if test="$tagNameUnique">
							<xsl:variable name="dummy" select="NumberIncrement:next()"/>
							<xsl:choose>
								<xsl:when test="$changeToPosVar = 0">
									<xsl:variable name="dummy" select="KIRStrUtils:setJVarActID($prgJntIndex, @id)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="dummy" select="KIRStrUtils:setPVarActID($prgJntIndex, @id)"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="$rbtAxes &gt; 4">
								<xsl:if test="$DensoApproach">
									<xsl:call-template name="WriteWarningMessages">
										<xsl:with-param name="warning" select="'WARNING: '"/>
										<xsl:with-param name="line1" select="'Joint Target move intended to download as APPROACH command is only supported for 4-axis robots.'"/>
										<xsl:with-param name="line2" select="concat('Operation &lt;', @Operation, '&gt; target is downloaded as position variable ', 'P[', $prgJntIndex, ']')"/>
									</xsl:call-template>
								</xsl:if>
								<xsl:if test="$departFollowing &gt; 0">
									<xsl:call-template name="WriteWarningMessages">
										<xsl:with-param name="warning" select="'WARNING: '"/>
										<xsl:with-param name="line1" select="'Joint Target move preceding a move intended to download as DEPART command is only supported for 4-axis robots.'"/>
										<xsl:with-param name="line2" select="concat('Operation &lt;', @Operation, '&gt; target is downloaded as position variable ', 'P[', $prgJntIndex, ']')"/>
									</xsl:call-template>
								</xsl:if>
							</xsl:if>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template> <!-- template match="Activity" mode="SetPrgTargetName" -->
	
	<!-- If IOBlock is not ON, output 'IOBLOCK ON' -->
	<xsl:template name="outputIOBlockOn">
		<xsl:variable name="ioblock" select="MotomanUtils:GetParameterData('IOBlockOn')"/>
		<xsl:if test="$ioblock = 0">
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('IOBlockOn', 1)"/>
			<xsl:text>IOBLOCK ON</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	
	<!-- Output 'IOBLOCK OFF' if no following SET IO, WAIT IO, or commented WAIT IO in Operation
		 includeself is 1 when processing commented out WaitIO of SetIO activity
	-->
	<xsl:template name="outputIOBlockOff">
		<xsl:param name="act"/>
		<xsl:param name="attr"/>
		<xsl:param name="includeself" select="0"/>
		<!-- following IO activities -->
		<xsl:variable name="fSetIO" select="$act/following-sibling::node()[@ActivityType='DNBSetSignalActivity']"/>
		<!-- NodeSet selection only works on 1 line. WaitIO is uploaded as Attribute of SetIO -->
		<xsl:variable name="fSetIO_Self" select="$act | $act/following-sibling::node()[@ActivityType='DNBSetSignalActivity']"/>
		<xsl:variable name="fWaitIO" select="$act/following-sibling::node()[@ActivityType='DNBWaitSignalActivity']"/>
		<!-- following Operations may have commented WAIT IO -->
		<xsl:variable name="fOp" select="$act/following-sibling::node()[@ActivityType='Operation']"/>
		<xsl:variable name="fOpRbtLang" select="$fOp/AttributeList/Attribute[AttributeName='Robot Language']"/>
		<!-- following Activities may have commented WAIT IO -->
		<xsl:variable name="fActRbtLang" select="$act/following-sibling::node()/AttributeList/Attribute[starts-with(AttributeValue, 'Robot Language:')]"/>
		<!-- following Attributes may have commented WAIT IO -->
		<xsl:variable name="fRbtLang" select="$attr/following-sibling::node()[starts-with(AttributeValue, 'Robot Language:')]"/>
		<!-- (preceding) ActivityList/AttributeList/Attribute nodeset -->
		<xsl:variable name="pAttrListAttr" select="$act/preceding-sibling::node()[last()]/Attribute"/>

		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('FollowingCommentedWaitIO', 0)"/>
		<xsl:for-each select="$fOpRbtLang">
			<xsl:variable name="attrValUpper">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="AttributeValue"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="starts-with($attrValUpper,'WAIT IO')">
				<xsl:variable name="numWaitIO" select="MotomanUtils:GetParameterData('FollowingCommentedWaitIO')"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('FollowingCommentedWaitIO', number($numWaitIO)+1)"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="$fActRbtLang">
			<xsl:variable name="attrValUpper">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="substring-after(AttributeValue, 'Robot Language:')"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="starts-with($attrValUpper,'WAIT IO')">
				<xsl:variable name="numWaitIO" select="MotomanUtils:GetParameterData('FollowingCommentedWaitIO')"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('FollowingCommentedWaitIO', number($numWaitIO)+1)"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="$fRbtLang">
			<xsl:variable name="attrValUpper">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="substring-after(AttributeValue, 'Robot Language:')"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="starts-with($attrValUpper,'WAIT IO')">
				<xsl:variable name="numWaitIO" select="MotomanUtils:GetParameterData('FollowingCommentedWaitIO')"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('FollowingCommentedWaitIO', number($numWaitIO)+1)"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="$pAttrListAttr">
			<xsl:variable name="attrValUpper">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="substring-after(AttributeValue, 'Robot Language:')"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="starts-with(AttributeName, 'PostComment') and starts-with($attrValUpper,'WAIT IO')">
				<xsl:variable name="numWaitIO" select="MotomanUtils:GetParameterData('FollowingCommentedWaitIO')"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('FollowingCommentedWaitIO', number($numWaitIO)+1)"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:variable name="numCommentedWaitIO" select="MotomanUtils:GetParameterData('FollowingCommentedWaitIO')"/>

		<xsl:variable name="followingIO">
			<xsl:choose>
				<xsl:when test="$includeself=1">
					<xsl:value-of select="count($fSetIO_Self) + count($fWaitIO) + number($numCommentedWaitIO)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="count($fSetIO) + count($fWaitIO) + number($numCommentedWaitIO)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$followingIO &gt; 0"/>
			<xsl:otherwise> <!-- no more IO activity or commented WAIT IO, output IOBLOCK OFF -->
				<xsl:variable name="ioblock" select="MotomanUtils:GetParameterData('IOBlockOn')"/>
				<xsl:if test="$ioblock = 1">
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('IOBlockOn', 0)"/>
					<xsl:text>IOBLOCK OFF</xsl:text>
					<xsl:value-of select="$cr"/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="Activity" mode="Instruction">
		<xsl:variable name="setSignalPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
		<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
		<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
		<xsl:variable name="setSignalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>

		<xsl:variable name="waitSignalPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
		<xsl:variable name="waitSignalName" select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
		<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
		<xsl:variable name="waitSignalMaxWaitTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>

		<xsl:choose>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity' and MotionAttributes/MotionType='Circular'">
				<!-- A CircularVia move and a Circular move together are necessary to create a Denso "MOVE C".
					 PreComment to Circular should NOT be output here as it will break
					 the Denso "MOVE C, P1, P2" command line.
				  -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="processComments">
					<xsl:with-param name="prefix" select="'Pre'"/>
					<xsl:with-param name="actOrActListNode" select="."/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:choose>
			<xsl:when test="@ActivityType='Operation'">
				<xsl:call-template name="outputOperationAttribute"/>
			</xsl:when>
			<xsl:when test="@ActivityType='DelayActivity'">
				<xsl:text>DELAY </xsl:text>
				<xsl:variable name="start">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="ActivityTime/StartTime"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="end">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="ActivityTime/EndTime"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="format-number(1000*($end - $start), $patInteger)"/>
				<xsl:call-template name="outputInlineComment"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBSetSignalActivity'">
				<xsl:call-template name="outputIOBlockOn"/>
				<xsl:choose>
					<xsl:when test="$setSignalValue='On'">SET</xsl:when>
					<xsl:when test="$setSignalValue='Off'">RESET</xsl:when>
					<xsl:otherwise>RESET</xsl:otherwise>
				</xsl:choose>
				<xsl:text> IO[</xsl:text>
				<xsl:value-of select="$setSignalPortNumber"/>
				<xsl:text>]</xsl:text>
				<xsl:if test="$setSignalValue='On' and $setSignalDuration != 0">
					<xsl:text>, </xsl:text>
					<xsl:value-of select="number(1000 * $setSignalDuration)"/>
				</xsl:if>
				<xsl:call-template name="outputInlineComment"/>
				<xsl:value-of select="$cr"/>
				<xsl:call-template name="outputIOBlockOff">
					<xsl:with-param name="act" select="."/>
					<xsl:with-param name="attr" select="AttributeList/Attribute[starts-with(AttributeValue, 'Robot Language:')][last()]"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
				<xsl:call-template name="outputIOBlockOn"/>
				<xsl:text>WAIT IO[</xsl:text>
				<xsl:value-of select="$waitSignalPortNumber"/>
				<xsl:text>]=</xsl:text>
				<xsl:choose>
					<xsl:when test="$waitSignalValue='On'">ON</xsl:when>
					<xsl:otherwise>OFF</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$waitSignalMaxWaitTime &gt; 0">
					<xsl:text>, </xsl:text>
					<xsl:value-of select="number(1000 * $waitSignalMaxWaitTime)"/>
					<!-- storage variable is valid only with TimeOut -->
					<xsl:variable name="storageVar" select="AttributeList/Attribute[AttributeName='StorageVariable']"/>
					<xsl:if test="$storageVar">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="$storageVar/AttributeValue"/>
					</xsl:if>
				</xsl:if>
				<xsl:call-template name="outputInlineComment"/>
				<xsl:value-of select="$cr"/>
				<xsl:call-template name="outputIOBlockOff">
					<xsl:with-param name="act" select="."/>
					<xsl:with-param name="attr" select="AttributeList/Attribute[starts-with(AttributeValue, 'Robot Language:')][last()]"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
				<xsl:variable name="robotTaskToCall">
					<xsl:call-template name="toupper">
						<xsl:with-param name="instring" select="CallName"/>
					</xsl:call-template>
				</xsl:variable>
		
				<xsl:variable name="taskName">
					<xsl:call-template name="TrimTrailing_1">
						<xsl:with-param name="instring" select="$robotTaskToCall"/>
					</xsl:call-template>
				</xsl:variable>
				
				<xsl:text>CALL PRO</xsl:text>
				<xsl:value-of select="NumberIncrement:getProgramNum($taskName)"/>
				<xsl:call-template name="outputInlineComment"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
				<xsl:variable name="moType" select="MotionAttributes/MotionType"/>
				<xsl:variable name="toolProfile" select="MotionAttributes/ToolProfile"/>
				<xsl:variable name="objFrameProfile" select="MotionAttributes/ObjectFrameProfile"/>
				<xsl:variable name="motionProfile" select="MotionAttributes/MotionProfile"/>
				<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
				<!--
				<xsl:variable name="storedAccel" select="MotomanUtils:GetParameterData('STORED_Accel')"/>
				<xsl:if test="number($storedAccel) &lt; 0">
					<xsl:variable name="controller" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller"/>
					<xsl:variable name="controllerMotionProfile" select="$controller/MotionProfileList/MotionProfile[Name=$motionProfile]"/>
					<xsl:variable name="speedValue" select="$controllerMotionProfile/Speed/@Value"/>
					<xsl:variable name="accel" select="number($speedValue * $speedValue div 100)"/>
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', $accel)"/>
				</xsl:if>
				-->
				<!-- preceding motion -->
				
				<xsl:variable name="precedingMove" select="preceding-sibling::node()[@ActivityType='DNBRobotMotionActivity'][1]"/>
				<xsl:variable name="followingMove" select="following-sibling::node()[@ActivityType='DNBRobotMotionActivity'][1]"/>
				<xsl:variable name="depart">
					<xsl:call-template name="CheckDepartMove">
						<xsl:with-param name="currentMove" select="."/>
						<xsl:with-param name="compareMove" select="$precedingMove"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="departFollowing">
					<xsl:call-template name="CheckDepartMove">
						<xsl:with-param name="currentMove" select="."/>
						<xsl:with-param name="compareMove" select="$followingMove"/>
						<xsl:with-param name="following" select="1"/>
					</xsl:call-template>
				</xsl:variable>

				<xsl:if test="$moType!='Circular'">
					<xsl:call-template name="outputTool">
						<xsl:with-param name="toolName" select="$toolProfile"/>
					</xsl:call-template>
	
					<!-- Output object profile only for Cartesian and Tag targets(Denso WORK/CHANGEWORK) -->
					<!-- xsl:if test="Target/@Default='Cartesian'" -->
						<xsl:call-template name="outputObjectFrame">
							<xsl:with-param name="activity" select="."/>
						</xsl:call-template>
					<!-- /xsl:if -->
				</xsl:if>
				<!-- Unit: MM -->
				<xsl:variable name="DensoApproach" select="AttributeList/Attribute[AttributeName='DensoApproach']"/>
				<xsl:variable name="DensoDepart" select="AttributeList/Attribute[AttributeName='DensoDepart']"/>
				
				<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
				<xsl:variable name="tagName" select="Target/CartesianTarget/Tag"/>
				<xsl:variable name="TAGNAME">
					<xsl:call-template name="toupper">
						<xsl:with-param name="instring" select="$tagName"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$DensoApproach">
						<xsl:text>APPROACH </xsl:text>
					</xsl:when>
					<xsl:when test="$DensoDepart and $depart &gt; 0">
						<xsl:text>DEPART </xsl:text>
					</xsl:when>
					<xsl:when test="$TAGNAME='HOME'">
						<!-- For GOHOME command before any move, speed/accel need to be output separately -->
						<xsl:call-template name="outputSpeedAccelLines">
							<xsl:with-param name="motionProfile" select="$motionProfile"/>
							<xsl:with-param name="attributelist" select="AttributeList"/>
						</xsl:call-template>
						
						<xsl:variable name="homePosOutput" select="MotomanUtils:GetParameterData('HomePosOutput')"/>
						<xsl:if test="$homePosOutput=0">
							<xsl:text>HOME (</xsl:text>
							<xsl:call-template name="outputPosData">
								<xsl:with-param name="pos" select="Target/CartesianTarget/Position"/>
								<xsl:with-param name="ori" select="Target/CartesianTarget/Orientation"/>
								<xsl:with-param name="coordinate" select="concat('Tag Target:',$tagGroup,'|',$tagName)"/>
								<xsl:with-param name="activity" select="."/>
							</xsl:call-template>
							<xsl:text>,</xsl:text>
							<xsl:call-template name="outputFigure"/>
							<xsl:text>)</xsl:text>
							<xsl:value-of select="$cr"/>
						</xsl:if>
						<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('HomePosOutput', 1)"/>
						<xsl:text>GOHOME</xsl:text>
					</xsl:when>
					<xsl:when test="$moType!='Circular'">
						<xsl:text>MOVE </xsl:text>
					</xsl:when>
				</xsl:choose>
				<!-- There can only be 1 CircularVia move followed immediately by 1 Circular move -->
				<xsl:variable name="nextMoAct" select="following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
				<xsl:variable name="nextMoType" select="$nextMoAct/MotionAttributes/MotionType"/>
				<xsl:if test="$moType='CircularVia' and $nextMoType!='Circular'">
					<xsl:call-template name="WriteWarningMessages">
						<xsl:with-param name="warning" select="'ERROR: '"/>
						<xsl:with-param name="line1" select="'A CircularVia motion activity must be followed immediately by a Circular motion activity'"/>
						<xsl:with-param name="line2" select="'to produce a valid Denso arc move command.'"/>
					</xsl:call-template>
				</xsl:if>
				<!-- Interpolation -->
				<xsl:choose>
					<xsl:when test="$TAGNAME='HOME'">
						<xsl:if test="$moType != 'Joint'">
							<!-- Warning message,  -->
							<xsl:call-template name="WriteWarningMessages">
								<xsl:with-param name="warning" select="'WARNING: '"/>
								<xsl:with-param name="line1" select="'When Tag target name is &lt;HOME&gt;, the Move is downloaded as Denso GOHOME command.'"/>
								<xsl:with-param name="line2" select="'However this Move is not in Joint motion.'"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$moType='Linear'">
								<xsl:text>L, </xsl:text>
							</xsl:when>
							<xsl:when test="$moType='Joint'">
								<xsl:text>P, </xsl:text>
							</xsl:when>
							<xsl:when test="$moType='CircularVia'">
								<xsl:text>C, </xsl:text>
							</xsl:when>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<!-- Accuracy -->
				<xsl:choose>
					<xsl:when test="$DensoApproach"/>
					<xsl:when test="$DensoDepart and $depart &gt; 0"/>
					<xsl:when test="$TAGNAME='HOME'"/>
					<xsl:when test="$moType='CircularVia'"/>
					<xsl:otherwise>
						<xsl:call-template name="outputAccuracy">
							<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				<!-- Target -->
				<xsl:variable name="approach">
					<xsl:choose>
						<xsl:when test="$DensoApproach">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="$DensoApproach/AttributeValue"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$DensoDepart and $depart &gt; 0"/>
					<xsl:when test="Target/@Default='Cartesian' and $tagName">
						<xsl:choose>
							<xsl:when test="$TAGNAME = 'HOME'"/>
							<xsl:otherwise>
								<!-- Combine TagGroupName and TagName, since tag name is unique in a tag group -->
								<!-- In case a tag is re-used, another position variable won't be created -->
								<xsl:variable name="index" select="KIRStrUtils:getTagName(concat($tagGroup,$tagName), '', '')"/>
								<xsl:value-of select="concat('P[', $index, ']')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="Target/@Default='Joint'">
						<xsl:variable name="index" select="KIRStrUtils:getTagName(@id, '', '')"/>
						<xsl:choose>
							<xsl:when test="$rbtAxes &gt; 4 and ($DensoApproach or $departFollowing &gt; 0)">
								<!-- Joint target move downloads to APPROACH with position variable for 5- & 6-axis robots -->
								<xsl:value-of select="concat('P[', $index, ']')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="concat('J[', $index, ']')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise> <!-- Cartesian target -->
						<xsl:text>(</xsl:text>
						<xsl:call-template name="outputPosData">
							<xsl:with-param name="pos" select="Target/CartesianTarget/Position"/>
							<xsl:with-param name="ori" select="Target/CartesianTarget/Orientation"/>
							<xsl:with-param name="coordinate" select="concat('Cartesian Target:',@Operation,'|Cartesian')"/>
							<xsl:with-param name="activity" select="."/>
							<xsl:with-param name="approach" select="$approach"/>
						</xsl:call-template>
						<xsl:text>,</xsl:text>
						<xsl:call-template name="outputFigure"/>
						<xsl:text>)</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<!-- Approach or Depart length -->
				<xsl:choose>
					<xsl:when test="$DensoApproach">
						<xsl:text>, </xsl:text>
						<xsl:call-template name="outputAccuracy">
							<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
						</xsl:call-template>
						<xsl:value-of select="format-number($approach, $patUptoD2)"/>
					</xsl:when>
					<xsl:when test="$DensoDepart and $depart &gt; 0">
						<xsl:call-template name="outputAccuracy">
							<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
						</xsl:call-template>
						<xsl:value-of select="format-number($depart, $patUptoD2)"/>
					</xsl:when>
					<xsl:when test="$TAGNAME='HOME'"/> <!-- no aux for GOHOME -->
					<xsl:when test="$moType != 'CircularVia'">
						<xsl:call-template name="outputAuxData"/>
					</xsl:when>
				</xsl:choose>
				<!-- motion attributes -->
				<xsl:choose>
					<xsl:when test="$TAGNAME='HOME'">
						<xsl:call-template name="outputInlineComment"/>
					</xsl:when>
					<xsl:when test="$moType='CircularVia'">
						<xsl:text>, </xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="outputSpeed">
							<xsl:with-param name="motionProfile" select="$motionProfile"/>
						</xsl:call-template>
						<xsl:call-template name="outputAccel">
							<xsl:with-param name="motionProfile" select="$motionProfile"/>
						</xsl:call-template>
						<xsl:call-template name="outputDecel">
							<xsl:with-param name="attributelist" select="AttributeList"/>
						</xsl:call-template>
						<xsl:variable name="DensoOptionAttribute" select="AttributeList/Attribute[AttributeName='DensoOption']"/>
						<xsl:if test="$DensoOptionAttribute">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="$DensoOptionAttribute/AttributeValue"/>
						</xsl:if>
						<xsl:call-template name="outputInlineComment"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$moType!='CircularVia'">
					<xsl:value-of select="$cr"/>
				</xsl:if>
				<!-- 'Robot Language:TAKEARM' attribute of activities upto the first motion activity are output right after
						the PROGRAM line. After processing the first motion activity, set parameter TakeArmOutput to 0
						in case there are other TAKEARM uploaded
					First motion activity(no preceding ones) -->
				<xsl:if test="string(not(preceding::Activity))='true'">
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('TakeArmOutput', 0)"/>
				</xsl:if>
			</xsl:when><!-- DNBRobotMotionActivity -->
		</xsl:choose>
	</xsl:template> <!-- template match="Activity" mode="Instruction" -->

	<xsl:template name="outputInlineComment">
		<xsl:variable name="InlineCommentAttribute" select="AttributeList/Attribute[AttributeName='InlineComment']"/>
		<xsl:if test="$InlineCommentAttribute">
			<xsl:text> '</xsl:text>
			<xsl:value-of select="$InlineCommentAttribute/AttributeValue"/>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="CheckDepartMove">
		<xsl:param name="currentMove"/>
		<xsl:param name="compareMove"/>
		<xsl:param name="following" select="0"/>
		
		<xsl:choose>
			<xsl:when test="$following=1 and boolean($compareMove/AttributeList/Attribute[AttributeName='DensoDepart'])=false">
				<xsl:value-of select="-3"/>
			</xsl:when>
			<xsl:when test="$compareMove">
				<xsl:variable name="target">
					<xsl:choose>
						<xsl:when test="$following = 1">
							<xsl:call-template name="GetPositionString">
								<xsl:with-param name="pos" select="$compareMove/Target/CartesianTarget/Position"/>
								<xsl:with-param name="ori" select="$compareMove/Target/CartesianTarget/Orientation"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="GetPositionString">
								<xsl:with-param name="pos" select="$currentMove/Target/CartesianTarget/Position"/>
								<xsl:with-param name="ori" select="$currentMove/Target/CartesianTarget/Orientation"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="target0">
					<xsl:choose>
						<xsl:when test="$following = 1">
							<xsl:call-template name="GetPositionString">
								<xsl:with-param name="pos" select="$currentMove/Target/CartesianTarget/Position"/>
								<xsl:with-param name="ori" select="$currentMove/Target/CartesianTarget/Orientation"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="GetPositionString">
								<xsl:with-param name="pos" select="$compareMove/Target/CartesianTarget/Position"/>
								<xsl:with-param name="ori" select="$compareMove/Target/CartesianTarget/Orientation"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="mat" select="MatrixUtils:dgXyzyprToMatrix($target)"/>
				<xsl:variable name="targetInvert" select="MatrixUtils:dgInvert()"/>
				<xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix($target0)"/>
				
				<xsl:variable name="x">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="y">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="z">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="w">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetYaw()"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="p">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetPitch()"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="r">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetRoll()"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="sqSum" select="$x*$x+$y*$y+$w*$w+$p*$p+$r*$r"/>
				<xsl:choose>
					<xsl:when test="$sqSum &lt; 0.001">
						<xsl:choose>
							<xsl:when test="$rbtAxes=4">
								<xsl:value-of select="-$z"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$z"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>-1</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>-2</xsl:otherwise>
		</xsl:choose>
	</xsl:template> <!-- CheckDepartMove -->
	
	<xsl:template name="IsObjectZero">
		<xsl:param name="pos"/>
		<xsl:param name="ori"/>
		
		<xsl:variable name="x">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="y">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="z">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="W">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="P">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="R">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Roll"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="sqSum" select="$x*$x + $y*$y + $z*$z + $W*$W + $P*$P + $R*$R"/>
		<xsl:choose>
			<xsl:when test="$sqSum &lt; 0.001">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:template> <!-- IsObjectZero -->
	
	<xsl:template name="GetPositionString">
		<xsl:param name="pos"/>
		<xsl:param name="ori"/>
		
		<xsl:variable name="x">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="y">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="z">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="W">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="P">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="R">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Roll"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="concat(number(1000*$x), ',', number(1000*$y), ',', number(1000*$z), ',', $W, ',', $P, ',', $R)"/>
	</xsl:template> <!-- GetPositionString -->
	
	<xsl:template name="CheckYawPitch">
		<xsl:param name="ori"/>
		<xsl:param name="coordinate"/>
		
		<xsl:variable name="W">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="P">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="($W*$W+$P*$P) &gt; 0.001">
			<xsl:call-template name="WriteWarningMessages">
				<xsl:with-param name="warning" select="'ERROR: '"/>
				<xsl:with-param name="line1" select="concat(substring-before($coordinate, ':'), ' &lt;', substring-after($coordinate, ':'), '&gt; cannot have non-zero Yaw or Pitch.')"/>
			</xsl:call-template>
		</xsl:if>
		
	</xsl:template>
	
	<xsl:template name="outputPosData">
		<xsl:param name="pos"/>
		<xsl:param name="ori"/>
		<xsl:param name="coordinate"/>
		<xsl:param name="activity"/>
		<xsl:param name="approach" select="0"/>
		<!-- parameter 'coordinate' is used to output error message if Target
		     has non-zero Yaw or Pitch for 4-Axis robots.
			       Tag Target:TagGroup|TagName		for tag target
			 Cartesian Target:@Operation|Cartesian	for Cartesian target
			     Joint Target:@Operation|Joint		for Joint target used in motion intended as APPROCH
		  -->
		<!-- CheckYawPitch calls WriteWarningMessages and cannot be assigned to a variable -->
		<xsl:if test="$rbtAxes = 4">
			<xsl:call-template name="CheckYawPitch">
				<xsl:with-param name="ori" select="$ori"/>
				<xsl:with-param name="coordinate" select="$coordinate"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="approachVar">
			<xsl:choose>
				<xsl:when test="$rbtAxes = 4">
					<xsl:value-of select="-$approach"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$approach"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="base" select="$activity/Target/BaseWRTWorld"/>
		<xsl:variable name="baseXyzwpr">
			<xsl:call-template name="GetPositionString">
				<xsl:with-param name="pos" select="$base/Position"/>
				<xsl:with-param name="ori" select="$base/Orientation"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="objname" select="$activity/MotionAttributes/ObjectFrameProfile"/>
		<xsl:variable name="obj" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objname]/ObjectFrame"/>
		<xsl:variable name="objXyzwpr">
			<xsl:call-template name="GetPositionString">
				<xsl:with-param name="pos" select="$obj/ObjectFramePosition"/>
				<xsl:with-param name="ori" select="$obj/ObjectFrameOrientation"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="objZero">
			<xsl:call-template name="IsObjectZero">
				<xsl:with-param name="pos" select="$obj/ObjectFramePosition"/>
				<xsl:with-param name="ori" select="$obj/ObjectFrameOrientation"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="tgtXyzwpr">
			<xsl:call-template name="GetPositionString">
				<xsl:with-param name="pos" select="$pos"/>
				<xsl:with-param name="ori" select="$ori"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$objZero = 1">
				<!-- ObjectFrame = 0, position is relative to robot base -->
				<xsl:variable name="tgt_WRT_base" select="MatrixUtils:dgXyzyprToMatrix($tgtXyzwpr)"/>
			</xsl:when>
			<xsl:when test="starts-with($coordinate, 'Cartesian Target:')">
				<!-- Cartesian target, position is relative to robot base -->
				<xsl:variable name="tgt_WRT_base" select="MatrixUtils:dgXyzyprToMatrix($tgtXyzwpr)"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- ObjectFrame != 0, position is relative to ObjectFrame -->
				<xsl:variable name="baseMat" select="MatrixUtils:dgXyzyprToMatrix($baseXyzwpr)"/>
				<xsl:variable name="baseMatInv" select="MatrixUtils:dgInvert()"/>
				<xsl:variable name="obj_WRT_base" select="MatrixUtils:dgCatXyzyprMatrix($objXyzwpr)"/>
				<xsl:variable name="tgt_WRT_base" select="MatrixUtils:dgCatXyzyprMatrix($tgtXyzwpr)"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="approachMat" select="concat('0,0,', $approachVar, ',0,0,0')"/>
		<xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix($approachMat)"/>

		<xsl:variable name="targetPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="targetPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="targetPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="targetOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="MatrixUtils:dgGetYaw()"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="targetOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="MatrixUtils:dgGetPitch()"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="targetOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="MatrixUtils:dgGetRoll()"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="format-number($targetPositionX, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($targetPositionY, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($targetPositionZ, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:if test="$rbtAxes &gt; 4">
			<xsl:value-of select="format-number($targetOrientationYaw, $patUptoD2)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetOrientationPitch, $patUptoD2)"/>
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:value-of select="format-number($targetOrientationRoll, $patUptoD2)"/>
	</xsl:template> <!-- outputPosData -->
	
	<xsl:template name="outputToolData">
		<xsl:param name="pos"/>
		<xsl:param name="ori"/>
		<xsl:param name="coordinate"/>
		<!-- parameter 'coordinate' is used to output error message if Tool/Object
		     has non-zero Yaw or Pitch for 4-Axis robots.
			 TOOL:ToolName			for tool profile
			 OBJECT:ObjectFrame		for object frame profile
		  -->
		<!-- CheckYawPitch calls WriteWarningMessages and cannot be assigned to a variable -->
		<xsl:if test="$rbtAxes = 4">
			<xsl:call-template name="CheckYawPitch">
				<xsl:with-param name="ori" select="$ori"/>
				<xsl:with-param name="coordinate" select="$coordinate"/>
			</xsl:call-template>
		</xsl:if>
		
		<xsl:variable name="x">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="y">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="z">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="W">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="P">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="R">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Roll"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:value-of select="format-number($x * 1000, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($y * 1000, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($z * 1000, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:if test="$rbtAxes &gt; 4">
			<xsl:value-of select="format-number($W, $patUptoD2)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($P, $patUptoD2)"/>
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:value-of select="format-number($R, $patUptoD2)"/>
	</xsl:template> <!-- outputToolData -->

	<xsl:template name="outputWorkData">
		<xsl:param name="pos"/>
		<xsl:param name="ori"/>
		<xsl:param name="coordinate"/>
		<xsl:param name="activity"/>
		<!-- parameter 'coordinate' is used to output error message if Tool/Object
		     has non-zero Yaw or Pitch for 4-Axis robots.
			 TOOL:ToolName			for tool profile
			 OBJECT:ObjectFrame		for object frame profile
		  -->
		<!-- CheckYawPitch calls WriteWarningMessages and cannot be assigned to a variable -->
		<xsl:if test="$rbtAxes = 4">
			<xsl:call-template name="CheckYawPitch">
				<xsl:with-param name="ori" select="$ori"/>
				<xsl:with-param name="coordinate" select="$coordinate"/>
			</xsl:call-template>
		</xsl:if>
		<!-- ObjectFrame -->
		<xsl:variable name="x">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="y">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="z">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="W">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="P">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="R">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Roll"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Activity/Target/BaseWRTWorld -->
		<xsl:variable name="BaseX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$activity/Target/BaseWRTWorld/Position/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="BaseY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$activity/Target/BaseWRTWorld/Position/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="BaseZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$activity/Target/BaseWRTWorld/Position/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="BaseW">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$activity/Target/BaseWRTWorld/Orientation/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="BaseP">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$activity/Target/BaseWRTWorld/Orientation/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="BaseR">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$activity/Target/BaseWRTWorld/Orientation/@Roll"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Transform to get Object Frame wrt Robot -->
		<xsl:variable name="BaseXyzwpr" select="concat($BaseX, ',', $BaseY, ',', $BaseZ, ',', $BaseW, ',', $BaseP, ',', $BaseR)"/>
		<xsl:variable name="BaseMat" select="MatrixUtils:dgXyzyprToMatrix($BaseXyzwpr)"/>
		<xsl:variable name="BaseMatInv" select="MatrixUtils:dgInvert()"/>
		<xsl:variable name="Xyzwpr" select="concat($x, ',', $y, ',', $z, ',', $W, ',', $P, ',', $R)"/>
		<xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix($Xyzwpr)"/>
		
		<xsl:variable name="objX" select="MatrixUtils:dgGetX()"/>
		<xsl:variable name="objY" select="MatrixUtils:dgGetY()"/>
		<xsl:variable name="objZ" select="MatrixUtils:dgGetZ()"/>
		<xsl:variable name="objW" select="MatrixUtils:dgGetYaw()"/>
		<xsl:variable name="objP" select="MatrixUtils:dgGetPitch()"/>
		<xsl:variable name="objR" select="MatrixUtils:dgGetRoll()"/>
		
		<xsl:value-of select="format-number($objX * 1000, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($objY * 1000, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($objZ * 1000, $patUptoD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:if test="$rbtAxes &gt; 4">
			<xsl:value-of select="format-number($objW, $patUptoD2)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($objP, $patUptoD2)"/>
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:value-of select="format-number($objR, $patUptoD2)"/>
	</xsl:template> <!-- outputWorkData -->

	<xsl:template name="outputJntData">
		<xsl:param name="approach"/>
		<xsl:param name="coordinate"/>

		<xsl:for-each select="Target/JointTarget/Joint">
			<!-- insert joint 4 position with 0,
				 do this before output a joint value
			  -->
			<xsl:if test="$rbtAxes=5 and @DOFNumber=4">
				<xsl:text>, 0</xsl:text>
			</xsl:if>
			<xsl:if test="@DOFNumber &gt; 1">
				<xsl:text>,</xsl:text>
			</xsl:if>
			<xsl:variable name="jVal">
				<xsl:call-template name="outputJntData1Joint"/>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$rbtAxes=4 and @JointType='Translational' and @DOFNumber=3">
					<xsl:value-of select="$jVal - $approach"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$jVal"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template> <!-- outputJntData -->
	
	<xsl:template name="outputAuxData">
		<xsl:if test="$dofAux &gt; 0">
			<xsl:text> EXA ((7,</xsl:text>
			<xsl:call-template name="outputAuxData1Joint">
				<xsl:with-param name="auxDofNum" select="1"/>
			</xsl:call-template>
			<!-- 2nd aux axis? -->
			<xsl:if test="$dofAux &gt; 1">
				<xsl:text>),(8,</xsl:text>
				<xsl:call-template name="outputAuxData1Joint">
					<xsl:with-param name="auxDofNum" select="2"/>
				</xsl:call-template>
			</xsl:if>
			<!-- closing parentheses -->
			<xsl:text>))</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template name="outputJntData1Joint">
		
		<xsl:variable name="jval">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="JointValue"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="@JointType = 'Rotational' and @Units = 'deg'">
				<xsl:value-of select="format-number($jval, $patUptoD2)"/>
			</xsl:when>
			<xsl:when test="@JointType = 'Translational' and @Units = 'm'">
				<xsl:value-of select="format-number($jval * 1000, $patUptoD2)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputAuxData1Joint">
		<xsl:param name="auxDofNum"/>
		<xsl:variable name="auxJoint" select="Target/JointTarget/AuxJoint[@DOFNumber=$rbtAxes+$auxDofNum]"/>
		<xsl:variable name="jval">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$auxJoint/JointValue"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$auxJoint/@Units = 'deg'">
				<xsl:value-of select="format-number($jval, $patUptoD2)"/>
			</xsl:when>
			<xsl:when test="$auxJoint/@Units = 'm'">
				<xsl:value-of select="format-number($jval * 1000, $patUptoD2)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="outputFigure">
		<xsl:variable name="cfg" select="Target/CartesianTarget/Config/@Name"/>
		<xsl:variable name="figure">
			<xsl:choose>
				<xsl:when test="$rbtAxes=6">
					<xsl:choose>
						<xsl:when test="$cfg='Config_1'">5</xsl:when>
						<xsl:when test="$cfg='Config_2'">1</xsl:when>
						<xsl:when test="$cfg='Config_3'">7</xsl:when>
						<xsl:when test="$cfg='Config_4'">3</xsl:when>
						<xsl:when test="$cfg='Config_5'">0</xsl:when>
						<xsl:when test="$cfg='Config_6'">4</xsl:when>
						<xsl:when test="$cfg='Config_7'">2</xsl:when>
						<xsl:when test="$cfg='Config_8'">6</xsl:when>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$rbtAxes=5">
					<xsl:choose>
						<xsl:when test="$cfg='Config_1'">5</xsl:when>
						<xsl:when test="$cfg='Config_3'">7</xsl:when>
						<xsl:when test="$cfg='Config_5'">4</xsl:when> <!-- different than 6-axis robots -->
						<xsl:when test="$cfg='Config_7'">6</xsl:when> <!-- different than 6-axis robots -->
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$rbtAxes=4">
					<xsl:choose>
						<xsl:when test="$cfg='Config_1'">1</xsl:when> <!-- Lefty -->
						<xsl:when test="$cfg='Config_2'">0</xsl:when> <!-- Righty -->
					</xsl:choose>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="j4figure">
			<xsl:call-template name="JFigure">
				<xsl:with-param name="joint" select="Target/JointTarget/Joint[@DOFNumber=4]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="j6figure">
			<xsl:call-template name="JFigure">
				<xsl:with-param name="joint" select="Target/JointTarget/Joint[@DOFNumber=6]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="number($figure) + number($j6figure)*8 + number($j4figure)*8*2"/>
	</xsl:template>
	
	<xsl:template name="JFigure">
		<xsl:param name="joint"/>
		<xsl:choose>
			<xsl:when test="$joint"> <!-- has the Joint or AuxJoint -->
				<xsl:choose>
					<xsl:when test="$joint"> <!-- has the Joint or AuxJoint -->
						<xsl:variable name="jVal">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="$joint/JointValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$jVal &gt; 180">1</xsl:when>
							<xsl:when test="$jVal &lt; -180">1</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise> <!-- Translational -->
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise> <!-- no joint -->
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputSpeed">
		<xsl:param name="motionProfile"/>
		
		<xsl:variable name="controllerMotionProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]"/>
		<xsl:variable name="motionBasis" select="$controllerMotionProfile/MotionBasis"/>
		<xsl:variable name="speedValue" select="$controllerMotionProfile/Speed/@Value"/>
		
		<xsl:if test="$motionBasis = 'Time'">
			<!-- give warning -->
			<xsl:call-template name="WriteWarningMessages">
				<xsl:with-param name="warning" select="'WARNING: '"/>
				<xsl:with-param name="line1" select="concat('Time based motion profile &lt;', $motionProfile, '&gt; is not supported. 50% Speed is used.')"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="percentSpeed">
			<xsl:choose>
				<xsl:when test="$motionBasis = 'Time'">
					<xsl:value-of select="50"/>
				</xsl:when>
				<xsl:when test="$motionBasis = 'Percent'">
					<xsl:value-of select="$speedValue"/>
				</xsl:when>
				<xsl:when test="$motionBasis = 'Absolute'">
					<xsl:value-of select="$speedValue div $maxSpeedValue * 100"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="speed" select="format-number($percentSpeed, $patUptoD1)"/>
		<xsl:variable name="speedStored" select="MotomanUtils:GetParameterData('STORED_Speed')"/>
		<xsl:variable name="spdDifference">
			<xsl:call-template name="fabs">
				<xsl:with-param name="innumber" select="$speed - $speedStored"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$spdDifference &gt; 0.0001">
			<xsl:text>, S=</xsl:text>
			<xsl:value-of select="$speed"/>
			<!-- store implied accel value for comparing to see if ACCEL output is necessary -->
			<xsl:variable name="impliedAccel" select="$speed * $speed div 100"/>
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', $impliedAccel)"/>
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Decel', $impliedAccel)"/>
		</xsl:if>
		
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Speed', $speed)"/>
	</xsl:template>
	
	<xsl:template name="outputAccel">
		<xsl:param name="motionProfile"/>
		
		<xsl:variable name="controllerMotionProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]"/>
		<xsl:variable name="accelValue" select="$controllerMotionProfile/Accel/@Value"/>
		<xsl:variable name="accel" select="format-number($accelValue, $patUptoD4)"/>
		<xsl:variable name="accelStored" select="MotomanUtils:GetParameterData('STORED_Accel')"/>
		<xsl:variable name="accelDifference">
			<xsl:call-template name="fabs">
				<xsl:with-param name="innumber" select="$accel - $accelStored"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$accelDifference &gt; 0.0001">
			<xsl:text>, ACCEL=</xsl:text>
			<xsl:value-of select="$accel"/>
		</xsl:if>
		
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', $accel)"/>
	</xsl:template>
	
	<xsl:template name="outputDecel">
		<xsl:param name="attributelist"/>

		<xsl:variable name="storedDecel" select="MotomanUtils:GetParameterData('STORED_Decel')"/>
		<xsl:for-each select="$attributelist/Attribute">
			<xsl:if test="AttributeName = 'DECEL'">
				<xsl:variable name="decelValue" select="AttributeValue"/>
				<xsl:variable name="decel" select="format-number($decelValue, $patUptoD4)"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Decel', $decel)"/>
				<xsl:variable name="decelDifference">
					<xsl:call-template name="fabs">
						<xsl:with-param name="innumber" select="$decel - $storedDecel"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test="$decelDifference &gt; 0.0001">
					<xsl:text>, DECEL=</xsl:text>
					<xsl:value-of select="$decel"/>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<!-- Outputs lines "SPEED number" and "ACCEL number" for GOHOME or move commands without the phrases -->
	<xsl:template name="outputSpeedAccelLines">
		<xsl:param name="motionProfile"/>
		<xsl:param name="attributelist"/>
		
		<xsl:variable name="controllerMotionProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]"/>
		<xsl:variable name="motionBasis" select="$controllerMotionProfile/MotionBasis"/>
		<xsl:variable name="speedValue" select="$controllerMotionProfile/Speed/@Value"/>
		
		<xsl:if test="$motionBasis = 'Time'">
			<!-- give warning -->
			<xsl:call-template name="WriteWarningMessages">
				<xsl:with-param name="warning" select="'WARNING: '"/>
				<xsl:with-param name="line1" select="concat('Time based motion profile &lt;', $motionProfile, '&gt; is not supported. 50% Speed is used.')"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="percentSpeed">
			<xsl:choose>
				<xsl:when test="$motionBasis = 'Time'">
					<xsl:value-of select="50"/>
				</xsl:when>
				<xsl:when test="$motionBasis = 'Percent'">
					<xsl:value-of select="$speedValue"/>
				</xsl:when>
				<xsl:when test="$motionBasis = 'Absolute'">
					<xsl:value-of select="$speedValue div $maxSpeedValue * 100"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="speed" select="format-number($percentSpeed, $patUptoD1)"/>
		<xsl:variable name="speedStored" select="MotomanUtils:GetParameterData('STORED_Speed')"/>
		<xsl:variable name="spdDifference">
			<xsl:call-template name="fabs">
				<xsl:with-param name="innumber" select="$speed - $speedStored"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$spdDifference &gt; 0.0001">
			<xsl:text>SPEED </xsl:text>
			<xsl:value-of select="$speed"/>
			<xsl:value-of select="$cr"/>
			<!-- store implied accel value for comparing to see if ACCEL output is necessary -->
			<xsl:variable name="impliedAccel" select="$speed * $speed div 100"/>
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', $impliedAccel)"/>
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Decel', $impliedAccel)"/>
		</xsl:if>
		
		<xsl:variable name="storedAccel" select="MotomanUtils:GetParameterData('STORED_Accel')"/>
		
		<xsl:variable name="accelValue" select="$controllerMotionProfile/Accel/@Value"/>
		<xsl:variable name="accel" select="format-number($accelValue, $patUptoD4)"/>
		<xsl:variable name="accelDifference">
			<xsl:call-template name="fabs">
				<xsl:with-param name="innumber" select="$accel - $storedAccel"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$accelDifference &gt; 0.0001">
			<xsl:text>ACCEL </xsl:text>
			<xsl:value-of select="$accel"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		
		<xsl:variable name="storedDecel" select="MotomanUtils:GetParameterData('STORED_Decel')"/>
		<!-- output DECEL attribute after SPEED and ACCEL -->
		<xsl:for-each select="$attributelist/Attribute">
			<xsl:if test="AttributeName = 'DECEL'">
				<xsl:variable name="decelValue" select="AttributeValue"/>
				<xsl:variable name="decel" select="format-number($decelValue, $patUptoD4)"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Decel', $decel)"/>
				<xsl:variable name="decelDifference">
					<xsl:call-template name="fabs">
						<xsl:with-param name="innumber" select="$decel - $storedDecel"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test="$decelDifference &gt; 0.0001">
					<xsl:text>DECEL </xsl:text>
					<xsl:value-of select="$decel"/>
					<xsl:value-of select="$cr"/>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>

		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Speed', $speed)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', $accel)"/>
	</xsl:template>
	
	<xsl:template name="outputAccuracy">
		<xsl:param name="accuracyProfile"/>
		
		<xsl:variable name="controllerAccuracyProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]"/>
		<xsl:variable name="flyByMode" select="$controllerAccuracyProfile/FlyByMode"/>
		<xsl:variable name="accuracyType" select="$controllerAccuracyProfile/AccuracyType"/>
		<xsl:variable name="accuracyValue" select="$controllerAccuracyProfile/AccuracyValue/@Value"/>
		<xsl:variable name="displacement">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$accuracyValue"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$accuracyType='Speed'">
				<xsl:choose>
					<xsl:when test="$flyByMode='On'">
						<xsl:choose>
							<xsl:when test="$displacement &gt; 99.99">
								<xsl:text>@P </xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$cr"/>
								<xsl:value-of select="$cr"/>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>Accuracy Profile &lt;</xsl:text>
								<xsl:value-of select="$accuracyProfile"/>
								<xsl:text>&gt; has a problem.</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>For Speed type Accuracy Profile with FlyBy Mode 'On',</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>Accuracy Value needs to be 100% for Denso Pass Motion '@P'.</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:value-of select="$cr"/>
								<!-- output to error info (download information) section -->
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO START</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>Accuracy Profile &lt;</xsl:text>
								<xsl:value-of select="$accuracyProfile"/>
								<xsl:text>&gt; has a problem.</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>For Speed type Accuracy Profile with FlyBy Mode 'On',</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>Accuracy Value needs to be 100% for Denso Pass Motion '@P'.</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO END</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$flyByMode='Off'">
						<xsl:text>@0 </xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$accuracyType='Distance'">
				<xsl:choose>
					<xsl:when test="$flyByMode='On'">
						<xsl:text>@</xsl:text>
						<xsl:value-of select="format-number(1000*$displacement, $patInteger)"/>
						<xsl:text> </xsl:text>
					</xsl:when>
					<xsl:when test="$flyByMode='Off'">
						<xsl:choose>
							<xsl:when test="$displacement &gt; 0.0001">
								<xsl:text>@0 </xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>@E </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="outputTool">
		<xsl:param name="toolName"/>
		
		<xsl:variable name="storedTool" select="MotomanUtils:GetParameterData('STORED_Tool')"/>
		<!-- check if tool is the same as current tool (STORED_Tool) -->
		<xsl:if test="string($toolName) != string($storedTool)">
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Tool', $toolName)"/>
			<xsl:variable name="toolsUsed" select="MotomanUtils:GetParameterData('ToolsUsed')"/>
			<xsl:variable name="toolNumber">
				<xsl:call-template name="GetToolNumber">
					<xsl:with-param name="toolName" select="$toolName"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="contains($toolsUsed, concat(':', $toolName, ';')) = false">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ToolsUsed', concat($toolsUsed, ':', $toolName, ';'))"/>
				<xsl:variable name="toolTCP" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name=$toolName]/TCPOffset"/>
				<xsl:variable name="toolZero">
					<xsl:call-template name="IsObjectZero">
						<xsl:with-param name="pos" select="$toolTCP/TCPPosition"/>
						<xsl:with-param name="ori" select="$toolTCP/TCPOrientation"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$toolZero = 1"/> <!-- no need to output "TOOL 0, (0,0,0,0,0,0)" -->
					<xsl:otherwise>
						<xsl:text>TOOL </xsl:text>
						<xsl:value-of select="$toolNumber"/>
						<xsl:text>, (</xsl:text>
						<xsl:call-template name="outputToolData">
							<xsl:with-param name="pos" select="$toolTCP/TCPPosition"/>
							<xsl:with-param name="ori" select="$toolTCP/TCPOrientation"/>
							<xsl:with-param name="coordinate" select="concat('TOOL:', $toolName)"/>
						</xsl:call-template>
						<xsl:text>)</xsl:text>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:text>CHANGETOOL </xsl:text>
			<xsl:value-of select="$toolNumber"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template> <!-- outputTool -->

	<!-- This is called only for Cartesian and Tag targets.
		 "WorkShift" denotes if a Denso non-zero WORK command is in effect.
		 It is set to 0 in template ActivityList mode"PAC"
	     Zero-Obj:
			WorkShift=0: No WORK output.
			WorkShift=1: CHANGEWORK 0; WorkShift=0.
	     Non-Zero-Obj:
			WorkShift=0:
				Cartesian: WORK/CHANGEWORK; WorkShift=1.
				Tag:
					ApplyOffsetToTags=On: WORK/CHANGEWORK; WorkShift=1.
					ApplyOffsetToTags=Off: No WORK output.
			WorkShift=1
				Obj Profile Same:
					Cartesian: No WORK output.
					Tag:
						ApplyOffsetToTags=On: No WORK output.
						ApplyOffsetToTags=Off: CHANGEWORK 0; WorkShift=0.
				Obj Profile Diff:
					Cartesian: WORK/CHANGEWORK.
					Tag:
						ApplyOffsetToTags=On: WORK/CHANGEWORK.
						ApplyOffsetToTags=Off: CHANGEWORK 0; WorkShift=0.
	  -->
	<xsl:template name="outputObjectFrame">
		<xsl:param name="activity"/>
		
		<xsl:variable name="objFrameName" select="$activity/MotionAttributes/ObjectFrameProfile"/>
		<xsl:variable name="objProfile" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objFrameName]"/>
		<xsl:variable name="objFrame" select="$objProfile/ObjectFrame"/>
		<xsl:variable name="objZero">
			<xsl:call-template name="IsObjectZero">
				<xsl:with-param name="pos" select="$objFrame/ObjectFramePosition"/>
				<xsl:with-param name="ori" select="$objFrame/ObjectFrameOrientation"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="objFrameNumber">
			<xsl:call-template name="GetObjectFrameNumber">
				<xsl:with-param name="objFrameName" select="$objFrameName"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="storedObjectFrame" select="MotomanUtils:GetParameterData('STORED_ObjectFrame')"/>
		<xsl:variable name="objectFramesUsed" select="MotomanUtils:GetParameterData('ObjectFramesUsed')"/>

		<xsl:variable name="workShift" select="MotomanUtils:GetParameterData('WorkShift')"/>
		<xsl:variable name="target" select="$activity/Target"/>
		
		<xsl:choose>
			<xsl:when test="$objZero = 1">
				<!-- zero object profile -->
				<xsl:if test="$workShift = '1'">
					<xsl:call-template name="outputDensoWork">
						<xsl:with-param name="objFrameName" select="'DensoWorkZero'"/>
						<xsl:with-param name="objFrameNumber" select="'0'"/>
						<xsl:with-param name="objFrame" select="$objFrame"/>
						<xsl:with-param name="workShift" select="'0'"/>
						<xsl:with-param name="activity" select="$activity"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise> <!-- non-zero object profile -->
				<xsl:choose>
					<xsl:when test="$workShift = '0'">
						<xsl:choose>
							<xsl:when test="$target/CartesianTarget/Tag">
								<xsl:if test="$objProfile/@ApplyOffsetToTags = 'On'">
									<xsl:call-template name="outputDensoWork">
										<xsl:with-param name="objFrameName" select="$objFrameName"/>
										<xsl:with-param name="objFrameNumber" select="$objFrameNumber"/>
										<xsl:with-param name="objFrame" select="$objFrame"/>
										<xsl:with-param name="workShift" select="'1'"/>
										<xsl:with-param name="activity" select="$activity"/>
									</xsl:call-template>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<!-- Cartesian target -->
								<xsl:call-template name="outputDensoWork">
									<xsl:with-param name="objFrameName" select="$objFrameName"/>
									<xsl:with-param name="objFrameNumber" select="$objFrameNumber"/>
									<xsl:with-param name="objFrame" select="$objFrame"/>
									<xsl:with-param name="workShift" select="'1'"/>
									<xsl:with-param name="activity" select="$activity"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise> <!-- WorkShift = '1' -->
						<xsl:choose>
							<xsl:when test="string($objFrameName) = string($storedObjectFrame)">
								<xsl:if test="$target/CartesianTarget/Tag and $objProfile/@ApplyOffsetToTags = 'Off'">
									<xsl:call-template name="outputDensoWork">
										<xsl:with-param name="objFrameName" select="'DensoWorkZero'"/>
										<xsl:with-param name="objFrameNumber" select="'0'"/>
										<xsl:with-param name="objFrame" select="$objFrame"/>
										<xsl:with-param name="workShift" select="'0'"/>
										<xsl:with-param name="activity" select="$activity"/>
									</xsl:call-template>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<!-- Different Object Profile -->
								<xsl:choose>
									<xsl:when test="$target/CartesianTarget/Tag and $objProfile/@ApplyOffsetToTags = 'Off'">
										<xsl:call-template name="outputDensoWork">
											<xsl:with-param name="objFrameName" select="'DensoWorkZero'"/>
											<xsl:with-param name="objFrameNumber" select="'0'"/>
											<xsl:with-param name="objFrame" select="$objFrame"/>
											<xsl:with-param name="workShift" select="'0'"/>
											<xsl:with-param name="activity" select="$activity"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<!-- Cartesian target or Tag target with ApplyOffsetToTags='On' -->
										<xsl:call-template name="outputDensoWork">
											<xsl:with-param name="objFrameName" select="$objFrameName"/>
											<xsl:with-param name="objFrameNumber" select="$objFrameNumber"/>
											<xsl:with-param name="objFrame" select="$objFrame"/>
											<xsl:with-param name="workShift" select="$workShift"/>
											<xsl:with-param name="activity" select="$activity"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise> <!-- WorkShift = '1' -->
				</xsl:choose>
			</xsl:otherwise> <!-- non-zero object profile -->
		</xsl:choose>
		
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_ObjectFrame', $objFrameName)"/>
	</xsl:template> <!-- outputObjectFrame -->
	
	<xsl:template name="outputDensoWork">
		<xsl:param name="objFrameName"/>
		<xsl:param name="objFrameNumber"/>
		<xsl:param name="objFrame"/>
		<xsl:param name="workShift"/>
		<xsl:param name="activity"/>

		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('WorkShift', $workShift)"/>
		<xsl:variable name="objectFramesUsed" select="MotomanUtils:GetParameterData('ObjectFramesUsed')"/>
		<xsl:if test="contains($objectFramesUsed, $objFrameName) = false">
			<xsl:text>WORK </xsl:text>
			<xsl:value-of select="$objFrameNumber"/>
			<xsl:text>, (</xsl:text>
			<xsl:call-template name="outputWorkData">
				<xsl:with-param name="pos" select="$objFrame/ObjectFramePosition"/>
				<xsl:with-param name="ori" select="$objFrame/ObjectFrameOrientation"/>
				<xsl:with-param name="coordinate" select="concat('OBJECTFRAME:', $objFrameName)"/>
				<xsl:with-param name="activity" select="$activity"/>
			</xsl:call-template>
			<xsl:text>)</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:text>CHANGEWORK </xsl:text>
		<xsl:value-of select="$objFrameNumber"/>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	
	<xsl:template name="GetToolNumber">
		<xsl:param name="toolName"/>
		
		<xsl:variable name="toolNameUpper">
			<xsl:call-template name="toupper">
				<xsl:with-param name="instring" select="$toolName"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="numberSuffix" select="substring-after($toolNameUpper, 'TOOL.')"/>
		<xsl:choose>
			<xsl:when test="$toolNameUpper='DEFAULT'">0</xsl:when>
			<xsl:when test="starts-with($toolNameUpper, 'TOOL.') and string(number($numberSuffix))!='NaN' and number($numberSuffix)!=0">
				<xsl:value-of select="$numberSuffix"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="toolProfiles" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile"/>
				<xsl:for-each select="$toolProfiles">
					<xsl:if test="./Name = $toolName">
						<xsl:value-of select="position()-1"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="GetObjectFrameNumber">
		<xsl:param name="objFrameName"/>
		
		<xsl:variable name="objFrameNameUpper">
			<xsl:call-template name="toupper">
				<xsl:with-param name="instring" select="$objFrameName"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="numberSuffix" select="substring-after($objFrameNameUpper, 'OBJECT.')"/>
		<xsl:choose>
			<xsl:when test="$objFrameNameUpper='DEFAULT'">0</xsl:when>
			<xsl:when test="starts-with($objFrameNameUpper, 'OBJECT.') and string(number($numberSuffix))!='NaN' and number($numberSuffix)!=0">
				<xsl:value-of select="$numberSuffix"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="objFrameProfiles" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile"/>
				<xsl:for-each select="$objFrameProfiles">
					<xsl:if test="./Name = $objFrameName">
						<xsl:value-of select="position()-1"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="outputDateTime">
		<xsl:variable name="formatter" select="java:java.text.SimpleDateFormat.new('yyyy/MM/dd hh:mm')"/>
		<xsl:variable name="calender" select="java:java.util.Calendar.getInstance()"/>
		<xsl:variable name="dateTime" select="java:getTime($calender)"/>
		
		<xsl:text>'!DATE "</xsl:text>
		<xsl:value-of select="java:format($formatter, $dateTime)"/>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>

	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="actOrActListNode"/>
		
		<xsl:for-each select="$actOrActListNode/AttributeList/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:if test="starts-with($attrname, $prefix)">
				<xsl:choose>
					<xsl:when test="starts-with($attrvalue, 'Robot Language:')">
						<xsl:variable name="attrValueStripped" select="substring-after($attrvalue, 'Robot Language:')"/>
						<xsl:variable name="rbtLanguageUpper">
							<xsl:call-template name="toupper">
								<xsl:with-param name="instring" select="$attrValueStripped"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:if test="starts-with($rbtLanguageUpper, 'WAIT IO')">
							<xsl:call-template name="outputIOBlockOn"/>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="starts-with($rbtLanguageUpper, 'TAKEARM')">
								<xsl:variable name="takeArmOutput" select="MotomanUtils:GetParameterData('TakeArmOutput')"/>
								<xsl:if test="$takeArmOutput = 0">
									<xsl:value-of select="$attrValueStripped"/>
									<xsl:value-of select="$cr"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="starts-with($rbtLanguageUpper, 'DECEL')">
								<!-- DECEL has to be output after SPEED & ACCEL -->
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$attrValueStripped"/>
								<xsl:value-of select="$cr"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="starts-with($rbtLanguageUpper, 'WAIT IO')">
							<!-- In case WaitIO is an Attribute of SetIO, include this Activity -->
							<xsl:variable name="include">
								<xsl:choose>
									<xsl:when test="$actOrActListNode/@ActivityType='DNBSetSignalActivity'">
										<xsl:value-of select="1"/>
									</xsl:when>
									<xsl:otherwise>0</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="$prefix='Pre'">
									<xsl:call-template name="outputIOBlockOff">
										<xsl:with-param name="act" select="$actOrActListNode"/>
										<xsl:with-param name="attr" select="."/>
										<xsl:with-param name="includeself" select="$include"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:when test="$prefix='Post'">
									<xsl:call-template name="outputIOBlockOff">
										<xsl:with-param name="act" select="."/> <!-- fake node, cannot pass blank -->
										<xsl:with-param name="attr" select="."/>
										<xsl:with-param name="includeself" select="$include"/>
									</xsl:call-template>
								</xsl:when>
							</xsl:choose>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>' </xsl:text>
						<xsl:value-of select="$attrvalue"/>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>

	</xsl:template>
	
	<!-- Called only for Operations -->
	<xsl:template name="outputOperationAttribute">
		<!-- This is uploaded with OLPStyle='OperationComments'.
			 There is 1 Attribute. AttributeName='Robot Language'|'Comment'
		-->
		<xsl:variable name="attrName" select="AttributeList/Attribute/AttributeName"/>
		<xsl:variable name="attrVal" select="AttributeList/Attribute/AttributeValue"/>
		<xsl:variable name="attrValCAP">
			<xsl:call-template name="toupper">
				<xsl:with-param name="instring" select="$attrVal"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- IOBlockOn -->
		<xsl:variable name="commentedWaitIO" select="$attrName='Robot Language' and starts-with($attrValCAP, 'WAIT IO')"/>
		<xsl:if test="$commentedWaitIO">
			<xsl:call-template name="outputIOBlockOn"/>
		</xsl:if>
		<!-- output AttributeValue -->
		<xsl:if test="$attrName='Comment'">
			<xsl:text>' </xsl:text>
		</xsl:if>
		<xsl:value-of select="$attrVal"/>
		<xsl:value-of select="$cr"/>
		<!-- IOBlockOff -->
		<xsl:if test="$commentedWaitIO">
			<xsl:call-template name="outputIOBlockOff">
				<xsl:with-param name="act" select="."/>
				<xsl:with-param name="attr" select="AttributeList/Attribute[starts-with(AttributeValue, 'Robot Language:')][last()]"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'e')"/>
					<xsl:with-param name="e" select="substring-after($Num,'e')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'E')"/>
					<xsl:with-param name="e" select="substring-after($Num,'E')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="fabs">
		<xsl:param name="innumber"/>
		
		<xsl:choose>
			<xsl:when test="$innumber &gt;= 0"> <!-- NOT "&ge;" -->
				<xsl:value-of select="$innumber"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="-$innumber"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="TrimTrailing_1">
		<xsl:param name="instring"/>
		
		<xsl:variable name="length" select="string-length($instring)"/>
		<xsl:choose>
			<xsl:when test="number($length) &gt; 2 and substring($instring, number($length)-1, 2) = '_1'">
				<xsl:call-template name="TrimTrailing_1">
					<xsl:with-param name="instring" select="substring($instring, 1, number($length)-2)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$instring"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="GetSuffix">
		<xsl:param name="instring"/>
		
		<xsl:variable name="suffix">
			<xsl:value-of select="substring-after($instring, '.')"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="contains($suffix, '.')">
				<xsl:call-template name="GetSuffix">
					<xsl:with-param name="instring" select="$suffix"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$suffix"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="toupper">
		<xsl:param name="instring"/>
		<xsl:value-of select="translate($instring, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	</xsl:template>

	<xsl:template name="WriteWarningMessages">
		<xsl:param name="warning"/>
		<xsl:param name="line1"/>
		<xsl:param name="line2"/>
		<xsl:param name="line3"/>
		<xsl:param name="line4"/>
		
		<xsl:text>VERSION INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:if test="$line1">
			<xsl:value-of select="$warning"/>
			<xsl:value-of select="$line1"/>
		</xsl:if>
		<xsl:if test="$line2">
			<xsl:value-of select="$cr"/>
			<xsl:value-of select="$warning"/>
			<xsl:value-of select="$line2"/>
		</xsl:if>
		<xsl:if test="$line3">
			<xsl:value-of select="$cr"/>
			<xsl:value-of select="$warning"/>
			<xsl:value-of select="$line3"/>
		</xsl:if>
		<xsl:if test="$line4">
			<xsl:value-of select="$cr"/>
			<xsl:value-of select="$warning"/>
			<xsl:value-of select="$line4"/>
		</xsl:if>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>VERSION INFO END</xsl:text>
	</xsl:template>
	
</xsl:stylesheet>
