<?xml version="1.0" encoding="UTF-8"?>
<!-- COPYRIGHT DASSAULT SYSTEMES 2003 -->
<!-- 13 Nov 2003 creation -->
<!-- IMPORTANT USAGE INFORMATION, PLEASE READ:
	This xslt stylesheet should be used as a starting point to create Delmia V5 OLP downloaders. This stylesheet is not a translator,
	since it doesn't output any of the values retrieved from processed XML elements. The task of a developer who will be using this 
	stylesheet is to create the output based on the syntax of a target robot language. For that purpose, xsl:text and xsl:value-of elements
	can be used, as well as the functions accessible through java xslt extensions. 
	Every line of source code has its matching comment. Please read the comments, since they often contain very important implementation
	information. 
	Some xsl variables have been set to contain default values. Comments above those variables will inform the developer that those 
	variable contents are expected to be changed. The effort has been made to create a very generic stylesheet that would require just 
	additional creation of output statements, while the structure of the file would stay unchanged. In case of translators that would need 
	development of additional templates, possibly to deal with issues not addressed here, it is developer's responsibility to provide them. 
	Also, if the content of this stylesheet needs to be changed, beyond the point of just adding output statements, it is up to the developer 
	to provide that modified xslt support. 
	This stylesheet can be extended either by declaring and referencing built-in java extensions in Xalan or by declaring and referencing 
	custom made java extension through their elements and functions. XSLT extension related literature is quite abundant and easy to find, primarily 
	through various www resources, and some of those resources are referenced in this stylesheet. -->
<!-- To see how to create and use java extensions within an xslt file, read in full the following references:
	http://xml.apache.org/xalan-j/extensions.html
	http://xml.apache.org/xalan-j/extensionslib.html
	http://xml.apache.org/xalan-j/extensions_xsltc.html -->
<!-- Declare the extensions within stylesheet element -->
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:lxslt="http://xml.apache.org/xslt" 
xmlns:java="http://xml.apache.org/xslt/java" 
xmlns:dnbigpolp="http://www.dassault_systemes.com/V5/Delmia/Igrip/Olp/DNBIgpOLPXSLExtension" 
xmlns:NumberIncrement="NumberIncrement" 
xmlns:MatrixUtils="MatrixUtils" 
xmlns:MotomanUtils="MotomanUtils" 
xmlns:FileUtils="FileUtils" 
xmlns:BitMaskUtils="BitMaskUtils" 
xmlns:NachiUtils="NachiUtils" 
extension-element-prefixes="NumberIncrement  MotomanUtils MatrixUtils FileUtils BitMaskUtils NachiUtils">
	<lxslt:component prefix="NumberIncrement" functions="next current" elements="startnum increment reset counternum getProgramNum setProgramNum">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<!--Declare the prefix and the functions to be accessed by using that prefix.
	     Here we just call the functons to set and get parameter and attribute data -->
	<lxslt:component prefix="dnbigpolp" functions="SetParameterData GetParameterData SetAttributeData GetAttributeData">
		<!-- Declare that java extensions were written in form of java class, and give the full class location -->
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils" functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll dgMatrixToQuaternions dgGetQ1 dgGetQ2 dgGetQ3 dgGetQ4" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"/>
	</lxslt:component>
	<lxslt:component prefix="NachiUtils" functions="setStartExpr getStartExpr setWaitExprStmt getWaitExprStmt " elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NachiUtils"/>
	</lxslt:component>
	<!-- Do not preserve whitespace-only text nodes for all the elements of the source XML document -->
	<xsl:strip-space elements="*"/>
	<!-- Indicates the output method for the result document. Text output method simply outputs all the result document's
		text nodes without modification -->
	<xsl:output method="text"/>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!-- HEADER INFORMATION -->
	<!-- You may modify the content of the select attribute, and type in the stylesheet version information relevant for your company -->
	<xsl:variable name="version" select="'DELMIA CORP. NACHI AX DOWNLOADER VERSION 5 RELEASE 27 SP5'"/>
	<!-- You may modify the content of the select attribute, and type in the copyright information relevant for your company -->
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 1986-2018, ALL RIGHTS RESERVED'"/>
	<!-- In general this shouldn't be modified, but if the meaning will stay the same, the wording can change -->
	<xsl:variable name="saveInfo" select="'If you decide to save this file, VERSION INFO header will not be taken into account.'"/>
	<!-- PRESENTATION PATTERNS-->
	<!-- Carriage Return character -->
	<xsl:variable name="cr" select="'&#xa;'"/>
	<!-- Decimal number format. For more information refer to java.text.DecimalFormat class documentation, in Java API Specification -->
	<xsl:variable name="programNumberPattern" select="'000'"/>
	<xsl:variable name="SR_speedPattern" select="'#'"/>
	<xsl:variable name="decimalNumberPattern" select="'#.###'"/>
	<xsl:variable name="targetNumberPattern" select="'#.#####'"/>
	<xsl:variable name="toolPattern" select="'0.000'"/>
	<!-- GENERAL INFORMATION ABOUT THE ROBOT AND DOWNLOADING TECHNIQUE -->
	<!-- Name of the robot which owns the downloaded task -->
	<xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<!-- Robot's maximum allowed speed -->
	<xsl:variable name="maxSpeed" select="/OLPData/Resource/GeneralInfo/MaxSpeed"/>
	<!-- Number of robot's base axes, excluding any type of external axes that can be detached from the robot -->
	<xsl:variable name="numberOfRobotAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<!-- Number of robot's rail and/or track axes -->
	<xsl:variable name="numberOfAuxiliaryAxes" select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<!-- Number of robot's end of arm tooling axes -->
	<xsl:variable name="numberOfExternalAxes" select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<!-- Number of workpiece positioner axes, that are controlled by the robot controller -->
	<xsl:variable name="numberOfPositionerAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<!-- Variable which value defines whether tag targets are generated with respect to the robot's base or with resptect to 
		the bases of the parts to which the tags are attached to -->
	<xsl:variable name="downloadCoordinates" select="/OLPData/Resource/GeneralInfo/DownloadCoordinates"/>
	<!-- Main task name is the name of the task that has been selected for download -->
	<xsl:variable name="mainTaskName" select="/OLPData/Resource/ActivityList/@Task"/>
	<!-- PrintLineNumbers default is true -->
	<xsl:variable name="printlinenum2" select="dnbigpolp:GetParameterData(string('PrintLineNumbers'))"/>
	<xsl:variable name="printlinenum">
		<xsl:choose>
			<xsl:when test="$printlinenum2 = 'false'">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- OutputMOVEX default is true -->
	<xsl:variable name="outputMOVEX2" select="dnbigpolp:GetParameterData('OutputMOVEX')"/>
	<xsl:variable name="outputMOVEX">
		<xsl:choose>
			<xsl:when test="$outputMOVEX2 = 'false'">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="downloadStyle">
		<xsl:choose>
			<xsl:when test="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBIgpCallRobotTask'][1]/DownloadStyle='File'">
				<xsl:text>File</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Subroutine</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES END~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TEMPLATE DEFINITIONS START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!--~~~~~ROOT TEMPLATE~~~~~ -->
	<!-- This template is called first, automatically, by XSLT processor -->
	<xsl:template match="/">
		<!-- Create header information -->
		<xsl:call-template name="HeaderInfo"/>
		<!-- Initialize parameter data -->
		<xsl:call-template name="InitializeParameters"/>
		<!-- This is optional. Call this template if you need to create target declaration section for downloaded program -->
		<!--<xsl:call-template name="CreateTargetDeclarationSection"/>-->
		<!-- Process all the ActivityList nodes in order they appear in the source XML file -->
		<xsl:apply-templates select="OLPData/Resource/ActivityList"/>
	</xsl:template>
	<!--~~~~~HEADER CREATOR TEMPLATE~~~~~ -->
	<!-- This template will not be called automatically by the processor. It is explicitly called from the root template.
		It creates version and copyright information blocks in-between VERSION INFO START and VERSION INFO END
		statements -->
	<xsl:template name="HeaderInfo">
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO START</xsl:text>
		<!-- Print carriage-return character to the result document -->
		<xsl:value-of select="$cr"/>
		<!-- Prints the version information to the result document -->
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the copyright information to the result document -->
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the save information to the result document -->
		<xsl:value-of select="$saveInfo"/>
		<xsl:value-of select="$cr"/>
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO END</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	<!-- ~~~~~PARAMETER DATA INITIALIZER TEMPLATE~~~~~ -->
	<!-- In order to make all the parameter data global, this template first stores all the parameter name / value pairs in java.
		After this template is processed all the parameter values will be accessible throughout this stylesheet. This template
		will not be called automatically by the processor. It is explicitly called from the root template. -->
	<xsl:template name="InitializeParameters">
		<!-- For each resource parameter defined in V5, get it's name and value, and store it in java extension -->
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<!-- Store parameter name -->
			<xsl:variable name="paramName" select="ParameterName"/>
			<!-- Store parameter value -->
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<!-- Store all the parameter name/value pairs in java -->
			<xsl:variable name="hr" select="dnbigpolp:SetParameterData( string($paramName), string($paramValue) )"/>
			<!-- IMPORTANT: In order to retrieve parameter value, from anywhere in the stylesheet use:
				String dnbigpolp:GetParameterData(String) function. For example, this will work:
				<xsl:variable name="result" select="dnbigpolp:GetParameterData('MyParameter1')"/>
				Pass the parameter name as a string, and the return value will be either 
				the parameter value (result, in the example above) returned as a string, or an empty string if the parameter
				 name does not exist in V5 for the downloading resource -->
		</xsl:for-each>

		<xsl:variable name="mech0" select="dnbigpolp:GetParameterData('Mechanism')"/>
		<xsl:if test="string-length(normalize-space($mech0)) &gt; 0">
			<xsl:variable name="mech" select="translate($mech0, ' ', '')"/>
			<xsl:choose>
				<xsl:when test="contains($mech, ':') = 'true'">
					<xsl:variable name="m">
						<xsl:choose>
							<xsl:when test="contains($mech, ';') = 'true'">
								<xsl:value-of select="substring-before(substring-after($mech, ':'), ';')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="substring-after($mech, ':')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="substring-before($mech, ':') = '2'">
							<xsl:variable name="hr" select="dnbigpolp:SetParameterData('M2', $m)"/>
						</xsl:when>
						<xsl:when test="substring-before($mech, ':') = '3'">
							<xsl:variable name="hr" select="dnbigpolp:SetParameterData('M3', $m)"/>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="hr" select="dnbigpolp:SetParameterData('M2', $mech)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<!-- ~~~~~TARGET DECLARATION SECTION CREATOR TEMPLATE~~~~~ -->
	<!-- This template should only be called if target declaration section needs to be created in the output document. By default, in this 
		stylesheet it is always called from the root template. You can comment out that call-template element if you do not need to 
		create target declaration section in the output document -->
	<xsl:template name="CreateTargetDeclarationSection">
		<!-- For each robot motion and follow path activity in the source XML file -->
		<xsl:for-each select="/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity'] | 
							 /OLPData/Resource/ActivityList/Activity[@ActivityType = 'FollowPathActivity'] ">
			<!-- Call template TransformRobotMotionActivityData with mode parameter set to 'TargetDeclarationSection' -->
			<xsl:if test="@ActivityType = 'DNBRobotMotionActivity' ">
				<xsl:call-template name="TransformRobotMotionActivityData">
					<!-- Send the currect node, as the first parameter -->
					<xsl:with-param name="motionActivityNode" select="."/>
					<!-- Send the mode string, as the second parameter -->
					<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
				</xsl:call-template>
			</xsl:if>
			<!-- Call template TransformFollowPathActivityData with mode parameter set to 'TargetDeclarationSection' -->
			<xsl:if test="@ActivityType = 'FollowPathActivity' ">
				<xsl:call-template name="TransformFollowPathActivityData">
					<!-- Send the currect node, as the first parameter -->
					<xsl:with-param name="followPathActivityNode" select="."/>
					<!-- Send the mode string, as the second parameter -->
					<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--~~~~~ATTRIBUTE DATA INITIALIZER TEMPLATE~~~~~ -->
	<!-- In order to make all the attribute data global, this template stores all the passed attribute name / value pairs in java.
	Afeter being stored, those attribute values will be accessible throughout this stylesheet. This template
	will not be called automatically by the processor. It is explicitly called from the ActivityList, Activity, and Action templates -->
	<xsl:template name="SetAttributes">
		<!-- Value of the id attribute of the XML element, for which attributes are to be set -->
		<xsl:param name="id"/>
		<!-- Valid AttributeList XML element that contains Attribute sub-elements -->
		<xsl:param name="attributeListNodeSet"/>
		<!-- For each Attribute element in supplied AttributeList element -->
		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<!-- Check if AttributeName sub-element exist -->
			<xsl:if test="AttributeName">
				<!-- Store the value of the AttributeName element -->
				<xsl:variable name="attribName" select="AttributeName"/>
				<!-- Check if AttributeValue sub-element exist -->
				<xsl:if test="AttributeValue">
					<!-- Store the value of the AttributeValue element -->
					<xsl:variable name="attribValue" select="AttributeValue"/>
					<!-- Call Java extension to store the attribute name/value pairs -->
					<xsl:variable name="hr" select="dnbigpolp:SetAttributeData( string($id), string($attribName), string($attribValue) )"/>
					<!-- IMPORTANT: In order to retrieve attribute value, from anywhere in the stylesheet use:
					String dnbigpolp:GetAttributeData(String, String) function. For example, this will work:
					<xsl:variable name="result" select="dnbigpolp:GetAttributeData('_123456', 'MyAttrib1')"/>
					Pass XML element's id attribute and an attribute name, both as strings, and the return value 
					(result, in the example above) will either be the attribute value (result, in the example above) 
					returned as a string, or an empty string if the attribute name does not exist in V5 for the XML 
					element which id attribute has been passed to the function -->
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--~~~~~MULTI FILE OUTPUT TEMPLATE~~~~~-->
	<!-- This template creates a new DATA FILE START / END block for each ActivityList element in the source XML file.
		The contents of each of these blocks will be saved in separate files, when users decide to save the downloaded
		 content to disk. Equally effective (if not more effective) alternative is to save files from an XSLT stylesheet directly. For
		 that purpose org.apache.xalan.lib.Redirect java extension should be used. To get more information on how to activate
		 this extension, check the following url: http://xml.apache.org/xalan-j/apidocs/org/apache/xalan/lib/Redirect.html.
		 This template is called automatically by XSLT processor. -->
	<!-- xsl:template match="ActivityList" -->
	<xsl:template match="ActivityList">
		<!-- Store the robot task name which content is represented by this ActivityList XML element -->
		<xsl:variable name="taskName" select="@Task"/>
		<xsl:variable name="useTaskName">
			<xsl:choose>
				<xsl:when test="contains($taskName, '-A.') and string(number(substring-after($taskName, '-A.')))!='NaN'">
					<xsl:text>true</xsl:text>
				</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- If there is an attribute list associated with the robot task, store its attributes in java -->
		<xsl:if test="AttributeList">
			<!-- Call SetAttributes template to store attribute values for subsequent retrievals -->
			<xsl:call-template name="SetAttributes">
				<!-- Task name is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="@Task"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>

    <xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[1]">
			<!-- the first ActivityList is assumed to be the main program -->
			<xsl:text>DATA FILE START </xsl:text>
			<!-- program name is composed of <resource name>-A.<program number> -->
			<xsl:choose>
				<xsl:when test="$useTaskName = 'true'">
					<xsl:value-of select="substring-before($taskName, '-A.')"/>
					<xsl:text>-A.</xsl:text>
					<xsl:variable name="numSuffix" select="substring-after($taskName, '-A.')"/>
					<xsl:value-of select="format-number($numSuffix, $programNumberPattern)"/>
					<xsl:variable name="progNum" select="NumberIncrement:setProgramNum($taskName, $numSuffix)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="resourceName" select="dnbigpolp:GetParameterData(string('ResourceName'))"/>
					<xsl:choose>
						<xsl:when test="string($resourceName) != '' and $resourceName != 'No Value'">
							<!-- parameter ResourceName is defined and non-blank -->
							<xsl:value-of select="$resourceName"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring-before($robotName, '.')"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>-A.</xsl:text>
					<xsl:variable name="progNum" select="NumberIncrement:getProgramNum($taskName)"/>
					<xsl:value-of select="format-number($progNum, $programNumberPattern)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$downloadStyle = 'Subroutine' and .!=/OLPData/Resource/ActivityList[1]">
			<!-- create LABEL statement -->
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:text>*</xsl:text>
			<!-- '.' and '_' allowed in labels -->
			<xsl:value-of select="translate(@Task, ' ', '_')"/>
		</xsl:if>
		<xsl:value-of select="$cr"/>
		
    <xsl:variable name="precomment" select="AttributeList/Attribute[AttributeName = 'PreComment1']/AttributeValue"/>
    <xsl:variable name="postcomment" select="AttributeList/Attribute[AttributeName = 'PostComment1']/AttributeValue"/>
    
		<xsl:if test="$precomment != ''">
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Pre'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		
		<xsl:apply-templates select="Activity | Action"/>
		
		<xsl:if test="$postcomment != ''">
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Post'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		
		<xsl:choose>
			<xsl:when test="$downloadStyle = 'File'">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>END</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<xsl:when test="$downloadStyle = 'Subroutine'">
				<xsl:choose>
					<xsl:when test=".=/OLPData/Resource/ActivityList[last()]">
						<xsl:call-template name="OutputLineNumber"/>
						<xsl:text>END</xsl:text>
						<xsl:value-of select="$cr"/>
					</xsl:when>
					<!-- checking last() must be ahead of checking position()=1 
						 so that END will be output for single task -->
					<xsl:when test=".=/OLPData/Resource/ActivityList[position()=1]">
						<xsl:call-template name="OutputLineNumber"/>
						<xsl:text>END</xsl:text>
						<xsl:value-of select="$cr"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="OutputLineNumber"/>
						<xsl:text>RETURN</xsl:text>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
		
		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[last()]">
			<xsl:text>DATA FILE END</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		
		<xsl:if test="$downloadStyle = 'File'">
			<NumberIncrement:reset/><!-- reset line number -->
		</xsl:if>
	</xsl:template>
	<!-- end ActivityList -->
	<!--~~~~~ACTION PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Action elements which are composed of one or more Activity elements. Only the 
		atomic activity types listed below are supported to be part of actions. This template is called automatically by
		XSLT processor. -->
	<xsl:template match="Action">
		<!-- All actions, regardless of their type, have these XML attributes and elkements -->
		<!-- Store this action's id number -->
		<xsl:variable name="actionID" select="@id"/>
		<!-- Store this action's type -->
		<xsl:variable name="actionType" select="@ActionType"/>
		<!-- Store this action's name -->
		<xsl:variable name="actionName" select="ActionName"/>
		<!-- Store name of the resource which performs this action-->
		<xsl:variable name="toolInActionName" select="ToolResource/ResourceName"/>
		<!--<xsl:text>SPOT </xsl:text>-->
		<!-- Set the attributes associated with this action. Refer to the comment marked as IMPORTANT in
			SetAttributes template -->
		<xsl:if test="AttributeList">
			<xsl:call-template name="SetAttributes">
				<!-- Action id attribute value is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="$actionID"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<!-- Based on action type, and the number and type of activities it contains, pick relevant portions of code contained below
		      in xsl:if element and create appropriate output. Delete redundant poritions of the code, and just keep what is needed.
		      All blocks of code below, by default, reference just the first activity of a specified type within an action. If there is more
		      than one activity of the same type within the action, each activity needs to be referenced based on its order of appearance
		      (of all the activities of the same type) within the action. 
		      For example, in case of Delay-Release-Delay combination of activities in one action:
		      
		      The first activity will be referenced, like:
		      
		      	<xsl:variable name="delayActivity" select="ActivityList/Activity[$activityType = 'DelayActivity' ][1]"/>
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			
			The second activity will be referenced, like:
			
			<xsl:variable name="releaseActivity" select="ActivityList/Activity[$activityType =  'ReleaseActivity' ][1]"/>
			<xsl:variable name="releaseActivityID" select="$releaseActivity/@id"/>
			<xsl:variable name="releaseActivityName" select="$releaseActivity/ActivityName"/>
			<xsl:if test="$releaseActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$releaseActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$releaseActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:variable name="releasedObject" select="$releaseActivity/ReleasedObject"/>
			<xsl:variable name="releasedDeviceParent" select="$releaseActivity/ReleasedObject/@ParentName"/>

			While the thrid activity will be referenced like:
			
		      	<xsl:variable name="delayActivity" select="ActivityList/Activity[$activityType = 'DelayActivity' ][2]"/>
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
		-->
		<xsl:if test="$actionType = 'Change this and specify action type relavant for the target robot language' ">
			<!-- DELAY ACTIVITY IN ACTION -->
			<!-- The first, by default, delay activity within the action -->
			<xsl:variable name="delayActivity" select="ActivityList/Activity[@ActivityType = 'DelayActivity' ][1]"/>
			<!-- Delay activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<!-- Delay activity's name, as specified in the source XML file -->
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<!-- Delay activity's begin time, as specified in the source XML file -->
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<!-- Delay activity's finish time, as specified in the source XML file -->
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<!-- Set the attributes associated with this activity. Refer to the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- ROBOT MOTION ACTIVITY IN ACTION -->
			<!-- The first, by default, delay robot motion within the action -->
			<xsl:variable name="robotMotionActivity" select="ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity'][1]"/>
			<!-- Robot Motion activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="robotMotionActivityID" select="$robotMotionActivity/@id"/>
			<!-- Robot Motion activity's name, as specified in the source XML file -->
			<xsl:variable name="robotMotionActivityName" select="$robotMotionActivity/ActivityName"/>
			<!-- Robot Motion activity's begin time, as specified in the source XML file -->
			<xsl:variable name="robotMotionActivityStartTime" select="$robotMotionActivity/ActivityTime/StartTime"/>
			<!-- Robot Motion activity's finish time, as specified in the source XML file -->
			<xsl:variable name="robotMotionActivityEndTime" select="$robotMotionActivity/ActivityTime/EndTime"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$robotMotionActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$robotMotionActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$robotMotionActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Motion type of this motion activity -->
			<xsl:variable name="motionType" select="$robotMotionActivity/MotionAttributes/MotionType"/>
			<!-- Target type of this motion activity -->
			<xsl:variable name="targetType" select="$robotMotionActivity/Target/@Default"/>
			<!-- Home position name for the tool in action -->
			<xsl:variable name="homeName" select="$robotMotionActivity/Target/JointTarget/HomeName"/>
			<!-- GRAB ACTIVITY IN ACTION -->
			<!-- The first, by default, grab activity within the action -->
			<xsl:variable name="grabActivity" select="ActivityList/Activity[@ActivityType =  'GrabActivity' ][1]"/>
			<!-- Grab activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="grabActivityID" select="$grabActivity/@id"/>
			<!-- Grab activity's name, as specified in the source XML file -->
			<xsl:variable name="grabActivityName" select="$grabActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$grabActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$grabActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$grabActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the grabbing device name -->
			<xsl:variable name="grabbingObject" select="$grabActivity/GrabbingObject"/>
			<!-- Get the grabbing device's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="grabbingDeviceParent" select="$grabActivity/GrabbingObject/@ParentName"/>
			<!-- Get the name of device which is to be grabbed-->
			<xsl:variable name="grabbedObject" select="$grabActivity/GrabbedObject"/>
			<!-- Get the grabbed device's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="grabbedDeviceParent" select="$grabActivity/GrabbedObject/@ParentName"/>
			<!-- RELEASE ACTIVITY IN ACTION -->
			<!-- The first, by default, release activity within the action -->
			<xsl:variable name="releaseActivity" select="ActivityList/Activity[@ActivityType =  'ReleaseActivity' ][1]"/>
			<!-- Release activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="releaseActivityID" select="$releaseActivity/@id"/>
			<!-- Release activity's name, as specified in the source XML file -->
			<xsl:variable name="releaseActivityName" select="$releaseActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$releaseActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$releaseActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$releaseActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the name of the device which is to be released -->
			<xsl:variable name="releasedObject" select="$releaseActivity/ReleasedObject"/>
			<!-- Get the released device's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="releasedDeviceParent" select="$releaseActivity/ReleasedObject/@ParentName"/>
			<!-- MOUNT ACTIVITY IN ACTION -->
			<!-- The first, by default, mount activity within the action -->
			<xsl:variable name="mountActivity" select="ActivityList/Activity[@ActivityType =  'DNBIgpMountActivity' ][1]"/>
			<!-- Mount activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="mountActivityID" select="$mountActivity/@id"/>
			<!-- Mount activity's name, as specified in the source XML file -->
			<xsl:variable name="mountActivityName" select="$mountActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$mountActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$mountActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$mountActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the tool name to be mounted -->
			<xsl:variable name="mountedTool" select="$mountActivity/MountTool"/>
			<!-- Get the tool's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="mountedToolParentName" select="$mountActivity/MountTool/@ParentName"/>
			<!-- Get the name of the tool profile which is to be set -->
			<xsl:variable name="setMountedToolProfile" select="$mountActivity/SetToolProfile"/>
			<!-- UNMOUNT ACTIVITY IN ACTION -->
			<!-- The first, by default, unmount activity within the action -->
			<xsl:variable name="unmountActivity" select="ActivityList/Activity[@ActivityType =  'DNBIgpUnMountActivity' ][1]"/>
			<!-- Unmount activity's ID number, as specified in the source XML file, used to set and get the attribute data -->
			<xsl:variable name="unmountActivityID" select="$unmountActivity/@id"/>
			<!-- Unmount activity's name, as specified in the source XML file -->
			<xsl:variable name="unmountActivityName" select="$unmountActivity/ActivityName"/>
			<!-- Set the attributes associated with this activity. See the comment marked as IMPORTANT in
					SetAttributes template -->
			<xsl:if test="$unmountActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$unmountActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$unmountActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Get the tool name to be unmounted -->
			<xsl:variable name="unmountedTool" select="$unmountActivity/UnmountTool"/>
			<!-- Get the tool's parent name. This may be important when workcell was imported from PPR Hub. -->
			<xsl:variable name="unmountedToolParentName" select="$unmountActivity/UnmountTool/@ParentName"/>
			<!-- Get the name of the tool profile which is to be set -->
			<xsl:variable name="setUnmountedToolProfile" select="$unmountActivity/UnsetToolProfile"/>
			<xsl:value-of select="$cr"/>
			<!-- CREATE THE OUTPUT -->
			<!-- Create the output based on the variable values and attribute values generated above -->
		</xsl:if>
		<!--<xsl:value-of select="$cr"/>-->
		<!-- Repeate xsl:if elements for as many action types which are relevant for the target robot language -->
	</xsl:template>
	<!--~~~~~ACTIVITY PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Activity elements in their order of appearance within the XML source file. Only the top level 
		Activity elements (the ones whose anscestors are OLPData, Resource, and AttributeList elements) will be transformed 
		by this template. Activity elements which are descendants of Action element, will be processed by Actions template above.
		This template is called automatically by XSLT processor. -->
	<xsl:template match="Activity">
		<!-- Global variables for all activities -->
		<!-- Get activity id number. It is used to set and get activity attributes. -->
		<xsl:variable name="activityID" select="@id"/>
		<!-- Get the activity type. Only the activity types specified below are supported. -->
		<xsl:variable name="activityType" select="@ActivityType"/>
		<!-- Get the activityName -->
		<xsl:variable name="activityName" select="ActivityName"/>
		<!-- Get the activity begin time -->
		<xsl:variable name="startTime" select="ActivityTime/StartTime"/>
		<!-- Get the activity finish time -->
		<xsl:variable name="endTime" select="ActivityTime/EndTime"/>
		<!-- Set the attributes associated with this activity. Refer to the comment marked as IMPORTANT in
			SetAttributes template -->
		<xsl:if test="AttributeList">
			<xsl:call-template name="SetAttributes">
				<!-- Activity id attribute value is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="$activityID"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<!-- Based on activity type (the value of ActivityType XML element), process all the supported activities -->
		<xsl:choose>
			<!-- ROBOT MOTION ACTIVITY -->
			<xsl:when test="$activityType = 'DNBRobotMotionActivity'">
				<!-- Call the template which will output a move statement -->
				<xsl:call-template name="TransformRobotMotionActivityData">
					<!-- Send this robot motion activity Nodeset as parameter -->
					<xsl:with-param name="motionActivityNode" select="."/>
					<!-- This parameter determines that called templete needs to create a move statement
						in designated robot language -->
					<xsl:with-param name="mode" select=" 'MoveStatement' "/>
				</xsl:call-template>
			</xsl:when>
			<!-- DELAY ACTIVITY -->
			<xsl:when test="$activityType = 'DelayActivity' ">
				<!-- Output formatted delay statement in chosen robot language -->
				<xsl:call-template name="OutputLineNumber"/>
				<!--<xsl:value-of select="$cr"/>-->
				<xsl:text>DELAY </xsl:text>
				<xsl:variable name="start" select="ActivityTime/StartTime"/>
				<xsl:variable name="end" select="ActivityTime/EndTime"/>
				<xsl:value-of select="format-number($endTime - $startTime,$decimalNumberPattern)"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- SET IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBSetSignalActivity' ">
				<xsl:call-template name="OutputLineNumber"/>
				<!-- Get signal name -->
				<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="setPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
				<!-- Get the signal pulse time -->
				<xsl:variable name="setSignalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>
				<!-- Output formatted set IO signal statement in chosen robot language -->
        <xsl:choose>
					<xsl:when test="$setSignalDuration != 0">
						<xsl:text>SETM O</xsl:text>
						<xsl:value-of select="$setPortNumber"/>
						<xsl:text>, </xsl:text>
						<xsl:choose>
							<xsl:when test="$setSignalValue='Off'">
								<xsl:text>0, </xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>1, </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$setSignalDuration"/>
					</xsl:when>
					<xsl:otherwise>
            <xsl:variable name="isSETM">
              <xsl:call-template name="CheckParameterValue">
                <xsl:with-param name="activity" select="."/>
                <xsl:with-param name="pName" select="'OutputSETM'"/>
                <xsl:with-param name="pValue" select="'true'"/>
              </xsl:call-template>
            </xsl:variable>
						<xsl:choose>
              <xsl:when test="$isSETM = 1">SETM</xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="$setSignalValue='Off'">RESET</xsl:when>
                  <xsl:otherwise>SET</xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
						</xsl:choose>
            <xsl:text> O</xsl:text>
						<xsl:value-of select="$setPortNumber"/>
            <xsl:if test="$isSETM = 1">
              <xsl:text>, </xsl:text>
              <xsl:choose>
                <xsl:when test="$setSignalValue='Off'">0</xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
              </xsl:choose>
            </xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- WAIT FOR IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBWaitSignalActivity' ">
				<xsl:call-template name="OutputLineNumber"/>
				<!-- Get signal name -->
				<xsl:variable name="waitSignalName" select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="waitPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
				<!-- Get maximum amount of time a robot will wait to receive a signal -->
				<xsl:variable name="waitMaxTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>
				<!-- Output formatted wait IO signal statement in chosen robot language -->
				<xsl:choose>
					<xsl:when test="$waitMaxTime != 0">
						<xsl:choose>
							<xsl:when test="$waitSignalValue = 'Off'">
								<xsl:text>'WAIT FOR SIGNAL OFF WITH DURATION IS NOT POSSIBLE IN NACHI AX</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>WAIT I</xsl:text>
								<xsl:value-of select="$waitPortNumber"/>
								<xsl:text>, </xsl:text>
								<xsl:value-of select="$waitMaxTime"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$waitSignalValue = 'Off'">WAITJ I</xsl:when>
							<xsl:otherwise>WAITI I</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$waitPortNumber"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- CALL ROBOT TASK ACTIVITY -->
			<xsl:when test="$activityType = 'DNBIgpCallRobotTask' ">
				<!-- Sub-program or sub-routine call. That depemds on the value of DownloadStyle element (below) -->
				<xsl:variable name="robotTaskToCall" select="CallName"/>
				<!-- This defines whether a subprogram - "File" or a subroutine - "Subroutine" will be called -->
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:if test="$downloadStyle='File'">
					<xsl:text>CALLP </xsl:text>
					<xsl:value-of select="NumberIncrement:getProgramNum($robotTaskToCall)"/>
				</xsl:if>
				<xsl:if test="$downloadStyle='Subroutine'">
					<xsl:text>GOSUB *</xsl:text>
					<!-- '.' and '_' allowed -->
					<xsl:value-of select="translate(CallName, ' ', '_')"/>
				</xsl:if>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- ENTER INTERFERENCE ZONE ACTIVITY -->
			<xsl:when test="$activityType = 'DNBEnterZoneActivity' ">
				<!-- By default only zone names are relevant for the robot program creation. If the names of resources
					that are monitored for this zone are important for the robot language, this variable can be changed to
					'true' -->
				<xsl:variable name="areResourceNamesImportant" select=" 'false' "/>
				<!-- Get interference zone name -->
				<xsl:variable name="zoneName" select="ZoneData/ZoneName"/>
				<!-- Based on the value of areResourceNamesImportant variable, create the output-->
				<xsl:choose>
					<!-- Resource names are important for the output robot program -->
					<xsl:when test="$areResourceNamesImportant">
						<!-- For each resource in interference zone -->
						<xsl:for-each select="ZoneData/ZoneResourceList/ZoneResource">
							<!-- Get the resource name -->
							<xsl:variable name="resourceName" select="@Name"/>
							<!-- Get resource parent's name, as it appears in the PPR tree. 
								This may be important when workcell was imported from PPR Hub. -->
							<xsl:variable name="parentName" select="@ParentName"/>
							<!-- Output formatted enter interference zone statement and define the resources involved, in 
								a chosen robot language -->
						</xsl:for-each>
					</xsl:when>
					<!-- Resource names are not important for the output robot program -->
					<xsl:otherwise>
						<!-- Output formatted enter interference zone statement in a chosen robot language -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- CLEAR INTERFERENCE ZONE ACTIVITY -->
			<xsl:when test="$activityType = 'DNBClearZoneActivity' ">
				<!-- By default only zone names are relevant for the robot program creation. If the names of resources
					that are monitored for this zone are important for the robot language, this variable can be changed to
					'true' -->
				<xsl:variable name="areResourceNamesImportant" select=" 'false' "/>
				<!-- Get interference zone name -->
				<xsl:variable name="zoneName" select="ZoneData/ZoneName"/>
				<!-- Based on the value of areResourceNamesImportant variable, create the output-->
				<xsl:choose>
					<!-- Resource names are important for the output robot program -->
					<xsl:when test="$areResourceNamesImportant">
						<!-- For each resource in interference zone -->
						<xsl:for-each select="ZoneData/ZoneResourceList/ZoneResource">
							<!-- Get the resource name -->
							<xsl:variable name="resourceName" select="@Name"/>
							<!-- Get resource parent's name, as it appears in the PPR tree. 
								This may be important when workcell was imported from PPR Hub. -->
							<xsl:variable name="parentName" select="@ParentName"/>
							<!-- Output formatted clear interference zone statement and define the resources involved, in 
								a chosen robot language -->
						</xsl:for-each>
					</xsl:when>
					<!-- Resource names are not important for the output robot program -->
					<xsl:otherwise>
						<!-- Output formatted clear interference zone statement in a chosen robot language -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- FOLLOW PATH ACTIVITY -->
			<xsl:when test="$activityType = 'FollowPathActivity'">
				<!-- Call the template which will output MoveAlong statement -->
				<xsl:call-template name="TransformFollowPathActivityData">
					<!-- Send this follow path activity Nodeset as parameter -->
					<xsl:with-param name="followPathActivityNode" select="."/>
					<!-- This parameter determines that called templete needs to create MoveAlong statement
						in designated robot language -->
					<xsl:with-param name="mode" select=" 'MoveAlongStatement' "/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$activityType = 'Operation'">
				<xsl:apply-templates select="AttributeList/Attribute"/>
						</xsl:when>
					</xsl:choose>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
	</xsl:template>
	<!-- end template match Activity-->
	<!--~~~~~ROBOT MOTION ACTIVITY MULTI-PURPOSE TEMPLATE~~~~~-->
	<xsl:template name="TransformRobotMotionActivityData">
		<!-- Nodeset that contains all the descendants of robot motion activity element -->
		<xsl:param name="motionActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveStatement' "> element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<xsl:variable name="activityName" select="ActivityName"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$motionActivityNode/@id"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile" select="$motionActivityNode/MotionAttributes/MotionProfile"/>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile" select="$motionActivityNode/MotionAttributes/AccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile" select="$motionActivityNode/MotionAttributes/ToolProfile"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<xsl:variable name="ofReplaced" select="$motionActivityNode/MotionAttributes/ObjectFrameProfile/@Replaced"/>
		<xsl:variable name="objectFrameProfile">
			<xsl:choose>
				<xsl:when test="string-length($ofReplaced) &gt; 0">
					<xsl:value-of select="$ofReplaced"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$motionActivityNode/MotionAttributes/ObjectFrameProfile"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Motion profile values used by this motion activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speed" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Tool profile values used by this motion activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X"/>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y"/>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z"/>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw"/>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch"/>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll"/>
		<!-- ObjectFrame profile values used by this motion activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<xsl:variable name="ofPosX" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X"/>
		<!-- Object frame's location offset in Y direction -->
		<xsl:variable name="ofPosY" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y"/>
		<!-- Object frame's location offset in Z direction -->
		<xsl:variable name="ofPosZ" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z"/>
		<!-- Object frame's orientation offset in Yaw direction -->
		<xsl:variable name="ofOriYaw" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
		<!-- Object frame's orientation offset in Pitch direction -->
		<xsl:variable name="ofOriPitch" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
		<!-- Object frame's orientation offset in Roll direction -->
		<xsl:variable name="ofOriRoll" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
		<!-- Motion type of this motion activity: 'Joint' or 'Linear' -->
		<xsl:variable name="motionType" select="$motionActivityNode/MotionAttributes/MotionType"/>
		<!-- Orient mode of this motion activity: 'Wrist', '1_Axis', '2_Axis', or '3_Axis' -->
		<xsl:variable name="orientMode" select="$motionActivityNode/MotionAttributes/OrientMode"/>
		<!-- Target type of this motion activity: 'Joint' or 'Cartesian' -->
		<xsl:variable name="targetTargetType" select="$motionActivityNode/Target/@Default"/>
		<xsl:variable name="targetType">
			<xsl:call-template name="getTargetType">
				<xsl:with-param name="targetTargetType" select="$targetTargetType"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="tagName" select="$motionActivityNode/Target/CartesianTarget/Tag"/>
		<!-- Boolean that specifies whether this target is a via point or not: 'true' or 'false' -->
		<xsl:variable name="isViaPoint" select="$motionActivityNode/Target/@ViaPoint"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this move -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basePositionX" select="$motionActivityNode/Target/BaseWRTWorld/Position/@X"/>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY" select="$motionActivityNode/Target/BaseWRTWorld/Position/@Y"/>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ" select="$motionActivityNode/Target/BaseWRTWorld/Position/@Z"/>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="baseOrientationYaw" select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Yaw"/>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="baseOrientationPitch" select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Pitch"/>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baseOrientationRoll" select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Roll"/>
		<xsl:variable name="Config" select="$motionActivityNode/Target/CartesianTarget/Config"/>
    <xsl:variable name="TurnNumber" select="$motionActivityNode/Target/CartesianTarget/TurnNumber"/>
		<xsl:variable name="J1Val" select="$motionActivityNode/Target/JointTarget/Joint[(@DOFNumber=1 or @JointName='Joint 1') and @JointType='Rotational']/JointValue"/>
		<xsl:variable name="Joint1Value">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$J1Val"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:if test="$mode = 'MoveStatement'">
			<xsl:call-template name="outputMove">
				<xsl:with-param name="targetType" select="$targetType"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="$outputMOVEX = 'false'">
					<xsl:call-template name="outputInterpolation">
						<xsl:with-param name="motionType" select="$motionType"/>
						<xsl:with-param name="targetType" select="$targetType"/>
					</xsl:call-template>
					<xsl:call-template name="outputPose">
						<xsl:with-param name="motionActivityNode" select="$motionActivityNode"/>
					</xsl:call-template>
					<xsl:call-template name="outputSpeedTime">
						<xsl:with-param name="speed" select="$speed"/>
						<xsl:with-param name="motionBasis" select="$motionBasis"/>
					</xsl:call-template>
					<xsl:call-template name="outputAccuracy">
						<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
					</xsl:call-template>
					<xsl:call-template name="outputAcceleration">
						<xsl:with-param name="accel" select="$accel"/>
					</xsl:call-template>
					<xsl:call-template name="outputSmoothness">
						<xsl:with-param name="activity" select="$motionActivityNode"/>
					</xsl:call-template>
					<xsl:call-template name="outputTool">
						<xsl:with-param name="toolProfile" select="$toolProfile"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise> <!-- output MOVEX syntax-->
					<xsl:call-template name="outputAccuracy">
						<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
					</xsl:call-template>
					<xsl:call-template name="outputAcceleration">
						<xsl:with-param name="accel" select="$accel"/>
					</xsl:call-template>
					<xsl:call-template name="outputSmoothness">
						<xsl:with-param name="activity" select="$motionActivityNode"/>
					</xsl:call-template>
					<xsl:call-template name="outputCoordination">
						<xsl:with-param name="activity" select="$motionActivityNode"/>
					</xsl:call-template>
					<xsl:call-template name="outputInterpolation">
						<xsl:with-param name="motionType" select="$motionType"/>
						<xsl:with-param name="targetType" select="$targetType"/>
					</xsl:call-template>
          <xsl:choose>
            <xsl:when test="starts-with($robotName, 'MR') and $numberOfRobotAxes &gt; 6">
              <xsl:call-template name="outputPose7">
                <xsl:with-param name="motionActivityNode" select="$motionActivityNode"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="outputPose">
                <xsl:with-param name="motionActivityNode" select="$motionActivityNode"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="$targetType != 'Joint'">
            <xsl:call-template name="outputConfig">
              <xsl:with-param name="Config" select="$Config"/>
              <xsl:with-param name="TurnNumber" select="$TurnNumber"/>
              <xsl:with-param name="Joint1Value" select="$Joint1Value"/>
            </xsl:call-template>
          </xsl:if>
          <xsl:call-template name="outputSpeedTime">
            <xsl:with-param name="speed" select="$speed"/>
            <xsl:with-param name="motionBasis" select="$motionBasis"/>
          </xsl:call-template>
          <xsl:call-template name="outputTool">
            <xsl:with-param name="toolProfile" select="$toolProfile"/>
          </xsl:call-template>
          <xsl:call-template name="outputSpeedRef">
            <xsl:with-param name="activity" select="$motionActivityNode"/>
          </xsl:call-template>
          <xsl:choose>
            <xsl:when test="$targetType = 'Joint'">
              <xsl:choose>
                <xsl:when test="starts-with($robotName, 'MR') and $numberOfRobotAxes &gt; 6">
                  <!-- output J1 as M2J -->
                  <xsl:call-template name="mechSpec7">
                    <xsl:with-param name="activity" select="$motionActivityNode"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                  <!-- other Mechanisms -->
                  <xsl:call-template name="mechSpec">
                    <xsl:with-param name="activity" select="$motionActivityNode"/>
                  </xsl:call-template>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise> <!-- Cartesian target -->
              <xsl:call-template name="mechSpec">
                <xsl:with-param name="activity" select="$motionActivityNode"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise> <!-- output MOVEX syntax-->
			</xsl:choose>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if test="$mode = 'MoveStatement'">
			<xsl:variable name="spotnode" select="following-sibling::*[1]"/>
			<xsl:variable name="spotActionType" select="$spotnode/@ActionType"/>
			<xsl:variable name="spotActionID" select="$spotnode/@id"/>
			<xsl:variable name="gun_number" select="$spotnode/AttributeList/Attribute[AttributeName='Gun_Number']/AttributeValue"/>
			<xsl:variable name="weld_cond_number" select="$spotnode/AttributeList/Attribute[AttributeName='Weld_Cond_Number']/AttributeValue"/>
			<xsl:variable name="weld_seq_number" select="$spotnode/AttributeList/Attribute[AttributeName='Weld_Seq_Number']/AttributeValue"/>
			<xsl:variable name="weld_point_number" select="$spotnode/AttributeList/Attribute[AttributeName='Weld_Point_Number']/AttributeValue"/>

			<xsl:if test="$spotActionType = 'AXSpotWeld'">
				<xsl:if test="$spotnode/AttributeList">
					<!-- Call SetAttributes template to store attribute values for subsequent retrievals -->
					<xsl:call-template name="SetAttributes">
						<!-- Task name is used as a unique identifier of this ActivityList XML element -->
						<xsl:with-param name="id" select="$spotActionID"/>
						<!-- Send the AttributeList XML element as the second parameter -->
						<xsl:with-param name="attributeListNodeSet" select="$spotnode/AttributeList"/>
					</xsl:call-template>
				</xsl:if>

				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>SPOT </xsl:text>
				<xsl:value-of select="$gun_number"/>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$weld_cond_number"/>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$weld_seq_number"/>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$weld_point_number"/>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:if>
		<xsl:variable name="userProfileType" select="MotionAttributes/UserProfile/@Type"/>
		<xsl:if test="$activityName='ARCON' or starts-with($userProfileType, 'NAXArcStartTable')">
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:text>AS </xsl:text>
			<xsl:apply-templates select="MotionAttributes/UserProfile"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if test="$activityName='ARCOFF' or starts-with($userProfileType, 'NAXArcEndTable')">
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:text>AE </xsl:text>
			<xsl:apply-templates select="MotionAttributes/UserProfile"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	<!-- End of TransformRobotMotionActivityData -->

	<xsl:template name="OutputLineNumber">
		<xsl:if test="$printlinenum = 'true'">
			<xsl:variable name="curnum" select="NumberIncrement:next()"/>
			<xsl:value-of select="$curnum"/>
			<xsl:text> </xsl:text>
		</xsl:if>
	</xsl:template>

	<!--~~~~~FOLLOW PATH ACTIVITY MULTI-PURPOSE TEMPLATE~~~~~-->
	<xsl:template name="TransformFollowPathActivityData">
		<!-- Nodeset that contains all the descendants of follow path activity element -->
		<xsl:param name="followPathActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveAlongStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveAlongStatement' "> 
		element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$followPathActivityNode/@id"/>
		<!-- Name of the part that contains geometric feature along which the path is defined -->
		<xsl:variable name="pathOnPart" select="$followPathActivityNode/PathOnPart"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile" select="$followPathActivityNode/PathMotionAttributes/PathMotionProfile"/>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile" select="$followPathActivityNode/PathMotionAttributes/PathAccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile" select="$followPathActivityNode/PathMotionAttributes/PathToolProfile"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<xsl:variable name="objectFrameProfile" select="$followPathActivityNode/PathMotionAttributes/PathObjectFrameProfile"/>
		<!-- Motion profile values used by this activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speed" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Tool profile values used by this activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X"/>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y"/>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z"/>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw"/>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch"/>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll"/>
		<!-- ObjectFrame profile values used by this activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<xsl:variable name="ofPositionX" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X"/>
		<!-- Object frame's location offset in Y direction -->
		<xsl:variable name="ofPositionY" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y"/>
		<!-- Object frame's location offset in Z direction -->
		<xsl:variable name="ofPositionZ" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z"/>
		<!-- Object frame's orientation offset in Yaw direction -->
		<xsl:variable name="ofOrientationYaw" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
		<!-- Object frame's orientation offset in Pitch direction -->
		<xsl:variable name="ofOrientationPitch" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
		<!-- Object frame's orientation offset in Roll direction -->
		<xsl:variable name="ofOrientationRoll" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
		<!-- Motion type of this follow path activity: Always 'Linear' -->
		<xsl:variable name="motionType" select="$followPathActivityNode/PathMotionAttributes/MotionType"/>
		<xsl:variable name="targetType" select="$followPathActivityNode/PathMotionAttributes/TargetType"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this path -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basePositionX" select="$followPathActivityNode/BaseWRTWorld/Position/@X"/>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY" select="$followPathActivityNode/BaseWRTWorld/Position/@Y"/>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ" select="$followPathActivityNode/BaseWRTWorld/Position/@Z"/>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="baseOrientationYaw" select="$followPathActivityNode/BaseWRTWorld/Orientation/@Yaw"/>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="baseOrientationPitch" select="$followPathActivityNode/BaseWRTWorld/Orientation/@Pitch"/>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baseOrientationRoll" select="$followPathActivityNode/BaseWRTWorld/Orientation/@Roll"/>
		<!-- Configuration string name. Config string is the same for all the nodes in the path. -->
		<xsl:variable name="Config" select="$followPathActivityNode/PathConfig"/>
    <xsl:variable name="TurnNumber" select="$followPathActivityNode/PathTurnNumber"/>
		<!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models.
			Turn numbers do not change between the nodes in the path.  -->
		<!-- If turn numbers have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn number value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turnNumber1" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 1]/@Value"/>
		<!-- Turn number value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turnNumber4" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 4]/@Value"/>
		<!-- Turn number value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnNumber5" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 5]/@Value"/>
		<!-- Turn number value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turnNumber6" select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 6]/@Value"/>
		<!-- Get all  turn signs. If the robot uses turn signs, there will be 4 joints that will always have turn signs enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does for all robot models.
			Turn signs do not change between the nodes in the path.  -->
		<!-- If turn signs have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn sign output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn sign value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turnSign1" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 1]/@Value"/>
		<!-- Turn sign value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turnSign4" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 4]/@Value"/>
		<!-- Turn sign value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnSign5" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 5]/@Value"/>
		<!-- Turn sign value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turnSign6" select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 6]/@Value"/>
		<!-- Get node numbers, positions and orientations -->
		<xsl:if test="count($followPathActivityNode/PathNodeList/Node) > 0">
			<!-- For each node in the path, get -->
			<xsl:for-each select="$followPathActivityNode/PathNodeList/Node">
				<!-- Creation of this node target declaration -->
				<xsl:if test=" $mode = 'TargetDeclarationSection' ">
					<!-- Create node target declaration output -->
				</xsl:if>
				<!-- Create move along the path statement-->
				<xsl:if test=" $mode = 'MoveAlongStatement' ">
					<xsl:call-template name="outputMove">
						<xsl:with-param name="targetType" select="$targetType"/>
					</xsl:call-template>
					<xsl:choose>
						<xsl:when test="$outputMOVEX = 'false'">
							<xsl:call-template name="outputInterpolation">
								<xsl:with-param name="motionType" select="$motionType"/>
								<xsl:with-param name="targetType" select="$targetType"/>
							</xsl:call-template>
							<xsl:call-template name="outputPoseNode">
								<xsl:with-param name="followPathActivityNode" select="$followPathActivityNode"/>
							</xsl:call-template>
							<xsl:text>, </xsl:text>
							<xsl:call-template name="outputSpeedTime">
								<xsl:with-param name="speed" select="$speed"/>
								<xsl:with-param name="motionBasis" select="$motionBasis"/>
							</xsl:call-template>
							<xsl:call-template name="outputAccuracy">
								<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
							</xsl:call-template>
							<xsl:call-template name="outputAcceleration">
								<xsl:with-param name="accel" select="$accel"/>
							</xsl:call-template>
							<xsl:call-template name="outputTool">
								<xsl:with-param name="toolProfile" select="$toolProfile"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="outputAccuracy">
								<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
							</xsl:call-template>
							<xsl:call-template name="outputAcceleration">
								<xsl:with-param name="accel" select="$accel"/>
							</xsl:call-template>
							<xsl:call-template name="outputInterpolation">
								<xsl:with-param name="motionType" select="$motionType"/>
								<xsl:with-param name="targetType" select="$targetType"/>
							</xsl:call-template>
							<xsl:call-template name="outputPoseNode">
								<xsl:with-param name="followPathActivityNode" select="$followPathActivityNode"/>
							</xsl:call-template>
							<xsl:text>, </xsl:text>
              <!-- no good here, no joint values -->
							<xsl:call-template name="outputConfig">
								<xsl:with-param name="Config" select="$Config"/>
                <xsl:with-param name="TurnNumber" select="$TurnNumber"/>
							</xsl:call-template>
							<xsl:call-template name="outputSpeedTime">
								<xsl:with-param name="speed" select="$speed"/>
								<xsl:with-param name="motionBasis" select="$motionBasis"/>
							</xsl:call-template>
							<xsl:call-template name="outputTool">
								<xsl:with-param name="toolProfile" select="$toolProfile"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:value-of select="$cr"/>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<xsl:template name="GetAccuracy">
		<xsl:param name="accuracyValue" select="0"/>
		<xsl:param name="accuracyType" select="Speed"/>

		<xsl:variable name="accuracyValueFlt">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$accuracyValue"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$accuracyType='Speed'">
				<xsl:choose>
					<xsl:when test="$accuracyValueFlt &lt; 5">1</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 10">2</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 15">3</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 25">4</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 50">5</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 75">6</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 100">7</xsl:when>
					<xsl:otherwise>8</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$accuracyType='Distance'">
				<xsl:choose>
					<xsl:when test="$accuracyValueFlt &lt; 0.005">1</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 0.010">2</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 0.025">3</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 0.050">4</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 0.100">5</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 0.200">6</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 0.500">7</xsl:when>
					<xsl:otherwise>8</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$accuracyValueFlt &lt; 5">1</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 10">2</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 15">3</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 25">4</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 50">5</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 75">6</xsl:when>
					<xsl:when test="$accuracyValueFlt &lt; 100">7</xsl:when>
					<xsl:otherwise>8</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- GetAccuracy Template End -->
	<xsl:template match="ToolProfileList">
		<xsl:apply-templates select="ToolProfile" mode="fileMode"/>
	</xsl:template>
	<!-- ToolProfileList Template End-->
	<xsl:template match="ToolProfile" mode="codeMode">
		<xsl:value-of select="count(preceding-sibling::ToolProfile)+1"/>
	</xsl:template>
	<!-- ToolProfile Template End -->
	<xsl:template match="ToolProfile" mode="fileMode">
		<xsl:value-of select="$cr"/>'TOOL <xsl:value-of select="position()-1"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="format-number(TCPOffset/TCPPosition/@X, $toolPattern)"/>, <xsl:value-of select="format-number(TCPOffset/TCPPosition/@Y, $toolPattern)"/>, <xsl:value-of select="format-number(TCPOffset/TCPPosition/@Z, $toolPattern)"/>
	</xsl:template>
	<!-- ToolProfile Template End -->
	<xsl:template name="toupper">
		<xsl:param name="instring"/>
		<xsl:value-of select="translate($instring, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	</xsl:template>
	<xsl:template match="Attribute">
		<xsl:variable name="attrname" select="AttributeName"/>
		<xsl:variable name="attrvalue" select="AttributeValue"/>
		<xsl:if test="substring($attrname,1,7) = 'Comment'">
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:variable name="prefixRem">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="substring($attrvalue,1,3)"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="$prefixRem != 'REM'">
				<xsl:text>'</xsl:text>
			</xsl:if>
			<xsl:value-of select="$attrvalue"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if test="substring($attrname,1,14) = 'Robot Language'">
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:value-of select="$attrvalue"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNodeSet"/>

		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
			<xsl:variable name="postcomchk" select="substring($attrname,1,11)"/>
			<xsl:variable name="remarkchk">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="substring($attrvalue,1,4)"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="roblangchk" select="substring($attrvalue,1,15)"/>
			<xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>

			<xsl:if test="($precomchk = 'PreComment' and $prefix = 'Pre') or ($postcomchk = 'PostComment' and 	$prefix = 'Post')">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:if test="$roblangchk != 'Robot Language:' and $remarkchk != 'REM '">
					<xsl:text>' </xsl:text>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$roblangchk = 'Robot Language:'">
						<xsl:value-of select="$striprobotlang"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$attrvalue"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="UserProfile">
		<xsl:variable name="name" select="."/>
		<xsl:variable name="type" select="./@Type"/>
		<xsl:variable name="userProfile" select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type=$type]"/>
		<xsl:variable name="instance" select="$userProfile/UserProfileInstance[@Name=$name]"/>
		<xsl:choose>
			<xsl:when test="contains($type,'NAXArcStartTable')">
				<xsl:variable name="arg1" select="$instance/UserDefinedAttribute[DisplayName='WeldNumber']/Value"/>
				<xsl:value-of select="$arg1"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg2" select="$instance/UserDefinedAttribute[DisplayName='ConditionFileSpecification']/Value"/>
				<xsl:value-of select="$arg2"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg3" select="$instance/UserDefinedAttribute[DisplayName='RetryFileNumber']/Value"/>
				<xsl:value-of select="$arg3"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg4" select="$instance/UserDefinedAttribute[DisplayName='AXVersion']/Value"/>
				<xsl:value-of select="$arg4"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg5" select="$instance/UserDefinedAttribute[DisplayName='CharacteristicsDataRegNum']/Value"/>
				<xsl:value-of select="$arg5"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg6" select="$instance/UserDefinedAttribute[DisplayName='WeldingMethod']/Value"/>
				<xsl:value-of select="$arg6"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg7" select="$instance/UserDefinedAttribute[DisplayName='CurrentConditionClassification']/Value"/>
				<xsl:value-of select="$arg7"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg8" select="$instance/UserDefinedAttribute[DisplayName='VoltageAdjMethod']/Value"/>
				<xsl:value-of select="$arg8"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg9" select="$instance/UserDefinedAttribute[DisplayName='SlopeConditionClassification']/Value"/>
				<xsl:value-of select="$arg9"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg10" select="$instance/UserDefinedAttribute[DisplayName='WeldingControlType']/Value"/>
				<xsl:value-of select="$arg10"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg11" select="$instance/UserDefinedAttribute[DisplayName='WeldingCurrent']/Value"/>
				<xsl:value-of select="$arg11"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg12" select="$instance/UserDefinedAttribute[DisplayName='WeldingVoltage']/Value"/>
				<xsl:value-of select="$arg12"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg13" select="$instance/UserDefinedAttribute[DisplayName='SlopeTimeDistance']/Value"/>
				<xsl:value-of select="$arg13"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg14" select="$instance/UserDefinedAttribute[DisplayName='WeldingSpeed']/Value"/>
				<xsl:value-of select="$arg14"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg15" select="$instance/UserDefinedAttribute[DisplayName='NumberOfChannelsUsed']/Value"/>
				<xsl:value-of select="$arg15"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg16" select="$instance/UserDefinedAttribute[DisplayName='CH3Setting']/Value"/>
				<xsl:value-of select="$arg16"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg17" select="$instance/UserDefinedAttribute[DisplayName='CH4Setting']/Value"/>
				<xsl:value-of select="$arg17"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg18" select="$instance/UserDefinedAttribute[DisplayName='PulseControlSpec']/Value"/>
				<xsl:value-of select="$arg18"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg19" select="$instance/UserDefinedAttribute[DisplayName='PeakCurrent']/Value"/>
				<xsl:value-of select="$arg19"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg20" select="$instance/UserDefinedAttribute[DisplayName='FillerWireSpeedAtPeakCurrent']/Value"/>
				<xsl:value-of select="$arg20"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg21" select="$instance/UserDefinedAttribute[DisplayName='PulseFrequency']/Value"/>
				<xsl:value-of select="$arg21"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg22" select="$instance/UserDefinedAttribute[DisplayName='PulseWidthRatio']/Value"/>
				<xsl:value-of select="$arg22"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg23" select="$instance/UserDefinedAttribute[DisplayName='FillerControl']/Value"/>
				<xsl:value-of select="$arg23"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg24" select="$instance/UserDefinedAttribute[DisplayName='FillerWireFeedCommandOutputTiming']/Value"/>
				<xsl:value-of select="$arg24"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg25" select="$instance/UserDefinedAttribute[DisplayName='FillerWireFeedCommandOutputTimingFall']/Value"/>
				<xsl:value-of select="$arg25"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg26" select="$instance/UserDefinedAttribute[DisplayName='WeavingSyncCommand']/Value"/>
				<xsl:value-of select="$arg26"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg27" select="$instance/UserDefinedAttribute[DisplayName='WeavingPhaseAdjustTime']/Value"/>
				<xsl:value-of select="$arg27"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg28" select="$instance/UserDefinedAttribute[DisplayName='PreheatingControl']/Value"/>
				<xsl:value-of select="$arg28"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg29" select="$instance/UserDefinedAttribute[DisplayName='PreheatingTime']/Value"/>
				<xsl:value-of select="$arg29"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg30" select="$instance/UserDefinedAttribute[DisplayName='PreheatingCurrent']/Value"/>
				<xsl:value-of select="$arg30"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg31" select="$instance/UserDefinedAttribute[DisplayName='PreheatFillerWireFeedSpeed']/Value"/>
				<xsl:value-of select="$arg31"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg32" select="$instance/UserDefinedAttribute[DisplayName='SlopeControl']/Value"/>
				<xsl:value-of select="$arg32"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg33" select="$instance/UserDefinedAttribute[DisplayName='RobotStopTimeDuringSlope']/Value"/>
				<xsl:value-of select="$arg33"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg34" select="$instance/UserDefinedAttribute[DisplayName='FillerWireFeedStartDelayTimeDuringUpslope']/Value"/>
				<xsl:value-of select="$arg34"/>
				<xsl:text>,0,0,0,0,0</xsl:text><!-- reserved parameters 35th ~ 39th -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="arg1" select="$instance/UserDefinedAttribute[DisplayName='WeldNumber']/Value"/>
				<xsl:value-of select="$arg1"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg2" select="$instance/UserDefinedAttribute[DisplayName='ConditionFileSpecification']/Value"/>
				<xsl:value-of select="$arg2"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg3" select="$instance/UserDefinedAttribute[DisplayName='AXVersion']/Value"/>
				<xsl:value-of select="$arg3"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg4" select="$instance/UserDefinedAttribute[DisplayName='CharacteristicsDataRegNum']/Value"/>
				<xsl:value-of select="$arg4"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg5" select="$instance/UserDefinedAttribute[DisplayName='WeldingMethod']/Value"/>
				<xsl:value-of select="$arg5"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg6" select="$instance/UserDefinedAttribute[DisplayName='CurrentConditionClassification']/Value"/>
				<xsl:value-of select="$arg6"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg7" select="$instance/UserDefinedAttribute[DisplayName='VoltageAdjMethod']/Value"/>
				<xsl:value-of select="$arg7"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg8" select="$instance/UserDefinedAttribute[DisplayName='SlopeConditionClassification']/Value"/>
				<xsl:value-of select="$arg8"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg9" select="$instance/UserDefinedAttribute[DisplayName='CraterCurrent']/Value"/>
				<xsl:value-of select="$arg9"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg10" select="$instance/UserDefinedAttribute[DisplayName='CraterVoltage']/Value"/>
				<xsl:value-of select="$arg10"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg11" select="$instance/UserDefinedAttribute[DisplayName='SlopeTime']/Value"/>
				<xsl:value-of select="$arg11"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg12" select="$instance/UserDefinedAttribute[DisplayName='CraterTime']/Value"/>
				<xsl:value-of select="$arg12"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg13" select="$instance/UserDefinedAttribute[DisplayName='AfterFlowTime']/Value"/>
				<xsl:value-of select="$arg13"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg14" select="$instance/UserDefinedAttribute[DisplayName='NumberOfChannelsUsed']/Value"/>
				<xsl:value-of select="$arg14"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg15" select="$instance/UserDefinedAttribute[DisplayName='CH3Setting']/Value"/>
				<xsl:value-of select="$arg15"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg16" select="$instance/UserDefinedAttribute[DisplayName='CH4Setting']/Value"/>
				<xsl:value-of select="$arg16"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg17" select="$instance/UserDefinedAttribute[DisplayName='SlopeControl']/Value"/>
				<xsl:value-of select="$arg17"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg18" select="$instance/UserDefinedAttribute[DisplayName='RobotStopTimeDuringSlope']/Value"/>
				<xsl:value-of select="$arg18"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg19" select="$instance/UserDefinedAttribute[DisplayName='RobotOperationDuringPostFlow']/Value"/>
				<xsl:value-of select="$arg19"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg20" select="$instance/UserDefinedAttribute[DisplayName='RetractSpeed']/Value"/>
				<xsl:value-of select="$arg20"/>
				<xsl:text>,</xsl:text>
				<xsl:variable name="arg21" select="$instance/UserDefinedAttribute[DisplayName='FillerWireFeedEndAdvanceTimeDuringDownSlope']/Value"/>
				<xsl:value-of select="$arg21"/>
				<xsl:text>,</xsl:text>
				<xsl:text>0,0,0,0,0,0,0</xsl:text>
				<!--<xsl:variable name="rest" select="substring-after($type, 'NAWArcOFFTable.')"></xsl:variable>
				<xsl:value-of select="$rest"/>-->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="outputMove">
		<xsl:param name="targetType"/>

		<xsl:call-template name="OutputLineNumber"/>
		<xsl:choose>
			<xsl:when test="$outputMOVEX = 'false'">
				<xsl:choose>
					<xsl:when test="$targetType='Joint'">
						<xsl:text>MOVEJ</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>MOVE</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>MOVEX</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> </xsl:text>
	</xsl:template>
	<!-- End of outputMove -->

	<xsl:template name="outputInterpolation">
		<xsl:param name="motionType"/>
		<xsl:param name="targetType"/>

		<xsl:if test="$outputMOVEX = 'true'">
			<xsl:choose>
				<xsl:when test="$targetType = 'Joint'">M1J</xsl:when>
				<xsl:otherwise>M1X</xsl:otherwise>
			</xsl:choose>
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:call-template name="moType">
			<xsl:with-param name="motionType" select="$motionType"/>
		</xsl:call-template>
	</xsl:template>
	<!-- End of outputInterpolation -->

	<xsl:template name="moType">
		<xsl:param name="motionType"/>

		<xsl:if test="$outputMOVEX = 'false' and starts-with($motionType, 'Circular')">
			<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
			<xsl:text>Error: Circular motion is only supported in MOVEX. Set 'OutputMOVEX' to TRUE.</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>ERROR INFO END</xsl:text>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$motionType = 'Joint'">
				<xsl:text>P</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'Linear'">
				<xsl:text>L</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'CircularVia' and $outputMOVEX = 'true'">
				<xsl:text>C1</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'Circular' and $outputMOVEX = 'true'">
				<xsl:text>C2</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'CircularVia' and $outputMOVEX = 'false'">
				<xsl:text>***ERROR***</xsl:text>
			</xsl:when>
			<xsl:when test="$motionType = 'Circular' and $outputMOVEX = 'false'">
				<xsl:text>***ERROR***</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>L</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>, </xsl:text>
	</xsl:template>

	<xsl:template name="mechSpec">
		<xsl:param name="activity"/>

		<xsl:variable name="auxJ" select="$activity/Target/JointTarget/AuxJoint"/>
		<xsl:variable name="downloadAuxJ" select="dnbigpolp:GetParameterData('DownloadAuxJoint')"/>
		<!-- if there are aux axes and either DownloadAuxJoint is not defined or DownloadAuxJoint is not zero -->
		<xsl:if test="boolean($auxJ) = 'true' and (string-length($downloadAuxJ) = 0 or $downloadAuxJ != 0)">
			<xsl:variable name="motionType" select="$activity/MotionAttributes/MotionType"/>
			<xsl:variable name="targetType" select="$activity/Target/@Default"/>
			<xsl:variable name="motionProfile" select="$activity/MotionAttributes/MotionProfile"/>
			<xsl:variable name="toolProfile" select="$activity/MotionAttributes/ToolProfile"/>
			<xsl:variable name="motionBasis" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
			<xsl:variable name="speed" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
			
			<xsl:variable name="j2mMap" select="dnbigpolp:GetParameterData('JointToMechanismMap')"/>
			<xsl:if test="string-length($j2mMap) = 0"> <!-- parameter not defined -->
				<xsl:text>, M2J, P, (</xsl:text>
				<!-- pose -->
				<xsl:choose>
					<xsl:when test="string-length($downloadAuxJ) != 0">
						<!-- DownloadAuxJoint is defined, output only 1 aux axis -->
            <xsl:call-template name="outputAuxJVal">
              <xsl:with-param name="auxJ1" select="$auxJ[@DOFNumber = $downloadAuxJ]"/>
            </xsl:call-template>
					</xsl:when>
					<xsl:otherwise> <!-- DownloadAuxJoint is not defined, output all aux joints -->
						<xsl:for-each select="$auxJ">
              <xsl:call-template name="outputAuxJVal">
                <xsl:with-param name="auxJ1" select="."/>
              </xsl:call-template>
 							<xsl:variable name="dofNumber" select="@DOFNumber"/>
							<xsl:if test="$dofNumber != count($activity/Target/JointTarget/Joint | $activity/Target/JointTarget/AuxJoint)">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>), </xsl:text>
				<xsl:call-template name="mechSpeedTool">
					<xsl:with-param name="activity" select="$activity"/>
					<xsl:with-param name="mechNum" select="2"/>
				</xsl:call-template>
			</xsl:if> <!-- JointToMechanismMap not defined -->
			<xsl:if test="string-length($j2mMap) != 0">
				<xsl:choose>
					<xsl:when test="contains($j2mMap, ';')">
            <!-- multiple aux mechanism -->
            <xsl:variable name="j2mMap1" select="substring-before($j2mMap, ';')"/>
						<xsl:variable name="j2mMap2" select="substring-after($j2mMap, ';')"/>
						<!-- DownloadAuxJoint not defined, output all mech defined by JointToMechanismMap -->
						<xsl:if test="string-length($downloadAuxJ) = 0">
              <xsl:call-template name="outputMechSpecs">
                <xsl:with-param name="activity" select="$activity"/>
                <xsl:with-param name="j2mMap1" select="$j2mMap1"/>
                <xsl:with-param name="j2mMap2" select="$j2mMap2"/>
              </xsl:call-template>
						</xsl:if> <!-- parameter DownloadAuxJoint not defined -->
						<!-- DownloadAuxJoint defined, output 1 joint defined by DownloadAuxJoint to mech -->
						<xsl:if test="string-length($downloadAuxJ) != 0">
							<xsl:variable name="mechNum">
                <xsl:call-template name="GetMechNumFromAuxJ">
                  <xsl:with-param name="j2mMap" select="$j2mMap"/>
                  <xsl:with-param name="auxJ" select="$downloadAuxJ"/>
                </xsl:call-template>
							</xsl:variable>
							<xsl:text>, M</xsl:text>
							<xsl:value-of select="$mechNum"/>
							<xsl:text>J, P, (</xsl:text>
							<!-- output aux axis set by DownloadAuxJoint -->
              <xsl:call-template name="outputAuxJVal">
                <xsl:with-param name="auxJ1" select="$auxJ[@DOFNumber = $downloadAuxJ]"/>
              </xsl:call-template>
              <xsl:text>), </xsl:text>
              <xsl:call-template name="mechSpeedTool">
                <xsl:with-param name="activity" select="$activity"/>
                <xsl:with-param name="mechNum" select="$mechNum"/>
              </xsl:call-template>
            </xsl:if> <!-- parameter DownloadAuxJoint defined -->
					</xsl:when> <!-- contains ';', outputs multiple mechanisms -->
					<xsl:otherwise> <!-- does not contain ';', output 1 mech -->
            <xsl:choose>
              <xsl:when test="string-length($downloadAuxJ) = 0">
                <xsl:call-template name="mechSpec1">
                  <xsl:with-param name="activity" select="$activity"/>
                  <xsl:with-param name="j2mMap" select="$j2mMap"/>
                </xsl:call-template>
              </xsl:when>
              <xsl:otherwise>
                <xsl:variable name="mechNum">
									<xsl:call-template name="GetParameterFromJ2M">
										<xsl:with-param name="j2mMap" select="$j2mMap"/>
										<xsl:with-param name="param" select="'Mechanism'"/>
									</xsl:call-template>
								</xsl:variable>
                <xsl:variable name="j2mMap2" select="concat($downloadAuxJ, ':', $mechNum)"/>
                <xsl:call-template name="mechSpec1">
                  <xsl:with-param name="activity" select="$activity"/>
                  <xsl:with-param name="j2mMap" select="$j2mMap2"/>
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
					</xsl:otherwise> <!-- does not contain ';', output 1 mech -->
				</xsl:choose> <!-- contains ';' -->
			</xsl:if> <!-- JointToMechanismMap defined -->
			
		</xsl:if> <!-- there are aux axes and parameter DownloadAuxJoint != 0 -->
	</xsl:template>
	<!-- End of mechSpec -->

  <!-- outputs recursively mechanism specifications -->
  <xsl:template name="outputMechSpecs">
    <xsl:param name="activity"/>
    <xsl:param name="j2mMap1"/>
    <xsl:param name="j2mMap2"/>

    <!-- parameter JointToMechanismMap format: "7,8:2;9:3;10,11,12:4" -->
    <!-- j2mMap1="7,8:2" and j2mMap2="9:3;10,11,12:4" -->
    <!-- outputs 1st part 7,8:2 (joints 7, 8 output to mechanism 2) -->
    <!-- splits j2mMap2 then calls self -->
    <xsl:call-template name="mechSpec1">
      <xsl:with-param name="activity" select="$activity"/>
      <xsl:with-param name="j2mMap" select="$j2mMap1"/>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="contains($j2mMap2, ';') = 'true'">
        <xsl:variable name="j2mMapA" select="$j2mMap2"/>
        <xsl:variable name="j2mMapA1" select="substring-before($j2mMapA, ';')"/>
        <xsl:variable name="j2mMapA2" select="substring-after($j2mMapA, ';')"/>
        <xsl:call-template name="outputMechSpecs">
          <xsl:with-param name="activity" select="$activity"/>
          <xsl:with-param name="j2mMap1" select="$j2mMapA1"/>
          <xsl:with-param name="j2mMap2" select="$j2mMapA2"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mechSpec1">
          <xsl:with-param name="activity" select="$activity"/>
          <xsl:with-param name="j2mMap" select="$j2mMap2"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- End of outputMechSpecs -->

  <!-- outputs 1 mechanism ", M?J, P, (number, ..., number), R=number, H=number" -->
  <xsl:template name="mechSpec1">
    <xsl:param name="activity"/>
    <xsl:param name="j2mMap"/>

    <xsl:variable name="auxJ" select="$activity/Target/JointTarget/AuxJoint"/>
    <xsl:variable name="mechNum">
			<xsl:call-template name="GetParameterFromJ2M">
				<xsl:with-param name="j2mMap" select="$j2mMap"/>
				<xsl:with-param name="param" select="'Mechanism'"/>
			</xsl:call-template>
		</xsl:variable>
    <xsl:variable name="aj">
			<xsl:call-template name="GetParameterFromJ2M">
				<xsl:with-param name="j2mMap" select="$j2mMap"/>
				<xsl:with-param name="param" select="'Joint'"/>
			</xsl:call-template>
		</xsl:variable>
    <xsl:text>, M</xsl:text>
    <xsl:value-of select="$mechNum"/>
    <xsl:text>J, P, (</xsl:text>
    <xsl:call-template name="outputAuxJoints">
      <xsl:with-param name="auxJoints" select="$auxJ"/>
      <xsl:with-param name="aj" select="$aj"/>
    </xsl:call-template>
    <xsl:text>), </xsl:text>
    <xsl:call-template name="mechSpeedTool">
      <xsl:with-param name="activity" select="$activity"/>
      <xsl:with-param name="mechNum" select="$mechNum"/>
    </xsl:call-template>
  </xsl:template>
  <!-- End of mechSpec1 -->

  <!-- outputs recursively values of aux joints specified in parameter aj.
       parameter aj lists aux joints to be output separated by commas.
       parameter auxJoints is AuxJoint node-set.
  -->
  <xsl:template name="outputAuxJoints">
    <xsl:param name="auxJoints"/>
    <xsl:param name="aj"/>

    <xsl:choose>
      <xsl:when test="contains($aj, ',') = 'true'">
        <xsl:variable name="aj1" select="substring-before($aj, ',')"/>
        <xsl:variable name="aj2" select="substring-after($aj, ',')"/>
        <xsl:call-template name="outputAuxJVal">
          <xsl:with-param name="auxJ1" select="$auxJoints[@DOFNumber=$aj1]"/>
        </xsl:call-template>
        <xsl:text>, </xsl:text>
        <xsl:call-template name="outputAuxJoints">
          <xsl:with-param name="auxJoints" select="$auxJoints"/>
          <xsl:with-param name="aj" select="$aj2"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="outputAuxJVal">
          <xsl:with-param name="auxJ1" select="$auxJoints[@DOFNumber=$aj]"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- End of outputAuxJoints -->
  
  <!-- outputs 1 joint value of AuxJoint node -->
  <xsl:template name="outputAuxJVal">
    <xsl:param name="auxJ1"/>
    
    <xsl:variable name="auxJVal">
      <xsl:call-template name="Scientific">
        <xsl:with-param name="Num" select="$auxJ1/JointValue"/>
        <xsl:with-param name="Units" select="1.0"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
			<xsl:when test="$auxJ1/@JointType='Translational'">
        <xsl:value-of select="format-number($auxJVal*1000.0, $targetNumberPattern)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number($auxJVal, $targetNumberPattern)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="GetMechNumFromAuxJ">
    <xsl:param name="j2mMap"/>
    <xsl:param name="auxJ"/>

    <xsl:choose>
      <xsl:when test="contains($j2mMap, ';')">
        <xsl:variable name="j2mMap1" select="substring-before($j2mMap, ';')"/>
        <xsl:variable name="j2mMap2" select="substring-after($j2mMap, ';')"/>
        <xsl:variable name="aj1">
					<xsl:call-template name="GetParameterFromJ2M">
						<xsl:with-param name="j2mMap" select="$j2mMap1"/>
						<xsl:with-param name="param" select="'Joint'"/>
					</xsl:call-template>
				</xsl:variable>
        <xsl:variable name="mechNum">
					<xsl:call-template name="GetParameterFromJ2M">
						<xsl:with-param name="j2mMap" select="$j2mMap1"/>
						<xsl:with-param name="param" select="'Mechanism'"/>
					</xsl:call-template>
				</xsl:variable>
        <xsl:choose>
          <xsl:when test="contains($aj1, $auxJ)">
            <xsl:value-of select="$mechNum"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="GetMechNumFromAuxJ">
              <xsl:with-param name="j2mMap" select="$j2mMap2"/>
              <xsl:with-param name="auxJ" select="$auxJ"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when> <!-- contains ';' -->
      <xsl:otherwise>
        <xsl:variable name="aj1">
					<xsl:call-template name="GetParameterFromJ2M">
						<xsl:with-param name="j2mMap" select="$j2mMap"/>
						<xsl:with-param name="param" select="'Joint'"/>
					</xsl:call-template>
				</xsl:variable>
        <xsl:variable name="mechNum">
					<xsl:call-template name="GetParameterFromJ2M">
						<xsl:with-param name="j2mMap" select="$j2mMap"/>
						<xsl:with-param name="param" select="'Mechanism'"/>
					</xsl:call-template>
				</xsl:variable>
        <xsl:choose>
          <xsl:when test="contains($aj1, $auxJ)">
            <xsl:value-of select="$mechNum"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
            <xsl:text>Error: No mechanism number found.</xsl:text>
            <xsl:value-of select="$cr"/>
            <xsl:text>ERROR INFO END</xsl:text>
            <xsl:text>2</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- End of GetMechNumFromAuxJ -->
	
	<!-- parameter JointToMechanismMap format: "7,8:2;9:3;10,11,12:4" -->
	<!-- parameter j2mMap passed in is split by ';', e.g. "7,8:2" -->
	<!-- For R24 and prior, it is "7,2" (limited to 1 joint) -->
	<!-- This template is used to get Joint (before separator ':' or ',') -->
	<!-- or Mechanism Number (after separator ':' or ',')) -->
	<xsl:template name="GetParameterFromJ2M">
		<xsl:param name="j2mMap"/>
		<xsl:param name="param"/>
		
		<xsl:if test="$param='Joint'">
			<xsl:choose>
				<xsl:when test="contains($j2mMap, ':') = 'true'">
					<xsl:value-of select="substring-before($j2mMap, ':')"/>
				</xsl:when>
				<xsl:when test="contains($j2mMap, ',') = 'true'">
					<xsl:value-of select="substring-before($j2mMap, ',')"/>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$param='Mechanism'">
			<xsl:choose>
				<xsl:when test="contains($j2mMap, ':') = 'true'">
					<xsl:value-of select="substring-after($j2mMap, ':')"/>
				</xsl:when>
				<xsl:when test="contains($j2mMap, ',') = 'true'">
					<xsl:value-of select="substring-after($j2mMap, ',')"/>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	
  <xsl:template name="mechSpec7">
    <xsl:param name="activity"/>

    <xsl:text>, M2J, P, (</xsl:text>
    <xsl:variable name="j1">
      <xsl:call-template name="Scientific">
        <xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 1']/JointValue"/>
        <xsl:with-param name="Units" select="1.0"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="format-number($j1, $targetNumberPattern)"/>
    <xsl:text>), </xsl:text>
    <xsl:call-template name="mechSpeedTool">
      <xsl:with-param name="activity" select="$activity"/>
      <xsl:with-param name="mechNum" select="2"/>
    </xsl:call-template>

  </xsl:template>
  <!-- End of mechSpec7 -->

  <!-- regular 6-axis robot target output -->
  <xsl:template name="outputPose">
    <xsl:param name="motionActivityNode"/>

    <xsl:text>(</xsl:text>

    <!-- when outputting MOVEX, ignore RailAxisIsJoint1 -->
    <xsl:variable name="railJoint1">
      <xsl:choose>
        <xsl:when test="$outputMOVEX = 'true'">
          <xsl:value-of select="'false'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="dnbigpolp:GetParameterData('RailAxisIsJoint1')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="railNodes" select="$motionActivityNode/Target/JointTarget/AuxJoint[@Type='RailTrackGantry']"/>
    <xsl:if test="$railJoint1 = 'true' and boolean($railNodes) = 'true'">
      <xsl:for-each select="$railNodes">
        <xsl:variable name="auxJointValue">
          <xsl:call-template name="Scientific">
            <xsl:with-param name="Num" select="JointValue"/>
            <xsl:with-param name="Units" select="1.0"/>
          </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="jointType" select="@JointType"/>
        <xsl:choose>
          <xsl:when test="$jointType='Translational'">
            <xsl:value-of select="format-number($auxJointValue*1000.0, $targetNumberPattern)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="format-number($auxJointValue, $targetNumberPattern)"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,</xsl:text>
      </xsl:for-each>
    </xsl:if>

		<xsl:variable name="targetTargetType" select="$motionActivityNode/Target/@Default"/>
		<xsl:variable name="targetType">
			<xsl:call-template name="getTargetType">
				<xsl:with-param name="targetTargetType" select="$targetTargetType"/>
			</xsl:call-template>
		</xsl:variable>
    <!-- Cartesian target components -->
    <xsl:if test="$targetType = 'Cartesian'">
      <!-- Cartesian target position and orientation values. Target offset is defined with respect to object frame or if object frame
				offset is zero (x, y, z, roll, pitch, and yaw values are all set to 0) with respect to robot's base frame -->
      <!-- base wrt world -->
      <xsl:variable name="base" select="$motionActivityNode/Target/BaseWRTWorld"/>
      <xsl:variable name="bsX" select="$base/Position/@X"/>
      <!-- Robot base location offset in Y direction -->
      <xsl:variable name="bsY" select="$base/Position/@Y"/>
      <!-- Robot base location offset in Z direction -->
      <xsl:variable name="bsZ" select="$base/Position/@Z"/>
      <!-- Robot base orientation offset in Yaw direction -->
      <xsl:variable name="bsW" select="$base/Orientation/@Yaw"/>
      <!-- Robot base orientation offset in Pitch direction -->
      <xsl:variable name="bsP" select="$base/Orientation/@Pitch"/>
      <!-- Robot base orientation offset in Roll direction -->
      <xsl:variable name="bsR" select="$base/Orientation/@Roll"/>
      <!-- object frame -->
      <xsl:variable name="objName" select="$motionActivityNode/MotionAttributes/ObjectFrameProfile"/>
      <xsl:variable name="obj" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objName]"/>
      <xsl:variable name="objX" select="$obj/ObjectFrame/ObjectFramePosition/@X"/>
      <xsl:variable name="objY" select="$obj/ObjectFrame/ObjectFramePosition/@Y"/>
      <xsl:variable name="objZ" select="$obj/ObjectFrame/ObjectFramePosition/@Z"/>
      <xsl:variable name="objW" select="$obj/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
      <xsl:variable name="objP" select="$obj/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
      <xsl:variable name="objR" select="$obj/ObjectFrame/ObjectFrameOrientation/@Roll"/>
      <xsl:variable name="objZero" select="$objX*$objX + $objY*$objY + $objZ*$objZ + $objW*$objW + $objP*$objP + $objR*$objR"/>
      <!-- Target location offset in X direction -->
      <xsl:variable name="target" select="$motionActivityNode/Target/CartesianTarget"/>
      <xsl:variable name="tpX">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="$target/Position/@X"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- Target location offset in Y direction -->
      <xsl:variable name="tpY">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="$target/Position/@Y"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- Target location offset in Z direction -->
      <xsl:variable name="tpZ">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="$target/Position/@Z"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- Target orientation offset in Yaw direction -->
      <xsl:variable name="toYaw">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="$target/Orientation/@Yaw"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- Target orientation offset in Pitch direction -->
      <xsl:variable name="toPitch">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="$target/Orientation/@Pitch"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- Target orientation offset in Roll direction -->
      <xsl:variable name="toRoll">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="$target/Orientation/@Roll"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- target wrt robot -->
      <xsl:variable name="tgtxyzwpr" select="concat($tpX, ',', $tpY, ',', $tpZ, ',', $toYaw, ',', $toPitch, ',', $toRoll)"/>
      <xsl:choose>
        <xsl:when test="$objZero > 0">
          <!-- for non-zero object frame, position output is relative to object frame -->
          <xsl:variable name="basexyzwpr" select="concat($bsX, ',', $bsY, ',', $bsZ, ',', $bsW, ',', $bsP, ',', $bsR)"/>
          <xsl:variable name="bsmat" select="MatrixUtils:dgXyzyprToMatrix($basexyzwpr)"/>
          <xsl:variable name="bsmatinv" select="MatrixUtils:dgInvert()"/>
          <xsl:variable name="objxyzwpr" select="concat($objX, ',', $objY, ',', $objZ, ',', $objW, ',', $objP, ',', $objR)"/>
          <xsl:variable name="objmat" select="MatrixUtils:dgCatXyzyprMatrix($objxyzwpr)"/>
					<xsl:if test="$obj/@ApplyOffsetToTags = 'On'">
						<xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix($objxyzwpr)"/>
					</xsl:if>
          <xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix($tgtxyzwpr)"/>
        </xsl:when>
        <xsl:otherwise>
          <!-- for zero object frame, position output is relative to robot -->
          <xsl:variable name="result" select="MatrixUtils:dgXyzyprToMatrix($tgtxyzwpr)"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="targetPositionX">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
          <xsl:with-param name="Units" select="1000"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="targetPositionY">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
          <xsl:with-param name="Units" select="1000"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="targetPositionZ">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
          <xsl:with-param name="Units" select="1000"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="targetOrientationRoll">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="MatrixUtils:dgGetRoll()"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="targetOrientationPitch">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="MatrixUtils:dgGetPitch()"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="targetOrientationYaw">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="MatrixUtils:dgGetYaw()"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- Configuration string name -->
      <xsl:variable name="configurationName" select="$motionActivityNode/Target/CartesianTarget/Config/@Name"/>
      <!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
				1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models. -->
      <!-- If turn numbers have been defined for this target, return value, stored in variables below, will not be empty. 
				Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
      <!-- Turn number value for joint 1, empty if doesn't exist -->
      <xsl:variable name="turnNumber1" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 1]/@Value"/>
      <!-- Turn number value for joint 4, empty if doesn't exist -->
      <xsl:variable name="turnNumber4" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 4]/@Value"/>
      <!-- Turn number value for joint 5, empty if doesn't exist -->
      <xsl:variable name="turnNumber5" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 5]/@Value"/>
      <!-- Turn number value for joint 6, empty if doesn't exist -->
      <xsl:variable name="turnNumber6" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 6]/@Value"/>
      <!-- Get all  turn signs. If the robot uses turn signs, there will be 4 joints that will always have turn signs enabled: 
				1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does for all robot models. -->
      <!-- If turn signs have been defined for this target, return value, stored in variables below, will not be empty. 
				Before creating turn sign output in the result document, variables below need to be tested for validity in xsl:if element -->
      <!-- Turn sign value for joint 1, empty if doesn't exist -->
      <xsl:variable name="turnSign1" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 1]/@Value"/>
      <!-- Turn sign value for joint 4, empty if doesn't exist -->
      <xsl:variable name="turnSign4" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 4]/@Value"/>
      <!-- Turn sign value for joint 5, empty if doesn't exist -->
      <xsl:variable name="turnSign5" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 5]/@Value"/>
      <!-- Turn sign value for joint 6, empty if doesn't exist -->
      <xsl:variable name="turnSign6" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 6]/@Value"/>
      <!-- Retrieve tag name, and possibly, tag group name and the name of the part to which that tag group is attached to -->
      <!-- If this Cartesian target has a Tag element defined, tagName variable will not be empty, and if the tag is contained in a tag group
				which is attached to a part, tagGroupName and partOwnerName variables will not be empty.  Before creating tag output
				 in the result document, variables below need to be tested for validity in xsl:if element -->
      <!-- Tag name, empty if doesn't exist -->
      <xsl:variable name="tagName" select="$motionActivityNode/Target/CartesianTarget/Tag"/>
      <!-- Tag group name that contains the above tag, empty if doesn't exist -->
      <xsl:variable name="tagGroupName" select="$motionActivityNode/Target/CartesianTarget/Tag/@TagGroup"/>
      <!-- Part name that has the above tag group attached, empty if doesn't exist -->
      <xsl:variable name="partOwnerName" select="$motionActivityNode/Target/CartesianTarget/Tag/@AttachedToPart"/>
      <!-- output cartesian coordinates -->
      <xsl:value-of select="format-number($targetPositionX, $targetNumberPattern)"/>
      <xsl:text>,</xsl:text>
      <xsl:value-of select="format-number($targetPositionY, $targetNumberPattern)"/>
      <xsl:text>,</xsl:text>
      <xsl:value-of select="format-number($targetPositionZ, $targetNumberPattern)"/>
      <xsl:text>,</xsl:text>
      <xsl:value-of select="format-number($targetOrientationRoll, $targetNumberPattern)"/>
      <xsl:text>,</xsl:text>
      <xsl:value-of select="format-number($targetOrientationPitch, $targetNumberPattern)"/>
      <xsl:text>,</xsl:text>
      <xsl:value-of select="format-number($targetOrientationYaw, $targetNumberPattern)"/>
    </xsl:if>
    <!-- end targetType 'Cartesian' -->
    <!-- If this target has been defined in joint coordinates -->
    <xsl:if test="$targetType = 'Joint'">
      <xsl:variable name="joint3linked" select="dnbigpolp:GetParameterData(string('Joint3Linked'))"/>
      <xsl:variable name="joint3adjust" select="dnbigpolp:GetParameterData(string('Joint3Adjust'))"/>
      <xsl:variable name="axis3adjust" select="dnbigpolp:GetParameterData(string('Axis3Adjust'))"/>
      <!-- Get joint names, values, types and dof numbers-->
      <xsl:if test="count($motionActivityNode/Target/JointTarget/Joint) > 0">
        <xsl:variable name="j2" select="number(Target/JointTarget/Joint[@JointName='Joint 2']/JointValue)"/>
        <!-- For each base robot joint (non-auxiliary joint), get -->
        <xsl:for-each select="$motionActivityNode/Target/JointTarget/Joint">
          <!-- Joint name -->
          <xsl:variable name="jointName" select="@JointName"/>
          <!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
          <xsl:variable name="jointType" select="@JointType"/>
          <!-- Degree of freedom number of this joint -->
          <xsl:variable name="dofNumber" select="@DOFNumber"/>
          <!-- Joint value -->
          <!--<xsl:variable name="jointValue" select="JointValue"/>-->
          <xsl:variable name="jointValue">
            <xsl:call-template name="Scientific">
              <xsl:with-param name="Num" select="JointValue"/>
              <xsl:with-param name="Units" select="1.0"/>
            </xsl:call-template>
          </xsl:variable>
          <!-- Output joint value to the result document -->
          <!-- Test if auxiliary target exists, since this may affect the output -->
          <xsl:choose>
            <xsl:when test="$joint3adjust = 'false' or $joint3adjust = 'FALSE' or $dofNumber != 3">
              <xsl:value-of select="format-number($jointValue, $targetNumberPattern)"/>
            </xsl:when>
            <xsl:when test="($joint3linked = 'false' or $joint3linked = 'FALSE' or string($joint3linked) = '') and $dofNumber = 3">
              <xsl:variable name="tmpVar" select="$jointValue + $j2"/>
              <!-- robot_axes_3 = igrip_axes_3 + igrip_axes_2 - axis3adj -->
              <xsl:variable name="robot_axes_3">
                <xsl:choose>
                  <xsl:when test="string($axis3adjust) = ''">
                    <xsl:value-of select="$tmpVar - 90"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$tmpVar - $axis3adjust"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:variable name="nj3">
                <xsl:call-template name="Scientific">
                  <xsl:with-param name="Num" select="$robot_axes_3"/>
                  <xsl:with-param name="Units" select="1.0"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:value-of select="format-number($nj3, $targetNumberPattern)"/>
            </xsl:when>
            <xsl:when test="($joint3linked = 'true' or $joint3linked = 'TRUE') and $dofNumber = 3">
              <xsl:variable name="tmpVar" select="$jointValue - $j2"/>
              <!-- robot_axes_3 = igrip_axes_3 + igrip_axes_2 - axis3adj -->
              <xsl:variable name="robot_axes_3">
                <xsl:choose>
                  <xsl:when test="string($axis3adjust) = ''">
                    <xsl:value-of select="$tmpVar + 90"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$tmpVar + $axis3adjust"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:variable name="nj3">
                <xsl:call-template name="Scientific">
                  <xsl:with-param name="Num" select="$robot_axes_3"/>
                  <xsl:with-param name="Units" select="1.0"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:value-of select="format-number($nj3, $targetNumberPattern)"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number($jointValue, $targetNumberPattern)"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="$dofNumber != count($motionActivityNode/Target/JointTarget/Joint)">
            <xsl:text>,</xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
    </xsl:if>
    <!-- end targetType 'Joint' -->
    <!-- auxiliary joints -->
    <xsl:if test="$outputMOVEX = 'false'">
      <xsl:for-each select="$motionActivityNode/Target/JointTarget/AuxJoint">
        <xsl:choose>
          <xsl:when test="$railJoint1 = 'true' and @Type = 'RailTrackGantry'">
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>,</xsl:text>
            <xsl:variable name="auxJointValue">
              <xsl:call-template name="Scientific">
                <xsl:with-param name="Num" select="JointValue"/>
                <xsl:with-param name="Units" select="1.0"/>
              </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="@JointType='Translational'">
                <xsl:value-of select="format-number($auxJointValue*1000.0, $targetNumberPattern)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number($auxJointValue, $targetNumberPattern)"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:if>

    <xsl:text>), </xsl:text>
  </xsl:template>
  <!-- End of outputPose -->

  <!-- 7-axis robot target output, MOVEX only, no auxiliary axis -->
  <xsl:template name="outputPose7">
		<xsl:param name="motionActivityNode"/>

		<xsl:text>(</xsl:text>

		<xsl:variable name="targetTargetType" select="$motionActivityNode/Target/@Default"/>
		<xsl:variable name="targetType">
			<xsl:call-template name="getTargetType">
				<xsl:with-param name="targetTargetType" select="$targetTargetType"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Cartesian target components -->
		<xsl:if test="$targetType = 'Cartesian'">
			<!-- Cartesian target position and orientation values. Target offset is defined with respect to object frame or if object frame
				offset is zero (x, y, z, roll, pitch, and yaw values are all set to 0) with respect to robot's base frame -->
			<!-- base wrt world -->
			<xsl:variable name="base" select="$motionActivityNode/Target/BaseWRTWorld"/>
			<xsl:variable name="bsX" select="$base/Position/@X"/>
			<!-- Robot base location offset in Y direction -->
			<xsl:variable name="bsY" select="$base/Position/@Y"/>
			<!-- Robot base location offset in Z direction -->
			<xsl:variable name="bsZ" select="$base/Position/@Z"/>
			<!-- Robot base orientation offset in Yaw direction -->
			<xsl:variable name="bsW" select="$base/Orientation/@Yaw"/>
			<!-- Robot base orientation offset in Pitch direction -->
			<xsl:variable name="bsP" select="$base/Orientation/@Pitch"/>
			<!-- Robot base orientation offset in Roll direction -->
			<xsl:variable name="bsR" select="$base/Orientation/@Roll"/>
			<!-- object frame -->
			<xsl:variable name="objName" select="$motionActivityNode/MotionAttributes/ObjectFrameProfile"/>
			<xsl:variable name="obj" select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objName]"/>
			<xsl:variable name="objX" select="$obj/ObjectFrame/ObjectFramePosition/@X"/>
			<xsl:variable name="objY" select="$obj/ObjectFrame/ObjectFramePosition/@Y"/>
			<xsl:variable name="objZ" select="$obj/ObjectFrame/ObjectFramePosition/@Z"/>
			<xsl:variable name="objW" select="$obj/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
			<xsl:variable name="objP" select="$obj/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
			<xsl:variable name="objR" select="$obj/ObjectFrame/ObjectFrameOrientation/@Roll"/>
			<xsl:variable name="objZero" select="$objX*$objX + $objY*$objY + $objZ*$objZ + $objW*$objW + $objP*$objP + $objR*$objR"/>
			<!-- Target location offset in X direction -->
			<xsl:variable name="target" select="$motionActivityNode/Target/CartesianTarget"/>
			<xsl:variable name="tpX">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$target/Position/@X"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target location offset in Y direction -->
			<xsl:variable name="tpY">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$target/Position/@Y"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target location offset in Z direction -->
			<xsl:variable name="tpZ">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$target/Position/@Z"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target orientation offset in Yaw direction -->
			<xsl:variable name="toYaw">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$target/Orientation/@Yaw"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target orientation offset in Pitch direction -->
			<xsl:variable name="toPitch">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$target/Orientation/@Pitch"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Target orientation offset in Roll direction -->
			<xsl:variable name="toRoll">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="$target/Orientation/@Roll"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- target wrt robot -->
			<xsl:variable name="tgtxyzwpr" select="concat($tpX, ',', $tpY, ',', $tpZ, ',', $toYaw, ',', $toPitch, ',', $toRoll)"/>
			<xsl:choose>
				<xsl:when test="$objZero > 0">
					<!-- for non-zero object frame, position output is relative to object frame -->
					<xsl:variable name="basexyzwpr" select="concat($bsX, ',', $bsY, ',', $bsZ, ',', $bsW, ',', $bsP, ',', $bsR)"/>
					<xsl:variable name="bsmat" select="MatrixUtils:dgXyzyprToMatrix($basexyzwpr)"/>
					<xsl:variable name="bsmatinv" select="MatrixUtils:dgInvert()"/>
					<xsl:variable name="objxyzwpr" select="concat($objX, ',', $objY, ',', $objZ, ',', $objW, ',', $objP, ',', $objR)"/>
					<xsl:variable name="objmat" select="MatrixUtils:dgCatXyzyprMatrix($objxyzwpr)"/>
					<xsl:if test="$obj/@ApplyOffsetToTags = 'On'">
						<xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix($objxyzwpr)"/>
					</xsl:if>
					<xsl:variable name="result" select="MatrixUtils:dgCatXyzyprMatrix($tgtxyzwpr)"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- for zero object frame, position output is relative to robot -->
					<xsl:variable name="result" select="MatrixUtils:dgXyzyprToMatrix($tgtxyzwpr)"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:variable name="targetPositionX">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
					<xsl:with-param name="Units" select="1000"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="targetPositionY">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
					<xsl:with-param name="Units" select="1000"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="targetPositionZ">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
					<xsl:with-param name="Units" select="1000"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="targetOrientationRoll">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="MatrixUtils:dgGetRoll()"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="targetOrientationPitch">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="MatrixUtils:dgGetPitch()"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="targetOrientationYaw">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="MatrixUtils:dgGetYaw()"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Configuration string name -->
			<xsl:variable name="configurationName" select="$motionActivityNode/Target/CartesianTarget/Config/@Name"/>
			<!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
				1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models. -->
			<!-- If turn numbers have been defined for this target, return value, stored in variables below, will not be empty. 
				Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
			<!-- Turn number value for joint 1, empty if doesn't exist -->
			<xsl:variable name="turnNumber1" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 1]/@Value"/>
			<!-- Turn number value for joint 4, empty if doesn't exist -->
			<xsl:variable name="turnNumber4" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 4]/@Value"/>
			<!-- Turn number value for joint 5, empty if doesn't exist -->
			<xsl:variable name="turnNumber5" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 5]/@Value"/>
			<!-- Turn number value for joint 6, empty if doesn't exist -->
			<xsl:variable name="turnNumber6" select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 6]/@Value"/>
			<!-- Get all  turn signs. If the robot uses turn signs, there will be 4 joints that will always have turn signs enabled: 
				1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does for all robot models. -->
			<!-- If turn signs have been defined for this target, return value, stored in variables below, will not be empty. 
				Before creating turn sign output in the result document, variables below need to be tested for validity in xsl:if element -->
			<!-- Turn sign value for joint 1, empty if doesn't exist -->
			<xsl:variable name="turnSign1" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 1]/@Value"/>
			<!-- Turn sign value for joint 4, empty if doesn't exist -->
			<xsl:variable name="turnSign4" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 4]/@Value"/>
			<!-- Turn sign value for joint 5, empty if doesn't exist -->
			<xsl:variable name="turnSign5" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 5]/@Value"/>
			<!-- Turn sign value for joint 6, empty if doesn't exist -->
			<xsl:variable name="turnSign6" select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 6]/@Value"/>
			<!-- Retrieve tag name, and possibly, tag group name and the name of the part to which that tag group is attached to -->
			<!-- If this Cartesian target has a Tag element defined, tagName variable will not be empty, and if the tag is contained in a tag group
				which is attached to a part, tagGroupName and partOwnerName variables will not be empty.  Before creating tag output
				 in the result document, variables below need to be tested for validity in xsl:if element -->
			<!-- Tag name, empty if doesn't exist -->
			<xsl:variable name="tagName" select="$motionActivityNode/Target/CartesianTarget/Tag"/>
			<!-- Tag group name that contains the above tag, empty if doesn't exist -->
			<xsl:variable name="tagGroupName" select="$motionActivityNode/Target/CartesianTarget/Tag/@TagGroup"/>
			<!-- Part name that has the above tag group attached, empty if doesn't exist -->
			<xsl:variable name="partOwnerName" select="$motionActivityNode/Target/CartesianTarget/Tag/@AttachedToPart"/>
			<!-- output cartesian coordinates -->
			<xsl:value-of select="format-number($targetPositionX, $targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetPositionY, $targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetPositionZ, $targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetOrientationRoll, $targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetOrientationPitch, $targetNumberPattern)"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="format-number($targetOrientationYaw, $targetNumberPattern)"/>
      <xsl:text>,</xsl:text>
      <xsl:variable name="j1">
        <xsl:call-template name="Scientific">
          <xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 1']/JointValue"/>
          <xsl:with-param name="Units" select="1.0"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:value-of select="format-number($j1, $targetNumberPattern)"/>
    </xsl:if>
		<!-- end targetType 'Cartesian' -->
		<!-- If this target has been defined in joint coordinates -->
		<xsl:if test="$targetType = 'Joint'">
			<xsl:variable name="joint3linked" select="dnbigpolp:GetParameterData(string('Joint3Linked'))"/>
			<xsl:variable name="joint3adjust" select="dnbigpolp:GetParameterData(string('Joint3Adjust'))"/>
			<xsl:variable name="axis3adjust" select="dnbigpolp:GetParameterData(string('Axis3Adjust'))"/>
			<!-- Get joint names, values, types and dof numbers-->
			<xsl:if test="count($motionActivityNode/Target/JointTarget/Joint) > 0">
        <!-- output joints 2,7 -->
        <xsl:variable name="j2">
          <xsl:call-template name="Scientific">
            <xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 2']/JointValue"/>
            <xsl:with-param name="Units" select="1.0"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="format-number($j2, $targetNumberPattern)"/>
        <xsl:text>,</xsl:text>
        <xsl:variable name="j7">
          <xsl:call-template name="Scientific">
            <xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 7']/JointValue"/>
            <xsl:with-param name="Units" select="1.0"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="format-number($j7, $targetNumberPattern)"/>
        <xsl:text>,</xsl:text>
        <!-- output joints 3,4,5,6 -->
				<xsl:for-each select="$motionActivityNode/Target/JointTarget/Joint">
					<!-- Degree of freedom number of this joint -->
					<xsl:variable name="dofNumber" select="@DOFNumber"/>
          <xsl:if test="$dofNumber &gt; 2 and $dofNumber &lt; 7">
            <xsl:variable name="jointValue">
              <xsl:call-template name="Scientific">
                <xsl:with-param name="Num" select="JointValue"/>
                <xsl:with-param name="Units" select="1.0"/>
              </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="format-number($jointValue, $targetNumberPattern)"/>

            <xsl:if test="$dofNumber != 6">
              <xsl:text>,</xsl:text>
            </xsl:if>
          </xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:if>
		<!-- end targetType 'Joint' -->
		
		<xsl:text>), </xsl:text>
	</xsl:template>
	<!-- End of outputPose7 -->

	<xsl:template name="outputPoseNode">
		<xsl:param name="followPathActivityNode"/>

		<xsl:text>(</xsl:text>
		<!-- Get node number -->
		<xsl:variable name="nodeNumber" select="@Number"/>
		<!-- Node position and orientation values. Node offset is always defined with respect to workcell's World coordinate system -->
		<!-- Node location offset in X direction -->
		<xsl:variable name="nodePositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Node location offset in Y direction -->
		<xsl:variable name="nodePositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Node location offset in Z direction -->
		<xsl:variable name="nodePositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Node orientation offset in Yaw direction -->
		<xsl:variable name="nodeOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Node orientation offset in Pitch direction -->
		<xsl:variable name="nodeOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Node orientation offset in Roll direction -->
		<xsl:variable name="nodeOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="format-number($nodePositionX, $targetNumberPattern)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($nodePositionY, $targetNumberPattern)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($nodePositionZ, $targetNumberPattern)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($nodeOrientationYaw, $targetNumberPattern)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($nodeOrientationPitch, $targetNumberPattern)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($nodeOrientationRoll, $targetNumberPattern)"/>
		<!-- Handle aux axes here -->
		<!-- end of aux axes handling -->
		<xsl:text>)</xsl:text>
	</xsl:template>
	<!-- End of outputPoseNode -->

	<xsl:template name="outputSpeedTime">
		<xsl:param name="speed"/>
		<xsl:param name="motionBasis"/>

		<xsl:choose>
			<xsl:when test="$motionBasis='Percent'">
				<xsl:text>R=</xsl:text>
				<xsl:choose>
					<xsl:when test="$speed &lt;= 0">
						<xsl:value-of select="format-number(1, $SR_speedPattern)"/>
					</xsl:when>
					<xsl:when test="$speed &gt;= 100">
						<xsl:value-of select="format-number(100, $SR_speedPattern)"/>
					</xsl:when>
					<xsl:when test="$speed &gt; 0 and $speed &lt; 100">
						<xsl:value-of select="format-number($speed, $SR_speedPattern)"/>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$motionBasis='Absolute'">
				<xsl:text>S=</xsl:text>
				<xsl:choose>
					<xsl:when test="$speed &lt;= 0">
						<xsl:value-of select="format-number(1, $SR_speedPattern)"/>
					</xsl:when>
					<xsl:when test="$speed &gt; $maxSpeed">
						<xsl:value-of select="format-number($maxSpeed * 1000, $SR_speedPattern)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number($speed * 1000, $SR_speedPattern)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$motionBasis='Time'">
				<xsl:text>T=</xsl:text>
				<xsl:choose>
					<xsl:when test="$speed &lt;= 0">
						<xsl:value-of select="format-number(0.1, $decimalNumberPattern)"/>
					</xsl:when>
					<xsl:when test="$speed &gt;=100">
						<xsl:value-of select="format-number(100, $decimalNumberPattern)"/>
					</xsl:when>
					<xsl:when test="$speed &gt; 0 and $speed &lt; 100">
						<xsl:value-of select="format-number($speed, $decimalNumberPattern)"/>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
		<xsl:text>, </xsl:text>
	</xsl:template>
	<!-- End of outputSpeedTime -->

	<xsl:template name="outputAccuracy">
		<xsl:param name="accuracyProfile"/>

		<xsl:variable name="flyByMode" select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/FlyByMode"/>
		<!-- Rounding criterium: 'Speed' or 'Distance' -->
		<xsl:variable name="accuracyType" select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyType"/>
		<!-- Rounding precision value -->
		<xsl:variable name="accuracyValue" select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyValue/@Value"/>
		<xsl:text>A=</xsl:text>
		<xsl:call-template name="GetAccuracy">
			<xsl:with-param name="accuracyValue" select="$accuracyValue"/>
			<xsl:with-param name="accuracyType" select="$accuracyType"/>
		</xsl:call-template>
		<xsl:if test="$flyByMode='Off'">
			<xsl:text>P</xsl:text>
		</xsl:if>
		<xsl:text>, </xsl:text>
	</xsl:template>
	<!-- End of outputAccuracy -->

	<xsl:template name="outputAcceleration">
		<xsl:param name="accel"/>

		<xsl:variable name="accelSpec" select="floor((100 - $accel) div 25)"/>
		<!-- xsl:if test="$accelSpec != 0" --><!-- comment out to always output AC -->
			<xsl:text>AC=</xsl:text>
			<xsl:value-of select="$accelSpec"/>
			<xsl:text>, </xsl:text>
		<!-- /xsl:if -->
	</xsl:template>
	<!-- End of outputAcceleration -->

	<xsl:template name="outputSmoothness">
		<xsl:param name="activity"/>

		<xsl:variable name="smActivity" select="$activity/AttributeList/Attribute[AttributeName = 'Smoothness']/AttributeValue"/>
		<xsl:variable name="smActList" select="$activity/../AttributeList/Attribute[AttributeName = 'Smoothness']/AttributeValue"/>
		<xsl:variable name="smParam" select="dnbigpolp:GetParameterData('Smoothness')"/>
		<xsl:choose>
			<xsl:when test="$smActivity">
				<xsl:text>SM=</xsl:text>
				<xsl:value-of select="$smActivity"/>
				<xsl:text>, </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$smActList">
						<xsl:text>SM=</xsl:text>
						<xsl:value-of select="$smActList"/>
						<xsl:text>, </xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>SM=</xsl:text>
						<xsl:value-of select="$smParam"/>
						<xsl:text>, </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- End of outputSmoothness -->

	<xsl:template name="outputCoordination">
		<xsl:param name="activity"/>

		<xsl:choose>
			<xsl:when test="$numberOfAuxiliaryAxes + $numberOfExternalAxes + $numberOfPositionerAxes = 0"/>
			<xsl:otherwise>
				<xsl:variable name="hmActivity" select="$activity/AttributeList/Attribute[AttributeName = 'Coordination']/AttributeValue"/>
				<xsl:variable name="hmActList" select="$activity/../AttributeList/Attribute[AttributeName = 'Coordination']/AttributeValue"/>
				<xsl:variable name="hmParam" select="dnbigpolp:GetParameterData('Coordination')"/>
				<xsl:choose>
					<xsl:when test="$hmActivity">
						<xsl:if test="$hmActivity &gt; 0">
							<xsl:text>HM=</xsl:text>
							<xsl:value-of select="$hmActivity"/>
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$hmActList">
								<xsl:if test="$hmActList &gt; 0">
									<xsl:text>HM=</xsl:text>
									<xsl:value-of select="$hmActList"/>
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="$hmParam and $hmParam &gt; 0">
									<xsl:text>HM=</xsl:text>
									<xsl:value-of select="$hmParam"/>
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- End of outputCoordination -->

	<xsl:template name="outputSpeedRef">
		<xsl:param name="activity"/>

		<xsl:variable name="msActivity" select="$activity/AttributeList/Attribute[AttributeName = 'SpeedReference']/AttributeValue"/>
		<xsl:variable name="msActList" select="$activity/../AttributeList/Attribute[AttributeName = 'SpeedReference']/AttributeValue"/>
		<xsl:variable name="msParam" select="dnbigpolp:GetParameterData('SpeedReference')"/>
		<xsl:choose>
			<xsl:when test="$msActivity">
				<xsl:if test="$msActivity = 1">, MS</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$msActList">
						<xsl:if test="$msActList = 1">, MS</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$msParam and $msParam = 1">, MS</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- End of outputSpeedRef -->

  <!-- This template checks if Activity Attribute pName exits, and if it's value is pValue, return 1;
       Then it checks if ActivityList has Attribute pName, if not then checks if Device has such a parameter.
  -->
  <xsl:template name="CheckParameterValue">
    <xsl:param name="activity"/>
    <xsl:param name="pName"/>
    <xsl:param name="pValue"/>

    <xsl:variable name="actVal" select="$activity/AttributeList/Attribute[AttributeName = $pName]/AttributeValue"/>
    <xsl:variable name="actListVal" select="$activity/../AttributeList/Attribute[AttributeName = $pName]/AttributeValue"/>
    <xsl:variable name="devParamVal" select="dnbigpolp:GetParameterData($pName)"/>
    <xsl:choose>
      <xsl:when test="$actVal">
        <xsl:choose>
          <xsl:when test="$actVal = $pValue">1</xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$actListVal">
        <xsl:choose>
          <xsl:when test="$actListVal = $pValue">1</xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$devParamVal">
        <xsl:choose>
          <xsl:when test="$devParamVal = $pValue">1</xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>0</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- End of CheckParameterValue -->
  
	<xsl:template name="outputTool">
		<xsl:param name="toolProfile"/>

		<xsl:choose>
			<xsl:when test="starts-with($toolProfile, 'H=')">
				<xsl:value-of select="$toolProfile"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>H=</xsl:text>
				<xsl:apply-templates select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolProfile]" mode="codeMode"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="mechSpeedTool">
		<xsl:param name="activity"/>
		<xsl:param name="mechNum" select="2"/>
		
		<xsl:variable name="motionProfile" select="$activity/MotionAttributes/MotionProfile"/>
		<xsl:variable name="toolProfile" select="$activity/MotionAttributes/ToolProfile"/>
		<xsl:variable name="motionBasis" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<xsl:variable name="speed" select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- speed -->
		<xsl:variable name="spdAttr" select="$activity/AttributeList/Attribute[AttributeName=concat('MechanismSpeed', $mechNum)]"/>
		<xsl:choose>
			<xsl:when test="$spdAttr">
				<xsl:value-of select="$spdAttr/AttributeValue"/>
				<xsl:text>, </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputSpeedTime">
					<xsl:with-param name="speed" select="$speed"/>
					<xsl:with-param name="motionBasis" select="$motionBasis"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<!-- tool -->
		<xsl:variable name="toolAttr" select="$activity/AttributeList/Attribute[AttributeName=concat('MechanismTool', $mechNum)]"/>
		<xsl:choose>
			<xsl:when test="$toolAttr">
				<xsl:value-of select="$toolAttr/AttributeValue"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputTool">
					<xsl:with-param name="toolProfile" select="$toolProfile"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="outputConfig">
		<xsl:param name="Config"/>
    <xsl:param name="TurnNumber"/>
    <xsl:param name="Joint1Value"/>

    <xsl:variable name="configName" select="$Config/@Name"/>
    <xsl:variable name="turn6" select="$TurnNumber/TNJoint[@Number=6]/@Value"/>

    <xsl:variable name="CONFj1expansion" select="dnbigpolp:GetParameterData(string('CONFJ1Expansion'))"/>

    <!-- CONF = I J K L -->
    <xsl:text>CONF=</xsl:text>
    <!-- I J -->
		<xsl:choose>
      <!-- NEW config mapping -->
      <xsl:when test="$configName = 'Config_1' or $configName = 'NUT'">10</xsl:when>
      <xsl:when test="$configName = 'Config_2' or $configName = 'FUT'">00</xsl:when>
      <xsl:when test="$configName = 'Config_3' or $configName = 'NDT'">11</xsl:when>
      <xsl:when test="$configName = 'Config_4' or $configName = 'FDT'">01</xsl:when>
      <xsl:when test="$configName = 'Config_5' or $configName = 'NDB'">11</xsl:when>
      <xsl:when test="$configName = 'Config_6' or $configName = 'FDB'">01</xsl:when>
      <xsl:when test="$configName = 'Config_7' or $configName = 'NUB'">10</xsl:when>
      <xsl:when test="$configName = 'Config_8' or $configName = 'FUB'">00</xsl:when>
      <xsl:when test="$configName = '000'">10</xsl:when>
      <xsl:when test="$configName = '010'">00</xsl:when>
      <xsl:when test="$configName = '001'">11</xsl:when>
      <xsl:when test="$configName = '011'">01</xsl:when>
      <xsl:when test="$configName = '101'">11</xsl:when>
      <xsl:when test="$configName = '111'">01</xsl:when>
      <xsl:when test="$configName = '100'">10</xsl:when>
      <xsl:when test="$configName = '110'">00</xsl:when>
      <!-- OLD config mapping
      <xsl:when test="$configName = 'Config_1' or $configName = 'NUT'">000</xsl:when>
			<xsl:when test="$configName = 'Config_2' or $configName = 'FUT'">010</xsl:when>
			<xsl:when test="$configName = 'Config_3' or $configName = 'NDT'">001</xsl:when>
			<xsl:when test="$configName = 'Config_4' or $configName = 'FDT'">011</xsl:when>
			<xsl:when test="$configName = 'Config_5' or $configName = 'NDB'">100</xsl:when>
			<xsl:when test="$configName = 'Config_6' or $configName = 'FDB'">110</xsl:when>
			<xsl:when test="$configName = 'Config_7' or $configName = 'NUB'">101</xsl:when>
			<xsl:when test="$configName = 'Config_8' or $configName = 'FUB'">111</xsl:when>
      -->
			<xsl:otherwise>
				<xsl:value-of select="$configName"/>
			</xsl:otherwise>
		</xsl:choose>
    <!-- K -->
    <xsl:choose>
      <xsl:when test="$CONFj1expansion = 'true'">
        <xsl:choose>
          <xsl:when test="$Joint1Value &lt;= 90 and $Joint1Value &gt;= -90">2</xsl:when>
          <xsl:otherwise>3</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <!-- Lefty(0)/Righty(1) -->
        <xsl:choose>
          <xsl:when test="$Joint1Value &gt;= 0">
            <xsl:text>0</xsl:text>
          </xsl:when>
          <xsl:otherwise>1</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
    <!-- L: TurnNumber 6: 0, 1, or 2 -->
    <xsl:choose>
      <xsl:when test="$turn6 = -1">2</xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$turn6"/>
      </xsl:otherwise>
    </xsl:choose>
		<xsl:text>, </xsl:text>
	</xsl:template>

	<!-- parameter DownloadTargetType overwrites TargetType -->
	<xsl:template name="getTargetType">
		<xsl:param name="targetTargetType"/>
		
		<xsl:variable name="downloadTargetType" select="dnbigpolp:GetParameterData('DownloadTargetType')"/>
		<xsl:choose>
			<xsl:when test="$downloadTargetType = 'Joint'">
				<xsl:text>Joint</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$targetTargetType"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
  <xsl:template name="fabs">
    <xsl:param name="innumber"/>

    <xsl:choose>
      <xsl:when test="$innumber &gt;= 0">
        <!-- NOT "&ge;" -->
        <xsl:value-of select="$innumber"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="-$innumber"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:param name="Units" select="1"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'e')"/>
						<xsl:with-param name="e" select="substring-after($Num,'e')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'E')"/>
						<xsl:with-param name="e" select="substring-after($Num,'E')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num*$Units"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
  
	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
