<?xml version="1.0" encoding="UTF-8"?>
<!-- COPYRIGHT DASSAULT SYSTEMES 2003 -->
<!-- 01 Feb 2008 creation -->
<!-- Declare the extensions within stylesheet element -->	
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lxslt="http://xml.apache.org/xslt" xmlns:java="http://xml.apache.org/xslt/java"
	xmlns:UserData="UserData" xmlns:NumberIncrement="NumberIncrement"
	xmlns:MatrixUtils="MatrixUtils" xmlns:ext="http://exslt.org/common"
	exclude-result-prefixes="ext">
	<!--Declare the prefix and the functions to be accessed by using that prefix.
	     Here we just call the functons to set and get parameter and attribute data -->
	<lxslt:component prefix="UserData"
		functions="SetParameterData GetParameterData SetAttributeData GetAttributeData" elements="">
		<!-- Declare that java extensions were written in form of java class, and give the full class location -->
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"
		/>
	</lxslt:component>
	<lxslt:component prefix="NumberIncrement" functions="next current startnum"
		elements="startnum increment reset counternum">
		<lxslt:script lang="javaclass"
			src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils"
		functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll"
		elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"
		/>
	</lxslt:component>
	<!-- Do not preserve whitespace-only text nodes for all the elements of the source XML document -->
	<xsl:strip-space elements="*"/>
	<!-- Indicates the output method for the result document. Text output method simply outputs all the result document's
		text nodes without modification -->
	<xsl:output method="text"/>

	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!-- HEADER INFORMATION -->
	<!-- You may modify the content of the select attribute, and type in the stylesheet version information relevant for your company -->
	<xsl:variable name="version"
		select="'DELMIA CORP. KAWASAKI AS  DOWNLOADER VERSION 5 RELEASE 26 SP 2'"/>
	<!-- You may modify the content of the select attribute, and type in the copyright information relevant for your company -->
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 1986-2014, ALL RIGHTS RESERVED'"/>
	<!-- In general this shouldn't be modified, but if the meaning will stay the same, the wording can change -->
	<xsl:variable name="saveInfo"
		select="'If you decide to save this file, VERSION INFO header will not be taken into account.'"/>
	<!-- PRESENTATION PATTERNS-->
	<!-- Carriage Return character -->
	<xsl:variable name="cr" select="'&#xa;'"/>
	<!-- Decimal number format. For more information refer to java.text.DecimalFormat class documentation, in Java API Specification -->
	<xsl:variable name="decimalNumberPattern" select="'0.000000'"/>
	<!--Time in seconds format -->
	<xsl:variable name="timePattern" select="'0.00'"/>
	<!-- GENERAL INFORMATION ABOUT THE ROBOT AND DOWNLOADING TECHNIQUE -->
	<!-- Name of the robot which owns the downloaded task -->
	<xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<!-- Robot's maximum allowed speed -->
	<xsl:variable name="maxSpeed" select="/OLPData/Resource/GeneralInfo/MaxSpeed"/>
	<!-- Number of robot's base axes, excluding any type of external axes that can be detached from the robot -->
	<xsl:variable name="numberOfRobotAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<!-- Number of robot's rail and/or track axes -->
	<xsl:variable name="numberOfAuxiliaryAxes"
		select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<!-- Number of robot's end of arm tooling axes -->
	<xsl:variable name="numberOfExternalAxes"
		select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<!-- Number of workpiece positioner axes, that are controlled by the robot controller -->
	<xsl:variable name="numberOfPositionerAxes"
		select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<!-- Variable which value defines whether tag targets are generated with respect to the robot's base or with resptect to 
		the bases of the parts to which the tags are attached to -->
	<xsl:variable name="downloadCoordinates"
		select="/OLPData/Resource/GeneralInfo/DownloadCoordinates"/>
	<!-- Main task name is the name of the task that has been selected for download -->
	<xsl:variable name="mainTaskName" select="/OLPData/Resource/ActivityList/@Task"/>
	<!-- global parameters for download -->
	<xsl:variable name="downloadblockformat">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='DownloadBlockFormat']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='DownloadBlockFormat']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="speedabstime">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='SpeedAbsTime']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='SpeedAbsTime']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="oxwxnegative">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OXWXNegative']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='OXWXNegative']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="ox_preoutput">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='OX_PreOutput']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='OX_PreOutput']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>true</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="numberofclamp">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='NumberOfClamp']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='NumberOfClamp']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>2</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="endofarmtooltype">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='EndOfArmToolType']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='EndOfArmToolType']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>AIRGUN</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="controller">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='Controller']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Controller']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>E</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="auxdataspeed">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='AUXDATA_SPEED']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='AUXDATA_SPEED']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0,100.0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="auxdataaccu">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='AUXDATA_ACCU']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='AUXDATA_ACCU']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>1.0,10.0,50.0,100.0,1.0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="auxdatatimer">
		<xsl:choose>
			<xsl:when
				test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='AUXDATA_TIMER']/ParameterValue) != ''">
				<xsl:value-of
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='AUXDATA_TIMER']/ParameterValue"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0.0,.1,.2,.3,.4,.5,.6,.7,.8,.9</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES END~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TEMPLATE DEFINITIONS START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!--~~~~~ROOT TEMPLATE~~~~~ -->
	<!-- This template is called first, automatically, by XSLT processor -->
	<xsl:template match="/">
		<!-- initialize counters -->
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<!-- Create header information -->
		<xsl:call-template name="HeaderInfo"/>
		<!-- Initialize parameter data -->
		<xsl:call-template name="InitializeParameters"/>
		<xsl:variable name="hr" select="UserData:SetParameterData('CURRENT_TASK_NAME', '')"/>
		<!-- Process all the ActivityList nodes in order they appear in the source XML file -->
		<xsl:apply-templates select="OLPData/Resource/ActivityList"/>
		<!-- Create Spot Weld subprograms, if any -->
		<xsl:variable name="spotWeldSubprograms"
			select="UserData:GetParameterData('SPOTWELD SUBPROGRAMS')"/>
		<xsl:value-of select="$spotWeldSubprograms"/>
		<!-- Call this template  to create target declaration section for downloaded program -->
		<xsl:call-template name="CreateTargetDeclarationSection"/>
		<xsl:text>DATA FILE END</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	<!--~~~~~HEADER CREATOR TEMPLATE~~~~~ -->
	<!-- This template will not be called automatically by the processor. It is explicitly called from the root template.
		It creates version and copyright information blocks in-between VERSION INFO START and VERSION INFO END
		statements -->
	<xsl:template name="HeaderInfo">
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO START</xsl:text>
		<!-- Print carriage-return character to the result document -->
		<xsl:value-of select="$cr"/>
		<!-- Prints the version information to the result document -->
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the copyright information to the result document -->
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the save information to the result document -->
		<xsl:value-of select="$saveInfo"/>
		<xsl:value-of select="$cr"/>
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO END</xsl:text>
	</xsl:template>
	<!-- ~~~~~PARAMETER DATA INITIALIZER TEMPLATE~~~~~ -->
	<!-- In order to make all the parameter data global, this template first stores all the parameter name / value pairs in java.
		After this template is processed all the parameter values will be accessible throughout this stylesheet. This template
		will not be called automatically by the processor. It is explicitly called from the root template. -->
	<xsl:template name="InitializeParameters">
		<!-- For each resource parameter defined in V5, get it's name and value, and store it in java extension -->
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<!-- Store parameter name -->
			<xsl:variable name="paramName" select="ParameterName"/>
			<!-- Store parameter value -->
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<!-- Store all the parameter name/value pairs in java -->
			<xsl:variable name="hr"
				select="UserData:SetParameterData( string($paramName), string($paramValue) )"/>
			<!-- IMPORTANT: In order to retrieve parameter value, from anywhere in the stylesheet use:
				String UserData:GetParameterData(String) function. For example, this will work:
				<xsl:variable name="result" select="UserData:GetParameterData('MyParameter1')"/>
				Pass the parameter name as a string, and the return value will be either 
				the parameter value (result, in the example above) returned as a string, or an empty string if the parameter
				 name does not exist in V5 for the downloading resource -->
		</xsl:for-each>
	</xsl:template>
	<!-- ~~~~~TARGET DECLARATION SECTION CREATOR TEMPLATE~~~~~ -->
	<!-- This template should only be called if target declaration section needs to be created in the output document. By default, in this 
		stylesheet it is always called from the root template. You can comment out that call-template element if you do not need to 
		create target declaration section in the output document -->
	<xsl:template name="CreateTargetDeclarationSection">
		<xsl:variable name="currentCounter" select="NumberIncrement:current()"/>
		<xsl:if
			test="count(/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity']/Target[@Default = 'Cartesian']) > 0">
			<xsl:value-of select="$cr"/>
			<xsl:text>.TRANS</xsl:text>
			<!-- For each robot motion in the source XML file -->
			<xsl:for-each
				select="/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity']/Target[@Default = 'Cartesian']">
				<!-- Call template TransformRobotMotionActivityData with mode parameter set to 'TargetDeclarationSection' -->
				<xsl:if test="../@ActivityType = 'DNBRobotMotionActivity' ">
					<xsl:call-template name="TransformRobotMotionActivityData">
						<!-- Send the currect node, as the first parameter -->
						<xsl:with-param name="motionActivityNode" select=".."/>
						<!-- Send the mode string, as the second parameter -->
						<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
						<xsl:with-param name="numberOfUnmarkedTargets" select="$currentCounter"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:for-each>
			<xsl:value-of select="$cr"/>
			<xsl:text>.END</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:if
			test="count(/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity']/Target[@Default = 'Joint']) > 0">
			<xsl:variable name="taskblockformat"
				select="../AttributeList/Attribute[AttributeName =  'DownloadBlockFormat']/AttributeValue"/>
			<xsl:variable name="activityblockformat">
				<xsl:choose>
					<!-- if this is not a robot motion use previous robot motion block format -->
					<xsl:when test="string($taskblockformat)!= ''">
						<xsl:value-of select="$taskblockformat"/>
					</xsl:when>
					<xsl:when test="string($downloadblockformat)!= ''">
						<xsl:value-of select="$downloadblockformat"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of
							select="string(AttributeList/Attribute[AttributeName='DownloadBlockFormat']/AttributeValue)"
						/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$activityblockformat != 'true'">
				<xsl:value-of select="$cr"/>
				<xsl:text>.JOINT</xsl:text>
				<xsl:for-each
					select="/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity']/Target[@Default = 'Joint']">
					<!-- Call template TransformRobotMotionActivityData with mode parameter set to 'TargetDeclarationSection' -->
					<xsl:if test="../@ActivityType = 'DNBRobotMotionActivity' ">
						<xsl:call-template name="TransformRobotMotionActivityData">
							<!-- Send the currect node, as the first parameter -->
							<xsl:with-param name="motionActivityNode" select=".."/>
							<!-- Send the mode string, as the second parameter -->
							<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
							<xsl:with-param name="numberOfUnmarkedTargets" select="$currentCounter"
							/>
						</xsl:call-template>

					</xsl:if>
				</xsl:for-each>
				<xsl:value-of select="$cr"/>
				<xsl:text>.END</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:if>
		<!-- Create ToolProfile and ObjectFrameProfile declarations -->
		<xsl:if test="string($downloadblockformat)!= 'true'">
			<xsl:value-of select="$cr"/>
			<xsl:text>.AUXDATA</xsl:text>
			<xsl:for-each select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile">
				<!-- Tool profile name -->
				<xsl:value-of select="$cr"/>
				<xsl:variable name="toolProfileName" select="Name"/>
				<xsl:value-of select="$toolProfileName"/>
				<!-- TCP location offset from robot's mount plate in X direction -->
				<xsl:variable name="tcpPositionX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@X"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP location offset from robot's mount plate in Y direction -->
				<xsl:variable name="tcpPositionY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Y"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP location offset from robot's mount plate in Z direction -->
				<xsl:variable name="tcpPositionZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Z"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
				<xsl:variable name="tcpOrientationYaw">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Yaw"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
				<xsl:variable name="tcpOrientationPitch">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Pitch"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP orientation offset from robot's mount plate in Roll direction -->
				<xsl:variable name="tcpOrientationRoll">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Roll"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- convert to OAT -->
				<xsl:variable name="xyzwpr"
					select="concat($tcpPositionX, ',', $tcpPositionY, ',', $tcpPositionZ, ',',
													   $tcpOrientationYaw, ',', $tcpOrientationPitch, ',', $tcpOrientationRoll)"/>
				<xsl:variable name="matxyzwpr" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
				<xsl:variable name="oat" select="MatrixUtils:xformToOAT()"/>
				<xsl:variable name="oat2" select="substring-after($oat, ',')"/>
				<xsl:variable name="o" select="substring-before($oat, ',')"/>
				<xsl:variable name="a" select="substring-before($oat2, ',')"/>
				<xsl:variable name="t" select="substring-after($oat2, ',')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="format-number($tcpPositionX, $decimalNumberPattern)"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="format-number($tcpPositionY, $decimalNumberPattern)"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="format-number($tcpPositionZ, $decimalNumberPattern)"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="format-number($o, $decimalNumberPattern)"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="format-number($a, $decimalNumberPattern)"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="format-number($t, $decimalNumberPattern)"/>
			</xsl:for-each>
			<xsl:for-each
				select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile">
				<!-- Object profile name00-->
				<xsl:variable name="objectProfileName" select="Name"/>
				<xsl:variable name="replaced" select="@Replaced"/>
				<xsl:if test="string($replaced)='' and string($objectProfileName)!='Default' and string($objectProfileName)!='WORK0'">
					<xsl:value-of select="$cr"/>					
					<xsl:value-of select="$objectProfileName"/>
					<!-- ObjectFrame location offset from robot's mount plate in X direction -->
					<xsl:variable name="ObjectFramePositionX">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@X"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($ObjectFramePositionX, $decimalNumberPattern)"/>
					<!-- ObjectFrame location offset from robot's mount plate in Y direction -->
					<xsl:variable name="ObjectFramePositionY">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@Y"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($ObjectFramePositionY, $decimalNumberPattern)"/>
					<!-- ObjectFrame location offset from robot's mount plate in Z direction -->
					<xsl:variable name="ObjectFramePositionZ">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@Z"/>
							<xsl:with-param name="Units" select="1000.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($ObjectFramePositionZ, $decimalNumberPattern)"/>
					<!-- ObjectFrame orientation offset from robot's mount plate in Yaw direction -->
					<xsl:variable name="ObjectFrameOrientationYaw">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFrameOrientation/@Yaw"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:text> </xsl:text>
					<xsl:value-of
						select="format-number($ObjectFrameOrientationYaw, $decimalNumberPattern)"/>
					<!-- ObjectFrame orientation offset from robot's mount plate in Pitch  direction -->
					<xsl:variable name="ObjectFrameOrientationPitch">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num"
								select="ObjectFrame/ObjectFrameOrientation/@Pitch"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:text> </xsl:text>
					<xsl:value-of
						select="format-number($ObjectFrameOrientationPitch, $decimalNumberPattern)"/>
					<!-- ObjectFrame orientation offset from robot's mount plate in Roll direction -->
					<xsl:variable name="ObjectFrameOrientationRoll">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="ObjectFrame/ObjectFrameOrientation/@Roll"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:text> </xsl:text>
					<xsl:value-of
						select="format-number($ObjectFrameOrientationRoll, $decimalNumberPattern)"/>					
				</xsl:if>
			</xsl:for-each>
			<xsl:value-of select="$cr"/>			
			<xsl:text>.END</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	<!-- end CreateTargetDeclarationSection -->
	<!--~~~~~ATTRIBUTE DATA CREATION TEMPLATE~~~~~ -->
	<xsl:template name="SetAttributes">
		<!-- Valid AttributeList XML element that contains Attribute sub-elements -->
		<xsl:param name="attributeListNodeSet"/>
		<xsl:param name="commentType" select="'PreComment'"/>
		<xsl:param name="outputOnlyAccelDecel" select="false()"/>
		<!-- For each Attribute element in supplied AttributeList element -->
		<xsl:for-each
			select="$attributeListNodeSet/Attribute | $attributeListNodeSet/OperationAttribute ">
			<!-- Check if AttributeName sub-element exist -->
			<xsl:if test="AttributeName">
				<!-- Store the value of the AttributeName element -->
				<xsl:variable name="attribName" select="AttributeName"/>
				<xsl:choose>
					<xsl:when test="starts-with($attribName, $commentType)">
						<!-- Check if AttributeValue sub-element exist -->
						<xsl:if test="AttributeValue">
							<!-- Store the value of the AttributeValue element -->
							<xsl:variable name="attribValue" select="AttributeValue"/>
							<xsl:call-template name="finishBlockRobotMotion"/>
							<!-- Write Comment to program-->
							<xsl:value-of select="$cr"/>
							<xsl:text>;</xsl:text>
							<xsl:value-of select="$attribValue"/>
						</xsl:if>
					</xsl:when>
					<xsl:when test="starts-with($attribName,'Robot Language')">
						<!-- Check if AttributeValue sub-element exist -->
						<xsl:if test="AttributeValue">
							<!-- Store the value of the AttributeValue element -->
							<xsl:variable name="attribValue" select="AttributeValue"/>
							<xsl:choose>
								<xsl:when test="string($outputOnlyAccelDecel) = 'true'">
									<xsl:if
										test="starts-with($attribValue, 'ACCEL ') = true or starts-with($attribValue, 'DECEL ') = true">
										<xsl:call-template name="finishBlockRobotMotion"/>
										<xsl:value-of select="$cr"/>
										<xsl:value-of select="$attribValue"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:if
										test="starts-with($attribValue, 'ACCEL ') = false and starts-with($attribValue, 'DECEL ') = false">
										<xsl:call-template name="finishBlockRobotMotion"/>
										<xsl:value-of select="$cr"/>
										<xsl:value-of select="$attribValue"/>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</xsl:when>
					<xsl:when test="starts-with($attribName,'Comment')">
						<!-- Check if AttributeValue sub-element exist -->
						<xsl:if test="AttributeValue">
							<!-- Store the value of the AttributeValue element -->
							<xsl:variable name="attribValue" select="AttributeValue"/>
							<xsl:call-template name="finishBlockRobotMotion"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>; </xsl:text>
							<xsl:value-of select="$attribValue"/>
						</xsl:if>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--~~~~~MULTI FILE OUTPUT TEMPLATE~~~~~-->
	<!-- This template creates a new DATA FILE START / END block for each ActivityList element in the source XML file.
		The contents of each of these blocks will be saved in separate files, when users decide to save the downloaded
		 content to disk. Equally effective (if not more effective) alternative is to save files from an XSLT stylesheet directly. For
		 that purpose org.apache.xalan.lib.Redirect java extension should be used. To get more information on how to activate
		 this extension, check the following url: http://xml.apache.org/xalan-j/apidocs/org/apache/xalan/lib/Redirect.html.
		 This template is called automatically by XSLT processor. -->
	<xsl:template match="ActivityList">
		<!-- Store the robot task name which content is represented by this ActivityList XML element -->
		<xsl:variable name="taskName" select="translate(@Task, '.', '')"/>
		<xsl:variable name="currentTaskName" select="UserData:GetParameterData('CURRENT_TASK_NAME')"/>
		<xsl:variable name="hr"
			select="UserData:SetParameterData('CURRENT_TASK_NAME', string($taskName))"/>
		<xsl:variable name="hr" select="UserData:SetParameterData('LAST_FLYBYMODE', 'FlyByMode ON')"/>
		<xsl:variable name="hr" select="UserData:SetParameterData('OXValue','OX=')"/>
		<xsl:variable name="hr" select="UserData:SetParameterData('PreOXValue','OX=')"/>
		<xsl:variable name="hr" select="UserData:SetParameterData('WXValue','WX=')"/>
		<xsl:variable name="hr" select="UserData:SetParameterData('TIMERValue', 'TIMER0')"/>

		<xsl:if test="string($currentTaskName)=''">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START </xsl:text>
			<xsl:value-of select="$taskName"/>
			<xsl:text>.as</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<!-- Write program name to a file -->
		<xsl:text>.PROGRAM </xsl:text>
		<xsl:value-of select="$taskName"/>
		<xsl:text>()</xsl:text>
		<!-- RESET THE OX output parameter -->
		<!-- Call the templates that will process all the Action and Activity nodes of this activity list -->
		<xsl:apply-templates select="Activity | Action"/>
		<!-- output comments appearing after the last motion statement -->
		<xsl:call-template name="SetAttributes">
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			<xsl:with-param name="commentType" select="'PostComment'"/>
		</xsl:call-template>
		<xsl:value-of select="$cr"/>
		<xsl:text>.END</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	<!--~~~~~ACTION PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Action elements which are composed of one or more Activity elements. Only the 
		atomic activity types listed below are supported to be part of actions. This template is called automatically by
		XSLT processor. -->
	<xsl:template match="Action">
		<!-- Store this action's type -->
		<xsl:variable name="actionType" select="@ActionType"/>
		<xsl:variable name="actionName" select="ActionName"/>
		<xsl:choose>
			<xsl:when test="$actionType = 'KawasakiASSpotWeld'">
				<!-- Get Spot Weld action attribute values -->
				<xsl:variable name="weldScheduleNumberAttribute"
					select="AttributeList/Attribute[AttributeName = 'Weld Schedule Number']/AttributeValue"/>
				<xsl:variable name="weldScheduleStartSignalNumberAttribute"
					select="AttributeList/Attribute[AttributeName = 'Weld Schedule Start Signal Number']/AttributeValue"/>
				<xsl:variable name="numberOfWeldScheduleSignalsAttribute"
					select="AttributeList/Attribute[AttributeName = 'Number of Weld Schedule Signals']/AttributeValue"/>
				<xsl:variable name="pulseSignalNumberAttribute"
					select="AttributeList/Attribute[AttributeName = 'Pulse Signal Number']/AttributeValue"/>
				<xsl:variable name="waitSignalNumberAttribute"
					select="AttributeList/Attribute[AttributeName = 'Wait Signal Number']/AttributeValue"/>
				<!-- Create the output -->
				<xsl:value-of select="$cr"/>
				<xsl:text>CALL Weld</xsl:text>
				<xsl:value-of select="$weldScheduleNumberAttribute"/>
				<!-- xsl:text>()</xsl:text -->
				<!-- Store the Spot Weld subprogram in memory -->
				<xsl:if
					test="not(contains(UserData:GetParameterData('LIST_OF_WELD_SCHEDULES'), concat(' ', string($weldScheduleNumberAttribute), ' ')))">
					<xsl:variable name="firstLine"
						select="concat($cr, '.PROGRAM Weld', string($weldScheduleNumberAttribute), '()', $cr)"/>
					<xsl:variable name="secondLine"
						select="concat('BITS ', string($weldScheduleStartSignalNumberAttribute), ',', string($numberOfWeldScheduleSignalsAttribute), ' = ', 
					                                                                                 string($weldScheduleNumberAttribute), $cr)"/>
					<xsl:variable name="thirdLine"
						select="concat('PULSE ', string($pulseSignalNumberAttribute), ', 1', $cr)"/>
					<xsl:variable name="fourthLine"
						select="concat('SWAIT ', string($waitSignalNumberAttribute), $cr)"/>
					<xsl:variable name="fifthLine" select="concat('.END', $cr)"/>
					<xsl:variable name="spotWeldSubroutine"
						select="concat($firstLine, $secondLine, $thirdLine, $fourthLine, $fifthLine)"/>
					<xsl:variable name="existingSpotWeldSubprograms"
						select="UserData:GetParameterData('SPOTWELD SUBPROGRAMS')"/>
					<xsl:variable name="updatedSpotWeldSubprograms"
						select="UserData:SetParameterData('SPOTWELD SUBPROGRAMS', concat($existingSpotWeldSubprograms, $spotWeldSubroutine))"
					/>
				</xsl:if>
				<xsl:variable name="hr"
					select="UserData:SetParameterData('LIST_OF_WELD_SCHEDULES', concat(UserData:GetParameterData('LIST_OF_WELD_SCHEDULES'), ' ', 
				                                                                                                     string($weldScheduleNumberAttribute), ' '))"
				/>
			</xsl:when>
			<xsl:when test="$actionType = 'KawasakiASSpotPick'">
				<xsl:value-of select="$cr"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="ActionName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of
					select="ActivityList/Activity[@ActivityType='GrabActivity']/GrabbedObjectList/GrabbedObject"
				/>
			</xsl:when>
			<xsl:when test="$actionType = 'KawasakiASSpotDrop'">
				<xsl:value-of select="$cr"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="ActionName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of
					select="ActivityList/Activity[@ActivityType='ReleaseActivity']/ReleasedObjectList/ReleasedObject"
				/>
			</xsl:when>
			<xsl:when test="$actionType = 'KawasakiASToolPick'">
				<xsl:value-of select="$cr"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="ActionName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="ToolResource/ResourceName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of
					select="ActivityList/Activity[@ActivityType='DNBIgpMountActivity']/SetToolProfile"
				/>
			</xsl:when>
			<xsl:when test="$actionType = 'KawasakiASToolDrop'">
				<xsl:value-of select="$cr"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="ActionName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="ToolResource/ResourceName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of
					select="ActivityList/Activity[@ActivityType='DNBIgpUnMountActivity']/UnsetToolProfile"
				/>
			</xsl:when>
		</xsl:choose>
		<xsl:variable name="macro"
			select="string(AttributeList/Attribute[AttributeName='Macro']/AttributeValue)"/>
		<xsl:if
			test="$macro != '' and contains($macro,'SPOT')=false and contains($macro,'Spot')=false and contains($macro,'spot')=false">
			<xsl:value-of select="$cr"/>
			<xsl:value-of select="$macro"/>
		</xsl:if>
	</xsl:template>
	<!--~~~~~ACTIVITY PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Activity elements in their order of appearance within the XML source file. Only the top level 
		Activity elements (the ones whose anscestors are OLPData, Resource, and AttributeList elements) will be transformed 
		by this template. Activity elements which are descendants of Action element, will be processed by Actions template above.
		This template is called automatically by XSLT processor. -->
	<xsl:template match="Activity">
		<!-- Global variables for all activities -->
		<!-- Get activity id number. It is used to set and get activity attributes. -->
		<xsl:variable name="activityID" select="@id"/>
		<!-- Get the activity type. Only the activity types specified below are supported. -->
		<xsl:variable name="activityType" select="@ActivityType"/>
		<!-- Get the activityName -->
		<xsl:variable name="activityName" select="ActivityName"/>
		<!-- Get the activity begin time -->
		<xsl:variable name="startTime" select="ActivityTime/StartTime"/>
		<!-- Get the activity finish time -->
		<xsl:variable name="endTime" select="ActivityTime/EndTime"/>
		<xsl:variable name="previousAct" select="preceding-sibling::node()[1]"/>
		<xsl:variable name="taskblockformat"
			select="../AttributeList/Attribute[AttributeName =  'DownloadBlockFormat']/AttributeValue"/>
		<xsl:variable name="activityblockformat">
			<xsl:choose>
				<!-- if this is not a robot motion use previous robot motion block format -->
				<xsl:when
					test="string(preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][1]/AttributeList/Attribute[AttributeName='DownloadBlockFormat']/AttributeValue)='true' and $activityType != 'DNBRobotMotionActivity'">
					<xsl:value-of
						select="preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][1]/AttributeList/Attribute[AttributeName='DownloadBlockFormat']/AttributeValue"
					/>
				</xsl:when>
				<xsl:when
					test="string(following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][1]/AttributeList/Attribute[AttributeName='DownloadBlockFormat']/AttributeValue)='true'  and $activityType != 'DNBRobotMotionActivity'">
					<xsl:value-of
						select="following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][1]/AttributeList/Attribute[AttributeName='DownloadBlockFormat']/AttributeValue"
					/>
				</xsl:when>
				<xsl:when test="string($taskblockformat)!= ''">
					<xsl:value-of select="$taskblockformat"/>
				</xsl:when>
				<xsl:when test="string($downloadblockformat)!= ''">
					<xsl:value-of select="$downloadblockformat"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of
						select="string(AttributeList/Attribute[AttributeName='DownloadBlockFormat']/AttributeValue)"
					/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!-- Set the attributes associated with this activity. Refer to the comment marked as IMPORTANT in
			SetAttributes template -->
		<xsl:if test="AttributeList">
			<xsl:call-template name="SetAttributes">
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if
			test="$activityblockformat='true' and $activityType!='DelayActivity' and $activityType!='DNBSetSignalActivity' and $activityType!='DNBWaitSignalActivity'">
			<xsl:call-template name="finishBlockRobotMotion"/>
		</xsl:if>
		<!-- Based on activity type (the value of ActivityType XML element), process all the supported activities -->
		<xsl:choose>
			<!-- ROBOT MOTION ACTIVITY -->
			<xsl:when test="$activityType = 'DNBRobotMotionActivity'">
				<xsl:variable name="currMode">
					<xsl:choose>
						<xsl:when test="$activityblockformat='true'">
							<xsl:text>BlockFormat</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>MoveStatement</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="wxAttrib"
					select="AttributeList/Attribute[AttributeName='WXValue']/AttributeValue"/>
				<xsl:if test="string($wxAttrib) != ''">
					<xsl:variable name="hr" select="UserData:SetParameterData('WXValue',$wxAttrib)"
					/>
				</xsl:if>
				<xsl:if test="$ox_preoutput = 'true'">
					<xsl:variable name="oxValue" select="UserData:GetParameterData('OXValue')"/>
					<xsl:variable name="hr"
						select="UserData:SetParameterData('PreOXValue', $oxValue)"/>
					<xsl:if test="$oxwxnegative='true'">
						<xsl:variable name="hr" select="UserData:SetParameterData('OXValue', 'OX=')"
						/>
					</xsl:if>
				</xsl:if>
				<!-- Call the template which will output a move statement -->
				<xsl:value-of select="$cr"/>
				<xsl:call-template name="TransformRobotMotionActivityData">
					<!-- Send this robot motion activity Nodeset as parameter -->
					<xsl:with-param name="motionActivityNode" select="."/>
					<!-- This parameter determines that called templete needs to create a move statement
						in designated robot language -->
					<xsl:with-param name="mode" select="$currMode"/>
					<xsl:with-param name="numberOfUnmarkedTargets" select="0"/>
				</xsl:call-template>
				<xsl:if test="$activityblockformat = 'true'">
					<xsl:variable name="hr"
						select="UserData:SetParameterData('PARTIALROBOTMOTION','true')"/>
				</xsl:if>
			</xsl:when>
			<!-- DELAY ACTIVITY -->
			<xsl:when test="$activityType = 'DelayActivity' ">
				<xsl:choose>
					<xsl:when test="$activityblockformat='true'">
						<xsl:variable name="timerValue">
							<xsl:choose>
								<!-- BLOCK SPEED -->
								<xsl:when test="substring($activityName, 1, 5) = 'TIMER'">
									<xsl:value-of select="$activityName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>TIMER</xsl:text>
									<xsl:if test="$auxdatatimer != ''">
										<xsl:variable name="timerSplit">
											<xsl:call-template name="stringsplit">
												<xsl:with-param name="pText" select="$auxdatatimer"
												/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="hr"
											select="UserData:SetParameterData('TIMERDONE','')"/>
										<xsl:for-each select="ext:node-set($timerSplit)/*">
											<xsl:variable name="timerDone"
												select="UserData:GetParameterData('TIMERDONE')"/>
											<xsl:choose>
												<xsl:when test="$timerDone='true'"> </xsl:when>
												<xsl:when
												test="(number($endTime)-number($startTime) = number(.))">
												<xsl:value-of select="position()-1"/>
												<xsl:variable name="hr"
												select="UserData:SetParameterData('TIMERDONE','true')"
												/>
												</xsl:when>
												<xsl:when
												test="(number($endTime)-number($startTime) &gt; .) and (number($endTime)-number($startTime) &lt; following-sibling::item[1])">
												<xsl:value-of select="position()-1"/>
												<xsl:variable name="hr"
												select="UserData:SetParameterData('TIMERDONE','true')"
												/>
												</xsl:when>
												<xsl:when
												test="number($endTime)-number($startTime) &gt; . and count(following-sibling::item) = 0">
												<xsl:value-of select="position()-1"/>
												<xsl:variable name="hr"
												select="UserData:SetParameterData('TIMERDONE','true')"
												/>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="hr"
							select="UserData:SetParameterData('TIMERValue', $timerValue)"/>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output formatted delay statement in chosen robot language -->
						<xsl:value-of select="$cr"/>
						<xsl:text>TWAIT </xsl:text>
						<xsl:value-of select="format-number($endTime - $startTime, $timePattern)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- SET IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBSetSignalActivity' ">
				<!-- Get signal name -->
				<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="setSignalValue"
					select="SetIOAttributes/IOSetSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="setPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>

				<xsl:choose>
					<xsl:when test="$activityblockformat='true'">
						<xsl:variable name="oxPortNumber">
							<xsl:if test="$oxwxnegative = 'true' and $setSignalValue = 'Off'">
								<xsl:text>-</xsl:text>
							</xsl:if>
							<xsl:value-of select="$setPortNumber"/>
						</xsl:variable>
						<xsl:variable name="oxValue" select="UserData:GetParameterData('OXValue')"/>
						<xsl:choose>
							<xsl:when test="substring($oxValue,1,3) != 'OX='">
								<xsl:variable name="hr"
									select="UserData:SetParameterData('OXValue','OX=')"/>
							</xsl:when>
							<xsl:when test="$oxValue = 'OX='"/>
							<xsl:when
								test="$oxValue=concat('OX=',$setPortNumber) or starts-with($oxValue,concat('OX=',concat($setPortNumber,','))) or (string-length(substring-before($oxValue, concat(',',$setPortNumber))) = string-length($oxValue)-string-length(concat(',',$setPortNumber))) or contains($oxValue,concat(concat(',',$setPortNumber),','))">
								<xsl:if test="$setSignalValue = 'Off' and $oxwxnegative!='true'">
									<xsl:choose>
										<xsl:when test="$oxValue=concat('OX=',$setPortNumber)">
											<xsl:variable name="hr"
												select="UserData:SetParameterData('OXValue','OX=')"
											/>
										</xsl:when>
										<xsl:when
											test="starts-with($oxValue,concat('OX=',concat($setPortNumber,',')))">
											<xsl:variable name="hr"
												select="UserData:SetParameterData('OXValue',concat('OX=',substring($oxValue,number(string-length(concat('OX=',concat($setPortNumber,','))))+1)))"
											/>
										</xsl:when>
										<xsl:when
											test="(string-length(substring-before($oxValue, concat(',',$setPortNumber))) = string-length($oxValue)-string-length(concat(',',$setPortNumber)))">
											<xsl:variable name="hr"
												select="UserData:SetParameterData('OXValue',substring($oxValue,1, string-length($oxValue)-string-length(concat(',',$setPortNumber))))"
											/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:variable name="hr"
												select="UserData:SetParameterData('OXValue',concat(concat(substring-before($oxValue,concat(concat(',',$setPortNumber),',')), ','), substring-after($oxValue,concat(concat(',',$setPortNumber),','))))"
											/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
						<xsl:if test="string($oxwxnegative) = 'true' or $setSignalValue != 'Off'">
							<xsl:variable name="oxValue2"
								select="UserData:GetParameterData('OXValue')"/>
							<xsl:if
								test="$oxValue2='OX=' or (starts-with($oxValue2,concat('OX=',concat($oxPortNumber,','))) = false and (string-length(substring-before($oxValue2, concat(',',$oxPortNumber))) != string-length($oxValue2)-string-length(concat(',',$oxPortNumber))) and  contains($oxValue2,concat(concat(',',$oxPortNumber),',')) = false)">
								<xsl:choose>
									<xsl:when test="string-length($oxValue2) &gt; 3">
										<xsl:variable name="oxValue3"
											select="substring($oxValue2, 4)"/>
										<xsl:variable name="reorderOX">
											<xsl:variable name="oxSplit">
												<xsl:call-template name="stringsplit">
												<xsl:with-param name="pText" select="$oxValue3"
												/>
												</xsl:call-template>
											</xsl:variable>
											<xsl:variable name="hr"
												select="UserData:SetParameterData('PORTDONE','')"/>
											<xsl:for-each select="ext:node-set($oxSplit)/*">
												<xsl:variable name="absCurrNumber">
												<xsl:choose>
												<xsl:when test="starts-with(., '-')">
												<xsl:value-of select="substring(.,2)"/>
												</xsl:when>
												<xsl:otherwise>
												<xsl:value-of select="."/>
												</xsl:otherwise>
												</xsl:choose>
												</xsl:variable>
												<xsl:variable name="portdone"
												select="UserData:GetParameterData('PORTDONE')"/>
												<xsl:if test="position()=1">
												<xsl:text>OX=</xsl:text>
												</xsl:if>
												<xsl:if
												test="number($setPortNumber) &lt; number($absCurrNumber) and $portdone != 'true'">
												<xsl:variable name="hr"
												select="UserData:SetParameterData('PORTDONE','true')"/>
												<xsl:value-of select="$oxPortNumber"/>
												<xsl:text>,</xsl:text>
												</xsl:if>
												<xsl:value-of select="number(.)"/>
												<xsl:if
												test="last() != position() or number($setPortNumber) &gt; number($absCurrNumber)">
												<xsl:text>,</xsl:text>
												</xsl:if>
												<xsl:if
												test="number($setPortNumber) &gt; number($absCurrNumber) and last() = position()">
												<xsl:value-of select="$oxPortNumber"/>
												</xsl:if>
											</xsl:for-each>
										</xsl:variable>
										<xsl:variable name="hr"
											select="UserData:SetParameterData('OXValue',$reorderOX)"
										/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable name="hr"
											select="UserData:SetParameterData('OXValue',concat($oxValue2,$oxPortNumber))"
										/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<!-- Get signal name -->
						<xsl:variable name="setSignalName"
							select="SetIOAttributes/IOSetSignal/@SignalName"/>
						<!-- Get signal value: On or Off -->
						<xsl:variable name="setSignalValue"
							select="SetIOAttributes/IOSetSignal/@SignalValue"/>
						<!-- Get port number -->
						<xsl:variable name="setPortNumber"
							select="SetIOAttributes/IOSetSignal/@PortNumber"/>
						<!-- Get the signal pulse time -->
						<xsl:variable name="setSignalDuration"
							select="string(SetIOAttributes/IOSetSignal/@SignalDuration)"/>
						<xsl:variable name="prvSignalDuration"
							select="$previousAct/SetIOAttributes/IOSetSignal/@SignalDuration"/>
						<!-- Output formatted set IO signal statement in chosen robot language -->
						<xsl:choose>
							<xsl:when test="number($setSignalDuration) &gt; 0">
								<xsl:value-of select="$cr"/>
								<xsl:text>PULSE </xsl:text>
								<xsl:value-of select="$setPortNumber"/>
								<xsl:text> </xsl:text>
								<xsl:value-of
									select="format-number($setSignalDuration, $timePattern)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when
										test="string($previousAct/@ActivityType) = 'DNBSetSignalActivity' and number($prvSignalDuration) = 0">
										<xsl:text>, </xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$cr"/>
										<xsl:text>SIGNAL </xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if test="$setSignalValue = 'Off'">
									<xsl:text>-</xsl:text>
								</xsl:if>
								<xsl:value-of select="$setPortNumber"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- WAIT FOR IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBWaitSignalActivity' ">
				<!-- Get signal name -->
				<xsl:variable name="waitSignalName"
					select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="waitSignalValue"
					select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="waitPortNumber"
					select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
				<!-- Get maximum amount of time a robot will wait to receive a signal -->
				<xsl:variable name="waitMaxTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>
				<xsl:choose>
					<xsl:when test="$activityblockformat='true'">
						<xsl:variable name="wxPortNumber">
							<xsl:if test="$oxwxnegative = 'true' and $waitSignalValue = 'Off'">
								<xsl:text>-</xsl:text>
							</xsl:if>
							<xsl:if test="$oxwxnegative = 'true' or $waitSignalValue = 'On'">
								<xsl:value-of select="$waitPortNumber"/>
							</xsl:if>
						</xsl:variable>
						<xsl:variable name="wxValue" select="UserData:GetParameterData('WXValue')"/>
						<xsl:choose>
							<xsl:when test="substring($wxValue,1,3) != 'WX='">
								<xsl:variable name="hr"
									select="UserData:SetParameterData('WXValue','WX=')"/>
							</xsl:when>
							<xsl:when test="$wxValue = 'WX='"/>
							<xsl:when
								test="string($oxwxnegative) = 'true' or $waitSignalValue != 'Off'">
								<xsl:variable name="hr"
									select="UserData:SetParameterData('WXValue',concat($wxValue,','))"
								/>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
						<xsl:if test="string($oxwxnegative) = 'true' or $waitSignalValue != 'Off'">
							<xsl:variable name="wxValue2"
								select="UserData:GetParameterData('WXValue')"/>
							<xsl:variable name="hr"
								select="UserData:SetParameterData('WXValue',concat($wxValue2,$wxPortNumber))"
							/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output formatted wait IO signal statement in chosen robot language -->
						<xsl:value-of select="$cr"/>
						<xsl:text>SWAIT </xsl:text>
						<xsl:if test="$waitSignalValue= 'Off'">
							<xsl:text>-</xsl:text>
						</xsl:if>
						<xsl:value-of select="$waitPortNumber"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- CALL ROBOT TASK ACTIVITY -->
			<xsl:when test="$activityType = 'DNBIgpCallRobotTask' ">
				<!-- Sub-program or sub-routine call. That depemds on the value of DownloadStyle element (below) -->
				<xsl:variable name="robotTaskToCall" select="CallName"/>
				<!-- This defines whether a subprogram - "File" or a subroutine - "Subroutine" will be called -->
				<xsl:variable name="downloadCalledTaskStyle" select="DownloadStyle"/>
				<!-- Output formatted call sub-program or call separate program statement in chosen robot language -->
				<xsl:value-of select="$cr"/>
				<xsl:text>CALL </xsl:text>
				<xsl:value-of select="$robotTaskToCall"/>
			</xsl:when>
		</xsl:choose>
		<xsl:if test="$activityblockformat = 'true'">
			<xsl:if test="position()=last()">
				<xsl:if
					test="$activityType='DNBRobotMotionActivity' or $activityType='DelayActivity' or $activityType='DNBSetSignalActivity' or $activityType='DNBWaitSignalActivity'">
					<xsl:variable name="timerValue" select="UserData:GetParameterData('TIMERValue')"/>
					<xsl:variable name="postTimer" select="UserData:GetParameterData('PostTimer')"/>
					<xsl:variable name="postIO" select="UserData:GetParameterData('PostIO')"/>
					<xsl:variable name="oxValue" select="UserData:GetParameterData('OXValue')"/>
					<xsl:variable name="preoxValue" select="UserData:GetParameterData('PreOXValue')"/>
					<xsl:variable name="wxValue" select="UserData:GetParameterData('WXValue')"/>
					<xsl:if test="$postTimer != ''">
						<xsl:value-of select="$timerValue"/>
						<xsl:variable name="hr"
							select="UserData:SetParameterData('TIMERValue', 'TIMER0')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="$postTimer"/>
						<xsl:variable name="hr" select="UserData:SetParameterData('PostTimer', '')"
						/>
					</xsl:if>
					<xsl:if test="$postIO != ''">
						<xsl:choose>
							<xsl:when test="$ox_preoutput='true'">
								<xsl:value-of select="$preoxValue"/>
								<xsl:if test="$oxwxnegative='true'">
									<xsl:variable name="hr"
										select="UserData:SetParameterData('OXValue', 'OX=')"/>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$oxValue"/>
								<xsl:if test="$oxwxnegative='true'">
									<xsl:variable name="hr"
										select="UserData:SetParameterData('OXValue', 'OX=')"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> </xsl:text>
						<xsl:value-of select="$wxValue"/>
						<xsl:variable name="hr" select="UserData:SetParameterData('WXValue', 'WX=')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="$postIO"/>
						<xsl:variable name="hr" select="UserData:SetParameterData('PostTimer', '')"
						/>
					</xsl:if>
					<xsl:variable name="hr"
						select="UserData:SetParameterData('PARTIALROBOTMOTION','')"/>
				</xsl:if>
			</xsl:if>
		</xsl:if>
		<xsl:variable name="macro"
			select="string(AttributeList/Attribute[AttributeName='Macro']/AttributeValue)"/>
		<xsl:if test="$macro != ''">
			<xsl:value-of select="$cr"/>
			<xsl:if
				test="contains($macro,'SPOT')=true or contains($macro,'Spot')=true or contains($macro,'spot')=true">
				<xsl:text>CALL </xsl:text>
			</xsl:if>
			<xsl:value-of select="$macro"/>
		</xsl:if>
	</xsl:template>
	<!--~~~~~ROBOT MOTION ACTIVITY MULTI-PURPOSE TEMPLATE~~~~~-->
	<xsl:template name="TransformRobotMotionActivityData">
		<!-- Nodeset that contains all the descendants of robot motion activity element -->
		<xsl:param name="motionActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveStatement' "> element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<xsl:param name="numberOfUnmarkedTargets"/>
		<!-- next motion activity accuracy profile's flyby mode -->
		<xsl:variable name="nextMotionActivity"
			select="following-sibling::node()[@ActivityType='DNBRobotMotionActivity'][1]"/>
		<xsl:variable name="nextAccuracyProfile"
			select="$nextMotionActivity/MotionAttributes/AccuracyProfile"/>
		<xsl:variable name="nextFlyByMode"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name=$nextAccuracyProfile]/FlyByMode"/>
		<xsl:variable name="nextMotionProfile"
			select="$nextMotionActivity/MotionAttributes/MotionProfile"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$motionActivityNode/@id"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityName" select="$motionActivityNode/ActivityName"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile"
			select="$motionActivityNode/MotionAttributes/MotionProfile"/>
		<xsl:if
			test="boolean(/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile])=false">
			<xsl:text>ERROR INFO START</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>Motion Profile &lt;</xsl:text>
			<xsl:value-of select="$motionProfile"/>
			<xsl:text>&gt; does NOT exist.</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>ERROR INFO END</xsl:text>
		</xsl:if>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile"
			select="$motionActivityNode/MotionAttributes/AccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile" select="$motionActivityNode/MotionAttributes/ToolProfile"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<!-- objectframe profile replaced (part coords) -->
		<xsl:variable name="replaced" select="$motionActivityNode/MotionAttributes/ObjectFrameProfile/@Replaced"/>
		<xsl:variable name="objectFrameProfile">
			<xsl:choose>
				<xsl:when test="string($replaced)=''">
					<xsl:value-of select="$motionActivityNode/MotionAttributes/ObjectFrameProfile"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$replaced"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Motion profile values used by this motion activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speed"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Accuracy profile values used by this motion activity -->
		<!-- Variable that determines whether rounding is turned on or off. Possible values: 'On' or 'Off' -->
		<xsl:variable name="flyByMode"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/FlyByMode"/>
		<!-- Rounding criterium: 'Speed' or 'Distance' -->
		<xsl:variable name="accuracyType"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyType"/>
		<!-- Rounding precision value -->
		<xsl:variable name="accuracyValue"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyValue/@Value"/>
		<xsl:variable name="accuracyUnits"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyValue/@Units"/>
		<!-- Tool profile values used by this motion activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType"
			select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame profile values used by this motion activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame"
			select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<xsl:variable name="ofPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Object frame's location offset in Y direction -->
		<xsl:variable name="ofPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Object frame's location offset in Z direction -->
		<xsl:variable name="ofPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Object frame's orientation offset in Yaw direction -->
		<xsl:variable name="ofOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Object frame's orientation offset in Pitch direction -->
		<xsl:variable name="ofOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Object frame's orientation offset in Roll direction -->
		<xsl:variable name="ofOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Motion type of this motion activity: 'Joint' or 'Linear' -->
		<xsl:variable name="motionType" select="$motionActivityNode/MotionAttributes/MotionType"/>
		<!-- Orient mode of this motion activity: 'Wrist', '1_Axis', '2_Axis', or '3_Axis' -->
		<xsl:variable name="orientMode" select="$motionActivityNode/MotionAttributes/OrientMode"/>
		<!-- Target type of this motion activity: 'Joint' or 'Cartesian' -->
		<xsl:variable name="targetType" select="string($motionActivityNode/Target/@Default)"/>
		<!-- Boolean that specifies whether this target is a via point or not: 'true' or 'false' -->
		<xsl:variable name="isViaPoint" select="$motionActivityNode/Target/@ViaPoint"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this move -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basePositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="baseOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="baseOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baseOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Location attribute in case of joint and pure Cartesian targets -->
		<xsl:variable name="locationAttribute"
			select="$motionActivityNode/AttributeList/Attribute[AttributeName = 'Location Name']/AttributeValue"/>
		<!-- Cartesian target components -->
		<!-- Cartesian target position and orientation values. Target offset is defined with respect to object frame or if object frame
			offset is zero (x, y, z, roll, pitch, and yaw values are all set to 0) with respect to robot's base frame -->
		<!-- Target location offset in X direction -->
		<xsl:variable name="targetPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/CartesianTarget/Position/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Target location offset in Y direction -->
		<xsl:variable name="targetPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/CartesianTarget/Position/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Target location offset in Z direction -->
		<xsl:variable name="targetPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/CartesianTarget/Position/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Target orientation offset in Yaw direction -->
		<xsl:variable name="targetOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/CartesianTarget/Orientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Target orientation offset in Pitch direction -->
		<xsl:variable name="targetOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/CartesianTarget/Orientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Target orientation offset in Roll direction -->
		<xsl:variable name="targetOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/CartesianTarget/Orientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="xyzwpr"
			select="concat($targetPositionX, ',', $targetPositionY, ',', $targetPositionZ, ',',
												   $targetOrientationYaw, ',', $targetOrientationPitch, ',', $targetOrientationRoll)"/>
		<xsl:variable name="matxyzwpr" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
		<xsl:variable name="oat" select="MatrixUtils:xformToOAT()"/>
		<xsl:variable name="oat2" select="substring-after($oat, ',')"/>
		<xsl:variable name="o" select="substring-before($oat, ',')"/>
		<xsl:variable name="a" select="substring-before($oat2, ',')"/>
		<xsl:variable name="t" select="substring-after($oat2, ',')"/>
		<!-- Tag name, empty if doesn't exist -->
		<xsl:variable name="tagName" select="string($motionActivityNode/Target/CartesianTarget/Tag)"/>
		<!-- If this target has been defined in cartesian coordinates -->
		<xsl:if test="$targetType = 'Cartesian'">
			<!-- Retrieve tag name, and possibly, tag group name and the name of the part to which that tag group is attached to -->
			<!-- If this Cartesian target has a Tag element defined, tagName variable will not be empty, and if the tag is contained in a tag group
				which is attached to a part, tagGroupName and partOwnerName variables will not be empty.  Before creating tag output
				 in the result document, variables below need to be tested for validity in xsl:if element -->
			<!-- Tag group name that contains the above tag, empty if doesn't exist -->
			<xsl:variable name="tagGroupName"
				select="$motionActivityNode/Target/CartesianTarget/Tag/@TagGroup"/>
			<!-- Part name that has the above tag group attached, empty if doesn't exist -->
			<xsl:variable name="partOwnerName"
				select="$motionActivityNode/Target/CartesianTarget/Tag/@AttachedToPart"/>
			<!-- ============== CREATION OF TARGET DECLARATION SECTION =============================================================== -->
			<!-- Creation of Cartesian target declaration -->
			<xsl:if test=" $mode = 'TargetDeclarationSection' ">
				<xsl:if
					test="($tagName != '' and not(contains(UserData:GetParameterData('LIST_OF_TAGS'), concat(' ', string($tagName), ' ')))) or (string($locationAttribute) != '' and not(contains(UserData:GetParameterData('LIST_OF_LOCS'), concat(' ', string($locationAttribute), ' '))))">
					<!-- Create the Cartesian target declaration output-->
					<xsl:value-of select="$cr"/>
					<xsl:choose>
						<!-- Cartesian taget with tag -->
						<xsl:when test="$tagName != '' ">
							<xsl:value-of select="$tagName"/>
							<xsl:variable name="hr"
								select="UserData:SetParameterData('LIST_OF_TAGS', concat(UserData:GetParameterData('LIST_OF_TAGS'), ' ', string($tagName), ' '))"
							/>
						</xsl:when>
						<!-- If location name attribute is defined -->
						<xsl:when test="$locationAttribute != '' ">
							<xsl:value-of select="$locationAttribute"/>
							<xsl:variable name="hr"
								select="UserData:SetParameterData('LIST_OF_LOCS', concat(UserData:GetParameterData('LIST_OF_LOCS'), ' ', string($locationAttribute), ' '))"
							/>
						</xsl:when>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($targetPositionX, $decimalNumberPattern)"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($targetPositionY, $decimalNumberPattern)"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($targetPositionZ, $decimalNumberPattern)"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($o, $decimalNumberPattern)"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($a, $decimalNumberPattern)"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="format-number($t, $decimalNumberPattern)"/>
					<!-- If this robot target has auxiliary values -->
					<xsl:if test="count($motionActivityNode/Target/JointTarget/AuxJoint) > 0">
						<!-- For each auxiliary joint, get -->
						<xsl:for-each select="$motionActivityNode/Target/JointTarget/AuxJoint">
							<!-- Auxiliary joint name -->
							<xsl:variable name="auxJointName" select="@JointName"/>
							<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
							<xsl:variable name="jointType" select="@JointType"/>
							<!-- V5 type of auxiliary axis. Valid entries are: 'EndOfArmTooling', 'RailTrackGantry', and 'WorkpiecePositioner' -->
							<xsl:variable name="auxJointType" select="@Type"/>
							<!-- Degree of freedom number of this auxiliary joint. Has to be greater than the highest dof number of base robot joints -->
							<xsl:variable name="auxDofNumber" select="@DOFNumber"/>
							<!-- Auxiliary joint value -->
							<xsl:variable name="auxJointValue">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="JointValue"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<!-- Creation of auxiliary target declaration -->
							<xsl:if test=" $mode = 'TargetDeclarationSection' ">
								<!-- Create auxiliary target declaration output -->
								<xsl:if test="$jointType = 'Translational' ">
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($auxJointValue * 1000.0, $decimalNumberPattern)"
									/>
								</xsl:if>
								<xsl:if test="$jointType = 'Rotational' ">
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($auxJointValue, $decimalNumberPattern)"
									/>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
				</xsl:if>
			</xsl:if>
		</xsl:if>
		<!-- If this target has been defined in joint coordinates -->
		<xsl:if test="$targetType = 'Joint' and $mode != 'BlockFormat'">
			<xsl:if
				test="(string($locationAttribute) != '' and not(contains(UserData:GetParameterData('LIST_OF_LOCS'), concat(' ', string($locationAttribute), ' '))) and (string($motionActivityNode/Target/JointTarget/HomeName) = '')) or (string($locationAttribute) = '' and (string($motionActivityNode/Target/JointTarget/HomeName) = ''))">
				<!-- Get joint names, values, types and dof numbers-->
				<xsl:if
					test="count($motionActivityNode/Target/JointTarget/Joint) > 0 and $mode = 'TargetDeclarationSection'">
					<xsl:value-of select="$cr"/>
					<xsl:choose>
						<xsl:when test="$locationAttribute != '' ">
							<xsl:text>#</xsl:text>
							<xsl:value-of select="$locationAttribute"/>
							<xsl:variable name="hr"
								select="UserData:SetParameterData('LIST_OF_LOCS', concat(UserData:GetParameterData('LIST_OF_LOCS'), ' ', string($locationAttribute), ' '))"
							/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>#P</xsl:text>
							<xsl:value-of select="NumberIncrement:next() - $numberOfUnmarkedTargets"
							/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- For each base robot joint (non-auxiliary joint), get -->
					<xsl:for-each select="$motionActivityNode/Target/JointTarget/Joint">
						<!-- Joint name -->
						<xsl:variable name="jointName" select="@JointName"/>
						<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
						<xsl:variable name="jointType" select="@JointType"/>
						<!-- Degree of freedom number of this joint -->
						<xsl:variable name="dofNumber" select="@DOFNumber"/>
						<!-- Joint value -->
						<xsl:variable name="jointValue">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="JointValue"/>
								<xsl:with-param name="Units" select="1.0"/>
							</xsl:call-template>
						</xsl:variable>
						<!-- Creation of joint target declaration -->
						<xsl:text> </xsl:text>
						<xsl:choose>
							<xsl:when test="$jointType = 'Translational'">
								<xsl:value-of
									select="format-number($jointValue * 1000.0, $decimalNumberPattern)"
								/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of
									select="format-number($jointValue, $decimalNumberPattern)"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<!-- If this robot target has auxiliary values -->
					<xsl:if test="count($motionActivityNode/Target/JointTarget/AuxJoint) > 0">
						<!-- For each auxiliary joint, get -->
						<xsl:for-each select="$motionActivityNode/Target/JointTarget/AuxJoint">
							<!-- Auxiliary joint name -->
							<xsl:variable name="auxJointName" select="@JointName"/>
							<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
							<xsl:variable name="jointType" select="@JointType"/>
							<!-- V5 type of auxiliary axis. Valid entries are: 'EndOfArmTooling', 'RailTrackGantry', and 'WorkpiecePositioner' -->
							<xsl:variable name="auxJointType" select="@Type"/>
							<!-- Degree of freedom number of this auxiliary joint. Has to be greater than the highest dof number of base robot joints -->
							<xsl:variable name="auxDofNumber" select="@DOFNumber"/>
							<!-- Auxiliary joint value -->
							<xsl:variable name="auxJointValue">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="JointValue"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<!-- Creation of auxiliary target declaration -->
							<xsl:if test=" $mode = 'TargetDeclarationSection' ">
								<!-- Create auxiliary target declaration output -->
								<xsl:if test="$jointType = 'Translational' ">
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($auxJointValue * 1000.0, $decimalNumberPattern)"
									/>
								</xsl:if>
								<xsl:if test="$jointType = 'Rotational' ">
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($auxJointValue, $decimalNumberPattern)"
									/>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
				</xsl:if>
			</xsl:if>
		</xsl:if>
		<!-- ================================================================== CREATION OF MOTION STATEMENT ========================================== -->
		<xsl:choose>
			<xsl:when test=" $mode = 'MoveStatement' ">
				<xsl:variable name="currentTaskName"
					select="UserData:GetParameterData('CURRENT_TASK_NAME')"/>
				<!-- Set Speed -->
				<xsl:variable name="lastTCPSpeed"
					select="UserData:GetParameterData('LAST_TCP_SPEED')"/>
				<!-- no need to output default speed which is 10% at the begining -->
				<xsl:if
					test="string-length($lastTCPSpeed)!=0 or $motionBasis!='Percent' or number($speed)!=10">
					<xsl:if
						test="string($lastTCPSpeed) != concat(string($currentTaskName), string($speed), string($motionBasis))">
						<xsl:value-of select="$cr"/>
						<xsl:text>SPEED </xsl:text>
						<xsl:choose>
							<xsl:when test="$motionBasis = 'Percent'">
								<xsl:value-of select="$speed"/>
							</xsl:when>
							<xsl:when test="$motionBasis = 'Absolute'">
								<xsl:value-of select="$speed * 1000"/>
								<xsl:text> MM/S</xsl:text>
							</xsl:when>
							<xsl:when test="$motionBasis = 'Time'">
								<xsl:value-of select="$speed"/>
								<xsl:text> S</xsl:text>
							</xsl:when>
						</xsl:choose>
						<xsl:if test="$nextMotionProfile = $motionProfile">
							<xsl:text> ALWAYS</xsl:text>
						</xsl:if>
						<xsl:variable name="hr"
							select="UserData:SetParameterData('LAST_TCP_SPEED', concat(string($currentTaskName), string($speed), string($motionBasis)))"
						/>
					</xsl:if>
				</xsl:if>
				<!-- Set Acceleration -->
				<xsl:choose>
					<xsl:when
						test="AttributeList/Attribute[AttributeName='Robot Language' and 
								(starts-with(AttributeValue, 'ACCEL ') or
								 starts-with(AttributeValue, 'DECEL '))]">
						<!-- output ACCEL/DECEL attributes -->
						<xsl:call-template name="SetAttributes">
							<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
							<xsl:with-param name="outputOnlyAccelDecel" select="true()"/>
						</xsl:call-template>
						<xsl:variable name="accelAttribute"
							select="AttributeList/Attribute[starts-with(AttributeValue, 'ACCEL ')]/AttributeValue"/>
						<xsl:variable name="accelVal">
							<xsl:call-template name="getAccelFromAttribute">
								<xsl:with-param name="accelAttribute" select="$accelAttribute"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:if test="string-length($accelVal)">
							<xsl:variable name="hr"
								select="UserData:SetParameterData('LAST_ACCELERATION', concat(string($currentTaskName), string($accelVal)))"
							/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<!-- output ACCEL value if no ACCEL attribute.
						 V5 ACCEL does not allow negative value, therefore there is no way to tell when to output DECEL.
					-->
						<xsl:variable name="lastAcceleration"
							select="UserData:GetParameterData('LAST_ACCELERATION')"/>
						<xsl:if test="string-length($lastAcceleration)!=0 or number($accel)!=100">
							<xsl:if
								test="string($lastAcceleration) != concat(string($currentTaskName), string($accel))">
								<xsl:value-of select="$cr"/>
								<xsl:text>ACCEL </xsl:text>
								<xsl:value-of select="$accel"/>
								<xsl:text> ALWAYS</xsl:text>
								<xsl:variable name="hr"
									select="UserData:SetParameterData('LAST_ACCELERATION', concat(string($currentTaskName), string($accel)))"
								/>
							</xsl:if>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<!-- Set Accuracy -->
				<xsl:variable name="lastAccuracy"
					select="UserData:GetParameterData('LAST_ACCURACY')"/>
				<xsl:variable name="lastFlyByMode"
					select="UserData:GetParameterData('LAST_FLYBYMODE')"/>
				<xsl:if
					test="string($lastAccuracy) != concat(string($currentTaskName), string($accuracyProfile))">
					<xsl:choose>
						<xsl:when test="$lastFlyByMode = 'FlyByMode OFF' and $flyByMode = 'Off'"/>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="$flyByMode = 'Off'">
									<xsl:value-of select="$cr"/>
									<xsl:text>CP OFF</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:if
										test="$flyByMode = 'On' and $lastFlyByMode = 'FlyByMode OFF'">
										<xsl:value-of select="$cr"/>
										<xsl:text>CP ON</xsl:text>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="$accuracyType = 'Distance'">
											<xsl:value-of select="$cr"/>
											<xsl:text>ACCURACY </xsl:text>
											<xsl:value-of select="$accuracyValue * 1000"/>
											<xsl:if test="$nextAccuracyProfile = $accuracyProfile">
												<xsl:text> ALWAYS</xsl:text>
											</xsl:if>
											<xsl:variable name="hr"
												select="UserData:SetParameterData('LAST_ACCURACY', concat(string($currentTaskName), string($accuracyProfile)))"
											/>
										</xsl:when>
										<xsl:when test="$accuracyType = 'Speed'">
											<xsl:value-of select="$cr"/>
											<xsl:text>;--- ACCURACY </xsl:text>
											<xsl:value-of select="$accuracyValue"/>
											<xsl:value-of select="$accuracyUnits"/>
											<xsl:text> ---</xsl:text>
											<xsl:text>ERROR INFO START</xsl:text>
											<xsl:value-of select="$cr"/>
											<xsl:text>ACCURACY TYPE PERCENT NOT ALLOWED FOR KAWASAKI AS TRANSLATION</xsl:text>
											<xsl:value-of select="$cr"/>
											<xsl:text>ERROR INFO END</xsl:text>
										</xsl:when>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
							<!-- save FlyBy status -->
							<xsl:variable name="sFlyByMode">
								<xsl:choose>
									<xsl:when test="$flyByMode = 'Off'">FlyByMode OFF</xsl:when>
									<xsl:otherwise>FlyByMode ON</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:variable name="hr"
								select="UserData:SetParameterData('LAST_FLYBYMODE', $sFlyByMode)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- Set Tool Profile -->
				<xsl:variable name="lastTool" select="UserData:GetParameterData('LAST_TOOL')"/>
				<xsl:if
					test="string($lastTool) != concat(string($currentTaskName), string($toolProfile))">
					<xsl:value-of select="$cr"/>
					<xsl:text>TOOL </xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:variable name="hr"
						select="UserData:SetParameterData('LAST_TOOL', concat(string($currentTaskName), string($toolProfile)))"
					/>
				</xsl:if>

				<!-- Set Base Profile -->
				<xsl:variable name="lastBase" select="UserData:GetParameterData('LAST_BASE')"/>
				<xsl:if
					test="string($lastBase) != concat(string($currentTaskName), string($objectFrameProfile))">
					<xsl:value-of select="$cr"/>
					<xsl:text>BASE </xsl:text>
					<xsl:choose>
						<xsl:when test="string($objectFrameProfile)='Default'">
							<xsl:text>WORK0</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectFrameProfile"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:variable name="hr"
						select="UserData:SetParameterData('LAST_BASE', concat(string($currentTaskName), string($objectFrameProfile)))"
					/>
				</xsl:if>

				<!-- Set Configutation for Cartesian targets -->
				<xsl:if test="$targetType = 'Cartesian'">
					<!-- Configuration string name -->
					<xsl:variable name="lastConfig"
						select="UserData:GetParameterData('LAST_CONFIG')"/>
					<xsl:variable name="configurationName"
						select="$motionActivityNode/Target/CartesianTarget/Config/@Name"/>
					<xsl:if
						test="string($lastConfig) != concat(string($currentTaskName), string($configurationName))">
						<xsl:value-of select="$cr"/>
						<xsl:choose>
							<xsl:when test="$configurationName = 'Config_1'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config1')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>UWRIST BELOW RIGHTY</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$configurationName = 'Config_2'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config2')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>DWRIST BELOW RIGHTY</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$configurationName = 'Config_3'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config3')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>UWRIST ABOVE RIGHTY</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$configurationName = 'Config_4'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config4')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>DWRIST ABOVE RIGHTY</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$configurationName = 'Config_5'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config5')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>UWRIST BELOW LEFTY</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$configurationName = 'Config_6'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config6')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>DWRIST BELOW LEFTY</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$configurationName = 'Config_7'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config7')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>UWRIST ABOVE LEFTY</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$configurationName = 'Config_8'">
								<xsl:variable name="cfgmap"
									select="UserData:GetParameterData('Config8')"/>
								<xsl:if test="string-length($cfgmap) &gt; 0">
									<xsl:value-of select="$cfgmap"/>
								</xsl:if>
								<xsl:if test="string-length($cfgmap) &lt; 1">
									<xsl:text>DWRIST ABOVE LEFTY</xsl:text>
								</xsl:if>
							</xsl:when>
						</xsl:choose>
						<xsl:variable name="hr"
							select="UserData:SetParameterData('LAST_CONFIG', concat(string($currentTaskName), string($configurationName)))"
						/>
					</xsl:if>
				</xsl:if>
				<!-- Set motion type -->
				<xsl:value-of select="$cr"/>
				<xsl:choose>
					<xsl:when test="$motionActivityNode/Target/JointTarget/HomeName">
						<xsl:choose>
							<xsl:when
								test="$motionActivityNode/Target/JointTarget/HomeName = UserData:GetParameterData('HOME')">
								<xsl:text>HOME</xsl:text>
							</xsl:when>
							<xsl:when
								test="$motionActivityNode/Target/JointTarget/HomeName = UserData:GetParameterData('HOME2')">
								<xsl:text>HOME2</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>HOME</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$motionType = 'Linear'">
						<xsl:text>LMOVE </xsl:text>
					</xsl:when>
					<xsl:when test="$motionType = 'Joint'">
						<xsl:text>JMOVE </xsl:text>
					</xsl:when>
					<xsl:when test="$motionType = 'Circular'">
						<xsl:text>C2MOVE </xsl:text>
					</xsl:when>
					<xsl:when test="$motionType = 'CircularVia'">
						<xsl:text>C1MOVE </xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="string($motionActivityNode/Target/JointTarget/HomeName) = ''">
					<!-- Cartesian taget with tag -->
					<xsl:if
						test="string($replaced) = '' and (round($ofPositionX) != 0 or round($ofPositionY) != 0 or round($ofPositionZ) != 0 or round($ofOrientationYaw) != 0 or round($ofOrientationPitch) != 0 or round($ofOrientationRoll) 	!= 0) and $targetType = 'Cartesian'">
						<!-- Create XYZYPR string for base with respect to World location-->
						<xsl:variable name="baseWorldXYZRPY"
							select="concat($basePositionX, ',',  $basePositionY,  ',' , $basePositionZ, ',' , $baseOrientationYaw, ',' ,  $baseOrientationPitch, ',' , 														$baseOrientationRoll)"/>
						<!-- Set Base/World XForm -->
						<xsl:variable name="baseFrameWorldXForm"
							select="MatrixUtils:dgXyzyprToMatrix($baseWorldXYZRPY)"/>
						<!-- Invert to World/Base XForm -->
						<xsl:variable name="worldBaseXForm" select="MatrixUtils:dgInvert()"/>
						<!-- Create XYZYPR string for object frame with respect to World location-->
						<xsl:variable name="objectFrameXYZRPY"
							select="concat($ofPositionX, ',',  $ofPositionY,  ',' , $ofPositionZ, ',' , $ofOrientationYaw, ',' ,  $ofOrientationPitch, ',' ,  $ofOrientationRoll)"/>
						<!-- Create object frame with respect to World XForm -->
						<xsl:variable name="objectFrameWorldXForm"
							select="MatrixUtils:dgCatXyzyprMatrix($objectFrameXYZRPY)"/>
						<!-- Get object frame with respect to World XYZYPR values -->
						<xsl:variable name="objectFrameWorldX" select="MatrixUtils:dgGetX()"/>
						<xsl:variable name="objectFrameWorldY" select="MatrixUtils:dgGetY()"/>
						<xsl:variable name="objectFrameWorldZ" select="MatrixUtils:dgGetZ()"/>
						<xsl:variable name="objectFrameWorldYaw" select="MatrixUtils:dgGetYaw()"/>
						<xsl:variable name="objectFrameWorldPitch" select="MatrixUtils:dgGetPitch()"/>
						<xsl:variable name="objectFrameWorldRoll" select="MatrixUtils:dgGetRoll()"/>
						<!-- Since object frame offset exists create a compound transformation for this target -->
						<xsl:text>TRANS(</xsl:text>
						<xsl:text> </xsl:text>
						<xsl:value-of
							select="format-number($objectFrameWorldX, $decimalNumberPattern)"/>
						<xsl:text> </xsl:text>
						<xsl:value-of
							select="format-number($objectFrameWorldY, $decimalNumberPattern)"/>
						<xsl:text> </xsl:text>
						<xsl:value-of
							select="format-number($objectFrameWorldZ, $decimalNumberPattern)"/>
						<xsl:text> </xsl:text>
						<xsl:value-of
							select="format-number($objectFrameWorldYaw, $decimalNumberPattern)"/>
						<xsl:text> </xsl:text>
						<xsl:value-of
							select="format-number($objectFrameWorldPitch, $decimalNumberPattern)"/>
						<xsl:text> </xsl:text>
						<xsl:value-of
							select="format-number($objectFrameWorldRoll, $decimalNumberPattern)"/>
						<xsl:text>)</xsl:text>
						<xsl:text> + </xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$motionActivityNode/Target/CartesianTarget/Tag != '' ">
							<xsl:value-of select="$motionActivityNode/Target/CartesianTarget/Tag"/>
						</xsl:when>
						<!-- If location name attribute is defined -->
						<xsl:when test="$locationAttribute != '' ">
							<xsl:if test="$targetType = 'Joint'">
								<xsl:text>#</xsl:text>
							</xsl:if>
							<xsl:value-of select="$locationAttribute"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="$targetType = 'Joint'">
									<xsl:text>#P</xsl:text>
									<xsl:value-of select="NumberIncrement:next()"/>
								</xsl:when>
								<xsl:when test="$targetType = 'Cartesian'">
									<xsl:text>TRANS( </xsl:text>
									<xsl:value-of
										select="format-number($targetPositionX, $decimalNumberPattern)"/>
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($targetPositionY, $decimalNumberPattern)"/>
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($targetPositionZ, $decimalNumberPattern)"/>
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($targetOrientationYaw, $decimalNumberPattern)"/>
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($targetOrientationPitch, $decimalNumberPattern)"/>
									<xsl:text> </xsl:text>
									<xsl:value-of
										select="format-number($targetOrientationRoll, $decimalNumberPattern)"/>
									<!-- If this robot target has auxiliary values -->
									<xsl:if
										test="count($motionActivityNode/Target/JointTarget/AuxJoint) > 0">
										<!-- For each auxiliary joint, get -->
										<xsl:for-each
											select="$motionActivityNode/Target/JointTarget/AuxJoint">
											<!-- Auxiliary joint name -->
											<xsl:variable name="auxJointName" select="@JointName"/>
											<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
											<xsl:variable name="jointType" select="@JointType"/>
											<!-- V5 type of auxiliary axis. Valid entries are: 'EndOfArmTooling', 'RailTrackGantry', and 'WorkpiecePositioner' -->
											<xsl:variable name="auxJointType" select="@Type"/>
											<!-- Degree of freedom number of this auxiliary joint. Has to be greater than the highest dof number of base robot joints -->
											<xsl:variable name="auxDofNumber" select="@DOFNumber"/>
											<!-- Auxiliary joint value -->
											<xsl:variable name="auxJointValue">
												<xsl:call-template name="Scientific">
												<xsl:with-param name="Num" select="JointValue"/>
												<xsl:with-param name="Units" select="1.0"/>
												</xsl:call-template>
											</xsl:variable>
											<!-- Creation of auxiliary target declaration -->
											<!-- Create auxiliary target declaration output -->
											<xsl:if test="$jointType = 'Translational' ">
												<xsl:text> </xsl:text>
												<xsl:value-of
												select="format-number($auxJointValue * 1000.0, $decimalNumberPattern)"
												/>
											</xsl:if>
											<xsl:if test="$jointType = 'Rotational' ">
												<xsl:text> </xsl:text>
												<xsl:value-of
												select="format-number($auxJointValue, $decimalNumberPattern)"
												/>
											</xsl:if>
										</xsl:for-each>
									</xsl:if>
									<xsl:text>)</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$mode='BlockFormat'">
				<xsl:choose>
					<xsl:when test="$motionType = 'Linear'">
						<xsl:text>LINEAR </xsl:text>
					</xsl:when>
					<xsl:when test="$motionType = 'Joint'">
						<xsl:text>JOINT </xsl:text>
					</xsl:when>
					<xsl:when test="$motionType = 'Circular'">
						<xsl:text>CIR2 </xsl:text>
					</xsl:when>
					<xsl:when test="$motionType = 'CircularVia'">
						<xsl:text>CIR </xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<!-- BLOCK SPEED -->
					<xsl:when test="substring($motionProfile, 1, 5) = 'SPEED'">
						<xsl:value-of select="$motionProfile"/>
					</xsl:when>
					<xsl:when test="$speedabstime != 'true'">
						<xsl:text>SPEED</xsl:text>
						<xsl:variable name="percentSpeed">
							<xsl:choose>
								<xsl:when test="$motionBasis = 'Percent'">
									<xsl:value-of select="$speed"/>
								</xsl:when>
								<xsl:when test="$motionBasis = 'Absolute'">
									<xsl:value-of select="number($speed*100.0 div $maxSpeed)"/>
								</xsl:when>
								<xsl:when test="$motionBasis = 'Time'"> </xsl:when>
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$motionBasis = 'Time'">
								<xsl:text>0</xsl:text>
								<xsl:text>ERROR INFO START</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>Motion Profile &lt;</xsl:text>
								<xsl:value-of select="$motionProfile"/>
								<xsl:text>&gt; time not valid for BLOCK format.</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO END</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="speedSplit">
									<xsl:call-template name="stringsplit">
										<xsl:with-param name="pText" select="$auxdataspeed"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="hr"
									select="UserData:SetParameterData('SPEEDDONE','')"/>
								<xsl:for-each select="ext:node-set($speedSplit)/*">
									<xsl:variable name="speeddone"
										select="UserData:GetParameterData('SPEEDDONE')"/>
									<xsl:if
										test="number($percentSpeed) = number(.) and $speeddone!='true'">
										<xsl:value-of select="position()-1"/>
										<xsl:variable name="hr"
											select="UserData:SetParameterData('SPEEDDONE','true')"/>
									</xsl:if>
									<xsl:if
										test="number($percentSpeed) &gt; number(.) and $speeddone!='true'">
										<xsl:if
											test="last()=position() or number($percentSpeed) &lt; number(following-sibling::*[1])">
											<xsl:value-of select="position()-1"/>
											<xsl:variable name="hr"
												select="UserData:SetParameterData('SPEEDDONE','true')"
											/>
										</xsl:if>
									</xsl:if>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>SPEED0</xsl:text>
						<xsl:text>ERROR INFO START</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:text>Motion Profile &lt;</xsl:text>
						<xsl:value-of select="$motionProfile"/>
						<xsl:text>&gt; not valid for BLOCK format.</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:text>ERROR INFO END</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<!-- BLOCK ACCU -->
				<xsl:choose>
					<xsl:when test="substring($accuracyProfile, 1, 4) = 'ACCU'">
						<xsl:value-of select="$accuracyProfile"/>
					</xsl:when>
					<xsl:when test="$flyByMode = 'Off'">
						<xsl:text>ACCU0</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>ACCU</xsl:text>
						<xsl:variable name="accuVal">
							<xsl:choose>
								<xsl:when test="$accuracyType = 'Distance'">
									<xsl:value-of select="$accuracyValue*1000.0"/>
								</xsl:when>
								<xsl:otherwise> </xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$accuracyType = 'Distance'">
								<xsl:variable name="accuracySplit">
									<xsl:call-template name="stringsplit">
										<xsl:with-param name="pText" select="$auxdataaccu"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="hr"
									select="UserData:SetParameterData('ACCUDONE','')"/>
								<xsl:for-each select="ext:node-set($accuracySplit)/*">
									<xsl:variable name="accudone"
										select="UserData:GetParameterData('ACCUDONE')"/>
									<xsl:if
										test="number($accuVal) = number(.) and last() != position() and $accudone != 'true'">
										<xsl:value-of select="position()"/>
										<xsl:variable name="hr"
											select="UserData:SetParameterData('ACCUDONE','true')"/>
									</xsl:if>
									<xsl:if
										test="$accuVal &gt; . and last() != position() and $accudone != 'true'">
										<xsl:if
											test="last()=(position()-1) or $accuVal &lt; following-sibling::*[1]">
											<xsl:value-of select="position()"/>
											<xsl:variable name="hr"
												select="UserData:SetParameterData('ACCUDONE','true')"
											/>
										</xsl:if>
									</xsl:if>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>0</xsl:text>
								<xsl:text>ERROR INFO START</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>Accuracy Profile &lt;</xsl:text>
								<xsl:value-of select="$accuracyProfile"/>
								<xsl:text>&gt; percent not valid for BLOCK format.</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO END</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<!-- SINCE TIMER (delay Activity) IS NEXT MUST STORE TOOL, WORK, CLAMP -->
				<xsl:variable name="postTimer">
					<xsl:choose>
						<xsl:when test="substring($toolProfile, 1, 4) = 'TOOL'">
							<xsl:value-of select="$toolProfile"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>TOOL</xsl:text>
							<xsl:apply-templates
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolProfile]"
							/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:choose>
						<xsl:when test="substring($objectFrameProfile, 1, 4) = 'WORK'">
							<xsl:value-of select="$objectFrameProfile"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>WORK0</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:choose>
						<xsl:when
							test="string(AttributeList/Attribute[AttributeName =  'CLAMP_Information1']/AttributeValue) != ''">
							<xsl:value-of
								select="AttributeList/Attribute[AttributeName =  'CLAMP_Information1']/AttributeValue"
							/>
						</xsl:when>
						<xsl:when
							test="number($numberofclamp) &gt; 1 or number($numberofclamp)=1">
							<xsl:choose>
								<xsl:when test="$endofarmtooltype='HANDLING'">
									<xsl:text>CLAMP1 (OFF,0,0,C)</xsl:text>
								</xsl:when>
								<xsl:when test="$endofarmtooltype='SERVOGUN'">
									<xsl:text>CLAMP1 (OFF,0,0,0)</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>CLAMP1 (OFF,0,0,O)</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if
								test="number($numberofclamp) &gt; 2 or number($numberofclamp)=2">
								<xsl:choose>
									<xsl:when test="$endofarmtooltype='HANDLING'">
										<xsl:text> 2 (OFF,0,0,C)</xsl:text>
									</xsl:when>
									<xsl:when test="$endofarmtooltype='SERVOGUN'">
										<xsl:text> 2 (OFF,0,0,0)</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text> 2 (OFF,0,0,O)</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if
									test="number($numberofclamp) &gt; 3 or number($numberofclamp)=3">
									<xsl:choose>
										<xsl:when test="$endofarmtooltype='HANDLING'">
											<xsl:text> 3 (OFF,0,0,C)</xsl:text>
										</xsl:when>
										<xsl:when test="$endofarmtooltype='SERVOGUN'">
											<xsl:text> 3 (OFF,0,0,0)</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text> 3 (OFF,0,0,O)</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:if test="number($numberofclamp)=4">
										<xsl:choose>
											<xsl:when test="$endofarmtooltype='HANDLING'">
												<xsl:text> 4 (OFF,0,0,C)</xsl:text>
											</xsl:when>
											<xsl:when test="$endofarmtooltype='SERVOGUN'">
												<xsl:text> 4 (OFF,0,0,0)</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text> 4 (OFF,0,0,O)</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</xsl:if>
							</xsl:if>
							<xsl:text> </xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<!-- SINCE OX WX (set/wait activity) IS NEXT MUST STORE CL1 and JOINTS -->
				<xsl:variable name="postIO">
					<xsl:choose>
						<xsl:when
							test="string(AttributeList/Attribute[AttributeName =  'CLAMP_Information2']/AttributeValue) != ''">
							<xsl:value-of
								select="AttributeList/Attribute[AttributeName =  'CLAMP_Information2']/AttributeValue"
							/>
						</xsl:when>
						<xsl:when test="($endofarmtooltype='SERVOGUN')">
							<xsl:choose>
								<xsl:when test="$controller='D'">
									<xsl:text>CL1=0.000,0.0,0.0,0.0,0.0</xsl:text>
								</xsl:when>
								<xsl:when test="$controller='E'">
									<xsl:text>CL1=0.000,0.0,0.0,0.0,0.0,0.0,0.0</xsl:text>
								</xsl:when>
								<xsl:otherwise> </xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<!-- JOINT VALUES -->
					<!-- Get joint names, values, types and dof numbers-->
					<xsl:if test="count($motionActivityNode/Target/JointTarget/Joint) > 0">
						<xsl:text>#[</xsl:text>

						<!-- For each base robot joint (non-auxiliary joint), get -->
						<xsl:for-each select="$motionActivityNode/Target/JointTarget/Joint">
							<!-- Joint name -->
							<xsl:variable name="jointName" select="@JointName"/>
							<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
							<xsl:variable name="jointType" select="@JointType"/>
							<!-- Degree of freedom number of this joint -->
							<xsl:variable name="dofNumber" select="@DOFNumber"/>
							<!-- Joint value -->
							<xsl:variable name="jointValue">
								<xsl:call-template name="Scientific">
									<xsl:with-param name="Num" select="JointValue"/>
									<xsl:with-param name="Units" select="1.0"/>
								</xsl:call-template>
							</xsl:variable>
							<!-- Creation of joint target declaration -->
							<xsl:choose>
								<xsl:when test="$jointType = 'Translational'">
									<xsl:value-of
										select="format-number($jointValue * 1000.0, $decimalNumberPattern)"
									/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of
										select="format-number($jointValue, $decimalNumberPattern)"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if
								test="position() &lt; count($motionActivityNode/Target/JointTarget/Joint) or count($motionActivityNode/Target/JointTarget/AuxJoint) != 0">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:for-each>
						<!-- If this robot target has auxiliary values -->
						<xsl:if test="count($motionActivityNode/Target/JointTarget/AuxJoint) > 0">
							<!-- For each auxiliary joint, get -->
							<xsl:for-each select="$motionActivityNode/Target/JointTarget/AuxJoint">
								<!-- Auxiliary joint name -->
								<xsl:variable name="auxJointName" select="@JointName"/>
								<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
								<xsl:variable name="jointType" select="@JointType"/>
								<!-- V5 type of auxiliary axis. Valid entries are: 'EndOfArmTooling', 'RailTrackGantry', and 'WorkpiecePositioner' -->
								<xsl:variable name="auxJointType" select="@Type"/>
								<!-- Degree of freedom number of this auxiliary joint. Has to be greater than the highest dof number of base robot joints -->
								<xsl:variable name="auxDofNumber" select="@DOFNumber"/>
								<!-- Auxiliary joint value -->
								<xsl:variable name="auxJointValue">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num" select="JointValue"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<!-- Creation of auxiliary target declaration -->

								<!-- Create auxiliary target declaration output -->
								<xsl:if test="$jointType = 'Translational' ">
									<xsl:value-of
										select="format-number($auxJointValue * 1000.0, $decimalNumberPattern)"
									/>
								</xsl:if>
								<xsl:if test="$jointType = 'Rotational' ">
									<xsl:value-of
										select="format-number($auxJointValue, $decimalNumberPattern)"
									/>
								</xsl:if>
								<xsl:if
									test="position() &lt; count($motionActivityNode/Target/JointTarget/AuxJoint)">
									<xsl:text>,</xsl:text>
								</xsl:if>
							</xsl:for-each>
						</xsl:if>
					</xsl:if>
					<xsl:text>]</xsl:text>
					<!-- END JOINT OUTPUT -->
				</xsl:variable>
				<xsl:variable name="hr" select="UserData:SetParameterData('PostTimer', $postTimer)"/>
				<xsl:variable name="hr" select="UserData:SetParameterData('PostIO', $postIO)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getAccelFromAttribute">
		<xsl:param name="accelAttribute"/>

		<xsl:if test="string-length($accelAttribute) &gt; 0">
			<xsl:variable name="accel" select="substring-after($accelAttribute, 'ACCEL ')"/>
			<xsl:choose>
				<xsl:when test="contains($accel, ' ALWAYS')">
					<xsl:value-of select="substring-before($accel, ' ALWAYS')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$accel"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:param name="Units"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'e')"/>
						<xsl:with-param name="e" select="substring-after($Num,'e')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'E')"/>
						<xsl:with-param name="e" select="substring-after($Num,'E')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num*$Units"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="stringsplit">
		<xsl:param name="pText"/>
		<xsl:if test="string-length($pText) >0">
			<xsl:choose>
				<xsl:when test="contains($pText,',')">
					<item>
						<xsl:value-of select="substring-before(concat($pText, ','), ',')"/>
					</item>

					<xsl:call-template name="stringsplit">
						<xsl:with-param name="pText" select="substring-after($pText, ',')"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="normalizedpText" select="normalize-space($pText)"/>
					<item>
						<xsl:value-of select="substring-before(concat($normalizedpText, ' '), ' ')"
						/>
					</item>

					<xsl:call-template name="stringsplit">
						<xsl:with-param name="pText" select="substring-after($normalizedpText, ' ')"
						/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template match="ToolProfile">
		<xsl:value-of select="count(preceding-sibling::ToolProfile)+1"/>
	</xsl:template>
	<xsl:template name="finishBlockRobotMotion">
		<xsl:variable name="partialRobotMotion"
			select="UserData:GetParameterData('PARTIALROBOTMOTION')"/>
		<xsl:if test="$partialRobotMotion='true'">
			<xsl:variable name="oxValue" select="UserData:GetParameterData('OXValue')"/>
			<xsl:variable name="timerValue" select="UserData:GetParameterData('TIMERValue')"/>
			<xsl:variable name="postTimer" select="UserData:GetParameterData('PostTimer')"/>
			<xsl:variable name="postIO" select="UserData:GetParameterData('PostIO')"/>
			<xsl:variable name="preoxValue" select="UserData:GetParameterData('PreOXValue')"/>
			<xsl:variable name="wxValue" select="UserData:GetParameterData('WXValue')"/>
			<xsl:if test="$postTimer != ''">
				<xsl:value-of select="$timerValue"/>
				<xsl:variable name="hr" select="UserData:SetParameterData('TIMERValue', 'TIMER0')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$postTimer"/>
				<xsl:variable name="hr" select="UserData:SetParameterData('PostTimer', '')"/>
			</xsl:if>

			<xsl:if test="$postIO != ''">
				<xsl:choose>
					<xsl:when test="$ox_preoutput='true'">
						<xsl:value-of select="$preoxValue"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$oxValue"/>
						<xsl:if test="$oxwxnegative='true'">
							<xsl:variable name="hr"
								select="UserData:SetParameterData('OXValue', 'OX=')"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$wxValue"/>
				<xsl:variable name="hr" select="UserData:SetParameterData('WXValue', 'WX=')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$postIO"/>
				<xsl:variable name="hr" select="UserData:SetParameterData('PostTimer', '')"/>
			</xsl:if>
			<xsl:variable name="hr" select="UserData:SetParameterData('PARTIALROBOTMOTION','')"/>
		</xsl:if>
	</xsl:template>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TEMPLATE DEFINITIONS END~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
</xsl:stylesheet>
