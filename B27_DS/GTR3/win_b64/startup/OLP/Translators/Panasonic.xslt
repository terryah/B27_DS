<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:lxslt="http://xml.apache.org/xslt" 
xmlns:java="http://xml.apache.org/xslt/java" 
xmlns:NumberIncrement="NumberIncrement" 
xmlns:MotomanUtils="MotomanUtils" 
xmlns:KIRStrUtils="KIRStrUtils" 
extension-element-prefixes="NumberIncrement MotomanUtils KIRStrUtils">
	<lxslt:component prefix="NumberIncrement" functions="next current" elements="startnum increment reset">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MotomanUtils" functions="JointToEncoderConversion SetParameterData GetParameterData" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"/>
	</lxslt:component>
	<lxslt:component prefix="KIRStrUtils" functions="storeToolNum retrieveToolNum" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.KIRStrUtils"/>
	</lxslt:component>
	<xsl:strip-space elements="*"/>
	<xsl:output method="text"/>
	<xsl:variable name="debug" select="0"/>
	<xsl:variable name="version" select="'DELMIA CORP. Panasonic CSR DOWNLOADER VERSION 5 RELEASE 27 SP2'"/>
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 1986-2017, ALL RIGHTS RESERVED'"/>
	<xsl:variable name="cr" select="'&#xa;'"/>
	<xsl:variable name="numpat" select="'0000'"/>
	<xsl:variable name="jointPositionPattern" select="'#'"/>
	<xsl:variable name="cartesianPositionPattern" select="'#.###'"/>
	<xsl:variable name="patD1" select="'0.0'"/>
	<xsl:variable name="patD2" select="'0.00'"/>
	<xsl:variable name="patD3" select="'0.000'"/>
	<xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<xsl:variable name="rbtAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<xsl:variable name="auxAxes" select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<xsl:variable name="extAxes" select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<xsl:variable name="posAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="dof" select="$rbtAxes+$auxAxes+$extAxes+$posAxes"/>
	<xsl:variable name="dofAux" select="$auxAxes+$extAxes+$posAxes"/>
	<xsl:variable name="maxSpeedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
	<xsl:variable name="downloadStyle">
		<xsl:choose>
			<xsl:when test="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBIgpCallRobotTask']/DownloadStyle='File'">
				<xsl:text>File</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<!-- xsl:text>Subroutine</xsl:text -->
				<xsl:text>File</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="/">
		<xsl:call-template name="HeaderInfo"/>
		<xsl:call-template name="InitializeParameters"/>
		<xsl:apply-templates select="OLPData"/>
	</xsl:template>

	<xsl:template name="HeaderInfo">
		<xsl:text>VERSION INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>VERSION INFO END</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	
	<xsl:template name="InitializeParameters">
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<xsl:variable name="paramName" select="ParameterName"/>
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<xsl:variable name="hr" select="MotomanUtils:SetParameterData( string($paramName), string($paramValue) )"/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="OLPData">
		<xsl:apply-templates select="Resource[GeneralInfo/ResourceName=$robotName]/ActivityList"/>
	</xsl:template>

	<xsl:template name="outputDate">
		<xsl:variable name="formatter" select="java:java.text.SimpleDateFormat.new('yyyy, M, d, H, m, s')"/>
		<xsl:variable name="c" select="java:java.util.Calendar.getInstance()"/>
		<xsl:variable name="c2" select="java:getTime($c)"/>
		<xsl:value-of select="java:format($formatter, $c2)"/>
	</xsl:template>

	<xsl:template name="getAttribute">
		<xsl:param name="taskName"/>
		<xsl:param name="attributeName"/>
		<xsl:param name="attributeValue"/>
		
		<xsl:for-each select="/OLPData/Resource/ActivityList[@Task=$taskName]/AttributeList/Attribute">
			<xsl:if test="starts-with(AttributeName, $attributeName) and starts-with(AttributeValue, $attributeValue)">
				<xsl:value-of select="AttributeValue"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="dec2hex">
		<xsl:param name="decNum"/>
		
		<xsl:if test="$decNum >= 16">
			<xsl:call-template name="dec2hex">
				<xsl:with-param name="decNum" select="floor($decNum div 16)"/>
			</xsl:call-template>
		</xsl:if>

		<xsl:variable name="hexDigit">
			<xsl:value-of select="$decNum mod 16"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$hexDigit &lt; 10">
				<xsl:value-of select="$hexDigit"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="translate($hexDigit - 10, '012345', 'ABCDEF')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
        
	<xsl:template match="ActivityList">
		<xsl:variable name="taskName" select="@Task"/>
		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[1]">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START </xsl:text>
			<!-- Job name limited to 8 characters -->
			<xsl:value-of select="translate(@Task, '.', '')"/>
			<xsl:text>.csr</xsl:text>
		</xsl:if>

		<xsl:value-of select="$cr"/>
		<xsl:text>[Description]</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>Robot, </xsl:text>
		<xsl:value-of select="substring-before($robotName, '.')"/>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="comment">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Description.'"/>
				<xsl:with-param name="attributeValue" select="'Comment,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($comment) &gt; 0">
				<xsl:value-of select="$comment"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Comment, </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="mechanism">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Description.'"/>
				<xsl:with-param name="attributeValue" select="'Mechanism,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($mechanism) &gt; 0">
				<xsl:value-of select="$mechanism"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Mechanism, 1(</xsl:text>
				<xsl:variable name="dofIn4" select="floor($dofAux div 4)"/>
				<xsl:choose>
					<xsl:when test="$dofIn4 = 0">000</xsl:when>
					<xsl:when test="$dofIn4 = 1">00</xsl:when>
					<xsl:when test="$dofIn4 = 2">0</xsl:when>
				</xsl:choose>
				<xsl:variable name="dofMod4" select="$dofAux mod 4"/>
				<xsl:choose>
					<xsl:when test="$dofMod4 = 0 and $dofIn4 &lt; 4">0</xsl:when>
					<xsl:when test="$dofMod4 = 1">1</xsl:when>
					<xsl:when test="$dofMod4 = 2">3</xsl:when>
					<xsl:when test="$dofMod4 = 3">7</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="$dofIn4 = 1">F</xsl:when>
					<xsl:when test="$dofIn4 = 2">FF</xsl:when>
					<xsl:when test="$dofIn4 = 3">FFF</xsl:when>
					<xsl:when test="$dofIn4 = 4">FFFF</xsl:when>
				</xsl:choose>
				<xsl:text>)</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="toolTerm">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Description.'"/>
				<xsl:with-param name="attributeValue" select="'Tool,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($toolTerm) &gt; 0">
				<xsl:value-of select="$toolTerm"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Tool, </xsl:text>
				<xsl:variable name="toolName" select="Activity[@ActivityType='DNBRobotMotionActivity'][1]/MotionAttributes/ToolProfile"/>
				<xsl:choose>
					<xsl:when test="$toolName">
						<xsl:call-template name="getToolNum">
							<xsl:with-param name="toolName" select="$toolName"/>
						</xsl:call-template>
						<xsl:text>:</xsl:text>
						<xsl:value-of select="$toolName"/>
					</xsl:when>
					<xsl:otherwise>1:TOOL01</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="creator">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Description.'"/>
				<xsl:with-param name="attributeValue" select="'Creator,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($creator) &gt; 0">
				<xsl:value-of select="$creator"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Creator, robot</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="createDate">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Description.'"/>
				<xsl:with-param name="attributeValue" select="'Create,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($createDate) &gt; 0">
				<xsl:value-of select="$createDate"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Create, </xsl:text>
				<xsl:call-template name="outputDate"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>

		<xsl:text>Update, </xsl:text>
		<xsl:call-template name="outputDate"/>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="original">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Description.'"/>
				<xsl:with-param name="attributeValue" select="'Original,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($original) &gt; 0">
				<xsl:value-of select="$original"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Original, </xsl:text>
				<xsl:value-of select="translate(@Task, '.', '')"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="edit">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Description.'"/>
				<xsl:with-param name="attributeValue" select="'Edit,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($edit) &gt; 0">
				<xsl:value-of select="$edit"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Edit, 0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>

		<xsl:text>[Pose]</xsl:text>
		<xsl:value-of select="$cr"/>

		<xsl:variable name="poseNameType">
			<xsl:call-template name="getAttribute">
				<xsl:with-param name="taskName" select="$taskName"/>
				<xsl:with-param name="attributeName" select="'Pose.'"/>
				<xsl:with-param name="attributeValue" select="'/Name, Type,'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($poseNameType) &gt; 0">
				<xsl:value-of select="$poseNameType"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>/Name, Type, RT, UA, FA, RW, BW, TW, </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
		
		<xsl:call-template name="setTargetStartNumber"/>
		
		<xsl:call-template name="outputPosData"/>
		<xsl:for-each select="AttributeList/Attribute[starts-with(AttributeName, 'Pose.')]">
			<xsl:if test="starts-with(AttributeValue, '/Name, Type,') = false">
				<xsl:value-of select="AttributeValue"/>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:for-each>
		<NumberIncrement:reset/>

		<xsl:value-of select="$cr"/>
		<xsl:text>[Variable]</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:variable name="variables" select="AttributeList/Attribute[starts-with(AttributeName, 'Variable.')]"/>
		<xsl:choose>
			<xsl:when test="count($variables) = 0">
				<xsl:call-template name="outputDefaultVariables"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="AttributeList/Attribute[starts-with(AttributeName, 'Variable.')]">
					<xsl:value-of select="AttributeValue"/>
					<xsl:value-of select="$cr"/>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$cr"/>
		
		<xsl:text>[Command]</xsl:text>
		<xsl:choose>
			<xsl:when test="Activity[1]/@ActivityType='Operation'">
				<xsl:variable name="attr">
					<xsl:call-template name="toupper">
						<xsl:with-param name="instring" select="Activity[1]/AttributeList/Attribute/AttributeValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="toolAttr" select="Activity[1]/AttributeList/Attribute[starts-with(AttributeName, 'Robot Language') and starts-with($attr, 'TOOL')]/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="string-length($toolAttr) &gt; 0"/>
					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>Tool, </xsl:text>
						<xsl:variable name="toolName" select="Activity[@ActivityType='DNBRobotMotionActivity'][1]/MotionAttributes/ToolProfile"/>
						<xsl:choose>
							<xsl:when test="$toolName">
								<xsl:call-template name="getToolNum">
									<xsl:with-param name="toolName" select="$toolName"/>
								</xsl:call-template>
								<xsl:text>:</xsl:text>
								<xsl:value-of select="$toolName"/>
							</xsl:when>
							<xsl:otherwise>1:TOOL01</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="toolAttr" select="Activity[1]/AttributeList/Attribute[starts-with(AttributeName, 'PreComment') and starts-with(AttributeValue, 'Robot Language:TOOL')]/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="string-length($toolAttr) &gt; 0"/>
					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>Tool, </xsl:text>
						<xsl:variable name="toolName" select="Activity[@ActivityType='DNBRobotMotionActivity'][1]/MotionAttributes/ToolProfile"/>
						<xsl:choose>
							<xsl:when test="$toolName">
								<xsl:call-template name="getToolNum">
									<xsl:with-param name="toolName" select="$toolName"/>
								</xsl:call-template>
								<xsl:text>:</xsl:text>
								<xsl:value-of select="$toolName"/>
							</xsl:when>
							<xsl:otherwise>1:TOOL01</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates select="Activity"/>
		
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNode" select="AttributeList"/>
		</xsl:call-template>
        
		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[last()]">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE END</xsl:text>
		</xsl:if>
	</xsl:template>
	<!-- end of template ActivityList -->
	
	<xsl:template match="Activity">
		<xsl:variable name="setSignalPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
		<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
		<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
		<xsl:variable name="setSignalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>

		<xsl:variable name="waitSignalPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
		<xsl:variable name="waitSignalName" select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
		<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
		<xsl:variable name="waitSignalMaxWaitTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNode" select="AttributeList"/>
		</xsl:call-template>

		<xsl:apply-templates select="AttributeList/Attribute"/>

		<xsl:choose>
			<xsl:when test="@ActivityType='Operation'">
			</xsl:when>
			<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
				<xsl:value-of select="$cr"/>
				<xsl:text>CALL, </xsl:text>
				<xsl:variable name="callname" select="CallName"/>
				<xsl:value-of select="translate($callname, '.', '')"/>
				<xsl:text>.csr</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DelayActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:text>DELAY, </xsl:text>
				<xsl:variable name="start" select="ActivityTime/StartTime"/>
				<xsl:variable name="end" select="ActivityTime/EndTime"/>
				<xsl:value-of select="format-number($end - $start, $patD2)"/>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:variable name="home" select="Target/JointTarget/HomeName"/>
				<xsl:if test="string-length($home) &gt; 0 and position() = last()">
					<xsl:text>GOHOME, </xsl:text>
				</xsl:if>
				<xsl:variable name="nextMoAct" select="following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
				<xsl:variable name="nextMoType" select="$nextMoAct/MotionAttributes/MotionType"/>
				<xsl:variable name="moType" select="MotionAttributes/MotionType"/>
				<xsl:variable name="wv" select="MotionAttributes/UserProfile[@Type='PANWeave']"/>
				<xsl:variable name="wvprofile" select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type='PANWeave']"/>
				<xsl:variable name="wvinstance" select="$wvprofile/UserProfileInstance[@Name=$wv]"/>
				<xsl:variable name="wvsetting" select="$wvinstance/UserDefinedAttribute[DisplayName='Weave']/Value"/>
				<xsl:variable name="weave">
					<xsl:if test="string-length($wvinstance) &gt; 0">W</xsl:if>
				</xsl:variable>
				<xsl:variable name="sync" select="MotionAttributes/UserProfile[@Type='PANSync']"/>
				<xsl:variable name="syncProfile" select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type='PANSync']"/>
				<xsl:variable name="syncInstance" select="$syncProfile/UserProfileInstance[@Name=$sync]"/>
				<xsl:variable name="syncSetting" select="$syncInstance/UserDefinedAttribute[DisplayName='Sync']/Value"/>
				<xsl:variable name="synchronization">
					<xsl:if test="$syncSetting = 'true'">+</xsl:if>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$nextMoType='CircularVia'"><!-- need at least 3 MOVEC commands -->
						<xsl:text>MOVEC</xsl:text>
						<xsl:value-of select="$weave"/>
						<xsl:value-of select="$synchronization"/>
					</xsl:when>
					<xsl:when test="$moType='Joint'">MOVEP</xsl:when>
					<xsl:when test="$moType='Linear'">
						<xsl:text>MOVEL</xsl:text>
						<xsl:value-of select="$weave"/>
						<xsl:value-of select="$synchronization"/>
					</xsl:when>
					<xsl:when test="$moType='Circular' or $moType='CircularVia'">
						<xsl:text>MOVEC</xsl:text>
						<xsl:value-of select="$weave"/>
						<xsl:value-of select="$synchronization"/>
					</xsl:when>
				</xsl:choose>
				
				<xsl:text>, </xsl:text>
				<xsl:variable name="identifier" select="AttributeList/Attribute[AttributeName='Identifier']/AttributeValue"/>
				<xsl:variable name="targetNum" select="AttributeList/Attribute[AttributeName='TargetNum']/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="string-length($identifier) &gt; 0">
						<xsl:value-of select="substring-before($identifier, ':')"/>
						<xsl:text>(</xsl:text>
						<xsl:value-of select="NumberIncrement:next()"/>
						<xsl:text>:</xsl:text>
						<xsl:value-of select="substring-after($identifier, ':')"/>
						<xsl:text>)</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="csrTargetName">
							<xsl:text>P</xsl:text>
							<xsl:choose>
								<xsl:when test="string-length($targetNum) &gt; 0">
									<xsl:value-of select="$targetNum"/>
								</xsl:when>
								<xsl:otherwise> <!-- Tag, Joint or Cartesian target -->
									<xsl:value-of select="NumberIncrement:next()"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="tagName">
							<xsl:choose>
								<xsl:when test="Target/@Default='Cartesian' and Target/CartesianTarget/Tag">
									<xsl:value-of select="concat(Target/CartesianTarget/Tag/@TagGroup, Target/CartesianTarget/Tag)"/>
								</xsl:when>
								<xsl:otherwise> <!-- Joint or Cartesian target -->
									<xsl:value-of select="$csrTargetName"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:value-of select="KIRStrUtils:getTagName($tagName, '', '')"/>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:text>, </xsl:text>
				<xsl:call-template name="outputSpeed">
					<xsl:with-param name="motionProfile" select="MotionAttributes/MotionProfile"/>
				</xsl:call-template>
				
				<xsl:if test="string-length($home) = 0 or position() != last()">
					<xsl:choose>
						<xsl:when test="MotionAttributes/MotionType='Joint'"/>
						<xsl:otherwise>
							<xsl:text>, </xsl:text>
							<xsl:variable name="clnoname" select="MotionAttributes/UserProfile[@Type='PANCLNo']"/>
							<xsl:variable name="clnoprofile" select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type='PANCLNo']"/>
							<xsl:variable name="clnoinstance" select="$clnoprofile/UserProfileInstance[@Name=$clnoname]"/>
							<xsl:variable name="clnosetting" select="$clnoinstance/UserDefinedAttribute[DisplayName='CLNo']/Value"/>
							<xsl:choose>
								<xsl:when test="string-length($clnosetting) &gt; 0">
									<xsl:value-of select="$clnosetting"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="CL_num" select="AttributeList/Attribute[AttributeName='CL No']/AttributeValue"/>
									<xsl:choose>
										<xsl:when test="string-length($CL_num) &gt; 0">
											<xsl:value-of select="$CL_num"/>
										</xsl:when>
										<xsl:otherwise>
											<!-- give warning -->
											<xsl:text>VERSION INFO START</xsl:text>
											<xsl:value-of select="$cr"/>
											<xsl:text>WARNING: Parameter "CL No" not set for </xsl:text>
											<xsl:value-of select="@Operation"/>
											<xsl:text>:</xsl:text>
											<xsl:value-of select="ActivityName"/>
											<xsl:text>. Zero(0) is used for CL Number.</xsl:text>
											<xsl:value-of select="$cr"/>
											<xsl:text>VERSION INFO END</xsl:text>
											<!-- output zero(0) -->
											<xsl:text>0</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
	
					<xsl:text>, </xsl:text>
					<xsl:variable name="weldname" select="MotionAttributes/UserProfile[@Type='PANWeld']"/>
					<xsl:variable name="weldprofile" select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type='PANWeld']"/>
					<xsl:variable name="weldinstance" select="$weldprofile/UserProfileInstance[@Name=$weldname]"/>
					<xsl:variable name="weldsetting" select="$weldinstance/UserDefinedAttribute[DisplayName='Weld']/Value"/>
					<xsl:choose>
						<xsl:when test="string-length($weldsetting) &gt; 0">
							<xsl:value-of select="$weldsetting"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="weld" select="AttributeList/Attribute[AttributeName='Air-Cut/Weld']/AttributeValue"/>
							<xsl:choose>
								<xsl:when test="string-length($weld) &gt; 0">
									<xsl:value-of select="$weld"/>
								</xsl:when>
								<xsl:otherwise>N</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
					
					<xsl:if test="string-length($wvinstance) &gt; 0">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="$wvinstance/UserDefinedAttribute[DisplayName='WeavePattern']/Value"/>
						<xsl:text>, </xsl:text>
						<xsl:value-of select="format-number($wvinstance/UserDefinedAttribute[DisplayName='WeaveFrequency']/Value, $patD1)"/>
						<xsl:text>, </xsl:text>
						<xsl:value-of select="format-number($wvinstance/UserDefinedAttribute[DisplayName='WeaveTimer']/Value, $patD1)"/>
						<xsl:variable name="wvDir2">
							<xsl:call-template name="toupper">
								<xsl:with-param name="instring" select="$wvinstance/UserDefinedAttribute[DisplayName='WeaveDirection']/Value"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="wvDir">
							<xsl:choose>
								<xsl:when test="$wvDir2 != 'VECTOR'">SIMPLE</xsl:when>
								<xsl:otherwise>VECTOR</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="dirChg2">
							<xsl:call-template name="toupper">
								<xsl:with-param name="instring" select="$wvinstance/UserDefinedAttribute[DisplayName='DirectionChange']/Value"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="dirChg">
							<xsl:choose>
								<xsl:when test="$dirChg2 != 'TOOL'">POINT</xsl:when>
								<xsl:otherwise>TOOL</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:if test="$wvDir = 'VECTOR' or $dirChg = 'TOOL'">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="$wvDir"/>
							<xsl:text>, </xsl:text>
							<xsl:value-of select="$dirChg"/>
						</xsl:if>
					</xsl:if>
					<!-- output comments between MOVE and ARC commands -->
					<xsl:call-template name="processComments">
						<xsl:with-param name="prefix" select="'Mid'"/>
						<xsl:with-param name="attributeListNode" select="AttributeList"/>
					</xsl:call-template>
					<!-- ARC-SET before ARC-ON -->
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcSET']"/>
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcSetTig']"/>
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcSetTigFC']"/>
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcON']"/>
					<!-- CRATER before ARC-OFF -->
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcCRATER']"/>
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcCraterTig']"/>
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcCraterTigFC']"/>
					<xsl:apply-templates select="MotionAttributes/UserProfile[@Type='PANArcOFF']"/>
				</xsl:if>
			</xsl:when>
			<!-- @ActivityType='DNBRobotMotionActivity' -->
			
			<xsl:when test="@ActivityType='DNBSetSignalActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:if test="$setSignalDuration != 0">
					<!-- give warning -->
					<xsl:text>VERSION INFO START</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>WARNING: Non-zero duration is ignored in SetSignal activity.</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>VERSION INFO END</xsl:text>
				</xsl:if>
				
				<xsl:variable name="oGroupBit" select="AttributeList/Attribute[AttributeName='Output Group Bit']/AttributeValue"/>
				<xsl:text>OUT</xsl:text>
				<xsl:text>, o</xsl:text>
				<xsl:choose>
					<xsl:when test="string-length($oGroupBit) &gt; 0">
						<xsl:value-of select="$oGroupBit"/>
					</xsl:when>
					<xsl:otherwise>1</xsl:otherwise>
				</xsl:choose>
				<xsl:text>#(</xsl:text>
				<xsl:value-of select="$setSignalPortNumber"/>
				<xsl:text>:</xsl:text>

				<xsl:variable name="identifier" select="AttributeList/Attribute[AttributeName='Identifier']/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="string-length($identifier) &gt; 0">
						<xsl:value-of select="$identifier"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$setSignalName"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>), </xsl:text>
				
				<xsl:choose>
					<xsl:when test="string-length($oGroupBit) = 0 or $oGroupBit = '1'">
						<xsl:choose>
							<xsl:when test="$setSignalValue='Off'">
								<xsl:text>OFF</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>ON</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$setSignalValue='Off'">
								<xsl:text>0</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>1</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			
			<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:text>WAIT_IP, i1#(</xsl:text>
				<xsl:value-of select="$waitSignalPortNumber"/>
				<xsl:text>:</xsl:text>

				<xsl:variable name="identifier" select="AttributeList/Attribute[AttributeName='Identifier']/AttributeValue"/>
				<xsl:choose>
					<xsl:when test="string-length($identifier) &gt; 0">
						<xsl:value-of select="$identifier"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$setSignalName"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>), </xsl:text>

				<xsl:choose>
					<xsl:when test="$waitSignalValue='Off'">
						<xsl:text>OFF, </xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>ON, </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="format-number($waitSignalMaxWaitTime, $patD2)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- end template Activity -->
	
	<xsl:template match="UserProfile">
		<xsl:variable name="name" select="."/>
		<xsl:variable name="type" select="./@Type"/>
		<xsl:variable name="userProfile" select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type=$type]"/>
		<xsl:variable name="instance" select="$userProfile/UserProfileInstance[@Name=$name]"/>
		<xsl:variable name="fileName" select="$instance/UserDefinedAttribute[DisplayName='FileName']/Value"/>
		<xsl:variable name="tableNumber" select="$instance/UserDefinedAttribute[DisplayName='TableNumber']/Value"/>
		<xsl:variable name="amperage" select="$instance/UserDefinedAttribute[DisplayName='Amperage']/Value"/>
		<xsl:variable name="voltage" select="$instance/UserDefinedAttribute[DisplayName='Voltage']/Value"/>
		<xsl:variable name="speed" select="$instance/UserDefinedAttribute[DisplayName='Speed']/Value"/>
		<xsl:variable name="time" select="$instance/UserDefinedAttribute[DisplayName='Time']/Value"/>
		<xsl:variable name="bsAmp" select="$instance/UserDefinedAttribute[DisplayName='BaseAmperage']/Value"/>
		<xsl:variable name="pkAmp" select="$instance/UserDefinedAttribute[DisplayName='PeakAmperage']/Value"/>
		<xsl:variable name="fillerSpeed" select="$instance/UserDefinedAttribute[DisplayName='FillerSpeed']/Value"/>
		<xsl:variable name="frequency" select="$instance/UserDefinedAttribute[DisplayName='Frequency']/Value"/>
		<xsl:variable name="bsWireVel" select="$instance/UserDefinedAttribute[DisplayName='BaseWireVelocity']/Value"/>
		<xsl:variable name="pkWireVel" select="$instance/UserDefinedAttribute[DisplayName='PeakWireVelocity']/Value"/>
		<xsl:variable name="pulseFrequency" select="$instance/UserDefinedAttribute[DisplayName='PulseFrequency']/Value"/>
		
		<xsl:choose>
			<xsl:when test="@Type = 'PANArcSET'">
				<xsl:value-of select="$cr"/>
				<xsl:text>ARC-SET, </xsl:text>
				<xsl:value-of select="$amperage"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($voltage, $patD1)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($speed, $patD2)"/>
			</xsl:when>
			<xsl:when test="@Type = 'PANArcON'">
				<xsl:value-of select="$cr"/>
				<xsl:text>ARC-ON, </xsl:text>
				<xsl:value-of select="$fileName"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$tableNumber"/>
			</xsl:when>
			<xsl:when test="@Type = 'PANArcOFF'">
				<xsl:value-of select="$cr"/>
				<xsl:text>ARC-OFF, </xsl:text>
				<xsl:value-of select="$fileName"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$tableNumber"/>
			</xsl:when>
			<xsl:when test="@Type = 'PANArcCRATER'">
				<xsl:value-of select="$cr"/>
				<xsl:text>CRATER, </xsl:text>
				<xsl:value-of select="$amperage"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($voltage, $patD1)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($time, $patD2)"/>
			</xsl:when>
			<xsl:when test="@Type = 'PANArcSetTig'">
				<xsl:value-of select="$cr"/>
				<xsl:text>ARC-SET_TIG, </xsl:text>
				<xsl:value-of select="$bsAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$pkAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($fillerSpeed, $patD2)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($frequency, $patD1)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($speed, $patD2)"/>
			</xsl:when>
			<xsl:when test="@Type = 'PANArcCraterTig'">
				<xsl:value-of select="$cr"/>
				<xsl:text>CRATER_TIG, </xsl:text>
				<xsl:value-of select="$bsAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$pkAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($fillerSpeed, $patD2)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($frequency, $patD1)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($time, $patD2)"/>
			</xsl:when>
			<xsl:when test="@Type = 'PANArcSetTigFC'">
				<xsl:value-of select="$cr"/>
				<xsl:text>ARC-SET_TIGFC, </xsl:text>
				<xsl:value-of select="$bsAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$pkAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($bsWireVel, $patD2)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($pkWireVel, $patD2)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($pulseFrequency, $patD1)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($speed, $patD2)"/>
			</xsl:when>
			<xsl:when test="@Type = 'PANArcCraterTigFC'">
				<xsl:value-of select="$cr"/>
				<xsl:text>CRATER_TIGFC, </xsl:text>
				<xsl:value-of select="$bsAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$pkAmp"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($bsWireVel, $patD2)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($pkWireVel, $patD2)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($pulseFrequency, $patD1)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="format-number($time, $patD2)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputSpeed">
		<xsl:param name="motionProfile"/>
		
		<xsl:variable name="speedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Value"/>
		<xsl:variable name="speedUnits" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Units"/>
		<xsl:variable name="motionBasis" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/MotionBasis"/>
		<xsl:choose>
			<xsl:when test="$motionBasis = 'Absolute' and $speedUnits = 'm/s'">
				<xsl:value-of select="format-number($speedValue * 60, $patD2)"/>
				<xsl:text>, m/min</xsl:text>
			</xsl:when>
			<xsl:when test="$motionBasis = 'Percent' and $speedUnits = '%'">
				<xsl:value-of select="format-number($speedValue, $patD2)"/>
				<xsl:text>, %</xsl:text>
			</xsl:when>
			<xsl:when test="$motionBasis = 'Time' and $speedUnits = 's'">
				<xsl:value-of select="format-number($speedValue * 60, $patD2)"/>
				<xsl:text>, m/min</xsl:text>
				<!-- give warning -->
				<xsl:text>VERSION INFO START</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>WARNING: Timed based motion profile &lt;</xsl:text>
				<xsl:value-of select="$motionProfile"/>
				<xsl:text>&gt; is used as Absolute motion profile.</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>VERSION INFO END</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNode"/>

		<xsl:for-each select="$attributeListNode/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:if test="starts-with($attrname, $prefix)">
				<xsl:choose>
					<xsl:when test="starts-with($attrvalue, 'Robot Language:')">
						<xsl:value-of select="$cr"/>
						<xsl:value-of select="substring-after($attrvalue, 'Robot Language:')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>REM, </xsl:text>
						<xsl:value-of select="$attrvalue"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="Attribute">
		<xsl:variable name="attrname" select = "AttributeName"/>
		<xsl:variable name="attrvalue" select="AttributeValue"/>

		<xsl:if test="substring($attrname,1,7) = 'Comment'">
			<xsl:value-of select="$cr"/>
			<xsl:text>REM, </xsl:text>
			<xsl:value-of select="$attrvalue"/>
		</xsl:if>

		<xsl:if test="substring($attrname,1,14) = 'Robot Language'">
			<xsl:value-of select="$cr"/>
			<xsl:value-of select="$attrvalue"/>
		</xsl:if>

	</xsl:template>

	<xsl:template name="getToolNum">
		<xsl:param name="toolName"/>

		<xsl:variable name="toolProfiles" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile"/>
		<xsl:for-each select="$toolProfiles">
			<xsl:if test="./Name = $toolName">
				<xsl:value-of select="position()"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="setTargetStartNumber">
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:increment inc="1"/>
		
		<xsl:variable name="targetNumAttrVal" select="Activity/AttributeList/Attribute[AttributeName='TargetNum']/AttributeValue"/>
		<xsl:for-each select="$targetNumAttrVal">
			<xsl:sort data-type="number"/>
		</xsl:for-each>
		<xsl:variable name="largestTargetNum" select="$targetNumAttrVal[last()]"/>
		
		<xsl:if test="string-length($largestTargetNum) &gt; 0">
			<xsl:variable name="dummy" select="NumberIncrement:startnum(number($largestTargetNum))"/>
		</xsl:if>
		
		<xsl:variable name="poseAttrVal" select="AttributeList/Attribute[starts-with(AttributeName, 'Pose.') and starts-with(AttributeValue, 'P')]/AttributeValue"/>
		<xsl:for-each select="$poseAttrVal">
			<xsl:sort select="substring-before(substring(., 2), ',')" data-type="number"/>
		</xsl:for-each>
		<xsl:variable name="largestPoseNum" select="substring-before(substring($poseAttrVal[last()], 2), ',')"/>
		
		<xsl:if test="string-length($largestPoseNum) &gt; 0">
			<xsl:choose>
				<xsl:when test="string-length($largestTargetNum) = 0">
					<xsl:variable name="dummy2" select="NumberIncrement:startnum(number($largestPoseNum))"/>
				</xsl:when>
				<xsl:when test="string-length($largestTargetNum) &gt; 0 and number($largestTargetNum) &lt; number($largestPoseNum)">
					<xsl:variable name="dummy2" select="NumberIncrement:startnum(number($largestPoseNum))"/>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		
		<NumberIncrement:reset/>
	</xsl:template>
	
	<xsl:template name="outputPosData">
		<xsl:variable name="robotMoves" select="Activity[@ActivityType='DNBRobotMotionActivity']"/>
		<xsl:for-each select="$robotMoves">
			<xsl:variable name="targetNum" select="AttributeList/Attribute[AttributeName='TargetNum']/AttributeValue"/>
			<!-- check if the target has been output -->
			<xsl:variable name="csrTargetName">
				<xsl:text>P</xsl:text>
				<xsl:choose>
					<xsl:when test="string-length($targetNum) &gt; 0">
						<xsl:value-of select="$targetNum"/>
					</xsl:when>
					<xsl:otherwise> <!-- Tag, Joint or Cartesian target -->
						<xsl:value-of select="NumberIncrement:next()"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="tagName">
				<xsl:choose>
					<xsl:when test="Target/@Default='Cartesian' and Target/CartesianTarget/Tag">
						<xsl:value-of select="concat(Target/CartesianTarget/Tag/@TagGroup,Target/CartesianTarget/Tag)"/>
					</xsl:when>
					<xsl:otherwise> <!-- Joint or Cartesian target -->
						<xsl:value-of select="$csrTargetName"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="tagNameUnique" select="KIRStrUtils:getTagName($tagName, 'DAT', $csrTargetName)"/>
			<xsl:if test="$tagNameUnique">
				<xsl:value-of select="$csrTargetName"/>
				<xsl:text>, AJ, </xsl:text>
				<xsl:for-each select="Target/JointTarget/Joint">
					<xsl:variable name="jval">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="./JointValue"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="@Units = 'deg'">
							<xsl:value-of select="$jval"/>
						</xsl:when>
						<xsl:when test="@Units = 'm'">
							<xsl:value-of select="$jval * 1000"/>
						</xsl:when>
					</xsl:choose>
					<xsl:if test="position() != last()">, </xsl:if>
				</xsl:for-each>
				<!-- cannot combine with Joint using java 1.4.2 -->
				<xsl:if test="Target/JointTarget/AuxJoint">
					<xsl:text>, </xsl:text>
				</xsl:if>
				<xsl:for-each select="Target/JointTarget/AuxJoint">
					<xsl:variable name="jval">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="./JointValue"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="@Units = 'deg'">
							<xsl:value-of select="$jval"/>
						</xsl:when>
						<xsl:when test="@Units = 'm'">
							<xsl:value-of select="$jval * 1000"/>
						</xsl:when>
					</xsl:choose>
					<xsl:if test="position() != last()">, </xsl:if>
				</xsl:for-each>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template> <!-- outputPosData -->
	
	<xsl:template name="outputDefaultVariables">
		<xsl:text>LB, LB001, Byte, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LB, LB002, Byte, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LB, LB003, Byte, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LB, LB004, Byte, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LB, LB005, Byte, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LI, LI001, Int, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LI, LI002, Int, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LI, LI003, Int, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LI, LI004, Int, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LI, LI005, Int, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LL, LL001, Long, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LL, LL002, Long, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LL, LL003, Long, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LL, LL004, Long, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LL, LL005, Long, 0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LR, LR001, Real, 0.000000000000000</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LR, LR002, Real, 0.000000000000000</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LR, LR003, Real, 0.000000000000000</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LR, LR004, Real, 0.000000000000000</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>LR, LR005, Real, 0.000000000000000</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	
	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'e')"/>
					<xsl:with-param name="e" select="substring-after($Num,'e')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'E')"/>
					<xsl:with-param name="e" select="substring-after($Num,'E')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="toupper">
		<xsl:param name="instring"/>
		<xsl:value-of select="translate($instring, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	</xsl:template>

</xsl:stylesheet>

