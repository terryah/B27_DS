<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:lxslt="http://xml.apache.org/xslt" 
xmlns:java="http://xml.apache.org/xslt/java" 
xmlns:NumberIncrement="NumberIncrement" 
xmlns:MotomanUtils="MotomanUtils" 
xmlns:KIRStrUtils="KIRStrUtils" 
extension-element-prefixes="NumberIncrement MotomanUtils KIRStrUtils">
	<lxslt:component prefix="NumberIncrement" functions="next current" elements="startnum increment reset">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MotomanUtils" functions="JointToEncoderConversion SetParameterData GetParameterData" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"/>
	</lxslt:component>
	<lxslt:component prefix="KIRStrUtils" functions="storeToolNum retrieveToolNum" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.KIRStrUtils"/>
	</lxslt:component>
	<xsl:strip-space elements="*"/>
	<xsl:output method="text"/>
	<xsl:variable name="debug" select="0"/>
	<xsl:variable name="version" select="'DELMIA CORP. MITSUBISHI DOWNLOADER VERSION 5 RELEASE 24 SP4'"/>
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 2014, ALL RIGHTS RESERVED'"/>
	<xsl:variable name="cr" select="'&#xa;'"/>
	<xsl:variable name="ampersand" select="'&amp;'"/>
	<xsl:variable name="numpat2" select="'00'"/>
	<xsl:variable name="jointPositionPattern" select="'#'"/>
	<xsl:variable name="cartesianPositionPattern" select="'#.###'"/>
	<xsl:variable name="patInteger" select="'#'"/>
	<xsl:variable name="patD1" select="'0.0'"/>
	<xsl:variable name="patD2" select="'0.00'"/>
	<xsl:variable name="patD3" select="'0.000'"/>
	<xsl:variable name="patUptoD1" select="'#.#'"/>
	<xsl:variable name="patUptoD2" select="'#.##'"/>
	<xsl:variable name="patUptoD3" select="'#.###'"/>
	<xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<xsl:variable name="rbtAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<xsl:variable name="auxAxes" select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<xsl:variable name="extAxes" select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<xsl:variable name="posAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="dof" select="$rbtAxes+$auxAxes+$extAxes+$posAxes"/>
	<xsl:variable name="dofAux" select="$auxAxes+$extAxes+$posAxes"/>
	<xsl:variable name="maxSpeedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
  <xsl:variable name="TOOLNAME" select="'PTL'"/>
	<xsl:variable name="downloadStyle">
		<xsl:choose>
			<xsl:when test="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBIgpCallRobotTask']/DownloadStyle='File'">
				<xsl:text>File</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<!-- xsl:text>Subroutine</xsl:text -->
				<xsl:text>File</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="/">
		<xsl:call-template name="HeaderInfo"/>
		<xsl:choose>
			<xsl:when test="$dofAux &gt; 2">
				<xsl:text>VERSION INFO START</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR: Mitsubishi programs allow a maximum of two(2) auxiliary axes.</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>VERSION INFO END</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="InitializeParameters"/>
				<xsl:apply-templates select="OLPData"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="HeaderInfo">
		<xsl:text>VERSION INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>VERSION INFO END</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	
	<xsl:template name="InitializeParameters">
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<xsl:variable name="paramName" select="ParameterName"/>
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<xsl:variable name="hr" select="MotomanUtils:SetParameterData( string($paramName), string($paramValue) )"/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="OLPData">
		<xsl:apply-templates select="Resource[GeneralInfo/ResourceName=$robotName]/ActivityList"/>
	</xsl:template>

	<xsl:template name="getAttribute">
		<xsl:param name="taskName"/>
		<xsl:param name="attributeName"/>
		<xsl:param name="attributeValue"/>
		
		<xsl:for-each select="/OLPData/Resource/ActivityList[@Task=$taskName]/AttributeList/Attribute">
			<xsl:if test="starts-with(AttributeName, $attributeName) and starts-with(AttributeValue, $attributeValue)">
				<xsl:value-of select="AttributeValue"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="dec2hex">
		<xsl:param name="decNum"/>
		
		<xsl:if test="$decNum >= 16">
			<xsl:call-template name="dec2hex">
				<xsl:with-param name="decNum" select="floor($decNum div 16)"/>
			</xsl:call-template>
		</xsl:if>

		<xsl:variable name="hexDigit">
			<xsl:value-of select="$decNum mod 16"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$hexDigit &lt; 10">
				<xsl:value-of select="$hexDigit"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="translate($hexDigit - 10, '012345', 'ABCDEF')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
        
	<xsl:template match="ActivityList">
		<xsl:variable name="taskName" select="translate(@Task, '.', '')"/>
		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[1]">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START </xsl:text>
			<xsl:call-template name="TrimTrailing_1">
				<xsl:with-param name="instring" select="$taskName"/>
			</xsl:call-template>
			<xsl:text>.prg</xsl:text>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		
		<!-- initialize motion parameters for each task(activitylist) -->
    <xsl:variable name="dummy" select="MotomanUtils:SetParameterData('Ovrd_Output', 0)"/>
    <xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Ovrd', 100)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_JntSpd', 0)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_LinSpd', 0)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', 100)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Cnt', 0)"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Tool', '')"/>
		<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ToolNumbersUsed', '')"/>
		<xsl:value-of select="KIRStrUtils:clearDAT('TagNameTable')"/>
		<xsl:value-of select="KIRStrUtils:clearDAT('TagNameList')"/>

		<!-- Tag target counter -->
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>
		<!-- Joint target counter -->
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:reset/>
		<!-- Line number counter -->
		<NumberIncrement:counternum counter="2"/>
		<NumberIncrement:reset/>
		
		<xsl:apply-templates select="Activity[@ActivityType='DNBRobotMotionActivity']" mode="SetPrgTargetName"/>
		<xsl:apply-templates select="Activity | Action" mode="Instruction"/>
		
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNode" select="AttributeList"/>
		</xsl:call-template>
		
		<xsl:call-template name="OutputLineNumber"/>
		<xsl:text>End</xsl:text>
		<xsl:value-of select="$cr"/>
		
		<xsl:call-template name="outputToolList">
			<xsl:with-param name="toolList" select="MotomanUtils:GetParameterData('ToolsUsed')"/>
		</xsl:call-template>
		<xsl:call-template name="outputPosList"/>
		
		<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[last()]">
			<xsl:text>DATA FILE END</xsl:text>
		</xsl:if>
	</xsl:template>
	<!-- end of template ActivityList -->

	<xsl:template match="Action" mode="Instruction">
		<xsl:variable name="handNum"      select="AttributeList/Attribute[AttributeName='HandNo.']/AttributeValue"/>
		<xsl:variable name="startF" select="AttributeList/Attribute[AttributeName='StartGraspForce']/AttributeValue"/>
		<xsl:variable name="holdF"    select="AttributeList/Attribute[AttributeName='HoldGraspForce']/AttributeValue"/>
		<xsl:variable name="holdT"    select="AttributeList/Attribute[AttributeName='StartGraspForceHoldTime']/AttributeValue"/>

		<xsl:variable name="actIdSkip" select="MotomanUtils:GetParameterData('ActionActivitySkipID')"/>

		<xsl:choose>
			<xsl:when test="@id=$actIdSkip">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ActionActivitySkipID', '')"/>
			</xsl:when>
			<xsl:when test="@ActionType='MitsubishiHClose'">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>HClose </xsl:text>
				<xsl:value-of select="$handNum"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<xsl:when test="@ActionType='MitsubishiHOpen'">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>HOpen </xsl:text>
				<xsl:value-of select="$handNum"/>
				<xsl:choose>
					<xsl:when test="string-length($startF) &gt; 0">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="$startF"/>
					</xsl:when>
					<xsl:when test="string-length($holdF) &gt; 0">
						<xsl:text>, </xsl:text>
					</xsl:when>
					<xsl:when test="string-length($holdT) &gt; 0">
						<xsl:text>, </xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="string-length($holdF) &gt; 0">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="$holdF"/>
					</xsl:when>
					<xsl:when test="string-length($holdT) &gt; 0">
						<xsl:text>, </xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="string-length($holdT) &gt; 0">
					<xsl:text>, </xsl:text>
					<xsl:value-of select="$holdT"/>
				</xsl:if>
				<xsl:value-of select="$cr"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="Activity" mode="Instruction">
		<xsl:variable name="setSignalPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
		<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
		<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
		<xsl:variable name="setSignalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>

		<xsl:variable name="waitSignalPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
		<xsl:variable name="waitSignalName" select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
		<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
		<xsl:variable name="waitSignalMaxWaitTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>

		<xsl:variable name="actIdSkip" select="MotomanUtils:GetParameterData('ActionActivitySkipID')"/>

		<xsl:choose>
			<xsl:when test="@id=$actIdSkip"/>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity' and starts-with(MotionAttributes/MotionType,'Circular')">
				<!-- PreComment to CircularVia and Circular should NOT be output here as it will break
					 the Mitsubishi MVR command line.
				-->
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="processComments">
					<xsl:with-param name="prefix" select="'Pre'"/>
					<xsl:with-param name="attributeListNode" select="AttributeList"/>
				</xsl:call-template>
		
				<xsl:apply-templates select="AttributeList/Attribute"/>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:choose>
			<xsl:when test="@id=$actIdSkip">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ActionActivitySkipID', '')"/>
			</xsl:when>
			<xsl:when test="@ActivityType='Operation'">
			</xsl:when>
			<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>CallP "</xsl:text>
				<xsl:variable name="callName" select="translate(CallName, '.', '')"/>
				<xsl:call-template name="TrimTrailing_1">
					<xsl:with-param name="instring" select="$callName"/>
				</xsl:call-template>
				<xsl:text>"</xsl:text>
				<xsl:call-template name="processOptions"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<xsl:when test="@ActivityType='DelayActivity'">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>Dly </xsl:text>
				<xsl:variable name="start" select="ActivityTime/StartTime"/>
				<xsl:variable name="end" select="ActivityTime/EndTime"/>
				<xsl:value-of select="format-number($end - $start, $patUptoD2)"/>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
				<xsl:variable name="moType" select="MotionAttributes/MotionType"/>
				<xsl:variable name="toolProfile" select="MotionAttributes/ToolProfile"/>
				<xsl:variable name="motionProfile" select="MotionAttributes/MotionProfile"/>
				<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
				<xsl:choose>
					<xsl:when test="$moType = 'CircularVia' or $moType = 'Circular'">
						<xsl:variable name="storedToolProfile" select="MotomanUtils:GetParameterData('STORED_ToolProfile')"/>
						<xsl:if test="string($toolProfile) != string($storedToolProfile)">
							<xsl:text>VERSION INFO START</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>WARNING: CircularVia and Circular moves may NOT change ToolProfile from preceding Linear move.</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>The change is ignored.</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>VERSION INFO END</xsl:text>
						</xsl:if>
						<xsl:variable name="storedMotionProfile" select="MotomanUtils:GetParameterData('STORED_MotionProfile')"/>
						<xsl:if test="string($motionProfile) != string($storedMotionProfile)">
							<xsl:text>VERSION INFO START</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>WARNING: CircularVia and Circular moves may NOT change MotionProfile from preceding Linear move.</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>The change is ignored.</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>VERSION INFO END</xsl:text>
						</xsl:if>
						<xsl:variable name="storedAccuracyProfile" select="MotomanUtils:GetParameterData('STORED_AccuracyProfile')"/>
						<xsl:if test="string($accuracyProfile) != string($storedAccuracyProfile)">
							<xsl:text>VERSION INFO START</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>WARNING: CircularVia and Circular moves may NOT change AccuracyProfile from preceding Linear move.</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>The change is ignored.</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>VERSION INFO END</xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="outputTool">
							<xsl:with-param name="toolProfile" select="$toolProfile"/>
						</xsl:call-template>
						<xsl:call-template name="outputSpeed">
							<xsl:with-param name="motionProfile" select="$motionProfile"/>
							<xsl:with-param name="motionType" select="$moType"/>
						</xsl:call-template>
						<xsl:call-template name="outputAccel">
							<xsl:with-param name="motionProfile" select="$motionProfile"/>
						</xsl:call-template>
						<xsl:call-template name="outputAccuracy">
							<xsl:with-param name="accuracyProfile" select="$accuracyProfile"/>
						</xsl:call-template>
						<!-- save current profiles -->
						<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_ToolProfile', $toolProfile)"/>
						<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_MotionProfile', $motionProfile)"/>
						<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_AccuracyProfile', $accuracyProfile)"/>
					</xsl:otherwise>
				</xsl:choose>
				<!-- output motion instruction -->
				<xsl:variable name="nextMoAct" select="following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
				<xsl:variable name="nextMoType" select="$nextMoAct/MotionAttributes/MotionType"/>
				<xsl:variable name="nextSib" select="following-sibling::node()[1]"/>
				<xsl:variable name="nextSibActName">
					<xsl:choose>
						<xsl:when test="local-name($nextSib) = 'Action'">
							<xsl:value-of select="$nextSib/ActionName"/>
						</xsl:when>
						<xsl:when test="local-name($nextSib) = 'Activity'">
							<xsl:value-of select="$nextSib/ActivityName"/>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="(($moType='Linear'      and $nextMoType='CircularVia') or
							   ($moType='CircularVia' and $nextMoType='Circular'))   and $nextSib != $nextMoAct">
					<!-- Lin, CirVia, and Cir can NOT be separated by other activities or actions -->
					<!-- give warning -->
					<xsl:text>VERSION INFO START</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>WARNING: A Linear move followed immediately by a CircularVia move and a Circular</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>move is necessary to produce a Mitsubishi Mvr command.</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>The </xsl:text>
					<xsl:value-of select="local-name($nextSib)"/>
					<xsl:text> '</xsl:text>
					<xsl:value-of select="$nextSibActName"/>
					<xsl:text>' is ignored.</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:value-of select="$cr"/>
					<xsl:text>VERSION INFO END</xsl:text>
					<!-- set global parameter to skip outputing the following action -->
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ActionActivitySkipID', $nextSib/@id)"/>
				</xsl:if>
				<xsl:variable name="prgMoveInstruction">
					<xsl:choose>
						<xsl:when test="$moType='Linear'">
							<xsl:choose>
								<xsl:when test="$nextMoType='CircularVia'">MVR</xsl:when>
								<xsl:otherwise>MVS</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$moType='Joint'">MOV</xsl:when>
						<xsl:when test="$moType='Circular'">DELMIA_CIR</xsl:when>
						<xsl:when test="$moType='CircularVia'">DELMIA_CIRV</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="starts-with($prgMoveInstruction, 'DELMIA_CIR')"/>
					<xsl:otherwise>
						<xsl:call-template name="OutputLineNumber"/>
						<xsl:value-of select="$prgMoveInstruction"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="Target/@Default='Cartesian' and Target/CartesianTarget/Tag">
						<!-- Combine TagGroupName and TagName, since tag name is unique in a tag group -->
						<!-- In case a tag is re-used, another position variable won't be created -->
						<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
						<xsl:variable name="tagName" select="Target/CartesianTarget/Tag"/>
						<xsl:value-of select="KIRStrUtils:getTagName(concat($tagGroup,$tagName), '', '')"/>
					</xsl:when>
					<xsl:when test="Target/@Default='Joint'">
						<xsl:value-of select="KIRStrUtils:getTagName(@id, '', '')"/>
					</xsl:when>
					<xsl:otherwise> <!-- Cartesian target -->
						<xsl:text>(</xsl:text>
						<xsl:call-template name="outputPosData">
							<xsl:with-param name="pos" select="Target/CartesianTarget/Position"/>
							<xsl:with-param name="ori" select="Target/CartesianTarget/Orientation"/>
						</xsl:call-template>
						<xsl:call-template name="outputAuxData"/>
						<xsl:text>)</xsl:text>
						<xsl:call-template name="outputFlags"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="$prgMoveInstruction='MVR' or $prgMoveInstruction='DELMIA_CIRV'">
						<xsl:text>,</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="processOptions"/>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- @ActivityType='DNBRobotMotionActivity' -->
			
			<xsl:when test="@ActivityType='DNBSetSignalActivity'">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>M_Out(</xsl:text>
				<xsl:value-of select="$setSignalPortNumber"/>
				<xsl:text>)=</xsl:text>
				<xsl:choose>
					<xsl:when test="$setSignalValue='Off'">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:if test="$setSignalDuration != 0">
					<xsl:text> Dly </xsl:text>
					<xsl:value-of select="$setSignalDuration"/>
				</xsl:if>

				<xsl:value-of select="$cr"/>
			</xsl:when>
			
			<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>Wait M_In(</xsl:text>
				<xsl:value-of select="$waitSignalPortNumber"/>
				<xsl:text>)=</xsl:text>
				<xsl:choose>
					<xsl:when test="$waitSignalValue='Off'">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$cr"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- end template Activity -->
	
	<xsl:template match="Activity" mode="SetPrgTargetName">
		<!-- For KIRStrUtils:getTagName, first call with parameters (DELMIA_tagName, 'DAT', Mitsubishi_targetName)
			 to set table; then call with (DELMIA_tagName, '', '') to get Mitsubishi_targetName.
		 -->
		<xsl:choose>
			<xsl:when test="Target/@Default='Cartesian' and Target/CartesianTarget/Tag">
				<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
				<xsl:variable name="tagName" select="Target/CartesianTarget/Tag"/>
				<NumberIncrement:counternum counter="0"/>
				<xsl:variable name="curNum" select="NumberIncrement:current()"/>
				<xsl:variable name="prgPosName" select="concat('P', $curNum+1)"/>
				<xsl:variable name="tagNameUnique" select="KIRStrUtils:getTagName(concat($tagGroup,$tagName), 'DAT', $prgPosName)"/>
				<xsl:if test="$tagNameUnique">
					<xsl:variable name="dummy" select="NumberIncrement:next()"/>
				</xsl:if>
			</xsl:when>
			<xsl:when test="Target/@Default='Joint'">
				<NumberIncrement:counternum counter="1"/>
				<xsl:variable name="curNum" select="NumberIncrement:current()"/>
				<xsl:variable name="prgJntName" select="concat('J', $curNum+1)"/>
				<xsl:variable name="tagNameUnique" select="KIRStrUtils:getTagName(@id, 'DAT', $prgJntName)"/>
				<xsl:if test="$tagNameUnique">
					<xsl:variable name="dummy" select="NumberIncrement:next()"/>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
		<!-- if tool profile is TOOLNAME<number>, store the number -->
		<xsl:variable name="tool">
			<xsl:call-template name="toupper">
				<xsl:with-param name="instring" select="MotionAttributes/ToolProfile"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="starts-with($tool, $TOOLNAME)">
			<xsl:variable name="toolNumber" select="concat(':', number(substring-after($tool, $TOOLNAME)), ';')"/>
			<xsl:if test="$toolNumber != ':NaN;'">
				<xsl:variable name="toolNumberList" select="MotomanUtils:GetParameterData('ToolNumbersUsed')"/>
				<xsl:if test="contains($toolNumberList, $toolNumber) = false">
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ToolNumbersUsed', concat($toolNumberList, $toolNumber))"/>
				</xsl:if>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="outputTool">
		<xsl:param name="toolProfile"/>
		
		<xsl:variable name="storedTool" select="MotomanUtils:GetParameterData('STORED_Tool')"/>
		<!-- check if tool is the same as current tool (STORED_Tool) -->
		<xsl:if test="string($toolProfile) != string($storedTool)">
			<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Tool', $toolProfile)"/>
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:text>Tool </xsl:text>
			<xsl:choose>
				<xsl:when test="starts-with($toolProfile, 'PTOOLCONSTANT')">
					<xsl:text>(</xsl:text>
					<xsl:variable name="tool" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name=$toolProfile]"/>
					<xsl:call-template name="outputPosData">
						<xsl:with-param name="pos" select="$tool/TCPOffset/TCPPosition"/>
						<xsl:with-param name="ori" select="$tool/TCPOffset/TCPOrientation"/>
					</xsl:call-template>
					<xsl:text>)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="toolNum">
						<xsl:call-template name="getMitsubishiToolNumber">
							<xsl:with-param name="toolName" select="$toolProfile"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:value-of select="$TOOLNAME"/>
          <xsl:value-of select="format-number($toolNum, $numpat2)"/>
					<!-- store current tool and tools used -->
					<xsl:variable name="toolsUsed" select="MotomanUtils:GetParameterData('ToolsUsed')"/>
					<xsl:variable name="toolNameNum" select="concat($toolProfile, ',', $toolNum)"/>
					<xsl:if test="contains($toolsUsed, $toolNameNum) = false">
						<xsl:choose>
							<xsl:when test="string($toolsUsed) = ''">
								<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ToolsUsed', $toolNameNum)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('ToolsUsed', concat($toolsUsed, ';', $toolNameNum))"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$cr"/>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="getMitsubishiToolNumber">
		<xsl:param name="toolName"/>
		
		<xsl:variable name="tool">
			<xsl:call-template name="toupper">
				<xsl:with-param name="instring" select="$toolName"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="ptoolNum">
			<xsl:choose>
				<xsl:when test="starts-with($tool, $TOOLNAME) and string(number(substring-after($tool, $TOOLNAME))) != 'NaN'">
					<xsl:value-of select="number(substring-after($tool, $TOOLNAME))"/>
				</xsl:when>
				<xsl:otherwise>NaN</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="toolSeqNum">
			<xsl:call-template name="getToolNum">
				<xsl:with-param name="toolName" select="$toolName"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="toolSeqNumDelim" select="concat(':', $toolSeqNum, ';')"/>
		<xsl:variable name="toolNumbersUsed" select="MotomanUtils:GetParameterData('ToolNumbersUsed')"/>
		<xsl:choose>
			<xsl:when test="$ptoolNum != 'NaN'"> <!-- TOOLNAME<number> -->
				<xsl:value-of select="$ptoolNum"/>
			</xsl:when>
			<xsl:when test="contains($toolNumbersUsed, $toolSeqNumDelim) = false"> <!-- tool sequence number -->
				<xsl:value-of select="$toolSeqNum"/>
				<xsl:variable name="dummy"  select="MotomanUtils:SetParameterData('ToolNumbersUsed', concat($toolNumbersUsed, $toolSeqNumDelim))"/>
			</xsl:when>
			<xsl:otherwise> <!-- find an unused number starting from 1 -->
				<xsl:call-template name="getUnusedToolNum"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputAccuracy">
		<xsl:param name="accuracyProfile"/>
		
		<xsl:variable name="controllerAccuracyProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]"/>
		<xsl:variable name="flyByMode" select="$controllerAccuracyProfile/FlyByMode"/>
		<xsl:variable name="accuracyType" select="$controllerAccuracyProfile/AccuracyType"/>
		<xsl:variable name="accuracyValue" select="$controllerAccuracyProfile/AccuracyValue/@Value"/>
		<xsl:variable name="op" select="'Cnt:Options:'"/>
		
		<xsl:variable name="storedCnt" select="MotomanUtils:GetParameterData('STORED_Cnt')"/>
		<xsl:choose>
			<xsl:when test="$flyByMode = 'Off'">
				<xsl:if test="string($storedCnt) != '0'">
					<xsl:call-template name="OutputLineNumber"/>
					<xsl:text>Cnt 0</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Cnt', 0)"/>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$flyByMode = 'On'">
				<xsl:choose>
					<xsl:when test="$accuracyType = 'Distance'">
						<xsl:variable name="accuracy" select="concat('1, ', format-number($accuracyValue * 1000, $patInteger))"/>
						<xsl:if test="string($storedCnt) != string($accuracy) or AttributeList/Attribute[starts-with(AttributeValue, $op)] != ''">
							<xsl:call-template name="OutputLineNumber"/>
							<xsl:text>Cnt </xsl:text>
							<xsl:value-of select="$accuracy"/>
							<xsl:call-template name="processOptions">
								<xsl:with-param name="op" select="$op"/>
							</xsl:call-template>
							<xsl:value-of select="$cr"/>
							<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Cnt', $accuracy)"/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise> <!-- AccuracyType=Speed -->
						<xsl:choose>
							<xsl:when test="$accuracyValue = 0">
								<xsl:if test="string($storedCnt) != '0'">
									<xsl:call-template name="OutputLineNumber"/>
									<xsl:text>Cnt 0</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Cnt', 0)"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="$accuracyValue = 100">
								<xsl:if test="string($storedCnt) != '1'">
									<xsl:call-template name="OutputLineNumber"/>
									<xsl:text>Cnt 1</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Cnt', 1)"/>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<!-- give warning -->
								<xsl:text>VERSION INFO START</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>WARNING: Speed type Accuracy Profile &lt;</xsl:text>
								<xsl:value-of select="$accuracyProfile"/>
								<xsl:text>&gt; is not supported. FlyBy off(Cnt 0) is assumed.</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>VERSION INFO END</xsl:text>
								<xsl:if test="string($storedCnt) != '0'">
									<xsl:call-template name="OutputLineNumber"/>
									<xsl:text>Cnt 0</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Cnt', 0)"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template> <!-- outputAccuracy -->

	<xsl:template name="outputAccel">
		<xsl:param name="motionProfile"/>
		<xsl:variable name="controllerMotionProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]"/>
		<xsl:variable name="accelValue" select="format-number($controllerMotionProfile/Accel/@Value, $patInteger)"/>
		<xsl:variable name="storedAccel" select="MotomanUtils:GetParameterData('STORED_Accel')"/>
		<xsl:variable name="op" select="'Accel:Options:'"/>
		<xsl:choose>
			<xsl:when test="$accelValue != $storedAccel or AttributeList/Attribute[starts-with(AttributeValue, $op)] != ''">
				<xsl:call-template name="OutputLineNumber"/>
				<xsl:text>Accel </xsl:text>
				<xsl:value-of select="$accelValue"/>
				<xsl:call-template name="processOptions">
					<xsl:with-param name="op" select="$op"/>
				</xsl:call-template>
				<xsl:value-of select="$cr"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Accel', $accelValue)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputSpeed">
		<xsl:param name="motionProfile"/>
		<xsl:param name="motionType"/>
		
		<xsl:variable name="controllerMotionProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]"/>
		<xsl:variable name="speedValue" select="$controllerMotionProfile/Speed/@Value"/>
		<xsl:variable name="speedUnits" select="$controllerMotionProfile/Speed/@Units"/>
		<xsl:variable name="motionBasis" select="$controllerMotionProfile/MotionBasis"/>
		
		<xsl:if test="$motionBasis = 'Time' and $speedUnits = 's'">
			<!-- give warning -->
			<xsl:text>VERSION INFO START</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>WARNING: Time based motion profile &lt;</xsl:text>
			<xsl:value-of select="$motionProfile"/>
			<xsl:text>&gt; is not supported. 'Default' Motion Profile is used.</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>VERSION INFO END</xsl:text>
		</xsl:if>
		<!-- if MotionBasis is Time, switch to MotionProfile Default -->
		<xsl:variable name="moProfile">
			<xsl:choose>
				<xsl:when test="$motionBasis = 'Time' and $speedUnits = 's'">
					<xsl:text>Default</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$motionProfile"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- get new MotionProfile values -->
		<xsl:variable name="ctrlMotionProfile" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$moProfile]"/>
		<xsl:variable name="spdValue" select="$ctrlMotionProfile/Speed/@Value"/>
		<xsl:variable name="spdUnits" select="$ctrlMotionProfile/Speed/@Units"/>
		<xsl:variable name="moBasis" select="$ctrlMotionProfile/MotionBasis"/>
		
		<!-- user desired Ovrd output from Attribute
			 increase speed value by 100/$ovrd
		  -->
		<xsl:variable name="dummy" select="MotomanUtils:GetParameterData('STORED_Ovrd')"/>
		<xsl:variable name="ovrd">
			<xsl:choose>
				<xsl:when test="contains($dummy, ',')">
					<!-- Ovrd may have 3 parameters: "Ovrd 1, 2, 3" -->
					<xsl:value-of select="substring-before($dummy, ',')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$dummy"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$motionType = 'Linear'">
				<xsl:variable name="absoluteSpd">
					<xsl:choose>
						<xsl:when test="$moBasis = 'Absolute'">
							<xsl:value-of select="$spdValue * 100 div $ovrd"/>
						</xsl:when>
						<xsl:when test="$moBasis = 'Percent'">
							<xsl:value-of select="$spdValue div 100 * $maxSpeedValue * 100 div $ovrd"/>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$absoluteSpd &gt; $maxSpeedValue">
					<!-- give warning -->
					<xsl:text>VERSION INFO START</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:choose>
						<xsl:when test="$ovrd=100">
							<xsl:text>WARNING: The motion profile speed setting is greater than robot's maximum speed. Maximum speed is used.</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>WARNING: The motion profile speed setting combined with Override is greater than robot's maximum speed. Maximum speed is used.</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$cr"/>
					<xsl:text>VERSION INFO END</xsl:text>
				</xsl:if>
				<xsl:variable name="linSpd">
					<xsl:choose>
						<xsl:when test="$absoluteSpd &gt; $maxSpeedValue">
							<xsl:value-of select="format-number($maxSpeedValue * 1000, $patUptoD2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number($absoluteSpd * 1000, $patUptoD2)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
        <xsl:variable name="storedOvrdOutput" select="MotomanUtils:GetParameterData('Ovrd_Output')"/>
        <xsl:if test="$storedOvrdOutput = 0">
          <xsl:call-template name="OutputLineNumber"/>
          <xsl:text>Ovrd </xsl:text>
          <xsl:value-of select="$ovrd"/>
          <xsl:value-of select="$cr"/>
          <xsl:variable name="dummy" select="MotomanUtils:SetParameterData('Ovrd_Output', 1)"/>
        </xsl:if>
				<xsl:variable name="storedLinSpd" select="MotomanUtils:GetParameterData('STORED_LinSpd')"/>
				<xsl:if test="string($storedLinSpd) != string($linSpd)">
					<xsl:call-template name="OutputLineNumber"/>
					<xsl:text>Spd </xsl:text>
					<xsl:value-of select="$linSpd"/>
					<xsl:value-of select="$cr"/>
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_LinSpd', $linSpd)"/>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$motionType = 'Joint'">
				<xsl:variable name="percentSpd">
					<xsl:choose>
						<xsl:when test="$moBasis = 'Absolute'">
							<xsl:value-of select="$spdValue div $maxSpeedValue * 100 * 100 div $ovrd"/>
						</xsl:when>
						<xsl:when test="$moBasis = 'Percent'">
							<xsl:value-of select="$spdValue * 100 div $ovrd"/>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$percentSpd &gt; 100">
					<!-- give warning -->
					<xsl:text>VERSION INFO START</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:choose>
						<xsl:when test="$ovrd=100">
							<xsl:text>WARNING: The motion profile speed setting is greater than 100%. 100% speed is used.</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>WARNING: The motion profile speed setting combined with Override is greater than 100%. 100% speed is used.</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$cr"/>
					<xsl:text>VERSION INFO END</xsl:text>
				</xsl:if>
				<xsl:variable name="jntSpd">
					<xsl:choose>
						<xsl:when test="$percentSpd &gt; 100">100</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number($percentSpd, $patUptoD2)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="storedJntSpd" select="MotomanUtils:GetParameterData('STORED_JntSpd')"/>
				<xsl:if test="string($storedJntSpd) != string($jntSpd)">
					<xsl:call-template name="OutputLineNumber"/>
					<xsl:text>JOvrd </xsl:text>
					<xsl:value-of select="$jntSpd"/>
					<xsl:value-of select="$cr"/>
					<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_JntSpd', $jntSpd)"/>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
	</xsl:template> <!-- outputSpeed -->

	<xsl:template name="processOptions">
		<xsl:param name="op" select="'Options:'"/>
		
		<xsl:for-each select="AttributeList/Attribute">
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:choose>
				<xsl:when test="starts-with($attrvalue, $op)">
					<xsl:variable name="attrValueStripped" select="substring-after($attrvalue, $op)"/>
					<xsl:value-of select="$attrValueStripped"/>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	
	<!-- Template processComments downloads activity attributes -->
	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNode"/>

		<xsl:for-each select="$attributeListNode/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:if test="starts-with($attrname, $prefix)">
				<xsl:choose>
					<xsl:when test="starts-with($attrvalue, 'Robot Language:')">
						<xsl:call-template name="OutputLineNumber"/>
						<xsl:variable name="attrValueStripped" select="substring-after($attrvalue, 'Robot Language:')"/>
						<xsl:value-of select="$attrValueStripped"/>
						<xsl:variable name="ovrdAttr">
							<xsl:call-template name="toupper">
								<xsl:with-param name="instring" select="$attrValueStripped"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:if test="starts-with($ovrdAttr, 'OVRD ')">
							<xsl:variable name="ovrdVal" select="substring-after($ovrdAttr, 'OVRD ')"/>
							<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Ovrd', $ovrdVal)"/>
              <xsl:variable name="dummy" select="MotomanUtils:SetParameterData('Ovrd_Output', 1)"/>
            </xsl:if>
						<xsl:value-of select="$cr"/>
					</xsl:when>
					<!-- This is dealt with in processOptions -->
					<xsl:when test="starts-with($attrvalue, 'Options:')"/>
					<xsl:when test="starts-with($attrvalue, 'Cnt:Options:')"/>
					<xsl:when test="starts-with($attrvalue, 'Accel:Options:')"/>
					<xsl:otherwise>
						<xsl:call-template name="OutputLineNumber"/>
						<xsl:text>' </xsl:text>
						<xsl:value-of select="$attrvalue"/>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- Template Attribute downloads Comments/Robot Language as independent Operation -->
	<xsl:template match="Attribute">
		<xsl:variable name="attrname" select = "AttributeName"/>
		<xsl:variable name="attrvalue" select="AttributeValue"/>

		<xsl:if test="substring($attrname,1,7) = 'Comment'">
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:text>' </xsl:text>
			<xsl:value-of select="$attrvalue"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>

		<xsl:if test="substring($attrname,1,14) = 'Robot Language'">
			<xsl:call-template name="OutputLineNumber"/>
			<xsl:value-of select="$attrvalue"/>
			<xsl:variable name="ovrdAttr">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="$attrvalue"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="starts-with($ovrdAttr, 'OVRD ')">
				<xsl:variable name="ovrdVal" select="substring-after($ovrdAttr, 'OVRD ')"/>
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('STORED_Ovrd', $ovrdVal)"/>
        <xsl:variable name="dummy" select="MotomanUtils:SetParameterData('Ovrd_Output', 1)"/>
			</xsl:if>
			<xsl:value-of select="$cr"/>
		</xsl:if>

	</xsl:template>

	<xsl:template name="getToolNum">
		<xsl:param name="toolName"/>

		<xsl:variable name="toolProfiles" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile"/>
		<xsl:for-each select="$toolProfiles">
			<xsl:if test="./Name = $toolName">
				<xsl:value-of select="position()"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="getUnusedToolNum">
		<xsl:param name="startNum" select="1"/>
		
		<xsl:variable name="toolNumbersUsed" select="MotomanUtils:GetParameterData('ToolNumbersUsed')"/>
		<xsl:variable name="toolNumDelim" select="concat(':', $startNum, ';')"/>
		<xsl:choose>
			<xsl:when test="contains($toolNumbersUsed, $toolNumDelim) = false">
				<xsl:value-of select="$startNum"/>
				<xsl:variable name="dummy"  select="MotomanUtils:SetParameterData('ToolNumbersUsed', concat($toolNumbersUsed, $toolNumDelim))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="getUnusedToolNum">
					<xsl:with-param name="startNum" select="$startNum+1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputToolList">
		<xsl:param name="toolList"/>
		
		<xsl:choose>
			<xsl:when test="contains($toolList, ';')">
				<xsl:variable name="toolNameNum" select="substring-before($toolList, ';')"/>
				<xsl:variable name="toolName" select="substring-before($toolNameNum, ',')"/>
				<xsl:variable name="toolNum" select="substring-after($toolNameNum, ',')"/>
				<xsl:call-template name="outputToolData">
					<xsl:with-param name="toolName" select="$toolName"/>
					<xsl:with-param name="toolNum" select="$toolNum"/>
				</xsl:call-template>
				<xsl:call-template name="outputToolList">
					<xsl:with-param name="toolList" select="substring-after($toolList, ';')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="toolName" select="substring-before($toolList, ',')"/>
				<xsl:variable name="toolNum" select="substring-after($toolList, ',')"/>
				<xsl:call-template name="outputToolData">
					<xsl:with-param name="toolName" select="$toolName"/>
					<xsl:with-param name="toolNum" select="$toolNum"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputToolData">
		<xsl:param name="toolName"/>
		<xsl:param name="toolNum"/>
		
    <xsl:value-of select="$TOOLNAME"/>
    <xsl:value-of select="format-number($toolNum, $numpat2)"/>
		<xsl:text>=(</xsl:text>
		<xsl:variable name="tool" select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name=$toolName]"/>
		<xsl:call-template name="outputPosData">
			<xsl:with-param name="pos" select="$tool/TCPOffset/TCPPosition"/>
			<xsl:with-param name="ori" select="$tool/TCPOffset/TCPOrientation"/>
		</xsl:call-template>
		<xsl:text>)(0,0)</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	
	<xsl:template name="outputPosList">
		<xsl:variable name="robotMoves" select="Activity[@ActivityType='DNBRobotMotionActivity']"/>
		<xsl:for-each select="$robotMoves">
			<xsl:choose>
				<xsl:when test="Target/@Default='Cartesian' and Target/CartesianTarget/Tag">
					<xsl:variable name="tagGroup" select="Target/CartesianTarget/Tag/@TagGroup"/>
					<xsl:variable name="tagName" select="Target/CartesianTarget/Tag"/>
					<xsl:variable name="tagGroupName" select="concat($tagGroup,$tagName)"/>
					<xsl:if test="KIRStrUtils:tagUsed($tagGroupName)=false">
						<xsl:variable name="dummy" select="KIRStrUtils:addTag($tagGroupName)"/>
						<xsl:variable name="prgPosName" select="KIRStrUtils:getTagName($tagGroupName, '', '')"/>
						<xsl:value-of select="$prgPosName"/>
						<xsl:text>=(</xsl:text>
						<xsl:call-template name="outputPosData">
							<xsl:with-param name="pos" select="Target/CartesianTarget/Position"/>
							<xsl:with-param name="ori" select="Target/CartesianTarget/Orientation"/>
						</xsl:call-template>
						<xsl:call-template name="outputAuxData"/>
						<xsl:text>)</xsl:text>
						<xsl:call-template name="outputFlags"/>
						<xsl:value-of select="$cr"/>
					</xsl:if>
				</xsl:when>
				<xsl:when test="Target/@Default='Joint'">
					<xsl:variable name="prgJntName" select="KIRStrUtils:getTagName(@id, '', '')"/>
					<xsl:value-of select="$prgJntName"/>
					<xsl:text>=(</xsl:text>
					<xsl:call-template name="outputJntData"/>
					<xsl:call-template name="outputAuxData"/>
					<xsl:text>)</xsl:text>
					<xsl:value-of select="$cr"/>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template> <!-- outputPosList -->

	<xsl:template name="outputPosData">
		<xsl:param name="pos"/>
		<xsl:param name="ori"/>
		
		<xsl:variable name="x">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="y">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="z">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$pos/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="W">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="P">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="R">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$ori/@Roll"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="format-number($x * 1000, $patD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($y * 1000, $patD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($z * 1000, $patD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($W, $patD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($P, $patD2)"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="format-number($R, $patD2)"/>
	</xsl:template> <!-- outputPosData -->


	<xsl:template name="outputJntData">
		<xsl:for-each select="Target/JointTarget/Joint">
			<xsl:if test="$rbtAxes=5 and @DOFNumber=4">
				<xsl:text>, 0</xsl:text> <!-- fill joint 4 position with 0 -->
			</xsl:if>
			<xsl:call-template name="outputJntData1Joint"/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="outputAuxData">
		<xsl:choose>
			<xsl:when test="$dofAux=0"/>
			<xsl:when test="$dofAux=1">
				<xsl:call-template name="outputAuxData1Joint">
					<xsl:with-param name="auxDofNum" select="1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputAuxData1Joint">
					<xsl:with-param name="auxDofNum" select="1"/>
				</xsl:call-template>
				<xsl:call-template name="outputAuxData1Joint">
					<xsl:with-param name="auxDofNum" select="2"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="outputJntData1Joint">
		<xsl:if test="@DOFNumber &gt; 1">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:variable name="jval">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="JointValue"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="@Units = 'deg'">
				<xsl:value-of select="format-number($jval, $patD2)"/>
			</xsl:when>
			<xsl:when test="@Units = 'm'">
				<xsl:value-of select="format-number($jval * 1000, $patD2)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputAuxData1Joint">
		<xsl:param name="auxDofNum"/>
		<xsl:text>, </xsl:text>
		<xsl:variable name="auxJoint" select="Target/JointTarget/AuxJoint[@DOFNumber=$rbtAxes+$auxDofNum]"/>
		<xsl:variable name="jval">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$auxJoint/JointValue"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$auxJoint/@Units = 'deg'">
				<xsl:value-of select="format-number($jval, $patD2)"/>
			</xsl:when>
			<xsl:when test="$auxJoint/@Units = 'm'">
				<xsl:value-of select="format-number($jval * 1000, $patD2)"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="outputFlags">
		<xsl:text>(</xsl:text>
		<xsl:variable name="cfg" select="Target/CartesianTarget/Config/@Name"/>
		<xsl:choose>
			<xsl:when test="$rbtAxes=4">
				<xsl:choose>
					<xsl:when test="$cfg='Config_1'">0</xsl:when> <!-- 000 Left -->
					<xsl:when test="$cfg='Config_2'">4</xsl:when> <!-- 100 Right -->
					<xsl:otherwise>
						<xsl:value-of select="$cfg"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- 6-axis robots have 8 configurations;
					 5-axis robots have 4 (1, 3, 5, 7)
				-->
				<xsl:choose>
					<xsl:when test="$cfg='Config_1'">7</xsl:when>
					<xsl:when test="$cfg='Config_2'">6</xsl:when>
					<xsl:when test="$cfg='Config_3'">5</xsl:when>
					<xsl:when test="$cfg='Config_4'">4</xsl:when>
					<xsl:when test="$cfg='Config_5'">1</xsl:when>
					<xsl:when test="$cfg='Config_6'">0</xsl:when>
					<xsl:when test="$cfg='Config_7'">3</xsl:when>
					<xsl:when test="$cfg='Config_8'">2</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$cfg"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<xsl:variable name="jTgt" select="Target/JointTarget"/>
		<xsl:variable name="FL2b1">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/Joint[@DOFNumber=1]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2b2">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/Joint[@DOFNumber=2]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2b3">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/Joint[@DOFNumber=3]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2b4">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/Joint[@DOFNumber=4]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2b5">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/Joint[@DOFNumber=5]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2b6">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/Joint[@DOFNumber=6]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2b7">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/AuxJoint[@DOFNumber=$rbtAxes+1]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2b8">
			<xsl:call-template name="outputFlag2">
				<xsl:with-param name="joint" select="$jTgt/AuxJoint[@DOFNumber=$rbtAxes+2]"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="FL2" select="concat($FL2b8,$FL2b7,$FL2b6,$FL2b5,$FL2b4,$FL2b3,$FL2b2,$FL2b1)"/>
		<xsl:choose>
			<xsl:when test="number($FL2) &lt; 10">
				<xsl:value-of select="number($FL2)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$ampersand"/>
				<xsl:text>H</xsl:text>
				<xsl:value-of select="$FL2"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template name="outputFlag2">
		<xsl:param name="joint"/>
		<xsl:choose>
			<xsl:when test="$joint"> <!-- has the Joint or AuxJoint -->
				<xsl:choose>
					<xsl:when test="$joint/@JointType='Rotational'">
						<xsl:variable name="jVal">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num" select="$joint/JointValue"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$jVal &gt; 180">1</xsl:when>
							<xsl:when test="$jVal &lt; -180">F</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise> <!-- Translational -->
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise> <!-- no joint -->
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'e')"/>
					<xsl:with-param name="e" select="substring-after($Num,'e')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'E')"/>
					<xsl:with-param name="e" select="substring-after($Num,'E')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="OutputLineNumber">
		<NumberIncrement:counternum counter="2"/>
		<xsl:value-of select="NumberIncrement:next()"/>
		<xsl:text> </xsl:text>
	</xsl:template>
	
	<xsl:template name="TrimTrailing_1">
		<xsl:param name="instring"/>
		<xsl:variable name="length" select="string-length($instring)"/>
		<xsl:choose>
			<xsl:when test="number($length) &gt; 2 and substring($instring, number($length)-1, 2) = '_1'">
				<xsl:call-template name="TrimTrailing_1">
					<xsl:with-param name="instring" select="substring($instring, 1, number($length)-2)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$instring"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="toupper">
		<xsl:param name="instring"/>
		<xsl:value-of select="translate($instring, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	</xsl:template>

</xsl:stylesheet>

