<?xml version="1.0" encoding="UTF-8"?>
<!-- COPYRIGHT DELMIA CORP. 2003-2005 -->
<!-- 2007/08/16 promotion for IR status -->
<!-- 13 Nov 2003 creation -->
<!-- IMPORTANT USAGE INFORMATION, PLEASE READ:
	This xslt stylesheet should be used as a starting point to create Delmia V5 OLP downloaders. This stylesheet is not a translator,
	since it doesn't output any of the values retrieved from processed XML elements. The task of a developer who will be using this 
	stylesheet is to create the output based on the syntax of a target robot language. For that purpose, xsl:text and xsl:value-of elements
	can be used, as well as the functions accessible through java xslt extensions. 
	Every line of source code has its matching comment. Please read the comments, since they often contain very important implementation
	information. 
	Some xsl variables have been set to contain default values. Comments above those variables will inform the developer that those 
	variable contents are expected to be changed. The effort has been made to create a very generic stylesheet that would require just 
	additional creation of output statements, while the structure of the file would stay unchanged. In case of translators that would need 
	development of additional templates, possibly to deal with issues not addressed here, it is developer's responsibility to provide them. 
	Also, if the content of this stylesheet needs to be changed, beyond the point of just adding output statements, it is up to the developer 
	to provide that modified xslt support. 
	This stylesheet can be extended either by declaring and referencing built-in java extensions in Xalan or by declaring and referencing 
	custom made java extension through their elements and functions. XSLT extension related literature is quite abundant and easy to find, primarily 
	through various www resources, and some of those resources are referenced in this stylesheet. -->
<!-- To see how to create and use java extensions within an xslt file, read in full the following references:
	http://xml.apache.org/xalan-j/extensions.html
	http://xml.apache.org/xalan-j/extensionslib.html
	http://xml.apache.org/xalan-j/extensions_xsltc.html -->
<!-- Declare the extensions within stylesheet element -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lxslt="http://xml.apache.org/xslt" xmlns:java="http://xml.apache.org/xslt/java"
	xmlns:math="http://exslt.org/math" xmlns:NumberIncrement="NumberIncrement"
	xmlns:MatrixUtils="MatrixUtils" xmlns:MotomanUtils="MotomanUtils" xmlns:FileUtils="FileUtils"
	xmlns:BitMaskUtils="BitMaskUtils"
	extension-element-prefixes="NumberIncrement  MotomanUtils MatrixUtils FileUtils BitMaskUtils math">
	<lxslt:component prefix="NumberIncrement" functions="next" elements="startnum increment reset">
		<lxslt:script lang="javaclass"
			src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MotomanUtils"
		functions="JointToEncoderConversion SetParameterData GetParameterData" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"
		/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils"
		functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll dgMatrixToQuaternions dgGetQ1 dgGetQ2 dgGetQ3 dgGetQ4 quaternionToYPR"
		elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"
		/>
	</lxslt:component>
	<lxslt:component prefix="BitMaskUtils" functions="bitAND bitOR bitShiftLeft bitShiftRight"
		elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.BitMaskUtils"
		/>
	</lxslt:component>
	<lxslt:component prefix="FileUtils"
		functions="fileOpen fileWrite fileClose fileOpenRead fileOpenClose fileLineRead" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.FileUtils"/>
	</lxslt:component>
	<!-- Do not preserve whitespace-only text nodes for all the elements of the source XML document -->
	<xsl:strip-space elements="*"/>
	<!-- Indicates the output method for the result document. Text output method simply outputs all the result document's
		text nodes without modification -->
	<xsl:output method="text"/>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!-- HEADER INFORMATION -->
	<!-- You may modify the content of the select attribute, and type in the stylesheet version information relevant for your company -->
	<xsl:variable name="version" select="'DELMIA CORP. RAPID  DOWNLOADER VERSION 5 RELEASE 26 SP1'"/>
	<!-- You may modify the content of the select attribute, and type in the copyright information relevant for your company -->
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 2003-2012, ALL RIGHTS RESERVED'"/>
	<!-- In general this shouldn't be modified, but if the meaning will stay the same, the wording can change -->
	<xsl:variable name="saveInfo"
		select="'If you decide to save this file, VERSION INFO header will not be taken into account.'"/>
	<!-- PRESENTATION PATTERNS-->
	<!-- Carriage Return character -->
	<xsl:variable name="cr" select="'&#xa;'"/>
	<!-- Decimal number format. For more information refer to java.text.DecimalFormat class documentation, in Java API Specification -->
	<xsl:variable name="decimalNumberPattern" select="'#.###'"/>
	<xsl:variable name="targetNumberPattern" select="'#.######'"/>
	<!-- GENERAL INFORMATION ABOUT THE ROBOT AND DOWNLOADING TECHNIQUE -->
	<!-- Name of the robot which owns the downloaded task -->
	<xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<!-- Robot's maximum allowed speed -->
	<xsl:variable name="maxSpeed" select="/OLPData/Resource/GeneralInfo/MaxSpeed"/>
	<!-- Number of robot's base axes, excluding any type of external axes that can be detached from the robot -->
	<xsl:variable name="numberOfRobotAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<!-- Number of robot's rail and/or track axes -->
	<xsl:variable name="numberOfAuxiliaryAxes"
		select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<!-- Number of robot's end of arm tooling axes -->
	<xsl:variable name="numberOfExternalAxes"
		select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<!-- Number of workpiece positioner axes, that are controlled by the robot controller -->
	<xsl:variable name="numberOfPositionerAxes"
		select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="worldCoords" select="MotomanUtils:GetParameterData(string('WorldCoords'))"/>
	<!-- Variable which value defines whether tag targets are generated with respect to the robot's base or with resptect to 
		the bases of the parts to which the tags are attached to -->
	<xsl:variable name="downloadCoordinates"
		select="/OLPData/Resource/GeneralInfo/DownloadCoordinates"/>
	<!-- Main task name is the name of the task that has been selected for download -->
	<xsl:variable name="maingroup0"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Maingroup' or ParameterName='MainGroup']/ParameterValue"/>
	<xsl:variable name="maingroup">
		<xsl:choose>
			<xsl:when test="string($maingroup0) != ''">
				<xsl:value-of select="$maingroup0"/>
			</xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="railgroup0">
		<xsl:if test="$numberOfAuxiliaryAxes &gt; 0">
			<xsl:value-of
				select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Railgroup' or ParameterName='RailGroup']/ParameterValue"
			/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="railgroup">
		<xsl:choose>
			<xsl:when test="string($railgroup0) != ''">
				<xsl:value-of select="$railgroup0"/>
			</xsl:when>
			<xsl:when test="$numberOfAuxiliaryAxes &gt; 0">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="toolgroup0">
		<xsl:if test="$numberOfExternalAxes &gt; 0">
			<xsl:value-of
				select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Toolgroup' or ParameterName='ToolGroup']/ParameterValue"
			/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="toolgroup">
		<xsl:choose>
			<xsl:when test="string($toolgroup0) != ''">
				<xsl:value-of select="$toolgroup0"/>
			</xsl:when>
			<xsl:when test="$numberOfExternalAxes &gt; 0">2</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="workgroup0">
		<xsl:if test="$numberOfPositionerAxes &gt; 0">
			<xsl:value-of
				select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Workgroup' or ParameterName='WorkGroup']/ParameterValue"
			/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="workgroup">
		<xsl:choose>
			<xsl:when test="string($workgroup0) != ''">
				<xsl:value-of select="$workgroup0"/>
			</xsl:when>
			<xsl:when test="$numberOfPositionerAxes &gt; 0">3</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="wobjOffset" select="MotomanUtils:GetParameterData(string('WobjOffset'))"/>
	<xsl:variable name="mainTaskName"
		select="translate(/OLPData/Resource/ActivityList[1]/@Task, '.', '')"/>
	<xsl:variable name="moduleName">
		<xsl:choose>
			<xsl:when test="contains($mainTaskName,'#')">
				<xsl:value-of select="substring-after($mainTaskName,'#')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($mainTaskName, '_mod')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="rapidVersion" select="MotomanUtils:GetParameterData('Version')"/>
	<!-- for auxiliary axes spees v_leax (linear) and v_reax (rotational) -->
	<xsl:variable name="max_v_leax"
	select="MotomanUtils:GetParameterData('Max_v_leax')"/>
	<xsl:variable name="max_v_reax"
		select="MotomanUtils:GetParameterData('Max_v_reax')"/>	
	
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES END~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TEMPLATE DEFINITIONS START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!--~~~~~ROOT TEMPLATE~~~~~ -->
	<!-- This template is called first, automatically, by XSLT processor -->
	<xsl:template match="/">
		<!-- Create header information -->
		<xsl:call-template name="HeaderInfo"/>
		<!-- Initialize parameter data -->
		<xsl:call-template name="InitializeParameters"/>
		<!-- DLY ADDED CURRENT PROCEDURE NAME -->
		<xsl:choose>
			<xsl:when test="contains($mainTaskName,'#')">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData( 'CURRENT_PROCEDURE_NAME', 'MODULE_PERS_ONLY' )"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData( 'CURRENT_PROCEDURE_NAME', 'MODULE_ALL_TARGETS' )"
				/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- This is optional. Call this template if you need to create target declaration section for downloaded program -->
		<xsl:call-template name="CreateTargetDeclarationSection"/>
		<!-- RAPID DLY ADDED CreateControllerDeclarationSection -->
		<xsl:call-template name="CreateControllerDeclarationSection"/>
		<!-- Process all the ActivityList nodes in order they appear in the source XML file -->
		<xsl:apply-templates select="OLPData/Resource/ActivityList"/>
		<xsl:text>ENDMODULE</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END</xsl:text>
	</xsl:template>
	<!--~~~~~HEADER CREATOR TEMPLATE~~~~~ -->
	<!-- This template will not be called automatically by the processor. It is explicitly called from the root template.
		It creates version and copyright information blocks in-between VERSION INFO START and VERSION INFO END
		statements -->
	<xsl:template name="HeaderInfo">
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO START</xsl:text>
		<!-- Print carriage-return character to the result document -->
		<xsl:value-of select="$cr"/>
		<!-- Prints the version information to the result document -->
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the copyright information to the result document -->
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<!-- Prints the save information to the result document -->
		<xsl:value-of select="$saveInfo"/>
		<xsl:value-of select="$cr"/>
		<!-- Please do not modify this string, since it is internally used by V5 -->
		<xsl:text>VERSION INFO END</xsl:text>
		<!-- set program file name -->
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE START </xsl:text>
		<xsl:value-of select="$moduleName"/>
		<xsl:text>.mod</xsl:text>
		<!-- RAPID OUTPUT FILE HEADER HERE -->
		<xsl:value-of select="$cr"/>
		<xsl:text>%%%</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>  VERSION:1</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>  LANGUAGE:ENGLISH</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>%%%</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>MODULE </xsl:text>
		<xsl:value-of select="$moduleName"/>
		<xsl:value-of select="$cr"/>
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:reset/>
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:reset/>
		<NumberIncrement:counternum counter="0"/>
	</xsl:template>
	<!-- end of HeaderInfo -->
	<!-- ~~~~~PARAMETER DATA INITIALIZER TEMPLATE~~~~~ -->
	<!-- In order to make all the parameter data global, this template first stores all the parameter name / value pairs in java.
		After this template is processed all the parameter values will be accessible throughout this stylesheet. This template
		will not be called automatically by the processor. It is explicitly called from the root template. -->
	<xsl:template name="InitializeParameters">
		<!-- For each resource parameter defined in V5, get it's name and value, and store it in java extension -->
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<!-- Store parameter name -->
			<xsl:variable name="paramName" select="ParameterName"/>
			<!-- Store parameter value -->
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<!-- Store all the parameter name/value pairs in java -->
			<xsl:variable name="hr"
				select="MotomanUtils:SetParameterData( string($paramName), string($paramValue) )"/>
			<!-- IMPORTANT: In order to retrieve parameter value, from anywhere in the stylesheet use:
				String MotomanUtils:GetParameterData(String) function. For example, this will work:
				<xsl:variable name="result" select="MotomanUtils:GetParameterData('MyParameter1')"/>
				Pass the parameter name as a string, and the return value will be either 
				the parameter value (result, in the example above) returned as a string, or an empty string if the parameter
				 name does not exist in V5 for the downloading resource -->
		</xsl:for-each>
		<xsl:for-each
			select="OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity']">
			<xsl:variable name="toolProfile"
				select="translate(MotionAttributes/ToolProfile, '.', '_')"/>
			<xsl:variable name="usedTool" select="concat('UsedToolProfile', $toolProfile)"/>
			<xsl:variable name="hr" select="MotomanUtils:SetParameterData($usedTool, '1')"/>
			<xsl:variable name="objectFrameProfile" select="MotionAttributes/ObjectFrameProfile"/>
			<xsl:variable name="objectFrameProfileReplaced"
				select="MotionAttributes/ObjectFrameProfile/@Replaced"/>
			<xsl:choose>
				<xsl:when test="string($objectFrameProfileReplaced) = ''">
					<xsl:variable name="usedObjFrame"
						select="concat('UsedObjectFrameProfile', $objectFrameProfile)"/>
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData($usedObjFrame, '1')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="usedObjFrame"
						select="concat('UsedObjectFrameProfile', $objectFrameProfileReplaced)"/>
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData($usedObjFrame, '1')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<xsl:for-each
			select="OLPData/Resource/ActivityList/Activity[@ActivityType = 'FollowPathActivity']">
			<xsl:variable name="toolProfile" select="PathMotionAttributes/PathToolProfile"/>
			<xsl:variable name="usedTool" select="concat('UsedToolProfile', $toolProfile)"/>
			<xsl:variable name="hr" select="MotomanUtils:SetParameterData($usedTool, '1')"/>
			<xsl:variable name="objectFrameProfile"
				select="PathMotionAttributes/PathObjectFrameProfile"/>
			<xsl:variable name="usedObjFrame"
				select="concat('UsedObjectFrameProfile', $objectFrameProfile)"/>
			<xsl:variable name="hr" select="MotomanUtils:SetParameterData($usedObjFrame, '1')"/>
		</xsl:for-each>
	</xsl:template>
	<!-- end of InitializeParameters -->
	<!-- ~~~~~TARGET DECLARATION SECTION CREATOR TEMPLATE~~~~~ -->
	<!-- This template should only be called if target declaration section needs to be created in the output document. By default, in this 
		stylesheet it is always called from the root template. You can comment out that call-template element if you do not need to 
		create target declaration section in the output document -->
	<xsl:template name="CreateTargetDeclarationSection">
		<!-- For each robot motion and follow path activity in the source XML file -->
		<xsl:for-each
			select="/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBRobotMotionActivity'] | 
							 /OLPData/Resource/ActivityList/Activity[@ActivityType = 'FollowPathActivity'] ">
			<!-- Call template TransformRobotMotionActivityData with mode parameter set to 'TargetDeclarationSection' -->
			<xsl:if test="@ActivityType = 'DNBRobotMotionActivity' ">
				<xsl:call-template name="TransformRobotMotionActivityData">
					<!-- Send the currect node, as the first parameter -->
					<xsl:with-param name="motionActivityNode" select="."/>
					<!-- Send the mode string, as the second parameter -->
					<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
				</xsl:call-template>
			</xsl:if>
			<!-- Call template TransformFollowPathActivityData with mode parameter set to 'TargetDeclarationSection' -->
			<xsl:if test="@ActivityType = 'FollowPathActivity' ">
				<xsl:call-template name="TransformFollowPathActivityData">
					<!-- Send the currect node, as the first parameter -->
					<xsl:with-param name="followPathActivityNode" select="."/>
					<!-- Send the mode string, as the second parameter -->
					<xsl:with-param name="mode" select=" 'TargetDeclarationSection' "/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end of CreateTargetDeclarationSection -->
	<!-- RAPID DLY - ADDED CONTROLLER DECLARATION -->
	<!-- ~~~~~CONTROLLER DECLARATION SECTION CREATOR TEMPLATE~~~~~ -->
	<!-- This template should only be called if controller declaration section needs to be created in the output document. By default,
	     in this stylesheet it is always called from the root template. You can comment out that call-template element if you do not
	      need to create controller declaration section in the output document -->
	<xsl:template name="CreateControllerDeclarationSection">
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:reset/>
		<xsl:variable name="profiledata_output"
			select="MotomanUtils:GetParameterData(string('ProfiledataOutput'))"/>
		<xsl:variable name="tooldata_output"
			select="MotomanUtils:GetParameterData(string('TooldataOutput'))"/>
		<xsl:variable name="wobjdata_output"
			select="MotomanUtils:GetParameterData(string('WobjdataOutput'))"/>
		<xsl:variable name="speeddata_output"
			select="MotomanUtils:GetParameterData(string('SpeeddataOutput'))"/>
		<xsl:variable name="zonedata_output"
			select="MotomanUtils:GetParameterData(string('ZonedataOutput'))"/>
		<xsl:variable name="spot_gundata_output"
			select="MotomanUtils:GetParameterData(string('SpotdataGundataOutput'))"/>
		<xsl:variable name="mocParamFileName"
			select="MotomanUtils:GetParameterData(string('MOC_ConfigFileName'))"/>
		<xsl:variable name="mocConfigFileName">
			<xsl:choose>
				<xsl:when test="string($mocParamFileName) != ''">
					<xsl:value-of select="$mocParamFileName"/>
				</xsl:when>
				<xsl:otherwise>MOC.cfg</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="basePositionX"
			select="MotomanUtils:GetParameterData(string('Base PositionX'))"/>
		<xsl:variable name="basePositionY"
			select="MotomanUtils:GetParameterData(string('Base PositionY'))"/>
		<xsl:variable name="basePositionZ"
			select="MotomanUtils:GetParameterData(string('Base PositionZ'))"/>
		<xsl:variable name="baseOrientationYaw"
			select="MotomanUtils:GetParameterData(string('Base Orientation Yaw'))"/>
		<xsl:variable name="baseOrientationPitch"
			select="MotomanUtils:GetParameterData(string('Base Orientation Pitch'))"/>
		<xsl:variable name="baseOrientationRoll"
			select="MotomanUtils:GetParameterData(string('Base Orientation Roll'))"/>
		<!-- For each toolprofile in the source XML file -->
		<xsl:if
			test="string($tooldata_output) != 'false' and string($profiledata_output) != 'false'">
			<xsl:for-each select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile">
				<xsl:variable name="toolName" select="translate(Name, '.', '_')"/>
				<xsl:variable name="toolType" select="ToolType"/>
				<xsl:variable name="toolMass">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Mass/@MassValue"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="centroidX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Centroid/CentroidPosition/@Cx"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="centroidY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Centroid/CentroidPosition/@Cy"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="centroidZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Centroid/CentroidPosition/@Cz"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="inertiaXX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Inertia/@Ixx"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="inertiaXY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Inertia/@Ixy"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="inertiaYY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Inertia/@Iyy"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="inertiaYZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Inertia/@Iyz"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="inertiazx">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Inertia/@Izx"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="inertiaZZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Inertia/@Izz"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP location offset from robot's mount plate in X direction -->
				<xsl:variable name="tcpPositionX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@X"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP location offset from robot's mount plate in Y direction -->
				<xsl:variable name="tcpPositionY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Y"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP location offset from robot's mount plate in Z direction -->
				<xsl:variable name="tcpPositionZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPPosition/@Z"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
				<xsl:variable name="tcpOrientationYaw">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Yaw"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
				<xsl:variable name="tcpOrientationPitch">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Pitch"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- TCP orientation offset from robot's mount plate in Roll direction -->
				<xsl:variable name="tcpOrientationRoll">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="TCPOffset/TCPOrientation/@Roll"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="xyzwpr"
					select="concat($tcpPositionX, ',',  $tcpPositionY,  ',' , $tcpPositionZ , ',' , $tcpOrientationYaw , ',' , $tcpOrientationPitch , ',' , $tcpOrientationRoll )"/>
				<xsl:variable name="matresult" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
				<xsl:variable name="quatresult" select="MatrixUtils:dgMatrixToQuaternions()"/>
				<xsl:variable name="checkToolUse" select="concat('UsedToolProfile', $toolName)"/>
				<xsl:variable name="toolUsed" select="MotomanUtils:GetParameterData($checkToolUse)"/>
				<xsl:if test="$toolName != 'tool0' and $toolName != 'Default' and $toolUsed = '1'">
					<xsl:text>  PERS tooldata </xsl:text>
					<xsl:choose>
						<xsl:when test="contains($toolName,',')">
							<xsl:value-of select="substring-before($toolName,',')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$toolName"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>:=[</xsl:text>
					<xsl:choose>
						<xsl:when test="$toolType = 'OnRobot'">
							<xsl:text>TRUE,[[</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>FALSE,[[</xsl:text>
							<xsl:choose>
								<xsl:when test="$worldCoords = '1' or $worldCoords = 'true'"> </xsl:when>
								<xsl:otherwise>
									<xsl:variable name="xyzwpr"
										select="concat($basePositionX, ',',  $basePositionY,  ',' , $basePositionZ , ',' , $baseOrientationYaw , ',' , $baseOrientationPitch , ',' , $baseOrientationRoll )"/>
									<xsl:variable name="matresult"
										select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
									<xsl:variable name="invresult" select="MatrixUtils:dgInvert()"/>
									<xsl:variable name="xyzwpr2"
										select="concat($tcpPositionX, ',',  $tcpPositionY,  ',' , $tcpPositionZ , ',' , $tcpOrientationYaw , ',' , $tcpOrientationPitch , ',' , $tcpOrientationRoll )"/>
									<xsl:variable name="tcp_robotbase"
										select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr2)"/>
									<xsl:variable name="result"
										select="MotomanUtils:SetParameterData('trackBasePositionX', '0.0')"/>
									<xsl:variable name="result"
										select="MotomanUtils:SetParameterData('trackBasePositionY','0.0')"/>
									<xsl:variable name="result"
										select="MotomanUtils:SetParameterData('trackBasePositionZ', '0.0')"/>
									<xsl:variable name="result"
										select="MotomanUtils:SetParameterData('trackBasePositionQ1', '1.0')"/>
									<xsl:variable name="result"
										select="MotomanUtils:SetParameterData('trackBasePositionQ2', '0.0')"/>
									<xsl:variable name="result"
										select="MotomanUtils:SetParameterData('trackBasePositionQ3', '0.0')"/>
									<xsl:variable name="result"
										select="MotomanUtils:SetParameterData('trackBasePositionQ4','0.0')"/>
									<xsl:if test="string($mocConfigFileName) != ''">
										<xsl:variable name="mocResult"
											select="FileUtils:fileOpenRead($mocConfigFileName)"/>
										<xsl:if test="$mocResult = 'Okay'">
											<xsl:call-template name="readMoc">
												<xsl:with-param name="line"
												select="FileUtils:fileLineRead()"/>
											</xsl:call-template>
											<xsl:variable name="closeFile"
												select="FileUtils:fileCloseRead()"/>
										</xsl:if>
									</xsl:if>
									<xsl:variable name="trackBasePositionX"
										select="MotomanUtils:GetParameterData('trackBasePositionX')"/>
									<xsl:variable name="trackAuxJoint"
										select="MotomanUtils:GetParameterData('Base PositionTrack')"/>
									<xsl:variable name="adjustedTrackBasePositionX"
										select="number($trackBasePositionX) + number($trackAuxJoint)"/>
									<xsl:variable name="trackBasePositionY"
										select="MotomanUtils:GetParameterData('trackBasePositionY')"/>
									<xsl:variable name="trackBasePositionZ"
										select="MotomanUtils:GetParameterData('trackBasePositionZ')"/>
									<xsl:variable name="trackBasePositionQ1"
										select="MotomanUtils:GetParameterData('trackBasePositionQ1')"/>
									<xsl:variable name="trackBasePositionQ2"
										select="MotomanUtils:GetParameterData('trackBasePositionQ2')"/>
									<xsl:variable name="trackBasePositionQ3"
										select="MotomanUtils:GetParameterData('trackBasePositionQ3')"/>
									<xsl:variable name="trackBasePositionQ4"
										select="MotomanUtils:GetParameterData('trackBasePositionQ4')"/>
									<xsl:variable name="trackBaseQuaternions"
										select="concat($trackBasePositionQ1, ',',  $trackBasePositionQ2,  ',' , $trackBasePositionQ3 , ',' , $trackBasePositionQ4)"/>
									<xsl:variable name="trackBaseYPR"
										select="MatrixUtils:quaternionToYPR($trackBaseQuaternions)"/>
									<xsl:variable name="trackbase"
										select="concat($adjustedTrackBasePositionX, ',',  $trackBasePositionY,  ',' , $trackBasePositionZ , ',' , $trackBaseYPR)"/>
									<!-- put  Robot base with respect to Tbase from MOC.cfg on stack -->
									<xsl:variable name="matresult2"
										select="MatrixUtils:dgXyzyprToMatrix($trackbase)"/>
									<!--xsl:value-of select="$cr"/>
											<xsl:text>ROBOT BASE/TRACK BASE:</xsl:text>
											<xsl:value-of select="$matresult2"/>
											<xsl:value-of select="$cr"/-->
									<!-- concat tcp with respect to Robot base from above -->
									<xsl:variable name="matresult3"
										select="MatrixUtils:dgCatXyzyprMatrix(substring-after($tcp_robotbase, 'result:'))"/>
									<!-- xsl:text>TOOL/TRACK BASE</xsl:text>
											<xsl:value-of select="$matresult3"/>
											<xsl:value-of select="$cr"/-->
									<xsl:variable name="quatresult"
										select="MatrixUtils:dgMatrixToQuaternions()"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
					<!-- TCP values -->
					<xsl:variable name="tcpResultX">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tcpResultY">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tcpResultZ">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<!-- TCP quaternions -->
					<xsl:variable name="tcpQuaternion1">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ1()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tcpQuaternion2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ2()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tcpQuaternion3">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ3()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="tcpQuaternion4">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ4()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:value-of select="format-number($tcpResultX,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($tcpResultY,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($tcpResultZ,$decimalNumberPattern)"/>
					<xsl:text>],[</xsl:text>
					<xsl:value-of select="format-number($tcpQuaternion1,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($tcpQuaternion2,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($tcpQuaternion3,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($tcpQuaternion4,$targetNumberPattern)"/>
					<xsl:text>]],[</xsl:text>
					<xsl:value-of select="format-number($toolMass,$targetNumberPattern)"/>
					<xsl:text>,[</xsl:text>
					<xsl:value-of select="format-number($centroidX,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($centroidY,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($centroidZ,$decimalNumberPattern)"/>
					<xsl:text>],[1,0,0,0],</xsl:text>
					<xsl:value-of select="format-number($inertiaXX,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($inertiaYY,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($inertiaZZ,$decimalNumberPattern)"/>
					<xsl:text>]];</xsl:text>
					<xsl:value-of select="$cr"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
		<xsl:variable name="objFrameProfileList"
			select="/OLPData/Resource/Controller/ObjectFrameProfileList"/>
		<xsl:if
			test="string($wobjdata_output) != 'false' and string($profiledata_output) != 'false'">
			<xsl:for-each select="$objFrameProfileList/ObjectFrameProfile">
				<xsl:variable name="objectName" select="Name"/>
				<xsl:variable name="objectNameReplaced" select="@Replaced"/>
				<!-- ObjectFrame location offset from robot's mount plate in X direction -->
				<xsl:variable name="objectPositionX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@X"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame location offset from robot's mount plate in Y direction -->
				<xsl:variable name="objectPositionY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@Y"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame location offset from robot's mount plate in Z direction -->
				<xsl:variable name="objectPositionZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="ObjectFrame/ObjectFramePosition/@Z"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame orientation offset from robot's mount plate in Yaw  direction -->
				<xsl:variable name="objectOrientationYaw">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="ObjectFrame/ObjectFrameOrientation/@Yaw"
						/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame orientation offset from robot's mount plate in Pitch  direction -->
				<xsl:variable name="objectOrientationPitch">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="ObjectFrame/ObjectFrameOrientation/@Pitch"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame orientation offset from robot's mount plate in Roll  direction -->
				<xsl:variable name="objectOrientationRoll">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="ObjectFrame/ObjectFrameOrientation/@Roll"
						/>
					</xsl:call-template>
				</xsl:variable>
				<!-- get ObjectFrameProfile_uframe coords, invert Matrix -->
				<xsl:variable name="underscore_num" select="substring-after($objectName, '_')"/>
				<xsl:variable name="obj_uframe">
					<xsl:choose>
						<xsl:when test="contains($underscore_num,'1')">
							<xsl:value-of
								select="concat(substring-before($objectName,'_1'), '_uframe_1', substring-after($underscore_num,'1'))"
							/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat($objectName, '_uframe')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="obj_uframeProfile"
					select="$objFrameProfileList/ObjectFrameProfile[Name=$obj_uframe]"/>
				<xsl:variable name="uframeX">
					<xsl:choose>
						<xsl:when test="string($obj_uframeProfile)!=''">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num"
									select="$obj_uframeProfile/ObjectFrame/ObjectFramePosition/@X"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectPositionX"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="uframeY">
					<xsl:choose>
						<xsl:when test="string($obj_uframeProfile)!=''">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num"
									select="$obj_uframeProfile/ObjectFrame/ObjectFramePosition/@Y"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectPositionY"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="uframeZ">
					<xsl:choose>
						<xsl:when test="string($obj_uframeProfile)!=''">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num"
									select="$obj_uframeProfile/ObjectFrame/ObjectFramePosition/@Z"/>
								<xsl:with-param name="Units" select="1000.0"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectPositionZ"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="uframeYaw">
					<xsl:choose>
						<xsl:when test="string($obj_uframeProfile)!=''">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num"
									select="$obj_uframeProfile/ObjectFrame/ObjectFrameOrientation/@Yaw"
								/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectOrientationYaw"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="uframePitch">
					<xsl:choose>
						<xsl:when test="string($obj_uframeProfile)!=''">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num"
									select="$obj_uframeProfile/ObjectFrame/ObjectFrameOrientation/@Pitch"
								/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectOrientationPitch"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="uframeRoll">
					<xsl:choose>
						<xsl:when test="string($obj_uframeProfile)!=''">
							<xsl:call-template name="Scientific">
								<xsl:with-param name="Num"
									select="$obj_uframeProfile/ObjectFrame/ObjectFrameOrientation/@Roll"
								/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectOrientationRoll"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="uframe_xyzwpr"
					select="concat($uframeX, ',', $uframeY, ',', $uframeZ, ',', $uframeYaw, ',', $uframePitch, ',', $uframeRoll)"/>
				<!--xsl:value-of select="$cr"/>
			<xsl:text>UFRAME:  </xsl:text>
			<xsl:value-of select="$uframe_xyzwpr"/>
			<xsl:value-of select="$cr"/-->
				<xsl:variable name="uframe_mat"
					select="MatrixUtils:dgXyzyprToMatrix($uframe_xyzwpr)"/>
				<xsl:variable name="uframe_quaternions" select="MatrixUtils:dgMatrixToQuaternions()"/>
				<xsl:variable name="uframe_inv" select="MatrixUtils:dgInvert()"/>
				<xsl:variable name="uframe_inv_xyzwpr"
					select="substring-after($uframe_inv, 'result:')"/>
				<xsl:variable name="xyzwpr"
					select="concat($objectPositionX, ',',  $objectPositionY,  ',' , $objectPositionZ , ',' , $objectOrientationYaw , ',' , $objectOrientationPitch , ',' , $objectOrientationRoll )"/>
				<xsl:variable name="checkObjFrameUse"
					select="concat('UsedObjectFrameProfile', $objectName)"/>
				<xsl:variable name="objFrameUsed"
					select="MotomanUtils:GetParameterData($checkObjFrameUse)"/>
				<xsl:if
					test="$objectName != 'wobj0' and $objectName != 'Default' and string($objectNameReplaced)='' and $objFrameUsed = '1'">
					<xsl:variable name="ufprogvalue">
						<xsl:choose>
							<xsl:when test="contains($objectName,'_ufprog')">
								<xsl:text>FALSE</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>TRUE</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:text>  PERS wobjdata </xsl:text>
					<xsl:choose>
						<xsl:when test="contains($objectName,'_ufprog')">
							<xsl:value-of select="substring-before($objectName,'_ufprog')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$objectName"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:variable name="wobjtypename" select="concat('WOBJTYPE',$objectName)"/>
					<xsl:variable name="wobjType"
						select="MotomanUtils:GetParameterData(string($wobjtypename))"/>
					<xsl:choose>
						<xsl:when test="$wobjType = 'Stationary'">
							<xsl:text>:=[TRUE,</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>:=[FALSE,</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$ufprogvalue"/>
					<xsl:text>,"",[[</xsl:text>
					<xsl:choose>
						<xsl:when
							test="$worldCoords = '1' or $wobjType = 'Stationary' or $worldCoords = 'true'">
							<!-- uframe/world -->
							<xsl:variable name="matresult"
								select="MatrixUtils:dgXyzyprToMatrix($uframe_xyzwpr)"/>
							<xsl:variable name="toQuaternions"
								select="MatrixUtils:dgMatrixToQuaternions()"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if
								test="math:abs($objectPositionX) &gt; .001 or math:abs($objectPositionY)  &gt; .001 or math:abs($objectPositionZ) &gt; .001 or math:abs($objectOrientationYaw) &gt; .001 or math:abs($objectOrientationPitch) &gt; .001 or math:abs($objectOrientationRoll) &gt; .001">
								<!-- first put V5World with respect to V5 Robot Base on matrice stack -->
								<xsl:variable name="xyzwpr2"
									select="concat($basePositionX, ',',  $basePositionY,  ',' , $basePositionZ , ',' , $baseOrientationYaw , ',' , $baseOrientationPitch , ',' , $baseOrientationRoll )"/>
								<xsl:variable name="matresult"
									select="MatrixUtils:dgXyzyprToMatrix($xyzwpr2)"/>
								<xsl:variable name="invresult" select="MatrixUtils:dgInvert()"/>
								<!--xsl:value-of select="$cr"/>
									<xsl:text>V5WORLD/ROBOT BASE: </xsl:text>
									<xsl:value-of select="$invresult"/-->
								<!-- concat Uframe with respect to V5world -->
								<!--xsl:value-of select="$cr"/>
									<xsl:text>UFRAME/V5WORLD</xsl:text>
									<xsl:value-of select="$uframe_xyzwpr"/>
									<xsl:value-of select="$cr"/-->
								<!-- gives us Uframe with respect to Robot Base -->
								<xsl:variable name="uframe_robotbase"
									select="MatrixUtils:dgCatXyzyprMatrix($uframe_xyzwpr)"/>
								<!--xsl:value-of select="$cr"/>
									<xsl:text>UFRAME/ROBOT BASE</xsl:text>
									<xsl:value-of select="$uframe_robotbase"/>
									<xsl:value-of select="$cr"/-->
								<xsl:variable name="result"
									select="MotomanUtils:SetParameterData('trackBasePositionX', '0.0')"/>
								<xsl:variable name="result"
									select="MotomanUtils:SetParameterData('trackBasePositionY','0.0')"/>
								<xsl:variable name="result"
									select="MotomanUtils:SetParameterData('trackBasePositionZ', '0.0')"/>
								<xsl:variable name="result"
									select="MotomanUtils:SetParameterData('trackBasePositionQ1', '1.0')"/>
								<xsl:variable name="result"
									select="MotomanUtils:SetParameterData('trackBasePositionQ2', '0.0')"/>
								<xsl:variable name="result"
									select="MotomanUtils:SetParameterData('trackBasePositionQ3', '0.0')"/>
								<xsl:variable name="result"
									select="MotomanUtils:SetParameterData('trackBasePositionQ4','0.0')"/>
								<xsl:if test="string($mocConfigFileName) != ''">
									<xsl:variable name="mocResult"
										select="FileUtils:fileOpenRead($mocConfigFileName)"/>
									<xsl:if test="$mocResult = 'Okay'">
										<xsl:call-template name="readMoc">
											<xsl:with-param name="line"
												select="FileUtils:fileLineRead()"/>
										</xsl:call-template>
										<xsl:variable name="closeFile"
											select="FileUtils:fileCloseRead()"/>
									</xsl:if>
								</xsl:if>
								<xsl:variable name="trackBasePositionX"
									select="MotomanUtils:GetParameterData('trackBasePositionX')"/>
								<xsl:variable name="trackAuxJoint"
									select="MotomanUtils:GetParameterData('Base PositionTrack')"/>
								<xsl:variable name="adjustedTrackBasePositionX"
									select="number($trackBasePositionX) + number($trackAuxJoint)"/>
								<xsl:variable name="trackBasePositionY"
									select="MotomanUtils:GetParameterData('trackBasePositionY')"/>
								<xsl:variable name="trackBasePositionZ"
									select="MotomanUtils:GetParameterData('trackBasePositionZ')"/>
								<xsl:variable name="trackBasePositionQ1"
									select="MotomanUtils:GetParameterData('trackBasePositionQ1')"/>
								<xsl:variable name="trackBasePositionQ2"
									select="MotomanUtils:GetParameterData('trackBasePositionQ2')"/>
								<xsl:variable name="trackBasePositionQ3"
									select="MotomanUtils:GetParameterData('trackBasePositionQ3')"/>
								<xsl:variable name="trackBasePositionQ4"
									select="MotomanUtils:GetParameterData('trackBasePositionQ4')"/>
								<xsl:variable name="trackBaseQuaternions"
									select="concat($trackBasePositionQ1, ',',  $trackBasePositionQ2,  ',' , $trackBasePositionQ3 , ',' , $trackBasePositionQ4)"/>
								<xsl:variable name="trackBaseYPR"
									select="MatrixUtils:quaternionToYPR($trackBaseQuaternions)"/>
								<xsl:variable name="trackbase"
									select="concat($adjustedTrackBasePositionX, ',',  $trackBasePositionY,  ',' , $trackBasePositionZ , ',' , $trackBaseYPR)"/>
								<!-- put  Robot base with respect to Tbase from MOC.cfg on stack -->
								<xsl:variable name="matresult2"
									select="MatrixUtils:dgXyzyprToMatrix($trackbase)"/>
								<!--xsl:value-of select="$cr"/>
											<xsl:text>ROBOT BASE/TRACK BASE:</xsl:text>
											<xsl:value-of select="$matresult2"/>
											<xsl:value-of select="$cr"/-->
								<!-- concat uframe with respect to Robot base from above -->
								<xsl:variable name="matresult3"
									select="MatrixUtils:dgCatXyzyprMatrix(substring-after($uframe_robotbase, 'result:'))"/>
								<!-- xsl:text>UFRAME/TRACK BASE</xsl:text>
											<xsl:value-of select="$matresult3"/>
											<xsl:value-of select="$cr"/-->
								<xsl:variable name="quatresult"
									select="MatrixUtils:dgMatrixToQuaternions()"/>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Object values -->
					<xsl:variable name="objectResultX">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objectResultY">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objectResultZ">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<!-- Object quaternions -->
					<xsl:variable name="objectQuaternion1">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ1()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objectQuaternion2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ2()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objectQuaternion3">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ3()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="objectQuaternion4">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ4()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:value-of select="format-number($objectResultX,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($objectResultY,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($objectResultZ,$decimalNumberPattern)"/>
					<xsl:text>],[</xsl:text>
					<xsl:value-of select="format-number($objectQuaternion1,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($objectQuaternion2,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($objectQuaternion3,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($objectQuaternion4,$targetNumberPattern)"/>
					<xsl:text>]],[[</xsl:text>
					<!-- put world/uframe on the stack -->
					<xsl:variable name="matresult"
						select="MatrixUtils:dgXyzyprToMatrix($uframe_inv_xyzwpr)"/>
					<!-- concat oframe/world -->
					<xsl:variable name="matresult" select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr)"/>
					<xsl:variable name="quatresult" select="MatrixUtils:dgMatrixToQuaternions()"/>
					<!-- Object values -->
					<xsl:variable name="oframeX">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="oframeY">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="oframeZ">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<!-- Object quaternions -->
					<xsl:variable name="oframeQ1">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ1()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="oframeQ2">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ2()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="oframeQ3">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ3()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="oframeQ4">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="MatrixUtils:dgGetQ4()"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:value-of select="format-number($oframeX, $decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($oframeY, $decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($oframeZ, $decimalNumberPattern)"/>
					<xsl:text>],[</xsl:text>
					<xsl:value-of select="format-number($oframeQ1, $targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($oframeQ2, $targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($oframeQ3, $targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($oframeQ4, $targetNumberPattern)"/>
					<xsl:text>]]];</xsl:text>
					<xsl:value-of select="$cr"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
		<xsl:if
			test="string($speeddata_output) != 'false' and string($profiledata_output) != 'false'">
			<xsl:for-each select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile">
				<xsl:variable name="motionName" select="Name"/>
				<xsl:variable name="motionBasis"
					select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionName]/MotionBasis"/>
				<!-- Robot's linear speed value -->
				<xsl:variable name="speed"
					select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionName]/Speed/@Value"/>
				<!-- Robot's linear acceleration value -->
				<xsl:variable name="accel"
					select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionName]/Accel/@Value"/>
				<!-- Robot's angular speed value -->
				<xsl:variable name="angularSpeedValue"
					select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionName]/AngularSpeedValue/@Value"/>
				<!-- Robot's angular acceleration value -->
				<xsl:variable name="angularAccelValue"
					select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionName]/AngularAccelValue/@Value"/>
				<xsl:choose>
					<xsl:when test="$motionName='v5'"> </xsl:when>
					<xsl:when test="$motionName='v10'"> </xsl:when>
					<xsl:when test="$motionName='v20'"> </xsl:when>
					<xsl:when test="$motionName='v30'"> </xsl:when>
					<xsl:when test="$motionName='v40'"> </xsl:when>
					<xsl:when test="$motionName='v50'"> </xsl:when>
					<xsl:when test="$motionName='v60'"> </xsl:when>
					<xsl:when test="$motionName='v80'"> </xsl:when>
					<xsl:when test="$motionName='v100'"> </xsl:when>
					<xsl:when test="$motionName='v150'"> </xsl:when>
					<xsl:when test="$motionName='v200'"> </xsl:when>
					<xsl:when test="$motionName='v300'"> </xsl:when>
					<xsl:when test="$motionName='v400'"> </xsl:when>
					<xsl:when test="$motionName='v500'"> </xsl:when>
					<xsl:when test="$motionName='v600'"> </xsl:when>
					<xsl:when test="$motionName='v800'"> </xsl:when>
					<xsl:when test="$motionName='v1000'"> </xsl:when>
					<xsl:when test="$motionName='v1500'"> </xsl:when>
					<xsl:when test="$motionName='v2000'"> </xsl:when>
					<xsl:when test="$motionName='v2500'"> </xsl:when>
					<xsl:when test="$motionName='v3000'"> </xsl:when>
					<xsl:when test="$motionName='v4000'"> </xsl:when>
					<xsl:when test="$motionName='v5000'"> </xsl:when>
					<xsl:when test="$motionName='v6000'"> </xsl:when>
					<xsl:when test="$motionName='v7000'"> </xsl:when>
					<xsl:when test="$motionName='vmax'"> </xsl:when>
					<xsl:otherwise>
						<xsl:if test="$motionBasis='Absolute'">
							<xsl:text>  PERS speeddata </xsl:text>
							<xsl:value-of select="$motionName"/>
							<xsl:text>:=[</xsl:text>
							<xsl:value-of select="$speed*1000"/>
							<xsl:text>,500,</xsl:text>
							<xsl:choose>
								<xsl:when test="string($max_v_leax) != ''">
									<xsl:value-of select="format-number($max_v_leax*$speed div $maxSpeed,$decimalNumberPattern)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>5000</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>,</xsl:text>
							<xsl:choose>
								<xsl:when test="string($max_v_reax) != ''">
									<xsl:value-of select="format-number($max_v_reax*$speed div $maxSpeed,$decimalNumberPattern)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>1000</xsl:text>
								</xsl:otherwise>
							</xsl:choose>							
							<xsl:text>];</xsl:text>
							<xsl:value-of select="$cr"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:if>
		<xsl:if
			test="string($zonedata_output) != 'false' and string($profiledata_output) != 'false'">
			<xsl:for-each select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile">
				<xsl:variable name="accuracyName" select="Name"/>
				<xsl:variable name="flyByMode"
					select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyName]/FlyByMode"/>
				<!-- Rounding criterium: 'Speed' or 'Distance' -->
				<xsl:variable name="accuracyType"
					select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyName]/AccuracyType"/>
				<!-- Rounding precision value -->
				<xsl:variable name="accuracyValue"
					select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyName]/AccuracyValue/@Value"/>
				<xsl:choose>
					<xsl:when test="$accuracyName='fine'"> </xsl:when>
					<xsl:when test="$accuracyName='z0'"> </xsl:when>
					<xsl:when test="$accuracyName='z1'"> </xsl:when>
					<xsl:when test="$accuracyName='z5'"> </xsl:when>
					<xsl:when test="$accuracyName='z10'"> </xsl:when>
					<xsl:when test="$accuracyName='z15'"> </xsl:when>
					<xsl:when test="$accuracyName='z20'"> </xsl:when>
					<xsl:when test="$accuracyName='z30'"> </xsl:when>
					<xsl:when test="$accuracyName='z40'"> </xsl:when>
					<xsl:when test="$accuracyName='z50'"> </xsl:when>
					<xsl:when test="$accuracyName='z60'"> </xsl:when>
					<xsl:when test="$accuracyName='z80'"> </xsl:when>
					<xsl:when test="$accuracyName='z100'"> </xsl:when>
					<xsl:when test="$accuracyName='z150'"> </xsl:when>
					<xsl:when test="$accuracyName='z200'"> </xsl:when>
					<xsl:otherwise>
						<xsl:if test="$accuracyType='Distance'">
							<xsl:text>  PERS zonedata </xsl:text>
							<xsl:value-of select="$accuracyName"/>
							<xsl:choose>
								<xsl:when test="$flyByMode='On'">
									<xsl:text>:=[FALSE,</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>:=[TRUE,</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of
								select="format-number($accuracyValue*1000,$decimalNumberPattern)"/>
							<xsl:text>,</xsl:text>
							<xsl:value-of
								select="format-number($accuracyValue*1000,$decimalNumberPattern)"/>
							<xsl:text>,</xsl:text>
							<xsl:value-of
								select="format-number($accuracyValue*1000,$decimalNumberPattern)"/>
							<xsl:text>,</xsl:text>
							<xsl:value-of
								select="format-number($accuracyValue*100,$decimalNumberPattern)"/>
							<xsl:text>,</xsl:text>
							<xsl:value-of
								select="format-number($accuracyValue*1000,$decimalNumberPattern)"/>
							<xsl:text>,</xsl:text>
							<xsl:value-of
								select="format-number($accuracyValue*100,$decimalNumberPattern)"/>
							<xsl:text>];</xsl:text>
							<xsl:value-of select="$cr"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:if>
		<xsl:if
			test="string($spot_gundata_output) != 'false' and string($profiledata_output) != 'false'">
			<xsl:for-each select="/OLPData/Resource/Controller/UserProfileList/UserProfile">
				<xsl:variable name="dataType">
					<xsl:choose>
						<xsl:when test="substring-before(@Type,'C5')">
							<xsl:value-of select="substring-before(@Type,'C5')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@Type"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$dataType  != 'WSUWeldTimeTable'">
					<xsl:for-each select="UserProfileInstance">
						<xsl:if
							test="substring-after($dataType,'ABB') != 'gundata' or number($rapidVersion) != 2">
							<xsl:text>  PERS  </xsl:text>
							<xsl:value-of select="substring-after($dataType,'ABB')"/>
							<xsl:text> </xsl:text>
							<xsl:variable name="nameBeforeDot" select="substring-before(@Name,'.')"/>
							<xsl:choose>
								<xsl:when test="$nameBeforeDot">
									<xsl:value-of select="$nameBeforeDot"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>:=[</xsl:text>
							<xsl:for-each select="UserDefinedAttribute">
								<xsl:if
									test="DisplayName !='Version' and starts-with(DisplayName,'$')=false">
									<xsl:value-of select="Value"/>
									<xsl:choose>
										<xsl:when test="position()=last()">
											<xsl:text>];</xsl:text>
											<xsl:value-of select="$cr"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>,</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<!-- end of CreateControllerDeclarationSection -->
	<!--~~~~~ATTRIBUTE DATA INITIALIZER TEMPLATE~~~~~ -->
	<!-- In order to make all the attribute data global, this template stores all the passed attribute name / value pairs in java.
	Afeter being stored, those attribute values will be accessible throughout this stylesheet. This template
	will not be called automatically by the processor. It is explicitly called from the ActivityList, Activity, and Action templates -->
	<xsl:template name="SetAttributes">
		<!-- Value of the id attribute of the XML element, for which attributes are to be set -->
		<xsl:param name="id"/>
		<!-- Valid AttributeList XML element that contains Attribute sub-elements -->
		<xsl:param name="attributeListNodeSet"/>
		<!-- For each Attribute element in supplied AttributeList element -->
		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<!-- Check if AttributeName sub-element exist -->
			<xsl:if test="AttributeName">
				<!-- Store the value of the AttributeName element -->
				<xsl:variable name="attribName" select="AttributeName"/>
				<!-- Check if AttributeValue sub-element exist -->
				<xsl:if test="AttributeValue">
					<!-- Store the value of the AttributeValue element -->
					<xsl:variable name="attribValue" select="AttributeValue"/>
					<!-- Call Java extension to store the attribute name/value pairs -->
					<xsl:variable name="hr"
						select="MotomanUtils:SetAttributeData( string($id), string($attribName), string($attribValue) )"/>
					<!-- IMPORTANT: In order to retrieve attribute value, from anywhere in the stylesheet use:
					String MotomanUtils:GetAttributeData(String, String) function. For example, this will work:
					<xsl:variable name="result" select="MotomanUtils:GetAttributeData('_123456', 'MyAttrib1')"/>
					Pass XML element's id attribute and an attribute name, both as strings, and the return value 
					(result, in the example above) will either be the attribute value (result, in the example above) 
					returned as a string, or an empty string if the attribute name does not exist in V5 for the XML 
					element which id attribute has been passed to the function -->
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end of SetAttributes -->
	<!--~~~~~MULTI FILE OUTPUT TEMPLATE~~~~~-->
	<!-- This template creates a new DATA FILE START / END block for each ActivityList element in the source XML file.
		The contents of each of these blocks will be saved in separate files, when users decide to save the downloaded
		 content to disk. Equally effective (if not more effective) alternative is to save files from an XSLT stylesheet directly. For
		 that purpose org.apache.xalan.lib.Redirect java extension should be used. To get more information on how to activate
		 this extension, check the following url: http://xml.apache.org/xalan-j/apidocs/org/apache/xalan/lib/Redirect.html.
		 This template is called automatically by XSLT processor. -->
	<xsl:template match="ActivityList">
		<!-- Store the robot task name which content is represented by this ActivityList XML element -->
		<xsl:variable name="taskName" select="translate(@Task, '.', '')"/>
		<!-- If there is an attribute list associated with the robot task, store its attributes in java -->
		<xsl:if test="AttributeList">
			<!-- Call SetAttributes template to store attribute values for subsequent retrievals -->
			<xsl:call-template name="SetAttributes">
				<!-- Task name is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="@Task"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="precomment"
			select="AttributeList/Attribute[AttributeName =  'PreComment1']/AttributeValue"/>
		<xsl:variable name="postcomment"
			select="AttributeList/Attribute[AttributeName =  'PostComment1']/AttributeValue"/>
		<xsl:if test="$precomment != ''">
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Pre'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<!-- If each ActivityList element needs to be saved in a new file, DATA FILE START/END block needs to be added and
			file extension specified below needs to changed from .txt to appropriate file extension for the chosen robot language -->
		<!-- FOR RAPID THERE IS ONE DATAFILE START/END FOR THE ENTIRE MODULE SO NONE HERE..JUST
                    OUTPUT PROC DECLARATION FOR TASK -->
		<xsl:if test="starts-with($taskName,'LOCAL_')">
			<xsl:text>LOCAL </xsl:text>
		</xsl:if>
		<xsl:text>PROC </xsl:text>
		<xsl:choose>
			<xsl:when test="starts-with($taskName,'LOCAL_')">
				<xsl:value-of select="substring-after($taskName,'LOCAL_')"/>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData( 'CURRENT_PROCEDURE_NAME', $taskName )"/>
			</xsl:when>
			<xsl:when test="contains($taskName,'#')">
				<xsl:value-of select="substring-before($taskName,'#')"/>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData( 'CURRENT_PROCEDURE_NAME', substring-before($taskName,'#') )"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$taskName"/>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData( 'CURRENT_PROCEDURE_NAME', $taskName )"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>()</xsl:text>
		<xsl:value-of select="$cr"/>
		<!-- DLY ADDED CURRENT PROCEDURE NAME AND OUTPUT OF TARGET/CONTROLLER  DECLARATION FOR EACH PROC-->
		<xsl:if test="contains($mainTaskName,'#')">
			<!-- This is optional. Call this template if you need to create target declaration section for downloaded program -->
			<xsl:call-template name="CreateTargetDeclarationSection"/>
		</xsl:if>
		<xsl:choose>
			<!-- If Call task activities exist and if they require creation of a new file for each task, output DATA FILE START/END block -->
			<xsl:when
				test="count(/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBIgpCallRobotTask']) > 0 and
							/OLPData/Resource/ActivityList/Activity[@ActivityType = 'DNBIgpCallRobotTask'][1]/DownloadStyle = 'File'">
				<!-- Start the block which contents will be saved in a file -->
				<xsl:value-of select="$cr"/>
				<!-- Call the templates that will process all the Action and Activity nodes of this activity list -->
				<xsl:apply-templates select="Activity | Action"/>
				<!-- End the block which contents will be saved in a file -->
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- If there are no Call task activities or if they require subroutine output, begin activity/action translation -->
			<xsl:otherwise>
				<!-- Call the templates that will process all the Action and Activity nodes of this activity list -->
				<xsl:apply-templates select="Activity | Action"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- RAPID ENDPROC OUTPUT-->
		<xsl:text>ENDPROC</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:if test="$postcomment != ''">
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Post'"/>
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- end of ActivityList -->
	<!--~~~~~ACTION PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Action elements which are composed of one or more Activity elements. Only the 
		atomic activity types listed below are supported to be part of actions. This template is called automatically by
		XSLT processor. -->
	<xsl:template match="Action">
		<!-- All actions, regardless of their type, have these XML attributes and elkements -->
		<!-- Store this action's id number -->
		<xsl:variable name="actionID" select="@id"/>
		<!-- Store this action's type -->
		<xsl:variable name="actionType" select="@ActionType"/>
		<!-- Store this action's name -->
		<xsl:variable name="actionName" select="ActionName"/>
		<!-- Store name of the resource which performs this action-->
		<xsl:variable name="toolInActionName" select="ToolResource/ResourceName"/>
		<!-- Set the attributes associated with this action. Refer to the comment marked as IMPORTANT in
			SetAttributes template -->
		<xsl:if test="AttributeList">
			<xsl:call-template name="SetAttributes">
				<!-- Action id attribute value is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="$actionID"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<!-- Based on action type, and the number and type of activities it contains, pick relevant portions of code contained below
		      in xsl:if element and create appropriate output. Delete redundant poritions of the code, and just keep what is needed.
		      All blocks of code below, by default, reference just the first activity of a specified type within an action. If there is more
		      than one activity of the same type within the action, each activity needs to be referenced based on its order of appearance
		      (of all the activities of the same type) within the action. 
		      For example, in case of Delay-Release-Delay combination of activities in one action:
		      
		      The first activity will be referenced, like:
		      
		      	<xsl:variable name="delayActivity" select="ActivityList/Activity[$activityType = 'DelayActivity' ][1]"/>
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			
			The second activity will be referenced, like:
			
			<xsl:variable name="releaseActivity" select="ActivityList/Activity[$activityType =  'ReleaseActivity' ][1]"/>
			<xsl:variable name="releaseActivityID" select="$releaseActivity/@id"/>
			<xsl:variable name="releaseActivityName" select="$releaseActivity/ActivityName"/>
			<xsl:if test="$releaseActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$releaseActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$releaseActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:variable name="releasedObject" select="$releaseActivity/ReleasedObject"/>
			<xsl:variable name="releasedDeviceParent" select="$releaseActivity/ReleasedObject/@ParentName"/>

			While the thrid activity will be referenced like:
			
		      	<xsl:variable name="delayActivity" select="ActivityList/Activity[$activityType = 'DelayActivity' ][2]"/>
			<xsl:variable name="delayActivityID" select="$delayActivity/@id"/>
			<xsl:variable name="delayActivityName" select="$delayActivity/ActivityName"/>
			<xsl:variable name="delayActivityStartTime" select="$delayActivity/ActivityTime/StartTime"/>
			<xsl:variable name="delayActivityEndTime" select="$delayActivity/ActivityTime/EndTime"/>
			<xsl:if test="$delayActivity/AttributeList">
				<xsl:call-template name="SetAttributes">
					<xsl:with-param name="id" select="$delayActivityID"/>
					<xsl:with-param name="attributeListNodeSet" select="$delayActivity/AttributeList"/>
				</xsl:call-template>
			</xsl:if>
		-->
		<!-- Repeate xsl:if elements for as many action types which are relevant for the target robot language -->
		<xsl:variable name="macro"
			select="AttributeList/Attribute[AttributeName='Macro']/AttributeValue"/>
		<xsl:if test="$macro != ''">
			<xsl:text>  </xsl:text>
			<xsl:value-of select="$macro"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
	</xsl:template>
	<!-- end of Action -->
	<!--~~~~~ACTIVITY PROCESSOR TEMPLATE~~~~~-->
	<!-- This template processes XML Activity elements in their order of appearance within the XML source file. Only the top level 
		Activity elements (the ones whose anscestors are OLPData, Resource, and AttributeList elements) will be transformed 
		by this template. Activity elements which are descendants of Action element, will be processed by Actions template above.
		This template is called automatically by XSLT processor. -->
	<xsl:template match="Activity">
		<!-- Global variables for all activities -->
		<!-- Get activity id number. It is used to set and get activity attributes. -->
		<xsl:variable name="activityID" select="@id"/>
		<!-- Get the activity type. Only the activity types specified below are supported. -->
		<xsl:variable name="activityType" select="@ActivityType"/>
		<!-- Get the activityName -->
		<xsl:variable name="activityName" select="ActivityName"/>
		<!-- Get the activity begin time -->
		<xsl:variable name="startTime" select="ActivityTime/StartTime"/>
		<!-- Get the activity finish time -->
		<xsl:variable name="endTime" select="ActivityTime/EndTime"/>
		<!-- Set the attributes associated with this activity. Refer to the comment marked as IMPORTANT in
			SetAttributes template -->
		<xsl:if test="AttributeList">
			<xsl:call-template name="SetAttributes">
				<!-- Activity id attribute value is used as a unique identifier of this ActivityList XML element -->
				<xsl:with-param name="id" select="$activityID"/>
				<!-- Send the AttributeList XML element as the second parameter -->
				<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
		<!-- Based on activity type (the value of ActivityType XML element), process all the supported activities -->
		<xsl:choose>
			<!-- ROBOT MOTION ACTIVITY -->
			<xsl:when test="$activityType = 'DNBRobotMotionActivity'">
				<!-- Call the template which will output a move statement -->
				<xsl:call-template name="TransformRobotMotionActivityData">
					<!-- Send this robot motion activity Nodeset as parameter -->
					<xsl:with-param name="motionActivityNode" select="."/>
					<!-- This parameter determines that called templete needs to create a move statement
						in designated robot language -->
					<xsl:with-param name="mode" select="'MoveStatement'"/>
				</xsl:call-template>
			</xsl:when>
			<!-- DELAY ACTIVITY -->
			<xsl:when test="$activityType = 'DelayActivity' ">
				<!-- Output formatted delay statement in chosen robot language -->
				<xsl:text>  WaitTime \InPos,</xsl:text>
				<xsl:value-of select="format-number($endTime - $startTime,$decimalNumberPattern)"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- SET IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBSetSignalActivity' ">
				<!-- Get signal name -->
				<xsl:variable name="setSignalName" select="SetIOAttributes/IOSetSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="setSignalValue"
					select="SetIOAttributes/IOSetSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="setPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
				<!-- Get the signal pulse time -->
				<xsl:variable name="setSignalDuration"
					select="SetIOAttributes/IOSetSignal/@SignalDuration"/>
				<!-- Output formatted set IO signal statement in chosen robot language -->
				<xsl:choose>
					<xsl:when test="$setSignalDuration &gt; 0 and $setSignalValue = 'On'">
						<xsl:text>  PulseDO \PLength:=</xsl:text>
						<xsl:value-of select="$setSignalDuration"/>
						<xsl:text>,</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>  SetDO </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$setSignalName"/>
				<xsl:choose>
					<xsl:when test="$setSignalDuration &gt; 0"> </xsl:when>
					<xsl:when test="$setSignalValue = 'On'">
						<xsl:text>,</xsl:text>
						<xsl:text>on</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>,</xsl:text>
						<xsl:text>off</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- WAIT FOR IO SIGNAL ACTIVITY -->
			<xsl:when test="$activityType = 'DNBWaitSignalActivity' ">
				<!-- Get signal name -->
				<xsl:variable name="waitSignalName"
					select="WaitIOAttributes/IOWaitSignal/@SignalName"/>
				<!-- Get signal value: On or Off -->
				<xsl:variable name="waitSignalValue"
					select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
				<!-- Get port number -->
				<xsl:variable name="waitPortNumber"
					select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
				<!-- Get maximum amount of time a robot will wait to receive a signal -->
				<xsl:variable name="waitMaxTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>
				<!-- Output formatted wait IO signal statement in chosen robot language -->
				<xsl:text>  WaitDI </xsl:text>
				<xsl:value-of select="$waitSignalName"/>
				<xsl:text>,</xsl:text>
				<xsl:value-of select="$waitSignalValue"/>
				<xsl:if test="$waitMaxTime &gt;  0">
					<xsl:text>\MaxTime:=</xsl:text>
					<xsl:value-of select="$waitMaxTime"/>
				</xsl:if>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:when>
			<!-- CALL ROBOT TASK ACTIVITY -->
			<xsl:when test="$activityType = 'DNBIgpCallRobotTask' ">
				<!-- Sub-program or sub-routine call. That depemds on the value of DownloadStyle element (below) -->
				<xsl:variable name="robotTaskToCall">
					<xsl:choose>
						<xsl:when test="starts-with(CallName,'LOCAL_')">
							<xsl:value-of
								select="translate(substring-after(CallName,'LOCAL_'),'.','')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="translate(CallName, '.', '')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- This defines whether a subprogram - "File" or a subroutine - "Subroutine" will be called -->
				<xsl:variable name="downloadCalledTaskStyle" select="DownloadStyle"/>
				<xsl:text>  </xsl:text>
				<xsl:value-of select="$robotTaskToCall"/>
				<xsl:text>;</xsl:text>
				<xsl:value-of select="$cr"/>
				<!-- Output formatted call sub-program or call separate program statement in chosen robot language -->
			</xsl:when>
			<!-- ENTER INTERFERENCE ZONE ACTIVITY -->
			<xsl:when test="$activityType = 'DNBEnterZoneActivity' ">
				<!-- By default only zone names are relevant for the robot program creation. If the names of resources
					that are monitored for this zone are important for the robot language, this variable can be changed to
					'true' -->
				<xsl:variable name="areResourceNamesImportant" select=" 'false' "/>
				<!-- Get interference zone name -->
				<xsl:variable name="zoneName" select="ZoneData/ZoneName"/>
				<!-- Based on the value of areResourceNamesImportant variable, create the output-->
				<xsl:choose>
					<!-- Resource names are important for the output robot program -->
					<xsl:when test="$areResourceNamesImportant">
						<!-- For each resource in interference zone -->
						<xsl:for-each select="ZoneData/ZoneResourceList/ZoneResource">
							<!-- Get the resource name -->
							<xsl:variable name="resourceName" select="@Name"/>
							<!-- Get resource parent's name, as it appears in the PPR tree. 
								This may be important when workcell was imported from PPR Hub. -->
							<xsl:variable name="parentName" select="@ParentName"/>
							<!-- Output formatted enter interference zone statement and define the resources involved, in 
								a chosen robot language -->
						</xsl:for-each>
					</xsl:when>
					<!-- Resource names are not important for the output robot program -->
					<xsl:otherwise>
						<!-- Output formatted enter interference zone statement in a chosen robot language -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- CLEAR INTERFERENCE ZONE ACTIVITY -->
			<xsl:when test="$activityType = 'DNBClearZoneActivity' ">
				<!-- By default only zone names are relevant for the robot program creation. If the names of resources
					that are monitored for this zone are important for the robot language, this variable can be changed to
					'true' -->
				<xsl:variable name="areResourceNamesImportant" select=" 'false' "/>
				<!-- Get interference zone name -->
				<xsl:variable name="zoneName" select="ZoneData/ZoneName"/>
				<!-- Based on the value of areResourceNamesImportant variable, create the output-->
				<xsl:choose>
					<!-- Resource names are important for the output robot program -->
					<xsl:when test="$areResourceNamesImportant">
						<!-- For each resource in interference zone -->
						<xsl:for-each select="ZoneData/ZoneResourceList/ZoneResource">
							<!-- Get the resource name -->
							<xsl:variable name="resourceName" select="@Name"/>
							<!-- Get resource parent's name, as it appears in the PPR tree. 
								This may be important when workcell was imported from PPR Hub. -->
							<xsl:variable name="parentName" select="@ParentName"/>
							<!-- Output formatted clear interference zone statement and define the resources involved, in 
								a chosen robot language -->
						</xsl:for-each>
					</xsl:when>
					<!-- Resource names are not important for the output robot program -->
					<xsl:otherwise>
						<!-- Output formatted clear interference zone statement in a chosen robot language -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- FOLLOW PATH ACTIVITY -->
			<xsl:when test="$activityType = 'FollowPathActivity'">
				<!-- Call the template which will output MoveAlong statement -->
				<xsl:call-template name="TransformFollowPathActivityData">
					<!-- Send this follow path activity Nodeset as parameter -->
					<xsl:with-param name="followPathActivityNode" select="."/>
					<!-- This parameter determines that called templete needs to create MoveAlong statement
						in designated robot language -->
					<xsl:with-param name="mode" select=" 'MoveAlongStatement' "/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
		<xsl:variable name="macro"
			select="AttributeList/Attribute[AttributeName='Macro']/AttributeValue"/>
		<xsl:if test="$macro != ''">
			<xsl:text>  </xsl:text>
			<xsl:value-of select="$macro"/>
			<xsl:value-of select="$cr"/>
		</xsl:if>
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
	</xsl:template>
	<!-- end of Activity -->
	<xsl:template name="TransformRobotMotionActivityData">
		<!-- Nodeset that contains all the descendants of robot motion activity element -->
		<xsl:param name="motionActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveStatement' "> element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$motionActivityNode/@id"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile"
			select="$motionActivityNode/MotionAttributes/MotionProfile"/>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile"
			select="$motionActivityNode/MotionAttributes/AccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile"
			select="translate($motionActivityNode/MotionAttributes/ToolProfile, '.', '_')"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<xsl:variable name="objectFrameName"
			select="$motionActivityNode/MotionAttributes/ObjectFrameProfile"/>
		<xsl:variable name="objectFrameReplaced"
			select="$motionActivityNode/MotionAttributes/ObjectFrameProfile/@Replaced"/>
		<xsl:choose>
			<xsl:when test="string($objectFrameReplaced) = ''">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameName)"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameReplaced)"
				/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="objectFrameProfile"
			select="MotomanUtils:GetParameterData('OBJECTPROFILENAME')"/>
		<!-- Motion profile values used by this motion activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speed"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Accuracy profile values used by this motion activity -->
		<!-- Variable that determines whether rounding is turned on or off. Possible values: 'On' or 'Off' -->
		<xsl:variable name="flyByMode"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/FlyByMode"/>
		<!-- Rounding criterium: 'Speed' or 'Distance' -->
		<xsl:variable name="accuracyType"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyType"/>
		<!-- Rounding precision value -->
		<xsl:variable name="accuracyValue"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyValue/@Value"/>
		<!-- Tool profile values used by this motion activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType"
			select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- DLY Start 2009/05/14 moved this here so that type is set prior to output of first target -->
		<xsl:variable name="wobjtypename" select="concat('WOBJTYPE',$objectFrameProfile)"/>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData( string($wobjtypename), string($toolType) )"/>
		<!-- DLY End 2009/05/14 -->
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame profile values used by this motion activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame"
			select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<!-- ObjectFrame location offset from robot's mount plate in X direction -->
		<xsl:variable name="ofPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame location offset from robot's mount plate in Y direction -->
		<xsl:variable name="ofPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame location offset from robot's mount plate in Z direction -->
		<xsl:variable name="ofPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame orientation offset from robot's mount plate in Yaw  direction -->
		<xsl:variable name="ofOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="ofOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame orientation offset from robot's mount plate in Roll  direction -->
		<xsl:variable name="ofOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Motion type of this motion activity: 'Joint' or 'Linear' -->
		<xsl:variable name="motionType" select="$motionActivityNode/MotionAttributes/MotionType"/>
		<!-- Orient mode of this motion activity: 'Wrist', '1_Axis', '2_Axis', or '3_Axis' -->
		<xsl:variable name="orientMode" select="$motionActivityNode/MotionAttributes/OrientMode"/>
		<!-- Target type of this motion activity: 'Joint' or 'Cartesian' -->
		<xsl:variable name="targetType" select="$motionActivityNode/Target/@Default"/>
		<!-- Boolean that specifies whether this target is a via point or not: 'true' or 'false' -->
		<xsl:variable name="isViaPoint" select="$motionActivityNode/Target/@ViaPoint"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this move -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basePositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData(string('Base PositionX'), $basePositionX)"/>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData(string('Base PositionY'), $basePositionY)"/>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData(string('Base PositionZ'), $basePositionZ)"/>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="baseOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData(string('Base Orientation Yaw'), $baseOrientationYaw)"/>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="baseOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData(string('Base Orientation Pitch'), $baseOrientationPitch)"/>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baseOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData(string('Base Orientation Roll'), $baseOrientationRoll)"/>
		<!-- Robot base location offset along track -->
		<xsl:variable name="trackAuxValue"
			select="$motionActivityNode/Target/JointTarget/AuxJoint[@Type='RailTrackGantry']/JointValue"/>
		<xsl:variable name="basePositionTrack">
			<xsl:choose>
				<xsl:when test="string($trackAuxValue) != ''">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/JointTarget/AuxJoint[@Type='RailTrackGantry']/JointValue"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="hr"
			select="MotomanUtils:SetParameterData(string('Base PositionTrack'), $basePositionTrack)"/>
		<xsl:variable name="arcSeamData" select="MotionAttributes/UserProfile[@Type='ABBseamdata']"/>
		<xsl:variable name="arcWeldData" select="MotionAttributes/UserProfile[@Type='ABBwelddata']"/>
		<xsl:variable name="arcWeaveData"
			select="MotionAttributes/UserProfile[@Type='ABBweavedata']"/>
		<xsl:variable name="beadName"
			select="AttributeList/Attribute[AttributeName='BeadData']/AttributeValue"/>		
		<!-- RAPID DLY moved tag name -->
		<xsl:variable name="tempTagName" select="$motionActivityNode/Target/CartesianTarget/Tag"/>
		<xsl:choose>
			<xsl:when test="$tempTagName">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('TAGNAME'),string($tempTagName))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="tempTagName"
					select="AttributeList/Attribute[AttributeName='Tag Name']/AttributeValue"/>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('TAGNAME'),string($tempTagName))"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="tagName" select="MotomanUtils:GetParameterData(string('TAGNAME'))"/>
		<xsl:choose>
			<xsl:when test="string($tagName) != '' and substring($tagName,1,13) != 'abb_explicit_'">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('EXPLICITTARGET'),string( '0'))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('EXPLICITTARGET'),string( '1'))"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="explicitTarget"
			select="MotomanUtils:GetParameterData(string('EXPLICITTARGET'))"/>
		<xsl:variable name="hr2"
			select="MotomanUtils:SetParameterData(string('ARCDATAISSET'),string( '0'))"/>
		<xsl:if test="$mode = 'MoveStatement'">
			<xsl:variable name="retractnode" select="following-sibling::*[2]"/>
			<xsl:variable name="retractActionType" select="$retractnode/@ActionType"/>
			<xsl:variable name="spotnode" select="following-sibling::*[1]"/>
			<xsl:variable name="spotActionType" select="$spotnode/@ActionType"/>
			<xsl:variable name="spotActionID" select="$spotnode/@id"/>
			<xsl:variable name="moveName" select="ActivityName"/>
			<xsl:variable name="spotName" select="substring($moveName,1,5)"/>
			<xsl:variable name="spotMName" select="substring($moveName,1,6)"/>
			<xsl:variable name="studName" select="substring($moveName,1,5)"/>
			<xsl:variable name="paintName" select="substring($moveName,1,6)"/>
			<xsl:variable name="onoffstr"
				select="AttributeList/Attribute[AttributeName='Gun']/AttributeValue"/>
			<xsl:variable name="moveSuffix">
				<xsl:choose>
					<xsl:when test="$motionType='Joint'">
						<xsl:text>J</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>L</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$moveName='ArcLStart' or $moveName='ArcLEnd'">
					<xsl:text>  </xsl:text>
					<xsl:value-of select="$moveName"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when test="$moveName='ArcCStart' or $moveName='ArcCEnd'">
					<xsl:choose>
						<xsl:when test="$motionType = 'CircularVia'">
							<xsl:text>  </xsl:text>
							<xsl:value-of select="$moveName"/>
							<xsl:text> </xsl:text>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$moveName='ArcC'">
					<xsl:choose>
						<xsl:when test="$motionType = 'CircularVia'">
							<xsl:text>  </xsl:text>
							<xsl:value-of select="$moveName"/>
							<xsl:text> </xsl:text>
							<xsl:if test="$onoffstr">
								<xsl:value-of select="$onoffstr"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$moveName='DispC'">
					<xsl:choose>
						<xsl:when test="$motionType = 'CircularVia'">
							<xsl:text>  </xsl:text>
							<xsl:value-of select="$moveName"/>
							<xsl:text> </xsl:text>
							<xsl:if test="$onoffstr">
								<xsl:value-of select="$onoffstr"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
				</xsl:when>				
				<xsl:when test="$moveName='ArcL' or $moveName='ArcL1' or $moveName='ArcL2'">
					<xsl:text>  </xsl:text>
					<xsl:value-of select="$moveName"/>
					<xsl:text> </xsl:text>
					<xsl:if test="$onoffstr">
						<xsl:value-of select="$onoffstr"/>
						<xsl:text>,</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="$moveName='DispL'">
					<xsl:text>  </xsl:text>
					<xsl:value-of select="$moveName"/>
					<xsl:text> </xsl:text>
					<xsl:if test="$onoffstr">
						<xsl:value-of select="$onoffstr"/>
						<xsl:text>,</xsl:text>
					</xsl:if>					
				</xsl:when>	
				<xsl:when test="$moveName='DispJ'">
					<xsl:text>  </xsl:text>
					<xsl:value-of select="$moveName"/>
					<xsl:text> </xsl:text>
					<xsl:if test="$onoffstr">
						<xsl:value-of select="$onoffstr"/>
						<xsl:text>,</xsl:text>
					</xsl:if>					
				</xsl:when>								
				<xsl:when test="$studName='StudL'">
					<xsl:text>  </xsl:text>
					<xsl:value-of select="$studName"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when test="$paintName='PaintL'">
					<xsl:text>  </xsl:text>
					<xsl:value-of select="$paintName"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when test="$spotName='SpotL'">
					<xsl:text>  Spot</xsl:text>
					<xsl:value-of select="$moveSuffix"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when test="$spotName='SpotJ'">
					<xsl:text>  Spot</xsl:text>
					<xsl:value-of select="$moveSuffix"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when test="$spotMName='SpotML'">
					<xsl:text>  SpotM</xsl:text>
					<xsl:value-of select="$moveSuffix"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when test="$spotMName='SpotMJ'">
					<xsl:text>  SpotM</xsl:text>
					<xsl:value-of select="$moveSuffix"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when
					test="string($arcSeamData) != '' and string($arcWeldData) != '' and string($arcWeaveData) != ''">
					<xsl:choose>
						<xsl:when test="$motionType = 'Joint'">
							<xsl:text>  MoveJ </xsl:text>
						</xsl:when>
						<xsl:when test="$motionType = 'CircularVia'">
							<xsl:text>  ArcC </xsl:text>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('ARCDATAISSET'),string('1'))"/>
							<xsl:if test="$onoffstr">
								<xsl:value-of select="$onoffstr"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$motionType = 'Circular'">
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('ARCDATAISSET'),string('1'))"
							/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>  ArcL </xsl:text>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('ARCDATAISSET'),string('1'))"/>
							<xsl:if test="$onoffstr">
								<xsl:value-of select="$onoffstr"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when
					test="string($beadName) != ''">
					<xsl:choose>
						<xsl:when test="$motionType = 'Joint'">
							<xsl:text>  DispJ </xsl:text>
						</xsl:when>
						<xsl:when test="$motionType = 'CircularVia'">
							<xsl:text>  DispC </xsl:text>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('BEADDATAISSET'),string('1'))"/>
							<xsl:if test="$onoffstr">
								<xsl:value-of select="$onoffstr"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$motionType = 'Circular'">
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('BEADDATAISSET'),string('1'))"
							/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>  DispL </xsl:text>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string('BEADDATAISSET'),string('1'))"/>
							<xsl:if test="$onoffstr">
								<xsl:value-of select="$onoffstr"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="$spotActionType = 'RAPIDSpotWeld'">
							<xsl:text>  Spot</xsl:text>
							<xsl:value-of select="$moveSuffix"/>
							<xsl:text> </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="$targetType='Joint'">
									<xsl:text>  MoveAbsJ </xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="$motionType = 'Joint'">
											<xsl:text>  MoveJ </xsl:text>
										</xsl:when>
										<xsl:when test="$motionType = 'CircularVia'">
											<xsl:text>  MoveC </xsl:text>
										</xsl:when>
										<xsl:when test="$motionType = 'Circular'"> </xsl:when>
										<xsl:otherwise>
											<xsl:text>  MoveL </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:variable name="concstr"
								select="AttributeList/Attribute[AttributeName='Conc']/AttributeValue"/>
							<xsl:if test="$concstr='1'">
								<xsl:text>\Conc</xsl:text>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:call-template name="OutputExplicitOrRobotTargetData">
			<!-- Send the currect node, as the first parameter -->
			<xsl:with-param name="motionActivityNode" select="$motionActivityNode"/>
			<!-- Send the mode string, as the second parameter -->
			<xsl:with-param name="mode" select="$mode"/>
		</xsl:call-template>
		<xsl:if test="$motionType = 'CircularVia'">
			<!-- DLY Start 2008/05/31 only output tagName for MoveStatement -->
			<xsl:if test="$mode='MoveStatement'">
				<!-- DLY End 2008/05/31 -->
				<!-- DLY Start 2008/05/06 moved ; $cr into OutputExplicitOrRobotTargetData template...just output , here between via and target -->
				<xsl:if test="$explicitTarget='0'">
					<xsl:value-of select="$tagName"/>
				</xsl:if>
				<xsl:text>,</xsl:text>
				<!-- DLY End 2008/05/06 -->
			</xsl:if>
		</xsl:if>
		<xsl:if test="$mode = 'MoveStatement' and $motionType != 'CircularVia'">
			<xsl:variable name="retractnode" select="following-sibling::*[2]"/>
			<xsl:variable name="retractActionType" select="$retractnode/@ActionType"/>
			<xsl:variable name="spotnode" select="following-sibling::*[1]"/>
			<xsl:variable name="spotActionType" select="$spotnode/@ActionType"/>
			<xsl:variable name="spotActionID" select="$spotnode/@id"/>
			<xsl:variable name="spotGunData"
				select="MotionAttributes/UserProfile[@Type='ABBgundata']"/>
			<xsl:variable name="spotSpotData"
				select="MotionAttributes/UserProfile[@Type='ABBspotdata']"/>
			<xsl:variable name="spotSpotDataC5"
				select="MotionAttributes/UserProfile[@Type='ABBspotdataC5']"/>
			<xsl:variable name="moveName" select="ActivityName"/>
			<xsl:variable name="spotName" select="substring($moveName,1,5)"/>
			<xsl:variable name="studName" select="substring($moveName,1,5)"/>
			<xsl:variable name="paintName" select="substring($moveName,1,6)"/>
			<xsl:variable name="idstr"
				select="AttributeList/Attribute[AttributeName='ID']/AttributeValue"/>
			<xsl:variable name="arcDataSet"
			select="MotomanUtils:GetParameterData(string('ARCDATAISSET'))"/>
			<xsl:variable name="beadDataSet"
				select="MotomanUtils:GetParameterData(string('BEADDATAISSET'))"/>			
			<xsl:choose>
				<xsl:when test="$spotActionType = 'RAPIDSpotWeld'">
					<xsl:if test="$explicitTarget = '0'">
						<xsl:choose>
							<xsl:when test="contains($tagName,',')">
								<xsl:variable name="tagName"
									select="substring-before($tagName, ',')"/>
								<xsl:value-of select="$tagName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$tagName"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$spotnode/AttributeList">
						<!-- Call SetAttributes template to store attribute values for subsequent retrievals -->
						<xsl:call-template name="SetAttributes">
							<!-- Task name is used as a unique identifier of this ActivityList XML element -->
							<xsl:with-param name="id" select="$spotActionID"/>
							<!-- Send the AttributeList XML element as the second parameter -->
							<xsl:with-param name="attributeListNodeSet"
								select="$spotnode/AttributeList"/>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="string($idstr) != ''">
						<xsl:text>,\ID:=</xsl:text>
						<xsl:value-of select="$idstr"/>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$motionProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:variable name="spotdata"
						select="$spotnode/AttributeList/Attribute[AttributeName='spotdata']/AttributeValue"/>
					<xsl:variable name="gundata"
						select="$spotnode/AttributeList/Attribute[AttributeName='gundata']/AttributeValue"/>
					<xsl:variable name="InPos"
						select="$spotnode/AttributeList/Attribute[AttributeName='InPos']/AttributeValue"/>
					<xsl:variable name="NoConc"
						select="$spotnode/AttributeList/Attribute[AttributeName='NoConc']/AttributeValue"/>
					<xsl:variable name="Retract"
						select="$spotnode/AttributeList/Attribute[AttributeName='Retract']/AttributeValue"/>
					<xsl:variable name="OpenHLift"
						select="$spotnode/AttributeList/Attribute[AttributeName='OpenHLift']/AttributeValue"/>
					<xsl:variable name="CloseHLift"
					select="$spotnode/AttributeList/Attribute[AttributeName='CloseHLift']/AttributeValue"/>
					<xsl:variable name="QuickRelease"
						select="$spotnode/AttributeList/Attribute[AttributeName='QuickRelease']/AttributeValue"/>					
					<xsl:variable name="Conc"
						select="$spotnode/AttributeList/Attribute[AttributeName='Conc']/AttributeValue"/>
					<xsl:text>multi check</xsl:text>
					<xsl:choose>
						<!-- multi spot here -->
						<xsl:when
							test="count(MotionAttributes/UserProfile[@Type='ABBspotdata']) > 1 or count(MotionAttributes/UserProfile[@Type='ABBspotdataC5']) > 1 or string($spotGunData) = ''">
							<xsl:for-each select="MotionAttributes/UserProfile[@Type='ABBspotdata']">
								<xsl:text>\G</xsl:text>
								<xsl:value-of select="position()"/>
								<xsl:text>:=</xsl:text>
								<xsl:if test="string(.) != ''">
									<xsl:variable name="spotSpotBeforeDot"
										select="substring-before(.,'.')"/>
									<xsl:choose>
										<xsl:when test="$spotSpotBeforeDot">
											<xsl:value-of select="$spotSpotBeforeDot"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="."/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
							<xsl:for-each
								select="MotionAttributes/UserProfile[@Type='ABBspotdataC5']">
								<xsl:text>\G</xsl:text>
								<xsl:value-of select="position()"/>
								<xsl:text>:=</xsl:text>
								<xsl:if test="string(.) != ''">
									<xsl:variable name="spotSpotBeforeDot"
										select="substring-before(.,'.')"/>
									<xsl:choose>
										<xsl:when test="$spotSpotBeforeDot">
											<xsl:value-of select="$spotSpotBeforeDot"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="."/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
							<xsl:if test="$InPos = 1">
								<xsl:text>\InPos</xsl:text>
							</xsl:if>
							<xsl:if test="$NoConc = 1">
								<xsl:text>\NoConc</xsl:text>
							</xsl:if>
							<xsl:if test="$Conc = 1">
								<xsl:text>\Conc</xsl:text>
							</xsl:if>
							<xsl:if test="$retractActionType = 'RAPIDSpotRetract'">
								<xsl:variable name="separateRetract"
									select="MotomanUtils:GetParameterData('SeparateRetract')"/>
								<xsl:choose>
									<xsl:when test="$separateRetract = 'true'"> </xsl:when>
									<xsl:when test="$OpenHLift = 1">
										<xsl:text>\OpenHLift</xsl:text>
									</xsl:when>
									<xsl:when test="$CloseHLift = 1">
										<xsl:text>\CloseHLift</xsl:text>
									</xsl:when>
									<xsl:otherwise> </xsl:otherwise>
								</xsl:choose>
							</xsl:if>
							<xsl:if test="$QuickRelease = 1">
								<xsl:text>\QuickRelease</xsl:text>
							</xsl:if>							
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>not multi</xsl:text>
							<xsl:choose>
								<!-- new S4C+ or C5 -->
								<xsl:when test="number($rapidVersion) > 1">
									<xsl:choose>
										<xsl:when test="string($gundata) != ''">
											<xsl:value-of select="$gundata"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="string($spotGunData) != ''">
												<xsl:variable name="spotGunBeforeDot"
												select="substring-before($spotGunData,'.')"/>
												<xsl:choose>
												<xsl:when test="$spotGunBeforeDot">
												<xsl:value-of select="$spotGunBeforeDot"/>
												</xsl:when>
												<xsl:otherwise>
												<xsl:value-of select="$spotGunData"/>
												</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="string($spotdata) != ''">
											<xsl:value-of select="$spotdata"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="string($spotSpotData) != ''">
												<xsl:variable name="spotSpotBeforeDot"
												select="substring-before($spotSpotData,'.')"/>
												<xsl:choose>
												<xsl:when test="$spotSpotBeforeDot">
												<xsl:value-of select="$spotSpotBeforeDot"/>
												</xsl:when>
												<xsl:otherwise>
												<xsl:value-of select="$spotSpotData"/>
												</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:if test="$InPos = 1">
										<xsl:text>\InPos</xsl:text>
									</xsl:if>
									<xsl:if test="$NoConc = 1">
										<xsl:text>\NoConc</xsl:text>
									</xsl:if>
									<xsl:if test="$Conc = 1">
										<xsl:text>\Conc</xsl:text>
									</xsl:if>
									<xsl:if test="$retractActionType = 'RAPIDSpotRetract'">
										<xsl:variable name="separateRetract"
											select="MotomanUtils:GetParameterData('SeparateRetract')"/>
										<xsl:choose>
											<xsl:when test="$separateRetract = 'true'"> </xsl:when>
											<xsl:otherwise>
												<xsl:text>\Retract</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<!-- multi spot here -->
						<xsl:when
							test="count(MotionAttributes/UserProfile[@Type='ABBspotdata']) > 1 or count(MotionAttributes/UserProfile[@Type='ABBspotdataC5']) > 1  or string($spotGunData) = ''"> </xsl:when>
						<xsl:otherwise>
							<xsl:text>againnotmulti,</xsl:text>
							<xsl:choose>
								<!-- new S4C+ or C5 -->
								<xsl:when test="number($rapidVersion) > 1">
									<xsl:choose>
										<xsl:when test="string($spotdata) != ''">
											<xsl:value-of select="$spotdata"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="string($spotSpotData) != ''">
												<xsl:variable name="spotSpotBeforeDot"
												select="substring-before($spotSpotData,'.')"/>
												<xsl:choose>
												<xsl:when test="$spotSpotBeforeDot">
												<xsl:value-of select="$spotSpotBeforeDot"/>
												</xsl:when>
												<xsl:otherwise>
												<xsl:value-of select="$spotSpotData"/>
												</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:if test="$InPos = 1">
										<xsl:text>\InPos</xsl:text>
									</xsl:if>
									<xsl:if test="$NoConc = 1">
										<xsl:text>\NoConc</xsl:text>
									</xsl:if>
									<xsl:if test="$Conc = 1">
										<xsl:text>\Conc</xsl:text>
									</xsl:if>
									<xsl:if test="$retractActionType = 'RAPIDSpotRetract'">
										<xsl:variable name="separateRetract"
											select="MotomanUtils:GetParameterData('SeparateRetract')"/>
										<xsl:choose>
											<xsl:when test="$separateRetract = 'true'"> </xsl:when>
											<xsl:when test="$OpenHLift = 1">
												<xsl:text>\OpenHLift</xsl:text>
											</xsl:when>
											<xsl:when test="$CloseHLift = 1">
												<xsl:text>\CloseHLift</xsl:text>
											</xsl:when>
										</xsl:choose>
									</xsl:if>
									<xsl:if test="$QuickRelease = 1">
										<xsl:text>\QuickRelease</xsl:text>
									</xsl:if>									
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="string($gundata) != ''">
											<xsl:value-of select="$gundata"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="string($spotGunData) != ''">
												<xsl:variable name="spotGunBeforeDot"
												select="substring-before($spotGunData,'.')"/>
												<xsl:choose>
												<xsl:when test="$spotGunBeforeDot">
												<xsl:value-of select="$spotGunBeforeDot"/>
												</xsl:when>
												<xsl:otherwise>
												<xsl:value-of select="$spotGunData"/>
												</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:if test="$objectFrameProfile != 'Default'">
						<xsl:text>\WObj:=</xsl:text>
						<xsl:choose>
							<xsl:when test="contains($objectFrameProfile,'_ufprog')">
								<xsl:value-of
									select="substring-before($objectFrameProfile,'_ufprog')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$objectFrameProfile"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$retractActionType = 'RAPIDSpotRetract'">
						<xsl:variable name="separateRetract"
							select="MotomanUtils:GetParameterData('SeparateRetract')"/>
						<xsl:choose>
							<xsl:when test="$separateRetract = 'true'">
								<xsl:text>;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:variable name="gunExec"
									select="$retractnode/AttributeList/Attribute[AttributeName='GunExec']/AttributeValue"/>
								<xsl:variable name="gunCheck"
									select="$retractnode/AttributeList/Attribute[AttributeName='GunCheck']/AttributeValue"/>
								<xsl:variable name="toolName"
									select="$retractnode/AttributeList/Attribute[AttributeName='Toolname']/AttributeValue"/>
								<xsl:variable name="gunCommand"
									select="$retractnode/ActivityList/Activity/Target/JointTarget/HomeName"/>
								<xsl:text>  Gun</xsl:text>
								<xsl:value-of select="$gunCommand"/>
								<xsl:text> </xsl:text>
								<xsl:if test="$gunCommand != 'Close'">
									<xsl:value-of select="$gunExec"/>
									<xsl:text>,</xsl:text>
								</xsl:if>
								<xsl:value-of select="$gunCheck"/>
								<xsl:text>,</xsl:text>
								<xsl:value-of select="$toolName"/>
							</xsl:when>
							<xsl:otherwise> </xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:when>
				<xsl:when test="$studName='StudL'">
					<xsl:if test="$explicitTarget = '0'">
						<xsl:value-of select="$tagName"/>
					</xsl:if>
					<xsl:if test="string($idstr) != ''">
						<xsl:text>,\ID:=</xsl:text>
						<xsl:value-of select="$idstr"/>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$motionProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of
						select="AttributeList/Attribute[AttributeName='Stud Prog']/AttributeValue"/>
					<xsl:value-of
						select="AttributeList/Attribute[AttributeName='Stud Gun']/AttributeValue"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:if test="$objectFrameProfile != 'Default'">
						<xsl:text>\WObj:=</xsl:text>
						<xsl:choose>
							<xsl:when test="contains($objectFrameProfile,'_ufprog')">
								<xsl:value-of
									select="substring-before($objectFrameProfile,'_ufprog')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$objectFrameProfile"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:when>
				<xsl:when test="$paintName='PaintL'">
					<xsl:if test="$explicitTarget = '0'">
						<xsl:value-of select="$tagName"/>
					</xsl:if>
					<xsl:if test="string($idstr) != ''">
						<xsl:text>,\ID:=</xsl:text>
						<xsl:value-of select="$idstr"/>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$motionProfile"/>
					<xsl:value-of
						 select="AttributeList/Attribute[AttributeName='EventData']/AttributeValue"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$accuracyProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:if test="$objectFrameProfile != 'Default'">
						<xsl:text>\WObj:=</xsl:text>
						<xsl:choose>
							<xsl:when test="contains($objectFrameProfile,'_ufprog')">
								<xsl:value-of
									select="substring-before($objectFrameProfile,'_ufprog')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$objectFrameProfile"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:when>
				<xsl:when
					test="$moveName='ArcL' or $moveName='ArcL1' or $moveName='ArcL2' or $moveName='ArcLStart' or $moveName='ArcLEnd' or $moveName='ArcCStart' or $moveName='ArcCEnd' or $moveName='ArcC' or $arcDataSet='1'">
					<xsl:if test="$explicitTarget = '0'">
						<xsl:value-of select="$tagName"/>
					</xsl:if>
					<xsl:if test="string($idstr) != ''">
						<xsl:text>,\ID:=</xsl:text>
						<xsl:value-of select="$idstr"/>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$motionProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:variable name="arcSeamBeforeDot"
						select="substring-before($arcSeamData,'.')"/>
					<xsl:choose>
						<xsl:when test="string($arcSeamBeforeDot) != ''">
							<xsl:value-of select="$arcSeamBeforeDot"/>
							<xsl:text>,</xsl:text>
						</xsl:when>
						<xsl:when test="string($arcSeamData) != ''">
							<xsl:value-of select="$arcSeamData"/>
							<xsl:text>,</xsl:text>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
					<xsl:variable name="arcWeldBeforeDot"
						select="substring-before($arcWeldData,'.')"/>
					<xsl:choose>
						<xsl:when test="string($arcWeldBeforeDot) != ''">
							<xsl:value-of select="$arcWeldBeforeDot"/>
							<xsl:text>,</xsl:text>
						</xsl:when>
						<xsl:when test="string($arcWeldData) != ''">
							<xsl:value-of select="$arcWeldData"/>
							<xsl:text>,</xsl:text>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
					<xsl:variable name="arcWeaveBeforeDot"
						select="substring-before($arcWeaveData,'.')"/>
					<xsl:if
						test="$moveName!='ArcLStart' and $moveName!='ArcLEnd' and $moveName!='ArcCStart' and $moveName!='ArcCEnd'">
						<xsl:choose>
							<xsl:when test="string($arcWeaveBeforeDot) != ''">
								<xsl:value-of select="$arcWeaveBeforeDot"/>
								<xsl:text>,</xsl:text>
							</xsl:when>
							<xsl:when test="string($arcWeaveData) != ''">
								<xsl:value-of select="$arcWeaveData"/>
								<xsl:text>,</xsl:text>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
					</xsl:if>
					<xsl:value-of select="$accuracyProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:if test="$objectFrameProfile != 'Default'">
						<xsl:text>\WObj:=</xsl:text>
						<xsl:choose>
							<xsl:when test="contains($objectFrameProfile,'_ufprog')">
								<xsl:value-of
									select="substring-before($objectFrameProfile,'_ufprog')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$objectFrameProfile"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:when>
				<xsl:when
					test="$moveName='DispL' or $moveName='DispJ' or $moveName='DispC' or $beadDataSet='1'">
					<xsl:if test="$explicitTarget = '0'">
						<xsl:value-of select="$tagName"/>
					</xsl:if>
					<xsl:if test="string($idstr) != ''">
						<xsl:text>,\ID:=</xsl:text>
						<xsl:value-of select="$idstr"/>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$motionProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:if test="$motionType != 'Joint'and string($beadName) != ''">
						<xsl:value-of select="$beadName"/>
						<xsl:text>,</xsl:text>
					</xsl:if>
					<xsl:value-of select="$accuracyProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:if test="$objectFrameProfile != 'Default'">
						<xsl:text>\WObj:=</xsl:text>
						<xsl:choose>
							<xsl:when test="contains($objectFrameProfile,'_ufprog')">
								<xsl:value-of
									select="substring-before($objectFrameProfile,'_ufprog')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$objectFrameProfile"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:variable name="noeoffs"
						select="AttributeList/Attribute[AttributeName='NoEOffs']/AttributeValue"/>
					<xsl:if test="$explicitTarget = '0'">
						<xsl:choose>
							<xsl:when test="contains($tagName,',')">
								<xsl:variable name="tagName"
									select="substring-before($tagName, ',')"/>
								<xsl:value-of select="$tagName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$tagName"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$targetType='Joint' and $noeoffs='1'">
						<xsl:text>\NoEOffs</xsl:text>
					</xsl:if>
					<xsl:if test="string($idstr) != ''">
						<xsl:text>,\ID:=</xsl:text>
						<xsl:value-of select="$idstr"/>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$motionProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$accuracyProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:if
						test="$objectFrameProfile != 'Default' and $objectFrameProfile != 'wobj0'">
						<xsl:choose>
							<xsl:when test="$targetType='Joint' and $toolType='OnRobot'">
								<!-- omit WObj for MoveAbsJ when robot holds tool -->
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>\WObj:=</xsl:text>
								<xsl:choose>
									<xsl:when test="contains($objectFrameProfile,'_ufprog')">
										<xsl:value-of
											select="substring-before($objectFrameProfile,'_ufprog')"
										/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$objectFrameProfile"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$spotActionType = 'RAPIDSpotRetract'">
						<xsl:variable name="separateRetract"
							select="MotomanUtils:GetParameterData('SeparateRetract')"/>
						<xsl:choose>
							<xsl:when test="$separateRetract = 'true'">
								<xsl:text>;</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:variable name="gunExec"
									select="$spotnode/AttributeList/Attribute[AttributeName='GunExec']/AttributeValue"/>
								<xsl:variable name="gunCheck"
									select="$spotnode/AttributeList/Attribute[AttributeName='GunCheck']/AttributeValue"/>
								<xsl:variable name="toolName"
									select="$spotnode/AttributeList/Attribute[AttributeName='Toolname']/AttributeValue"/>
								<xsl:variable name="gunCommand"
									select="$spotnode/ActivityList/Activity/Target/JointTarget/HomeName"/>
								<xsl:text>  Gun</xsl:text>
								<xsl:value-of select="$gunCommand"/>
								<xsl:text> </xsl:text>
								<xsl:if test="$gunCommand != 'Close'">
									<xsl:value-of select="$gunExec"/>
									<xsl:text>,</xsl:text>
								</xsl:if>
								<xsl:value-of select="$gunCheck"/>
								<xsl:text>,</xsl:text>
								<xsl:value-of select="$toolName"/>
							</xsl:when>
							<xsl:otherwise> </xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:variable name="tagDownloaded"
			select="MotomanUtils:GetParameterData(string('TAGNAMEDOWNLOADED'))"/>
		<!-- DLY Start 2008/05/16 - must not output ; $cr here for declaration since it is already output in OutputExplicitOrRobotTargetData template -->
		<xsl:if test="($mode='MoveStatement')">
			<xsl:if test="$motionType != 'CircularVia'">
				<xsl:text>;</xsl:text>
				<xsl:value-of select="$cr"/>
			</xsl:if>
		</xsl:if>
		<!-- DLY End 2008/05/16 -->
	</xsl:template>
	<!-- end of TransformRobotMotionActivityData -->
	<xsl:template name="TransformFollowPathActivityData">
		<!-- Nodeset that contains all the descendants of follow path activity element -->
		<xsl:param name="followPathActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveAlongStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveAlongStatement' "> 
		element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<!-- Unique activity identifier -->
		<xsl:variable name="activityID" select="$followPathActivityNode/@id"/>
		<!-- Name of the part that contains geometric feature along which the path is defined -->
		<xsl:variable name="pathOnPart" select="$followPathActivityNode/PathOnPart"/>
		<!-- Controller profile values referenced by this activity -->
		<!-- Name of the referenced motion profile, as it appears in Controller element -->
		<xsl:variable name="motionProfile"
			select="$followPathActivityNode/PathMotionAttributes/PathMotionProfile"/>
		<!-- Name of the referenced accuracy profile, as it appears in Controller element -->
		<xsl:variable name="accuracyProfile"
			select="$followPathActivityNode/PathMotionAttributes/PathAccuracyProfile"/>
		<!-- Name of the referenced tool profile, as it appears in Controller element -->
		<xsl:variable name="toolProfile"
			select="$followPathActivityNode/PathMotionAttributes/PathToolProfile"/>
		<!-- Name of the referenced object frame profile, as it appears in Controller element -->
		<xsl:variable name="objectFrameName"
			select="$followPathActivityNode/PathMotionAttributes/PathObjectFrameProfile"/>
		<xsl:variable name="objectFrameReplaced"
			select="$followPathActivityNode/PathMotionAttributes/PathObjectFrameProfile"/>
		<xsl:choose>
			<xsl:when test="string($objectFrameReplaced) = ''">
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameName)"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData(string('OBJECTPROFILENAME'), $objectFrameReplaced)"
				/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="objectFrameProfile"
			select="MotomanUtils:GetParameterData('OBJECTPROFILENAME')"/>
		<!-- Motion profile values used by this activity -->
		<!-- This element determines the units for the following motion profile variables: 'Percent', 'Absolute', or 'Time' -->
		<xsl:variable name="motionBasis"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/MotionBasis"/>
		<!-- Robot's linear speed value -->
		<xsl:variable name="speed"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Speed/@Value"/>
		<!-- Robot's linear acceleration value -->
		<xsl:variable name="accel"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/Accel/@Value"/>
		<!-- Robot's angular speed value -->
		<xsl:variable name="angularSpeedValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularSpeedValue/@Value"/>
		<!-- Robot's angular acceleration value -->
		<xsl:variable name="angularAccelValue"
			select="/OLPData/Resource/Controller/MotionProfileList/MotionProfile[Name = $motionProfile]/AngularAccelValue/@Value"/>
		<!-- Accuracy profile values used by this activity -->
		<!-- Variable that determines whether rounding is turned on or off. Possible values: 'On' or 'Off' -->
		<xsl:variable name="flyByMode"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/FlyByMode"/>
		<!-- Rounding criterium: 'Speed' or 'Distance' -->
		<xsl:variable name="accuracyType"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyType"/>
		<!-- Rounding precision value -->
		<xsl:variable name="accuracyValue"
			select="/OLPData/Resource/Controller/AccuracyProfileList/AccuracyProfile[Name = $accuracyProfile]/AccuracyValue/@Value"/>
		<!-- Tool profile values used by this activity -->
		<!-- Tool type can be either 'Stationary' for fixed tcp case or 'OnRobot' for mobile tcp case -->
		<xsl:variable name="toolType"
			select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/ToolType"/>
		<!-- TCP location offset from robot's mount plate in X direction -->
		<xsl:variable name="tcpPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP location offset from robot's mount plate in Y direction -->
		<xsl:variable name="tcpPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP location offset from robot's mount plate in Z direction -->
		<xsl:variable name="tcpPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPPosition/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Yaw direction -->
		<xsl:variable name="tcpOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="tcpOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- TCP orientation offset from robot's mount plate in Roll direction -->
		<xsl:variable name="tcpOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ToolProfileList/ToolProfile[Name = $toolProfile]/TCPOffset/TCPOrientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame profile values used by this activity -->
		<!-- Object frame's (or UFRAME's) frame of reference: 'World' if object frame's offset is defined with respect to the world coordinate system or
			'RobotBase' if  object frame's offset is defined with respect to the robot's kinematics base coordinate system -->
		<xsl:variable name="referenceFrame"
			select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/@ReferenceFrame"/>
		<!-- Object frame's location offset in X direction -->
		<xsl:variable name="ofPositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame location offset from robot's mount plate in Y direction -->
		<xsl:variable name="ofPositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame location offset from robot's mount plate in Z direction -->
		<xsl:variable name="ofPositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFramePosition/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame orientation offset from robot's mount plate in Yaw  direction -->
		<xsl:variable name="ofOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame orientation offset from robot's mount plate in Pitch  direction -->
		<xsl:variable name="ofOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- ObjectFrame orientation offset from robot's mount plate in Roll  direction -->
		<xsl:variable name="ofOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name = $objectFrameProfile]/ObjectFrame/ObjectFrameOrientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Motion type of this follow path activity: Always 'Linear' -->
		<xsl:variable name="motionType"
			select="$followPathActivityNode/PathMotionAttributes/MotionType"/>
		<!-- Robot base offset with respect to the world coordinate system at the end of this path -->
		<!-- Robot base location offset in X direction -->
		<xsl:variable name="basePositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/BaseWRTWorld/Position/@X"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="basePositionX" select="$followPathActivityNode/BaseWRTWorld/Position/@X"/>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/BaseWRTWorld/Position/@Y"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="basePositionY" select="$followPathActivityNode/BaseWRTWorld/Position/@Y"/>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$followPathActivityNode/BaseWRTWorld/Position/@Z"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="basePositionZ" select="$followPathActivityNode/BaseWRTWorld/Position/@Z"/>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="basePositionYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$followPathActivityNode/BaseWRTWorld/Orientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="basePositionPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$followPathActivityNode/BaseWRTWorld/Orientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="basePositionRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$followPathActivityNode/BaseWRTWorld/Orientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Configuration string name. Config string is the same for all the nodes in the path. -->
		<xsl:variable name="configurationName" select="$followPathActivityNode/PathConfig/@Name"/>
		<!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models.
			Turn numbers do not change between the nodes in the path.  -->
		<!-- If turn numbers have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn number value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turnNumber1"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 1]/@Value"/>
		<!-- Turn number value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turnNumber4"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 4]/@Value"/>
		<!-- Turn number value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnNumber5"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 5]/@Value"/>
		<!-- Turn number value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turnNumber6"
			select="$followPathActivityNode/PathTurnNumber/PathTNJoint[@Number = 6]/@Value"/>
		<!-- Get all  turn signs. If the robot uses turn signs, there will be 4 joints that will always have turn signs enabled: 
			1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does for all robot models.
			Turn signs do not change between the nodes in the path.  -->
		<!-- If turn signs have been defined for this activity, return values, stored in variables below, will not be empty. 
			Before creating turn sign output in the result document, variables below need to be tested for validity in xsl:if element -->
		<!-- Turn sign value for joint 1, empty if doesn't exist -->
		<xsl:variable name="turnSign1"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 1]/@Value"/>
		<!-- Turn sign value for joint 4, empty if doesn't exist -->
		<xsl:variable name="turnSign4"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 4]/@Value"/>
		<!-- Turn sign value for joint 5, empty if doesn't exist -->
		<xsl:variable name="turnSign5"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 5]/@Value"/>
		<!-- Turn sign value for joint 6, empty if doesn't exist -->
		<xsl:variable name="turnSign6"
			select="$followPathActivityNode/PathTurnSign/PathTSJoint[@Number = 6]/@Value"/>
		<!-- Get node numbers, positions and orientations -->
		<xsl:if test="count($followPathActivityNode/PathNodeList/Node) > 0">
			<!-- For each node in the path, get -->
			<xsl:for-each select="$followPathActivityNode/PathNodeList/Node">
				<!-- Get node number -->
				<xsl:variable name="nodeNumber" select="@Number"/>
				<!-- Node position and orientation values. Node offset is always defined with respect to workcell's World coordinate system -->
				<!-- Node location offset in X direction -->
				<xsl:variable name="nodePositionX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$followPathActivityNode/PathNodeList/Node/NodePosition/@X"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node location offset in Y direction -->
				<xsl:variable name="nodePositionY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Y"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node location offset in Z direction -->
				<xsl:variable name="nodePositionZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$followPathActivityNode/PathNodeList/Node/NodePosition/@Z"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node orientation offset in Yaw direction -->
				<xsl:variable name="nodeOrientationYaw">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Yaw"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node orientation offset in Pitch direction -->
				<xsl:variable name="nodeOrientationPitch">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Pitch"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Node orientation offset in Roll direction -->
				<xsl:variable name="nodeOrientationRoll">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$followPathActivityNode/PathNodeList/Node/NodeOrientation/@Roll"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="xyzwpr"
					select="concat($nodePositionX, ',',  $nodePositionY,  ',' , $nodePositionZ , ',' , $nodeOrientationYaw , ',' , $nodeOrientationPitch , ',' , $nodeOrientationRoll )"/>
				<xsl:variable name="matresult" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
				<xsl:variable name="quatresult" select="MatrixUtils:dgMatrixToQuaternions()"/>
				<!-- Node quaternions -->
				<xsl:variable name="nodeQuaternion1">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ1()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="nodeQuaternion2">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ2()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="nodeQuaternion3">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ3()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="nodeQuaternion4">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ4()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Creation of this node target declaration -->
				<xsl:if test=" $mode = 'TargetDeclarationSection' ">
					<!-- Create node target declaration output -->
				</xsl:if>
				<!-- Create move along the path statement-->
				<xsl:if test=" $mode = 'MoveAlongStatement' ">
					<!-- Create move along statement output -->
					<xsl:choose>
						<xsl:when test="$motionType = 'Joint'">
							<xsl:text>  MoveJ </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>  MoveL </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>[[</xsl:text>
					<xsl:value-of select="format-number($nodePositionX,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodePositionY,$decimalNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodePositionZ,$decimalNumberPattern)"/>
					<xsl:text>],[</xsl:text>
					<xsl:value-of select="format-number($nodeQuaternion1,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodeQuaternion2,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodeQuaternion3,$targetNumberPattern)"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="format-number($nodeQuaternion4,$targetNumberPattern)"/>
					<xsl:text>],[0,0,0,</xsl:text>
					<xsl:choose>
						<xsl:when test="$configurationName = 'Config_1'">
							<xsl:text>0</xsl:text>
						</xsl:when>
						<xsl:when test="$configurationName = 'Config_2'">
							<xsl:text>1</xsl:text>
						</xsl:when>
						<xsl:when test="$configurationName = 'Config_3'">
							<xsl:text>2</xsl:text>
						</xsl:when>
						<xsl:when test="$configurationName = 'Config_4'">
							<xsl:text>3</xsl:text>
						</xsl:when>
						<xsl:when test="$configurationName = 'Config_5'">
							<xsl:text>6</xsl:text>
						</xsl:when>
						<xsl:when test="$configurationName = 'Config_6'">
							<xsl:text>7</xsl:text>
						</xsl:when>
						<xsl:when test="$configurationName = 'Config_7'">
							<xsl:text>4</xsl:text>
						</xsl:when>
						<xsl:when test="$configurationName = 'Config_8'">
							<xsl:text>5</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>0</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]],</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$motionProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$accuracyProfile"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="$toolProfile"/>
					<xsl:if test="$objectFrameProfile != 'Default'">
						<xsl:text>\WObj:=</xsl:text>
						<xsl:choose>
							<xsl:when test="contains($objectFrameProfile,'_ufprog')">
								<xsl:value-of
									select="substring-before($objectFrameProfile,'_ufprog')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$objectFrameProfile"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:text>;</xsl:text>
					<xsl:value-of select="$cr"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<!-- end of TransformFollowPathActivityData -->
	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNodeSet"/>
		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
			<xsl:variable name="postcomchk" select="substring($attrname,1,11)"/>
			<xsl:variable name="roblangchk" select="substring($attrvalue,1,15)"/>
			<xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
			<xsl:if
				test="($precomchk = 'PreComment' and $prefix = 'Pre') or ($postcomchk = 'PostComment' and 	$prefix = 'Post')">
				<xsl:choose>
					<xsl:when test="$roblangchk = 'Robot Language:'">
						<xsl:text>  </xsl:text>
						<xsl:value-of select="$striprobotlang"/>
						<xsl:value-of select="$cr"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>!   </xsl:text>
						<xsl:value-of select="$attrvalue"/>
						<xsl:value-of select="$cr"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end of processComments -->
	<!--OutputExplicitOrRobotTargetData template for output of target data in the declaration section or output of explicit target data into the motion command -->
	<xsl:template name="OutputExplicitOrRobotTargetData">
		<!-- Nodeset that contains all the descendants of robot motion activity element -->
		<xsl:param name="motionActivityNode"/>
		<!-- Format the output based on the value of mode parameter (TargetDeclarationSection or MoveStatement). Use
		<xsl:if test="$mode = 'TargetDeclarationSection ' "> element to output the motion values required for
		creation of target definition section (if any), or <xsl:if test="$mode = 'MoveStatement' "> element  to output robot move statement -->
		<!-- Variable that specifies processing mode for this template. Refer to the comment above -->
		<xsl:param name="mode"/>
		<xsl:variable name="targetDeclType"
			select="$motionActivityNode/AttributeList/Attribute[AttributeName='TargetDeclarationType']/AttributeValue"/>
		<xsl:variable name="targetType" select="string($motionActivityNode/Target/@Default)"/>
		<xsl:variable name="tagName" select="MotomanUtils:GetParameterData(string('TAGNAME'))"/>
		<xsl:variable name="explicitTarget"
			select="MotomanUtils:GetParameterData(string('EXPLICITTARGET'))"/>
		<xsl:variable name="downloadTagName" select="concat('Download Tag:', $tagName)"/>
		<xsl:variable name="tagNameDownloaded"
			select="MotomanUtils:GetParameterData(string($downloadTagName))"/>
		<xsl:variable name="currentProc"
			select="MotomanUtils:GetParameterData('CURRENT_PROCEDURE_NAME')"/>
		<xsl:variable name="tagGroup"
			select="string($motionActivityNode/Target/CartesianTarget/Tag/@TagGroup)"/>
		<xsl:variable name="hr" select="MotomanUtils:SetParameterData('TAGNAMEDOWNLOADED','0')"/>
		<xsl:variable name="basePositionX">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@X"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base location offset in Y direction -->
		<xsl:variable name="basePositionY">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@Y"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base location offset in Z direction -->
		<xsl:variable name="basePositionZ">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Position/@Z"/>
				<xsl:with-param name="Units" select="1000.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Yaw direction -->
		<xsl:variable name="baseOrientationYaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Yaw"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Pitch direction -->
		<xsl:variable name="baseOrientationPitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Pitch"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Robot base orientation offset in Roll direction -->
		<xsl:variable name="baseOrientationRoll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$motionActivityNode/Target/BaseWRTWorld/Orientation/@Roll"/>
				<xsl:with-param name="Units" select="1.0"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if
			test="(string($targetDeclType)='PERS' and string($currentProc)='MODULE_PERS_ONLY') or (string($targetType)='Joint' and string($currentProc)='MODULE_PERS_ONLY' and string($targetDeclType)!='') or (contains(string($tagGroup),string($currentProc))) or ($currentProc='MODULE_ALL_TARGETS') or ($mode != 'TargetDeclarationSection')">
			<!-- used to determine when a dummy target (empty TargetDeclarationType) exists....in this case no target is output in declaration section -->
                  <xsl:variable name="storedTargetTypeOutput" select="MotomanUtils:GetParameterData('STORETARGETTYPEOUTPUT')"/>
                  <xsl:variable name="targetTypeOutput">
				<xsl:choose>
					<xsl:when test="$targetDeclType">
					    <xsl:value-of select="$targetDeclType"/>
					</xsl:when>
                              <xsl:when test="string($storedTargetTypeOutput)!=''">
                                  <xsl:value-of select="$storedTargetTypeOutput"/>
                              </xsl:when>
					<xsl:otherwise>
						<xsl:text>PERS</xsl:text>
					</xsl:otherwise>
				</xsl:choose>					
			</xsl:variable>
                  <xsl:variable name="result" select="MotomanUtils:SetParameterData('STORETARGETTYPEOUTPUT', string($targetTypeOutput))"/>
			<!-- Cartesian target components -->
			<!-- If this target has been defined in cartesian coordinates -->
			<xsl:if test="$targetType = 'Cartesian'">
				<!-- Cartesian target position and orientation values. Target offset is defined with respect to object frame or if object frame
				offset is zero (x, y, z, roll, pitch, and yaw values are all set to 0) with respect to robot's base frame -->
				<!-- Target location offset in X direction -->
				<xsl:variable name="targetStartX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/CartesianTarget/Position/@X"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Target location offset in Y direction -->
				<xsl:variable name="targetStartY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/CartesianTarget/Position/@Y"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Target location offset in Z direction -->
				<xsl:variable name="targetStartZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/CartesianTarget/Position/@Z"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Target orientation offset in Yaw direction -->
				<xsl:variable name="targetOrientationYaw">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/CartesianTarget/Orientation/@Yaw"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Target orientation offset in Pitch direction -->
				<xsl:variable name="targetOrientationPitch">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/CartesianTarget/Orientation/@Pitch"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Target orientation offset in Roll direction -->
				<xsl:variable name="targetOrientationRoll">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/CartesianTarget/Orientation/@Roll"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="xyzwpr"
					select="concat($targetStartX, ',', $targetStartY, ',', 		$targetStartZ, ',', $targetOrientationYaw, ',', $targetOrientationPitch, ',', $targetOrientationRoll)"/>
				<xsl:variable name="objframeName"
					select="MotomanUtils:GetParameterData(string('OBJECTPROFILENAME'))"/>
				<!-- ObjectFrame location offset from robot's mount plate in X direction -->
				<xsl:variable name="objframex">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objframeName]/ObjectFrame/ObjectFramePosition/@X"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame location offset from robot's mount plate in Y direction -->
				<xsl:variable name="objframey">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objframeName]/ObjectFrame/ObjectFramePosition/@Y"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame location offset from robot's mount plate in Z direction -->
				<xsl:variable name="objframez">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objframeName]/ObjectFrame/ObjectFramePosition/@Z"/>
						<xsl:with-param name="Units" select="1000.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame orientation offset from robot's mount plate in Yaw  direction -->
				<xsl:variable name="objframew">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objframeName]/ObjectFrame/ObjectFrameOrientation/@Yaw"
						/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame orientation offset from robot's mount plate in Pitch  direction -->
				<xsl:variable name="objframep">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objframeName]/ObjectFrame/ObjectFrameOrientation/@Pitch"
						/>
					</xsl:call-template>
				</xsl:variable>
				<!-- ObjectFrame orientation offset from robot's mount plate in Roll  direction -->
				<xsl:variable name="objframer">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objframeName]/ObjectFrame/ObjectFrameOrientation/@Roll"
						/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="objframeTotal"
					select="number($objframex) + number($objframey) + number($objframez) + number($objframep) + number($objframew) + number($objframer)"/>
				<xsl:variable name="wobjtypename" select="concat('WOBJTYPE',$objframeName)"/>
				<xsl:variable name="wobjtype"
					select="MotomanUtils:GetParameterData(string($wobjtypename))"/>
				<xsl:choose>
					<xsl:when
						test="($worldCoords = '1' or $worldCoords = 'true') and $objframeTotal &lt; .001 and $wobjtype = 'OnRobot' and string($downloadCoordinates)!='Part'">
						<xsl:variable name="xyzwpr2"
							select="concat($basePositionX, ',', $basePositionY, ',', 	$basePositionZ, 	',', $baseOrientationYaw, ',', $baseOrientationPitch, ',', $baseOrientationRoll)"/>
						<!-- set base/world -->
						<xsl:variable name="matresult"
							select="MatrixUtils:dgXyzyprToMatrix($xyzwpr2)"/>
						<!-- concat tag/base -->
						<xsl:variable name="matresult2"
							select="MatrixUtils:dgCatXyzyprMatrix($xyzwpr)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="matresult"
							select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:variable name="quatresult" select="MatrixUtils:dgMatrixToQuaternions()"/>
				<!-- Target Position -->
				<xsl:variable name="targetPositionX">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetX()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="targetPositionY">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetY()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="targetPositionZ">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetZ()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Target quaternions -->
				<xsl:variable name="targetQuaternion1">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ1()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="targetQuaternion2">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ2()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="targetQuaternion3">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ3()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="targetQuaternion4">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="MatrixUtils:dgGetQ4()"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Configuration string name -->
				<xsl:variable name="configurationName"
					select="$motionActivityNode/Target/CartesianTarget/Config/@Name"/>
				<xsl:variable name="rapidcfg1"
					select="$motionActivityNode/AttributeList/Attribute[AttributeName = 'Rapid cf1']/AttributeValue"/>
				<xsl:variable name="rapidcfg4"
					select="$motionActivityNode/AttributeList/Attribute[AttributeName = 'Rapid cf4']/AttributeValue"/>
				<xsl:variable name="rapidcfg6"
					select="$motionActivityNode/AttributeList/Attribute[AttributeName = 'Rapid cf6']/AttributeValue"/>
				<xsl:variable name="rapidcfgx"
					select="$motionActivityNode/AttributeList/Attribute[AttributeName = 'Rapid cfx']/AttributeValue"/>
				<!-- Get all  turn numbers. If the robot uses turn numbers, there will be 4 joints that will always have turn numbers enabled: 
				1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does, for all robot models. -->
				<!-- If turn numbers have been defined for this target, return value, stored in variables below, will not be empty. 
				Before creating turn number output in the result document, variables below need to be tested for validity in xsl:if element -->
				<!-- Turn number value for joint 1, empty if doesn't exist -->
				<xsl:variable name="turnNumber1"
					select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 1]/@Value"/>
				<!-- Turn number value for joint 4, empty if doesn't exist -->
				<xsl:variable name="turnNumber4"
					select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 4]/@Value"/>
				<!-- Turn number value for joint 5, empty if doesn't exist -->
				<xsl:variable name="turnNumber5"
					select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 5]/@Value"/>
				<!-- Turn number value for joint 6, empty if doesn't exist -->
				<xsl:variable name="turnNumber6"
					select="$motionActivityNode/Target/CartesianTarget/TurnNumber/TNJoint[@Number = 6]/@Value"/>
				<!-- Get all  turn signs. If the robot uses turn signs, there will be 4 joints that will always have turn signs enabled: 
				1, 4, 5, and 6. Although some robots do not use all 4 values, V5 internally does for all robot models. -->
				<!-- If turn signs have been defined for this target, return value, stored in variables below, will not be empty. 
				Before creating turn sign output in the result document, variables below need to be tested for validity in xsl:if element -->
				<!-- Turn sign value for joint 1, empty if doesn't exist -->
				<xsl:variable name="turnSign1"
					select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 1]/@Value"/>
				<!-- Turn sign value for joint 4, empty if doesn't exist -->
				<xsl:variable name="turnSign4"
					select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 4]/@Value"/>
				<!-- Turn sign value for joint 5, empty if doesn't exist -->
				<xsl:variable name="turnSign5"
					select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 5]/@Value"/>
				<!-- Turn sign value for joint 6, empty if doesn't exist -->
				<xsl:variable name="turnSign6"
					select="$motionActivityNode/Target/CartesianTarget/TurnSign/TSJoint[@Number = 6]/@Value"/>
				<!-- RAPID GET JOINTVALUE FOR AXES 1,4,5, and 6 so RAPID CONFIGS can be calculated -->
				<xsl:variable name="joint1value">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/JointTarget/Joint[@DOFNumber = 1]/JointValue"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="joint4value">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/JointTarget/Joint[@DOFNumber = 4]/JointValue"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="joint5value">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/JointTarget/Joint[@DOFNumber = 5]/JointValue"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="joint6value">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$motionActivityNode/Target/JointTarget/Joint[@DOFNumber = 6]/JointValue"/>
						<xsl:with-param name="Units" select="1.0"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Retrieve tag name, and possibly, tag group name and the name of the part to which that tag group is attached to -->
				<!-- If this Cartesian target has a Tag element defined, tagName variable will not be empty, and if the tag is contained in a tag group
				which is attached to a part, tagGroupName and partOwnerName variables will not be empty.  Before creating tag output
				 in the result document, variables below need to be tested for validity in xsl:if element -->
				<!-- Tag name, empty if doesn't exist -->
				<!-- Tag group name that contains the above tag, empty if doesn't exist -->
				<xsl:variable name="tagGroupName"
					select="$motionActivityNode/Target/CartesianTarget/Tag/@TagGroup"/>
				<!-- Part name that has the above tag group attached, empty if doesn't exist -->
				<xsl:variable name="partOwnerName"
				select="$motionActivityNode/Target/CartesianTarget/Tag/@AttachedToPart"/>
				<!-- RAPID DLY ADDED xsl:if for TargetDeclaration -->
				<xsl:if test=" $mode = 'TargetDeclarationSection' or $explicitTarget = '1'">
					<xsl:if
						test="$tagName and substring($tagName,1,13) != 'abb_explicit_' and not($tagNameDownloaded) and $mode = 'TargetDeclarationSection' and $targetTypeOutput != ''">
						<xsl:text>  </xsl:text>
						<xsl:value-of select="$targetTypeOutput"/>
						<xsl:text> robtarget </xsl:text>
						<xsl:choose>
							<xsl:when test="contains($tagName,',')">
								<xsl:variable name="tagName"
									select="substring-before($tagName, ',')"/>
								<xsl:value-of select="$tagName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$tagName"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData(string($downloadTagName), string($tagName))"/>
						<!-- set parameter for checking in template TransformRobotMotionActivityData -->
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData('TAGNAMEDOWNLOADED','1')"/>
						<xsl:text>:=</xsl:text>
					</xsl:if>
					<xsl:if
						test="($explicitTarget='0' and not($tagNameDownloaded) and $targetTypeOutput != '') or ($explicitTarget = '1' and $mode = 'MoveStatement')">
						<xsl:text>[[</xsl:text>
						<xsl:value-of select="format-number($targetPositionX,$decimalNumberPattern)"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of select="format-number($targetPositionY,$decimalNumberPattern)"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of select="format-number($targetPositionZ,$decimalNumberPattern)"/>
						<xsl:text>],[</xsl:text>
						<xsl:value-of
							select="format-number($targetQuaternion1,$targetNumberPattern)"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of
							select="format-number($targetQuaternion2,$targetNumberPattern)"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of
							select="format-number($targetQuaternion3,$targetNumberPattern)"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of
							select="format-number($targetQuaternion4,$targetNumberPattern)"/>
						<xsl:text>],[</xsl:text>
						<!-- for now bypass forcing the rapid config....if someone asks for it...we'll come back to it
						<xsl:choose>
							<xsl:when test="string($rapidcfg1) != ''">
								<xsl:value-of select="$rapidcfg1"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="floor($joint1value div 90)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:choose>
							<xsl:when test="string($rapidcfg4) != ''">
								<xsl:value-of select="$rapidcfg4"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="floor($joint4value div 90)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:choose>
							<xsl:when test="string($rapidcfg6) != ''">
								<xsl:value-of select="$rapidcfg6"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="floor($joint6value div 90)"/>
							</xsl:otherwise>
						</xsl:choose>
						-->
						<xsl:choose>
							<xsl:when
								test="$joint1value &lt; .00009999 and $joint1value &gt; -.00009999">
								<xsl:text>0</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="floor($joint1value div 90)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:choose>
							<xsl:when
								test="$joint4value &lt; .00009999 and $joint4value &gt; -.00009999">
								<xsl:text>0</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="floor($joint4value div 90)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:choose>
							<xsl:when
								test="$joint6value &lt; .00009999 and $joint6value &gt; -.00009999">
								<xsl:text>0</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="floor($joint6value div 90)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:choose>
							<xsl:when test="contains($robotName,'540')">
								<xsl:choose>
									<xsl:when
										test="$joint5value &lt; .00009999 and $joint5value &gt; -.00009999">
										<xsl:text>0</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="floor($joint5value div 90)"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when
								test="contains($robotName, '1400') or contains($robotName, '2400') or contains($robotName, '3400') or contains($robotName, '4400') or contains($robotName, '6400')">
								<xsl:choose>
									<xsl:when test="string($rapidcfgx) != ''">
										<xsl:value-of select="$rapidcfgx"/>
									</xsl:when>								
									<xsl:otherwise>
										<xsl:text>0</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="$configurationName = 'Config_1'">
										<xsl:text>0</xsl:text>
									</xsl:when>
									<xsl:when test="$configurationName = 'Config_2'">
										<xsl:text>1</xsl:text>
									</xsl:when>
									<xsl:when test="$configurationName = 'Config_3'">
										<xsl:text>2</xsl:text>
									</xsl:when>
									<xsl:when test="$configurationName = 'Config_4'">
										<xsl:text>3</xsl:text>
									</xsl:when>
									<xsl:when test="$configurationName = 'Config_5'">
										<xsl:text>6</xsl:text>
									</xsl:when>
									<xsl:when test="$configurationName = 'Config_6'">
										<xsl:text>7</xsl:text>
									</xsl:when>
									<xsl:when test="$configurationName = 'Config_7'">
										<xsl:text>4</xsl:text>
									</xsl:when>
									<xsl:when test="$configurationName = 'Config_8'">
										<xsl:text>5</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>0</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>],[</xsl:text>
						<!-- Test if auxiliary targest exist, since this may affect the output -->
						<xsl:choose>
							<!-- Auxiliary targets exist -->
							<xsl:when
								test="count($motionActivityNode/Target/JointTarget/AuxJoint) > 0">
								<!-- Creation of Cartesian target declaration -->
								<xsl:if test=" $mode = 'TargetDeclarationSection' ">
									<!-- Create the Cartesian target declaration output taking into account that auxiliary target values need to be appended -->
								</xsl:if>
								<!-- Creation of Cartesian move statement -->
								<xsl:if test=" $mode = 'MoveStatement' ">
									<!-- Create the Cartesian move statement output taking into account that auxiliary target values need to be appended -->
								</xsl:if>
							</xsl:when>
							<!-- Auxiliary targets do not exist -->
							<xsl:otherwise>
								<!-- Creation of Cartesian target declaration -->
								<xsl:if test=" $mode = 'TargetDeclarationSection' ">
									<!-- Create the Cartesian target declaration output completely, since no auxiliary target values need to be appended -->
								</xsl:if>
								<!-- Creation of Cartesian move statement -->
								<xsl:if test=" $mode = 'MoveStatement' ">
									<!-- Create the Cartesian move statement output completely, since no auxiliary target values need to be appended -->
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			<!-- If this target has been defined in joint coordinates -->
			<xsl:if test="$targetType = 'Joint'">
				<!-- Get joint names, values, types and dof numbers-->
				<xsl:if test="count($motionActivityNode/Target/JointTarget/Joint) > 0">
					<xsl:if test=" $mode = 'TargetDeclarationSection' or $explicitTarget = '1'">
						<xsl:if
							test="$tagName and substring($tagName,1,13) != 'abb_explicit_' and not($tagNameDownloaded) and $mode = 'TargetDeclarationSection' and $targetTypeOutput != ''">
							<xsl:text>  </xsl:text>
                            <xsl:value-of select="$targetTypeOutput"/>
							<xsl:text> jointtarget </xsl:text>
							<xsl:value-of select="$tagName"/>
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData(string($downloadTagName), string($tagName))"/>
							<!-- set parameter for checking in template TransformRobotMotionActivityData -->
							<xsl:variable name="hr"
								select="MotomanUtils:SetParameterData('TAGNAMEDOWNLOADED','1')"/>
							<xsl:text>:=</xsl:text>
						</xsl:if>
						<xsl:if
							test="($explicitTarget = '0' and not($tagNameDownloaded) and $targetTypeOutput != '') or ($explicitTarget = '1' and $mode = 'MoveStatement')">
							<xsl:text>[[</xsl:text>
							<!-- For each base robot joint (non-auxiliary joint), get -->
							<xsl:for-each select="$motionActivityNode/Target/JointTarget/Joint">
								<!-- Joint name -->
								<xsl:variable name="jointName" select="@JointName"/>
								<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
								<xsl:variable name="jointType" select="@JointType"/>
								<!-- Degree of freedom number of this joint -->
								<xsl:variable name="dofNumber" select="@DOFNumber"/>
								<!-- Joint value -->
								<xsl:variable name="jointValue">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num" select="JointValue"/>
										<xsl:with-param name="Units" select="1.0"/>
									</xsl:call-template>
								</xsl:variable>
								<!-- Output joint value to the result document -->
								<!-- Test if auxiliary target exists, since this may affect the output -->
								<xsl:if
									test="$mode = 'TargetDeclarationSection' or $explicitTarget = '1'">
									<xsl:value-of
										select="format-number($jointValue,$decimalNumberPattern)"/>
									<xsl:if
										test="$dofNumber != count($motionActivityNode/Target/JointTarget/Joint)">
										<xsl:text>,</xsl:text>
									</xsl:if>
								</xsl:if>
								<xsl:choose>
									<!-- Auxiliary targets exist -->
									<xsl:when
										test="count($motionActivityNode/Target/JointTarget/AuxJoint) > 0">
										<!-- Creation of joint target declaration -->
										<xsl:if test=" $mode = 'TargetDeclarationSection' ">
											<!-- Create the joint target declaration output taking into account that auxiliary target values need to be appended -->
										</xsl:if>
										<!-- Creation of joint move statement -->
										<xsl:if test=" $mode = 'MoveStatement' ">
											<!-- Create the joint move statement output taking into account that auxiliary target values need to be appended -->
										</xsl:if>
									</xsl:when>
									<!-- Auxiliary targets do not exist -->
									<xsl:otherwise>
										<!-- Creation of joint target declaration -->
										<xsl:if test=" $mode = 'TargetDeclarationSection' ">
											<!-- Create the joint target declaration output completely, since no auxiliary target values need to be appended -->
										</xsl:if>
										<!-- Creation of joint move statement -->
										<xsl:if test=" $mode = 'MoveStatement' ">
											<!-- Create the joint move statement output completely, since no auxiliary target values need to be appended -->
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							<xsl:if
								test="$mode = 'TargetDeclarationSection' or $explicitTarget = '1'">
								<xsl:text>],[</xsl:text>
							</xsl:if>
						</xsl:if>
					</xsl:if>
					<!-- If robot is instructed to go to its home position -->
					<xsl:if test="$motionActivityNode/Target/JointTarget/HomeName">
						<!-- Get the home position name -->
						<xsl:variable name="homeName"
							select="$motionActivityNode/Target/JointTarget/HomeName"/>
						<!-- Creation of home target declaration -->
						<xsl:if test=" $mode = 'TargetDeclarationSection' ">
							<!-- Create home position declaration output -->
						</xsl:if>
						<!-- Creation of home move statement -->
						<xsl:if test=" $mode = 'MoveStatement' ">
							<!-- Create home position move statement output -->
						</xsl:if>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			<!-- If this robot target has auxiliary values -->
			<xsl:variable name="numAux"
				select="count($motionActivityNode/Target/JointTarget/AuxJoint)"/>
			<xsl:variable name="numRailAux"
				select="count($motionActivityNode/Target/JointTarget/AuxJoint[@Type='RailTrackGantry'])"/>
			<xsl:variable name="numToolAux"
				select="count($motionActivityNode/Target/JointTarget/AuxJoint[@Type='EndOfArmTooling'])"/>
			<xsl:variable name="numWorkAux"
				select="count($motionActivityNode/Target/JointTarget/AuxJoint[@Type='WorkpiecePositioner'])"/>
			<!-- DLY Start 2008/06/04 - change auxiliary axes output to use Toolgroup, Railgroup and Workgroup -->
			<xsl:if test="$numAux > '0'">
				<!-- For each auxiliary joint, get -->
				<xsl:if
					test=" (($explicitTarget='0' and $mode = 'TargetDeclarationSection' and not($tagNameDownloaded) and $targetTypeOutput != '') or ($explicitTarget = '1' and $mode= 'MoveStatement'))">
					<xsl:choose>
						<xsl:when
							test="(contains($railgroup,'1') and number($numRailAux) > 0) or (contains($toolgroup,'1') and number($numToolAux) > 0) or (contains($workgroup,'1') and number($numWorkAux) > 0) "> </xsl:when>
						<xsl:when
							test="(contains($railgroup,'2') and number($numRailAux) > 0) or (contains($toolgroup,'2') and number($numToolAux) > 0) or (contains($workgroup,'2') and number($numWorkAux) > 0) ">
							<xsl:text>9E+09,</xsl:text>
							<NumberIncrement:startnum start="1"/>
						</xsl:when>
						<xsl:when
							test="(contains($railgroup,'3') and number($numRailAux) > 0) or (contains($toolgroup,'3') and number($numToolAux) > 0) or (contains($workgroup,'3') and number($numWorkAux) > 0) ">
							<xsl:text>9E+09,9E+09,</xsl:text>
							<NumberIncrement:startnum start="2"/>
						</xsl:when>
						<xsl:when
							test="(contains($railgroup,'4') and number($numRailAux) > 0) or (contains($toolgroup,'4') and number($numToolAux) > 0) or (contains($workgroup,'4') and number($numWorkAux) > 0) ">
							<xsl:text>9E+09,9E+09,9E+09,</xsl:text>
							<NumberIncrement:startnum start="3"/>
						</xsl:when>
						<xsl:when
							test="(contains($railgroup,'5') and number($numRailAux) > 0) or (contains($toolgroup,'5') and number($numToolAux) > 0) or (contains($workgroup,'5') and number($numWorkAux) > 0) ">
							<xsl:text>9E+09,9E+09,9E+09,9E+09,</xsl:text>
							<NumberIncrement:startnum start="4"/>
						</xsl:when>
						<xsl:when
							test="(contains($railgroup,'6') and number($numRailAux) > 0) or (contains($toolgroup,'6') and number($numToolAux) > 0) or (contains($workgroup,'6') and number($numWorkAux) > 0) ">
							<xsl:text>9E+09,9E+09,9E+09,9E+09,9E+09,</xsl:text>
							<NumberIncrement:startnum start="5"/>
						</xsl:when>
					</xsl:choose>
				</xsl:if>
				<NumberIncrement:reset/>
				<xsl:for-each select="$motionActivityNode/Target/JointTarget/AuxJoint">
					<!-- Auxiliary joint name -->
					<xsl:variable name="auxJointName" select="@JointName"/>
					<!-- Joint non-restricted motion type: 'Translational' or 'Rotational' -->
					<xsl:variable name="jointType" select="@JointType"/>
					<!-- V5 type of auxiliary axis. Valid entries are: 'EndOfArmTooling', 'RailTrackGantry', and 'WorkpiecePositioner' -->
					<xsl:variable name="auxJointType" select="@Type"/>
					<!-- Degree of freedom number of this auxiliary joint. Has to be greater than the highest dof number of base robot joints -->
					<xsl:variable name="auxDofNumber" select="@DOFNumber"/>
					<!-- Auxiliary joint value -->
					<xsl:variable name="auxJointValue">
						<xsl:call-template name="Scientific">
							<xsl:with-param name="Num" select="JointValue"/>
							<xsl:with-param name="Units" select="1.0"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="auxDofCount" select="NumberIncrement:next()"/>
					<!-- Creation of auxiliary target declaration -->
					<xsl:choose>
						<xsl:when
							test="((($explicitTarget='0' and $mode = 'TargetDeclarationSection' and not($tagNameDownloaded) and $targetTypeOutput != '') or ($explicitTarget = '1' and $mode= 'MoveStatement')) and (($auxJointType = 'RailTrackGantry' and contains($railgroup,$auxDofCount)) or  ($auxJointType = 'EndOfArmTooling' and contains($toolgroup,$auxDofCount)) or ($auxJointType = 'WorkpiecePositioner' and contains($workgroup,$auxDofCount)) or (string($railgroup)='' and string($toolgroup)='' and string($workgroup)='')))">
							<!-- Create auxiliary target declaration output -->
							<xsl:choose>
								<xsl:when test="$jointType='Translational'">
									<xsl:value-of
										select="format-number($auxJointValue*1000.0,$decimalNumberPattern)"
									/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of
										select="format-number($auxJointValue,$decimalNumberPattern)"
									/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:variable name="auxDofCount" select="NumberIncrement:current()"/>
							<xsl:if
								test="($auxDofNumber != '12' and not (number($auxDofCount) = 6))">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if
								test="(($explicitTarget='0' and $mode = 'TargetDeclarationSection' and not($tagNameDownloaded) and $targetTypeOutput != '') or ($explicitTarget = '1' and $mode= 'MoveStatement'))">
								<xsl:text>9E+09</xsl:text>
								<xsl:if test="$auxDofNumber != '12'">
									<xsl:text>,</xsl:text>
								</xsl:if>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:if>
			<xsl:if
				test="(($explicitTarget='0' and $mode = 'TargetDeclarationSection' and not($tagNameDownloaded) and $targetTypeOutput != '') or ($explicitTarget = '1' and $mode= 'MoveStatement'))">
				<xsl:variable name="auxDofCount" select="NumberIncrement:current()"/>
				<xsl:choose>
					<xsl:when test="number($auxDofCount) = 6">
						<xsl:text>]]</xsl:text>
					</xsl:when>
					<xsl:when test="number($auxDofCount) = 4">
						<xsl:text>9E+09,</xsl:text>
					</xsl:when>
					<xsl:when test="number($auxDofCount) = 3">
						<xsl:text>9E+09,9E+09,</xsl:text>
					</xsl:when>
					<xsl:when test="number($auxDofCount) = 2">
						<xsl:text>9E+09,9E+09,9E+09,</xsl:text>
					</xsl:when>
					<xsl:when test="number($auxDofCount) = 1">
						<xsl:text>9E+09,9E+09,9E+09,9E+09,</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>9E+09,9E+09,9E+09,9E+09,9E+09,</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="number($auxDofCount)!=6">
					<xsl:choose>
						<xsl:when test="string($wobjOffset) != ''">
							<xsl:text>0]]</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>9E+09]]</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$mode = 'TargetDeclarationSection'">
					<xsl:text>;</xsl:text>
					<xsl:value-of select="$cr"/>
				</xsl:if>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!-- end of OutputExplicitOrRobotTargetData -->
	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:param name="Units" select="1"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'e')"/>
						<xsl:with-param name="e" select="substring-after($Num,'e')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:variable name="newNum">
					<xsl:call-template name="Scientific_Helper">
						<xsl:with-param name="m" select="substring-before($Num,'E')"/>
						<xsl:with-param name="e" select="substring-after($Num,'E')"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$newNum*$Units"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num*$Units"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of Scientific -->
	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- end of Scientific_Helper -->
	<xsl:template name="readMoc">
		<xsl:param name="line"/>
		<xsl:if test="$line">
			<xsl:if test="substring($line, 1, 6) = 'ROBOT:'">
				<xsl:variable name="storeRobot"
					select="MotomanUtils:SetParameterData('MOCRobotFound', 'TRUE')"/>
			</xsl:if>
			<xsl:if test="substring($line, 1, 7) = 'SINGLE:'">
				<xsl:variable name="storeSingle"
					select="MotomanUtils:SetParameterData('MOCSingleFound', 'TRUE')"/>
				<xsl:variable name="storeRobot"
					select="MotomanUtils:SetParameterData('MOCRobotFound', 'FALSE')"/>
			</xsl:if>
			<xsl:if test="substring($line, 1, 12) = 'SINGLE_TYPE:'">
				<xsl:variable name="storeSingle"
					select="MotomanUtils:SetParameterData('MOCSingleFound', 'FALSE')"/>
				<xsl:variable name="storeRobot"
					select="MotomanUtils:SetParameterData('MOCRobotFound', 'FALSE')"/>
			</xsl:if>
			<xsl:variable name="base_frame_pos_x"
				select="substring-after($line, '-base_frame_pos_x')"/>
			<xsl:variable name="base_frame_pos_y"
				select="substring-after($line, '-base_frame_pos_y')"/>
			<xsl:variable name="base_frame_pos_z"
				select="substring-after($line, '-base_frame_pos_z')"/>
			<xsl:variable name="base_frame_orient_u0"
				select="substring-after($line, '-base_frame_orient_u0')"/>
			<xsl:variable name="base_frame_orient_u1"
				select="substring-after($line, '-base_frame_orient_u1')"/>
			<xsl:variable name="base_frame_orient_u2"
				select="substring-after($line, '-base_frame_orient_u2')"/>
			<xsl:variable name="base_frame_orient_u3"
				select="substring-after($line, '-base_frame_orient_u3')"/>
			<xsl:variable name="base_frame_pos_x_value"
				select="substring( $base_frame_pos_x, 2, string-length($base_frame_pos_x)-1)"/>
			<xsl:variable name="base_frame_pos_y_value"
				select="substring( $base_frame_pos_y, 2, string-length($base_frame_pos_y)-1)"/>
			<xsl:variable name="base_frame_pos_z_value"
				select="substring( $base_frame_pos_z, 2, string-length($base_frame_pos_z)-1)"/>
			<xsl:variable name="base_frame_orient_u0_value"
				select="substring( $base_frame_orient_u0, 2, string-length($base_frame_orient_u0)-1)"/>
			<xsl:variable name="base_frame_orient_u1_value"
				select="substring( $base_frame_orient_u1, 2, string-length($base_frame_orient_u1)-1)"/>
			<xsl:variable name="base_frame_orient_u2_value"
				select="substring( $base_frame_orient_u2, 2, string-length($base_frame_orient_u2)-1)"/>
			<xsl:variable name="base_frame_orient_u3_value"
				select="substring( $base_frame_orient_u3, 2, string-length($base_frame_orient_u3)-1)"/>
			<xsl:if
				test="$base_frame_pos_x_value != '' and MotomanUtils:GetParameterData('MOCSingleFound') = 'TRUE'">
				<xsl:variable name="result"
					select="MotomanUtils:SetParameterData('trackBasePositionX', $base_frame_pos_x_value)"/>
				<xsl:if test="contains($base_frame_pos_x_value, '\')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionX', substring-before($base_frame_pos_x_value,'\'))"
					/>
				</xsl:if>
				<xsl:if test="contains($base_frame_pos_x_value,' ')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionX', substring-before($base_frame_pos_x_value,' '))"
					/>
				</xsl:if>
				<xsl:value-of select="$cr"/>
				<xsl:text>TRACK X POSITION:</xsl:text>
				<xsl:value-of select="MotomanUtils:GetParameterData('trackBasePositionX')"/>
				<xsl:value-of select="$cr"/>
			</xsl:if>
			<xsl:if
				test="$base_frame_pos_y_value != '' and MotomanUtils:GetParameterData('MOCSingleFound') = 'TRUE'">
				<xsl:variable name="result"
					select="MotomanUtils:SetParameterData('trackBasePositionY', $base_frame_pos_y_value)"/>
				<xsl:if test="contains($base_frame_pos_y_value, '\')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionY', substring-before($base_frame_pos_y_value,'\'))"
					/>
				</xsl:if>
				<xsl:if test="contains($base_frame_pos_y_value,' ')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionY', substring-before($base_frame_pos_y_value,' '))"
					/>
				</xsl:if>
			</xsl:if>
			<xsl:if
				test="$base_frame_pos_z_value != '' and MotomanUtils:GetParameterData('MOCSingleFound') = 'TRUE'">
				<xsl:variable name="result"
					select="MotomanUtils:SetParameterData('trackBasePositionZ', $base_frame_pos_z_value)"/>
				<xsl:if test="contains($base_frame_pos_z_value, '\')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionZ', substring-before($base_frame_pos_z_value,'\'))"
					/>
				</xsl:if>
				<xsl:if test="contains($base_frame_pos_z_value,' ')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionZ', substring-before($base_frame_pos_z_value,' '))"
					/>
				</xsl:if>
			</xsl:if>
			<xsl:if
				test="$base_frame_orient_u0_value != '' and MotomanUtils:GetParameterData('MOCRobotFound') = 'TRUE'">
				<xsl:variable name="result"
					select="MotomanUtils:SetParameterData('trackBasePositionQ1', $base_frame_orient_u0_value)"/>
				<xsl:if test="contains($base_frame_orient_u0_value, '\')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ1', substring-before($base_frame_orient_u0_value,'\'))"
					/>
				</xsl:if>
				<xsl:if test="contains($base_frame_orient_u0_value,' ')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ1', substring-before($base_frame_orient_u0_value,' '))"
					/>
				</xsl:if>
			</xsl:if>
			<xsl:if
				test="$base_frame_orient_u1_value != '' and MotomanUtils:GetParameterData('MOCRobotFound') = 'TRUE'">
				<xsl:variable name="result"
					select="MotomanUtils:SetParameterData('trackBasePositionQ2', $base_frame_orient_u1_value)"/>
				<xsl:if test="contains($base_frame_orient_u1_value, '\')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ2', substring-before($base_frame_orient_u1_value,'\'))"
					/>
				</xsl:if>
				<xsl:if test="contains($base_frame_orient_u1_value,' ')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ2', substring-before($base_frame_orient_u1_value,' '))"
					/>
				</xsl:if>
			</xsl:if>
			<xsl:if
				test="$base_frame_orient_u2_value != '' and MotomanUtils:GetParameterData('MOCRobotFound') = 'TRUE'">
				<xsl:variable name="result"
					select="MotomanUtils:SetParameterData('trackBasePositionQ3', $base_frame_orient_u2_value)"/>
				<xsl:if test="contains($base_frame_orient_u2_value, '\')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ3', substring-before($base_frame_orient_u2_value,'\'))"
					/>
				</xsl:if>
				<xsl:if test="contains($base_frame_orient_u2_value,' ')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ3', substring-before($base_frame_orient_u2_value,' '))"
					/>
				</xsl:if>
			</xsl:if>
			<xsl:if
				test="$base_frame_orient_u3_value != '' and MotomanUtils:GetParameterData('MOCRobotFound') = 'TRUE'">
				<xsl:variable name="result"
					select="MotomanUtils:SetParameterData('trackBasePositionQ4', $base_frame_orient_u3_value)"/>
				<xsl:if test="contains($base_frame_orient_u3_value, '\')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ4', substring-before($base_frame_orient_u3_value,'\'))"
					/>
				</xsl:if>
				<xsl:if test="contains($base_frame_orient_u3_value,' ')">
					<xsl:variable name="result"
						select="MotomanUtils:SetParameterData('trackBasePositionQ4', substring-before($base_frame_orient_u3_value,' '))"
					/>
				</xsl:if>
			</xsl:if>
			<xsl:call-template name="readMoc">
				<xsl:with-param name="line" select="FileUtils:fileLineRead()"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
