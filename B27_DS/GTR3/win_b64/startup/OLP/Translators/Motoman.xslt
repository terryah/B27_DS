<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lxslt="http://xml.apache.org/xslt" xmlns:java="http://xml.apache.org/xslt/java"
	xmlns:NumberIncrement="NumberIncrement" xmlns:MotomanUtils="MotomanUtils"
	xmlns:KIRStrUtils="KIRStrUtils"
	extension-element-prefixes="NumberIncrement MotomanUtils KIRStrUtils">
	<lxslt:component prefix="NumberIncrement" functions="next current" elements="startnum increment reset">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MotomanUtils" functions="JointToEncoderConversion SetParameterData GetParameterData" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MotomanUtils"/>
	</lxslt:component>
	<lxslt:component prefix="KIRStrUtils" functions="storeToolNum retrieveToolNum outputRCONF" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.KIRStrUtils"/>
	</lxslt:component>
	<xsl:strip-space elements="*"/>
	<xsl:output method="text"/>
	<xsl:variable name="debug" select="0"/>
	<xsl:variable name="version" select="'DELMIA CORP. MOTOMAN MRC INFORM II DOWNLOADER VERSION 5 RELEASE 24 SP2'"/>
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 1986-2014, ALL RIGHTS RESERVED'"/>
	<xsl:variable name="cr" select="'&#xa;'"/>
	<xsl:variable name="pvar" select="'P'"/>
	<xsl:variable name="bpvar" select="'BP'"/>
	<xsl:variable name="exvar" select="'EX'"/>
	<xsl:variable name="ufnumstart" select="'UserFrameNumberStart'"/>
	<xsl:variable name="numpat3" select="'000'"/>
	<xsl:variable name="numpat4" select="'0000'"/>
	<xsl:variable name="numpat5" select="'00000'"/>
	<xsl:variable name="jointPositionPattern" select="'#'"/>
	<xsl:variable name="cartesianPositionPattern" select="'#.###'"/>
	<xsl:variable name="patD1" select="'0.0'"/>
	<xsl:variable name="patD2" select="'0.00'"/>
	<xsl:variable name="patD3" select="'0.000'"/>
  <xsl:variable name="patD4" select="'0.0000'"/>
  <!-- xsl:variable name="patUptoD1" select="'0.0'"/ -->
	<!-- xsl:variable name="patUptoD2" select="'0.00'"/ -->
  <xsl:variable name="patUptoD1" select="'0.#'"/>
  <xsl:variable name="patUptoD2" select="'0.##'"/>
  <xsl:variable name="robotName" select="/OLPData/Resource/GeneralInfo/ResourceName"/>
	<xsl:variable name="rbtAxes" select="/OLPData/Resource/GeneralInfo/NumberOfRobotAxes"/>
	<xsl:variable name="auxAxes" select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<xsl:variable name="extAxes" select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<xsl:variable name="posAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="downloadCoord" select="/OLPData/Resource/GeneralInfo/DownloadCoordinates"/>
	<xsl:variable name="dof" select="$rbtAxes+$auxAxes+$extAxes+$posAxes"/>
  <xsl:variable name="controller" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Controller']/ParameterValue"/>
  <xsl:variable name="xrc_pl"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='XRCPositionLevel']/ParameterValue"/>
	<xsl:variable name="outputTools"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='OutputTools']/ParameterValue"/>
	<xsl:variable name="outputUFrames"
		select="/OLPData/Resource/ParameterList/Parameter[ParameterName='OutputUFrames']/ParameterValue"/>
	<xsl:variable name="dnldTgtParCvar">
		<xsl:call-template name="toupper">
			<xsl:with-param name="instring" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='DownloadTarget']/ParameterValue"/>
		</xsl:call-template>
	</xsl:variable>
	
	<xsl:variable name="fixedTCP_moves">
		<xsl:call-template name="FixedTCP_Moves"/>
	</xsl:variable>
	
	<xsl:variable name="downloadTarget">
		<xsl:choose>
			<xsl:when test="$fixedTCP_moves = 'true'">
				<xsl:text>Cartesian</xsl:text>
			</xsl:when>
			<xsl:when test="$dnldTgtParCvar='CARTESIAN' or $dnldTgtParCvar='RECTAN'">
				<xsl:text>Cartesian</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Pulse</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="dnldTgtParBCvar">
		<xsl:call-template name="toupper">
			<xsl:with-param name="instring" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='DownloadTargetBC']/ParameterValue"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="downloadTargetBC">
		<xsl:choose>
			<xsl:when test="$dnldTgtParBCvar='CARTESIAN' or $dnldTgtParBCvar='RECTAN'">
				<xsl:text>Cartesian</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Pulse</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="downloadStyle">
		<xsl:choose>
			<xsl:when
				test="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBIgpCallRobotTask']/DownloadStyle='File'">
				<xsl:text>File</xsl:text>
			</xsl:when>
			<xsl:when test="/OLPData/Resource/ProgramLogic/Jobs/Job/CallTask/@DownloadStyle='File'">
				<xsl:text>File</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>File</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template name="FixedTCP_Moves">
		<xsl:variable name="stationaryToolName" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[ToolType='Stationary']/Name"/>
		<xsl:choose>
			<xsl:when test="string-length($stationaryToolName) &gt; 0">
				<xsl:value-of select="/OLPData/Resource/ActivityList/Activity/MotionAttributes/ToolProfile=$stationaryToolName"/>
			</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="GetAccuracy">
		<xsl:param name="flybyMode" select="Off"/>
		<xsl:param name="accuracyValue" select="0"/>
		<xsl:param name="accuracyType" select="Speed"/>

		<xsl:variable name="mPL1a" select="MotomanUtils:GetParameterData('MotomanPL1')"/>
		<xsl:variable name="mPL1">
			<xsl:choose>
				<xsl:when test="string-length($mPL1a)=0">0.025</xsl:when>
				<xsl:when test="string(number($mPL1a))='NaN'">0.025</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$mPL1a div 1000"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="mPL2a" select="MotomanUtils:GetParameterData('MotomanPL2')"/>
		<xsl:variable name="mPL2">
			<xsl:choose>
				<xsl:when test="string-length($mPL2a)=0">0.05</xsl:when>
				<xsl:when test="string(number($mPL2a))='NaN'">0.05</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$mPL2a div 1000"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="mPL3a" select="MotomanUtils:GetParameterData('MotomanPL3')"/>
		<xsl:variable name="mPL3">
			<xsl:choose>
				<xsl:when test="string-length($mPL3a)=0">0.1</xsl:when>
				<xsl:when test="string(number($mPL3a))='NaN'">0.1</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$mPL3a div 1000"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="mPL4a" select="MotomanUtils:GetParameterData('MotomanPL4')"/>
		<xsl:variable name="mPL4">
			<xsl:choose>
				<xsl:when test="string-length($mPL4a)=0">0.15</xsl:when>
				<xsl:when test="string(number($mPL4a))='NaN'">0.15</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$mPL4a div 1000"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="mPL5a" select="MotomanUtils:GetParameterData('MotomanPL5')"/>
		<xsl:variable name="mPL5">
			<xsl:choose>
				<xsl:when test="string-length($mPL5a)=0">0.2</xsl:when>
				<xsl:when test="string(number($mPL5a))='NaN'">0.2</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$mPL5a div 1000"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="mPL6a" select="MotomanUtils:GetParameterData('MotomanPL6')"/>
		<xsl:variable name="mPL6">
			<xsl:choose>
				<xsl:when test="string-length($mPL6a)=0">0.3</xsl:when>
				<xsl:when test="string(number($mPL6a))='NaN'">0.3</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$mPL6a div 1000"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="mPL7a" select="MotomanUtils:GetParameterData('MotomanPL7')"/>
		<xsl:variable name="mPL7">
			<xsl:choose>
				<xsl:when test="string-length($mPL7a)=0">0.4</xsl:when>
				<xsl:when test="string(number($mPL7a))='NaN'">0.4</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$mPL7a div 1000"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$flybyMode = 'Off'">0</xsl:when>
			<xsl:when test="$xrc_pl = 8">
				<xsl:choose>
					<xsl:when test="$accuracyType = 'Speed'">
						<xsl:choose>
							<xsl:when test="$accuracyValue = 0">0</xsl:when>
							<xsl:when test="$accuracyValue &gt;  0.0 and $accuracyValue &lt;= 12.5">1</xsl:when>
							<xsl:when test="$accuracyValue &gt; 12.5 and $accuracyValue &lt;= 25.0">2</xsl:when>
							<xsl:when test="$accuracyValue &gt; 25.0 and $accuracyValue &lt;= 37.5">3</xsl:when>
							<xsl:when test="$accuracyValue &gt; 37.5 and $accuracyValue &lt;= 50.0">4</xsl:when>
							<xsl:when test="$accuracyValue &gt; 50.0 and $accuracyValue &lt;= 62.5">5</xsl:when>
							<xsl:when test="$accuracyValue &gt; 62.5 and $accuracyValue &lt;= 75.0">6</xsl:when>
							<xsl:when test="$accuracyValue &gt; 75.0 and $accuracyValue &lt;= 87.5">7</xsl:when>
							<xsl:otherwise>8</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise> <!-- accuracy type is distance -->
						<xsl:choose>
							<xsl:when test="$accuracyValue = 0">0</xsl:when>
							<xsl:when test="$accuracyValue &gt; 0.000 and $accuracyValue &lt;= $mPL1">1</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL1 and $accuracyValue &lt;= $mPL2">2</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL2 and $accuracyValue &lt;= $mPL3">3</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL3 and $accuracyValue &lt;= $mPL4">4</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL4 and $accuracyValue &lt;= $mPL5">5</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL5 and $accuracyValue &lt;= $mPL6">6</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL6 and $accuracyValue &lt;= $mPL7">7</xsl:when>
							<xsl:otherwise>8</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise> <!-- position level is 4 -->
				<xsl:choose>
					<xsl:when test="$accuracyType = 'Speed'">
						<xsl:choose>
							<xsl:when test="$accuracyValue = 0">0</xsl:when>
							<xsl:when test="$accuracyValue &gt;  0 and $accuracyValue &lt;= 25">1</xsl:when>
							<xsl:when test="$accuracyValue &gt; 25 and $accuracyValue &lt;= 50">2</xsl:when>
							<xsl:when test="$accuracyValue &gt; 50 and $accuracyValue &lt;= 75">3</xsl:when>
							<xsl:otherwise>4</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise> <!-- accuracy type is distance -->
						<xsl:choose>
							<xsl:when test="$accuracyValue = 0">0</xsl:when>
							<xsl:when test="$accuracyValue &gt; 0.000 and $accuracyValue &lt;= $mPL1">1</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL1 and $accuracyValue &lt;= $mPL2">2</xsl:when>
							<xsl:when test="$accuracyValue &gt; $mPL2 and $accuracyValue &lt;= $mPL3">3</xsl:when>
							<xsl:otherwise>4</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="/">
		<xsl:variable name="paramPulseZero">
			<xsl:choose>
				<xsl:when test="$downloadTarget = 'Cartesian'">0</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="chkParamPulseZero"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$paramPulseZero != 0">
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>Please correct parameter PulseValue and/or ZeroValue and re-download</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$fixedTCP_moves='true'">
					<xsl:choose>
						<xsl:when test="$dnldTgtParCvar='CARTESIAN' or $dnldTgtParCvar='RECTAN'"/>
						<xsl:otherwise>
							<xsl:value-of select="$cr"/>
							<xsl:text>ERROR INFO START</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>The program is downloaded in Cartesian because there are Fixed TCP moves in task.</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$cr"/>
							<xsl:text>ERROR INFO END</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- counter 0 is C/BC/EC var counter -->
				<NumberIncrement:counternum counter="0"/>
				<NumberIncrement:startnum start="-1"/>
				<NumberIncrement:increment inc="1"/>
				<NumberIncrement:reset/>
				<!-- counter 1 is P/BP/EX var counter -->
				<NumberIncrement:counternum counter="1"/>
				<NumberIncrement:startnum start="-1"/>
				<NumberIncrement:increment inc="1"/>
				<NumberIncrement:reset/>
				<!-- counter 2 counts duplicate P vars -->
				<NumberIncrement:counternum counter="2"/>
				<NumberIncrement:startnum start="-1"/>
				<NumberIncrement:increment inc="1"/>
				<NumberIncrement:reset/>
				<!-- counter 3 counts oscillation points -->
				<NumberIncrement:counternum counter="3"/>
				<NumberIncrement:startnum start="0"/>
				<NumberIncrement:increment inc="1"/>
				<NumberIncrement:reset/>

				<xsl:call-template name="HeaderInfo"/>
				<xsl:call-template name="InitializeParameters"/>
				<xsl:apply-templates select="OLPData"/>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template name="chkParamPulseZero">
		<xsl:variable name="paramList" select="/OLPData/Resource/ParameterList/Parameter"/>

		<!-- tree fragment: parameter PulseValue.? -->
		<xsl:variable name="p" select="'PulseValue.'"/>
		<xsl:variable name="pulseNodes"
			select="$paramList/ParameterValue[substring(../ParameterName, 1, string-length($p))=$p]"/>
		<!-- number of parameter PulseValue.? must equal dof -->
		<xsl:variable name="paramPulseStatus">
			<xsl:choose>
				<xsl:when test="count($pulseNodes) >= $dof">
					<xsl:value-of select="0"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="-1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!-- tree fragment: parameter ZeroValue.? -->
		<xsl:variable name="z" select="'ZeroValue.'"/>
		<xsl:variable name="ZeroNodes"
			select="$paramList/ParameterValue[substring(../ParameterName, 1, string-length($z))=$z]"/>
		<!-- number of parameter ZeroValue.? must equal dof -->
		<xsl:variable name="paramZeroStatus">
			<xsl:choose>
				<xsl:when test="count($ZeroNodes) >= $dof">
					<xsl:value-of select="0"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="-1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="chkStatus">
			<xsl:choose>
				<xsl:when test="($paramPulseStatus = 0) and ($paramZeroStatus = 0)">
					<xsl:value-of select="0"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="-1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:value-of select="$chkStatus"/>
	</xsl:template>

	<xsl:template name="HeaderInfo">
		<xsl:text>VERSION INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>VERSION INFO END</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>

	<xsl:template name="InitializeParameters">
		<xsl:for-each select="/OLPData/Resource/ParameterList/Parameter">
			<xsl:variable name="paramName" select="ParameterName"/>
			<xsl:variable name="paramValue" select="ParameterValue"/>
			<xsl:variable name="hr"	select="MotomanUtils:SetParameterData(string($paramName), string($paramValue))"/>
		</xsl:for-each>
		<xsl:variable name="hr"	select="MotomanUtils:SetParameterData('CONVEYOR_INITIAL_POSITION', '0.0')"/>
    <xsl:variable name="hs"	select="MotomanUtils:SetParameterData('CONVEYOR_INBOUND_OFFSET', '0.0')"/>

    <xsl:variable name="mPL1a" select="MotomanUtils:GetParameterData('MotomanPL1')"/>
		<xsl:if test="string(number($mPL1a))='NaN'">
			<xsl:call-template name="errorMsg_MotomanPL">
				<xsl:with-param name="param" select="'MotomanPL1'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="mPL2a" select="MotomanUtils:GetParameterData('MotomanPL2')"/>
		<xsl:if test="string(number($mPL2a))='NaN'">
			<xsl:call-template name="errorMsg_MotomanPL">
				<xsl:with-param name="param" select="'MotomanPL2'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="mPL3a" select="MotomanUtils:GetParameterData('MotomanPL3')"/>
		<xsl:if test="string(number($mPL3a))='NaN'">
			<xsl:call-template name="errorMsg_MotomanPL">
				<xsl:with-param name="param" select="'MotomanPL3'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="mPL4a" select="MotomanUtils:GetParameterData('MotomanPL4')"/>
		<xsl:if test="string(number($mPL4a))='NaN'">
			<xsl:call-template name="errorMsg_MotomanPL">
				<xsl:with-param name="param" select="'MotomanPL4'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="mPL5a" select="MotomanUtils:GetParameterData('MotomanPL5')"/>
		<xsl:if test="string(number($mPL5a))='NaN'">
			<xsl:call-template name="errorMsg_MotomanPL">
				<xsl:with-param name="param" select="'MotomanPL5'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="mPL6a" select="MotomanUtils:GetParameterData('MotomanPL6')"/>
		<xsl:if test="string(number($mPL6a))='NaN'">
			<xsl:call-template name="errorMsg_MotomanPL">
				<xsl:with-param name="param" select="'MotomanPL6'"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:variable name="mPL7a" select="MotomanUtils:GetParameterData('MotomanPL7')"/>
		<xsl:if test="string(number($mPL7a))='NaN'">
			<xsl:call-template name="errorMsg_MotomanPL">
				<xsl:with-param name="param" select="'MotomanPL7'"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="errorMsg_MotomanPL">
		<xsl:param name="param"/>
		<xsl:text>ERROR INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>Parameter </xsl:text>
		<xsl:value-of select="$param"/>
		<xsl:text> is not set to numeric value. Default value is used.</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>ERROR INFO END</xsl:text>
	</xsl:template>

  <xsl:template name="errorMsg_LineTrackCV">
    <xsl:text>ERROR INFO START</xsl:text>
    <xsl:value-of select="$cr"/>
    <xsl:text>For conveyor line tracking, Object Frame Profile name must be "CV#(?)",</xsl:text>
    <xsl:value-of select="$cr"/>
    <xsl:text>or Activity attribute "ConveyorNumber" must be setup.</xsl:text>
    <xsl:value-of select="$cr"/>
    <xsl:text>ERROR INFO END</xsl:text>
  </xsl:template>
  
	<xsl:template match="OLPData">
		<xsl:call-template name="programLogic">
			<xsl:with-param name="plm" select="Resource[GeneralInfo/ResourceName=$robotName]/ProgramLogic"/>
		</xsl:call-template>
		<xsl:apply-templates select="Resource[GeneralInfo/ResourceName=$robotName]/ActivityList">
			<xsl:with-param name="type" select="'job'"/>
		</xsl:apply-templates>
		<xsl:if test="$outputTools = 'true'">
			<xsl:call-template name="outputToolCND"/>
		</xsl:if>
		<!-- set parameter 'TOOL PROFILES USED' to a string containing the names of tools used -->
		<xsl:for-each select="/OLPData/Resource/ActivityList/Activity/MotionAttributes/ToolProfile">
			<xsl:variable name="toolsUsed" select="MotomanUtils:GetParameterData('TOOL PROFILES USED')"/>
			<xsl:if test="contains($toolsUsed, .) = false">
				<xsl:variable name="dummy" select="MotomanUtils:SetParameterData('TOOL PROFILES USED', concat($toolsUsed, ' ', .))"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="$outputUFrames = 'true' or $downloadCoord='Part'">
			<xsl:call-template name="outputUFrameCND"/>
		</xsl:if>
	</xsl:template>

	<!-- Program Logic section -->
	<xsl:template name="programLogic">
		<xsl:param name="plm"/>

		<xsl:if test="$plm">
			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START </xsl:text>
			<xsl:variable name="jobName"
				select="substring(translate($plm/Jobs/@Name, '.', ''), 1, 8)"/>
			<xsl:value-of select="$jobName"/>
			<xsl:text>.jbi</xsl:text>

			<xsl:value-of select="$cr"/>
			<xsl:text>/JOB</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>//NAME </xsl:text>
			<xsl:value-of select="$jobName"/>
			<xsl:value-of select="$cr"/>
			<xsl:text>//POS</xsl:text>
			<xsl:value-of select="$cr"/>

			<xsl:text>///NPOS 0,0,0,0,0,0</xsl:text>

			<xsl:value-of select="$cr"/>
			<xsl:text>//INST</xsl:text>

			<xsl:call-template name="outputDate"/>
			<xsl:call-template name="outputAttr"/>

			<xsl:value-of select="$cr"/>
			<xsl:text>NOP</xsl:text>

			<xsl:value-of select="$cr"/>
			<xsl:text>*start</xsl:text>

			<xsl:apply-templates select="$plm/Jobs/Job"/>

			<xsl:value-of select="$cr"/>
			<xsl:text>JUMP *start</xsl:text>

			<xsl:value-of select="$cr"/>
			<xsl:text>END</xsl:text>
			<xsl:if test="$downloadStyle = 'File'">
				<xsl:value-of select="$cr"/>
				<xsl:text>DATA FILE END</xsl:text>
			</xsl:if>
		</xsl:if>
		<!-- program logic exists -->

	</xsl:template>
	<!-- end of template programLogic -->

	<xsl:template match="Job">
		<xsl:variable name="numStartExpr" select="count(StartCondition/Expression)"/>
		<xsl:choose>
			<xsl:when test="$numStartExpr=1 and StartCondition/Expression='true'">
				<xsl:apply-templates select="SetOutput|WaitExpression|CallTask"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$cr"/>
				<xsl:text>JUMP *</xsl:text>
				<xsl:value-of select="./@Name"/>
				<xsl:text> IF </xsl:text>
				<xsl:apply-templates select="StartCondition/Expression">
					<xsl:with-param name="negate" select="1"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="SetOutput|WaitExpression|CallTask"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>*</xsl:text>
				<xsl:value-of select="./@Name"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Job -->

	<xsl:template match="Expression">
		<xsl:param name="negate"/>

		<xsl:choose>
			<xsl:when test="local-name(following-sibling::node()[1])='Operator'">
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>Motoman language does NOT support complex conditions</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
			</xsl:when>
			<xsl:when test="local-name(preceding-sibling::node()[1])='Operator'">(</xsl:when>
		</xsl:choose>

		<xsl:variable name="ioName" select="normalize-space(substring-before(., '='))"/>
		<xsl:variable name="ioVal" select="normalize-space(substring-after(., '='))"/>
		<xsl:variable name="ioNum" select="../../../../Inputs/Input[@InputName=$ioName]/@InputNumber"/>
		<xsl:call-template name="singleExpr">
			<xsl:with-param name="ioName" select="$ioName"/>
			<xsl:with-param name="ioVal" select="$ioVal"/>
			<xsl:with-param name="ioNum" select="$ioNum"/>
			<xsl:with-param name="negate" select="$negate"/>
		</xsl:call-template>

		<xsl:choose>
			<xsl:when test="local-name(following-sibling::node()[1])='Operator'">
				<xsl:text>) </xsl:text>
				<!-- output the next logical operator (and|or) -->
				<xsl:value-of select="following-sibling::node()[1]"/>
				<xsl:text> </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(preceding-sibling::node()[1])='Operator'">)</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Expression -->

	<xsl:template name="singleExpr">
		<xsl:param name="ioName"/>
		<xsl:param name="ioVal"/>
		<xsl:param name="ioNum"/>
		<xsl:param name="negate"/>

		<xsl:text>IN#(</xsl:text>
		<xsl:value-of select="$ioNum"/>
		<xsl:text>)=</xsl:text>

		<xsl:choose>
			<xsl:when test="contains($ioVal, '=')">
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:text>Motoman language does NOT support complex conditions</xsl:text>
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>

				<xsl:variable name="ioVal0" select="substring-before($ioVal, ' ')"/>
				<xsl:choose>
					<xsl:when test="$ioVal0='true'">ON</xsl:when>
					<xsl:otherwise>OFF</xsl:otherwise>
				</xsl:choose>

				<xsl:variable name="subExpr0" select="normalize-space(substring-after($ioVal, ' '))"/>
				<!-- logic operator -->
				<xsl:text> </xsl:text>
				<xsl:value-of select="substring-before($subExpr0, ' ')"/>
				<xsl:text> </xsl:text>

				<xsl:variable name="subExpr" select="normalize-space(substring-after($subExpr0, ' '))"/>
				<xsl:variable name="ioName1" select="normalize-space(substring-before($subExpr, '='))"/>
				<xsl:variable name="ioVal1" select="normalize-space(substring-after($subExpr, '='))"/>
				<xsl:variable name="ioNum1" select="../../../../Inputs/Input[@InputName=$ioName1]/@InputNumber"/>
				<xsl:call-template name="singleExpr">
					<xsl:with-param name="ioName" select="$ioName1"/>
					<xsl:with-param name="ioVal" select="$ioVal1"/>
					<xsl:with-param name="ioNum" select="$ioNum1"/>
					<xsl:with-param name="negate" select="$negate"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$negate=0">
						<xsl:choose>
							<xsl:when test="$ioVal='true'">ON</xsl:when>
							<xsl:otherwise>OFF</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$ioVal='true'">OFF</xsl:when>
							<xsl:otherwise>ON</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template singleExpr -->

	<xsl:template match="SetOutput">
		<xsl:variable name="ioName" select="normalize-space(substring-before(., '='))"/>
		<xsl:variable name="ioVal" select="normalize-space(substring-after(., '='))"/>
		<xsl:variable name="ioNum" select="../../../Outputs/Output[@OutputName=$ioName]/@OutputNumber"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>DOUT OT#(</xsl:text>
		<xsl:value-of select="$ioNum"/>
		<xsl:text>) </xsl:text>
		<xsl:choose>
			<xsl:when test="$ioVal='true'">ON</xsl:when>
			<xsl:otherwise>OFF</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="WaitExpression">
		<xsl:value-of select="$cr"/>
		<xsl:text>WAIT </xsl:text>
		<xsl:apply-templates select="Expression">
			<xsl:with-param name="negate" select="0"/>
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="CallTask">
		<xsl:variable name="callname" select="string(.)"/>
		<xsl:variable name="callType" select="string(/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList[@Task=$callname]/AttributeList/Attribute[AttributeName='Type']/AttributeValue)"/>
		<xsl:choose>
			<xsl:when test="contains($callname,'_FLATFILE') or string($callType) = 'flat'">
				<xsl:apply-templates select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList[@Task=$callname]">
					<xsl:with-param name="type" select="'flat'"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:when test="contains($callname,'TSYNC')">
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$callname"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$cr"/>
				<xsl:text>CALL JOB:</xsl:text>
				<xsl:value-of select="translate($callname, '.', '')"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="not(contains($callname,'_FLATFILE') and string($callType) != 'flat')">
			<xsl:variable name="condition" select="AttributeList/Attribute[starts-with(AttributeValue, 'Call Task Condition:')]/AttributeValue"/>
			<xsl:if test="$condition">
				<xsl:text> </xsl:text>
				<xsl:value-of select="substring-after($condition, ':')"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!-- end Program Logic section -->

	<xsl:template name="outputDate">
		<xsl:variable name="formatter"
			select="java:java.text.SimpleDateFormat.new('yyyy/MM/dd hh:mm')"/>
		<xsl:variable name="c" select="java:java.util.Calendar.getInstance()"/>
		<xsl:variable name="c2" select="java:getTime($c)"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>///DATE </xsl:text>
		<xsl:value-of select="java:format($formatter, $c2)"/>
	</xsl:template>

	<xsl:template name="outputAttr">
		<xsl:variable name="header_attr"
			select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Header_Attr']/ParameterValue"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>///ATTR </xsl:text>
		<xsl:choose>
			<xsl:when test="boolean($header_attr)">
				<xsl:value-of select="$header_attr"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>SC,RW</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="ActivityList">
		<xsl:param name="type"/>

		<xsl:variable name="activityListNode" select="current()"/>
		<xsl:variable name="taskType"
			select="string(AttributeList/Attribute[AttributeName='Type']/AttributeValue)"/>
		<xsl:if test="not(contains(@Task,'_FLATFILE') or $taskType = 'flat')">
			<NumberIncrement:counternum counter="0"/>
			<NumberIncrement:reset/>
			<!-- reset RCONF so it will be output to subprograms -->
			<xsl:variable name="dummyVar" select="KIRStrUtils:outputRCONF(-1, -1, -1, -1, -1, -1)"/>

      <xsl:variable name="jobName">
        <xsl:choose>
          <xsl:when test="$controller='DX100'">
            <xsl:value-of select="substring(translate(@Task, '.', ''), 1, 32)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="substring(translate(@Task, '.', ''), 1, 8)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

			<xsl:if
				test="$downloadStyle = 'File' or (boolean(/OLPData/Resource/ProgramLogic)=false and .=/OLPData/Resource/ActivityList[1])">
				<xsl:value-of select="$cr"/>
				<xsl:text>DATA FILE START </xsl:text>
				<!-- Job name limited to 8 characters -->
				<xsl:value-of select="$jobName"/>
				<xsl:text>.JBI</xsl:text>
			</xsl:if>

			<xsl:value-of select="$cr"/>
			<xsl:text>/JOB</xsl:text>
			<xsl:value-of select="$cr"/>
			<xsl:text>//NAME </xsl:text>
			<xsl:value-of select="$jobName"/>
			<xsl:value-of select="$cr"/>
			<xsl:text>//POS</xsl:text>
			<xsl:value-of select="$cr"/>

			<xsl:text>///NPOS </xsl:text>
			<xsl:variable name="robotMoveNodeSetPrecedingC"
				select="preceding-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and not(AttributeList/Attribute/AttributeName=$pvar)]"/>
			<xsl:variable name="robotMoveNodeSetPrecedingBP"
				select="preceding-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$bpvar]"/>
			<xsl:variable name="robotMoveNodeSetPrecedingEX"
				select="preceding-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$exvar]"/>
			<xsl:variable name="robotMoveNodeSetPrecedingP"
				select="preceding-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$pvar]"/>
			<xsl:variable name="robotMoveNodeSetFollowingC"
				select="following-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and not(AttributeList/Attribute/AttributeName=$pvar)]"/>
			<xsl:variable name="robotMoveNodeSetFollowingBP"
				select="following-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$bpvar]"/>
			<xsl:variable name="robotMoveNodeSetFollowingEX"
				select="following-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$exvar]"/>
			<xsl:variable name="robotMoveNodeSetFollowingP"
				select="following-sibling::ActivityList[(contains(@Task,'_FLATFILE') or string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'flat') and $activityListNode/Activity[@ActivityType='DNBIgpCallRobotTask']/CallName=@Task]/Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$pvar]"/>
			<xsl:variable name="robotMoveNodeSetC"
				select="Activity[@ActivityType='DNBRobotMotionActivity' and not(AttributeList/Attribute/AttributeName=$pvar)]"/>
			<xsl:variable name="robotOperationNodeSetC"
				select="Activity[@ActivityType='Operation' and AttributeList/Attribute[AttributeName='Type' and AttributeValue='oscillation']]"/>
			<xsl:variable name="robotMoveNodeSetP"
				select="Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$pvar]"/>
			<xsl:variable name="robotOperationNodeSetP"
				select="Activity[@ActivityType='Operation' and AttributeList/Attribute[AttributeName='Type' and AttributeValue='oscillation'] and AttributeList/Attribute/AttributeName=$pvar]"/>
			<xsl:variable name="robotMoveNodeSetBP"
				select="Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$bpvar]"/>
			<xsl:variable name="robotOperationNodeSetBP"
				select="Activity[@ActivityType='Operation' and AttributeList/Attribute[AttributeName='Type' and AttributeValue='oscillation'] and AttributeList/Attribute/AttributeName=$bpvar]"/>
			<xsl:variable name="robotMoveNodeSetEX"
				select="Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$exvar]"/>
			<xsl:variable name="robotOperationNodeSetEX"
				select="Activity[@ActivityType='Operation' and AttributeList/Attribute[AttributeName='Type' and AttributeValue='oscillation'] and AttributeList/Attribute/AttributeName=$exvar]"/>
			<xsl:variable name="numOfCPositions" select="count($robotMoveNodeSetPrecedingC|$robotMoveNodeSetC|$robotOperationNodeSetC|$robotMoveNodeSetFollowingC)"/>
			<xsl:variable name="numOfBCPositions">
				<xsl:choose>
					<xsl:when test="$auxAxes > 0">
						<xsl:value-of select="$numOfCPositions"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="numOfECPositions">
				<xsl:choose>
					<xsl:when test="$extAxes + $posAxes > 0">
						<xsl:value-of select="$numOfCPositions"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="duplicatePvar">
				<xsl:call-template name="getNumOfDuplicatePosVars">
					<xsl:with-param name="nodeprecedeSet" select="$robotMoveNodeSetPrecedingP"/>
					<xsl:with-param name="nodeSet" select="$robotMoveNodeSetP"/>
					<xsl:with-param name="nodefollowSet" select="$robotMoveNodeSetFollowingP"/>
					<xsl:with-param name="posVar" select="$pvar"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- number of non-zero object profiles -->
			<NumberIncrement:counternum counter="0"/>
			<NumberIncrement:startnum start="0"/>
			<!-- must reset for startnum -->
			<NumberIncrement:reset/>
			<!-- must reset for startnum -->
			<xsl:variable name="obj"
				select="/OLPData/Resource/Controller/ObjectFrameProfileList/ObjectFrameProfile"/>
			<xsl:for-each select="$obj">
				<xsl:if test="./@ApplyOffsetToTags='On'">
					<xsl:variable name="uframeName" select="Name"/>
					<xsl:variable name="posType">
						<xsl:call-template name="getPOSTYPE">
							<xsl:with-param name="uframeName" select="$uframeName"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:if test="$posType = 'USER'">
						<xsl:if
							test="count(/OLPData/Resource/ActivityList/Activity/MotionAttributes[ObjectFrameProfile=$uframeName]/ObjectFrameProfile) > 0">
							<xsl:variable name="dummy">
								<xsl:call-template name="getNumIncNext"/>
							</xsl:variable>
						</xsl:if>
					</xsl:if>
				</xsl:if>
			</xsl:for-each>
			<xsl:variable name="numOfObj">
				<xsl:call-template name="getNumIncCurrent"/>
			</xsl:variable>
			<xsl:variable name="numOfPPositions2"
				select="count($robotMoveNodeSetPrecedingP|$robotMoveNodeSetP|$robotMoveNodeSetFollowingP)-$duplicatePvar"/>
			<xsl:variable name="numOfPPositions">
				<xsl:choose>
					<xsl:when test="$downloadTarget = 'Cartesian'">
						<xsl:value-of select="$numOfPPositions2"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$numOfPPositions2 + $numOfObj"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- largest Pvar number, $ufnumstart = 'UserFrameNumberStart' -->
			<xsl:variable name="hr" select="MotomanUtils:SetParameterData($ufnumstart, 0)"/>
			<xsl:for-each select="$robotMoveNodeSetP">
				<xsl:variable name="pvarNum"
					select="AttributeList/Attribute[AttributeName=$pvar]/AttributeValue"/>
				<xsl:variable name="ufstart" select="MotomanUtils:GetParameterData($ufnumstart)"/>
				<xsl:if test="$pvarNum >= $ufstart">
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData($ufnumstart, $pvarNum+1)"/>
				</xsl:if>
			</xsl:for-each>
			<!-- set start to -1 for position var -->
			<NumberIncrement:counternum counter="0"/>
			<NumberIncrement:startnum start="-1"/>
			<!-- must reset for startnum -->
			<NumberIncrement:reset/>
			<!-- must reset for startnum -->
			<xsl:variable name="duplicateBPvar">
				<xsl:call-template name="getNumOfDuplicatePosVars">
					<xsl:with-param name="nodeprecedeSet" select="$robotMoveNodeSetPrecedingBP"/>
					<xsl:with-param name="nodeSet" select="$robotMoveNodeSetBP"/>
					<xsl:with-param name="nodefollowSet" select="$robotMoveNodeSetFollowingBP"/>
					<xsl:with-param name="posVar" select="$bpvar"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="numOfBPPositions">
				<xsl:choose>
					<xsl:when test="$auxAxes > 0">
						<xsl:value-of
							select="count($robotMoveNodeSetPrecedingBP|$robotMoveNodeSetBP|$robotMoveNodeSetFollowingBP)-$duplicateBPvar"
						/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="duplicateEXvar">
				<xsl:call-template name="getNumOfDuplicatePosVars">
					<xsl:with-param name="nodeprecedeSet" select="$robotMoveNodeSetPrecedingEX"/>
					<xsl:with-param name="nodeSet" select="$robotMoveNodeSetEX"/>
					<xsl:with-param name="nodefollowSet" select="$robotMoveNodeSetFollowingEX"/>
					<xsl:with-param name="posVar" select="$exvar"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="numOfEXPositions">
				<xsl:choose>
					<xsl:when test="$extAxes > 0">
						<xsl:value-of
							select="count($robotMoveNodeSetPrecedingEX|$robotMoveNodeSetEX|$robotMoveNodeSetFollowingEX)-$duplicateEXvar"
						/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:value-of select="$numOfCPositions"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$numOfBCPositions"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$numOfECPositions"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$numOfPPositions"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$numOfBPPositions"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$numOfEXPositions"/>
			<!-- end of ///NPOS output -->

			<xsl:variable name="posVarDigits"
				select="AttributeList/Attribute[AttributeName='PosVarDigits']/AttributeValue"/>
			<xsl:variable name="numpat">
				<xsl:choose>
					<xsl:when test="$posVarDigits = '3'">
						<xsl:value-of select="$numpat3"/>
					</xsl:when>
					<xsl:when test="$posVarDigits = '4'">
						<xsl:value-of select="$numpat4"/>
					</xsl:when>
					<xsl:when test="$posVarDigits = '5'">
						<xsl:value-of select="$numpat5"/>
					</xsl:when>
          <xsl:when test="$controller='NX100' or $controller='DX100'">
            <xsl:value-of select="$numpat5"/>
          </xsl:when>
          <xsl:otherwise>
						<xsl:value-of select="$numpat4"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="downloadPvar">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="AttributeList/Attribute[AttributeName='DownloadPvar']/AttributeValue"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="hr"	select="MotomanUtils:SetParameterData('DOWNLOADPVAR', $downloadPvar)"/>
			<xsl:variable name="downloadBPvar">
				<xsl:call-template name="toupper">
					<xsl:with-param name="instring" select="AttributeList/Attribute[AttributeName='DownloadBPvar']/AttributeValue"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="hr"	select="MotomanUtils:SetParameterData('DOWNLOADBPVAR', $downloadBPvar)"/>

			<xsl:choose>
				<xsl:when test="$downloadTarget = 'Cartesian'">
					<!-- Cartesian: Output position variables -->
					<xsl:call-template name="outputC_P_cart">
						<xsl:with-param name="rbtMoveNodeSetPrecede"
							select="$robotMoveNodeSetPrecedingC"/>
						<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetC"/>
						<xsl:with-param name="rbtMoveNodeSetFollow"
							select="$robotMoveNodeSetFollowingC"/>
						<xsl:with-param name="varName" select="'C'"/>
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!-- Pulses: Output position variables -->
					<xsl:call-template name="outputC_P">
						<xsl:with-param name="rbtMoveNodeSetPrecede"
							select="$robotMoveNodeSetPrecedingC"/>
						<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetC"/>
						<xsl:with-param name="rbtOperationNodeSet" select="$robotOperationNodeSetC"/>
						<xsl:with-param name="rbtMoveNodeSetFollow"
							select="$robotMoveNodeSetFollowingC"/>
						<xsl:with-param name="varName" select="'C'"/>
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:if test="$numOfBCPositions + $numOfECPositions > 0">
				<xsl:variable name="currentTool" select="KIRStrUtils:retrieveToolNum()"/>
				<xsl:if test="$currentTool != 0">
					<xsl:value-of select="$cr"/>
					<xsl:text>///TOOL 0</xsl:text>
					<xsl:value-of select="KIRStrUtils:storeToolNum(0)"/>
				</xsl:if>
			</xsl:if>

			<xsl:call-template name="outputBC_EC_BP_EX">
				<xsl:with-param name="rbtMoveNodeSetPrecede" select="$robotMoveNodeSetPrecedingC"/>
				<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetC"/>
				<xsl:with-param name="rbtOperationNodeSet" select="$robotOperationNodeSetC"/>
				<xsl:with-param name="rbtMoveNodeSetFollow" select="$robotMoveNodeSetFollowingC"/>
				<xsl:with-param name="varName" select="'BC'"/>
				<xsl:with-param name="numpat" select="$numpat"/>
			</xsl:call-template>

			<xsl:call-template name="outputBC_EC_BP_EX">
				<xsl:with-param name="rbtMoveNodeSetPrecede" select="$robotMoveNodeSetPrecedingC"/>
				<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetC"/>
				<xsl:with-param name="rbtOperationNodeSet" select="$robotOperationNodeSetC"/>
				<xsl:with-param name="rbtMoveNodeSetFollow" select="$robotMoveNodeSetFollowingC"/>
				<xsl:with-param name="varName" select="'EC'"/>
				<xsl:with-param name="numpat" select="$numpat"/>
			</xsl:call-template>
			
			<!-- output position variables: P, BP, and EX -->
			<xsl:choose>
				<xsl:when test="$downloadPvar = 'PULSE'">
					<xsl:call-template name="outputC_P">
						<xsl:with-param name="rbtMoveNodeSetPrecede"
							select="$robotMoveNodeSetPrecedingP"/>
						<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetP"/>
						<xsl:with-param name="rbtOperationNodeSet" select="$robotOperationNodeSetP"/>
						<xsl:with-param name="rbtMoveNodeSetFollow"
							select="$robotMoveNodeSetFollowingP"/>
						<xsl:with-param name="varName" select="'P'"/>
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
					<xsl:call-template name="outputBase">
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$downloadPvar = 'RECTAN' or $downloadPvar = 'CARTESIAN'">
					<!-- Cartesian: Output position variables -->
					<xsl:call-template name="outputC_P_cart">
						<xsl:with-param name="rbtMoveNodeSetPrecede"
							select="$robotMoveNodeSetPrecedingP"/>
						<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetP"/>
						<xsl:with-param name="rbtMoveNodeSetFollow"
							select="$robotMoveNodeSetFollowingP"/>
						<xsl:with-param name="varName" select="'P'"/>
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$downloadTarget = 'Cartesian'">
					<!-- Cartesian: Output position variables -->
					<xsl:call-template name="outputC_P_cart">
						<xsl:with-param name="rbtMoveNodeSetPrecede"
							select="$robotMoveNodeSetPrecedingP"/>
						<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetP"/>
						<xsl:with-param name="rbtMoveNodeSetFollow"
							select="$robotMoveNodeSetFollowingP"/>
						<xsl:with-param name="varName" select="'P'"/>
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="outputC_P">
						<xsl:with-param name="rbtMoveNodeSetPrecede"
							select="$robotMoveNodeSetPrecedingP"/>
						<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetP"/>
						<xsl:with-param name="rbtOperationNodeSet" select="$robotOperationNodeSetP"/>
						<xsl:with-param name="rbtMoveNodeSetFollow"
							select="$robotMoveNodeSetFollowingP"/>
						<xsl:with-param name="varName" select="'P'"/>
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
					<xsl:call-template name="outputBase">
						<xsl:with-param name="numpat" select="$numpat"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:if test="$numOfBPPositions + $numOfEXPositions > 0">
				<xsl:variable name="currentTool" select="KIRStrUtils:retrieveToolNum()"/>
				<xsl:if test="$currentTool != 0">
					<xsl:value-of select="$cr"/>
					<xsl:text>///TOOL 0</xsl:text>
					<xsl:value-of select="KIRStrUtils:storeToolNum(0)"/>
				</xsl:if>
			</xsl:if>

			<xsl:call-template name="outputBC_EC_BP_EX">
				<xsl:with-param name="rbtMoveNodeSetPrecede" select="$robotMoveNodeSetPrecedingBP"/>
				<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetBP"/>
				<xsl:with-param name="rbtOperationNodeSet" select="$robotOperationNodeSetBP"/>
				<xsl:with-param name="rbtMoveNodeSetFollow" select="$robotMoveNodeSetFollowingBP"/>
				<xsl:with-param name="varName" select="'BP'"/>
				<xsl:with-param name="numpat" select="$numpat"/>
			</xsl:call-template>

			<xsl:call-template name="outputBC_EC_BP_EX">
				<xsl:with-param name="rbtMoveNodeSetPrecede" select="$robotMoveNodeSetPrecedingEX"/>
				<xsl:with-param name="rbtMoveNodeSet" select="$robotMoveNodeSetEX"/>
				<xsl:with-param name="rbtOperationNodeSet" select="$robotOperationNodeSetEX"/>
				<xsl:with-param name="rbtMoveNodeSetFollow" select="$robotMoveNodeSetFollowingEX"/>
				<xsl:with-param name="varName" select="'EX'"/>
				<xsl:with-param name="numpat" select="$numpat"/>
			</xsl:call-template>

			<NumberIncrement:counternum counter="0"/>
			<NumberIncrement:reset/>

			<xsl:if test="not(contains(@Task,'_FLATFILE') or $taskType = 'flat') or $type='flat'">
				<xsl:call-template name="processVarDefComments">
					<xsl:with-param name="attributeListNode" select="AttributeList"/>
				</xsl:call-template>
			</xsl:if>

			<xsl:value-of select="$cr"/>
			<xsl:text>//INST</xsl:text>

			<xsl:call-template name="outputDate"/>
			<!--
		<xsl:call-template name="outputAttr"/>

		<xsl:value-of select="$cr"/>
		<xsl:text>///GROUP1 RB1</xsl:text>
		-->
		</xsl:if>
		<!-- _FLATFILE -->
		<xsl:if test="not(contains(@Task,'_FLATFILE') or $taskType = 'flat') or $type='flat'">
			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Pre'"/>
				<xsl:with-param name="attributeListNode" select="AttributeList"/>
				<xsl:with-param name="instAttr" select="1"/>
				<xsl:with-param name="type" select="$type"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="not(contains(@Task,'_FLATFILE') or $taskType = 'flat')">
			<xsl:value-of select="$cr"/>
			<xsl:text>NOP</xsl:text>
		</xsl:if>
		<!-- _FLATFILE -->
		<xsl:if test="not(contains(@Task,'_FLATFILE') or $taskType = 'flat') or $type='flat'">
			<xsl:apply-templates select="Activity | Action"/>

			<xsl:call-template name="processComments">
				<xsl:with-param name="prefix" select="'Post'"/>
				<xsl:with-param name="attributeListNode" select="AttributeList"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="not(contains(@Task,'_FLATFILE') or $taskType = 'flat')">
			<xsl:value-of select="$cr"/>
			<xsl:text>END</xsl:text>
			<xsl:if test="$downloadStyle = 'File' or .=/OLPData/Resource/ActivityList[last()]">
				<xsl:value-of select="$cr"/>
				<xsl:text>DATA FILE END</xsl:text>
			</xsl:if>
		</xsl:if>
		<!-- _FLATFILE -->
	</xsl:template>
	<!-- end of template ActivityList -->

	<xsl:template match="Action">
		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNode" select="AttributeList"/>
		</xsl:call-template>
		<xsl:variable name="gunNumFromParameter">
			<xsl:call-template name="getToolNum">
				<xsl:with-param name="toolName" select="ToolResource/ResourceName"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="gunNum">
			<xsl:choose>
				<xsl:when test="$gunNumFromParameter &gt; 0">
					<xsl:value-of select="$gunNumFromParameter"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of
						select="AttributeList/Attribute[AttributeName = 'GUN#']/AttributeValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:if test="@ActionType = 'MRCSpotWeld'">
			<xsl:choose>
				<xsl:when test="$extAxes = 0">
					<xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO START</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>Robot must have an external axis to download Servo Gun program.</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO END</xsl:text>
					<xsl:text># ERROR !!!</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text># Robot must have an external axis to download Servo Gun program.</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text># ERROR !!!</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="clfval"
						select="string(AttributeList/Attribute[AttributeName = 'CLF#']/AttributeValue)"/>
					<xsl:if test="$clfval = ''">
						<xsl:value-of select="$cr"/>
						<xsl:text>SVSPOT GUN#(</xsl:text>
						<xsl:value-of select="$gunNum"/>
						<xsl:text>) PRESS#(</xsl:text>
						<xsl:variable name="press_num"
							select="AttributeList/Attribute[AttributeName = 'PRESS#']/AttributeValue"/>
						<xsl:variable name="press_var"
							select="AttributeList/Attribute[AttributeName = 'PRESS_V#']/AttributeValue"/>
						<xsl:choose>
							<xsl:when test="$press_num != '-1'">
								<xsl:value-of select="$press_num"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$press_var"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>) WTM=</xsl:text>
						<xsl:variable name="wtm_num"
							select="AttributeList/Attribute[AttributeName = 'WTM']/AttributeValue"/>
						<xsl:variable name="wtm_var"
							select="AttributeList/Attribute[AttributeName = 'WTM_V']/AttributeValue"/>
						<xsl:choose>
							<xsl:when test="$wtm_num != '-1'">
								<xsl:value-of select="$wtm_num"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$wtm_var"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> WST=</xsl:text>
						<xsl:value-of
							select="AttributeList/Attribute[AttributeName = 'WST']/AttributeValue"/>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="@ActionType = 'MRCSpotDry'">
			<xsl:choose>
				<xsl:when test="$extAxes = 0">
					<xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO START</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>Robot must have an external axis to download Servo Gun program.</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text>ERROR INFO END</xsl:text>
					<xsl:text># ERROR !!!</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text># Robot must have an external axis to download Servo Gun program.</xsl:text>
					<xsl:value-of select="$cr"/>
					<xsl:text># ERROR !!!</xsl:text>
				</xsl:when>
				<xsl:otherwise>
          <xsl:value-of select="$cr"/>
          <xsl:text>SVGUNCL GUN#(</xsl:text>
					<xsl:value-of select="$gunNum"/>
					<xsl:text>) PRESSCL#(</xsl:text>
					<xsl:value-of
						select="AttributeList/Attribute[AttributeName = 'PRESSCL#']/AttributeValue"/>
					<xsl:text>)</xsl:text>
					<xsl:variable name="twc"
						select="AttributeList/Attribute[AttributeName = 'TWC']/AttributeValue"/>
					<xsl:choose>
						<xsl:when test="$twc = ''"/>
						<xsl:when test="$twc = 'A' or $twc = 'B' or $twc = 'C'">
							<xsl:text> TWC-</xsl:text>
							<xsl:value-of select="$twc"/>
						</xsl:when>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<xsl:if test="@ActionType = 'MRCSpotToolPick'">
      <xsl:value-of select="$cr"/>
      <xsl:text>GUNCHG GUN#(</xsl:text>
			<xsl:value-of select="$gunNum"/>
			<xsl:text>) PICK</xsl:text>
		</xsl:if>

		<xsl:if test="@ActionType = 'MRCSpotToolPlace'">
      <xsl:value-of select="$cr"/>
      <xsl:text>GUNCHG GUN#(</xsl:text>
			<xsl:value-of select="$gunNum"/>
			<xsl:text>) PLACE</xsl:text>
		</xsl:if>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNode" select="AttributeList"/>
		</xsl:call-template>

	</xsl:template>
	<!-- end of template Action -->

	<xsl:template match="Activity">
		<xsl:variable name="StationGroup">
			<xsl:choose>
				<xsl:when test="string(ancestor::ActivityList/AttributeList/Attribute[AttributeName='StationGroup']/AttributeValue)!=''">
					<xsl:value-of select="number(ancestor::ActivityList/AttributeList/Attribute[AttributeName='StationGroup']/AttributeValue)"/>
				</xsl:when>
				<xsl:when test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='StationGroup']/ParameterValue)!=''">
					<xsl:value-of select="number(/OLPData/Resource/ParameterList/Parameter[ParameterName='StationGroup']/ParameterValue)"/>
				</xsl:when>
				<xsl:when test="contains(string(ancestor::ActivityList/AttributeList/Attribute[starts-with(AttributeName,'PreComment') and contains(AttributeValue,'GROUP2')]/AttributeValue),'ST')">
          <xsl:text>2</xsl:text>
        </xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="BaseGroup">
			<xsl:choose>
				<xsl:when test="string(ancestor::ActivityList/AttributeList/Attribute[AttributeName='BaseGroup']/AttributeValue)!=''">
					<xsl:value-of select="number(ancestor::ActivityList/AttributeList/Attribute[AttributeName='BaseGroup']/AttributeValue)"/>
				</xsl:when>
				<xsl:when test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='BaseGroup']/ParameterValue)!=''">
					<xsl:value-of select="number(/OLPData/Resource/ParameterList/Parameter[ParameterName='BaseGroup']/ParameterValue)"/>
				</xsl:when>
				<xsl:when test="contains(string(ancestor::ActivityList/AttributeList/Attribute[starts-with(AttributeName,'PreComment') and contains(AttributeValue,'GROUP2')]/AttributeValue),'BS')">
          <xsl:text>2</xsl:text>
        </xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="currentTask" select="ancestor::ActivityList/@Task"/>
		<xsl:variable name="motionProfile" select="MotionAttributes/MotionProfile"/>
		<xsl:variable name="speedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Value"/>
		<xsl:variable name="angularSpeedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/AngularSpeedValue/@Value"/>
		<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
		<xsl:variable name="flybyMode"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/FlyByMode"/>
		<xsl:variable name="accuracyValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyValue/@Value"/>
		<xsl:variable name="accuracyType"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyType"/>
		<xsl:variable name="speedUnits"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Units "/>
		<xsl:variable name="maxSpeedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
		<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
		<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
		<xsl:variable name="signalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>
		<xsl:variable name="outputPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
		<xsl:variable name="inputPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
		<xsl:variable name="maxWaitTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>
		<xsl:variable name="objectName" select="MotionAttributes/ObjectFrameProfile"/>
		<xsl:variable name="toolName" select="MotionAttributes/ToolProfile"/>
		<xsl:variable name="toolType" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/ToolType"/>
		<xsl:variable name="toolNum">
			<xsl:call-template name="matchToolProfileName">
				<xsl:with-param name="nameToMatch" select="$toolName"/>
				<xsl:with-param name="toolProfileNodeSet" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="posVarDigits" select="../AttributeList/Attribute[AttributeName='PosVarDigits']/AttributeValue"/>
		<xsl:variable name="numpat">
			<xsl:choose>
				<xsl:when test="$posVarDigits = '3'">
					<xsl:value-of select="$numpat3"/>
				</xsl:when>
				<xsl:when test="$posVarDigits = '4'">
					<xsl:value-of select="$numpat4"/>
				</xsl:when>
				<xsl:when test="$posVarDigits = '5'">
					<xsl:value-of select="$numpat5"/>
				</xsl:when>
        <xsl:when test="$controller='NX100' or $controller='DX100'">
          <xsl:value-of select="$numpat5"/>
        </xsl:when>
        <xsl:otherwise>
					<xsl:value-of select="$numpat4"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNode" select="AttributeList"/>
		</xsl:call-template>

		<!-- Call the template that outputs the contents of operation activities uploaded with OLPStyle=OperationComments -->
		<xsl:apply-templates select="AttributeList/Attribute"/>
		<xsl:if test="@ActivityType!='Operation'">
			<!-- counter 3 counts oscillation points -->
			<NumberIncrement:counternum counter="3"/>
			<NumberIncrement:reset/>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@ActivityType='Operation'">
				<xsl:choose>
					<xsl:when test="starts-with(ActivityName, 'ARCON AC=') or
                          starts-with(ActivityName, 'ARCOF AC=') or
                          starts-with(ActivityName, 'ARCON ASF#(') or
                          starts-with(ActivityName, 'ARCOF AEF#(')">
						<xsl:value-of select="$cr"/>
						<xsl:value-of select="ActivityName"/>
					</xsl:when>
					<xsl:when test="string(AttributeList/Attribute[AttributeName='Type']/AttributeValue) = 'oscillation'">
						<xsl:value-of select="$cr"/>
						<xsl:if test="$BaseGroup='2' or $StationGroup='2'">
							<xsl:text>S</xsl:text>
						</xsl:if>
						<xsl:text>REFP </xsl:text>
						<!-- counter 3 counts oscillation points -->
						<NumberIncrement:counternum counter="3"/>
						<xsl:call-template name="getNumIncNext"/>
						<xsl:variable name="storeName" select="concat('STORED_C',$currentTask,./ActivityName,./@id)"/>
						<xsl:variable name="storePos" select="MotomanUtils:GetParameterData($storeName)"/>
						<xsl:if test="string($storePos) != ''">
							<xsl:text> </xsl:text>
							<xsl:value-of select="$storePos"/>
						</xsl:if>
						<xsl:variable name="storeNameBC" select="concat('STORED_BC',$currentTask,./ActivityName,./@id)"/>
						<xsl:variable name="storePosBC" select="MotomanUtils:GetParameterData($storeNameBC)"/>
						<xsl:if test="string($storePosBC) != ''">
							<xsl:if test="$BaseGroup='2'">
								<xsl:text> +MOVJ</xsl:text>
							</xsl:if>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$storePosBC"/>
						</xsl:if>
						<xsl:variable name="storeNameEC" select="concat('STORED_EC',$currentTask,./ActivityName,./@id)"/>
						<xsl:variable name="storePosEC" select="MotomanUtils:GetParameterData($storeNameEC)"/>
						<xsl:if test="string($storePosEC) != ''">
							<xsl:if test="$BaseGroup='1' and $StationGroup='2'">
								<xsl:text> +MOVJ</xsl:text>
							</xsl:if>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$storePosEC"/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise> </xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
				<xsl:variable name="refp" select="string(AttributeList/Attribute[AttributeName='REFP']/AttributeValue)"/>
				<xsl:variable name="type" select="string(AttributeList/Attribute[AttributeName='Type']/AttributeValue)"/>
				<xsl:variable name="clfval" select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='CLF#']/AttributeValue)"/>
				<xsl:variable name="plinval" select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='PLIN']/AttributeValue)"/>
				<xsl:variable name="ploutval" select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='PLOUT']/AttributeValue)"/>
				<xsl:variable name="wgoval" select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='WGO']/AttributeValue)"/>
				<xsl:variable name="wtmval" select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='WTM']/AttributeValue)"/>
				<xsl:variable name="wstval" select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='WST']/AttributeValue)"/>
				<xsl:variable name="pressval" select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='PRESS#']/AttributeValue)"/>

        <xsl:variable name="gunNumFromParameter">
					<xsl:call-template name="getToolNum">
						<xsl:with-param name="toolName" select="ToolResource/ResourceName"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="gunNum">
					<xsl:choose>
						<xsl:when test="$gunNumFromParameter &gt; 0">
							<xsl:value-of select="$gunNumFromParameter"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string(following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName = 'GUN#']/AttributeValue)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="conveyorNumber" select="string(AttributeList/Attribute[AttributeName='ConveyorNumber']/AttributeValue)"/>
				<xsl:variable name="objectProfileName">
					<xsl:choose>
						<xsl:when test="string(MotionAttributes/ObjectFrameProfile/@Replaced) != ''">
							<xsl:value-of select="string(MotionAttributes/ObjectFrameProfile/@Replaced)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string(MotionAttributes/ObjectFrameProfile)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
        <!-- Conveyor Taught Position -->
				<xsl:variable name="tmpConveyorTaughtPosition" select="string(AttributeList/Attribute[AttributeName='ConveyorTaughtPosition']/AttributeValue)"/>
				<xsl:if test="$tmpConveyorTaughtPosition != ''">
					<xsl:variable name="hr"	select="MotomanUtils:SetParameterData('CONVEYOR_INBOUND_OFFSET', $tmpConveyorTaughtPosition)"/>
				</xsl:if>
				<xsl:variable name="ConveyorTaughtPosition" select="MotomanUtils:GetParameterData('CONVEYOR_INBOUND_OFFSET')"/>
        <!-- Conveyor Initial Position -->
				<xsl:variable name="tmpConveyorInitialPosition" select="string(AttributeList/Attribute[AttributeName='ConveyorInitialPosition']/AttributeValue)"/>
				<xsl:if test="$tmpConveyorInitialPosition != ''">
					<xsl:variable name="hr"	select="MotomanUtils:SetParameterData('CONVEYOR_INITIAL_POSITION', $tmpConveyorInitialPosition)"/>
				</xsl:if>
				<xsl:variable name="ConveyorInitialPosition" select="MotomanUtils:GetParameterData('CONVEYOR_INITIAL_POSITION')"/>
        
				<xsl:if test="preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][1]/MotionAttributes/ObjectFrameProfile!=$objectName">
					<xsl:if test="$downloadTarget != 'Cartesian'">
						<xsl:call-template name="outputShiftOp">
							<xsl:with-param name="obj" select="../../Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$objectName]"/>
							<xsl:with-param name="numpat" select="$numpat"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:if>

        <!-- sync start -->
        <xsl:if test="string(Target/CartesianTarget/Tag/@TrackTag)='true'">
          <xsl:variable name="systart" select="MotomanUtils:GetParameterData('STORE_SYSTART')"/>
          <xsl:choose>
            <xsl:when test="string($systart) = ''">
              <!-- first one, use object frame profile CV#(?) or set activity attribute ConveyorNumber -->
              <xsl:call-template name="set_cv">
                <xsl:with-param name="objprofilename" select="$objectProfileName"/>
                <xsl:with-param name="conveyornumber" select="$conveyorNumber"/>
              </xsl:call-template>
              <xsl:variable name="systartcomp">
                <xsl:call-template name="get_systart">
                  <xsl:with-param name="conveyorinitialposition" select="$ConveyorInitialPosition"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:value-of select="$cr"/>
              <xsl:value-of select="$systartcomp"/>
              <xsl:variable name="hr" select="MotomanUtils:SetParameterData('STORE_SYSTART', string($systartcomp))"/>
            </xsl:when>
            <xsl:otherwise>
              <!-- not first one, output if different -->
              <xsl:variable name="update_cv">
                <xsl:call-template name="get_cv">
                  <xsl:with-param name="objprofilename" select="$objectProfileName"/>
                  <xsl:with-param name="conveyornumber" select="$conveyorNumber"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:variable name="systartcomp">
                <xsl:call-template name="get_systart">
                  <xsl:with-param name="conveyorinitialposition" select="$ConveyorInitialPosition"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:if test="string($systart) != string($systartcomp)">
                <xsl:value-of select="$cr"/>
                <xsl:value-of select="$systartcomp"/>
                <xsl:variable name="hr" select="MotomanUtils:SetParameterData('STORE_SYSTART', string($systartcomp))"/>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <!-- end if TrackTag -->
        
        <!-- get C or P number -->
				<xsl:variable name="pvarAttrVal" select="AttributeList/Attribute[AttributeName=$pvar]/AttributeValue"/>
				<xsl:variable name="curnum">
					<xsl:choose>
						<xsl:when test="boolean($pvarAttrVal)">
							<xsl:value-of select="$pvarAttrVal"/>
						</xsl:when>
						<xsl:otherwise>
							<NumberIncrement:counternum counter="0"/>
							<xsl:call-template name="getNumIncNext"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="curnumC_P" select="format-number($curnum, $numpat)"/>
				<!-- get BP number -->
				<xsl:variable name="bpvarAttrVal" select="AttributeList/Attribute[AttributeName=$bpvar]/AttributeValue"/>
				<xsl:variable name="curnumBP" select="format-number($bpvarAttrVal, $numpat)"/>
				<!-- get EX number -->
				<xsl:variable name="exvarAttrVal" select="AttributeList/Attribute[AttributeName=$exvar]/AttributeValue"/>
				<xsl:variable name="curnumEX" select="format-number($exvarAttrVal, $numpat)"/>

				<xsl:value-of select="$cr"/>
        <xsl:variable name="nextMoAct" select="following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
        <xsl:variable name="nextMoType" select="$nextMoAct/MotionAttributes/MotionType"/>
        <xsl:variable name="moType" select="MotionAttributes/MotionType"/>
				<xsl:variable name="activityName" select="string(ActivityName)"/>
				<xsl:variable name="viaPoint" select="string(Target/@ViaPoint)"/>
				<xsl:if test="($StationGroup='2' or $BaseGroup='2') and $viaPoint='false' and not(contains($activityName,'MOV'))">
					<xsl:text>S</xsl:text>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$clfval != ''">SVSPOTMOV</xsl:when>
					<xsl:when test="$refp != ''">
						<xsl:text>REFP </xsl:text>
						<xsl:value-of select="$refp"/>
					</xsl:when>
					<xsl:when test="$type = 'search' and string($moType)='Linear'">
						<xsl:text>REFP 1</xsl:text>
					</xsl:when>
					<xsl:when test="contains($activityName,'MOV')">
						<xsl:choose>
              <xsl:when test="(starts-with($activityName,'MOVJ.') and $moType!='Joint') or
                              (starts-with($activityName,'MOVL.') and $moType!='Linear') or
                              (starts-with($activityName,'MOVC.') and starts-with($moType,'Circular')=false)">
                <!-- activity name does not match motion type -->
                <xsl:call-template name="outputMotionCommand">
                  <xsl:with-param name="moType" select="$moType"/>
                  <xsl:with-param name="nextMoType" select="$nextMoType"/>
                  <xsl:with-param name="toolType" select="$toolType"/>
                  <xsl:with-param name="toolNum" select="$toolNum"/>
                </xsl:call-template>
              </xsl:when>
              <xsl:when test="contains($activityName,'.')">
								<xsl:value-of select="substring-before($activityName,'.')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$activityName"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
          <!-- conveyor line tracking move -->
					<xsl:when test="string(Target/CartesianTarget/Tag/@TrackTag)='true'">
						<xsl:text>SYMOV</xsl:text>
						<xsl:choose>
							<xsl:when test="$moType='Joint'">
								<xsl:text>J</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>L</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
          </xsl:when>
          <!-- end when TrackTag -->
          <xsl:otherwise>
            <xsl:call-template name="outputMotionCommand">
              <xsl:with-param name="moType" select="$moType"/>
              <xsl:with-param name="nextMoType" select="$nextMoType"/>
              <xsl:with-param name="toolType" select="$toolType"/>
              <xsl:with-param name="toolNum" select="$toolNum"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
				<xsl:variable name="storeName" select="concat('STORED_C',$currentTask,./ActivityName,./@id)"/>
				<xsl:variable name="storePos" select="MotomanUtils:GetParameterData($storeName)"/>
				<xsl:choose>
					<xsl:when test="string($storePos) != ''">
						<xsl:text> </xsl:text>
						<xsl:value-of select="$storePos"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="boolean($pvarAttrVal)"> P</xsl:when>
							<xsl:otherwise> C</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$curnumC_P"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="count(Target/JointTarget/AuxJoint[@Type = 'RailTrackGantry'])">
					<xsl:if test="$BaseGroup='2'">
						<xsl:call-template name="outputVelocityAccuracy">
							<xsl:with-param name="userData" select="'false'"/>
							<xsl:with-param name="moveNode" select="."/>
							<xsl:with-param name="baseGroup" select="'1'"/>
							<xsl:with-param name="stationGroup" select="'1'"/>
						</xsl:call-template>
						<xsl:text> +MOVJ</xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="boolean($bpvarAttrVal)">
							<xsl:text> BP</xsl:text>
							<xsl:value-of select="$curnumBP"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="storePosBC"
								select="MotomanUtils:GetParameterData(concat('STORED_BC',$currentTask,./ActivityName,./@id))"/>
							<xsl:choose>
								<xsl:when test="string($storePosBC) != ''">
									<xsl:text> </xsl:text>
									<xsl:value-of select="$storePosBC"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text> BC</xsl:text>
									<xsl:value-of select="$curnumC_P"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="count(Target/JointTarget/AuxJoint[@Type = 'EndOfArmTooling' or @Type = 'WorkpiecePositioner'])">
					<xsl:if test="$BaseGroup='1' and $StationGroup='2'">
						<xsl:call-template name="outputVelocityAccuracy">
							<xsl:with-param name="userData" select="'false'"/>
							<xsl:with-param name="moveNode" select="."/>
							<xsl:with-param name="baseGroup" select="'1'"/>
							<xsl:with-param name="stationGroup" select="'1'"/>
						</xsl:call-template>
            <xsl:choose>
              <xsl:when test="$refp != ''">
                <xsl:text> +REFP</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text> +MOVJ</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
					<xsl:choose>
						<xsl:when test="boolean($exvarAttrVal)">
							<xsl:text> EX</xsl:text>
							<xsl:value-of select="$curnumEX"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="storePosEC"
								select="MotomanUtils:GetParameterData(concat('STORED_EC',$currentTask,./ActivityName,./@id))"/>
							<xsl:choose>
								<xsl:when test="string($storePosEC) != ''">
									<xsl:text> </xsl:text>
									<xsl:value-of select="$storePosEC"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text> EC</xsl:text>
									<xsl:value-of select="$curnumC_P"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
        <xsl:if test="starts-with($activityName,'SMOVL')=false">
          <!-- no speed output for SMOVL -->
          <xsl:call-template name="outputVelocityAccuracy">
            <xsl:with-param name="userData" select="'true'"/>
            <xsl:with-param name="moveNode" select="."/>
            <xsl:with-param name="baseGroup" select="$BaseGroup"/>
            <xsl:with-param name="stationGroup" select="$StationGroup"/>
          </xsl:call-template>
        </xsl:if>
        <!-- output motion options uploaded as attribute -->
				<xsl:value-of select="AttributeList/Attribute[AttributeName='MotionOptions']/AttributeValue"/>
        <!-- conveyor line tracking -->
        <xsl:if test="string(Target/CartesianTarget/Tag/@TrackTag)='true'">
          <xsl:text> </xsl:text>
          <xsl:call-template name="get_cv">
            <xsl:with-param name="objprofilename" select="$objectProfileName"/>
            <xsl:with-param name="conveyornumber" select="$conveyorNumber"/>
          </xsl:call-template>
          <xsl:text> CTP=</xsl:text>
          <xsl:value-of select="format-number($ConveyorTaughtPosition*1000.0, $patD3)"/>
        </xsl:if>

        <!-- check next move to see if SYEND needs to be output -->
        <xsl:if test="string($nextMoAct/Target/CartesianTarget/Tag/@TrackTag) != 'true'">
          <xsl:variable name="systart" select="MotomanUtils:GetParameterData('STORE_SYSTART')"/>
          <!-- set STORE_SYEND with uploaded 'Robot Language' -->
          <xsl:call-template name="set_syend">
            <xsl:with-param name="moveNode" select="."/>
          </xsl:call-template>
          
          <xsl:variable name="syend" select="MotomanUtils:GetParameterData('STORE_SYEND')"/>
          <xsl:if test="string($systart)!='' and string($syend)=''">
            <xsl:value-of select="$cr"/>
            <xsl:text>SYEND </xsl:text>
            <xsl:call-template name="get_cv">
              <xsl:with-param name="objprofilename" select="$objectProfileName"/>
              <xsl:with-param name="conveyornumber" select="$conveyorNumber"/>
            </xsl:call-template>
            <!-- clear SYSTART -->
            <xsl:variable name="hr1" select="MotomanUtils:SetParameterData('STORE_SYSTART', '')"/>
            <xsl:variable name="hr2" select="MotomanUtils:SetParameterData('STORE_SYEND', '')"/>
          </xsl:if>
        </xsl:if>
      </xsl:when>
			<xsl:when test="@ActivityType='DelayActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:text>TIMER T=</xsl:text>
				<xsl:variable name="start" select="ActivityTime/StartTime"/>
				<xsl:variable name="end" select="ActivityTime/EndTime"/>
				<xsl:value-of select="format-number($end - $start, $patD2)"/>
			</xsl:when>

			<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
				<xsl:variable name="callname" select="string(CallName)"/>
				<xsl:variable name="callType"
					select="string(/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList[@Task=$callname]/AttributeList/Attribute[AttributeName='Type']/AttributeValue)"/>
				<xsl:choose>
					<xsl:when test="contains($callname,'_FLATFILE') or string($callType) = 'flat'">
						<xsl:apply-templates
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/ActivityList[@Task=$callname]">
							<xsl:with-param name="type" select="'flat'"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:when test="contains($callname,'TSYNC')">
						<xsl:value-of select="$cr"/>
						<xsl:value-of select="$callname"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>CALL JOB:</xsl:text>
						<xsl:value-of select="translate($callname, '.', '')"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="not(contains($callname,'_FLATFILE')  or string($callType) = 'flat')">
					<xsl:variable name="condition"
						select="AttributeList/Attribute[starts-with(AttributeValue, 'Call Task Condition:')]/AttributeValue"/>
					<xsl:if test="$condition">
						<xsl:text> </xsl:text>
						<xsl:value-of select="substring-after($condition, ':')"/>
					</xsl:if>
				</xsl:if>
			</xsl:when>

			<xsl:when test="@ActivityType='DNBSetSignalActivity'">
				<xsl:choose>
					<xsl:when test="$signalDuration != 0 and $setSignalValue='On'">
						<xsl:value-of select="$cr"/>
						<xsl:text>PULSE OT#(</xsl:text>
						<xsl:value-of select="$outputPortNumber"/>
						<xsl:text>) T=</xsl:text>
						<xsl:value-of
							select="format-number($signalDuration, $cartesianPositionPattern)"/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>DOUT OT#(</xsl:text>
						<xsl:value-of select="$outputPortNumber"/>
						<xsl:text>)</xsl:text>

						<xsl:choose>
							<xsl:when test="$setSignalValue='Off'">
								<xsl:text> OFF</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> ON</xsl:text>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>

			<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:text>WAIT IN#(</xsl:text>
				<xsl:value-of select="$inputPortNumber"/>
				<xsl:text>)=</xsl:text>
				<xsl:choose>
					<xsl:when test="$waitSignalValue='Off'">
						<xsl:text>OFF</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>ON</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:if test="$maxWaitTime != 0 ">
					<xsl:text> T=</xsl:text>
					<xsl:value-of select="format-number($maxWaitTime, $cartesianPositionPattern)"/>
				</xsl:if>

			</xsl:when>

		</xsl:choose>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNode" select="AttributeList"/>
		</xsl:call-template>

	</xsl:template>
	<!-- end of template Activity -->

	<xsl:template name="matchToolProfileName">
		<xsl:param name="nameToMatch"/>
		<xsl:param name="toolProfileNodeSet"/>

		<xsl:variable name="p"
			select="/OLPData/Resource/ParameterList/Parameter[starts-with(ParameterName, 'ToolProfile.') and ParameterValue=$nameToMatch]"/>
		<xsl:choose>
			<xsl:when test="$p">
				<xsl:value-of select="substring-after($p/ParameterName, '.')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="$toolProfileNodeSet">
					<xsl:if test="./Name = $nameToMatch">
						<xsl:value-of select="position()-1"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="matchObjectFrameProfileName">
		<xsl:param name="nameToMatch"/>
		<xsl:param name="objectFrameProfileNodeSet"/>

		<xsl:variable name="p"
			select="/OLPData/Resource/ParameterList/Parameter[starts-with(ParameterName, 'ObjectFrameProfile.') and ParameterValue=$nameToMatch]"/>
		<xsl:choose>
			<xsl:when test="$p">
				<xsl:value-of select="substring-after($p/ParameterName, '.')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="$objectFrameProfileNodeSet">
					<xsl:if test="./Name = $nameToMatch">
						<!-- base ROBOT=0, base BASE=1, the 3rd base is output as 1 -->
						<xsl:value-of select="position()-2"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end matchObjectFrameProfileName -->

	<xsl:template name="getPOSTYPE">
		<xsl:param name="uframeName" select="'Default'"/>

		<xsl:choose>
			<xsl:when test="$uframeName = 'ROBOT'">ROBOT</xsl:when>
			<xsl:when test="$uframeName = 'BASE'">BASE</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="objFrameProfile"
					select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile[Name=$uframeName]"/>
				<xsl:variable name="x">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$objFrameProfile/ObjectFrame/ObjectFramePosition/@X"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="y">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$objFrameProfile/ObjectFrame/ObjectFramePosition/@Y"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="z">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$objFrameProfile/ObjectFrame/ObjectFramePosition/@Z"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="w">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$objFrameProfile/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="p">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$objFrameProfile/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="r">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num"
							select="$objFrameProfile/ObjectFrame/ObjectFrameOrientation/@Roll"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="sumOfSquares"
					select="1000000*($x*$x + $y*$y + $z*$z) + $w*$w + $p*$p + $r*$r"/>
				<xsl:choose>
					<xsl:when test="$sumOfSquares > 0.0001">USER</xsl:when>
					<xsl:when test="$sumOfSquares > 0.0">BASE</xsl:when>
					<xsl:otherwise>ROBOT</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end getPOSTYPE -->

	<!-- This template outputs the contents of operation activities uploaded with OLPStyle=OperationComments -->
	<xsl:template match="Attribute">
		<xsl:variable name="attrname" select="AttributeName"/>
		<xsl:variable name="attrvalue" select="AttributeValue"/>

		<xsl:if test="substring($attrname,1,7) = 'Comment'">
			<xsl:value-of select="$cr"/>
			<xsl:text>'</xsl:text>
			<!-- comment limited to 32 characters -->
			<xsl:value-of select="substring($attrvalue, 1, 32)"/>
		</xsl:if>

		<xsl:if test="substring($attrname,1,14) = 'Robot Language'">
			<xsl:value-of select="$cr"/>
			<xsl:value-of select="$attrvalue"/>
			<xsl:if test="starts-with($attrvalue, 'SYSTART CV#(')">
				<xsl:variable name="hr" select="MotomanUtils:SetParameterData('STORE_SYSTART',$attrvalue)"/>
			</xsl:if>
			<xsl:if test="starts-with($attrvalue, 'SYEND CV#(')">
				<xsl:variable name="hr" select="MotomanUtils:SetParameterData('STORE_SYEND',$attrvalue)"/>
			</xsl:if>
		</xsl:if>

	</xsl:template>

	<xsl:template name="rconf">
		<xsl:param name="target"/>

		<xsl:variable name="j4">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num"
					select="$target/JointTarget/Joint[@DOFNumber=4]/JointValue"/>
			</xsl:call-template>
		</xsl:variable>
    <xsl:variable name="j5">
      <xsl:call-template name="Scientific">
        <xsl:with-param name="Num" select="$target/JointTarget/Joint[@DOFNumber=5]/JointValue"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="L">
      <xsl:choose>
        <xsl:when test="$controller='DX100'">
          <xsl:choose>
            <xsl:when test="$j5 &lt; 0">1</xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$j4 &gt;  -90 and $j4 &lt;= 90">0</xsl:when>
            <xsl:when test="$j4 &gt;  270 and $j4 &lt;= 360">0</xsl:when>
            <xsl:when test="$j4 &gt; -360 and $j4 &lt;= -270">0</xsl:when>
            <xsl:otherwise>1</xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="cfg" select="$target/CartesianTarget/Config/@Name"/>
		<xsl:variable name="M">
			<xsl:choose>
				<xsl:when test="$cfg = 'Config_1'">0</xsl:when>
				<xsl:when test="$cfg = 'Config_2'">0</xsl:when>
				<xsl:when test="$cfg = 'Config_7'">0</xsl:when>
				<xsl:when test="$cfg = 'Config_8'">0</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="N">
			<xsl:choose>
				<xsl:when test="$cfg = 'Config_1'">0</xsl:when>
				<xsl:when test="$cfg = 'Config_2'">0</xsl:when>
				<xsl:when test="$cfg = 'Config_3'">0</xsl:when>
				<xsl:when test="$cfg = 'Config_4'">0</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="turn" select="$target/CartesianTarget/TurnNumber"/>
		<xsl:variable name="O">
			<xsl:choose>
				<xsl:when test="$turn/TNJoint[@Number=4]/@Value=0">0</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="P">
			<xsl:choose>
				<xsl:when test="$turn/TNJoint[@Number=6]/@Value=0">0</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="Q">
			<xsl:choose>
				<xsl:when test="$turn/TNJoint[@Number=1]/@Value=0">0</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="KIRStrUtils:outputRCONF($L, $M, $N, $O, $P, $Q) = 'true'">
			<xsl:value-of select="$cr"/>
			<xsl:text>///RCONF </xsl:text>
			<xsl:value-of select="$L"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$M"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$N"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$O"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$P"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="$Q"/>
			<xsl:text>,0,0</xsl:text>
      <xsl:if test="$controller='DX100'">
        <!--       1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6 -->
        <xsl:text>,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0</xsl:text>
      </xsl:if>
    </xsl:if>
	</xsl:template>

	<xsl:template name="outputBase">
		<xsl:param name="numpat"/>

		<xsl:variable name="actList" select="."/>
		<xsl:variable name="ufnumOne" select="MotomanUtils:GetParameterData($ufnumstart)"/>

		<xsl:for-each
			select="../Controller/ObjectFrameProfileList/ObjectFrameProfile[@ApplyOffsetToTags='On']">
			<xsl:variable name="uframeName" select="Name"/>
			<xsl:if
				test="count($actList/Activity/MotionAttributes[ObjectFrameProfile=$uframeName]/ObjectFrameProfile) > 0">
				<xsl:variable name="posType">
					<xsl:call-template name="getPOSTYPE">
						<xsl:with-param name="uframeName" select="$uframeName"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test="$posType = 'USER'">
					<xsl:variable name="ufnum" select="MotomanUtils:GetParameterData($ufnumstart)"/>
					<xsl:if test="$ufnum = $ufnumOne">
						<xsl:value-of select="$cr"/>
						<xsl:text>///POSTYPE BASE</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:text>///RECTAN</xsl:text>
						<xsl:variable name="hr"
							select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'RECTAN')"/>
					</xsl:if>
					<!-- associate objectprofile named Name with ufnum -->
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData(concat('ObjectFrameProfileName_', Name), $ufnum)"/>
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData($ufnumstart, $ufnum+1)"/>
					<xsl:value-of select="$cr"/>
					<xsl:text>P</xsl:text>
					<xsl:value-of select="format-number($ufnum, $numpat)"/>
					<xsl:text>=</xsl:text>
					<xsl:call-template name="outputBaseVal">
						<xsl:with-param name="obj" select="."/>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="outputBaseVal">
		<xsl:param name="obj"/>
		
		<xsl:variable name="xxx">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$obj/ObjectFrame/ObjectFramePosition/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="yyy">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$obj/ObjectFrame/ObjectFramePosition/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="zzz">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$obj/ObjectFrame/ObjectFramePosition/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="yaw">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$obj/ObjectFrame/ObjectFrameOrientation/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="pitch">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$obj/ObjectFrame/ObjectFrameOrientation/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="roll">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$obj/ObjectFrame/ObjectFrameOrientation/@Roll"/>
			</xsl:call-template>
		</xsl:variable>

    <xsl:variable name="patRot">
      <xsl:choose>
        <xsl:when test="$controller='DX100'">
          <xsl:value-of select="$patD4"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$patD2"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="format-number($xxx * 1000, $patD3)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($yyy * 1000, $patD3)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($zzz * 1000, $patD3)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($yaw, $patRot)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($pitch, $patRot)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($roll, $patRot)"/>
  </xsl:template>

	<xsl:template name="outputShiftOp">
		<xsl:param name="obj"/>
		<xsl:param name="numpat"/>

		<xsl:variable name="ufnum"
			select="MotomanUtils:GetParameterData(concat('ObjectFrameProfileName_', $obj/Name))"/>
		<xsl:choose>
			<xsl:when test="string-length($ufnum) = 0">
				<xsl:if test="MotomanUtils:GetParameterData('ShiftOperationStatus')='ON'">
					<xsl:value-of select="$cr"/>
					<xsl:text>SFTOF</xsl:text>
				</xsl:if>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('ShiftOperationStatus', 'OFF')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$cr"/>
				<xsl:text>SFTON P</xsl:text>
				<xsl:value-of select="format-number($ufnum, $numpat)"/>
				<xsl:text> BF</xsl:text>
				<xsl:variable name="hr"
					select="MotomanUtils:SetParameterData('ShiftOperationStatus', 'ON')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="processVarDefComments">
		<xsl:param name="attributeListNode"/>
		
		<xsl:variable name="vardef" select="'Variable Definition'"/>
		<xsl:variable name="len" select="string-length($vardef)"/>
		<xsl:for-each select="$attributeListNode/Attribute">
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:variable name="attrprefix" select="substring($attrvalue,1,$len)"/>
			<xsl:variable name="attrtext" select="substring-after($attrvalue,concat($vardef,':'))"/>
			<xsl:if test="$attrprefix = $vardef">
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$attrtext"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<!-- This template outputs the comment attributes of activities uploaded with OLPStyle!=OperationComments -->
	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNode"/>
		<xsl:param name="instAttr" select="0"/>
		<xsl:param name="type" select="'job'"/>
		<xsl:for-each select="$attributeListNode/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
			<xsl:variable name="postcomchk" select="substring($attrname,1,11)"/>
			<xsl:variable name="roblangchk" select="substring($attrvalue,1,15)"/>
			<xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
			<xsl:if test="($precomchk = 'PreComment' and $prefix = 'Pre') or ($postcomchk = 'PostComment' and $prefix = 'Post')">
				<xsl:choose>
					<xsl:when test="starts-with($attrvalue, 'Robot Language:////FRAME')">
						<xsl:call-template name="outputSlash4Frame"/>
					</xsl:when>
					<xsl:when test="$roblangchk = 'Robot Language:'">
						<xsl:if test="not(starts-with($striprobotlang,'///')) or $type = 'job'">
							<xsl:value-of select="$cr"/>
							<xsl:value-of select="$striprobotlang"/>
							<xsl:if test="starts-with($striprobotlang, 'SYSTART CV#(')">
								<xsl:variable name="hr" select="MotomanUtils:SetParameterData('STORE_SYSTART',$striprobotlang)"/>
							</xsl:if>
							<xsl:if test="starts-with($striprobotlang, 'SYEND CV#(')">
								<xsl:variable name="hr" select="MotomanUtils:SetParameterData('STORE_SYEND',$striprobotlang)"/>
							</xsl:if>
						</xsl:if>
					</xsl:when>
					<xsl:when test="$attrname = $pvar"> </xsl:when>
					<xsl:when test="starts-with($attrvalue, 'Call Task Condition:')"> </xsl:when>
					<xsl:when test="starts-with($attrvalue, 'Variable Definition:')"/>
					<xsl:otherwise>
						<xsl:value-of select="$cr"/>
						<xsl:text>'</xsl:text>
						<xsl:value-of select="substring($attrvalue, 1, 32)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="$instAttr and $type = 'job'">
			<xsl:variable name="preCommAttr"
				select="$attributeListNode/Attribute[starts-with(AttributeName, 'PreComment') and starts-with(AttributeValue, 'Robot Language:///ATTR')]"/>
			<xsl:if test="not($preCommAttr)">
				<!-- if the following does not exist
					<Attribute>
						<AttributeName>PreComment?</AttributeName>
						<AttributeValue>Robot Language:///ATTR</AttributeValue>
					</Attribute>
				-->
				<xsl:variable name="header_attr"
					select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Header_Attr']/ParameterValue"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>///ATTR </xsl:text>
				<xsl:choose>
					<xsl:when test="boolean($header_attr)">
						<xsl:value-of select="$header_attr"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>SC,RW</xsl:text>
            <xsl:if test="$downloadTarget='Cartesian'">
              <xsl:text>,RJ</xsl:text>
            </xsl:if>
          </xsl:otherwise>
				</xsl:choose>

				<xsl:call-template name="outputSlash4Frame"/>
				<xsl:value-of select="$cr"/>
				<xsl:text>///GROUP1 RB1</xsl:text>
				<xsl:variable name="StationGroup">
					<xsl:choose>
						<xsl:when
							test="string($attributeListNode/Attribute[AttributeName='StationGroup']/AttributeValue)!=''">
							<xsl:value-of
								select="number($attributeListNode/Attribute[AttributeName='StationGroup']/AttributeValue)"
							/>
						</xsl:when>
						<xsl:when
							test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='StationGroup']/ParameterValue)!=''">
							<xsl:value-of
								select="number(/OLPData/Resource/ParameterList/Parameter[ParameterName='StationGroup']/ParameterValue)"
							/>
						</xsl:when>
						<xsl:when
							test="contains(string($attributeListNode/AttributeList/Attribute[starts-with(AttributeName,'PreComment') and contains(AttributeValue,'GROUP2')]/AttributeValue),'ST')"
							>2</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="BaseGroup">
					<xsl:choose>
						<xsl:when
							test="string($attributeListNode/Attribute[AttributeName='BaseGroup']/AttributeValue)!=''">
							<xsl:value-of
								select="number($attributeListNode/Attribute[AttributeName='BaseGroup']/AttributeValue)"
							/>
						</xsl:when>
						<xsl:when
							test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='BaseGroup']/ParameterValue)!=''">
							<xsl:value-of
								select="number(/OLPData/Resource/ParameterList/Parameter[ParameterName='BaseGroup']/ParameterValue)"
							/>
						</xsl:when>
						<xsl:when
							test="contains(string($attributeListNode/AttributeList/Attribute[starts-with(AttributeName,'PreComment') and contains(AttributeValue,'GROUP2')]/AttributeValue),'BS')"
							>2</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="StationNumber">
					<xsl:choose>
						<xsl:when
							test="string($attributeListNode/Attribute[AttributeName='StationNumber']/AttributeValue)!=''">
							<xsl:value-of
								select="number($attributeListNode/Attribute[AttributeName='StationNumber']/AttributeValue)"
							/>
						</xsl:when>
						<xsl:when
							test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='StationNumber']/ParameterValue)!=''">
							<xsl:value-of
								select="number(/OLPData/Resource/ParameterList/Parameter[ParameterName='StationNumber']/ParameterValue)"
							/>
						</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="BaseNumber">
					<xsl:choose>
						<xsl:when
							test="string($attributeListNode/Attribute[AttributeName='BaseNumber']/AttributeValue)!=''">
							<xsl:value-of
								select="number($attributeListNode/Attribute[AttributeName='BaseNumber']/AttributeValue)"
							/>
						</xsl:when>
						<xsl:when
							test="string(/OLPData/Resource/ParameterList/Parameter[ParameterName='BaseNumber']/ParameterValue)!=''">
							<xsl:value-of
								select="number(/OLPData/Resource/ParameterList/Parameter[ParameterName='BaseNumber']/ParameterValue)"
							/>
						</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$auxAxes > 0">
					<xsl:choose>
						<xsl:when test="$BaseGroup = '2'">
							<xsl:value-of select="$cr"/>
							<xsl:text>///GROUP2 BS</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>,BS</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$BaseNumber"/>
				</xsl:if>
				<xsl:if test="$extAxes > 0 or $posAxes > 0">
					<xsl:choose>
						<xsl:when test="$StationGroup = '2' and $BaseGroup='1'">
							<xsl:value-of select="$cr"/>
							<xsl:text>///GROUP2 ST</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>,ST</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$StationNumber"/>
				</xsl:if>
			</xsl:if>
		</xsl:if>
	</xsl:template>
  
	<xsl:template name="outputSlash4Frame">
		<xsl:if test="$downloadTarget = 'Cartesian'">
			<xsl:value-of select="$cr"/>
			<xsl:variable name="firstmv"
				select="/OLPData/Resource/ActivityList/Activity[@ActivityType='DNBRobotMotionActivity'][1]"/>
			<xsl:variable name="uframeName" select="$firstmv/MotionAttributes/ObjectFrameProfile"/>
			<xsl:variable name="posType">
				<xsl:call-template name="getPOSTYPE">
					<xsl:with-param name="uframeName" select="$uframeName"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- get uframe number -->
			<xsl:variable name="uframeNum">
				<xsl:call-template name="matchObjectFrameProfileName">
					<xsl:with-param name="nameToMatch" select="$uframeName"/>
					<xsl:with-param name="objectFrameProfileNodeSet"
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile"
					/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:text>////FRAME </xsl:text>
			<xsl:value-of select="$posType"/>
			<xsl:if test="$posType = 'USER'">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$uframeNum"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="UserProfile">
		<xsl:variable name="name" select="."/>
		<xsl:variable name="type" select="./@Type"/>
		<xsl:variable name="userProfile"
			select="/OLPData/Resource/Controller/UserProfileList/UserProfile[@Type=$type]"/>
		<xsl:variable name="instance" select="$userProfile/UserProfileInstance[@Name=$name]"/>
		<xsl:variable name="current"
			select="$instance/UserDefinedAttribute[DisplayName='Current']/Value"/>
		<xsl:variable name="voltage"
			select="$instance/UserDefinedAttribute[DisplayName='Voltage']/Value"/>
		<xsl:variable name="timer"
			select="$instance/UserDefinedAttribute[DisplayName='Timer']/Value"/>
		<xsl:variable name="speed"
			select="$instance/UserDefinedAttribute[DisplayName='Speed']/Value"/>
		<xsl:variable name="retry"
			select="$instance/UserDefinedAttribute[DisplayName='Retry']/Value"/>
		<xsl:variable name="antstk"
			select="$instance/UserDefinedAttribute[DisplayName='AntStk']/Value"/>

		<xsl:if test="@Type = 'ARCStart'">
			<xsl:value-of select="$cr"/>
			<xsl:text>ARCON </xsl:text>
			<xsl:choose>
				<xsl:when test="starts-with(., 'file_as')">
					<xsl:text>ASF#(</xsl:text>
					<xsl:variable name="arcStartFileNum"
						select="substring(., string-length('file_as')+1)"/>
					<xsl:choose>
						<xsl:when test="contains($arcStartFileNum, '.')">
							<xsl:value-of select="substring-before($arcStartFileNum, '.')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$arcStartFileNum"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>AC=</xsl:text>
					<xsl:value-of select="$current"/>
					<xsl:text> AVP=</xsl:text>
					<xsl:value-of select="$voltage"/>
					<xsl:text> T=</xsl:text>
					<xsl:value-of select="$timer"/>
					<!-- when ARCOn speed is not set, weld speed is the same
						 as the next motion speed -->
					<xsl:if test="$speed &gt; 0">
						<xsl:text> V=</xsl:text>
						<xsl:value-of select="$speed"/>
					</xsl:if>
					<xsl:if test="$retry='true'"> RETRY</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<xsl:if test="@Type = 'ARCEnd'">
			<xsl:value-of select="$cr"/>
			<xsl:text>ARCOF </xsl:text>
			<xsl:choose>
				<xsl:when test="starts-with(., 'file_ae')">
					<xsl:text>AEF#(</xsl:text>
					<xsl:variable name="arcEndFileNum"
						select="substring(., string-length('file_ae')+1)"/>
					<xsl:choose>
						<xsl:when test="contains($arcEndFileNum, '.')">
							<xsl:value-of select="substring-before($arcEndFileNum, '.')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$arcEndFileNum"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>AC=</xsl:text>
					<xsl:value-of select="$current"/>
					<xsl:text> AVP=</xsl:text>
					<xsl:value-of select="$voltage"/>
					<xsl:text> T=</xsl:text>
					<xsl:value-of select="$timer"/>
					<xsl:if test="$antstk='true'"> ANTSTK</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

	</xsl:template>

	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'e')"/>
					<xsl:with-param name="e" select="substring-after($Num,'e')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'E')"/>
					<xsl:with-param name="e" select="substring-after($Num,'E')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="outputC_P">
		<xsl:param name="rbtMoveNodeSetPrecede"/>
		<xsl:param name="rbtMoveNodeSet"/>
		<xsl:param name="rbtOperationNodeSet"/>
		<xsl:param name="rbtMoveNodeSetFollow"/>
		<xsl:param name="varName"/>
		<xsl:param name="numpat" select="$numpat4"/>

		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>
		<xsl:variable name="combineNodeSet" select="$rbtMoveNodeSetPrecede|$rbtMoveNodeSet|$rbtOperationNodeSet|$rbtMoveNodeSetFollow"/>
		<xsl:for-each select="$combineNodeSet">
			<xsl:if test="$varName = 'C' or ($varName = 'P' and string(@ActivityType) != 'Operation')">
				<xsl:variable name="varNum">
					<xsl:choose>
						<xsl:when test="$varName=$pvar">
							<xsl:value-of select="AttributeList/Attribute[AttributeName=$varName]/AttributeValue"/>
						</xsl:when>
						<xsl:otherwise>
							<NumberIncrement:counternum counter="0"/>
							<xsl:value-of select="NumberIncrement:next()"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- store position variable: reused target have different activity name and ID
					 and needs to be found by the combination of varName and ActivityName and ID -->
				<xsl:variable name="currentTask" select="ancestor::ActivityList/@Task"/>
				<xsl:variable name="storeName" select="concat('STORED_', $varName, $currentTask, ./ActivityName, ./@id)"/>
				<xsl:variable name="storeValue" select="concat($varName, format-number($varNum, $numpat))"/>
				<xsl:variable name="parmResult" select="MotomanUtils:SetParameterData($storeName, $storeValue)"/>
				<!-- Pvar number may be DUPLICATE and not output multiple times -->
				<xsl:variable name="curnum">
					<xsl:if test="$varName=$pvar">
						<xsl:variable name="nodeSet" select="preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$varName]"/>
						<xsl:for-each select="$nodeSet">
							<xsl:variable name="varNum2" select="AttributeList/Attribute[AttributeName=$varName]/AttributeValue"/>
							<xsl:if test="number($varNum) = number($varNum2)">
								<xsl:value-of select="'DUPLICATE'"/>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
					<xsl:value-of select="$varNum"/>
				</xsl:variable>
				<xsl:if test="starts-with($curnum, 'DUPLICATE')=false">
					<xsl:variable name="toolName" select="string(MotionAttributes/ToolProfile)"/>
					<xsl:variable name="toolType"
						select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile[Name=$toolName]/ToolType"/>
					<xsl:variable name="lastRobotTool">
						<xsl:if test="$toolType = 'OnRobot'">
							<xsl:value-of select="$toolName"/>
						</xsl:if>
					</xsl:variable>
					<xsl:if test="$toolName != ''">
						<!-- get tool number -->
						<xsl:variable name="toolNum">
							<xsl:call-template name="matchToolProfileName">
								<xsl:with-param name="nameToMatch" select="$toolName"/>
								<xsl:with-param name="toolProfileNodeSet"
									select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile"
								/>
							</xsl:call-template>
						</xsl:variable>
						<!-- check to see if output tool number for C and P vars-->
						<xsl:choose>
							<xsl:when test="$varName=$pvar">
								<!-- P var -->
								<xsl:variable name="currentTool"
									select="KIRStrUtils:retrieveToolNum()"/>
								<xsl:if test="$toolNum != $currentTool">
									<xsl:value-of select="$cr"/>
									<xsl:text>///TOOL </xsl:text>
									<xsl:value-of select="$toolNum"/>
									<xsl:value-of select="KIRStrUtils:storeToolNum($toolNum)"/>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<!-- C var -->
								<!-- posvalue is previous Activity -->
								<xsl:variable name="posvalue" select="position() - 1"/>
								<xsl:variable name="prevTool"
									select="string($combineNodeSet[$posvalue]/MotionAttributes/ToolProfile)"/>
								<xsl:if
									test="$prevTool != $toolName and $toolName != '' and $prevTool != '' or number($curnum)=0">
									<xsl:value-of select="$cr"/>
									<xsl:text>///TOOL </xsl:text>
									<xsl:value-of select="$toolNum"/>
									<xsl:value-of select="KIRStrUtils:storeToolNum($toolNum)"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<!-- test="$toolName" -->
					<xsl:variable name="posType" select="MotomanUtils:GetParameterData('CURRENTPOSTYPE')"/>
					<xsl:choose>
						<xsl:when test="number($curnum)=0 and $varName='C'">
							<xsl:value-of select="$cr"/>
							<xsl:text>///POSTYPE PULSE</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>///PULSE</xsl:text>
							<xsl:variable name="hr"	select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'PULSE')"/>
						</xsl:when>
						<xsl:when test="(string($posType) = '' or $posType = 'RECTAN') and $varName='P'">
							<xsl:value-of select="$cr"/>
							<xsl:text>///POSTYPE PULSE</xsl:text>
							<xsl:value-of select="$cr"/>
							<xsl:text>///PULSE</xsl:text>
							<xsl:variable name="hr"	select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'PULSE')"/>
						</xsl:when>
					</xsl:choose>

					<xsl:for-each
						select="AttributeList/Attribute[starts-with(AttributeName,'PrePosAttr')]/AttributeValue">
						<xsl:value-of select="$cr"/>
						<xsl:value-of select="."/>
					</xsl:for-each>
					<xsl:value-of select="$cr"/>
					<xsl:value-of select="$varName"/>
					<xsl:value-of select="format-number($curnum, $numpat)"/>
					<xsl:text>=</xsl:text>
					<xsl:for-each select="Target/JointTarget/Joint">
						<xsl:variable name="pulseString"
							select="concat('PulseValue.', string(./@DOFNumber))"/>
						<xsl:variable name="zeroString"
							select="concat('ZeroValue.', string(./@DOFNumber))"/>
						<xsl:variable name="pulseValue"
							select="MotomanUtils:GetParameterData(string($pulseString))"/>
						<xsl:variable name="zeroValue"
							select="MotomanUtils:GetParameterData(string($zeroString))"/>
						<xsl:variable name="pval"
							select="MotomanUtils:JointToEncoderConversion(string(./@JointType), $pulseValue, ./JointValue, $zeroValue)"/>
						<xsl:value-of select="format-number($pval, $jointPositionPattern)"/>

						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:for-each
						select="AttributeList/Attribute[starts-with(AttributeName,'joint')]">
						<xsl:variable name="dofNum"
							select="substring-after(string(./AttributeName),'joint')"/>
						<xsl:variable name="pulseString" select="concat('PulseValue.', $dofNum)"/>
						<xsl:variable name="zeroString" select="concat('ZeroValue.', $dofNum)"/>
						<xsl:variable name="pulseValue"
							select="MotomanUtils:GetParameterData(string($pulseString))"/>
						<xsl:variable name="zeroValue"
							select="MotomanUtils:GetParameterData(string($zeroString))"/>
						<xsl:variable name="pval"
							select="MotomanUtils:JointToEncoderConversion('Rotational', $pulseValue, ./AttributeValue, $zeroValue)"/>
						<xsl:value-of select="format-number($pval, $jointPositionPattern)"/>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:for-each
						select="AttributeList/Attribute[starts-with(AttributeName,'PostPosAttr')]/AttributeValue">
						<xsl:value-of select="$cr"/>
						<xsl:value-of select="."/>
					</xsl:for-each>
				</xsl:if>
				<!-- if non-duplicate -->
			</xsl:if>
			<!-- pvar ActivityType=Operation -->
		</xsl:for-each>
	</xsl:template>
	<!-- end template outputC_P -->

	<xsl:template name="outputBC_EC_BP_EX">
		<xsl:param name="rbtMoveNodeSetPrecede"/>
		<xsl:param name="rbtMoveNodeSet"/>
		<xsl:param name="rbtOperationNodeSet"/>
		<xsl:param name="rbtMoveNodeSetFollow"/>
		<xsl:param name="varName"/>
		<xsl:param name="numpat" select="$numpat4"/>

		<xsl:variable name="auxDevType">
			<xsl:choose>
				<xsl:when test="$varName='BC' or $varName='BP'">RailTrackGantry</xsl:when>
				<xsl:otherwise>EndOfArmTooling</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="auxDevType2">
			<xsl:choose>
				<xsl:when test="$varName='BC' or $varName='BP'">RailTrackGantry</xsl:when>
				<xsl:otherwise>WorkpiecePositioner</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="downloadBPvar" select="MotomanUtils:GetParameterData('DOWNLOADBPVAR')"/>
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>

		<xsl:variable name="combineNodeSet"
			select="$rbtMoveNodeSetPrecede|$rbtMoveNodeSet|$rbtOperationNodeSet|$rbtMoveNodeSetFollow"/>

		<xsl:for-each select="$combineNodeSet">
			<xsl:variable name="auxType" select="Target/JointTarget/AuxJoint/@Type"/>
			<xsl:variable name="attrbp" select="string(AttributeList/Attribute[AttributeName='BP']/AttributeValue)"/>
			<xsl:variable name="attrex" select="string(AttributeList/Attribute[AttributeName='EX']/AttributeValue)"/>

			<xsl:variable name="cPosType" select="MotomanUtils:GetParameterData('CURRENTPOSTYPE')"/>
			<xsl:choose>
				<xsl:when test="$varName = 'BC' and $auxType='RailTrackGantry' and string($attrbp)=''">
					<xsl:choose>
						<xsl:when test="$cPosType='RECTAN'">
							<xsl:if test="$downloadTargetBC = 'Pulse'">
								<xsl:value-of select="$cr"/>
								<xsl:text>///POSTYPE PULSE</xsl:text>
								<xsl:value-of select="$cr"/>
								<xsl:text>///PULSE</xsl:text>
								<xsl:variable name="hr" select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'PULSE')"/>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$cPosType='PULSE'">
							<xsl:if test="$downloadTargetBC = 'Cartesian'">
								<xsl:value-of select="$cr"/>
								<xsl:text>///POSTYPE </xsl:text>
								<xsl:call-template name="getPOSTYPE">
									<xsl:with-param name="uframeName" select="$rbtMoveNodeSet[AttributeList/Attribute/AttributeName=$varName][1]/MotionAttributes/ObjectFrameProfile"/>
								</xsl:call-template>
								<xsl:value-of select="$cr"/>
								<xsl:text>///RECTAN</xsl:text>
								<xsl:variable name="hr" select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'RECTAN')"/>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$varName = 'BP' and $auxType='RailTrackGantry' and string($attrbp)!=''">
					<xsl:choose>
						<xsl:when test="$cPosType='RECTAN'">
							<xsl:choose>
								<!-- attribute downloadBPvar supercedes parameter downloadTargetBC -->
								<xsl:when test="$downloadBPvar = 'RECTAN' or $downloadBPvar = 'CARTESIAN'"/>
								<xsl:when test="$downloadBPvar = 'PULSE' or $downloadTargetBC = 'Pulse'">
									<xsl:value-of select="$cr"/>
									<xsl:text>///POSTYPE PULSE</xsl:text>
									<xsl:value-of select="$cr"/>
									<xsl:text>///PULSE</xsl:text>
									<xsl:variable name="hr" select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'PULSE')"/>
								</xsl:when>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$cPosType='PULSE'">
							<xsl:choose>
								<!-- attribute downloadBPvar supercedes parameter downloadTargetBC -->
								<xsl:when test="$downloadBPvar = 'PULSE'"/>
								<xsl:when test="$downloadBPvar = 'RECTAN' or $downloadBPvar = 'CARTESIAN' or $downloadTargetBC = 'Cartesian'">
									<xsl:value-of select="$cr"/>
									<xsl:text>///POSTYPE </xsl:text>
									<xsl:call-template name="getPOSTYPE">
										<xsl:with-param name="uframeName" select="$rbtMoveNodeSet[AttributeList/Attribute/AttributeName=$varName][1]/MotionAttributes/ObjectFrameProfile"/>
									</xsl:call-template>
									<xsl:value-of select="$cr"/>
									<xsl:text>///RECTAN</xsl:text>
									<xsl:variable name="hr" select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'RECTAN')"/>
								</xsl:when>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
				<xsl:when
					test="$varName = 'EC' and ($auxType='EndOfArmTooling' or $auxType='WorkpiecePositioner') and string($attrex)=''">
					<xsl:if test="$cPosType='RECTAN'">
						<xsl:value-of select="$cr"/>
						<xsl:text>///POSTYPE PULSE</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:text>///PULSE</xsl:text>
						<xsl:variable name="hr" select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'PULSE')"/>
					</xsl:if>
				</xsl:when>
				<xsl:when
					test="$varName = 'EX' and ($auxType='EndOfArmTooling' or $auxType='WorkpiecePositioner') and string($attrex)!='EX'">
					<xsl:if test="$cPosType='RECTAN'">
						<xsl:value-of select="$cr"/>
						<xsl:text>///POSTYPE PULSE</xsl:text>
						<xsl:value-of select="$cr"/>
						<xsl:text>///PULSE</xsl:text>
						<xsl:variable name="hr" select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'PULSE')"/>
					</xsl:if>
				</xsl:when>
			</xsl:choose>

			<xsl:if test="$varName = 'BC' or $varName = 'EC' or ($varName = 'BP' and string(@ActivityType) != 'Operation') or ($varName = 'EX' and string(@ActivityType) != 'Operation')">
				<xsl:variable name="varNum">
					<xsl:choose>
						<xsl:when test="$varName='BP'">
							<xsl:value-of select="$attrbp"/>
						</xsl:when>
						<xsl:when test="$varName='EX'">
							<xsl:value-of select="$attrex"/>
						</xsl:when>
						<xsl:otherwise>
							<NumberIncrement:counternum counter="0"/>
							<xsl:value-of select="NumberIncrement:next()"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="varType" select="AttributeList/Attribute[AttributeName='PosType']/AttributeValue"/>
				<!-- store position variable: reused target have different activity name and ID
					 and needs to be found by the combination of varName and ActivityName and ID -->
				<xsl:variable name="currentTask" select="ancestor::ActivityList/@Task"/>
				<xsl:variable name="storeName" select="concat('STORED_', $varName, $currentTask, ./ActivityName, ./@id)"/>
				<xsl:variable name="storeValue" select="concat($varName, format-number($varNum, $numpat))"/>
				<xsl:variable name="parmResult" select="MotomanUtils:SetParameterData($storeName, $storeValue)"/>
				<!-- variable curnum may be DUPLICATE and not output multiple times -->
				<xsl:variable name="curnum">
					<xsl:if test="$varName=$bpvar or $varName=$exvar">
						<xsl:variable name="nodeSet" select="preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$varName]"/>
						<xsl:for-each select="$nodeSet">
							<xsl:variable name="varNum2">
								<xsl:choose>
									<xsl:when test="$varName='BP'">
										<xsl:value-of select="AttributeList/Attribute[AttributeName='BP']/AttributeValue"/>
									</xsl:when>
									<xsl:when test="$varName='EX'">
										<xsl:value-of select="AttributeList/Attribute[AttributeName='EX']/AttributeValue"/>
									</xsl:when>
								</xsl:choose>
							</xsl:variable>
							<xsl:if test="number($varNum) = number($varNum2)">
								<xsl:value-of select="'DUPLICATE'"/>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
					<xsl:value-of select="$varNum"/>
				</xsl:variable>

				<xsl:if test="starts-with($curnum, 'DUPLICATE')=false">
					<xsl:choose>
						<xsl:when test="@ActivityType='Operation'">
							<xsl:call-template name="outputAuxJoints">
								<xsl:with-param name="rbtMoveNode"
									select="preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][1]"/>
								<xsl:with-param name="varName" select="$varName"/>
								<xsl:with-param name="numpat" select="$numpat"/>
								<xsl:with-param name="curnum" select="$curnum"/>
								<xsl:with-param name="auxDevType" select="string($auxDevType)"/>
								<xsl:with-param name="auxDevType2" select="string($auxDevType2)"/>
								<xsl:with-param name="actName" select="./ActivityName"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="outputAuxJoints">
								<xsl:with-param name="rbtMoveNode" select="."/>
								<xsl:with-param name="varName" select="$varName"/>
								<xsl:with-param name="numpat" select="$numpat"/>
								<xsl:with-param name="curnum" select="$curnum"/>
								<xsl:with-param name="auxDevType" select="$auxDevType"/>
								<xsl:with-param name="auxDevType2" select="$auxDevType2"/>
								<xsl:with-param name="actName" select="./ActivityName"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- end template outputBC_EC_BP_EX -->

	<xsl:template name="outputC_P_cart">
		<xsl:param name="rbtMoveNodeSetPrecede"/>
		<xsl:param name="rbtMoveNodeSet"/>
		<xsl:param name="rbtMoveNodeSetFollow"/>
		<xsl:param name="varName"/>
		<xsl:param name="numpat" select="$numpat4"/>

		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>

		<xsl:variable name="combineNodeSet" select="$rbtMoveNodeSetPrecede|$rbtMoveNodeSet|$rbtMoveNodeSetFollow"/>
		<xsl:for-each select="$combineNodeSet">
			<xsl:variable name="curnum">
				<xsl:choose>
					<xsl:when test="$varName=$pvar">
						<xsl:variable name="varNum" select="AttributeList/Attribute[AttributeName=$varName]/AttributeValue"/>
						<xsl:variable name="nodeSet" select="preceding-sibling::Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$varName]"/>
						<xsl:for-each select="$nodeSet">
							<xsl:variable name="varNum2" select="AttributeList/Attribute[AttributeName=$varName]/AttributeValue"/>
							<xsl:if test="number($varNum) = number($varNum2)">
								<xsl:value-of select="'DUPLICATE'"/>
							</xsl:if>
						</xsl:for-each>
						<xsl:value-of select="$varNum"/>
					</xsl:when>
					<xsl:otherwise>
						<NumberIncrement:counternum counter="0"/>
						<xsl:value-of select="NumberIncrement:next()"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="starts-with($curnum, 'DUPLICATE')=false">
				<xsl:variable name="uframeName" select="MotionAttributes/ObjectFrameProfile"/>
				<xsl:variable name="posType">
					<xsl:call-template name="getPOSTYPE">
						<xsl:with-param name="uframeName" select="$uframeName"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- get uframe number -->
				<xsl:variable name="uframeNum">
					<xsl:call-template name="matchObjectFrameProfileName">
						<xsl:with-param name="nameToMatch" select="$uframeName"/>
						<xsl:with-param name="objectFrameProfileNodeSet"
							select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile"
						/>
					</xsl:call-template>
				</xsl:variable>

				<xsl:variable name="currentUFrame" select="KIRStrUtils:retrieveUFrameNum()"/>
				<xsl:if test="(position() = 1 or $uframeNum != $currentUFrame) and $posType = 'USER'">
					<xsl:value-of select="$cr"/>
					<xsl:text>///USER </xsl:text>
					<xsl:value-of select="$uframeNum"/>
					<xsl:value-of select="KIRStrUtils:storeUFrameNum($uframeNum)"/>
				</xsl:if>

				<xsl:variable name="toolName" select="MotionAttributes/ToolProfile"/>
				<xsl:if test="$toolName">
					<!-- get tool number -->
					<xsl:variable name="toolNum">
						<xsl:call-template name="matchToolProfileName">
							<xsl:with-param name="nameToMatch" select="$toolName"/>
							<xsl:with-param name="toolProfileNodeSet"
								select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile"
							/>
						</xsl:call-template>
					</xsl:variable>

					<!-- check to see if output tool number for C and P vars -->
					<xsl:choose>
						<xsl:when test="$varName=$pvar">
							<!-- P var -->
							<xsl:variable name="currentTool" select="KIRStrUtils:retrieveToolNum()"/>
							<xsl:if test="$toolNum != $currentTool">
								<xsl:value-of select="$cr"/>
								<xsl:text>///TOOL </xsl:text>
								<xsl:value-of select="$toolNum"/>
								<xsl:value-of select="KIRStrUtils:storeToolNum($toolNum)"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<!-- C var -->
							<!-- posvalue is previous Activity -->
							<xsl:variable name="posvalue" select="position() - 1"/>
							<xsl:if
								test="not($combineNodeSet[$posvalue]/MotionAttributes/ToolProfile = $toolName)">
								<xsl:value-of select="$cr"/>
								<xsl:text>///TOOL </xsl:text>
								<xsl:value-of select="$toolNum"/>
								<xsl:value-of select="KIRStrUtils:storeToolNum($toolNum)"/>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- test="$toolName" -->

				<xsl:if test="position() = 1 or $uframeNum != $currentUFrame">
					<xsl:value-of select="$cr"/>
					<xsl:text>///POSTYPE </xsl:text>
					<xsl:value-of select="$posType"/>
					<xsl:value-of select="KIRStrUtils:storeUFrameNum($uframeNum)"/>
				</xsl:if>
				<xsl:if test="position() = 1">
					<xsl:value-of select="$cr"/>
					<xsl:text>///RECTAN</xsl:text>
					<xsl:variable name="hr"
						select="MotomanUtils:SetParameterData('CURRENTPOSTYPE', 'RECTAN')"/>
				</xsl:if>
				<xsl:call-template name="rconf">
					<xsl:with-param name="target" select="Target"/>
				</xsl:call-template>

				<xsl:for-each
					select="AttributeList/Attribute[starts-with(AttributeName,'PrePosAttr')]/AttributeValue">
					<xsl:value-of select="$cr"/>
					<xsl:value-of select="."/>
				</xsl:for-each>

				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$varName"/>
				<xsl:value-of select="format-number($curnum, $numpat)"/>
				<xsl:text>=</xsl:text>
				<xsl:variable name="cartTgt" select="Target/CartesianTarget"/>
				<xsl:variable name="x">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$cartTgt/Position/@X"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="y">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$cartTgt/Position/@Y"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="z">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$cartTgt/Position/@Z"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="W">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$cartTgt/Orientation/@Yaw"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="P">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$cartTgt/Orientation/@Pitch"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="R">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="$cartTgt/Orientation/@Roll"/>
					</xsl:call-template>
				</xsl:variable>
        <xsl:variable name="patRot">
          <xsl:choose>
            <xsl:when test="$controller='DX100'">
              <xsl:value-of select="$patD4"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$patD2"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="format-number($x * 1000, $patD3)"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="format-number($y * 1000, $patD3)"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="format-number($z * 1000, $patD3)"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="format-number($W, $patRot)"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="format-number($P, $patRot)"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="format-number($R, $patRot)"/>

        <xsl:for-each
					select="AttributeList/Attribute[starts-with(AttributeName,'PostPosAttr')]/AttributeValue">
					<xsl:value-of select="$cr"/>
					<xsl:value-of select="."/>
				</xsl:for-each>
			</xsl:if>
			<!-- if non-duplicate -->
		</xsl:for-each>
	</xsl:template>
	<!-- end template outputC_P_cart -->

	<xsl:template name="outputAuxJoints">
		<xsl:param name="rbtMoveNode"/>
		<xsl:param name="varName"/>
		<xsl:param name="numpat" select="$numpat4"/>
		<xsl:param name="curnum"/>
		<xsl:param name="auxDevType"/>
		<xsl:param name="auxDevType2"/>
		<xsl:param name="actName"/>

		<xsl:variable name="downloadBPvar" select="MotomanUtils:GetParameterData('DOWNLOADBPVAR')"/>

		<xsl:for-each select="$rbtMoveNode/Target/JointTarget/AuxJoint[@Type = $auxDevType or @Type = $auxDevType2]">
			<xsl:variable name="pulseString" select="concat('PulseValue.', string(./@DOFNumber))"/>
			<xsl:variable name="zeroString" select="concat('ZeroValue.', string(./@DOFNumber))"/>

			<xsl:variable name="pulseValue" select="MotomanUtils:GetParameterData(string($pulseString))"/>
			<xsl:variable name="zeroValue" select="MotomanUtils:GetParameterData(string($zeroString))"/>

			<xsl:if test="position() = 1">
				<xsl:value-of select="$cr"/>
				<xsl:value-of select="$varName"/>
				<xsl:value-of select="format-number($curnum, $numpat)"/>
				<xsl:text>=</xsl:text>
			</xsl:if>
			<xsl:variable name="jval">
				<xsl:call-template name="Scientific">
					<xsl:with-param name="Num" select="./JointValue"/>
				</xsl:call-template>
			</xsl:variable>

			<xsl:variable name="pulsePar" select="boolean(string-length($pulseValue) != 0 and string-length($zeroValue) != 0)"/>
			<xsl:choose>
				<xsl:when test="$varName = 'BC'">
					<xsl:choose>
						<xsl:when test="$downloadTargetBC = 'Pulse' and $pulsePar = 'true'">
							<xsl:variable name="auxValue"
								select="MotomanUtils:JointToEncoderConversion(string(./@JointType), $pulseValue, ./JointValue, $zeroValue)"/>
							<xsl:value-of select="format-number($auxValue, $jointPositionPattern)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="./@Units = 'deg'">
									<xsl:value-of select="$jval"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of
										select="format-number($jval * 1000, $cartesianPositionPattern)"
									/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$varName = 'BP'">
					<xsl:choose>
						<xsl:when test="$downloadBPvar = 'RECTAN' or $downloadBPvar = 'CARTESIAN'">
							<xsl:choose>
								<xsl:when test="./@Units = 'deg'">
									<xsl:value-of select="$jval"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number($jval * 1000, $cartesianPositionPattern)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$downloadBPvar = 'PULSE' and $pulsePar = 'true'">
							<xsl:variable name="auxValue"
								select="MotomanUtils:JointToEncoderConversion(string(./@JointType), $pulseValue, ./JointValue, $zeroValue)"/>
							<xsl:value-of select="format-number($auxValue, $jointPositionPattern)"/>
						</xsl:when>
						<xsl:when test="$downloadTargetBC = 'Pulse' and $pulsePar = 'true'">
							<xsl:variable name="auxValue"
								select="MotomanUtils:JointToEncoderConversion(string(./@JointType), $pulseValue, ./JointValue, $zeroValue)"/>
							<xsl:value-of select="format-number($auxValue, $jointPositionPattern)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="./@Units = 'deg'">
									<xsl:value-of select="$jval"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number($jval * 1000, $cartesianPositionPattern)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="$pulsePar = 'true'">
							<xsl:variable name="auxValue"
								select="MotomanUtils:JointToEncoderConversion(string(./@JointType), $pulseValue, ./JointValue, $zeroValue)"/>
							<xsl:value-of select="format-number($auxValue, $jointPositionPattern)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="./@Units = 'deg'">
									<xsl:value-of select="$jval"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of
										select="format-number($jval * 1000, $cartesianPositionPattern)"
									/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- outputAuxJoints -->

	<xsl:template name="getNumIncNext">
		<xsl:choose>
			<xsl:when test="$debug">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NumberIncrement:next()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="getNumIncCurrent">
		<xsl:choose>
			<xsl:when test="$debug">1</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NumberIncrement:current()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="getNumOfDuplicatePosVars">
		<xsl:param name="nodeprecedeSet"/>
		<xsl:param name="nodeSet"/>
		<xsl:param name="nodefollowSet"/>
		<xsl:param name="posVar"/>

		<NumberIncrement:counternum counter="2"/>
		<NumberIncrement:startnum start="0"/>
		<!-- must reset for startnum -->
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:reset/>
		<!-- must reset for startnum -->
		<xsl:variable name="combineNodeSet" select="$nodeprecedeSet|$nodeSet|$nodefollowSet"/>
		<xsl:for-each select="$combineNodeSet">
			<xsl:variable name="posVarNum"
				select="AttributeList/Attribute[AttributeName=$posVar]/AttributeValue"/>
			<xsl:variable name="nodeSet2"
				select="following-sibling::Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute/AttributeName=$posVar]"/>
			<xsl:for-each select="$nodeSet2">
				<xsl:variable name="posVarNum2"
					select="AttributeList/Attribute[AttributeName=$posVar]/AttributeValue"/>
				<xsl:if test="number($posVarNum) = number($posVarNum2)">
					<xsl:variable name="tmpVar">
						<xsl:call-template name="getNumIncNext"/>
					</xsl:variable>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:call-template name="getNumIncCurrent"/>
	</xsl:template>

	<xsl:template name="getNumOfDuplicatePosVars2">
		<xsl:param name="nodeprecedeSet"/>
		<xsl:param name="nodeSet"/>
		<xsl:param name="nodefollowSet"/>
		<xsl:param name="posVar"/>

		<NumberIncrement:counternum counter="2"/>
		<NumberIncrement:startnum start="0"/>
		<!-- must reset for startnum -->
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:reset/>
		<!-- must reset for startnum -->
		<xsl:variable name="combineNodeSet" select="$nodeprecedeSet|$nodeSet|$nodefollowSet"/>
		<xsl:for-each select="$combineNodeSet">
			<xsl:variable name="posVarNum"
				select="AttributeList/Attribute[AttributeName='PosReg']/AttributeValue"/>
			<xsl:variable name="posVarType"
				select="AttributeList/Attribute[AttributeName='PosType']/AttributeValue"/>
			<xsl:variable name="nodeSet2"
				select="following-sibling::Activity[@ActivityType='DNBRobotMotionActivity' and AttributeList/Attribute[AttributeName='PosType']/AttributeValue=$posVar]"/>
			<xsl:for-each select="$nodeSet2">
				<xsl:variable name="posVarNum2"
					select="AttributeList/Attribute[AttributeName='PosReg']/AttributeValue"/>
				<xsl:if
					test="number($posVarNum) = number($posVarNum2) and string($posVarType)=string($posVar)">
					<xsl:variable name="tmpVar">
						<xsl:call-template name="getNumIncNext"/>
					</xsl:variable>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:call-template name="getNumIncCurrent"/>
	</xsl:template>
	
	<xsl:template name="outputUFrameCND">
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE START UFRAME.CND</xsl:text>
		<xsl:call-template name="outputUFrame"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END</xsl:text>
	</xsl:template>

	<xsl:template name="outputUFrame">
		<xsl:param name="counter" select="1"/>
		
		<xsl:variable name="toolProfiles" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile"/>
		<xsl:variable name="numTool" select="count($toolProfiles)"/>
		<xsl:variable name="objFrameProfiles" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ObjectFrameProfileList/ObjectFrameProfile"/>
		<xsl:variable name="numObjFrame" select="count($objFrameProfiles)"/>
		<xsl:variable name="toolsUsed" select="MotomanUtils:GetParameterData('TOOL PROFILES USED')"/>
		
		<xsl:variable name="stopNum">
			<xsl:choose>
				<xsl:when test="$numTool &gt; $numObjFrame">
					<xsl:value-of select="$numTool"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$numObjFrame"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:if test="$counter &lt; $stopNum+1"> <!-- "&le;" does not work -->
		
			<xsl:choose>
				<xsl:when test="$counter &lt; $numObjFrame+1"> <!-- "&le;" does not work -->
					<xsl:choose>
						<xsl:when test="$toolProfiles[$counter]/ToolType = 'Stationary' and contains($toolsUsed, $toolProfiles[$counter]/Name)">
							<xsl:call-template name="outputUFrameA">
								<xsl:with-param name="ufnum" select="$counter - 1"/><!-- UFrame numbers start from 0 -->
								<xsl:with-param name="name" select="$toolProfiles[$counter]/Name"/>
							</xsl:call-template>
							<xsl:call-template name="outputToolVal">
								<xsl:with-param name="tool" select="$toolProfiles[$counter]"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="outputUFrameA">
								<xsl:with-param name="ufnum" select="$counter - 1"/><!-- UFrame numbers start from 0 -->
								<xsl:with-param name="name" select="$objFrameProfiles[$counter]/Name"/>
							</xsl:call-template>
							<xsl:call-template name="outputBaseVal">
								<xsl:with-param name="obj" select="$objFrameProfiles[$counter]"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$numObjFrame &lt; $numTool">
					<xsl:if test="$toolProfiles[$counter]/ToolType = 'Stationary' and contains($toolsUsed, $toolProfiles[$counter]/Name)">
						<xsl:call-template name="outputUFrameA">
							<xsl:with-param name="ufnum" select="$counter - 1"/><!-- UFrame numbers start from 0 -->
							<xsl:with-param name="name" select="$toolProfiles[$counter]/Name"/>
						</xsl:call-template>
						<xsl:call-template name="outputToolVal">
							<xsl:with-param name="tool" select="$toolProfiles[$counter]"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:when>
			</xsl:choose>
				
			<xsl:call-template name="outputUFrame">
				<xsl:with-param name="counter" select="$counter + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="outputUFrameA">
		<xsl:param name="ufnum"/>
		<xsl:param name="name"/>
		
		<xsl:value-of select="$cr"/>
		<xsl:text>//UFRAME </xsl:text>
		<xsl:value-of select="$ufnum"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>///NAME </xsl:text>
		<xsl:value-of select="$name"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>///TOOL 1</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>///GROUP 1,0,0,0,0,0,0,0</xsl:text>
    <!--
    <xsl:if test="$controller='DX100'">
      <xsl:text>,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0</xsl:text>
    </xsl:if>
    -->
    <xsl:value-of select="$cr"/>
		<xsl:text>///PULSE</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>////RORG C000= 0,0,0,0,0,0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>////RXX C001= 0,0,0,0,0,0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>////RYY C002= 0,0,0,0,0,0</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:text>////BUSER </xsl:text>
	</xsl:template>
	
	<xsl:template name="outputToolCND">
		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE START TOOL.CND</xsl:text>

		<xsl:variable name="toolProfiles"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/ToolProfileList/ToolProfile"/>
		<xsl:for-each select="$toolProfiles">
			<xsl:value-of select="$cr"/>
			<xsl:text>//TOOL </xsl:text>
			<xsl:value-of select="position()-1"/>
			<xsl:value-of select="$cr"/>
			<xsl:text>///NAME</xsl:text>
			<xsl:if test="Name != 'Default'">
				<xsl:text> </xsl:text>
				<xsl:value-of select="Name"/>
			</xsl:if>
			<xsl:value-of select="$cr"/>
			<xsl:call-template name="outputToolVal">
				<xsl:with-param name="tool" select="."/>
			</xsl:call-template>

      <!-- centroid -->
      <xsl:value-of select="$cr"/>
      <xsl:value-of select="'0.000,0.000,0.000'"/>
      <!-- mass -->
      <xsl:value-of select="$cr"/>
			<xsl:value-of select="'0.000'"/>
      <!-- newer controller additional lines -->
      <xsl:if test="$xrc_pl=4 or $xrc_pl=8 or $controller='NX100' or $controller='DX100'">
        <!-- inertia -->
        <xsl:value-of select="$cr"/>
        <xsl:value-of select="'0.000,0.000,0.000'"/>
        <!-- reserved -->
        <xsl:value-of select="$cr"/>
        <xsl:choose>
          <xsl:when test="$controller='NX100'">
            <xsl:value-of select="'0.000,0'"/>
          </xsl:when>
          <xsl:when test="$controller='DX100'">
            <xsl:value-of select="'0.000,0'"/><!-- not additional '1' -->
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'0,0'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
		</xsl:for-each>
	
		<xsl:call-template name="outputDummyTool">
			<xsl:with-param name="stop" select="count($toolProfiles)"/>
		</xsl:call-template>

		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END</xsl:text>
	</xsl:template>

	<xsl:template name="outputToolVal">
		<xsl:param name="tool"/>
		
		<xsl:variable name="x">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$tool/TCPOffset/TCPPosition/@X"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="y">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$tool/TCPOffset/TCPPosition/@Y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="z">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$tool/TCPOffset/TCPPosition/@Z"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="w">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$tool/TCPOffset/TCPOrientation/@Yaw"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="p">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$tool/TCPOffset/TCPOrientation/@Pitch"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="r">
			<xsl:call-template name="Scientific">
				<xsl:with-param name="Num" select="$tool/TCPOffset/TCPOrientation/@Roll"/>
			</xsl:call-template>
		</xsl:variable>
    <xsl:variable name="patRot">
      <xsl:choose>
        <xsl:when test="$controller='DX100'">
          <xsl:value-of select="$patD4"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$patD2"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="format-number($x*1000, $patD3)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($y*1000, $patD3)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($z*1000, $patD3)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($w, $patRot)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($p, $patRot)"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="format-number($r, $patRot)"/>
  </xsl:template>
	
	<xsl:template name="outputDummyTool">
		<xsl:param name="stop"/>

    <xsl:variable name="numoftools">
      <xsl:choose>
        <xsl:when test="$controller='DX100'">
          <xsl:value-of select="64"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="24"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="$stop &lt; $numoftools">
      <xsl:value-of select="$cr"/>
			<xsl:text>//TOOL </xsl:text>
			<xsl:value-of select="$stop"/>
			<xsl:value-of select="$cr"/>
			<xsl:text>///NAME</xsl:text>
			<xsl:value-of select="$cr"/>
      <xsl:choose>
        <xsl:when test="$controller='DX100'">
          <xsl:value-of select="'0.000,0.000,0.000,0.0000,0.0000,0.0000'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'0.000,0.000,0.000,0.00,0.00,0.00'"/>
        </xsl:otherwise>
      </xsl:choose>
      <!-- centroid -->
      <xsl:value-of select="$cr"/>
      <xsl:value-of select="'0.000,0.000,0.000'"/>
      <!-- mass -->
      <xsl:value-of select="$cr"/>
			<xsl:value-of select="'0.000'"/>
      <!-- newer controller additional lines -->
      <xsl:if test="$xrc_pl=4 or $xrc_pl=8 or $controller='NX100' or $controller='DX100'">
        <!-- inertia -->
        <xsl:value-of select="$cr"/>
        <xsl:value-of select="'0.000,0.000,0.000'"/>
        <!-- reserved -->
        <xsl:value-of select="$cr"/>
        <xsl:choose>
          <xsl:when test="$controller='NX100'">
            <xsl:value-of select="'0.000,0'"/>
          </xsl:when>
          <xsl:when test="$controller='DX100'">
            <xsl:value-of select="'0.000,0'"/><!-- no additional '1' -->
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'0,0'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>

			<xsl:call-template name="outputDummyTool">
				<xsl:with-param name="stop" select="$stop + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="getToolNum">
		<xsl:param name="toolName"/>

		<xsl:variable name="pList" select="/OLPData/Resource/ParameterList/Parameter"/>
		<xsl:variable name="p"
			select="$pList[starts-with(ParameterName, 'Tool.') and ParameterValue=$toolName]"/>
		<xsl:choose>
			<xsl:when test="$p != ''">
				<xsl:value-of select="substring-after($p/ParameterName, '.')"/>
			</xsl:when>
			<xsl:otherwise>-1</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

  <!-- output standard motion commands -->
  <xsl:template name="outputMotionCommand">
    <xsl:param name="moType"/>
    <xsl:param name="nextMoType"/>
    <xsl:param name="toolType"/>
    <xsl:param name="toolNum"/>

    <xsl:choose>
      <xsl:when test="$nextMoType='CircularVia'">
        <xsl:choose>
          <xsl:when test="$toolType='Stationary'">
            <xsl:text>EIMOVC UF#(</xsl:text>
            <xsl:value-of select="$toolNum"/>
            <xsl:text>)</xsl:text>
          </xsl:when>
          <xsl:otherwise>MOVC</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$moType='Joint'">MOVJ</xsl:when>
      <xsl:when test="$moType='Linear'">
        <xsl:choose>
          <xsl:when test="$toolType='Stationary'">
            <xsl:text>EIMOVL UF#(</xsl:text>
            <xsl:value-of select="$toolNum"/>
            <xsl:text>)</xsl:text>
          </xsl:when>
          <xsl:otherwise>MOVL</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$moType='Circular' or $moType='CircularVia'">
        <xsl:choose>
          <xsl:when test="$toolType='Stationary'">
            <xsl:text>EIMOVC UF#(</xsl:text>
            <xsl:value-of select="$toolNum"/>
            <xsl:text>)</xsl:text>
          </xsl:when>
          <xsl:otherwise>MOVC</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <!-- end template outputMotionCommand -->

  <xsl:template name="outputVelocityAccuracy">
		<xsl:param name="userData"/>
		<xsl:param name="moveNode"/>
		<xsl:param name="baseGroup"/>
		<xsl:param name="stationGroup"/>
		<xsl:variable name="refp"
			select="string($moveNode/AttributeList/Attribute[AttributeName='REFP']/AttributeValue)"/>
		<xsl:variable name="type"
			select="string($moveNode/AttributeList/Attribute[AttributeName='Type']/AttributeValue)"/>
		<xsl:variable name="clfval"
			select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='CLF#']/AttributeValue)"/>
		<xsl:variable name="plinval"
			select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='PLIN']/AttributeValue)"/>
		<xsl:variable name="ploutval"
			select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='PLOUT']/AttributeValue)"/>
		<xsl:variable name="wgoval"
			select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='WGO']/AttributeValue)"/>
		<xsl:variable name="wtmval"
			select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='WTM']/AttributeValue)"/>
		<xsl:variable name="wstval"
			select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='WST']/AttributeValue)"/>
		<xsl:variable name="pressval"
			select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName='PRESS#']/AttributeValue)"/>
		<xsl:variable name="moProfileAttr" select="$moveNode/AttributeList/Attribute[AttributeName='MotionProfile']/AttributeValue"/>
		<xsl:variable name="gunNumFromParameter">
			<xsl:call-template name="getToolNum">
				<xsl:with-param name="toolName" select="ToolResource/ResourceName"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="gunNum">
			<xsl:choose>
				<xsl:when test="$gunNumFromParameter &gt; 0">
					<xsl:value-of select="$gunNumFromParameter"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of
						select="string($moveNode/following-sibling::*[position() = 1]/AttributeList/Attribute[AttributeName = 'GUN#']/AttributeValue)"
					/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="motionProfile" select="string($moveNode/MotionAttributes/MotionProfile)"/>
		<xsl:variable name="moType" select="string($moveNode/MotionAttributes/MotionType)"/>
    <xsl:variable name="nextMoAct" select="$moveNode/following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
    <xsl:variable name="nextMoType" select="string($nextMoAct/MotionAttributes/MotionType)"/>
    <xsl:variable name="speedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Value"/>
		<xsl:variable name="angularSpeedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/AngularSpeedValue/@Value"/>
		<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
		<xsl:variable name="flybyMode"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/FlyByMode"/>
		<xsl:variable name="accuracyValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyValue/@Value"/>
		<xsl:variable name="accuracyType"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]/AccuracyType"/>
		<xsl:variable name="speedUnits"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Units "/>
		<xsl:variable name="maxSpeedValue"
			select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
		<!-- Velocity -->
		<xsl:choose>
			<xsl:when test="$refp != ''"/>
			<xsl:when test="$type = 'search' and string($moType)='Linear'"/>
			<xsl:when test="$baseGroup='2' or $stationGroup='2'">
				<xsl:text> VJ=</xsl:text>
				<xsl:choose>
					<xsl:when test="$speedUnits = '%'">
						<xsl:value-of select="format-number($speedValue, $patUptoD2)"/>
					</xsl:when>
					<xsl:when test="$speedUnits = 'm/s'">
						<xsl:value-of
							select="format-number($speedValue div $maxSpeedValue * 100, $patUptoD2)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="50"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when
				test="starts-with($motionProfile,'VJ=D') or starts-with($motionProfile,'VJ=I') or starts-with($motionProfile,'V=D') or starts-with($motionProfile,'V=I')">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$motionProfile"/>
			</xsl:when>
			<xsl:when test="$moProfileAttr != ''">
				<!-- attribute MotionProfile stores motion speed replaced by ARCOn speed -->
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="$moType='Joint'">
						<xsl:if test="starts-with($moProfileAttr,'VJ=')">
							<xsl:value-of select="$moProfileAttr"/>
						</xsl:if>
						<xsl:if test="starts-with($moProfileAttr,'V=')">
							<xsl:variable name="attrSpeed" select="number(substring-after($moProfileAttr,'V='))"/>
							<xsl:text>VJ=</xsl:text>
							<xsl:value-of select="format-number($attrSpeed div ($maxSpeedValue * 10), $patUptoD2)"/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="starts-with($moProfileAttr,'V=')">
							<xsl:value-of select="$moProfileAttr"/>
						</xsl:if>
						<xsl:if test="starts-with($moProfileAttr,'VJ=')">
							<xsl:variable name="attrSpeed" select="number(substring-after($moProfileAttr, 'VJ='))"/>
							<xsl:text>V=</xsl:text>
							<xsl:value-of select="format-number($attrSpeed * $maxSpeedValue * 10, $patUptoD1)"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$moType='Joint' and $nextMoType!='CircularVia'">
				<xsl:text> VJ=</xsl:text>
				<xsl:choose>
					<xsl:when test="$speedUnits = '%'">
						<xsl:value-of select="format-number($speedValue, $patUptoD2)"/>
					</xsl:when>
					<xsl:when test="$speedUnits = 'm/s'">
						<xsl:value-of
							select="format-number($speedValue div $maxSpeedValue * 100, $patUptoD2)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="50"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$moType='Linear' or
        $nextMoType='CircularVia' or
				$moType='Circular' or
				$moType='CircularVia'">
				<xsl:choose>
					<xsl:when test="starts-with($motionProfile, 'VR')">
						<xsl:text> VR=</xsl:text>
						<xsl:value-of select="format-number($angularSpeedValue, $patD1)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> V=</xsl:text>
						<xsl:choose>
							<xsl:when test="$speedUnits = '%'">
								<xsl:value-of select="format-number($speedValue * $maxSpeedValue * 10, $patUptoD1)"/>
							</xsl:when>
							<xsl:when test="$speedUnits = 'm/s'">
								<xsl:value-of select="format-number($speedValue * 1000, $patUptoD1)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="500"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
		<!-- Position Level -->
		<xsl:choose>
			<xsl:when test="$refp != ''"/>
			<xsl:when test="$type = 'search' and string($moType)='Linear'"/>
			<xsl:when test="$clfval != ''">
				<xsl:if test="$plinval != ''">
					<xsl:text> PLIN=</xsl:text>
					<xsl:value-of select="$plinval"/>
				</xsl:if>
				<xsl:if test="$ploutval != ''">
					<xsl:text> PLOUT=</xsl:text>
					<xsl:value-of select="$ploutval"/>
				</xsl:if>
				<xsl:text> CLF#(</xsl:text>
				<xsl:value-of select="$clfval"/>
				<xsl:text>)</xsl:text>
				<xsl:text> GUN#(</xsl:text>
				<xsl:value-of select="$gunNum"/>
				<xsl:text>)</xsl:text>
				<xsl:text> PRESS#(</xsl:text>
				<xsl:value-of select="$pressval"/>
				<xsl:text>)</xsl:text>
				<xsl:text> WTM=</xsl:text>
				<xsl:value-of select="$wtmval"/>
				<xsl:text> WST=</xsl:text>
				<xsl:value-of select="$wstval"/>
				<xsl:if test="$wgoval != ''">
					<xsl:text> WGO=</xsl:text>
					<xsl:value-of select="$wgoval"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="spdl" select="string($moveNode/AttributeList/Attribute[AttributeName='SPDL']/AttributeValue)"/>
				<xsl:choose>
					<xsl:when test="$spdl='0'"><xsl:text> SPDL=0</xsl:text></xsl:when>
					<xsl:when test="starts-with($accuracyProfile,'MotomanDefault')='true'"></xsl:when>
					<xsl:when test="$flybyMode='On' and $accuracyType = 'Speed' and $accuracyValue = 100"></xsl:when>
					<xsl:otherwise>
						<xsl:text> PL=</xsl:text>
						<xsl:call-template name="GetAccuracy">
							<xsl:with-param name="flybyMode">
								<xsl:value-of select="$flybyMode"/>
							</xsl:with-param>
							<xsl:with-param name="accuracyValue">
								<xsl:value-of select="$accuracyValue"/>
							</xsl:with-param>
							<xsl:with-param name="accuracyType">
								<xsl:value-of select="$accuracyType"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$userData='true'">
					<xsl:apply-templates select="$moveNode/MotionAttributes/UserProfile"/>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

  <xsl:template name="get_systart">
    <xsl:param name="conveyorinitialposition"/>

    <xsl:text>SYSTART </xsl:text>
    <xsl:call-template name="get_cv"/>
    <xsl:text> STP=</xsl:text>
    <xsl:value-of select="format-number($conveyorinitialposition*1000.0, $patD3)"/>
  </xsl:template>

  <xsl:template name="set_cv">
    <xsl:param name="objprofilename"/>
    <xsl:param name="conveyornumber"/>

    <xsl:choose>
      <xsl:when test="starts-with($objprofilename,'CV#(') and 
                      substring($objprofilename, string-length($objprofilename), 1) = ')' and
                      string(number(substring($objprofilename,5,string-length($objprofilename)-5)))!='NaN'">
        <xsl:variable name="hr" select="MotomanUtils:SetParameterData('CV#', $objprofilename)"/>
      </xsl:when>
      <xsl:when test="$conveyornumber != ''">
        <xsl:variable name="hr" select="MotomanUtils:SetParameterData('CV#', concat('CV#(',$conveyornumber,')'))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="errorMsg_LineTrackCV"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="get_cv">
    <xsl:param name="objprofilename" select="''"/>
    <xsl:param name="conveyornumber" select="''"/>

    <xsl:choose>
      <xsl:when test="starts-with($objprofilename,'CV#(') and 
                      substring($objprofilename, string-length($objprofilename), 1) = ')' and
                      string(number(substring($objprofilename,5,string-length($objprofilename)-5)))!='NaN'">
        <xsl:variable name="hr" select="MotomanUtils:SetParameterData('CV#', $objprofilename)"/>
      </xsl:when>
      <xsl:when test="$conveyornumber != ''">
        <xsl:variable name="hr" select="MotomanUtils:SetParameterData('CV#', concat('CV#(',$conveyornumber,')'))"/>
      </xsl:when>
    </xsl:choose>
    <xsl:value-of select="MotomanUtils:GetParameterData('CV#')"/>
  </xsl:template>

  <!-- set STORE_SYEND if uploaded as 'Robot Language' -->
  <xsl:template name="set_syend">
    <xsl:param name="moveNode"/>

    <xsl:variable name="hr" select="MotomanUtils:SetParameterData('SET_SYEND', 'TRUE')"/>
    <!-- check following siblings until a robot motion activity -->
    <xsl:variable name="fsibs" select="$moveNode/following-sibling::node()"/>
    <xsl:for-each select="$fsibs">
      <xsl:if test="@ActivityType = 'DNBRobotMotionActivity'">
        <xsl:variable name="hr1" select="MotomanUtils:SetParameterData('SET_SYEND', 'FALSE')"/>
      </xsl:if>
      <xsl:variable name="hr_set_syend" select="MotomanUtils:GetParameterData('SET_SYEND')"/>
      <xsl:if test="string($hr_set_syend) = 'TRUE'">
        <xsl:for-each select="AttributeList/Attribute">
          <xsl:variable name="attrname" select="AttributeName"/>
          <xsl:variable name="attrvalue" select="AttributeValue"/>
          <xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
          <xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
          <xsl:if test="$precomchk = 'PreComment' and starts-with($striprobotlang, 'SYEND CV#(')">
            <xsl:variable name="hr1" select="MotomanUtils:SetParameterData('STORE_SYEND', $striprobotlang)"/>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>
    
    <!-- if STORE_SYEND is blank, set with the following move -->
    <xsl:variable name="hr_syend" select="MotomanUtils:GetParameterData('STORE_SYEND')"/>
    <xsl:if test="string($hr_syend) = ''">
      <xsl:variable name="fmov" select="$moveNode/following-sibling::Activity[@ActivityType='DNBRobotMotionActivity'][position()=1]"/>
      <xsl:for-each select="$fmov/AttributeList/Attribute">
        <xsl:variable name="attrname" select="AttributeName"/>
        <xsl:variable name="attrvalue" select="AttributeValue"/>
        <xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
        <xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
        <xsl:if test="$precomchk = 'PreComment' and starts-with($striprobotlang, 'SYEND CV#(')">
          <xsl:variable name="hr1" select="MotomanUtils:SetParameterData('STORE_SYEND', $striprobotlang)"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:if>
    
    <!-- if STORE_SYEND is blank, set with activity list post comment -->
    <xsl:variable name="fmov" select="$moveNode/following-sibling::Activity[@ActivityType='DNBRobotMotionActivity']"/>
    <!-- if there is a non-Track move and no 'SYEND' uploaded as 'Robot Language',
         SYEND should be output before non-Track move.
         Therefore, only check activitylist attributes when there is no more moves
    -->
    <xsl:variable name="hr_syend_act" select="MotomanUtils:GetParameterData('STORE_SYEND')"/>
    <xsl:if test="string($hr_syend_act) = '' and count($fmov) = 0">
      <xsl:variable name="actlist" select="$moveNode/parent::node()"/>
      <xsl:for-each select="$actlist/AttributeList/Attribute">
        <xsl:variable name="attrname" select="AttributeName"/>
        <xsl:variable name="attrvalue" select="AttributeValue"/>
        <xsl:variable name="postcomchk" select="substring($attrname,1,11)"/>
        <xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
        <xsl:if test="$postcomchk = 'PostComment' and starts-with($striprobotlang, 'SYEND CV#(')">
          <xsl:variable name="hr1" select="MotomanUtils:SetParameterData('STORE_SYEND', $striprobotlang)"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  <!-- end set_syend-->
  
  <xsl:template name="SetHighestPositionNumber">
		<xsl:param name="varName" select="'C'"/>
		<xsl:variable name="varType">
			<xsl:choose>
				<xsl:when test="$varName='BP' or $varName='EX'">P</xsl:when>
				<xsl:otherwise>C</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="reg">
			<xsl:choose>
				<xsl:when
					test="string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue) != ''">
					<xsl:value-of
						select="string(AttributeList/Attribute[AttributeName='PosReg']/AttributeValue)"
					/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="localtagname" select="Target/CartesianTarget/Tag"/>
					<xsl:variable name="localtagSuffix"
						select="substring-after($localtagname,$varType)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="curReg" select="NumberIncrement:current()"/>
		<xsl:if test="string($reg) != '' and number($reg) &gt; number($curReg)">
			<xsl:call-template name="IncrementToHighest">
				<xsl:with-param name="count" select="number($reg)-number($curReg)"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="IncrementToHighest">
		<xsl:param name="count"/>
		<xsl:if test="number($count)>0">
			<xsl:variable name="incReg" select="NumberIncrement:next()"/>
			<xsl:call-template name="IncrementToHighest">
				<xsl:with-param name="count" select="number($count)-1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="toupper">
		<xsl:param name="instring"/>
		<xsl:value-of select="translate($instring, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	</xsl:template>
</xsl:stylesheet>
