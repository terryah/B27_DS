<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:lxslt="http://xml.apache.org/xslt" 
xmlns:java="http://xml.apache.org/xslt/java" 
xmlns:NumberIncrement="NumberIncrement" 
xmlns:MatrixUtils="MatrixUtils" 
xmlns:FileUtils="FileUtils" 
xmlns:BitMaskUtils="BitMaskUtils" 
extension-element-prefixes="NumberIncrement MatrixUtils FileUtils BitMaskUtils">
	
	<lxslt:component prefix="NumberIncrement" functions="next getProgramNum" elements="startnum increment reset counternum">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.NumberIncrement"/>
	</lxslt:component>
	<lxslt:component prefix="MatrixUtils" functions="dgXyzyprToMatrix dgInvert dgCatXyzyprMatrix dgGetX dgGetY dgGetZ dgGetYaw dgGetPitch dgGetRoll" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.MatrixUtils"/>
	</lxslt:component>
	<lxslt:component prefix="BitMaskUtils" functions="bitAND bitOR bitShiftLeft bitShiftRight" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.BitMaskUtils"/>
	</lxslt:component>
	<lxslt:component prefix="FileUtils" functions="fileOpen fileWrite fileClose" elements="">
		<lxslt:script lang="javaclass" src="com.dassault_systemes.delmia.XSLExtensions.FileUtils"/>
	</lxslt:component>

	<xsl:strip-space elements="*"/>
	<xsl:output method="text"/>
	<xsl:variable name="debug" select="0"/>
	<xsl:variable name="version" select="'DELMIA CORP. DAIHEN DOWNLOADER VERSION 5 RELEASE 20 SP2'"/>
	<xsl:variable name="copyright" select="'COPYRIGHT DELMIA CORP. 1986-2010, ALL RIGHTS RESERVED'"/>
	<xsl:variable name="rbttype" select="Robot"/>
	<xsl:variable name="cr" select="'&#xa;'"/>
	<xsl:variable name="openCurlyBrace"  select="'&#x7B;'"/>
	<xsl:variable name="closeCurlyBrace" select="'&#x7D;'"/>
	<xsl:variable name="pat" select="'0'"/>
	<xsl:variable name="robotName" select="/OLPData/Resource[@ResourceType='Robot']/GeneralInfo/ResourceName"/>
	<xsl:variable name="NumBaseAuxAxes"    select="/OLPData/Resource/GeneralInfo/NumberOfAuxiliaryAxes"/>
	<xsl:variable name="NumToolAuxAxes"    select="/OLPData/Resource/GeneralInfo/NumberOfExternalAxes"/>
	<xsl:variable name="NumPositionerAxes" select="/OLPData/Resource/GeneralInfo/NumberOfPositionerAxes"/>
	<xsl:variable name="downloadCoord"     select="/OLPData/Resource/GeneralInfo/DownloadCoordinates"/>
	<xsl:variable name="numpat" select="'0'"/>
	<xsl:variable name="posnum" select="0"/>
	<xsl:variable name="targetUnits" select="1000.0"/>
	<xsl:variable name="jValPat" select="'0.0000'"/>
	<xsl:variable name="posPat" select="'0.0'"/>
	<xsl:variable name="oriPat" select="'0.0000'"/>
	<xsl:variable name="delayTimePat" select="'0.0'"/>
	<xsl:variable name="portNumPat" select="'000'"/>
	<xsl:variable name="progNumPat" select="'000'"/>
	<xsl:variable name="toolPattern" select="'0.0##'"/>
	<xsl:variable name="maxdigital" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Digital_IO']/ParameterValue"/>
	<xsl:variable name="maxrobot" select="/OLPData/Resource/ParameterList/Parameter[ParameterName='Robot_IO']/ParameterValue"/>
	<xsl:variable name="maxSpeedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/GeneralInfo/MaxSpeed "/>
	<xsl:variable name="jointSpdPat" select="'0'"/>
	<xsl:variable name="linearSpdPat" select="'0'"/>
	<xsl:variable name="pi" select="3.141592653589"/>
	<xsl:variable name="toDeg" select="180 div $pi"/>
	<xsl:variable name="toRad" select="$pi div 180"/>
	<xsl:variable name="toCMperMIN" select="6000"/>

	<xsl:template match="/">
		<NumberIncrement:counternum counter="0"/> <!-- sequence number -->
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>
		<NumberIncrement:counternum counter="1"/> <!-- number of nested IF blocks -->
		<NumberIncrement:startnum start="0"/>
		<NumberIncrement:increment inc="1"/>

		<xsl:call-template name="HeaderInfo"/>

		<xsl:call-template name="InitializeParameters"/>
		<xsl:apply-templates select="OLPData"/>
	</xsl:template>

	<xsl:template name="HeaderInfo">
		<xsl:text>VERSION INFO START</xsl:text>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$version"/>
		<xsl:value-of select="$cr"/>
		<xsl:value-of select="$copyright"/>
		<xsl:value-of select="$cr"/>
		<xsl:text>VERSION INFO END</xsl:text>
		<xsl:value-of select="$cr"/>
	</xsl:template>
	
	<xsl:template name="IOSignal"/>

	<xsl:template name="InitializeParameters">
	</xsl:template>
	
	<xsl:template match="OLPData">
		<xsl:call-template name="programLogic">
			<xsl:with-param name="plm" select="Resource[GeneralInfo/ResourceName=$robotName]/ProgramLogic"/>
		</xsl:call-template>
		<xsl:apply-templates select="Resource[GeneralInfo/ResourceName=$robotName]/ActivityList"/>
	</xsl:template>

	<!-- Program Logic section -->
	<xsl:template name="programLogic">
		<xsl:param name="plm"/>

		<xsl:if test="$plm">
			<NumberIncrement:counternum counter="0"/>
			<NumberIncrement:reset/>

			<xsl:for-each select="$plm/Jobs/Job">
				<xsl:variable name="numStartExpr" select="count(StartCondition/Expression)"/>
				<xsl:choose>
					<xsl:when test="$numStartExpr=1 and StartCondition/Expression='true'">
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="StartCondition/Operator">
							<xsl:if test=".='or'">
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
								<xsl:text>Logic operator OR not supported.</xsl:text>
								<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO END</xsl:text>
							</xsl:if>
						</xsl:for-each>
				
						<xsl:for-each select="StartCondition/Expression">
							<xsl:call-template name="getExprSeqNum">
								<xsl:with-param name="expr" select="."/>
							</xsl:call-template>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:for-each select="CallTask">
					<xsl:variable name="tmpVar">
						<xsl:call-template name="getNumIncNext"/>
					</xsl:variable>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:variable name="seqNum">
				<xsl:call-template name="getNumIncNext"/>
			</xsl:variable>

			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE START P</xsl:text>
			<xsl:variable name="progNum">
				<xsl:call-template name="getProgNum">
					<xsl:with-param name="progName" select="$plm/Jobs/@Name"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:value-of select="format-number($progNum, $progNumPat)"/>
			<xsl:text>.ASC</xsl:text>

			<xsl:value-of select="$cr"/>
			<xsl:text> P</xsl:text>
			<xsl:value-of select="format-number($progNum, $progNumPat)"/>
			<xsl:text>.ASC </xsl:text>
			<xsl:value-of select="$seqNum"/>
			<xsl:choose>
				<xsl:when test="AttributeList">
					<xsl:apply-templates select="AttributeList"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="/OLPData/Resource/ActivityList[1]/AttributeList"/>
				</xsl:otherwise>
			</xsl:choose>

			<NumberIncrement:counternum counter="0"/>
			<NumberIncrement:reset/>

			<!-- PM must be followed by P or L. Can't do infinit loop w/o moves.
			<xsl:call-template name="seq"/>
			<xsl:text>PM 1 1 0 0 0;</xsl:text>
			-->

			<xsl:apply-templates select="$plm/Jobs/Job"/>

			<!-- no infinit loop w/o moves
			<xsl:call-template name="seq"/>
			<xsl:text>JP 4 000 1;</xsl:text>
			-->

			<xsl:call-template name="seq"/>
			<xsl:text>END;</xsl:text>

			<xsl:value-of select="$cr"/>
			<xsl:text>DATA FILE END</xsl:text>
		</xsl:if>
		<!-- program logic exists -->

	</xsl:template>
	<!-- end of template programLogic -->

	<!-- output sequence number -->
	<xsl:template name="seq">
		<xsl:value-of select="$cr"/>
		<xsl:text> </xsl:text>
		<NumberIncrement:counternum counter="0"/>
		<xsl:call-template name="getNumIncNext"/>
		<xsl:text> </xsl:text>
	</xsl:template>

	<xsl:template name="getNumIncNext">
		<xsl:choose>
			<xsl:when test="$debug">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NumberIncrement:next()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="getNumIncCurrent">
		<xsl:choose>
			<xsl:when test="$debug">1</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NumberIncrement:current()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="getProgNum">
		<xsl:param name="progName"/>

		<xsl:choose>
			<xsl:when test="$debug">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="NumberIncrement:getProgramNum($progName)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="getExprSeqNum">
		<xsl:param name="expr"/>

		<xsl:choose>
			<xsl:when test="contains($expr, ' and ')">
				<xsl:variable name="tmpVar">
					<xsl:call-template name="getNumIncNext"/> <!-- IF -->
				</xsl:variable>
				<xsl:variable name="tmpVar">
					<xsl:call-template name="getNumIncNext"/> <!-- FI -->
				</xsl:variable>

				<xsl:variable name="tmpBln" select="normalize-space(substring-after($expr, '='))"/>
				<xsl:variable name="tmpOp" select="normalize-space(substring-after($tmpBln, ' '))"/>
				<xsl:variable name="expr2" select="normalize-space(substring-after($tmpOp, ' '))"/>
				<xsl:call-template name="getExprSeqNum">
					<xsl:with-param name="expr" select="$expr2"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="contains($expr, ' or ')">
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>Logic operator OR not supported.</xsl:text>
				<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="tmpVar">
					<xsl:call-template name="getNumIncNext"/> <!-- IF -->
				</xsl:variable>
				<xsl:variable name="tmpVar2">
					<xsl:call-template name="getNumIncNext"/> <!-- FI -->
				</xsl:variable>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template getExprSeqNum -->

	<xsl:template match="Job">
		<NumberIncrement:counternum counter="1"/>
		<NumberIncrement:reset/>

		<xsl:value-of select="$cr"/>
		<xsl:text> 0 CMT </xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>;</xsl:text>

		<xsl:variable name="numStartExpr" select="count(StartCondition/Expression)"/>
		<xsl:choose>
			<xsl:when test="$numStartExpr=1 and StartCondition/Expression='true'">
				<xsl:apply-templates select="SetOutput|WaitExpression|CallTask"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="StartCondition/Expression">
					<xsl:with-param name="type" select="'StartCondition'"/>
				</xsl:apply-templates>

				<xsl:apply-templates select="SetOutput|WaitExpression|CallTask"/>

				<NumberIncrement:counternum counter="1"/>
				<xsl:variable name="numIF">
					<xsl:call-template name="getNumIncCurrent"/>
				</xsl:variable>
				<xsl:call-template name="outputFI">
					<xsl:with-param name="num" select="$numIF"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Job -->

	<xsl:template name="outputFI">
		<xsl:param name="num"/>

		<xsl:call-template name="seq"/>
		<xsl:text>FI</xsl:text>
		<xsl:choose>
			<xsl:when test="$num=1">
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputFI">
					<xsl:with-param name="num" select="number($num)-1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="Expression">
		<xsl:param name="type"/>

		<xsl:variable name="ioName" select="normalize-space(substring-before(., '='))"/>
		<xsl:variable name="ioVal"  select="normalize-space(substring-after(., '='))"/>
		<xsl:variable name="ioNum"  select="../../../../Inputs/Input[@InputName=$ioName]/@InputNumber"/>
		<xsl:choose>
			<xsl:when test="$type='StartCondition'">
				<xsl:call-template name="singleExpr_Start">
					<xsl:with-param name="ioName" select="$ioName"/>
					<xsl:with-param name="ioVal" select="$ioVal"/>
					<xsl:with-param name="ioNum" select="$ioNum"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$type='WaitExpression'">
				<xsl:call-template name="singleExpr_Wait">
					<xsl:with-param name="ioName" select="$ioName"/>
					<xsl:with-param name="ioVal" select="$ioVal"/>
					<xsl:with-param name="ioNum" select="$ioNum"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- end of template Expression -->

	<xsl:template name="singleExpr_Start">
		<xsl:param name="ioName"/>
		<xsl:param name="ioVal"/>
		<xsl:param name="ioNum"/>

		<xsl:call-template name="seq"/>
		<xsl:text>IF 1 I</xsl:text>
		<xsl:value-of select="$ioNum"/>
		<xsl:text> N</xsl:text>

		<NumberIncrement:counternum counter="1"/>
		<xsl:variable name="tmpVar">
			<xsl:call-template name="getNumIncNext"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="contains($ioVal, '=')">
				<xsl:variable name="ioVal0" select="substring-before($ioVal, ' ')"/>
				<xsl:choose>
					<xsl:when test="$ioVal0='true'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>;</xsl:text>

				<xsl:variable name="subExpr0" select="normalize-space(substring-after($ioVal, ' '))"/>
				<xsl:variable name="subExpr" select="normalize-space(substring-after($subExpr0, ' '))"/>
				<xsl:variable name="ioName1" select="normalize-space(substring-before($subExpr, '='))"/>
				<xsl:variable name="ioVal1"  select="normalize-space(substring-after($subExpr, '='))"/>
				<xsl:variable name="ioNum1"  select="../../../../Inputs/Input[@InputName=$ioName1]/@InputNumber"/>
				<xsl:call-template name="singleExpr_Start">
					<xsl:with-param name="ioName" select="$ioName1"/>
					<xsl:with-param name="ioVal"  select="$ioVal1"/>
					<xsl:with-param name="ioNum"  select="$ioNum1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$ioVal='true'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template singleExpr_Start -->

	<xsl:template name="singleExpr_Wait">
		<xsl:param name="ioName"/>
		<xsl:param name="ioVal"/>
		<xsl:param name="ioNum"/>

		<xsl:value-of select="$cr"/>
		<xsl:text> 0 </xsl:text>

		<xsl:choose>
			<xsl:when test="contains($ioVal, '=')">
				<xsl:variable name="ioVal0" select="substring-before($ioVal, ' ')"/>
				<xsl:choose>
					<xsl:when test="$ioVal0='true'">N </xsl:when>
					<xsl:otherwise>F </xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="format-number($ioNum, $portNumPat)"/>
				<xsl:text> -1;</xsl:text>

				<xsl:variable name="subExpr0" select="normalize-space(substring-after($ioVal, ' '))"/>
				<xsl:variable name="subExpr" select="normalize-space(substring-after($subExpr0, ' '))"/>
				<xsl:variable name="ioName1" select="normalize-space(substring-before($subExpr, '='))"/>
				<xsl:variable name="ioVal1"  select="normalize-space(substring-after($subExpr, '='))"/>
				<xsl:variable name="ioNum1"  select="../../../../Inputs/Input[@InputName=$ioName1]/@InputNumber"/>
				<xsl:call-template name="singleExpr_Wait">
					<xsl:with-param name="ioName" select="$ioName1"/>
					<xsl:with-param name="ioVal"  select="$ioVal1"/>
					<xsl:with-param name="ioNum"  select="$ioNum1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$ioVal='true'">N </xsl:when>
					<xsl:otherwise>F </xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="format-number($ioNum, $portNumPat)"/>
				<xsl:text> -1;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
	<!-- end of template singleExpr_Wait -->

	<xsl:template match="SetOutput">
		<xsl:variable name="ioName" select="normalize-space(substring-before(., '='))"/>
		<xsl:variable name="ioVal"  select="normalize-space(substring-after(., '='))"/>
		<xsl:variable name="ioNum"  select="../../../Outputs/Output[@OutputName=$ioName]/@OutputNumber"/>
		<xsl:value-of select="$cr"/>
		<xsl:choose>
			<xsl:when test="$ioVal='true'">
				<xsl:text> 0 S </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> 0 R </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="format-number($ioNum, $portNumPat)"/>
		<xsl:text>;</xsl:text>
	</xsl:template>

	<xsl:template match="WaitExpression">
		<!-- check for OR operator -->
		<xsl:for-each select="Operator">
			<xsl:if test=".='or'">
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>Logic operator OR not supported.</xsl:text>
				<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
			</xsl:if>
		</xsl:for-each>

		<xsl:for-each select="Expression">
			<xsl:if test="contains(., ' or ')">
				<xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
				<xsl:text>Logic operator OR not supported.</xsl:text>
				<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
				<xsl:text>ERROR INFO END</xsl:text>
			</xsl:if>
		</xsl:for-each>

		<xsl:apply-templates select="Expression">
			<xsl:with-param name="type" select="'WaitExpression'"/>
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="CallTask">
		<xsl:call-template name="seq"/>
		<xsl:text>CL 4 000 P</xsl:text>
		<xsl:variable name="progNum">
			<xsl:call-template name="getProgNum">
				<xsl:with-param name="progName" select="."/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="format-number($progNum, $progNumPat)"/>
		<xsl:text>;</xsl:text>
	</xsl:template>
	<!-- end Program Logic section -->

	<xsl:template match="ActivityList">
		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>
		<!-- Total Sequence Number -->
		<xsl:for-each select="Activity">
			<xsl:choose>
				<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
					<xsl:variable name="tmpVar">
						<xsl:call-template name="getNumIncNext"/>
					</xsl:variable>
				</xsl:when>
				<xsl:when test="@ActivityType='DelayActivity'">
					<xsl:variable name="tmpVar">
						<xsl:call-template name="getNumIncNext"/>
					</xsl:variable>
				</xsl:when>
				<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
					<xsl:variable name="tmpVar">
						<xsl:call-template name="getNumIncNext"/>
					</xsl:variable>
				</xsl:when>
				<xsl:when test="@ActivityType='DNBSetSignalActivity'">
				</xsl:when>
				<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
		<!-- statment END; counts into sequence number, so call next() -->
		<xsl:variable name="seqNum">
			<xsl:call-template name="getNumIncNext"/>
		</xsl:variable>

		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE START P</xsl:text>
		<xsl:variable name="progNum">
			<xsl:call-template name="getProgNum">
				<xsl:with-param name="progName" select="@Task"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="format-number($progNum, $progNumPat)"/>
		<xsl:text>.ASC</xsl:text>

		<xsl:value-of select="$cr"/>
		<xsl:text> P</xsl:text>
		<xsl:value-of select="format-number($progNum, $progNumPat)"/>
		<xsl:text>.ASC </xsl:text>
		<xsl:value-of select="$seqNum"/>

		<!-- xsl:apply-templates select="/OLPData/Resource/ParameterList"/ -->
		<xsl:apply-templates select="AttributeList"/>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

		<NumberIncrement:counternum counter="0"/>
		<NumberIncrement:reset/>
		<xsl:apply-templates select="Activity"/>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>
	
		<xsl:call-template name="seq"/>
		<xsl:text>END;</xsl:text>

		<xsl:value-of select="$cr"/>
		<xsl:text>DATA FILE END</xsl:text>
	</xsl:template>
	<!-- end of template ActivityList -->

	<xsl:template match="ParameterList">
		<xsl:text> </xsl:text>
		<xsl:choose>
			<xsl:when test="$debug">00 00 00 00 00</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="dateTimeFormatter" select="java:java.text.SimpleDateFormat.new('yy MM dd HH mm')"/>
				<xsl:variable name="c" select="java:java.util.Calendar.getInstance()"/>
				<xsl:variable name="c" select="java:getTime($c)"/>
				<xsl:value-of select="java:format($dateTimeFormatter, $c)"/>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> </xsl:text>
		<xsl:variable name="protect" select="Parameter[ParameterName='Protect']/ParameterValue"/>
		<xsl:choose>
			<xsl:when test="$protect!=''">
				<xsl:value-of select="$protect"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>E0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text> </xsl:text>
		<xsl:variable name="version" select="Parameter[ParameterName='Version']/ParameterValue"/>
		<xsl:choose>
			<xsl:when test="$version!=''">
				<xsl:value-of select="$version"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>2</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text> </xsl:text>
		<xsl:variable name="systemID" select="Parameter[ParameterName='SystemID']/ParameterValue"/>
		<xsl:choose>
			<xsl:when test="$systemID!=''">
				<xsl:value-of select="$systemID"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:variable name="comment" select="Parameter[ParameterName='Comment']/ParameterValue"/>
		<xsl:choose>
			<xsl:when test="$comment!=''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$comment"/>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>;</xsl:text>

		<xsl:value-of select="$cr"/>
		<xsl:text> </xsl:text>
		<xsl:variable name="prd" select="Parameter[ParameterName='PRDLine']/ParameterValue"/>
		<xsl:choose>
			<xsl:when test="$prd!=''">
				<xsl:choose>
					<xsl:when test="starts-with($prd, 'PRD ')">
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>PRD </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$prd"/>
				<xsl:if test="substring($prd, string-length($prd))!=';'">
					<xsl:text>;</xsl:text>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>PRD 0 0 0 0 0;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:variable name="tact" select="Parameter[ParameterName='TACTLine']/ParameterValue"/>
		<xsl:choose>
			<xsl:when test="$tact!=''">
				<xsl:value-of select="$cr"/>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="starts-with($tact, 'TACT ')">
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>TACT </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$tact"/>
				<xsl:if test="substring($tact, string-length($tact))!=';'">
					<xsl:text>;</xsl:text>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="AttributeList">
		<xsl:text> </xsl:text>
		<xsl:choose>
			<xsl:when test="$debug">00 00 00 00 00</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="dateTimeFormatter" select="java:java.text.SimpleDateFormat.new('yy MM dd HH mm')"/>
				<xsl:variable name="c" select="java:java.util.Calendar.getInstance()"/>
				<xsl:variable name="c" select="java:getTime($c)"/>
				<xsl:value-of select="java:format($dateTimeFormatter, $c)"/>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text> </xsl:text>
		<xsl:variable name="protect" select="Attribute[AttributeName='Protect']/AttributeValue"/>
		<xsl:choose>
			<xsl:when test="$protect!=''">
				<xsl:value-of select="$protect"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>E0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text> </xsl:text>
		<xsl:variable name="version" select="Attribute[AttributeName='Version']/AttributeValue"/>
		<xsl:choose>
			<xsl:when test="$version!=''">
				<xsl:value-of select="$version"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>2</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text> </xsl:text>
		<xsl:variable name="systemID" select="Attribute[AttributeName='SystemID']/AttributeValue"/>
		<xsl:choose>
			<xsl:when test="$systemID!=''">
				<xsl:value-of select="$systemID"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:variable name="comment" select="Attribute[AttributeName='Comment']/AttributeValue"/>
		<xsl:choose>
			<xsl:when test="$comment!=''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$comment"/>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>;</xsl:text>

		<xsl:value-of select="$cr"/>
		<xsl:text> </xsl:text>
		<xsl:variable name="prd" select="Attribute[AttributeName='PRDLine']/AttributeValue"/>
		<xsl:choose>
			<xsl:when test="$prd!=''">
				<xsl:choose>
					<xsl:when test="starts-with($prd, 'PRD ')">
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>PRD </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$prd"/>
				<xsl:if test="substring($prd, string-length($prd))!=';'">
					<xsl:text>;</xsl:text>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>PRD 0 0 0 0 0;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:variable name="tact" select="Attribute[AttributeName='TACTLine']/AttributeValue"/>
		<xsl:choose>
			<xsl:when test="$tact!=''">
				<xsl:value-of select="$cr"/>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="starts-with($tact, 'TACT ')">
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>TACT </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$tact"/>
				<xsl:if test="substring($tact, string-length($tact))!=';'">
					<xsl:text>;</xsl:text>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="Attribute">
		<xsl:variable name="attrName" select = "AttributeName"/>
		<xsl:variable name="attrValue" select="AttributeValue"/>

		<xsl:value-of select="$cr"/>
		<xsl:choose>
			<xsl:when test="substring($attrName,1,7) = 'Comment'">
				<xsl:value-of select="$attrValue"/>
			</xsl:when>
			<xsl:when test="substring($attrName,1,6) = 'Header'">
				<xsl:text>P001.ASC</xsl:text>
				<xsl:value-of select="$attrValue"/>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>	
		</xsl:choose>
	</xsl:template>
	<!-- end of template Attribute -->

	<xsl:template match="Activity">

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Pre'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

		<xsl:choose>
			<xsl:when test="@ActivityType='DNBIgpCallRobotTask'">
				<xsl:call-template name="seq"/>
				<xsl:text>CL 4 000 P</xsl:text>
				<xsl:variable name="progNum">
					<xsl:call-template name="getProgNum">
						<xsl:with-param name="progName" select="CallName"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="format-number($progNum, $progNumPat)"/>
				<xsl:text>;</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DelayActivity'">
				<xsl:variable name="start" select="ActivityTime/StartTime"/>
				<xsl:variable name="end" select="ActivityTime/EndTime"/>
				<xsl:variable name="delay" select="format-number($end - $start, $delayTimePat)"/>

				<xsl:call-template name="seq"/>
				<xsl:text>T </xsl:text>
				<xsl:value-of select="$delay"/>
				<xsl:text>;</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBSetSignalActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:variable name="setSignalValue" select="SetIOAttributes/IOSetSignal/@SignalValue"/>
				<xsl:choose>
					<xsl:when test="$setSignalValue='On'">
						<xsl:text> 0 S </xsl:text>
					</xsl:when>
					<xsl:when test="$setSignalValue='Off'">
						<xsl:text> 0 R </xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:variable name="outputPortNumber" select="SetIOAttributes/IOSetSignal/@PortNumber"/>
				<xsl:value-of select="format-number($outputPortNumber, $portNumPat)"/>
				<xsl:text> 0.0;</xsl:text>
				<!-- ignore SignalDuration and Delay Time to 0
				<xsl:variable name="signalDuration" select="SetIOAttributes/IOSetSignal/@SignalDuration"/>
				<xsl:value-of select="format-number($signalDuration, $delayTimePat)"/>
				<xsl:text>;</xsl:text>
				-->
			</xsl:when>
			<xsl:when test="@ActivityType='DNBWaitSignalActivity'">
				<xsl:value-of select="$cr"/>
				<xsl:variable name="waitSignalValue" select="WaitIOAttributes/IOWaitSignal/@SignalValue"/>
				<xsl:choose>
					<xsl:when test="$waitSignalValue='On'">
						<xsl:text> 0 N </xsl:text>
					</xsl:when>
					<xsl:when test="$waitSignalValue='Off'">
						<xsl:text> 0 F </xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:variable name="inputPortNumber" select="WaitIOAttributes/IOWaitSignal/@PortNumber"/>
				<xsl:value-of select="format-number($inputPortNumber, $portNumPat)"/>
				<xsl:text> </xsl:text>
				<xsl:variable name="maxWaitTime" select="WaitIOAttributes/IOWaitSignal/@MaxWaitTime"/>
				<xsl:choose>
					<xsl:when test="$maxWaitTime='0'">
						<xsl:text>-1</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number($maxWaitTime, $delayTimePat)"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>;</xsl:text>
			</xsl:when>
			<xsl:when test="@ActivityType='DNBRobotMotionActivity'">
				<xsl:variable name="moType" select="MotionAttributes/MotionType"/>
				<xsl:variable name="motionProfile" select="MotionAttributes/MotionProfile"/>
				<xsl:variable name="accuracyProfile" select="MotionAttributes/AccuracyProfile"/>
				<xsl:variable name="speedValue" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Value"/>
				<xsl:variable name="speedUnits" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/MotionProfileList/MotionProfile[Name=$motionProfile]/Speed/@Units"/>
				<xsl:variable name="accuracyNode" select="/OLPData/Resource[GeneralInfo/ResourceName=$robotName]/Controller/AccuracyProfileList/AccuracyProfile[Name=$accuracyProfile]"/>
				<xsl:variable name="accuracyFlyBy" select="$accuracyNode/FlyByMode"/>
				<xsl:variable name="accuracyType" select="$accuracyNode/AccuracyType"/>
				<xsl:variable name="accuracyValue" select="$accuracyNode/AccuracyValue/@Value"/>
				<xsl:variable name="accuracyUnits" select="$accuracyNode/AccuracyValue/@Units"/>
				<xsl:variable name="speed">
					<xsl:call-template name="GetSpeed">
						<xsl:with-param name="moType" select="$moType"/>
						<xsl:with-param name="speedValue" select="$speedValue"/>
						<xsl:with-param name="speedUnits" select="$speedUnits"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="overlap">
					<xsl:call-template name="GetOverlap">
						<xsl:with-param name="accuracyFlyBy" select="$accuracyFlyBy"/>
						<xsl:with-param name="accuracyType" select="$accuracyType"/>
						<xsl:with-param name="accuracyValue" select="$accuracyValue"/>
						<xsl:with-param name="accuracyUnits" select="$accuracyUnits"/>
					</xsl:call-template>
				</xsl:variable>

				<xsl:call-template name="seq"/>
				<!-- H instruction -->
				<xsl:if test="$NumBaseAuxAxes + $NumPositionerAxes &gt; 0">
					<xsl:text>H </xsl:text>
					<xsl:choose>
						<xsl:when test="$moType='Joint'">
							<xsl:text>3</xsl:text>
						</xsl:when>
						<xsl:when test="$moType='Linear' or $moType='Circular' or $moType='CircularVia'">
							<xsl:text>1</xsl:text>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> 1 1 0 </xsl:text>
					<xsl:choose>
						<xsl:when test="$NumBaseAuxAxes">1 </xsl:when>
						<xsl:otherwise>0 </xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="$NumPositionerAxes">1;</xsl:when>
						<xsl:otherwise>0;</xsl:otherwise>
					</xsl:choose>

					<!-- new line & sequence number 0 for motion instructions -->
					<xsl:value-of select="$cr"/>
					<xsl:text> 0 </xsl:text>
				</xsl:if>

				<xsl:choose>
					<xsl:when test="$moType='Joint'">
						<xsl:text>P </xsl:text>
					</xsl:when>
					<xsl:when test="$moType='Linear'">
						<xsl:text>L</xsl:text>
						<xsl:if test="$speed &gt; 999">'</xsl:if>
						<xsl:text> </xsl:text>
					</xsl:when>
					<xsl:when test="$moType='CircularVia'">
						<xsl:text>C1</xsl:text>
						<xsl:if test="$speed &gt; 999">'</xsl:if>
						<xsl:text> </xsl:text>
					</xsl:when>
					<xsl:when test="$moType='Circular'">
						<xsl:text>C2</xsl:text>
						<xsl:if test="$speed &gt; 999">'</xsl:if>
						<xsl:text> </xsl:text>
					</xsl:when>
				</xsl:choose>
				<!-- Mechanism -->
				<xsl:text>1 </xsl:text>
				<xsl:value-of select="$speed"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$overlap"/>
				<!-- Joint Values -->
				<xsl:text> </xsl:text>
				<xsl:variable name="j1">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 1' or @DOFNumber='1']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="j2">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 2' or @DOFNumber='2']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="j3">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 3' or @DOFNumber='3']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="j4">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 4' or @DOFNumber='4']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="j5">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 5' or @DOFNumber='5']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="j6">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/JointTarget/Joint[@JointName='Joint 6' or @DOFNumber='6']/JointValue"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="format-number($j1 * $toRad, $jValPat)"/><xsl:text>  </xsl:text>
				<xsl:value-of select="format-number($j2 * $toRad, $jValPat)"/><xsl:text>  </xsl:text>
				<xsl:value-of select="format-number($j3 * $toRad, $jValPat)"/><xsl:text>  </xsl:text>
				<xsl:value-of select="format-number($j4 * $toRad, $jValPat)"/><xsl:text>  </xsl:text>
				<xsl:value-of select="format-number($j5 * $toRad, $jValPat)"/><xsl:text>  </xsl:text>
				<xsl:value-of select="format-number($j6 * $toRad, $jValPat)"/>
				<!-- Position -->
				<xsl:value-of select="$cr"/>
				<xsl:variable name="x0">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@X"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="y0">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@Y"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="z0">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Position/@Z"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="x" select="$x0 * $targetUnits"/>
				<xsl:variable name="y" select="$y0 * $targetUnits"/>
				<xsl:variable name="z" select="$z0 * $targetUnits"/>
				<xsl:variable name="w">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Orientation/@Yaw"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="p">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Orientation/@Pitch"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="r">
					<xsl:call-template name="Scientific">
						<xsl:with-param name="Num" select="Target/CartesianTarget/Orientation/@Roll"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="xyzwpr" select="concat($x, ',', $y, ',', $z, ',', $w, ',', $p, ',', $r)"/>
				<xsl:variable name="matxyzwpr" select="MatrixUtils:dgXyzyprToMatrix($xyzwpr)"/>
				<xsl:variable name="zyz" select="MatrixUtils:xformToZYZ()"/>
				<xsl:variable name="zyz2" select="substring-after($zyz, ',')"/>
				<xsl:variable name="a" select="substring-before($zyz, ',')"/>
				<xsl:variable name="b" select="substring-before($zyz2, ',')"/>
				<xsl:variable name="c" select="substring-after($zyz2, ',')"/>

				<xsl:text>     </xsl:text>
				<xsl:value-of select="format-number($x, $posPat)"/><xsl:text>   </xsl:text>
				<xsl:value-of select="format-number($y, $posPat)"/><xsl:text>   </xsl:text>
				<xsl:value-of select="format-number($z, $posPat)"/><xsl:text>   </xsl:text>
				<xsl:value-of select="format-number($a, $oriPat)"/><xsl:text>   </xsl:text>
				<xsl:value-of select="format-number($b, $oriPat)"/><xsl:text>   </xsl:text>
				<xsl:value-of select="format-number($c, $oriPat)"/>
				<!-- Coordinate System Type -->
				<xsl:choose>
					<xsl:when test="$downloadCoord = 'Part'"> 7;</xsl:when>
					<xsl:otherwise> 3;</xsl:otherwise>
				</xsl:choose>

				<!-- Rail(Slider) instruction -->
				<xsl:if test="$NumBaseAuxAxes &gt; 0">
					<xsl:value-of select="$cr"/>
					<xsl:text> 0 </xsl:text>
					<xsl:choose>
						<xsl:when test="$moType='Joint'">
							<xsl:text>PS 3 </xsl:text>
							<xsl:value-of select="$speed"/>
						</xsl:when>
						<xsl:when test="$moType='Linear' or $moType='Circular' or $moType='CircularVia'">
							<xsl:text>LS 3</xsl:text>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$overlap"/>
					<xsl:for-each select="Target/JointTarget/AuxJoint[@Type='RailTrackGantry']">
						<xsl:text> </xsl:text>
						<xsl:choose>
							<xsl:when test="@JointType='Translational' and @Units='m'">
								<xsl:variable name="auxj">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num" select="JointValue"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:value-of select="format-number($auxj * $targetUnits, $posPat)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
								<xsl:text>RailTrackGantry must be Translational.</xsl:text>
								<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO END</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<!-- filling with 0.0 -->
					<xsl:choose>
						<xsl:when test="$NumBaseAuxAxes = 1"> 0.0 0.0 2;</xsl:when>
						<xsl:when test="$NumBaseAuxAxes = 2"> 0.0 2;</xsl:when>
					</xsl:choose>
				</xsl:if>
				<!-- Positioner instruction -->
				<xsl:if test="$NumPositionerAxes &gt; 0">
					<xsl:value-of select="$cr"/>
					<xsl:text> 0 </xsl:text>
					<xsl:choose>
						<xsl:when test="$moType='Joint'">
							<xsl:text>PP 4 </xsl:text>
							<xsl:value-of select="$speed"/>
						</xsl:when>
						<xsl:when test="$moType='Linear' or $moType='Circular' or $moType='CircularVia'">
							<xsl:text>LS 4</xsl:text>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$overlap"/>
					<!-- IR 397266: 2-axis positioner -->
					<xsl:if test="$NumPositionerAxes = 2"> 0.0000</xsl:if>
					<xsl:for-each select="Target/JointTarget/AuxJoint[@Type='WorkpiecePositioner']">
						<xsl:text> </xsl:text>
						<xsl:choose>
							<xsl:when test="@JointType='Rotational' and @Units='deg'">
								<xsl:variable name="auxj">
									<xsl:call-template name="Scientific">
										<xsl:with-param name="Num" select="JointValue"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:value-of select="format-number($auxj * $toRad, $jValPat)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO START</xsl:text><xsl:value-of select="$cr"/>
								<xsl:text>WorkpiecePositioner must be Rotational.</xsl:text>
								<xsl:value-of select="$cr"/><xsl:value-of select="$cr"/>
								<xsl:text>ERROR INFO END</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<!-- filling with 0.0000 -->
					<xsl:if test="$NumPositionerAxes = 1"> 0.0000 0.0000</xsl:if>
					<!-- Coordinate System Type -->
					<xsl:text> 5;</xsl:text>
				</xsl:if>
			</xsl:when>
		</xsl:choose>

		<xsl:call-template name="processComments">
			<xsl:with-param name="prefix" select="'Post'"/>
			<xsl:with-param name="attributeListNodeSet" select="AttributeList"/>
		</xsl:call-template>

	</xsl:template>
	<!-- end of template Activity -->

	<xsl:template name="GetSpeed">
		<xsl:param name="moType" select="Joint"/>
		<xsl:param name="speedValue" select="100"/>
		<xsl:param name="speedUnits" select="'%'"/>
	
		<xsl:choose>
			<xsl:when test="$moType='Joint'">
				<xsl:choose>
					<xsl:when test="$speedUnits = '%'">
						<xsl:value-of select="format-number($speedValue, $jointSpdPat)"/>
					</xsl:when>
					<xsl:when test="$speedUnits = 'm/s'">
						<xsl:value-of select="format-number($speedValue div $maxSpeedValue * 100, $jointSpdPat)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>50</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$speedUnits = '%'">
						<xsl:value-of select="format-number($speedValue * $maxSpeedValue * 60, $linearSpdPat)"/>
					</xsl:when>
					<xsl:when test="$speedUnits = 'm/s'">
						<xsl:value-of select="format-number($speedValue * $toCMperMIN, $linearSpdPat)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="60"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- end of template GetSpeed -->
	<xsl:template name="GetOverlap">
		<xsl:param name="accuracyFlyBy" select="'On'"/>
		<xsl:param name="accuracyType" select="'Speed'"/>
		<xsl:param name="accuracyValue" select="0"/>
		<xsl:param name="accuracyUnits" select="'%'"/>

		<xsl:choose>
			<xsl:when test="$accuracyFlyBy='Off'">0</xsl:when>
			<xsl:when test="$accuracyType='Speed' and $accuracyUnits='%'">
				<xsl:choose>
					<xsl:when test="$accuracyValue &gt; 100">
						<xsl:text>100</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number($accuracyValue, $jointSpdPat)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$accuracyType='Distance' and $accuracyUnits='m'">
				<xsl:choose>
					<xsl:when test="$accuracyValue &gt; 1">
						<xsl:text>100</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number(($accuracyValue+0.005)*100, $jointSpdPat)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Wrong Accuracy Profile</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="processComments">
		<xsl:param name="prefix"/>
		<xsl:param name="attributeListNodeSet"/>
		<xsl:for-each select="$attributeListNodeSet/Attribute">
			<xsl:variable name="attrname" select="AttributeName"/>
			<xsl:variable name="attrvalue" select="AttributeValue"/>
			<xsl:variable name="precomchk" select="substring($attrname,1,10)"/>
			<xsl:variable name="postcomchk" select="substring($attrname,1,11)"/>
			<xsl:variable name="roblangchk" select="substring($attrvalue,1,15)"/>
			<xsl:variable name="striprobotlang" select="substring($attrvalue,16)"/>
			<xsl:if test="($precomchk = 'PreComment' and $prefix = 'Pre') or ($postcomchk = 'PostComment' and $prefix = 'Post')">
				<xsl:value-of select="$cr"/>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="$roblangchk = 'Robot Language:'">
						<xsl:value-of select="$striprobotlang"/>
						<xsl:if test="substring($striprobotlang, string-length($striprobotlang))!=';'">
							<xsl:text>;</xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0 CMT </xsl:text>
						<xsl:choose>
							<xsl:when test="string-length($attrvalue) &gt; 22">
								<xsl:value-of select="substring($attrvalue, 1, 22)"/>
								<xsl:text>;</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$attrvalue"/>
								<xsl:if test="substring($attrvalue, string-length($attrvalue))!=';'">
									<xsl:text>;</xsl:text>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="Scientific">
		<xsl:param name="Num"/>
		<xsl:choose>
			<xsl:when test="boolean(number(substring-after($Num,'e')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'e')"/>
					<xsl:with-param name="e" select="substring-after($Num,'e')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="boolean(number(substring-after($Num,'E')))">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="substring-before($Num,'E')"/>
					<xsl:with-param name="e" select="substring-after($Num,'E')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Num"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Scientific_Helper">
		<xsl:param name="m"/>
		<xsl:param name="e"/>
		<xsl:choose>
			<xsl:when test="$e = 0 or not(boolean($e))">
				<xsl:value-of select="$m"/>
			</xsl:when>
			<xsl:when test="$e &gt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m * 10"/>
					<xsl:with-param name="e" select="$e - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$e &lt; 0">
				<xsl:call-template name="Scientific_Helper">
					<xsl:with-param name="m" select="$m div 10"/>
					<xsl:with-param name="e" select="$e + 1"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
