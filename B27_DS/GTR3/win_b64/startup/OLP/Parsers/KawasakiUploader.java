/*
 * DNBIgpOlpKawasakiUploader.java
 *
 * Created on January 9, 2008, 11:59 AM
 */

/**
 *
 * @author  NIZ
 */
//DOM and Parser classes

import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;
import java.util.*;
import java.util.List.*;

//IO classes
import java.io.*;
import java.lang.reflect.Array;

import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Enumeration;
import java.util.Arrays;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;

// for kawasaki
import java.util.StringTokenizer;

import org.w3c.dom.*;
// import Kuka.util;

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class KawasakiUploader extends DNBIgpOlpUploadBaseClass
{
    
	// Member Variables.
	private static final String VERSION = "Delmia Corp. Kawasaki AS Uploader Version 5 Release 26 SP2.\n";
	private static final String COPYRIGHT = "Copyright Delmia Corp. 1986-2014, All Rights Reserved.\n";
    private static final String kDefault = "Kawasaki_Default";
    private static final String sFlyBy_OFF = "FlyBy OFF";

	private int m_moveCounter = 1;
	private int m_signalCounter = 1;
    private int m_objectFrameCounter = 1;
    private int m_toolPickCounter = 1;
    private int m_toolDropCounter = 1;
    private int m_spotPickCounter = 1;
    private int m_spotDropCounter = 1;
    private int m_spotWeldCounter = 1;
    private int m_gunMoveCounter = 1;
    private int m_callCounter = 1;
	
	private int programCallCount;
	
	private int m_PickCounter = 1;
	private int m_DropCounter = 1;
	
	private String ioName = "DO[";
	
	// Member Objects
	private Document m_xmlDoc;
	private String m_currentTagPoint;
	private int m_currentToolNumber;
	
	
	//private ArrayList mountedTools;
	private Pattern[] m_keywords;
	
	private String[] m_axisTypes;
	private String[] m_toolProfileNames;
    private String [] m_cfgmap = new String [9];
    
	private ArrayList m_listOfSpeedValues = new ArrayList();
	private ArrayList m_listOfCommentNames = new ArrayList();
	private ArrayList m_listOfCommentValues = new ArrayList();
	private ArrayList m_listOfParsedAndUnParsedStatements = new ArrayList();
	
	private ArrayList programCallNames;
	private ArrayList commentLines;
	private ArrayList inputNameNum, outputNameNum;
	
	       
	private String prgFileEncoding = "UTF-8";
	
	private Hashtable m_htTransVals = new Hashtable();
	private Hashtable m_htAuxVals = new Hashtable();
	private Hashtable m_htJointVals = new Hashtable();
	
	private Hashtable m_htMotionProfiles = new Hashtable();
	private Hashtable m_htSpotProfiles = new Hashtable();
	
	private static boolean toolFileExists = false;
    private static boolean m_bContinuousPath = true;
    
    private static String m_sCurrentProgramName         = "Default";
	private static String m_sCurrentMotionProfileName   = kDefault;
	private static String m_sCurrentToolProfileName	 = "Default";
	private static String m_sCurrentAccuracyProfileName = kDefault;
	private static String m_sCurrentObjectProfileName   = "Default";
	private static String m_sCurrentTimerName = "TIMER0";
	private static String m_sCurrentBlockClamp = "";
	private static String m_sCurrentBlockOX = "OX=";
	private static String m_sCurrentBlockWX= "WX=";
	private static String m_sCurrentBlockCL= "";
	private static String m_sPersistentMotionProfileName   = kDefault;
	private static String m_sPersistentAccuracyProfileName = kDefault;
	private static  DNBIgpOlpUploadEnumeratedTypes.MotionType m_eCurrentMotionType = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
    private String m_sHomeName1 = "Home_1";
    private String m_sHomeName2 = "Home_2";
	
	private static String m_sCurrentConfig = "Config_1";

	private String m_split_comma_or_space = "\\s+,\\s+|\\s+,|,\\s+|\\s+"; // split number string on a comma or space

	private static int _FileLength = 0;
    private int m_RobotTaskCounter = 1;
	private int m_RobotTaskCounter2 = 1;
	
	private int m_RobotTagGroupCounter = 1;
	private int m_RobotTagGroupCounter2 = 1;
	
	private ArrayList m_sProgramNames;
	private ArrayList m_sTagGroupNames;

	//-------------param based
	private ArrayList p_listOfRobotTargets;
	private ArrayList p_listOfAuxTargets;
	private ArrayList p_listOfExtTargets;
	private Hashtable p_targetTypes;
	private Hashtable p_targetTools;
	private Hashtable p_tagNames;
	//private String prgFileEncoding = "US-ASCII";
	private Hashtable m_targetTypes;
	private Hashtable m_targetTools;
	private Hashtable m_tagNames;
	private Hashtable m_toolNumberMapping;
	private ArrayList m_listOfRobotTargets;
	private ArrayList m_listOfAuxTargets;
	private ArrayList m_listOfExtTargets;
	//private ArrayList m_listOfSpeedValues;
	private ArrayList m_listOfAccuracyValues;
	private int[] m_bcAxisMapping;
	private int[] m_ecAxisMapping;
	private double[] m_resolution;
	private double[] m_zeroOffset;
	private ArrayList mountedTools;
	private String m_mountedTool;
    private String [] m_homeNames;
	private int m_targetType;
	private int m_numCStatements;
	private int m_numRobotAxes;
	private int m_numAuxAxes;
	private int m_numExtAxes;
	private int m_numWorkAxes;
	private int m_commentCounter = 1;
	private int m_rbtLangCounter = 1;
	private int m_delayCounter = 1;
    
	// block format change parameter globals
	private boolean m_downloadBlockFormat = false;
	private boolean m_speedAbsTime = false;
	private boolean m_OXWXNegative = false;
	private boolean m_OXPreOutput = true;
	private int m_numberOfClamp = 2;
	private String m_endOfArmToolType = "AIRGUN";
	private String m_controller = "E";
	private String m_auxdataSpeed = "10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0 100.0";
	private String m_auxdataAccu = "1.0 10.0 50.0 100.0 1.0";
	private String m_auxdataTimer = "0.0 .1 .2 .3 .4 .5 .6 .7 .8 .9";
	private String[] m_auxdataTool = {"0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0"};
	private String[] m_auxdataWork = {"0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0", "0.0 0.0 0.0 0.0 0.0 0.0"};
    private String m_clampInformation1 = "";
    private String m_clampInformation2 = "";
    private Hashtable m_OXValues = new Hashtable();
    private String m_currentAUXDATAFile = "";
    private boolean m_auxdataSpeedDone = false;
    private boolean m_auxdataAccuDone = false;
    private boolean m_auxdataTimerDone = false;
    private boolean[] m_auxdataToolDone = {false,false,false,false,false,false,false,false,false}; 
    private boolean[] m_auxdataWorkDone = {false,false,false,false,false,false,false,false,false}; 
    
	private int m_motionProfNum;
    private int m_accProfNum;
    private int m_toolProfNum;
	
    private Hashtable m_parameterData;
    private int m_worldCoords = 0;
    private Hashtable m_auxAxesTypes;
    private boolean m_commentWaitSignal = true;
    private String m_OLPStyle = "General";	
	
	private int jobCount, arcStartCount, arcEndCount;
	//private static boolean toolFileExists;
	private static boolean firstCircular;
	private boolean EndOfArmToolingFirst;
	//------------------------

	//private util kku;
	private static BufferedWriter bw;
    
    private static String [] m_multipleRobotPrograms;

	private static String[] parameters2 = {
		"D:\\00Defects\\Kawasaki R19 Regress False Error\\Program1.AS",
		"C:\\DOCUME~1\\niz\\LOCALS~1\\Temp\\simImport.xml",
		"SimProfileXMLPath	C:\\DOCUME~1\\niz\\LOCALS~1\\Temp\\SimProfiles.xml",
		"1	1	2",
        "Rotational	Rotational	Rotational	Rotational	Rotational	Rotational	",
        "6	0	0	0",
		"R2000iA-165F_20081208-424-64416.1",
		"Default	ROCKER1#1-16765-17132-268-2334-12156.1.1	",
        "C:\\DOCUME~1\\niz\\LOCALS~1\\Temp\\uploadLog.txt",
		"",
		"D:\\WRKSPS\\NIZ_R207_INT1\\intel_a\\resources\\xsd\\DELRobotics.xsd",
		"Default	",
        "D:\\00_Incident_Reports\\Kawasaki Uploader\\MultipleUpload2\\RobotTask2.as	D:\\00_Incident_Reports\\Kawasaki Uploader\\MultipleUpload2\\RobotTask3.as	D:\\00_Incident_Reports\\Kawasaki Uploader\\MultipleUpload2\\RobotTask4.as"
		         // param 13 is multiple program names
	};
    
	/** Creates a new instance of DNBIgpOlpKawasakiUploader */
	public KawasakiUploader(String[] parameters)  throws ParserConfigurationException
	{
		super(parameters);

		m_xmlDoc = super.getDOMDocument();
        m_parameterData =  super.getParameterData();
		
        m_auxAxesTypes = new Hashtable();
        int auxii;
        for (auxii=1;auxii<=m_numAuxAxes;auxii++)
        {
        	m_auxAxesTypes.put(String.valueOf(auxii), "RailTrackGantry");
        }
        for (auxii=auxii;auxii<=(m_numAuxAxes + m_numExtAxes);auxii++)
        {
        	m_auxAxesTypes.put(String.valueOf(auxii), "EndOfArmTooling");
        }
        for (auxii=auxii;auxii<=(m_numAuxAxes + m_numExtAxes + m_numWorkAxes);auxii++)
        {
        	m_auxAxesTypes.put(String.valueOf(auxii), "WorkPositioner");
        }
        
		processParameters(parameters);

	}


	//---------------------------------------------------------------------------
	// public static void main(String[] parameters)
	//
	//
	//---------------------------------------------------------------------------
	public static void main(String[] parameters) throws SAXException, ParserConfigurationException, TransformerConfigurationException, NumberFormatException, StringIndexOutOfBoundsException
	{
		// ?? Sleep thread to give time to attach debugger
        //try {
        //     Thread.currentThread().sleep(15000);
        //}
        // catch (InterruptedException e) {
        //     e.printStackTrace();
        //}
      
        
		KawasakiUploader uploader = new KawasakiUploader(parameters);

		uploader.createAccuracyProfile(kDefault, true, "1.0");
		uploader.createAccuracyProfile(sFlyBy_OFF, false, "0.0");
		
        DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
        moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
        double spd = 10.0, defaultVal = 100.0;
        
        Element motionProfileList = uploader.getMotionProfileListElement();
		Element defMotionProfile = uploader.createMotionProfile(motionProfileList, 
                                        kDefault, 
                                        moBasis,
                                        spd,            // speed
                                        defaultVal,     // dAcceleration,
                                        defaultVal,     // dAngularSpeed, 
                                        defaultVal );   // dAngularAcceleration)
        
        // After motion profile Kawasaki_Default is created, this does not get executed.
		if (motionProfileList.hasChildNodes() == false)
			uploader.createSpeedProfile("%50", "Default");
				
		//Get the robot program and parse it line by line

        int nmultipleprogramsize = 0;
        if (m_multipleRobotPrograms != null)
        {
            nmultipleprogramsize = m_multipleRobotPrograms.length;
        }
        String[] allRobotPrograms =  new String[nmultipleprogramsize + 1];
        
        String sRobotProgramPath = uploader.getPathToRobotProgramFile();
        allRobotPrograms[0] = sRobotProgramPath;
 
       if (m_multipleRobotPrograms != null)
       {
        	// allRobotPrograms now contains complete list of programs to translate.
        	System.arraycopy(m_multipleRobotPrograms, 0,  allRobotPrograms, 1, nmultipleprogramsize);
        }
       
       File currFile = new File(sRobotProgramPath);

       // first read the current program to find AUXDATA block speed, accur, etc....
       uploader.compileAUXDATAKeywords();
       if(currFile.exists())
       {
    		    BufferedReader rbt;
    		    int errCode = 0;
     		try
     		{
     			FileInputStream FIStream = new FileInputStream(sRobotProgramPath);
     			rbt = new BufferedReader(new InputStreamReader(FIStream, uploader.prgFileEncoding));

     			uploader.m_currentAUXDATAFile = currFile.getName();
     			errCode = uploader.processAUXDATAlines(rbt);
     			FIStream.close();
    		    }//end try

     		catch (IOException e)
     		{
     			try
     			{
     				bw.write("ERROR: " + e.getMessage() + "\n");
     			}
     			catch (IOException bwe)
     			{
     			}
     		}                 
       }   		   
       
       // now look in current directory for all "*.AS" files and scan for .AUXDATA speed, tool, work, timer, etc....
       String currPath = currFile.getParent() + currFile.separator;
       
       File currDir = new File(currPath);
       
       String[] currFiles = currDir.list();
              
       for( int jj = 0; jj < currFiles.length; jj++) 
       {
    	   if (currFiles[jj].toUpperCase().indexOf(".AS") > 0 && uploader.checkAUXDATADone() == false)
    	   {
              currFile  = new File(currPath + currFiles[jj]);
               
               // Read in the robot program.
              int errCode = 0;
                   
              if(currFile.exists())
              {
           		    BufferedReader rbt;

            		try
            		{
            			FileInputStream FIStream = new FileInputStream(currPath + currFiles[jj]);
            			rbt = new BufferedReader(new InputStreamReader(FIStream, uploader.prgFileEncoding));
            			
            			uploader.m_currentAUXDATAFile = currFile.getName();
            			errCode = uploader.processAUXDATAlines(rbt);
            			FIStream.close();
           		    }//end try

            		catch (IOException e)
            		{
            			try
            			{
            				bw.write("ERROR: " + e.getMessage() + "\n");
            			}
            			catch (IOException bwe)
            			{
            			}
            		}                 
              }   		   
    	   }
       }
       
        for( int i = 0; i < allRobotPrograms.length; i++) 
        {
            //uploader.setPathToRobotProgramFile(allRobotPrograms[i]);
        
            String robotProgramName = "";
			
			robotProgramName = allRobotPrograms[i];
				
            File f = new File(robotProgramName);
            _FileLength = (int)f.length();

            String path = f.getParent() + f.separator;

            File toolCND = new File(path + "TOOL.CND");
            if (toolCND.exists())
            {
                uploader.processToolFile(toolCND);
                toolFileExists = true;
            }

            String uploadInfoFileName = uploader.getPathToErrorLogFile();
		
            Matcher match;

            try
            {
				if( i == 0)
				{ // Support multiple file upload. Write header only for first uploaded
				  // program.
					bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));
					bw.write(VERSION);
					bw.write(COPYRIGHT);
					bw.write("\nStart of java parsing.\n\n");
				}

                // Read in the robot program.
                int errCode = 0;
                
                if(f.exists())
                {
                	errCode = uploader.processRobotProgram(robotProgramName);
                }

                if (errCode == 0)
                {
                    String calledProgName;
                    for (int ii = 0; ii < uploader.programCallCount; ii++)
                    {
                        calledProgName = (String)uploader.programCallNames.get(ii);
                        robotProgramName = path + calledProgName;
                        errCode = uploader.processRobotProgram(robotProgramName);
                    }
                }

				if(i == allRobotPrograms.length - 1)
				{ // Support Multiple Program Upload. Write trailer after last program is processed.
					if (errCode == 0 )
					{ 
						bw.write("All the program statements have been parsed.\n");
						bw.write("\nJava parsing completed successfully.\n");
					}
					else if (errCode == 2)
					{
                    bw.write("\nERROR-Robot program upload failed.\n");
					}

					bw.write("\nEnd of java parsing.\n");
					bw.flush();
					bw.close();
				}

            } // try
            catch (IOException e)
            {
                e.getMessage();
            }

            // BLOCK controller SPEED, TOOL, WORK, ACCU
    		Element resourceElem = uploader.getResourceElement();
    		uploader.addParameter(resourceElem, "AUXDATA_ACCU", uploader.m_auxdataAccu);
            String [] sAccuVals = uploader.m_auxdataAccu.split("[,\\s{}]+");
            for (int ii = 0;ii<sAccuVals.length;ii++)
            {
            	try
            	{
            		if (ii < 4)
            		{
            			uploader.createAccuracyProfile("ACCU" + String.valueOf(ii+1), true, sAccuVals[ii].trim());	
            		}
            		else
            		{
            			uploader.createAccuracyProfile("ACCU0", false, sAccuVals[ii].trim());	       		
            		}
            	}
            	catch (NumberFormatException e)
        		{
        		}
            }

    		uploader.addParameter(resourceElem, "AUXDATA_SPEED", uploader.m_auxdataSpeed);            
            String [] sSpeedVals = uploader.m_auxdataSpeed.split("[,\\s{}]+");
            for (int ii = 0;ii<sSpeedVals.length;ii++)
            {
            	String speedStr = "%" + sSpeedVals[ii].trim();
                if (uploader.m_speedAbsTime == true)
                {
                    switch (ii)
                    {
                    	case 0:
                    	case 1:
                    	case 2:
                    	case 3:
                    	case 4:
                    	case 5:
                            spd = Double.parseDouble(sSpeedVals[ii].trim())/1000.0;
                            speedStr = Double.toString(spd);   		
                    	break;
                    	case 6:
                    	case 7:
                    		speedStr = "T" + sSpeedVals[ii].trim();
                    	break;
                    }
                }
                uploader.createSpeedProfile(speedStr, "SPEED" + String.valueOf(ii));
            }        

    		uploader.addParameter(resourceElem, "AUXDATA_TIMER", uploader.m_auxdataTimer);            
            
            for (int ii = 0;ii<uploader.m_auxdataTool.length;ii++)
            {
            	String toolLine = "TOOL" + String.valueOf(ii+1) + " " + uploader.m_auxdataTool[ii];
            	uploader.processTOOLStatement(toolLine);
            }
            
            String workLine = "WORK0 " + "0.0 0.0 0.0 0.0 0.0 0.0";
            uploader.processUFRAMEStatement(workLine);
            
            for (int ii = 0;ii<uploader.m_auxdataWork.length;ii++)
            {
            	workLine = "WORK" + String.valueOf(ii+1) + " " + uploader.m_auxdataWork[ii];
            	uploader.processUFRAMEStatement(workLine);
            }        
            
			if( i == allRobotPrograms.length - 1)
			{ // Support multiple file upload. Write xml only after last uploaded
				  // program.
				try
				{
					uploader.writeXMLStreamToFile();
				}

				catch (IOException e)
				{
					e.getMessage();
				}
				catch (TransformerException e)
				{
					e.getMessage();
				}
			}
        }       
	} // public static void main

	private boolean checkAUXDATADone()
	{
	   boolean isDone = true;
	   if (m_auxdataSpeedDone == false || m_auxdataAccuDone == false || m_auxdataTimerDone == false) 
	   {
		   isDone = false;
	   }
	   for (int ii=0;ii<9;ii++)
	   {
	       if (m_auxdataToolDone[ii] == false || m_auxdataWorkDone[ii] == false)
	       {
	    	   isDone = false;
	       }
	   }
	   return(isDone);
	}
	
	//---------------------------------------------------------------------------
	// private int processRobotProgram(String robotProgramName)
	//
	//
	//---------------------------------------------------------------------------

	private int processRobotProgram(String robotProgramName)
	{
		compileKeywords();

		BufferedReader rbt;
		int errCode;

		try
		{
			FileInputStream FIStream = new FileInputStream(robotProgramName);
			rbt = new BufferedReader(new InputStreamReader(FIStream, prgFileEncoding));

			bw.write("Parsing entities from the file \"");
			bw.write(robotProgramName);
			bw.write("\":\n\n");

			errCode = processRobotProgramLines(rbt);
			for (int ii=0;ii<m_listOfParsedAndUnParsedStatements.size();ii++)
			{
		        bw.write((String)m_listOfParsedAndUnParsedStatements.get(ii) + "\n");
			}
			m_listOfParsedAndUnParsedStatements.clear();  
			if (errCode != 0)
				return errCode;
		}//end try

		catch (IOException e)
		{
			try
			{
				bw.write("ERROR: " + e.getMessage() + "\n");
			}
			catch (IOException bwe)
			{
			}
			return 2;
		}
		m_OXValues.clear();
		return 0;
	}//end processRobotProgram

	
	private int processAUXDATAlines(BufferedReader rbt)
	{
		String line = "NULL", section = "NONE";
		Matcher match;
		
		try
		{
			line = "";

			while (line != null)
			{
                line = line.trim();
				if (line.equals(""))
				{
					line = rbt.readLine();
					// java.lang.System.out.println("reading line " + line);
					continue;
				}

				for (int ii = 0; ii < m_keywords.length; ii++)
				{
					match = m_keywords[ii].matcher(line);

					if (match.find() == true)
					{
						//Call appropriate methods to handle the input
						switch (ii)
						{
							case 0: // .AUXDATA
								section = "AUXDATA";
								break;

							case 1: // .END
                                section = "";
								break;

							case 2: // SPEED
								if (section.equals("AUXDATA"))
								{
									if (match.group(1) != null)
									{
										if (m_auxdataSpeedDone == false)
										{
											m_listOfParsedAndUnParsedStatements.add("AUXDATA SPEED <" + m_currentAUXDATAFile + "> : " + line);
											m_auxdataSpeed = match.group(1);
											m_auxdataSpeedDone = true;
										}
									}
								}
								break;
								
							case 3: // ACCUR
								if (section.equals("AUXDATA"))
								{
									if (match.group(1) != null)
									{
										if (m_auxdataAccuDone == false)
										{
											m_listOfParsedAndUnParsedStatements.add("AUXDATA ACCUR <" + m_currentAUXDATAFile + "> : " + line);											
											m_auxdataAccu = match.group(1);
											m_auxdataAccuDone = true;
										}
									}
								}
								break;
								
                            case 4: // TIMER
								if (section.equals("AUXDATA"))
								{
									if (match.group(1) != null)
									{
										if (m_auxdataTimerDone == false)
										{
											m_listOfParsedAndUnParsedStatements.add("AUXDATA TIMER <" + m_currentAUXDATAFile + "> : " + line);											
											m_auxdataTimer = match.group(1);
										    m_auxdataTimerDone = true;
										}
									}
								}
								break;
                            	
                            case 5: // TOOL 1-9
								if (section.equals("AUXDATA"))
								{
									if (match.group(1) != null)
									{
										int toolNum = Integer.parseInt(match.group(1));
										if (toolNum > 0 && toolNum < 10)
										{
											if (match.group(2) != null)
											{	
												if (m_auxdataToolDone[toolNum-1] == false)
												{
													m_listOfParsedAndUnParsedStatements.add("AUXDATA TOOL" + String.valueOf(toolNum) + " <" + m_currentAUXDATAFile + "> : " + line);
													m_auxdataTool[toolNum-1] = match.group(2);
													m_auxdataToolDone[toolNum-1] = true;
												}
											}
										}
									}
								}
                            	break;
                            case 6: // WORK 1-9
								if (section.equals("AUXDATA"))
								{
									if (match.group(1) != null)
									{
										int workNum = Integer.parseInt(match.group(1));
										if (workNum > 0 && workNum < 10)
										{
											if (match.group(2) != null)
											{	
												if (m_auxdataWorkDone[workNum-1] == false)
												{
													m_listOfParsedAndUnParsedStatements.add("AUXDATA WORK" + String.valueOf(workNum) + " <" + m_currentAUXDATAFile + "> : " + line);													
													m_auxdataWork[workNum-1] = match.group(2);
													m_auxdataWorkDone[workNum-1] = true;
												}
											}
										}
									}
								}                            	
								break;
						}
					}//end if
					else if(ii == m_keywords.length - 1)
					{
					}
				}//end for

				line = rbt.readLine();
				// java.lang.System.out.println("reading line " + line);

			}//end while
			rbt.close();
		}//end try

		catch (IOException e)
		{
			e.getMessage();
			// System.out.println(e.toString());
			return 2;
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			try
			{
				bw.write("ERROR- Array Index out of bounds during translation.\n");
			}
			catch (IOException bwe)
			{
			}
			return 2;
		}
		return 0;
	} // end of processAUXDATAprogram
	
	//---------------------------------------------------------------------------
	// int processRobotProgramLines(BufferedReader rbt)
	//
	//
	//---------------------------------------------------------------------------

	private int processRobotProgramLines(BufferedReader rbt)
	{
		String line = "NULL", gunJointType, section = "NONE";
		Matcher match;
		Element activityListElem = null, attributeListElem;
		int errCode, offset;

		Element resourceElem = super.getResourceElement();

		errCode = 0;
		
		try
		{
			///////////////////////////////////////////////////////////////////
			// We need to parse the .TRANS, .AUXDATA and .JOINTS sections first
			// We will have to make a pass through the entire file
			// java.lang.System.out.println("--Read Trans and AUXDATA---");
			boolean bmarkok = rbt.markSupported() ;
			
			rbt.mark(_FileLength + 1); // mark the beginning of the file
            
			line = "";

			while (line != null)	// read the file until .TRANS, .AUXDATA or .JOINTS
			{						// are found
				line = rbt.readLine();
			//	java.lang.System.out.println("reading " + line);
				
				if(line == null)
					break;
                else
                    line = line.trim();
				
				if (line.equals(""))
				{
					//java.lang.System.out.println(line + " Blank Line"); // debug
					
					//line = rbt.readLine();
					//java.lang.System.out.println("reading " + line);
					continue;
				}

				//////////////////////////
				// READ the TRANS Section
				if( m_keywords[15].matcher(line).find() == true) // .TRANS
				{
				    //java.lang.System.out.println(line + " Matched .TRANS"); //debug
				    while (line != null) // Shouldn't be NULL initially... 
				    {
						line = rbt.readLine(); // read the first line in .TRANS section
						//java.lang.System.out.println("reading " + line);
						
						if(line == null)
							break;
                        else
                            line = line.trim();
						
						if (line.equals(""))
						{
							//java.lang.System.out.println("Blank Line"); // debug
							//line = rbt.readLine();
							//java.lang.System.out.println("reading " + line);
							
							continue;
						}
                           
                        match = m_keywords[17].matcher(line); // .END of .TRANS
						if (match.find() == true)
						{
                           //java.lang.System.out.println(line + " Matched .END of .TRANS"); //debug
							break; // break out of the .TRANS while loop
						}
                        
						// The trans section tends to be one long line. Break this
						// single line into an array of matchable statements.
						String[] sTransStatements = processTRANSStatement(line, activityListElem);
                                                
                        // Process parsed line for anything besides blank and end.
						for (int i = 0; i < sTransStatements.length; i++)
						{
							match = m_keywords[23].matcher(sTransStatements[i]); // a uframe + 6tuple
							if (match.find() == true)
							{
								//java.lang.System.out.println(sTransStatements[i] + " Matched uframe + 6tuple."); // debug
								processUFRAMEStatement(sTransStatements[i]);
								continue;
							}
							match = m_keywords[38].matcher(sTransStatements[i]); // a tool + 6tuple
							if (match.find() == true)
							{
								//java.lang.System.out.println(sTransStatements[i] + " Matched tool + 6tuple."); // debug
								processTOOLStatement(sTransStatements[i]);
								continue;
							}

							match = m_keywords[356].matcher(sTransStatements[i]); // SPEED
							if (match.find() == true)
							{
								continue;
							}
							
							match = m_keywords[357].matcher(sTransStatements[i]); // ACCUR
							if (match.find() == true)
							{
								continue;
							}

							match = m_keywords[358].matcher(sTransStatements[i]); // TIMER
							if (match.find() == true)
							{
								continue;
							}
							match = m_keywords[359].matcher(sTransStatements[i]); // TOOL
							if (match.find() == true)
							{
								continue;
							}
							match = m_keywords[360].matcher(sTransStatements[i]); // WORK
							if (match.find() == true)
							{
								continue;
							}
														
							match = m_keywords[2000].matcher(sTransStatements[i]); // a general id + 6tuple
							if (match.find() == true)
							{
								String shashKey = getHashFromIdPlusSixtuple(sTransStatements[i]);
								String shashValue = sTransStatements[i];
								
								m_htTransVals.put(shashKey, shashValue);
								//java.lang.System.out.println(sTransStatements[i] + " Matched a general id + 6tuple");
								continue;
							}
                            
                            //java.lang.System.out.println(sTransStatements[i] + " ****NO MATCH in sTransStatements*****");
                            
						} // for sTransStatements
           
					} // while .TRANS readline.
				} // match .TRANS keyword.
				
				//////////////////////////
				// READ the .AUXDATA Section
                else if( m_keywords[19].matcher(line).find() == true) // .AUXDATA
				{
                    //java.lang.System.out.println(line + " Matched .AUXDATA");
					while (line != null) // Shouldn't be NULL initially... read the .AUXDATA section
					{
						line = rbt.readLine(); // read the first line in .AUXDATA section
						//java.lang.System.out.println("reading " + line);
						
						if(line == null)
						break;
                        else
                            line = line.trim();
						
						if (line.equals(""))
						{
							//line = rbt.readLine();
							continue;
						}

						match = m_keywords[17].matcher(line); // .END of .AUXDATA
						if (match.find() == true)
						{
                            //java.lang.System.out.println(line + " Matched .END of .AUXDATA"); // debug
							break; // break out of the .AUXDATA while loop
						}
						match = m_keywords[23].matcher(line); // a uframe + 6tuple
						if (match.find() == true)
						{
							//java.lang.System.out.println(line + " Matched uframe + 6tuple."); // debug
							processUFRAMEStatement(line);
							continue;
						}
						match = m_keywords[38].matcher(line); // a tool + 6tuple
						if (match.find() == true)
						{
							//java.lang.System.out.println(line + " Matched tool + 6tuple."); // debug
							processTOOLStatement(line);
							continue;
						}
						match = m_keywords[2000].matcher(line); // a general id + 6tuple
						if (match.find() == true)
						{
							String shashKey = getHashFromIdPlusSixtuple(line);
							String shashValue = line.trim();
								
							m_htAuxVals.put(shashKey, line);
                            //java.lang.System.out.println(line + " Matched general id + 6tuple."); // debug
							continue;
						}
						
					} // while .AUXDATA readline

				} // if match .AUXDATA keyword

                //////////////////////////
				// READ the .JOINTS Section
                else if( m_keywords[18].matcher(line).find() == true) // .JOINTS
				{
                    //java.lang.System.out.println(line + " Matched .JOINTS");
					while (line != null) // Shouldn't be NULL initially... read the .JOINTS section
					{
						line = rbt.readLine(); // read the first line in .JOINTS section
						//java.lang.System.out.println("reading " + line);
						
						if(line == null)
						break;
                        else
                            line = line.trim();
						
						if (line.equals(""))
						{
							continue;
						}

						match = m_keywords[17].matcher(line); // .END of .JOINTS
						if (match.find() == true)
						{
                            //java.lang.System.out.println(line + " Matched .END of .JOINTS"); // debug
							break; // break out of the .AUXDATA while loop
						}
						
						match = m_keywords[2000].matcher(line);  // general id + 6tuple
						if (match.find() == true)
						{
							String shashKey = getHashFromIdPlusSixtuple(line);
							String shashValue = line;
								
							m_htJointVals.put(shashKey, line);
                            //java.lang.System.out.println(line + " Matched general id + 6tuple.");
							continue;
						}
					} // while
				} // else if keyword = .JOINTS
                else if (line.toUpperCase().startsWith(".PROGRAM WELD"))
                {
                    String stitle = ".PROGRAM WELD";
                    String [] spotComponents = new String [4];
                    String spotKey;
                    int idx1, idx2;
                    String [] kwSpotParameters = {"1","1","1","1","1"};
                    idx1 = stitle.length();
                    idx2 = line.indexOf('(');
                    spotKey = line.substring(idx1, idx2);
                    kwSpotParameters[0] = spotKey;
                    
                    while (line != null)
                    {
                        line = rbt.readLine();
                        if (line == null)
                            break;
                        else
                            line = line.trim();
                        if (line.equals(""))
                            continue;
                        if (line.equalsIgnoreCase(".END"))
                            break;
                        
                        if (line.toUpperCase().startsWith("BITS "))
                        {
                            spotComponents = line.split("[ ,]+", 4);
                            kwSpotParameters[1] = spotComponents[1];
                            kwSpotParameters[2] = spotComponents[2];
                        }
                        if (line.toUpperCase().startsWith("PULSE "))
                        {
                            spotComponents = line.split("[ ,]+", 4);
                            kwSpotParameters[3] = spotComponents[1];
                        }
                        if (line.toUpperCase().startsWith("SWAIT "))
                        {
                            spotComponents = line.split("[ ,]+", 4);
                            kwSpotParameters[4] = spotComponents[1];
                        }
                    }
                    kwSpotWeldObj kwSpot = new kwSpotWeldObj(kwSpotParameters);
                    m_htSpotProfiles.put(spotKey, kwSpot);
                }
                 
			} //While readline .TRANS, .AUXDATA, .JOINTS
			///////////////////////////////////////////////////////

			// reset line parser to beginning...
			rbt.reset();
			line = rbt.readLine();
			// java.lang.System.out.println("reading line " + line);
			while (line != null)
			{
                line = line.trim();
				if (line.equals(""))
				{
					line = rbt.readLine();
					// java.lang.System.out.println("reading line " + line);
					continue;
				}

				for (int ii = 0; ii < m_keywords.length; ii++)
				{
					match = m_keywords[ii].matcher(line);

					if (match.find() == true)
					{
						//Call appropriate methods to handle the input
						switch (ii)
						{
							case 0: // comment
								processCOMMENTStatement(line, activityListElem);
								break;

							case 1: // .PROGRAM
                                section = "PROGRAM";
								activityListElem = processPROGRAMStatement(line, rbt);
								break;

							case 2: // SPEED <number> MM/(S|MIN)    (absolute)
							case 3: // SPEED <number> S (time based)
                            case 4: // SPEED <number>   (percentage)
                            	if (line.split(" ").length < 8)
                            	{
                            		processSPEEDStatement(line, match, activityListElem);
                            	}
								break;
                            /*
							case 4: // ACCEL<number>ALWAYS
								processACCELStatement(line, activityListElem);
								break;

							case 5: // DECEL<number>ALWAYS
								processDECELStatement(line, activityListElem);
								break;
                             */
							case 6: // ACCURACY<number>
                            	if (line.split(" ").length < 4)
                            	{
    								processACCURACYStatement(line, activityListElem);                            		
                            	}								
								break;

							case 7: // UWRIST BELOW RIGHTY
								processUWRISTBELOWRIGHTYStatement(line, activityListElem);
								break;

							case 8: // UWRIST ABOVE RIGHTY
								processUWRISTABOVERIGHTYStatement(line, activityListElem);
								break;

							case 9: // UWRIST BELOW LEFTY
								processUWRISTBELOWLEFTYStatement(line, activityListElem);
								break;

							case 10: // UWRIST ABOVE LEFTY
								processUWRISTABOVELEFTYStatement(line, activityListElem);
								break;
							
							case 11: // DWRIST BELOW RIGHTY
								processDWRISTBELOWRIGHTYStatement(line, activityListElem);
								break;

							case 12: // DWRIST ABOVE RIGHTY
								processDWRISTABOVERIGHTYStatement(line, activityListElem);
								break;

							case 13: // DWRIST BELOW LEFTY
								processDWRISTBELOWLEFTYStatement(line, activityListElem);
								break;

							case 14: // DWRIST ABOVE LEFTY
								processDWRISTABOVELEFTYStatement(line, activityListElem);
								break;

							case 15: // .TRANS
                                section = "TRANS";
								break;

							case 17: // .END not used We should hit .JOINTS or .TRANS first
                                if (true == section.equals("PROGRAM"))
                                    setCommentStatements(activityListElem, "Post");
                                section = "NONE";
                                m_OXValues.clear();
								break;

							case 18: // .JOINT
                                section = "JOINT";
									break;

							case 19: // .AUXDATA
                                section = "AUXDATA";
								break;

							case 20: // .PROGRAM
								// TOOL <tool_name>
								// .END
								processTOOLNAMEStatement(line, activityListElem);
								break;

							case 21:
								// BASE uframe_<number>
								processBASEStatement(line, activityListElem);
								break;

							case 22:
								// BASE uframe_<number>
								processBASEStatement2(line, activityListElem);
								break;

							case 23:
								// uframe_<number> <xx> <yy> <zz> <oo> <aa> <tt>
								// This line is processed in .TRANS section
                                // in .PROGRAM section, deal with it as Robot Language
                                if (true == section.equals("PROGRAM"))
            						errCode = processUNPARSEDStatement(line, activityListElem, section);
								break;

							case 24:
								// SIGNAL <number>[, <number>]
								processSIGNALStatement(line, activityListElem);
								break;

							case 26:
								// PULSE <number> <time>
								processPULSEStatement(line, activityListElem);
								break;

							case 27:
								// TIMER <timer_number> = <initial_time>
								processTIMERStatement(line, activityListElem);
								break;

							case 28:
								// WAIT SIG( <number> or <-number> OR TIMER(<timer_number>) <time>
								processWAITStatement(line, activityListElem);
								break;

							case 29:
								// WAIT SIG( <number> or <-number> OR TIMER(<timer_number>) <time>
								processWAITStatement2(line, activityListElem);
								break;

							case 30:
								// TWAIT <number> (number is in seconds) 
								processTWAITStatement(line, activityListElem);
								break;

							case 31:
								// CALL WELDnub
								processSpotWeldStatement(line, activityListElem);
								break;

							case 32:
								// JAPPRO <tag_name> <distance_along_z_axis>
								processJAPPROStatement(line, activityListElem);
								break;

							case 33:
								// LAPPRO <tag_name> <distance_along_z_axis>
								processLAPPROStatement(line, activityListElem);
								break;

							case 34:
								// CMOVE <tag_name>
								String moveName = match.group(1);
								if (moveName != null && moveName.equals("C2MOVE"))
								{
									processC2MoveStatement(line, activityListElem, false);
								}
								else
								{
									processC1MoveStatement(line, activityListElem, false);
								}
								break;

							case 35:
								// CMOVE #P1
								moveName = match.group(1);
								if (moveName != null && moveName.equals("C2MOVE"))
								{
									processC2MoveStatement(line, activityListElem, true);
								}
								else
								{
									processC1MoveStatement(line, activityListElem, true);
								}
								break;

							case 36:
								// JMOVE <tag_name>
								processJMOVEStatement(line, activityListElem);
								break;

							case 37:
								// LMOVE <tag_name>
								processLMOVEStatement(line, activityListElem);
								break;

							case 38:
								// Tool profile
								// This line is processed in .TRANS and .AUXDATA sections
                                // in .PROGRAM section, deal with it as Robot Language
                                if (true == section.equals("PROGRAM"))
            						errCode = processUNPARSEDStatement(line, activityListElem, section);
								break;
								
							case 39:
								// JMOVE Trans(tx, ty, ...)
								processJMOVETRANSTStatement(line, activityListElem);
								break;
								
							case 40:
							// JMOVE #P1
								processJMOVEJOINTStatement(line, activityListElem);
								break;
								
							case 41:
							// JMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + TRANS(tX, tY, tZ, tYaw, tPitch, tRoll)
								processJMOVETRANSPLUSTRANSStatement(line, activityListElem);
								break;
								
							case 42:
							// JMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + Tag1 
								processJMOVETRANSPLUSTAGStatement(line, activityListElem);
								break;

							case 43:
							// LMOVE TRANS(X, Y, Z, Yaw, Pitch, Roll)
								processLMOVETRANSTStatement(line, activityListElem);
								break;

							case 44:
							// LMOVE #P1
								processLMOVEJOINTStatement(line, activityListElem);
								break;

							case 45:
							// LMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + TRANS(tX, tY, tZ, tYaw, tPitch, tRoll)
								processLMOVETRANSPLUSTRANSStatement(line, activityListElem);
								break;

							case 46:
							// LMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + Tag1 
								processLMOVETRANSPLUSTAGStatement(line, activityListElem);
								break;
                            case 47:
                                // spot isnt working
                                // processCloseClampNumberStatement(line, activityListElem);
                                break;
                                
                            case 48:
                            // SWAIT (<number> or <-number>)
                            	processSWAITStatement(line, activityListElem);
                            	break;
                            	
                            case 100: // CP ON|OFF
                                processCPstatement(match);
                                break;
                            case 200: // HOME | HOME2
                                processHOMEstatement(line, activityListElem);
                                break;
                                
							case 300:
								// CALL <proc_name>
								processCALLStatement(line, activityListElem);
								break;
							case 350:
								// BLOCK statement
								processBLOCKStatementProfiles(line, match);
								for (int jj = 351; jj < 356; jj++)
								{
									match = m_keywords[jj].matcher(line);

									if (match.find() == true)
									{
										//Call appropriate methods to handle the input
										switch (jj)
										{
											case 351:
												processBLOCKStatementClamps(line, match);
											break;
											case 352:
												processBLOCKStatementIO(line,match);
											break;
											case 353:
											case 354:
												processBLOCKStatementCL(line,match);
											break;
											case 355:
												processBLOCKStatementPosition(line,match,activityListElem);
											break;											
										}
									}
								}
								break;
							case 2000: // General Sixtuple. We should hit .JOINTS or .TRANS first
								// probably an error.
								//java.lang.System.out.println("Match 6tuple " + line);
								break;

							default:
								break;
						}//end switch

			        	m_listOfParsedAndUnParsedStatements.add("Parsed: " + line);
						//match found & processed, break out for loop
						break;
					}//end if
					else if(ii == m_keywords.length - 1)
					{
						//java.lang.System.out.println("**Unparsed " + line);
						errCode = processUNPARSEDStatement(line, activityListElem, section);
					}
				}//end for

				line = rbt.readLine();
				// java.lang.System.out.println("reading line " + line);

			}//end while
			rbt.close();
		}//end try

		catch (IOException e)
		{
			e.getMessage();
			// System.out.println(e.toString());
			return 2;
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			try
			{
				bw.write("ERROR- Array Index out of bounds during translation.\n");
			}
			catch (IOException bwe)
			{
			}
			return 2;
		}
		return errCode;
	} // end of processRobotProgramLines

	private void compileAUXDATAKeywords()
	{
		m_keywords = new Pattern[2001];
		// initialize m_keywords array
		for (int ii = 0; ii < m_keywords.length; ii++)
		{
			m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
		}
		String dec_number = "[+-]?((\\d+(\\.\\d*)?)|(\\.\\d+))";
		m_keywords[0] = Pattern.compile("^\\d*\\s*.AUXDATA\\s*$", Pattern.CASE_INSENSITIVE);
		m_keywords[1] = Pattern.compile("^\\d*\\s*.END\\s*$", Pattern.CASE_INSENSITIVE);						
		m_keywords[2] = Pattern.compile("^\\d*\\s*SPEED\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		m_keywords[3] = Pattern.compile("^\\d*\\s*ACCUR\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s*(" + dec_number + ")?)", Pattern.CASE_INSENSITIVE);
		m_keywords[4] = Pattern.compile("^\\d*\\s*TIMER\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		m_keywords[5] = Pattern.compile("^\\d*\\s*TOOL([1-9])\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		m_keywords[6] = Pattern.compile("^\\d*\\s*WORK([1-9])\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);		
	}
	
	private int compileKeywords()
	{
		m_keywords = new Pattern[2001];
		// initialize m_keywords array
		for (int ii = 0; ii < m_keywords.length; ii++)
		{
			m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
		}

        // decimal number -1, +2, 3.4, 5., .6
		/* This will match anything having six or more numbers
         * String dec_number = "[-+]?(?:\\d+)|(?:\\d+\\.\\d+)|(?:\\d+\\.)|(?:\\.\\d+)";
         */
		String identifier = "[A-Za-z]*[A-Za-z0-9_\\.\\[\\]]*";
		String dec_number = "[-+]?\\d*\\.?\\d*";
		String number = "([-+]?\\d+)";
        String seperator = "\\s+|,";
		String sixtuple = "(?:" + dec_number + seperator + "){5}" + dec_number;
        
		//String tsixtuple = "t" + dec_number + "\\s*[,]?\\s*t" + dec_number + "\\s*[,]?\\s*t" + dec_number + "\\s*[,]?\\s*t" + dec_number + "\\s*[,]?\\s*t" + dec_number + "\\s*[,]?\\s*t" + dec_number;
		
		//String ofsixtuple = "of" + dec_number + "\\s*[,]?\\s*of" + dec_number + "\\s*[,]?\\s*of" + dec_number + "\\s*[,]?\\s*of" + dec_number + "\\s*[,]?\\s*of" + dec_number + "\\s*[,]?\\s*of" + dec_number;
	
		// Comment statement
		m_keywords[0] = Pattern.compile("^;", Pattern.CASE_INSENSITIVE);

		// .PROGRAM
		m_keywords[1] = Pattern.compile("^\\d*\\s*.PROGRAM\\s*", Pattern.CASE_INSENSITIVE);

		//---------------------------------------
		// Controller Profiles and Configuration
		//---------------------------------------

		// SPEED <number> MM/(S|MIN)
		m_keywords[2] = Pattern.compile("^\\d*\\s*SPEED\\s*(" + dec_number + ")\\s*(MM)(?:\\/(S|MIN))", Pattern.CASE_INSENSITIVE);
		// SPEED <number> S
		m_keywords[3] = Pattern.compile("^\\d*\\s*SPEED\\s*(" + dec_number + ")\\s*(S)", Pattern.CASE_INSENSITIVE);
		// SPEED <number> (number is between 0 and 100)
		m_keywords[4] = Pattern.compile("^\\d*\\s*SPEED\\s*(" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		// ACCEL<number>ALWAYS
		// m_keywords[4] = Pattern.compile("^\\d*\\s*ACCEL\\s*\\d{1,3}\\s*ALWAYS", Pattern.CASE_INSENSITIVE);
		// DECEL<number>ALWAYS
		// m_keywords[5] = Pattern.compile("^\\d*\\s*DECEL\\s*\\d{1,3}\\s*ALWAYS", Pattern.CASE_INSENSITIVE);
		// ACCURACY<number>
		m_keywords[6] = Pattern.compile("^\\d*\\s*ACCURACY\\s*\\d{1,3}\\s*", Pattern.CASE_INSENSITIVE);
		
		// UWRIST BELOW RIGHTY
		m_keywords[7] = Pattern.compile("^\\d*\\s*UWRIST\\s*BELOW\\s*RIGHTY\\s*$", Pattern.CASE_INSENSITIVE);
		// UWRIST ABOVE RIGHTY
		m_keywords[8] = Pattern.compile("^\\d*\\s*UWRIST\\s*ABOVE\\s*RIGHTY\\s*$", Pattern.CASE_INSENSITIVE);
		// UWRIST BELOW LEFTY
		m_keywords[9] = Pattern.compile("^\\d*\\s*UWRIST\\s*BELOW\\s*LEFTY\\s*$", Pattern.CASE_INSENSITIVE);
		// UWRIST ABOVE LEFTY
		m_keywords[10] = Pattern.compile("^\\d*\\s*UWRIST\\s*ABOVE\\s*LEFTY\\s*$", Pattern.CASE_INSENSITIVE);
		// DWRIST BELOW RIGHTY
		m_keywords[11] = Pattern.compile("^\\d*\\s*DWRIST\\s*BELOW\\s*RIGHTY\\s*$", Pattern.CASE_INSENSITIVE);
		// DWRIST ABOVE RIGHTY
		m_keywords[12] = Pattern.compile("^\\d*\\s*DWRIST\\s*ABOVE\\s*RIGHTY\\s*$", Pattern.CASE_INSENSITIVE);
		// DWRIST BELOW LEFTY
		m_keywords[13] = Pattern.compile("^\\d*\\s*DWRIST\\s*BELOW\\s*LEFTY\\s*$", Pattern.CASE_INSENSITIVE);
		// DWRIST ABOVE LEFTY
		m_keywords[14] = Pattern.compile("^\\d*\\s*DWRIST\\s*ABOVE\\s*LEFTY\\s*$", Pattern.CASE_INSENSITIVE);

		// ------------------------------------------
		// Tool, Object Frame Offsets and Tag Points
		//-------------------------------------------

		// 
		// .TRANS
		// ...
		// <tag_name> <xx> <yy> <zz> <oo> <aa> <tt>
		// ...
		// .END 

		// NOTE: Items in .TRANS are local to the program and not known to the controller
		m_keywords[15] = Pattern.compile("^\\d*\\s*.TRANS", Pattern.CASE_INSENSITIVE);

		m_keywords[17] = Pattern.compile("^\\d*\\s*.END{1}", Pattern.CASE_INSENSITIVE);

		// .JOINTS
		// #<location_id> <j1> <j2> <j3> <j4> <j5> <j6>
		// .END
		// ? <location_id>
		m_keywords[18] = Pattern.compile("^\\d*\\s*.JOINT", Pattern.CASE_INSENSITIVE);

		
		// .AUXDATA
		// TOOL1 <xx> <yy> <zz> <oo> <aa> <tt>
		// TOOL2 <xx> <yy> <zz> <oo> <aa> <tt>
		// .END

		m_keywords[19] = Pattern.compile("^\\d*\\s*.AUXDATA\\s*$", Pattern.CASE_INSENSITIVE);

		// .PROGRAM
		// TOOL <tool_name>
		// .END
		m_keywords[20] = Pattern.compile("^\\d*\\s*TOOL\\s*(" + identifier + ")", Pattern.CASE_INSENSITIVE);

		// TOOL utool_1

		// .TRANS
		// utool_1 <xx> <yy> <zz> <oo> <aa> <tt>
		// .END

		// .AUXDATA
		// BASE1 <xx> <yy> <zz> <oo> <aa> <tt>
		// BASE2 <xx> <yy> <zz> <oo> <aa> <tt>
		// .END
		m_keywords[21] = Pattern.compile("\\w*BASE\\w*" + sixtuple, Pattern.CASE_INSENSITIVE);

		// BASE uframe_<number>
		m_keywords[22] = Pattern.compile("^\\d*BASE|base{1}\\s*uframe_\\d*", Pattern.CASE_INSENSITIVE);

		// .TRANS
		// uframe_<number> <xx> <yy> <zz> <oo> <aa> <tt>
		m_keywords[23] = Pattern.compile("^\\d*\\s*\\w*uframe|UFRAME{1}\\w*\\s*" + sixtuple, Pattern.CASE_INSENSITIVE);

		// ---------------------------
		// Digital Signals and Delays
		// ---------------------------

		// SIGNAL<number>
		m_keywords[24] = Pattern.compile("^\\d*\\s*SIGNAL\\s+-?\\d+", Pattern.CASE_INSENSITIVE);

		// PULSE <number> <time>
		m_keywords[26] = Pattern.compile("^\\d*\\s*PULSE\\s*\\d*\\s*\\d*", Pattern.CASE_INSENSITIVE);

		// TIMER <timer_number> = <initial_time>
		m_keywords[27] = Pattern.compile("^\\d*\\s*TIMER\\s*\\d*\\s*/=\\s*\\d*", Pattern.CASE_INSENSITIVE);

		// WAIT SIG( <number> or <-number> OR TIMER(<timer_number>) <time>
		m_keywords[28] = Pattern.compile("^\\d*\\s*WAIT\\s*SIG\\s*/(\\s*\\d*\\s*/)\\s*OR\\s*TIMER\\s*/(\\S*\\d*\\s*/)\\s*\\d*", Pattern.CASE_INSENSITIVE);
		m_keywords[29] = Pattern.compile("^\\d*\\s*WAIT\\s*SIG\\s*/(\\s*/-\\s*\\d*\\s*/)\\s*OR\\s*TIMER\\s*/(\\S*\\d*\\s*/)\\s*\\d*", Pattern.CASE_INSENSITIVE);

		// TWAIT <number> (number is in seconds) 
		m_keywords[30] = Pattern.compile("^\\d*\\s*TWAIT\\s*\\d*", Pattern.CASE_INSENSITIVE);

		// -------------------------
		//	Control Flow Statements
		// -------------------------

		// CALL Weldnum
		m_keywords[31] = Pattern.compile("CALL\\s+WELD\\d+", Pattern.CASE_INSENSITIVE);

		// -------------------------
		//	Move Statements
		// -------------------------

		// JAPPRO <tag_name> <distance_along_z_axis>
		m_keywords[32] = Pattern.compile("^\\d*\\s*JAPPRO\\s*(" + identifier + ")", Pattern.CASE_INSENSITIVE);

		// LAPPRO <tag_name> <distance_along_z_axis>
		m_keywords[33] = Pattern.compile("^\\d*\\s*LAPPRO\\s*(" + identifier + ")", Pattern.CASE_INSENSITIVE);

		// CMOVE <tag_name>
		m_keywords[34] = Pattern.compile("^\\d*\\s*(C[1|2]MOVE)\\s*(" + identifier + ")\\s*$", Pattern.CASE_INSENSITIVE);

		// CMOVE #P1
		m_keywords[35] = Pattern.compile("^\\d*\\s*(C[1|2]MOVE)\\s+#\\s*(" + identifier + ")\\s*$", Pattern.CASE_INSENSITIVE);

		// JMOVE <tag_name>
		m_keywords[36] = Pattern.compile("^\\d*\\s*JMOVE\\s*(" + identifier + ")\\s*$", Pattern.CASE_INSENSITIVE);

		// LMOVE <tag_name>
		m_keywords[37] = Pattern.compile("^\\d*\\s*LMOVE\\s*(" + identifier + ")\\s*$", Pattern.CASE_INSENSITIVE);
		
		m_keywords[38] = Pattern.compile("^\\d*\\s*\\w*tool|TOOL{1}\\w*\\s*" + sixtuple, Pattern.CASE_INSENSITIVE);
		
		// JMOVE TRANS(tX, tY, tZ, tYaw, tPitch, tRoll)
		m_keywords[39] = Pattern.compile("^\\d*\\s*JMOVE\\s*TRANS\\s*\\(\\s*" + sixtuple + "\\s*\\)\\s*$", Pattern.CASE_INSENSITIVE);

		// JMOVE #P1
		m_keywords[40] = Pattern.compile("^\\d*\\s*JMOVE\\s+#\\s*(" + identifier + ")\\s*$", Pattern.CASE_INSENSITIVE);
	
		// JMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + TRANS(tX, tY, tZ, tYaw, tPitch, tRoll)
		m_keywords[41] = Pattern.compile("^\\d*\\s*JMOVE\\s*TRANS\\s*\\(" + sixtuple + "\\)" + "\\s*" +
				"\\+\\s*TRANS\\s*\\(" + sixtuple + "\\)", Pattern.CASE_INSENSITIVE);
		
		// JMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + Tag1 
		m_keywords[42] = Pattern.compile("^\\d*\\s*JMOVE\\s*TRANS\\s*\\(\\s*" + sixtuple + "\\s*\\)\\s*\\+\\s*\\w*\\s*$", Pattern.CASE_INSENSITIVE);
		
		// LMOVE TRANS(X, Y, Z, Yaw, Pitch, Roll)
		m_keywords[43] = Pattern.compile("^\\d*\\s*LMOVE\\s*TRANS\\s*\\(\\s*" + sixtuple + "\\s*\\)\\s*$", Pattern.CASE_INSENSITIVE);

		// LMOVE #P1
		m_keywords[44] = Pattern.compile("^\\d*\\s*LMOVE\\s+#\\s*(" + identifier + ")\\s*$", Pattern.CASE_INSENSITIVE);
				
		// LMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + TRANS(tX, tY, tZ, tYaw, tPitch, tRoll)
		m_keywords[45] = Pattern.compile("^\\d*\\s*LMOVE\\s*TRANS\\s*\\(" + sixtuple + "\\)" + "\\s*" +
				"\\+\\s*TRANS\\s*\\(" + sixtuple + "\\)", Pattern.CASE_INSENSITIVE);

		// LMOVE TRANS(ofX, ofY, ofZ, ofYaw, ofPitch, ofRoll) + Tag1 
		m_keywords[46] = Pattern.compile("^\\d*\\s*LMOVE\\s*TRANS\\s*\\(\\s*" + sixtuple + "\\s*\\)\\s*\\+\\s*\\w*\\s*$", Pattern.CASE_INSENSITIVE);
        
        /* CLOSE <clamp_number>  
		m_keywords[47] = Pattern.compile("^\\d*\\s*CLOSE\\s*\\d*\\s*$", Pattern.CASE_INSENSITIVE);
		*/
		// SWAIT <number> (number is TRUE/FALSE 0 or 1) 
		m_keywords[48] = Pattern.compile("^\\d*\\s*SWAIT\\s*\\d*", Pattern.CASE_INSENSITIVE);
	
        // CP ON|OFF (Continuous Path switch)
        m_keywords[100] = Pattern.compile("CP\\s+(ON|OFF)", Pattern.CASE_INSENSITIVE);
        m_keywords[200] = Pattern.compile("HOME", Pattern.CASE_INSENSITIVE);
		//CALL <proc_name>
		m_keywords[300] = Pattern.compile("CALL\\s+(" + identifier + ")", Pattern.CASE_INSENSITIVE);

		// BLOCK FORMAT
		String clampStr = "((CLAMP1|2|3|4)\\s*\\(\\s*(ON|OFF)\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*([0-7]|O|C)\\s*\\)\\s*)"; 
		String cl1Str = "(CL[1-9]\\s*\\=\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*)";
		String cl2Str = "(CL[1-9]\\s*\\=\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*)";
		String positionStr = "(#\\s*\\[\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number+ ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number+ ")?\\s*,?\\s*(" + dec_number + ")?\\])\\s*";
		String ioStr = "((OX|WX)\\s*\\=\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?)\\s*";
		m_keywords[350] = Pattern.compile("(JOINT|LINEAR|CIR|CIR2)\\s*(SPEED[0-9])\\s*(ACCU[0-4])\\s*(TIMER[0-9])\\s*(TOOL[1-9])\\s*(WORK[0-9])\\s*");
		m_keywords[351] = Pattern.compile("(" + clampStr + clampStr + "?" + clampStr + "?" + clampStr + "?" + ")");
		m_keywords[352] = Pattern.compile(ioStr + ioStr);
		m_keywords[353] = Pattern.compile(cl1Str);
		m_keywords[354] = Pattern.compile(cl2Str);		
		m_keywords[355] = Pattern.compile(positionStr);
		// following are so unparsed errors don't occur when reading BLOCK .AUXDATA
		// these lines are interpreted using compileAUXDATAKeywords...not these keywords
		m_keywords[356] = Pattern.compile("^\\d*\\s*SPEED\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		m_keywords[357] = Pattern.compile("^\\d*\\s*ACCUR\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		m_keywords[358] = Pattern.compile("^\\d*\\s*TIMER\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		m_keywords[359] = Pattern.compile("^\\d*\\s*TOOL([1-9])\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);
		m_keywords[360] = Pattern.compile("^\\d*\\s*WORK([1-9])\\s*(" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + "\\s+" + dec_number + ")", Pattern.CASE_INSENSITIVE);		
		
		// general identifier must be the last one to match
		// a general identifier plus a sixtuple
		m_keywords[2000] = Pattern.compile("#?(" + identifier + ")\\s+" + sixtuple, Pattern.CASE_INSENSITIVE);

		return 0;
	} // compileKeywords

	private void setCommentStatements(Element activityElem, String prefix)
	{
        String prepostComment;
		int numbercomments = m_listOfCommentNames.size();
		if(numbercomments > 0)
		{
			for(int k = 0; k < numbercomments; k++)
			{
                prepostComment = (String)m_listOfCommentNames.get(k);
                if (prepostComment.startsWith("Comment"))
                    prepostComment = prefix + prepostComment;
                addAttribute(activityElem, prepostComment, (String)m_listOfCommentValues.get(k));
			}
		}
		
        m_listOfCommentNames.clear();
        m_listOfCommentValues.clear();
		return;
    }
	
	private void addAttribute(Element actElem, String attrName, String attrVal)
	{
		String[] attrNames = { attrName };
		String[] attrValues = { attrVal };
		Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
	} // end addAttribute
	
	private void addParameter(Element actElem, String paramName, String paramVal)
	{
		String[] paramNames = { paramName };
		String[] paramValues = { paramVal };
		Element parameterListElem = super.createParameterList(actElem, paramNames, paramValues);
	} // end addAttribute	
    //--------------------------------------------------------------------
    // createSpeedProfile
    //
    // Should probably be called createMotionProfile.
    //--------------------------------------------------------------------
	public void createSpeedProfile(String speedString, String profileName)
	{
		//<MotionProfile>
		//<Name>Default</Name>
		//<MotionBasis>Percent|Absolute</MotionBasis>
		//<Speed Units="%"|"m/s" Value="50" />
		//<Accel Units="%" Value="100" />
		//<AngularSpeedValue Units="%" Value="100" />
		//<AngularAccelValue Units="%" Value="100" />
		//</MotionProfile>

        DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
        double spd = 0.0, defaultVal = 100.0;

		if (speedString.startsWith("%"))
		{
			moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
			spd = Double.parseDouble(speedString.substring(1));
		}
        else if (speedString.startsWith("T"))
        {
            moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME;
            spd = Double.parseDouble(speedString.substring(1));
        }
		else
		{
			moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
			spd = Double.parseDouble(speedString);
		}

		if (profileName.startsWith("SPEED"))
		{
			// This function is being called from processSpeedStatement.
			// See if we can use the current motion profile
			DELOlpJProfile foundProfile = (DELOlpJProfile)m_htMotionProfiles.get(profileName);
			if (foundProfile == null)
			{
				// No Motion Profile by the name <profileName>, create it.
				DELOlpJProfile profileTracker = new DELOlpJProfile(profileName, spd, defaultVal /*accel*/, moBasis );
                profileTracker.Speed(spd); // dont allow speed to be reset.
				m_htMotionProfiles.put(profileTracker.Name(), profileTracker);
				
				m_sCurrentMotionProfileName = profileTracker.Name();

				Element motionProfileListElement = super.getMotionProfileListElement();
				Element motionProfileElem = super.createMotionProfile(motionProfileListElement, profileName, moBasis, spd, defaultVal, defaultVal, defaultVal);
			}
			else
            {
                // Motion Profile <profileName> exists, set current motion profile
				m_sCurrentMotionProfileName = profileName;
			}
		} // Speed

	    if (profileName.startsWith("ACCEL") || profileName.startsWith("DECEL"))
        {
            // This function is being called from processAccelStatement.
            // See if we can use the current motion profile
            DELOlpJProfile foundProfile = (DELOlpJProfile)m_htMotionProfiles.get(m_sCurrentMotionProfileName);
			if (foundProfile == null)
			{   
				// No profile, or Acceleration has already been set on the current profile.
				// We need to create a new motion profile 
				DELOlpJProfile profileTracker = null;
                                              
                if (foundProfile != null)
                {
                    profileTracker = new DELOlpJProfile(profileName, foundProfile.Speed(), foundProfile.Acceleration(), foundProfile.MotionBasis());
                }
                else
                {
                    profileTracker = new DELOlpJProfile(profileName, 100, spd /*accel*/, moBasis );
                }
                
                profileTracker.Acceleration(spd); // dont allow acceleraton to be reset.
				m_htMotionProfiles.put(profileTracker.Name(), profileTracker);

				m_sCurrentMotionProfileName = profileTracker.Name();

				Element motionProfileListElement = super.getMotionProfileListElement();
				Element motionProfileElem = super.createMotionProfile(motionProfileListElement, profileTracker.Name(), profileTracker.MotionBasis(), profileTracker.Speed(), spd , 100.0, 100.0);
			}
            else
            {   // foundprofile != null && !foundprofile.accelerationset()
             // we can set the acceleration on the current profile
                SetDOMMotionProfileElementValue(m_sCurrentMotionProfileName, "Accel", spd);
                foundProfile.Acceleration(spd); // dont allow accel to be reset.
            }
       
        } // ACCEL
        
        //m_sCurrentMotionProfileName = profileName;
        
	} // end of createSpeedProfile

	//--------------------------------------------------------------
	// createAccuracyProfile
	//
	//
	//--------------------------------------------------------------
	public void createAccuracyProfile(String profileName, boolean flyByMode, String accuracyString) throws NumberFormatException
	{
        /*
		<AccuracyProfile>
            <Name>Default</Name>
            <FlyByMode>On|Off</FlyByMode>
            <AccuracyType>Distance|Speed</AccuracyType>
            <AccuracyValue Units="m|%" Value="0" />
		</AccuracyProfile>
         */

		Element accuracyProfileList = super.getAccuracyProfileListElement();

		double pLevel = Double.parseDouble(accuracyString);
		double accuracyVal = pLevel/1000.0;

		Element accuracyProfileElem = super.createAccuracyProfile(
                                                accuracyProfileList,
                                                profileName,
                                                DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE,
                                                flyByMode,
                                                accuracyVal);
	} // end of createAccuracyProfile

	private void processCPstatement(Matcher match)
	{
        if (match.group(1).equalsIgnoreCase("OFF"))
            m_bContinuousPath = false;
        else
            m_bContinuousPath = true;
    }
    
	//-----------------------------------------------------------------------
	// processUNPARSEDStatement
	//
	// 
	//-----------------------------------------------------------------------
	private int processUNPARSEDStatement(String line, Element activityListElem, String section)
	{
		String upperMacro = new String(line);
		upperMacro = upperMacro.toUpperCase();
        boolean macroFound = false;
        int errCode = 0;
        
        if (upperMacro != null && section.trim().equals("PROGRAM"))
        {
      	   int pickIndex = upperMacro.indexOf("PICK");
      	   int grabIndex = upperMacro.indexOf("GRAB");
      	   int dropIndex = upperMacro.indexOf("DROP");
      	   int releaseIndex = upperMacro.indexOf("RELEASE");
      	   int spotIndex = upperMacro.indexOf("SPOT");
      	   if (pickIndex >= 0 || grabIndex >= 0)
      	   {
      		   String activityName = "GrabActivity." + String.valueOf(m_PickCounter++);
               Element actElem = super.createGrabActivity( activityListElem, activityName, "", "");      		   
      		   macroFound = true;
      	   }
      	   else if (dropIndex >= 0 || releaseIndex >= 0)
      	   {
      		   String activityName = "ReleaseActivity." + String.valueOf(m_DropCounter++);
               Element actElem = super.createReleaseActivity( activityListElem, activityName, "");      		   
      		   macroFound = true;
      	   }
      	   else if (spotIndex >= 0 && pickIndex < 0 && grabIndex < 0 && dropIndex < 0 && releaseIndex < 0)
      	   {
      		   Element actionElem = super.createActionHeader( activityListElem, line, "KawasakiASSpotWeld", "");
      	       macroFound = true;
      	   }
           else for (int ii=1; ii<9; ii++)
           {
               if (upperMacro.equalsIgnoreCase(m_cfgmap[ii]))
               {
                   m_sCurrentConfig = "Config_" + ii;
          	       macroFound = true;
                   break;
        }                     
           }
        }
        if (macroFound == false)
        {
        	if (section.trim().equals("PROGRAM"))
        	{
                if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
                {
                    String [] nativeNames = {"Robot Language"};
                    String [] nativeValues = {line};
                    Element actElem = super.createActivityHeader(activityListElem, line, "Operation");
                    super.createAttributeList(actElem, nativeNames, nativeValues);
                }
                else
                {
                    m_listOfCommentNames.add("Robot Language" + m_commentCounter);
                    m_listOfCommentValues.add(line);
                    m_commentCounter++;
                }
        	}
            
        	m_listOfParsedAndUnParsedStatements.add("WARNING UnParsed Statement: " + line);
        	errCode = 1;
	}
		return errCode;
	} // processUNPARSEDStatement
	//-----------------------------------------------------------------------
	// processPROGRAMStatement
	//
	// 
	//-----------------------------------------------------------------------
	private Element processPROGRAMStatement(String line, BufferedReader rbt)
	{
        String stitle = ".PROGRAM WELD";
        String sprogramName = "";
        if (line.toUpperCase().startsWith(stitle))
		{
            try
			{
                int len = stitle.length();
                int idx = line.indexOf('(');
                sprogramName = line.substring(len, idx);
                Integer dummyInt = Integer.decode(sprogramName);
                // program name is WELD<number>, eat lines until .END
                while (line != null)
                {
                    line = rbt.readLine();
                    if (line.equalsIgnoreCase(".END"))
                        return null;
			}
		}
            catch (NumberFormatException nfe) { } // not a spot weld schedule, continue
            catch (IOException ioe) {ioe.getMessage();}
        }
		
		String [] spComponents = line.split("[ ()]+");
            
        if (spComponents.length > 1)
            sprogramName = spComponents[1];
        else
            sprogramName = "RobotTask." + m_RobotTaskCounter;
		
		m_sProgramNames.add(sprogramName);
        
		m_RobotTaskCounter++;

        m_sCurrentProgramName = sprogramName;
		Element activityListElem = super.createActivityList(sprogramName);
        // create an empty child AttributeList element to ActivityList
        // or when Activity/AttributeList is created, it will be found and
        // used as ActivityList/AttributeList
        Element attributeListElem = super.createAttributeList(activityListElem, null, null);

        return activityListElem;
	}
	//-----------------------------------------------------------------------
	// mangleTaskName
	//
	// 
	//-----------------------------------------------------------------------
	private String mangleTaskName(String taskName)
	{
		String strtemp = taskName;
		
		if(!m_sProgramNames.isEmpty())
		{
			strtemp = taskName;
			strtemp += ("." + m_RobotTaskCounter2);
			m_RobotTaskCounter2++;
			
			if(m_sProgramNames.contains(strtemp))
			{
				strtemp = mangleTaskName(taskName);
			}
		}
		
		m_RobotTaskCounter2 = 1;
		return strtemp;
	}
	
	//-----------------------------------------------------------------------
	// mangleTagGroupName
	//
	// should provide suffix to name equivalent to current task (progrm)
	//-----------------------------------------------------------------------
	private String mangleTagGroupName(String taskName)
	{
		String strtemp = taskName;
		
		if(!m_sTagGroupNames.isEmpty())
		{
			strtemp = taskName;
			strtemp += ("." + m_RobotTagGroupCounter2);
			m_RobotTagGroupCounter2++;
			
			if(m_sTagGroupNames.contains(strtemp))
			{
				strtemp = mangleTaskName(taskName);
			}
		}
		
		m_RobotTagGroupCounter2 = 1;
		return strtemp;
	}
	
	private void processSPEEDStatement(String line, Matcher match, Element activityListElem)
	{
		String speedString = match.group(1);
		String motionProfileName = "Default";

        double factor = 1.0, spd;
        if (match.groupCount() > 2)
        {
            // distance units
            if (match.group(2).equalsIgnoreCase("MM"))
                factor = 1000.0;
            else if (match.group(2).equalsIgnoreCase("CM"))
                factor = 100.0;
        
            // time units
            if (match.group(3).equalsIgnoreCase("MIN"))
                factor *= 60.0;

            spd = Double.parseDouble(match.group(1))/factor;
            speedString = Double.toString(spd);
            motionProfileName = "SPEED " + match.group(1) + " " + match.group(2) + "/" + match.group(3);
        }
        else if (match.groupCount() > 1)
		{
            // Time based motion profile
			motionProfileName = "SPEED " + speedString + " S";
            speedString = "T" + speedString;
		}
		else
		{
			motionProfileName = "SPEED " + speedString;
			speedString = "%" + speedString; // the percent is a flag to createSpeedProfile
		}

			createSpeedProfile(speedString, motionProfileName);
		
        m_sCurrentMotionProfileName = motionProfileName;
        if (line.toUpperCase().indexOf("ALWAYS") > 0)
            m_sPersistentMotionProfileName = motionProfileName;
	} // processSPEEDStatement

	//-----------------------------------------------------------------------
	// processACCELStatement
	//
	// Target is RobotController.MotionProfile.Acceleration(Value = <number>).
	//-----------------------------------------------------------------------
	private void processACCELStatement(String line, Element activityListElem)
	{
		String speedString = "";
		String motionProfileName = "Default";

		line = line.trim();

		String[] spComponents = line.split("\\s+");

		speedString = spComponents[1].trim();

		
		if (line.indexOf("MM/S") != -1 || line.indexOf("mm/s") != -1)
		{
			// V5 speed unit is m/s, not mm/s
			motionProfileName = "ACCEL " + speedString + " MM/S";
		}
		else
		{
			motionProfileName = "ACCEL " + speedString;
            speedString = "%" + speedString; // the percent is a flag to createSpeedProfile
		}

		//boolean isInList = m_listOfAccelValues.contains(speedString);

		//if (false == isInList)
		//{
		//m_listOfAccelValues.add(speedString);
		createSpeedProfile(speedString, motionProfileName);
		//}
	}

	//-----------------------------------------------------------------------
	// processDECELStatement
	//
	// 
	//-----------------------------------------------------------------------
	private void processDECELStatement(String line, Element activityListElem)
	{
		String speedString = "";
		String motionProfileName = "Default";
		String[] spComponents = new String[6];

		int i = 0;
		StringTokenizer st = new StringTokenizer(line);
		while (st.hasMoreTokens())
		{
			spComponents[i] = st.nextToken();
			i++;
		}

		speedString = spComponents[1].trim();

		motionProfileName = line.trim();

		if (line.indexOf("MM/S") != -1 || line.indexOf("mm/s") != -1)
		{
			// V5 speed unit is m/s, not mm/s
			motionProfileName = "DECEL " + speedString + " MM/S";
		}
		else
		{
			motionProfileName = "DECEL " + speedString;
            speedString = "%" + speedString; // the percent is a flag to createSpeedProfile
		}

		//boolean isInList = m_listOfAccelValues.contains(speedString);

		//if (false == isInList)
		//{
		//m_listOfAccelValues.add(speedString);
		createSpeedProfile(speedString, motionProfileName);
		//}
	}

	//---------------------------------------------------------------------------
	// processACCURACYStatement
	//
	// robot : ACCURACY <number> ALWAYS
	// v5 : RobotController.AccuracyProfile.Distance( Unit = <number> )
	//----------------------------------------------------------------------------
	private void processACCURACYStatement(String line, Element activityListElem)
	{
		String accuracyString = "";
		String accuracyProfileName = "Default";
		String [] spComponents = line.split("[ \\t]+");

        accuracyString = spComponents[1].trim();
        accuracyProfileName = spComponents[0].trim() + " " + spComponents[1].trim() + "mm";
        createAccuracyProfile(accuracyProfileName, true, accuracyString);

        m_sCurrentAccuracyProfileName = accuracyProfileName;
        if (spComponents.length > 2)
		{
            if (spComponents[2].equalsIgnoreCase("ALWAYS"))
                m_sPersistentAccuracyProfileName = accuracyProfileName;
		}
		}

	//---------------------------------------------------------------------------
	// processUWRISTStatement
	//
	// kawasaki:UWRIST, joint 5 is positive
	// v5: RobotMotionActivity.Config1
	//----------------------------------------------------------------------------
	private void processUWRISTStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_1"; // config 1,3,5,7
	}

	//---------------------------------------------------------------------------
	// processDWRISTStatement
	//
	// kawasaki:DWRIST, joint 5 is negative
	// v5: RobotMotionActivity.Config2
	//----------------------------------------------------------------------------
	private void processDWRISTStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_2"; // config 2,4,6,8
	}

	//---------------------------------------------------------------------------
	// processTAGNAMEStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processTAGNAMEStatement(String line, Element activityListElem)
	{
		//Parse the string into array
		String[] spComponents = line.split(m_split_comma_or_space);
		if (spComponents.length > 7)
		{
			// we have line numbers in file. 
		}
	}

	//---------------------------------------------------------------------------
	// processTRANSStatement
	//
	// We expect a single line consisting of a series of (identifiers + sixtuples)
	// 
	//----------------------------------------------------------------------------
	private String[] processTRANSStatement(String line, Element activityListElem)
	{
		//Parse the string into array
		line.trim();

		String[] spComponents = new String[1];
		
		spComponents[0] = line;
        
		/*
		int nsize = spComponents.length;
		int lines_size = nsize / 7;

		String[] spLines = new String[lines_size];

		int i, j = 0;
		for (i = 0; i < lines_size; i++)
		{

			spLines[i] = "";

			for (j = i * 7; j < i * 7 + 7; j++)
			{
				spLines[i] = spLines[i] + " " + spComponents[j];

			}
		}
		*/
		return spComponents;
	}

	//---------------------------------------------------------------------------
	// processJOINTSStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processJOINTSStatement(String line, Element activityListElem)
	{
		//Parse the string into array
		String[] spComponents = line.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
	}

	//---------------------------------------------------------------------------
	// processTOOLStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processTOOLStatement(String line)
	{
		String [] spComponents = line.split("[ ,]+");
		String toolName = spComponents[0];

		String sXYZOAT = spComponents[1];
        for (int ii=2; ii<7; ii++)
            sXYZOAT += "," + spComponents[ii];

        DNBIgpOlpUploadMatrixUtils matrixUtils = new DNBIgpOlpUploadMatrixUtils();
        String sXYZWPR = matrixUtils.dgXyzoatToMatrix(sXYZOAT);
        String [] toolPosition = sXYZWPR.split("[ ,]+");

		double[] dtoolValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        double dlocation;

		for (int i = 0; i < 6; i++)
		{
			dlocation = Double.parseDouble(toolPosition[i]);
			if (i < 3)
                dtoolValues[i] = dlocation/1000.0;  
                        else
                dtoolValues[i] = dlocation; 
		}

		Element tpfList = super.getToolProfileListElement();
		Element toolProfileElem = super.createToolProfile(tpfList, toolName, 
					  DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, 
				  dtoolValues, // double [] daToolOffset
    				  null,    // Double dMass
				  null,    // double [] daCentroid
				  null);   // double [] daInertia
	}

	//---------------------------------------------------------------------------
	// processTOOLNAMEStatement
	//
	// .PROGRAM
	// TOOL <tool_name>
	// .END
	// 
	//----------------------------------------------------------------------------
	private void processTOOLNAMEStatement(String line, Element activityListElem)
	{
		String[] spComponents = line.split("\\s+");
		m_sCurrentToolProfileName = spComponents[1];
	}

	//---------------------------------------------------------------------------
	// processBASEStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processBASEStatement(String line, Element activityListElem)
	{
		String[] spComponents = line.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
	}

	//---------------------------------------------------------------------------
	// processBASEStatement2
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processBASEStatement2(String line, Element activityListElem)
	{
		String[] spComponents = line.split("\\s+");
		m_sCurrentObjectProfileName = spComponents[1];
	}

	//---------------------------------------------------------------------------
	// processUFRAMEStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processUFRAMEStatement(String line)
	{
		line = line.trim();

		String[] spComponents = line.split("[ ,]+");
		createObjectProfile(spComponents);
	}
		
	private void createObjectProfile(String[] spComponents)
	{
		String frameName = spComponents[0];

		String sFrameValue = null;

		double[] dframeValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

		for (int i = 1; i < 7; i++)
		{
			// skip leading component, its an identifier
			double dlocation = Double.parseDouble(spComponents[i]);
			if(i < 4) // not the pitch roll yaw
                dframeValues[i - 1] = dlocation/1000.0;  
            else
			dframeValues[i - 1] = dlocation;
		}

		Element frameListElement = super.getObjectFrameProfileListElement();

		boolean bapplyOffsetToTags = false;

		Element frameProfileElement = createObjectFrameProfile(frameListElement, 
                                            frameName, 
                                            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE, 
                                            dframeValues,
                                            bapplyOffsetToTags);
	}

	//---------------------------------------------------------------------------
	// processSIGNALStatement
	//
	// DNBIgpSetSignalActivity.PortNumber (Value = <number>).SignalValue (Value = ON)
    // DNBIgpSetSignalActivity.PortNumber (Value = <number>).SignalValue (Value = OFF)
    // Kawaski   SIGNAL <number>
    //           SIGNAL <-number> 
	// 
	//----------------------------------------------------------------------------
	private void processSIGNALStatement(String line, Element activityListElem)
	{
		String [] spComponents  = line.split("[ ,]+");
		String sActivityName, sSignalName;
        boolean bSignalValue;
        Integer iPortNumber;
        int intVal;
        Double dSignalDuration = new Double(0.0);
				
        for (int ii=1; ii<spComponents.length; ii++)
        {
        	if (spComponents[ii].startsWith(";") == false)
        	{
	            intVal = Integer.parseInt(spComponents[ii]);
	            bSignalValue = true;
	            if (intVal < 0)
	            {
	                intVal = -intVal;
	            bSignalValue = false;
	            }
	            iPortNumber = new Integer(intVal); // here intVal > 0
	        
	            sSignalName = ioName + iPortNumber;
	            if (ioName.endsWith("["))
	                sSignalName += "]";
	            if (bSignalValue)
	                sActivityName = "Set-" + sSignalName + " = ON";
	            else
	                sActivityName = "Set-" + sSignalName + " = OFF";
			
	            Element ioElem = super.createSetIOActivity(activityListElem, sActivityName,
	                    bSignalValue, sSignalName, iPortNumber, dSignalDuration);
	            setCommentStatements(ioElem, "Pre");
        	}
        	else
        	{
        		break;
        	}
        }
	}

	//---------------------------------------------------------------------------
	// processPULSEStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processPULSEStatement(String line, Element activityListElem)
	{
		String [] spComponents  = line.split("[ ,]+");
		String sActivityName;
		boolean bSignalValue = true;
		
		Integer iPortNumber = new Integer(spComponents[1]);
		
		double duration = 0.2;
        if (spComponents.length > 2)
            duration = Double.parseDouble(spComponents[2]);
		Double dSignalDuration = new Double(duration);
		
        String sSignalName = ioName + iPortNumber;
        if (ioName.endsWith("["))
            sSignalName += "]";
        
        sActivityName = "Set-" + sSignalName + " = ON [" + duration + "s ]";
        
		Element ioElem = super.createSetIOActivity(activityListElem, sActivityName,
                bSignalValue, sSignalName, iPortNumber, dSignalDuration);
        setCommentStatements(ioElem, "Pre");
	}

	//---------------------------------------------------------------------------
	// processTIMERStatement
	//
	// kawasaki: TIMER <timer_number> = <initial_time> 
	// v5 : DNBIgpWaitSignalActivity.PortNumber (Value = <number>).SignalValue (Value = ON or OFF)
	// .MaxWaitTime (Value = <time>)
	//----------------------------------------------------------------------------
	private void processTIMERStatement(String line, Element activityListElem)
	{
		                     
		String[] spComponents  = line.split("\\s+");
				
		String sActivityName = "uninitialized";
		
		boolean bSignalValue = true;
		
		String sSignalName = "Signal" + m_signalCounter;
		m_signalCounter++;
		
		Integer iPortNumber = new Integer(spComponents[1]);;
		//iPortNumber.parseInt(spComponents[1]);
		
		double duration = 1.0;
		Double dSignalDuration = new Double(duration);
		
		double portNumber = 0.0;
		
		//Element actElem = super.createWaitForIOActivity(activityListElem, 
		//												sActivityName, 
		//												bSignalValue, 
		//												portNumber, 
		//												iPortNumber, 
		//												dSignalDuration);

       //         addAttributeList(actElem, "Pre");
	}

	//---------------------------------------------------------------------------
	// processWAITStatement
	//
	// v5 : DNBIgpWaitSignalActivity.PortNumber (Value = <number>)
	// .SignalValue (Value = ON or OFF).MaxWaitTime (Value = <time>)
	// 
	//----------------------------------------------------------------------------
	private void processWAITStatement(String line, Element activityListElem)
	{
	}

	//---------------------------------------------------------------------------
	// processWAITStatement2
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processWAITStatement2(String line, Element activityListElem)
	{
	}

	//---------------------------------------------------------------------------
	// processTWAITStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processTWAITStatement(String line, Element activityListElem)
	{
		 String [] components = line.split(" ");

		 if (components.length > 1)
	{
	         Double dStartTime = new Double(0.0);
	         Double dEndTime = Double.valueOf(components[1].trim());
	         String sActivityName = "RobotDelay." + m_delayCounter;

	         Element actElem = super.createDelayActivity(activityListElem, sActivityName, dStartTime, dEndTime);
	         setCommentStatements(actElem, "Pre");     
	         m_delayCounter++;
         }
                 	 
	}
	private void processSWAITStatement(String line, Element activityListElem)
	{
		 String [] components = line.split(" ");

         Integer IValue = Integer.valueOf(components[1].trim());
         int ival = IValue.intValue();
                  
         String sActivityName = "Wait"; // Not Used?
                  
         boolean bSignalValue = true;
         if(ival <= 0){
        	 bSignalValue = false;
        	 ival = -ival;
         }
                 	 
         String SPort = new String();
         SPort = String.valueOf(ival);
         
         Integer IValue2 = new Integer(ival);
         
         //public Element createWaitForIOActivity(Element oActivityListElem, 
         //        String sActivityName, 
         //        boolean bSignalValue, 
         //        String sSignalName, 
         //        Integer iPortNumber, 
         //        Double dMaxWaitTime)
         Double fakeTime = new Double(0.0);
         
        if (m_commentWaitSignal == false)
        {
        	Element actElem = super.createWaitForIOActivity(activityListElem, 
        		 sActivityName, 
                 bSignalValue, 
                 SPort, 
                 IValue2, 
                 fakeTime);
            setCommentStatements(actElem, "Pre");
        }
        else
        {
        	int errCode = processUNPARSEDStatement(line, activityListElem, "PROGRAM");
        }
        // m_delayCounter++;
            
        // addAttributeList(actElem, "Pre");
	}
	
	//---------------------------------------------------------------------------
	// processCALLStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processCALLStatement(String line, Element activityListElem)
	{
		String [] spComponents = line.split("[ ;]+");

		String sCalledTaskName = spComponents[1];
		
		String sActivityName = "RobotCall." + String.valueOf(m_callCounter++); 
        
		for (int i = sCalledTaskName.length() - 1; i > 0; i--)
		{
            String stemp = sCalledTaskName.substring(i-1,i);
            
			if ( stemp.equals("(") )
			{
				sCalledTaskName = sCalledTaskName.substring(0, i-1);
                break;
			}
		}
		
		String upperCallName = new String(sCalledTaskName);
		upperCallName = upperCallName.toUpperCase();
		if (upperCallName.indexOf("SPOT") < 0)
			{
		Element callTaskElement = createCallTaskActivity(activityListElem, 
                                       sActivityName, 
                                      sCalledTaskName);
		setCommentStatements(callTaskElement, "Pre");
	}
		else
		{
			processUNPARSEDStatement(sCalledTaskName, activityListElem, "PROGRAM");
		}
	}

	//---------------------------------------------------------------------------
	// processJAPPROStatement
	//
	// kawasaki: JAPPRO <tag_name> <distance_along_Z_axis>
	//
	// v5:
	// DNBRobotMotionActivity.JointInterpolation.CartesianTarget. 
	// (TargetType = Tag, TagName = <tag_name>, shift the target along approach vector 
	// for <distance_along_Z_axis> value).
	// 
	//----------------------------------------------------------------------------
	private void processJAPPROStatement(String line, Element activityListElem)
	{
		
		String[] spComponents = line.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
		
		String stagName = spComponents[1];
		
		String sapproachDistance = spComponents[2];
		
		String sTagValue = null;
		
		double dtagLocation[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		getTagFromAuxOrTrans(stagName, dtagLocation);

		// Create Motion Element
		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

		String motionProfileName   = m_sCurrentMotionProfileName;
		String toolProfileName     = m_sCurrentToolProfileName;
		String accuracyProfileName = m_sCurrentAccuracyProfileName;
	    String objectProfileName   = m_sCurrentObjectProfileName;
		
		Element moAttrElem = super.createMotionAttributes(
                                    actElem, 
															motionProfileName, 
															accuracyProfileName, 
															toolProfileName, 
															objectProfileName,
															DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);								// eMotype

		boolean bIsViaPoint = false;													
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);
		
		Element jointTargetElem = super.createCartesianTarget(
						targetElem,	    // Element oTargetElem, 
						dtagLocation,   // location 
						m_sCurrentConfig);	// ? String sConfigName

		Element tagElem = super.createTag(jointTargetElem, stagName);
		
		setCommentStatements(actElem, "Pre");
	}

	//---------------------------------------------------------------------------
	// processLAPPROStatement
	//
	// 
	// v5: DNBRobotMotionActivity.LinearInterpolation.CartesianTarget. 
	// (TargetType = Tag, TagName = <tag_name>, shift the target along 
	// approach vector for <distance_along_Z_axis> value)
	//----------------------------------------------------------------------------
	private void processLAPPROStatement(String line, Element activityListElem)
	{
		String[] spComponents = line.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
		
		String stagName = spComponents[1];
		
		String sapproachDistance = spComponents[2];
		
		String sTagValue = null;

		double dtagLocation[] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		getTagFromAuxOrTrans(stagName, dtagLocation);

		// Create Motion Element
		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

		String motionProfileName   = m_sCurrentMotionProfileName;
		String toolProfileName     = m_sCurrentToolProfileName;
		String accuracyProfileName = m_sCurrentAccuracyProfileName;
	    String objectProfileName   = m_sCurrentObjectProfileName;
		
		Element moAttrElem = super.createMotionAttributes(
                                    actElem, 
															motionProfileName, 
															accuracyProfileName, 
															toolProfileName, 
															objectProfileName,
                                    DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION,
                                    DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS);    // eMotype

		boolean bIsViaPoint = false;													
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);
		
		double location[] = {0.0, 0.0, 0.0};
        
		Element jointTargetElem = super.createCartesianTarget(
						targetElem,	    // Element oTargetElem, 
						dtagLocation,   // location 
						m_sCurrentConfig);	// ? String sConfigName

		Element tagElem = super.createTag(jointTargetElem, stagName);
		
		setCommentStatements(actElem, "Pre");
	} // processLAPPROStatement

	//---------------------------------------------------------------------------
	// processC1MoveStatement
	//
	// kawasaki: C1MOVE <tag_name>
	// v5: DNBRobotMotionActivity.CircularInterpolation.CartesianTarget.CircularViaPoint
	// (TargetType = Tag, TagName = <tag_name>)
	// 
	//----------------------------------------------------------------------------
	private void processC1MoveStatement(String line, Element activityListElem, boolean isJointTarget)
	{
		if (isJointTarget)
        {
			createJointTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION);
		}
            else
		{
			createTagTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION);
        }
	}

	//---------------------------------------------------------------------------
	// processC2MoveStatement
	//
	// 
	// v5: DNBRobotMotionActivity.CircularInterpolation.CartesianTarget.CircularEndPoint
	// (TargetType = Tag, TagName = <tag_name>)
	//----------------------------------------------------------------------------
	private void processC2MoveStatement(String line, Element activityListElem, boolean isJointTarget)
	{
		if (isJointTarget)
        {
			createJointTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION);
		}
			else
		{
			createTagTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION);
        }
	}

	//---------------------------------------------------------------------------
	// processJMOVEStatement
	//
	// V5 : DNBRobotMotionActivity.JointInterpolation.CartesianTarget. (TargetType = Tag, TagName = <tag_name>)
	// 
	// Kawasaki : JMOVE <tag_name>
	//----------------------------------------------------------------------------
	private void processJMOVEStatement(String line, Element activityListElem)
	{
         createTagTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);
	}

	//---------------------------------------------------------------------------
	// processBLOCKStatementProfiles
	//
	// 
	// m_keywords[350] = Pattern.compile("(JOINT|LINEAR|CIR|CIR2)\\s*(SPEED[0-9])\\s*(ACCU[0-4])\\s*(TIMER[0-9])\\s*(TOOL[1-9])\\s*(WORK[0-9])\\s*");	
	// 
	//----------------------------------------------------------------------------
	private void processBLOCKStatementProfiles(String line, Matcher match)
	{
		if (match.group(1) != null)
		{
			if (match.group(1).equals("JOINT"))
			{
				m_eCurrentMotionType = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
			}
			else if (match.group(1).equals("LINEAR"))
			{
				m_eCurrentMotionType = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
				
			}
			else if (match.group(1).equals("CIR"))
			{
				m_eCurrentMotionType = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
			}
			else if (match.group(1).equals("CIR2"))
			{
				m_eCurrentMotionType = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;				
			}
		}
		if (match.group(2) != null)
		{
			m_sCurrentMotionProfileName = match.group(2);  
		}
		if (match.group(3) != null)
		{
			m_sCurrentAccuracyProfileName = match.group(3);  
		}
		if (match.group(4) != null)
		{
			m_sCurrentTimerName = match.group(4);  
		}		
		if (match.group(5) != null)
		{
			m_sCurrentToolProfileName = match.group(5);  
		}
		if (match.group(6) != null)
		{
			m_sCurrentObjectProfileName = match.group(6);  
		}
	}
	//---------------------------------------------------------------------------
	// processBLOCKStatementClamps
	//
	// 	
	// String clampStr = "((CLAMP1|2)\\s*\\(\\s*(ON|OFF)\\s*,\\s*" + dec_number + "\\s*,\\s*" + dec_number + "\\s*,\\s*[0-7]?(O|C)?\\s*\\))\\s*"; 
	// m_keywords[351] = Pattern.compile("(" + clampStr + clampStr + ")");
	// 
	//----------------------------------------------------------------------------	
	private void processBLOCKStatementClamps(String line, Matcher match)
	{
		if (match.group(1) != null)
		{
			m_sCurrentBlockClamp = match.group(1);
		}		
	}

	//---------------------------------------------------------------------------
	// processBLOCKStatementIO
	//
	// 	
	// String ioStr = "((OX|WX)\\s*\\=\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?\\s*,?\\s*" + number + "?)\\s*";
	// m_keywords[352] = Pattern.compile(ioStr + ioStr);
	// 
	//----------------------------------------------------------------------------	
	private void processBLOCKStatementIO(String line, Matcher match)
	{
		if (match.group(1) != null)
		{
			m_sCurrentBlockOX = match.group(1);
		}		
		if (match.group(9) != null)
		{
			m_sCurrentBlockWX = match.group(9);
		}				
	}
	//---------------------------------------------------------------------------
	// processBLOCKStatementCL
	//
	// 		
	// String cl1Str = "((CL1)?\\s*\\=?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?)?\\s*";
	// m_keywords[353] = Pattern.compile(cl1Str);
	// 
	//----------------------------------------------------------------------------	
	private void processBLOCKStatementCL(String line, Matcher match)
	{
		if (match.group(1) != null)
		{
			m_sCurrentBlockCL = match.group(1);
		}
	}
	
	//---------------------------------------------------------------------------
	// processBLOCKStatementPosition
	//
	// 		
	// String positionStr = "(#\\s*\\[\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number+ ")?\\s*,?\\s*(" + dec_number + ")?\\s*,?\\s*(" + dec_number+ ")?\\s*,?\\s*(" + dec_number + ")?\\])?\\s*";
	// m_keywords[354] = Pattern.compile(positionStr);
	// 
	//----------------------------------------------------------------------------	
	private void processBLOCKStatementPosition(String line, Matcher match, Element activityListElem)
	{
        if (match.group(1) != null)
        {
        	String posLine = match.group(1);
        	createBlockJointTargetWithMotype(posLine, activityListElem, m_eCurrentMotionType);
        }
	}
	
	private void createBlockJointTargetWithMotype(String line, Element activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType eMotionType)
	{
		// MotionProfile name
		String[] spComponents = line.split("[\\[\\] ,]+");
		        
		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		// <Activity ActivityType="DNBRobotMotionActivity">
		// <ActivityName>RobotMotion.1</ActivityName>
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);
        Element moAttrElem = null;	
		//<MotionAttributes>
		//<MotionProfile>SPEED 50</MotionProfile>
		//<ToolProfile>Default</ToolProfile>
		//<AccuracyProfile>Default</AccuracyProfile>
		//<ObjectFrameProfile>Default</ObjectFrameProfile>
		//<MotionType>Joint</MotionType>
		//</MotionAttributes>
		
        String accuracyProfileName = sFlyBy_OFF;
        // When CP is ON, assign accuracy profile for creating move activity
        if (true == m_bContinuousPath)
            accuracyProfileName = m_sCurrentAccuracyProfileName;
        
		if (eMotionType == DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION)
		{
			moAttrElem = super.createMotionAttributes(
                                actElem,
														  m_sCurrentMotionProfileName, 
                                accuracyProfileName,
														  m_sCurrentToolProfileName, 
														  m_sCurrentObjectProfileName,
	                                                        eMotionType);
		}
		else
		{
			moAttrElem = super.createMotionAttributes(
                                actElem,
					m_sCurrentMotionProfileName, 
                                accuracyProfileName,
                    m_sCurrentToolProfileName, 
                    m_sCurrentObjectProfileName,
                                eMotionType,
                                DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS);
		}		

        m_sCurrentAccuracyProfileName = m_sPersistentAccuracyProfileName;
        m_sCurrentMotionProfileName = m_sPersistentMotionProfileName;
        
        ArrayList [] oaJointTarget = new ArrayList [ m_numRobotAxes+m_numAuxAxes+m_numExtAxes+m_numWorkAxes];

		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, bIsViaPoint);		

		int numOfDOF = m_numAuxAxes + m_numExtAxes + m_numWorkAxes + m_numRobotAxes; // from input parameters.
		if (numOfDOF > 0)
    	{
			if ((spComponents.length) > numOfDOF)
			{
			    int ii, jj;
			    for (ii=1; ii<= numOfDOF; ii++)
                {
			         oaJointTarget[ii-1] = new ArrayList();
		
			         String sJointName = "Joint" + (ii);
			         oaJointTarget[ii-1].add(0,sJointName);

			         Double DjointValue = new Double(spComponents[ii]);
			         double dblValue = DjointValue.doubleValue();

			         String axesType = "";
			         axesType = m_axisTypes[ii-1];
		
			         if (axesType.equalsIgnoreCase("Translational") )
			         {
			        	 dblValue *= .001;  
			         }
			         else
			         {
			        	 dblValue = Math.toRadians(dblValue);
			         }

			         DjointValue = Double.valueOf(String.valueOf(dblValue));
			         oaJointTarget[ii-1].add(1, DjointValue);

			         Integer iDOFValue = new Integer(ii);
			         oaJointTarget[ii-1].add(2,iDOFValue);
		
			         if (axesType.equalsIgnoreCase("Translational") )
			         {
			        	 oaJointTarget[ii-1].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL);
			         }
			         else
			         {
			        	 oaJointTarget[ii-1].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL);
			         }             
		
			         if (ii > m_numRobotAxes)
			         {
				         int extAxesCount = 0;
				         int auxAxesCount = 0;
				         int workAxesCount = 0;
		                 String auxAxesType = (String)m_auxAxesTypes.get(String.valueOf(ii));
		                 if (auxAxesType != null) {
		                     if (auxAxesType.equalsIgnoreCase("RailTrackGantry")) {
		                          extAxesCount++;
		                          oaJointTarget[ii-1].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY);                          
		                     }
		                     else if ( auxAxesType.equalsIgnoreCase("EndOfArmTooling")) {            
		                         extAxesCount++;
		                         oaJointTarget[ii-1].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING);                     
		                     }
		                     else if ( auxAxesType.equalsIgnoreCase("WorkPositioner")) {
		                         workAxesCount++;
		                         oaJointTarget[ii-1].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER);
		                     }
		                 }
		                 else {
		                     extAxesCount++;
		                     oaJointTarget[ii-1].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING);
		                 }		        		        	 
			         }
			    }    
			}
			else
			{
	            try
	            {
				    bw.write("WARNING: MISMATCH BETWEEN NUMBER OF AUX. AXES FOR UPLOAD ROBOT AND PROGRAM\n");
	            } // try
	            catch (IOException e)
	            {
	                e.getMessage();
	            }				
			}
		}
	    
		Element jointTargetElem = super.createJointTarget(
				targetElem,	    //Element oTargetElem, 
						oaJointTarget);	    //String sConfigName
		
		
        // OX values ON (preoutput or append)
        if (m_sCurrentBlockOX.trim().equals("OX=") == false)
        {
    		String[] spOXvalues = {};
    		spOXvalues = m_sCurrentBlockOX.trim().split("[=,]+");
    		if (spOXvalues.length > 0)
    		{
    			for (int kk=1;kk<spOXvalues.length;kk++)
    			{
    				String oxValue = (String)m_OXValues.get(spOXvalues[kk].trim());
    				if (oxValue == null || (oxValue.equals("true") == false) || m_OXWXNegative == true)
    				{
        		        boolean bSignalValue;
        		        Integer iPortNumber;
        		        int intVal;
        		        Double dSignalDuration = new Double(0.0);
                        String sSignalName;
                        
        			    intVal = Integer.parseInt(spOXvalues[kk].trim());
        			    bSignalValue = true;
        			    if (intVal < 0)
        			    {
        			    	continue;
         			    }
        			    iPortNumber = new Integer(intVal); // here intVal > 0
        			        
        			    m_OXValues.put(Integer.toString(intVal), "true");
        			    
        			    sSignalName = "OX" + iPortNumber;

    		            sActivityName = "Set-" + sSignalName + " = ON";
    					
        			    Element outputAct = super.createSetIOActivity(activityListElem, sActivityName,
        			                    bSignalValue, sSignalName, iPortNumber, dSignalDuration);    				
        				if (m_OXPreOutput == true) 
        				{
        					// for preOutput case move output prior to move
                            activityListElem.removeChild(outputAct);
                            activityListElem.insertBefore(outputAct, actElem);
        				}    					
    				}
    			}
    		}
        }
        // TIMER (no preoutput)
        if (m_sCurrentTimerName.trim().equals("TIMER0") == false)
        {
        	String[] timerValues = m_auxdataTimer.trim().split("[,\\s{}]+");
            if (m_sCurrentTimerName.trim().indexOf("TIMER") == 0 && m_sCurrentTimerName.trim().length() == 6)
            {
            	String indexStr = m_sCurrentTimerName.substring(5);
                int index = Integer.parseInt(indexStr);
                if (index < timerValues.length)
                {
                	Pattern dec_number = Pattern.compile("[-+]?\\d*\\.?\\d*?");
            		Matcher match;
					match = dec_number.matcher(timerValues[index]);

					if (match.find() == true)
					{
				        Double dStartTime = new Double(0.0);
				        Double dEndTime = Double.valueOf(timerValues[index]);

				        super.createDelayActivity(activityListElem, m_sCurrentTimerName.trim(), dStartTime, dEndTime);	
					}
                }
            }
        }
        // WX waits
        if (m_sCurrentBlockWX.trim().equals("WX=") == false)
        {
    		String[] spWXvalues = m_sCurrentBlockWX.trim().split("[=,]+");
    		if (spWXvalues.length > 0)
    		{
    			if (m_commentWaitSignal==false)
    			{
    			for (int kk=1;kk<spWXvalues.length;kk++)
    			{
    		        boolean bSignalValue;
    		        Integer iPortNumber;
    		        int intVal;
                    String sSignalName;
                    
    			    intVal = Integer.parseInt(spWXvalues[kk].trim());
    			    bSignalValue = true;
    			    if (intVal < 0)
    			    {
    			    	bSignalValue = false;
    			    	intVal *= -1;
     			    }
    			    iPortNumber = new Integer(intVal); // here intVal > 0
    			        
    			    sSignalName = "WX" + iPortNumber;
		            if (bSignalValue)
		                sActivityName = "Wait-" + sSignalName + " = ON";
		            else
		                sActivityName = "Wait-" + sSignalName + " = OFF";
					   
		            //public Element createWaitForIOActivity(Element oActivityListElem, 
		            //        String sActivityName, 
		            //        boolean bSignalValue, 
		            //        String sSignalName, 
		            //        Integer iPortNumber, 
		            //        Double dMaxWaitTime)
		            Double fakeTime = new Double(0.0);
		            
	           	     super.createWaitForIOActivity(activityListElem, 
	           		 sActivityName, 
	                    bSignalValue, 
	                    sSignalName, 
	                    iPortNumber, 
	                    fakeTime);	            
	    			}
    			}
    			else
    			{
    	        	addAttribute(actElem, "WXValue", m_sCurrentBlockWX);
    			}
    		}
        }
        // OX values OFF (preoutput or append)
        if (m_sCurrentBlockOX.trim().equals("OX=") == false || m_OXWXNegative == false )
        {
    		ArrayList spOXvalues = new ArrayList();
        	if (m_sCurrentBlockOX.trim().equals("OX=") == false)
        	{
        		String oxValues[] = m_sCurrentBlockOX.trim().split("[=,]+");
        		for (int ii=1;ii<oxValues.length;ii++)
        		{
        			spOXvalues.add(oxValues[ii]);
        		}
        		// if not OX= and no negative values then all values turned on earlier that are
        		// not turned on here will now be turned off
        		//  i.e.   OX=1,4,26
        		//         OX=1,26   -- turn off 4 if negative values not allowed
        		if (m_OXWXNegative == false) 
        		{
	        		Collection values = m_OXValues.values();
	    			Enumeration keys = m_OXValues.keys();
	    	        Iterator itr = values.iterator();
	        		while(itr.hasNext())
	        		{
	        			String value = (String)itr.next();
	        			String key = (String)keys.nextElement();
	        			if (value.trim().equals("true") && spOXvalues.contains(key.trim()) == false)
	        			{
	        				String negKey = String.valueOf(-1*Integer.parseInt(key.trim()));
	        			    spOXvalues.add(negKey);
	        			    m_OXValues.remove(key);
	    	        		values = m_OXValues.values();
	    	    			keys = m_OXValues.keys();
	    	    	        itr = values.iterator();
	        			}
	        		}
        		}
        	}
        	else
        	{
        		// if OX= then all values turned on are now turned off
        		Collection values = m_OXValues.values();
    			Enumeration keys = m_OXValues.keys();
    	        Iterator itr = values.iterator();
        		while(itr.hasNext())
        		{
        			String value = (String)itr.next();
        			String key = (String)keys.nextElement();
        			if (value.trim().equals("true"))
        			{
        			    spOXvalues.add(key);
        			}
        		}
        		m_OXValues.clear();
        	}

    		if (spOXvalues.size() > 0)
    		{
    			for (int kk=0;kk<spOXvalues.size();kk++)
    			{
    		        boolean bSignalValue;
    		        Integer iPortNumber;
    		        int intVal;
    		        Double dSignalDuration = new Double(0.0);
                    String sSignalName;
                    
    			    intVal = Integer.parseInt(((String)spOXvalues.get(kk)).trim());
    			    bSignalValue = false;
    			    if (intVal > 0)
    			    {
    			    	if (m_sCurrentBlockOX.trim().equals("OX=") == false)
    			    	{
    			    		continue;
    			    	}
     			    }
    			    else
    			    {
    			        intVal *= -1;
    			    }
    			      
    			    iPortNumber = new Integer(intVal); // here intVal < 0
    			    
    			    m_OXValues.put(Integer.toString(intVal), "false");
    			    
    			    sSignalName = "OX" + iPortNumber;
		            sActivityName = "Set-" + sSignalName + " = OFF";
					
    			    Element outputAct = super.createSetIOActivity(activityListElem, sActivityName,
    			                    bSignalValue, sSignalName, iPortNumber, dSignalDuration);    				
    				if (m_OXPreOutput == true) 
    				{
    					// for preOutput case move output prior to move
                        activityListElem.removeChild(outputAct);
                        activityListElem.insertBefore(outputAct, actElem);
    				}
    			}
    		}
        }
       
		setCommentStatements(actElem, "Pre");
		
        addAttribute(actElem, "DownloadBlockFormat", "true");
        addAttribute(actElem, "CLAMP_Information1", m_sCurrentBlockClamp);
        m_sCurrentBlockClamp = "";
        if (m_sCurrentBlockCL.equals("") == false)
        {
        	addAttribute(actElem, "CLAMP_Information2", m_sCurrentBlockCL);
        	m_sCurrentBlockCL = "";
        }
        
	} // createBlockJointTarget
	
	private void createTagTargetWithMotype(String line, Element activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType eMotionType)
	{
		// MotionProfile name
		String[] spComponents = line.split("[ ,]+");
		String stagName = spComponents[1];
		
		String sTagValue = null;
		
		// look for the tag in the TRANS hashtable.
		sTagValue = (String)m_htTransVals.get(stagName);
		if(sTagValue == null)
        {
			// look for the tag in the .AUX hashtable.
            sTagValue = (String)m_htAuxVals.get(stagName);
			if(sTagValue == null)
            {
                // error
				sTagValue = "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0";
			}
		}
                
        double[] dtagLocation = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        
        sTagValue = sTagValue.trim();
        String[] spTagComponents = sTagValue.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
        String xyzoat = spTagComponents[1];
        for( int ii = 2; ii < 7; ii++)
            xyzoat += "," + spTagComponents[ii];
        
        DNBIgpOlpUploadMatrixUtils matrixUtils = new DNBIgpOlpUploadMatrixUtils();
        String xyzwpr = matrixUtils.dgXyzoatToMatrix(xyzoat);
        String [] tagPosition = xyzwpr.split("[ ,]+");
        for (int ii=0; ii<6; ii++)
        {
            double dlocation = Double.parseDouble(tagPosition[ii]);
            if (ii < 3)
                dtagLocation[ii] = dlocation/1000.0;
            else
                dtagLocation[ii] = dlocation;
        }

		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		// <Activity ActivityType="DNBRobotMotionActivity">
		// <ActivityName>RobotMotion.1</ActivityName>
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);
        Element moAttrElem = null;	
		//<MotionAttributes>
		//<MotionProfile>SPEED 50</MotionProfile>
		//<ToolProfile>Default</ToolProfile>
		//<AccuracyProfile>Default</AccuracyProfile>
		//<ObjectFrameProfile>Default</ObjectFrameProfile>
		//<MotionType>Joint</MotionType>
		//</MotionAttributes>
		
        String accuracyProfileName = sFlyBy_OFF;
        // When CP is ON, assign accuracy profile for creating move activity
        if (true == m_bContinuousPath)
            accuracyProfileName = m_sCurrentAccuracyProfileName;
        
		if (eMotionType == DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION)
		{
			moAttrElem = super.createMotionAttributes(
                                actElem,
														  m_sCurrentMotionProfileName, 
                                accuracyProfileName,
														  m_sCurrentToolProfileName, 
														  m_sCurrentObjectProfileName,
	                                                        eMotionType);
		}
		else
		{
			moAttrElem = super.createMotionAttributes(
                                actElem,
					m_sCurrentMotionProfileName, 
                                accuracyProfileName,
                    m_sCurrentToolProfileName, 
                    m_sCurrentObjectProfileName,
                                eMotionType,
                                DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS);
		}		

        m_sCurrentAccuracyProfileName = m_sPersistentAccuracyProfileName;
        m_sCurrentMotionProfileName = m_sPersistentMotionProfileName;
        
		//<Target Default="Cartesian" ViaPoint="true">
		// ...
		//<Tag>ViaPoint1</Tag>
		//</CartesianTarget>
		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);
		
		double location[] = {0.0, 0.0, 0.0};
        
		Element cartTargetElem = super.createCartesianTarget(
						targetElem,	    //Element oTargetElem, 
                                        dtagLocation,   // double [] daLocation,
						m_sCurrentConfig);	    //String sConfigName

	    Element tagElem = super.createTag(cartTargetElem, spComponents[1]);
        if (tagElem != null)
        {
			int tempcount = m_RobotTaskCounter - 1; // task was created and count incremented before move created.
			String stagGroupName = "KWTagGroup" + tempcount;
			
			//stagGroupName = mangleTagGroupName(stagGroupName);
			m_sTagGroupNames.add(stagGroupName);
			
            tagElem.setAttribute("TagGroup", stagGroupName);
            
            SetDOMTagPositionUnits("m", targetElem);
        }

		int numOfAuxDOF = m_numAuxAxes + m_numExtAxes + m_numWorkAxes; // from input parameters.
		if (numOfAuxDOF > 0)
	{
			if ((spTagComponents.length-m_numRobotAxes) > numOfAuxDOF)
			{
			    int ii, jj;
			    ArrayList [] oaJointTarget = new ArrayList[numOfAuxDOF]; // there is one array per joint
			    for (ii=0; ii< numOfAuxDOF; ii++)
                {
			         oaJointTarget[ii] = new ArrayList();
		
			         String sJointName = "Joint" + (ii + m_numRobotAxes + 1);
			         oaJointTarget[ii].add(0,sJointName);

			         Double DjointValue = new Double(spTagComponents[ii+m_numRobotAxes+1]);
			         double dblValue = DjointValue.doubleValue();

			         String axesType = "";
			         axesType = m_axisTypes[ii+m_numRobotAxes];
		
			         if (axesType.equalsIgnoreCase("Translational") )
			         {
			        	 dblValue *= .001;  
			         }
			         else
			         {
			        	 dblValue = Math.toRadians(dblValue);
			         }

			         DjointValue = Double.valueOf(String.valueOf(dblValue));
			         oaJointTarget[ii].add(1, DjointValue);

			         Integer iDOFValue = new Integer(ii + m_numRobotAxes + 1);
			         oaJointTarget[ii].add(2,iDOFValue);
		
			         if (axesType.equalsIgnoreCase("Translational") )
			         {
			        	 oaJointTarget[ii].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL);
			         }
			         else
			         {
			        	 oaJointTarget[ii].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL);
			         }             
		
			         int extAxesCount = 0;
			         int auxAxesCount = 0;
			         int workAxesCount = 0;
	                 String auxAxesType = (String)m_auxAxesTypes.get(String.valueOf(ii + m_numRobotAxes + 1));
	                 if (auxAxesType != null) {
	                     if (auxAxesType.equalsIgnoreCase("RailTrackGantry")) {
	                          extAxesCount++;
	                          oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY);                          
	                     }
	                     else if ( auxAxesType.equalsIgnoreCase("EndOfArmTooling")) {            
	                         extAxesCount++;
	                         oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING);                     
	                     }
	                     else if ( auxAxesType.equalsIgnoreCase("WorkPositioner")) {
	                         workAxesCount++;
	                         oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER);
	                     }
	                 }
	                 else {
	                     extAxesCount++;
	                     oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING);
	                 }		        
			    }
        
			    Element jointTargetElem = super.createJointTarget(
						targetElem,	    //Element oTargetElem, 
								oaJointTarget);	    //String sConfigName
			}
			else
			{
	            try
	            {
				    bw.write("WARNING: MISMATCH BETWEEN NUMBER OF AUX. AXES FOR UPLOAD ROBOT AND PROGRAM\n");
	            } // try
	            catch (IOException e)
	            {
	                e.getMessage();
	            }				
			}
		}

		setCommentStatements(actElem, "Pre");
	} // createTagTargetWithMotype

	//---------------------------------------------------------------------------
	// processLMOVEStatement
	//
	// V5: DNBRobotMotionActivity.LinearInterpolation.CartesianTarget. (TargetType = Tag, TagName = <tag_name>)
	// 
	//----------------------------------------------------------------------------
	private void processLMOVEStatement(String line, Element activityListElem)
	{
        createTagTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION);
	}

	//---------------------------------------------------------------------------
	// processCOMMENTStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processCOMMENTStatement(String line, Element activityListElem)
	{
        if (line.startsWith(";KawasakiASToolPick"))
        {
            createToolPickAction(line, activityListElem);
            return;
        }
        else
        if (line.startsWith(";KawasakiASToolDrop"))
        {
            createToolDropAction(line, activityListElem);
            return;
        }
        else
        if (line.startsWith(";KawasakiASSpotPick"))
        {
            createSpotPickAction(line, activityListElem);
            return;
        }
        else
        if (line.startsWith(";KawasakiASSpotDrop"))
        {
            createSpotDropAction(line, activityListElem);
            return;
        }
        
        String comment = line.substring(line.indexOf(';') + 1);
        if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
        {
            String [] commentNames = {"Comment"};
            String [] commentValues = {comment};
            Element actElem = super.createActivityHeader(activityListElem, comment, "Operation");
            super.createAttributeList(actElem, commentNames, commentValues);
        }
        else
        {
            m_listOfCommentNames.add("Comment" + m_commentCounter);
            m_listOfCommentValues.add(comment);
        }
        m_commentCounter++;
	}
	
	//---------------------------------------------------------------------------
	// processJMOVETRANSTStatement
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processJMOVETRANSTStatement(String line, Element activityListElem)
	{
		
		// MotionProfile name
		String[] spComponents = line.split("\\s*\\(\\s*|\\s*\\)\\s*|\\s+,\\s+|\\s+,|,\\s+|\\s+");
				
		String sTagValue = null;
		
		double dtagLocation[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        
        int nlen = spComponents.length;
        
		for(int i = 0; i < 6; i++){
            dtagLocation[i] = Double.parseDouble(spComponents[i + 2]);
            if (i<3)
            {
            	dtagLocation[i] /= 1000.00; // to meters
            }
            else
            {
            	// dtagLocation[i] = Math.toRadians(dtagLocation[i]);
            }
		}        
		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

		Element moAttrElem = super.createMotionAttributes(actElem, 
														  m_sCurrentMotionProfileName, 
														  m_sCurrentAccuracyProfileName, 
                                                          m_sCurrentToolProfileName, 
                                                          m_sCurrentObjectProfileName,
                                                          DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);


		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);
				        
		Element cartTargetElem = super.createCartesianTarget(
						targetElem,	    //Element oTargetElem, 
						dtagLocation, 
						m_sCurrentConfig);	    //String sConfigName
        
        Element frameListElement = super.getObjectFrameProfileListElement();
        
        boolean bapplyOffsetToTags = true;
        String sframeName = "ObjectFrame" + m_objectFrameCounter;
        m_objectFrameCounter++;
        
        double[] dframeValues = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        
        Element frameProfileElement = createObjectFrameProfile(frameListElement, 
                                            sframeName, 
                                            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD, 
                                            dframeValues,
                                            bapplyOffsetToTags);

		setCommentStatements(actElem, "Pre");
				
		return;
	}

	//---------------------------------------------------------------------------
	// processJMOVEJOINTStatement
	// 
    // kawasaki:
    // .JOINTS
    // ...
    // # < location_name > <j1> <j2> <j3> <j4> <j5> <j6>
    // ...
    // .END 
    // 
	// v5: All the joint targets referenced by RobotMotionActivities will be uploaded 
    // by creating �Location Name� attribute  with the value of <location_name>. 
	//----------------------------------------------------------------------------
	private void processJMOVEJOINTStatement(String line, Element activityListElem)
	{
		createJointTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);
		return;
	}

	private void createJointTargetWithMotype(String line, Element activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType eMotionType)
	{
        // MotionProfile name
		String[] spComponents = line.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
		String stagName = spComponents[1];
		
		String sTagValue = null;
		
		// look for the tag in the JOINTS hashtable.
		sTagValue = (String)m_htJointVals.get(stagName);
		if(sTagValue == null){
			// look for the tag in the .AUX hashtable.
            sTagValue = (String)m_htAuxVals.get(stagName);
			if(sTagValue == null){
                // error
				sTagValue = "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0";
			}
		}
        
        String[] scomps = sTagValue.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
        
        ArrayList sTagCompsList = new ArrayList();
        for(int i = 0; i < scomps.length; i++)
            sTagCompsList.add(scomps[i]);
        
		if (sTagCompsList.size() > 6)
        {
			sTagCompsList.remove(0); // id such as #p1.
		}
                
        int numOfDOF = m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes; // from input parameters.
        int ii, jj;
        ArrayList [] oaJointTarget = new ArrayList[numOfDOF]; // there is one array per joint
	    boolean auxError = false;
        for (ii=0; ii< numOfDOF; ii++)
        {
	    	 if (ii < scomps.length)
	    	 {
             oaJointTarget[ii] = new ArrayList();
        
             String sJointName = "Joint" + (ii + 1);
             oaJointTarget[ii].add(0,sJointName);
                
             Double DjointValue = new Double((String)sTagCompsList.get(ii));
             double dblValue = DjointValue.doubleValue();
             
             String axesType = "";
             axesType = m_axisTypes[ii];
             
             if (axesType.equalsIgnoreCase("Translational") )
             {
            	 dblValue *= .001;  
             }
             else
             {
            	 dblValue = Math.toRadians(dblValue);
             }
 
             DjointValue = Double.valueOf(String.valueOf(dblValue));
             oaJointTarget[ii].add(1, DjointValue);
             
             Integer iDOFValue = new Integer(ii + 1);
             oaJointTarget[ii].add(2,iDOFValue);
 
             if (axesType.equalsIgnoreCase("Translational") )
             {
            	 oaJointTarget[ii].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL);
             }
             else
             {
            	 oaJointTarget[ii].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL);
             }             
            
		         int extAxesCount = 0;
		         int auxAxesCount = 0;
		         int workAxesCount = 0;
	             String auxAxesType = (String)m_auxAxesTypes.get(String.valueOf(ii + 1));
                if (auxAxesType != null)
                {
                    if (auxAxesType.equalsIgnoreCase("RailTrackGantry"))
                    {
	                      extAxesCount++;
	                      oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY);                          
	                 }
                    else if ( auxAxesType.equalsIgnoreCase("EndOfArmTooling"))
                    {
	                     extAxesCount++;
	                     oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING);                     
	                 }
                    else if ( auxAxesType.equalsIgnoreCase("WorkPositioner"))
                    {
	                     workAxesCount++;
	                     oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER);
	                 }
	             }
                else
                {
	                 if (ii >= m_numRobotAxes)
	                 {
	                	 extAxesCount++;
	                	 oaJointTarget[ii].add(4,DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING);
	                 }
	             }		        
	             
             //eAuxAxisType
             //bisLocked;
	    	 }
	    	 else
	    	 {
                 auxError = true;	 
	    	 }
	    }
            
	    if (auxError == true)
	    {
	        try
	        {
			    bw.write("WARNING: MISMATCH BETWEEN NUMBER OF AUX. AXES FOR UPLOAD ROBOT AND PROGRAM\n");
	        } // try
	        catch (IOException e)
	        {
	            e.getMessage();
        }
		}
        
        String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);
		Element moAttrElem = null;

        String accuracyProfileName = sFlyBy_OFF;
        // When CP is ON, assign accuracy profile for creating move activity
        if (true == m_bContinuousPath)
            accuracyProfileName = m_sCurrentAccuracyProfileName;
        
		if (eMotionType == DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION)
		{
			moAttrElem = super.createMotionAttributes(
                                actElem, 
															m_sCurrentMotionProfileName, 
                                accuracyProfileName, 
                                                            m_sCurrentToolProfileName, 
                                                            m_sCurrentObjectProfileName,
	                                                        eMotionType);
		}
		else
		{
			moAttrElem = super.createMotionAttributes(
                                actElem, 
					m_sCurrentMotionProfileName, 
                                accuracyProfileName, 
                    m_sCurrentToolProfileName, 
                    m_sCurrentObjectProfileName,
                                eMotionType,
                                DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS);		
		}
        
        m_sCurrentAccuracyProfileName = m_sPersistentAccuracyProfileName;
        m_sCurrentMotionProfileName = m_sPersistentMotionProfileName;
        
		boolean bIsViaPoint = false;
        
        Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, bIsViaPoint);
		
        Element jointTargetElem = super.createJointTarget(
						targetElem,	    //Element oTargetElem, 
						oaJointTarget);	    //String sConfigName

                    
		setCommentStatements(actElem, "Pre");
	} // createJointTargetWithMotype

    private void processHOMEstatement(String line, Element activityListElem)
    {
        String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);
		Element moAttrElem = null;

        String accuracyProfileName = sFlyBy_OFF;
        // When CP is ON, assign accuracy profile for creating move activity
        if (true == m_bContinuousPath)
            accuracyProfileName = m_sCurrentAccuracyProfileName;
        
        moAttrElem = super.createMotionAttributes(
                                actElem, 
                                m_sCurrentMotionProfileName, 
                                accuracyProfileName, 
                                m_sCurrentToolProfileName, 
                                m_sCurrentObjectProfileName,
                                DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);
        
        m_sCurrentAccuracyProfileName = m_sPersistentAccuracyProfileName;
        m_sCurrentMotionProfileName = m_sPersistentMotionProfileName;
        
		boolean bIsViaPoint = false;
        
        Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, bIsViaPoint);
		
        String sHomeName = m_sHomeName1;
        if (line.equalsIgnoreCase("HOME2"))
            sHomeName = m_sHomeName2;
        Element jointTargetElem = super.createJointTarget(targetElem, sHomeName);

		setCommentStatements(actElem, "Pre");
	}

	//---------------------------------------------------------------------------
	// processJMOVETRANSPLUSTAGStatement
	// 
	// 
	//----------------------------------------------------------------------------
	private void processJMOVETRANSPLUSTAGStatement(String line, Element activityListElem)
	{
		// MotionProfile name
        //String[] spComponents = line.split("\\s*\\(\\s*|\\s*\\)\\s*|\\s*\\+\\s*|\\s+,\\s+|\\s+,|,\\s+|\\s+");
		String[] spComponents = line.split("\\s*\\(\\s*|\\s*\\)\\s*\\+\\s*|\\s*,\\s*|\\s+");
		String stagName = spComponents[8];
		
		String sTagValue = null;
		
		// look for the tag in the TRANS hashtable.
		sTagValue = (String)m_htTransVals.get(stagName);
		if(sTagValue == null){
			// look for the tag in the .AUX hashtable.
            sTagValue = (String)m_htAuxVals.get(stagName);
			if(sTagValue == null){
                // error
				sTagValue = "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0";
			}
		}
        
        double[] doframeLocation = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		for(int ii = 0; ii < 6; ii++)
        {
			doframeLocation[ii] = Double.parseDouble(spComponents[ii+2]); // skip JMOVE and TRANS
            if (ii<3)
                doframeLocation[ii] /= 1000.0;
		}

        double[] dtagLocation = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        
        sTagValue = sTagValue.trim();
        String[] spTagComponents = sTagValue.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
        String xyzoat = spTagComponents[1];
        for (int ii = 2; ii < 7; ii++)
            xyzoat += "," + spTagComponents[ii];
        
        DNBIgpOlpUploadMatrixUtils matrixUtils = new DNBIgpOlpUploadMatrixUtils();
        String xyzwpr = matrixUtils.dgXyzoatToMatrix(xyzoat);
        String [] tagPosition = xyzwpr.split("[ ,]+");
        for (int ii = 0; ii < 6; ii++)
        {
            double dlocation = Double.parseDouble(tagPosition[ii]);
            if (ii < 3)
            	dtagLocation[ii] = dlocation/1000.0;
            else
            	dtagLocation[ii] = dlocation;
        }

		Element frameListElement = super.getObjectFrameProfileListElement();

		boolean bapplyOffsetToTags = false;

		String frameName = "TRANS" + m_objectFrameCounter;
		m_objectFrameCounter++;
		
		Element frameProfileElement = createObjectFrameProfile(frameListElement,
                                            frameName,
                                            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE,
                                            doframeLocation,
                                            bapplyOffsetToTags);

		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

		Element moAttrElem = super.createMotionAttributes(actElem, 
														  m_sCurrentMotionProfileName, 
														  m_sCurrentAccuracyProfileName, 
                                                          m_sCurrentToolProfileName, 
                                                          frameName,
								DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);


		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);
		
		
		double location[] = {0.0, 0.0, 0.0};
        
		Element jointTargetElem = super.createCartesianTarget(
						targetElem,	    //Element oTargetElem, 
						dtagLocation, /*location*/	    //double [] daLocation, 
						m_sCurrentConfig);	    //String sConfigName

		Element tagElem = super.createTag(jointTargetElem, stagName);

		setCommentStatements(actElem, "Pre");

		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processJMOVETRANSPLUSTRANSStatement(String line, Element activityListElem)
	{
		// MotionProfile name
        String[] spComponents = line.split("\\s*\\(\\s*|\\s*\\)\\s*\\+\\s*|\\s*\\)\\s*|\\s*,\\s*|\\s+");
		ArrayList listComponents = new ArrayList();
		for(int i = 0; i < spComponents.length; i++){
			listComponents.add(spComponents[i]);
		}
		
		boolean bjmv = listComponents.contains("JMOVE");
		if(bjmv)
			listComponents.remove("JMOVE");
		
		bjmv = listComponents.contains("TRANS");
		if(bjmv)
			listComponents.remove("TRANS");
		
		bjmv = listComponents.contains("TRANS");
		if(bjmv)
			listComponents.remove("TRANS");
		
        double[] doframeLocation = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		for(int j = 0; j < 6; j++){
			doframeLocation[j] = Double.parseDouble((String)listComponents.get(j));
		}
        
		double[] dtagLocation = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		for(int k = 6; k < 12; k++){
			dtagLocation[k-6] = Double.parseDouble((String)listComponents.get(k));
			if (k < 9)
			{
				dtagLocation[k-6] /= 1000.0;
			}
			else
			{
				// dtagLocation[k-6] = Math.toRadians(dtagLocation[k-6] );
			}
		}
		
		Element frameListElement = super.getObjectFrameProfileListElement();

		boolean bapplyOffsetToTags = true;

		String frameName = "ObjectFrame" + m_objectFrameCounter;
		m_objectFrameCounter++;
		
		Element frameProfileElement = createObjectFrameProfile(frameListElement, 
                                            frameName, 
                                            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD, 
                                            doframeLocation,
                                            bapplyOffsetToTags);
       
		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
				
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

		Element moAttrElem = super.createMotionAttributes(actElem, 
														  m_sCurrentMotionProfileName, 
														  m_sCurrentAccuracyProfileName, 
                                                          m_sCurrentToolProfileName, 
                                                          frameName,
								DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);


		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);
				
		double location[] = {0.0, 0.0, 0.0};
        
		Element jointTargetElem = super.createCartesianTarget(
						targetElem,	    //Element oTargetElem, 
						dtagLocation, //location	    //double [] daLocation, 
						m_sCurrentConfig);	    //String sConfigName

		// no tag for this one.
		// Element tagElem = super.createTag(jointTargetElem, spComponents[1]);
		
		setCommentStatements(actElem, "Pre");

		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processLMOVETRANSTStatement(String line, Element activityListElem)
	{
		/*
		// MotionProfile name
		String[] spComponents = line.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
		String stagName = spComponents[1];
		
		String sTagValue = null;
		
		// look for the tag in the TRANS hashtable.
		sTagValue = (String)m_htTransVals.get(stagName);
		if(sTagValue == null){
			// look for the tag in the .AUX hashtable.
            sTagValue = (String)m_htAuxVals.get(stagName);
			if(sTagValue == null){
                // error
				sTagValue = "0.0, 0.0, 0.0, 0.0, 0.0, 0.0";
			}
		}
                
        double[] ftagLocation = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        
        sTagValue = sTagValue.trim();
        String[] spTagComponents = sTagValue.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
        for( int i = 1; i < 7; i++)
        {
            // skip leading component, its an identifier
            float flocation = Float.parseFloat(spTagComponents[i]);
            ftagLocation[i - 1] = flocation;            
        }

		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;
		
		//createMotionActivityHeader(...)<BR>
		//createMotionAttributes(...)<BR>
		//createTarget(...)<BR>
		//createCartesianTarget(...)<BR>
		//createTurnNumbers(...)<BR>
		//createTag(...)<BR>
		
		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);


		Element moAttrElem = super.createMotionAttributes(actElem, 
															m_sCurrentMotionProfileName, 
														  m_sCurrentAccuracyProfileName, 
														  m_sCurrentToolProfileName, 
														  m_sCurrentObjectProfileName,
								DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);								// eMotype


		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);
		
		
		double location[] = {0.0, 0.0, 0.0};
        
		Element jointTargetElem = super.createCartesianTarget(
						targetElem,	    //Element oTargetElem, 
						ftagLocation, 
						m_sCurrentConfig);	    //String sConfigName

		Element tagElem = super.createTag(jointTargetElem, spComponents[1]);

		setCommentStatements(actElem);
		*/

		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processLMOVEJOINTStatement(String line, Element activityListElem)
	{
		createJointTargetWithMotype(line, activityListElem, DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION);

		return;
	}
	//---------------------------------------------------------------------------
	// processLMOVETRANSPLUSTRANSStatement
	// 
	// 
	//----------------------------------------------------------------------------
	private void processLMOVETRANSPLUSTRANSStatement(String line, Element activityListElem)
	{
		// MotionProfile name
		String[] spComponents = line.split("\\s*\\(\\s*|\\s*\\)\\s*\\+\\s*|\\s*\\)\\s*|\\s*,\\s*|\\s+");
		ArrayList listComponents = new ArrayList();
		for (int i = 0; i < spComponents.length; i++)
		{
			listComponents.add(spComponents[i]);
		}

		boolean bjmv = listComponents.contains("LMOVE");
		if (bjmv)
			listComponents.remove("LMOVE");

		bjmv = listComponents.contains("TRANS");
		if (bjmv)
			listComponents.remove("TRANS");

		bjmv = listComponents.contains("TRANS");
		if (bjmv)
			listComponents.remove("TRANS");

		double[] doframeLocation = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		for (int j = 0; j < 6; j++)
		{
			doframeLocation[j] = Double.parseDouble((String)listComponents.get(j));
		}

		double[] dtagLocation = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		for (int k = 6; k < 12; k++)
		{
			dtagLocation[k - 6] = Double.parseDouble((String)listComponents.get(k));
			
            if(k < 9) // dont scale roll pitch yaw
                dtagLocation[k - 6] = dtagLocation[k - 6]/1000.0;      
            // else
               // dtagLocation[k - 6] = Math.toRadians(dtagLocation[k - 6]); 			
		}

		Element frameListElement = super.getObjectFrameProfileListElement();

		boolean bapplyOffsetToTags = true;

		String frameName = "ObjectFrame" + m_objectFrameCounter;
		m_objectFrameCounter++;

		Element frameProfileElement = createObjectFrameProfile(frameListElement,
											frameName,
											DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD,
											doframeLocation,
											bapplyOffsetToTags);

		String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;

		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

		Element moAttrElem = super.createMotionAttributes(
                                    actElem,
														  m_sCurrentMotionProfileName,
														  m_sCurrentAccuracyProfileName,
														  m_sCurrentToolProfileName,
														  frameName,
                                    DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION,
                                    DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS);

		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);

		double location[] = { 0.0, 0.0, 0.0 };

		Element jointTargetElem = super.createCartesianTarget(
						targetElem,	    //Element oTargetElem, 
						dtagLocation, //location	    //double [] daLocation, 
						m_sCurrentConfig);	    //String sConfigName

		// no tag for this one.
		// Element tagElem = super.createTag(jointTargetElem, spComponents[1]);

		setCommentStatements(actElem, "Pre");
		
		return;
	} // processLMOVETRANSPLUSTRANSStatement
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processLMOVETRANSPLUSTAGStatement(String line, Element activityListElem)
	{
		// MotionProfile name
		//String[] spComponents = line.split("\\s*\\(\\s*|\\s*\\)\\s*|\\s*\\+\\s*|\\s+,\\s+|\\s+,|,\\s+|\\s+");
		String[] spComponents = line.split("\\s*\\(\\s*|\\s*\\)\\s*\\+\\s*|\\s*,\\s*|\\s+");
		String stagName = spComponents[8];

		String sTagValue = null;

		// look for the tag in the TRANS hashtable.
		sTagValue = (String)m_htTransVals.get(stagName);
		if (sTagValue == null)
		{
			// look for the tag in the .AUX hashtable.
			sTagValue = (String)m_htAuxVals.get(stagName);
			if (sTagValue == null)
			{
				// error
				sTagValue = "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0";
			}
		}

        double[] doframeLocation = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		for(int ii = 0; ii < 6; ii++)
        {
			doframeLocation[ii] = Double.parseDouble(spComponents[ii+2]); // skip JMOVE and TRANS
            if (ii<3)
                doframeLocation[ii] /= 1000.0;
		}

        double[] dtagLocation = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

		sTagValue = sTagValue.trim();
		String[] spTagComponents = sTagValue.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
        String xyzoat = spTagComponents[1];
        for (int ii = 2; ii < 7; ii++)
            xyzoat += "," + spTagComponents[ii];
        
        DNBIgpOlpUploadMatrixUtils matrixUtils = new DNBIgpOlpUploadMatrixUtils();
        String xyzwpr = matrixUtils.dgXyzoatToMatrix(xyzoat);
        String [] tagPosition = xyzwpr.split("[ ,]+");
		for (int ii = 0; ii < 6; ii++)
		{
			// skip leading component, its an identifier
			double dlocation = Double.parseDouble(tagPosition[ii]);
            if (ii < 3) // dont scale roll pitch yaw
                dtagLocation[ii] = dlocation/1000.0;      
            else
                dtagLocation[ii] = dlocation; 			
			
		}

		Element frameListElement = super.getObjectFrameProfileListElement();

		boolean bapplyOffsetToTags = false;

		String frameName = "TRANS" + m_objectFrameCounter;
		m_objectFrameCounter++;
		
		Element frameProfileElement = createObjectFrameProfile(frameListElement,
                                            frameName,
                                            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE,
                                            doframeLocation,
                                            bapplyOffsetToTags);

        String sActivityName = "RobotMotion." + m_moveCounter;
		m_moveCounter++;

		Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

		Element moAttrElem = super.createMotionAttributes(
                                    actElem,
														  m_sCurrentMotionProfileName,
														  m_sCurrentAccuracyProfileName,
														  m_sCurrentToolProfileName,
														  frameName,
                                    DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION,
                                    DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS);

		boolean bIsViaPoint = false;
		Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, bIsViaPoint);


		double location[] = { 0.0, 0.0, 0.0 };

		Element jointTargetElem = super.createCartesianTarget(
						targetElem,	    //Element oTargetElem, 
						dtagLocation,   //double [] daLocation, 
						m_sCurrentConfig);	    //String sConfigName

		Element tagElem = super.createTag(jointTargetElem, stagName);

		setCommentStatements(actElem, "Pre");

		return;
		
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processUWRISTBELOWRIGHTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_1";
		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processUWRISTABOVERIGHTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_3";
		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processUWRISTBELOWLEFTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_5";
		return;
	}
	//---------------------------------------------------------------------------
	// 
	// 
	// 
	//----------------------------------------------------------------------------
	private void processUWRISTABOVELEFTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_7";
		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processDWRISTBELOWRIGHTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_2";
		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processDWRISTABOVERIGHTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_4";
		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processDWRISTBELOWLEFTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_6";
		return;
	}
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processDWRISTABOVELEFTYStatement(String line, Element activityListElem)
	{
		m_sCurrentConfig = "Config_8";
		return;
	}
	//---------------------------------------------------------------------------
	// getHashFromIdPlusSixtuple
	//
	// Utility function to get a hash key from a .TRANS or .AUXDATA item.
	// We expect a string like "100 tag_one 0.0 1.0 2.0 3.0 4.0 5.0" 
	// or "tag_one 0.0 1.0 2.0 3.0 4.0 5.0". We want to return 'tag_one' for
	// later use as a hash key.
	//----------------------------------------------------------------------------
	private String getHashFromIdPlusSixtuple(String IdPlusSixtuple)
	{
        IdPlusSixtuple = IdPlusSixtuple.trim();
        
		String[] stempSplit = IdPlusSixtuple.split(m_split_comma_or_space);
        								
		String shashKey = null;
		
		// some error checking?
		Pattern keyWord = Pattern.compile("^[#A-Za-z0-9]*", Pattern.CASE_INSENSITIVE);
		if (keyWord.matcher(stempSplit[0]).find() == true)
        {
			// We have a line number.
				shashKey = stempSplit[0];
		}
		else
        {
			if (m_keywords[15].matcher(stempSplit[1]).find() == true)
            {
				shashKey = stempSplit[1];
		}
		}
	
		return shashKey;
	}

    private void createToolPickAction(String line, Element activityListElem)
	{
        String[] spComponents = line.split("\\s+");
		String toolName = spComponents[1];
        String toolProfile = spComponents[2];

        String toolPickActionName = "KawasakiASToolPick." + m_toolPickCounter;
        
		Element actionElem = super.createActionHeader( activityListElem, toolPickActionName, "KawasakiASToolPick", m_mountedTool);
        
        Element toolPickActivityListElem = super.createActivityListWithinAction(actionElem);
        
        String activityName = "DNBIgpMountActivity." + m_toolPickCounter;
        Element actElem = super.createMountActivity(toolPickActivityListElem, activityName, toolName, toolProfile);
        m_toolPickCounter++;
    }

    private void createToolDropAction(String line, Element activityListElem)
	{
        String[] spComponents = line.split("\\s+");
		String toolName = spComponents[1];
        String toolProfile = spComponents[2];

        String toolDropActionName = "KawasakiASToolDrop." + m_toolDropCounter;
        
		Element actionElem = super.createActionHeader( activityListElem, toolDropActionName, "KawasakiASToolDrop", m_mountedTool);
        
        Element toolDropActivityListElem = super.createActivityListWithinAction(actionElem);
        
        String activityName = "DNBIgpUnMountActivity." + m_toolDropCounter;
        Element actElem = super.createUnMountActivity(toolDropActivityListElem, activityName, toolName, toolProfile);
        m_toolDropCounter++;
    }

    private void createSpotPickAction(String line, Element activityListElem)
	{
        String[] spComponents = line.split("\\s+");
		String gunName = spComponents[1];

        String spotPickActionName = "KawasakiASSpotPick." + m_spotPickCounter;
        
		Element actionElem = super.createActionHeader( activityListElem, spotPickActionName, "KawasakiASSpotPick", m_mountedTool);
        
        Element spotPickActivityListElem = super.createActivityListWithinAction(actionElem);
        
        String activityName = "RobotMotion." + m_gunMoveCounter++;
        super.createMotionActivityWithinAction(spotPickActivityListElem, activityName,
                Double.valueOf("0.0"), Double.valueOf("1.0"),
                DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,
                m_homeNames[1]);
        
        activityName = "GrabActivity." + String.valueOf(m_spotPickCounter);
        Element actElem = super.createGrabActivity(spotPickActivityListElem, activityName, m_mountedTool, gunName);
        m_spotPickCounter++;
    }

    private void createSpotDropAction(String line, Element activityListElem)
	{
        String[] spComponents = line.split("\\s+");
		String gunName = spComponents[1];

        String spotDropActionName = "KawasakiASSpotDrop." + m_spotDropCounter;
        
		Element actionElem = super.createActionHeader( activityListElem, spotDropActionName, "KawasakiASSpotDrop", m_mountedTool);
        
        Element spotDropActivityListElem = super.createActivityListWithinAction(actionElem);
        
        String activityName = "RobotMotion." + m_gunMoveCounter++;
        super.createMotionActivityWithinAction(spotDropActivityListElem, activityName,
                Double.valueOf("0.0"), Double.valueOf("1.0"),
                DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,
                m_homeNames[1]);
        
        activityName = "ReleaseActivity." + m_spotDropCounter;
        Element actElem = super.createReleaseActivity(spotDropActivityListElem, activityName, gunName);
        m_spotDropCounter++;
    }
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void SetDOMMotionProfileElementValue(String sProfileName, String sElementName, double dVal)
	{
		// Set the acceleration on the current motion profile and 'reuse' it. 
		Element motionProfileListElement = super.getMotionProfileListElement();

		// public NodeList getElementsByTagName(String name)
		NodeList motionProfileNodeList = motionProfileListElement.getElementsByTagName("MotionProfile");
		int nlistlength = motionProfileNodeList.getLength(); // expect length == 1
		if (nlistlength > 0)
		{
			for(int i = 0; i < nlistlength; i++)
			{
				Element currentMotionProfileElement = (Element)motionProfileNodeList.item(i);
				NodeList nameNodeList = currentMotionProfileElement.getElementsByTagName("Name");
				int nnamelength = nameNodeList.getLength();
				if (nnamelength > 0)
                {
					// expect there is one name
					Node nameNode = nameNodeList.item(0);
					if (nameNode != null)
                    {
                         Node nameNodeChild = nameNode.getFirstChild();
                         String ChildName = nameNodeChild.getNodeValue();
                                               
                        if (ChildName != null && ChildName.equals(sProfileName))
                        {
                            NodeList elementNodeList = currentMotionProfileElement.getElementsByTagName(sElementName);
                            int nelementlength = elementNodeList.getLength();
                            if (nlistlength > 0)
                            {
                                Node element = elementNodeList.item(0);
                                NamedNodeMap attribMap = element.getAttributes();

                                Node theattrib = attribMap.getNamedItem("Value");

                                String sValue = Double.toString(dVal);
                                theattrib.setNodeValue(sValue);
                            }
                        }
					}
                }				
			}
        }
    }
	
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void SetDOMTagPositionUnits(String sUnits, Element targetElem)
	{
		// public NodeList getElementsByTagName(String name)
		NodeList tagTargetNodeList = targetElem.getElementsByTagName("CartesianTarget");
		int nlistlength = tagTargetNodeList.getLength(); // expect length == 1
		
		for(int i = 0; i < nlistlength; i++)
		{
			Element currentTagTargetNodeListElement = (Element)tagTargetNodeList.item(i);
			if(currentTagTargetNodeListElement != null){
				NodeList tagPositionNodeList = targetElem.getElementsByTagName("Position");
				int ntagposlen = tagPositionNodeList.getLength();
				
				for(int j =0; j < ntagposlen; j++){
					Element currentTagPositionNodeListElement = (Element)tagPositionNodeList.item(i);
					currentTagPositionNodeListElement.setAttribute("Units", sUnits);
				}
				
			}
		}
    }
	//---------------------------------------------------------------------------
	//
	// 
	// 
	//----------------------------------------------------------------------------
	private void processToolFile(File toolf)
	{
		Element toolProfileElem;
		Element tpfList = super.getToolProfileListElement();
		BufferedReader tool;
		String line, toolName;
		String[] pos;
		int toolNum, ii;
		double[] toolValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

		try
		{
			tool = new BufferedReader(new FileReader(toolf));
			line = tool.readLine(); // line <//TOOL 0>
			while (line != null)
			{
				toolNum = Integer.parseInt(line.substring(7).trim());
				toolName = "TOOL " + toolNum;
				line = tool.readLine(); // line <///NAME TL1>
				if (line.length() > 8)
				{
					toolName = line.substring(8).trim();
					if (toolName.length() == 0)
						toolName = "TOOL " + toolNum;
				}

				if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
					m_toolProfileNames[toolNum] = toolName;
				else
					if (m_toolProfileNames[toolNum] == null)
						m_toolProfileNames[toolNum] = toolName;

				line = tool.readLine(); // line <x,y,z,w,p,r>
				pos = line.split(",");

				for (ii = 0; ii < 3; ii++)
					toolValues[ii] = Double.parseDouble(pos[ii].trim()) / 1000;
				for (ii = 3; ii < 6; ii++)
					toolValues[ii] = Double.parseDouble(pos[ii].trim());

				toolProfileElem = super.createToolProfile(tpfList, toolName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, null, null, null);

				line = tool.readLine(); // line <centroid>
				line = tool.readLine(); // line <mass>
				line = tool.readLine(); // line <//TOOL ?> or <Inertia>
				if (line != null)
				{
					if (line.startsWith("//TOOL "))
					{
						// next tool
					}
					else
					{
						line = tool.readLine(); // line <reserved>
						line = tool.readLine(); // line <//TOOL ?>
					}
				}
			}
		}
		catch (IOException e)
		{
			try
			{
				bw.write("ERROR: " + e.getMessage() + "\n");
			}
			catch (IOException bwe)
			{
			}
		}
	} // end of processToolFile

	private int getTagFromAuxOrTrans(String stagName, double[] dTagLocation)
	{
		// initialize to something.
        for(int i = 0; i < 6; i++){
            dTagLocation[i] = 0.0;
        }

		// look for the tag in the TRANS hashtable.
		String sTagValue = (String)m_htTransVals.get(stagName);
		if (sTagValue == null)
		{
			// look for the tag in the .AUX hashtable.
			sTagValue = (String)m_htAuxVals.get(stagName);
			if (sTagValue == null)
			{
				return -1; // error
			}
		}
		
		sTagValue = sTagValue.trim();
		String[] spTagComponents = sTagValue.split("\\s+,\\s+|\\s+,|,\\s+|\\s+");
		for (int i = 1; i < 7; i++)
		{
			// skip leading component, its an identifier
			double dlocation = Double.parseDouble(spTagComponents[i]);
            if(i < 4) // not the pitch roll yaw
                dTagLocation[i - 1] = dlocation/1000.0;  
            else
                dTagLocation[i - 1] = dlocation; 

		}

           return 0;
	}

	private void processParameters(String[] parameters)
	{
        // Param 0 = Robot Program File
        // Param 1 = XML Output File
        // Param 2 = Parameters
        // Param 3 = Last Profile Numbers
        String [] profileNumbers = parameters[3].split("[\\t]+");
        m_motionProfNum = Integer.parseInt(profileNumbers[0]);
        m_accProfNum    = Integer.parseInt(profileNumbers[1]);
        m_toolProfNum   = Integer.parseInt(profileNumbers[2]);
		// Param 4 Axis Types
        // Param 5 Number of Axes
        // Param 6 Mounted Tool
        m_mountedTool = parameters[6];
        mountedTools = new ArrayList(1);
        mountedTools.add(0, m_mountedTool);
        // Param 7 Tool Profile Names
        String [] toolProfileNames = parameters[6].split("[\\t]+");
        // Param 8 Upload Log File Name
        // Param 9 Home Names
        m_homeNames = parameters[9].split("[\\t]+");
        // Param 10 Schema Loc
        // Param 11 Object Profile Names
        // Param 12 Tab Separated Program File Names
        if (parameters.length > 12)
        {
        	m_multipleRobotPrograms = parameters[12].split("[\\t]+");
        }
       // ? m_motionProfNum = Integer.parseInt(profileNumbers[0]);
       // ? m_accProfNum    = Integer.parseInt(profileNumbers[1]);
       // ? m_toolProfNum   = Integer.parseInt(profileNumbers[2]);

        //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1
        m_numRobotAxes = super.getNumberOfRobotAxes();
        m_numAuxAxes   = super.getNumberOfRailAuxiliaryAxes();
        m_numExtAxes   = super.getNumberOfToolAuxiliaryAxes();
        m_numWorkAxes  = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();

        m_bcAxisMapping = new int[m_numAuxAxes];
        m_ecAxisMapping = new int[m_numExtAxes + m_numWorkAxes];
        m_resolution    = new double[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];
        m_zeroOffset    = new double[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];

        m_currentTagPoint = new String("");
        m_currentToolNumber = 0;

        //m_keywords = new Pattern [300];
        m_axisTypes = new String[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];
        m_axisTypes = super.getAxisTypes();
        m_listOfRobotTargets   = new ArrayList(5);
        m_listOfAuxTargets     = new ArrayList(2);
        m_listOfExtTargets     = new ArrayList(2);
        m_listOfSpeedValues    = new ArrayList(5);
        m_listOfAccuracyValues = new ArrayList(3);
        m_targetTypes          = new Hashtable();
        m_targetTools          = new Hashtable();
        m_tagNames             = new Hashtable();
        m_toolNumberMapping    = new Hashtable();

        p_listOfRobotTargets = new ArrayList(5);
        p_listOfAuxTargets   = new ArrayList(2);
        p_listOfExtTargets   = new ArrayList(2);
        p_targetTypes        = new Hashtable();
        p_targetTools        = new Hashtable();
        p_tagNames           = new Hashtable();
        
        
        programCallNames = new ArrayList(1);
        commentLines     = new ArrayList(2);
        inputNameNum     = new ArrayList(1);
        outputNameNum    = new ArrayList(1);
		m_sProgramNames  = new ArrayList(5);
		m_sTagGroupNames = new ArrayList(5);
		
		
        Enumeration parameterKeys = m_parameterData.keys();
        Enumeration parameterValues = m_parameterData.elements();

        while(parameterKeys.hasMoreElements())
        {
            String parameterName = parameterKeys.nextElement().toString();
            String parameterValue = parameterValues.nextElement().toString();

          //  System.out.println("PARAMETER NAME: " + parameterName + " PARAMETER VALUE: " + parameterValue + "\n");
            int index = parameterName.indexOf('.');
            String testString = parameterName.substring(0, index + 1);

            try
            {
                if (parameterName.equalsIgnoreCase("WorldCoords"))
                {
                     if (parameterValue.equalsIgnoreCase("true") || parameterValue.equals("1"))
                     {
                         m_worldCoords = 1;
                     }
                 }
                 else if (parameterName.equalsIgnoreCase("Railgroup")) {
                     String [] auxAxesNumbers = parameterValue.split(";");
                     m_auxAxesTypes.clear();
                     if (auxAxesNumbers.length > 0)
                     {
                         for (int kk=0;kk<auxAxesNumbers.length;kk++) {
                             m_auxAxesTypes.put(auxAxesNumbers[kk], "RailTrackGantry");
                         }
                     }
                     else
                     {
                         m_auxAxesTypes.put(parameterValue, "RailTrackGantry");
                     }
                 }
                 else if (parameterName.equalsIgnoreCase("Toolgroup")) {
                     String [] auxAxesNumbers = parameterValue.split(";");
                     m_auxAxesTypes.clear();
                     if (auxAxesNumbers.length > 0)
                     {
                         for (int kk=0;kk<auxAxesNumbers.length;kk++)
                             m_auxAxesTypes.put(auxAxesNumbers[kk], "EndOfArmTooling");                             
                     }
                     else
                     {
                         m_auxAxesTypes.put(parameterValue, "EndOfArmTooling");  
                     }
               }
               else if (parameterName.equalsIgnoreCase("Workgroup")) {
                   String [] auxAxesNumbers = parameterValue.split(";");
                   m_auxAxesTypes.clear();
                   if (auxAxesNumbers.length > 0)
                   {                        
                       for (int kk=0;kk<auxAxesNumbers.length;kk++)
                           m_auxAxesTypes.put(auxAxesNumbers[kk], "WorkPositioner");
                   }
                   else
                   {
                       m_auxAxesTypes.put(parameterValue, "WorkPositioner");  
                   }                            
               }
                else if (parameterName.equalsIgnoreCase("ProgramFileEncoding"))
                {
                   prgFileEncoding = parameterValue;
               }
                else if (parameterName.equalsIgnoreCase("OLPStyle"))
                {
                   m_OLPStyle = parameterValue;
               }                    
                else if (parameterName.equalsIgnoreCase("CommentWaitSignal"))
                {
                     if (parameterValue.equalsIgnoreCase("false") == true)
                     {
                        m_commentWaitSignal = false;
                     }
              }              
                else if (parameterName.equalsIgnoreCase("HOME2"))
                {
                    m_sHomeName2 = parameterValue;
           }
                else if (parameterName.equalsIgnoreCase("HOME"))
                {
                    m_sHomeName1 = parameterValue;
                }
                else if (parameterName.startsWith("Config"))
                {
                    m_cfgmap[Integer.parseInt(parameterName.substring(parameterName.length()-1))] = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("IOName"))
                {
                    if (parameterValue.endsWith("[]"))
                        ioName = parameterValue.substring(0, parameterValue.lastIndexOf(']'));
                    else
                        ioName = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("DownloadBlockFormat"))
                {
                    if (parameterValue.equalsIgnoreCase("false") == true)
                    {
                       m_downloadBlockFormat = false;
                    }                	
                    else
                    {
                    	m_downloadBlockFormat = true;
                    }
                } 
                else if (parameterName.equalsIgnoreCase("SpeedAbsTime"))
                {
                    if (parameterValue.equalsIgnoreCase("false") == true)
                    {
                       m_speedAbsTime = false;
                    }                	
                    else
                    {
                    	m_speedAbsTime = true;
                    }
                } 
                else if (parameterName.equalsIgnoreCase("OXWXNegative"))
                {
                    if (parameterValue.equalsIgnoreCase("false") == true)
                    {
                       m_OXWXNegative = false;
                    }                	
                    else
                    {
                    	m_OXWXNegative = true;
                    }
                }                
                else if (parameterName.equalsIgnoreCase("OX_PreOutput"))
                {
                    if (parameterValue.equalsIgnoreCase("false") == true)
                    {
                       m_OXPreOutput = false;
                    }                	
                    else
                    {
                    	m_OXPreOutput = true;
                    }
                } 
                else if (parameterName.equalsIgnoreCase("NumberOfClamp"))
                {
                    m_numberOfClamp = Integer.parseInt(parameterValue);
                }
                else if (parameterName.equalsIgnoreCase("EndOfArmToolType"))
                {
                    m_endOfArmToolType = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("Controller"))
                {
                    m_controller = parameterValue;
                }               
                else if (parameterName.equalsIgnoreCase("AUXDATA_SPEED"))
                {
                    m_auxdataSpeed = parameterValue;
                }               
                else if (parameterName.equalsIgnoreCase("AUXDATA_ACCU"))
                {
                    m_auxdataAccu = parameterValue;
                }               
                else if (parameterName.equalsIgnoreCase("AUXDATA_TIMER"))
                {
                    m_auxdataTimer = parameterValue;
                }                         
            }

            finally
            {
               continue;
           } 
       }		
	}
		
    // CALL WELDnumber
    public void processSpotWeldStatement(String line, Element activityListElem)
    {
        int len = new String("CALL WELD").length();
        String spotKey = line.substring(len);
        kwSpotWeldObj kwSpot = (kwSpotWeldObj)m_htSpotProfiles.get(spotKey);
        String [] spotAttributeNames = {"Weld Schedule Number",
                                        "Weld Schedule Start Signal Number",
                                        "Number of Weld Schedule Signals",
                                        "Pulse Signal Number",
                                        "Wait Signal Number"};
        String [] spotAttributeValues = {"1", "17", "5", "23", "1018"};
        
        if (null != kwSpot)
        {
            spotAttributeValues[0] = kwSpot._weldShedule;
            spotAttributeValues[1] = kwSpot._startSignal;
            spotAttributeValues[2] = kwSpot._numberSignal;
            spotAttributeValues[3] = kwSpot._pulseSignal;
            spotAttributeValues[4] = kwSpot._waitSignal;
	    }
        
        String actionName = "KawasakiASSpotWeld." + m_spotWeldCounter++;
        Element actionElem = super.createActionHeader(activityListElem, actionName, "KawasakiASSpotWeld", m_mountedTool);
        super.createAttributeList(actionElem, spotAttributeNames, spotAttributeValues);
        
        Element spotActivityListElem = super.createActivityListWithinAction(actionElem);
        
        String activityName = "RobotMotion." + m_gunMoveCounter++;
        super.createMotionActivityWithinAction(spotActivityListElem, activityName,
                Double.valueOf("0.0"), Double.valueOf("1.0"),
                DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,
                m_homeNames[0]);
        
        String delayActName = "DelayActivity." + m_delayCounter++;
        super.createDelayActivity(spotActivityListElem, delayActName,
                Double.valueOf("0.0"), Double.valueOf("1.5"));
        
        activityName = "RobotMotion." + m_gunMoveCounter++;
        super.createMotionActivityWithinAction(spotActivityListElem, activityName,
                Double.valueOf("0.0"), Double.valueOf("1.0"),
                DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,
                m_homeNames[1]);
    }

    public class kwSpotWeldObj extends java.lang.Object {
        private String _weldShedule = "1";
        private String _startSignal = "1";
        private String _numberSignal = "1";
        private String _pulseSignal = "1";
        private String _waitSignal = "1";

        public kwSpotWeldObj() {}

        public kwSpotWeldObj(String [] weldparameter) {
            _weldShedule = weldparameter[0];
            _startSignal = weldparameter[1];
            _numberSignal = weldparameter[2];
            _pulseSignal = weldparameter[3];
            _waitSignal = weldparameter[4];
        }
    }
} // class KawasakiUploader extends DNBIgpOlpUploadBaseClass

	/*

				// Set the acceleration on the current motion profile and 'reuse' it. 
				Element motionProfileListElement = super.getMotionProfileListElement();

				// public NodeList getElementsByTagName(String name)
				NodeList motionProfileNodeList = motionProfileListElement.getElementsByTagName("MotionProfile");
				int nlistlength = motionProfileNodeList.getLength(); // expect length == 1
				if (nlistlength > 0)
				{
					Element currentMotionProfileElement = (Element)motionProfileNodeList.item(0);

					NodeList accelNodeList = currentMotionProfileElement.getElementsByTagName("Accel");
					nlistlength = accelNodeList.getLength();
					if (nlistlength > 0)
					{
						Node accelElement = accelNodeList.item(0);
						NamedNodeMap attribMap = accelElement.getAttributes();

						Node accelattrib = attribMap.getNamedItem("Value");


						accelattrib.setNodeValue(sSpeed);
					}

				}
				*/

