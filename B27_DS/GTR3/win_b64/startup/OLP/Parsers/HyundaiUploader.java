// --------------------------------------------------------
// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// HyundaiUploader.java
// 
//===================================================================
//// Usage notes:
//===================================================================
//  May 2007  Creation   DLY
//===================================================================
//DOM and Parser classes
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;

// environment variable class
import java.util.Properties;

// set CLASSPATH=D:\WRKSPS\R15WS\OLPWS\BSS_OLPR15Prj\DNBIgpOlpJavaBase\DNBIgpOlpUploaderTools.mj\src;%CLASSPATH%
import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class HyundaiUploader extends DNBIgpOlpUploadBaseClass 
{
    // TODO - 
    private static boolean bDebugFlag = false;
    private static boolean firstCircularMove = true;
    private static FileWriter fw;
    //Constants
    private static final int JOINT = 1;
    private static final double ZERO_TOL = .001;
    private static final int CARTESIAN = 2;
    private static final String VERSION = "Delmia Corp. Hyundai Uploader Version 5 Release 19 SP3.\n";
    private static final String COPYRIGHT = "Copyright Delmia Corp. 1986-2007, All Rights Reserved.\n\n";

    //Member primitive variables
    private Element m_toolProfileListElement;
    private Element m_motionProfileListElement;
    private Element m_accuracyProfileListElement;
    private Element m_objectProfileListElement;
    private boolean m_taskNameSet;
    private int m_logicProgramInputCount;
    private int m_logicProgramOutputCount;
    private int m_jobNum;
    private int m_numLabels;
    private int m_AccuracyType = 1;	// Speed
    private int m_worldCoords = 0;
    private double m_targetYaw;
    private double m_targetPitch;
    private double m_targetRoll;
    private int m_targetType;
    private int m_targetNumber;
    private int m_targetTurnValue;
    private String m_intAxes ="False";
    private int m_numCStatements;
    private int m_numRobotAxes;
    private int m_numAuxAxes;
    private int m_numExtAxes;
    private int m_numWorkAxes;
    private int m_auxAxesCount;
    private int m_extAxesCount;
    private int m_workAxesCount;
    private int m_auxAxesStart;
    private int m_extAxesStart;
    private int m_workAxesStart;    
    private int m_procCount;
    private int m_extAxesGroupNumber;
    private int m_auxAxesGroupNumber;
    private int m_workAxesGroupNumber;        
    private int m_delayCounter;
    private int m_callCounter;
    private int m_calledProgramCounter;
    private int m_spotCounter;
    private int m_backupCounter;
    private int m_macroCounter;
    private int m_releaseCounter ;
    private int m_grabCounter;
    private int m_moveCounter;
    private int m_nIncr;
    private int m_gunmoveCounter;
    private int m_mountCounter;
    private int m_unmountCounter;
    private int m_enterZoneCounter;
    private int m_clearZoneCounter;
    private int m_accProfNum;
    private int m_motionProfNum;
    private int m_toolProfNum;
    private int m_objectProfNum;
    private String m_logicProgram;
    private boolean m_inLogicProgram;
    private int m_logicProgramJobCount;
    private int m_logicProgramExpressionCount;
    private Element m_logicProgramElement;
    private Element m_logicProgramJobElement;
    private Element m_logicProgramJobsElement;
    private boolean m_createJointTarget;
    private boolean m_moduleFound;
    private static BufferedWriter bw;
    //private boolean m_arconcreated;
    //private boolean m_arcoffcreated;
    //private boolean m_arcweavecreated;
    private int lastTargetNumber = 0;
    private String ProgName="HYUNDAIUPLOAD";
    private boolean m_swapRollYaw;
    private double  m_axis3Adjust;
    private boolean m_joint3Linked;
    private String m_cTagPrefix;
    private String m_jTagPrefix;
    private boolean m_commWaitSig;
    private double [] m_jointHomes;
    private double [] m_encoderOffsets;
    private double [] m_encoderScales;
    private double m_BCoR2;
    private double m_R1CoR2;
    private double m_R1CoB;
    
    // member variables related to Pose File
    private String m_sControllerName;
    private String m_sRobotModelName;
    private String m_sProgName;
    private String m_schemaPath;
    private int m_nControllerCode;
    private int m_nPosNumbers;
    private int m_nNumberOfAxes;

    //Member objects
    private Document m_xmlDoc;
    private String m_currentTagPoint;
    private String m_currentToolNumber;
    private String m_currentObjectNumber;
    private String m_pickHome;
    private String m_dropHome;
    private String m_grabPart;
    private String m_partGrabbed;
    private String m_mountedToolName;
    private String m_prevAccuracyValue;
    private boolean railAxisIsJoint1;
    private String [] m_axisTypes;
    private String [] m_commentNames;
    private String [] m_commentValues;
    private String prgFileEncoding = "US-ASCII";
    private String uploadStyle = "General";
    private Element m_currentElement;
    private int m_commentCount;
    private Pattern [] m_keywords;
    private ArrayList m_listOfRobotTargets;
    private ArrayList m_listOfAuxTargets;
    private ArrayList m_listOfExtTargets;
    private ArrayList m_listOfWorkTargets;
    private ArrayList m_listOfSpeedValues;
    private ArrayList m_listOfAccuracyValues;
    private ArrayList m_listOfCalledPrograms;
    private ArrayList m_listofHomeNames;
    private ArrayList m_listOfPoseValues;
    private Hashtable m_auxAxesTypes;
    private Hashtable m_targetTypes;
    private Hashtable m_targetNumbers;
    private Hashtable m_weldScheduleHomes;
    private Hashtable m_weldScheduleDelays;
    private Hashtable m_macroCommands;
    private Hashtable m_backupHomes;
    private Hashtable m_tagNames;
    private Hashtable m_procNameList;
    private Hashtable m_toolNumberMapping;
    private Hashtable m_objectNumberMapping;
    private Hashtable m_parameterData;
    private Hashtable m_commentLines;
    private String    m_OLP_HONDA_UPLOAD;
    private String    m_OLP_REMOVE_EXISTING_PROFILES;

    public HyundaiUploader(String [] parameters, String [] profileNumbers, 
                            String[] toolProfileNames, String[] homeNames, 
                            String[] objectProfileNames) 
                            throws SAXException, 
                            ParserConfigurationException, 
                            TransformerConfigurationException, 
                            NumberFormatException
    {
        super(parameters);

        m_toolProfileListElement = null;
        m_accuracyProfileListElement = null;
        m_motionProfileListElement = null;
        m_objectProfileListElement = null;
        m_targetType = CARTESIAN;
        m_targetTurnValue = 0;
        m_targetYaw = 0.0;
        m_targetPitch = 0.0;
        m_targetRoll = 0.0;
        m_targetNumber = 0;
        m_numCStatements = 0;
        m_delayCounter = 1;
        m_callCounter = 1;
        m_calledProgramCounter = 0;
        m_moveCounter = 1;
        m_nIncr = 0;
        m_spotCounter = 1;
        m_backupCounter = 1;
        m_procCount = 0;
        m_macroCounter = 1;
        m_grabCounter = 1;
        m_releaseCounter = 1;
        m_mountCounter = 1;
        m_unmountCounter = 1;
        m_enterZoneCounter = 1;
        m_clearZoneCounter = 1;
        m_numRobotAxes = super.getNumberOfRobotAxes();
        m_numAuxAxes = super.getNumberOfRailAuxiliaryAxes();
        m_numExtAxes = super.getNumberOfToolAuxiliaryAxes();
        m_numWorkAxes = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();
        m_auxAxesCount = 0;
        m_extAxesCount = 0;
        m_workAxesCount = 0;
        m_auxAxesStart = 1;
        m_extAxesStart = 1;
        m_workAxesStart = 1;             
        m_commentCount = 0;
        m_moduleFound = false;
        m_auxAxesGroupNumber = 1;
        m_extAxesGroupNumber = 2;
        m_workAxesGroupNumber = 0;
        m_createJointTarget = false;
        m_logicProgram = "Program1";
        m_inLogicProgram = false;
        m_numLabels = 0;
        m_taskNameSet = false;
        m_logicProgramInputCount = 0;
        m_logicProgramOutputCount = 0;
        m_logicProgramJobCount = 1;
        m_logicProgramExpressionCount = 0;
        m_logicProgramElement = null;
        m_logicProgramJobElement = null;
        m_logicProgramJobsElement = null;
        m_swapRollYaw = false;
        m_axis3Adjust = 90.0;
        m_joint3Linked = false;
        m_cTagPrefix = "";
        m_jTagPrefix = "";
        m_commWaitSig = true;

        m_taskNameSet = false;
        m_logicProgramInputCount = 1;
        m_logicProgramOutputCount = 1;
        m_jobNum = 1;

        //m_arconcreated = false;
        //m_arcoffcreated = false;
        //m_arcweavecreated = false;

        m_motionProfNum = Integer.parseInt(profileNumbers[0]);
        m_accProfNum = Integer.parseInt(profileNumbers[1]);
        m_toolProfNum = Integer.parseInt(profileNumbers[2]);


        //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1

        m_pickHome = new String("");
        m_dropHome = new String("");
        m_grabPart = new String("");
        m_partGrabbed = new String("");
        m_mountedToolName = new String("");
        m_prevAccuracyValue = new String("");
        railAxisIsJoint1 = false;

        m_currentTagPoint = new String("");
        m_currentToolNumber = new String("0");
        m_currentObjectNumber = new String("0");

        m_currentElement = null;
        m_keywords = new Pattern [10];
        m_axisTypes = new String [m_numRobotAxes+m_numExtAxes+m_numAuxAxes+m_numWorkAxes];
        m_axisTypes = super.getAxisTypes();
        m_listOfRobotTargets = new ArrayList(5);
        m_listOfAuxTargets = new ArrayList(2);
        m_listOfCalledPrograms = new ArrayList(1);
        m_listOfExtTargets = new ArrayList(2);
        m_listOfWorkTargets = new ArrayList(2);
        m_listOfSpeedValues = new ArrayList(5);
        m_listOfAccuracyValues = new ArrayList(3);
        m_listOfPoseValues = new ArrayList(6);
        m_auxAxesTypes = new Hashtable();
        m_targetTypes = new Hashtable();
        m_targetNumbers = new Hashtable();
        m_tagNames = new Hashtable();
        m_toolNumberMapping = new Hashtable();
        m_objectNumberMapping = new Hashtable();
        m_weldScheduleHomes = new Hashtable();
        m_weldScheduleDelays = new Hashtable();
        m_macroCommands = new Hashtable();
        m_procNameList = new Hashtable();
        m_backupHomes = new Hashtable();
        m_parameterData =  super.getParameterData();
        m_listofHomeNames = new ArrayList(2);
        m_commentLines = new Hashtable();
        m_commentNames = new String [1000];
        m_commentValues = new String [1000];
        clearComments();
        m_xmlDoc = super.getDOMDocument();     
        m_toolProfileListElement = super.getToolProfileListElement();
        m_motionProfileListElement = super.getMotionProfileListElement();
        m_accuracyProfileListElement = super.getAccuracyProfileListElement();
        m_objectProfileListElement = super.getObjectFrameProfileListElement();            

        m_jointHomes = new double [12];
        m_encoderOffsets = new double [12];
        m_encoderScales = new double [12];
        
        m_BCoR2 = .00781250;
        m_R1CoR2 = -.0133;
        m_R1CoB = .0134;
        
        for (int jj = 0; jj < 12; jj++)
        {
            m_jointHomes[jj] = 0.0;
            int decInt = Integer.parseInt("400000",16);
            m_encoderOffsets[jj] = decInt;
            m_encoderScales[jj] = .000003985845;
        }
        m_jointHomes[1] = 90.0;
        m_encoderScales[0] = -.000004006666;
        m_encoderScales[1] = .000003985845;
        m_encoderScales[2] = -.000003985845;
        m_encoderScales[3] = -.000003255179;
        m_encoderScales[4] = .000003077031;
        m_encoderScales[5] = -.000005276941;
        
        for (int jj = 0; jj < toolProfileNames.length; jj++) 
        {
            m_toolNumberMapping.put("T="+String.valueOf(jj), toolProfileNames[jj]);
        }

        for (int jj = 0; jj < objectProfileNames.length; jj++) 
        {
            m_objectNumberMapping.put(String.valueOf(jj), objectProfileNames[jj]);
        }            

        for (int jj=0; jj < homeNames.length; jj++ ) 
        {
                m_listofHomeNames.add(jj, homeNames[jj]);
        }

        Enumeration parameterKeys = m_parameterData.keys();
        Enumeration parameterValues = m_parameterData.elements();

        while(parameterKeys.hasMoreElements()) 
        {
            String parameterName = parameterKeys.nextElement().toString();			 
            String parameterValue = parameterValues.nextElement().toString();

            int index = parameterName.indexOf('.');
            String testString = parameterName.substring(0, index + 1);
			
            try 
            {
                if (testString.equalsIgnoreCase("ToolProfile.")) 
                {
                    String profileName = parameterName.substring(index + 1);
                    m_toolNumberMapping.put("T="+profileName, parameterValue);
                }
                else if (testString.equalsIgnoreCase("ObjectProfile.")) 
                {
                    String profileName = parameterName.substring(index + 1);
                    m_objectNumberMapping.put(profileName, parameterValue);
                }                     
                else if (testString.equalsIgnoreCase("WeldHome.")) 
                {
                    int homeIndex = Integer.parseInt(parameterName.substring(index + 1));
                    if (homeIndex < 1)
                        homeIndex = 1;

                    if (homeIndex > homeNames.length)
                        homeIndex = homeNames.length;

                    String [] homeComponents = parameterValue.split(";");

                    for (int kk = 0; kk < homeComponents.length; kk++)
                    {
                        m_weldScheduleHomes.put(homeComponents[kk], homeNames[homeIndex-1]);
                    }
                }   
                else if (testString.equalsIgnoreCase("OpenHome.")) 
                {
                    int homeIndex = Integer.parseInt(parameterName.substring(index + 1));
                    if (homeIndex < 1)
                        homeIndex = 1;

                    if (homeIndex > homeNames.length)
                        homeIndex = homeNames.length;

                    String [] homeComponents = parameterValue.split(";");

                    for (int kk = 0; kk < homeComponents.length; kk++)
                        m_backupHomes.put(homeComponents[kk], homeNames[homeIndex-1]); 
                }
                else if (testString.equalsIgnoreCase("WeldDelay.")) 
                {
                    double delayTime = Double.valueOf(parameterName.substring(index + 1)).doubleValue() * 0.001;

                    String [] delayComponents = parameterValue.split(";");

                    for (int kk=0;kk<delayComponents.length;kk++)
                        m_weldScheduleDelays.put(delayComponents[kk], String.valueOf(delayTime));
                }
                else if (testString.equalsIgnoreCase("Macro.")) 
                {
                    String macroAction = parameterName.substring(index+1);
                    String [] macroComponents = parameterValue.split(";");
                    for (int kk=0;kk<macroComponents.length;kk++)
                        m_macroCommands.put(macroComponents[kk], macroAction);                          
                }
                else if (parameterName.equalsIgnoreCase("PickHome")) 
                {
                    m_pickHome = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("DropHome")) 
                {
                    m_dropHome = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("GrabPart")) 
                {
                    m_grabPart = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("PartGrabbed")) 
                {
                    m_partGrabbed = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("MountedToolName")) 
                {
                    m_mountedToolName = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("WorldCoords")) 
                {
                    m_worldCoords = Integer.valueOf(parameterValue).intValue();
                }
                else if (parameterName.equalsIgnoreCase("Railgroup")) 
                {
                    String [] auxAxesNumbers = parameterValue.split(";");
                    for (int kk=0;kk<auxAxesNumbers.length;kk++) 
                    {
                        m_auxAxesTypes.put(auxAxesNumbers[kk], "RailTrackGantry");
                    }
                }
                else if (parameterName.equalsIgnoreCase("Toolgroup")) 
                {
                    String [] auxAxesNumbers = parameterValue.split(";");
                    for (int kk=0;kk<auxAxesNumbers.length;kk++)
                        m_auxAxesTypes.put(auxAxesNumbers[kk], "EndOfArmTooling");                             
                }
                else if (parameterName.equalsIgnoreCase("Workgroup")) 
                {
                    String [] auxAxesNumbers = parameterValue.split(";");
                    for (int kk=0;kk<auxAxesNumbers.length;kk++)
                        m_auxAxesTypes.put(auxAxesNumbers[kk], "WorkPositioner");   
                }
                else if (parameterName.equalsIgnoreCase("Axis3Adjust"))
                {
                    Double tmpVar = new Double(parameterValue);

                    if (tmpVar.isNaN())
                        m_axis3Adjust = 90.0; // Default value
                    else
                        m_axis3Adjust = Double.parseDouble(parameterValue);
                }
                else if (parameterName.equalsIgnoreCase("Joint3Linked"))
                {
                    m_joint3Linked = new Boolean(parameterValue).booleanValue();
                }
                else if (parameterName.equalsIgnoreCase("LogicProgram"))
                {
                    m_logicProgram = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("CommentWaitSignal"))
                {
                    m_commWaitSig = new Boolean(parameterValue).booleanValue();
                }
                else if (parameterName.equalsIgnoreCase("RailAxisIsJoint1"))
                {
                    if (parameterValue.equalsIgnoreCase("TRUE"))
                        railAxisIsJoint1 = true;
                }
                else if (parameterName.equalsIgnoreCase("ProgramFileEncoding")) {
                    prgFileEncoding = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("OLPStyle")) {
                    uploadStyle = parameterValue;
                }
                else if (parameterName.equalsIgnoreCase("JointHome1")) {
                    m_jointHomes[0] = Double.parseDouble(parameterValue);
                } 
                else if (parameterName.equalsIgnoreCase("JointHome2")) {
                    m_jointHomes[1] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome3")) {
                    m_jointHomes[2] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome4")) {
                    m_jointHomes[3] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome5")) {
                    m_jointHomes[4] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome6")) {
                    m_jointHomes[5] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome7")) {
                    m_jointHomes[6] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome8")) {
                    m_jointHomes[7] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome9")) {
                    m_jointHomes[8] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome10")) {
                    m_jointHomes[9] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome11")) {
                    m_jointHomes[10] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("JointHome12")) {
                    m_jointHomes[11] = Double.parseDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset1")) {
                    m_encoderOffsets[0] = convertHexToDouble(parameterValue);
                } 
                else if (parameterName.equalsIgnoreCase("EncoderOffset2")) {
                    m_encoderOffsets[1] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset3")) {
                    m_encoderOffsets[2] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset4")) {
                    m_encoderOffsets[3] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset5")) {
                    m_encoderOffsets[4] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset6")) {
                    m_encoderOffsets[5] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset7")) {
                    m_encoderOffsets[6] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset8")) {
                    m_encoderOffsets[7] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset9")) {
                    m_encoderOffsets[8] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset10")) {
                    m_encoderOffsets[9] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset11")) {
                    m_encoderOffsets[10] = convertHexToDouble(parameterValue);
                }                
                else if (parameterName.equalsIgnoreCase("EncoderOffset12")) {
                    m_encoderOffsets[11] = convertHexToDouble(parameterValue);
                }
                else if (parameterName.equalsIgnoreCase("EncoderScale1")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[0] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[0] = Double.parseDouble(parameterValue);
                    }
                } 
                else if (parameterName.equalsIgnoreCase("EncoderScale2")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[1] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[1] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale3")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[2] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[2] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale4")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[3] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[3] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale5")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[4] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[4] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale6")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[5] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[5] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale7")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[6] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[6] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale8")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[7] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[7] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale9")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[8] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[8] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale10")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[9] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[9] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale11")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[10] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[10] = Double.parseDouble(parameterValue);
                    }
                }                
                else if (parameterName.equalsIgnoreCase("EncoderScale12")) {
                    String [] scaleValue = parameterValue.split("=");
                    if (scaleValue.length > 1)
                    {
                        m_encoderScales[11] = Double.parseDouble(scaleValue[1]);
                    }
                    else
                    {
                        m_encoderScales[11] = Double.parseDouble(parameterValue);
                    }
                }   
                else if (parameterName.equalsIgnoreCase("BCoR2")) {
                    m_BCoR2 = Double.parseDouble(parameterValue);
                } 
                else if (parameterName.equalsIgnoreCase("R1CoR2")) {
                    m_R1CoR2 = Double.parseDouble(parameterValue);
                }                                                
                else if (parameterName.equalsIgnoreCase("R1CoB")) {
                    m_R1CoB = Double.parseDouble(parameterValue);
                }                                                                
            }

            finally 
            {
                continue;
            }
        }
        String num = "[0-9Ee\\-\\.\\+]*";
        String hex_num = "[&0-9A-H]*";
        String pose_encoder = "\\((" + hex_num + "\\s*,\\s*" + hex_num + "\\s*,\\s*" + hex_num + "\\s*,\\s*" + hex_num + "\\s*,?\\s*" + hex_num + "?\\s*,?\\s*" + hex_num + "?\\s*,?\\s*" + hex_num + "?" + "\\s*,?\\s*" + hex_num + "?" + "\\s*,?\\s*" + hex_num + "?" + "\\s*,?\\s*" + hex_num + "?" + "\\s*,?\\s*" + hex_num + "?" + "\\s*,?\\s*" + hex_num + "?" + ")\\)\\s*E";
        String pose_cart = "\\((" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,?\\s*" + num + "?" + "\\s*,?\\s*" + num + "?" + "\\s*,?\\s*" + num + "?" + "\\s*,?\\s*" + num + "?" + "\\s*,?\\s*" + num + "?" + "\\s*,?\\s*" + num + "?," + hex_num + ")\\)";
        m_keywords[0] = Pattern.compile("^\\s*S[0-9]*\\s*MOVE\\s*([P|L|C])\\s*,\\s*S\\s*=\\s*(-?[0-9]*\\.?[0-9]*)(%|sec|mm/sec|cm/min)\\s*,\\s*(A\\s*=\\s*[0-5])\\s*,\\s*(T\\s*=\\s*[0-7])\\s*,?\\s*(MX)?\\s*,?\\s*(MX2)?\\s*,?\\s*(G1)?\\s*,?\\s*(G2)?\\s*,?\\s*" + pose_encoder, Pattern.CASE_INSENSITIVE);
        m_keywords[1] = Pattern.compile("^\\s*S[0-9]*\\s*MOVE\\s*([P|L|C])\\s*,\\s*S\\s*=\\s*(-?[0-9]*\\.?[0-9]*)(%|sec|mm/sec|cm/min)\\s*,\\s*(A\\s*=\\s*[0-5])\\s*,\\s*(T\\s*=\\s*[0-7])\\s*,?\\s*(MX)?\\s*,?\\s*(MX2)?\\s*,?\\s*(G1)?\\s*,?\\s*(G2)?\\s*,?\\s*" + pose_cart, Pattern.CASE_INSENSITIVE);
        m_keywords[2] = Pattern.compile("^\\s*DELAY\\s*([0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);
        m_keywords[3] = Pattern.compile("^\\s*WAIT\\s*DI([0-9]*)", Pattern.CASE_INSENSITIVE);
        m_keywords[4] = Pattern.compile("^\\s*DO([0-9]*)\\s*=\\s*(1|0)", Pattern.CASE_INSENSITIVE);        
        m_keywords[5] = Pattern.compile("^\\s*'", Pattern.CASE_INSENSITIVE);
        m_keywords[6] = Pattern.compile("^\\s*REM\\s*", Pattern.CASE_INSENSITIVE);
        m_keywords[7] = Pattern.compile("^\\s*Program\\s*File\\s*Format\\s*Version\\s*:\\s*(-?[0-9]*\\.?[0-9]*)\\s*TotalAxis:\\s*([0-9]*)\\s*AuxAxis:\\s*([0-6])", Pattern.CASE_INSENSITIVE);  
        m_keywords[8] = Pattern.compile("^\\s*END\\s*", Pattern.CASE_INSENSITIVE);
        m_keywords[9] = Pattern.compile("^\\s*CALL\\s+(\\d+)", Pattern.CASE_INSENSITIVE);
    } // end constructor

    public static void main(String [] parameters) 
                                throws SAXException, 
                                ParserConfigurationException, 
                                TransformerConfigurationException, 
                                NumberFormatException,
                                Exception
    {
        // for debugging
        if (bDebugFlag == true)
        {
            //fw = new FileWriter("Hyundai_output.txt", true);
        }
        //Create the object from the class 
        String [] parameterData = parameters[2].split("\\t");
        String [] profileNumbers = parameters[3].split("\\t");
        String [] axisTypes = parameters[4].split("\\t");
        String [] numOfAxis = parameters[5].split("\\t");
        String [] mountedGunName = parameters[6].split("\\t");
        String [] toolProfileNames = parameters[7].split("\\t");
        String [] homeNames = parameters[9].split("\\t");
        String schemaName = parameters[10];
        String [] objectProfileNames = parameters[11].split("\\t");
        
        String [] additionalProgramNames = null;
        if (parameters.length > 12)
        {
            additionalProgramNames = parameters[12].split("\\t");
        }        
        
        HyundaiUploader uploader = new HyundaiUploader(parameters, profileNumbers, toolProfileNames, homeNames, objectProfileNames);

		//Get the robot program and parse it line by line
        String robotProgramName = uploader.getPathToRobotProgramFile();
        File f = new File(robotProgramName);
        String path = f.getParent() + f.separator;
        String uploadInfoFileName = uploader.getPathToErrorLogFile();
        
        boolean createTool = false;
        double xx = 0.0;
        double yy = 0.0;
        double zz = 0.0;
        double yaw = 0.0;
        double pitch = 0.0;
        double roll = 0.0; 
        int toolCount = 0;
        int objectCount = 0;

        double [] objectFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};
        
        double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
        Double massValue = Double.valueOf("0.0");
        double [] cogValues = {0.0, 0.0, 0.0};
        double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};
        Element tmpelem = uploader.createToolProfile(uploader.m_toolProfileListElement, "T=0", DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
        uploader.m_toolNumberMapping.put("T=0", "T=0");      
        
        try
        {
            bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));
            bw.write(VERSION);
            bw.write(COPYRIGHT);

            if (additionalProgramNames != null)
            {
                for (int jj = 0; jj<additionalProgramNames.length;jj++)
                {
                    int index = uploader.m_listOfCalledPrograms.indexOf(additionalProgramNames[jj]);
                    if (index < 0) {
                        uploader.m_listOfCalledPrograms.add(uploader.m_calledProgramCounter, additionalProgramNames[jj] );
                        uploader.m_listOfCalledPrograms.trimToSize(); 
                        uploader.m_calledProgramCounter++;
                    }
                }
            }            
            uploader.processRobotProgram(robotProgramName, mountedGunName[0], false); 
            for (int ii = 0; ii < uploader.m_calledProgramCounter; ii++) 
            {
                if (uploader.m_inLogicProgram == false)
                {
                    robotProgramName = (String)uploader.m_listOfCalledPrograms.get(ii);
                    uploader.processRobotProgram(robotProgramName, mountedGunName[0], false);
                }
            }
            
            bw.write("End of java parsing.\n");
            bw.flush();
            bw.close();
        }
        catch (IOException e) {
            e.getMessage();
        }

        try 
        {
            uploader.writeXMLStreamToFile();
        }
        catch (TransformerException e) 
        {
            e.getMessage();
        }
        catch (IOException e) 
        {
            e.getMessage();
        }

        // TODO 
        //fw.close();

    } // end main

    private void processRobotProgram(String robotProgramName, String mountedGunName, boolean logicOnly) throws Exception
    {
        Element activityListElem = null;
        
        File f = new File(robotProgramName);
        String taskName = f.getName();
        
        if (activityListElem == null)
        {
            if (logicOnly == false)
            {
                activityListElem = super.createActivityList(taskName);
                processComments(activityListElem, "Pre");
            }
        }
        try 
        {
            BufferedReader inProcRead = new BufferedReader(new FileReader(robotProgramName));
            bw.write("Translating program file \"");
            bw.write(robotProgramName);
            bw.write("\"\n\n");
            
            String procLine = inProcRead.readLine();
			
            while(procLine != null) 
            {
                if (procLine.equals("")) 
                {
                    procLine = inProcRead.readLine();
                    continue;
                }
                Matcher procMatch = m_keywords[9].matcher(procLine);
                if (procMatch.find() == true)
                {
                    String progName = procMatch.group(1);
                    m_procCount++;
                    m_procNameList.put(progName, String.valueOf(m_procCount));
                }

                procLine = inProcRead.readLine();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));
            boolean handle_attr = false;
            boolean handle_appl = false;
            
            String line = in.readLine();
            int lineNumber = 1;
            boolean unparsedEntitiesExist = false;
                
            StringBuffer unparsedStatement = new StringBuffer();
            String unparsedString = new String();
                   
            while(line != null) 
            {
                if (line.equals("")) 
                {
                    line = in.readLine();
                    continue;
                }
                
                unparsedStatement.append("Line ").append(lineNumber).append(": ").append(line).append("\n");
                unparsedString = unparsedStatement.toString();
                 
                /****
                m_nIncr = 0;
                boolean bStatus = isLineNumPresent(line);
                if (bStatus == true)
                {
                    line = preProcessLine(line);
                }
                 ****/
                
                for (int ii = 0; ii < m_keywords.length; ii++) 
                {
                    Matcher match = m_keywords[ii].matcher(line);
                    if (match != null && match.find() == true) 
                    {
                         bw.write("Parsing line number " + String.valueOf(lineNumber) + ": " + line + "\n");
                         bw.flush();                          
                        //Call appropriate methods to handle the input
                        switch(ii) 
                        {
                            case 0: 
                                if (logicOnly == false)
                                {
                                    processMOVEStatement(match, activityListElem, mountedGunName, false);
                                }
                                break;
                            case 1: 
                                if (logicOnly == false)
                                {
                                    processMOVEStatement(match, activityListElem, mountedGunName, true);
                                }
                                break;
                            case 2: 
                                if (logicOnly == false)
                                {
                                    processDELAYStatement(match, activityListElem);
                                }
                                break;
                            case 3:
                                if (m_commWaitSig == false)
                                {
                                    int matchShift = 0;
                                    processWAITStatement(match, activityListElem);
                                }
                                else
                                {
                                    if (uploadStyle.equalsIgnoreCase("OperationComments"))
                                        creOpComm(line, true, activityListElem);
                                    else
                                        addToComments(line, true, true);
                                }
                                break;                                
                            case 4: 
                                if (logicOnly == false)
                                {
                                    processSETStatement(match, activityListElem);
                                }
                                break;
                            case 5:
                            case 6:
                                if (logicOnly == false)
                                {
                                    if (uploadStyle.equalsIgnoreCase("OperationComments"))
                                        creOpComm(line, false, activityListElem);
                                    else
                                        addToComments(line, false, false);
                                }
                                break;
                            case 7:
                                break;
                            case 8:
                                //if (logicOnly == false)
                                {
                                    processComments(m_currentElement, "Post");
                                    //processENDStatement(match, activityListElem, false);
                                }
                                break;
                            case 9:
                                processCALLStatement(match.group(1), activityListElem, robotProgramName, logicOnly);
                                break;
                        } // switch
                        break;
                    } // if
                    else if (ii == (m_keywords.length - 1))
                    {
                        String macroCommand = line;
                        boolean macroFound = false;
                        macroCommand = macroCommand.trim();
                        if (macroCommand != null) 
                        {
                            String macroAction = (String)(m_macroCommands.get( macroCommand ));
                            if (macroAction != null)
                            {
                                // TODO - handle macros
                                //processMacroStatement( macroCommand, macroAction, activityListElem, mountedGunName );
                                macroFound = true;
                            }
                        }
                        if (macroFound == false)// && m_moduleFound == true)
                        {
                            if (uploadStyle.equalsIgnoreCase("OperationComments"))
                            {
                                creOpComm(line, true, activityListElem);
                            }
                            else
                            {
                                addToComments(line, true, true);
 
                            }
                                bw.write("WARNING Unparsed Entity at ");
                                bw.write(unparsedString);
                                unparsedEntitiesExist = true;                                
                                bw.flush();                            
                        }
                    } // else if
                } // for
                    
                if (handle_attr == true || handle_appl == true)
                    addToComments(line, true, false);
                        
                /* AFTER processing the END line(add PostComment)
                 * skip EndOfFile characters added by some system
                 */
                if (line.equalsIgnoreCase("END"))
                    break;

                line = in.readLine();
                lineNumber++;
                    
                int numOfChar = unparsedStatement.length();
                unparsedStatement = unparsedStatement.delete(0, numOfChar);
            } // while
            in.close();
                
            if (unparsedEntitiesExist == false)
            {
                bw.write("\nAll the program statements have been parsed.\n");
                bw.write("Java parsing completed successfully.\n\n");
            }
        } // try

        catch (IOException e) 
        {
            try {
                bw.write("ERROR: " + e.getMessage() + "\n\n");
            }
            catch (IOException bwe) {
                bwe.getMessage();
            }
        }
    } // end - processRobotProgram
    
    private void creOpComm(String line, boolean nativeLanguage, Element actListElem)
    {
        String activityName = line.trim();
        Element actElem = super.createActivityHeader(actListElem, activityName, "Operation");
        String [] nativeNames = {"Robot Language"}, nativeValues = {line};
        String [] commNames = {"Comment"}, commValues = {line.substring(1)};
        if (nativeLanguage == true)
            super.createAttributeList(actElem, nativeNames, nativeValues);
        else
            super.createAttributeList(actElem, commNames, commValues);
    }
    
    private void addToComments(String line, boolean header, boolean nativeLanguage)
    {
        //System.out.println("-->AddToComments");
        String [] commentComponents = line.split("'");
        String  newLine = line;
        String commentConcat = "";
        for (int jj = 1; jj < commentComponents.length; jj++)
        {
            commentConcat = commentConcat.concat(commentComponents[jj]);
        }
        if (nativeLanguage)
            newLine = "Robot Language:" + line;

        String commentName = "Comment" + String.valueOf(m_commentCount+1);
        if (header == true)
            m_commentValues[m_commentCount] = newLine;
        else 
            m_commentValues[m_commentCount] = commentConcat;

        m_commentNames[m_commentCount] = commentName;
        m_commentCount++;
    } // end - addToComments


        
    private void processComments(Element addCommentElem, String commentPrefix)
    {   
        if (m_commentCount == 0)
        {
            return;
        }
        
        String [] commentNames = new String [m_commentCount];
        String [] commentValues = new String [m_commentCount];
		
        for (int ii = 0; ii < m_commentCount; ii++) 
        {
            if (m_commentNames[ii].indexOf("Comment") == 0)
                commentNames[ii] = commentPrefix + m_commentNames[ii];
            else
                commentNames[ii] = m_commentNames[ii]; 
            commentValues[ii] = m_commentValues[ii];
        }
        super.createAttributeList(addCommentElem, commentNames, commentValues);
        m_commentCount = 0;
        clearComments();

    } // end - processComments

    private void clearComments() 
    {
        for (int ii=0;ii<m_commentNames.length;ii++) 
        {
            m_commentNames[ii] = "";
        }
        for (int ii=0;ii<m_commentValues.length;ii++) 
        {
            m_commentValues[ii] = "";
        }
    } // end - clearComments

    private void processMOVEStatement(Matcher match, Element activityListElem, String gunName, boolean cartTarget)
    {
        // first look for MX or MX2 and if they exist then do a retract
        if (match.group(6) != null || match.group(7) != null)
        {
            processBackupStatement(match, activityListElem, gunName, match.group(6), match.group(7));
        }        
        //Create DOM Nodes and set appropriate attributes
        String activityName = "RobotMotion." + String.valueOf(m_moveCounter);
        Element actElem = super.createMotionActivityHeader(activityListElem, activityName);

        String toolProfileName = "T=0";
        String objProfileName = "";
        String motionProfileName = "S=100%";
        String concStr = "";
        String eoffStr = "";

        //String moveComponent = match.group(1);
        String motionType = match.group(1);
        String speedTimeComp = match.group(3);

        //---------------------------------------------------------------------------------------
        // Sept 6th - Start
        int targetNumber = 0;
        targetNumber = m_targetNumber;

        String components = match.group(10);
        String positionValues [] = components.split(",");
        int len = m_nNumberOfAxes = positionValues.length;
        for (int ii = 0; ii < len; ii++)
            positionValues[ii] = positionValues[ii].trim();

        //Add the target to a dynamic array
        m_listOfRobotTargets.add(targetNumber, positionValues);
        //Resize the array
        m_listOfRobotTargets.trimToSize();

        //Connect the array with the target type through hashtable 
        Integer tmpInt = new Integer(targetNumber);
        m_targetTypes.put(tmpInt.toString(), new Integer(m_targetType));

        //Connect the array with tag names through hashtable
        if (!m_currentTagPoint.equals("")) 
        {   
            m_tagNames.put(tmpInt.toString()+1, m_currentTagPoint + ".");
            m_currentTagPoint = "";
        }
        else 
        { 
            String tpName = tmpInt.toString();
            m_tagNames.put(tmpInt.toString()+1, tpName + ".");
        }
        if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)
        {
            String [] auxAxisValues = {"0", "0", "0", "0", "0", "0"};
            int totalAxes = m_numRobotAxes+m_numExtAxes+m_numAuxAxes+m_numWorkAxes;
            // copy all the aux-axes values into the auxAxisValues array
            for (int ii = m_numRobotAxes, jj = 0; ii < totalAxes; ii++, jj++)
            {
                auxAxisValues[jj] = positionValues[ii];
            }

            //Get the target number as an object
            targetNumber = m_targetNumber;

            //Add the target to a dynamic array
            if (m_numAuxAxes > 0)
            {
                m_listOfAuxTargets.add(m_targetNumber, auxAxisValues);
                m_listOfAuxTargets.trimToSize();
            }
            if (m_numExtAxes > 0)
            {
                m_listOfExtTargets.add(m_targetNumber, auxAxisValues);
                m_listOfExtTargets.trimToSize();
            }
            if (m_numWorkAxes > 0)
            {
                m_listOfWorkTargets.add(m_targetNumber, auxAxisValues);
                m_listOfWorkTargets.trimToSize();
            }
        }
        // Sept 6th - End
        //---------------------------------------------------------------------------------------
        // Speed component is mandatory in Hyundai MOVE statement
        if (speedTimeComp != null) // must be non-null
        {
            boolean isInList = false;
            String speedTimeValue = match.group(2);
            if (speedTimeValue != null) // must be non-null
            {
                isInList = m_listOfSpeedValues.contains(speedTimeValue);
                if (!isInList)
                    m_listOfSpeedValues.add(speedTimeValue);
            }

            motionProfileName = "S=" + speedTimeValue + speedTimeComp;

            if (!isInList)
            {
                if (speedTimeComp.equalsIgnoreCase("mm/sec"))	//Speed
                {
                    Double speedVal = Double.valueOf(speedTimeValue);
                    double speed = speedVal.doubleValue()/1000.0;
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speed,
                                         100.0, 100.0, 100.0);
                }
                else if (speedTimeComp.equalsIgnoreCase("cm/min"))
                {
                    Double speedVal = Double.valueOf(speedTimeValue);
                    double speed = speedVal.doubleValue()*10.0/60.0;
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speed,
                                         100.0, 100.0, 100.0);                    
                }
                else if (speedTimeComp.equalsIgnoreCase("%"))
                {
                    Double speedVal = Double.valueOf(speedTimeValue);
                    double speed = speedVal.doubleValue();
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT, speed,
                                         100.0, 100.0, 100.0);                    
                }
                else	// Time
                {
                    Double timeVal = Double.valueOf(speedTimeValue);
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeVal.doubleValue(),
                                         100.0, 100.0, 100.0);
                }
            }
        }
        //----------------------------------------------------------------------------------------
        // Tool component in Hyundai MOVE statement is optional, if omitted, use the previous value
        toolProfileName = match.group(5);
        String toolProfileCheck = "";
        if (toolProfileName != null &&  toolProfileName != "") 
        {

            toolProfileCheck = (String) m_toolNumberMapping.get(toolProfileName);

            if (toolProfileCheck == null)
            {
                double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Double massValue = Double.valueOf("0.0");
                double [] cogValues = {0.0, 0.0, 0.0};
                double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Element tmpelem = super.createToolProfile(m_toolProfileListElement, toolProfileName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
                m_toolNumberMapping.put(toolProfileName, toolProfileName);                        
            }
            else
            {
                toolProfileName = toolProfileCheck;
            }
        }
        else
            toolProfileName = "T=0";

        //----------------------------------------------------------------------------------------
        // Accuracy component in Hyundai MOVE statement is optional, if omitted, previous value is taken
        String accuracyProfileName = match.group(4);


        if (accuracyProfileName != null)
        {
            boolean isInList = m_listOfAccuracyValues.contains(accuracyProfileName);
            if (!isInList)
                m_listOfAccuracyValues.add(accuracyProfileName);

            if (!isInList)
            {
                boolean flyby = true;
                
                if (accuracyProfileName.equals("A=0"))
                    flyby = false;

                double pLevel = Double.parseDouble(accuracyProfileName.substring(2,3));

                this.CreateAccuracyProfile(accuracyProfileName, flyby, m_AccuracyType, pLevel);
            }

            m_prevAccuracyValue = accuracyProfileName;
        }
        else	// use previous accuracy value
        {
            if (m_prevAccuracyValue != null)
            {
                accuracyProfileName = m_prevAccuracyValue;
            }
        }

        //------------------------------------------------------------------------------------------
        // ObjectProfile
        if (m_currentObjectNumber != null || m_currentObjectNumber != "") 
        {
            objProfileName = (String) m_objectNumberMapping.get(m_currentObjectNumber);
            if (objProfileName == null)
                objProfileName = "ObjectProfile." + m_currentObjectNumber;
        }
        else
            objProfileName = "ObjectProfile." + m_currentObjectNumber;

        //------------------------------------------------------------------------------------------
        DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype;
        if (motionType.equalsIgnoreCase("P"))
        {
            firstCircularMove = true;
            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype);  
        }
        else if (motionType.equalsIgnoreCase("L"))
        {
            firstCircularMove = true;
            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype, DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS);
        }
        else
        {
            // circular motion - supported from R15 onwards
            if (firstCircularMove == true)
            {
                firstCircularMove = false;
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
            }
            else
            {
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
            }
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype);
        }
 
        //Call the method to process the target
        processTarget(actElem, match, cartTarget);
        
        processComments(actElem, "Pre");
        m_currentElement = actElem;
        m_moveCounter++;
        m_targetNumber = m_targetNumber + 1;
		
        if (match.group(8) != null || match.group(9) != null)
        {
            processSPOTStatement(match, activityListElem, gunName, match.group(8), match.group(9));
        }
        
    } // end - processMOVEStatement

    /* If RailAxisIsJoint1 is TRUE, move targetValues[0] to targetValues[6] and
     * targetValues[1]~targetValues[6] up one element
     */
    private void swapTargetValues(String [] targetValues)
    {
        if (m_numAuxAxes <= 0 || railAxisIsJoint1 == false)
            return;

        String s = targetValues[0];
        for (int ii=0; ii<6; ii++)
            targetValues[ii] = targetValues[ii+1];
        targetValues[6] = s;
    }

    private void processDELAYStatement (Matcher match, Element activityListElem)
    {
        String timeInSec = match.group(1);
        createTimerStatement(timeInSec, activityListElem, true);
        //String delayActName = "RobotDelay." + String.valueOf(m_delayCounter);     
        //Element actElem = super.createDelayActivity(activityListElem, delayActName, Double.valueOf("0.0"), Double.valueOf(timeInSec));
        //processComments( actElem, "Pre");
        //m_currentElement = actElem;
        //m_delayCounter++;
    }

    private void processSETStatement(Matcher match, Element activityListElem)
    {
        String portNumber = match.group(1);
        String condition = match.group(2);

        boolean signalValue = false;
        String status = "OFF";

        if (condition.equals("1"))
        {
            signalValue = true;
            status = "ON";
        }
        else
        {
            signalValue = false;
            status = "OFF";
        }

        String signalName = "DO" + portNumber;
        String ioname;
        String time = "0";
        String delay = "0";
 
        ioname = "Set - DO" + portNumber + "=" + status;

        Element actElem = super.createSetIOActivity(activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(time));

        processComments(actElem, "Pre");
        m_currentElement = actElem;
    }

    private void processWAITStatement(Matcher match, Element activityListElem)
    {
        String signalVariable = "DI";
        String portNumber = match.group(1);
        String maxTime = "0";
        String status = "ON";

        boolean signalValue = true;

        String signalName = "DI" + portNumber;
        String ioname = "Wait Until " + signalVariable + signalName + " = " + status;
            
        Element actElem = super.createWaitForIOActivity( activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(maxTime)); 
           
        processComments(actElem, "Pre");
        m_currentElement = actElem;
    }

    private void processSPOTStatement(Matcher match, Element activityListElem, String gunName, String G1, String G2)
    {
        String gunNumber = "0";
        String weldCondNumber = "0";
        String weldSeqNumber = "0";
        String weldPointNumber = "0";
        String actionName = "DNBIgpSpotWeld";
        
        String spotActionName = actionName + "." + String.valueOf(m_spotCounter);

        Element actionElem = super.createActionHeader( activityListElem, spotActionName, actionName, gunName);   
 
        m_currentElement = activityListElem;

        String weldHomeName = null;
        if (G1 != null)
        {
            weldHomeName = (String)m_weldScheduleHomes.get(G1);
        }
        if (G2 != null)
        {
            weldHomeName = (String)m_weldScheduleHomes.get(G2);
            processSpotAttribute(actionElem, "Gun", "G2");
        }
        if (G1 != null && G2 != null)
        {
            weldHomeName = (String)m_weldScheduleHomes.get(G1 + "," + G2);
            processSpotAttribute(actionElem, "Gun", "G1,G2");
        }
        
        String openHomeName = null;
        if (G1 != null)
        {
            openHomeName = (String)m_backupHomes.get(G1);
        }

        if (G2 != null)
        {
            openHomeName = (String)m_backupHomes.get(G2);
        }
        if (G1 != null && G2 != null)
        {
            openHomeName = (String)m_backupHomes.get(G1 + "," + G2);            
        }
        
        String weldScheduleDelay = null;
        if (G1 != null)
        {
            weldScheduleDelay = (String)m_weldScheduleDelays.get(G1);
        }

        if (G2 != null)
        {
            weldScheduleDelay = (String)m_weldScheduleDelays.get(G2);
        }        
        if (G1 != null && G2 != null)
        {
            weldScheduleDelay = (String)m_weldScheduleDelays.get(G1 + "," + G2);            
        }
        
        String delayTime = ".5";
        if (weldScheduleDelay != null)
            delayTime = weldScheduleDelay;

        // weld home move
        if (weldHomeName == null)
            weldHomeName = (String)m_listofHomeNames.get(0);

        Element spotActivityListElem = super.createActivityListWithinAction(actionElem);

        processSpotHomeMove(weldHomeName, spotActivityListElem, false);

        // delay
        createTimerStatement(delayTime, spotActivityListElem, false);

        // open/backup home move
        
        if (openHomeName == null) 
        {
            if (m_listofHomeNames.size() > 1) 
            {
                openHomeName = (String)m_listofHomeNames.get(1);
            }
            else
            {
                openHomeName = "Home_2";
            }
        }

        processSpotHomeMove(openHomeName, spotActivityListElem, true);

        m_spotCounter++;
    }
    
    private void processBackupStatement(Matcher match, Element activityListElem, String gunName, String MX, String MX2) {    

                    //Create DOM Nodes and set appropriate attributes
        String backupActionName = "DNBIgpSpotRetract." + String.valueOf(m_backupCounter);
        Element actionElem = super.createActionHeader( activityListElem, backupActionName, "DNBIgpSpotRetract", gunName); 
        m_currentElement = actionElem;

        String backupHomeName = null;
        if (MX != null)
        {
            backupHomeName = (String)m_backupHomes.get(MX);
        }

        if (MX2 != null)
        {
            backupHomeName = (String)m_backupHomes.get(MX2);
            processSpotAttribute(actionElem, "Retract", "MX2");
        }
        
        if (MX2 != null && MX != null)
        {
            backupHomeName = (String)m_backupHomes.get(MX + "," + MX2);
            processSpotAttribute(actionElem, "Retract", "MX,MX2");
        }        

        Element activityList = super.createActivityListWithinAction( actionElem );                

        
        /* open/backup home move */

        if (backupHomeName == null) {
            if (m_listofHomeNames.size() > 1) {
            backupHomeName = (String)m_listofHomeNames.get( 1 );
            }
            else{
                backupHomeName = "Home_2";
            }
        }
        processSpotHomeMove( backupHomeName, activityList, true );

        m_backupCounter++;
    }
    
    private void processCALLStatement(String subProgNum, Element activityListElem, String robotProgramName, boolean logicOnly)
    {
        File f = new File(robotProgramName);
        String nameNoPath = f.getName();
        String path = f.getParent() + f.separator;
        
        while (subProgNum.length() < 3)
            subProgNum = "0" + subProgNum;
        
        int iLength = nameNoPath.length();
        String progName = nameNoPath.substring(0, iLength-3) + subProgNum;
        
        if (logicOnly == false)
        {
            String callActName = "RobotCall." + String.valueOf(m_callCounter);
            Element actElem = super.createCallTaskActivity( activityListElem, callActName, progName);
            processComments( actElem, "Pre");
            m_currentElement = actElem;
            m_callCounter++;
        }

        String progNameWithPath = path + progName;
        int index = m_listOfCalledPrograms.indexOf(progNameWithPath);
        if (index < 0) 
        {
            m_listOfCalledPrograms.add(m_calledProgramCounter, progNameWithPath);
            m_listOfRobotTargets.trimToSize(); 
            m_calledProgramCounter++;
        }
    } // end processCALLStatement

    private void CreateAccuracyProfile(String accuracyProfileName, boolean flyby, 
                                       int accuracyType, double pLevel)
    {
        double accuracyValue = Math.floor((pLevel) * 100 / 5);

        if (accuracyType == 0)
        {
            createAccuracyProfile(m_accuracyProfileListElement, accuracyProfileName,
                                  DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE,
                                  flyby, accuracyValue);
        }
        else
        {
            createAccuracyProfile(m_accuracyProfileListElement, accuracyProfileName,
                                  DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                                  flyby, accuracyValue);
        }

    } // end - CreateAccuracyProfile

    private void processTarget(Element robotMotionElem, Matcher match, boolean cartTarget)
    {
        String [] targetValues = (String [])m_listOfRobotTargets.get(m_targetNumber);

        Integer targetType = Integer.getInteger("0");
        if (cartTarget == false)
        {
            targetType = new Integer(JOINT);
        }
        else
        {
            targetType = new Integer(CARTESIAN);
        }

        //Process the joint target
        if (targetType.equals(new Integer(JOINT))) 
        {
            enc2joint( targetValues );
            Element targetElem = super.createTarget(robotMotionElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false); 
            m_createJointTarget = true;
            ArrayList [] jointTargetArrayList = new ArrayList[m_numRobotAxes+m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
            //Print out the target (joint) values
            for (int ii = 0; ii < targetValues.length; ii++)
                formatJointValueInPulses(jointTargetArrayList, targetValues[ii] , "Joint", null, ii);
                
            //If there are auxiliary axes present, print out their joint values
            int totalAxes = m_numRobotAxes+m_numExtAxes+m_numAuxAxes+m_numWorkAxes;
            if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) 
            {
                if (  m_nNumberOfAxes == totalAxes )
                {
                    Integer extendedTargetNum = new Integer(m_targetNumber);
                    String Position = extendedTargetNum.toString();
                    if (m_numAuxAxes > 0)//&& m_auxAxesCount == m_numAuxAxes)
                        processExtendedTarget(jointTargetArrayList, Position, m_listOfAuxTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes, targetValues);
                    if (m_numExtAxes > 0)//&& m_extAxesCount == m_numExtAxes)
                        processExtendedTarget(jointTargetArrayList, Position, m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING , m_numExtAxes, targetValues);
                    if (m_numWorkAxes > 0)//&& m_workAxesCount == m_numWorkAxes)
                        processExtendedTarget(jointTargetArrayList, Position, m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes, targetValues);
                }
                else
                {
                  try {
                        bw.write("HYUNDAI PARSER ERROR: Mismatch between Number of Axes in V5 Device and number counted in Program\n");
                        bw.write("HYUNDAI PARSER ERROR: Auxiliary Axes will not be set\n\n");
                        bw.flush();
                  }
                  catch (IOException e) {
                                e.getMessage();
                            }                    
                }
            }

            super.createJointTarget(targetElem, jointTargetArrayList);

            // TODO - does HyundaiAX have any use of the below lines of code?
            //if (explicit == false) 
            //{
                    //m_commentNames[m_commentCount] = "Tag Name";
                    //m_commentValues[m_commentCount] = tagcomponent;
                    //m_commentCount++;
            //}
        }
        else if (targetType.equals(new Integer(CARTESIAN))) 
        {
            Element targetElem = super.createTarget(robotMotionElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, false);
            m_createJointTarget = false;
            ArrayList [] jointTargetArrayList = new ArrayList[m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
                
            double [] cartValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            cartValues[0] = Double.valueOf(targetValues[0]).doubleValue() * 0.001;
            cartValues[1] = Double.valueOf(targetValues[1]).doubleValue() * 0.001;
            cartValues[2] = Double.valueOf(targetValues[2]).doubleValue() * 0.001;

            m_targetYaw = Double.valueOf(targetValues[3]).doubleValue();
            m_targetPitch = Double.valueOf(targetValues[4]).doubleValue();
            m_targetRoll = Double.valueOf(targetValues[5]).doubleValue();
                
            cartValues[3] = m_targetYaw;
            cartValues[4] = m_targetPitch;
            cartValues[5] = m_targetRoll;

            
            String cfgName = "Posture__1";
            int [] turnNumbers = {0,0,0,0};
           
            int totalAxes = m_numAuxAxes+m_numExtAxes+m_numWorkAxes + m_numRobotAxes;
            
            String configBitMask = targetValues[targetValues.length-1];
            if (configBitMask != null)
            {
                String [] configComponents = configBitMask.split("H");
                if (configComponents != null && configComponents.length > 1)
                {
                    String configValueStr = configComponents[configComponents.length-1];
                    if (configValueStr != null)
                    {
                        int configValue = Integer.parseInt(configValueStr,16);
                        int configAuto = configValue & 1;
                        int configSelect = configValue & 14;
                        if (configAuto == 0)
                        {
                            switch (configSelect)
                            {
                                case 0:
                                     cfgName = "Posture_1";
                                     break;
                                case 2:
                                     cfgName = "Posture_7";
                                     break;
                                case 4:
                                     cfgName = "Posture_3";
                                     break;
                                case 6:
                                     cfgName = "Posture_5";
                                     break;
                                case 8:
                                     cfgName = "Posture_2";
                                     break;
                                case 10:
                                     cfgName = "Posture_8";
                                     break;
                                case 12:
                                     cfgName = "Posture_4";
                                     break;
                                case 14:
                                     cfgName = "Posture_6";
                                     break;
                            }
                            turnNumbers[0] = configValue & 16;
                            turnNumbers[1] = configValue & 32;
                            turnNumbers[3] = configValue & 64;
                        }
                    }
                }
            }
            
            Element cartTargetElem = super.createCartesianTarget(targetElem, cartValues, cfgName);
     
            if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0 || ((m_nNumberOfAxes-1) > m_numRobotAxes) ) 
            {
                if ( totalAxes == (m_nNumberOfAxes - 1) )
                {
                Integer extendedTargetNum = new Integer(m_targetNumber);
                String Position = extendedTargetNum.toString();
                if (m_numAuxAxes > 0)//&& m_numAuxAxes == m_auxAxesCount )
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfAuxTargets,  DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes, targetValues);
                if (m_numExtAxes > 0)//&& m_numExtAxes == m_extAxesCount )
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING, m_numExtAxes, targetValues);
                if (m_numWorkAxes > 0)//&& m_numWorkAxes == m_workAxesCount )
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes, targetValues);
                super.createJointTarget( targetElem, jointTargetArrayList);
                }
                else
                {
                  try {
                        bw.write("HYUNDAI PARSER ERROR: Mismatch between Number of Axes in V5 Device and number counted in Program\n");
                        bw.write("HYUNDAI PARSER ERROR: Auxiliary Axes will not be set\n\n");
                        bw.flush();
                  }
                  catch (IOException e) {
                                e.getMessage();
                            }                                        
                }
            } //end if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)

            super.createTurnNumbers( cartTargetElem, turnNumbers);
            String tagcomp = "P" + m_moveCounter + ".";
            super.createTag( cartTargetElem, tagcomp);
            tagcomp = "";
        } //end else if
		
    }	// end - processTarget

    double convertHexToDouble( String targetValue )
    {
            String [] targetValueComponents = targetValue.split("H");
            
            double retVal = 0.0;
            if (targetValueComponents != null)
            {
                String targetValueStr = targetValueComponents[targetValueComponents.length-1];
                if (targetValueStr != null)
                {
                    int decValue = Integer.parseInt(targetValueStr, 16);
                    retVal = decValue;
                }
            }
            return retVal;
    }
    void enc2joint( String [] targetValues )
    {
         double [] jntval = new double[targetValues.length];
         double [] encval = new double[targetValues.length];
         
         for( int ii = 0; ii < targetValues.length; ii++ )
         {
            encval[ii] = convertHexToDouble(targetValues[ii]);
            jntval[ ii ] = m_encoderScales[ ii ] * ( encval[ ii ] - m_encoderOffsets[ ii ] );
    
           // System.out.println("Scale factor " + Double.toString(m_encoderScales[ ii ]) + " offset " + Double.toString(m_encoderOffsets[ ii ]) + " encval " + Double.toString(encval[ii]) + " jntval " +  Double.toString(jntval[ii]) + "\n");
    
         }

        if (targetValues.length > 5)
        {
            jntval[ 5 ] += ( jntval[ 4 ] * m_R1CoB + jntval[ 3 ] * m_R1CoR2 );
        }
        if (targetValues.length > 4)
        {
            jntval[ 4 ] += ( jntval[ 3 ] * m_BCoR2 );
        }
         
        for( int ii = 0; ii < targetValues.length; ii++ )
        {
            jntval[ ii ] = Math.toDegrees(jntval[ii]) + m_jointHomes[ii];
            targetValues[ii] = String.valueOf(jntval[ii]);
        }
        
    }

    private void processExtendedTarget(ArrayList [] jointTargetArrayList, 
                                       String sExtendedTarget, 
                                       ArrayList listOfExtendedTargets,  
                                       DNBIgpOlpUploadEnumeratedTypes.AuxAxisType extendedAxisType, 
                                       int len, 
                                       String [] targetValues) 
    {
        Integer extendedTargetNum = new Integer(sExtendedTarget);
        String [] extendedValues;
        extendedValues = new String[6];

        int start = 1;

        String extendedAxisStrType = extendedAxisType.toString();

        for (int ii = 0; ii < targetValues.length - 6; ii++) 
        {
            extendedValues[ii] = targetValues[ii+6];
        }
        //else 
        /*
        {
            extendedValues = (String []) listOfExtendedTargets.get(extendedTargetNum.intValue());
        }
        */
        /*
        if (extendedAxisStrType.equals("RailTrackGantry")) 
        {
            start = m_auxAxesStart;
        }
        else if (extendedAxisStrType.equals("EndOfArmTooling")) 
        {
            start = m_extAxesStart;
        }
        else 
        {
            start = m_workAxesStart;
        }
        */
        //int start = 1;
        if (extendedAxisStrType.equals("RailTrackGantry"))
            start = 1;
        else if (extendedAxisStrType.equals("EndOfArmTooling"))
            start = m_numAuxAxes + 1;
        else
            start = m_numAuxAxes + m_extAxesStart + 1;

        for (int ii = start-1; ii < start + len - 1; ii++) 
        {
            formatJointValueInPulses(jointTargetArrayList, extendedValues[ii], "AuxJoint", extendedAxisType, ii);
        } //end for
    }

    public void formatJointValueInPulses(ArrayList [] jointTargetArrayList, 
                                         String sTarget, 
                                         String robotOrAuxJoint, 
                                         DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxJointType, 
                                         int ii) 
    {
        double targetScale = 1.0;
        String jointName = "";
        String axesType = "";
        int index = ii;
        int arrayIndex = ii;

        if (auxJointType != null)
            index = ii+m_numRobotAxes;

        if (m_createJointTarget == true)
            arrayIndex = index;

        if (index >= m_axisTypes.length)
        {
            return;
        }
        axesType = m_axisTypes[index];
        if (axesType.equals("Translational") )
            targetScale = .001;   
        jointName = "Joint" + String.valueOf(index+1);

        if (auxJointType != null) 
        {
            jointTargetArrayList[arrayIndex] = new ArrayList(5);
        }
        else 
        {
            jointTargetArrayList[arrayIndex] = new ArrayList(4);
        }

        jointTargetArrayList[arrayIndex].add( 0, jointName);

        double targetValue = 0.0;

        try 
        {
            targetValue = Double.parseDouble(sTarget);
            targetValue = targetValue*targetScale;
        }

        catch(NumberFormatException e) 
        {
            e.getMessage();
            targetValue = Double.NaN;
        }

        if (axesType.equals("Rotational"))
            targetValue = Math.toRadians(targetValue);

        String scaledTarget = String.valueOf(targetValue);

        jointTargetArrayList[arrayIndex].add(1, Double.valueOf(scaledTarget)); 

        String dofNum = String.valueOf(index+1);

        jointTargetArrayList[arrayIndex].add(2, Integer.valueOf(dofNum));

        if (axesType.equals("Rotational") ) 
        {
            jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL);
        }
        else 
        {
            jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL);
        }

        if (auxJointType != null)
            jointTargetArrayList[arrayIndex].add(4, auxJointType);
    }

    private int getTurnValue(int axCfg) 
    {
        int turn = 0;
        switch(axCfg)
        {
            case 0:
            case 1:
            case -1:
            case -2: 
                turn = 0;
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                turn = 1;
                break;
            case -3:
            case -4:
            case -5:
            case -6:
                turn = -1;
        }
        return(turn);
    }

    private void createTimerStatement( String timeInSec, Element activityListElem, boolean doComments )
    {   
        //Create DOM Nodes and set appropriate attributes
        String delayActName = "RobotDelay." + String.valueOf(m_delayCounter);     
        if (doComments == false)
        {
            delayActName = "WeldTime";
        }        
        Element actElem = super.createDelayActivity(activityListElem, delayActName, Double.valueOf("0.0"), Double.valueOf(timeInSec));
        if (doComments == true) 
        {
            processComments(actElem, "Pre");
            m_currentElement = actElem;
        }
        m_delayCounter++;
    }

    private void processSpotHomeMove(String homeName, Element actListElem, boolean open ) 
    {
        String activityName = "";
        if (open)
        {
            activityName = "OpenGun";
        }
        else
        {
            activityName = "CloseGun";
        }
 
        super.createMotionActivityWithinAction( actListElem, activityName, Double.valueOf("0.0"), Double.valueOf("0.0"), DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,  homeName);
    }

       private void processSpotAttribute(Element actElem, String attribNameValue, String attribVal ) {
            String [] attribNames = new String [1];
            String [] attribValues = new String [1];
            attribNames[0] = attribNameValue;
            attribValues[0] = attribVal;
            super.createAttributeList(actElem, attribNames, attribValues);
       }    
    //-------------------------------------------------------------------
    // writeOutput
    //-------------------------------------------------------------------
    private void writeOutput(String fnName, String value, String [] valArray) throws Exception
    {
        if ( bDebugFlag == true)
        {
            fnName = fnName + "\n";
            fw.write(fnName, 0, fnName.length());
            fw.flush();

            if (value != null)
            {
                value = value + "\n";
                fw.write(value, 0, value.length());
                fw.flush();
            }

            if (valArray != null)
            {
                int nSize = valArray.length;
                String tmp = "Array Size =" + nSize + "\n";
                fw.write(tmp, 0, tmp.length());
                fw.flush();
                tmp = "\n";

                for (int ii = 0; ii < nSize; ii++)
                {
                    fw.write(valArray[ii], 0, valArray[ii].length());
                    fw.write(tmp, 0, tmp.length());
                    fw.flush();
                }
            }
        }
    }

    private boolean isLineNumPresent(String line)
    {
        String testStr = new String(line);
        char [] charArray = testStr.toCharArray();

        Character CharObj = new Character(charArray[0]);

        int strLen = line.length();

        while (((CharObj.isDigit(charArray[m_nIncr]) == true) ||
               (CharObj.isWhitespace(charArray[m_nIncr]) == true)) && 
               (m_nIncr < strLen) )
        {
            m_nIncr = m_nIncr + 1;
        }

        if (m_nIncr == 0)
            return false;
        else
            return true;
    }

    //-------------------------------------------------------------------
    // preProcessLine
    //-------------------------------------------------------------------
    private String preProcessLine(String line)
    {
        int strLen = line.length();

        char [] charArray = line.toCharArray();
        String newStr = new String(charArray, m_nIncr, strLen-m_nIncr);

        newStr = newStr.trim();

        m_nIncr = 0;

        return newStr;
    }

    private void processARCONStatement(Matcher match, Element activityListElem)
    {
        Element userProfileListElem = getUserProfileListElement();

        String  startCondNum = match.group(1);

        LinkedList [] llarcOnDataUserProfile = new LinkedList[4];
        for (int ii = 0; ii < 4; ii++)
            llarcOnDataUserProfile[ii] = new LinkedList();

        llarcOnDataUserProfile[0].add(0,"WeldSpeed");
        llarcOnDataUserProfile[1].add(0,"StartDwellTime");
        llarcOnDataUserProfile[2].add(0,"Voltage");
        llarcOnDataUserProfile[3].add(0,"Current");

        llarcOnDataUserProfile[0].add(1,"double");
        llarcOnDataUserProfile[1].add(1,"double");
        llarcOnDataUserProfile[2].add(1,"double");
        llarcOnDataUserProfile[3].add(1,"double");

        // these values are defined default for tables 1-10 provided int the CATfct file.
        // now..set a default of 0 for each.
        String weldspeedvalue = "0";
        //String enddwelltimevalue = "0";
        String startdwelltimevalue = "0";
        String voltagevalue = "0";
        String currentvalue = "0";

        llarcOnDataUserProfile[0].add(2,weldspeedvalue);
        //llarcOnDataUserProfile[1].add(2,enddwelltimevalue);
        llarcOnDataUserProfile[1].add(2,startdwelltimevalue);
        llarcOnDataUserProfile[2].add(2,voltagevalue);
        llarcOnDataUserProfile[3].add(2,currentvalue);

        Node lastNode = findLastMoveActivityNode();
        if (lastNode != null)
        {
            lastNode.setNodeValue("ARCON");
            String arcon = "";
            arcon = "NAWArcONTable." + startCondNum;
            createUserProfile(userProfileListElem, arcon, arcon, llarcOnDataUserProfile);

            Element uP = findChildElemByName(userProfileListElem, "UserProfile", "Type", arcon);
            if (uP == null)
            {
                //uP = m_xmlDoc.createElement("UserProfile");
                //userProfileListElem.appendChild(uP);
                //uP.setAttribute("Type", arcon);
            }
            Element act = (Element)activityListElem.getLastChild();
            Element actName = findChildElemByName(act, "ActivityName", null, null);
            //actName.getFirstChild().setNodeValue("ArcOn." + startCondNum);
            Element moAttr = findChildElemByName(act, "MotionAttributes", null, null);
            Element moType = findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", arcon);
			
            Text uPVal = m_xmlDoc.createTextNode(arcon);
            uPmot.appendChild(uPVal);
        }
    }

    private void processARCOFFStatement(Matcher match, Element activityListElem)
    {
        Element userProfileListElem = getUserProfileListElement();
        String  endCondNum = match.group(1);

        LinkedList [] llarcOffDataUserProfile = new LinkedList[4];
        for (int ii = 0; ii < 4; ii++)
            llarcOffDataUserProfile[ii] = new LinkedList();

        llarcOffDataUserProfile[0].add(0,"WeldSpeed");
        llarcOffDataUserProfile[1].add(0,"EndDwellTime");
        //llarcOffDataUserProfile[1].add(0,"startdwelltime");
        llarcOffDataUserProfile[2].add(0,"Voltage");
        llarcOffDataUserProfile[3].add(0,"Current");

        llarcOffDataUserProfile[0].add(1,"double");
        llarcOffDataUserProfile[1].add(1,"double");
        llarcOffDataUserProfile[2].add(1,"double");
        llarcOffDataUserProfile[3].add(1,"double");
        //llarcOffDataUserProfile[4].add(1,"double");

        // these values are defined default for tables 1-10 provided int the CATfct file.
        // now..set a default of 0 for each.
        String weldspeedvalue = "0";
        String enddwelltimevalue = "0";
        //String startdwelltimevalue = "0";
        String voltagevalue = "0";
        String currentvalue = "0";

        llarcOffDataUserProfile[0].add(2,weldspeedvalue);
        llarcOffDataUserProfile[1].add(2,enddwelltimevalue);
        //llarcOffDataUserProfile[2].add(2,startdwelltimevalue);
        llarcOffDataUserProfile[2].add(2,voltagevalue);
        llarcOffDataUserProfile[3].add(2,currentvalue);

        Node lastNode = findLastMoveActivityNode();
        if (lastNode != null)
        {
            lastNode.setNodeValue("ARCOFF");
            String arcoff = "";
            arcoff = "NAWArcOFFTable." + endCondNum;
            createUserProfile(userProfileListElem, arcoff, arcoff, llarcOffDataUserProfile);

            Element uP = findChildElemByName(userProfileListElem, "UserProfile", "Type", arcoff);
            if (uP == null)
            {
                //uP = m_xmlDoc.createElement("UserProfile");
                //userProfileListElem.appendChild(uP);
                //uP.setAttribute("Type", arcoff);
            }
            Element act = (Element)activityListElem.getLastChild();
            Element actName = findChildElemByName(act, "ActivityName", null, null);
            //actName.getFirstChild().setNodeValue("ArcOn." + startCondNum);
            Element moAttr = findChildElemByName(act, "MotionAttributes", null, null);
            Element moType = findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", arcoff);
            Text uPVal = m_xmlDoc.createTextNode(arcoff);
            uPmot.appendChild(uPVal);
        }
    }

    private void processSWEAVEStatement(Matcher match, Element activityListElem)
    {
        Element userProfileListElem = getUserProfileListElement();

        String weavestatevalue = match.group(1);
        String subprognum = match.group(2);

        LinkedList [] llarcWeaveDataUserProfile = new LinkedList[2];
        for (int ii = 0; ii < 1; ii++)
            llarcWeaveDataUserProfile[ii] = new LinkedList();

        llarcWeaveDataUserProfile[0].add(0,"WeaveState");
        llarcWeaveDataUserProfile[1].add(0,"SubProgNumber");

        llarcWeaveDataUserProfile[0].add(1,"String");
        llarcWeaveDataUserProfile[1].add(1,"Integer");

        llarcWeaveDataUserProfile[0].add(2,weavestatevalue);
        llarcWeaveDataUserProfile[1].add(2,subprognum);

        String weavename = "NAWWeaveTable";// + subprognum;
        Node lastNode = findLastMoveActivityNode();
        if (lastNode != null)
        {
            createUserProfile(userProfileListElem, weavename, weavename, llarcWeaveDataUserProfile);

            Element uP = findChildElemByName(userProfileListElem, "UserProfile", "Type", "NAWWeaveTable");
            if (uP == null)
            {
                //uP = m_xmlDoc.createElement("UserProfile");
                //userProfileListElem.appendChild(uP);
                //uP.setAttribute("Type", "NAWArcONTable");
            }
            Element act = (Element)activityListElem.getLastChild();
            Element actName = findChildElemByName(act, "ActivityName", null, null);
            //actName.getFirstChild().setNodeValue("ArcOn." + startCondNum);
            Element moAttr = findChildElemByName(act, "MotionAttributes", null, null);
            Element moType = findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", "NAWWeaveTable");
            Text uPVal = m_xmlDoc.createTextNode(weavename);
            uPmot.appendChild(uPVal);
        }
    }

    private void processLABELStatement(String line, Matcher match, Element activityListElem, boolean logicOnly)
    {
        String progName = match.group(1);
        progName = progName.trim();

        if (progName != null)
        {
            String procNumber = (String)m_procNameList.get(progName);
            if (procNumber != null) 
            {
                if (logicOnly == false)
                {
                    //String callActName = "RobotCall." + String.valueOf(m_callCounter);

                    //Element actElem = super.createCallTaskActivity( activityListElem, callActName, progName);
                    //processComments( actElem, "Pre");
                    //m_currentElement = actElem;
                    //m_callCounter++;
                }
                else
                {
                    if (logicOnly == true)
                    {
                        //createCallTaskInProgramLogic(m_logicProgramJobElement, progName);
                    }
                }
            }
        }
    }

    private void processSETOUTPUTStatement(String line, Matcher match, boolean logicOnly)
    {
        String signalName = match.group(1);
        //System.out.println(signalName);

        String signalValue = match.group(2);
        //System.out.println(signalValue);

        if (signalValue.equalsIgnoreCase("1"))
            signalValue = "true";
        else
            signalValue = "false";

        String setoutExpr = signalName + " = " + signalValue;

        if (logicOnly == true) 
        {
            //createSetSignalInProgramLogic(m_logicProgramJobElement, setoutExpr);
            //createOutputDefinitionInProgramLogic(m_logicProgramElement, signalName, m_logicProgramOutputCount);
            //m_logicProgramOutputCount++;
        }
    }
    
    private void processRETURNStatement()
    {
    }

    public Node findLastMoveActivityNode()
    {
        Node lastMovActivityNode = null;
        Element rootElem = super.getRootElement();
        int index = 0;
        if (rootElem != null)
        {
            NodeList childNodes = rootElem.getChildNodes();
            int nRootChildren = childNodes.getLength();

            for (int ii = 0; ii < nRootChildren; ii++)
            {
                Element resElem = (Element)childNodes.item(ii);
                if (resElem.getTagName().equals("Resource"))
                {
                    NodeList activitylistNodeList = resElem.getElementsByTagName("ActivityList");
                    if (activitylistNodeList != null && activitylistNodeList.getLength() > 0)
                    {
                        for (int jj = 0; jj < activitylistNodeList.getLength(); jj++)
                        {
                            Node activityListNode = activitylistNodeList.item(jj);
                            Element activityListElem = (Element)activityListNode;
                            NodeList actActivityNodeList = activityListElem.getElementsByTagName("Activity");
                            if (actActivityNodeList != null && actActivityNodeList.getLength() > 0)
                            {
                                int numActs = actActivityNodeList.getLength();
                                for (int kk = numActs-1; kk >= 0 ; kk--)
                                {
                                    Node activityNode = actActivityNodeList.item(kk);
                                    Element activityElem = (Element)activityNode;
                                    String attrType = activityElem.getAttribute("ActivityType");
                                    if (attrType.equalsIgnoreCase("DNBRobotMotionActivity"))
                                    {
                                        NodeList namelist = activityElem.getElementsByTagName("ActivityName");
                                        if (namelist != null && namelist.getLength() > 0)
                                        {
                                            index = index + 1;
                                            Node nameNode = namelist.item(0);
                                            lastMovActivityNode = nameNode.getFirstChild();
                                            String actLabel = lastMovActivityNode.getNodeValue();
                                            return lastMovActivityNode;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return lastMovActivityNode;
    }

    private void renameTask(String name)
    {
        Element rootElem = super.getRootElement();
        if (rootElem != null)
        {
            NodeList childNodes = rootElem.getChildNodes();
            int nRootChildren = childNodes.getLength();
            for (int ii = 0; ii < nRootChildren; ii++)
            {
                Element resElem = (Element)childNodes.item(ii);
                if (resElem.getTagName().equals("Resource"))
                {
                    NodeList activitylistNodeList = resElem.getElementsByTagName("ActivityList");
                    if (activitylistNodeList != null && activitylistNodeList.getLength() > 0)
                    {
                        Node activityListNode = activitylistNodeList.item(0);
                        Element activityListElem = (Element)activityListNode;
                        String oldName = activityListElem.getAttribute("Task");
                        activityListElem.setAttribute("Task", name);
                    }
                }
            }
        }
    }

    public Element findChildElemByName(Element currentElem, String name, String attrib, String attribVal)
    {
        Element tmpElem;

        NodeList childNodes = currentElem.getChildNodes();
        for (int ii=0; ii<childNodes.getLength(); ii++)
        {
            tmpElem = (Element)childNodes.item(ii);
            if (tmpElem.getTagName().equals(name))
            {
                if (attrib == null)
                {
                    return (tmpElem);
                }
                else
                {
                    String [] a = attrib.split(";");
                    String [] av = attribVal.split(";");
                    int jj;
                    for (jj=0; jj<a.length; jj++)
                    {
                        if (tmpElem.getAttribute(a[jj]).equals(av[jj]))
                            continue;
                        else
                            break;
                    }
                    if (jj==a.length)
                        return (tmpElem);
                }
            }
        }
        return (null);
    } // end findChildElemByName

} // end class

