//DOM and Parser classes
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;

import Kuka.util;

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class MitsubishiUploader extends DNBIgpOlpUploadBaseClass {

        //Constants
        private static final int JOINT = 1, CARTESIAN = 2;
        private static final String VERSION = "Delmia Corp. Mitsubishi Uploader Version 5 Release 24 SP4.\n";
        private static final String COPYRIGHT = "Copyright Delmia Corp. 2014, All Rights Reserved.\n";

        //Member primitive variables
        private int m_numRobotAxes;
        private int m_numAuxAxes;
        private int m_numExtAxes;
        private int m_numWorkAxes;
        private int m_rbtLangCounter;
        private int m_delayCounter;
        private int m_moveCounter;
        private int m_motionProfileCounter, m_accuracyProfileCounter, m_toolCounter;
        private int m_spotPickCounter;
        private int m_spotDropCounter;
        private int m_handMoveCounter;
        private int programCallCount;
        private int robotCallNumber;
        private int operationCommentCounter;
        private int commentCount;
        private int m_nIncr;
        private boolean EndOfArmToolingFirst;
        private float m_Ovrd, m_JOvrd, m_Spd, m_Accel;
        private Float m_Accuracy;
        
        private boolean m_commentWaitSignal;

        private String ioName = "DO[";
        private String m_pickHome;
        private String m_dropHome;
        private String m_grabPart;
        private String m_partGrabbed;
        private String m_program = "1.prg";
        
        //Member objects
        private Document m_xmlDoc;
        private int p_currentToolNumber;
        private String m_mountedTool;
        private ArrayList mountedTools;
        private Pattern [] m_keywords;
        private String [] m_axisTypes;
        private String [] p_toolProfileNames;
        private String m_toolProfileName;
        private ArrayList programCallNames;
        private ArrayList commentLines;
        private Hashtable m_positionVariableTbl;
        private Hashtable m_jointVariableTbl;
        private String prgFileEncoding = "US-ASCII";
        private String uploadStyle = "OperationComments";
        private String m_orientMode;
        private util kku;
        private static BufferedWriter bw;

        //Constructor
        public MitsubishiUploader(String [] parameters) throws ParserConfigurationException
        {
            super(parameters);
            m_rbtLangCounter = 1;
            m_delayCounter = 1;
            m_moveCounter = 1;
            m_motionProfileCounter = 1;
            m_accuracyProfileCounter = 1;
            m_toolCounter =1 ;
            m_spotPickCounter = 1;
            m_spotDropCounter = 1;
            m_handMoveCounter = 1;
            programCallCount = 0;
            robotCallNumber = 1;
            operationCommentCounter = 1;
            commentCount = 0;
            m_nIncr = 0;
            m_Ovrd = 100;  // percentage speed
            m_JOvrd = 100; // percentage speed
            m_Spd = 10000;    // absolute speed, M_NSpd default 10,000 mm/s
            m_Accel = 100;
            m_Accuracy = new Float(0.0);
            m_toolProfileName = "Default";
            m_pickHome = new String("Close");
            m_dropHome = new String("Open");
            m_grabPart = new String("");
            m_partGrabbed = new String("Part");
            m_orientMode = "2_axis";
            
            m_mountedTool = parameters[6];
            mountedTools = new ArrayList(1);
            mountedTools.add(0, m_mountedTool);

            String [] toolProfileNames = parameters[7].split("[\\t]+");
            String [] HandHomes = parameters[9].split("[\\t]+");
            // assume hand home names starting with "O" is for hand open
            // assume hand home names starting with "C" is for hand close
            for (int ii=0; ii<3; ii++) // DELMIA simulation gripper may have 3 home positions
            {
                if (HandHomes.length > ii)
                {
                    if (HandHomes[ii].startsWith("O"))
                        m_dropHome = HandHomes[ii];
                    else
                        if (HandHomes[ii].startsWith("C"))
                            m_pickHome = HandHomes[ii];
                }
            }

            //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1
            m_numRobotAxes = super.getNumberOfRobotAxes();
            m_numAuxAxes   = super.getNumberOfRailAuxiliaryAxes();
            m_numExtAxes   = super.getNumberOfToolAuxiliaryAxes();
            m_numWorkAxes  = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();

            p_currentToolNumber = 0;

            m_keywords = new Pattern [300];
            m_axisTypes = new String[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];
            m_axisTypes = super.getAxisTypes();
            m_positionVariableTbl = new Hashtable();
            m_jointVariableTbl = new Hashtable();

            programCallNames = new ArrayList(1);
            commentLines = new ArrayList(2);
            
            kku = new util();
             
            m_xmlDoc = super.getDOMDocument();

            int ii;
            p_toolProfileNames = new String [toolProfileNames.length];
            for (ii=0; ii<toolProfileNames.length; ii++)
            {
                p_toolProfileNames[ii] = toolProfileNames[ii];
            }

            EndOfArmToolingFirst = true;
            m_commentWaitSignal = true;

            Hashtable m_parameterData = super.getParameterData();
            Enumeration parameterKeys = m_parameterData.keys();
            Enumeration parameterValues = m_parameterData.elements();
            int index, arrayIndex;
            String pName, pValue, testString;
            while (parameterKeys.hasMoreElements() == true && parameterValues.hasMoreElements() == true)
            {
                pName = parameterKeys.nextElement().toString();
                pValue = parameterValues.nextElement().toString();
                index = pName.indexOf('.');
                testString = pName.substring(0, index + 1);

                try
                {
                    if(pName.equalsIgnoreCase("ProgramFileEncoding"))
                    {
                        prgFileEncoding = pValue;
                    }
                    else if(pName.equalsIgnoreCase("OLPStyle"))
                    {
                        uploadStyle = pValue;
                    }
                    else if (pName.equalsIgnoreCase("PickHome"))
                    {
                        m_pickHome = pValue;
                    }
                    else if (pName.equalsIgnoreCase("DropHome"))
                    {
                        m_dropHome = pValue;
                    }
                    else if(pName.equalsIgnoreCase("GrabPart"))
                    {
                        m_grabPart = pValue;
                    }
                    else if(pName.equalsIgnoreCase("PartGrabbed"))
                    {
                        m_partGrabbed = pValue;
                    }
                    else if (pName.equalsIgnoreCase("OrientMode"))
                    {
                        m_orientMode = pValue;
                    }
                    else if (pName.equalsIgnoreCase("CommentWaitSignal"))
                    {
                        if (pValue.equalsIgnoreCase("false") == true)
                            m_commentWaitSignal = false;
                    }
                    else if (pName.equalsIgnoreCase("IOName"))
                    {
                        if (pValue.endsWith("[]"))
                            ioName = pValue.substring(0, pValue.lastIndexOf(']'));
                        else
                            ioName = pValue;
                    }
                }

                finally {
                    continue;
                }
            }

            for (ii = 0; ii < 300; ii++)
            {
                m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
            }

            String sflt = "((?:\\+|\\-)?(?:\\d*\\.?\\d+|\\d+\\.?\\d*))";
            String sFlt = "((?:\\+|\\-)?(?:\\d*\\.?\\d+|\\d+\\.?\\d*))?"; // may be ommited
            String flag = "([0-7]|&H\\p{XDigit}+|&B[01]+)?";
            String pConst = "(\\(\\s*" + sFlt + "(?:\\s*,\\s*" + sFlt + "){1,7}\\s*\\)\\s*(\\(\\s*" + flag + "\\s*,\\s*" + flag + "\\s*\\))?)";
            String jConst = "\\(\\s*" + sFlt + "(?:\\s*,\\s*" + sFlt + "){3,7}\\s*\\)";
            
            m_keywords[0] = Pattern.compile("^Ovrd\\s+"  + sflt, Pattern.CASE_INSENSITIVE);
            m_keywords[1] = Pattern.compile("^JOvrd\\s+" + sflt, Pattern.CASE_INSENSITIVE);
            m_keywords[2] = Pattern.compile("^Spd\\s+"   + sflt, Pattern.CASE_INSENSITIVE);
            m_keywords[3] = Pattern.compile("^Accel" + "(?:\\s+(\\d+))?", Pattern.CASE_INSENSITIVE);
            m_keywords[9] = Pattern.compile("^End", Pattern.CASE_INSENSITIVE);

            m_keywords[10] = Pattern.compile("^Cnt\\s+(0|1)" + "(?:\\s*,\\s*)?" + sFlt, Pattern.CASE_INSENSITIVE);
            m_keywords[12] = Pattern.compile("^TOOL\\s+(PT\\w+)", Pattern.CASE_INSENSITIVE);
            m_keywords[13] = Pattern.compile("^TOOL\\s+" + pConst, Pattern.CASE_INSENSITIVE);
            m_keywords[21] = Pattern.compile("^M_OUT\\s*\\(\\s*(\\d+)\\s*\\)\\s*=\\s*(1|0)(?:\\s+DLY\\s+" + sflt + ")?", Pattern.CASE_INSENSITIVE);
            m_keywords[22] = Pattern.compile("^DLY\\s+" + sflt, Pattern.CASE_INSENSITIVE);
            m_keywords[23] = Pattern.compile("^CALLP\\s+\"(\\w+)\"", Pattern.CASE_INSENSITIVE);
            m_keywords[24] = Pattern.compile("^WAIT\\s+M_IN\\(\\s*(\\d+)\\s*\\)\\s*=\\s*(1|0)", Pattern.CASE_INSENSITIVE);
            m_keywords[30] = Pattern.compile("^(MOV|MVS|MVR)\\s+((?:P|J)\\w+|" + pConst + ")(?:\\s*,\\s*((?:P|J)\\w+|" + pConst + ")\\s*,\\s*((?:P|J)\\w+|" + pConst + "))?", Pattern.CASE_INSENSITIVE);
            
            m_keywords[40] = Pattern.compile("^(P\\w+)\\s*=\\s*" + pConst, Pattern.CASE_INSENSITIVE);
            m_keywords[41] = Pattern.compile("^(J\\w+)\\s*=\\s*" + jConst, Pattern.CASE_INSENSITIVE);

            m_keywords[50] = Pattern.compile("^HCLOSE\\s+(\\d)", Pattern.CASE_INSENSITIVE);
            //                                    group 1: hand number   2: force           3: force           4: time
            m_keywords[51] = Pattern.compile("^HOPEN\\s+(\\d)(?:\\s*,\\s*(\\d+)?(?:\\s*,\\s*(\\d+)?(?:\\s*,\\s*(\\d*\\.?\\d+|\\d+\\.?\\d*))?)?)?", Pattern.CASE_INSENSITIVE);
            // HOpen 1, 63, 63, 0.5; HOpen 1, , , 0.5
            m_keywords[90] = Pattern.compile("^(?:REM|')(.*)", Pattern.CASE_INSENSITIVE);
        }

    	public static void main(String [] parameters) throws SAXException, ParserConfigurationException, TransformerConfigurationException, NumberFormatException, StringIndexOutOfBoundsException
        {
            
            MitsubishiUploader uploader = new MitsubishiUploader(parameters);
            String [] additionalProgramNames = null;
            if (parameters.length > 12 && parameters[12].trim().length() > 0)
                additionalProgramNames = parameters[12].split("[\\t]+");
            
            //Get the robot program and parse it line by line
            String robotProgramName = uploader.getPathToRobotProgramFile();
            File f = new File(robotProgramName);
            String path = f.getParent() + f.separator;
            String robotProgramNameOnly = f.getName();
            uploader.m_program = robotProgramNameOnly;

            String uploadInfoFileName = uploader.getPathToErrorLogFile();
            Matcher match;
            
            Element motionProfileList = uploader.getMotionProfileListElement();
            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
            moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;

            try
            {
                bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));
                bw.write(VERSION);
                bw.write(COPYRIGHT);
                bw.write("\nStart of java parsing.\n\n");

                int errCode;
                errCode = uploader.processRobotProgram(robotProgramName);
                
                String sAddProg = null;
                int index;
                if (additionalProgramNames != null)
                {
                    for (int ii=0; ii<additionalProgramNames.length; ii++)
                    {
                        f = new File(additionalProgramNames[ii]);
                        sAddProg = f.getName();
                        index = uploader.programCallNames.indexOf(sAddProg);
                        if (index < 0)
                        {
                            uploader.programCallNames.add(uploader.programCallCount, sAddProg);
                            uploader.programCallNames.trimToSize();
                            uploader.programCallCount++;
                        }
                    }
                }

                if (errCode == 0)
                {
                    String calledProgName;
                    for (int ii=0; ii<uploader.programCallCount; ii++)
                    {
                        calledProgName = (String)uploader.programCallNames.get(ii);
                        if (calledProgName.equalsIgnoreCase(robotProgramNameOnly))
                            continue; // skip the "main" program already uploaded
                        
                        uploader.m_program = calledProgName;
                        // init program default settings
                        uploader.m_Ovrd = 100;
                        uploader.m_JOvrd = 100;
                        uploader.m_Spd = 10000;
                        uploader.m_Accel = 100;
                        robotProgramName = path + calledProgName;
                        errCode = uploader.processRobotProgram(robotProgramName);
                    }
                }

                if (errCode == 0)
                {
                    bw.write("All the program statements have been parsed.\n");
                    bw.write("\nJava parsing completed successfully.\n");
                }
                else
                {
                    bw.write("\nERROR-Robot program upload failed.\n");
                }

                bw.write("\nEnd of java parsing.\n");
                bw.flush();
                bw.close();
            }//end try

            catch (IOException e) {
                e.getMessage();
            }

            Element accuracyProfileList = uploader.getAccuracyProfileListElement();
            if (accuracyProfileList.hasChildNodes() == false)
                uploader.createAccuracyProfile(accuracyProfileList, "Default",
                    DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                    false, 0.0);
        
            try {
                uploader.writeXMLStreamToFile();
            }

            catch (IOException e) {
                e.getMessage();
            }
            catch (TransformerException e) {
                e.getMessage();
            }
        }//end main

        private int processRobotProgram(String robotProgramName)
        {
            BufferedReader rbt;
            int errCode;

            Element resourceElem = super.getResourceElement();
            File f = new File(robotProgramName);
            String taskName = f.getName();
            taskName = taskName.substring(0, taskName.lastIndexOf('.'));
            Element activityListElem = super.createActivityList(taskName);
            /* create AttributeList attached to the ActivityList right away
             * because after the AttributeList attached to the Activity is created
             */
            super.createAttributeList(activityListElem, null, null);
            resourceElem.appendChild(activityListElem);
            
            try
            {
                rbt = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));

                bw.write("Unparsed entities from the file \"");
                bw.write(robotProgramName);
                bw.write("\":\n\n"); 

                errCode = getTargets(rbt);
                rbt.close();

                rbt = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));
                errCode = processRobotProgramLines(rbt, activityListElem);
                if (errCode != 0)
                    return errCode;
            }//end try

            catch (IOException e) {
                try {
                    bw.write("ERROR: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            return 0;
        }//end processRobotProgram

        private int getTargets(BufferedReader rbt)
        {
            String line;
            Matcher match;
            int errCode;
            
            try
            {
                line = rbt.readLine();
                while (line != null)
                {
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                        continue;
                    }
                    m_nIncr = 0;
                    boolean bStatus = isLineNumPresent(line);
                    if (bStatus == true)
                        line = preProcessLine(line);
                    
                    for (int ii=0; ii<m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);
                        if (match.find() == true)
                        {
                            switch (ii)
                            {
                                case 40:
                                    getPositionConst(line);
                                    break;
                                case 41:
                                    getJointConst(line);
                                    break;
                            }
                            break; // match found & processed, break out FOR loop
                        } // end IF match found
                    } // enf FOR loop
                    
                    line = rbt.readLine();
                } // end WHILE loop
            } // end TRY
            catch (IOException e)
            {
                e.getMessage();
                return 1;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                e.getMessage();
                return 1;
            }
            
            return 0;
        } // getTargets
        
        private int processRobotProgramLines(BufferedReader rbt, Element activityListElem)
        {
            String line, attrName, sOptions;
            Matcher match;
            Element attributeListElem;
            int errCode, offset, iMatchEnd;

            try
            {
                line = rbt.readLine();
                while (line != null)
                {
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                        continue;
                    }
                    
                    m_nIncr = 0;
                    boolean bStatus = isLineNumPresent(line);
                    if (bStatus == true)
                        line = preProcessLine(line);
                    
                    for (int ii = 0; ii < m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);
                        if (match.find() == true)
                        {
                            iMatchEnd = match.end();
                            sOptions = line.substring(iMatchEnd);
                            //Call appropriate methods to handle the input
                            switch (ii)
                            {
                                case 0: // Ovrd percentageSpeed; store value, create MotionProfile before the next motion command
                                    m_Ovrd = Float.parseFloat(match.group(1));
                                    processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 1: // JOvrd percentageSpeed; store value, create MotionProfile before the next motion command
                                    m_JOvrd = Float.parseFloat(match.group(1));
                                    break;
                                case 2: // Spd absoluteSpeed; store value, create MotionProfile before the next motion command
                                    m_Spd = Float.parseFloat(match.group(1));
                                    break;
                                case 3: // Accel accelerationRate; store value, create MotionProfile before the next motion command
                                    if (match.groupCount() > 0 && match.group(1) != null)
                                    {
                                        m_Accel = Float.parseFloat(match.group(1));
                                        processComment(sOptions, activityListElem, "Accel:Options");
                                    }
                                    else
                                        m_Accel = 100; // 100 if omitted
                                    break;
                                case 9: // End (of command section)
                                    break;
                                case 10: // Cnt 0|1[, 25[, 50]]
                                    if (match.group(1).equals("0")) // Cnt 0
                                        m_Accuracy = new Float(0.0);
                                    else // Cnt 1[, 25[, 50]]
                                        if (match.groupCount() > 1)
                                            if (match.group(2) == null)
                                                m_Accuracy = null;
                                            else
                                                m_Accuracy = new Float(match.group(2));
                                        processComment(sOptions, activityListElem, "Cnt:Options");
                                    break;
                                case 12: // Tool PTool1
                                    if (0 != processTool(match.group(1)))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 13: // Tool (x,y,z,a,b,c)
                                    processToolConst(match);
                                    break;
                                case 21: processSignalOut(line, match, activityListElem);
                                    break;
                                case 22: processDelay(match.group(1), activityListElem);
                                    break;
                                case 23: processCall(match, activityListElem, sOptions);
                                    break;
                                case 24: processWait(line, match, activityListElem);
                                    break;
                                case 30:
                                    if (0 != processMove(match, activityListElem, sOptions))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 50: processHClose(match, activityListElem);
                                    break;
                                case 51: processHOpen(match, activityListElem);
                                    break;
                                case 90: processComment(match.group(1).trim(), activityListElem, "Comment");
                                    break;
                            }//end switch

                            //match found & processed, break out for loop
                            break;
                        } // end if match found
                        else
                        {
                            if (ii == (m_keywords.length - 1))
                            {
                                processComment(line, activityListElem, "Robot Language");
                            }
                        } // end if match NOT found
                    } // end for keywords
                    
                    /* AFTER processing the END line(add PostComment)
                     * skip EndOfFile characters added by some system
                     */
                    if (line.equalsIgnoreCase("END"))
                        break;
                    
                    line = rbt.readLine();

                } // end while
                rbt.close();
            } // end try

            catch (IOException e) {
                e.getMessage();
                return 1;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-The number of joints of the Robot does NOT match the program being uploaded.\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            
            addAttributeList(activityListElem, "Post");
            
            return 0;
        } // end of processRobotProgramLines

        private void getJointConst(String line)
        {
            //J1=(1, 2, 3, 4, 5, 6, 7, 8)
            String sFlt  = "((?:\\+|\\-)?\\d*\\.?\\d+|\\d+\\.?\\d*)?";
            //                  J1        =      (        J1          ,        J2          ,        J3          ,        J4             ,          J5             ,          J6             ,          J7             ,          J8            )
            String sJntDef = "^(J\\w+)\\s*=\\s*\\(\\s*" + sFlt + "\\s*,\\s*" + sFlt + "\\s*,\\s*" + sFlt + "\\s*,\\s*" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "\\s*\\)";
            Pattern p = Pattern.compile(sJntDef, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(line);
            if (m.find()==false)
                return;
            String jVar = m.group(1).toUpperCase();
            String [] jConst = {"0", "0", "0", "0", "0", "0", "0", "0"};
            for (int jj=1; jj<9; jj++)
            {
                if (m.group(jj+1) != null)
                    jConst[jj-1] = m.group(jj+1);
            }
            m_jointVariableTbl.put(jVar, jConst);
        } // end of getJointConst

        // Position constant or Tool constant
        // P1|PTool1 = (1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0) (0,0)
        private void getPositionConst(String line)
        {
            String sFlt = "((?:\\+|\\-)?(?:\\d*\\.?\\d+|\\d+\\.?\\d*))?"; // may be ommited
            String flag = "([0-7]|&H\\p{XDigit}+|&B[01]+)?";
            //                  P1        =      (        X           ,        Y              ,          Z              ,          A              ,          B              ,          C              ,          L1             ,          L2            )       (                    ,                      )
            String sPosDef = "^(P\\w+)\\s*=\\s*\\(\\s*" + sFlt + "\\s*,\\s*" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "(?:\\s*,\\s*)?" + sFlt + "\\s*\\)\\s*(\\(\\s*" + flag + "\\s*,\\s*" + flag + "\\s*\\))?";
            Pattern p = Pattern.compile(sPosDef, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(line);
            if (m.find()==false)
                return;
            String pVar = m.group(1).toUpperCase();
            String [] pConst = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};
            String sGrp;
            for (int jj=1; jj<11; jj++)
            {
                if (jj > 8)
                    sGrp = m.group(jj+2); // groups 11,12; for Tools, the last pair of "()" is optional
                else
                    sGrp = m.group(jj+1); // groups 2,3,4, 5,6,7, 8,9
                if (sGrp != null)
                    pConst[jj-1] = sGrp;
            }
            m_positionVariableTbl.put(pVar, pConst);
            
            if (pVar.startsWith("PTOOL") && !pVar.startsWith("PTOOLCONSTANT") || pVar.startsWith("PTL"))
            {
                int toolNum = getVarNumber(pVar);
                // set tool counter to be larger than defined tool constants
                if (m_toolCounter <= toolNum)
                    m_toolCounter = toolNum + 1;
            }
        } // end of getPositionConst

        // TOOL PTool2
        private int processTool(String toolName)
        {
            toolName = toolName.trim().toUpperCase();
            double [] cartValues = {0, 0, 0, 0, 0, 0};
            double [] auxValues = {0, 0};
            String [] flags = {"0", "0"};
            if (getPosValues(toolName, cartValues, auxValues, flags) == 1)
                return 1;

            // check if the tool profile is already created
            Element toolProfileList = super.getToolProfileListElement();
            NodeList toolProfiles = toolProfileList.getChildNodes();
            Element profile, name, tcpOffset, tcpPos, tcpOri;
            double [] toolValues = {0, 0, 0, 0, 0, 0};
            double tmp = 0;
            for (int ii=0; ii<toolProfiles.getLength(); ii++)
            {
                profile = (Element)toolProfiles.item(ii);
                name = kku.findChildElemByName(profile, "Name", null, null);
                if (name.getFirstChild().getNodeValue().equals(toolName))
                {
                    // This tool profile is already created
                    // check if they have the same values
                    tcpOffset = kku.findChildElemByName(profile, "TCPOffset", null, null);
                    tcpPos = kku.findChildElemByName(tcpOffset, "TCPPosition", null, null);
                    tcpOri = kku.findChildElemByName(tcpOffset, "TCPOrientation", null, null);
                    toolValues[0] = Float.parseFloat(tcpPos.getAttribute("X"));
                    toolValues[1] = Float.parseFloat(tcpPos.getAttribute("Y"));
                    toolValues[2] = Float.parseFloat(tcpPos.getAttribute("Z"));
                    toolValues[3] = Float.parseFloat(tcpOri.getAttribute("Yaw"));
                    toolValues[4] = Float.parseFloat(tcpOri.getAttribute("Pitch"));
                    toolValues[5] = Float.parseFloat(tcpOri.getAttribute("Roll"));
                    for (int jj=0; jj<6; jj++)
                        tmp += (cartValues[jj] - toolValues[jj])*(cartValues[jj] - toolValues[jj]);
                    if (tmp < 0.000001) // same values
                    {
                        m_toolProfileName = toolName;
                        return 0;
                    }
                    int toolNum = 1;
                    String newToolName = toolName + "_" + toolNum;
                    while (ToolNameUsed(newToolName)==true)
                    {
                        toolNum++;
                        newToolName = toolName + "_" + toolNum;
                    }
                    toolName = newToolName;
                    try {
                        bw.write("Tool \"" + toolName + "\" is defined differently in subprogram \"" + m_program + "\".\n");
                        bw.write("Tool \"" + toolName + "\" is renamed to \"" + newToolName + "\".\n");
                    }
                    catch (IOException bwe) {
                    }
                    break; // FOR loop
                }
            }
            
            DNBIgpOlpUploadEnumeratedTypes.ToolType toolType;
            toolType = DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT;
            super.createToolProfile(toolProfileList, toolName, toolType, cartValues, null, null, null);
            m_toolProfileName = toolName;
            
            return 0;
        } // processTool
        
        // TOOL (x,y,z,a,b,c)
        private int processToolConst(Matcher match)
        {
            String toolName = "PTOOLCONSTANT" + m_toolCounter;
            String line = toolName + "=" + match.group(1);
            getPositionConst(line); // store to m_positionVariableTbl

            double [] cartValues = {0, 0, 0, 0, 0, 0};
            double [] auxValues = {0, 0};
            String [] flags = {"0", "0"};
            if (getPosValues(toolName, cartValues, auxValues, flags) == 1)
                return 1;

            Element toolProfileList = super.getToolProfileListElement();
            NodeList toolProfiles = toolProfileList.getChildNodes();
            Element profile, name, tcpOffset, tcpPos, tcpOri;
            double [] toolValues = {0, 0, 0, 0, 0, 0};
            double tmp = 0;
            for (int ii=0; ii<toolProfiles.getLength(); ii++)
            {
                profile = (Element)toolProfiles.item(ii);
                tcpOffset = kku.findChildElemByName(profile, "TCPOffset", null, null);
                tcpPos = kku.findChildElemByName(tcpOffset, "TCPPosition", null, null);
                tcpOri = kku.findChildElemByName(tcpOffset, "TCPOrientation", null, null);
                toolValues[0] = Float.parseFloat(tcpPos.getAttribute("X"));
                toolValues[1] = Float.parseFloat(tcpPos.getAttribute("Y"));
                toolValues[2] = Float.parseFloat(tcpPos.getAttribute("Z"));
                toolValues[3] = Float.parseFloat(tcpOri.getAttribute("Yaw"));
                toolValues[4] = Float.parseFloat(tcpOri.getAttribute("Pitch"));
                toolValues[5] = Float.parseFloat(tcpOri.getAttribute("Roll"));
                for (int jj=0; jj<6; jj++)
                    tmp += (cartValues[jj] - toolValues[jj])*(cartValues[jj] - toolValues[jj]);
                if (tmp < 0.000001)
                {
                    name = kku.findChildElemByName(profile, "Name", null, null);
                    m_toolProfileName = name.getFirstChild().getNodeValue();
                        
                    return 0;
                }
            }

            DNBIgpOlpUploadEnumeratedTypes.ToolType toolType;
            toolType = DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT;
            super.createToolProfile(toolProfileList, toolName, toolType, cartValues, null, null, null);
            m_toolProfileName = toolName;
            m_toolCounter++;
            
            return 0;
        } // processToolConst

        private boolean ToolNameUsed(String toolName)
        {
            Element toolProfileList = super.getToolProfileListElement();
            NodeList toolProfiles = toolProfileList.getChildNodes();
            Element profile, name;
            for (int ii=0; ii<toolProfiles.getLength(); ii++)
            {
                profile = (Element)toolProfiles.item(ii);
                name = kku.findChildElemByName(profile, "Name", null, null);
                if (name.getFirstChild().getNodeValue().equals(toolName))
                    return true;
            }
            
            return false;
        }
        
        private int getPosValues(String posName, double [] cartValues, double [] auxValues, String [] flags)
        {
            String [] sValues = (String []) m_positionVariableTbl.get(posName);
            if (sValues == null)
            {
                try {
                    bw.write("Position variable " + posName + " is not defined.\n");
                }
                catch (IOException bwe) {
                }

                return 1;
            }
            
            for (int ii=0; ii<6; ii++)
            {
                cartValues[ii] = Double.valueOf(sValues[ii]).doubleValue();
                if (ii<3)
                    cartValues[ii] *= 0.001;
            }
            
            auxValues[0] = Double.valueOf(sValues[6]).doubleValue();
            auxValues[1] = Double.valueOf(sValues[7]).doubleValue();

            flags[0] = sValues[8];
            flags[1] = sValues[9];
            
            return 0;
        }
        
        private int getVarNumber(String varName)
        {
            varName = varName.trim();
            int strLen = varName.length();
            char [] charArray = varName.toCharArray();
            Character CharObj = new Character('9');
            int ii = strLen - 1;
            while (CharObj.isDigit(charArray[ii]) == true)
                ii--;
            if (ii == strLen - 1)
                return -1;
            else
                return Integer.parseInt(varName.substring(ii+1));
        }
        
        // M_OUT(1)=0
        private void processSignalOut(String line, Matcher match, Element activityListElem)
        {
            String sPortNumber = match.group(1);
            String sStatus = match.group(2);
            String sDuration = match.group(3);
            
            boolean bSignalValue = false;
            if (sStatus.equals("1"))
                bSignalValue = true;
            
            Integer iPortNumber = Integer.valueOf(sPortNumber);
            String sSignalName = ioName + sPortNumber;
            if (ioName.endsWith("["))
                sSignalName += "]";
            
            Double dTime = new Double(0);
            if (sDuration != null)
                dTime = new Double(sDuration);
            Element actElem = super.createSetIOActivity(activityListElem, line, bSignalValue, sSignalName, iPortNumber, dTime);
            
            addAttributeList(actElem, "Pre");
        }

        // Wait M_In(1)=0
        private void processWait(String line, Matcher match, Element activityListElem)
        {
            String sPortNumber = match.group(1);
            String sStatus = match.group(2);

            Integer iPortNumber = Integer.valueOf(sPortNumber);
            String sSignalName = ioName + sPortNumber;
            if (ioName.endsWith("["))
                sSignalName += "]";

            Double dTime = new Double(0);
            boolean bSignalValue = false;
            if (sStatus.endsWith("1"))
                bSignalValue = true;
            if (m_commentWaitSignal == false)
            {
                Element actElem = super.createWaitForIOActivity(activityListElem, line, bSignalValue, sSignalName, iPortNumber, dTime);
                addAttributeList(actElem, "Pre");
            }
            else
            {
                processComment(line, activityListElem, "Robot Language");
            }
        } // end of processWait

        private int processMove(Matcher match, Element activityListElem, String sOptions)
        {
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype;
            String sMoType = match.group(1);
            String sTarget = match.group(2).toUpperCase();
            if (sMoType.equalsIgnoreCase("MOV"))
            {
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
                if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, true)!=0)
                    return 1;
            }
            else
            {
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                if (sMoType.equalsIgnoreCase("MVS"))
                {
                    if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, true)!=0)
                        return 1;
                }
                else
                {
                    if (sMoType.equalsIgnoreCase("MVR"))
                    {
                        // Linear move
                        if (createRobotMotion(activityListElem, eMotype, sTarget, "", true)!=0)
                            return 1;

                        // Circular Via move
                        eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
                        sTarget = match.group(9).toUpperCase();
                        if (createRobotMotion(activityListElem, eMotype, sTarget, "", false)!=0)
                            return 1;

                        // Circular move
                        eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
                        sTarget = match.group(16).toUpperCase();
                        if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, true)!=0)
                            return 1;
                    }
                }
            }


            return 0;
        } // processMove
        
        private int createRobotMotion(
                        Element activityListElem,
                        DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype,
                        String sTarget,
                        String sOptions,
                        boolean bAddAttributeList)
        {
            // MotionProfile name
            String motionProfileName = "Default";
            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
            float fSpd;
            if (eMotype == DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION)
            {
                moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
                fSpd = m_Ovrd / 100 * m_JOvrd;
            }
            else
            {
                moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
                fSpd = m_Ovrd / 100 * m_Spd / 1000;
            }
            motionProfileName = getOrCreateMotionProfile(moBasis, fSpd, m_Accel);
            
            // ObjectProfile name
            String objectProfileName = "Default";
            
            // AccuracyProfile name
            String accuracyProfileName = getOrCreateAccuracyProfile(m_Accuracy);

            DNBIgpOlpUploadEnumeratedTypes.OrientMode eOrientType;
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS;
            if (m_orientMode.equalsIgnoreCase("1_axis") == true)
                eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS;
            if (m_orientMode.equalsIgnoreCase("3_axis") == true)
                eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.THREE_AXIS;
            if (m_orientMode.equalsIgnoreCase("wrist") == true)
                eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.WRIST;

            String sActivityName = "RobotMotion." + m_moveCounter;
            Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);
            Element moAttrElem = super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, m_toolProfileName, objectProfileName, eMotype, eOrientType);

            sTarget = sTarget.toUpperCase();
            String tagName;
            if (sTarget.startsWith("P") || sTarget.startsWith("J"))
            {
                tagName = sTarget;
            }
            else
            {
                tagName = "PositionConstant";
                String posVarDefLine = tagName + "=" + sTarget;
                getPositionConst(posVarDefLine);
            }
            if (createTarget(tagName, actElem) != 0)
                return 1;
            m_moveCounter++;
            
            if (sTarget.startsWith("P") || sTarget.startsWith("J"))
                addAttribute(actElem, "TargetName", sTarget);

            if (sOptions != null && sOptions.length() > 0)
                processComment(sOptions, activityListElem, "Options");
            
            if (bAddAttributeList)
                addAttributeList(actElem, "Pre");

            return 0;
        } // createRobotMotion
        
        private void processDelay(String sDelayTime, Element activityListElem)
        {
            Double dStartTime = new Double(0.0);
            Double dEndTime = Double.valueOf(sDelayTime);
            String sActivityName = "RobotDelay." + m_delayCounter;

            Element actElem = super.createDelayActivity(activityListElem, sActivityName, dStartTime, dEndTime);

            m_delayCounter++;
            
            addAttributeList(actElem, "Pre");
        } // end of processDelay

        private void processComment(String line, Element activityListElem, String attributeName)
        {
            String comment = line;
            if (attributeName.endsWith("Options") && comment.length() == 0)
                return;
            
            if (attributeName.endsWith("Options"))
            {
                comment = attributeName + ":" + comment;
                commentLines.add(commentCount, comment);
                commentCount++;
            }
            else if (uploadStyle.equalsIgnoreCase("OperationComments"))
            {
                //Create DOM Nodes and set appropriate attributes
                String actName = "Comment.1";
                if (attributeName.equals("Comment"))
                    actName = attributeName + '.' + operationCommentCounter++;
                else
                if (attributeName.equals("Robot Language"))
                    actName = attributeName + '.' + m_rbtLangCounter++;
                Element actElem = super.createActivityHeader(activityListElem, actName, "Operation");

                String [] attrNames = {attributeName};
                String [] attrValues = {comment};
                Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
            }
            else
            {
                if (attributeName.equals("Robot Language"))
                    comment = "Robot Language:" + comment;
                commentLines.add(commentCount, comment);
                commentCount++;
            }
        } // end of processComment

        private void addAttributeList(Element actElem, String prefix)
        {
            if (commentCount == 0)
                return;

            String [] attrNames = new String[commentCount];
            String [] attrValues = new String[commentCount];
            for (int ii=0; ii<commentCount; ii++)
            {
                attrNames[ii] = prefix + "Comment" + String.valueOf(ii+1);
                attrValues[ii] = (String)commentLines.get(ii);
            }

            Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
            commentCount = 0;
        } // end addAttributeList

        private void addAttribute(Element actElem, String attrName, String attrVal)
        {
            String [] attrNames = {attrName};
            String [] attrValues = {attrVal};
            Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
        } // end addAttribute

        private int createTarget(String sTarget, Element actElem)
        {
            int numOfDOF = m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes;
            int numOfAuxDOF = m_numAuxAxes+m_numExtAxes+m_numWorkAxes;
            int ii;
            sTarget = sTarget.toUpperCase();
            if (sTarget.startsWith("P"))
            {
                double [] cartValues = {0, 0, 0, 0, 0, 0};
                double [] auxValues = {0, 0};
                String [] flags = {"0", "0"};
                if (getPosValues(sTarget, cartValues, auxValues, flags) == 1)
                    return 1;
                
                String sFL1 = flags[0];
                int radix = 10;
                if (sFL1.startsWith("&B"))
                {
                    radix = 2;
                    sFL1 = sFL1.substring(2);
                }
                int MitsubishiCFG = Integer.parseInt(sFL1, radix);
                
                String cfgName = "Config_1";
                if (m_numRobotAxes == 4)
                {
                    switch (MitsubishiCFG)
                    {
                        case 0: cfgName = "Config_1"; break;
                        case 4: cfgName = "Config_2"; break;
                    }
                }
                else
                {
                    switch (MitsubishiCFG)
                    {
                        case 7: cfgName = "Config_1"; break;
                        case 6: cfgName = "Config_2"; break;
                        case 5: cfgName = "Config_3"; break;
                        case 4: cfgName = "Config_4"; break;
                        case 1: cfgName = "Config_5"; break;
                        case 0: cfgName = "Config_6"; break;
                        case 3: cfgName = "Config_7"; break;
                        case 2: cfgName = "Config_8"; break;
                    }
                }
                
                Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, false);
                Element cartTargetElem = super.createCartesianTarget(targetElem, cartValues, cfgName);

                int [] turnNumbers = {0, 0, 0, 0}; // DELMIA turn number {J1, J4, J5, J6}
                String sFL2 = flags[1];
                int numDigits = sFL2.length();
                // FL2 joint list start from aux axes 8 and 7, then joint 6 to 1
                // FL2: &H87654321
                radix = 10;
                if (sFL2.startsWith("&H"))
                    radix = 16;
                
                // Flag2 must have at least one digit for Joint1, 1(>180) or F(<-180)
                int axis = 0;
                int turnNum = Integer.parseInt(sFL2.substring(numDigits-axis-1, numDigits-axis), radix);
                if (turnNum > 7)
                    turnNum = turnNum - 16;
                turnNumbers[axis] = turnNum;
                
                if (sFL2.startsWith("&H"))
                {
                    // skip joints 2 and 3, do joints 4, 5, and 6
                    for (axis=3; axis<6; axis++)
                    {
                        turnNum = Integer.parseInt(sFL2.substring(numDigits-axis-1, numDigits-axis), radix);
                        if (turnNum > 7)
                            turnNum = turnNum - 16;
                        turnNumbers[axis-2] = turnNum;
                    }
                }
                super.createTurnNumbers(cartTargetElem, turnNumbers);
                if (sTarget.equalsIgnoreCase("PositionConstant")==false)
                    super.createTag(cartTargetElem, sTarget);
                // If auxiliary axes exist, create elements JointTarget/AuxJoint
                if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)
                {
                    /* when there are aux axes in Mitsubishi programs, they are number 7 and 8
                     * ii: DELMIA joint number; mRobotAxis: Mitsubishi joint number
                     */
                    int mRobotAxis;
                    ArrayList [] oaJointTarget = new ArrayList [numOfAuxDOF];
                    for (ii=0; ii<numOfAuxDOF; ii++)
                        oaJointTarget[ii] = new ArrayList(1);
                    
                    if (m_numAuxAxes > 0)
                        for (ii=0; ii<m_numAuxAxes; ii++)
                            setJointArrayList(auxValues[ii], "RailTrackGantry", ii+m_numRobotAxes, oaJointTarget[ii]);

                    if (m_numExtAxes > 0)
                        for (ii=m_numAuxAxes; ii<m_numAuxAxes+m_numExtAxes; ii++)
                            setJointArrayList(auxValues[ii], "EndOfArmTooling", ii+m_numRobotAxes, oaJointTarget[ii]);

                    if (m_numWorkAxes > 0)
                        for (ii=m_numAuxAxes+m_numExtAxes; ii<numOfAuxDOF; ii++)
                            setJointArrayList(auxValues[ii], "WorkpiecePositioner", ii+m_numRobotAxes, oaJointTarget[ii]);

                    super.createJointTarget(targetElem, oaJointTarget);
                }
            }

            if (sTarget.startsWith("J"))
            {
                String [] sTargetComponents;
                sTargetComponents = (String []) m_jointVariableTbl.get(sTarget);
                if (sTargetComponents == null)
                {
                    try {
                        bw.write("Joint variable " + sTarget + " is not defined.\n");
                    }
                    catch (IOException bwe) {
                    }
                    
                    return 1;
                }

                double [] jval = {0, 0, 0, 0, 0, 0, 0, 0};
                for (ii=0; ii<8; ii++)
                    jval[ii] = Double.parseDouble(sTargetComponents[ii]);
                
                ArrayList [] oaJointTarget = new ArrayList[numOfDOF];
                for (ii=0; ii<numOfDOF; ii++)
                    oaJointTarget[ii] = new ArrayList(1);

                if (m_numRobotAxes == 5)
                {
                    for (ii=0; ii<3; ii++)
                        setJointArrayList(jval[ii], "Robot", ii, oaJointTarget[ii]);

                    /* Mitsubishi 5-axis robot Joint Constants has a dummy J4 value
                     * assign Mitsubishi Joint ii+1 value to DELMIA Joint ii
                     */
                    for (ii=3; ii<5; ii++)
                        setJointArrayList(jval[ii+1], "Robot", ii, oaJointTarget[ii]);
                }
                else
                {
                    for (ii=0; ii<m_numRobotAxes; ii++)
                        setJointArrayList(jval[ii], "Robot", ii, oaJointTarget[ii]);
                }
                
                /* when there are aux axes in Mitsubishi programs, they are number 7 and 8
                 * ii: DELMIA joint number; mRobotAxis: Mitsubishi joint number
                 */
                int mRobotAxis;
                if (m_numAuxAxes > 0 && m_numAuxAxes < numOfAuxDOF + 1)
                {
                    for (ii=m_numRobotAxes; ii<m_numRobotAxes+m_numAuxAxes; ii++)
                    {
                        mRobotAxis = 6 + ii - m_numRobotAxes;
                        setJointArrayList(jval[mRobotAxis], "RailTrackGantry", ii, oaJointTarget[ii]);
                    }
                }

                if (m_numExtAxes > 0 && m_numExtAxes < numOfAuxDOF - m_numAuxAxes + 1)
                {
                    for (ii=m_numRobotAxes+m_numAuxAxes; ii<m_numRobotAxes+m_numAuxAxes+m_numExtAxes; ii++)
                    {
                        mRobotAxis = 6 + ii - m_numRobotAxes;
                        setJointArrayList(jval[mRobotAxis], "EndOfArmTooling", ii, oaJointTarget[ii]);
                    }
                }

                if (m_numWorkAxes > 0 && m_numWorkAxes < numOfAuxDOF - m_numAuxAxes - m_numExtAxes + 1)
                {
                    for (ii=m_numRobotAxes+m_numAuxAxes+m_numExtAxes; ii<numOfDOF; ii++)
                    {
                        mRobotAxis = 6 + ii - m_numRobotAxes;
                        setJointArrayList(jval[mRobotAxis], "WorkpiecePositioner", ii, oaJointTarget[ii]);
                    }
                }

                Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false);
                Element jointTargetElem = super.createJointTarget(targetElem, oaJointTarget);
            }
            return 0;
        } //end createTarget method

        private void processCall(Matcher match, Element activityListElem, String sOptions)
        {
            String taskNameStr = match.group(1);
            String callNameStr = taskNameStr + ".prg";

            String activityName = "RobotCall." + robotCallNumber++;
            Element actElem = super.createCallTaskActivity(activityListElem, activityName, taskNameStr);

            int index = programCallNames.indexOf(callNameStr);
            if (index < 0)
            {
                programCallNames.add(programCallCount, callNameStr);
                programCallCount++;
            }
            
            processComment(sOptions, activityListElem, "Options");
            addAttributeList(actElem, "Pre");
        } // end of processCallJobStatement

        private void processHClose(Matcher match, Element activityListElem)
        {
            String spotPickActionName = "MitsubishiHClose." + m_spotPickCounter;
            Element actionElem = super.createActionHeader(activityListElem, spotPickActionName, "MitsubishiHClose", m_mountedTool);
            String [] attrNames = {"HandNo."};
            String [] attrValues = {match.group(1)};
            String [] attrTypes = {"integer"};
            super.createAttributeList(actionElem, attrNames, attrValues, attrTypes);
            
            Element spotPickActivityListElem = super.createActivityListWithinAction(actionElem);
            
            String activityName = "RobotMotion." + m_handMoveCounter;
            Element motionActivity = super.createMotionActivityWithinAction(spotPickActivityListElem, activityName, 
                                                Double.valueOf("0.0"), Double.valueOf("1.0"),
                                                DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,
                                                m_pickHome);
            activityName = "GrabActivity." + m_spotPickCounter;
            if (m_grabPart.length() == 0)
                m_grabPart = m_mountedTool;
            Element grabActivity = super.createGrabActivity(spotPickActivityListElem, activityName, m_grabPart, m_partGrabbed);

            m_handMoveCounter++;
            m_spotPickCounter++;
        }

        private void processHOpen(Matcher match, Element activityListElem)
        {
            String spotDropActionName = "MitsubishiHOpen." + m_spotDropCounter;
            Element actionElem = super.createActionHeader(activityListElem, spotDropActionName, "MitsubishiHOpen", m_mountedTool);
            String [] attrNames = {"HandNo.", "StartGraspForce", "HoldGraspForce", "StartGraspForceHoldTime"};
            String [] attrValues = {"", "", "", ""};
            for (int ii=1; ii<5; ii++)
                if (match.group(ii) != null)
                    attrValues[ii-1] = match.group(ii);
            String [] attrTypes = {"integer", "string", "string", "string"};
            super.createAttributeList(actionElem, attrNames, attrValues, attrTypes);
            
            Element spotDropActivityListElem = super.createActivityListWithinAction(actionElem);
            
            String activityName = "RobotMotion." + m_handMoveCounter;
            Element motionActivity = super.createMotionActivityWithinAction(spotDropActivityListElem, activityName, 
                                                Double.valueOf("0.0"), Double.valueOf("1.0"),
                                                DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,
                                                m_dropHome);
            activityName = "ReleaseActivity." + m_spotDropCounter;
            Element releaseActivity = super.createReleaseActivity(spotDropActivityListElem, activityName, m_partGrabbed);

            m_handMoveCounter++;
            m_spotDropCounter++;
        }
        
        private String getToolName()
        {
            String toolName = null;

            if (p_currentToolNumber < p_toolProfileNames.length)
                toolName = p_toolProfileNames[p_currentToolNumber];

            if (toolName == null)
                toolName = "Default";

            return toolName;
        } // end of getToolName
        
        public void setJointArrayList(double jVal, String robotOrAuxJointType, int iJoint, ArrayList jTarget)
        {
            DNBIgpOlpUploadEnumeratedTypes.DOFType jType;
            if (m_axisTypes[iJoint].equals("Rotational"))
            {
                jVal = Math.toRadians(jVal);
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL;
            }
            else
            {
                jVal = jVal * 0.001;
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL;
            }

            String jName = "Joint " + (iJoint + 1);
            jTarget.add(0, jName); // set joint name
            jTarget.add(1, new Double(jVal)); // set joint value
            jTarget.add(2, new Integer(iJoint + 1)); // set joint number
            jTarget.add(3, jType); // set joint type(rotational or translationsl)

            if (robotOrAuxJointType.equals("RailTrackGantry") ||
                robotOrAuxJointType.equals("EndOfArmTooling") ||
                robotOrAuxJointType.equals("WorkpiecePositioner"))
            {
                DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxType;
                auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                if (robotOrAuxJointType.equals("RailTrackGantry"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                else
                if (robotOrAuxJointType.equals("EndOfArmTooling"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING;
                else
                if (robotOrAuxJointType.equals("WorkpiecePositioner"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER;

                jTarget.add(4, auxType);
            }
        } // end of setJointArrayList

        public String getOrCreateMotionProfile(DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis, float fSpd, float fAccel)
        {
            //<MotionProfile>
                //<Name>Default</Name>
                //<MotionBasis>Percent|Absolute</MotionBasis>
                //<Speed Units="%"|"m/s" Value="50" />
                //<Accel Units="%" Value="100" />
                //<AngularSpeedValue Units="%" Value="100" />
                //<AngularAccelValue Units="%" Value="100" />
            //</MotionProfile>

            String profileName;
            Element motionProfileList = super.getMotionProfileListElement();
            NodeList moProfiles = motionProfileList.getChildNodes();
            Element profile, name, basis, speed, accel;
            float speedVal, accelVal;
            String basisVal;
            DNBIgpOlpUploadEnumeratedTypes.MotionBasis enumBasisVal;
            for (int ii=0; ii<moProfiles.getLength(); ii++)
            {
                profile = (Element)moProfiles.item(ii);
                basis = kku.findChildElemByName(profile, "MotionBasis", null, null);
                speed = kku.findChildElemByName(profile, "Speed", null, null);
                accel = kku.findChildElemByName(profile, "Accel", null, null);
                basisVal = basis.getFirstChild().getNodeValue();
                if (basisVal.equals("Percent"))
                    enumBasisVal = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
                else
                    enumBasisVal = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
                speedVal = Float.parseFloat(speed.getAttribute("Value"));
                accelVal = Float.parseFloat(accel.getAttribute("Value"));
                if (enumBasisVal == moBasis && Math.abs(speedVal-fSpd)<0.001 && Math.abs(accelVal-fAccel)<0.001)
                {
                    name = kku.findChildElemByName(profile, "Name", null, null);
                    profileName = name.getFirstChild().getNodeValue();
                    return profileName;
                }
            }

            if (moBasis == DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT)
                profileName = "JMotionProfile." + m_motionProfileCounter;
            else
                profileName = "LMotionProfile." + m_motionProfileCounter;
            Element motionProfileElem = super.createMotionProfile(motionProfileList, profileName, moBasis, fSpd, fAccel, 100.0, 100.0);
            m_motionProfileCounter++;
            return profileName;
        } // end of getOrCreateMotionProfile

        public String getOrCreateAccuracyProfile(Float Accuracy)
        {
            //<AccuracyProfile>
                //<Name>Default</Name>
                //<FlyByMode>Off</FlyByMode>
                //<AccuracyType>Speed</AccuracyType>
                //<AccuracyValue Units="%" Value="0" />
            //</AccuracyProfile>

            float accuracy;
            // set accuracy type, accuracy value, and flyby mode
            double accuracyValue = 0.0;
            boolean flyByMode = true;
            DNBIgpOlpUploadEnumeratedTypes.AccuracyType accuracyType;
            accuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE;
            if (Accuracy == null)
            {
                accuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED;
                accuracyValue = 100.0; // percentage
            }
            else
            {
                accuracy = Accuracy.floatValue();
                if (accuracy == 0)
                    flyByMode = false;
                else
                    accuracyValue = accuracy / 1000.0; // unit Meter
            }

            String profileName;
            Element accuracyProfileList = super.getAccuracyProfileListElement();
            // check if accuracy profile of the type, value, and flyby mode exists
            NodeList accuracyProfiles = accuracyProfileList.getChildNodes();
            Element profile, name, accuracyTypeElem, accuracyValueElem, flyByModeElem;
            boolean profileFlyByMode = true;
            float profileAccuracyValue;
            DNBIgpOlpUploadEnumeratedTypes.AccuracyType profileAccuracyType;
            profileAccuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE;
            for (int ii=0; ii<accuracyProfiles.getLength(); ii++)
            {
                profile = (Element)accuracyProfiles.item(ii);
                flyByModeElem = kku.findChildElemByName(profile, "FlyByMode", null, null);
                if (flyByModeElem.getFirstChild().getNodeValue().equals("Off"))
                    profileFlyByMode = false;
                else
                    profileFlyByMode = true;

                accuracyTypeElem = kku.findChildElemByName(profile, "AccuracyType", null, null);
                if (accuracyTypeElem.getFirstChild().getNodeValue().equals("Speed"))
                    profileAccuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED;
                else
                    profileAccuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE;

                accuracyValueElem = kku.findChildElemByName(profile, "AccuracyValue", null, null);
                profileAccuracyValue = Float.parseFloat(accuracyValueElem.getAttribute("Value"));
                if (flyByMode == profileFlyByMode && accuracyType == profileAccuracyType && Math.abs(accuracyValue-profileAccuracyValue) < 0.01)
                {
                    name = kku.findChildElemByName(profile, "Name", null, null);
                    profileName = name.getFirstChild().getNodeValue();
                    return profileName;
                }
            }
            
            if (accuracyType == DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED)
                profileName = "VAccuracy." + m_accuracyProfileCounter;
            else
                profileName = "DAccuracy." + m_accuracyProfileCounter;
            Element accuracyProfileElem = super.createAccuracyProfile(accuracyProfileList, profileName, accuracyType, flyByMode, accuracyValue);
            
            m_accuracyProfileCounter++;
            return profileName;
         } // end of getOrCreateAccuracyProfile

    // returns true if space and/or digit is in the front
	private boolean isLineNumPresent(String line)
	{
		String testStr = new String(line.trim());
		char [] charArray = testStr.toCharArray();

		Character CharObj = new Character(charArray[0]);

		int strLen = testStr.length();

		while( (CharObj.isDigit(charArray[m_nIncr]) == true) && (m_nIncr < strLen) )
		{
			m_nIncr = m_nIncr + 1;
		}

		if(m_nIncr == 0)
			return false;
		else
			return true;
	}

	private String preProcessLine(String line)
	{
		int strLen = line.trim().length();

		char [] charArray = line.toCharArray();
		String newStr = new String(charArray, m_nIncr, strLen-m_nIncr);

		newStr = newStr.trim();

		m_nIncr = 0;

		return newStr;
	}

}//end class
