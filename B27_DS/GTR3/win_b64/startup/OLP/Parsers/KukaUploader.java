//DOM and Parser classes
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.File;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

//Java Exception classes
import java.io.IOException;
import java.io.FileNotFoundException;
import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;

import Kuka.*;
import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class KukaUploader extends DNBIgpOlpUploadBaseClass {

        //Constants
        private static final int JOINT = 1;
        private static final int CARTESIAN = 2;      
        private static final String VERSION = "Delmia Corp. KUKA KRC Uploader Version 5 Release 20.\n";
        private static final String COPYRIGHT = "Copyright Delmia Corp. 1986-2009, All Rights Reserved.\n";
        private static final String DEFAULT = "Default";
        private static final String ktg = "_kukataggroup_";
        private static final String ktp = "_kukaattachedtopart_";
        private static final String numPatStr = "\\s+-?[0-9]+\\.?[0-9]*";
        private static final String auxRail = "RailTrackGantry";
        private static final String auxTool = "EndOfArmTooling";
        private static final String auxPos  = "WorkpiecePositioner";
        private static final int ktgL = ktg.length();
        private static final int ktpL = ktp.length();
        private static final float TO_DEG = 180 / new Float(Math.PI).floatValue();
        private static final float TO_RAD = new Float(Math.PI).floatValue() / 180;

        //Member primitive variables
        private int m_targetType;
        private int m_numCStatements;
        private int m_numRobotAxes;
        private int m_numAuxAxes;
        private int m_numExtAxes;
        private int m_numPositionerAxes;
        private String [] DauxAxisGrp={ auxRail, auxTool, auxPos };
        private String [] KauxAxisGrp={ auxRail, auxTool, auxPos };
        private int [] DauxAxisNum=new int[3];
        private int [] KauxAxisNum=new int[3];
        private int headerCount;
        private int commentCount;
        private int programCallCount;
        private int jobCount;
        private static boolean userProgStart;
        private static boolean processingUnrecog;

        //Member objects
        private Document m_xmlDoc;
        private String m_currentTagPoint;
        private String m_mountedTool;
        private Pattern [] m_keywords;
        private String [] m_axisTypes;
        private String [] k_toolProfileNames = new String [17];
        private String [] k_objProfileNames = new String [33];
        private String currMotionProfile = DEFAULT;
        private String currAccuracyProfile = DEFAULT;
        private String currToolProfile = DEFAULT;
        private String currObjFrameProfile = DEFAULT;
        private String svgNeg = "false";
        private String weldHome, closeHome, openHome;
        private String grpMidHome, grpCloseHome, grpOpenHome;
        private String configFile = null;
        private String machineFile = null;
        private static String railConfig = null;
        private String signalName = null;
        private String currentTask = null;
        private Hashtable m_tagNames;
        private Hashtable m_toolNumberMapping;
        private Hashtable m_objFrameNumberMapping;
        private ArrayList partIndex, baseNameIndex, toolNameIndex;
        private boolean [] basetoolSwapped;
        private ArrayList programCallNames;
        private ArrayList fileProgramList;
        private ArrayList globalProgramList;
        private ArrayList commentLines;
        private ArrayList inputNameNum, outputNameNum;
        private ArrayList jTgtName_arrList;
        private ArrayList jTgtAval_arrList;
        private ArrayList jTgtEval_arrList;

        private spot    kks;
        private util    kku;
        private servogun    kksvg;
        private gripper     kkgrp;
        private counters    kkc;
        private static boolean  retractAction = false;
        private static boolean  spotAction = false;
        private static boolean  [] kirAttribute;
        private String [] kirParent;
        private static boolean  [] kirBaseAttribute;
        private String [] kirMachine;
        private String [][] kirMachineBase;

        private String uploadStyle = "General";
        private static BufferedWriter bw;

        //Constructor
        public KukaUploader(String [] parameters) throws ParserConfigurationException
        /*
        (String [] parameterData, String [] profileNumbers, String [] axisTypes,
                            String [] numOfAxis, String [] toolProfileNames, String [] objProfileNames, String [] homeNames,
                            String mountedTool, Document xmlDoc)
         */
        {
            super(parameters);
            m_targetType = 0;
            m_numCStatements = 0;
            programCallCount = 0;
            headerCount = 0;
            commentCount = 0;
            jobCount = 0;
            
            m_mountedTool = parameters[6];
            String [] toolProfileNames = parameters[7].split("[\\t]+");
            String [] homeNames = parameters[9].split("\\t");
            String [] objProfileNames = parameters[11].split("[\\t]+");

            m_numRobotAxes = super.getNumberOfRobotAxes();
            m_numAuxAxes = super.getNumberOfRailAuxiliaryAxes();
            m_numExtAxes = super.getNumberOfToolAuxiliaryAxes();
            m_numPositionerAxes = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();
            DauxAxisNum[0] = m_numAuxAxes;
            DauxAxisNum[1] = m_numExtAxes;
            DauxAxisNum[2] = m_numPositionerAxes;
            KauxAxisNum[0] = m_numAuxAxes;
            KauxAxisNum[1] = m_numExtAxes;
            KauxAxisNum[2] = m_numPositionerAxes;

            m_currentTagPoint = new String("");

            m_keywords = new Pattern [5000];
            m_axisTypes = new String[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numPositionerAxes];
            m_axisTypes = super.getAxisTypes();
            weldHome = new String("Home_1");
            closeHome = new String("Home_2");
            openHome = new String("Home_2");
            grpOpenHome = new String("Home_1");
            grpCloseHome = new String("Home_2");
            grpMidHome = new String("Home_2");
            m_tagNames = new Hashtable();
            m_toolNumberMapping = new Hashtable();
            m_objFrameNumberMapping = new Hashtable();
            for (int ii=0; ii<17; ii++)
            {
                if (ii<toolProfileNames.length)
                    k_toolProfileNames[ii] = toolProfileNames[ii];
                else
                    k_toolProfileNames[ii] = "ToolProfile." + ii;
            }
            for (int ii=0; ii<33; ii++)
            {
                if (ii<objProfileNames.length)
                    k_objProfileNames[ii] = objProfileNames[ii];
                else
                    k_objProfileNames[ii] = "ObjectProfile." + ii;
            }
            programCallNames = new ArrayList(1);
            fileProgramList = new ArrayList(1);
            globalProgramList = new ArrayList(1);
            commentLines = new ArrayList(2);
            inputNameNum = new ArrayList(1);
            outputNameNum = new ArrayList(1);
            partIndex = new ArrayList(33);
            baseNameIndex = new ArrayList(33);
            toolNameIndex = new ArrayList(33);
            basetoolSwapped = new boolean [33];
            userProgStart = false;
            processingUnrecog = false;
            kirAttribute = new boolean [33];
            kirParent = new String [33];
            kirBaseAttribute = new boolean [33];
            kirMachine = new String [33];
            kirMachineBase = new String [33][6];
            for (int ii=0; ii<33; ii++)
            {
                partIndex.add(ii, "");
                kirAttribute[ii] = false;
                kirParent[ii] = " ";
                kirBaseAttribute[ii] = false;
                kirMachine[ii] = " ";
                for (int jj=0; jj<6; jj++)
                    kirMachineBase[ii][jj] = "0.0";
                baseNameIndex.add(ii, "");
                toolNameIndex.add(ii, "");
                basetoolSwapped[ii] = false;
            }

            jTgtName_arrList = new ArrayList(1);
            jTgtName_arrList.add(0, "HOME");

            jTgtAval_arrList = new ArrayList(1);
            double [] axis = {0.0, -90.0, 90.0, 0.0, 0.0, 0.0};
            jTgtAval_arrList.add(0, axis);

            jTgtEval_arrList = new ArrayList(1);
            double [] ext = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
            jTgtEval_arrList.add(0, ext);

            kks = new spot();
            kku = new util();
            kkc = new counters();
            
            m_xmlDoc = super.getDOMDocument();
            Element resource = super.getResourceElement();
            Element controller = super.getControllerElement();
            Element genInfo = m_xmlDoc.createElement("GeneralInfo");
            resource.insertBefore(genInfo, controller);
            Element pickPart = m_xmlDoc.createElement("SelectPartsOnUpload");
            genInfo.appendChild(pickPart);
            pickPart.appendChild(m_xmlDoc.createTextNode("false"));

            if (homeNames.length > 0)
            {
                weldHome = homeNames[0];
                grpOpenHome = homeNames[0];
            }
            if (homeNames.length > 1)
            {
                closeHome = homeNames[1];
                grpCloseHome = homeNames[1];
            }
            if (homeNames.length > 2)
            {
                openHome = homeNames[2];
                grpMidHome = homeNames[2];
            }

            Hashtable m_parameterData = super.getParameterData();
            Enumeration parameterKeys = m_parameterData.keys();
            Enumeration parameterValues = m_parameterData.elements();
            int dotIdx;
            String pName, pValue, testString;
            String auxAxisOrder;
            String tlprf = "ToolProfile.";
            String ofprf = "ObjectFrameProfile.";
            while (parameterKeys.hasMoreElements() == true && parameterValues.hasMoreElements() == true)
            {
                pName = parameterKeys.nextElement().toString();
                pValue = parameterValues.nextElement().toString();
                dotIdx = pName.indexOf('.');
                if (dotIdx == -1)
                    testString = pName;
                else
                    testString = pName.substring(0, dotIdx+1);
                
                try
                {
                    if (testString.equalsIgnoreCase(tlprf))
                    {
                        String toolID = pName.substring(dotIdx+1);
                        m_toolNumberMapping.put(toolID, pValue);
                    }
                    else if (testString.equalsIgnoreCase(ofprf))
                    {
                        String objID = pName.substring(dotIdx+1);
                        m_objFrameNumberMapping.put(objID, pValue);
                    }
                    else if (testString.equalsIgnoreCase("WeldHome"))
                    {
                        weldHome = pValue;
                    }
                    else if (testString.equalsIgnoreCase("CloseHome"))
                    {
                        closeHome = pValue;
                    }
                    else if (testString.equalsIgnoreCase("OpenHome"))
                    {
                        openHome = pValue;
                    }
                    else if (testString.equalsIgnoreCase("ConfigFile"))
                    {
                        configFile = pValue;
                    }
                    else if (testString.equalsIgnoreCase("MachineFile"))
                    {
                        machineFile = pValue;
                    }
                    else if (testString.equalsIgnoreCase("ServoGunOpenNegative"))
                    {
                        svgNeg = pValue;
                    }
                    else if (testString.equalsIgnoreCase("DelmiaAuxAxisOrder"))
                    {
                        auxAxisOrder = pValue;
                        int idx, ii;
                        for( ii=0; ii<3; ii++ )
                        {
                            idx = auxAxisOrder.indexOf(',');
                            if (idx > 0)
                            {
                                DauxAxisGrp[ii] = auxAxisOrder.substring(0, idx);
                                auxAxisOrder = auxAxisOrder.substring(idx+1);
                            }
                            else
                            {
                                DauxAxisGrp[ii] = auxAxisOrder;
                                break;
                            }
                        }
                        for (ii=0; ii<3; ii++)
                        {
                            if (DauxAxisGrp[ii].equals(auxRail))
                                DauxAxisNum[ii] = m_numAuxAxes;
                            else
                            if (DauxAxisGrp[ii].equals(auxTool))
                                DauxAxisNum[ii] = m_numExtAxes;
                            else
                            if (DauxAxisGrp[ii].equals(auxPos))
                                DauxAxisNum[ii] = m_numPositionerAxes;
                        }
                    }
                    else if (testString.equalsIgnoreCase("KukaAuxAxisOrder"))
                    {
                        auxAxisOrder = pValue;
                        int idx, ii;
                        for( ii=0; ii<3; ii++ )
                        {
                            idx = auxAxisOrder.indexOf(',');
                            if (idx > 0)
                            {
                                KauxAxisGrp[ii] = auxAxisOrder.substring(0, idx);
                                auxAxisOrder = auxAxisOrder.substring(idx+1);
                            }
                            else
                            {
                                KauxAxisGrp[ii] = auxAxisOrder;
                                break;
                            }
                        }
                        for (ii=0; ii<3; ii++)
                        {
                            if (KauxAxisGrp[ii].equals(auxRail))
                                KauxAxisNum[ii] = m_numAuxAxes;
                            else
                            if (KauxAxisGrp[ii].equals(auxTool))
                                KauxAxisNum[ii] = m_numExtAxes;
                            else
                            if (KauxAxisGrp[ii].equals(auxPos))
                                KauxAxisNum[ii] = m_numPositionerAxes;
                        }
                    }
                    else if (testString.equalsIgnoreCase("OLPStyle"))
                    {
                        uploadStyle = pValue;
                    }
                }

                finally
                {
                    continue;
                }
            }
            
            if (machineFile != null)
            {
                kku.readMachineDat(machineFile);
                railConfig = kku.getRailConfig();
            }

            for (int ii = 0; ii < m_keywords.length; ii++ )
            {
                m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
            }

            m_keywords[0] = Pattern.compile("^&", Pattern.CASE_INSENSITIVE);
            m_keywords[1] = Pattern.compile("^;FOLD\\s+.*%CCOMMENT", Pattern.CASE_INSENSITIVE);
            m_keywords[3] = Pattern.compile("^;FOLD\\s+.*%MKUKATPSERVOTECH", Pattern.CASE_INSENSITIVE);
            m_keywords[5] = Pattern.compile("^;FOLD\\s+.*%MKUKATPGRP", Pattern.CASE_INSENSITIVE);
            m_keywords[15] = Pattern.compile("^;", Pattern.CASE_INSENSITIVE);

            m_keywords[20] = Pattern.compile("^A20", Pattern.CASE_INSENSITIVE);
            m_keywords[30] = Pattern.compile("^A10_INI", Pattern.CASE_INSENSITIVE);
            m_keywords[40] = Pattern.compile("^USER_GRP", Pattern.CASE_INSENSITIVE);
            m_keywords[50] = Pattern.compile("^USERSPOT\\s*\\(", Pattern.CASE_INSENSITIVE);
            m_keywords[55] = Pattern.compile("^TRIGGER\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[60] = Pattern.compile("^H70", Pattern.CASE_INSENSITIVE);

            m_keywords[70] = Pattern.compile("^DEFDAT\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[71] = Pattern.compile("^ENDDAT", Pattern.CASE_INSENSITIVE);
            m_keywords[72] = Pattern.compile("^DEF\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[73] = Pattern.compile("^END\\s*$", Pattern.CASE_INSENSITIVE);

            m_keywords[100] = Pattern.compile("^PDAT_ACT\\s*=", Pattern.CASE_INSENSITIVE);
            m_keywords[110] = Pattern.compile("^LDAT_ACT\\s*=", Pattern.CASE_INSENSITIVE);
            m_keywords[120] = Pattern.compile("^FDAT_ACT\\s*=", Pattern.CASE_INSENSITIVE);
            m_keywords[130] = Pattern.compile("^S_ACT\\.\\w*\\s*=", Pattern.CASE_INSENSITIVE);
            m_keywords[135] = Pattern.compile("^S_READY\\s*=", Pattern.CASE_INSENSITIVE);
            m_keywords[140] = Pattern.compile("^WAIT\\s+FOR\\s+S_READY", Pattern.CASE_INSENSITIVE);

            m_keywords[150] = Pattern.compile("^TOOL_DATA\\s*\\[", Pattern.CASE_INSENSITIVE);
            m_keywords[151] = Pattern.compile("^TOOL_NAME\\s*\\[", Pattern.CASE_INSENSITIVE);
            m_keywords[160] = Pattern.compile("^BASE_DATA\\s*\\[", Pattern.CASE_INSENSITIVE);
            m_keywords[161] = Pattern.compile("^BASE_NAME\\s*\\[", Pattern.CASE_INSENSITIVE);
            m_keywords[165] = Pattern.compile("^MACHINE_FRAME_DAT", Pattern.CASE_INSENSITIVE);
            m_keywords[170] = Pattern.compile("^MACHINE_DEF", Pattern.CASE_INSENSITIVE);
            m_keywords[175] = Pattern.compile("^PART_BASE_MAP", Pattern.CASE_INSENSITIVE);

            m_keywords[200] = Pattern.compile("^PTP\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[210] = Pattern.compile("^LIN\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[220] = Pattern.compile("^CIRC\\s", Pattern.CASE_INSENSITIVE);

            m_keywords[300] = Pattern.compile("^EXT\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[310] = Pattern.compile("^GLOBAL\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[320] = Pattern.compile("^INTERRUPT\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[330] = Pattern.compile("^BAS\\s*\\(", Pattern.CASE_INSENSITIVE);
            m_keywords[340] = Pattern.compile("^\\$", Pattern.CASE_INSENSITIVE);
            m_keywords[350] = Pattern.compile("^PULSE", Pattern.CASE_INSENSITIVE);

            m_keywords[500] = Pattern.compile("^(DECL\\s+)?PDAT\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[510] = Pattern.compile("^(DECL\\s+)?LDAT\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[520] = Pattern.compile("^(DECL\\s+)?FDAT\\s", Pattern.CASE_INSENSITIVE);
            m_keywords[525] = Pattern.compile("^(DECL\\s+)?E6AXIS\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[530] = Pattern.compile("^(DECL\\s+)?E6POS\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[535] = Pattern.compile("^(DECL\\s+)?E3POS\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[540] = Pattern.compile("^(DECL\\s+)?POS\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[550] = Pattern.compile("^(DECL\\s+)?INT\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[560] = Pattern.compile("^(DECL\\s+)?SPOT_TYPE\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[570] = Pattern.compile("^(DECL\\s+)?GRP_TYP\\s+", Pattern.CASE_INSENSITIVE);

            m_keywords[700] = Pattern.compile("^WAIT\\s+SEC\\s+[0-9]+\\.?[0-9]*", Pattern.CASE_INSENSITIVE);
            m_keywords[710] = Pattern.compile("^WAIT\\s+FOR\\s+", Pattern.CASE_INSENSITIVE);
            m_keywords[800] = Pattern.compile("^CONTINUE", Pattern.CASE_INSENSITIVE);
          //m_keywords[810] = Pattern.compile("^IF\\s+", Pattern.CASE_INSENSITIVE);
          //m_keywords[820] = Pattern.compile("^ENDIF\\s*$", Pattern.CASE_INSENSITIVE);
          //m_keywords[830] = Pattern.compile("^WHILE\\s+TRUE", Pattern.CASE_INSENSITIVE);
          //m_keywords[840] = Pattern.compile("^ENDWHILE\\s*$", Pattern.CASE_INSENSITIVE);

            m_keywords[3000] = Pattern.compile("^[a-zA-Z_]\\w*\\s*\\(\\s*\\)", Pattern.CASE_INSENSITIVE);
        }

    	public static void main(String [] parameters)
            throws SAXException, ParserConfigurationException, TransformerConfigurationException,
                    NumberFormatException, StringIndexOutOfBoundsException
        {
            KukaUploader uploader = new KukaUploader(parameters);
            String [] additionalProgramNames = null;
            if (parameters.length > 12 && parameters[12].trim().length() > 0)
                additionalProgramNames = parameters[12].split("[\\t]+");

            //Get the robot program and parse it line by line
            String robotProgramName = uploader.getPathToRobotProgramFile();
            File f = new File(robotProgramName);
            String path = f.getParent() + f.separator;
            int strLen = robotProgramName.length();
            robotProgramName = robotProgramName.substring(0, strLen-4);
            String robotProgramNameOnly = f.getName();
            strLen = robotProgramNameOnly.length();
            robotProgramNameOnly = robotProgramNameOnly.substring(0, strLen-4);

            String uploadInfoFileName = uploader.getPathToErrorLogFile();
            String logicProgramName = uploader.getPathToRobotProgramFile();
            boolean logicProg = false;
            Matcher match;

            try
            {
                bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));

                bw.write(VERSION);
                bw.write(COPYRIGHT);
            }

            catch (IOException e)
            {
                e.getMessage();
            }
            
            if (uploader.configFile != null)
                uploader.processConfigDat();
            
            try
            {
                if (railConfig != null)
                {
                    bw.write("\nRobot-Rail Configuration: ");
                    
                    if (railConfig.equals("A"))
                        bw.write("90-degree A\n");
                    else
                    if (railConfig.equals("B"))
                        bw.write("90-degree B\n");
                    else
                    if (railConfig.equals("C"))
                        bw.write("90-degree C\n");
                    else
                    if (railConfig.equals("D"))
                        bw.write("90-degree D\n");
                    else
                    if (railConfig.equals("H"))
                        bw.write("45-degree D(A-45-B)\n");
                    else
                    if (railConfig.equals("I"))
                        bw.write("45-degree A(B-45-C)\n");
                    else
                    if (railConfig.equals("J"))
                        bw.write("45-degree B(C-45-D)\n");
                    else
                    if (railConfig.equals("K"))
                        bw.write("45-degree C(D-45-A)\n");
                }
                
                bw.write("\nStart of java parsing.\n\n");

                if (logicProgramName.toLowerCase().endsWith(".src"))
                {
                    logicProg = true;
                    BufferedReader rbt;
                    try
                    {
                        rbt = new BufferedReader(new FileReader(logicProgramName));

                        String line = rbt.readLine();
                        while (line != null)
                        {
                            line = line.trim();
                            if (line.equals(""))
                            {
                                line = rbt.readLine();
                                continue;
                            }
                            if (line.equalsIgnoreCase("END"))
                                break;

                            for (int ii = 0; ii < uploader.m_keywords.length; ii++)
                            {
                                match = uploader.m_keywords[ii].matcher(line);

                                if ( match.find() == true )
                                {
                                    switch(ii)
                                    {
                                        case 200: //PTP  Xtagname [C_PTP [C_DIS|C_VEL|C_ORI]]
                                        case 210: //LIN  Xtagname        [C_DIS|C_VEL|C_ORI]
                                        case 220: //CIRC Xtagname        [C_DIS|C_VEL|C_ORI]
                                            logicProg = false;
                                            break;
                                        default:
                                            break;
                                    }//end switch

                                    //match found & processed, break out for loop
                                    break;
                                }//end if
                            }//end for
                            line = rbt.readLine();

                        }//end while
                        rbt.close();
                    }//end try

                    catch (IOException e)
                    {
                        try {
                            bw.write("ERROR: "+e.getMessage()+"\n");
                        }
                        catch (IOException bwe) {
                        }
                    }
                }//end if

                int errCode;
                if (logicProg == true)
                    errCode = uploader.processRobotLogicProgram(logicProgramName);
                else
                    errCode = uploader.processRobotProgram(robotProgramName);

                String sAddProg = null;
                int index;
                if (additionalProgramNames != null)
                {
                    int iStoreProgramCallCount = uploader.programCallCount;
                    for (int ii=0; ii<additionalProgramNames.length; ii++)
                    {
                        f = new File(additionalProgramNames[ii]);
                        sAddProg = f.getName().toUpperCase();
                        if (sAddProg.endsWith(".SRC") || sAddProg.endsWith(".DAT"))
                            sAddProg = sAddProg.substring(0, sAddProg.length()-4);
                        
                        index = uploader.programCallNames.indexOf(sAddProg);
                        if (index < 0)
                        {
                            uploader.programCallNames.add(uploader.programCallCount, sAddProg);
                            uploader.programCallNames.trimToSize();
                            uploader.programCallCount++;
                        }
                    }
                    uploader.programCallCount = iStoreProgramCallCount;
                }
                
                if (errCode == 0)
                {
                    String calledProgName;
                    while (uploader.programCallNames.isEmpty() == false)
                    {
                        calledProgName = (String)uploader.programCallNames.get(0);
                        uploader.programCallNames.remove(calledProgName);
                        
                        if (calledProgName.equalsIgnoreCase(robotProgramNameOnly))
                            continue;
                        
                        robotProgramName = path + calledProgName.trim();
                        errCode = uploader.processRobotProgram(robotProgramName);
                    }
                }

                if (errCode == 0)
                {
                    bw.write("All the program statements have been parsed.\n");
                    bw.write("\nJava parsing completed successfully.\n");
                }
                else
                {
                    bw.write("\nERROR-Robot program upload failed.\n");
                }

                bw.write("\nEnd of java parsing.\n");
                bw.flush();
                bw.close();
            }//end try

            catch (IOException e)
            {
                e.getMessage();
            }

            try
            {
                uploader.writeXMLStreamToFile();
            }

            catch (TransformerException e)
            {
                e.getMessage();
            }
            catch (IOException e)
            {
                e.getMessage();
            }
        }//end main

        private void transformCartTgt4ZeroBase(Element tgtElem)
        {
            if (machineFile == null)
                return;
            if (tgtElem.getAttribute(DEFAULT).equals("Cartesian") == false)
                return;
            
            Element jntTgtElem = kku.findChildElemByName(tgtElem, "JointTarget", null, null);
            if (jntTgtElem == null)
                return;
            Element auxJointElem = kku.findChildElemByName(jntTgtElem, "AuxJoint", "Type", auxRail);
            if (auxJointElem == null)
                return;
            Element jointValElem = kku.findChildElemByName(auxJointElem, "JointValue", null, null);
            if (jointValElem == null)
                return;
            Element cartTgtElem = kku.findChildElemByName(tgtElem, "CartesianTarget", null, null);
            if (cartTgtElem == null)
                return;
            Element posElem = kku.findChildElemByName(cartTgtElem, "Position", null, null);
            if (posElem == null)
                return;
            Element oriElem = kku.findChildElemByName(cartTgtElem, "Orientation", null, null);
            if (oriElem == null)
                return;
            
            String railConfig = kku.getRailConfig();
            if (railConfig.equalsIgnoreCase("None"))
                return;
            float zOffset = kku.getZOffset()/1000;
            
            // rotate base of KUKA robot-on-rail to line up with rail positive direction
            String sVector = "0,0,0,0,0,";
            int roll = 0;
            if (railConfig.equals("A"))
                roll = 0;
            else
            if (railConfig.equals("B"))
                roll = 90;
            else
            if (railConfig.equals("C"))
                roll = 180;
            else
            if (railConfig.equals("D"))
                roll = 270;
            else
            if (railConfig.equals("H")) // KUKA robot base is same as config B
                roll = 90;
            else
            if (railConfig.equals("I"))
                roll = 90;
            else
            if (railConfig.equals("J")) // KUKA robot base is same as config D
                roll = -90;
            else
            if (railConfig.equals("K"))
                roll = -90;
            
            DNBIgpOlpUploadMatrixUtils matrixUtils = new DNBIgpOlpUploadMatrixUtils();
            String tmpStr = sVector + roll;
            matrixUtils.dgXyzyprToMatrix(tmpStr);
            
            String railPos = jointValElem.getFirstChild().getNodeValue();
            tmpStr = railPos + ',' + 0 + ',' + zOffset + ",0,0,0";
            matrixUtils.dgCatXyzyprMatrix(tmpStr);
            
            /* positions in KUKA program are relative to KUKA robot base which is fixed 450mm below robot when
             * rail position is zero(0). Shift position by rail position and robot mount height
             */
            float x, y, z;
            x = Float.parseFloat(posElem.getAttribute("X")) - Float.parseFloat(matrixUtils.dgGetX());
            y = Float.parseFloat(posElem.getAttribute("Y")) - Float.parseFloat(matrixUtils.dgGetY());
            z = Float.parseFloat(posElem.getAttribute("Z")) - Float.parseFloat(matrixUtils.dgGetZ());
            
            /* for 45-degree configurations, KUKA positions are still WRT square coordinates of config B or D
             * transform the position to V5 robot coordinates which is fixed and aligns with V5 robot itself
             */
            roll = 0;
            if (railConfig.equals("H"))
                roll = -45;
            else
            if (railConfig.equals("I"))
                roll = 45;
            else
            if (railConfig.equals("J"))
                roll = -45;
            else
            if (railConfig.equals("K"))
                roll = 45;
            
            if (roll != 0)
            {
                tmpStr = sVector + roll;
                matrixUtils.dgXyzyprToMatrix(tmpStr);
                String xyz, ypr;
                ypr = oriElem.getAttribute("Yaw") + ',' + oriElem.getAttribute("Pitch") + ',' + oriElem.getAttribute("Roll");
                xyz = String.valueOf(x) + ',' + String.valueOf(y) + ',' + String.valueOf(z);
                matrixUtils.dgCatXyzyprMatrix(xyz + ',' + ypr);
                
                oriElem.setAttribute("Yaw",   String.valueOf(matrixUtils.dgGetYaw()));
                oriElem.setAttribute("Pitch", String.valueOf(matrixUtils.dgGetPitch()));
                oriElem.setAttribute("Roll",  String.valueOf(matrixUtils.dgGetRoll()));
                
                x = Float.parseFloat(matrixUtils.dgGetX());
                y = Float.parseFloat(matrixUtils.dgGetY());
                z = Float.parseFloat(matrixUtils.dgGetZ());
            }
            
            posElem.setAttribute("X", String.valueOf(x));
            posElem.setAttribute("Y", String.valueOf(y));
            posElem.setAttribute("Z", String.valueOf(z));
        } // end transformCartTgt4ZeroBase
        
        private void transformBase4RobotOnRail(Element objFrameElem)
        {
            String railConfig = kku.getRailConfig();
            if (railConfig.equalsIgnoreCase("None"))
                return;
            
            /* KUKA robot base is fixed 450mm below robot when rail position is zero(0)
             * for 45-degree configurations, this base is still square with rail as in configuration B or D
             * transform this base to be WRT V5 robot as this base is created with RobotBase as ReferenceFrame
             */
            int roll = 0;
            if (railConfig.equals("H"))
                roll = -45;
            else
            if (railConfig.equals("I"))
                roll = 45;
            else
            if (railConfig.equals("J"))
                roll = -45;
            else
            if (railConfig.equals("K"))
                roll = 45;
            
            DNBIgpOlpUploadMatrixUtils matrixUtils = new DNBIgpOlpUploadMatrixUtils();
            matrixUtils.dgXyzyprToMatrix("0,0,0,0,0," + roll);
            
            float zOffset = kku.getZOffset()/1000;
            
            Element objFramePosElem = kku.findChildElemByName(objFrameElem, "ObjectFramePosition", null, null);
            Element objFrameOriElem = kku.findChildElemByName(objFrameElem, "ObjectFrameOrientation", null, null);
            
            String sX = objFramePosElem.getAttribute("X");
            String sY = objFramePosElem.getAttribute("Y");
            String sZ = objFramePosElem.getAttribute("Z");
            float z = Float.parseFloat(sZ) - zOffset;
            
            String sW = objFrameOriElem.getAttribute("Yaw");
            String sP = objFrameOriElem.getAttribute("Pitch");
            String sR = objFrameOriElem.getAttribute("Roll");
            
            matrixUtils.dgCatXyzyprMatrix(sX + ',' + sY + ',' + String.valueOf(z) + ',' + sW + ',' + sP + ',' + sR);
            
            objFramePosElem.setAttribute("X", matrixUtils.dgGetX());
            objFramePosElem.setAttribute("Y", matrixUtils.dgGetY());
            objFramePosElem.setAttribute("Z", matrixUtils.dgGetZ());
            
            objFrameOriElem.setAttribute("Yaw", matrixUtils.dgGetYaw());
            objFrameOriElem.setAttribute("Pitch", matrixUtils.dgGetPitch());
            objFrameOriElem.setAttribute("Roll", matrixUtils.dgGetRoll());
        } // end transformBase4RobotOnRail
        
        private void processConfigDat()
        {
            Element resource = super.getResourceElement();
            Element genInfo = kku.findChildElemByName(resource, "GeneralInfo", null, null);
            Matcher match;
            try
            {
                BufferedReader sysvarin = new BufferedReader(new FileReader(configFile));
                String line = sysvarin.readLine();
                while (line != null)
                {
                    line = line.trim();
                    if (line.equals(""))
                    {
                        line = sysvarin.readLine();
                        continue;
                    }
                    for (int ii=0; ii<m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);
                        if (match.find() == true)
                        {
                            switch(ii)
                            {
                                case 150: //TOOL_DATA[number] = {X float, ..., C float}
                                    processToolStatement(line);
                                    break;
                                case 151: //TOOL_NAME[number,] = "tool_name"
                                    processBaseNameStatement(line, 1);
                                    break;
                                case 160: //BASE_DATA[number] = {X float, ..., C float}
                                    processBaseStatement(line);
                                    break;
                                case 161: //BASE_NAME[number,] = "base_name"
                                    processBaseNameStatement(line, 0);
                                    break;
                                case 165:
                                    processMachineFrameDat(line);
                                    break;
                                case 170:
                                    processMachineDef(line);
                                    break;
                                case 175:
                                    processPartBaseMap(line);
                                    break;
                                case 525: //E6AXIS X
                                    setJointTarget(line);
                                    break;
                            } // end switch
                            
                            // match found, break out FOR loop
                            break;
                        }//end if
                    }//end for
                    line = sysvarin.readLine();
                }//end while
            }//end try
            catch (IOException e)
            {
                try {
                    bw.write("\nWARNING: Specified ConfigFile cannot be found.");
                    bw.write("\nWARNING: Default HOME position will be used.");
                    bw.write("\nWARNING: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) { }
            }
            
            Element master;
            for (int ii=0; ii<17; ii++)
            {
                if (kirParent[ii].trim().length() > 0)
                {
                    master = m_xmlDoc.createElement("Master");
                    genInfo.appendChild(master);
                    master.setAttribute("Name", kirParent[ii]);
                    
                    break;
                }
            }

            /* after the $config.dat(BASE_DATA & BASE_NAME, TOOL_DATA & TOOL_NAME) is processed
             * sets ObjectFrameProfile Name elements with names stored in baseNameIndex & reference frame
             */
            setFrameNames(1); // set tool names
            setFrameNames(0); // set base names
        } // end processConfigDat

        private void setFrameNames(int iTool)
        {
            String tmpBaseName, name;
            Element objFrameProfileElem, objFrameProfileNameElem, objFrameElem;

            Element frameProfileList;
            String sObjectFrameProfile;
            String sObjectProfile;
            if (iTool == 1)
            {
                frameProfileList = super.getToolProfileListElement();
                sObjectFrameProfile = "ToolProfile";
                sObjectProfile = "ToolProfile.";
            }
            else // iTool == 0
            {
                frameProfileList = super.getObjectFrameProfileListElement();
                sObjectFrameProfile = "ObjectFrameProfile";
                sObjectProfile = "ObjectProfile.";
            }

            NodeList objFrameProfileNodes = frameProfileList.getChildNodes();
            for (int ii=0; ii<objFrameProfileNodes.getLength(); ii++)
            {
                objFrameProfileElem = (Element)objFrameProfileNodes.item(ii);
                if (objFrameProfileElem.getTagName().equals(sObjectFrameProfile))
                {
                    objFrameProfileNameElem = kku.findChildElemByName(objFrameProfileElem, "Name", null, null);
                    for (int baseNum=1; baseNum<33; baseNum++)
                    {
                        tmpBaseName = sObjectProfile + Integer.toString(baseNum);
                        if (objFrameProfileNameElem.getFirstChild().getNodeValue().equals(tmpBaseName))
                        {
                            if (iTool == 1)
                                name = toolNameIndex.get(baseNum).toString();
                            else
                                name = baseNameIndex.get(baseNum).toString();
                            if (name.length() > 0)
                                objFrameProfileNameElem.getFirstChild().setNodeValue(name);

                            if (iTool == 0 && kirParent[baseNum].trim().length() > 0)
                            {
                                objFrameElem = kku.findChildElemByName(objFrameProfileElem, "ObjectFrame", null, null);
                                objFrameElem.setAttribute("ReferenceFrame", "MasterFlange");
                            }

                            break;
                        }
                    }
                }
            }
        } // end setFrameNames

        public void setJointTarget(String line)
        {
            String tmpStr = line.split("=")[0];
            String [] argStrings = tmpStr.split("[\\s+]");
            String name = argStrings[argStrings.length-1].substring(1).toUpperCase();
            int idx = jTgtName_arrList.indexOf(name);

            double [] axis = {0.0, -90.0, 90.0, 0.0, 0.0, 0.0};
            double [] ext  = {0.0,   0.0,  0.0, 0.0, 0.0, 0.0};
            String patStr;
            Pattern p;
            Matcher m;

            for (int ii=1; ii<=6; ii++)
            {
                patStr = "A" + ii + numPatStr;
                p = Pattern.compile(patStr, Pattern.CASE_INSENSITIVE);
                m = p.matcher(line);
                if (m.find() == true)
                    axis[ii-1] = new Double(m.group().split("[\\s+]")[1].trim()).doubleValue();

                patStr = "E" + ii + numPatStr;
                p = Pattern.compile(patStr, Pattern.CASE_INSENSITIVE);
                m = p.matcher(line);
                if (m.find() == true)
                    ext[ii-1] = new Double(m.group().split("[\\s+]")[1].trim()).doubleValue();
            }

            if (idx == -1)
            {
                jTgtName_arrList.add(name);
                jTgtAval_arrList.add(axis);
                jTgtEval_arrList.add(ext);
            }
            else
            {
                jTgtAval_arrList.set(idx, axis);
                jTgtEval_arrList.set(idx, ext);
            }
        } // end setJointTarget
        
        // TOOL_DATA
        private void processToolStatement(String line)
        {
            String [] components = line.split("[\\[\\]]");
            int toolNum = Integer.parseInt(components[1].trim());

            String name = "ToolProfile." + Integer.toString(toolNum);
            Element toolProfileElem, toolProfileNameElem, tcpOffsetElem, tcpPositionElem, tcpOrientationElem;
            Element toolProfileListElem = super.getToolProfileListElement();
            NodeList toolProfileNodes = toolProfileListElem.getChildNodes();
            for (int ii=0; ii<toolProfileNodes.getLength(); ii++)
            {
                toolProfileElem = (Element)toolProfileNodes.item(ii);
                if (toolProfileElem.getTagName().equals("ToolProfile"))
                {
                    toolProfileNameElem = kku.findChildElemByName(toolProfileElem, "Name", null, null);
                    if (toolProfileNameElem.getFirstChild().getNodeValue().equals(name))
                    {
                        tcpOffsetElem = kku.findChildElemByName(toolProfileElem, "TCPOffset", null, null);
                        tcpPositionElem = kku.findChildElemByName(tcpOffsetElem, "TCPPosition", null, null);
                        setPositionValues(tcpPositionElem, line);
                        tcpOrientationElem = kku.findChildElemByName(tcpOffsetElem, "TCPOrientation", null, null);
                        setOrientationValues(tcpOrientationElem, line);

                        return;
                    }
                }
            }
/*
            if (zeroPos(line))
                return;
*/
            toolProfileElem = m_xmlDoc.createElement("ToolProfile");
            toolProfileListElem.appendChild(toolProfileElem);

            toolProfileNameElem = m_xmlDoc.createElement("Name");
            toolProfileElem.appendChild(toolProfileNameElem);

            Text toolProfileName = m_xmlDoc.createTextNode(name);
            toolProfileNameElem.appendChild(toolProfileName);

            Element toolTypeElem = m_xmlDoc.createElement("ToolType");
            toolProfileElem.appendChild(toolTypeElem);
            Text toolTypeValue = m_xmlDoc.createTextNode("OnRobot");
            toolTypeElem.appendChild(toolTypeValue);

            tcpOffsetElem = m_xmlDoc.createElement("TCPOffset");
            toolProfileElem.appendChild(tcpOffsetElem);

            tcpPositionElem = m_xmlDoc.createElement("TCPPosition");
            tcpOffsetElem.appendChild(tcpPositionElem);
            setPositionValues(tcpPositionElem, "x 0.0,y 0.0,z 0.0"); //set to zero in case line is not supported
            setPositionValues(tcpPositionElem, line);

            tcpOrientationElem = m_xmlDoc.createElement("TCPOrientation");
            tcpOffsetElem.appendChild(tcpOrientationElem);
            setOrientationValues(tcpOrientationElem, "a 0.0,b 0.0,c 0.0"); //set to zero in case line is not supported
            setOrientationValues(tcpOrientationElem, line);
        } //end processToolStatement

        private void processMachineFrameDat(String line)
        {
            int index = line.indexOf("\"");
            int index2 = line.indexOf("\"", index+1);
            String parent = line.substring(index+1, index2);
            if (parent.equalsIgnoreCase("WORLD"))
                return;
            if (parent.trim().length()==0)
                return;

            String [] components = line.split("[\\[\\]]");
            int baseNum = Integer.parseInt(components[1].trim());
            kirAttribute[baseNum] = true;
            kirParent[baseNum] = parent;

            return;
        } //end processMachineFrameDat

        private void processMachineDef(String line)
        {
            int index = line.indexOf("\"");
            int index2 = line.indexOf("\"", index+1);
            String machineName = line.substring(index+1, index2);
            if (machineName.trim().length()==0)
                return;

            String [] components = line.split("[\\[\\]]", 3);
            int baseNum = Integer.parseInt(components[1].trim());
            kirBaseAttribute[baseNum] = true;
            kirMachine[baseNum] = machineName;

            Pattern p;
            Matcher m;
            char [] cc = {'X', 'Y', 'Z', 'A', 'B', 'C'};
            String patternStr, posStr, valueComponent;
            String [] posComponents;
            float xyzabc;
            DecimalFormat posFormatter = new DecimalFormat("0.0######");
            String resultStr;

            String [] base = line.split("[{}]");
            for (int ii=0; ii<6; ii++)
            {
                patternStr = cc[ii] + numPatStr;
                p = Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
                m = p.matcher(base[2]);
                if (m.find() == true)
                {
                    posStr = m.group();
                    posComponents = posStr.split("[\\s]+");
                    valueComponent = posComponents[1];
                    xyzabc = Float.valueOf(valueComponent.trim()).floatValue();
                    if (ii < 3)
                    {
                        xyzabc /= 1000;
                        index = ii;
                    }
                    else
                    {
                        xyzabc *= TO_RAD;
                        index = 8 - ii;
                    }
                    resultStr = posFormatter.format(xyzabc);
                    kirMachineBase[baseNum][index] = resultStr;
                }
            }

            return;
        } //end processMachineDef

        private void processBaseStatement(String line)
        {
            /* BASE_DATA[number] = {X float, ..., C float}
             */
            String [] components = line.split("[\\[\\]]");
            int baseNum = Integer.parseInt(components[1].trim());
            String name = "ObjectProfile." + Integer.toString(baseNum);

            /* go through object frame profile list, if this object frame profile exists, sets its values and returns
             */
            Element objFrameProfileElem, objFrameProfileNameElem, objFrameElem, objFramePositionElem, objFrameOrientationElem;
            Element objFrameProfileListElem = super.getObjectFrameProfileListElement();
            NodeList objFrameProfileNodes = objFrameProfileListElem.getChildNodes();
            for (int ii=0; ii<objFrameProfileNodes.getLength(); ii++)
            {
                objFrameProfileElem = (Element)objFrameProfileNodes.item(ii);
                if (objFrameProfileElem.getTagName().equals("ObjectFrameProfile"))
                {
                    objFrameProfileNameElem = kku.findChildElemByName(objFrameProfileElem, "Name", null, null);
                    if (objFrameProfileNameElem.getFirstChild().getNodeValue().equals(name))
                    {
                        objFrameElem = kku.findChildElemByName(objFrameProfileElem, "ObjectFrame", null, null);
                        objFramePositionElem = kku.findChildElemByName(objFrameElem, "ObjectFramePosition", null, null);
                        setPositionValues(objFramePositionElem, line);
                        objFrameOrientationElem = kku.findChildElemByName(objFrameElem, "ObjectFrameOrientation", null, null);
                        setOrientationValues(objFrameOrientationElem, line);
                        
                        transformBase4RobotOnRail(objFrameElem);
                        
                        return;
                    }
                }
            }
/*
            if (zeroPos(line))
                return;
*/
            /* if the object frame profile does not exist, creates it and sets its values
             */
            objFrameProfileElem = m_xmlDoc.createElement("ObjectFrameProfile");
            objFrameProfileListElem.appendChild(objFrameProfileElem);

            objFrameProfileNameElem = m_xmlDoc.createElement("Name");
            objFrameProfileElem.appendChild(objFrameProfileNameElem);

            Text objFrameProfileName = m_xmlDoc.createTextNode(name);
            objFrameProfileNameElem.appendChild(objFrameProfileName);

            objFrameElem = m_xmlDoc.createElement("ObjectFrame");
            objFrameProfileElem.appendChild(objFrameElem);

            objFrameElem.setAttribute("ReferenceFrame", "RobotBase");

            objFramePositionElem = m_xmlDoc.createElement("ObjectFramePosition");
            objFrameElem.appendChild(objFramePositionElem);
            setPositionValues(objFramePositionElem, line);

            objFrameOrientationElem = m_xmlDoc.createElement("ObjectFrameOrientation");
            objFrameElem.appendChild(objFrameOrientationElem);
            setOrientationValues(objFrameOrientationElem, line);
            
            transformBase4RobotOnRail(objFrameElem);
        } //end processBaseStatement

        private void processBaseNameStatement(String line, int iTool)
        {
            /* BASE_NAME[number,] = "base name"
             * TOOL_NAME[number,] = "tool name"
             * this routine sets baseNameIndex or toolNameIndex with (baseNum, baseName) pair
             */
            String [] components = line.split("[\\[,]");
            int baseNum = Integer.parseInt(components[1].trim());
            int index1 = line.indexOf("\"")+1;
            int index2 = line.indexOf("\"", index1);
            String baseName = line.substring(index1, index2).trim();
            if (baseName.length() > 0)
            {
                if (iTool == 1)
                {
                    toolNameIndex.set(baseNum, baseName);
                }
                else
                {
                    baseNameIndex.set(baseNum, baseName);
                }
            }
        } //end processBaseNameStatement

        private int processRobotProgram(String robotProgramName)
        {
            File f = new File(robotProgramName);
            String prgName = f.getName().toUpperCase();
            currentTask = prgName;
            if (globalProgramList.contains(prgName) == true)
                return 0;

            globalProgramList.add(prgName);
            // fileProgramList lists routines defined in a single file
            fileProgramList.clear();
            fileProgramList.add(prgName);

            BufferedReader rbt;
            headerCount = 0;

            Element resourceElem = super.getResourceElement();
            Element activityListElem = super.createActivityList(prgName);
            Element programLogicElem = kku.findChildElemByName(resourceElem, "ProgramLogic", null, null);
            resourceElem.insertBefore(activityListElem, programLogicElem);
            activityListElem.appendChild(m_xmlDoc.createElement("AttributeList"));

            int errCode;
            String [] rbtPrg = {robotProgramName+".src", robotProgramName+".dat"};
            for (int jj = 0; jj < 2; jj++)
            {
                try
                {
                    rbt = new BufferedReader(new FileReader(rbtPrg[jj]));

                    bw.write("Translating program file \"");
                    bw.write(rbtPrg[jj]);
                    bw.write("\"\n\n"); 

                    errCode = processRobotProgramLines(rbt, activityListElem, jj);
                    if (errCode != 0)
                        return errCode;
                }//end try

                catch (IOException e)
                {
                    try {
                        bw.write("ERROR: "+e.getMessage()+"\n");
                    }
                    catch (IOException bwe) {
                    }
                    return 1;
                }
            }

            return 0;
        }// end processRobotProgram

        private int processRobotProgramLines(BufferedReader rbt, Element activityListElem, int prgNum)
        {
            String line, currComment, motionLine;
            String [] moData = new String [2];
            Matcher match;
            Element programLogicElem, actElem;
            boolean subRoutine=false;
            int numEndFolds = 2, size, errCode = 0;

            Element resourceElem = super.getResourceElement();
            Element motionProfileListElem = super.getMotionProfileListElement();
            Element accuracyProfileListElem = super.getAccuracyProfileListElement();
            Element attributeListElem = kku.findChildElemByName(activityListElem, "AttributeList", null, null);

            userProgStart = false;
            processingUnrecog = false;
            try
            {
                line = rbt.readLine();
                while (line != null)
                {
                    line = line.trim();
/*
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                        continue;
                    }
*/
                    for (int ii = 0; ii < m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);

                        if ( match.find() == true )
                        {
                            //Call appropriate methods to handle the input
                            switch(ii) {
                                case 0: //header
                                    if (prgNum==0)  //SRC file
                                    {
                                        processHeader(line, attributeListElem);
                                    }
                                    break;
                                case 1: // fold comment
                                    processFoldComment(line, activityListElem);
                                    line = rbt.readLine(); // eat <;ENDFOLD>
                                    break;
                                case 3: // fold servo gun
                                    if (kksvg == null)
                                    {
                                        errCode = initSVG();
                                        if (errCode != 0)
                                            return errCode;
                                    }
                                    motionLine = kksvg.motion(line);
                                    if (motionLine != null)
                                    {
                                        moData = motionLine.split(";", 2);
                                        currMotionProfile   = moData[1];
                                        currAccuracyProfile = moData[1];
                                        processMove(moData[0], activityListElem);
                                        createMotionProfile(line, motionProfileListElem);
                                        createAccuracyProfile(moData[0], accuracyProfileListElem);
                                    }
                                    actElem = kksvg.processFoldServoGun(line, kkc, m_xmlDoc, activityListElem);
                                    addAttributeList(actElem, "Pre");

                                    while (line.substring(0,8).equalsIgnoreCase(";ENDFOLD")==false)
                                    {
                                        line = rbt.readLine().trim();
                                        while (line.length() < 8)
                                            line = rbt.readLine().trim();
                                    }
                                    break;
                                case 5: // fold gripper
                                    if (kkgrp == null)
                                        initGRP();
                                    actElem = kkgrp.processFoldGripper(line, kkc, m_xmlDoc, activityListElem);
                                    addAttributeList(actElem, "Pre");

                                    while (line.substring(0,8).equalsIgnoreCase(";ENDFOLD")==false)
                                    {
                                        line = rbt.readLine().trim();
                                        while (line.length() < 8)
                                            line = rbt.readLine().trim();
                                    }
                                    break;
                                case 15: // general comment
                                    if (line.startsWith(";Make ") && line.indexOf("modifications")>0)
                                    {
                                        addAttributeList(activityListElem, "Pre");
                                        userProgStart = true;
                                        // skip next two <;ENDFOLD ...> lines
                                        while (numEndFolds > 0)
                                        {
                                            line = rbt.readLine();
                                            if (line.trim().startsWith(";ENDFOLD"))
                                                numEndFolds--;
                                            else
                                            if (line.trim().startsWith(";FOLD "))
                                                numEndFolds++;
                                        }
                                    }
                                    else
                                    {
                                        processComment(line, prgNum);
                                    }
                                    break;
                                case 50:
                                    processUSERSPOTStatement(line);
                                    break;
                                case 55:
                                    processTRIGGERStatement(line);
                                    break;
                                case 70: //DEFDAT
                                    processDEFDATStatement(line, activityListElem);
                                    break;
                                case 72: //DEF
                                    if (subRoutine)
                                    {
                                        activityListElem = m_xmlDoc.createElement("ActivityList");
                                        programLogicElem = kku.findChildElemByName(resourceElem, "ProgramLogic", null, null);
                                        resourceElem.insertBefore(activityListElem, programLogicElem);

                                        attributeListElem = m_xmlDoc.createElement("AttributeList");
                                        activityListElem.appendChild(attributeListElem);
                                        
                                        processDEFStatement(line, activityListElem);
                                        subRoutine = false;
                                    }
                                    break;
                                case 73: //END
                                    addAttributeList(activityListElem, "Post");
                                    subRoutine = true;
                                    break;
                                case 100: //PDAT_ACT=PPDAT1
                                case 110: //LDAT_ACT=LCPDAT1
                                    selectMoAccuProfile(line);
                                    break;
                                case 120: //FDAT_ACT=FP1
                                    selectFrameProfile(line);
                                    break;
                                case 130: //S_ACT.* = ...
                                    kks.setSpotData(line);
                                    break;
                                case 135: //S_READY = TRUE|FALSE
                                    break;
                                case 140: //WAIT FOR S_READY(otherwise upload as attribute and downloads)
                                    break;
                                case 150: //TOOL_DATA[number] = {X float, ..., C float}
                                    processToolStatement(line);
                                    break;
                                case 160: //BASE_DATA[number] = {X float, ..., C float}
                                    processBaseStatement(line);
                                    break;
                                case 200: //PTP  Xtagname [C_PTP [C_DIS|C_VEL|C_ORI]]
                                case 210: //LIN  Xtagname        [C_DIS|C_VEL|C_ORI]
                                case 220: //CIRC Xtagname        [C_DIS|C_VEL|C_ORI]
                                    processMove(line, activityListElem);
                                    createAccuracyProfile(line, accuracyProfileListElem);
                                    String gunInfo = m_mountedTool+";"+openHome+";"+closeHome+";"+weldHome;
                                    if (spotAction)
                                        kks.createSpotWeldAction(gunInfo, kkc, m_xmlDoc, activityListElem);
                                    if (retractAction)
                                        kks.createSpotRetractAction(gunInfo, kkc, m_xmlDoc, activityListElem);
                                    spotAction = false;
                                    retractAction = false;
                                    break;
                                case 330: // BAS(
                                    createMotionProfile(line, motionProfileListElem);
                                    break;
                                case 340: // $sysVar
                                    processSysVarStatement(line, activityListElem);
                                    break;
                                case 350: // PULSE($OUT[x], TRUE, 0.5)
                                    processPulseStatement(line, activityListElem);
                                    break;
                                case 500: //[DECL ]PDAT PPDAT1={VEL number, ACC number, APO_DIST number}
                                    modifyMotionProfile("PDAT", line, motionProfileListElem);
                                    modifyAccuracyProfile("PDAT", line, accuracyProfileListElem);
                                    break;
                                case 510: //[DECL ]LDAT LCPDAT1={VEL number, ACC number, APO_DIST number[, APO_FAC 50.0]}
                                    modifyMotionProfile("LDAT", line, motionProfileListElem);
                                    modifyAccuracyProfile("LDAT", line, accuracyProfileListElem);
                                    break;
                                case 520: //[DECL ]FDAT Ftagname={TOOL_NO 1,BASE_NO 0,IPO_FRAME #BASE}
                                    modifyMoveActivityToolProfile(line);
                                    break;
                                case 530: //[DECL ]E6POS Xtagname={X float, ..., E6 float}
                                    modifyMoveActivity("E6POS", line);
                                    break;
                                case 535: //[DECL ]E3POS Xtagname={X float, ..., E3 float}
                                    modifyMoveActivity("E3POS", line);
                                    break;
                                case 540: //[DECL ]POS Xtagname={X float, ..., T int}
                                    modifyMoveActivity("POS", line);
                                    break;
                                case 550: //[DECL ]INT ...
                                    break;
                                case 560: //DECL SPOT_TYPE ...
                                    kks.spotDataDecl(line);
                                    kks.modifyWeldAttributes(resourceElem);
                                    break;
                                case 570: //DECL GRP_TYP ...
                                    kkgrp.grpDataDecl(line);
                                    kkgrp.modifyGrpAttributes(resourceElem);
                                    break;
                                case 700: //WAIT SEC number
                                    createDelayActivity(line, activityListElem);
                                    break;
                                case 710: //WAIT FOR $IN[number]==TRUE|FALSE
                                    createWaitSignalActivity(line, activityListElem);
                                    break;
                                case 800: //CONTINUE
                                    break;
                                case 810: //IF
                                    break;
                                case 820: //ENDIF
                                    break;
                                case 830: //WHILE TRUE
                                case 840: //ENDWHILE
                                    break;
                                case 3000: //subprogram call
                                    processCallStatement(line, activityListElem);
                                    break;
                                default:
                                    break;
                            }//end switch

                            //match found & processed, break out for loop
                            break;
                        }//end if
                        else
                        {
                            if (ii==m_keywords.length-1 && userProgStart)
                            {
                                if (false == line.equals("") &&
                                    false == processingUnrecog)
                                {
                                    size = commentLines.size();
                                    if (size>0)
                                    {
                                        currComment = (String)commentLines.get(size-1);
                                        if (currComment.startsWith("Robot Language:;FOLD "))
                                            commentCount++;
                                    }
                                    processingUnrecog = true;
                                }
                                if (prgNum == 1)
                                    commentLines.add(commentCount, "DAT:Robot Language:" + line);
                                else
                                    commentLines.add(commentCount, "Robot Language:" + line);
                                commentCount++;
                            }
                        }
                    }//end for
                    line = rbt.readLine();

                }//end while
                rbt.close();
            }//end try

            catch (IOException e)
            {
                try {
                    bw.write("ERROR-Caught IOException: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            catch (NullPointerException e)
            {
                try {
                    bw.write("ERROR-Caught NullPointerException: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-Caught ArrayIndexOutOfBoundsException: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            catch (IndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-Caught IndexOutOfBoundsException: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }

            return 0;
        }//end processRobotProgramLines

        private int processRobotLogicProgram(String logicProgramName)
        {
            BufferedReader rbt;
            String line;

            Element resourceElem = super.getResourceElement();
            Element programLogicElem = m_xmlDoc.createElement("ProgramLogic");
            resourceElem.appendChild(programLogicElem);
            Element inputsElem = m_xmlDoc.createElement("Inputs");
            programLogicElem.appendChild(inputsElem);
            Element outputsElem = m_xmlDoc.createElement("Outputs");
            programLogicElem.appendChild(outputsElem);
            Element jobsElem = m_xmlDoc.createElement("Jobs");
            programLogicElem.appendChild(jobsElem);

            String name = logicProgramName;
            int lastSlash = name.lastIndexOf('/');
            int lastBackSlash = name.lastIndexOf('\\');
            if (lastSlash < lastBackSlash)
                lastSlash = lastBackSlash;
            if (lastSlash < 0)
                lastSlash = 0;
            jobsElem.setAttribute("Name", name.substring(lastSlash+1, name.length()-4));

            Element jobElem = null;
            Matcher match;
            int errCode;

            try
            {
                rbt = new BufferedReader(new FileReader(logicProgramName));

                line = rbt.readLine();
                while (line != null)
                {
                    line = line.trim();
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                        continue;
                    }
                    if (line.equalsIgnoreCase("END"))
                    {
                        rbt.mark(80);
                        line = rbt.readLine();
                        if (line != null)
                        {
                            rbt.reset();

                            Element activityListElem = m_xmlDoc.createElement("ActivityList");
                            resourceElem.insertBefore(activityListElem, programLogicElem);

                            Element attributeListElem = m_xmlDoc.createElement("AttributeList");
                            activityListElem.appendChild(attributeListElem);

                            errCode = processRobotProgramLines(rbt, activityListElem, 0);
                            if (errCode != 0)
                                return errCode;
                        }
                        break;
                    }

                    for (int ii = 0; ii < m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);

                        if ( match.find() == true )
                        {
                            //Call appropriate methods to handle the input
                            switch(ii) {
                                case 340: // $sysVar
                                    jobElem = checkJobElem(jobElem, jobsElem);
                                    processSysVarLogicProg(line, jobElem);
                                    break;
                                case 710: //WAIT FOR $IN[number]==TRUE|FALSE
                                    jobElem = checkJobElem(jobElem, jobsElem);
                                    processWaitSignalLogicProg(line, jobElem);
                                    break;
                                case 810: //IF
                                    jobElem = processJobStartLogicProg(line, jobsElem);
                                    break;
                                case 820: //ENDIF
                                    jobElem = null;
                                    break;
                                case 830: //WHILE TRUE
                                case 840: //ENDWHILE
                                    break;
                                case 3000: //subprogram call
                                    jobElem = checkJobElem(jobElem, jobsElem);
                                    processCallLogicProg(line, jobElem);
                                    break;
                                default:
                                    break;
                            }//end switch

                            //match found & processed, break out for loop
                            break;
                        }//end if
                    }//end for
                    line = rbt.readLine();

                }//end while
            }

            catch (IOException e)
            {
                try {
                    bw.write("ERROR: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }

            Element ioElem;
            String ioNum;
            for (int ii=0; ii<inputNameNum.size(); ii++)
            {
                ioNum = (String)inputNameNum.get(ii);
                ioElem = m_xmlDoc.createElement("Input");
                ioElem.setAttribute("InputName", "DI"+ioNum);
                ioElem.setAttribute("InputNumber", ioNum);
                inputsElem.appendChild(ioElem);
            }

            for (int ii=0; ii<outputNameNum.size(); ii++)
            {
                ioNum = (String)outputNameNum.get(ii);
                ioElem = m_xmlDoc.createElement("Output");
                ioElem.setAttribute("OutputName", "DO"+ioNum);
                ioElem.setAttribute("OutputNumber", ioNum);
                outputsElem.appendChild(ioElem);
            }

            return 0;
        }//end processRobotLogicProgram

        private Element checkJobElem(Element jobElem, Element jobsElem)
        {
            if (jobElem == null)
            {
                jobElem = m_xmlDoc.createElement("Job");
                jobsElem.appendChild(jobElem);
                jobCount++;
                jobElem.setAttribute("Name", "Job"+jobCount);

                Element startConditionElem = m_xmlDoc.createElement("StartCondition");
                jobElem.appendChild(startConditionElem);

                Element expressionElem = m_xmlDoc.createElement("Expression");
                startConditionElem.appendChild(expressionElem);
                Text exprTrue = m_xmlDoc.createTextNode("true");
                expressionElem.appendChild(exprTrue);
            }

            return jobElem;
        } // end checkJobElem

        private Element processJobStartLogicProg(String line, Element jobsElem)
        {
            Element jobElem = m_xmlDoc.createElement("Job");
            jobsElem.appendChild(jobElem);
            jobCount++;
            jobElem.setAttribute("Name", "Job"+jobCount);

            Element startConditionElem = m_xmlDoc.createElement("StartCondition");
            jobElem.appendChild(startConditionElem);

            processLogicExpr(line.split("[\\s+]", 2)[1], startConditionElem);
            return jobElem;
        } // end processJobStartLogicProg

        private void processWaitSignalLogicProg(String line, Element jobElem)
        {
            Pattern p;
            Matcher m;

            p = Pattern.compile("WAIT\\s+FOR\\s+(.*)", Pattern.CASE_INSENSITIVE);
            m = p.matcher(line);
            if (m.find() == false)
                return;

            Element waitExprElem = m_xmlDoc.createElement("WaitExpression");
            jobElem.appendChild(waitExprElem);

            processLogicExpr(m.group(1), waitExprElem);
        } // end processWaitSignalLogicProg

        private void processLogicExpr(String logicExpr, Element waitExprElem)
        {
            Pattern [] p;
            Matcher m;
            int logicNot, holdOutput;
            Element opElem, exprElem;
            Text opText, exprText;
            String op, expr;

            p = new Pattern [1000];
            for (int i=0; i<1000; i++)
                p[i] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);

            p[40] = Pattern.compile("\\$IN\\[([0-9]+)\\]==(TRUE|FALSE)", Pattern.CASE_INSENSITIVE);
            p[50] = Pattern.compile("\\$IN\\[([0-9]+)\\]", Pattern.CASE_INSENSITIVE);
            p[100] = Pattern.compile("NOT", Pattern.CASE_INSENSITIVE);
            p[110] = Pattern.compile("AND", Pattern.CASE_INSENSITIVE);
            p[120] = Pattern.compile("OR",  Pattern.CASE_INSENSITIVE);

            String [] parts;
            String [] components = logicExpr.split("[\\(\\)]");
            int num = components.length;
            logicNot = 0;
            expr = "";
            holdOutput = 0;
            for (int cc=0; cc<num; cc++)
                components[cc] = components[cc].trim();
            for (int cc=0; cc<num; cc++)
            {
                parts = components[cc].split("[\\s+]");
                for (int ii=0; ii<parts.length; ii++)
                {
                    for (int pp=1; pp<p.length; pp++)
                    {
                        m = p[pp].matcher(parts[ii]);
                        if (m.find() == true)
                        {
                            switch(pp)
                            {
                                case 40: // $IN[number] == true|false
                                    addToArrayList(inputNameNum, m.group(1));
                                    expr = expr + "DI" + m.group(1) + " = " + m.group(2).toLowerCase();
                                    break;
                                case 50: // $IN[number]
                                    addToArrayList(inputNameNum, m.group(1));
                                    expr = expr + "DI" + m.group(1) + " = ";
                                    if (logicNot == 0)
                                        expr = expr + "true";
                                    else
                                        expr = expr + "false";
                                    holdOutput = 0;
                                    if (cc+1 < components.length && logicNot == 1)
                                    {
                                        if (components[cc+1].length() > 2 &&
                                            components[cc+1].substring(0,3).equalsIgnoreCase("AND") ||
                                            components[cc+1].length() > 1 &&
                                            components[cc+1].substring(0,2).equalsIgnoreCase("OR"))
                                            holdOutput = 1;
                                    }
                                    logicNot = 0;
                                    break;
                                case 100: // NOT
                                    logicNot = 1;
                                    break;
                                case 110: // AND
                                case 120: // OR
                                    op = parts[ii].toLowerCase();
                                    if (parts.length == 1)
                                    {
                                        if (expr.length() > 0)
                                        {
                                            exprElem = m_xmlDoc.createElement("Expression");
                                            waitExprElem.appendChild(exprElem);
                                            exprText = m_xmlDoc.createTextNode(expr);
                                            exprElem.appendChild(exprText);
                                            expr = "";
                                        }

                                        opElem = m_xmlDoc.createElement("Operator");
                                        waitExprElem.appendChild(opElem);
                                        opText = m_xmlDoc.createTextNode(op);
                                        opElem.appendChild(opText);
                                    }
                                    else
                                    {
                                        if (expr.length() > 0)
                                        {
                                            expr = expr + " " + op + " ";
                                            if (ii+1 < parts.length && parts[ii+1].equalsIgnoreCase("not"))
                                                holdOutput = 1;
                                        }
                                        else
                                        {
                                            // expr is empty, was output before getting here
                                            opElem = m_xmlDoc.createElement("Operator");
                                            waitExprElem.appendChild(opElem);
                                            opText = m_xmlDoc.createTextNode(op);
                                            opElem.appendChild(opText);
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            } // switch
                            break;
                        } // if
                    } // for pattern
                } // for parts split by " "

                if (expr.length() > 0 && holdOutput == 0)
                {
                    exprElem = m_xmlDoc.createElement("Expression");
                    waitExprElem.appendChild(exprElem);
                    exprText = m_xmlDoc.createTextNode(expr);
                    exprElem.appendChild(exprText);
                    expr = "";
                }
            } // for components split by [()]
            return;
        } // end processLogicExpr

        private void processSysVarLogicProg(String line, Element jobElem)
        {
            Pattern p;
            Matcher m;

            p = Pattern.compile("\\$OUT\\s*\\[\\s*([0-9]+)\\s*\\]\\s*=\\s*(TRUE|FALSE)", Pattern.CASE_INSENSITIVE);
            m = p.matcher(line);
            if (m.find() == true)
            {
                Element setOutput = m_xmlDoc.createElement("SetOutput");
                jobElem.appendChild(setOutput);
                
                String portNumber = m.group(1);
                String status = m.group(2);
                String setOutputStr = "DO" + portNumber + " = " + status.toLowerCase();
                Text setOutputVal = m_xmlDoc.createTextNode(setOutputStr);
                setOutput.appendChild(setOutputVal);

                addToArrayList(outputNameNum, portNumber);
            }
        } // end processSysVarLogicProg

        private void addToArrayList(ArrayList arrayList, String str)
        {
            int index = arrayList.indexOf(str);
            if (index < 0)
            {
                int num = arrayList.size();
                if (num == 0)
                {
                    arrayList.add(0, str);
                }
                else
                {
                    int ii, val;
                    for (ii=0; ii<num; ii++)
                    {
                        val = Integer.parseInt((String)arrayList.get(ii));
                        if ( val > Integer.parseInt(str))
                        {
                            arrayList.add(ii, str);
                            break;
                        }
                        if (ii+1 == num)
                            arrayList.add(num, str);
                    }
                }
            }
        } // end addToArrayList

        private void processCallLogicProg(String line, Element jobElem)
        {
            String [] progName = line.split("[(\\s]");
            progName[0] = progName[0].toUpperCase();
            Element callTask = m_xmlDoc.createElement("CallTask");
            jobElem.appendChild(callTask);
            Text callTaskVal = m_xmlDoc.createTextNode(progName[0]);
            callTask.appendChild(callTaskVal);

            int index = programCallNames.indexOf(progName[0]);
            if (index < 0)
                programCallNames.add(progName[0]);

            programCallCount++;
        } // end processCallLogicProg

        private void processCallStatement(String line, Element activityListElem)
        {
            String progName = line.substring(0, line.indexOf('(')).trim().toUpperCase();

            if (progName.indexOf(' ') != -1)
            {
                processComment(line, 0);
                return;
            }
            if (progName.equals("SET_TQ_VALUES"))
            {
                processingUnrecog = true;
                commentCount++;
                processComment(line, 0);
                return;
            }

            Element actElem = m_xmlDoc.createElement("Activity");
            activityListElem.appendChild(actElem);

            actElem.setAttribute("SimEventType", "CALLTASK");
            actElem.setAttribute("ActivityType", "DNBIgpCallRobotTask");

            Element activityName = m_xmlDoc.createElement("ActivityName");
            actElem.appendChild(activityName);

            programCallCount++;
            Text callActNameValue = m_xmlDoc.createTextNode("RobotCall." + programCallCount);
            activityName.appendChild(callActNameValue);

            Element callName = m_xmlDoc.createElement("CallName");
            actElem.appendChild(callName);
            Text callNameValue = m_xmlDoc.createTextNode(progName);
            callName.appendChild(callNameValue);

            int index = programCallNames.indexOf(progName);
            if (index < 0)
            {
                programCallNames.add(progName);
            }

            addAttributeList(actElem, "Pre");

            int idx = line.indexOf(';');
            if (idx > 0)
            {
                commentLines.add(commentCount, "InLineComment:" + line.substring(idx));
                commentCount++;
                addAttributeList(actElem, "Post");
            }
        } // end processCallStatement

        private void processDEFStatement(String line, Element activityListElem)
        {
            String [] components = line.split("[( \\t]+");
            String progName = components[1].toUpperCase();
            programCallNames.remove(progName);

            activityListElem.setAttribute("Task", progName);
            fileProgramList.add(fileProgramList.size(), progName);

        } // end processDEFStatement

        private void processHeader(String line, Element attributeListElem)
        {
            Element attributeElem = m_xmlDoc.createElement("Attribute");
            attributeListElem.appendChild(attributeElem);

            Element attributeNameElem = m_xmlDoc.createElement("AttributeName");
            attributeElem.appendChild(attributeNameElem);

            headerCount++;
            String headerName = "Header" + String.valueOf(headerCount);
            Text attribNameNode = m_xmlDoc.createTextNode(headerName);
            attributeNameElem.appendChild(attribNameNode);

            Element attributeValueElem = m_xmlDoc.createElement("AttributeValue");
            attributeElem.appendChild(attributeValueElem);

            String attribVal = line.substring(1);
            Text attribValNode = m_xmlDoc.createTextNode(attribVal);
            attributeValueElem.appendChild(attribValNode);
        } // end processHeader

        //"^\\s*USERSPOT\\*\\("
        private void processUSERSPOTStatement(String line)
        {
            String [] component= line.split("[\\(\\)\\s,]");
            if (component[1].equalsIgnoreCase("#INIT"))
                return;
            if (component[1].equalsIgnoreCase("#ADVSPOT"))
                spotAction = true;
        } // end processUSERSPOTStatement

        private void processTRIGGERStatement(String line)
        {
            int indexUSERSPOT = line.indexOf("USERSPOT");
            if (indexUSERSPOT>0)
            {
                int indexRETRACT = line.indexOf("#RETR");
                if (indexRETRACT>0)
                    retractAction = true;
                int indexPRESPOT = line.indexOf("#PRESPOT");
                if (indexPRESPOT>0)
                    spotAction = true;
                int indexSPOT = line.indexOf("#SPOT");
                if (indexSPOT>0)
                    spotAction = true;
            }
            else
            {
                commentLines.add(commentCount, "Robot Language:" + line);
                commentCount++;
            }
        } // end processTRIGGERStatement

        private void processFoldComment(String line, Element actListElem)
        {
            if (userProgStart == false)
                return;

            String comment;
            if (uploadStyle.equals("OperationComments"))
            {
                // Normal comment
                comment = kku.field(line, 2);
                Element actElem = m_xmlDoc.createElement("Activity");
                actListElem.appendChild(actElem);
                actElem.setAttribute("ActivityType", "Operation");
                Element activityName = m_xmlDoc.createElement("ActivityName");
                actElem.appendChild(activityName);
                String sActivityName = ";" + comment;
                Text activityNameTxt = m_xmlDoc.createTextNode(sActivityName);
                activityName.appendChild(activityNameTxt);
                
                Element attrListElem = m_xmlDoc.createElement("AttributeList");
                actElem.appendChild(attrListElem);
                Element attrElem = m_xmlDoc.createElement("Attribute");
                attrListElem.appendChild(attrElem);
                Element attrNameElem = m_xmlDoc.createElement("AttributeName");
                attrElem.appendChild(attrNameElem);
                Text attrNameTxt = m_xmlDoc.createTextNode("Comment");
                attrNameElem.appendChild(attrNameTxt);
                Element attrValueElem = m_xmlDoc.createElement("AttributeValue");
                attrElem.appendChild(attrValueElem);
                Text attrValueTxt = m_xmlDoc.createTextNode(comment);
                attrValueElem.appendChild(attrValueTxt);
            }
            else
            {
                if (line.indexOf("%VSTAMP") < 0)
                {
                    comment = kku.field(line, 2);
                }
                else
                {
                    comment = "StampComment:" + kku.field(line, 2) + "," +
                                                kku.field(line, 4) + "," +
                                                kku.field(line, 6);
                }
                commentLines.add(commentCount, comment);
                commentCount++;
            }
        } // end processFoldComment

        private void processFoldFields(String line)
        {
            signalName = null;

            if (line.indexOf("%MKUKATPBASIS,%COUT,%VOUTX") > 0 ||
                line.indexOf("%MKUKATPBASIS,%COUT,%VPULSE") > 0 ||
                line.indexOf("%MKUKATPBASIS,%CWAIT_FOR,%VWAIT_FOR") > 0)
            {
                signalName = kku.field(line, 3);
                if (signalName.startsWith("'"))
                    signalName = signalName.substring(1);
                if (signalName.endsWith("'"))
                    signalName = signalName.substring(0, signalName.length()-1);
            }
            if (line.indexOf("%MKUKATPBASIS,%CEXT_WAIT_FOR,%VEXT_WAIT_FOR") > 0)
                signalName = kku.field(line, 7);
        }

        private void processComment(String line, int prgNum)
        {
            if (userProgStart == false)
                return;

            // any line starts with ';'
            String datPrefix = "";
            if (prgNum == 1)
                datPrefix = "DAT:";
            if (line.startsWith(";FOLD "))
            {
                commentLines.add(commentCount, datPrefix + "Robot Language:" + line);
                if (line.indexOf("%MKUKATPBASIS") < 0 &&
                    line.indexOf("%MKUKATPGRP") < 0 &&
                    line.indexOf("%MKUKATPSPOT") < 0 &&
                    line.indexOf("%MKUKATPSERVOTECH") < 0 )
                {
                    commentCount++;
                    processingUnrecog = true;
                }
                else
                if (line.indexOf("%MKUKATPBASIS,%CIBUS,%VMAP") > 0 ||
                    line.indexOf("%MKUKATPBASIS,%CSYNCHRON,%VPROGSYNC") > 0 ||
                    line.indexOf("%MKUKATPBASIS,%COUT,%VSYNOUT") > 0)
                {
                    commentCount++;
                    processingUnrecog = true;
                }
                else
                    processingUnrecog = false;
                // increment commentCount if the following statement is unrecognized
                processFoldFields(line);
            }
            else
            if (line.startsWith(";ENDFOLD"))
            {
                if (processingUnrecog)
                {
                    commentLines.add(commentCount, datPrefix + "Robot Language:" + line);
                    commentCount++;
                }
                processingUnrecog = false;
            }
            else
            if (line.startsWith(";DELMIA Process Info:"))
            {
                String subLine = line.substring(line.indexOf(':')+1);
                String [] simuInfo = subLine.split("[;:]");
                for (int ii=0; ii<simuInfo.length; ii+=2)
                {
                    if (simuInfo[ii].equalsIgnoreCase("grabbingObj"))
                        kkc.grabbingObj = simuInfo[ii+1];
                    if (simuInfo[ii].equalsIgnoreCase("grabbedObj"))
                        kkc.grabbedObj = simuInfo[ii+1];
                }
            }
            else
            {
                commentLines.add(commentCount, datPrefix + "Robot Language:" + line);
                commentCount++;
            }
        } // end processComment

        private void addAttributeList(Element actElem, String prefix)
        {
            if (commentCount == 0)
            {
                // ";FOLD" is added to commentLines, but
                // commentCount is incremented only if the following line is unrecognized
                commentLines.clear();
                return;
            }

            int start = 0, ii;
            NodeList attributeNameNodes;
            Element attributeListElem;
            Element attributeElem, attributeNameElem, attributeValueElem;
            attributeListElem = kku.findChildElemByName(actElem, "AttributeList", null, null);
            if (attributeListElem == null)
            {
                attributeListElem = m_xmlDoc.createElement("AttributeList");
                actElem.appendChild(attributeListElem);
            }
            else
            {
                attributeNameNodes = attributeListElem.getElementsByTagName("AttributeName");
                for (ii=0; ii<attributeNameNodes.getLength(); ii++)
                {
                    attributeNameElem = (Element)attributeNameNodes.item(ii);
                    if (attributeNameElem.getFirstChild().getNodeValue().startsWith(prefix))
                        start++;
                }
            }

            Text attribNameNode, attribValNode;
            String commentName, attribVal;
            for (ii=0; ii<commentCount; ii++)
            {
                attributeElem = m_xmlDoc.createElement("Attribute");
                attributeListElem.appendChild(attributeElem);

                attributeNameElem = m_xmlDoc.createElement("AttributeName");
                attributeElem.appendChild(attributeNameElem);

                commentName = prefix + "Comment" + String.valueOf(ii+start+1);
                attribNameNode = m_xmlDoc.createTextNode(commentName);
                attributeNameElem.appendChild(attribNameNode);

                attributeValueElem = m_xmlDoc.createElement("AttributeValue");
                attributeElem.appendChild(attributeValueElem);

                attribVal = (String)commentLines.get(ii);
                attribValNode = m_xmlDoc.createTextNode(attribVal);
                attributeValueElem.appendChild(attribValNode);
            }
            commentCount = 0;
            commentLines.clear();
        } // end addAttributeList

        /* sets the ActivityList element's Task attribute to robot program name
         */
        private void processDEFDATStatement(String line, Element activityListElem)
        {
            int progNameIndex = line.indexOf('T');
            String programName = line.substring(progNameIndex + 1).toUpperCase();

            activityListElem.setAttribute("Task", programName.trim());
        }

        private void processSysVarStatement(String line, Element actListElem)
        {
            Pattern p;
            Matcher m;

            p = Pattern.compile("\\$OUT\\s*\\[\\s*([0-9]+)\\s*\\]\\s*=\\s*(TRUE|FALSE)", Pattern.CASE_INSENSITIVE);
            m = p.matcher(line);
            if (m.find() == true)
            {
                createSetSignalActivity(line, m, actListElem);
                return;
            }

            if (line.startsWith("$BWDSTART") || line.startsWith("$H_POS"))
                return;

            commentLines.add(commentCount, "Robot Language:" + line);
            commentCount++;
        }

        private void processPulseStatement(String line, Element actListElem)
        {
            Pattern p;
            Matcher m;

            p = Pattern.compile("\\(\\s*\\$OUT\\s*\\[\\s*([0-9]+)\\s*\\]\\s*,\\s*(TRUE|FALSE)\\s*,\\s*([0-9]+\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);
            m = p.matcher(line);
            if (m.find() == true)
            {
                createSetSignalActivity(line, m, actListElem);
                return;
            }
        }

        // $OUT[number]=TRUE|FALSE
        private void createSetSignalActivity(String line, Matcher m, Element actListElem)
        {
            String portNumber = m.group(1);
            String status = m.group(2);
            String duration;
            if (m.groupCount()==3)
                duration = m.group(3);
            else
                duration = "0";

            Element actElem = m_xmlDoc.createElement("Activity");
            actListElem.appendChild(actElem);

            actElem.setAttribute("SimEventType", "SETSIGNAL");
            actElem.setAttribute ("ActivityType", "DNBSetSignalActivity");

            Element actNameElem = m_xmlDoc.createElement("ActivityName");
            actElem.appendChild(actNameElem);
            Text setIOActNameValue = m_xmlDoc.createTextNode(line);
            actNameElem.appendChild(setIOActNameValue);

            Element setIOAttribElem = m_xmlDoc.createElement("SetIOAttributes");
            actElem.appendChild(setIOAttribElem);

            Element ioSetSignal = m_xmlDoc.createElement("IOSetSignal");
            setIOAttribElem.appendChild(ioSetSignal);
           
            ioSetSignal.setAttribute("PortNumber", portNumber);
            if (signalName != null)
                ioSetSignal.setAttribute("SignalName", signalName);
            else
                ioSetSignal.setAttribute("SignalName", portNumber);
            if (status.equalsIgnoreCase("TRUE"))
            {
                ioSetSignal.setAttribute("SignalValue", "On");
            }
            else
            {
                ioSetSignal.setAttribute("SignalValue", "Off");
            }
            
            ioSetSignal.setAttribute("SignalDuration", duration);

            addAttributeList(actElem, "Pre");
        } // end createSetSignalActivity

        // WAIT FOR $IN[number]==TRUE|FALSE
        private void createWaitSignalActivity(String line, Element actListElem)
        {
            Pattern [] p;
            Matcher m;
            p = new Pattern [3];
            p[0] = Pattern.compile("^WAIT\\s+FOR\\s+\\$IN\\s*\\[\\s*([0-9]+)\\s*\\]\\s*==\\s*(TRUE|FALSE)\\s*$", Pattern.CASE_INSENSITIVE);
            p[1] = Pattern.compile("^WAIT\\s+FOR\\s+\\(\\s*\\$IN\\s*\\[\\s*([0-9]+)\\s*\\]\\s*\\)\\s*$", Pattern.CASE_INSENSITIVE);
            p[2] = Pattern.compile("^WAIT\\s+FOR\\s+NOT\\s+\\(\\s*\\$IN\\s*\\[\\s*([0-9]+)\\s*\\]\\s*\\)\\s*$", Pattern.CASE_INSENSITIVE);

            String portNumber = "1";
            String status = "TRUE";
            int ii;
            for (ii=0; ii<p.length; ii++)
            {
                m = p[ii].matcher(line);

                if (m.find() == true)
                {
                    portNumber = m.group(1);
                    switch(ii)
                    {
                        case 0: // $IN[x] == TRUE|FALSE
                            status = m.group(2);
                            break;
                        case 1: // ( $IN[x] )
                            status = "TRUE";
                            break;
                        case 2: // NOT ( $IN[x] )
                            status = "FALSE";
                            break;
                                    }
                    // match found & processed, break out for loop
                    break;
                }
            }
            if (ii==p.length)
            {
                /* increment commentCount if there is preceding <;FOLD > line
                 */
                int size = commentLines.size();
                if (size>0)
                {
                    String currComment = (String)commentLines.get(size-1);
                    if (currComment.startsWith("Robot Language:;FOLD "))
                        commentCount++;
                }

                commentLines.add(commentCount, "Robot Language:" + line);
                commentCount++;
                processingUnrecog = true;

                return;
            }

            Element actElem = m_xmlDoc.createElement("Activity");
            actListElem.appendChild(actElem);

            actElem.setAttribute("SimEventType", "WAITSIGNAL");
            actElem.setAttribute("ActivityType", "DNBWaitSignalActivity");

            Element actNameElem = m_xmlDoc.createElement("ActivityName");
            actElem.appendChild(actNameElem);
            Text setIOActNameValue = m_xmlDoc.createTextNode(line);
            actNameElem.appendChild(setIOActNameValue);

            Element waitIOAttribElem = m_xmlDoc.createElement("WaitIOAttributes");
            actElem.appendChild(waitIOAttribElem);

            Element ioWaitSignal = m_xmlDoc.createElement("IOWaitSignal");
            waitIOAttribElem.appendChild(ioWaitSignal);

            ioWaitSignal.setAttribute("PortNumber", portNumber);
            if (signalName != null)
                ioWaitSignal.setAttribute("SignalName", signalName);
            else
                ioWaitSignal.setAttribute("SignalName", portNumber);
            if (status.equalsIgnoreCase("TRUE"))
            {
                ioWaitSignal.setAttribute("SignalValue", "On");
            }
            else
            {
                ioWaitSignal.setAttribute("SignalValue", "Off");
            }

            ioWaitSignal.setAttribute("MaxWaitTime", "0");

            addAttributeList(actElem, "Pre");
        } // end createWaitSignalActivity

        // WAIT SEC 1.23457
        private void createDelayActivity(String line, Element actListElem)
        {
            String [] argStrings = line.split("[\\s+]");
            Element actElem;

            String actName = "DelayActivity." + kkc.delay++;
            Double startTime = new Double(0.0), endTime = Double.valueOf(argStrings[2]);
            actElem = kku.createDelayActivity(actListElem, actName, startTime, endTime, null, null, m_xmlDoc);

            addAttributeList(actElem, "Pre");
        } // end createDelayActivity

        //PDAT_ACT=PPDAT1
        //LDAT_ACT=LCPDAT1
        private void selectMoAccuProfile(String line)
        {
            int index = line.indexOf('=');
            String profileName = line.substring(index + 1).trim().substring(1);
            currMotionProfile = profileName;
            currAccuracyProfile = profileName;
        } // end selectMoAccuProfile

        // FDAT_ACT = FP1
        private void selectFrameProfile(String line)
        {
            int index = line.indexOf('=');
            String profileName = line.substring(index + 1).trim().substring(1);
            if (profileName.equalsIgnoreCase("HOME"))
            {
                currToolProfile = k_toolProfileNames[0];
                if (currToolProfile == null)
                    currToolProfile = DEFAULT;
                currObjFrameProfile = DEFAULT;
            }
            else
            {
                currToolProfile = profileName;
                currObjFrameProfile = profileName;
            }
        } // end selectFrameProfile

        //BAS(#*, *)
        private void createMotionProfile( String line, Element motionProfileListElem )
        {
            //<MotionProfile>
                //<Name>Default</Name>
                //<MotionBasis>Percent</MotionBasis>
                //<Speed Units="%" Value="50" />
                //<Accel Units="%" Value="100" />
                //<AngularSpeedValue Units="%" Value="100" />
                //<AngularAccelValue Units="%" Value="100" />
            //</MotionProfile>
            if (kku.listHasProfile(motionProfileListElem, "MotionProfile", currMotionProfile))
                return;

            int index = line.indexOf('(');
            String tmpstr = line.substring(index + 1).trim();

            String [] argStrings = tmpstr.split("[,)]");
            String moType = argStrings[0].trim();
            if (moType.equalsIgnoreCase("#PTP_PARAMS") == false &&
                moType.equalsIgnoreCase("#CP_PARAMS") == false &&
                moType.equalsIgnoreCase("#VEL_PTP") == false &&
                moType.equalsIgnoreCase("#VEL_CP") == false)
                return;

            String speedStr = argStrings[1].trim();
            try
            {
                Float dummyFloat = new Float(speedStr);
            }
            catch (NumberFormatException nfe)
            {
                speedStr = "-1";
            }

            Element motionProfileElem = m_xmlDoc.createElement("MotionProfile");
            motionProfileListElem.appendChild(motionProfileElem);

            Element motionProfileName = m_xmlDoc.createElement("Name");
            motionProfileElem.appendChild(motionProfileName);
            Text nameValue = m_xmlDoc.createTextNode(currMotionProfile);
            motionProfileName.appendChild(nameValue);

            Element motionBasisElem = m_xmlDoc.createElement("MotionBasis");
            motionProfileElem.appendChild(motionBasisElem);

            Text moBasisVal;
            if (moType.equalsIgnoreCase("#CP_PARAMS") ||
                moType.equalsIgnoreCase("#VEL_CP"))
            {
                moBasisVal = m_xmlDoc.createTextNode("Absolute");
            }
            else
            {
                //BAS(#PTP_PARAMS|VEL_PTP, ...
                moBasisVal = m_xmlDoc.createTextNode("Percent");
            }
            motionBasisElem.appendChild(moBasisVal);

            Element speedElem = m_xmlDoc.createElement("Speed");
            motionProfileElem.appendChild(speedElem);
            if (moType.equalsIgnoreCase("#CP_PARAMS") ||
                moType.equalsIgnoreCase("#VEL_CP"))
            {
                speedElem.setAttribute("Units", "m/s");
            }
            else
            {
                //BAS(#PTP_PARAMS|VEL_PTP, ...
                speedElem.setAttribute("Units", "%");
            }
            speedElem.setAttribute("Value", speedStr);

            Element accelElem = m_xmlDoc.createElement("Accel");
            motionProfileElem.appendChild(accelElem);
            accelElem.setAttribute("Units", "%");
            accelElem.setAttribute("Value", "100");

            Element asvElem = m_xmlDoc.createElement("AngularSpeedValue");
            motionProfileElem.appendChild(asvElem);
            asvElem.setAttribute("Units", "%");
            asvElem.setAttribute("Value", "100");

            Element aacElem = m_xmlDoc.createElement("AngularAccelValue");
            motionProfileElem.appendChild(aacElem);
            aacElem.setAttribute("Units", "%");
            aacElem.setAttribute("Value", "100");
        } // end createMotionProfile

        //[DECL ]PDAT|LDAT PPDAT1|LCPDAT1={VEL number, ACC number, APO_DIST number[, APO_FAC 50.0]}
        private void modifyMotionProfile( String dataType, String line, Element motionProfileListElem )
        {
            int index = line.indexOf(dataType);
            String tmpStr = line.substring(index + dataType.length()).trim().substring(1);
            String [] argStrings = tmpStr.split("[={},\\s+]");
            String profileName = argStrings[0];
            String speedString = argStrings[3];
            String accelString = argStrings[5];

            Element motionProfileElem, nameElem, speedElem, accelElem;
            NodeList motionProfileNodes = motionProfileListElem.getChildNodes();
            for (int ii=0; ii<motionProfileNodes.getLength(); ii++)
            {
                motionProfileElem = (Element)motionProfileNodes.item(ii);
                if (motionProfileElem.getTagName().equals("MotionProfile"))
                {
                    nameElem = kku.findChildElemByName(motionProfileElem, "Name", null, null);
                    if (nameElem.getFirstChild().getNodeValue().equals(profileName))
                    {
                        speedElem = kku.findChildElemByName(motionProfileElem, "Speed", "Value", "-1");
                        if ( speedElem != null )
                            speedElem.setAttribute("Value", speedString);

                        accelElem = kku.findChildElemByName(motionProfileElem, "Accel", null, null);
                        accelElem.setAttribute("Value", accelString);
                    }
                }
            }
        } // end modifyMotionProfile

        /* This method creates the AccuracyProfile element
           PTP  Xtagname [C_PTP [C_DIS|C_VEL|C_ORI]]
           LIN  Xtagname        [C_DIS|C_VEL|C_ORI]
           CIRC Xtagvia Xtagtgt [C_DIS|C_VEL|C_ORI]
         */
        private void createAccuracyProfile( String line, Element accuracyProfileListElem )
        {
            if (kku.listHasProfile(accuracyProfileListElem, "AccuracyProfile", currAccuracyProfile))
                return;

            String [] argStrings = line.split("\\s+");
            /*
            <AccuracyProfile>
                <Name>Default</Name>
                <FlyByMode>Off</FlyByMode>
                <AccuracyType>Speed</AccuracyType>
                <AccuracyValue Units="%" Value="0" />
            </AccuracyProfile>
            */
            Element accuracyProfileElem = m_xmlDoc.createElement("AccuracyProfile");
            accuracyProfileListElem.appendChild(accuracyProfileElem);

            Element accuracyProfileName = m_xmlDoc.createElement("Name");
            accuracyProfileElem.appendChild(accuracyProfileName);
            Text nameValue = m_xmlDoc.createTextNode(currAccuracyProfile);
            accuracyProfileName.appendChild(nameValue);

            Element flyByElem = m_xmlDoc.createElement("FlyByMode");
            accuracyProfileElem.appendChild(flyByElem);
            
            Text flyByValue;
            if (argStrings[0].length() == 3 && argStrings.length >= 3 ||
                argStrings[0].length() == 4 && argStrings.length == 4)
            {
                flyByValue = m_xmlDoc.createTextNode("On");
            }
            else
            {
                flyByValue = m_xmlDoc.createTextNode("Off");
            }
            flyByElem.appendChild(flyByValue);

            Element accuracyTypeElem = m_xmlDoc.createElement("AccuracyType");
            accuracyProfileElem.appendChild(accuracyTypeElem);
            Text typeValue = m_xmlDoc.createTextNode("Speed");

            Element accuracyValueElem = m_xmlDoc.createElement("AccuracyValue");
            accuracyProfileElem.appendChild(accuracyValueElem);
            accuracyValueElem.setAttribute("Units", "%");
            // Will be set when processing PDAT/LDAT statement
            accuracyValueElem.setAttribute("Value", "100.0");

            if (argStrings[0].length() == 3 && argStrings.length == 3)
            {
                if (argStrings[2].equalsIgnoreCase("C_PTP") ||
                    argStrings[2].equalsIgnoreCase("C_DIS"))
                {
                    typeValue = m_xmlDoc.createTextNode("Distance");
                    accuracyValueElem.setAttribute("Units", "m");
                    accuracyValueElem.setAttribute("Value", "0.1");
                }
            }

            if (argStrings[0].equalsIgnoreCase("PTP") && argStrings.length == 4)
            {
                if (argStrings[2].equalsIgnoreCase("C_PTP") &&
                    argStrings[3].equalsIgnoreCase("C_DIS"))
                {
                    typeValue = m_xmlDoc.createTextNode("Distance");
                    accuracyValueElem.setAttribute("Units", "m");
                    accuracyValueElem.setAttribute("Value", "0.1");
                }
            }

            if (argStrings[0].length() == 4 && argStrings.length == 4)
            {
                if (argStrings[3].equalsIgnoreCase("C_DIS"))
                {
                    typeValue = m_xmlDoc.createTextNode("Distance");
                    accuracyValueElem.setAttribute("Units", "m");
                    accuracyValueElem.setAttribute("Value", "0.1");
                }
            }

            accuracyTypeElem.appendChild(typeValue);
        } // end createAccuracyProfile

        /* This method set the Value attribute of AccuracyValue
          [DECL ]PDAT|LDAT PPDAT1|LCPDAT1={VEL number, ACC number, APO_DIST number[, APO_FAC 50.0]}
         */
        private void modifyAccuracyProfile(String moType, String line, Element accuracyProfileListElem)
        {
            /*
            <AccuracyProfile>
                <Name>Default</Name>
                <FlyByMode>Off</FlyByMode>
                <AccuracyType>Speed</AccuracyType>
                <AccuracyValue Units="%" Value="0" />
            </AccuracyProfile>
            */
            int index = line.indexOf(moType);
            String tmpStr = line.substring(index + moType.length()).trim().substring(1);
            String [] argStrings = tmpStr.split("=");
            String profileName = argStrings[0];
            
            Pattern p = Pattern.compile("APO_DIST\\s+[0-9]+\\.?[0-9]*", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(line);
            if (m.find() == false)
            {
                return;
            }
            
            tmpStr = m.group();
            String [] accStrings = tmpStr.split("\\s+");
            String accVal = accStrings[1];

            Element accuracyProfileElem, nameElem, accValElem;
            NodeList accuracyProfileNodes = accuracyProfileListElem.getChildNodes();
            for (int ii=0; ii<accuracyProfileNodes.getLength(); ii++)
            {
                accuracyProfileElem = (Element)accuracyProfileNodes.item(ii);
                if (accuracyProfileElem.getTagName().equals("AccuracyProfile"))
                {
                    nameElem = kku.findChildElemByName(accuracyProfileElem, "Name", null, null);
                    if (nameElem.getFirstChild().getNodeValue().equals(profileName))
                    {
                        accValElem = kku.findChildElemByName(accuracyProfileElem, "AccuracyValue", null, null);
                        String accValUnits = accValElem.getAttribute("Units");
                        if (accValUnits.equals("m"))
                        {
                            accVal = String.valueOf(Float.valueOf(accVal).floatValue()/1000);
                        }
                        accValElem.setAttribute("Value", accVal);
                        break;
                    }
                }
            }
        } // end modifyAccuracyProfile

        /* This method creates the Activity element for the following statements
         * PTP  Xtagname [C_PTP [C_DIS|C_VEL|C_ORI]]
         * LIN  Xtagname        [C_DIS|C_VEL|C_ORI]
         * CIRC Xtagvia Xtagtgt [C_DIS|C_VEL|C_ORI]
         */
        private void processMove( String line, Element actListElem )
        {
            String tagName = null, tagGroup = null, tagPart = null;
            String moveType = "Joint";
            Element actElem;
            String [] argStrings = line.split(",?\\s+");
            int idxgroup = argStrings[1].indexOf(ktg);
            int idxpart = argStrings[1].indexOf(ktp);
            int idxinst = argStrings[1].lastIndexOf("_");
            if (idxgroup > 0 && idxpart > idxgroup && argStrings[1].substring(idxinst+1).matches("[0-9]+"))
            {
                tagName = argStrings[1].substring(1, idxgroup);
                tagGroup = argStrings[1].substring(idxgroup+ktgL, idxpart);
                tagPart = argStrings[1].substring(idxpart+ktpL, idxinst);
            }
            else
            {
                tagName = argStrings[1].substring(1);
            }

            if (argStrings[0].equalsIgnoreCase("LIN"))
                moveType = "Linear";
            else
            if (argStrings[0].equalsIgnoreCase("CIRC"))
                moveType = "CircularVia";

            actElem = createMoveActivity(actListElem, moveType, tagName, tagGroup, tagPart);
            addAttributeList(actElem, "Pre");

            if (argStrings[0].equalsIgnoreCase("CIRC"))
            {
                idxgroup = argStrings[2].indexOf(ktg);
                idxpart = argStrings[2].indexOf(ktp);
                idxinst = argStrings[2].lastIndexOf("_");
                if (idxgroup > 0 && idxpart > idxgroup && argStrings[2].substring(idxinst+1).matches("[0-9]+"))
                {
                    tagName = argStrings[2].substring(1, idxgroup);
                    tagGroup = argStrings[2].substring(idxgroup+ktgL, idxpart);
                    tagPart = argStrings[2].substring(idxpart+ktpL, idxinst);
                }
                else
                {
                    tagName = argStrings[2].substring(1);
                }

                actElem = createMoveActivity(actListElem, "Circular", tagName, tagGroup, tagPart);
            }
        }

        private Element createMoveActivity( Element actListElem, String moveType, String tagName,
                                            String tagGroup, String tagPart )
        {
            Element actElem = m_xmlDoc.createElement("Activity");
            actListElem.appendChild(actElem);
            actElem.setAttribute("SimEventType", "IGRIPMove");
            actElem.setAttribute("ActivityType", "DNBRobotMotionActivity");

            Element actName = m_xmlDoc.createElement("ActivityName");
            actElem.appendChild(actName);
            Text actNameValue = m_xmlDoc.createTextNode("RobotMotion." + kkc.move++);
            actName.appendChild(actNameValue);

            Element motionAttribElem = m_xmlDoc.createElement("MotionAttributes");
            actElem.appendChild(motionAttribElem);

            Element motionProfileElem = m_xmlDoc.createElement("MotionProfile");
            motionAttribElem.appendChild(motionProfileElem);
            Text motionProfileName = m_xmlDoc.createTextNode(currMotionProfile);
            motionProfileElem.appendChild(motionProfileName);

            Element toolProfileElem = m_xmlDoc.createElement("ToolProfile");
            motionAttribElem.appendChild(toolProfileElem);
            Text toolProfileName = m_xmlDoc.createTextNode(currToolProfile);
            toolProfileElem.appendChild(toolProfileName);

            Element accuracyProfileElem = m_xmlDoc.createElement("AccuracyProfile");
            motionAttribElem.appendChild(accuracyProfileElem);
            Text accuracyProfileName = m_xmlDoc.createTextNode(currAccuracyProfile);
            accuracyProfileElem.appendChild(accuracyProfileName);

            Element objFrameProfileElem = m_xmlDoc.createElement("ObjectFrameProfile");
            motionAttribElem.appendChild(objFrameProfileElem);
            Text objFrameProfileName = m_xmlDoc.createTextNode(currObjFrameProfile);
            objFrameProfileElem.appendChild(objFrameProfileName);

            Element motionTypeElem = m_xmlDoc.createElement("MotionType");
            motionAttribElem.appendChild(motionTypeElem);

            Text motionTypeName = m_xmlDoc.createTextNode(moveType);
            motionTypeElem.appendChild(motionTypeName);

            if (moveType.equalsIgnoreCase("Linear")   ||
                moveType.equalsIgnoreCase("Circular") ||
                moveType.equalsIgnoreCase("CircularVia"))
            {
                Element orientModeElem = m_xmlDoc.createElement("OrientMode");
                motionAttribElem.appendChild(orientModeElem);

                Text orientModeName = m_xmlDoc.createTextNode("1_Axis");
                orientModeElem.appendChild(orientModeName);
            }

            /* create Target element here and plant Tag name for later
             * search and modify Target when processing E6POS/POS statements
             */
            Element targetElem = m_xmlDoc.createElement("Target");
            actElem.appendChild(targetElem);

            int idx = jTgtName_arrList.indexOf(tagName);
            if (idx >= 0)
            {
                double [] axis = (double [])jTgtAval_arrList.get(idx);

                targetElem.setAttribute(DEFAULT, "Joint");
                Element jTargetElem = m_xmlDoc.createElement("JointTarget");
                targetElem.appendChild(jTargetElem);

                Element jElem, jvalElem;
                Text jvalText;
                for (int ii=0; ii<6; ii++)
                {
                    jElem = m_xmlDoc.createElement("Joint");
                    jTargetElem.appendChild(jElem);
                    jElem.setAttribute("DOFNumber", String.valueOf(ii+1));
                    jElem.setAttribute("JointName", "Joint " + String.valueOf(ii+1));
                    jElem.setAttribute("JointType", m_axisTypes[ii]);
                    if (m_axisTypes[ii].equals("Rotational"))
                    {
                        jElem.setAttribute("Units", "deg");
                        jvalText = m_xmlDoc.createTextNode(String.valueOf(axis[ii]));
                    }
                    else
                    {
                        jElem.setAttribute("Units", "m");
                        jvalText = m_xmlDoc.createTextNode(String.valueOf(axis[ii]/1000.0));
                    }
                    jvalElem = m_xmlDoc.createElement("JointValue");
                    jElem.appendChild(jvalElem);
                    jvalElem.appendChild(jvalText);
                }

                if (m_numAuxAxes + m_numExtAxes + m_numPositionerAxes > 0)
                {
                    double [] ext  = (double [])jTgtEval_arrList.get(idx);
                    String jvalStr;
                    int Doffset = 0, Koffset;

                    for (int dgrp=0; dgrp<3; dgrp++)
                    {
                        if (dgrp > 0)
                            Doffset = Doffset + DauxAxisNum[dgrp-1];

                        Koffset = 0;
                        for (int kgrp=0; kgrp<3; kgrp++)
                        {
                            if (DauxAxisGrp[dgrp].equals(KauxAxisGrp[kgrp]))
                                break;
                            else
                                Koffset = Koffset + KauxAxisNum[kgrp];
                        }
                        for (int jj=1; jj<=DauxAxisNum[dgrp]; jj++)
                        {
                            jvalStr = Double.toString(ext[jj-1+Koffset]);
                            createAuxJointElem(jTargetElem, jj, Doffset, DauxAxisGrp[dgrp], jvalStr);
                        }
                    }
                }
            }
            else
            {
                targetElem.setAttribute(DEFAULT, "Cartesian");
/*            
                Element baseWRTWorldElem = m_xmlDoc.createElement("BaseWRTWorld");
                targetElem.appendChild(baseWRTWorldElem);

                Element basePositionElem = m_xmlDoc.createElement("Position");
                baseWRTWorldElem.appendChild(basePositionElem);

                basePositionElem.setAttribute("X", "0.0");
                basePositionElem.setAttribute("Y", "0.0");
                basePositionElem.setAttribute("Z", "0.0");
                basePositionElem.setAttribute("Units", "m");

                Element baseOrientationElem = m_xmlDoc.createElement("Orientation");
                baseWRTWorldElem.appendChild(baseOrientationElem);

                baseOrientationElem.setAttribute("Yaw", "0.0");
                baseOrientationElem.setAttribute("Pitch", "0.0");
                baseOrientationElem.setAttribute("Roll", "0.0");
                baseOrientationElem.setAttribute("Units", "deg");
*/            
                Element cartTargetElem = m_xmlDoc.createElement("CartesianTarget");
                targetElem.appendChild(cartTargetElem);

                Element positionElem = m_xmlDoc.createElement("Position");
                cartTargetElem.appendChild(positionElem);
                positionElem.setAttribute("X", "0.0");
                positionElem.setAttribute("Y", "0.0");
                positionElem.setAttribute("Z", "0.0");

                Element orientationElem = m_xmlDoc.createElement("Orientation");
                cartTargetElem.appendChild(orientationElem);
                orientationElem.setAttribute("Yaw", "0.0");
                orientationElem.setAttribute("Pitch", "0.0");
                orientationElem.setAttribute("Roll", "0.0");

                Element cfgElem = m_xmlDoc.createElement("Config");
                cartTargetElem.appendChild(cfgElem);
                cfgElem.setAttribute("Name", "S6");

                Element turnSignElem = m_xmlDoc.createElement("TurnSign");
                cartTargetElem.appendChild(turnSignElem);

                Element tsJointElem;
                tsJointElem = m_xmlDoc.createElement("TSJoint");
                turnSignElem.appendChild(tsJointElem);
                tsJointElem.setAttribute("Number", "1");
                tsJointElem.setAttribute("Value", "+");
                tsJointElem = m_xmlDoc.createElement("TSJoint");
                turnSignElem.appendChild(tsJointElem);
                tsJointElem.setAttribute("Number", "4");
                tsJointElem.setAttribute("Value", "+");
                tsJointElem = m_xmlDoc.createElement("TSJoint");
                turnSignElem.appendChild(tsJointElem);
                tsJointElem.setAttribute("Number", "5");
                tsJointElem.setAttribute("Value", "+");
                tsJointElem = m_xmlDoc.createElement("TSJoint");
                turnSignElem.appendChild(tsJointElem);
                tsJointElem.setAttribute("Number", "6");
                tsJointElem.setAttribute("Value", "+");

                /* plant Tag name here for later identify the Target element to modify
                 */
                Element tagElem = m_xmlDoc.createElement("Tag");
                cartTargetElem.appendChild(tagElem);
                Text tagNameTextNode = m_xmlDoc.createTextNode(tagName);
                tagElem.appendChild(tagNameTextNode);

                if (tagGroup != null)
                    tagElem.setAttribute("TagGroup", tagGroup);
                
                if (tagPart != null)
                    tagElem.setAttribute("AttachedToPart", tagPart);

            }
            return actElem;
        } // end createMoveActivity

        /* This method adds the Target element to Activity for the following statements
         *[DECL ]E6POS Xtagname={X float, Y float, Z float, A float, B float, C float, S int, T int,
         *                       E1 float, E2 float, E3 float, E4 float, E5 float, E6 float}
         *[DECL ]POS   Xtagname={X float, Y float, Z float, A float, B float, C float, S int, T int}
         */
        private void modifyMoveActivity( String posType, String line )
        {
            String tagName = null, tagGroup = null, tagPart = null, tagCombo = null;
            int index = line.indexOf(posType);
            String tmpstr = line.substring(index + posType.length()).trim().substring(1);

            String [] argStrings = tmpstr.split("=");
            int idxgroup = argStrings[0].indexOf(ktg);
            int idxpart = argStrings[0].indexOf(ktp);
            int idxinst = argStrings[0].lastIndexOf("_");
            if (idxgroup > 0 && idxpart > idxgroup && argStrings[0].substring(idxinst+1).matches("[0-9]+"))
            {
                tagName = argStrings[0].substring(0, idxgroup);
                tagGroup = argStrings[0].substring(idxgroup+ktgL, idxpart);
                tagPart = argStrings[0].substring(idxpart+ktpL, idxinst);
                tagCombo = tagGroup + ";" + tagPart;
            }
            else
            {
                tagName = argStrings[0];
            }

            Element actListElem, actElem, tgtElem, cartTgtElem;
            Element tagElem, positionElem, orientationElem, cfgElem, turnSignElem;
            Element jTgtElem, auxJointElem, jValElem;
            Text jVal;
            NodeList actNodes;

            Element resourceElem = super.getResourceElement();
            actListElem = (Element)resourceElem.getFirstChild();
            while (actListElem != null)
            {
                if (actListElem.getTagName().equals("ActivityList") &&
                    fileProgramList.contains(actListElem.getAttribute("Task").toUpperCase()))
                {
                    actNodes = actListElem.getChildNodes();
                    for (int ii=0; ii<actNodes.getLength(); ii++)
                    {
                        actElem = (Element)actNodes.item(ii);
                        if (actElem.getNodeName().equals("Activity") &&
                            actElem.getAttribute("SimEventType").equals("IGRIPMove"))
                        {
                            tgtElem = kku.findChildElemByName(actElem, "Target", DEFAULT, "Cartesian");
                            if (tgtElem == null)
                                continue;

                            cartTgtElem = kku.findChildElemByName(tgtElem, "CartesianTarget", null, null);
                            if (tagGroup!=null && tagPart!=null)
                                tagElem = kku.findChildElemByName(cartTgtElem, "Tag", "TagGroup;AttachedToPart", tagCombo);
                            else
                                tagElem = kku.findChildElemByName(cartTgtElem, "Tag", null, null);

                            if (tagElem.getFirstChild().getNodeValue().equalsIgnoreCase(tagName))
                            {
                                positionElem = kku.findChildElemByName(cartTgtElem, "Position", null, null);
                                setPositionValues( positionElem, line );
                                orientationElem = kku.findChildElemByName(cartTgtElem, "Orientation", null, null);
                                setOrientationValues( orientationElem, line );

                                cfgElem = kku.findChildElemByName(cartTgtElem, "Config", null, null);
                                Pattern cfgP = Pattern.compile("S\\s+\\d+", Pattern.CASE_INSENSITIVE);
                                Matcher cfgM = cfgP.matcher(line);
                                if (cfgM.find() == true)
                                {
                                    String cfgString = cfgM.group();
                                    String [] cfgComponents = cfgString.split("[\\s]+");
                                    int status = Integer.parseInt(cfgComponents[1]);
                                    while (status >= 8)
                                        status -= 8;
                                    cfgElem.setAttribute("Name", "S" + status);
                                }

                                turnSignElem = kku.findChildElemByName(cartTgtElem, "TurnSign", null, null);
                                Pattern turnP = Pattern.compile("T\\s+[0-6]?[0-9]", Pattern.CASE_INSENSITIVE);
                                Matcher turnM = turnP.matcher(line);
                                if (turnM.find() == true)
                                {
                                    String turnString = turnM.group();
                                    String [] turnComponents = turnString.split("[\\s]+");
                                    int turnNumber = Integer.valueOf(turnComponents[1]).intValue();

                                    setTurnSign(turnSignElem, turnNumber, 1);
                                    setTurnSign(turnSignElem, turnNumber, 4);
                                    setTurnSign(turnSignElem, turnNumber, 5);
                                    setTurnSign(turnSignElem, turnNumber, 6);
                                }

                                if (m_numAuxAxes + m_numExtAxes + m_numPositionerAxes > 0 &&
                                    (posType.equalsIgnoreCase("E3POS") || posType.equalsIgnoreCase("E6POS") ) )
                                {
                                    Pattern extAxisP;
                                    Matcher extAxisM;
                                    String  extAxisStr, patStr;
                                    String [] extAxisComponents;
                                    String [] extAxisValStr = new String [6];
                                    for (int aux=1; aux<=6; aux++)
                                    {
                                        patStr = "E" + String.valueOf(aux) + numPatStr;
                                        extAxisP = Pattern.compile(patStr, Pattern.CASE_INSENSITIVE);
                                        extAxisM = extAxisP.matcher(line);
                                        if (extAxisM.find() == true)
                                        {
                                            extAxisStr = extAxisM.group();
                                            extAxisComponents = extAxisStr.split("[\\s+]+");
                                            extAxisValStr[aux-1] = extAxisComponents[1].trim();
                                        }
                                    }

                                    jTgtElem = m_xmlDoc.createElement("JointTarget");
                                    tgtElem.appendChild(jTgtElem);

                                    int Doffset, Koffset;
                                    Doffset = 0;
                                    for (int dgrp=0; dgrp<3; dgrp++)
                                    {
                                        if (dgrp > 0)
                                            Doffset = Doffset + DauxAxisNum[dgrp-1];

                                        Koffset = 0;
                                        for (int kgrp=0; kgrp<3; kgrp++)
                                        {
                                            if (DauxAxisGrp[dgrp].equals(KauxAxisGrp[kgrp]))
                                                break;
                                            else
                                                Koffset = Koffset + KauxAxisNum[kgrp];
                                        }
                                        for (int jj=1; jj<=DauxAxisNum[dgrp]; jj++)
                                        {
                                            createAuxJointElem(jTgtElem, jj, Doffset, DauxAxisGrp[dgrp], extAxisValStr[jj-1+Koffset]);
                                            
                                            if (DauxAxisGrp[dgrp].equals(auxTool) && kksvg != null)
                                                kksvg.modifySVGOpenJoint(actElem, m_axisTypes[jj-1+m_numRobotAxes+Doffset], extAxisValStr[jj-1+Koffset], m_xmlDoc);
                                        }
                                    }
                                }
                                addAttributeList(actElem, "Pre");
                                //break;
                            }
                        }
                    }
                }
                actListElem = (Element)actListElem.getNextSibling();
            }
        } // end modifyMoveActivity

        private void createAuxJointElem( Element pElem, int auxNum, int offset, String auxType, String auxVal )
        {
            Element auxJointElem, jValElem;
            Text jVal;
            int auxDOFNum = auxNum + m_numRobotAxes + offset;
            float auxJointVal = Float.valueOf(auxVal).floatValue();

            auxJointElem = m_xmlDoc.createElement("AuxJoint");
            pElem.appendChild(auxJointElem);

            jValElem = m_xmlDoc.createElement("JointValue");
            auxJointElem.appendChild(jValElem);

            auxJointElem.setAttribute("Type", auxType);
            auxJointElem.setAttribute("DOFNumber", String.valueOf(auxDOFNum));
            auxJointElem.setAttribute("JointName", "Joint " + String.valueOf(auxNum));
            auxJointElem.setAttribute("JointType", m_axisTypes[auxDOFNum-1]);
            if (m_axisTypes[auxDOFNum-1].equals("Rotational"))
            {
                auxJointElem.setAttribute("Units", "deg");
                jVal = m_xmlDoc.createTextNode(String.valueOf(auxJointVal));
            }
            else
            {
                auxJointElem.setAttribute("Units", "m");
                jVal = m_xmlDoc.createTextNode(String.valueOf(auxJointVal/1000.0));
            }
            jValElem.appendChild(jVal);
        } //end createAuxJointElem

        //DECL FDAT FP1={TOOL_NO 1,BASE_NO 0,IPO_FRAME #BASE}
        private void modifyMoveActivityToolProfile( String line )
        {
            Element objFrameProfileList;
            NodeList objFrameProfiles;
            objFrameProfileList = super.getObjectFrameProfileListElement();
            objFrameProfiles = objFrameProfileList.getElementsByTagName("ObjectFrameProfile");
            
            int index = line.indexOf("FDAT");
            String tmpstr = line.substring(index + 4).trim().substring(1);

            String [] argStrings = tmpstr.split("=");
            String profileName = argStrings[0]; // P1

            String toolNumberStr = "0";
            int toolNumber = 0;
            Pattern p = Pattern.compile("TOOL_NO\\s+[0-9]+", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(line);
            if (m.find() == true)
            {
                String toolString = m.group();
                String [] toolComponents = toolString.split("[\\s]+");
                toolNumberStr = toolComponents[1];
                toolNumber = Integer.parseInt(toolNumberStr);
            }

            String baseNumberStr = "0";
            int baseNumber = 0, baseNumberSave = 0;
            p = Pattern.compile("BASE_NO\\s+[0-9]+",Pattern.CASE_INSENSITIVE);
            m = p.matcher(line);
            if (m.find() == true)
            {
                String baseString = m.group();
                String [] baseComponents = baseString.split("[\\s]+");
                baseNumberStr = baseComponents[1];
                baseNumber = Integer.parseInt(baseNumberStr);
                baseNumberSave = baseNumber;
            }
            String partName = getPartName(baseNumber); // get partName by baseNumber before swap with toolNumber

            boolean fixedTCP = false;
            p = Pattern.compile("IPO_FRAME\\s+#(BASE|TCP)", Pattern.CASE_INSENSITIVE);
            m = p.matcher(line);
            if (m.find() == true)
                fixedTCP = m.group(1).equalsIgnoreCase("TCP");

            if (fixedTCP == true)
            {
                swapBaseToolProfile(baseNumber, toolNumber);
                
                String tmpStr = toolNumberStr;
                toolNumberStr = baseNumberStr;
                baseNumberStr = tmpStr;
                
                int tmpInt = toolNumber;
                toolNumber = baseNumber;
                baseNumber = tmpInt;
            }

            String v5ProfileName = DEFAULT;
            Element tgtElem, cartTgtElem, tagElem;
            Element actListElem, actElem, moAttribElem, toolProfileElem, objFrameProfileElem;
            Element attributeList, attribute, attributeName, attributeValue;
            Element nameElem;
            Element objFrame, objFramePos, objFrameOri;
            Text textNode;
            String aName = "AttributeName";
            String aVal = "AttributeValue";
            String kir = "_kuka_kir_ref_frame_to_part_xform_", objFrameProfileName;
            String kir_base = "_robot_base_to_ref_robot_base_";
            char [] cc = {'x', 'y', 'z', 'w', 'p', 'r'};
            String [] xyzwpr;
            xyzwpr = new String [6];
            NodeList actNodes;
            int ii, jj, kk, mdef, numObjProfiles;

            Element resourceElem = super.getResourceElement();
            Element genInfo = kku.findChildElemByName(resourceElem, "GeneralInfo", null, null);
            Element pickPart = kku.findChildElemByName(genInfo, "SelectPartsOnUpload", null, null);
            actListElem = (Element)resourceElem.getFirstChild();
            while (actListElem != null)
            {
                if (actListElem.getTagName().equals("ActivityList") &&
                    fileProgramList.contains(actListElem.getAttribute("Task").toUpperCase()))
                {
                    actNodes = actListElem.getChildNodes();
                    for (ii=0; ii<actNodes.getLength(); ii++)
                    {
                        actElem = (Element)actNodes.item(ii);
                        if (actElem.getNodeName().equals("Activity") &&
                            actElem.getAttribute("SimEventType").equals("IGRIPMove"))
                        {
                            moAttribElem = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
                            toolProfileElem = kku.findChildElemByName(moAttribElem, "ToolProfile", null, null);
                            if (toolProfileElem.getFirstChild().getNodeValue().equalsIgnoreCase(profileName))
                            {
                                v5ProfileName = toolNameIndex.get(toolNumber).toString();
                                if (v5ProfileName.length() == 0)
                                    v5ProfileName = k_toolProfileNames[toolNumber]; // k_toolProfileNames is from V5 toolProfile list
                                if (v5ProfileName.length() == 0)
                                    toolProfileElem.getFirstChild().setNodeValue(DEFAULT);
                                
                                toolProfileElem.getFirstChild().setNodeValue(v5ProfileName);
                            }
                            
                            boolean useV5objProfile = false;
                            // baseNameIndex is from $config.dat
                            v5ProfileName = baseNameIndex.get(baseNumber).toString();
                            if (v5ProfileName.length() == 0)
                            {
                                // k_objProfileNames is from V5 objProfile list
                                v5ProfileName = k_objProfileNames[baseNumber];
                                if (v5ProfileName.length() > 0)
                                    useV5objProfile = true;
                            }
                            if (v5ProfileName.length() == 0)
                                v5ProfileName = "ObjectProfile." + baseNumberStr;

                            objFrameProfileElem = kku.findChildElemByName(moAttribElem, "ObjectFrameProfile", null, null);
                            if (objFrameProfileElem.getFirstChild().getNodeValue().equalsIgnoreCase(profileName))
                            {
                                if (baseNumberStr.equals("0"))
                                    objFrameProfileElem.getFirstChild().setNodeValue(DEFAULT);
                                else
                                    objFrameProfileElem.getFirstChild().setNodeValue(v5ProfileName);
                                /* transformCartTgt4ZeroBase can be called here since a JointTarget/AuxJoint is created
                                 * after processing E6POS line.
                                 */
                                tgtElem = kku.findChildElemByName(actElem, "Target", DEFAULT, "Cartesian");
                                if (tgtElem != null && baseNumberSave == 0)
                                    transformCartTgt4ZeroBase(tgtElem);
                                
                                if (partName.length() > 0)
                                {
                                    tgtElem = kku.findChildElemByName(actElem, "Target", DEFAULT, "Cartesian");
                                    cartTgtElem = kku.findChildElemByName(tgtElem, "CartesianTarget", null, null);
                                    tagElem = kku.findChildElemByName(cartTgtElem, "Tag", null, null);
                                    tagElem.setAttribute("AttachedToPart", partName);
                                    tagElem.setAttribute("AttachmentType", "ModifyReference");
                                    tagElem.setAttribute("TagGroup", "TagGroup");
                                }
                                else if (useV5objProfile == false)
                                {
                                    /* search Controller/ObjectFrameProfileList to see if an OFP needs
                                     * to be created
                                     */
                                    numObjProfiles = objFrameProfiles.getLength();
                                    for (jj=0; jj<numObjProfiles; jj++)
                                    {
                                        objFrameProfileElem = (Element)objFrameProfiles.item(jj);
                                        nameElem = kku.findChildElemByName(objFrameProfileElem, "Name", null, null);
                                        if (nameElem.getFirstChild().getNodeValue().equals(v5ProfileName))
                                            break;
                                    }
                                    if (jj == numObjProfiles)
                                    {
                                        pickPart.getFirstChild().setNodeValue("true");

                                        objFrameProfileElem = m_xmlDoc.createElement("ObjectFrameProfile");
                                        objFrameProfileList.appendChild(objFrameProfileElem);

                                        nameElem = m_xmlDoc.createElement("Name");
                                        objFrameProfileElem.appendChild(nameElem);

                                        textNode = m_xmlDoc.createTextNode(v5ProfileName);
                                        nameElem.appendChild(textNode);

                                        objFrame = m_xmlDoc.createElement("ObjectFrame");
                                        objFrameProfileElem.appendChild(objFrame);

                                        objFrame.setAttribute("ReferenceFrame", "World");

                                        objFramePos = m_xmlDoc.createElement("ObjectFramePosition");
                                        objFrame.appendChild(objFramePos);
                                        setPositionValues(objFramePos, "x 0.0,y 0.0,z 0.0");

                                        objFrameOri = m_xmlDoc.createElement("ObjectFrameOrientation");
                                        objFrame.appendChild(objFrameOri);
                                        setOrientationValues(objFrameOri, "a 0.0,b 0.0,c 0.0");
                                    }
                                }

                                // KIR attributes
                                if (kirAttribute[baseNumber]==true)
                                {
                                    if (elemHasAttribute(actElem, "_kuka_kir_ref_frame"))
                                        continue;

                                    attributeList = kku.findChildElemByName(actElem, "AttributeList", null, null);
                                    if (attributeList == null)
                                    {
                                        attributeList = m_xmlDoc.createElement("AttributeList");
                                        actElem.appendChild(attributeList);
                                    }

                                    for (jj=0; jj<6; jj++)
                                        xyzwpr[jj] = "0.0";
                                    numObjProfiles = objFrameProfiles.getLength();
                                    for (jj=0; jj<numObjProfiles; jj++)
                                    {
                                        objFrameProfileElem = (Element)objFrameProfiles.item(jj);
                                        nameElem = kku.findChildElemByName(objFrameProfileElem, "Name", null, null);
                                        objFrameProfileName = nameElem.getFirstChild().getNodeValue();

                                        if (objFrameProfileName.equals(v5ProfileName))
                                        {
                                            objFrame = kku.findChildElemByName(objFrameProfileElem, "ObjectFrame", null, null);
                                            objFramePos = kku.findChildElemByName(objFrame, "ObjectFramePosition", null, null);
                                            objFrameOri = kku.findChildElemByName(objFrame, "ObjectFrameOrientation", null, null);
                                            xyzwpr[0] = objFramePos.getAttribute("X");
                                            xyzwpr[1] = objFramePos.getAttribute("Y");
                                            xyzwpr[2] = objFramePos.getAttribute("Z");
                                            xyzwpr[3] = objFrameOri.getAttribute("Yaw");
                                            xyzwpr[4] = objFrameOri.getAttribute("Pitch");
                                            xyzwpr[5] = objFrameOri.getAttribute("Roll");
                                            xyzwpr[3] = String.valueOf(Float.valueOf(xyzwpr[3]).floatValue()*TO_RAD);
                                            xyzwpr[4] = String.valueOf(Float.valueOf(xyzwpr[4]).floatValue()*TO_RAD);
                                            xyzwpr[5] = String.valueOf(Float.valueOf(xyzwpr[5]).floatValue()*TO_RAD);

                                            break;
                                        }
                                    }

                                    attribute = m_xmlDoc.createElement("Attribute");
                                    attributeList.appendChild(attribute);
                                    attributeName = m_xmlDoc.createElement(aName);
                                    attribute.appendChild(attributeName);
                                    textNode = m_xmlDoc.createTextNode("_kuka_kir_ref_frame");
                                    attributeName.appendChild(textNode);

                                    attributeValue = m_xmlDoc.createElement(aVal);
                                    attribute.appendChild(attributeValue);
                                    textNode = m_xmlDoc.createTextNode(kirParent[baseNumber] + "_FLANGE");
                                    attributeValue.appendChild(textNode);

                                    for (kk=0; kk<6; kk++)
                                    {
                                        attribute = m_xmlDoc.createElement("Attribute");
                                        attributeList.appendChild(attribute);
                                        attributeName = m_xmlDoc.createElement(aName);
                                        attribute.appendChild(attributeName);
                                        textNode = m_xmlDoc.createTextNode(kir + cc[kk]);
                                        attributeName.appendChild(textNode);

                                        attributeValue = m_xmlDoc.createElement(aVal);
                                        attribute.appendChild(attributeValue);
                                        textNode = m_xmlDoc.createTextNode(xyzwpr[kk]);
                                        attributeValue.appendChild(textNode);
                                    }

                                    for (mdef=0; mdef<17; mdef++)
                                    {
                                        if (kirMachine[mdef].equalsIgnoreCase(kirParent[baseNumber]))
                                        {
                                            for (kk=0; kk<6; kk++)
                                            {
                                                attribute = m_xmlDoc.createElement("Attribute");
                                                attributeList.appendChild(attribute);
                                                attributeName = m_xmlDoc.createElement(aName);
                                                attribute.appendChild(attributeName);
                                                textNode = m_xmlDoc.createTextNode(kir_base + cc[kk]);
                                                attributeName.appendChild(textNode);

                                                attributeValue = m_xmlDoc.createElement(aVal);
                                                attribute.appendChild(attributeValue);
                                                textNode = m_xmlDoc.createTextNode(kirMachineBase[mdef][kk]);
                                                attributeValue.appendChild(textNode);
                                            }
                                            break;
                                        }
                                    }
                                } // if (kirAttribute[baseNumber]==true)
                            }
                        }
                    }
                }
                actListElem = (Element)actListElem.getNextSibling();
            }
        } // end modifyMoveActivityToolProfile
        
        private void swapBaseToolProfile(int baseNum, int toolNum)
        {
            if (true == basetoolSwapped[baseNum])
                return;
            
            // remove obj profile and create a stationary tool profile
            String baseName = baseNameIndex.get(baseNum).toString();
            if (baseName.length() == 0)
                baseName = "ObjectProfile." + baseNum;
            String toolName = toolNameIndex.get(toolNum).toString();
            if (toolName.length() == 0)
                toolName = "ToolProfile." + toolNum;
            Element baseListElem = super.getObjectFrameProfileListElement();
            Element toolListElem = super.getToolProfileListElement();
            NodeList baseNodes = baseListElem.getChildNodes();
            Element baseElem, nameElem, tmpElem, frame, pos, ori, refElem;
            Element toolElem, toolTypeElem, tcpOffsetElem, tcpPositionElem, tcpOrientationElem;
            for (int ii=0; ii<baseNodes.getLength(); ii++)
            {
                baseElem = (Element)baseNodes.item(ii);
                if (baseElem.getTagName().equals("ObjectFrameProfile"))
                {
                    nameElem = kku.findChildElemByName(baseElem, "Name", null, null);
                    if (nameElem.getFirstChild().getNodeValue().equals(baseName))
                    {
                        tmpElem = (Element)baseListElem.removeChild(baseElem);
                        
                        refElem = (Element)toolListElem.getChildNodes().item(baseNum+1);
                        toolElem = m_xmlDoc.createElement("ToolProfile");
                        if (refElem != null)
                            toolListElem.insertBefore(toolElem, refElem);
                        else
                            toolListElem.appendChild(toolElem);
                        nameElem.getFirstChild().setNodeValue(toolName);
                        toolElem.appendChild(nameElem);
                        toolTypeElem = m_xmlDoc.createElement("ToolType");
                        toolElem.appendChild(toolTypeElem);
                        toolTypeElem.appendChild(m_xmlDoc.createTextNode("StationaryRobot"));
                        tcpOffsetElem = m_xmlDoc.createElement("TCPOffset");
                        toolElem.appendChild(tcpOffsetElem);
                        tcpPositionElem = m_xmlDoc.createElement("TCPPosition");
                        tcpOffsetElem.appendChild(tcpPositionElem);
                        tcpOrientationElem = m_xmlDoc.createElement("TCPOrientation");
                        tcpOffsetElem.appendChild(tcpOrientationElem);
                        
                        frame = kku.findChildElemByName(tmpElem, "ObjectFrame", null, null);
                        pos = kku.findChildElemByName(frame, "ObjectFramePosition", null, null);
                        ori = kku.findChildElemByName(frame, "ObjectFrameOrientation", null, null);
                        tcpPositionElem.setAttribute("X", pos.getAttribute("X"));
                        tcpPositionElem.setAttribute("Y", pos.getAttribute("Y"));
                        tcpPositionElem.setAttribute("Z", pos.getAttribute("Z"));
                        tcpOrientationElem.setAttribute("Yaw",   ori.getAttribute("Yaw"));
                        tcpOrientationElem.setAttribute("Pitch", ori.getAttribute("Pitch"));
                        tcpOrientationElem.setAttribute("Roll",  ori.getAttribute("Roll"));
                        
                        break;
                    }
                }
            }
            
            // remove tool profile and create an obj profile
            NodeList toolNodes = toolListElem.getChildNodes();
            for (int ii=0; ii<toolNodes.getLength(); ii++)
            {
                toolElem = (Element)toolNodes.item(ii);
                if (toolElem.getTagName().equals("ToolProfile"))
                {
                    nameElem = kku.findChildElemByName(toolElem, "Name", null, null);
                    toolTypeElem = kku.findChildElemByName(toolElem, "ToolType", null, null);
                    if (nameElem.getFirstChild().getNodeValue().equals(toolName) &&
                        toolTypeElem.getFirstChild().getNodeValue().equalsIgnoreCase("StationaryRobot") == false)
                    {
                        tmpElem = (Element)toolListElem.removeChild(toolElem);
                        
                        refElem = (Element)baseListElem.getChildNodes().item(toolNum+1);
                        baseElem = m_xmlDoc.createElement("ObjectFrameProfile");
                        if (refElem != null)
                            baseListElem.insertBefore(baseElem, refElem);
                        else
                            baseListElem.appendChild(baseElem);
                        nameElem.getFirstChild().setNodeValue(baseName);
                        baseElem.appendChild(nameElem);
                        frame = m_xmlDoc.createElement("ObjectFrame");
                        baseElem.appendChild(frame);
                        frame.setAttribute("ReferenceFrame", "World");
                        pos = m_xmlDoc.createElement("ObjectFramePosition");
                        ori = m_xmlDoc.createElement("ObjectFrameOrientation");
                        frame.appendChild(pos);
                        frame.appendChild(ori);
                        
                        tcpOffsetElem = kku.findChildElemByName(tmpElem, "TCPOffset", null, null);
                        tcpPositionElem = kku.findChildElemByName(tcpOffsetElem, "TCPPosition", null, null);
                        tcpOrientationElem = kku.findChildElemByName(tcpOffsetElem, "TCPOrientation", null, null);
                        pos.setAttribute("X", tcpPositionElem.getAttribute("X"));
                        pos.setAttribute("Y", tcpPositionElem.getAttribute("Y"));
                        pos.setAttribute("Z", tcpPositionElem.getAttribute("Z"));
                        ori.setAttribute("Yaw",   tcpOrientationElem.getAttribute("Yaw"));
                        ori.setAttribute("Pitch", tcpOrientationElem.getAttribute("Pitch"));
                        ori.setAttribute("Roll",  tcpOrientationElem.getAttribute("Roll"));
                        
                        break;
                    }
                }
            }
            
            basetoolSwapped[baseNum] = true;
        }
        
        private boolean elemHasAttribute(Element elem, String attr)
        {
            Element attributeList = kku.findChildElemByName(elem, "AttributeList", null, null);
            if (attributeList == null)
                return false;

            Element attribute, attributeName;
            NodeList attributeNodes = attributeList.getElementsByTagName("Attribute");
            for (int ii=0; ii<attributeNodes.getLength(); ii++)
            {
                attribute = (Element)attributeNodes.item(ii);
                attributeName = kku.findChildElemByName(attribute, "AttributeName", null, null);
                if (attributeName == null)
                    return false;

                if (attributeName.getFirstChild().getNodeValue().equals(attr))
                    return true;
            }
            return false;
        }
        /* This method creates a TSJoint element, appends to turnSignElem,
         * sets attribute Value to '+' or '-'
         * Kuka turn bit is set to 1 if joint value is negative
         */
        private void setTurnSign(Element turnSignElem, int k_turn, int joint)
        {
            int [] divider = {1, 1, 2, 4, 8, 16, 32};
            int turnBit, ii;

            String jointNumStr = Integer.toString(joint);
            Element tsJointElem = kku.findChildElemByName(turnSignElem, "TSJoint", "Number", jointNumStr);

            //Element tsJointElem = m_xmlDoc.createElement("TSJoint");
            //turnSignElem.appendChild(tsJointElem);
            tsJointElem.setAttribute("Number", Integer.toString(joint));
            
            turnBit = k_turn;
            for (ii=6; ii>joint; ii--)
            {
                turnBit %= divider[ii];
            }
            turnBit = turnBit/divider[joint];
            if (1 == turnBit)
            {
                tsJointElem.setAttribute("Value", "-");
            }
            else
            {
                tsJointElem.setAttribute("Value", "+");
            }
        } // end setTurnSign

        /* This method sets the 'X', 'Y', and 'Z' attributes of posElem based on
         * the info in the argument 'line': X num, Y num, Z num
         */
        private void setPositionValues(Element posElem, String line)
        {
            Pattern p;
            Matcher m;
            String posString, valueComponent, patternStr;
            String [] posComponents;
            float xyz;
            DecimalFormat posFormatter = new DecimalFormat("0.0######");
            String resultStr;

            for (char cc='X'; cc<='Z'; cc++)
            {
                patternStr = cc + numPatStr;
                p = Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
                m = p.matcher(line);
                if (m.find() == true)
                {
                    posString = m.group();
                    posComponents = posString.split("[\\s]+");
                    valueComponent = posComponents[1];
                    xyz = Float.valueOf(valueComponent.trim()).floatValue()/1000;
                    resultStr = posFormatter.format(xyz);
                    //posElem.setAttribute(Character.toString(cc), resultStr);
                    posElem.setAttribute(Character.toString(cc), String.valueOf(xyz));
                }
            }

            posElem.setAttribute("Units", "m");
        } // end setPositionValues

        /* This method sets the rotation components of oriElem based on
         * the info in the argument 'line': A num, B num, C num
         * A->Roll, B->Pitch, C->Yaw
         */
        private void setOrientationValues(Element oriElem, String line)
        {
            Pattern p;
            Matcher m;
            String oriString, valueComponent, patternStr;
            String [] oriComponents;
            String [] oriNames = {"Roll", "Pitch", "Yaw"};
            float xyz;

            for (char cc='C'; cc>='A'; cc--)
            {
                patternStr = cc + numPatStr;
                p = Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
                m = p.matcher(line);
                if (m.find() == true)
                {
                    oriString = m.group();
                    oriComponents = oriString.split("[\\s]+");
                    valueComponent = oriComponents[1];
                    xyz = Float.valueOf(valueComponent.trim()).floatValue();
                    oriElem.setAttribute(oriNames[cc-'A'], String.valueOf(xyz));
                }
            }

            oriElem.setAttribute("Units", "deg");
        } // end setOrientationValues

        private boolean zeroPos(String line)
        {
            Pattern p;
            Matcher m;
            char [] cc = {'X', 'Y', 'Z', 'A', 'B', 'C'};
            String patternStr, posStr, valueComponent;
            String [] posComponents;
            float xyzabc, sqrSum = 0;

            String [] pos = line.split("[{}]");
            for (int ii=0; ii<6; ii++)
            {
                patternStr = cc[ii] + numPatStr;
                p = Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
                m = p.matcher(pos[1]);
                if (m.find() == true)
                {
                    posStr = m.group();
                    posComponents = posStr.split("[\\s]+");
                    valueComponent = posComponents[1];
                    xyzabc = Float.valueOf(valueComponent.trim()).floatValue();
                    sqrSum = sqrSum + xyzabc*xyzabc;
                }
            }

            if (sqrSum < 0.000001)
                return true;
            else
                return false;
        } // end nonZeroToolBase

    public void processPartBaseMap(String line)
    {
        String [] components = new String[3];
        String partName;
        int baseNum, qstart, qend;

        components = line.split("[\\[\\]]", 3);
        baseNum = Integer.parseInt(components[1]);

        components = line.split("[{},]", 3);

        qstart = components[1].indexOf('\"')+1;
        qend   = components[1].lastIndexOf('\"');
        partName = components[1].substring(qstart, qend);

        partIndex.set(baseNum, partName);
    }

    public String getPartName(int baseIndex)
    {
        return (String)partIndex.get(baseIndex);
    }

    public int initSVG()
    {
        try
        {
            String gunInfo = m_mountedTool+";"+openHome+";"+closeHome+";"+weldHome+";"+m_axisTypes[m_numRobotAxes]+";"+svgNeg;
            kksvg = new servogun(gunInfo);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            try {
                bw.write("ERROR-Robot must have an external axis to upload Servo Gun program.\n");
            }
            catch (IOException bwe) {
            }

            return 1;
        }

        return 0;
    }

    public void initGRP()
    {
        // gripper home sequence: open, close, and mid(if any)
        String grpInfo = m_mountedTool+";"+grpOpenHome+";"+grpCloseHome+";"+grpMidHome;
        kkgrp = new gripper(grpInfo);
    }

}//end class
