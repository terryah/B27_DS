//DOM and Parser classes
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;

//Classes for DELMIA Translator
import Kuka.util;
import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class DensoUploader extends DNBIgpOlpUploadBaseClass {

        // Constants
        private static final int JOINT = 1, CARTESIAN = 2;
        private static final String VERSION = "Delmia Corp. Denso Wave PAC Uploader Version 5 Release 24 SP0.\n";
        private static final String COPYRIGHT = "Copyright Delmia Corp. 2012, All Rights Reserved.\n";

        // Member primitive variables
        private int m_targetType;
        private int m_numRobotAxes;
        private int m_numAuxAxes;
        private int m_numExtAxes;
        private int m_numWorkAxes;
        private double [] auxValues = null;
        private boolean reportAuxAxisMismatch = true;
        private String m_ApproachDist = null;
        private int m_ApproachCounter;
        private String m_DepartDist = null;
        // X, Y, Z, Yaw, Pitch, Roll, Figure, type
        // J1, J2, J3, J4, J5, J6, (Figure), type
        private double [] prevTarget = {0,0,0,0,0,0, -1, 0};
        private String sPrevTargetName = null;
        private int operationCommentCounter;
        private int rbtLangCounter;
        private int m_delayCounter;
        private int m_moveCounter;
        private int m_motionProfileCounter, m_accuracyProfileCounter;
        private int programCallCount;
        private int robotCallNumber;
        private int commentCount, headerCount;
        private int numCircularMoves;
        private float m_Spd, m_Accel, m_Decel;
        private String m_motionProfileName = "Default";
        private String m_accuracyProfileName = "Default";

        private boolean m_commentWaitSignal;
        private String p_section = "";

        private String ioName = "DO[";
        private String m_program = "PRO1.PAC";

        //Member objects
        private Document m_xmlDoc;
        private String m_currentTagPoint;
        private int p_currentToolNumber;
        private int xrc_pl = -1;
        private String m_mountedTool;
        private ArrayList mountedTools;
        private Pattern [] m_keywords;
        private String pConst, jConst;
        private String [] m_axisTypes;
        private String [] p_toolProfileNames;
        private String m_toolProfileName = "Default";
        private String m_objectProfileName = "Default";
        private ArrayList p_listOfRobotTargets;
        private ArrayList posCommented;
        private ArrayList posUsed;
        private int tgtNum;
        private ArrayList m_listOfAuxTargets;
        private ArrayList m_listOfExtTargets;
        private ArrayList m_listOfSpeedValues;
        private ArrayList p_listOfAccuracyValues;
        private ArrayList programCallNames;
        private ArrayList commentLines;
        private String inLineComment;
        private ArrayList inputNameNum, outputNameNum;
        private Hashtable m_targetTypes;
        private Hashtable m_targetTools;
        private Hashtable m_tagNames;
        private ArrayList p_listOfAuxTargets;
        private ArrayList p_listOfExtTargets;
        private Hashtable p_targetTypes;
        private Hashtable p_targetTools;
        private Hashtable p_tagNames;
        private Hashtable positionVariableTbl;
        private Hashtable jointVariableTbl;
        private Hashtable variableTypeTbl;
        private String prgFileEncoding = "US-ASCII";
        private String uploadStyle = "General";
        private String speedArcSet = null;
        private String orientMode;
        private util kku;
        private static BufferedWriter bw;

        //Constructor
        public DensoUploader(String [] parameters) throws ParserConfigurationException
        {
            super(parameters);
            m_targetType = 0;
            operationCommentCounter = 1;
            rbtLangCounter = 1;
            m_delayCounter = 1;
            m_moveCounter = 0;
            m_motionProfileCounter = 1;
            m_accuracyProfileCounter = 1;
            programCallCount = 0;
            robotCallNumber = 1;
            commentCount = 0;
            headerCount = 1;
            m_ApproachCounter = 1;
            orientMode = "2_axis";

            m_mountedTool = parameters[6];
            mountedTools = new ArrayList(1);
            mountedTools.add(0, m_mountedTool);

            String [] toolProfileNames = parameters[7].split("[\\t]+");
            String [] homeNames = parameters[9].split("[\\t]+");

            //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1
            m_numRobotAxes = super.getNumberOfRobotAxes();
            m_numAuxAxes   = super.getNumberOfRailAuxiliaryAxes();
            m_numExtAxes   = super.getNumberOfToolAuxiliaryAxes();
            m_numWorkAxes  = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();

            m_currentTagPoint = new String("");
            p_currentToolNumber = 0;

            m_keywords = new Pattern [300];
            pConst = new String("");
            jConst = new String("");
            m_axisTypes = new String[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];
            m_axisTypes = super.getAxisTypes();
            positionVariableTbl = new Hashtable();
            jointVariableTbl = new Hashtable();
            variableTypeTbl = new Hashtable();
            p_listOfRobotTargets = new ArrayList(5);
            posCommented = new ArrayList(5);
            posUsed = new ArrayList(5);
            tgtNum = 0;
            m_listOfAuxTargets = new ArrayList(2);
            m_listOfExtTargets = new ArrayList(2);
            m_listOfSpeedValues = new ArrayList(5);
            p_listOfAccuracyValues = new ArrayList(3);
            m_targetTypes = new Hashtable();
            m_targetTools = new Hashtable();
            m_tagNames = new Hashtable();

            p_listOfAuxTargets = new ArrayList(2);
            p_listOfExtTargets = new ArrayList(2);
            p_targetTypes = new Hashtable();
            p_targetTools = new Hashtable();
            p_tagNames = new Hashtable();

            programCallNames = new ArrayList(1);
            commentLines = new ArrayList(2);
            inLineComment = new String("");
            inputNameNum = new ArrayList(1);
            outputNameNum = new ArrayList(1);
            
            kku = new util();
             
            m_xmlDoc = super.getDOMDocument();

            int ii;
            p_toolProfileNames = new String [toolProfileNames.length];
            for (ii=0; ii<toolProfileNames.length; ii++)
            {
                p_toolProfileNames[ii] = toolProfileNames[ii];
            }

            m_commentWaitSignal = true;

            Hashtable m_parameterData = super.getParameterData();
            Enumeration parameterKeys = m_parameterData.keys();
            Enumeration parameterValues = m_parameterData.elements();
            String pName, pValue;
            while (parameterKeys.hasMoreElements() == true && parameterValues.hasMoreElements() == true)
            {
                pName = parameterKeys.nextElement().toString();
                pValue = parameterValues.nextElement().toString();
                try
                {
                    if(pName.equalsIgnoreCase("ProgramFileEncoding"))
                    {
                        prgFileEncoding = pValue;
                    }
                    else if(pName.equalsIgnoreCase("OLPStyle"))
                    {
                        uploadStyle = pValue;
                    }
                    else if(pName.equalsIgnoreCase("OrientMode"))
                    {
                        orientMode = pValue;
                    }
                    else if (pName.equalsIgnoreCase("CommentWaitSignal"))
                    {
                        if (pValue.equalsIgnoreCase("false") == true)
                            m_commentWaitSignal = false;
                    }
                    else if (pName.equalsIgnoreCase("IOName"))
                    {
                        if (pValue.endsWith("[]"))
                            ioName = pValue.substring(0, pValue.lastIndexOf(']'));
                        else
                            ioName = pValue;
                    }
                }
                finally {
                    continue;
                }
            }

            for (ii = 0; ii < m_keywords.length; ii++)
            {
                m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
            }
            
            String sflt = "((?:\\+|\\-)?(?:\\d*\\.?\\d+|\\d+\\.?\\d*))";
            // sections:    float, must have at least 1 digit;       scientific, optional
            String sSci = "((?:\\+|\\-)?(?:\\d*\\.?\\d+|\\d+\\.?\\d*)(?:[Ee][\\+\\-]?\\d+)?)";
            String sFlt = sflt + "?"; // optional
            String sSci_optional = "(?:\\s*,\\s*" + sSci + ")?";
            if (m_numRobotAxes == 4)
            {
                //       J1     [, J2]          [, J3]          [, J4]
                jConst = sSci + sSci_optional + sSci_optional + sSci_optional;
                //       X      [, Y]           [, Z]           [, R]
                pConst = jConst + "(?:\\s*,\\s*(\\-?\\d+))?";
                //                 [      ,     FIG      ]
            }
            else
            {
                //       J1     [, J2]          [, J3]          [, J4]          [, J5]          [, J6]
                jConst = sSci + sSci_optional + sSci_optional + sSci_optional + sSci_optional + sSci_optional;
                //       X      [, Y]           [, Z]           [, W]           [, P]           [, R]
                pConst = jConst + "(?:\\s*,\\s*(\\-?\\d+))?";
                //                 [      ,     FIG      ]
            }
            //                        Joint  ,        Value
            String jAux = "\\s*\\(\\s*(\\d)\\s*,\\s*" + sflt + "\\s*\\)\\s*";
            String ext2 = "\\s+EXA\\s*\\(" + jAux + "\\s*,\\s*" + jAux + "\\)";
            String ext1 = "\\s+EXA\\s*\\(" + jAux + "\\)";
            
            String moOption = "(?:\\s*,\\s*(S|SPEED|ACCEL|DECEL)\\s*=\\s*" + sflt + ")?";
            String nextOption = "(?:\\s*,\\s*(NEXT))?";
            String rounding = "\\s*,?\\s*(\\@(?:\\d+|P|E)\\s+)?";
            String pVar = "\\s*,?\\s*([PJ]\\[?\\d+\\]?|\\w+|\\(" + pConst + "\\))?";
            String inlineComm = "\\s*(?:(?:REM|')(.*))?";
            
            m_keywords[0] = Pattern.compile("^\\s*PROGRAM\\s+\\w+" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[10] = Pattern.compile("^\\s*SPEED\\s+" + sflt + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[20] = Pattern.compile("^\\s*ACCEL\\s+" + sflt + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[22] = Pattern.compile("^\\s*DECEL\\s+" + sflt + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[30] = Pattern.compile("^\\s*WORK\\s+(\\d+)" + pVar + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[32] = Pattern.compile("^\\s*CHANGEWORK\\s+(\\d+)" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[40] = Pattern.compile("^\\s*TOOL\\s+(\\d+)" + pVar + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[42] = Pattern.compile("^\\s*CHANGETOOL\\s+(\\d+)" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[50] = Pattern.compile("^\\s*MOVE\\s+(P|L|C)" + rounding + pVar + rounding + pVar + ext2 + moOption + moOption + moOption + nextOption + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[51] = Pattern.compile("^\\s*MOVE\\s+(P|L|C)" + rounding + pVar + rounding + pVar + ext1 + moOption + moOption + moOption + nextOption + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[52] = Pattern.compile("^\\s*MOVE\\s+(P|L|C)" + rounding + pVar + rounding + pVar + moOption + moOption + moOption + nextOption + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[54] = Pattern.compile("^\\s*APPROACH\\s+(P|L)" + pVar + rounding + sflt + moOption + moOption + moOption + nextOption + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[56] = Pattern.compile("^\\s*DEPART\\s+(P|L)" + rounding + sflt + moOption + moOption + moOption + nextOption + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[60] = Pattern.compile("^\\s*HOME\\s+" + pVar + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[62] = Pattern.compile("^\\s*GOHOME" + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[70] = Pattern.compile("^\\s*SET\\s+IO\\[(\\d+)\\](?:\\s*,\\s*(\\d+))?" + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[72] = Pattern.compile("^\\s*RESET\\s+IO\\[(\\d+)\\]" + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[80] = Pattern.compile("^\\s*WAIT\\s+IO\\[(\\d+)\\]\\s*=\\s*(ON|OFF)(?:\\s*,\\s*(\\d+))?(?:\\s*,\\s*(\\w+))?" + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[82] = Pattern.compile("^\\s*DELAY\\s+(\\d+)" + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[90] = Pattern.compile("^\\s*CALL\\s+(\\w+)" + inlineComm, Pattern.CASE_INSENSITIVE);
            m_keywords[200] = Pattern.compile("^\\s*\\s*TAKEARM(?:\\s+\\d+)?" + inlineComm, Pattern.CASE_INSENSITIVE);
            // Upload GIVEARM as RobotLanguage, so that it will be downloaded
            // m_keywords[202] = Pattern.compile("^GIVEARM", Pattern.CASE_INSENSITIVE);
            m_keywords[210] = Pattern.compile("^\\s*IOBLOCK\\s+(ON|OFF)" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[250] = Pattern.compile("^\\s*DEFPOS\\s+(\\w+)(?:\\s*=\\s*\\(\\s*" + pConst + "\\s*\\))?" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[251] = Pattern.compile("^\\s*(P\\d+)(?:\\s*=\\s*\\(\\s*" + pConst + "\\s*\\))" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[252] = Pattern.compile("^\\s*DEFJNT\\s+(\\w+)(?:\\s*=\\s*\\(\\s*" + jConst + "\\s*\\))?" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[290] = Pattern.compile("^\\s*(?:REM|')(.*)" + "\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[299] = Pattern.compile("^\\s*END" + "\\s*", Pattern.CASE_INSENSITIVE);
        }

    	public static void main(String [] parameters) throws SAXException, ParserConfigurationException, TransformerConfigurationException, NumberFormatException, StringIndexOutOfBoundsException
        {
            DensoUploader uploader = new DensoUploader(parameters);
            String [] additionalProgramNames = null;
            if (parameters.length > 12 && parameters[12].trim().length() > 0 )
                additionalProgramNames = parameters[12].split("[\\t]+");
            
            //Get the robot program and parse it line by line
            String robotProgramName = uploader.getPathToRobotProgramFile();
            File f = new File(robotProgramName);
            String path = f.getParent() + f.separator;
            String robotProgramNameOnly = f.getName();
            uploader.m_program = robotProgramNameOnly;

            String uploadInfoFileName = uploader.getPathToErrorLogFile();

            try
            {
                bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));
                bw.write(VERSION);
                bw.write(COPYRIGHT);
                bw.write("\nStart of java parsing.\n\n");

                uploader.getTargets(path);
                
                int errCode;
                errCode = uploader.processRobotProgram(robotProgramName);
                
                String sAddProg = null;
                int index;
                if (additionalProgramNames != null)
                {
                    for (int ii=0; ii<additionalProgramNames.length; ii++)
                    {
                        f = new File(additionalProgramNames[ii]);
                        sAddProg = f.getName();
                        index = uploader.programCallNames.indexOf(sAddProg);
                        if (index < 0)
                        {
                            uploader.programCallNames.add(uploader.programCallCount, sAddProg);
                            uploader.programCallNames.trimToSize();
                            uploader.programCallCount++;
                        }
                    }
                }
                if (errCode == 0)
                {
                    String calledProgName;
                    for (int ii=0; ii<uploader.programCallCount; ii++)
                    {
                        calledProgName = (String)uploader.programCallNames.get(ii);
                        if (calledProgName.equalsIgnoreCase(robotProgramNameOnly))
                            continue; // skip the "main" program already uploaded
                        
                        uploader.m_program = calledProgName;
                        robotProgramName = path + calledProgName;
                        errCode = uploader.processRobotProgram(robotProgramName);
                    }
                }
                if (errCode == 0)
                {
                    bw.write("All the program statements have been parsed.\n");
                    bw.write("\nJava parsing completed successfully.\n");
                }
                else
                {
                    bw.write("\nERROR-Robot program upload failed.\n");
                }
                bw.write("\nEnd of java parsing.\n");
                bw.flush();
                bw.close();
            }//end try
            catch (IOException e) {
                e.getMessage();
            }
            
            Element motionProfileList = uploader.getMotionProfileListElement();
            if (motionProfileList.hasChildNodes() == false)
                uploader.createSpeedProfile("%50", "Default");

            Element accuracyProfileList = uploader.getAccuracyProfileListElement();
            if (accuracyProfileList.hasChildNodes() == false)
                uploader.createAccuracyProfile(accuracyProfileList, "Default",
                    DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                    false, 0.0);
        
            try {
                uploader.writeXMLStreamToFile();
            }

            catch (IOException e) {
                e.getMessage();
            }
            catch (TransformerException e) {
                e.getMessage();
            }
        }//end main

        private void getTargets(String path)
        {
            Pattern [] pattern = new Pattern [2];
            // position type (Cartesian:   Index   ,        pConst
            pattern[0] = Pattern.compile("^\\d+\\s*,\\s*" + pConst, Pattern.CASE_INSENSITIVE);
            // joint    type (Joint:       Index   ,        jConst
            pattern[1] = Pattern.compile("^\\d+\\s*,\\s*" + jConst, Pattern.CASE_INSENSITIVE);

            BufferedReader varIn;
            Matcher match;
            String [] components;
            String targetNumber;
            String line;
            File file;
            String [] varTypes = {"P", "J"};
            String [] varFiles = {"", ""};
            for (int ii=0; ii<2; ii++)
            {
                varFiles[ii] = path + "Var_" + varTypes[ii] + ".csv";
                file = new File(varFiles[ii]);
                if (false == file.exists())
                    continue;
                
                try
                {
                    varIn = new BufferedReader(new FileReader(varFiles[ii]));
                    bw.write("Reading variable file \"");
                    bw.write(varFiles[ii]);
                    bw.write("\"\n\n");
                    
                    line = varIn.readLine();
                    while (line != null)
                    {
                        line = line.trim();
                        if (line.equals(""))
                        {
                            line = varIn.readLine();
                            continue;
                        }
                        match = pattern[ii].matcher(line);
                        if (match.find() == true)
                        {
                            components = line.split("[\\s,]", 2);
                            storePose(match, ii, varTypes[ii] + components[0]);
                        }
                        
                        line = varIn.readLine();
                    } // end while
                } // end try
                catch (IOException e)
                {
                    try {
                        bw.write("ERROR: "+e.getMessage()+"\n");
                    }
                    catch (IOException bwe) {
                    }
                }
            }
        } // getTargets
        
        private void storePose(Matcher match, int type, String name)
        {
            if (type == 0)
            {
                String [] pVal = {"0", "0", "0", "0", "0", "0", "-1"}; // FIG undef = -1
                int shift = 0;
                for (int ii=1; ii<match.groupCount()+1; ii++)
                {
                    if (match.group(ii) != null)
                    {
                        if (m_numRobotAxes == 4 && ii > 3) // no Yaw or Pitch
                            shift = 2;

                        pVal[ii-1+shift] = match.group(ii);
                    }
                }
                positionVariableTbl.put(name, pVal);
            }
            else if (type == 1) // Joint type
            {
                String [] jVal = {"0", "0", "0", "0", "0", "0"};
                for (int ii=1; ii<match.groupCount()+1; ii++)
                {
                    if (match.group(ii) != null)
                        jVal[ii-1] = match.group(ii);
                }
                jointVariableTbl.put(name, jVal);
            }
        } // storePose
        
        private int getPose(String name, int type, double [] poseValues)
        {
            int ii;
            String [] poseTypes = {"Position", "Joint"};
            String [] sValues; // DO NOT MODIFY. IT CHANGES VALUES IN THE TABLES
            if (type == 0)
                sValues = (String []) positionVariableTbl.get(name);
            else
                sValues = (String []) jointVariableTbl.get(name);
            
            if (sValues == null)
            {
                try {
                    bw.write("Warning: " + poseTypes[type] + " variable " + name + " is not defined.\n\n");
                }
                catch (IOException bwe) {
                }

                return 1;
            }
            int len = sValues.length;
            String [] tValues = new String[len]; // MODIFY tValues
            for (ii=0; ii<len; ii++)
                tValues[ii] = sValues[ii];

            if (type == 0)
            {
                String tmpStr;
                DNBIgpOlpUploadMatrixUtils mat = new DNBIgpOlpUploadMatrixUtils();
                
                if (m_ApproachDist != null)
                {
                    tmpStr = tValues[0];
                    for (ii=1; ii<6; ii++)
                        tmpStr += "," + tValues[ii];
                    mat.dgXyzyprToMatrix(tmpStr);
                    
                    if (m_numRobotAxes == 4)
                        tmpStr = "0,0,"  + m_ApproachDist + ",0,0,0"; // +Z direction
                    else
                        tmpStr = "0,0,-" + m_ApproachDist + ",0,0,0"; // -Z direction
                    mat.dgCatXyzyprMatrix(tmpStr);

                    tValues[0] = mat.dgGetX();
                    tValues[1] = mat.dgGetY();
                    tValues[2] = mat.dgGetZ();
                    tValues[3] = mat.dgGetYaw();
                    tValues[4] = mat.dgGetPitch();
                    tValues[5] = mat.dgGetRoll();
                }
                
                String [] objFrame;
                /* Cartesian target "POSITIONCONSTANT" is applied object frame during simulation,
                 * no transform here
                 */
                if (false == m_objectProfileName.equals("Default") && false == name.startsWith("POSITIONCONSTANT"))
                {
                    setObjectFrameProfileApplyOffset();
                    
                    objFrame = (String [])positionVariableTbl.get(m_objectProfileName);
                    if (objFrame == null)
                    {
                        try {
                            bw.write("Error: Work(Object) frame " + m_objectProfileName + " is not defined.\n");
                            bw.write("Error: Position " + name + " may not be correct.\n\n");
                        }
                        catch (IOException bwe) {
                        }
                    }
                    else
                    {
                        tmpStr = objFrame[0];
                        for (ii=1; ii<6; ii++)
                            tmpStr += "," + objFrame[ii];
                        mat.dgXyzyprToMatrix(tmpStr);
                        mat.dgInvert();

                        tmpStr = tValues[0];
                        for (ii=1; ii<6; ii++)
                            tmpStr += "," + tValues[ii];
                        mat.dgCatXyzyprMatrix(tmpStr);

                        tValues[0] = mat.dgGetX();
                        tValues[1] = mat.dgGetY();
                        tValues[2] = mat.dgGetZ();
                        tValues[3] = mat.dgGetYaw();
                        tValues[4] = mat.dgGetPitch();
                        tValues[5] = mat.dgGetRoll();
                    }
                }
                
                for (ii=0; ii<7; ii++)
                {
                    poseValues[ii] = Double.valueOf(tValues[ii]).doubleValue();
                    if (ii<3)
                        poseValues[ii] *= 0.001;
                }
            }
            else // Joint type target
            {
                for (ii=0; ii<6; ii++)
                    poseValues[ii] = Double.valueOf(tValues[ii]).doubleValue();
                // APPROACH command with Joint type target for 4-DOF Scara robot
                if (m_ApproachDist != null && m_numRobotAxes == 4)
                {
                    poseValues[2] += Double.parseDouble(m_ApproachDist);
                }
                else
                {
                }
            }
            
            /* set prevTarget for DEPART command
             * prevTarget[0] ~ prevTarget[5]: (X Y Z W P R) or (J1 ~ J6)
             * prevTarget[6]: Figure for Position type target
             * prevTarget[7] = type; 0 for Position and 1 for Joint
             * use prevTarget instead of getting the last target from name stored,
             * since prevTarget has APPROACH distance taken into account
             * this is correct if DEPART follows APPROACH.
             */
            for (ii=0; ii<6; ii++)
            {
                prevTarget[ii] = poseValues[ii];
                if (type ==0 && ii<3)
                    prevTarget[ii] *= 1000;
            }
            if (type == 0)
                prevTarget[6] = poseValues[6];
            prevTarget[7] = type;
            
            return 0;
        } // getPose
        
        private int processRobotProgram(String robotProgramName)
        {
            BufferedReader rbt;
            int errCode;

            Element resourceElem = super.getResourceElement();
            File f = new File(robotProgramName);
            String taskName = f.getName();
            taskName = taskName.substring(0, taskName.lastIndexOf('.'));
            Element activityListElem = super.createActivityList(taskName);
            /* create AttributeList attached to the ActivityList right away
             * because after the AttributeList attached to the Activity is created
             */
            super.createAttributeList(activityListElem, null, null);
            resourceElem.appendChild(activityListElem);
            
            try
            {
                rbt = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));

                bw.write("Translating program file \"" + robotProgramName + "\"\n");

                errCode = processRobotProgramLines(rbt, activityListElem);
                if (errCode != 0)
                    return errCode;
            } // end try

            catch (IOException e) {
                try {
                    bw.write("ERROR: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            return 0;
        } // end processRobotProgram

        private int processRobotProgramLines(BufferedReader rbt, Element activityListElem)
        {
            String line;
            Matcher match;

            try
            {
                line = rbt.readLine();
                while (line != null)
                {
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                        continue;
                    }
                    for (int ii = 0; ii < m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);
                        if (match.matches() == true) // matches entire line
                        {
                            //Call appropriate methods to handle the input
                            switch (ii)
                            {
                                case 0: // program name should be the same as file name
                                    break;
                                case 10: // SPEED value
                                    // percentage speed; store value, get/create MotionProfile before next motion command
                                    m_Spd = Float.parseFloat(match.group(1));
                                    /* accel is changed only with speed command,
                                     * not with the option in move commands.
                                     */
                                    m_Accel = m_Spd * m_Spd / 100;
                                    m_Decel = m_Accel;
                                    break;
                                case 20: // ACCEL value
                                    // percentage acceleration; store value, get/create MotionProfile before next motion command
                                    m_Accel = Float.parseFloat(match.group(1));
                                    break;
                                case 22: // DECEL value
                                    // percentage deceleration; store value, create motion activity attribute
                                    m_Decel = Float.parseFloat(match.group(1));
                                    break;
                                case 30: processDefineWork(match);
                                    break;
                                case 32: processChangeWork(match);
                                    break;
                                case 40: processDefineTool(match);
                                    break;
                                case 42: processChangeTool(match);
                                    break;
                                case 50:
                                case 51:
                                case 52:
                                    if (0 != processMove(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 54:
                                    processApproach(match, activityListElem);
                                    m_ApproachDist = null;
                                    break;
                                case 56:
                                    processDepart(match, activityListElem);
                                    m_DepartDist = null;
                                    break;
                                case 60: DefineHome(match);
                                    break;
                                case 62: 
                                    processGoHome(match, activityListElem);
                                    break;
                                case 70: // SET IO[1], Duration
                                    processSetIO(match, activityListElem, false);
                                    break;
                                case 72: // RESET IO[1]
                                    processSetIO(match, activityListElem, true);
                                    break;
                                case 80: processWait(line, match, activityListElem);
                                    break;
                                case 82: processDelay(match, activityListElem);
                                    break;
                                case 90: processCall(match, activityListElem);
                                    break;
                                case 200: // TAKEARM
                                    m_Accel = 100;
                                    m_Decel = 100;
                                    m_Spd = 100;
                                    m_toolProfileName = "Default";
                                    m_objectProfileName = "Default";
                                    processComment(line, activityListElem, "Robot Language");
                                    break;
                                //case 202: // GIVEARM
                                    //break;
                                case 210: // IOBLOCK ON|OFF     (Ignore, not upload as Robot Language)
                                    break;
                                case 250: // DEFPOS ident[=(...)]
                                case 251: // P2=(...)
                                    processDefPos(match);
                                    break;
                                case 252: // DEFJNT ident[=(...)]
                                    processDefJnt(match);
                                    break;
                                case 290: // comment REM|'
                                    processComment(match.group(1).trim(), activityListElem, "Comment");
                                    break;
                                case 299: // END
                                    break;
                            } // End switch
                            
                            break;
                        } // End if match found
                        else
                        {
                            if (ii == (m_keywords.length - 1))
                            {
                                processComment(line, activityListElem, "Robot Language");
                            }
                        } // End if match NOT found
                    } // End For
                    
                    line = rbt.readLine();
                    
                } // end while
                rbt.close();
            } // end try
            catch (IOException e) {
                e.getMessage();
                return 1;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-Robot must have an external axis to upload Servo Gun program.\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            
            addAttributeList(activityListElem, "Post");
            
            return 0;
        } // end of processRobotProgramLines

        // Create Tool Profile
        // TOOL 3, (10, 20, 30, 0, 0, 0) // X, Y, Z, Yaw, Pitch, Roll
        // TOOL 3, (10, 20, 30, 0)       // X, Y, Z, Roll
        private void processDefineTool(Matcher match)
        {
            String [] sValues = {"0", "0", "0", "0", "0", "0", "-1"};
            double [] dValues = new double[6];
            String toolNumber = match.group(1);
            String toolProfileName = "Tool." + toolNumber;
            m_toolProfileName = toolProfileName;
            
            int ii, NumberOfGroups = match.groupCount();
            if (match.group(3) == null) // position variable
            {
                String tTarget = match.group(2);
                sValues = (String [])positionVariableTbl.get(tTarget);
                if (sValues == null)
                {
                    try {
                        bw.write("Error: Position variable " + tTarget + " is not defined.\n\n");
                    }
                    catch (IOException bwe) {
                    }
                    return;
                }
            }
            else
            {
                int shift = 0;
                for (ii=3; ii<NumberOfGroups+1; ii++)
                {
                    if (match.group(ii) != null)
                    {
                        if (m_numRobotAxes == 4 && ii > 5)
                            shift = 2;

                        sValues[ii-3+shift] = match.group(ii);
                    }
                }
            }
            
            for (ii=0; ii<6; ii++)
            {
                dValues[ii] = Double.valueOf(sValues[ii]).doubleValue();
                if (ii < 3)
                    dValues[ii] /= 1000;
            }
            
            Element toolProfileList = super.getToolProfileListElement();
            DNBIgpOlpUploadEnumeratedTypes.ToolType toolType;
            toolType = DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT;
            super.createToolProfile(toolProfileList, toolProfileName, toolType, dValues, null, null, null);
        }

        // CHANGETOOL 3
        private void processChangeTool(Matcher match)
        {
            String toolNumber = match.group(1);
            if (toolNumber.equals("0"))
                m_toolProfileName = "Default";
            else
                m_toolProfileName = "Tool." + toolNumber;
        }

        // Create Object Frame Profile
        // WORK 3, (10, 20, 30, 0, 0, 0) // X, Y, Z, Yaw, Pitch, Roll
        // WORK 3, (10, 20, 30, 0)       // X, Y, Z, Roll
        // WORK 3, P3                    // position variable
        private void processDefineWork(Matcher match)
        {
            String [] sValues = {"0", "0", "0", "0", "0", "0", "-1"};
            double [] dValues = new double[6];
            String workNumber = match.group(1);
            String objProfileName = "Object." + workNumber;
            m_objectProfileName = objProfileName;

            int ii, NumberOfGroups = match.groupCount();
            if (match.group(3) == null) // position variable
            {
                String wTarget = match.group(2);
                sValues = (String [])positionVariableTbl.get(wTarget);
                if (sValues == null)
                {
                    try {
                        bw.write("Error: Position variable " + wTarget + " is not defined.\n\n");
                    }
                    catch (IOException bwe) {
                    }
                    return;
                }

                positionVariableTbl.put(objProfileName, sValues);
            }
            else
            {
                int shift = 0;
                for (ii=3; ii<NumberOfGroups+1; ii++)
                {
                    if (match.group(ii) != null)
                    {
                        if (m_numRobotAxes == 4 && ii > 5)
                            shift = 2;

                        sValues[ii-3+shift] = match.group(ii);
                    }
                }

                // store object frame for target transformation
                positionVariableTbl.put(objProfileName, sValues);
            }
            
            for (ii=0; ii<6; ii++)
            {
                dValues[ii] = Double.valueOf(sValues[ii]).doubleValue();
                if (ii < 3)
                    dValues[ii] /= 1000;
            }
            Element objProfileList = super.getObjectFrameProfileListElement();
            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType objType;
            objType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE;
            boolean applyOffsetToTags = false; // set to true when obj frame is used on a tag target
            super.createObjectFrameProfile(objProfileList, objProfileName, objType, dValues, applyOffsetToTags);
        } // processDefineWork

        // CHANGEWORK 3
        private void processChangeWork(Matcher match)
        {
            String workNumber = match.group(1);
            if (workNumber.equals("0"))
                m_objectProfileName = "Default";
            else
                m_objectProfileName = "Object." + workNumber;
        }

        private void setObjectFrameProfileApplyOffset()
        {
            Element objFrameProfileList = super.getObjectFrameProfileListElement();
            NodeList objFrameProfileNodes = objFrameProfileList.getChildNodes();
            Element objFrameProfileElem, objFrameProfileNameElem;
            for (int ii=0; ii<objFrameProfileNodes.getLength(); ii++)
            {
                objFrameProfileElem = (Element)objFrameProfileNodes.item(ii);
                // skip CurrentObjectFrameProfile
                if (objFrameProfileElem.getTagName().equals("ObjectFrameProfile"))
                {
                    objFrameProfileNameElem = kku.findChildElemByName(objFrameProfileElem, "Name", null, null);
                    if (objFrameProfileNameElem.getFirstChild().getNodeValue().equals(m_objectProfileName))
                    {
                        objFrameProfileElem.setAttribute("ApplyOffsetToTags", "On");
                        break;
                    }
                }
            }
        }
        
        // Create Robot Motion
        // MOVE P|L, @0 P2 EXA ((7, 0), (8, 0)), S=50, NEXT
        private int processMove(Matcher match, Element activityListElem)
        {
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype;
            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
            moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
            
            int numGroups = match.groupCount();
            inLineComment = match.group(numGroups);
            String sOptions = match.group(numGroups-1);
            if (numGroups > 29) // numGroups=31; 2 aux axes
            {
                if (auxValues == null || auxValues.length == 1)
                    auxValues = new double[2];
                auxValues[0] = Double.parseDouble(match.group(21));
                auxValues[1] = Double.parseDouble(match.group(23));
            }
            else
            {
                if (numGroups > 27)
                {
                    if (auxValues == null)
                        auxValues = new double[1];
                    auxValues[0] = Double.parseDouble(match.group(21));
                }
            }
            String sMoType = match.group(1);
            String sDensoAccuracy = match.group(2);
            String sTarget = match.group(3).toUpperCase();
            sTarget = sTarget.replaceAll("\\[", "");
            sTarget = sTarget.replaceAll("\\]", "");
                
            String [] sMoOption = new String[3];
            String [] sMoOptionValue = new String[3];
            if (numGroups > 4) // exclude "MOVE P, @0 HOME" (=GOHOME)
            {
                for (int ii=0; ii<3; ii++)
                {
                    sMoOption[ii] = match.group(numGroups-7+2*ii);
                    sMoOptionValue[ii] = match.group(numGroups-6+2*ii);
                    
                    if (sMoOption[ii] != null)
                    {
                        if (sMoOption[ii].equalsIgnoreCase("S") || sMoOption[ii].equalsIgnoreCase("SPEED"))
                        {
                            m_Spd = Float.parseFloat(sMoOptionValue[ii]);
                            m_Accel = m_Spd * m_Spd / 100;
                            m_Decel = m_Accel;
                        }
                        else
                        {
                            if (sMoOption[ii].equalsIgnoreCase("ACCEL"))
                                m_Accel = Float.parseFloat(sMoOptionValue[ii]);
                            else
                                if (sMoOption[ii].equalsIgnoreCase("DECEL"))
                                    m_Decel = Float.parseFloat(sMoOptionValue[ii]);
                        }
                    }
                }
            }
            m_motionProfileName = getOrCreateMotionProfile(moBasis, m_Spd, m_Accel);
            
            if (sMoType.equalsIgnoreCase("C"))
            {
                sDensoAccuracy = match.group(11);
                if (sDensoAccuracy == null)
                    sDensoAccuracy = "@0";
                m_accuracyProfileName = getOrCreateAccuracyProfile(sDensoAccuracy);

                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
                // add pre-comment to circular-via, won't break "MOVE C" when output
                if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, true) != 0)
                    return 1;

                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
                sTarget = match.group(12).toUpperCase();
                sTarget = sTarget.replaceAll("\\[", "");
                sTarget = sTarget.replaceAll("\\]", "");
                if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, false) != 0)
                    return 1;
            }
            else
            {
                if (sDensoAccuracy == null)
                    sDensoAccuracy = "@0";
                m_accuracyProfileName = getOrCreateAccuracyProfile(sDensoAccuracy);

                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
                if (sMoType.equalsIgnoreCase("L"))
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                
                if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, true) != 0)
                    return 1;
            }
            
            return 0;
        } // processMove
        
        // APPROACH P|L, P2, @0 200, S=50, NEXT
        // APPROACH P|L, (...), @0 200, S=500, NEXT
        private int processApproach(Matcher match, Element activityListElem)
        {
            /* no external axis for Approach command
             * previous auxValues if exists will be used to create Target
             */
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype;
            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
            moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
            
            int numGroups = match.groupCount();
            inLineComment = match.group(numGroups);
            String sOptions = match.group(numGroups-1);
            
            String sMoType = match.group(1);
            String sTarget = match.group(2).toUpperCase();
            sTarget = sTarget.replaceAll("\\[", "");
            sTarget = sTarget.replaceAll("\\]", "");
            // in case target is constant(explicit), get from the end
            String sDensoAccuracy = match.group(numGroups-9);
            m_ApproachDist = match.group(numGroups-8);
            
            String [] sMoOption = new String[3];
            String [] sMoOptionValue = new String[3];
            for (int ii=0; ii<3; ii++)
            {
                sMoOption[ii] = match.group(numGroups-7+2*ii);
                sMoOptionValue[ii] = match.group(numGroups-6+2*ii);
                
                if (sMoOption[ii] != null)
                {
                    if (sMoOption[ii].equalsIgnoreCase("S") || sMoOption[ii].equalsIgnoreCase("SPEED"))
                    {
                        m_Spd = Float.parseFloat(sMoOptionValue[ii]);
                        m_Accel = m_Spd * m_Spd / 100;
                        m_Decel = m_Accel;
                    }
                    else
                        if (sMoOption[ii].equalsIgnoreCase("ACCEL"))
                            m_Accel = Float.parseFloat(sMoOptionValue[ii]);
                        else
                            if (sMoOption[ii].equalsIgnoreCase("DECEL"))
                                m_Decel = Float.parseFloat(sMoOptionValue[ii]);
                }
            }
            m_motionProfileName = getOrCreateMotionProfile(moBasis, m_Spd, m_Accel);
            
            if (sDensoAccuracy == null)
                sDensoAccuracy = "@0";
            m_accuracyProfileName = getOrCreateAccuracyProfile(sDensoAccuracy);

            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            if (sMoType.equalsIgnoreCase("L"))
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;

            if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, true) != 0)
                return 1;
            
            return 0;
        } // processApproach
        
        // DEPART P|L, @0 200, S=50, NEXT
        private int processDepart(Matcher match, Element activityListElem)
        {
            /* no external axis for Depart command
             * previous auxValues if exists will be used to create Target
             */
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype;
            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
            moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
            
            int numGroups = match.groupCount();
            inLineComment = match.group(numGroups);
            String sOptions = match.group(numGroups-1);
            
            String sMoType = match.group(1);
            String sTarget = sPrevTargetName + "_DEPART";
            String sDensoAccuracy = match.group(2);
            m_DepartDist = match.group(3);
            
            String [] sMoOption = new String[3];
            String [] sMoOptionValue = new String[3];
            for (int ii=0; ii<3; ii++)
            {
                sMoOption[ii] = match.group(numGroups-7+2*ii);
                sMoOptionValue[ii] = match.group(numGroups-6+2*ii);
                
                if (sMoOption[ii] != null)
                {
                    if (sMoOption[ii].equalsIgnoreCase("S") || sMoOption[ii].equalsIgnoreCase("SPEED"))
                    {
                        m_Spd = Float.parseFloat(sMoOptionValue[ii]);
                        m_Accel = m_Spd * m_Spd / 100;
                        m_Decel = m_Accel;
                    }
                    else
                        if (sMoOption[ii].equalsIgnoreCase("ACCEL"))
                            m_Accel = Float.parseFloat(sMoOptionValue[ii]);
                        else
                            if (sMoOption[ii].equalsIgnoreCase("DECEL"))
                                m_Decel = Float.parseFloat(sMoOptionValue[ii]);
                }
            }
            m_motionProfileName = getOrCreateMotionProfile(moBasis, m_Spd, m_Accel);
            
            if (sDensoAccuracy == null)
                sDensoAccuracy = "@0";
            m_accuracyProfileName = getOrCreateAccuracyProfile(sDensoAccuracy);
            
            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            if (sMoType.equalsIgnoreCase("L"))
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
            
            if (createRobotMotion(activityListElem, eMotype, sTarget, sOptions, true) != 0)
                return 1;
            
            return 0;
        } // processDepart

        private int createRobotMotion(
                        Element activityListElem,
                        DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype,
                        String sTarget,
                        String sOptions,
                        boolean bAddAttributeList)
        {
            DNBIgpOlpUploadEnumeratedTypes.OrientMode eOrientType;
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS;
            if (orientMode.equalsIgnoreCase("1_axis") == true)
                eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS;
            if (orientMode.equalsIgnoreCase("3_axis") == true)
                eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.THREE_AXIS;
            if (orientMode.equalsIgnoreCase("wrist") == true)
                eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.WRIST;

            String sActivityName = "RobotMotion." + m_moveCounter;
            Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);
            Element moAttrElem = super.createMotionAttributes(actElem, m_motionProfileName, m_accuracyProfileName, m_toolProfileName, m_objectProfileName, eMotype, eOrientType);

            sTarget = sTarget.toUpperCase();
            String tagName;
            String varType = (String) variableTypeTbl.get(sTarget);
            if (sTarget.startsWith("P") || sTarget.startsWith("J") || 
                sTarget.equals("HOME") || sTarget.endsWith("_DEPART") ||
                (varType != null && (varType.equals("Position") || varType.equals("Joint"))))
            {
                tagName = sTarget;
            }
            else
            {
                tagName = "POSITIONCONSTANT";
                Pattern p = Pattern.compile("^\\(" + pConst + "\\)", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(sTarget);
                if (m.find() == true)
                    storePose(m, 0, tagName);
            }
            
            sPrevTargetName = tagName;
            if (createTarget(tagName, actElem) != 0)
            {
                activityListElem.removeChild(actElem);
                return 1;
            }
            m_moveCounter++;
            
            int departIndex = sTarget.indexOf("_DEPART");
            if (sTarget.startsWith("P") || sTarget.startsWith("J"))
            {
                if (departIndex == -1)
                    addAttribute(actElem, "TargetNum", sTarget.substring(1));
                else
                    addAttribute(actElem, "TargetNum", sTarget.substring(1, departIndex));
            }
            
            if (m_ApproachDist != null)
                addAttribute(actElem, "DensoApproach", m_ApproachDist);
            else
                if (m_DepartDist != null)
                    addAttribute(actElem, "DensoDepart", "");

            addAttribute(actElem, "DECEL", String.valueOf(m_Decel));
            if (sOptions != null && sOptions.length() > 0)
                addAttribute(actElem, "DensoOption", sOptions);
            
            if (inLineComment != null && inLineComment.length() > 0)
            {
                addAttribute(actElem, "InlineComment", inLineComment);
                inLineComment = null;
            }
            
            /* create attribute "DENSO TURNS" and set to anything other than "-100"
             * so that infra-structure will calculate turns
             */
            addAttribute(actElem, "DENSO TURNS", "DensoUploader");

            if (bAddAttributeList)
                addAttributeList(actElem, "Pre");

            return 0;
        } // createRobotMotion

        /* return motion profile name if motion basis, speed and accel are the same;
         * otherwise create a motion profile.
           <MotionProfile>
              <Name>Default</Name>
              <MotionBasis>Percent|Absolute</MotionBasis>
              <Speed Units="%"|"m/s" Value="50" />
              <Accel Units="%" Value="100" />
              <AngularSpeedValue Units="%" Value="100" />
              <AngularAccelValue Units="%" Value="100" />
           </MotionProfile>
        */
        private String getOrCreateMotionProfile(DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis, float fSpd, float fAccel)
        {
            String profileName;
            Element motionProfileList = super.getMotionProfileListElement();
            NodeList moProfiles = motionProfileList.getChildNodes();
            Element profile, name, basis, speed, accel;
            float speedVal, accelVal;
            String basisVal;
            DNBIgpOlpUploadEnumeratedTypes.MotionBasis enumBasisVal;
            for (int ii=0; ii<moProfiles.getLength(); ii++)
            {
                profile = (Element)moProfiles.item(ii);
                basis = kku.findChildElemByName(profile, "MotionBasis", null, null);
                speed = kku.findChildElemByName(profile, "Speed", null, null);
                accel = kku.findChildElemByName(profile, "Accel", null, null);
                basisVal = basis.getFirstChild().getNodeValue();
                if (basisVal.equals("Percent"))
                    enumBasisVal = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
                else
                    enumBasisVal = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
                speedVal = Float.parseFloat(speed.getAttribute("Value"));
                accelVal = Float.parseFloat(accel.getAttribute("Value"));
                if (enumBasisVal == moBasis && Math.abs(speedVal-fSpd)<0.001 && Math.abs(accelVal-fAccel)<0.001)
                {
                    name = kku.findChildElemByName(profile, "Name", null, null);
                    profileName = name.getFirstChild().getNodeValue();
                    return profileName;
                }
            }

            if (moBasis == DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT)
                profileName = "JMotionProfile." + m_motionProfileCounter;
            else
                profileName = "LMotionProfile." + m_motionProfileCounter;
            Element motionProfileElem = super.createMotionProfile(motionProfileList, profileName, moBasis, fSpd, fAccel, 100.0, 100.0);
            m_motionProfileCounter++;
            
            return profileName;
        } // end of getOrCreateMotionProfile

        private String getOrCreateAccuracyProfile(String sDensoAccuracy)
        {
            sDensoAccuracy = sDensoAccuracy.trim().toUpperCase();
            float accuracy;
            // set accuracy type, accuracy value, and flyby mode
            double accuracyValue = 0.0;
            boolean flyByMode = true;
            DNBIgpOlpUploadEnumeratedTypes.AccuracyType accuracyType;
            accuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE;
            if (sDensoAccuracy.equals("@P"))
            {
                accuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED;
                accuracyValue = 100.0; // percentage
            }
            else
            {
                if (sDensoAccuracy.equals("@E"))
                {
                    flyByMode = false;
                }
                else
                {
                    accuracy = Float.parseFloat(sDensoAccuracy.substring(1));
                    accuracyValue = accuracy / 1000.0; // unit Meter
                }
            }
            
            String profileName;
            Element accuracyProfileList = super.getAccuracyProfileListElement();
            // check if accuracy profile of the type, value, and flyby mode exists
            NodeList accuracyProfiles = accuracyProfileList.getChildNodes();
            Element profile, name, accuracyTypeElem, accuracyValueElem, flyByModeElem;
            boolean profileFlyByMode = true;
            float profileAccuracyValue;
            DNBIgpOlpUploadEnumeratedTypes.AccuracyType profileAccuracyType;
            profileAccuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE;
            for (int ii=0; ii<accuracyProfiles.getLength(); ii++)
            {
                profile = (Element)accuracyProfiles.item(ii);
                flyByModeElem = kku.findChildElemByName(profile, "FlyByMode", null, null);
                if (flyByModeElem.getFirstChild().getNodeValue().equals("Off"))
                    profileFlyByMode = false;
                else
                    profileFlyByMode = true;

                accuracyTypeElem = kku.findChildElemByName(profile, "AccuracyType", null, null);
                if (accuracyTypeElem.getFirstChild().getNodeValue().equals("Speed"))
                    profileAccuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED;
                else
                    profileAccuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE;

                accuracyValueElem = kku.findChildElemByName(profile, "AccuracyValue", null, null);
                profileAccuracyValue = Float.parseFloat(accuracyValueElem.getAttribute("Value"));
                
                // check if flyby, accuracy type and value match required accuracy profile
                if (flyByMode == profileFlyByMode && accuracyType == profileAccuracyType && Math.abs(accuracyValue-profileAccuracyValue) < 0.0001)
                {
                    name = kku.findChildElemByName(profile, "Name", null, null);
                    profileName = name.getFirstChild().getNodeValue();
                    return profileName;
                }
            }
            
            if (accuracyType == DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED)
                profileName = "VAccuracy." + m_accuracyProfileCounter;
            else
                profileName = "DAccuracy." + m_accuracyProfileCounter;
            Element accuracyProfileElem = super.createAccuracyProfile(accuracyProfileList, profileName, accuracyType, flyByMode, accuracyValue);
            
            m_accuracyProfileCounter++;
            
            return profileName;
        } // end of getOrCreateAccuracyProfile

        private void DefineHome(Matcher match)
        {
            String [] pVal = {"0", "0", "0", "0", "0", "0", "-1"}; // FIG undef = -1
            int NumberOfGroups = match.groupCount();
            if (match.group(2) == null) // position variable
            {
                String sTarget = match.group(1).toUpperCase();
                sTarget = sTarget.replaceAll("\\[", "");
                sTarget = sTarget.replaceAll("\\]", "");
                String [] sValues = (String [])positionVariableTbl.get(sTarget);
                if (sValues == null)
                {
                    try {
                        bw.write("Error: Position variable " + sTarget + " is not defined.\n\n");
                    }
                    catch (IOException bwe) {
                    }
                    return;
                }

                positionVariableTbl.put("HOME", sValues);
            }
            else
            {
                int shift = 0;
                for (int ii=2; ii<NumberOfGroups+1; ii++)
                {
                    if (match.group(ii) != null)
                    {
                        if (m_numRobotAxes == 4 && ii > 4) // no Yaw or Pitch
                            shift = 2;

                        pVal[ii-2+shift] = match.group(ii);
                    }
                }

                positionVariableTbl.put("HOME", pVal);
            }
        }

        //DEFPOS lp1=(543.652, 0, 348.973, 0, -90, -180)
        //HOME lp1
        //GOHOME
        private int processGoHome(Matcher match, Element activityListElem)
        {
            String line = "MOVE P, @0 HOME";
            String inlineComm = "\\s*(?:(?:REM|')(.*))?";
            String sTmp = match.group(1);
            if (sTmp != null && sTmp.length() > 0)
                line += " '" + sTmp;
            Pattern p = Pattern.compile("MOVE\\s+(P),\\s*(@0)\\s+(HOME)" + inlineComm, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(line);
            if (m.find() == true)
            {
                if (0 != processMove(m, activityListElem))
                    return 1;
                
                return 0;
            }
            else
                return 1;
        } // processGoHome

        // DELAY 1000 <-- its unit is ms
        private void processDelay(Matcher match, Element activityListElem)
        {
            Double dStartTime = new Double(0.0);
            String sDelayTime = match.group(1);
            Double dEndTime = new Double(Double.valueOf(sDelayTime).doubleValue() / 1000);
            String sActivityName = "RobotDelay." + m_delayCounter;
            
            Element actElem = super.createDelayActivity(activityListElem, sActivityName, dStartTime, dEndTime);
            
            int numGroups = match.groupCount();
            inLineComment = match.group(numGroups);
            if (inLineComment != null && inLineComment.length() > 0)
            {
                addAttribute(actElem, "InlineComment", inLineComment);
                inLineComment = null;
            }
            
            addAttributeList(actElem, "Pre");
            m_delayCounter++;
        } // end of processDelay

        // SET IO[2], 1000 <-- its unit is ms
        // RESET IO[2]
        private void processSetIO(Matcher match, Element activityListElem, boolean bReset)
        {
            boolean bSignalValue;
            String sPortNumber = match.group(1);
            String sSignalName = ioName + sPortNumber;
            if (ioName.endsWith("["))
                sSignalName += "]";
            Integer iPortNumber = Integer.valueOf(sPortNumber);
            String actName;
            if (bReset == false)
            {
                bSignalValue = true;
                actName = "SET IO[" + sPortNumber + "]";
            }
            else
            {
                bSignalValue = false;
                actName = "RESET IO[" + sPortNumber + "]";
            }
            
            Double dTime = new Double(0);
            if (match.groupCount() > 2)
            {
                String sDuration = match.group(2);
                if (sDuration != null)
                {
                    dTime = new Double(Double.valueOf(sDuration).doubleValue() / 1000);
                    actName += ", " + sDuration;
                }
            }
            Element actElem = super.createSetIOActivity(activityListElem, actName, bSignalValue, sSignalName, iPortNumber, dTime);
            
            int numGroups = match.groupCount();
            inLineComment = match.group(numGroups);
            if (inLineComment != null && inLineComment.length() > 0)
            {
                addAttribute(actElem, "InlineComment", inLineComment);
                inLineComment = null;
            }
            
            addAttributeList(actElem, "Pre");
        } // processSetIO

        // WAIT IO[1] = ON, 1000
        private void processWait(String line, Matcher match, Element activityListElem)
        {
            String sPortNumber = match.group(1);
            String status = match.group(2);
            boolean bSignalValue = false;
            if (status.equalsIgnoreCase("ON"))
                bSignalValue = true;
            String sSignalName = ioName + sPortNumber;
            if (ioName.endsWith("["))
                sSignalName += "]";
            Integer iPortNumber = Integer.valueOf(sPortNumber);
            String sTime = match.group(3);
            Double dTime = new Double(0);
            if (sTime != null)
                dTime = new Double(Double.parseDouble(sTime) / 1000);

            if (m_commentWaitSignal == false)
            {
                Element actElem = super.createWaitForIOActivity(activityListElem, line, bSignalValue, sSignalName, iPortNumber, dTime);
                
                String sOptions = match.group(4);
                if (sOptions != null && sOptions.length() > 0)
                {
                    sOptions = sOptions.trim();
                    if (sOptions.startsWith(","))
                        sOptions = sOptions.substring(1);
                    sOptions = sOptions.trim();
                    
                    addAttribute(actElem, "StorageVariable", sOptions);
                }
                
                int numGroups = match.groupCount();
                inLineComment = match.group(numGroups);
                if (inLineComment != null && inLineComment.length() > 0)
                {
                    addAttribute(actElem, "InlineComment", inLineComment);
                    inLineComment = null;
                }

                addAttributeList(actElem, "Pre");
            }
            else
            {
                processComment(line, activityListElem, "Robot Language");
            }
        } // end of processWait

        //CALL PRO3
        private void processCall(Matcher match, Element activityListElem)
        {
            String taskNameStr = match.group(1).toUpperCase();
            String callNameStr = taskNameStr + ".PAC";
            
            String activityName = "RobotCall." + robotCallNumber++;
            Element actElem = super.createCallTaskActivity(activityListElem, activityName, taskNameStr);
            
            int index = programCallNames.indexOf(callNameStr);
            if (index < 0)
            {
                programCallNames.add(programCallCount, callNameStr);
                programCallCount++;
            }
            
            int numGroups = match.groupCount();
            inLineComment = match.group(numGroups);
            if (inLineComment != null && inLineComment.length() > 0)
            {
                addAttribute(actElem, "InlineComment", inLineComment);
                inLineComment = null;
            }
            
            addAttributeList(actElem, "Pre");
        } // end of processCallJobStatement

        private void processComment(String line, Element activityListElem, String attributeName)
        {
            if (line.startsWith("!"))
            {
                // !TITLE, !AUTHOR, or !DATE
                String [] components = line.substring(1).split("[\\s+]", 2);
                components[1] = components[1].trim();
                if (components[1].startsWith("\""))
                    components[1] = components[1].substring(1);
                if (components[1].endsWith("\""))
                {
                    int len = components[1].length();
                    components[1] = components[1].substring(0, len-1);
                }
                addAttribute(activityListElem, components[0], components[1]);
            }
            else if (uploadStyle.equalsIgnoreCase("OperationComments"))
            {
                //Create DOM Nodes and set appropriate attributes
                String actName = "Comment.1";
                if (attributeName.equals("Comment"))
                    actName = attributeName + '.' + operationCommentCounter++;
                else
                    if (attributeName.equals("Robot Language"))
                        actName = attributeName + '.' + rbtLangCounter++;
                Element actElem = super.createActivityHeader(activityListElem, actName, "Operation");
                
                String [] attrNames = {attributeName};
                String [] attrValues = {line};
                Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
            }
            else
            {
                String comment = line;
                if (attributeName.equals("Robot Language"))
                    comment = "Robot Language:" + line;
                
                commentLines.add(commentCount, comment);
                commentCount++;
            }
        } // end of processComment

        private void addAttributeList(Element actElem, String prefix)
        {
            if (commentCount == 0)
                return;

            String [] attrNames = new String[commentCount];
            String [] attrValues = new String[commentCount];
            for (int ii=0; ii<commentCount; ii++)
            {
                attrNames[ii] = prefix + "Comment" + String.valueOf(ii+1);
                attrValues[ii] = (String)commentLines.get(ii);
            }

            super.createAttributeList(actElem, attrNames, attrValues);
            commentCount = 0;
        } // end addAttributeList

        private void addAttribute(Element actElem, String attrName, String attrVal)
        {
            String [] attrNames = {attrName};
            String [] attrValues = {attrVal};
            super.createAttributeList(actElem, attrNames, attrValues);
        } // end addAttribute

        private int createTarget(String sTarget, Element actElem)
        {
            int numOfDOF = m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes;
            int numOfAuxDOF = m_numAuxAxes+m_numExtAxes+m_numWorkAxes;
            /*
            if (auxValues != null)
            {
                if (numOfAuxDOF != auxValues.length && reportAuxAxisMismatch == true)
                {
                    reportAuxAxisMismatch = false;
                    try {
                        bw.write("WARNING: Robot has " + numOfAuxDOF + " auxiliary axes, but there are only " + auxValues.length + " available in the program.\n");
                    }
                    catch (IOException bwe) {
                    }
                }
            }
            else
            {
                if (numOfAuxDOF > 0 && reportAuxAxisMismatch == true)
                {
                    reportAuxAxisMismatch = false;
                    try {
                        bw.write("WARNING: Robot has " + numOfAuxDOF + " auxiliary axes, but there is no external axis in the program.\n");
                    }
                    catch (IOException bwe) {
                    }
                }
            }
            */
            int ii;
            sTarget = sTarget.toUpperCase();
            String varType = (String) variableTypeTbl.get(sTarget);
            
            if (sTarget.startsWith("P") || (varType != null && varType.equals("Position")) ||
                sTarget.equals("HOME") || (sTarget.endsWith("_DEPART") && prevTarget[7] == 0))
            {
                double [] cartFigValues = {0, 0, 0, 0, 0, 0, -1};
                if (sTarget.endsWith("_DEPART") == true && m_DepartDist != null)
                {
                    String tmpStr = String.valueOf(prevTarget[0]);
                    for (ii=1; ii<6; ii++)
                        tmpStr += "," + prevTarget[ii];

                    DNBIgpOlpUploadMatrixUtils mat = new DNBIgpOlpUploadMatrixUtils();
                    mat.dgXyzyprToMatrix(tmpStr);

                    if (m_numRobotAxes == 4)
                        tmpStr = "0,0," + m_DepartDist + ",0,0,0"; // +Z direction
                    else
                        tmpStr = "0,0,-" + m_DepartDist + ",0,0,0"; // -Z direction
                    mat.dgCatXyzyprMatrix(tmpStr);

                    // update prevTarget in case of consecutive DEPART commands
                    prevTarget[0] = Double.parseDouble(mat.dgGetX());
                    prevTarget[1] = Double.parseDouble(mat.dgGetY());
                    prevTarget[2] = Double.parseDouble(mat.dgGetZ());
                    prevTarget[3] = Double.parseDouble(mat.dgGetYaw());
                    prevTarget[4] = Double.parseDouble(mat.dgGetPitch());
                    prevTarget[5] = Double.parseDouble(mat.dgGetRoll());
                    
                    for (ii=0; ii<7; ii++)
                    {
                        cartFigValues[ii] = prevTarget[ii];
                        if (ii<3)
                            cartFigValues[ii] *= 0.001;
                    }
                }
                else
                {
                    if (getPose(sTarget, 0, cartFigValues) == 1)
                        return 1;
                }
                
                double [] cartValues = {0, 0, 0, 0, 0, 0};
                for (ii=0; ii<6; ii++)
                    cartValues[ii] = cartFigValues[ii];
                
                int figure = (int) cartFigValues[6];
                
                int [] turnNumbers = {0, 0, 0, 0}; // DELMIA turn number {J1, J4, J5, J6}
                if (m_numRobotAxes == 4)
                {
                    if (figure > 1)
                        turnNumbers[1] = 1; // OR -1
                }
                else
                {
                    if (m_numRobotAxes == 5)
                    {
                        if (figure > 7)
                            turnNumbers[3] = 1; // OR -1
                    }
                    else
                    {
                        if (figure > 23)
                        {
                            turnNumbers[1] = 1; // OR -1
                            turnNumbers[3] = 1; // OR -1
                        }
                        else
                        {
                            if (figure > 15)
                                turnNumbers[1] = 1; // OR -1
                            else
                                if (figure > 7)
                                    turnNumbers[3] = 1; // OR -1
                        }
                    }
                }
                
                figure %= 8;
                String cfgName = "Config_1";
                if (m_numRobotAxes == 4)
                {
                    switch (figure)
                    {
                        case 1: cfgName = "Config_1"; break;
                        case 0: cfgName = "Config_2"; break;
                    }
                }
                else
                {
                    if (m_numRobotAxes == 5)
                    {
                        switch (figure)
                        {
                            case 5: cfgName = "Config_1"; break;
                            case 7: cfgName = "Config_3"; break;
                            case 4: cfgName = "Config_5"; break;
                            case 6: cfgName = "Config_7"; break;
                        }
                    }
                    else
                    {
                        switch (figure)
                        {
                            case 5: cfgName = "Config_1"; break;
                            case 1: cfgName = "Config_2"; break;
                            case 7: cfgName = "Config_3"; break;
                            case 3: cfgName = "Config_4"; break;
                            case 0: cfgName = "Config_5"; break;
                            case 4: cfgName = "Config_6"; break;
                            case 2: cfgName = "Config_7"; break;
                            case 6: cfgName = "Config_8"; break;
                        }
                    }
                }
                
                Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, false);
                Element cartTargetElem = super.createCartesianTarget(targetElem, cartValues, cfgName);
                // only for 6-axis robot?
                super.createTurnNumbers(cartTargetElem, turnNumbers);
                
                if (sTarget.startsWith("POSITIONCONSTANT") == false)
                {
                    if (m_ApproachDist != null)
                        sTarget += "_APPROACH" + m_ApproachCounter++;

                    super.createTag(cartTargetElem, sTarget);
                }

                // if auxiliary axes exist, create elements JointTarget/AuxJoint
                if (auxValues != null && numOfAuxDOF > 0)
                {
                    // when there are aux axes in Denso programs, they are number 7 and 8
                    ArrayList [] oaJointTarget = new ArrayList [numOfAuxDOF];
                    for (ii=0; ii<numOfAuxDOF; ii++)
                        oaJointTarget[ii] = new ArrayList(1);
                    
                    /* Upload only the external axes available in PAC program file.
                     * (If robot does not have as many aux axes, the AuxJoint element created in xml is ignored.)
                     */
                    int jointNum = m_numAuxAxes < auxValues.length ? m_numAuxAxes : auxValues.length;
                    if (m_numAuxAxes > 0)
                        for (ii = 0; ii<jointNum; ii++)
                            setJointArrayList(auxValues[ii], "RailTrackGantry", ii+m_numRobotAxes, oaJointTarget[ii]);

                    jointNum = m_numAuxAxes+m_numExtAxes < auxValues.length ? m_numAuxAxes+m_numExtAxes : auxValues.length;
                    if (m_numExtAxes > 0)
                        for (ii=m_numAuxAxes; ii<jointNum; ii++)
                            setJointArrayList(auxValues[ii], "EndOfArmTooling", ii+m_numRobotAxes, oaJointTarget[ii]);

                    jointNum = numOfAuxDOF < auxValues.length ? numOfAuxDOF : auxValues.length;
                    if (m_numWorkAxes > 0)
                        for (ii=m_numAuxAxes+m_numExtAxes; ii<jointNum; ii++)
                            setJointArrayList(auxValues[ii], "WorkpiecePositioner", ii+m_numRobotAxes, oaJointTarget[ii]);
                    
                    super.createJointTarget(targetElem, oaJointTarget);
                }
            }
            
            if (sTarget.startsWith("J") || (varType != null && varType.equals("Joint")) ||
                (sTarget.endsWith("_DEPART") && prevTarget[7] == 1))
            {
                double [] jval = {0, 0, 0, 0, 0, 0};
                if (sTarget.endsWith("_DEPART") == true && m_DepartDist != null && m_numRobotAxes == 4)
                {
                    prevTarget[2] += Double.parseDouble(m_DepartDist);
                    
                    for (ii=0; ii<m_numRobotAxes; ii++)
                        jval[ii] = prevTarget[ii];
                }
                else
                {
                    if (getPose(sTarget, 1, jval) == 1)
                        return 1;
                }

                ArrayList [] oaJointTarget = new ArrayList[numOfDOF];
                for (ii=0; ii<numOfDOF; ii++)
                    oaJointTarget[ii] = new ArrayList(1);
                
                if (m_numRobotAxes == 5)
                {
                    for (ii=0; ii<3; ii++)
                        setJointArrayList(jval[ii], "Robot", ii, oaJointTarget[ii]);
                    
                    /* Denso 5-axis robot Joint Constants has a dummy J4 value
                     * assign Denso Joint 5 & 6 value to DELMIA Joint 4 & 5
                     */
                    for (ii=3; ii<5; ii++)
                        setJointArrayList(jval[ii+1], "Robot", ii, oaJointTarget[ii]);
                }
                else
                {
                    for (ii=0; ii<m_numRobotAxes; ii++)
                        setJointArrayList(jval[ii], "Robot", ii, oaJointTarget[ii]);
                }
                
                // if auxiliary axes exist, create elements JointTarget/AuxJoint
                if (auxValues != null && numOfAuxDOF > 0)
                {
                    /* when there are aux axes in Denso programs, they are number 7 and 8
                    
                    /* Upload only the external axes available in PAC program file.
                     * (If robot does not have as many aux axes, the AuxJoint element created in xml is ignored.)
                     */
                    int jj;
                    int jointNum = m_numAuxAxes < auxValues.length ? m_numAuxAxes : auxValues.length;
                    if (m_numAuxAxes > 0)
                        for (ii=0; ii<jointNum; ii++)
                        {
                            jj = ii + m_numRobotAxes;
                            setJointArrayList(auxValues[ii], "RailTrackGantry", jj, oaJointTarget[jj]);
                        }

                    jointNum = m_numAuxAxes+m_numExtAxes < auxValues.length ? m_numAuxAxes+m_numExtAxes : auxValues.length;
                    if (m_numExtAxes > 0)
                        for (ii=m_numAuxAxes; ii<jointNum; ii++)
                        {
                            jj = ii + m_numRobotAxes;
                            setJointArrayList(auxValues[ii], "EndOfArmTooling", jj, oaJointTarget[jj]);
                        }

                    jointNum = numOfAuxDOF < auxValues.length ? numOfAuxDOF : auxValues.length;
                    if (m_numWorkAxes > 0)
                        for (ii=m_numAuxAxes+m_numExtAxes; ii<jointNum; ii++)
                        {
                            jj = ii + m_numRobotAxes;
                            setJointArrayList(auxValues[ii], "WorkpiecePositioner", jj, oaJointTarget[jj]);
                        }
                }
                Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false);
                Element jointTargetElem = super.createJointTarget(targetElem, oaJointTarget);
            }
            
            return 0;
        }//end createTarget method
        
        public void setJointArrayList(double jVal, String robotOrAuxJointType, int iJoint, ArrayList jTarget)
        {
            DNBIgpOlpUploadEnumeratedTypes.DOFType jType;
            if (m_axisTypes[iJoint].equals("Rotational"))
            {
                jVal = Math.toRadians(jVal);
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL;
            }
            else
            {
                jVal = jVal * 0.001;
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL;
            }

            String jName = "Joint " + (iJoint + 1);
            jTarget.add(0, jName); // set joint name
            jTarget.add(1, new Double(jVal)); // set joint value
            jTarget.add(2, new Integer(iJoint + 1)); // set joint number
            jTarget.add(3, jType); // set joint type(rotational or translational)

            if (robotOrAuxJointType.equals("RailTrackGantry") ||
                robotOrAuxJointType.equals("EndOfArmTooling") ||
                robotOrAuxJointType.equals("WorkpiecePositioner"))
            {
                DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxType;
                auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                if (robotOrAuxJointType.equals("RailTrackGantry"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                else
                if (robotOrAuxJointType.equals("EndOfArmTooling"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING;
                else
                if (robotOrAuxJointType.equals("WorkpiecePositioner"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER;

                jTarget.add(4, auxType);
            }
        } // end of setJointArrayList

        public void createSpeedProfile(String speedString, String profileName)
        {
            //<MotionProfile>
                //<Name>Default</Name>
                //<MotionBasis>Percent|Absolute</MotionBasis>
                //<Speed Units="%"|"m/s" Value="50" />
                //<Accel Units="%" Value="100" />
                //<AngularSpeedValue Units="%" Value="100" />
                //<AngularAccelValue Units="%" Value="100" />
            //</MotionProfile>

            Element motionProfileList = super.getMotionProfileListElement();

            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
            double dSpd;
            if( speedString.startsWith("%") )
            {
                moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
                dSpd = Double.parseDouble(speedString.substring(1));
            }
            else
            {
                moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
                dSpd = Double.parseDouble(speedString);
            }

            Element motionProfileElem = super.createMotionProfile(motionProfileList, profileName, moBasis, dSpd, 100.0, 100.0, 100.0);
        } // end of createSpeedProfile

        private void processDefPos(Matcher match)
        {
            String name = match.group(1).toUpperCase();
            String [] sValues = {"0", "0", "0", "0", "0", "0", "-1"}; // FIG undef = -1
            int shift = 0;
            for (int ii=2; ii<match.groupCount()+1; ii++)
            {
                if (match.group(ii) != null)
                {
                    if (m_numRobotAxes == 4 && ii > 4) // no Yaw or Pitch
                        shift = 2;

                    sValues[ii-2+shift] = match.group(ii);
                }
            }
            
            positionVariableTbl.put(name, sValues);
            variableTypeTbl.put(name, "Position");
        }

        private void processDefJnt(Matcher match)
        {
            String name = match.group(1).toUpperCase();
            String [] sValues = {"0", "0", "0", "0", "0", "0"};
            for (int ii=2; ii<match.groupCount()+1; ii++)
            {
                if (match.group(ii) != null)
                    sValues[ii-2] = match.group(ii);
            }
            
            jointVariableTbl.put(name, sValues);
            variableTypeTbl.put(name, "Joint");
        }
}//end class
