//DOM and Parser classes
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;

import Kuka.util;

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class PanasonicUploader extends DNBIgpOlpUploadBaseClass {

        //Constants
        private static final int JOINT = 1, CARTESIAN = 2;
        private static final String VERSION = "Delmia Corp. Panasonic CSR Uploader Version 5 Release 21.\n";
        private static final String COPYRIGHT = "Copyright Delmia Corp. 2007-2011, All Rights Reserved.\n";

        //Member primitive variables
        private int m_targetType;
        private int m_numCStatements;
        private int m_numRobotAxes;
        private int m_numAuxAxes;
        private int m_numExtAxes;
        private int m_numWorkAxes;
        private int m_commentCounter;
        private int m_rbtLangCounter;
        private int m_delayCounter;
        private int m_moveCounter;
        private int m_homeCounter;
        private int m_spotWeldCounter;
        private int m_spotDryCounter;
        private int m_spotPickCounter;
        private int m_spotPlaceCounter;
        private int programCallCount;
        private int robotCallNumber;
        private int commentCount, headerCount;
        private int jobCount, arcStartCount, arcEndCount, arcSetCount, arcCraterCount;
        private int arcSetTigCount, arcCraterTigCount, arcSetTigFCCount, arcCraterTigFCCount;
        private int weaveProfileCount;
        private int numCircularMoves;
        private boolean EndOfArmToolingFirst;

        private int [] m_bcAxisMapping;
        private int [] m_ecAxisMapping;
        private double [] m_resolution;
        private double [] m_zeroOffset;
        private boolean m_commentWaitSignal;
        private String p_section = "";

        private String ioName = "DO[";
        private String weldHome;
        private String closeHome;
        private String openHome;

        //Member objects
        private Document m_xmlDoc;
        private String m_currentTagPoint;
        private int p_currentToolNumber;
        private int xrc_pl = -1;
        private String m_mountedTool;
        private ArrayList mountedTools;
        private Pattern [] m_keywords;
        private String [] arcON_OFF_Cond = {"FileName", "TableNumber"};
        private String [] arcSET_Cond    = {"Amperage", "Voltage", "Speed"};
        private String [] arcCRATER_Cond = {"Amperage", "Voltage", "Time"};
        private String [] arcSET_TIG_Cond    = {"BaseAmperage", "PeakAmperage", "FillerSpeed", "Frequency", "Speed"};
        private String [] arcCRATER_TIG_Cond = {"BaseAmperage", "PeakAmperage", "FillerSpeed", "Frequency", "Time"};
        private String [] arcSET_TIGFC_Cond    = {"BaseAmperage", "PeakAmperage", "BaseWireVelocity", "PeakWireVelocity", "PulseFrequency", "Speed"};
        private String [] arcCRATER_TIGFC_Cond = {"BaseAmperage", "PeakAmperage", "BaseWireVelocity", "PeakWireVelocity", "PulseFrequency", "Time"};
        private String [] weaveProfile = {"WeavePattern", "WeaveFrequency", "WeaveTimer", "WeaveDirection", "DirectionChange"};
        private String [] m_axisTypes;
        private String [] p_toolProfileNames;
        private ArrayList p_listOfRobotTargets;
        private ArrayList posCommented;
        private ArrayList posUsed;
        private int tgtNum;
        private ArrayList m_listOfAuxTargets;
        private ArrayList m_listOfExtTargets;
        private ArrayList m_listOfSpeedValues;
        private ArrayList p_listOfAccuracyValues;
        private ArrayList programCallNames;
        private ArrayList commentLines;
        private ArrayList inputNameNum, outputNameNum;
        private Hashtable m_targetTypes;
        private Hashtable m_targetTools;
        private Hashtable m_tagNames;
        private ArrayList p_listOfAuxTargets;
        private ArrayList p_listOfExtTargets;
        private Hashtable p_targetTypes;
        private Hashtable p_targetTools;
        private Hashtable p_tagNames;
        private String prgFileEncoding = "US-ASCII";
        private String uploadStyle = "General";
        private String speedArcSet = null;
        private util kku;
        private static BufferedWriter bw;

        //Constructor
        public PanasonicUploader(String [] parameters) throws ParserConfigurationException
        {
            super(parameters);
            m_targetType = 0;
            m_numCStatements = 0;
            m_commentCounter = 1;
            m_rbtLangCounter = 1;
            m_delayCounter = 1;
            m_moveCounter = 1;
            m_homeCounter = 1;
            m_spotWeldCounter = 1;
            m_spotDryCounter = 1;
            m_spotPickCounter = 1;
            m_spotPlaceCounter = 1;
            programCallCount = 0;
            robotCallNumber = 1;
            commentCount = 0;
            headerCount = 1;
            jobCount = 0;
            arcStartCount = 1;
            arcEndCount = 1;
            arcSetCount = 1;
            arcCraterCount = 1;
            arcSetTigCount = 1;
            arcCraterTigCount = 1;
            arcSetTigFCCount = 1;
            arcCraterTigFCCount = 1;
            weaveProfileCount = 1;

            m_mountedTool = parameters[6];
            mountedTools = new ArrayList(1);
            mountedTools.add(0, m_mountedTool);

            String [] toolProfileNames = parameters[7].split("[\\t]+");
            String [] homeNames = parameters[9].split("[\\t]+");

            //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1
            m_numRobotAxes = super.getNumberOfRobotAxes();
            m_numAuxAxes   = super.getNumberOfRailAuxiliaryAxes();
            m_numExtAxes   = super.getNumberOfToolAuxiliaryAxes();
            m_numWorkAxes  = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();

            m_bcAxisMapping = new int[m_numAuxAxes];
            m_ecAxisMapping = new int[m_numExtAxes + m_numWorkAxes];
            m_resolution = new double[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];
            m_zeroOffset = new double[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];

            m_currentTagPoint = new String("");
            p_currentToolNumber = 0;

            m_keywords = new Pattern [300];
            m_axisTypes = new String[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];
            m_axisTypes = super.getAxisTypes();
            p_listOfRobotTargets = new ArrayList(5);
            posCommented = new ArrayList(5);
            posUsed = new ArrayList(5);
            tgtNum = 0;
            m_listOfAuxTargets = new ArrayList(2);
            m_listOfExtTargets = new ArrayList(2);
            m_listOfSpeedValues = new ArrayList(5);
            p_listOfAccuracyValues = new ArrayList(3);
            m_targetTypes = new Hashtable();
            m_targetTools = new Hashtable();
            m_tagNames = new Hashtable();

            p_listOfAuxTargets = new ArrayList(2);
            p_listOfExtTargets = new ArrayList(2);
            p_targetTypes = new Hashtable();
            p_targetTools = new Hashtable();
            p_tagNames = new Hashtable();
            
            weldHome = new String("Home_1");
            closeHome = new String("Home_2");
            openHome = new String("Home_3");
            programCallNames = new ArrayList(1);
            commentLines = new ArrayList(2);
            inputNameNum = new ArrayList(1);
            outputNameNum = new ArrayList(1);
            
            kku = new util();
             
            m_xmlDoc = super.getDOMDocument();

            int ii;
            p_toolProfileNames = new String [toolProfileNames.length];
            for (ii=0; ii<toolProfileNames.length; ii++)
            {
                p_toolProfileNames[ii] = toolProfileNames[ii];
            }

            numCircularMoves = 0;
            EndOfArmToolingFirst = true;
            m_commentWaitSignal = true;

            for (ii = 0; ii < m_numAuxAxes; ii++)
                m_bcAxisMapping[ii] = ii + m_numRobotAxes + 1;

            for (ii = 0; ii < m_numExtAxes + m_numWorkAxes; ii++)
                m_ecAxisMapping[ii] = ii + m_numRobotAxes + m_numAuxAxes + 1;

            for (ii = 0; ii < m_resolution.length; ii++)
                m_resolution[ii] = Double.NaN;
             
            for (ii = 0; ii < m_resolution.length; ii++)
                m_zeroOffset[ii] = Double.NaN;

            if (homeNames.length > 0)
                weldHome = homeNames[0];
            if (homeNames.length > 1)
                closeHome = homeNames[1];
            if (homeNames.length > 2)
                openHome = homeNames[2];

            Hashtable m_parameterData = super.getParameterData();
            Enumeration parameterKeys = m_parameterData.keys();
            Enumeration parameterValues = m_parameterData.elements();
            int index, arrayIndex;
            String pName, pValue, testString;
            while (parameterKeys.hasMoreElements() == true && parameterValues.hasMoreElements() == true)
            {
                pName = parameterKeys.nextElement().toString();
                pValue = parameterValues.nextElement().toString();
                index = pName.indexOf('.');
                testString = pName.substring(0, index + 1);

                try
                {
                    if(pName.equalsIgnoreCase("ProgramFileEncoding"))
                    {
                        prgFileEncoding = pValue;
                    }
                    else if(pName.equalsIgnoreCase("OLPStyle"))
                    {
                        uploadStyle = pValue;
                    }
                    else if (pName.equalsIgnoreCase("CommentWaitSignal"))
                    {
                        if (pValue.equalsIgnoreCase("false") == true)
                            m_commentWaitSignal = false;
                    }
                    else if (pName.equalsIgnoreCase("IOName"))
                    {
                        if (pValue.endsWith("[]"))
                            ioName = pValue.substring(0, pValue.lastIndexOf(']'));
                        else
                            ioName = pValue;
                    }
                }

                finally {
                    continue;
                }
            }

            for (ii = 0; ii < 300; ii++)
            {
                m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
            }

            String sFlt  = ",\\s*(\\d*\\.?\\d+|\\d+\\.?\\d*)";
            String sFltQ = "(,\\s*(\\d*\\.?\\d+|\\d+\\.?\\d*))?";
            
            m_keywords[0] = Pattern.compile("^\\[Description\\]", Pattern.CASE_INSENSITIVE);
            m_keywords[1] = Pattern.compile("^\\[Pose\\]", Pattern.CASE_INSENSITIVE);
            m_keywords[2] = Pattern.compile("^\\[Variable\\]", Pattern.CASE_INSENSITIVE);
            m_keywords[3] = Pattern.compile("^\\[Command\\]", Pattern.CASE_INSENSITIVE);
            
            m_keywords[11] = Pattern.compile("^P\\d+,\\s*AJ,\\s*[-?\\d*\\.?\\d*,?\\s*]*", Pattern.CASE_INSENSITIVE);
            
            m_keywords[20] = Pattern.compile("^TOOL,\\s*\\d+:.", Pattern.CASE_INSENSITIVE);
            m_keywords[21] = Pattern.compile("^OUT,\\s*O(\\d+)#\\((\\d+):([.[^\\)]]*)\\),\\s*(ON|OFF|1|0)", Pattern.CASE_INSENSITIVE);
            m_keywords[22] = Pattern.compile("^DELAY" + sFlt, Pattern.CASE_INSENSITIVE);
            m_keywords[23] = Pattern.compile("^CALL,\\s*(\\w+\\.\\w+)", Pattern.CASE_INSENSITIVE);
            m_keywords[24] = Pattern.compile("^WAIT_IP,\\s*I(\\d+)#\\((\\d+):([.[^\\)]]*)\\),\\s*(ON|OFF|1|0)" + sFlt, Pattern.CASE_INSENSITIVE);
            //                             (MOVEP|MOVEL|MOVEC),                                  P1|GA#(1:label),     180.00,         m/min,          0, A|Air-cut|N|NoWeld|W|Weld           WeaveTimer
            m_keywords[30] = Pattern.compile("^(MOVE[PLC])(W)?(\\+)?,\\s*((P)(\\d+)|(G[AP]#)\\((\\d+):([.[^\\)]]*)\\))" + sFlt + ",\\s*([.[^,]]*),\\s*((\\d+),)?\\s*(\\w+)(,\\s*(\\d))?" + sFltQ + sFltQ + "(,\\s*(\\w+))?(,\\s*(\\w+))?", Pattern.CASE_INSENSITIVE);
            //       match groups              1          2   3          45  6      7          8      9                   10           11             113           14    15    16         17 18   19 20    21    22      23    24
            //                                                                                                                                        2                WeavePattern, WeaveFrequency,
            m_keywords[31] = Pattern.compile("^(GOHOME),\\s*(MOVE[PLC]),\\s*(P)(\\d+)" + sFlt + ",\\s*(%|m/min)", Pattern.CASE_INSENSITIVE);
            
            m_keywords[40] = Pattern.compile("^(ARC-ON|ARC-OFF),\\s*([\\w\\.]+),\\s*(\\d)", Pattern.CASE_INSENSITIVE);
            m_keywords[41] = Pattern.compile("^(ARC-SET),\\s*(\\d+)" + sFlt + sFlt, Pattern.CASE_INSENSITIVE);
            m_keywords[42] = Pattern.compile("^(CRATER),\\s*(\\d+)" + sFlt + sFlt, Pattern.CASE_INSENSITIVE);
            m_keywords[43] = Pattern.compile("^(ARC-SET_TIG),\\s*(\\d+),\\s*(\\d+)" +sFlt + sFlt + sFlt, Pattern.CASE_INSENSITIVE);
            m_keywords[44] = Pattern.compile("^(CRATER_TIG),\\s*(\\d+),\\s*(\\d+)" + sFlt + sFlt + sFlt, Pattern.CASE_INSENSITIVE);
            m_keywords[45] = Pattern.compile("^(ARC-SET_TIGFC),\\s*(\\d+),\\s*(\\d+)" + sFlt + sFlt + sFlt + sFlt, Pattern.CASE_INSENSITIVE);
            m_keywords[46] = Pattern.compile("^(CRATER_TIGFC),\\s*(\\d+),\\s*(\\d+)" + sFlt + sFlt + sFlt + sFlt, Pattern.CASE_INSENSITIVE);

            m_keywords[50] = Pattern.compile("REM,\\s*(.*)", Pattern.CASE_INSENSITIVE);
        }

    	public static void main(String [] parameters) throws SAXException, ParserConfigurationException, TransformerConfigurationException, NumberFormatException, StringIndexOutOfBoundsException
        {
            
            PanasonicUploader uploader = new PanasonicUploader(parameters);
            
            Element uPList = uploader.getUserProfileListElement();
            Element uP = uploader.m_xmlDoc.createElement("UserProfile");
            uPList.appendChild(uP);
            
            String uPType = "PANCLNo", uPName = "PANCLNo.";
            uP.setAttribute("Type", uPType);
            LinkedList [] llaAttrData = new LinkedList[1];
            llaAttrData[0] = new LinkedList();
            llaAttrData[0].add("CLNo");
            llaAttrData[0].add("integer");
            for (int ii=0; ii<5; ii++)
            {
                llaAttrData[0].add(String.valueOf(ii)); // CL number 0 ~ 4
                uP = uploader.createUserProfile(uPList, uPType, uPName+ii, llaAttrData);
                llaAttrData[0].removeLast(); // remove the CL number for adding the next one
            }
            
            llaAttrData[0].clear();
            uPType = "PANWeld";
            uPName = "PANWeld.1";
            llaAttrData[0].add("Weld");
            llaAttrData[0].add("string");
            llaAttrData[0].add("W");
            uP = uploader.createUserProfile(uPList, uPType, uPName, llaAttrData);
            
            llaAttrData[0].clear();
            uPType = "PANSync";
            uPName = "PANSync.1";
            llaAttrData[0].add("Sync");
            llaAttrData[0].add("boolean");
            llaAttrData[0].add("true");
            uP = uploader.createUserProfile(uPList, uPType, uPName, llaAttrData);
            
            //Get the robot program and parse it line by line
            String robotProgramName = uploader.getPathToRobotProgramFile();
            
            String [] additionalProgramNames = null;
            if (parameters.length > 12 && parameters[12].trim().length() > 0)
                additionalProgramNames = parameters[12].split("[\\t]+");
            
            File f = new File(robotProgramName);
            String path = f.getParent() + f.separator;
            String robotProgramNameOnly = f.getName();

            String uploadInfoFileName = uploader.getPathToErrorLogFile();
            Matcher match;

            try
            {
                bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));
                bw.write(VERSION);
                bw.write(COPYRIGHT);
                bw.write("\nStart of java parsing.\n\n");

                int errCode;
                errCode = uploader.processRobotProgram(robotProgramName);
                
                String sAddProg = null;
                int index;
                if (additionalProgramNames != null)
                {
                    for (int ii=0; ii<additionalProgramNames.length; ii++)
                    {
                        f = new File(additionalProgramNames[ii]);
                        sAddProg = f.getName();
                        index = uploader.programCallNames.indexOf(sAddProg);
                        if (index < 0)
                        {
                            uploader.programCallNames.add(uploader.programCallCount, sAddProg);
                            uploader.programCallNames.trimToSize();
                            uploader.programCallCount++;
                        }
                    }
                }

                if (errCode == 0)
                {
                    String calledProgName;
                    for (int ii=0; ii<uploader.programCallCount; ii++)
                    {
                        calledProgName = (String)uploader.programCallNames.get(ii);
                        if (calledProgName.equalsIgnoreCase(robotProgramNameOnly))
                            continue; // skip the "main" program already uploaded
                        
                        robotProgramName = path + calledProgName;
                        errCode = uploader.processRobotProgram(robotProgramName);
                    }
                }

                if (errCode == 0)
                {
                    bw.write("All the program statements have been parsed.\n");
                    bw.write("\nJava parsing completed successfully.\n");
                }
                else
                {
                    bw.write("\nERROR-Robot program upload failed.\n");
                }

                bw.write("\nEnd of java parsing.\n");
                bw.flush();
                bw.close();
            }//end try

            catch (IOException e) {
                e.getMessage();
            }
            
            Element motionProfileList = uploader.getMotionProfileListElement();
            if (motionProfileList.hasChildNodes() == false)
                uploader.createSpeedProfile("%50", "Default");

            Element accuracyProfileList = uploader.getAccuracyProfileListElement();
            if (accuracyProfileList.hasChildNodes() == false)
                uploader.createAccuracyProfile(accuracyProfileList, "Default",
                    DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                    false, 0.0);
        
            try {
                uploader.writeXMLStreamToFile();
            }

            catch (IOException e) {
                e.getMessage();
            }
            catch (TransformerException e) {
                e.getMessage();
            }
        }//end main

        private int processRobotProgram(String robotProgramName)
        {
            BufferedReader rbt;
            int errCode;

            Element resourceElem = super.getResourceElement();
            File f = new File(robotProgramName);
            String taskName = f.getName();
            taskName = taskName.substring(0, taskName.lastIndexOf('.'));
            Element activityListElem = super.createActivityList(taskName);
            resourceElem.appendChild(activityListElem);
            
            try
            {
                rbt = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));

                bw.write("Unparsed entities from the file \"");
                bw.write(robotProgramName);
                bw.write("\":\n\n"); 

                errCode = processRobotProgramLines(rbt, activityListElem);
                if (errCode != 0)
                    return errCode;
            }//end try

            catch (IOException e) {
                try {
                    bw.write("ERROR: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            return 0;
        }//end processRobotProgram

        private int processRobotProgramLines(BufferedReader rbt, Element activityListElem)
        {
            String line, attrName;
            Matcher match;
            Element attributeListElem;
            int errCode, offset;

            try
            {
                line = rbt.readLine();
                while (line != null)
                {
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                        continue;
                    }
                    
                    for (int ii = 0; ii < m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);

                        if (match.find() == true)
                        {
                            //Call appropriate methods to handle the input
                            switch (ii)
                            {
                                case 0: p_section = "Description";
                                    break;
                                case 1: p_section = "Pose";
                                    break;
                                case 2: p_section = "Variable";
                                    break;
                                case 3: p_section = "Command";
                                    break;
                                case 11: processTarget(line);
                                    break;
                                case 20: // Tool, 1:TOOL01
                                    processTool(line, activityListElem);
                                    break;
                                case 21: processSignalOut(line, match, activityListElem);
                                    break;
                                case 22: processDelay(match.group(1), activityListElem);
                                    break;
                                case 23: processCall(match, activityListElem);
                                    break;
                                case 24: processWait_IP(line, match, activityListElem);
                                    break;
                                case 30:
                                    if (0 != processMove(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 31:
                                    if (0 != processGoHome(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 40:
                                    if (1 == processArcON_OFF(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 41:
                                    if (1 == processArcSET(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 42:
                                    if (1 == processCrater(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 43:
                                    if (1 == processArcSET_TIG(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 44:
                                    if (1 == processCrater_TIG(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 45:
                                    if (1 == processArcSET_TIGFC(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 46:
                                    if (1 == processCrater_TIGFC(match, activityListElem))
                                        processComment(line, activityListElem, "Robot Language");
                                    break;
                                case 50: processComment(match.group(1), activityListElem, "Comment");
                                    break;
                            }//end switch

                            //match found & processed, break out for loop
                            break;
                        } // end if match found
                        else
                        {
                            if (ii == (m_keywords.length - 1))
                            {
                                if (p_section.equalsIgnoreCase("Description"))
                                {
                                    attrName = "Description." + headerCount;
                                    addAttribute(activityListElem, attrName, line);
                                    headerCount++;
                                }
                                else
                                {
                                    if (p_section.equalsIgnoreCase("Pose"))
                                    {
                                        attrName = "Pose." + headerCount;
                                        addAttribute(activityListElem, attrName, line);
                                        headerCount++;
                                    }
                                    else
                                    {
                                        if (p_section.equalsIgnoreCase("Variable"))
                                        {
                                            attrName = "Variable." + headerCount;
                                            addAttribute(activityListElem, attrName, line);
                                            headerCount++;
                                        }
                                        else // [Command] section
                                        {
                                            processComment(line, activityListElem, "Robot Language");
                                        }
                                    }
                                }
                            }
                        } // end if match NOT found
                    } // end for keywords
                    
                    /* AFTER processing the END line(add PostComment)
                     * skip EndOfFile characters added by some system
                     */
                    if (line.equalsIgnoreCase("END"))
                        break;
                    
                    line = rbt.readLine();

                } // end while
                rbt.close();
            } // end try

            catch (IOException e) {
                e.getMessage();
                return 1;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-Robot must have an external axis to upload Servo Gun program.\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            
            addAttributeList(activityListElem, "Post");
            
            return 0;
        } // end of processRobotProgramLines

        private void processTarget(String line)
        {
            //parse the string into array
            //P1, AJ, 0.0, 10.0, 20.0, 30.0, 40.0, 50.0
            String [] components = line.split("[, \\t]+");
            int iTgtNum = Integer.parseInt(components[0].substring(1));

            int arraySize, ii;
            //Add the target to a dynamic array
            arraySize = p_listOfRobotTargets.size();
            if (arraySize > iTgtNum)
            {
                p_listOfRobotTargets.set(iTgtNum, components);
                posCommented.set(iTgtNum, Boolean.FALSE);
                posUsed.set(iTgtNum, Boolean.FALSE);
            }
            else
            {
                for (ii=arraySize; ii<iTgtNum; ii++)
                {
                    p_listOfRobotTargets.add(ii, "Target Undefined");
                    posCommented.add(ii, Boolean.FALSE);
                    posUsed.add(ii, Boolean.FALSE);
                }
                
                p_listOfRobotTargets.add(iTgtNum, components);
                posCommented.add(iTgtNum, Boolean.FALSE);
                posUsed.add(iTgtNum, Boolean.FALSE);
            }
            //Resize the array
            p_listOfRobotTargets.trimToSize();
            tgtNum = p_listOfRobotTargets.size()-1; // array starts with zero(0)
        } // end of processTarget

        // TOOL, 1:TOOL01
        private void processTool(String line, Element activityListElem)
        {
            String [] components = line.split("[, :]+");
            p_currentToolNumber = Integer.parseInt(components[1]) - 1;
            
            if (p_section.equalsIgnoreCase("Command"))
            {
                processComment(line, activityListElem, "Robot Language");
            }
            else if (p_section.equalsIgnoreCase("Description"))
            {
                String attrName = "Description." + headerCount;
                addAttribute(activityListElem, attrName, line);
                headerCount++;
            }
        }
        
        // OUT, o16#(14:Terminal Name)
        private void processSignalOut(String line, Matcher match, Element activityListElem)
        {
            // Output Group Bit: 1, 4, 8, or 16
            String sOutputGroupBit = match.group(1);
            String sPortNumber = match.group(2);
            String sIdentifier = match.group(3);
            String status = match.group(4);
            
            boolean bSignalValue = false;
            if (status.equalsIgnoreCase("ON") || status.equals("1"))
                bSignalValue = true;
            
            Integer iPortNumber = Integer.valueOf(sPortNumber);
            String sSignalName = ioName + sPortNumber;
            if (ioName.endsWith("["))
                sSignalName += "]";
            
            Double dTime = new Double(0);
            Element actElem = super.createSetIOActivity(activityListElem, line, bSignalValue, sSignalName, iPortNumber, dTime);
            addAttribute(actElem, "Output Group Bit", sOutputGroupBit);
            addAttribute(actElem, "Identifier", sIdentifier);
            
            addAttributeList(actElem, "Pre");
        }
        
        private void processWait_IP(String line, Matcher match, Element activityListElem)
        {
            String sInputGroupBit = match.group(1);
            String sPortNumber = match.group(2);
            String sIdentifier = match.group(3);
            String status = match.group(4);
            String sTime = match.group(5);

            Double dTime = Double.valueOf(sTime);
            Integer iPortNumber = Integer.valueOf(sPortNumber);
            String sSignalName = ioName + sPortNumber;
            if (ioName.endsWith("["))
                sSignalName += "]";

            boolean bSignalValue = false;
            if (status.equalsIgnoreCase("ON") || status.endsWith("1"))
                bSignalValue = true;
            if (m_commentWaitSignal == false)
            {
                Element actElem = super.createWaitForIOActivity(activityListElem, line, bSignalValue, sSignalName, iPortNumber, dTime);

                addAttributeList(actElem, "Pre");
                addAttribute(actElem, "Output Group Bit", sInputGroupBit);
                addAttribute(actElem, "Identifier", sIdentifier);

                addAttributeList(actElem, "Pre");
            }
            else
            {
                processComment(line, activityListElem, "Robot Language");
            }
        } // end of processWait_IP
       
        private int processMove(Matcher match, Element activityListElem)
        {
            String sTarget = "1"; // sTarget needs defined for createTarget
            if (match.group(5) != null) // P1
                sTarget = match.group(6);
            else if (match.group(7) != null)  //GA#(1:identifier)
                sTarget = match.group(8);
            
            int iTgtNum = Integer.parseInt(sTarget);
            if (iTgtNum > p_listOfRobotTargets.size() - 1  ||
                (iTgtNum < p_listOfRobotTargets.size() &&
                 p_listOfRobotTargets.get(iTgtNum).toString().equals("Target Undefined")))
            {
                try {
                    bw.write("ERROR: Target not defined or target definition not supported: P" + sTarget + "\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            
            String [] sTargetComponents = (String []) p_listOfRobotTargets.get(iTgtNum);
            if (match.group(7) != null)  //GA#(1:identifier)
            {
                Boolean posComm = (Boolean) posCommented.get(iTgtNum);
                if (false == posComm.booleanValue())
                {
                    String attrName = "Pose." + headerCount;
                    String line = "";
                    int numComp = sTargetComponents.length;
                    for (int ii=0; ii<numComp-1; ii++)
                        line = line + sTargetComponents[ii] + ", ";
                    line = line + sTargetComponents[numComp-1];

                    addAttribute(activityListElem, attrName, line);
                    headerCount++;

                    posCommented.set(iTgtNum, Boolean.TRUE);
                }
                return 1;
            }
            
            String speedString = "", spdUnit = "";
            
            String sActivityName = "RobotMotion." + m_moveCounter;
            Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

            // MotionProfile name
            String motionProfileName = "Default";
            
            speedString = match.group(10);
            spdUnit = match.group(11);
            if (speedArcSet != null)
            {
                speedString = speedArcSet;
                spdUnit = "m/min";
            }
            motionProfileName = "V = " + speedString + " " + spdUnit;
            if (spdUnit.equalsIgnoreCase("m/min"))
            {
                double dSpd = Double.parseDouble(speedString)/60.0;
                speedString = Double.toString(dSpd);
            }
            else if (spdUnit.equals("%"))
            {
                speedString = "%" + speedString;
            }

            boolean isInList;
            isInList = m_listOfSpeedValues.contains(motionProfileName);
            if( false == isInList )
            {
                m_listOfSpeedValues.add(motionProfileName);
                createSpeedProfile(speedString, motionProfileName);
            }

            // ToolProfile name
            String toolProfileName = getToolName();

            // AccuracyProfile name
            String accuracyProfileName = "Default";
            isInList = p_listOfAccuracyValues.contains(accuracyProfileName);
            if (isInList == false)
            {
                p_listOfAccuracyValues.add(accuracyProfileName);
                createAccuracyProfile("0", accuracyProfileName);
            }

            // MotionType
            String sMoType = match.group(1);
            String sSync = match.group(3);
            String sCLNo = match.group(13);
            String sWeld = match.group(14);
            
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            
            Hashtable userProfiles = null;
            if (sWeld.equalsIgnoreCase("W"))
            {
                userProfiles = new Hashtable();
                userProfiles.put("PANWeld", "PANWeld.1");
            }
            
            if (sMoType.equalsIgnoreCase("MOVEP"))
            {
                numCircularMoves = 0;
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            }
            else
            {
                if (userProfiles == null)
                    userProfiles = new Hashtable();
                userProfiles.put("PANCLNo", "PANCLNo." + sCLNo);
                if (sSync != null && sSync.equals("+"))
                    userProfiles.put("PANSync", "PANSync.1");
                
                if (sMoType.equalsIgnoreCase("MOVEL"))
                {
                    numCircularMoves = 0;
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                }
                else
                {
                    if (sMoType.equalsIgnoreCase("MOVEC"))
                    {
                        if (numCircularMoves == 0) // the first MOVEC is uploaded as MOVEL
                        {
                            numCircularMoves = 1;
                            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                        }
                        else
                        if (numCircularMoves == 1)
                        {
                            numCircularMoves = 2;
                            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
                        }
                        else
                        {
                            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
                        }
                    }
                }
            }
            
            if (match.group(2) != null)
            {
                String sUserProfileType = "PANWeave";
                String wvPattern = match.group(16);
                String wvFrequency = match.group(18);
                String wvTimer = match.group(20);
                String wvDirection = match.group(22);
                String directionChange = match.group(24);
                
                Element uPList = super.getUserProfileListElement();
                Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
                if (uP == null)
                {
                    uP = m_xmlDoc.createElement("UserProfile");
                    uPList.appendChild(uP);
                    uP.setAttribute("Type", sUserProfileType);
                }
                String uProfileName = "";
                int uPInstNum = 0;
                String [] weaveData = {wvPattern, wvFrequency, wvTimer};
                uPInstNum = uPInstExist(uP, weaveData, weaveProfile);
                if (uPInstNum > 0)
                {
                    uProfileName = sUserProfileType + "." + uPInstNum;
                }
                else
                {
                    uProfileName = sUserProfileType + "." + weaveProfileCount++;
                    
                    LinkedList [] llaAttrData = new LinkedList[5];
                    for (int ii=0; ii<5; ii++)
                        llaAttrData[ii] = new LinkedList();
                    llaAttrData[0].add(weaveProfile[0]);
                    llaAttrData[0].add("integer");
                    llaAttrData[0].add(wvPattern);
                    llaAttrData[1].add(weaveProfile[1]);
                    llaAttrData[1].add("double");
                    llaAttrData[1].add(wvFrequency);
                    llaAttrData[2].add(weaveProfile[2]);
                    llaAttrData[2].add("double");
                    llaAttrData[2].add(wvTimer);
                    llaAttrData[3].add(weaveProfile[3]);
                    llaAttrData[3].add("string");
                    llaAttrData[3].add(wvDirection);
                    llaAttrData[4].add(weaveProfile[4]);
                    llaAttrData[4].add("string");
                    llaAttrData[4].add(directionChange);
                    uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
                }
                
                if (userProfiles == null)
                    userProfiles = new Hashtable();
                userProfiles.put(sUserProfileType, uProfileName);
            }
            Element moAttrElem = super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, "Default", userProfiles, eMotype);

            createTarget(sTarget, actElem);
            m_moveCounter++;

            // create activity attributes AFTER creating target
            if (match.group(7) != null)
                addAttribute(actElem, "Identifier", match.group(7) + ":" + match.group(9));
            /* these attributes are now in the user applicative profiles
            if (sCLNo != null)
                addAttribute(actElem, "CL No", sCLNo);

            addAttribute(actElem, "Air-Cut/Weld", sWeld);
            */
            
            Boolean bPosUsed = (Boolean) posUsed.get(iTgtNum);
            if (true == bPosUsed.booleanValue())
            {
                addAttribute(actElem, "TargetNum", String.valueOf(++tgtNum));
            }
            else
            {
                addAttribute(actElem, "TargetNum", sTargetComponents[0].substring(1));
            }
            addAttributeList(actElem, "Pre");
            posUsed.set(iTgtNum, Boolean.TRUE);
            
            return 0;
        } // processMove
        
        private int processGoHome(Matcher match, Element activityListElem)
        {
            String sTarget = match.group(4);
            
            int iTgtNum = Integer.parseInt(sTarget);
            if (iTgtNum > p_listOfRobotTargets.size() - 1  ||
                (iTgtNum < p_listOfRobotTargets.size() &&
                 p_listOfRobotTargets.get(iTgtNum).toString().equals("Target Undefined")))
            {
                try {
                    bw.write("ERROR: Target not defined or target definition not supported: P" + sTarget + "\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            
            String speedString = "", spdUnit = "";
            
            String sActivityName = "RobotMotion." + m_moveCounter;
            Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);

            // MotionProfile name
            String motionProfileName = "Default";
            
            speedString = match.group(5);
            spdUnit = match.group(6);
            motionProfileName = "V = " + speedString + " " + spdUnit;
            if (spdUnit.equalsIgnoreCase("m/min"))
            {
                double dSpd = Double.parseDouble(speedString)/60.0;
                speedString = Double.toString(dSpd);
            }
            else if (spdUnit.equals("%"))
            {
                speedString = "%" + speedString;
            }

            boolean isInList;
            isInList = m_listOfSpeedValues.contains(motionProfileName);
            if( false == isInList )
            {
                m_listOfSpeedValues.add(motionProfileName);
                createSpeedProfile(speedString, motionProfileName);
            }

            // ToolProfile name
            String toolProfileName = getToolName();

            // AccuracyProfile name
            String accuracyProfileName = "Default";
            isInList = p_listOfAccuracyValues.contains(accuracyProfileName);
            if (isInList == false)
            {
                p_listOfAccuracyValues.add(accuracyProfileName);
                createAccuracyProfile("0", accuracyProfileName);
            }

            // MotionType
            String sMoType = match.group(2);
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            
            if (sMoType.equalsIgnoreCase("MOVEP"))
            {
                numCircularMoves = 0;
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            }
            else
            {
                if (sMoType.equalsIgnoreCase("MOVEL"))
                {
                    numCircularMoves = 0;
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                }
                else
                {
                    if (sMoType.equalsIgnoreCase("MOVEC"))
                    {
                        if (numCircularMoves == 0)
                        {
                            numCircularMoves = 1;
                            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                        }
                        else
                        if (numCircularMoves == 1)
                        {
                            numCircularMoves = 2;
                            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
                        }
                        else
                        {
                            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
                        }
                    }
                }
            }

            Element moAttrElem = super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, "Default", eMotype);

            createTarget(sTarget, actElem);
            
            Element tgt = kku.findChildElemByName(actElem, "Target", null, null);
            Element jTgt = kku.findChildElemByName(tgt, "JointTarget", null, null);
            Element home = m_xmlDoc.createElement("HomeName");
            jTgt.appendChild(home);
            
            String sHomeName = "Home." + activityListElem.getAttribute("Task") + "." + m_homeCounter;
            Text tHome = m_xmlDoc.createTextNode(sHomeName);
            home.appendChild(tHome);
            
            m_homeCounter++;
            m_moveCounter++;
            
            addAttributeList(actElem, "Pre");
            
            return 0;
        } // processGoHome

        private void processDelay(String sDelayTime, Element activityListElem)
        {
            Double dStartTime = new Double(0.0);
            Double dEndTime = Double.valueOf(sDelayTime);
            String sActivityName = "RobotDelay." + m_delayCounter;

            Element actElem = super.createDelayActivity(activityListElem, sActivityName, dStartTime, dEndTime);

            m_delayCounter++;
            
            addAttributeList(actElem, "Pre");
        } // end of processDelay

        private void processComment(String line, Element activityListElem, String attributeName)
        {
            String comment = line;
            if (uploadStyle.equalsIgnoreCase("OperationComments"))
            {
                //Create DOM Nodes and set appropriate attributes
                Element actElem = m_xmlDoc.createElement("Activity");
                activityListElem.appendChild (actElem);

                actElem.setAttribute ("ActivityType", "Operation");

                Element activityName = m_xmlDoc.createElement("ActivityName");
                actElem.appendChild(activityName);
                String S_actName = "Comment.1";
                if (attributeName.equals("Comment"))
                    S_actName = attributeName + '.' + m_commentCounter++;
                else
                if (attributeName.equals("Robot Language"))
                    S_actName = attributeName + '.' + m_rbtLangCounter++;

                Text activityNameTxt = m_xmlDoc.createTextNode(S_actName);
                activityName.appendChild(activityNameTxt);

                String [] attrNames = {attributeName};
                String [] attrValues = {comment};
                Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
                
                // this may be MidComment if subsequent statements are ARC commands
                commentLines.add(commentCount, comment);
                commentCount++;
            }
            else
            {
                if (attributeName.equals("Robot Language"))
                    comment = "Robot Language:" + comment;
                commentLines.add(commentCount, comment);
                commentCount++;
            }
        } // end of processComment

        private void addAttributeList(Element actElem, String prefix)
        {
            if (commentCount == 0)
                return;
            /* When parameter OLPStyle = OperationComments, comments are accumulated for MidComment
             * Reset commentCount for PreComment and PostComment.
             */
            if (uploadStyle.equalsIgnoreCase("OperationComments") && false == prefix.equals("Mid"))
            {
                commentCount = 0;
                return;
            }

            String [] attrNames = new String[commentCount];
            String [] attrValues = new String[commentCount];
            for (int ii=0; ii<commentCount; ii++)
            {
                attrNames[ii] = prefix + "Comment" + String.valueOf(ii+1);
                attrValues[ii] = (String)commentLines.get(ii);
            }

            Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
            commentCount = 0;
        } // end addAttributeList

        private void addAttribute(Element actElem, String attrName, String attrVal)
        {
            String [] attrNames = {attrName};
            String [] attrValues = {attrVal};
            Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
        } // end addAttribute

        private void createTarget(String sTarget, Element actElem)
        {
            String [] sTargetComponents;
            sTargetComponents = (String []) p_listOfRobotTargets.get(Integer.parseInt(sTarget));
            int jNum = sTargetComponents.length - 2;
            String [] sTargetValues = new String[jNum];
            for (int ii=0; ii<jNum; ii++)
                sTargetValues[ii] = sTargetComponents[ii+2];

            int numOfDOF = m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes;
            int ii, jj;
            ArrayList [] oaJointTarget = new ArrayList[numOfDOF];
            for (ii=0; ii<numOfDOF; ii++)
                oaJointTarget[ii] = new ArrayList(1);

            for (ii=0; ii<m_numRobotAxes; ii++)
                setJointArrayList(sTargetValues[ii], "Robot", ii, oaJointTarget[ii]);

            int targetNum, extAxisDOF;
            if (m_numAuxAxes > 0 && m_numAuxAxes < jNum - m_numRobotAxes + 1)
            {
                for (ii=m_numRobotAxes; ii<m_numRobotAxes+m_numAuxAxes; ii++)
                    setJointArrayList(sTargetValues[ii], "RailTrackGantry", ii, oaJointTarget[ii]);
            }

            if (m_numExtAxes > 0 && m_numExtAxes < jNum - m_numRobotAxes - m_numAuxAxes + 1)
            {
                for (ii=m_numRobotAxes+m_numAuxAxes; ii<m_numRobotAxes+m_numAuxAxes+m_numExtAxes; ii++)
                    setJointArrayList(sTargetValues[ii], "EndOfArmTooling", ii, oaJointTarget[ii]);
            }
            
            if (m_numWorkAxes > 0 && m_numWorkAxes < jNum - m_numRobotAxes - m_numAuxAxes - m_numExtAxes + 1)
            {
                for (ii=m_numRobotAxes+m_numAuxAxes+m_numExtAxes; ii<numOfDOF; ii++)
                    setJointArrayList(sTargetValues[ii], "WorkpiecePositioner", ii, oaJointTarget[ii]);
            }

            Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false);
            double [] cartValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            Element cartTargetElem = super.createCartesianTarget(targetElem, cartValues, "ConfigNoMatter");
            super.createTag(cartTargetElem, "P"+sTarget);
            Element jointTargetElem = super.createJointTarget(targetElem, oaJointTarget);
        }//end createTarget method

        private void processCall(Matcher match, Element activityListElem)
        {
            String callNameStr = match.group(1);
            String taskNameStr = callNameStr.substring(0, callNameStr.lastIndexOf('.'));

            String activityName = "RobotCall." + robotCallNumber++;
            Element actElem = super.createCallTaskActivity(activityListElem, activityName, taskNameStr);

            int index = programCallNames.indexOf(callNameStr);
            if (index < 0)
            {
                programCallNames.add(programCallCount, callNameStr);
                programCallCount++;
            }

            addAttributeList(actElem, "Pre");
        } // end of processCallJobStatement

        private int processArcON_OFF(Matcher match, Element activityListElem)
        {
            String sUserProfileType = "PANArcOFF";
            String sActName = "ARC-OFF.";
            String sFileName = "ArcStart1.prg";
            String sTableNumber = "1";
            if (match.group(1).equalsIgnoreCase("ARC-ON"))
            {
                sUserProfileType = "PANArcON";
                sActName = "ARC-ON.";
            }
            if (match.group(1).equalsIgnoreCase("ARC-OFF"))
            {
                sUserProfileType = "PANArcOFF";
                sActName = "ARC-OFF.";
                
                speedArcSet = null;
            }
            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", sUserProfileType);
            }

            Element actElem = (Element)activityListElem.getLastChild();
            Element tmpElem;
            while (null != actElem)
            {
                if (actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") &&
                    null != kku.findChildElemByName(actElem, "MotionAttributes", null, null))
                {
                    addAttributeList(actElem, "Mid");
                    break;
                }
                
                if (actElem.getAttribute("ActivityType").equals("Operation"))
                {
                    tmpElem = actElem;
                    actElem = (Element)actElem.getPreviousSibling();
                    activityListElem.removeChild(tmpElem);
                }
                else
                {
                    actElem = (Element)actElem.getPreviousSibling();
                }
            }
            if (null == actElem ||
                kku.findChildElemByName(actElem, "ActivityName", null, null).getFirstChild().getNodeValue().startsWith("ARC-O"))
            {
                try {
                    bw.write("ERROR: No move command in front of " + sActName + "\n");
                }
                catch (IOException bwe) {
                }
                
                return 1;
            }
            Element actName = kku.findChildElemByName(actElem, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue(sActName + (m_moveCounter-1));
            Element moAttr = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", sUserProfileType);

            String uProfileName = "";
            
            int uPInstNum = 0;
            sFileName = match.group(2);
            sTableNumber = match.group(3);
            String [] saArcON_OFF_Data = {sFileName, sTableNumber};
            uPInstNum = uPInstExist(uP, saArcON_OFF_Data, arcON_OFF_Cond);
            if (uPInstNum > 0)
            {
                uProfileName = sUserProfileType + "." + uPInstNum;
            }
            else
            {
                int arcCount = 1;
                if (match.group(1).equalsIgnoreCase("ARC-ON"))
                    arcCount = arcStartCount++;
                else if (match.group(1).equalsIgnoreCase("ARC-OFF"))
                    arcCount = arcEndCount++;
                
                uProfileName = sUserProfileType + "." + arcCount;
            }
            
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return 0;

            LinkedList [] llaAttrData = new LinkedList[2];
            for (int ii=0; ii<2; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcON_OFF_Cond[0]);
            llaAttrData[0].add("string");
            llaAttrData[0].add(sFileName);
            llaAttrData[1].add(arcON_OFF_Cond[1]);
            llaAttrData[1].add("integer");
            llaAttrData[1].add(sTableNumber);
            uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
            
            return 0;
        } // end processArcON_OFF

        private int processArcSET(Matcher match, Element activityListElem)
        {
            String sUserProfileType = "PANArcSET";
            String sActName = "Arc-SET.";
            String sAmperage = match.group(2);
            String sVoltage = match.group(3);
            String sSpeed = match.group(4);
            speedArcSet = sSpeed;

            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", sUserProfileType);
            }

            Element actElem = (Element)activityListElem.getLastChild();
            while (null != actElem &&
                   (false == actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") ||
                    null == kku.findChildElemByName(actElem, "MotionAttributes", null, null)))
            {
                actElem = (Element)actElem.getPreviousSibling();
            }
            if (null == actElem ||
                kku.findChildElemByName(actElem, "ActivityName", null, null).getFirstChild().getNodeValue().startsWith("ARC-O"))
            {
                try {
                    bw.write("ERROR: No move command in front of ARC-SET.\n");
                }
                catch (IOException bwe) {
                }
                
                return 1;
            }
            Element actName = kku.findChildElemByName(actElem, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue(sActName + (m_moveCounter-1));
            Element moAttr = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", sUserProfileType);

            String uProfileName = "";
            
            int uPInstNum = 0;
            String [] saArcSET_Data = {sAmperage, sVoltage, sSpeed};
            uPInstNum = uPInstExist(uP, saArcSET_Data, arcSET_Cond);
            if (uPInstNum > 0)
                uProfileName = sUserProfileType + "." + uPInstNum;
            else
                uProfileName = sUserProfileType + "." + arcSetCount++;
            
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return 0;

            LinkedList [] llaAttrData = new LinkedList[3];
            for (int ii=0; ii<3; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcSET_Cond[0]);
            llaAttrData[0].add("integer");
            llaAttrData[0].add(sAmperage);
            llaAttrData[1].add(arcSET_Cond[1]);
            llaAttrData[1].add("double");
            llaAttrData[1].add(sVoltage);
            llaAttrData[2].add(arcSET_Cond[2]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(sSpeed);
            uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
            
            return 0;
        } // end processArcSET

        private int processCrater(Matcher match, Element activityListElem)
        {
            String sUserProfileType = "PANArcCRATER";
            String sActName = "CRATER.";
            String sAmperage = match.group(2);
            String sVoltage = match.group(3);
            String sTime = match.group(4);

            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", sUserProfileType);
            }

            Element actElem = (Element)activityListElem.getLastChild();
            while (null != actElem &&
                   (false == actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") ||
                    null == kku.findChildElemByName(actElem, "MotionAttributes", null, null)))
            {
                actElem = (Element)actElem.getPreviousSibling();
            }
            if (null == actElem ||
                kku.findChildElemByName(actElem, "ActivityName", null, null).getFirstChild().getNodeValue().startsWith("ARC-O"))
            {
                try {
                    bw.write("ERROR: No move command in front of CRATER.\n");
                }
                catch (IOException bwe) {
                }
                
                return 1;
            }
            Element actName = kku.findChildElemByName(actElem, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue(sActName + (m_moveCounter-1));
            Element moAttr = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", sUserProfileType);

            String uProfileName = "";
            
            int uPInstNum = 0;
            String [] saArcCRATER_Data = {sAmperage, sVoltage, sTime};
            uPInstNum = uPInstExist(uP, saArcCRATER_Data, arcCRATER_Cond);
            if (uPInstNum > 0)
                uProfileName = sUserProfileType + "." + uPInstNum;
            else
                uProfileName = sUserProfileType + "." + arcCraterCount++;
            
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return 0;

            LinkedList [] llaAttrData = new LinkedList[3];
            for (int ii=0; ii<3; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcCRATER_Cond[0]);
            llaAttrData[0].add("integer");
            llaAttrData[0].add(sAmperage);
            llaAttrData[1].add(arcCRATER_Cond[1]);
            llaAttrData[1].add("double");
            llaAttrData[1].add(sVoltage);
            llaAttrData[2].add(arcCRATER_Cond[2]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(sTime);
            uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
            
            return 0;
        } // end processCrater
        
        private int processArcSET_TIG(Matcher match, Element activityListElem)
        {
            String sUserProfileType = "PANArcSetTig";
            String sActName = "Arc-SET_TIG.";
            String bsAmp = match.group(2);
            String pkAmp = match.group(3);
            String sFillerSpeed = match.group(4);
            String sFrequency = match.group(5);
            String sSpeed = match.group(6);
            speedArcSet = sSpeed;

            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", sUserProfileType);
            }

            Element actElem = (Element)activityListElem.getLastChild();
            while (null != actElem &&
                   (false == actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") ||
                    null == kku.findChildElemByName(actElem, "MotionAttributes", null, null)))
            {
                actElem = (Element)actElem.getPreviousSibling();
            }
            if (null == actElem ||
                kku.findChildElemByName(actElem, "ActivityName", null, null).getFirstChild().getNodeValue().startsWith("ARC-O"))
            {
                try {
                    bw.write("ERROR: No move command in front of ARC-SET_TIG.\n");
                }
                catch (IOException bwe) {
                }
                
                return 1;
            }
            Element actName = kku.findChildElemByName(actElem, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue(sActName + (m_moveCounter-1));
            Element moAttr = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", sUserProfileType);

            String uProfileName = "";
            
            int uPInstNum = 0;
            String [] saArcSET_TIG_Data = {bsAmp, pkAmp, sFillerSpeed, sFrequency, sSpeed};
            uPInstNum = uPInstExist(uP, saArcSET_TIG_Data, arcSET_TIG_Cond);
            if (uPInstNum > 0)
                uProfileName = sUserProfileType + "." + uPInstNum;
            else
                uProfileName = sUserProfileType + "." + arcSetTigCount++;
            
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return 0;

            LinkedList [] llaAttrData = new LinkedList[5];
            for (int ii=0; ii<5; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcSET_TIG_Cond[0]);
            llaAttrData[0].add("integer");
            llaAttrData[0].add(bsAmp);
            llaAttrData[1].add(arcSET_TIG_Cond[1]);
            llaAttrData[1].add("integer");
            llaAttrData[1].add(pkAmp);
            llaAttrData[2].add(arcSET_TIG_Cond[2]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(sFillerSpeed);
            llaAttrData[3].add(arcSET_TIG_Cond[3]);
            llaAttrData[3].add("double");
            llaAttrData[3].add(sFrequency);
            llaAttrData[4].add(arcSET_TIG_Cond[4]);
            llaAttrData[4].add("double");
            llaAttrData[4].add(sSpeed);
            uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
            
            return 0;
        } // end processArcSET_TIG
        
        private int processArcSET_TIGFC(Matcher match, Element activityListElem)
        {
            String sUserProfileType = "PANArcSetTigFC";
            String sActName = "Arc-SET_TIGFC.";
            String bsAmp = match.group(2);
            String pkAmp = match.group(3);
            String bsWireVel = match.group(4);
            String pkWireVel = match.group(5);
            String sPulseFrequency = match.group(6);
            String sSpeed = match.group(7);
            speedArcSet = sSpeed;

            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", sUserProfileType);
            }

            Element actElem = (Element)activityListElem.getLastChild();
            while (null != actElem &&
                   (false == actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") ||
                    null == kku.findChildElemByName(actElem, "MotionAttributes", null, null)))
            {
                actElem = (Element)actElem.getPreviousSibling();
            }
            if (null == actElem ||
                kku.findChildElemByName(actElem, "ActivityName", null, null).getFirstChild().getNodeValue().startsWith("ARC-O"))
            {
                try {
                    bw.write("ERROR: No move command in front of ARC-SET_TIGFC.\n");
                }
                catch (IOException bwe) {
                }
                
                return 1;
            }
            Element actName = kku.findChildElemByName(actElem, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue(sActName + (m_moveCounter-1));
            Element moAttr = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", sUserProfileType);

            String uProfileName = "";
            
            int uPInstNum = 0;
            String [] saArcSET_TIGFC_Data = {bsAmp, pkAmp, bsWireVel, pkWireVel, sPulseFrequency, sSpeed};
            uPInstNum = uPInstExist(uP, saArcSET_TIGFC_Data, arcSET_TIGFC_Cond);
            if (uPInstNum > 0)
                uProfileName = sUserProfileType + "." + uPInstNum;
            else
                uProfileName = sUserProfileType + "." + arcSetTigFCCount++;
            
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return 0;

            LinkedList [] llaAttrData = new LinkedList[6];
            for (int ii=0; ii<6; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcSET_TIGFC_Cond[0]);
            llaAttrData[0].add("integer");
            llaAttrData[0].add(bsAmp);
            llaAttrData[1].add(arcSET_TIGFC_Cond[1]);
            llaAttrData[1].add("integer");
            llaAttrData[1].add(pkAmp);
            llaAttrData[2].add(arcSET_TIGFC_Cond[2]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(bsWireVel);
            llaAttrData[3].add(arcSET_TIGFC_Cond[3]);
            llaAttrData[3].add("double");
            llaAttrData[3].add(pkWireVel);
            llaAttrData[4].add(arcSET_TIGFC_Cond[4]);
            llaAttrData[4].add("double");
            llaAttrData[4].add(sPulseFrequency);
            llaAttrData[5].add(arcSET_TIGFC_Cond[5]);
            llaAttrData[5].add("double");
            llaAttrData[5].add(sSpeed);
            uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
            
            return 0;
        } // end processArcSET_TIGFC

        private int processCrater_TIG(Matcher match, Element activityListElem)
        {
            String sUserProfileType = "PANArcCraterTig";
            String sActName = "CRATER_TIG.";
            String bsAmp = match.group(2);
            String pkAmp = match.group(3);
            String sFillerSpeed = match.group(4);
            String sFrequency = match.group(5);
            String sTime = match.group(6);

            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", sUserProfileType);
            }

            Element actElem = (Element)activityListElem.getLastChild();
            while (null != actElem &&
                   (false == actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") ||
                    null == kku.findChildElemByName(actElem, "MotionAttributes", null, null)))
            {
                actElem = (Element)actElem.getPreviousSibling();
            }
            if (null == actElem ||
                kku.findChildElemByName(actElem, "ActivityName", null, null).getFirstChild().getNodeValue().startsWith("ARC-O"))
            {
                try {
                    bw.write("ERROR: No move command in front of CRATER_TIG.\n");
                }
                catch (IOException bwe) {
                }
                
                return 1;
            }
            Element actName = kku.findChildElemByName(actElem, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue(sActName + (m_moveCounter-1));
            Element moAttr = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", sUserProfileType);

            String uProfileName = "";
            
            int uPInstNum = 0;
            String [] saArcCRATER_TIG_Data = {bsAmp, pkAmp, sFillerSpeed, sFrequency, sTime};
            uPInstNum = uPInstExist(uP, saArcCRATER_TIG_Data, arcCRATER_TIG_Cond);
            if (uPInstNum > 0)
                uProfileName = sUserProfileType + "." + uPInstNum;
            else
                uProfileName = sUserProfileType + "." + arcCraterTigCount++;
            
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return 0;

            LinkedList [] llaAttrData = new LinkedList[5];
            for (int ii=0; ii<5; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcCRATER_TIG_Cond[0]);
            llaAttrData[0].add("integer");
            llaAttrData[0].add(bsAmp);
            llaAttrData[1].add(arcCRATER_TIG_Cond[1]);
            llaAttrData[1].add("integer");
            llaAttrData[1].add(pkAmp);
            llaAttrData[2].add(arcCRATER_TIG_Cond[2]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(sFillerSpeed);
            llaAttrData[3].add(arcCRATER_TIG_Cond[3]);
            llaAttrData[3].add("double");
            llaAttrData[3].add(sFrequency);
            llaAttrData[4].add(arcCRATER_TIG_Cond[4]);
            llaAttrData[4].add("double");
            llaAttrData[4].add(sTime);
            uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
            
            return 0;
        } // end processCrater_TIG

        private int processCrater_TIGFC(Matcher match, Element activityListElem)
        {
            String sUserProfileType = "PANArcCraterTigFC";
            String sActName = "CRATER_TIGFC.";
            String bsAmp = match.group(2);
            String pkAmp = match.group(3);
            String bsWireVel = match.group(4);
            String pkWireVel = match.group(5);
            String sPulseFrequency = match.group(6);
            String sTime = match.group(7);

            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", sUserProfileType);
            }

            Element actElem = (Element)activityListElem.getLastChild();
            while (null != actElem &&
                   (false == actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") ||
                    null == kku.findChildElemByName(actElem, "MotionAttributes", null, null)))
            {
                actElem = (Element)actElem.getPreviousSibling();
            }
            if (null == actElem ||
                kku.findChildElemByName(actElem, "ActivityName", null, null).getFirstChild().getNodeValue().startsWith("ARC-O"))
            {
                try {
                    bw.write("ERROR: No move command in front of CRATER_TIGFC.\n");
                }
                catch (IOException bwe) {
                }
                
                return 1;
            }
            Element actName = kku.findChildElemByName(actElem, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue(sActName + (m_moveCounter-1));
            Element moAttr = kku.findChildElemByName(actElem, "MotionAttributes", null, null);
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", sUserProfileType);

            String uProfileName = "";
            
            int uPInstNum = 0;
            String [] saArcCRATER_TIGFC_Data = {bsAmp, pkAmp, bsWireVel, pkWireVel, sPulseFrequency, sTime};
            uPInstNum = uPInstExist(uP, saArcCRATER_TIGFC_Data, arcCRATER_TIGFC_Cond);
            if (uPInstNum > 0)
                uProfileName = sUserProfileType + "." + uPInstNum;
            else
                uProfileName = sUserProfileType + "." + arcCraterTigFCCount++;
            
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return 0;

            LinkedList [] llaAttrData = new LinkedList[6];
            for (int ii=0; ii<6; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcCRATER_TIGFC_Cond[0]);
            llaAttrData[0].add("integer");
            llaAttrData[0].add(bsAmp);
            llaAttrData[1].add(arcCRATER_TIGFC_Cond[1]);
            llaAttrData[1].add("integer");
            llaAttrData[1].add(pkAmp);
            llaAttrData[2].add(arcCRATER_TIGFC_Cond[2]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(bsWireVel);
            llaAttrData[3].add(arcCRATER_TIGFC_Cond[3]);
            llaAttrData[3].add("double");
            llaAttrData[3].add(pkWireVel);
            llaAttrData[4].add(arcCRATER_TIGFC_Cond[4]);
            llaAttrData[4].add("double");
            llaAttrData[4].add(sPulseFrequency);
            llaAttrData[5].add(arcCRATER_TIGFC_Cond[5]);
            llaAttrData[5].add("double");
            llaAttrData[5].add(sTime);
            uP = super.createUserProfile(uPList, sUserProfileType, uProfileName, llaAttrData);
            
            return 0;
        } // end processCrater_TIGFC
        
        private int uPInstExist(Element uP, String [] arcData, String [] arcDataRef)
        {
            Element uPInst;
            NodeList uDefAttrNodes;
            Element uDefAttr, name, val;
            int sameData, numParam;

            NodeList uPInstNodes = uP.getChildNodes();
            numParam = arcData.length;
            for (int ii=0; ii<uPInstNodes.getLength(); ii++)
            {
                uPInst = (Element)uPInstNodes.item(ii);
                uDefAttrNodes = uPInst.getChildNodes();
                sameData = 0;
                for (int jj=0; jj<uDefAttrNodes.getLength(); jj++)
                {
                    uDefAttr = (Element)uDefAttrNodes.item(jj);

                    name = kku.findChildElemByName(uDefAttr, "DisplayName", null, null);
                    val = kku.findChildElemByName(uDefAttr, "Value", null, null);
                    // in case the user attributes' sequence differs, loop
                    for (int kk=0; kk<numParam; kk++)
                    {
                        if (name.getFirstChild().getNodeValue().equals(arcDataRef[kk]) &&
                            val.getFirstChild().getNodeValue().equals(arcData[kk]))
                        {
                            sameData++;
                            break;
                        }
                    } 

                    if (sameData == numParam)
                        return ii+1; // return instance number
                }
            }

            return 0;
        } // end uPInstExist

        private String getToolName()
        {
            String toolName = null;

            if (p_currentToolNumber < p_toolProfileNames.length)
                toolName = p_toolProfileNames[p_currentToolNumber];

            if (toolName == null)
                toolName = "Default";

            return toolName;
        } // end of getToolName
        
        public void setJointArrayList(String sTarget, String robotOrAuxJointType, int iJoint, ArrayList jTarget)
        {
            double targetValue = 0.0;
            try {
                targetValue = Double.parseDouble(sTarget);
                }

            catch(NumberFormatException e) {
                e.getMessage();
                targetValue = Double.NaN;
                }

            DNBIgpOlpUploadEnumeratedTypes.DOFType jType;
            if (m_axisTypes[iJoint].equals("Rotational"))
            {
                targetValue = Math.toRadians(targetValue);
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL;
            }
            else
            {
                targetValue = targetValue * 0.001;
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL;
            }

            String jName = "Joint " + (iJoint + 1);
            jTarget.add(0, jName); // set joint name
            jTarget.add(1, new Double(targetValue)); // set joint value
            jTarget.add(2, new Integer(iJoint + 1)); // set joint number
            jTarget.add(3, jType); // set joint type(rotational or translationsl)

            if (robotOrAuxJointType.equals("RailTrackGantry") ||
                robotOrAuxJointType.equals("EndOfArmTooling") ||
                robotOrAuxJointType.equals("WorkpiecePositioner"))
            {
                DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxType;
                auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                if (robotOrAuxJointType.equals("RailTrackGantry"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                else
                if (robotOrAuxJointType.equals("EndOfArmTooling"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING;
                else
                if (robotOrAuxJointType.equals("WorkpiecePositioner"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER;

                jTarget.add(4, auxType);
            }
        } // end of setJointArrayList

        public void createSpeedProfile(String speedString, String profileName)
        {
            //<MotionProfile>
                //<Name>Default</Name>
                //<MotionBasis>Percent|Absolute</MotionBasis>
                //<Speed Units="%"|"m/s" Value="50" />
                //<Accel Units="%" Value="100" />
                //<AngularSpeedValue Units="%" Value="100" />
                //<AngularAccelValue Units="%" Value="100" />
            //</MotionProfile>

            Element motionProfileList = super.getMotionProfileListElement();

            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis;
            double dSpd;
            if( speedString.startsWith("%") )
            {
                moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
                dSpd = Double.parseDouble(speedString.substring(1));
            }
            else
            {
                moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
                dSpd = Double.parseDouble(speedString);
            }

            Element motionProfileElem = super.createMotionProfile(motionProfileList, profileName, moBasis, dSpd, 100.0, 100.0, 100.0);
        } // end of createSpeedProfile

        public void createAccuracyProfile(String accuracyString, String profileName) throws NumberFormatException
        {
            //<AccuracyProfile>
                //<Name>Default</Name>
                //<FlyByMode>Off</FlyByMode>
                //<AccuracyType>Speed</AccuracyType>
                //<AccuracyValue Units="%" Value="0" />
            //</AccuracyProfile>

            Element accuracyProfileList = super.getAccuracyProfileListElement();

            boolean flyByMode = true;
            if (accuracyString.equals("0"))
                flyByMode = false;

            double accuracyVal = 0.0;

            Element accuracyProfileElem = super.createAccuracyProfile(accuracyProfileList, profileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED, flyByMode, accuracyVal);
         } // end of createAccuracyProfile

}//end class
