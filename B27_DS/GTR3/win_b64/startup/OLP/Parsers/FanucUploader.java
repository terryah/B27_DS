//DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2002-2005
// promote this
import org.w3c.dom.*;

import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

//Java Exception classes
import java.io.IOException;
import java.io.FileNotFoundException;
import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class FanucUploader extends DNBIgpOlpUploadBaseClass  {

        //Constants
	    private static final int JOINT = 1;
        private static final double ZERO_TOL = .001;
        private static final int CARTESIAN = 2;
        private static final String VERSION = "Delmia Corp. FANUC RJ TPE Uploader Version 5 Release R27 SP6 HF24.\n";
        private static final String COPYRIGHT = "Copyright Delmia Corp. 2002-2019, All Rights Reserved.\n";
        
        //Member primitive variables
     
        private Element m_toolProfileListElement;
        private Element m_motionProfileListElement;
        private Element m_accuracyProfileListElement;
        private Element m_objectProfileListElement;
        private boolean m_firstProgram = true;
        private boolean m_oldStyleStationary = false;
        private int m_targetType;
        private String m_intAxes ="false";
        private int m_numCStatements;        
        private int m_numRobotAxes;
        private int m_numAuxAxes;
        private int m_numExtAxes;
        private int m_numWorkAxes;
        private int m_auxAxesCount;
        private int m_extAxesCount;
        private int m_workAxesCount;
        private boolean m_auxAxesError;
        private boolean m_extAxesError;
        private boolean m_workAxesError;        
        private int m_auxAxesStart;
        private int m_extAxesStart;
        private int m_workAxesStart;        
        private int m_extAxesGroupNumber;
        private int m_auxAxesGroupNumber;
        private int m_workAxesGroupNumber;
        private String m_extAxesGroupString;
        private String m_auxAxesGroupString;
        private String m_workAxesGroupString;
        private int [] axesLocked;
        private int m_delayCounter;
        private int m_callCounter;
        private int m_calledProgramCounter;
        private int m_spotCounter;
        private int m_backupCounter;
        private int m_macroCounter;
        private int m_releaseCounter;
        private int m_grabCounter;
        private int m_moveCounter;
        private int m_gunmoveCounter;
        private int m_mountCounter;
        private int m_unmountCounter;
        private int m_enterZoneCounter;
        private int m_clearZoneCounter;
        private int m_accProfNum;
        private int m_motionProfNum;
        private int m_toolProfNum;
        private String m_TagSuffix;
        private int m_objectProfNum;
        private String m_vsCamClb;
        private int    m_vsCamClbTagCount;
        private String m_visionShiftAxis;
        private String m_encoding;
        private Element m_lastActivityCall;
        private int lastTargetNumber = 0;
        private String m_Turn1="True";
        private String m_Turn5="False";
        private String ProgName="TPEUPLOAD";
        private String m_progExtension = "ls";
        private double m_rcsVersion = 5.31;
        private String m_circularViaLine = "";
        private Element m_firstRobotMotion = null;
        private Element m_lastRobotMotion = null;
        private Element m_motionAttributeElement = null;
        private int m_robotMotionCompleted = 0;
        private int m_servoAttribsAdded = 0;
        private int m_servoUpload = 0;
        private int m_commentWaitSignal = 1;
        private String m_dropToolProfileName;
        private BufferedWriter m_bw;

        //Member objects
        private Document m_xmlDoc;
        private String m_currentTagPoint;
        private String m_currentToolNumber;
        private String m_currentObjectNumber;
        private String m_currentToolGroup;
        private String m_currentObjectGroup;
        private String m_maximumToolNumber;
        private String m_currentPosRegNumber;
        private String m_currentPosRegGroup;
        private String m_currentToolType;
        private String m_pickHome;
        private String m_dropHome;
        private String m_grabPart;
        private String m_partGrabbed;
        private String m_mountedToolName;
        private String m_currentArcStartIndex;
        private String m_currentArcEndIndex;
        private int    m_commentCount;
        private int    m_maxRobotSignal;
        private int    m_maxDigitalSignal;
        private int    m_maxWeldSignal;
        private double m_tipDistanceOffset;
        private double m_tipDistanceScale;
        private double m_arm1Length;
        private double m_arm2Length;
        private int    m_negateTipDistanceJoint;
        private boolean m_createJointTarget;
        
        private Pattern [] m_keywords;
        private Pattern [] sysvar_keywords;
        private String [] m_axisTypes;
        private ArrayList m_listOfRobotTargets;
        private ArrayList m_listOfPosRegisters;
        private ArrayList m_listOfAuxTargets;
        private ArrayList m_listOfExtTargets;
        private ArrayList m_listOfWorkTargets;
        private ArrayList m_listOfPosRegAuxTargets;
        private ArrayList m_listOfPosRegExtTargets;
        private ArrayList m_listOfPosRegWorkTargets;        
        private ArrayList m_listOfSpeedValues;
        private ArrayList m_listOfAccuracyValues;
        private ArrayList m_listOfCalledPrograms;
        private ArrayList m_listOfObjectFrameValues;
        private ArrayList m_listOfToolFrameValues;   
        private Hashtable m_targetTypes;
        private Hashtable m_posRegTargetTypes;
        private Hashtable m_weldScheduleHomes;
        private Hashtable m_weldScheduleDelays;
        private Hashtable m_macroCommands;
        private Hashtable m_backupHomes;
        private Hashtable m_tagNames;
        private Hashtable m_tagComments;
        private Hashtable m_toolNumberMapping;
        private Hashtable m_toolTypeMapping;
        private Hashtable m_objectNumberMapping;
        private Hashtable m_objectApplyOffsetsMapping;
        private Hashtable m_parameterData;
        private String [] m_commentNames;
        private String [] m_commentValues;        
        private ArrayList m_listofHomeNames;  
        private Element m_lastActElem;
        private String m_OLPStyle;
        private int m_operationCommentCount;
        private int m_robotLanguageCommentCount;
        private boolean m_toolProfileCreated;
        private boolean m_objectProfileCreated;
        private int     m_numberOfXmlToolProfiles;
        private int     m_numberOfXmlObjectProfiles;
        private String m_TagPrefix;
        private String m_orientMode;
        private boolean [] m_uframesUsed;
        private boolean [] m_utoolsUsed;
        private boolean [] m_uframesCreated;
        private int [] m_uframesSwap;
        private boolean [] m_utoolsCreated;  
        private boolean m_sysvarFound;

        
        
        //Constructor
        public FanucUploader(String [] parameterData, String [] profileNumbers, String [] axisTypes, String [] numOfAxis, 
            String [] mountedGunName, String[] toolProfileNames, String[] homeNames, String [] objectProfileNames) throws ParserConfigurationException {

             super(parameterData);
             
              
             m_targetType = CARTESIAN;
             m_numCStatements = 0;
             m_delayCounter = 1;
             m_callCounter = 1;
             m_calledProgramCounter = 0;
             m_moveCounter = 1;
             m_spotCounter = 1;
             m_backupCounter = 1;
             
             m_macroCounter = 1;
             m_grabCounter = 1;
             m_releaseCounter = 1;
             m_mountCounter = 1;
             m_unmountCounter = 1;
             m_enterZoneCounter = 1;
             m_clearZoneCounter = 1;
             m_commentWaitSignal = 1;
             
             m_numRobotAxes = super.getNumberOfRobotAxes();
             m_numAuxAxes = super.getNumberOfRailAuxiliaryAxes();
             m_numExtAxes = super.getNumberOfToolAuxiliaryAxes();
             m_numWorkAxes = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();
             m_auxAxesCount = 0;
             m_extAxesCount = 0;
             m_workAxesCount = 0;
             m_auxAxesError = false;
             m_extAxesError = false;
             m_workAxesError = false;             
             m_auxAxesStart = 1;
             m_extAxesStart = 1;
             m_workAxesStart = 1;             
             m_commentCount = 0;
             m_maxDigitalSignal = 100;
             m_maxRobotSignal = 200;
             m_maxWeldSignal = 300;
             m_tipDistanceOffset = 0.0;
             m_tipDistanceScale = 1.0;
             m_negateTipDistanceJoint = 0;
             m_createJointTarget = false;
             m_arm1Length = 0.0;
             m_arm2Length = 0.0;
             m_auxAxesGroupNumber = 1;
             m_extAxesGroupNumber = 2;
             m_workAxesGroupNumber = 3;
             m_auxAxesGroupString = "*,*,*,*,*";
             m_extAxesGroupString = "*,*,*,*,*";
             m_workAxesGroupString = "*,*,*,*,*";
             m_robotMotionCompleted = 0;
             m_servoAttribsAdded = 0;
             m_servoUpload = 0;
 
             m_motionProfNum = Integer.parseInt(profileNumbers[0]);
             m_accProfNum = Integer.parseInt(profileNumbers[1]);
             m_toolProfNum = Integer.parseInt(profileNumbers[2]);
             m_vsCamClb = "";
             m_vsCamClbTagCount = 0;
             m_visionShiftAxis = "X";
             m_encoding = "UTF-8";
             m_lastActivityCall = null;
             m_bw = null;
        /*     m_objectProfNum = Integer.parseInt(profileNumbers[3]); */
             
             //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1

             m_pickHome = new String("Home_1");
             m_dropHome = new String("Home_2");
             m_grabPart = new String("");
             m_partGrabbed = new String("");
             m_mountedToolName = new String("");
            
             m_currentTagPoint = new String("");
             m_currentToolNumber = new String("1");
             m_currentToolGroup = new String("");
             m_maximumToolNumber = new String("1");
             m_currentObjectNumber = new String("0");
             m_currentObjectGroup = new String("");
             m_currentPosRegNumber = new String("0");
             m_currentPosRegGroup = null;
             m_currentToolType = new String("OnRobot");
             m_currentArcStartIndex = new String("[0]");
             m_currentArcEndIndex = new String("[0]");

             m_keywords = new Pattern [34];
             sysvar_keywords = new Pattern [11];
             m_axisTypes = new String[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes];
             m_axisTypes = super.getAxisTypes();
             m_listOfRobotTargets = new ArrayList(5);
             m_listOfPosRegisters = new ArrayList(5);
             m_listOfAuxTargets = new ArrayList(2);
             m_listOfCalledPrograms = new ArrayList(1);
             m_listOfExtTargets = new ArrayList(2);
             m_listOfWorkTargets = new ArrayList(2);
             m_listOfPosRegAuxTargets = new ArrayList(2);
             m_listOfPosRegExtTargets = new ArrayList(2);
             m_listOfPosRegWorkTargets = new ArrayList(2);             
             m_listOfSpeedValues = new ArrayList(5);
             m_listOfAccuracyValues = new ArrayList(3);
             m_listOfObjectFrameValues = new ArrayList(2);
             m_listOfToolFrameValues = new ArrayList(2);
             m_targetTypes = new Hashtable();
             m_posRegTargetTypes = new Hashtable();
             m_tagNames = new Hashtable();
             m_tagComments = new Hashtable();
             m_toolNumberMapping = new Hashtable();
             m_toolTypeMapping = new Hashtable();
             m_objectNumberMapping = new Hashtable();
             m_objectApplyOffsetsMapping = new Hashtable();
             m_weldScheduleHomes = new Hashtable();
             m_weldScheduleDelays = new Hashtable();
             m_macroCommands = new Hashtable();
             m_backupHomes = new Hashtable();
             m_listofHomeNames = new ArrayList(2);
             m_commentNames = new String [1000];
             m_commentValues = new String [1000];             
             
             m_parameterData =  super.getParameterData();
             
             m_firstRobotMotion = null;
             m_lastRobotMotion = null;
             
             m_dropToolProfileName = "Default";
            

             m_xmlDoc = super.getDOMDocument();     
             m_toolProfileListElement = super.getToolProfileListElement();
             m_motionProfileListElement = super.getMotionProfileListElement();
             m_accuracyProfileListElement = super.getAccuracyProfileListElement();
             m_objectProfileListElement = super.getObjectFrameProfileListElement(); 
             m_lastActElem = null;      
             
             m_OLPStyle = "";
             m_operationCommentCount = 1;
             m_robotLanguageCommentCount = 1;
             m_toolProfileCreated = false;
             m_objectProfileCreated = false;
             m_TagPrefix = "";
             m_TagSuffix = "";
             m_orientMode = "2_axis";
             m_utoolsUsed = new boolean [20];           
             m_uframesUsed = new boolean [20];
             m_utoolsCreated = new boolean [20];           
             m_uframesCreated = new boolean [20];
             m_uframesSwap = new int [20];
             m_sysvarFound = false;
             
             
             for (int ii=0;ii<20;ii++)
             {
                 m_utoolsUsed[ii] = false;
                 m_uframesUsed[ii] = false;
                 m_utoolsCreated[ii] = false;
                 m_uframesCreated[ii] = false;     
                 m_uframesSwap[ii] = 0;
             }
             
             for(int ii = 0; ii < axisTypes.length; ii++)
                m_axisTypes[ii] = axisTypes[ii];

             m_numberOfXmlToolProfiles = toolProfileNames.length;
             for(int jj = 0; jj < toolProfileNames.length; jj++) {
                         m_toolNumberMapping.put(String.valueOf(jj+1), toolProfileNames[jj]);  
                         m_toolTypeMapping.put(String.valueOf(jj+1), "OnRobot");
             }
             m_numberOfXmlObjectProfiles = objectProfileNames.length;
             for(int jj = 0; jj < objectProfileNames.length; jj++) {
                         m_objectNumberMapping.put(String.valueOf(jj+1), objectProfileNames[jj]); 
                         m_objectApplyOffsetsMapping.put(String.valueOf(jj+1), "Off");
             }            
             for (int jj=0; jj < homeNames.length; jj++ ) {
                m_listofHomeNames.add(jj, homeNames[jj]);
             }
                          
             Enumeration parameterKeys = m_parameterData.keys();
             Enumeration parameterValues = m_parameterData.elements();
             
             while(parameterKeys.hasMoreElements()) {
                 
                 String parameterName = parameterKeys.nextElement().toString();
                 String parameterValue = parameterValues.nextElement().toString();
                 
                 int index = parameterName.indexOf('.');
                 String testString = parameterName.substring(0, index + 1);
                 
                 try {
                     if(parameterName.equalsIgnoreCase("Turn1")) {
                         m_Turn1 = parameterValue;
                     }
                     else if(parameterName.equalsIgnoreCase("Turn5")) {
                         m_Turn5 = parameterValue;
                     }                     
                     else if(parameterName.equalsIgnoreCase("IntegeratedAxes")) {
                         m_intAxes = parameterValue;
                     }
                    else if(parameterName.equalsIgnoreCase("WorldCoords")) {
                         m_intAxes = parameterValue;
// System.out.println("WorldCoords:"+m_intAxes);
                     }                     
                     else if(testString.equalsIgnoreCase("ToolProfile.")) {
                         String profileName = parameterName.substring(index + 1);
                         m_toolNumberMapping.put(profileName, parameterValue);
                     }
                     else if(testString.equalsIgnoreCase("ObjectProfile.")) {
                         String profileName = parameterName.substring(index + 1);
                         m_objectNumberMapping.put(profileName, parameterValue);
                     }                     
                     else if(testString.equalsIgnoreCase("WeldHome.")) {
                        int homeIndex = Integer.parseInt(parameterName.substring(index + 1));

                         if (homeIndex < 0)
                             homeIndex = 0;
                         
                         if (homeIndex > homeNames.length)
                             homeIndex = homeNames.length-1;
                         
                         String [] homeComponents = parameterValue.split(";");
                         
                         for (int kk=0;kk<homeComponents.length;kk++)
                              m_weldScheduleHomes.put(homeComponents[kk], homeNames[homeIndex-1]);
                     }   
                     else if(testString.equalsIgnoreCase("OpenHome.")) {
                         int homeIndex = Integer.parseInt(parameterName.substring(index + 1));
                         if (homeIndex < 0)
                             homeIndex = 0;
                         
                         if (homeIndex > homeNames.length)
                             homeIndex = homeNames.length-1;
                         
                         String [] homeComponents = parameterValue.split(";");
                         
                         for (int kk=0;kk<homeComponents.length;kk++)
                              m_backupHomes.put(homeComponents[kk], homeNames[homeIndex-1]); 
                         
                     }
                      else if(testString.equalsIgnoreCase("WeldDelay.")) {                        
                         double delayTime = Double.valueOf(parameterName.substring(index + 1)).doubleValue() * 0.001;                    
                         
                         String [] delayComponents = parameterValue.split(";");
                         
                         for (int kk=0;kk<delayComponents.length;kk++)
                              m_weldScheduleDelays.put(delayComponents[kk], String.valueOf(delayTime));
                     }   
                      else if (testString.equalsIgnoreCase("Macro.")) {
                          String macroAction = parameterName.substring(index+1);
                          String [] macroComponents = parameterValue.split(";");
                          for (int kk=0;kk<macroComponents.length;kk++)
                              m_macroCommands.put(macroComponents[kk], macroAction);
                          
                      }
                      else if (parameterName.equalsIgnoreCase("PickHome")) {
                          m_pickHome = parameterValue;
                      }
                       else if (parameterName.equalsIgnoreCase("DropHome")) {
                          m_dropHome = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("GrabPart")) {
                          
                          m_grabPart = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("PartGrabbed")) {
                          m_partGrabbed = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("MountedToolName")) {
                          m_mountedToolName = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("Railgroup")) {
                          String sRailGrp = parameterValue;
                          if (sRailGrp.indexOf(',') < 0)
                          {
                              m_auxAxesGroupNumber = Integer.valueOf(parameterValue).intValue();
                          }
                          else
                          {
                              m_auxAxesGroupNumber = 0;
                              m_auxAxesGroupString = sRailGrp;
                          }
                      }
                      else if (parameterName.equalsIgnoreCase("Toolgroup")) {
                          String sToolGrp = parameterValue;
                          if (sToolGrp.indexOf(',') < 0)
                          {
                              m_extAxesGroupNumber = Integer.valueOf(parameterValue).intValue();
                          }
                          else
                          {
                              m_extAxesGroupNumber = 0;
                              m_extAxesGroupString = sToolGrp;
                          }
                      }
                      else if (parameterName.equalsIgnoreCase("Workgroup")) {
                          String sWorkGrp = parameterValue;
                          if (sWorkGrp.indexOf(',') < 0)
                          {
                              m_workAxesGroupNumber = Integer.valueOf(parameterValue).intValue();
                          }
                          else
                          {
                              m_workAxesGroupNumber = 0;
                              m_workAxesGroupString = sWorkGrp;
                          }
                      }
                      else if (parameterName.equalsIgnoreCase("ServoGun")) {
                          m_servoUpload = Integer.valueOf(parameterValue).intValue();
                      }
                      else if (parameterName.equalsIgnoreCase("Digital_IO")) {
                          m_maxDigitalSignal = Integer.valueOf(parameterValue).intValue();
                      }  
                       else if (parameterName.equalsIgnoreCase("Robot_IO")) {
                          m_maxRobotSignal = Integer.valueOf(parameterValue).intValue();
                      }                   
                       else if (parameterName.equalsIgnoreCase("Weld_IO")) { 
                          m_maxWeldSignal = Integer.valueOf(parameterValue).intValue();
                      }  
                      else if (parameterName.equalsIgnoreCase("TipDistanceOffset")) {
                          m_tipDistanceOffset = Double.valueOf(parameterValue).doubleValue();
                      } 
                      else if (parameterName.equalsIgnoreCase("TipDistanceScale")) {
                          m_tipDistanceScale = Double.valueOf(parameterValue).doubleValue();
                      } 
                      else if (parameterName.equalsIgnoreCase("Arm1Length")) {
                          m_arm1Length = Double.valueOf(parameterValue).doubleValue();
                      }
                      else if (parameterName.equalsIgnoreCase("Arm2Length")) {
                          m_arm2Length = Double.valueOf(parameterValue).doubleValue();
                      } 
                      else if (parameterName.equalsIgnoreCase("NegateTipDistanceJoint")) {
                          m_negateTipDistanceJoint = Integer.valueOf(parameterValue).intValue();
                      }                      
                      else if (parameterName.equalsIgnoreCase("VisionShiftAxis")) {
                          m_visionShiftAxis = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("ProgramFileEncoding")) {
                          m_encoding = parameterValue;
                      }                     
                      else if (parameterName.equalsIgnoreCase("OLPStyle")) {
                          m_OLPStyle = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("TagPrefix")) {
                          m_TagPrefix = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("DropToolProfileName")) {
                          m_dropToolProfileName = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("AuxiliaryLock")) {
                          String [] axes = parameterValue.split(",");
                          int numAxes = axes.length;
                          axesLocked = new int[numAxes];
                          for (int ii=0; ii<numAxes; ii++)
                              axesLocked[ii] = Integer.parseInt(axes[ii]);
                      }
                      else if (parameterName.equalsIgnoreCase("CommentWaitSignal")) {
                          if (parameterValue.equalsIgnoreCase("false") == true)
                          {
                              m_commentWaitSignal = 0;
                          }
                      }
                      else if (parameterName.equalsIgnoreCase("OrientMode")) {
                          m_orientMode = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("FileExtension")) {
                          m_progExtension = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("RCS Version")) {
                    	  m_rcsVersion = Double.parseDouble(parameterValue);
                      }
                 }

                 finally {
                   continue;
                 } 
             }
             
             m_keywords[0] = Pattern.compile("^\\s*[0-9]*:\\s*UTOOL_NUM\\[?(GP[1-5])?\\]?\\s*=\\s*([A-F0-9]*)", Pattern.CASE_INSENSITIVE);
             m_keywords[1] = Pattern.compile("^P\\[\\s*([0-9]*)\\s*:?\\s*(.*)?\\s*\\]\\s*\\{", Pattern.CASE_INSENSITIVE);
             m_keywords[2] = Pattern.compile("^\\s*[0-9]*:\\s*([CLJ])\\s*(P|PR)\\[", Pattern.CASE_INSENSITIVE);
             // circular move very similar to item 1 above for postion def...no curly bracket
             m_keywords[3] = Pattern.compile("^\\s*:?\\s*(P|PR)\\[\\s*([0-9]*)\\s*:?\\s*(.*)?\\s*\\]\\s*", Pattern.CASE_INSENSITIVE);
             m_keywords[4] = Pattern.compile("^\\s*[0-9]*:\\s*([WDGR]*O)\\[\\s*([0-9]*)\\s*:?\\s*(.*)?\\s*\\]\\s*=\\s*PULSE\\s*,?\\s*([0-9]+)?(\\.)?([0-9]+)?\\s*", Pattern.CASE_INSENSITIVE);
             m_keywords[5] = Pattern.compile("^\\s*[0-9]*:\\s*([WDGR]*O)\\[\\s*([0-9]*)\\s*:?\\s*(.*)?\\s*\\]\\s*=\\s*([\\[A-Z0-9]+)", Pattern.CASE_INSENSITIVE);
             m_keywords[6] = Pattern.compile("^\\s*[0-9]*:\\s*WAIT\\s*([WDGRIO]+)\\[\\s*([0-9]*)\\s*:?\\s*(.*)?\\s*\\]\\s*=\\s*([\\[A-Z0-9]+)\\s*([0-9]+)?(\\.)?([0-9]+)?", Pattern.CASE_INSENSITIVE);
             m_keywords[7] = Pattern.compile("^\\s*[0-9]*:\\s*WAIT\\s*([0-9]*)(\\.)?([0-9]*)?", Pattern.CASE_INSENSITIVE);
             m_keywords[8] = Pattern.compile("SPOT\\[", Pattern.CASE_INSENSITIVE);
             m_keywords[9] = Pattern.compile("^\\s*\\/PROG\\s*(.*)", Pattern.CASE_INSENSITIVE);
             m_keywords[10] = Pattern.compile("^\\s*[0-9]*:\\s*UFRAME_NUM\\[?(GP[1-5])?\\]?\\s*=\\s*([0-9])", Pattern.CASE_INSENSITIVE);
             m_keywords[11] = Pattern.compile("CALL\\s*VSCAMCLB\\(([0-9])", Pattern.CASE_INSENSITIVE);
             m_keywords[12] = Pattern.compile("^\\s*[0-9]*:\\s*!", Pattern.CASE_INSENSITIVE);
             m_keywords[13] = Pattern.compile("^\\s*\\/ATTR\\s*", Pattern.CASE_INSENSITIVE);
             m_keywords[14] = Pattern.compile("^\\s*\\/APPL\\s*", Pattern.CASE_INSENSITIVE);
             m_keywords[15] = Pattern.compile("^\\s*\\/MN\\s*", Pattern.CASE_INSENSITIVE);
             m_keywords[16] = Pattern.compile("BACKUP\\[", Pattern.CASE_INSENSITIVE);
             m_keywords[17] = Pattern.compile("^\\s*[0-9]*:\\s*CALL\\s*([.[^;]]*)", Pattern.CASE_INSENSITIVE);
             m_keywords[18] = Pattern.compile("^\\s*[0-9]*:\\s*Arc Start\\s*\\[([.[^;]]*)", Pattern.CASE_INSENSITIVE);
             m_keywords[19] = Pattern.compile("^\\s*[0-9]*:\\s*Arc End\\s*\\[([.[^;]]*)", Pattern.CASE_INSENSITIVE);
             m_keywords[20] = Pattern.compile("^\\s*[0-9]*:\\s*(Weave)(.*)", Pattern.CASE_INSENSITIVE);
             m_keywords[21] = Pattern.compile("WELD_SPEED\\s*", Pattern.CASE_INSENSITIVE);
             m_keywords[22] = Pattern.compile("\\s+ACC([0-9]*)\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[23] = Pattern.compile("\\s+SKIP,", Pattern.CASE_INSENSITIVE);
             m_keywords[24] = Pattern.compile("\\s+OFFSET\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[25] = Pattern.compile("\\s+OFFSET,", Pattern.CASE_INSENSITIVE);
             m_keywords[26] = Pattern.compile("\\s+INC\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[27] = Pattern.compile("\\s+TOOL_OFFSET\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[28] = Pattern.compile("\\s+TOOL_OFFSET,", Pattern.CASE_INSENSITIVE);
             m_keywords[29] = Pattern.compile("\\s+TB\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[30] = Pattern.compile("\\s+TA\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[31] = Pattern.compile("\\s+PRESS_MOTION\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[32] = Pattern.compile("\\s+DB\\s+", Pattern.CASE_INSENSITIVE);
             m_keywords[33] = Pattern.compile("\\s+PTH\\s+", Pattern.CASE_INSENSITIVE);       
              
             
             sysvar_keywords[0] = Pattern.compile("MNUFRAME", Pattern.CASE_INSENSITIVE);
             sysvar_keywords[1] = Pattern.compile("MNUTOOL", Pattern.CASE_INSENSITIVE);
             sysvar_keywords[2] = Pattern.compile("POSREG", Pattern.CASE_INSENSITIVE);
             sysvar_keywords[3] = Pattern.compile("\\s*\\[?[A-F0-9]*?,([A-F0-9]*)?:?(.*)?\\]?\\s*Group:\\s*[0-9]\\s*Config:\\s*([A-Z])\\s*[A-Z]\\s*([A-Z])\\s*([A-Z])\\s*,\\s*([\\-0-9]*)\\s*,\\s*([\\-0-9]*)\\s*,\\s*([\\-0-9]*)", Pattern.CASE_INSENSITIVE);
             sysvar_keywords[4] = Pattern.compile("^\\s*X\\s*[:=]+\\s*([\\-0-9]+)?(\\.)?([0-9]+)?\\s*[m,]*?\\s*Y\\s*[:=]+\\s*([\\-0-9]+)?(\\.)?([0-9]+)?\\s*[m,]*?\\s*Z\\s*[:=]+\\s*([\\-0-9]+)?(\\.)?([0-9]+)?\\s*", Pattern.CASE_INSENSITIVE);
             sysvar_keywords[5] = Pattern.compile("^\\s*W\\s*[:=]+\\s*([\\-0-9]+)?(\\.)?([0-9]+)?\\s*[deg,]*?\\s*P\\s*[:=]+\\s*([\\-0-9]+)?(\\.)?([0-9]+)?\\s*[deg,]*?\\s*R\\s*[:=]+\\s*([\\-0-9]+)?(\\.)?([0-9]+)?\\s*", Pattern.CASE_INSENSITIVE);
             sysvar_keywords[6] = Pattern.compile("^\\s*\\[[A-F0-9]*,([A-F0-9]*):?(.*)?\\]\\s*", Pattern.CASE_INSENSITIVE);
             sysvar_keywords[7] = Pattern.compile("^\\s*\\[\\s*([A-F0-9]*)\\s*,\\s*([A-F0-9]*)\\s*\\]\\s*=\\s*'(.*)?'\\s*Group:\\s*([0-9]*)",Pattern.CASE_INSENSITIVE);
             sysvar_keywords[8] = Pattern.compile("[E|J]1\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*[E|J]?2?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*[E|J]?3?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);             
             sysvar_keywords[9] = Pattern.compile("[E|J]4\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*[E|J]?5?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*[E|J]?6?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);                     
             sysvar_keywords[10] = Pattern.compile("[Config|CONFIG]\\s*:\\s*'\\s*([A-Z])\\s*[A-Z]?\\s*([A-Z])\\s*([A-Z])\\s*,\\s*([\\-0-9]*)\\s*,\\s*([\\-0-9]*)\\s*,\\s*([\\-0-9]*)", Pattern.CASE_INSENSITIVE);
        }

    	public static void main(String [] parameters) throws SAXException, ParserConfigurationException, TransformerConfigurationException, NumberFormatException {

               //Create the object from the class 
                String [] parameterData = parameters[2].split("\\t");
                String [] profileNumbers = parameters[3].split("\\t");
                String [] axisTypes = parameters[4].split("\\t");
                String [] numOfAxis = parameters[5].split("\\t");
                String [] mountedGunName = parameters[6].split("\\t");
                String [] toolProfileNames = parameters[7].split("\\t");
                String [] homeNames = parameters[9].split("\\t");
                String schemaName = parameters[10];

                String [] objectProfileNames = parameters[11].split("\\t");

                String [] additionalProgramNames = null;
                if (parameters.length > 12)
                {
                    additionalProgramNames = parameters[12].split("\\t");
                }                
                
                FanucUploader uploader = new FanucUploader(parameters, profileNumbers, axisTypes, numOfAxis, mountedGunName, 
                    toolProfileNames, homeNames, objectProfileNames);

               // Convert Encoding 
               // replace with WWG method String [] convertArray = {parameters[0], uploader.m_encoding, parameters[0] + ".enc", "UTF-8"}; 
               // uploader.EncodingConverter(convertArray);
               // Get the robot program and parse it line by line
               // String originalRobotProgramName = parameters[0];
                String robotProgramName = parameters[0];
                int fargingIndex = robotProgramName.lastIndexOf(".");
                String fileExtension = "";
                if (fargingIndex < (robotProgramName.length()-1))
                {
                    fileExtension = robotProgramName.substring(fargingIndex+1);
                }
               
                if (fileExtension.equalsIgnoreCase("ls") || fileExtension.equalsIgnoreCase("pe") )
                {
                    uploader.m_progExtension = fileExtension;
                }
       
                String uploadInfoFileName = parameters[8];
                int slash_index = parameters[0].lastIndexOf('/');
                int backslash_index = parameters[0].lastIndexOf('\\');
                
                if (slash_index > backslash_index)
                    backslash_index = slash_index;
                
                if (backslash_index < 0)
                    backslash_index = 0;
                
                boolean createTool = false;
                boolean createObject = false;
                boolean createPosReg = false;
                boolean createPosRegAux = false;
                
                int lastPosRegNumber = 0;
                int lastPosRegAuxNumber = 0;
                int lastPosRegExtNumber = 0;
                int lastPosRegWorkNumber = 0;
                String currentFlipNoFlip = new String("N");
                String currentUpDown = new String("U");
                String currentFrontBack = new String("T");
                String currentTurn1 = new String("0");
                String currentTurn4 = new String("0");
                String currentTurn6 = new String("0");
                double xx = 0.0;
                double yy = 0.0;
                double zz = 0.0;
                double yaw = 0.0;
                double pitch = 0.0;
                double roll = 0.0; 
                double j1 = 0.0;
                double j2 = 0.0;
                double j3 = 0.0;
                double j4 = 0.0;
                double j5 = 0.0;
                double j6 = 0.0;
                int aux_offset = 0;
                
                String [] extended_components;
                extended_components = new String[10];  

                int toolCount = 0;
                int objectCount = 0;
                double[][] toolValues = new double[20][6];
                double[][] objectValues = new double[20][6];
                
                for (int ii = 0;ii<9;ii++)
                {
                    for (int kk=0;kk<6;kk++)
                    {
                        toolValues[ii][kk] = 0.0;
                        objectValues[ii][kk] = 0.0;
                    }
                }
                
                 // create all zero UFRAME_NUM = 0
                 double [] objectFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                 objectFrameValues[0] = 0.0;
                 objectFrameValues[1] = 0.0;
                 objectFrameValues[2] = 0.0;
                 objectFrameValues[3] = 0.0;
                 objectFrameValues[4] = 0.0;
                 objectFrameValues[5] = 0.0;            
                 
                 DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE;
                
                 if (uploader.m_intAxes.equals("1") || uploader.m_intAxes.equalsIgnoreCase("true")) {
                    objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
                 }
                 else {
                    objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE;
                 }
                 
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "FINE", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED, false, 0.0); 
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "100%", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT, 100.0, 100.0, 100.0, 100.0);                 
                uploader.createObjectFrameProfile( uploader.m_objectProfileListElement, "UFRAME_NUM = 0", objFrameType, objectFrameValues);                
                              
               if (!uploader.m_mountedToolName.equals(""))
                   mountedGunName[0] = uploader.m_mountedToolName;
               
                // look for sysvars.ls file and set m_sysvarFound global 
                String robotSysvarNameCheck = parameters[0].substring( 0, backslash_index+1) + "sysvars.ls";
                BufferedReader sysvarin = null;
                try {
                    sysvarin = new BufferedReader(new FileReader(robotSysvarNameCheck));
                }
                catch (IOException e) { 
                    e.getMessage();
                }                            
                if (sysvarin == null)
                {
                    robotSysvarNameCheck = parameters[0].substring( 0, backslash_index+1) + "sysvarsoutput.ls";
                    try {
                        sysvarin = new BufferedReader(new FileReader(robotSysvarNameCheck));
                    }
                    catch (IOException e) { 
                        e.getMessage();
                    }
                }
                if (sysvarin != null)
                {
                    try {
                        String line = sysvarin.readLine();
                        while (line != null) {
                            if (line.equals("")) {
                                line = sysvarin.readLine();
                                continue;
                            }
                               for(int ii = 0; ii < uploader.sysvar_keywords.length; ii++) {
                                Matcher match = uploader.sysvar_keywords[ii].matcher(line);

                                if(match.find() == true) {
                                    //Call appropriate methods to handle the input
                                    switch(ii) {
                                        case 0:  
                                                createTool = false;
                                                createObject = true;
                                                createPosReg = false;
                                                createPosRegAux = false;
                                        break;
                                        case 1: 
                                                createTool = true;
                                                createObject = false;
                                                createPosReg = false;
                                                createPosRegAux = false;
                                        break;
                                        case 2:
                                                createPosReg = true;
                                                createPosRegAux = false;
                                                createTool = false;
                                                createObject = false;
                                        break;
                                        case 3:
                                            if (createPosReg == true)
                                            {
                                                uploader.m_currentPosRegNumber = match.group(1);
                                                String posComment = match.group(2);
                                                if (posComment != null)
                                                {
                                                	String tpName = uploader.m_TagPrefix + "PR[" + uploader.m_currentPosRegNumber + "]"  + uploader.m_TagSuffix;
                                                	uploader.m_tagComments.put(tpName, posComment);
                                                }	
                                                currentFlipNoFlip = match.group(3);
                                                currentUpDown = match.group(4);
                                                currentFrontBack = match.group(5);
                                                currentTurn1 = match.group(6);
                                                currentTurn4 = match.group(7);
                                                currentTurn6 = match.group(8);
                                            }
                                            else if (createTool == true)
                                            {
                                                String toolNumber = match.group(1);
                                                if (toolNumber != null && toolNumber.length() > 0 )
                                                {
                                                     if (toolNumber.equals("A") == true)
                                                     {
                                                         toolNumber = "10";
                                                     }
                                                     uploader.m_currentToolNumber = toolNumber;   
                                                }
                                                String toolType = match.group(2);
                                                if (toolType != null && toolType.length() > 0)
                                                {
                                                	 if (toolType == "Stationary")
                                                	 {
                                                		 uploader.m_oldStyleStationary = true;
                                                	 }
                                                     uploader.m_currentToolType = toolType;   
                                                }                                                
                                            }
                                            else
                                            {
                                                String objectNumber = match.group(1);
                                                String applyOffsets = match.group(2);
                                                if (objectNumber != null && objectNumber.length() > 0)
                                                {
                                                     uploader.m_currentObjectNumber = objectNumber;
                                                     if (applyOffsets != null && applyOffsets.equals("ApplyOffsetToTags"))
                                                     {
                                                    	 uploader.m_objectApplyOffsetsMapping.put(objectNumber, "On");
                                                     }
                                                     else
                                                     {
                                                    	 uploader.m_objectApplyOffsetsMapping.put(objectNumber, "Off");
                                                     }
                                                }                                            
                                                uploader.m_currentToolType = null;
                                            }                                      

                                        break;
                                        case 4:
                                            String fullValue = match.group(1);
                                            String dot = match.group(2);
                                            String decimalValue = match.group(3);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            String value = fullValue + dot + decimalValue;
                                            xx = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(4);
                                            dot = match.group(5);
                                            decimalValue = match.group(6);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            yy = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(7);
                                            dot = match.group(8);
                                            decimalValue = match.group(9);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            zz = Double.valueOf(value).doubleValue();                                        
                                        break;
                                        case 5:
                                            fullValue = match.group(1);
                                            dot = match.group(2);
                                            decimalValue = match.group(3);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            yaw = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(4);
                                            dot = match.group(5);
                                            decimalValue = match.group(6);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            pitch = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(7);
                                            dot = match.group(8);
                                            decimalValue = match.group(9);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            roll = Double.valueOf(value).doubleValue(); 
                                            int kk;
                                            if (createPosReg)
                                            {
                                                String []  target_components = new String [13];
                                                // add dummy targets if we skipped an index....otherwise...crash
                                                for(int jj = lastPosRegNumber; jj < Integer.parseInt(uploader.m_currentPosRegNumber)-1; jj++) {
                                                      uploader.m_listOfPosRegisters.add(jj,target_components);
                                                }

                                                target_components[0] = currentFlipNoFlip;
                                                target_components[1] = currentUpDown;
                                                target_components[2] = currentFrontBack;
                                                target_components[3] = currentTurn1;
                                                target_components[4] = currentTurn4;
                                                target_components[5] = currentTurn6;
                                                target_components[6] = String.valueOf(xx);
                                                target_components[7] = String.valueOf(yy);
                                                target_components[8] = String.valueOf(zz);
                                                target_components[9] = String.valueOf(yaw);
                                                target_components[10] = String.valueOf(pitch);
                                                target_components[11] = String.valueOf(roll);

                                                //Add the target to a dynamic array
                                                uploader.m_listOfPosRegisters.add(Integer.parseInt(uploader.m_currentPosRegNumber)-1,target_components);
                                                //Resize the array
                                                uploader.m_listOfPosRegisters.trimToSize();
                                                lastPosRegNumber = Integer.parseInt(uploader.m_currentPosRegNumber);
                                                uploader.m_targetType = CARTESIAN;
                                                //Connect the array with the target type through hashtable
                                                uploader.m_posRegTargetTypes.put(uploader.m_currentPosRegNumber,new Integer(uploader.m_targetType));
                                                createPosRegAux = true;                                           
                                            }
                                            else if (createTool == true)
                                            {
                                            	if (Integer.parseInt(uploader.m_currentToolNumber) > Integer.parseInt(uploader.m_maximumToolNumber))
                                            	{
                                            		uploader.m_maximumToolNumber = uploader.m_currentToolNumber;
                                            	}
                                                double [] toolFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                                            	if (uploader.m_currentToolType != null)
                                                    uploader.m_toolTypeMapping.put(String.valueOf(Integer.parseInt(uploader.m_currentToolNumber)), uploader.m_currentToolType);                                            
                                                toolFrameValues[0] = xx;
                                                toolFrameValues[1] = yy;
                                                toolFrameValues[2] = zz;
                                                toolFrameValues[3] = yaw;
                                                toolFrameValues[4] = pitch;
                                                toolFrameValues[5] = roll;
                                                for (int mm=uploader.m_listOfToolFrameValues.size();mm < Integer.parseInt(uploader.m_currentToolNumber);mm++)
                                                {
                                                	uploader.m_listOfToolFrameValues.add(mm, toolFrameValues);
                                                }
                                                uploader.m_listOfToolFrameValues.add(Integer.parseInt(uploader.m_currentToolNumber), toolFrameValues);
                                            }
                                            else
                                            {                         	
                                                objectFrameValues[0] = xx;
                                                objectFrameValues[1] = yy;
                                                objectFrameValues[2] = zz;
                                                objectFrameValues[3] = yaw;
                                                objectFrameValues[4] = pitch;
                                                objectFrameValues[5] = roll;
                                                for (int mm=uploader.m_listOfToolFrameValues.size();mm < Integer.parseInt(uploader.m_currentToolNumber);mm++)
                                                {
                                                	uploader.m_listOfObjectFrameValues.add(mm, objectFrameValues);
                                                }                                                
                                                uploader.m_listOfObjectFrameValues.add(Integer.parseInt(uploader.m_currentToolNumber), objectFrameValues);                                            	
                                            }

                                        break;
                                        case 6:
                                            if (createPosReg == true)
                                            {
                                            }
                                            else if (createTool == true)
                                                {
                                                    String toolNumber = match.group(1);
                                                    if (toolNumber != null && toolNumber.length() > 0)
                                                    {
                                                         if (toolNumber.equals("A") == true)
                                                         {
                                                             toolNumber = "10";
                                                         }
                                                         uploader.m_currentToolNumber = toolNumber;   
                                                    }
                                                    String toolType = match.group(2);
                                                    if (toolType != null && toolType.length() > 0)
                                                    {
	                                                   	 if (toolType == "Stationary")
	                                                	 {
	                                                		 uploader.m_oldStyleStationary = true;
	                                                	 }
                                                    	
                                                         uploader.m_currentToolType = toolType;   
                                                    }                                                
                                                }
                                                else
                                                {
                                                    String objectNumber = match.group(1);
                                                    String applyOffsets = match.group(2);
                                                    if (objectNumber != null && objectNumber.length() > 0)
                                                    {
                                                         uploader.m_currentObjectNumber = objectNumber;
                                                         if (applyOffsets != null && applyOffsets.equals("ApplyOffsetToTags"))
                                                         {
                                                        	 uploader.m_objectApplyOffsetsMapping.put(objectNumber, "On");
                                                         }
                                                         else
                                                         {
                                                        	 uploader.m_objectApplyOffsetsMapping.put(objectNumber, "Off");
                                                         }
                                                         
                                                    }                                            
                                                    uploader.m_currentToolType = null;
                                                }
                                            break;
                                            // Group
                                        case 7:
                                            if (createPosReg == true)
                                            {
                                               String posRegIndexStr = match.group(1);
                                               int posRegIndex = 0;
                                               if (posRegIndexStr != null)
                                               {
                                                   posRegIndex = Integer.parseInt(posRegIndexStr);
                                                   if (posRegIndex == 2)
                                                   {
                                                       createPosRegAux = true;
                                                   }
                                                   else
                                                   {
                                                       createPosRegAux = false;
                                                   }
                                               }
                                               uploader.m_currentPosRegNumber = match.group(2);
                                               String posComment = match.group(3);
                                               if (posComment != null)
                                               {
                                               	String tpName = uploader.m_TagPrefix + "PR[" + uploader.m_currentPosRegNumber + "]"  + uploader.m_TagSuffix;
                                               	uploader.m_tagComments.put(tpName, posComment);
                                               }	
                                                
                                               uploader.m_currentPosRegGroup = match.group(4);
                                            }   
                                            break;
                                            // J1-J3
                                        case 8:
                                            value = match.group(1);
                                         
                                            if (value != null && !value.equals(""))
                                            {
                                                j1 = Double.valueOf(value).doubleValue();
                                            }
                                            else
                                            {
                                                j1 = 0.0;
                                            }
                                            value = match.group(3);
                                            if (value != null && !value.equals(""))
                                            {
                                                j2 = Double.valueOf(value).doubleValue();
                                            }
                                            else
                                            {
                                                j2 = 0.0;
                                            }
                                            
                                            value = match.group(5);
                                            if (value != null && !value.equals(""))
                                            {
                                                j3 = Double.valueOf(value).doubleValue();         
                                            }
                                            else
                                            {
                                                j3 = 0.0;
                                            }
                                            
                                           if (createPosRegAux == true)
                                           {
                                                extended_components = new String[10];     
                                                for (int ll=0;ll<10;ll++)
                                                {
                                                    extended_components[ll] = "";
                                                }                                                                   
                                                uploader.m_extAxesCount = 0;
                                                uploader.m_auxAxesCount = 0;
                                                uploader.m_workAxesCount = 0;                                               
                                                aux_offset = uploader.m_extAxesCount + uploader.m_auxAxesCount + uploader.m_workAxesCount;
 
                                                extended_components[aux_offset] = match.group(1);
                                                extended_components[1+aux_offset] = match.group(3);
                                                extended_components[2+aux_offset] = match.group(5);

                                               if (uploader.m_currentPosRegGroup != null)
                                               {
                                                   int currPosRegNum = Integer.parseInt(uploader.m_currentPosRegNumber);
                                                   int currPosRegGroup = Integer.parseInt(uploader.m_currentPosRegGroup);

                                                    int group_num = Integer.parseInt(uploader.m_currentPosRegGroup);
                                                    if (group_num > 0)
                                                    {
                                                        if (group_num == uploader.m_auxAxesGroupNumber)
                                                        {
                                                            // add dummy targets if we skipped an index....otherwise...crash
                                                            for(int jj = lastPosRegAuxNumber; jj < currPosRegNum-1; jj++) {
                                                                  uploader.m_listOfPosRegAuxTargets.add(jj,extended_components);
                                                            }                   
                                                            //Add the target to a dynamic array
                                                            uploader.m_listOfPosRegAuxTargets.add(currPosRegNum-1, extended_components);
                                                            //Resize the array
                                                            uploader.m_listOfPosRegAuxTargets.trimToSize();
                                                            lastPosRegAuxNumber = currPosRegNum;
                                                            for (kk=0;kk<3;kk++) {
                                                                if (extended_components[kk+aux_offset] != null && extended_components[kk+aux_offset].equals("") != true) 
                                                                {   
                                                                     uploader.m_auxAxesCount++;
                                                                }
                                                            }                                                                                                                                    
                                                        }
                                                         if (group_num == uploader.m_extAxesGroupNumber)
                                                        {
                                                            // add dummy targets if we skipped an index....otherwise...crash
                                                            for(int jj = lastPosRegExtNumber; jj < currPosRegNum-1; jj++) {
                                                                  uploader.m_listOfPosRegExtTargets.add(jj,extended_components);
                                                            }                   
                                                            //Add the target to a dynamic array
                                                            uploader.m_listOfPosRegExtTargets.add(currPosRegNum-1, extended_components);
                                                            //Resize the array
                                                            uploader.m_listOfPosRegExtTargets.trimToSize();
                                                            lastPosRegExtNumber = currPosRegNum;
                                                            for (kk=0;kk<3;kk++) {
                                                                if (extended_components[kk+aux_offset] != null && extended_components[kk+aux_offset].equals("") != true) 
                                                                {   
                                                                     uploader.m_extAxesCount++;
                                                                }
                                                            }                                                                                                           
                                                        }                    
                                                         if (group_num == uploader.m_workAxesGroupNumber)
                                                        {
                                                            // add dummy targets if we skipped an index....otherwise...crash
                                                            for(int jj = lastPosRegWorkNumber; jj < currPosRegNum-1; jj++) {
                                                                  uploader.m_listOfPosRegWorkTargets.add(jj,extended_components);
                                                            }                   
                                                            //Add the target to a dynamic array
                                                            uploader.m_listOfPosRegWorkTargets.add(currPosRegNum-1, extended_components);
                                                            //Resize the array
                                                            uploader.m_listOfPosRegWorkTargets.trimToSize();
                                                            lastPosRegWorkNumber = currPosRegNum;
                                                            for (kk=0;kk<3;kk++) {
                                                                if (extended_components[kk+aux_offset] != null && extended_components[kk+aux_offset].equals("") != true) 
                                                                {   
                                                                     uploader.m_workAxesCount++;
                                                                }
                                                            }                                                                                                           
                                                        }                                                                            
                                                    }
                                               }
                                           }                                                                                        
                                            break;
                                            //J4-J6
                                        case 9:
                                            value = match.group(1);
                                            boolean allAxes = true;
                                            
                                            if (value != null && !value.equals(""))
                                            {
                                                j4 = Double.valueOf(value).doubleValue();
                                            }
                                            else
                                            {
                                                allAxes = false;
                                                j4 = 0.0;
                                            }                                         
                                            
                                            value = match.group(3);
                                            
                                            if (value != null && !value.equals(""))
                                            {
                                                j5 = Double.valueOf(value).doubleValue();
                                            }
                                            else
                                            {
                                                allAxes = false;
                                                j5 = 0.0;
                                            }
                                         
                                            value = match.group(5);
                                            if (value != null && !value.equals(""))
                                            {
                                                j6 = Double.valueOf(value).doubleValue();
                                            }
                                            else
                                            {
                                                allAxes = false;
                                                j6 = 0.0;
                                            }                                          
 
                                           if (createPosRegAux == true)
                                           {                                      
                                                aux_offset = uploader.m_extAxesCount + uploader.m_auxAxesCount + uploader.m_workAxesCount;

                                                extended_components[aux_offset] = match.group(1);
                                                extended_components[1+aux_offset] = match.group(3);
                                                extended_components[2+aux_offset] = match.group(5);

                                               if (uploader.m_currentPosRegGroup != null)
                                               {
                                                   int currPosRegNum = Integer.parseInt(uploader.m_currentPosRegNumber);
                                                   int currPosRegGroup = Integer.parseInt(uploader.m_currentPosRegGroup);

                                                    int group_num = Integer.parseInt(uploader.m_currentPosRegGroup);
                                                    if (group_num > 0)
                                                    {
                                                        if (group_num == uploader.m_auxAxesGroupNumber)
                                                        {

                                                            //Set the target to a dynamic array
                                                            uploader.m_listOfPosRegAuxTargets.set(currPosRegNum-1, extended_components);
                                                            //Resize the array
                                                            uploader.m_listOfPosRegAuxTargets.trimToSize();
                                                            for (kk=0;kk<3;kk++) {
                                                                if (extended_components[kk+aux_offset] != null && extended_components[kk+aux_offset].equals("") != true) 
                                                                {   
                                                                     uploader.m_auxAxesCount++;
                                                                }
                                                            }                                                                                                                                    
                                                        }
                                                         if (group_num == uploader.m_extAxesGroupNumber)
                                                        {
 
                                                            //Set the target to a dynamic array
                                                            uploader.m_listOfPosRegExtTargets.set(currPosRegNum-1, extended_components);
                                                            //Resize the array
                                                            uploader.m_listOfPosRegExtTargets.trimToSize();
                                                            for (kk=0;kk<3;kk++) {
                                                                if (extended_components[kk+aux_offset] != null && extended_components[kk+aux_offset].equals("") != true) 
                                                                {   
                                                                     uploader.m_extAxesCount++;
                                                                }
                                                            }                                                                                                           
                                                        }                    
                                                         if (group_num == uploader.m_workAxesGroupNumber)
                                                        {

                                                            //Set the target to a dynamic array
                                                            uploader.m_listOfPosRegWorkTargets.set(currPosRegNum-1, extended_components);
                                                            //Resize the array
                                                            uploader.m_listOfPosRegWorkTargets.trimToSize();
                                                            for (kk=0;kk<3;kk++) {
                                                                if (extended_components[kk+aux_offset] != null && extended_components[kk+aux_offset].equals("") != true) 
                                                                {   
                                                                     uploader.m_workAxesCount++;
                                                                }
                                                            }                                                                                                           
                                                        }                                                                            
                                                    }
                                               }
                                           }                                                                              
                                           else if (createPosReg && allAxes == true)
                                            {
                                                String []  target_components = new String [13];
                                                // add dummy targets if we skipped an index....otherwise...crash
                                                for(int jj = lastPosRegNumber; jj < Integer.parseInt(uploader.m_currentPosRegNumber)-1; jj++) {
                                                      uploader.m_listOfPosRegisters.add(jj,target_components);
                                                }

                                                target_components[0] = currentFlipNoFlip;
                                                target_components[1] = currentUpDown;
                                                target_components[2] = currentFrontBack;
                                                target_components[3] = currentTurn1;
                                                target_components[4] = currentTurn4;
                                                target_components[5] = currentTurn6;
                                                target_components[6] = String.valueOf(j1);
                                                target_components[7] = String.valueOf(j2);
                                                target_components[8] = String.valueOf(j3);
                                                target_components[9] = String.valueOf(j4);
                                                target_components[10] = String.valueOf(j5);
                                                target_components[11] = String.valueOf(j6);

                                                //Add the target to a dynamic array
                                                uploader.m_listOfPosRegisters.add(Integer.parseInt(uploader.m_currentPosRegNumber)-1,target_components);
                                                //Resize the array
                                                uploader.m_listOfPosRegisters.trimToSize();
                                                lastPosRegNumber = Integer.parseInt(uploader.m_currentPosRegNumber);
                                                
                                                uploader.m_targetType = JOINT;
                                                //Connect the array with the target type through hashtable
                                                uploader.m_posRegTargetTypes.put(uploader.m_currentPosRegNumber,new Integer(uploader.m_targetType));
                                            }                                            
                                            break;
                                        case 10:
                                            if (createPosReg == true)
                                            {
                                                currentFlipNoFlip = match.group(1);
                                                currentUpDown = match.group(2);
                                                currentFrontBack = match.group(3);
                                                currentTurn1 = match.group(4);
                                                currentTurn4 = match.group(5);
                                                currentTurn6 = match.group(6);
                                            }
                                            break;
                                    } // switch  
                               } // if
                               }  // for
                             line = sysvarin.readLine();
                        } // while                   
                        uploader.m_sysvarFound = true;
                        sysvarin.close();
                        sysvarin = null;
                    }
                     catch (IOException e) { 
                        e.getMessage();
                    }
                } // if sysvarin != null
                
                createTool = false;
                createObject = false;
                createPosReg = false;
                if (additionalProgramNames != null)
                {                
                    for (int jj = 0; jj<additionalProgramNames.length;jj++)
                    {
                        int index = uploader.m_listOfCalledPrograms.indexOf(additionalProgramNames[jj]);
                        if (index < 0) {
                            uploader.m_listOfCalledPrograms.add(uploader.m_calledProgramCounter, additionalProgramNames[jj] );
                            uploader.m_listOfCalledPrograms.trimToSize(); 
                            uploader.m_calledProgramCounter++;
                        }
                    }
                }
                
                String mainProgram = new String(robotProgramName);
                uploader.processRobotProgram( robotProgramName, uploadInfoFileName, mountedGunName[0] ); 
                for (int ii = 0; ii<uploader.m_calledProgramCounter; ii++) {
                   // originalRobotProgramName = (String)uploader.m_listOfCalledPrograms.get(ii);
                    robotProgramName = (String)uploader.m_listOfCalledPrograms.get(ii);
                    // uploader.m_TagSuffix = "_" + String.valueOf(ii+1);
                    // replace with WWG method Convert Encoding
                   // String [] convertArray2 = {originalRobotProgramName, uploader.m_encoding, robotProgramName, "UTF-8"}; 
                   // uploader.EncodingConverter(convertArray2);
                   if (!robotProgramName.equals(mainProgram))
                   {
                      // System.out.println("PROCESS PROGRAM " + robotProgramName + " NOT EQUAL TO " + mainProgram + "\n");
                      uploader.processRobotProgram( robotProgramName, uploadInfoFileName, mountedGunName[0] );
                   }
                }
                
                String saveToolNumber = uploader.m_currentToolNumber;
                String saveObjectNumber = uploader.m_currentObjectNumber;
                
                // look for sysvars.ls file and if not found look for sysvarsoutput.ls file 
                robotSysvarNameCheck = parameters[0].substring( 0, backslash_index+1) + "sysvars.ls";
                try {
                    sysvarin = new BufferedReader(new FileReader(robotSysvarNameCheck));
                }
                catch (IOException e) { 
                    e.getMessage();
                }                            
                if (sysvarin == null)
                {
                    robotSysvarNameCheck = parameters[0].substring( 0, backslash_index+1) + "sysvarsoutput.ls";
                    try {
                        sysvarin = new BufferedReader(new FileReader(robotSysvarNameCheck));
                    }
                    catch (IOException e) { 
                        e.getMessage();
                    }
                }
                
                if (sysvarin != null)
                {
                    try {
                   
                        String line = sysvarin.readLine();
                        while (line != null) {
                            if (line.equals("")) {
                                line = sysvarin.readLine();
                                continue;
                            }
                               for(int ii = 0; ii < uploader.sysvar_keywords.length; ii++) {
                                Matcher match = uploader.sysvar_keywords[ii].matcher(line);

                                if(match.find() == true) {
                                    //Call appropriate methods to handle the input
                                    switch(ii) {
                                        case 0:  
                                                createTool = false;
                                                createObject = true;
                                                createPosReg = false;
                                        break;
                                        case 1: 
                                                createTool = true;
                                                createObject = false;
                                                createPosReg = false;
                                        break;
                                        case 2:
                                                createPosReg = true;
                                                createTool = false;
                                                createObject = false;
                                        break;
                                        case 3:
                                            if (createPosReg == true)
                                            {
                                                uploader.m_currentPosRegNumber = match.group(1);
                                                currentFlipNoFlip = match.group(3);
                                                currentUpDown = match.group(4);
                                                currentFrontBack = match.group(5);
                                                currentTurn1 = match.group(6);
                                                currentTurn4 = match.group(7);
                                                currentTurn6 = match.group(8);
                                            }
                                            else if (createTool == true)
                                            {
                                                String toolNumber = match.group(1);
                                                if (toolNumber != null && toolNumber.length() > 0 )
                                                {
                                                     if (toolNumber.equals("A") == true)
                                                     {
                                                         toolNumber = "10";
                                                     }
                                                     uploader.m_currentToolNumber = toolNumber;   
                                                }
                                                String toolType = match.group(2);
                                                if (toolType != null && toolType.length() > 0)
                                                {
	                                               	 if (toolType == "Stationary")
	                                            	 {
	                                            		 uploader.m_oldStyleStationary = true;
	                                            	 }
                                                	
                                                     uploader.m_currentToolType = toolType;   
                                                }                                                
                                            }
                                            else
                                            {
                                                String objectNumber = match.group(1);
                                                if (objectNumber != null && objectNumber.length() > 0)
                                                {
                                                     uploader.m_currentObjectNumber = objectNumber;   
                                                }                                            
                                                uploader.m_currentToolType = null;
                                            }                                      

                                        break;
                                        case 4:
                                            String fullValue = match.group(1);
                                            String dot = match.group(2);
                                            String decimalValue = match.group(3);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            String value = fullValue + dot + decimalValue;
                                            xx = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(4);
                                            dot = match.group(5);
                                            decimalValue = match.group(6);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            yy = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(7);
                                            dot = match.group(8);
                                            decimalValue = match.group(9);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            zz = Double.valueOf(value).doubleValue();                                        
                                        break;
                                        case 5:
                                            fullValue = match.group(1);
                                            dot = match.group(2);
                                            decimalValue = match.group(3);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            yaw = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(4);
                                            dot = match.group(5);
                                            decimalValue = match.group(6);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            pitch = Double.valueOf(value).doubleValue();
                                            fullValue = match.group(7);
                                            dot = match.group(8);
                                            decimalValue = match.group(9);
                                            fullValue = (fullValue == null)?"":fullValue;
                                            dot = (dot == null)?"":dot;
                                            decimalValue = (decimalValue == null)?"":decimalValue;
                                            value = fullValue + dot + decimalValue;
                                            roll = Double.valueOf(value).doubleValue(); 
                                            int kk;
                                            if (createTool == true)
                                            {
                                                if (Integer.valueOf(uploader.m_currentToolNumber).intValue() > uploader.m_toolNumberMapping.size()) {

                                                    for (kk = 0;kk<toolCount;kk++)
                                                    {
                                                        double result = Math.abs(xx-toolValues[kk][0]) +
                                                                     Math.abs(yy-toolValues[kk][1]) +
                                                                     Math.abs(zz-toolValues[kk][2]) +                  
                                                                     Math.abs(yaw-toolValues[kk][3]) +
                                                                     Math.abs(pitch-toolValues[kk][4]) +                                                             
                                                                     Math.abs(roll-toolValues[kk][5]);                                                             
                                                         if (result < ZERO_TOL)
                                                             break;
                                                    }

                                                    if (kk == toolCount)
                                                    {
                                                        toolValues[toolCount][0] = xx;
                                                        toolValues[toolCount][1] = yy;
                                                        toolValues[toolCount][2] = zz;
                                                        toolValues[toolCount][3] = yaw;
                                                        toolValues[toolCount][4] = pitch;
                                                        toolValues[toolCount][5] = roll;
                                                        toolCount++;
                                                        uploader.processToolFrameStatement( xx, yy, zz, yaw, pitch, roll, uploader.m_toolProfileListElement);
                                                    }
                                                }
                                                else {
                                                    uploader.processToolFrameStatement( xx, yy, zz, yaw, pitch, roll, uploader.m_toolProfileListElement);
                                                    toolValues[toolCount][0] = xx;
                                                    toolValues[toolCount][1] = yy;
                                                    toolValues[toolCount][2] = zz;
                                                    toolValues[toolCount][3] = yaw;
                                                    toolValues[toolCount][4] = pitch;
                                                    toolValues[toolCount][5] = roll;
                                                    toolCount++;                                          
                                                }
                                            }
                                            else if (createObject == true)
                                            {
                                                if (Integer.valueOf(uploader.m_currentObjectNumber).intValue() > uploader.m_objectNumberMapping.size()) {

                                                    for (kk = 0;kk<objectCount;kk++)
                                                    {
                                                        double result = Math.abs(xx-objectValues[kk][0])  +
                                                                     Math.abs(yy-objectValues[kk][1]) +
                                                                     Math.abs(zz-objectValues[kk][2]) +                  
                                                                     Math.abs(yaw-objectValues[kk][3]) +
                                                                     Math.abs(pitch-objectValues[kk][4]) +                                                             
                                                                     Math.abs(roll-objectValues[kk][5]);                                                             
                                                         if (result < ZERO_TOL)
                                                             break;
                                                    }
                                                    if (kk == objectCount)
                                                    {
                                                        objectValues[objectCount][0] = xx;
                                                        objectValues[objectCount][1] = yy;
                                                        objectValues[objectCount][2] = zz;
                                                        objectValues[objectCount][3] = yaw;
                                                        objectValues[objectCount][4] = pitch;
                                                        objectValues[objectCount][5] = roll;
                                                        objectCount++;
                                                        uploader.processObjectFrameStatement( xx, yy, zz, yaw, pitch, roll, uploader.m_objectProfileListElement);                                                
                                                    }
                                                }
                                                else {
                                                    uploader.processObjectFrameStatement( xx, yy, zz, yaw, pitch, roll, uploader.m_objectProfileListElement);
                                                    objectValues[objectCount][0] = xx;
                                                    objectValues[objectCount][1] = yy;
                                                    objectValues[objectCount][2] = zz;
                                                    objectValues[objectCount][3] = yaw;
                                                    objectValues[objectCount][4] = pitch;
                                                    objectValues[objectCount][5] = roll;
                                                    objectCount++;                                            
                                                } 
                                            }

                                        break;
                                        case 6:
                                            if (createPosReg == true)
                                            {
                                            }
                                            else if (createTool == true)
                                                {
                                                    String toolNumber = match.group(1);
                                                    if (toolNumber != null && toolNumber.length() > 0)
                                                    {
                                                         if (toolNumber.equals("A") == true)
                                                         {
                                                             toolNumber = "10";
                                                         }
                                                         uploader.m_currentToolNumber = toolNumber;   
                                                    }
                                                    String toolType = match.group(2);
                                                    if (toolType != null && toolType.length() > 0)
                                                    {
	                                                   	 if (toolType == "Stationary")
	                                                	 {
	                                                		 uploader.m_oldStyleStationary = true;
	                                                	 }
                                                    	
                                                         uploader.m_currentToolType = toolType;   
                                                    }                                                
                                                }
                                                else
                                                {
                                                    String objectNumber = match.group(1);
                                                    if (objectNumber != null && objectNumber.length() > 0)
                                                    {
                                                         uploader.m_currentObjectNumber = objectNumber;   
                                                    }                                            
                                                    uploader.m_currentToolType = null;
                                                }

                                    } // switch  
                               } // if
                               }  // for
                             line = sysvarin.readLine();
                        } // while

                    } // try
                    catch (IOException e) { 
                        e.getMessage();
                    }
                } // sysvarin != null

                for (int ii=0;ii<20;ii++)
                {
                    if (uploader.m_utoolsUsed[ii] == true && uploader.m_utoolsCreated[ii] == false)
                    {
                         double [] toolVals = {0.0,0.0,0.0,0.0,0.0,0.0};
                         Double massValue = Double.valueOf("0.0");
                         double [] cogValues = {0.0, 0.0, 0.0};
                         double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};

                         toolVals[0] = 0.0;
                         toolVals[1] = 0.0;
                         toolVals[2] = 0.0;
                         toolVals[3] = 0.0;
                         toolVals[4] = 0.0;
                         toolVals[5] = 0.0;

                         cogValues[0] = 0.0;
                         cogValues[1] = 0.0;
                         cogValues[2] = 0.0;

                         inertiaValues[0] = 0.0;
                         inertiaValues[2] = 0.0;
                         inertiaValues[5] = 0.0;
                         String toolProfileName = "UTOOL_NUM = " + String.valueOf(ii+1);

                         uploader.createToolProfile( uploader.m_toolProfileListElement, toolProfileName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolVals, massValue, cogValues, inertiaValues); 
                         
                    }
                    if (uploader.m_uframesUsed[ii] == true && uploader.m_uframesCreated[ii] == false)
                    {
                         // create all zero UFRAME_NUM = 0
                         double [] objectFrameVals = {0.0,0.0,0.0,0.0,0.0,0.0};
                         objectFrameVals[0] = 0.0;
                         objectFrameVals[1] = 0.0;
                         objectFrameVals[2] = 0.0;
                         objectFrameVals[3] = 0.0;
                         objectFrameVals[4] = 0.0;
                         objectFrameVals[5] = 0.0;            

                         if (uploader.m_intAxes.equals("1") || uploader.m_intAxes.equalsIgnoreCase("true")) {
                            objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
                         }
                         else {
                            objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE;
                         }

                         String objFrameProfileName = "UFRAME_NUM = " + String.valueOf(ii+1);
                         uploader.createObjectFrameProfile( uploader.m_objectProfileListElement, objFrameProfileName, objFrameType, objectFrameVals);
                    }
                }
               uploader.m_currentToolNumber = saveToolNumber;
               uploader.m_currentObjectNumber = saveObjectNumber;              
                
                try {
                    uploader.writeXMLStreamToFile();
                }
                catch (TransformerException e) {
                    e.getMessage();
                }
                catch (IOException e) {
                    e.getMessage();
                }
               
        }

    private void processRobotProgram( String robotProgramName, String uploadInfoFileName, String mountedGunName)
    {
        Element activityListElem = null;
        
            try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), m_encoding));
                    String line = in.readLine();

                    while(line != null) {

                        if(line.equals("")) {
                            line = in.readLine();
                            continue;

                        }

                        Matcher match = m_keywords[1].matcher(line);
                            
                            if(match.find() == true) {
                                 processPosStatement(match,line, in);
                            }

                        line = in.readLine();
                    }
                    in.close();
                }

                catch (IOException e) {
                    e.getMessage();
                }
      
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), m_encoding));
                    BufferedWriter bw = null;
                    boolean handle_attr = false;
                    boolean handle_appl = false;
                   
                    if (m_firstProgram == true) {
                        bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));    
                    }
                    else {
                        bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
                    }
                    m_bw = bw;

                    m_firstProgram = false;

                    bw.write(VERSION);
                    bw.write(COPYRIGHT);
                    bw.write("\nStart of java parsing.\n\n");
                    bw.write("Unparsed entities from the file: \"");
                    bw.write(robotProgramName);
                    bw.write("\":\n\n");
           
                    String line = in.readLine();
                    int lineNumber = 1;
                    boolean unparsedEntitiesExist = false;
                    
                    StringBuffer unparsedStatement = new StringBuffer();
                    String unparsedString = new String();
                    String motOpString = new String();
                    int motOpIndex = 0;
                    int semiColonIndex = 0;
                   
                    while(line != null) {

                        if(line.equals("")) {
                            line = in.readLine();
                            continue;
                        }
                        
                        unparsedStatement.append("Line ").append(lineNumber).append(": ").append(line).append("\n");
                        unparsedString = unparsedStatement.toString();
                        boolean lineHandled = false;
                        
                        for(int ii = 0; ii < m_keywords.length; ii++) {
                            Matcher match = m_keywords[ii].matcher(line);
                            
                            if(match.find() == true) {
                                if (ii != 17)
                                    lineHandled = true;
                                //Call appropriate methods to handle the input
                                switch(ii) {
                                    case 0: m_currentToolGroup = match.group(1);
                                    	    m_currentToolNumber = match.group(2);  
                                            if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
                                                addToComments(activityListElem, line, false, true);
                                            else
                                                addToComments(null, line, true, true);
                                    break;
                                    case 1: // POS statements handled above 
                                                processComments(m_lastActElem, "Post");
                                    break;
                                    case 2:
                                                 String moveType = match.group(1);
                                                 if (moveType != null && moveType.equals("C")) {
                                                     m_circularViaLine = line;
                                                     continue;
                                                 }
                                                 else
                                                 {
                                                     processMOVStatement(line, activityListElem);
                                                     if (m_vsCamClbTagCount >= 3)
                                                     {
                                                         for (int jj = m_vsCamClbTagCount; jj<9;jj++)
                                                         {
                                                             //Get the target type
                                                             Integer targetType = (Integer) m_targetTypes.get(String.valueOf(lastTargetNumber));

                                                             //Get the C position target values (in joint or cartesian coordinates)
                                                             String [] targetValues = (String []) m_listOfRobotTargets.get(lastTargetNumber-1);
                                                             String [] auxValues = null;
                                                             String [] extValues = null;
                                                             String [] workValues = null;
                                                              if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {
                                                                  if(m_numAuxAxes > 0 && m_auxAxesCount == m_numAuxAxes) {
                                                                        auxValues = (String []) m_listOfAuxTargets.get(lastTargetNumber-1);
                                                                  }
                                                                  if(m_numExtAxes > 0 && m_extAxesCount == m_numExtAxes) {
                                                                        extValues = (String []) m_listOfExtTargets.get(lastTargetNumber-1);
                                                                  }
                                                                  if(m_numWorkAxes > 0 && m_workAxesCount == m_numWorkAxes) {
                                                                        workValues = (String []) m_listOfWorkTargets.get(lastTargetNumber-1);
                                                                  }
                                                           
                                                             }
                                                             
                                                             double workValue = 0.0;
                                                             double workValue2 = 0.0;
                                                             boolean bypassOutput = false;

                                                             bypassOutput = false;
                                                             
                                                             switch(m_vsCamClbTagCount)
                                                             {
                                                                 case 3:
                                                                     if (m_vsCamClb.equals("1")) {
                                                                         // rotate about Z positive 30
                                                                         workValue = Double.valueOf(targetValues[11]).doubleValue();
                                                                         workValue += 30.0;
                                                                         targetValues[11] = String.valueOf(workValue);
                                                                     }
                                                                     else {
                                                                         bypassOutput = true;
                                                                         m_vsCamClbTagCount++;
                                                                     }   
                                                                 break;
                                                                 case 4:
                                                                     if (m_vsCamClb.equals("1")) {
                                                                         // rotate about Z negative 30
                                                                         // and remove 30 from above
                                                                         workValue = Double.valueOf(targetValues[11]).doubleValue();
                                                                         workValue -= 60.0;
                                                                         targetValues[11] = String.valueOf(workValue);
                                                                     }
                                                                     else {
                                                                         bypassOutput = true;
                                                                         m_vsCamClbTagCount++;
                                                                     }                                                                     
                                                                 break;
                                                                 case 5:
                                                                     // translate along Z 50 mm
                                                                     // and remove -30 from above if FIND1
                                                                     workValue = Double.valueOf(targetValues[11]).doubleValue();
                                                                     workValue2 = Double.valueOf(targetValues[8]).doubleValue();
                                                                     workValue += 30.0;
                                                                     workValue2 += 50.0;
                                                                     if (m_vsCamClb.equals("1")) {
                                                                        targetValues[11] = String.valueOf(workValue);
                                                                     }
                                                                     targetValues[8] = String.valueOf(workValue2);
                                                                 break;
                                                                 case 6:
                                                                      // translate along Z -50 mm
                                                                     // and remove 50 from above
                                                                     workValue2 = Double.valueOf(targetValues[8]).doubleValue();
                                                                     workValue2 -=100.0;
                                                                     targetValues[8] = String.valueOf(workValue2);                                                                
                                                                 break;
                                                                 case 7:
                                                                     // rotate about X or Y 15
                                                                     // and remove -50 from above
                                                                     workValue2 = Double.valueOf(targetValues[8]).doubleValue();
                                                                     workValue2 += 50.0;
                                                                     targetValues[8] = String.valueOf(workValue2);
                                                                     if (m_visionShiftAxis.equals("Y")) {
                                                                         workValue = Double.valueOf(targetValues[10]).doubleValue();
                                                                         workValue += 15.0;
                                                                         targetValues[10] = String.valueOf(workValue);
                                                                     }
                                                                     else {
                                                                         workValue = Double.valueOf(targetValues[9]).doubleValue();
                                                                         workValue += 15.0;
                                                                         targetValues[9] = String.valueOf(workValue);
                                                                     }
                                                                 break;
                                                                 case 8:
                                                                     // rotate about X or Y -15
                                                                     // and remove 15 from above
                                                                     if (m_visionShiftAxis.equals("Y")) {
                                                                         workValue = Double.valueOf(targetValues[10]).doubleValue();
                                                                         workValue -= 30.0;
                                                                         targetValues[10] = String.valueOf(workValue);
                                                                     }
                                                                     else {
                                                                         workValue = Double.valueOf(targetValues[9]).doubleValue();
                                                                         workValue -= 30.0;
                                                                         targetValues[9] = String.valueOf(workValue);
                                                                     }                         

                                                                 break;
                                                             }
                                                             if (bypassOutput == false) {
                                                                 m_targetTypes.put(String.valueOf(lastTargetNumber+1),targetType);
                                                                 m_listOfRobotTargets.add(lastTargetNumber,targetValues);
                                                                 m_listOfRobotTargets.trimToSize();
                                                                 if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {
                                                                      if(m_numAuxAxes > 0 && m_auxAxesCount == m_numAuxAxes) {
                                                                            m_listOfAuxTargets.add(lastTargetNumber, auxValues);
                                                                            m_listOfAuxTargets.trimToSize();
                                                                      }
                                                                      if(m_numExtAxes > 0 && m_extAxesCount == m_numExtAxes) {
                                                                            m_listOfExtTargets.add(lastTargetNumber, extValues);
                                                                            m_listOfExtTargets.trimToSize();
                                                                      }
                                                                      if(m_numWorkAxes > 0 && m_workAxesCount == m_numWorkAxes) {
                                                                            m_listOfWorkTargets.add(lastTargetNumber, workValues);
                                                                            m_listOfWorkTargets.trimToSize();
                                                                      }
                                                                 }                                                         
                                                                 lastTargetNumber++;
                                                                 processMOVStatement(line, activityListElem);
                                                             }
                                                         }
                                                         m_vsCamClb = "";
                                                         m_vsCamClbTagCount = 0;
                                                     }
                                                     continue; // continue here so SPOT command on same line is handled.
                                                 }
                                    case 3:  
                                                 processMOVStatement(line, activityListElem);
                                                 if (!m_circularViaLine.equals("")) {
                                                     m_circularViaLine = "";
                                                     processMOVStatement(line, activityListElem);
                                                 }
                                                 continue; // continue here so SPOT command on same line is handled.
                                                 
                                    case 4: 
                                        processPULSEStatement(line, match, activityListElem);
                                    break;
                                    case 5: processDOUTStatement(line, match,activityListElem, "0");
                                    break;
                                    case 6:  
                                    	processWAITStatement(line, match, activityListElem);
                                    break; 
                                    case 7:  
                                                processTimerStatement(line, match, activityListElem);
                                    break;
                                    case 8:   
                                                processSpotStatement( line, activityListElem, mountedGunName );
                                    break;
                                    case 9:
                                             String localProgName = match.group(1);
                                             ProgName = localProgName.trim();
                                             activityListElem = super.createActivityList(ProgName);
                                             m_lastActElem = activityListElem;
                                    break;
                                    case 10: m_currentObjectGroup = match.group(1);
                                    	    m_currentObjectNumber = match.group(2);
                                            if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
                                                addToComments(activityListElem, line, false, true);
                                            else
                                                addToComments(null, line, true, true);
                                          //   if (m_currentObjectNumber.equals("0"))
                                          //       m_currentObjectNumber = "10";
                                    break;
                                    case 11:                               
                                            m_vsCamClb = match.group(1);
                                            continue; // so that comment or CALL can be handled
                                    case 12:                                     
                                            addToComments(activityListElem, line, false, false);
                                    break;
                                    case 13:                       
                                             handle_attr = true;
                                    break;
                                    case 14: handle_attr = false;
                                             handle_appl = true;
                                    break;
                                    case 15:
                                             handle_attr = false; 
                                             handle_appl = false;
                                       
                                               processComments(activityListElem, "Pre");
                                    break;
                                    case 16:  
                                               processBackupStatement( line, activityListElem, mountedGunName );
                                    break;
                                    case 17:
                                        if (!match.group(1).equals("VSCAMCLB")) {
                                      //     if (m_lastActivityCall == m_lastActElem)
                                      //          continue;
                                            lineHandled = true;
                                            processCallStatement( match.group(1), line, activityListElem, robotProgramName);
                                            m_lastActivityCall = m_lastActElem;
                                        }
                                        else {
 
                                                  addToComments(activityListElem, line, true, true);
                                           
                                        }
                                    break;
                                    // ArcStart
                                    case 18: m_currentArcStartIndex = match.group(1);
                                             m_currentArcEndIndex = "[0]";
                                             if (!m_currentArcStartIndex.equals("[0]"))
                                             {
                                                String oUserProfileInstanceText = "Arc Start[" + m_currentArcStartIndex;
                                                LinkedList [] llEmptyAttribs = new LinkedList[0];
                                                super.createUserProfile(super.getUserProfileListElement(), "TPEArcStart", oUserProfileInstanceText, llEmptyAttribs);
                                                addUserProfile("TPEArcStart", oUserProfileInstanceText);
                                                m_currentArcStartIndex = "[0]";
                                             }                                           
                                    break;
                                    // ArcEnd
                                    case 19: m_currentArcEndIndex = match.group(1);
                                             m_currentArcStartIndex = "[0]";
                                             if (!m_currentArcEndIndex.equals("[0]"))
                                             {  
                                                String oUserProfileInstanceText = "Arc End[" + m_currentArcEndIndex;
                                                LinkedList [] llEmptyAttribs = new LinkedList[0];
                                                super.createUserProfile(super.getUserProfileListElement(), "TPEArcEnd", oUserProfileInstanceText, llEmptyAttribs);                   
                                                addUserProfile("TPEArcEnd", oUserProfileInstanceText);
                                                m_currentArcEndIndex = "[0]";
                                             }                                                          
                                    break;
                                    // Weave
                                    case 20: 
                                            addToComments(activityListElem, "Weave"+match.group(2), true, true);
                                    break;
                                    case 22: 
                                            processSpotAttribute(m_lastActElem, "Motion Option", "ACC"+match.group(1));
                                    break;
                                    case 23:
                                            motOpString = getMotOpString("SKIP", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;
                                    case 24:
                                 
                                            motOpString = getMotOpString("OFFSET", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;  
                                    case 25:
                                 
                                            motOpString = getMotOpString("OFFSET", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;
                                            
                                    case 26:
                                          
                                            motOpString = getMotOpString("INC", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;  
                                            
                                    case 27:
                                           
                                            motOpString = getMotOpString("TOOL_OFFSET", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;
                                                                                                                                    
                                    case 28:
                                         
                                            motOpString = getMotOpString("TOOL_OFFSET", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;  
                                    case 29:
                                       
                                            motOpString = getMotOpString("TB", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;  
                                    case 30:
                                          
                                            motOpString = getMotOpString("TA", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;  
                                    case 31:
                                          
                                            motOpString = getMotOpString("PRESS_MOTION", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;  
                                    case 32:
                                         
                                            motOpString = getMotOpString("DB", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;  
                                    case 33:
                                            motOpString = getMotOpString("PTH", line, in);
                                            processSpotAttribute(m_lastActElem, "Motion Option", motOpString);
                                            break;                                    
                                  
                                } // switch
                                // if a valid line was parsed and it was not a call statement reset the call flag
                              
                                    break;
                            } // if
                            else if(ii == (m_keywords.length - 1))
                            {
                               
                                      Pattern stmtLine;

                                      stmtLine = Pattern.compile("^\\s*[0-9]*:\\s*(.*)\\s*;", Pattern.CASE_INSENSITIVE);

                                      Matcher    mstmtLine = stmtLine.matcher(line);

                                      if(true == mstmtLine.find()) {
                                          String macroCommand = mstmtLine.group(1);
                                    
                                          macroCommand = macroCommand.trim();
                                          if (macroCommand != null) {
                                              String macroAction = (String)(m_macroCommands.get( macroCommand ));
                                              if (macroAction != null)
                                              {
                                                  processMacroStatement( macroCommand, macroAction, activityListElem, mountedGunName );

                                                  lineHandled = true;
                                              }
                                          }
                                      } // if
                                      if (lineHandled == false) {
                                              Pattern unparsedLine;

                                              unparsedLine = Pattern.compile("^\\s*[0-9]*:(.*)", Pattern.CASE_INSENSITIVE);

                                              Matcher    unparsedStmtLine = unparsedLine.matcher(line);

                                              if(true == unparsedStmtLine.find()) {
                                                  String unparsedCommand = unparsedStmtLine.group(1); 
                                                  if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
                                                    addToComments(activityListElem, unparsedCommand, false, true);
                                                  else
                                                    addToComments(activityListElem, unparsedCommand, true, true);
                                                  bw.write(unparsedString);
                                                  unparsedEntitiesExist = true;        
                                              }
                              
                                      }
                            } // else if

                        } // for
                        
                    
                            if (handle_attr == true || handle_appl == true)
                                 addToComments(activityListElem, line, true, false);
                   
                        
                        line = in.readLine();
                        lineNumber++;
                        
                    
                        int numOfChar = unparsedStatement.length();
                        unparsedStatement = unparsedStatement.delete(0, numOfChar);
                   
                        
                    } // while
                    in.close();
                    
         
                    if(unparsedEntitiesExist == false)
                        bw.write("All the program statements have been parsed.\n");

                    bw.write("\nJava parsing completed successfully.\n");
                    bw.write("\nEnd of java parsing.\n");
                    bw.flush();
                    bw.close();
                 
                } // try

                catch (IOException e) {
                    e.getMessage();
                }

        }

    
        private String getMotOpString(String prefix, String line,  BufferedReader in)
        {
            String motOpString = "";
            try {
                int motOpIndex = line.indexOf(prefix);
                int semiColonIndex = line.indexOf(";");
                if (semiColonIndex < 0)
                {
                      String tmpLine = "";
                      String nextLine = in.readLine();

                      int colonIndex = nextLine.indexOf(":");
                      semiColonIndex = nextLine.indexOf(";");
                      if (colonIndex >= 0 && semiColonIndex > 0)
                      {
                         tmpLine = nextLine.substring(colonIndex + 1, semiColonIndex);
                         nextLine = tmpLine;
                      }
                      motOpString = line.substring(motOpIndex) + nextLine;
                }
                else
                {
                    if (motOpIndex > 0 && motOpIndex <= semiColonIndex)
                        motOpString = line.substring(motOpIndex, semiColonIndex); 
                }
            }
            catch (IOException e) {
                    e.getMessage();
                }           
            return(motOpString);
        }
            
        private void addToComments( Element actListElem, String line, boolean header, boolean nativeLanguage )
        {
            String [] commentComponents = line.split("!");
            String  newLine = line;
            String commentConcat = "";
          Pattern unparsedLine;

          if (nativeLanguage ==  true) {
              unparsedLine = Pattern.compile("^\\s*[0-9]*:(.*)", Pattern.CASE_INSENSITIVE);

              Matcher    unparsedStmtLine = unparsedLine.matcher(line);

			  if (true == unparsedStmtLine.find())
			  {
				  newLine = line = unparsedStmtLine.group(1);		
			  }
          }
          else {
              if (header == false) {
                    for (int jj=1;jj< commentComponents.length;jj++)
                    {
                        commentConcat = commentConcat.concat(commentComponents[jj]);
                    }
				    /***** DLY 2007/04/25 this causes ; to be removed in roundload of comment so take it out 
                    String commaCheck = commentConcat;
                    if ( commaCheck.trim().lastIndexOf(';') == (commaCheck.trim().length()-1) )
                    {
                        int commaIndex = commentConcat.lastIndexOf(';');
                        commaCheck = commentConcat.substring(0,commaIndex-1);
                        commentConcat = commaCheck;
                    }
					*****/
                    newLine = line = commentConcat;
              }
          }
          
          //  while (line.indexOf("<") >= 0) {
          //       String newCommentValue = new String("");
          //       newCommentValue = line.substring(0, line.indexOf("<")) + "&lt;" + line.substring(line.indexOf("<")+1,line.length());
          //         line = newCommentValue; 
          //  }
          //  while (line.indexOf(">") >= 0) {
          //       String newCommentValue = new String("");
          //       newCommentValue = line.substring(0, line.indexOf(">")) + "&gt;" + line.substring(line.indexOf(">")+1, line.length());
          //         line = newCommentValue; 
          //  }
          
            if ((m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))  && header == false) {
                String actName = "Comment." + String.valueOf(m_operationCommentCount++);
                if (nativeLanguage == true) {
                    actName = "Robot Language." + String.valueOf(m_robotLanguageCommentCount++);
                }
                Element operationElem = super.createActivityHeader( actListElem, actName, "Operation");
                
                String [] commentNames = {"Comment"};
                if (nativeLanguage == true) {
                    commentNames[0] = "Robot Language";
					
                }
                
                String [] commentValues = {line};

				
                
                super.createAttributeList(operationElem, commentNames, commentValues);
                
            }
            else {

                if (nativeLanguage)
                    newLine = "Robot Language:" + line;

                String commentName = "Comment" + String.valueOf(m_commentCount+1);
                if (header == true)
                    m_commentValues[m_commentCount] = newLine;
                else 
                    m_commentValues[m_commentCount] = commentConcat;
                m_commentNames[m_commentCount] = commentName;
                m_commentCount++;
            }
        }        

        private void processComments(Element addCommentElem, String commentPrefix) { 
            
            if (m_commentCount == 0)
                return;
                        
            String [] commentNames = new String [m_commentCount];
            String [] commentValues = new String [m_commentCount];
            for (int ii=0;ii<m_commentCount;ii++) {
                if (m_commentNames[ii].indexOf("Comment") == 0)
                    commentNames[ii] = commentPrefix + m_commentNames[ii];
                else
                    commentNames[ii] = m_commentNames[ii]; 
                commentValues[ii] = m_commentValues[ii];
               // while (commentValues[ii].indexOf("<") >= 0) {
               //      String newCommentValue = new String("");
               //      newCommentValue = commentValues[ii].substring(0, commentValues[ii].indexOf("<")) + "&lt;" + commentValues[ii].substring(commentValues[ii].indexOf("<")+1,commentValues[ii].length());
               //        commentValues[ii] = newCommentValue; 
               // }
               // while (commentValues[ii].indexOf(">") >= 0) {
               //      String newCommentValue = new String("");
               //      newCommentValue = commentValues[ii].substring(0, commentValues[ii].indexOf(">")) + "&gt;" + commentValues[ii].substring(commentValues[ii].indexOf(">")+1, commentValues[ii].length());
               //        commentValues[ii] = newCommentValue; 
               // }

            }
            super.createAttributeList(addCommentElem, commentNames, commentValues);
            
            m_commentCount = 0;
            clearComments();
            
        }
        private void clearComments() {
             for (int ii=0;ii<m_commentNames.length;ii++) {
                 m_commentNames[ii] = "";
             }
             for (int ii=0;ii<m_commentValues.length;ii++) {
                 m_commentValues[ii] = "";
             }
        }        
        
        private void processToolFrameStatement( double xx, double yy, double zz, double yaw, double pitch, double roll, Element toolProfileListElem) {
             String toolProfileName = null;
             int utool_num = 0;
             if(m_currentToolNumber != null && m_currentToolNumber != "") {
                 utool_num = Integer.parseInt(m_currentToolNumber);                
                 boolean utool_used = false;
                 if (utool_num > 0 && utool_num < 20)
                 {
                     utool_used = m_utoolsUsed[utool_num-1];
                 }
                 if (utool_used == false)
                     return;
                if (m_sysvarFound == false)
                {
                    toolProfileName = (String) m_toolNumberMapping.get(m_currentToolNumber);
                }                
                if(toolProfileName == null || m_currentToolGroup != null)
                {
                   if (m_currentToolGroup != null && m_currentToolGroup != "")
                   {
                       toolProfileName = "UTOOL_NUM[" + m_currentToolGroup + "]=" + m_currentToolNumber;     	   
                   }
                   else
                   {
                       toolProfileName = "UTOOL_NUM = " + m_currentToolNumber;
                   }
                   m_toolNumberMapping.put(String.valueOf(Integer.parseInt(m_currentToolNumber)), toolProfileName);
                }                   
                if (m_currentToolType != null)
                    m_toolTypeMapping.put(String.valueOf(Integer.parseInt(m_currentToolNumber)), m_currentToolType);
            }
            else
                toolProfileName = "UTOOL_NUM = 1";

             double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
             Double massValue = Double.valueOf("0.0");
             double [] cogValues = {0.0, 0.0, 0.0};
             double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};

             toolValues[0] = xx*.001;
             toolValues[1] = yy*.001;
             toolValues[2] = zz*.001;
             toolValues[3] = yaw;
             toolValues[4] = pitch;
             toolValues[5] = roll;
             
             cogValues[0] = 0.0;
             cogValues[1] = 0.0;
             cogValues[2] = 0.0;

             inertiaValues[0] = 0.0;
             inertiaValues[2] = 0.0;
             inertiaValues[5] = 0.0;
             
             
            if (utool_num > 0 && utool_num < 20 && m_utoolsCreated[utool_num-1] == false)
            {
                if (m_currentToolType != null && m_currentToolType.equals("Stationary"))
                {
                    DNBIgpOlpUploadEnumeratedTypes.ToolType toolType = DNBIgpOlpUploadEnumeratedTypes.ToolType.STATIONARY_ROBOT;
                    
                    if (m_intAxes.equals("1") || m_intAxes.equalsIgnoreCase("true")) {
                       toolType = DNBIgpOlpUploadEnumeratedTypes.ToolType.STATIONARY;
                    }
                	
                	super.createToolProfile( m_toolProfileListElement, toolProfileName, toolType, toolValues, massValue, cogValues, inertiaValues);
                }
                else
                {
                    super.createToolProfile( m_toolProfileListElement, toolProfileName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
                }
                m_toolProfileCreated = true;
                m_utoolsCreated[utool_num-1] = true; 
            }
        }
        
        private void processObjectFrameStatement( double xx, double yy, double zz, double yaw, double pitch, double roll, Element objProfileListElem) {
             
             String objectProfileName = null;
             int uframe_num = 0; 
             if(m_currentObjectNumber != null && m_currentObjectNumber != "") {
                 uframe_num = Integer.parseInt(m_currentObjectNumber);
                 boolean uframe_used = false;
                 if (uframe_num > 0 && uframe_num < 20)
                 {
                     uframe_used = m_uframesUsed[uframe_num-1];
                 }
                 if (uframe_used == false)
                     return;               
                if (m_sysvarFound == false)
                {
                    objectProfileName = (String) m_objectNumberMapping.get(m_currentObjectNumber);
                }
                if(objectProfileName == null || m_currentToolGroup != null)
                {
                   if (m_currentObjectGroup != null && m_currentObjectGroup != "")
                   {
                       objectProfileName = "UFRAME_NUM[" + m_currentObjectGroup + "]=" + m_currentObjectNumber;     	   
                   }
                   else
                   {
                       objectProfileName = "UFRAME_NUM = " + m_currentObjectNumber;
                   }
                   m_objectNumberMapping.put(String.valueOf(Integer.parseInt(m_currentObjectNumber)), objectProfileName);
                } 
            }
            else
            {
                objectProfileName = "UFRAME_NUM = " + m_currentObjectNumber;
            }
            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE;
            if (m_intAxes.equals("1") || m_intAxes.equalsIgnoreCase("true")) {
                objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
            }
            else {
                objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE;
            }
             
             double [] objectFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};
             objectFrameValues[0] = xx*.001;
             objectFrameValues[1] = yy*.001;
             objectFrameValues[2] = zz*.001;
             objectFrameValues[3] = yaw;
             objectFrameValues[4] = pitch;
             objectFrameValues[5] = roll; 
            if (uframe_num > 0 && uframe_num < 20 && m_uframesCreated[uframe_num-1] == false)
            {
            	if (m_uframesSwap[uframe_num] != 0)
            	{
                    // set uframe values equal to tool values
            		objectFrameValues = (double [])m_listOfToolFrameValues.get(m_uframesSwap[uframe_num]);
                    objectFrameValues[0] *= .001;
                    objectFrameValues[1] *= .001;
                    objectFrameValues[2] *= .001;            		
            		// create stationary tool using utool values
            		m_currentToolNumber = String.valueOf(Integer.parseInt(m_maximumToolNumber)+1);
            		m_currentToolType = "Stationary";
            		processToolFrameStatement( xx, yy, zz, yaw, pitch, roll, m_toolProfileListElement);	
                   	objFrameType = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
            	}

                Element curObjectFrame = super.createObjectFrameProfile( m_objectProfileListElement, objectProfileName, objFrameType, objectFrameValues);
                m_objectProfileCreated = true;
                m_uframesCreated[uframe_num-1] = true;
                String currentObjectApplyOffsets = (String)m_objectApplyOffsetsMapping.get(m_currentObjectNumber);
                if (curObjectFrame != null && currentObjectApplyOffsets.equals("On"))
                {
                    curObjectFrame.setAttribute("ApplyOffsetToTags", "On");
                }
                else
                {
                	curObjectFrame.setAttribute("ApplyOffsetToTags", "Off");   
                }
          
            }
        }
        
        private void processPosStatement(Matcher match, String line,BufferedReader in) throws NumberFormatException {
            int targetNumber = 0;
            //parse the string into array
            String [] components = {"",""};
            String [] target_components;
            int groupNumber = 0;

            // initialize these everytime otherwise offset for auxiliary axes gets screwed up.
            m_extAxesCount = 0;
            m_auxAxesCount = 0;
            m_workAxesCount = 0;
            
            //Get the target number as an object
            targetNumber = Integer.parseInt(match.group(1).trim());

            components[0] = match.group(1).trim();
            components[1] = match.group(2).trim();
            //Connect the array with tag names through hashtable

            String tpName = "P[" + components[0] + "]";
            m_tagNames.put(components[0], m_TagPrefix + tpName + m_TagSuffix);
            
            m_tagComments.put(tpName, components[1]);
            
            try {
                // GP line
                String inline = in.readLine();
                if(inline.equals("")) {
                    inline = in.readLine();
                }
            
                 Pattern pgroup;
                 pgroup = Pattern.compile("\\s*GP([0-9]):", Pattern.CASE_INSENSITIVE);
                 Matcher    mgroup = pgroup.matcher(inline);
                 if(true == mgroup.find()) {
                     groupNumber = Integer.valueOf(mgroup.group(1)).intValue();
                 }
            //Read UF UT Config line
            inline = in.readLine();

                         if(inline.equals("")) {
                            inline = in.readLine();
                        }
            
             Pattern pUtoolUframe;
             pUtoolUframe = Pattern.compile("\\s*UF\\s*:\\s*([0-9]*)\\s*,\\s*UT\\s*:\\s*([A-Z0-9]*)", Pattern.CASE_INSENSITIVE);
             Matcher    mUtoolUframe = pUtoolUframe.matcher(inline);
             if(true == mUtoolUframe.find()) {
                String uframe_numStr = mUtoolUframe.group(1);
                String utool_numStr = mUtoolUframe.group(2);
                int uframe_num = 0;
                int utool_num = 0;
                
                if (uframe_numStr != null && uframe_numStr.equals("") != true)
                    uframe_num = Integer.parseInt(uframe_numStr);
                if (utool_numStr != null && utool_numStr.equals("") != true && utool_numStr.equals("A") != true)
                    utool_num = Integer.parseInt(utool_numStr);
                if (utool_numStr != null && utool_numStr.equals("") != true && utool_numStr.equals("A") == true)
                    utool_num = Integer.parseInt(utool_numStr, 16);
                
                if (uframe_num > 0 && uframe_num < 20)
                {
                    m_uframesUsed[uframe_num-1] = true;
                    if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda") || m_sysvarFound == true)
                    {                    
                        String objProfileName = "UFRAME_NUM = " + String.valueOf(uframe_num);
                        m_objectNumberMapping.put(String.valueOf(uframe_num), objProfileName); 
                    }
                }
                
                if (utool_num > 0 && utool_num < 20)
                {
                    m_utoolsUsed[utool_num-1] = true;
                    if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda") || m_sysvarFound == true)
                    {
                        String toolProfileName = "UTOOL_NUM = " + String.valueOf(utool_num);
                        m_toolNumberMapping.put(String.valueOf(utool_num), toolProfileName);
                    }
                }              
             }
            
            Pattern putuf;
            
            if (m_Turn1.equalsIgnoreCase("true") || m_Turn5.equalsIgnoreCase("true"))
                putuf = Pattern.compile("CONFIG\\s*:\\s*'\\s*([A-Z])\\s*([A-Z])?\\s*?([A-Z])?\\s*?,\\s*(-?[0-9])\\s*,\\s*(-?[0-9])\\s*,\\s*(-?[0-9])\\s*'\\s*,", Pattern.CASE_INSENSITIVE);
            else
                putuf = Pattern.compile("CONFIG\\s*:\\s*'\\s*([A-Z])\\s*([A-Z])?\\s*?([A-Z])?\\s*?,\\s*(-?[0-9])\\s*,\\s*,\\s*(-?[0-9])\\s*'\\s*,", Pattern.CASE_INSENSITIVE);
                    
            Matcher    mutuf = putuf.matcher(inline);
            
            target_components = new String [13];
            
            if(true == mutuf.find()) {                
                target_components[0] = mutuf.group(1);
                target_components[1] = mutuf.group(2);
                if (target_components[1] == null)
                    target_components[1] = "";
                target_components[2] = mutuf.group(3);
                if (target_components[2] == null)
                    target_components[2] = "";
                
                if (m_Turn1.equalsIgnoreCase("True") )
                {
                    target_components[3] = mutuf.group(4);
                    target_components[4] = mutuf.group(5);
                    target_components[5] = mutuf.group(6); 
                }
                else if (m_Turn5.equalsIgnoreCase("True") )
                {
                	target_components[3] = mutuf.group(4);
                    target_components[4] = mutuf.group(5);
                    target_components[5] = mutuf.group(6);                     
                }
                else
                {
                    target_components[3] = "0";
                    target_components[4] = mutuf.group(4);          
                    target_components[5] = mutuf.group(5);      
                }
            }
            else
            {
                target_components[0] = "N";
                target_components[1] = "U";
                target_components[2] = "T";
                target_components[3] = "0";
                target_components[4] = "0";
                target_components[5] = "0";
            }
            //Read X,Y,Z line
            inline = in.readLine(); 
            
                         if(inline.equals("")) {
                            inline = in.readLine();
                        } 
            
            Pattern pxyz = Pattern.compile("X\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*mm\\s*,\\s*Y\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*mm\\s*,\\s*Z\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*mm\\s*,", Pattern.CASE_INSENSITIVE);
            Matcher mxyz = pxyz.matcher(inline);
                                   
            if(true == mxyz.find())
            {
                target_components[6] = mxyz.group(1);
                target_components[7] = mxyz.group(2);
                target_components[8] = mxyz.group(3);
                m_targetType = CARTESIAN;
            }
            else
            {
                Pattern pjnt1 = Pattern.compile("J1\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*deg\\s*,\\s*J2\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*deg\\s*,\\s*J3\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,", Pattern.CASE_INSENSITIVE);
                Matcher mjnt1 = pjnt1.matcher(inline);
                if (true == mjnt1.find())
                {
                    target_components[6] = mjnt1.group(1);
                    target_components[7] = mjnt1.group(2);
                    target_components[8] = mjnt1.group(3);                    
                }
                m_targetType = JOINT;
            }
            
            //Connect the array with the target type through hashtable
            m_targetTypes.put(components[0],new Integer(m_targetType));            

            //Read W,P,R line
            inline = in.readLine();
            
                        if(inline.equals("")) {
                            inline = in.readLine();
                        }  
            
            Pattern pwpr = Pattern.compile("W\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*deg\\s*,\\s*P\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*deg\\s*,\\s*R\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*deg\\s*", Pattern.CASE_INSENSITIVE);           
            Matcher mwpr = pwpr.matcher(inline);

            if(true == mwpr.find())
            {
                target_components[9] = mwpr.group(1);
                target_components[10] = mwpr.group(2);
                target_components[11] = mwpr.group(3);
            }
            else
            {
                Pattern pjnt2 = Pattern.compile("J4\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*J?5?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*J?6?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                Matcher mjnt2 = pjnt2.matcher(inline);
                if (true == mjnt2.find())
                {
                    target_components[9] = mjnt2.group(1);
                    target_components[10] = "0.0";
                    target_components[11] = "0.0";
                    if (mjnt2.group(3) != null && !mjnt2.group(3).equals(""))
                    {
                    	target_components[10] = mjnt2.group(3);
                    }
                    if (mjnt2.group(5) != null && !mjnt2.group(5).equals(""))
                    {                    
                        target_components[11] = mjnt2.group(5);
                    }
                }
            }            
            
            if (targetNumber > m_listOfRobotTargets.size())
            {
            	// add dummy targets if we skipped an index....otherwise...crash
            	for(int ii = lastTargetNumber; ii < targetNumber-1; ii++) {
                  m_listOfRobotTargets.add(ii,target_components);
            	}
            
            	//Add the target to a dynamic array
            	m_listOfRobotTargets.add(targetNumber-1,target_components);
            }
            else
            {
            	m_listOfRobotTargets.set(targetNumber-1,target_components);           	
            }
            //Resize the array
            m_listOfRobotTargets.trimToSize(); 
            
            //Read Aux line
            inline = in.readLine();
            
                         if(inline.equals("")) {
                            inline = in.readLine();
                        }      
            String [] extended_components;
            extended_components = new String[10];  
 
            String [] grpAux = m_auxAxesGroupString.split(",");
            String [] grpExt = m_extAxesGroupString.split(",");
            String [] grpWork = m_workAxesGroupString.split(",");
            boolean firstAux = true, firstExt = true, firstWork = true;
            int index1Axes = m_numRobotAxes + 1;
            String auxJIndex1 = "J" + String.valueOf(index1Axes);
            int index2Axes = m_numRobotAxes + 4;
            String auxJIndex2 = "J" + String.valueOf(index2Axes);            
            while (inline.indexOf("};") < 0 )
            {
                if (inline.indexOf("J4") < 0 && inline.indexOf(auxJIndex2) < 0 && inline.indexOf("E4") < 0) {
                    extended_components = new String[10];  
                    for (int ii=0;ii<10;ii++) {
                        extended_components[ii] = "";
                    }
                }
                
                if ( inline.indexOf("E1") >= 0 || inline.indexOf(auxJIndex1) >= 0 || inline.indexOf(auxJIndex2) >= 0 || inline.indexOf("E4") >= 0)
                {
                    int aux_offset = 0;
                    aux_offset = m_auxAxesCount + m_extAxesCount + m_workAxesCount;

                    Pattern pext = Pattern.compile("");
                   
           
                    pext = Pattern.compile("J([0-9]*)\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*J?([0-9]*)?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*J?([0-9]*)?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                    /*
                    if (inline.indexOf("J10") >= 0) {
                        pext = Pattern.compile("J10\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*J?1?1?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*J?1?2?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                    }
                    else {
                        pext = Pattern.compile("J7\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*J?8?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*J?9?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                    }
                    */
                    Matcher mext = pext.matcher(inline);
                    if (true == mext.find())
                    {
                        extended_components[aux_offset] = mext.group(2);
                        extended_components[aux_offset+1] = mext.group(5);
                        extended_components[aux_offset+2] = mext.group(8);               
                    }
                    else
                    {
                        Pattern pext2 = Pattern.compile("");
                        if (inline.indexOf("E4") >= 0) {
                            pext2 = Pattern.compile("E4\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*E?5?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*E?6?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                        }
                        else {
                            pext2 = Pattern.compile("E1\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*E?2?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*E?3?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                        }
                        Matcher mext2 = pext2.matcher(inline);
                        if (true == mext2.find())
                        {
                            extended_components[aux_offset] = mext2.group(1);
                            extended_components[aux_offset+1] = mext2.group(3);
                            extended_components[aux_offset+2] = mext2.group(5); 
                        }
                    }
 
                    if ((groupNumber == m_auxAxesGroupNumber || grpAux[groupNumber-1].equals("*") == false) && extended_components[0].equals("") != true)
                    {
                        for (int ii=0;ii<3;ii++) {
                            if (extended_components[ii+aux_offset].equals("") != true) {
                                if (inline.indexOf("E4") >= 0 || inline.indexOf(auxJIndex2) >= 0)
                                    m_auxAxesCount++;
                                else
                                    m_auxAxesCount++;
                            }
                        }                        

                        
                        if (targetNumber > m_listOfAuxTargets.size())
                        {
                            // add dummy targets if we skipped an index....otherwise...crash
                            for(int ii = lastTargetNumber; ii < targetNumber-1; ii++) {
                                  m_listOfAuxTargets.add(ii,extended_components);
                            }                   
                            //Add the target to a dynamic array
                            m_listOfAuxTargets.add(targetNumber-1, extended_components);
                        }
                        else
                        {
                            m_listOfAuxTargets.set(targetNumber-1, extended_components);          	
                        }
                                               
                         //Resize the array
                        m_listOfAuxTargets.trimToSize();
                    }

                    if ((groupNumber == m_extAxesGroupNumber || grpExt[groupNumber-1].equals("*") == false) && extended_components[0].equals("") != true)
                    {
                        for (int ii=0;ii<3;ii++) {
                            if (extended_components[ii+aux_offset].equals("") != true) {
                                if (inline.indexOf("E4") >= 0 || inline.indexOf(auxJIndex2) >= 0)
                                    m_extAxesCount++;
                                else
                                    m_extAxesCount++;
                            }
                        }

                        if (targetNumber > m_listOfExtTargets.size())
                        {
                            // add dummy targets if we skipped an index....otherwise...crash
                            for(int ii = lastTargetNumber; ii < targetNumber-1; ii++) {
                                m_listOfExtTargets.add(ii,extended_components);
                            }
                            //Add the target to a dynamic array
                            m_listOfExtTargets.add(targetNumber-1, extended_components);                        	
                        }
                        else
                        {
                            m_listOfExtTargets.set(targetNumber-1, extended_components);          	
                        }
                        
                        //Resize the array
                        m_listOfExtTargets.trimToSize();
                    }

                    if ((groupNumber == m_workAxesGroupNumber || grpWork[groupNumber-1].equals("*") == false) && extended_components[0].equals("") != true)
                    {
                        for (int ii=0;ii<3;ii++) {
                            if (extended_components[ii+aux_offset].equals("") != true) {
                                if (inline.indexOf("E4") >= 0 || inline.indexOf(auxJIndex2) >= 0)
                                    m_workAxesCount++;
                                else
                                    m_workAxesCount++;
                            }
                        }                        

                        if (targetNumber > m_listOfWorkTargets.size())
                        {
                            // add dummy targets if we skipped an index....otherwise...crash
                            for(int ii = lastTargetNumber; ii < targetNumber-1; ii++) {
                                m_listOfWorkTargets.add(ii,extended_components);
                            }
                            //Add the target to a dynamic array
                            m_listOfWorkTargets.add(targetNumber-1, extended_components);                        	
                        }
                        else
                        {
                            m_listOfWorkTargets.set(targetNumber-1, extended_components);          	
                        }
                        
                        //Resize the array
                        m_listOfWorkTargets.trimToSize();
                    }
                    
                }
                if ( inline.indexOf("GP") >= 0 || inline.indexOf("J4") >= 0)
                {
                     int aux_offset = 0;
                     aux_offset = m_auxAxesCount + m_extAxesCount + m_workAxesCount;
                    
                    Pattern pext3 = Pattern.compile("");
                    if (inline.indexOf("GP") >= 0) {
                         pext3 = Pattern.compile("J1\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*J?2?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*J?3?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                         Pattern pgroup2;                         
                         pgroup2 = Pattern.compile("\\s*GP([0-9]):", Pattern.CASE_INSENSITIVE);
                         Matcher    mgroup2 = pgroup2.matcher(inline);
                         if(true == mgroup2.find()) {
                             groupNumber = Integer.valueOf(mgroup2.group(1)).intValue();
                             if (groupNumber == m_auxAxesGroupNumber || grpAux[groupNumber-1].equals("*") == false)
                             {
                                 if (firstAux)
                                 {
                                    m_auxAxesStart = aux_offset + 1;
                                    firstAux = false;
                                 }
                             }
                             if (groupNumber == m_extAxesGroupNumber || grpExt[groupNumber-1].equals("*") == false)
                             {
                                 if (firstExt)
                                 {
                                    m_extAxesStart = aux_offset + 1;
                                    firstExt = false;
                                 }
                             }
                             if (groupNumber == m_workAxesGroupNumber || grpWork[groupNumber-1].equals("*") == false)
                             {
                                 if (firstWork)
                                 {
                                    m_workAxesStart = aux_offset + 1;
                                    firstWork = false;
                                 }
                             }
                         } 
                        // eat UF UT line
                        inline = in.readLine();

                             if(inline.equals("")) {
                                inline = in.readLine();

                            }      

                        inline = in.readLine();

                             if(inline.equals("")) {
                                inline = in.readLine();
                            } 
                    }
                    else {
                        pext3 = Pattern.compile("J4\\s*=\\s*(-?[0-9]*\\.?[0-9]*)\\s*(deg|mm)\\s*,?\\s*J?5?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*,?\\s*J?6?\\s*=?\\s*(-?[0-9]*\\.?[0-9]*)?\\s*(deg|mm)?\\s*", Pattern.CASE_INSENSITIVE);
                    }

                    Matcher mext3 = pext3.matcher(inline);
                    if (true == mext3.find())
                    {
                        extended_components[aux_offset] = mext3.group(1);
                        extended_components[1+aux_offset] = mext3.group(3);
                        extended_components[2+aux_offset] = mext3.group(5);
                    }

                    if ((groupNumber == m_auxAxesGroupNumber || grpAux[groupNumber-1].equals("*") == false) && extended_components[aux_offset].equals("") != true)
                    {
                        for (int ii=0;ii<3;ii++) {
                            if (extended_components[aux_offset+ii].equals("") != true) {
                                if (inline.indexOf("J4") >= 0)
                                    m_auxAxesCount++;
                                else
                                    m_auxAxesCount++;
                            }
                        }

                        if (targetNumber > m_listOfAuxTargets.size())
                        {
                            // add dummy targets if we skipped an index....otherwise...crash
                            for(int ii = lastTargetNumber; ii < targetNumber-1; ii++) {
                                  m_listOfAuxTargets.add(ii,extended_components);
                            }                   
                            //Add the target to a dynamic array
                            m_listOfAuxTargets.add(targetNumber-1, extended_components);
                        }
                        else
                        {
                            m_listOfAuxTargets.set(targetNumber-1, extended_components);          	
                        }
                        
                        //Resize the array
                        m_listOfAuxTargets.trimToSize();
                    }

                    if ((groupNumber == m_extAxesGroupNumber || grpExt[groupNumber-1].equals("*") == false) && extended_components[aux_offset].equals("") != true)
                    {
                        for (int ii=0;ii<3;ii++) {
                            if (extended_components[aux_offset+ii].equals("") != true) {
                                if (inline.indexOf("J4") >= 0)
                                    m_extAxesCount++;
                                else
                                    m_extAxesCount++;
                            }
                        }

                        if (targetNumber > m_listOfExtTargets.size())
                        {
                            // add dummy targets if we skipped an index....otherwise...crash
                            for(int ii = lastTargetNumber; ii < targetNumber-1; ii++) {
                                  m_listOfExtTargets.add(ii,extended_components);
                            }                   
                            //Add the target to a dynamic array
                            m_listOfExtTargets.add(targetNumber-1, extended_components);
                        }
                        else
                        {
                            m_listOfExtTargets.set(targetNumber-1, extended_components);          	
                        }
                        
                        //Resize the array
                        m_listOfExtTargets.trimToSize();
                    }

                    if ((groupNumber == m_workAxesGroupNumber || grpWork[groupNumber-1].equals("*") == false) && extended_components[aux_offset].equals("") != true)
                    {
                        for (int ii=0;ii<3;ii++) {
                            if (extended_components[aux_offset+ii].equals("") != true) {
                                if (inline.indexOf("J4") >= 0)
                                    m_workAxesCount++;
                                else
                                    m_workAxesCount++;
                            }
                        }

                        if (targetNumber > m_listOfWorkTargets.size())
                        {
                            // add dummy targets if we skipped an index....otherwise...crash
                            for(int ii = lastTargetNumber; ii < targetNumber-1; ii++) {
                                  m_listOfWorkTargets.add(ii,extended_components);
                            }                   
                            //Add the target to a dynamic array
                            m_listOfWorkTargets.add(targetNumber-1, extended_components);
                        }
                        else
                        {
                            m_listOfWorkTargets.set(targetNumber-1, extended_components);          	
                        }
                        
                       //Resize the array
                        m_listOfWorkTargets.trimToSize();
                    }

                }
                //Read Aux line again..for possible next group
                inline = in.readLine();

                 if(inline.equals("")) {
                    inline = in.readLine();

                }
            } // while
            } // try

             catch (IOException e) {
                    e.getMessage();
             }
            lastTargetNumber = targetNumber;
        }

        private void processMOVStatement(String line, Element activityListElem) {

            int listIndex = -1;
            boolean newSpeedProfile = false, newAccuracyProfile = false;
            boolean doWeldSpeed = false;
            String speedString ="Default";
            String accuracyString = "";
            String accuracyValue = "";
            String accuracyProfileName = "Default";
            String [] spComponents, acComponents ;
             
            // search for arc statement now so activity name can be changed
             Pattern arcStartPattern = Pattern.compile("Arc Start\\s*\\[([.[^;]]*)", Pattern.CASE_INSENSITIVE);
             Pattern arcEndPattern = Pattern.compile("Arc End\\s*\\[([.[^;]]*)", Pattern.CASE_INSENSITIVE);
             Matcher arcStartMatch = arcStartPattern.matcher(line);
             Matcher arcEndMatch = arcEndPattern.matcher(line);

            if(true == arcStartMatch.find())
            {
                m_currentArcStartIndex = arcStartMatch.group(1);
                m_currentArcEndIndex = "[0]";
            }
            else
            {
                if (true == arcEndMatch.find())
                {
                    m_currentArcEndIndex = arcEndMatch.group(1);
                    m_currentArcStartIndex = "[0]";
                }
            }          
            
            String moveActNameValue;
            if (!m_currentArcStartIndex.equals("[0]"))
            {
                moveActNameValue = "Arc Start[" + m_currentArcStartIndex;
            }
            else if (!m_currentArcEndIndex.equals("[0]"))
            {
                moveActNameValue = "Arc End[" + m_currentArcEndIndex;
            }
            else
            {
                moveActNameValue = "RobotMotion." + m_moveCounter; 
            }
            
            Element actElem = super.createMotionActivityHeader(activityListElem, moveActNameValue);
            m_lastActElem = actElem;
            if (m_robotMotionCompleted == 0) {
                m_firstRobotMotion = actElem;
            }
            m_lastRobotMotion = actElem;

            Pattern pv = Pattern.compile("(-?[0-9]*\\.?[0-9]*)(%|mm/sec|sec|deg/sec|cm/min|in/min)", Pattern.CASE_INSENSITIVE);
            Matcher mv = pv.matcher(line);
            String motionProfileName = "Default";
            if(true == mv.find()) {

                speedString = mv.group();
                speedString.trim();

                boolean isInList = m_listOfSpeedValues.contains(speedString);
                
                if(isInList == false) {
                    m_listOfSpeedValues.add(speedString);
                    listIndex = m_listOfSpeedValues.indexOf(speedString);
                 // taken out per request from Honda   motionProfileName = "Motion." + new Integer(listIndex + m_motionProfNum + 1).toString();
                    motionProfileName = speedString;
                    createNewSpeedProfile(speedString, motionProfileName);
                }
                else
                {
                    listIndex = m_listOfSpeedValues.indexOf(speedString);
                 // taken out per request from Honda  motionProfileName = "Motion." + new Integer(listIndex + m_motionProfNum + 1).toString();
                    motionProfileName = speedString;
                }
            }
            else
            {
                if (line.indexOf("WELD_SPEED") > 0)
                {
                    doWeldSpeed = true;
                }
            }

            Pattern ppl = Pattern.compile("\\s*(CNT|CD|FINE)([0-9]*+)", Pattern.CASE_INSENSITIVE);
            Matcher mpl = ppl.matcher(line);
           
            String motionOption = "";
            
            if(true == mpl.find()) {
                accuracyString = mpl.group(1);
                accuracyString.trim();
                accuracyValue =mpl.group(2);
                accuracyProfileName = accuracyString + accuracyValue;

                boolean isInList = m_listOfAccuracyValues.contains(accuracyProfileName);

                if(isInList == false) {
                    m_listOfAccuracyValues.add(accuracyProfileName);
                    listIndex = m_listOfAccuracyValues.indexOf(accuracyProfileName);
                    
                    createNewAccuracyProfile(accuracyString, accuracyValue,  accuracyProfileName);
                }
                int indexOfAccuracy = line.indexOf(accuracyProfileName);
                int indexOfSemiColon = line.lastIndexOf(';');
           
                if (indexOfAccuracy > 0 && (indexOfSemiColon > (indexOfAccuracy + accuracyProfileName.length())) )
                {
                    motionOption = line.substring(indexOfAccuracy + accuracyProfileName.length(), indexOfSemiColon);
                    motionOption.trim();
                }
            }
            
            Pattern prtcp = Pattern.compile("\\s*RTCP\\s*", Pattern.CASE_INSENSITIVE);
            Matcher mrtcp = prtcp.matcher(line);
       
            String toolProfileName = "UTOOL_NUM = " + m_currentToolNumber;
            if (m_currentToolGroup != null && m_currentToolGroup != "")
            {
            	toolProfileName = "UTOOL_NUM[" + m_currentToolGroup + "]=" + m_currentToolNumber;
               	m_toolNumberMapping.put(String.valueOf(Integer.parseInt(m_currentToolNumber)), toolProfileName);               	
            }
            m_currentToolType = "OnRobot";

            String objProfileName = "UFRAME_NUM = " + m_currentObjectNumber;
            if (m_currentObjectGroup != null && m_currentObjectGroup != "")
            {
            	objProfileName = "UFRAME_NUM[" + m_currentObjectGroup + "]=" + m_currentObjectNumber;
            	m_objectNumberMapping.put(String.valueOf(Integer.parseInt(m_currentObjectNumber)), objProfileName);   
            }            
            
            String storeToolNumber = m_currentToolNumber;
            if(true == mrtcp.find() && m_oldStyleStationary == false)
            {	
                m_uframesSwap[Integer.parseInt(m_currentObjectNumber)] = Integer.parseInt(m_currentToolNumber);
            	m_currentToolNumber = String.valueOf(Integer.parseInt(m_maximumToolNumber)+1);
            	m_utoolsUsed[Integer.parseInt(m_currentToolNumber)-1] = true;
            	toolProfileName = "UTOOL_NUM = " + m_currentToolNumber; // SET TO NEXT AVAILABLE TOOL
            	m_currentToolType = "Stationary";
            }
            else
            {
	            if(m_currentToolNumber != null && m_currentToolNumber != "") {
	               
	                String toolProfileCaption  = (String) m_toolNumberMapping.get(String.valueOf(Integer.parseInt(m_currentToolNumber)));              
	               
	                m_currentToolType = (String)m_toolTypeMapping.get(String.valueOf(Integer.parseInt(m_currentToolNumber)));
	                if(toolProfileCaption != null)
	                    toolProfileName = toolProfileCaption;
	 
	            }
            }
             
            if(m_currentObjectNumber != null && m_currentObjectNumber != "") {

                if (!m_currentObjectNumber.equals("0")) {
                    String objProfileCaption = (String) m_objectNumberMapping.get(m_currentObjectNumber);
                    if(objProfileCaption != null)
                        objProfileName = objProfileCaption;
                }
  
            }
  
            Hashtable userProfileInstance = new Hashtable();
           
           /***** DLY 2006/05/31 IRA0533027 create instance name based on the ArcStart or ArcEnd index 
            if (doWeldSpeed == true) 
            {
            *****/
                if (!m_currentArcStartIndex.equals("[0]"))
                {
                   String oUserProfileInstanceText = "Arc Start[" + m_currentArcStartIndex;
                   LinkedList [] llEmptyAttribs = new LinkedList[0];
                   super.createUserProfile(super.getUserProfileListElement(), "TPEArcStart", oUserProfileInstanceText, llEmptyAttribs);
                   userProfileInstance.put("TPEArcStart", oUserProfileInstanceText);
                   m_currentArcStartIndex = "[0]";
                }
                else if (!m_currentArcEndIndex.equals("[0]"))
                {  
                   String oUserProfileInstanceText = "Arc End[" + m_currentArcEndIndex;
                   LinkedList [] llEmptyAttribs = new LinkedList[0];
                   super.createUserProfile(super.getUserProfileListElement(), "TPEArcEnd", oUserProfileInstanceText, llEmptyAttribs);                   
                   userProfileInstance.put("TPEArcEnd", oUserProfileInstanceText);
                   m_currentArcEndIndex = "[0]";
                }
           /*****
           }
           *****/
            
            String compileLine = line;
            if (!m_circularViaLine.equals("")) {
                compileLine = m_circularViaLine;
            }
            
            String motype = "J";
            Pattern pmovtag = Pattern.compile("(J|L|C)\\s*(P|PR)\\[([0-9]*):?(.*)?\\]", Pattern.CASE_INSENSITIVE);
            Matcher mmov = pmovtag.matcher(compileLine);
            
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;;
            DNBIgpOlpUploadEnumeratedTypes.OrientMode eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS;
           
            if (m_orientMode.equalsIgnoreCase("1_axis") == true)
            {
                 eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS;   
            }
            if (m_orientMode.equalsIgnoreCase("3_axis") == true)
            {
                 eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.THREE_AXIS;   
            }          
            if (m_orientMode.equalsIgnoreCase("wrist") == true)
            {
                 eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.WRIST;   
            }                      
            
            if(true == mmov.find())
            {
                motype = mmov.group(1);
                if (motype.equals("J")) {
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
                }
                else if (motype.equals("L")) {
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                }
                else if (motype.equals("C")) {
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
                }               
            }
            else
            {
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
            }   

            
            ppl = Pattern.compile("\\s*WJNT\\s*", Pattern.CASE_INSENSITIVE);
            mpl = ppl.matcher(compileLine);
            
            if(true == mpl.find())
            {  
                eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.WRIST;
            }
           
            m_motionAttributeElement = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileInstance, eMotype, eOrientType); 
                
            //Call the method to process the target
            if (m_circularViaLine.equals("")) {
                processTarget (actElem, line);
            }
            else {
                processTarget (actElem, m_circularViaLine);
            }
            
           
            if(true == mrtcp.find() || doWeldSpeed == true) {
                String [] attribNames = new String [1];
                
                if (doWeldSpeed)
                {
                    processSpotAttribute( actElem, "WELD_SPEED", "TRUE");
                }
                else
                {
                    processSpotAttribute( actElem, "Motion Option", "RTCP");
                } 
            }
            else
            {
                if (!motionOption.equals("") && !motionOption.equals(" ") && (motionOption.indexOf("SPOT") < 0) && (motionOption.indexOf("Arc Start") < 0) && (motionOption.indexOf("Arc End") < 0) && (motionOption.indexOf("WJNT") < 0))
                {
                    processSpotAttribute( actElem, "Motion Option", motionOption);
                }
            }
           // if (m_currentObjectNumber.equals("10"))
           //     processSpotAttribute( actElem, "UFRAME_NUM", "0");           
            processComments(actElem, "Pre");
            
            m_moveCounter++;
            m_robotMotionCompleted = 1;
            m_currentToolNumber = storeToolNumber;
         }
        private void addUserProfile(String sType, String sValue)
        {
        	if (m_motionAttributeElement != null)
        	{	
			    NodeList moTypeNodeList = m_motionAttributeElement.getElementsByTagName("MotionType");
	            Element moTypeElem = null;
	            if (moTypeNodeList != null && moTypeNodeList.getLength() > 0)
	                moTypeElem = (Element)moTypeNodeList.item(0);
	
	            
	           Element	userProfileElem = m_xmlDoc.createElement("UserProfile");
	
	           userProfileElem.setAttribute("Type", sType);
	           Text oUserProfileInstanceText = m_xmlDoc.createTextNode(sValue);
	           userProfileElem.appendChild(oUserProfileInstanceText);
	           m_motionAttributeElement.insertBefore(userProfileElem, moTypeElem);
        	}
        }
        private void processTimerStatement(String line, Matcher match, Element activityListElem) {

            String fullSeconds = match.group(1);
            String dot = match.group(2);
            String decimal = match.group(3);
            
            fullSeconds = (fullSeconds == null)?"":fullSeconds;
            dot = (dot == null)?"":dot;
            decimal = (decimal == null)?"":decimal;
            String timeInSec = fullSeconds + dot + decimal;         
            
           try
            {
                createTimerStatement( timeInSec, activityListElem, true );
            }

            catch (NumberFormatException e)
            {
                Pattern unparsedLine;

                unparsedLine = Pattern.compile("^\\s*[0-9]*:(.*)", Pattern.CASE_INSENSITIVE);

                Matcher    unparsedStmtLine = unparsedLine.matcher(line);

                if(true == unparsedStmtLine.find()) {
                    String unparsedCommand = unparsedStmtLine.group(1); 
                    if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
                      addToComments(activityListElem, unparsedCommand, false, true);
                    else
                      addToComments(null, unparsedCommand, true, true);
                }        	
            }    	                        
 
       }
        
       private void createTimerStatement( String timeInSec, Element activityListElem, boolean doComments )
       {
        
            //Create DOM Nodes and set appropriate attributes          
            String delayActName = "RobotDelay." + String.valueOf(m_delayCounter);     
            Element actElem = super.createDelayActivity(activityListElem, delayActName, Double.valueOf("0.0"), Double.valueOf(timeInSec));
            m_lastActElem = actElem;

            if (doComments == true) {
                processComments( actElem, "Pre");
            }
            m_delayCounter++;
        }       

        private void processCallStatement( String progName, String line, Element activityListElem, String robotProgramName)
        {
            String callActName = "RobotCall." + String.valueOf(m_callCounter);
            Element actElem = super.createCallTaskActivity( activityListElem, callActName, progName.trim());
            m_lastActElem = actElem;
            m_callCounter++;
       
            int slash_index = robotProgramName.lastIndexOf('/');
            int backslash_index = robotProgramName.lastIndexOf('\\');

            if (slash_index > backslash_index)
                backslash_index = slash_index;

            if (backslash_index < 0)
                backslash_index = 0;
            
            String callProgramName = robotProgramName.substring( 0, backslash_index+1) + progName.trim() + "." + m_progExtension;  

            int index = m_listOfCalledPrograms.indexOf(callProgramName);
            if (index < 0) {
                m_listOfCalledPrograms.add(m_calledProgramCounter, callProgramName);
                m_listOfCalledPrograms.trimToSize(); 
                m_calledProgramCounter++;
            }
            
              Pattern stmtLine;
              
              stmtLine = Pattern.compile("^\\s*[0-9]*:\\s*CALL\\s*[a-zA-Z_0-9]*\\s*(.*);", Pattern.CASE_INSENSITIVE);

              Matcher    mstmtLine = stmtLine.matcher(line);

              if(true == mstmtLine.find()) {
                  String arguments = mstmtLine.group(1);
                  if ( (arguments.trim().length() > 0) && (arguments.indexOf('(') >= 0) && (arguments.indexOf(')') > 0) ) {
                       processSpotAttribute(actElem, "CallArguments", arguments);         
                  }
              }
                  
        }

         private void processTarget(Element actElem, String line) {

             //Parse the string into array
             String [] components;
             String tagcomponent = null;
             String postype = null;
             
            Pattern pmovtag = Pattern.compile("(J|L|C)?\\s*(P|PR)\\[([0-9]*):?(.*)?\\]", Pattern.CASE_INSENSITIVE);
            Matcher mmov = pmovtag.matcher(line);
            
            components = new String[2];
            
            if(true == mmov.find())
            {
                components[0] = mmov.group(1);
                postype = mmov.group(2);
                components[1] = mmov.group(3);
                if (m_vsCamClbTagCount >= 3) {
                    components[1] = String.valueOf(lastTargetNumber);
                }
                tagcomponent = mmov.group(4).trim();
                if (tagcomponent != null)
                {
                	if (tagcomponent.indexOf(']') == 0 )
                	{
                		tagcomponent = "";
                	}
                }
            }
            
            String tagName = "";                        
                 
           if (postype.equals("P")) {
                 //Get the C position number
                 Integer position = new Integer(components[1]);
                 
                 tagName = (String) m_tagNames.get(components[1]);
                 
                 //Get the target type
                 Integer targetType = (Integer) m_targetTypes.get(components[1]);


                 //Get the C position target values (in joint or cartesian coordinates)
                 String [] targetValues = (String []) m_listOfRobotTargets.get(position.intValue()-1);

                 //Process the joint target
                 
                 if(targetType.equals(new Integer(JOINT))) {
                     Element targetElem = super.createTarget( actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false); 
                     m_createJointTarget = true;
                     ArrayList [] jointTargetArrayList = new ArrayList [ m_numRobotAxes+m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
                    //Print out the target (joint) values
                    for(int ii = 0; ii < m_numRobotAxes; ii++)
                        formatJointValueInPulses( jointTargetArrayList, targetValues[ii+6] , null, ii);
           
                     boolean noJointTarget = false;
                     //If there are auxiliary axes present, print out their joint values
                    if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {
                    	 if( (m_auxAxesGroupNumber != m_extAxesGroupNumber && m_auxAxesGroupNumber != m_workAxesGroupNumber && m_extAxesGroupNumber != m_workAxesGroupNumber) || m_extAxesGroupNumber == 0 || m_auxAxesGroupNumber == 0 || m_workAxesGroupNumber == 0)
                    	 {
	                         if(m_numAuxAxes > 0 && m_auxAxesCount == m_numAuxAxes)
	                            noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfAuxTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes);
	                         if(m_numExtAxes > 0 && m_extAxesCount == m_numExtAxes)
	                            noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING , m_numExtAxes);
	                         if(m_numWorkAxes > 0 && m_workAxesCount == m_numWorkAxes)
	                            noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes);
	                        
	                          try {
	                              if(m_numAuxAxes > 0 && m_auxAxesCount != m_numAuxAxes) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Mismatch between Number of RailTrackGantry Axes in V5 Device and number counted in Program\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                              if(m_numExtAxes > 0 && m_extAxesCount != m_numExtAxes) {
	                                    noJointTarget = true;  
	                                    if (m_extAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Mismatch between Number of EndOfArmTooling Axes in V5 Device and number counted in Program");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_extAxesError = true;
	                                    }
	                              }
	                              if(m_numWorkAxes > 0 && m_workAxesCount != m_numWorkAxes) {
	                                    noJointTarget = true;  
	                                    if (m_workAxesError == false)
	                                    {
	                                      m_bw.write("FANUC PARSER ERROR: Mismatch between Number of Workpiece Axes in V5 Device and number counted in Program");
	                                      m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                      m_workAxesError = true;
	                                    }
	                              }    
	                          }
	                          catch (IOException e) {
	                                        e.getMessage();
	                                    }
	                          
                    	 }
                    	 else
                    	 {
	                          try {
	                              if(m_auxAxesGroupNumber == m_extAxesGroupNumber) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and EndOfArmTooling Axes (Toolgroup) can not be the same\n");
	                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                              if(m_auxAxesGroupNumber == m_workAxesGroupNumber) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and Workpiece Axes (Workgroup) can not be the same\n");
	                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                              if(m_workAxesGroupNumber == m_extAxesGroupNumber) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Group number for EndOfArmToolng Axes (Toolgroup) and Workpiece Axes (Workgroup) can not be the same\n");
	                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                          }
	                          catch (IOException e) {
	                                        e.getMessage();
	                                    }
                    	 }
                    }
                    
                    if (noJointTarget == false)
                        super.createJointTarget(targetElem, jointTargetArrayList);
                     
                    // Add PosReg index here for NRL teach 

                    processSpotAttribute(actElem, "PosType", "P");                
                    processSpotAttribute(actElem, "PosReg", components[1]);                
 
                 }
                 else if(targetType.equals(new Integer(CARTESIAN))) {
                      processCartesianTarget(actElem, targetValues, components, false);
                }//end else if
                 
                String tagCommentValue = "";
                tagCommentValue = (String)m_tagComments.get(tagName);
                if (tagcomponent != null && !tagcomponent.equals("") && tagCommentValue.equals(""))
                    tagCommentValue = tagcomponent;
                    
                if (tagCommentValue != null && !tagCommentValue.equals(""))
                     processSpotAttribute( actElem, "Pos Comment", tagCommentValue); 
            } // if postype == "P"
            else if (postype.equals("PR")) {
                  //Get the C position number
                 Integer position = new Integer(components[1]);

                 tagName = m_TagPrefix + "PR[" + components[1] + "]" + m_TagSuffix;

                 //Get the target type
                 Integer targetType = (Integer) m_posRegTargetTypes.get(components[1]);


                String [] targetValues = new String [13];
                
                if (m_listOfPosRegisters.size() >= position.intValue())
                {
                    targetValues = (String []) m_listOfPosRegisters.get(position.intValue()-1);
                }
                else
                {
                    targetValues[0] = "N";
                    targetValues[1] = "U";
                    targetValues[2] = "T";
                    targetValues[3] = "0";
                    targetValues[4] = "0";
                    targetValues[5] = "0";
                    targetValues[6] = "0.0";
                    targetValues[7] = "0.0";
                    targetValues[8] = "0.0";
                    targetValues[9] = "0.0";
                    targetValues[10] = "0.0";
                    targetValues[11] = "0.0";
                }                 
                
                 //Process the joint target
                 
                 if(targetType != null && targetType.equals(new Integer(JOINT))) {
                     Element targetElem = super.createTarget( actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false); 
                     m_createJointTarget = true;
                     ArrayList [] jointTargetArrayList = new ArrayList [ m_numRobotAxes+m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
                    //Print out the target (joint) values
                    for(int ii = 0; ii < m_numRobotAxes; ii++)
                        formatJointValueInPulses( jointTargetArrayList, targetValues[ii+m_numRobotAxes] , null, ii);
           
                     boolean noJointTarget = false;
                     //If there are auxiliary axes present, print out their joint values
                    if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {
	                   	 if( (m_auxAxesGroupNumber != m_extAxesGroupNumber && m_auxAxesGroupNumber != m_workAxesGroupNumber && m_extAxesGroupNumber != m_workAxesGroupNumber) || m_extAxesGroupNumber == 0 || m_auxAxesGroupNumber == 0 || m_workAxesGroupNumber == 0)
	                	 {
	                    	if(m_numAuxAxes > 0 && m_auxAxesCount == m_numAuxAxes)
	                            noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfPosRegAuxTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes);
	                         if(m_numExtAxes > 0 && m_extAxesCount == m_numExtAxes)
	                            noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfPosRegExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING , m_numExtAxes);
	                         if(m_numWorkAxes > 0 && m_workAxesCount == m_numWorkAxes)
	                            noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfPosRegWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes);
	                        
	                          try {
	                              if(m_numAuxAxes > 0 && m_auxAxesCount != m_numAuxAxes) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Mismatch between Number of RailTrackGantry Axes in V5 Device and number counted in Program\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                              if(m_numExtAxes > 0 && m_extAxesCount != m_numExtAxes) {
	                                    noJointTarget = true;  
	                                    if (m_extAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Mismatch between Number of EndOfArmTooling Axes in V5 Device and number counted in Program");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_extAxesError = true;
	                                    }
	                              }
	                              if(m_numWorkAxes > 0 && m_workAxesCount != m_numWorkAxes) {
	                                    noJointTarget = true;  
	                                    if (m_workAxesError == false)
	                                    {
	                                      m_bw.write("FANUC PARSER ERROR: Mismatch between Number of Workpiece Axes in V5 Device and number counted in Program");
	                                      m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                      m_workAxesError = true;
	                                    }
	                              }    
	                          }
	                          catch (IOException e) {
	                                        e.getMessage();
	                                    }
	                	 }
	                   	 else
	                   	 {
	                          try {
	                              if(m_auxAxesGroupNumber == m_extAxesGroupNumber) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and EndOfArmTooling Axes (Toolgroup) can not be the same\n");
	                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                              if(m_auxAxesGroupNumber == m_workAxesGroupNumber) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and Workpiece Axes (Workgroup) can not be the same\n");
	                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                              if(m_workAxesGroupNumber == m_extAxesGroupNumber) {
	                                    noJointTarget = true;
	                                    if (m_auxAxesError == false)
	                                    {
	                                        m_bw.write("FANUC PARSER ERROR: Group number for EndOfArmToolng Axes (Toolgroup) and Workpiece Axes (Workgroup) can not be the same\n");
	                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
	                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                        m_auxAxesError = true;
	                                    }
	                              }
	                          }
	                          catch (IOException e) {
	                                        e.getMessage();
	                                    }	                   		
	                   	 }
                    }
                    
                    if (noJointTarget == false)
                        super.createJointTarget(targetElem, jointTargetArrayList);
                     
                    // Add PosReg index here for NRL teach 

                    processSpotAttribute(actElem, "PosType", "PR");                
                    processSpotAttribute(actElem, "PosReg", components[1]);                
 
                 }
                 else if(targetType != null && targetType.equals(new Integer(CARTESIAN))) {
                      processCartesianTarget(actElem, targetValues, components, true);
                }//end else if
                else {
                    targetValues[0] = "N";
                    targetValues[1] = "U";
                    targetValues[2] = "T";
                    targetValues[3] = "0";
                    targetValues[4] = "0";
                    targetValues[5] = "0";
                    targetValues[6] = "0.0";
                    targetValues[7] = "0.0";
                    targetValues[8] = "0.0";
                    targetValues[9] = "0.0";
                    targetValues[10] = "0.0";
                    targetValues[11] = "0.0";
                    processCartesianTarget(actElem, targetValues, components, true);
                }
                     
                String tagCommentValue = "";
                tagCommentValue = (String)m_tagComments.get(tagName);
                if (tagcomponent != null && !tagcomponent.equals("") && tagCommentValue.equals(""))
                    tagCommentValue = tagcomponent;

                if (tagCommentValue != null && !tagCommentValue.equals(""))
                     processSpotAttribute( actElem, "Pos Comment", tagCommentValue);                
                
                
                /******** old way without all the whistles and bells
                Integer position = new Integer(components[1]);
                String [] targetValues = new String [13];
                if (m_listOfPosRegisters.size() >= position.intValue())
                {
                    targetValues = (String []) m_listOfPosRegisters.get(position.intValue()-1);
                }
                else
                {
                    targetValues[0] = "N";
                    targetValues[1] = "U";
                    targetValues[2] = "T";
                    targetValues[3] = "0";
                    targetValues[4] = "0";
                    targetValues[5] = "0";
                    targetValues[6] = "0.0";
                    targetValues[7] = "0.0";
                    targetValues[8] = "0.0";
                    targetValues[9] = "0.0";
                    targetValues[10] = "0.0";
                    targetValues[11] = "0.0";
                }
                processCartesianTarget(actElem, targetValues, components,  true);
                 **********************/
            } // posotype PR
          

                
        }//end processTarget method

        private void processCartesianTarget(Element actElem, String [] targetValues, String [] components,  boolean posRegister) {
                Element targetElem = super.createTarget( actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, false);
                m_createJointTarget = false;
                ArrayList [] jointTargetArrayList = new ArrayList [ m_numAuxAxes+m_numExtAxes+m_numWorkAxes];

                if (m_currentToolType != null && m_currentToolType.equals("Stationary")) {
                    DNBIgpOlpUploadMatrixUtils matrixUtilities = new DNBIgpOlpUploadMatrixUtils();

                    matrixUtilities.dgXyzyprToMatrix(targetValues[6] + "," + targetValues[7] + "," + targetValues[8] + "," + targetValues[9] + "," + targetValues[10] + "," + targetValues[11]);
                    matrixUtilities.dgInvert();
                    targetValues[6] = matrixUtilities.dgGetX();
                    targetValues[7] = matrixUtilities.dgGetY();
                    targetValues[8] = matrixUtilities.dgGetZ();
                    targetValues[9] = matrixUtilities.dgGetYaw();
                    targetValues[10] = matrixUtilities.dgGetPitch();
                    targetValues[11] = matrixUtilities.dgGetRoll();
                }
                String cfgName = targetValues[0] + targetValues[1] + targetValues[2];               
               
                double [] cartValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                cartValues[0] = Double.valueOf(targetValues[6]).doubleValue() * 0.001;
                cartValues[1] = Double.valueOf(targetValues[7]).doubleValue() * 0.001;
                cartValues[2] = Double.valueOf(targetValues[8]).doubleValue() * 0.001;   
                cartValues[3] = Double.valueOf(targetValues[9]).doubleValue();
                cartValues[4] = Double.valueOf(targetValues[10]).doubleValue();
                cartValues[5] = Double.valueOf(targetValues[11]).doubleValue();               
                
                Element cartTargetElem = super.createCartesianTarget( targetElem, cartValues, cfgName);
                int [] turnValues = { 0, 0, 0, 0 };
                if (m_Turn5.equalsIgnoreCase("True") && m_Turn1.equalsIgnoreCase("False"))
                {
                	turnValues[0] = 0;
                	turnValues[1] = Integer.valueOf(targetValues[3]).intValue();
                	turnValues[2] = Integer.valueOf(targetValues[4]).intValue();
                	turnValues[3] = Integer.valueOf(targetValues[5]).intValue();                 
                }
                else
                {
                	turnValues[0] = Integer.valueOf(targetValues[3]).intValue();
                	turnValues[1] = Integer.valueOf(targetValues[4]).intValue();
                	turnValues[2] = 0;
                	turnValues[3] = Integer.valueOf(targetValues[5]).intValue();
                }
                super.createTurnNumbers( cartTargetElem, turnValues); 
                     boolean noJointTarget = false;
                     //If there are auxiliary axes present, print out their joint values
                    if (posRegister == false)
                    {
	                    if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {
	   	                   	 if( (m_auxAxesGroupNumber != m_extAxesGroupNumber && m_auxAxesGroupNumber != m_workAxesGroupNumber && m_extAxesGroupNumber != m_workAxesGroupNumber) || m_extAxesGroupNumber == 0 || m_auxAxesGroupNumber == 0 || m_workAxesGroupNumber == 0)
		                	 {                    	
	                        	if(m_numAuxAxes > 0 && m_auxAxesCount == m_numAuxAxes)
	                                noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfAuxTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes);
	                             if(m_numExtAxes > 0 && m_extAxesCount == m_numExtAxes)
	                                noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING, m_numExtAxes);
	                             if(m_numWorkAxes > 0 && m_workAxesCount == m_numWorkAxes)
	                                noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes);
	                              try {
	                                  if(m_numAuxAxes > 0 && m_auxAxesCount != m_numAuxAxes) {
	                                        noJointTarget = true;
	                                        if (m_auxAxesError == false)
	                                        {
	                                            m_bw.write("FANUC PARSER ERROR: Mismatch between Number of RailTrackGantry Axes in V5 Device and number counted in Program\n");
	                                            m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                            m_auxAxesError = true;
	                                        }
	                                  }
	                                  if(m_numExtAxes > 0 && m_extAxesCount != m_numExtAxes) {
	                                        noJointTarget = true;  
	                                        if (m_extAxesError == false)
	                                        {
	                                            m_bw.write("FANUC PARSER ERROR: Mismatch between Number of EndOfArmTooling Axes in V5 Device and number counted in Program\n");
	                                            m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                            m_extAxesError = true;
	                                        }
	                                  }
	                                  if(m_numWorkAxes > 0 && m_workAxesCount != m_numWorkAxes) {
	                                        noJointTarget = true;  
	                                        if (m_workAxesError == false)
	                                        {
	                                          m_bw.write("FANUC PARSER ERROR: Mismatch between Number of Workpiece Axes in V5 Device and number counted in Program\n");
	                                          m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                          m_workAxesError = true;
	                                        }
	                                  }
	                              }
	                              catch (IOException e) {
	                                            e.getMessage();
	                                        }
	                        }
	                        else
	                        {
		                          try {
		                              if(m_auxAxesGroupNumber == m_extAxesGroupNumber) {
		                                    noJointTarget = true;
		                                    if (m_auxAxesError == false)
		                                    {
		                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and EndOfArmTooling Axes (Toolgroup) can not be the same\n");
		                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
		                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
		                                        m_auxAxesError = true;
		                                    }
		                              }
		                              if(m_auxAxesGroupNumber == m_workAxesGroupNumber) {
		                                    noJointTarget = true;
		                                    if (m_auxAxesError == false)
		                                    {
		                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and Workpiece Axes (Workgroup) can not be the same\n");
		                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
		                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
		                                        m_auxAxesError = true;
		                                    }
		                              }
		                              if(m_workAxesGroupNumber == m_extAxesGroupNumber) {
		                                    noJointTarget = true;
		                                    if (m_auxAxesError == false)
		                                    {
		                                        m_bw.write("FANUC PARSER ERROR: Group number for EndOfArmToolng Axes (Toolgroup) and Workpiece Axes (Workgroup) can not be the same\n");
		                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
		                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
		                                        m_auxAxesError = true;
		                                    }
		                              }
		                          }
		                          catch (IOException e) {
		                                        e.getMessage();
		                                    }	                        	
	                        }
                        }
                    }
                    else
                    {
	                    if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {
	   	                   	 if( (m_auxAxesGroupNumber != m_extAxesGroupNumber && m_auxAxesGroupNumber != m_workAxesGroupNumber && m_extAxesGroupNumber != m_workAxesGroupNumber) || m_extAxesGroupNumber == 0 || m_auxAxesGroupNumber == 0 || m_workAxesGroupNumber == 0)
		                	 {                    	
	                    		if(m_numAuxAxes > 0 && m_auxAxesCount == m_numAuxAxes)
	                                noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfPosRegAuxTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes);
	                             if(m_numExtAxes > 0 && m_extAxesCount == m_numExtAxes)
	                                noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfPosRegExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING, m_numExtAxes);
	                             if(m_numWorkAxes > 0 && m_workAxesCount == m_numWorkAxes)
	                                noJointTarget = processExtendedTarget(jointTargetArrayList, components[1], m_listOfPosRegWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes);
	                              try {
	                                  if(m_numAuxAxes > 0 && m_auxAxesCount != m_numAuxAxes) {
	                                        noJointTarget = true;
	                                        if (m_auxAxesError == false)
	                                        {
	                                            m_bw.write("FANUC PARSER ERROR: Mismatch between Number of RailTrackGantry Axes in V5 Device and number counted in Program\n");
	                                            m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                            m_auxAxesError = true;
	                                        }
	                                  }
	                                  if(m_numExtAxes > 0 && m_extAxesCount != m_numExtAxes) {
	                                        noJointTarget = true;  
	                                        if (m_extAxesError == false)
	                                        {
	                                            m_bw.write("FANUC PARSER ERROR: Mismatch between Number of EndOfArmTooling Axes in V5 Device and number counted in Program\n");
	                                            m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                            m_extAxesError = true;
	                                        }
	                                  }
	                                  if(m_numWorkAxes > 0 && m_workAxesCount != m_numWorkAxes) {
	                                        noJointTarget = true;  
	                                        if (m_workAxesError == false)
	                                        {
	                                          m_bw.write("FANUC PARSER ERROR: Mismatch between Number of Workpiece Axes in V5 Device and number counted in Program\n");
	                                          m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
	                                          m_workAxesError = true;
	                                        }
	                                  }
	                              }
	                              catch (IOException e) {
	                                            e.getMessage();
	                                        }                     
	                    	}
	                    	else
	                    	{
		                          try {
		                              if(m_auxAxesGroupNumber == m_extAxesGroupNumber) {
		                                    noJointTarget = true;
		                                    if (m_auxAxesError == false)
		                                    {
		                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and EndOfArmTooling Axes (Toolgroup) can not be the same\n");
		                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
		                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
		                                        m_auxAxesError = true;
		                                    }
		                              }
		                              if(m_auxAxesGroupNumber == m_workAxesGroupNumber) {
		                                    noJointTarget = true;
		                                    if (m_auxAxesError == false)
		                                    {
		                                        m_bw.write("FANUC PARSER ERROR: Group number for RailTrackGantry Axes (Railgroup) and Workpiece Axes (Workgroup) can not be the same\n");
		                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
		                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
		                                        m_auxAxesError = true;
		                                    }
		                              }
		                              if(m_workAxesGroupNumber == m_extAxesGroupNumber) {
		                                    noJointTarget = true;
		                                    if (m_auxAxesError == false)
		                                    {
		                                        m_bw.write("FANUC PARSER ERROR: Group number for EndOfArmToolng Axes (Toolgroup) and Workpiece Axes (Workgroup) can not be the same\n");
		                                        m_bw.write("FANUC PARSER ERROR: Set all three parameters Railgroup, Toolgroup, and Workgroup to different values\n");
		                                        m_bw.write("FANUC PARSER ERROR: Auxiliary Axes will not be set\n\n");
		                                        m_auxAxesError = true;
		                                    }
		                              }
		                          }
		                          catch (IOException e) {
		                                        e.getMessage();
		                                    }                    		
	                    	}
                        }                       
                    }
                     if (noJointTarget == false)
                        super.createJointTarget(targetElem, jointTargetArrayList);                
                
                    String tagName = "";
                    if (posRegister == false) {

                           tagName = (String) m_tagNames.get(components[1]);                
 
                    }
                    else {
                        tagName = m_TagPrefix + "PR[" + components[1] + "]" + m_TagSuffix;
                    }
                    if (!m_vsCamClb.equals("")) {
                        m_vsCamClbTagCount++;
                        switch (m_vsCamClbTagCount)
                        {
                            case 1:
                                tagName = "VISION_SHIFT_" + m_vsCamClb + "_APPROACH1";
                                super.createTag( cartTargetElem, tagName); 
                            break;
                            case 2:
                                tagName = "VISION_SHIFT_" + m_vsCamClb + "_APPROACH2";
                                super.createTag( cartTargetElem, tagName); 
                            break;
                            case 3:
                                tagName = "VISION_SHIFT_" + m_vsCamClb + "_HOME";
                                String tagGroupName = "VISION_SHIFT_FIND" + m_vsCamClb;
                                super.createTag( cartTargetElem, tagName, tagGroupName,"");
                            break;
                            default:
                                tagName = "VISION_SHIFT_FIND" + m_vsCamClb + "_" + String.valueOf(m_vsCamClbTagCount-2);
                                tagGroupName = "VISION_SHIFT_FIND" + m_vsCamClb;
                                super.createTag( cartTargetElem, tagName, tagGroupName,"");
                            break;
                        }
                    }
                    else {
                        super.createTag( cartTargetElem, tagName);
                    }
          }
       
         private boolean processExtendedTarget(ArrayList [] jointTargetArrayList, String sExtendedTarget, ArrayList listOfExtendedTargets,  DNBIgpOlpUploadEnumeratedTypes.AuxAxisType extendedAxisType, int len) {
            
            Integer extendedTargetNum = new Integer(sExtendedTarget);
            if (listOfExtendedTargets == null || listOfExtendedTargets.size() < extendedTargetNum.intValue())
            {
                return(true);
            }
            String [] extendedValues = (String []) listOfExtendedTargets.get(extendedTargetNum.intValue()-1);
            int start = 1;
            String extendedAxisStrType = extendedAxisType.toString();
            if (extendedAxisStrType.equals("RailTrackGantry"))
            {
                start = m_auxAxesStart;
            }
            else if (extendedAxisStrType.equals("EndOfArmTooling"))
            {
                start = m_extAxesStart;
                for( int ii = start; ii < start + len; ii++) {
                    if (m_tipDistanceOffset > 0.0 || m_tipDistanceScale != 1.0)
                    {
                        double dblExtendedValue = Double.valueOf(extendedValues[ii-1]).doubleValue();
                        dblExtendedValue = (dblExtendedValue-m_tipDistanceOffset)/m_tipDistanceScale;
                        extendedValues[ii-1] = String.valueOf(dblExtendedValue);
                    }
                    if (m_arm1Length > 0.0 && m_arm2Length > 0.0)
                    {
                        double dblExtendedValue = Double.valueOf(extendedValues[ii-1]).doubleValue();
 
                        dblExtendedValue = SSSA( dblExtendedValue, m_arm1Length, m_arm2Length);
                        dblExtendedValue = Math.toDegrees(dblExtendedValue);
                        if (m_negateTipDistanceJoint == 1)
                            dblExtendedValue *= -1.0;
                        extendedValues[ii-1] = String.valueOf(dblExtendedValue);
                    }
                }
            }
            else
            {
                start = m_workAxesStart;
            }    
            
            for( int ii = start-1; ii < start + len - 1; ii++) {
                            
                    formatJointValueInPulses(jointTargetArrayList, extendedValues[ii], extendedAxisType, ii);
            
            }//end for
            return(false);
        }       
 
         private void processDOUTStatement(String line, Matcher match, Element activityListElem, String time) throws IllegalStateException{
            String portType = match.group(1);
            String portNumber = match.group(2);
            String signalName = "";
            String signalComment = match.group(3);
            String status = match.group(4);
            String  ioname;

            // System.out.println("DOUT matched" + ", " + portNumber + ", " + status);
  
            if (status.startsWith("R["))
            {
	            Pattern unparsedLine;
	
	            unparsedLine = Pattern.compile("^\\s*[0-9]*:(.*)", Pattern.CASE_INSENSITIVE);
	
	            Matcher    unparsedStmtLine = unparsedLine.matcher(line);
	
	            if(true == unparsedStmtLine.find()) {
	                String unparsedCommand = unparsedStmtLine.group(1); 
	                if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
	                  addToComments(activityListElem, unparsedCommand, false, true);
	                else
	                  addToComments(null, unparsedCommand, true, true); 
	            }
            }
            else
            {
	            boolean signalValue = false;
	            
	            if (status.equalsIgnoreCase("ON")) 
	                signalValue = true;
	            else
	                signalValue = false;
	            
	            signalName = portType + "[" + portNumber + "]";
	            
	            if (portType.equalsIgnoreCase("RO")) {
	                    int newPortNumber = Integer.valueOf(portNumber).intValue() + m_maxDigitalSignal;
	                    portNumber = String.valueOf(newPortNumber);
	            }
	            if (portType.equalsIgnoreCase("WO")) {
	                    int newPortNumber = Integer.valueOf(portNumber).intValue() + m_maxRobotSignal;
	                    portNumber = String.valueOf(newPortNumber);
	            }
	             if (portType.equalsIgnoreCase("GO")) {
	                    int newPortNumber = Integer.valueOf(portNumber).intValue() + m_maxWeldSignal;
	                    portNumber = String.valueOf(newPortNumber);
	            }
	            
	            if(!time.equals("0"))
	                ioname = "Set-" + signalName + " = " + status + " [" + time + "s]";
	            else
	                ioname = "Set-" + signalName + " = " + status;            
	        
	                Element actElem = super.createSetIOActivity(activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(time));
	                m_lastActElem = actElem;
	              if (portType.equalsIgnoreCase("GO")) {
	                  processSpotAttribute(actElem, "GroupValue", status);
	              }
	              processSpotAttribute(actElem, "SignalComment", signalComment);
	                processComments(actElem, "Pre");
            }
         }

        
        private void processPULSEStatement(String line, Matcher match, Element activityListElem) throws IllegalStateException{
            String portNumber = match.group(2);
            String signalName = match.group(3);
            String hasTime = "always";
            String fullSeconds = match.group(4);
            String dot = match.group(5);
            String decimal = match.group(6);

            String timeInSec;

            String doutOn = "1: DO[" + portNumber + ":" + signalName + "] = ON";
            //String doutOff = "DOUT OT#(" + portNumber + ") OFF";

            Matcher matchDoutOn = this.m_keywords[5].matcher(doutOn);
            //Matcher matchDoutOff = this.m_keywords[5].matcher(doutOff);

            if(hasTime != null) {
                fullSeconds = (fullSeconds == null)?"":fullSeconds;
                dot = (dot == null)?"":dot;
                decimal = (decimal == null)?"":decimal;
                timeInSec = fullSeconds + dot + decimal;
             }
            else
                 timeInSec = "0.3";

            //String timer = "TIMER=" + timeInSec;

            if(matchDoutOn.find() == true)
                processDOUTStatement(line, matchDoutOn, activityListElem, timeInSec);

            //processTimerStatement(timer, activityListElem);
            
            //if(matchDoutOff.find() == true)
                //processDOUTStatement(matchDoutOff, activityListElem);

            // System.out.println("PULSE matched" + ", " + portNumber + ", " + hasTime + ", " + fullSeconds + ", " + dot + ", " + decimal);
        }

       private void processWAITStatement(String line, Matcher match, Element activityListElem) {
            String portType = match.group(1);
            String portNumber = match.group(2);
            String signalComment = match.group(3);
            String signalName = "";
            String status = match.group(4);
            boolean signalValue = false;

            String time;
            
            String fullSeconds = match.group(5);
            String dot = match.group(6);
            String decimal = match.group(7);
            
            fullSeconds = (fullSeconds == null)?"":fullSeconds;
            dot = (dot == null)?"":dot;
            decimal = (decimal == null)?"":decimal;
            time = fullSeconds + dot + decimal;                      
            
            if (status.equalsIgnoreCase("ON")) 
                signalValue = true;
            else
                signalValue = false;            

            // System.out.println("WAIT matched" + ", " + portNumber + ", " + status);
            
           
            signalName = portType + "[" + portNumber + "]";
          
            if (portType.equalsIgnoreCase("RI")) {
                    int newPortNumber = Integer.valueOf(portNumber).intValue() + m_maxDigitalSignal;
                    portNumber = String.valueOf(newPortNumber);
            }
            if (portType.equalsIgnoreCase("WI")) {
                    int newPortNumber = Integer.valueOf(portNumber).intValue() + m_maxRobotSignal;
                    portNumber = String.valueOf(newPortNumber);
            }
            if (portType.equalsIgnoreCase("GI")) {
                    int newPortNumber = Integer.valueOf(portNumber).intValue() + m_maxWeldSignal;
                    portNumber = String.valueOf(newPortNumber);
            }            
             
            String ioname = "Wait Until " + signalName + " = " + status;
            if (!time.equals(""))
            {
                ioname += " " + time + "(sec)";
            }
            else
            {
            	time = "0.0";	
            }
  
            if (m_commentWaitSignal == 0 && !status.startsWith("R[") && line.indexOf("AND") < 0 && line.indexOf("OR") < 0)
            {
            	int labelIndex = line.indexOf("LBL[");
            	String labelStr = "";
            	if (labelIndex > 0)
            	{
                   	if (line.indexOf("TIMEOUT") > 0)
                	{
                		labelIndex = line.indexOf("TIMEOUT");
                		time = "2.5";
                	}            		
            	    int bracketIndex = line.indexOf("]", labelIndex);
            	    if (bracketIndex > labelIndex)
            	    {
            	        labelStr = line.substring(labelIndex,bracketIndex+1);
            	    }
            	}
                Element actElem = super.createWaitForIOActivity( activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(time));
                m_lastActElem = actElem;

                  if (portType.equalsIgnoreCase("GI")) {
                      processSpotAttribute(actElem, "GroupValue", status);
                  }
                  if (!labelStr.equals(""))
                  {
                	  processSpotAttribute(actElem,"TimeoutLabel", labelStr);
                  }
                  processSpotAttribute(actElem, "SignalComment", signalComment);
                  processComments( actElem, "Pre");
            }
            else
            {
              Pattern unparsedLine;

              unparsedLine = Pattern.compile("^\\s*[0-9]*:(.*)", Pattern.CASE_INSENSITIVE);

              Matcher    unparsedStmtLine = unparsedLine.matcher(line);

              if(true == unparsedStmtLine.find()) {
                  String unparsedCommand = unparsedStmtLine.group(1); 
                  if (m_OLPStyle.equalsIgnoreCase("OperationComments") || m_OLPStyle.equalsIgnoreCase("Honda"))
                    addToComments(activityListElem, unparsedCommand, false, true);
                  else
                    addToComments(null, unparsedCommand, true, true);
              }
            }
   
        }

        private void processSpotStatement(String line, Element activityListElem, String gunName ) {
            
            Pattern spotcmd = Pattern.compile("SPOT\\[\\s*(E=|EQ=|BU=|SD=)?([\\(\\*0-9C-O,\\)]*)?\\s*,?\\s*(P=)?([\\(0-9,\\)]*)?\\s*,?\\s*(S=)?([\\(0-9,R\\[\\)]*)?\\s*,?\\s*(EQ=)?([\\(0-9,\\)]*)?\\s*,?\\s*(BU=|ED=)?([\\(\\*0-9C-O,\\)]*)?", Pattern.CASE_INSENSITIVE);
            Matcher mspot = spotcmd.matcher(line);
            Pattern spotMacroCmd = Pattern.compile("^\\s*[0-9]*:(.*);");
            Matcher mMacroSpot = spotMacroCmd.matcher(line);                  
            if(true == mspot.find())
            {
                String Equipment = "";
                String Backup1 = "";
                String Pressure_lmh = "";
                String scheduleToken = null;
                String weldSched = "";
                String Backup2 = "";
                
                String Macro = "";
                
                if (mMacroSpot.find())
                {
                	Macro = mMacroSpot.group(1);
                }
                
                if (mspot.group(1) != null && mspot.group(2) != null)
                {
                    Backup1 = mspot.group(1) + mspot.group(2);
                }
                if (mspot.group(3) != null && mspot.group(4) != null)
                {
                    Pressure_lmh = mspot.group(3) + mspot.group(4);
                }
                if (mspot.group(5) != null && mspot.group(6) != null)
                {
                    scheduleToken = mspot.group(5);
                    weldSched = mspot.group(5) + mspot.group(6);
                }
                if (mspot.group(7) != null && mspot.group(8) != null)
                {
                    Equipment = mspot.group(7) + mspot.group(8);
                }            
                if (mspot.group(9) != null && mspot.group(10) != null)
                {
                    Backup2 = mspot.group(9) + mspot.group(10);
                }                            
                
                int index = Backup1.lastIndexOf(',');
                if (index > 0 && index == Backup1.length()-1)
                    Backup1 = Backup1.substring(0, index);
                index = Pressure_lmh.lastIndexOf(',');
                if (index > 0 && index == Pressure_lmh.length()-1)
                    Pressure_lmh = Pressure_lmh.substring(0, index);                
                index = weldSched.lastIndexOf(',');
                if (index > 0 && index == weldSched.length()-1)
                    weldSched = weldSched.substring(0, index);   
                index = Equipment.lastIndexOf(',');
                if (index > 0 && index == Equipment.length()-1)
                    Equipment = Equipment.substring(0, index);
                index = Backup2.lastIndexOf(',');
                if (index > 0 && index == Backup2.length()-1)
                    Backup2 = Backup2.substring(0, index); 

                if (weldSched.indexOf("R[") > 0)
                {
                    weldSched += "]";
                }
                
                String weldHomeName = (String)m_weldScheduleHomes.get( weldSched );
                String backupHomeName = (String)m_backupHomes.get( Backup2 );
                String weldScheduleDelay = (String)m_weldScheduleDelays.get( weldSched );
                
                String delayTime = ".5";
                if (weldScheduleDelay != null)
                    delayTime = weldScheduleDelay;
                
                /* weld home move */
                
                if (weldHomeName == null)
                    weldHomeName = (String)m_listofHomeNames.get( 0 );

                if (backupHomeName == null) {
                    if (m_listofHomeNames.size() > 1) {
                    backupHomeName = (String)m_listofHomeNames.get( 1 );
                    }
                    else{
                        backupHomeName = "Home_2";
                    }
                }
                
                //Create DOM Nodes and set appropriate attibutes for TPESpotWeld
                // NOTE: ONLY FOR NON-SERVO (AIR GUN) UPLOADS
                    
                //Create DOM Nodes and set appropriate attributes
                String spotActionName = "TPESpotWeld." + String.valueOf(m_spotCounter);
                Element actionElem = super.createActionHeader( activityListElem, spotActionName, "TPESpotWeld", gunName); 
                m_lastActElem = actionElem;

                // if there is no schedule token "S=" then must be a weld timer table index
                if (scheduleToken == null && Macro.indexOf("S=") < 0 && mspot.group(2) != null) { 
                    String timerTableName = "WSUWeldTimeTable." + mspot.group(2);
                    LinkedList [] llEmptyAttribs = new LinkedList[0];
                    super.createUserProfile(super.getUserProfileListElement(), "WSUWeldTimeTable", timerTableName, llEmptyAttribs);
                    actionElem.setAttribute("TimerTableName", timerTableName);
                }
                
                processSpotAttribute( actionElem, "Equipment", Equipment);
                processSpotAttribute( actionElem, "Weld_sched", weldSched);
                processSpotAttribute( actionElem, "Pressure_lmh", Pressure_lmh);
                processSpotAttribute( actionElem, "Backup1", Backup1);
                processSpotAttribute( actionElem, "Backup2", Backup2); 
                if (scheduleToken == null && Macro.indexOf("S=") > 0) 
                {
                	processSpotAttribute( actionElem, "Macro", Macro);                	
                }                
                processSpotAttribute( actionElem, "Motion Option", "");
                processSpotAttribute( actionElem, "Pos Comment", "");
                Element activityList = super.createActivityListWithinAction(actionElem);             
                processSpotHomeMove( weldHomeName, activityList );

                /* delay */

                createTimerStatement( delayTime, activityList, false );

                /* open/backup home move */


                processSpotHomeMove( backupHomeName, activityList );

                m_spotCounter++;
                
                if (m_servoUpload == 1) {                   
	  	              // SERVO GUN RRS ATTRIBUTES ADDED FOR R13 SP2
	  	              // R20SP4 create these always...motion planner will avoid simulating both RRS and action
	  	            if (m_servoAttribsAdded == 0 && m_firstRobotMotion != null) {
	  	                if (m_lastRobotMotion == m_firstRobotMotion) {
	  	                    setRRSServoAttribs( m_firstRobotMotion, line);
	  	                }   
	  	                else {
	  	                    createRRSServoAttribs(m_firstRobotMotion);
	  	                }                
	  	            }
	  	            if (m_lastRobotMotion != null && m_lastRobotMotion != m_firstRobotMotion) {
	  	                setRRSServoAttribs(m_lastRobotMotion, line);
	  	            }                	
                } // servo upload
            
            } // spot find          
        }

        private void processBackupStatement(String line, Element activityListElem, String gunName ) {
            
            Pattern backupcmd = Pattern.compile("BACKUP\\[([\\(A-Z0-9,\\)]*)\\s*", Pattern.CASE_INSENSITIVE);
            Matcher mback = backupcmd.matcher(line);
           
            if(true == mback.find())
            {
                String Backup = mback.group(1);
 
                            //Create DOM Nodes and set appropriate attributes
                String backupActionName = "TPESpotRetract." + String.valueOf(m_backupCounter);
                Element actionElem = super.createActionHeader( activityListElem, backupActionName, "TPESpotRetract", gunName); 
                m_lastActElem = actionElem;


                processSpotAttribute( actionElem, "Macro", "");
                
                String backupHomeName = (String)m_backupHomes.get( Backup );

                Element activityList = super.createActivityListWithinAction( actionElem );                

                

                /* open/backup home move */
                
                if (backupHomeName == null) {
                    if (m_listofHomeNames.size() > 1) {
                    backupHomeName = (String)m_listofHomeNames.get( 1 );
                    }
                    else{
                        backupHomeName = "Home_2";
                    }
                }
                processSpotHomeMove( backupHomeName, activityList );
                
                m_backupCounter++;
            }
        }
        
       private void processSpotAttribute(Element actElem, String attribNameValue, String attribVal ) {
            String [] attribNames = new String [1];
            String [] attribValues = new String [1];
            attribNames[0] = attribNameValue;
            attribValues[0] = attribVal;
            super.createAttributeList(actElem, attribNames, attribValues);
            processComments( actElem, "Pre");
       }
         private void processMacroStatement(String macroStr, String macroAction, Element activityListElem, String gunName ) {
                Element zoneAct = null;
                Element actElem = null;
                Element activityInActionElem = null;
                Element actionActivityList = null;
                Element toolResource = null;
                
                int index = 0;
                index = macroAction.indexOf('.');
                 
                if (index >= 0) {
                     String macroActionStore = new String("");
                     macroActionStore = macroAction;
                     gunName = macroAction.substring(index + 1, macroActionStore.length());
                     macroAction = macroActionStore.substring(0, index);
                }
               
                if (macroAction.length() >= 20 && macroAction.substring(0,20).equalsIgnoreCase("DNBEnterZoneActivity")) {
                    //Create DOM Nodes and set appropriate attributes
                    String activityName = "DNBEnterZoneActivity." + String.valueOf(m_enterZoneCounter);
                    actElem = super.createZoneActivity( activityListElem, activityName, DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType.ENTER_ZONE, macroStr);
 

                    m_enterZoneCounter++;
                }
                else if (macroAction.length() >= 20 && macroAction.substring(0,20).equalsIgnoreCase("DNBClearZoneActivity")) {
                    String activityName = "DNBClearZoneActivity." + String.valueOf(m_clearZoneCounter);
                    //Create DOM Nodes and set appropriate attributes
                    actElem = super.createZoneActivity( activityListElem, activityName, DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType.CLEAR_ZONE, macroStr);
                    m_clearZoneCounter++;
                }
                else
                {
                    //Create DOM Nodes and set appropriate attributes

                    String activityName = macroAction.trim() + "." + String.valueOf(m_macroCounter);
                    
                    actElem = super.createActionHeader( activityListElem, activityName, macroAction.trim(), gunName);
                    actionActivityList = super.createActivityListWithinAction( actElem);
                }

                
                if (macroAction.length() >= 11 && macroAction.substring(0,11).equalsIgnoreCase("TPESpotPick")) {
 
                     if (m_pickHome.length() > 0) {
                        processSpotHomeMove( m_pickHome, actionActivityList);
                     }
                    
                   //Create DOM Nodes and set appropriate attributes

                    String grabActName = "GrabActivity."  + String.valueOf(m_grabCounter);

                    activityInActionElem = super.createGrabActivity( actionActivityList, grabActName, m_grabPart, m_partGrabbed);
                    m_grabCounter++;
                                            
                }
                else if (macroAction.length() >= 11 && macroAction.substring(0,11).equalsIgnoreCase("TPESpotDrop")) {
                    
                      if (m_dropHome.length() > 0) {
                        processSpotHomeMove( m_dropHome, actionActivityList);
                     }
                    
                     String releaseActName = "ReleaseActivity."  + String.valueOf(m_releaseCounter);
                     
                     activityInActionElem = super.createReleaseActivity( actionActivityList, releaseActName, m_partGrabbed);
                    
                    m_releaseCounter++;
                }
                else if (macroAction.length() >= 11 && macroAction.substring(0,11).equalsIgnoreCase("TPEToolPick")) {
                                        
                   //Create DOM Nodes and set appropriate attributes

                    String mountActivityName = "DNBIgpMountActivity."  + String.valueOf(m_mountCounter);
                    String localMountedToolName = new String(m_mountedToolName);
                    if (localMountedToolName.equals(""))
                        localMountedToolName = gunName;
                    
                    String mountToolProfileName = localMountedToolName + ".ToolFrame";

                    for(int jj = 0; jj < m_toolNumberMapping.size(); jj++) {
                        String currentToolName = (String)m_toolNumberMapping.get(String.valueOf(jj));
                        String compName = localMountedToolName;
                        int dotindex = 0;
                        dotindex = localMountedToolName.indexOf('.');

                        if (dotindex >= 0) {
                            compName = localMountedToolName.substring(0, dotindex);                   
                        }
                        if (currentToolName != null && currentToolName.startsWith(compName))
                        {
                            mountToolProfileName = currentToolName;
                            break;
                        }
                    }
                    
                    activityInActionElem = super.createMountActivity( actionActivityList, mountActivityName, localMountedToolName, mountToolProfileName);
                    m_mountCounter++;    
                }
                else if (macroAction.length() >= 11 && macroAction.substring(0,11).equalsIgnoreCase("TPEToolDrop")) {
                    //Create DOM Nodes and set appropriate attributes

                    String unmountActivityName = "DNBIgpUnMountActivity."  + String.valueOf(m_unmountCounter);
                  
                    String localMountedToolName = new String(m_mountedToolName);
                    if (localMountedToolName.equals(""))
                        localMountedToolName = gunName;
                                     
                    activityInActionElem = super.createUnMountActivity( actionActivityList, unmountActivityName, localMountedToolName, m_dropToolProfileName);
                }  


                processSpotAttribute( actElem, "Macro", macroStr);
                processComments(actElem, "Pre");
                m_lastActElem = actElem;
                
                m_macroCounter++;  
         }
         

        private void processSpotHomeMove(String homeName, Element actListElem ) {
            
            String activityName = "DNBRobotMotionActivity." + String.valueOf(m_gunmoveCounter);
 
            super.createMotionActivityWithinAction( actListElem, activityName, Double.valueOf("0.0"), Double.valueOf("0.0"), DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,  homeName);
        }       
        
        public void createRRSServoAttribs(Element actElem) {
        	// rcs less than 6.31 uses old variables with emulation
        	if ((6.31 - m_rcsVersion) > .001)
        	{
        		String [] rrsAttribNames = {"_do_spot_weld", "_servo_spot_schedule\\srvgun_eq_wld", "_servo_spot_schedule\\srvgun_eq_aftwld", "_servo_spot_schedule\\srvgun_wldsch_gn1", "_servo_spot_schedule\\srvgun_wldsch_gn2",
        				"_servo_spot_schedule\\srvgun_prs", "_servo_spot_schedule\\srvgun_bu", "_weld_duration", "_srvgn1_setup\\rbtclosdir", "_srvgn1_setup\\tipclosdir",
        				// following are for spot emulation which does not exist in RCS version 6 or lower
        				"_srvgn1_spot_emulation_setup\\joint_num", "_srvgn1_spot_emulation_setup\\gun_closed_joint_value", "_sgsch1\\sftch_ta", "_sgsch1\\sftch_tb", "_sgsch1\\soft_touch", "_sgsch1\\sync_ta",
        				"_sgsch1\\sync_tb", "_sgsch1\\sync_term", "_sgsch1\\sync_acc", "_sgsch1\\decel_rate", "_sgsch1\\prcls_term", "_sgsch1\\prcls_acc", "_sgsch1\\sftopn_ta", "_sgsch1\\sftopn_tb",
        				"_sgsch1\\sftopn_term", "_sgsch1\\sftopn_acc", "_sgsch1\\away_term", "_sgsch1\\away_acc", "_sgsch1\\gun_open", "_sgsch1\\thickness", "_sgsch1\\push_depth", "_sgback1\\stroke"};
        		String [] rrsAttribValues = {"0", "0", "0", "0",
        				"0", "0", "0", "0", "0", "0",
        				// following are for spot emulation which does not exist in RCS version 6 or lower
        				"7", "0.0", "0", "0", "1", "0",
        				"0", "0", "100", "100", "0", "100", "0", "0",
        				"0", "100", "0", "100", "1", "0", "0", "1"};
        		String [] rrsAttribTypes = {"integer", "integer", "integer", "integer",
        				"integer", "integer", "integer", "double", "integer", "integer",
        				// following are for spot emulation which does not exist in RCS version 6 or lower
        				"integer", "double", "double", "double", "integer", "double",
        				"double", "integer", "integer", "integer","integer", "integer", "double", "double",
        				"integer", "integer", "integer", "integer", "integer", "double", "double", "double"};
        		super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);
        	}
        	// rcs less than 7.0 but greater than or equal to 6.31 uses old variable but no emulation        	
        	else if ((7.0 - m_rcsVersion) > .001)
        	{
        		String [] rrsAttribNames = {"_do_spot_weld", "_servo_spot_schedule\\srvgun_eq_wld", "_servo_spot_schedule\\srvgun_eq_aftwld", "_servo_spot_schedule\\srvgun_wldsch_gn1", "_servo_spot_schedule\\srvgun_wldsch_gn2",
        				"_servo_spot_schedule\\srvgun_prs", "_servo_spot_schedule\\srvgun_bu", "_weld_duration", "_srvgn1_setup\\rbtclosdir", "_srvgn1_setup\\tipclosdir"};
        		String [] rrsAttribValues = {"0", "0", "0", "0",
        				"0", "0", "0", "0", "0", "0"};
        		String [] rrsAttribTypes = {"integer", "integer", "integer", "integer",
        				"integer", "integer", "integer", "double", "integer", "integer"};
        		super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);
        	}
        	// rcs greater than or equal to 7.0 uses new _v7 variables        	
        	else
        	{
        		String [] rrsAttribNames = {"_do_spot_weld", "_v7_servo_spot_schedule\\gn1_start_dist_sch", "_v7_servo_spot_schedule\\gn2_start_dist_sch", "_v7_servo_spot_schedule\\gn1_pressure_sch", "_v7_servo_spot_schedule\\gn2_pressure_sch",
        				"_v7_servo_spot_schedule\\gn1_weld_sch", "_v7_servo_spot_schedule\\gn2_weld_sch", "_v7_servo_spot_schedule\\gn1_end_dist_sch", "_v7_servo_spot_schedule\\gn2_end_dist_sch", "_weld_duration", "_srvgn1_setup\\rbtclosdir", "_srvgn1_setup\\tipclosdir"};
        		String [] rrsAttribTypes = {"integer", "integer", "integer", "integer",
        				"integer", "integer", "integer", "integer","integer", "double", "integer", "integer"};
        		String [] rrsAttribValues = {"0", "0", "0", "0",
        				"0", "0", "0", "0", "0", "0", "0", "0"};

        		super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);
        	}
            m_servoAttribsAdded = 1;
        }
        public void setRRSServoAttribs( Element actElem, String line) {
        	// rcs less than 6.31 uses old variables with emulation
        	if ((6.31 - m_rcsVersion) > .001)
        	{
            	//  GROUPS                                      1      2                 3     4                5        6      7                 8           9              10     11                12     13
                Pattern spotcmd = Pattern.compile("SPOT\\[\\s*(EQ=)?([0-9]*)?\\s*,?\\s*(P=)?([0-9]*)?\\s*,?\\s*(S=)\\s*(\\()?([0-9]*)\\s*,?\\s*([0-9]*)?\\s*(\\))?\\s*,?\\s*(EQ=)?([0-9]*)?\\s*,?\\s*(BU=)?([0-9]*)?", Pattern.CASE_INSENSITIVE);
                Matcher mspot = spotcmd.matcher(line);
               
                
                if(true == mspot.find())
                {            	
	        		String [] rrsAttribNames = {"_do_spot_weld","_servo_spot_schedule\\srvgun_eq_wld", "_servo_spot_schedule\\srvgun_eq_aftwld", "_servo_spot_schedule\\srvgun_wldsch_gn1", "_servo_spot_schedule\\srvgun_wldsch_gn2",
	        				"_servo_spot_schedule\\srvgun_prs", "_servo_spot_schedule\\srvgun_bu", "_weld_duration", "_srvgn1_setup\\rbtclosdir", "_srvgn1_setup\\tipclosdir",
	        				// following are for spot emulation which does not exist in RCS version 6 or lower
	        				"_srvgn1_spot_emulation_setup\\joint_num", "_srvgn1_spot_emulation_setup\\gun_closed_joint_value", "_sgsch1\\sftch_ta", "_sgsch1\\sftch_tb", "_sgsch1\\soft_touch", "_sgsch1\\sync_ta",
	        				"_sgsch1\\sync_tb", "_sgsch1\\sync_term", "_sgsch1\\sync_acc", "_sgsch1\\decel_rate", "_sgsch1\\prcls_term", "_sgsch1\\prcls_acc", "_sgsch1\\sftopn_ta", "_sgsch1\\sftopn_tb",
	        				"_sgsch1\\sftopn_term", "_sgsch1\\sftopn_acc", "_sgsch1\\away_term", "_sgsch1\\away_acc", "_sgsch1\\gun_open", "_sgsch1\\thickness", "_sgsch1\\push_depth", "_sgback1\\stroke"};
	        		String [] rrsAttribValues = {"1", "1", "1", "1", "0",
	        				"1", "1", "125", "1", "0",
	        				// following are for spot emulation which does not exist in RCS version 6 or lower
	        				"7", "0.0", "0", "0", "1", "0",
	        				"0", "0", "100", "100", "0", "100", "0", "0",
	        				"0", "100", "0", "100", "1", "0", "0", "1"};
	        		String [] rrsAttribTypes = {"integer", "integer", "integer", "integer",
	        				"integer", "integer", "integer", "double", "integer", "integer",
	        				// following are for spot emulation which does not exist in RCS version 6 or lower
	        				"integer", "double", "double", "double", "integer", "double",
	        				"double", "integer", "integer", "integer","integer", "integer", "double", "double",
	        				"integer", "integer", "integer", "integer", "integer", "double", "double", "double"};
                	
	        		if (mspot.group(2) != null && !mspot.group(2).equals(""))
                	{
                		rrsAttribValues[0] = mspot.group(2);
                	}
                
                	if (mspot.group(11) != null && !mspot.group(11).equals(""))
                	{
                		rrsAttribValues[1] = mspot.group(11);
                	}
                	
                	if (mspot.group(7) != null && !mspot.group(7).equals(""))
                	{
                		rrsAttribValues[2] = mspot.group(7);
                	} 
                	
                	if (mspot.group(8) != null && !mspot.group(8).equals(""))
                	{
                	    rrsAttribValues[3] = mspot.group(8);
                	}   
                	
                	if (mspot.group(4) != null && !mspot.group(4).equals(""))
                	{
                		rrsAttribValues[4] = mspot.group(4);
                	}               	          
                	
                	if (mspot.group(13) != null && !mspot.group(13).equals(""))
                	{
                		rrsAttribValues[5] = mspot.group(13);
                	}               	                          	
                	super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);
                }
            }
        	// rcs less than 7.0 but greater than or equal to 6.31 uses old variable but no emulation
        	else if ((7.0 - m_rcsVersion) > .001)
        	{
            	//  GROUPS                                      1      2                 3     4                5        6      7                 8           9              10     11                12     13
                Pattern spotcmd = Pattern.compile("SPOT\\[\\s*(EQ=)?([0-9]*)?\\s*,?\\s*(P=)?([0-9]*)?\\s*,?\\s*(S=)\\s*(\\()?([0-9]*)\\s*,?\\s*([0-9]*)?\\s*(\\))?\\s*,?\\s*(EQ=)?([0-9]*)?\\s*,?\\s*(BU=)?([0-9]*)?", Pattern.CASE_INSENSITIVE);
                Matcher mspot = spotcmd.matcher(line);
               
                
                if(true == mspot.find())
                {            	
	        		String [] rrsAttribNames = {"_do_spot_weld", "_servo_spot_schedule\\srvgun_eq_wld", "_servo_spot_schedule\\srvgun_eq_aftwld", "_servo_spot_schedule\\srvgun_wldsch_gn1", "_servo_spot_schedule\\srvgun_wldsch_gn2",
	        				"_servo_spot_schedule\\srvgun_prs", "_servo_spot_schedule\\srvgun_bu",  "_weld_duration", "_srvgn1_setup\\rbtclosdir", "_srvgn1_setup\\tipclosdir"};
	        		String [] rrsAttribValues = {"1", "1", "1", "1", "0",
	        				"1", "1", "125", "1", "0"};
	        		String [] rrsAttribTypes = {"integer", "integer", "integer", "integer",
	        				"integer", "integer", "integer", "double", "integer", "integer"};
	        	
	        		if (mspot.group(2) != null && !mspot.group(2).equals(""))
                	{
                		rrsAttribValues[0] = mspot.group(2);
                	}
                
                	if (mspot.group(11) != null && !mspot.group(11).equals(""))
                	{
                		rrsAttribValues[1] = mspot.group(11);
                	}
                	
                	if (mspot.group(7) != null && !mspot.group(7).equals(""))
                	{
                		rrsAttribValues[2] = mspot.group(7);
                	} 
                	
                	if (mspot.group(8) != null && !mspot.group(8).equals(""))
                	{
                	    rrsAttribValues[3] = mspot.group(8);
                	}   
                	
                	if (mspot.group(4) != null && !mspot.group(4).equals(""))
                	{
                		rrsAttribValues[4] = mspot.group(4);
                	}               	          
                	
                	if (mspot.group(13) != null && !mspot.group(13).equals(""))
                	{
                		rrsAttribValues[5] = mspot.group(13);
                	}               	                          		        		
                	super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);
                }
        	}        	
        	// rcs greater than or equal to 7.0 uses new _v7 variables
            else
            {
                // GROUP                                        1        2      3                   4           5               6       7       8                 9           10              11      12      13               14          15              16        17      18                19          
            	Pattern spotcmd = Pattern.compile("SPOT\\[\\s*(SD=)?\\s*(\\()?([0-9]*)?\\s*,?\\s*([0-9]*)?\\s*(\\))?\\s*,?\\s*(P=)?\\s*(\\()?([0-9]*)?\\s*,?\\s*([0-9]*)?\\s*(\\))?\\s*,?\\s*(S=)\\s*(\\()?([0-9]*)\\s*,?\\s*([0-9]*)?\\s*(\\))?\\s*,?\\s*(ED=)?\\s*(\\()?([0-9]*)?\\s*,?\\s*([0-9]*)?\\s*(\\))?", Pattern.CASE_INSENSITIVE);
                Matcher mspot = spotcmd.matcher(line);
               
                
                if(true == mspot.find())
                {            	           	
	        		String [] rrsAttribNames = {"_do_spot_weld", "_v7_servo_spot_schedule\\gn1_start_dist_sch", "_v7_servo_spot_schedule\\gn2_start_dist_sch", "_v7_servo_spot_schedule\\gn1_pressure_sch", "_v7_servo_spot_schedule\\gn2_pressure_sch",
	        				"_v7_servo_spot_schedule\\gn1_weld_sch", "_v7_servo_spot_schedule\\gn2_weld_sch", "_v7_servo_spot_schedule\\gn1_end_dist_sch", "_v7_servo_spot_schedule\\gn2_end_dist_sch", "_weld_duration", "_srvgn1_setup\\rbtclosdir", "_srvgn1_setup\\tipclosdir"};
	        		String [] rrsAttribTypes = {"integer", "integer", "integer", "integer",
	        				"integer", "integer", "integer", "integer","integer", "double", "integer", "integer"};
	        		String [] rrsAttribValues = {"1", "1", "0", "1", "0",
	        				"1", "0", "1", "0", "125", "1", "0"};
	        		
	        		if (mspot.group(3) != null && !mspot.group(3).equals(""))
                	{
                		rrsAttribValues[0] = mspot.group(3);
                	}
                
                	if (mspot.group(4) != null && !mspot.group(4).equals(""))
                	{
                		rrsAttribValues[1] = mspot.group(4);
                	}
                	
                	if (mspot.group(8) != null && !mspot.group(8).equals(""))
                	{
                		rrsAttribValues[2] = mspot.group(8);
                	} 
                	
                	if (mspot.group(9) != null && !mspot.group(9).equals(""))
                	{
                	    rrsAttribValues[3] = mspot.group(9);
                	}   
                	
                	if (mspot.group(13) != null && !mspot.group(13).equals(""))
                	{
                		rrsAttribValues[4] = mspot.group(13);
                	}               	          
                	
                	if (mspot.group(14) != null && !mspot.group(14).equals(""))
                	{
                		rrsAttribValues[5] = mspot.group(14);
                	}   
        
                	if (mspot.group(18) != null && !mspot.group(18).equals(""))
                	{
                		rrsAttribValues[6] = mspot.group(18);
                	}               	          
                	
                	if (mspot.group(19) != null && !mspot.group(19).equals(""))
                	{
                		rrsAttribValues[7] = mspot.group(19);
                	}               	                          		                	
                	super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);
                }            	
            }    
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                     UTILITY FUNCTIONS
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////
       public void formatJointValueInPulses(ArrayList [] jointTargetArrayList, String sTarget, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxJointType, int ii) {
           
           double targetScale = 1.0;
           String jointName = "";
           String axesType = "";
           int index = ii;
           int arrayIndex = ii;
            
            if (auxJointType != null)
                index = ii+m_numRobotAxes;
                
            if (m_createJointTarget == true)
                arrayIndex = index;
           
            axesType = m_axisTypes[index];
            if (axesType.equals("Translational") )
                targetScale = .001;   
            jointName = "Joint" + String.valueOf(index+1);
            
            if(auxJointType != null) {
                jointTargetArrayList[arrayIndex] = new ArrayList(6);
            }
            else {
                jointTargetArrayList[arrayIndex] = new ArrayList(4);
            }
            
            jointTargetArrayList[arrayIndex].add( 0, jointName);

            double targetValue = 0.0;
                    
            try {
                targetValue = Double.parseDouble(sTarget);
                targetValue = targetValue*targetScale;
            }
                    
            catch(NumberFormatException e) {
                e.getMessage();
                targetValue = Double.NaN;
            }
             
            if (axesType.equals("Rotational"))
                targetValue = Math.toRadians(targetValue);
            
            String scaledTarget = String.valueOf(targetValue);
             
            jointTargetArrayList[arrayIndex].add(1, Double.valueOf(scaledTarget)); 
            
            String dofNum = String.valueOf(index+1);
            
            jointTargetArrayList[arrayIndex].add(2, Integer.valueOf(dofNum));
            
            if (axesType.equals("Rotational") ) {
                jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL);
            }
            else {
                jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL);
            }
       
            if (auxJointType != null)
            {
                jointTargetArrayList[arrayIndex].add(4, auxJointType);

                boolean lockFound = false;
                for (int jj=0; axesLocked != null && jj<axesLocked.length; jj++)
                {
                    if ((m_createJointTarget == false) && (arrayIndex == axesLocked[jj] - m_numRobotAxes - 1) ||
                        (m_createJointTarget == true)  && (arrayIndex == axesLocked[jj] - 1))
                    {
                        jointTargetArrayList[arrayIndex].add(5, Boolean.TRUE);
                        lockFound = true;
                        break;
                    }
                }
                if (lockFound == false)
                    jointTargetArrayList[arrayIndex].add(5, Boolean.FALSE);
            }
        }
       
        public String formatDouble(String input, String axisType, int decCount) {
            String formattedValue = "";
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(decCount);
            
            if(axisType.equals("Translational"))
                formattedValue = nf.format(Double.parseDouble(input)*0.001);
            else if(axisType.equals("Rotational"))
                formattedValue = nf.format(Double.parseDouble(input));
            else
               formattedValue = "No_axis_type"; 
            
               return formattedValue;
        }


        public void createNewSpeedProfile( String speedString, String profileName) {
            //<MotionProfile>
                //<Name>Default</Name>
                //<MotionBasis>Percent</MotionBasis>
                //<Speed Units="%" Value="50" />
                //<Accel Units="%" Value="100" />
                //<AngularSpeedValue Units="%" Value="100" />
                //<AngularAccelValue Units="%" Value="100" />
            //</MotionProfile>
            
            String[] speedComponents=speedString.split("[0-9]");
            String speedUnits = speedComponents[speedComponents.length-1];
            speedUnits.trim();
            speedComponents=speedString.split(speedUnits);
            speedString = speedComponents[0];
                
            
            if (speedUnits.equals("%"))
            {
                 double speedValue = Double.parseDouble(speedString);
                 if (!profileName.equals("100%"))
                 {
                     super.createMotionProfile( m_motionProfileListElement, profileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT, speedValue, 100.0, 100.0, 100.0);
                 }
            }
            else if (speedUnits.equals("sec"))
            {
                 double speedValue = Double.parseDouble(speedString);
                 super.createMotionProfile( m_motionProfileListElement, profileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, speedValue, 100.0, 100.0, 100.0);
            }
            else
            {
                double speedValue;
                double angularSpeedValue = 100.0;
                if (speedUnits.equals("deg/sec"))
                {
                    // rotational speed converted to m/sec based on 360.0 deg/sec max.
                    speedValue = Double.parseDouble(speedString)*2000.0/(360.0*1000.0); 
                    angularSpeedValue = Double.parseDouble(speedString)*100.0/360.0;
                }
                else if (speedUnits.equals("cm/min"))
                {
                	   speedValue = Double.parseDouble(speedString)/6000.0;  // cm/min to m/sec divide by 100 cm/meter and 60 sec/min   
                }          
                else if (speedUnits.equals("inch/min"))
                {
              	   speedValue = Double.parseDouble(speedString)*.0254/60.0;  // inch/min to m/sec multiply by 25.4 inch/meter and divide by 60 sec/min   
                }                      	
                else
                {
                    speedValue = Double.parseDouble(speedString)/1000.0;                  
                }
                super.createMotionProfile( m_motionProfileListElement, profileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speedValue, 100.0, angularSpeedValue, 100.0);
            }       
        }

        public void createNewAccuracyProfile( String accuracyString, String accuracyValue, String profileName) throws NumberFormatException {
            //<AccuracyProfile>
                //<Name>Default</Name>
                //<FlyByMode>Off</FlyByMode>
                //<AccuracyType>Speed</AccuracyType>
                //<AccuracyValue Units="%" Value="0" />
            //</AccuracyProfile>
            boolean flyByMode = false;
            
            
            if (accuracyString.equals("FINE"))
                flyByMode = false;
            else
                flyByMode = true;
            
            if (accuracyString.equals("CD") )
            {
                double accuracyVal = Double.parseDouble(accuracyValue)/1000.0;
                super.createAccuracyProfile( m_accuracyProfileListElement, profileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, flyByMode, accuracyVal);
            }
            else
            {
                double accuracyVal = 0.0;
                if (!accuracyValue.equals("")) {
                    accuracyVal = Double.parseDouble(accuracyValue);
                }
                if (!profileName.equals("FINE"))
                {
                    super.createAccuracyProfile( m_accuracyProfileListElement, profileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED, flyByMode, accuracyVal);
                }
            }
         }
       
         public double SSSA(double side1, double side2, double side3) {
            double result;
            
            double	tmp;

            tmp = ( side2*side2 + side3*side3 - side1*side1 ) / ( 2.0*side2*side3 );

            if( tmp < -1.0 ) return( Math.PI );
            if( tmp >  1.0 ) return( 0.0 );
            result = Math.acos( tmp );          

            return(result);
        } 

    public void EncodingConverter(String[] a) {
      String inFile = a[0];
      String inCharsetName = a[1];
      String outFile = a[2];
      String outCharsetName = a[3];
      try {
         InputStreamReader in = new InputStreamReader(
            new FileInputStream(inFile), inCharsetName);
         OutputStreamWriter out = new OutputStreamWriter(
            new FileOutputStream(outFile), outCharsetName);
         int c = in.read();
         int n = 0;
         while (c!=-1) {
            out.write(c);
            n++;
            c = in.read();
         }
         in.close();
         out.close();
        // System.out.println("Number of characters: "+n);
        // System.out.println("Number of input bytes: "
        //   +(new File(inFile)).length());
        // System.out.println("Number of output bytes: "
        //    +(new File(outFile)).length());
      } catch (IOException e) {
        // System.out.println(e.toString());
      }
   }     

}//end class
