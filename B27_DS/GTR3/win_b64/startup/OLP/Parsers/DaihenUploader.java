//DOM and Parser classes
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

//Java Exception classes
import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class DaihenUploader extends DNBIgpOlpUploadBaseClass
{
    //Constants
    private static final String VERSION = "Delmia Corp. Daihen Uploader Version 5 Release 20 SP2.\n";
    private static final String COPYRIGHT = "Copyright Delmia Corp. 1986-2010, All Rights Reserved.\n";
    private static final String DEFAULT = "Default";
    private static final String auxRail = "RailTrackGantry";
    private static final String auxTool = "EndOfArmTooling";
    private static final String auxPos  = "WorkpiecePositioner";

    //Member primitive variables
    private int d_targetType;
    private int m_numCStatements;
    private int m_numRobotAxes;
    private int m_numAuxAxes;
    private int m_numExtAxes;
    private int m_numPositionerAxes;

    private int delayCounter;
    private int moveCounter;
    private int moProfileCounter;
    private int accuracyProfileCounter;
    private int headerCount;
    private int programCallCount;
    private int callTaskNumber;
    private int jobCount;
    private static boolean RailTrackGantryFirst;

    //Member objects
    private Document d_xmlDoc;
    private String m_currentTagPoint;
    private String d_mountedTool;
    private Pattern [] d_keywords;
    private String [] m_axisTypes;
    private String [] d_toolProfileNames;
    private String currMotionProfile = DEFAULT;
    private String currAccuracyProfile = DEFAULT;
    private String currToolProfile = DEFAULT;
    private String currObjFrameProfile = DEFAULT;
    private String weldHome;
    private String closeHome;
    private String openHome;
    private Hashtable m_tagNames;
    private Hashtable m_toolNumberMapping;
    private Hashtable m_objFrameNumberMapping;
    private Hashtable ioNameMap;
    private ArrayList programCallNames;
    private ArrayList commentLines;
    private ArrayList inputNameNum, outputNameNum;
    private String prgFileEncoding = "US-ASCII";

    private static BufferedWriter bw;

    //Constructor
    public DaihenUploader(String [] parameters) throws ParserConfigurationException
    {
        super(parameters);
        m_numCStatements = 0;
        delayCounter = 1;
        moveCounter = 1;
        moProfileCounter = 1;
        accuracyProfileCounter = 1;
        programCallCount = 0;
        callTaskNumber = 1;
        headerCount = 0;
        jobCount = 0;

        d_mountedTool = parameters[6];
        String [] toolProfileNames = parameters[7].split("[\\t]+");
        String [] homeNames = parameters[9].split("\\t");

        m_numRobotAxes = super.getNumberOfRobotAxes();
        m_numAuxAxes = super.getNumberOfRailAuxiliaryAxes();
        m_numExtAxes = super.getNumberOfToolAuxiliaryAxes();
        m_numPositionerAxes = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();

        m_currentTagPoint = new String("");

        d_keywords = new Pattern [5000];
        m_axisTypes = new String[m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numPositionerAxes];
        m_axisTypes = super.getAxisTypes();
        weldHome = new String("Home_1");
        closeHome = new String("Home_2");
        openHome = new String("Home_2");
        m_tagNames = new Hashtable();
        m_toolNumberMapping = new Hashtable();
        m_objFrameNumberMapping = new Hashtable();
        ioNameMap = new Hashtable();
        d_toolProfileNames = toolProfileNames;
        programCallNames = new ArrayList(1);
        commentLines = new ArrayList(2);
        inputNameNum = new ArrayList(1);
        outputNameNum = new ArrayList(1);
        RailTrackGantryFirst = true;
        
        d_xmlDoc = super.getDOMDocument();

        if (homeNames.length > 0)
            weldHome = homeNames[0];
        if (homeNames.length > 1)
            closeHome = homeNames[1];
        if (homeNames.length > 2)
            openHome = homeNames[2];

        Hashtable parameterData = super.getParameterData();
        Enumeration parameterKeys = parameterData.keys();
        Enumeration parameterValues = parameterData.elements();
        int index, arrayIndex;
        String pName, pValue, testString;
        String ofprf = "ObjectFrameProfile.";
        while (parameterKeys.hasMoreElements() && parameterValues.hasMoreElements())
        {
            pName = parameterKeys.nextElement().toString();
            pValue = parameterValues.nextElement().toString();
            index = pName.indexOf('.');
            testString = pName.substring(0, index+1);
            try
            {
                if (testString.equals("ToolProfile."))
                {
                    String toolProfileName = pName.substring(index+1);
                    m_toolNumberMapping.put(toolProfileName, pValue);
                }
                else if (testString.equals("ObjectFrameProfile."))
                {
                    String objFrameProfileName = pName.substring(index+1);
                    m_objFrameNumberMapping.put(objFrameProfileName, pValue);
                }
                else if (testString.equals("IOMap."))
                {
                    ioNameMap.put(pName.substring(index+1), pValue);
                }
                else if (pName.equalsIgnoreCase("WeldHome"))
                {
                    weldHome = pValue;
                }
                else if (pName.equalsIgnoreCase("CloseHome"))
                {
                    closeHome = pValue;
                }
                else if (pName.equalsIgnoreCase("OpenHome"))
                {
                    openHome = pValue;
                }
                else if (pName.equalsIgnoreCase("RailTrackGantryFirst"))
                {
                    RailTrackGantryFirst = new Boolean(pValue).booleanValue();
                }
                else if (pName.equals("ProgramFileEncoding"))
                {
                    prgFileEncoding = pValue;
                }
            }

            finally
            {
                continue;
            }
        }

        for (int ii = 0; ii < 5000; ii++ )
        {
            d_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
        }
  
        // P001.ASC Seq Year Month Day Hour Minute
        d_keywords[10] = Pattern.compile("^\\s*P[0-9]{3}\\.ASC\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[20] = Pattern.compile("^\\s*PRD\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[30] = Pattern.compile("^\\s*TACT\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[40] = Pattern.compile("^\\s*0\\s+CMT\\s+", Pattern.CASE_INSENSITIVE);

        String seqStr = "^\\s*[0-9]{1,3}\\s+";
        d_keywords[100] = Pattern.compile(seqStr+"NOP", Pattern.CASE_INSENSITIVE);

        d_keywords[200] = Pattern.compile(seqStr+"P\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[300] = Pattern.compile(seqStr+"L\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[310] = Pattern.compile(seqStr+"L'\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[320] = Pattern.compile(seqStr+"C1\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[325] = Pattern.compile(seqStr+"C1'\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[330] = Pattern.compile(seqStr+"C2\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[335] = Pattern.compile(seqStr+"C2'\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[400] = Pattern.compile(seqStr+"H\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[410] = Pattern.compile(seqStr+"PP\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[420] = Pattern.compile(seqStr+"PS\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[430] = Pattern.compile(seqStr+"LS\\s+", Pattern.CASE_INSENSITIVE);

        d_keywords[1000] = Pattern.compile("^\\s*0\\s+S\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[1010] = Pattern.compile("^\\s*0\\s+R\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[1100] = Pattern.compile("^\\s*0\\s+N\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[1110] = Pattern.compile("^\\s*0\\s+F\\s+", Pattern.CASE_INSENSITIVE);

        d_keywords[1200] = Pattern.compile(seqStr+"T\\s+", Pattern.CASE_INSENSITIVE);

        d_keywords[1300] = Pattern.compile(seqStr+"IF\\s+\\d\\s+", Pattern.CASE_INSENSITIVE);
        d_keywords[1310] = Pattern.compile(seqStr+"FI", Pattern.CASE_INSENSITIVE);

        d_keywords[2000] = Pattern.compile(seqStr+"CL\\s+4\\s+000\\s+", Pattern.CASE_INSENSITIVE);

        d_keywords[3000] = Pattern.compile(seqStr+"END", Pattern.CASE_INSENSITIVE);

        // process as comments
        d_keywords[4000] = Pattern.compile("^\\s*0\\s+\\w+\\s+", Pattern.CASE_INSENSITIVE);
    }

    public static void main(String [] parameters)
        throws SAXException, ParserConfigurationException,
                TransformerConfigurationException, NumberFormatException,
                StringIndexOutOfBoundsException
    {
        DaihenUploader uploader = new DaihenUploader(parameters);

        String [] additionalProgramNames = null;
        if (parameters.length > 12 && parameters[12].trim().length() > 0)
            additionalProgramNames = parameters[12].split("[\\t]+");
        
        //Get the robot program and parse it line by line
        String robotProgramName = uploader.getPathToRobotProgramFile();
        File f = new File(robotProgramName);
        String path = f.getParent() + f.separator;
        String robotProgramNameOnly = f.getName();
        int lastDot = robotProgramNameOnly.toUpperCase().lastIndexOf(".ASC");
        if (lastDot != -1)
            robotProgramNameOnly = robotProgramNameOnly.substring(0, lastDot);

        String uploadInfoFileName = uploader.getPathToErrorLogFile();
        String logicProgramName = robotProgramName;
        Matcher match;
        boolean bLogicProg;

        try
        {
            bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));

            bw.write(VERSION);
            bw.write(COPYRIGHT);
            bw.write("\nStart of java parsing.\n\n");

            bLogicProg = false; // set to FALSE, no program logic stuff
            BufferedReader rbt;
            try
            {
                rbt = new BufferedReader(new FileReader(logicProgramName));

                String line = rbt.readLine();
                while (line != null)
                {
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                        continue;
                    }

                    for (int ii=0; ii<uploader.d_keywords.length; ii++)
                    {
                        match = uploader.d_keywords[ii].matcher(line);

                        if (match.find() == true)
                        {
                            switch(ii)
                            {
                                case 200: // P
                                case 300: // L
                                case 310: // L'
                                case 320: // C1
                                case 325: // C1'
                                case 330: // C2
                                case 335: // C2'
                                case 1200: // T
                                    bLogicProg = false;
                                    break;
                                default:
                                    break;
                            }

                            break;
                        } // end if match.find()
                    } // end for each keyword
                    line = rbt.readLine();
                } // end while line
                rbt.close();
            } // end try

            catch (IOException e)
            {
                e.getMessage();
            }

            int errCode;
            if (bLogicProg == true)
                errCode = uploader.processRobotLogicProgram(robotProgramName);
            else
                errCode = uploader.processRobotProgram(robotProgramName);

            String sAddProg = null;
            int index;
            if (additionalProgramNames != null)
            {
                for (int ii=0; ii<additionalProgramNames.length; ii++)
                {
                    f = new File(additionalProgramNames[ii]);
                    sAddProg = f.getName();
                    if (sAddProg.endsWith(".ASC") || sAddProg.endsWith(".asc") || sAddProg.endsWith(".Asc"))
                        sAddProg = sAddProg.substring(0, sAddProg.length()-4);
                    
                    index = uploader.programCallNames.indexOf(sAddProg);
                    if (index < 0)
                    {
                        uploader.programCallNames.add(uploader.programCallCount, sAddProg);
                        uploader.programCallNames.trimToSize();
                        uploader.programCallCount++;
                    }
                }
            }

            if (errCode == 0)
            {
                String calledProgName;
                for (int ii=0; ii<uploader.programCallCount; ii++)
                {
                    calledProgName = (String)uploader.programCallNames.get(ii);
                    if (calledProgName.equalsIgnoreCase(robotProgramNameOnly))
                        continue; // skip the "main" program already uploaded
                    
                    robotProgramName = path + calledProgName + ".ASC";
                    errCode = uploader.processRobotProgram(robotProgramName);
                }
            }

            if (errCode == 0)
            {
                bw.write("All the program statements have been parsed.\n");
                bw.write("\nJava parsing completed successfully.\n");
            }
            else
            {
                bw.write("\nERROR-Robot Program upload failed.\n");
            }

            bw.write("\nEnd of java parsing.\n");
            bw.flush();
            bw.close();
        }//end try

        catch (IOException e)
        {
            e.getMessage();
        }
        
        Element motionProfileList = uploader.getMotionProfileListElement();
        DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
        if (motionProfileList.hasChildNodes() == false)
            uploader.createMotionProfile(motionProfileList, "Default", moBasis, 50.0, 100.0, 100.0, 100.0);
        
        Element accuracyProfileList = uploader.getAccuracyProfileListElement();
        DNBIgpOlpUploadEnumeratedTypes.AccuracyType accuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED;
        if (accuracyProfileList.hasChildNodes() == false)
            uploader.createAccuracyProfile(accuracyProfileList, "Default", accuracyType,  false, 0.0);

        try {
            uploader.writeXMLStreamToFile();
        }

        catch (IOException e) {
            e.getMessage();
        }
        catch (TransformerException e) {
            e.getMessage();
        }
    }//end main

    private int processRobotLogicProgram(String logicProgramName)
    {
        BufferedReader rbt;
        String line, port, status;
        String [] logicArgs;
        logicArgs = new String [9];

        Element resourceElem = super.getResourceElement();
        Element programLogicElem = d_xmlDoc.createElement("ProgramLogic");
        resourceElem.appendChild(programLogicElem);
        Element inputsElem = d_xmlDoc.createElement("Inputs");
        programLogicElem.appendChild(inputsElem);
        Element outputsElem = d_xmlDoc.createElement("Outputs");
        programLogicElem.appendChild(outputsElem);
        Element jobsElem = d_xmlDoc.createElement("Jobs");
        programLogicElem.appendChild(jobsElem);

        String name = logicProgramName;
        int lastSlash = name.lastIndexOf('/');
        int lastBackSlash = name.lastIndexOf('\\');
        if (lastSlash < lastBackSlash)
            lastSlash = lastBackSlash;
        if (lastSlash < 0)
            lastSlash = 0;
        jobsElem.setAttribute("Name", name.substring(lastSlash+1, name.length()-4));

        Element jobElem = null, startConditionElem = null, waitExprElem = null;
        Element opElem;
        Text opText;
        Matcher match;

        try
        {
            rbt = new BufferedReader(new FileReader(logicProgramName));

            line = rbt.readLine();
            while (line != null)
            {
                if (line.equals(""))
                {
                    line = rbt.readLine();
                    continue;
                }

                for (int ii=0; ii<d_keywords.length; ii++)
                {
                    match = d_keywords[ii].matcher(line);

                    if (match.find() == true)
                    {
                        switch(ii)
                        {
                            case 1000: // S: SET output
                            case 1010: // R: RESET output
                                jobElem = checkJobElem(jobElem, jobsElem);
                                processSetOutputLogicProg(line, jobElem);
                                waitExprElem = null;
                                break;
                            case 1100: // N: Wait for input is ON
                            case 1110: // F: Wait for intput is OFF
                                jobElem = checkJobElem(jobElem, jobsElem);
                                if (waitExprElem == null)
                                {
                                    waitExprElem = d_xmlDoc.createElement("WaitExpression");
                                    jobElem.appendChild(waitExprElem);
                                }
                                else
                                {
                                    opElem = d_xmlDoc.createElement("Operator");
                                    waitExprElem.appendChild(opElem);
                                    opText = d_xmlDoc.createTextNode("and");
                                    opElem.appendChild(opText);
                                }
                                logicArgs = line.trim().split("\\s+");
                                port = logicArgs[2];
                                if (logicArgs[1].equalsIgnoreCase("N"))
                                    status = "true";
                                else
                                    status = "false";
                                processLogicExpr(port, status, waitExprElem);
                                break;
                            case 1300: // IF
                                if (jobElem == null || startConditionElem == null)
                                {
                                    jobElem = d_xmlDoc.createElement("Job");
                                    jobsElem.appendChild(jobElem);
                                    jobCount++;
                                    jobElem.setAttribute("Name", "Job"+jobCount);

                                    startConditionElem = d_xmlDoc.createElement("StartCondition");
                                    jobElem.appendChild(startConditionElem);
                                }
                                else
                                {
                                    opElem = d_xmlDoc.createElement("Operator");
                                    startConditionElem.appendChild(opElem);
                                    opText = d_xmlDoc.createTextNode("and");
                                    opElem.appendChild(opText);
                                }
                                logicArgs = line.trim().split("[;\\s]+");
                                port = logicArgs[3].substring(1);
                                if (logicArgs[4].equalsIgnoreCase("N1"))
                                    status = "true";
                                else
                                    status = "false";
                                processLogicExpr(port, status, startConditionElem);
                                waitExprElem = null;
                                break;
                            case 1310: // FI
                                jobElem = null;
                                startConditionElem = null;
                                waitExprElem = null;
                                break;
                            case 2000: // CL: Call subprogram
                                jobElem = checkJobElem(jobElem, jobsElem);
                                processProgramCallLogicProg(line, jobElem);
                                waitExprElem = null;
                                break;
                            default:
                                waitExprElem = null;
                                break;
                        } // end switch

                        break;
                    } // end if
                } // end for
                line = rbt.readLine();
            } // end while
            rbt.close();
        }

        catch (IOException e)
        {
            e.getMessage();
            return 1;
        }

        Element ioElem;
        String ioNum;
        for (int ii=0; ii<inputNameNum.size(); ii++)
        {
            ioNum = (String)inputNameNum.get(ii);
            ioElem = d_xmlDoc.createElement("Input");
            ioElem.setAttribute("InputName", "DI"+ioNum);
            ioElem.setAttribute("InputNumber", ioNum);
            inputsElem.appendChild(ioElem);
        }

        for (int ii=0; ii<outputNameNum.size(); ii++)
        {
            ioNum = (String)outputNameNum.get(ii);
            ioElem = d_xmlDoc.createElement("Output");
            ioElem.setAttribute("OutputName", "DO"+ioNum);
            ioElem.setAttribute("OutputNumber", ioNum);
            outputsElem.appendChild(ioElem);
        }

        return 0;
    } // end processRobotLogicProgram

    private Element checkJobElem(Element jobElem, Element jobsElem)
    {
        if (jobElem == null)
        {
            jobElem = d_xmlDoc.createElement("Job");
            jobsElem.appendChild(jobElem);
            jobCount++;
            jobElem.setAttribute("Name", "Job"+jobCount);

            Element startConditionElem = d_xmlDoc.createElement("StartCondition");
            jobElem.appendChild(startConditionElem);

            Element expressionElem = d_xmlDoc.createElement("Expression");
            startConditionElem.appendChild(expressionElem);
            Text exprTrue = d_xmlDoc.createTextNode("true");
            expressionElem.appendChild(exprTrue);
        }

        return jobElem;
    } // end checkJobElem

    private void addToArrayList(ArrayList arrayList, int ioNum)
    {
        String str = Integer.toString(ioNum);
        int index = arrayList.indexOf(str);
        if (index < 0)
        {
            int num = arrayList.size();
            if (num == 0)
            {
                arrayList.add(0, str);
            }
            else
            {
                int ii, val;
                for (ii=0; ii<num; ii++)
                {
                    val = Integer.parseInt((String)arrayList.get(ii));
                    if ( val > Integer.parseInt(str))
                    {
                        arrayList.add(ii, str);
                        break;
                    }
                    if (ii+1 == num)
                        arrayList.add(num, str);
                }
            }
        }
    } // end addToArrayList

    private void processLogicExpr(String port, String status, Element exprParentElem)
    {
        Element exprElem = d_xmlDoc.createElement("Expression");
        exprParentElem.appendChild(exprElem);

        int portNum = Integer.parseInt(port);
        Text expr = d_xmlDoc.createTextNode("DI" + portNum + " = " + status);
        exprElem.appendChild(expr);

        addToArrayList(inputNameNum, portNum);
    } // end processLogicExpr

    private void processSetOutputLogicProg(String line, Element jobElem)
    {
        String s1 = line.trim();
        String [] s = s1.substring(0, s1.lastIndexOf(";")).trim().split("[\\s]+");
        int portNum = Integer.parseInt(s[2]);
        String status = "false";
        if (s[1].equalsIgnoreCase("S"))
            status = "true";

        Element setOutput = d_xmlDoc.createElement("SetOutput");
        jobElem.appendChild(setOutput);

        Text setOutputVal= d_xmlDoc.createTextNode("DO" + portNum + " = " + status);
        setOutput.appendChild(setOutputVal);

        addToArrayList(outputNameNum, portNum);
    }

    private void processProgramCallLogicProg(String line, Element jobElem)
    {
        String s1 = line.trim();
        String [] s = s1.substring(0, s1.lastIndexOf(";")).trim().split("[\\s]+");

        Element callTask = d_xmlDoc.createElement("CallTask");
        jobElem.appendChild(callTask);

        Text callTaskVal = d_xmlDoc.createTextNode(s[4]);
        callTask.appendChild(callTaskVal);

        int index = programCallNames.indexOf(s[4]);
        if (index < 0)
        {
            programCallNames.add(programCallCount, s[4]);
            programCallCount++;
        }
    } // end processProgramCallLogicProg

    private int processRobotProgram(String robotProgramName)
    {
        BufferedReader rbt;
        int lineNumber, numOfChar;
        StringBuffer unparsedStatement = new StringBuffer();
        String unparsedString = new String();
        String line, line2;
        boolean stmtStart = false;

        headerCount = 0;

        String name = robotProgramName;
        int lastSlash = name.lastIndexOf('/');
        int lastBackSlash = name.lastIndexOf('\\');
        if (lastSlash < lastBackSlash)
            lastSlash = lastBackSlash;
        if (lastSlash < 0)
            lastSlash = 0;
        String rbtTaskName = name.substring(lastSlash + 1, name.length() - 4);
        Element activityListElem = super.createActivityList(rbtTaskName);

        Element resourceElem = super.getResourceElement();
        Element programLogicElem = findChildElemByName(resourceElem, "ProgramLogic", null, null);
        resourceElem.insertBefore(activityListElem, programLogicElem);

        String rbtPrg = robotProgramName;
        Matcher match;

        try
        {
            rbt = new BufferedReader(new InputStreamReader(new FileInputStream(rbtPrg), prgFileEncoding));

            bw.write("Unparsed entities from the file: \"");
            bw.write(rbtPrg);
            bw.write("\":\n\n"); 

            line = rbt.readLine();
            lineNumber = 1;
            while(line != null)
            {
                if (line.equals(""))
                {
                    line = rbt.readLine();
                    lineNumber++;
                    continue;
                }

                for (int ii = 0; ii < d_keywords.length; ii++)
                {
                    match = d_keywords[ii].matcher(line);

                    if ( match.find() == true )
                    {
                        //Call appropriate methods to handle the input
                        switch(ii) {
                            case 10: // header block
                            case 20: // production control data
                            case 30: // tact time data
                                processHeader(line, activityListElem);
                                break;
                            case 40: // comment
                                processComment(line);
                                break;
                            case 100: // NOP: No operation
                                if (stmtStart == false)
                                {
                                    addAttributeList(activityListElem, "Pre");
                                    stmtStart = true;
                                }
                                processNOP(activityListElem);
                                break;
                            case 200: // P: PTP move
                            case 300: // L: Linear move
                            case 310: // L': High Speed Linear move
                            case 320: // C1
                            case 325: // C1'
                            case 330: // C2
                            case 335: // C2'
                                if (stmtStart == false)
                                {
                                    addAttributeList(activityListElem, "Pre");
                                    stmtStart = true;
                                }
                                line2 = rbt.readLine();
                                processMove(line, line2, activityListElem);
                                break;
                            case 400: // H instruction
                                break;
                            case 410: // PP
                            case 420: // PS
                            case 430: // LS
                                processAuxAxis(line, activityListElem);
                                break;
                            case 1000: // S: Set output
                            case 1010: // R: Reset output
                                if (stmtStart == false)
                                {
                                    addAttributeList(activityListElem, "Pre");
                                    stmtStart = true;
                                }
                                processSetOutput(line, activityListElem);
                                break;
                            case 1100: // N: Wait for input is ON for time
                            case 1110: // F: Wait for input is OFF for time
                                if (stmtStart == false)
                                {
                                    addAttributeList(activityListElem, "Pre");
                                    stmtStart = true;
                                }
                                processWaitForInput(line, activityListElem);
                                break;
                            case 1200: // T: Wait for time
                                if (stmtStart == false)
                                {
                                    addAttributeList(activityListElem, "Pre");
                                    stmtStart = true;
                                }
                                processWaitTime(line, activityListElem);
                                break;
                            case 2000: // CL: Call Subprogram
                                if (stmtStart == false)
                                {
                                    addAttributeList(activityListElem, "Pre");
                                    stmtStart = true;
                                }
                                processProgramCall(line, activityListElem);
                                break;
                            case 3000: // END
                                addAttributeList(activityListElem, "Post");
                                break;
                            default:
                                break;
                        }//end switch

                        //match found & processed, break out for loop
                        break;
                    }//end if
                    else
                    {
                        if (ii == (d_keywords.length - 1))
                        {
                            commentLines.add(commentLines.size(), "Robot Language:" + line.trim());
                        }
                    }
                }//end for
                line = rbt.readLine();
                lineNumber++;

                numOfChar = unparsedStatement.length();
                unparsedStatement = unparsedStatement.delete(0, numOfChar);

            }//end while
            rbt.close();
        }

        catch (IOException e)
        {
            try {
                bw.write("ERROR: " + e.getMessage() + "\n");
            }
            catch (IOException bwe) {
            }
            return 1;
        }

        return 0;
    }//end processRobotProgram

    private void processComment(String line)
    {
        String comment = line.substring(line.indexOf("0 CMT ") + 6);
        commentLines.add(commentLines.size(), comment.trim());
    }

    private void addAttributeList(Element actElem, String prefix)
    {
        int numOfComment = commentLines.size();
        if (numOfComment == 0)
            return;
        
        String [] attrNames, attrValues;
        attrNames  = new String[numOfComment];
        attrValues = new String[numOfComment];
        for (int ii=0; ii<numOfComment; ii++)
        {
            attrNames[ii]  = prefix + "Comment" + String.valueOf(ii+1);
            attrValues[ii] = (String)commentLines.get(ii);
        }
        Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
        commentLines.clear();
    }

    private void processProgramCall(String line, Element activityList)
    {
        String s1 = line.trim();
        String [] s = s1.substring(0, s1.lastIndexOf(";")).trim().split("[\\s]+");

        String calledTaskName = s[4];
        int index = programCallNames.indexOf(calledTaskName);
        if (index < 0)
        {
            programCallNames.add(programCallCount, calledTaskName);
            programCallCount++;
        }
        String activityName = "CallTask." + callTaskNumber++;

        Element activity = super.createCallTaskActivity(activityList, activityName, calledTaskName);

        addAttributeList(activity, "Pre");
    }

    private void processWaitForInput(String line, Element activityList)
    {
        String s1 = line.trim();
        String [] s = s1.substring(0, s1.lastIndexOf(";")).trim().split("[\\s]+");

        Integer iPortNum = new Integer(s[2]);
        int portNum = iPortNum.intValue();
        String signalName = (String)ioNameMap.get(String.valueOf(portNum));
        if (signalName == null)
            signalName = "IO" + portNum;

        boolean sigVal = true;
        String OnOff = "On";
        if (s[1].equalsIgnoreCase("N"))
        {
            sigVal = true;
            OnOff = "On";
        }
        else
        if (s[1].equalsIgnoreCase("F"))
        {
            sigVal = false;
            OnOff = "Off";
        }

        String ioName = "";
        if (s[3].equals("-1"))
        {
            s[3] = "0";
            ioName = "Wait Until " + signalName + "=" + OnOff;
        }
        else
            ioName = "Wait Until " + signalName + "=" + OnOff + "[" + s[3] + "s]";

        Double dMaxWaitTime = new Double(s[3]);
        Element activity = super.createWaitForIOActivity(activityList, ioName, sigVal, signalName, iPortNum, dMaxWaitTime);

        addAttributeList(activity, "Pre");
    }

    private void processSetOutput(String line, Element activityList)
    {
        String s1 = line.trim();
        String [] s = s1.substring(0, s1.lastIndexOf(";")).trim().split("[\\s]+");
        Integer iPortNum = new Integer(s[2]);
        int portNum = iPortNum.intValue();
        String signalName = (String)ioNameMap.get(String.valueOf(portNum));
        if (signalName == null)
            signalName = "IO" + portNum;

        boolean sigVal = true;
        String OnOff = "On";
        if (s[1].equalsIgnoreCase("S"))
        {
            OnOff = "On";
            sigVal = true;
        }
        else
        if (s[1].equalsIgnoreCase("R"))
        {
            OnOff = "Off";
            sigVal = false;
        }

        String ioName = "";
        if (s[3].equals("0.0"))
            ioName = "Set-" + signalName + "=" + OnOff;
        else
            ioName = "Set-" + signalName + "=" + OnOff + "[" + s[3] + "s]";

        Double dSigDuration = new Double(0.0);
        Element activity = super.createSetIOActivity(activityList, ioName, sigVal, signalName, iPortNum, dSigDuration);

        addAttributeList(activity, "Pre");
    }

    private void processHeader(String line, Element activityListElem)
    {
        String [] attrNames, attrValues;

        String s = line.trim();
        s = s.substring(0, s.lastIndexOf(";")).trim();
        if (s.startsWith("PRD ") || s.startsWith("TACT "))
        {
            attrNames = new String[1];
            if (s.startsWith("PRD "))
                attrNames[0] = "PRDLine";
            else
                attrNames[0] = "TACTLine";

            attrValues = new String[1];
            attrValues[0] = s;
        }
        else
        {
            String [] args = s.split("[\\s]+", 11);
            if (args.length > 10)
            {
                attrNames = new String[4];
                attrValues = new String[4];
            }
            else
            {
                attrNames = new String[3];
                attrValues = new String[3];
            }
            attrNames[0] = "Protect";
            attrNames[1] = "Version";
            attrNames[2] = "SystemID";
            for (int ii = 0; ii < 3; ii++)
                attrValues[ii] = args[ii + 7];
            if (args.length > 10)
            {
                attrNames[3] = "Comment";
                attrValues[3] = args[10];
            }
        }

        Element attrListElem = super.createAttributeList(activityListElem, attrNames, attrValues);
    } // end processHeader

    private void processNOP(Element activityListElem)
    {
        Element parentElem = (Element)activityListElem.getLastChild();
        if (parentElem.getTagName() == "AttributeList")
        {
            parentElem = (Element)parentElem.getParentNode();
        }
        else
        if (parentElem.getTagName() == "Activity")
        {
        }
        else
        {
            return;
        }

        String [] attrNames = {"NOP"}, attrValues = {"NOP"};
        Element attributeListElem = super.createAttributeList(parentElem, attrNames, attrValues);
    } // end processNOP

    private void processMove(String line, String line2, Element activityList)
    {
        String [] s = line.trim().split("[\\s]+");
        String [] s2 = line2.trim().split("[\\s]+");

        String actName = "RobotMotion." + moveCounter;
        Element activity = super.createMotionActivityHeader(activityList, actName);

        DNBIgpOlpUploadEnumeratedTypes.MotionBasis motionBasis;
        motionBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
        double speed = Double.parseDouble(s[3]);
        if (s[1].equalsIgnoreCase("P") == false)
        {
            motionBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
            speed = speed / 6000; // cm/min -> m/s
        }
        String spdStr = String.valueOf(speed);

        String moName = lookupMotionProfile(motionBasis, spdStr);
        if (moName == null)
        {
            moName = "Motion." + moProfileCounter++;
            Element moProfileList = super.getMotionProfileListElement();
            Element moProfile = super.createMotionProfile(moProfileList, moName, motionBasis, speed, 100.0, 100.0, 100.0);
        }

        DNBIgpOlpUploadEnumeratedTypes.AccuracyType accuracyType = DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED;
        boolean bFlyByMode = true;
        int accuracy = Integer.parseInt(s[4]);
        if (accuracy == 0)
            bFlyByMode = false;
        String accuracyName = lookupAccuracyProfile(accuracy);
        if (accuracyName == null)
        {
            accuracyName = "Accuracy." + accuracyProfileCounter++;
            Element accuracyProfileList = super.getAccuracyProfileListElement();
            Element accuracyProfile = super.createAccuracyProfile(accuracyProfileList, accuracyName, accuracyType, bFlyByMode, (double)accuracy);
        }

        DNBIgpOlpUploadEnumeratedTypes.MotionType eMoType;
        eMoType = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
        if (s[1].equalsIgnoreCase("L") || s[1].equalsIgnoreCase("L'"))
            eMoType = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
        else
        if (s[1].equalsIgnoreCase("C1") || s[1].equalsIgnoreCase("C1'"))
            eMoType = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
        else
        if (s[1].equalsIgnoreCase("C2") || s[1].equalsIgnoreCase("C2'"))
            eMoType = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;

        ArrayList [] oaJointTarget = new ArrayList[m_numRobotAxes];
        for (int ii=0; ii<m_numRobotAxes; ii++)
            oaJointTarget[ii] = new ArrayList(1);

        String jName;
        Double jVal;
        DNBIgpOlpUploadEnumeratedTypes.DOFType jType;
        for (int ii=0; ii<m_numRobotAxes; ii++)
        {
            jName = "Joint " + (ii + 1);
            jVal  = new Double(s[ii+5]);

            oaJointTarget[ii].add(0, jName);
            oaJointTarget[ii].add(1, jVal);
            oaJointTarget[ii].add(2, new Integer(ii+1));
            jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL;
            if (m_axisTypes[ii].equals("Translational"))
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL;
            oaJointTarget[ii].add(3, jType);
        }

        Element moAttrElem = super.createMotionAttributes(activity, moName, accuracyName, DEFAULT, DEFAULT, eMoType);
        Element targetElem = super.createTarget(activity, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false);
        Element jTargetElem = super.createJointTarget(targetElem, oaJointTarget);

        moveCounter++;

        addAttributeList(activity, "Pre");
    } // end processMove

    private void processAuxAxis(String line, Element actList)
    {
        Element auxJointElem, jValElem, auxJointPositioner;
        Text jVal;
        int jaux, auxDOFNum, offset;
        double auxJointVal;

        String [] s = line.trim().split("\\s+");
        Element act = (Element)actList.getLastChild();
        Element target = findChildElemByName(act, "Target", null, null);
        Element jointTarget = findChildElemByName(target, "JointTarget", null, null);

        offset = 3; // LS instruction has no speed
        if (s[1].equalsIgnoreCase("PS") || s[1].equalsIgnoreCase("PP"))
            offset++; // PS & PP has speed

        if (s[1].equalsIgnoreCase("PS") || (s[1].equalsIgnoreCase("LS") && s[2].equals("3")))
        {
            for (jaux=1; jaux<=m_numAuxAxes; jaux++)
            {
                auxJointElem = d_xmlDoc.createElement("AuxJoint");
                if (RailTrackGantryFirst == true)
                {
                    auxJointPositioner = findChildElemByName(jointTarget, "AuxJoint", "Type", auxPos);
                    jointTarget.insertBefore(auxJointElem, auxJointPositioner);
                }
                else
                {
                    jointTarget.appendChild(auxJointElem);
                }
                jValElem = d_xmlDoc.createElement("JointValue");
                auxJointElem.appendChild(jValElem);

                auxDOFNum = m_numRobotAxes + jaux;
                if (RailTrackGantryFirst == false)
                    auxDOFNum += m_numPositionerAxes;
                auxJointElem.setAttribute("Type", auxRail);
                auxJointElem.setAttribute("DOFNumber", String.valueOf(auxDOFNum));
                auxJointElem.setAttribute("JointName", "Joint " + jaux);
                auxJointElem.setAttribute("JointType", "Translational");
                auxJointVal = Double.parseDouble(s[jaux+ offset])/1000.0;
                jVal = d_xmlDoc.createTextNode(String.valueOf(auxJointVal));
                jValElem.appendChild(jVal);
            }
        }
        else
        if (s[1].equalsIgnoreCase("PP") || (s[1].equalsIgnoreCase("LS") && s[2].equals("4")))
        {
            if (m_numPositionerAxes == 2)
                offset++;

            for (jaux=1; jaux<=m_numPositionerAxes; jaux++)
            {
                auxJointElem = d_xmlDoc.createElement("AuxJoint");
                if (RailTrackGantryFirst == true)
                {
                    jointTarget.appendChild(auxJointElem);
                }
                else
                {
                    auxJointPositioner = findChildElemByName(jointTarget, "AuxJoint", "Type", auxRail);
                    jointTarget.insertBefore(auxJointElem, auxJointPositioner);
                }
                jValElem = d_xmlDoc.createElement("JointValue");
                auxJointElem.appendChild(jValElem);

                auxDOFNum = m_numRobotAxes + jaux;
                if (RailTrackGantryFirst == true)
                    auxDOFNum += m_numAuxAxes;
                auxJointElem.setAttribute("Type", auxPos);
                auxJointElem.setAttribute("DOFNumber", String.valueOf(auxDOFNum));
                auxJointElem.setAttribute("JointName", "Joint " + jaux);
                auxJointElem.setAttribute("JointType", "Rotational");
                jVal = d_xmlDoc.createTextNode(s[jaux + offset]);
                jValElem.appendChild(jVal);
            }
        }
    } // end processAuxAxis

    private String lookupMotionProfile(DNBIgpOlpUploadEnumeratedTypes.MotionBasis motionBasis, String spdStr)
    {
        Element moProfileList = super.getMotionProfileListElement();
        NodeList moProfiles = moProfileList.getElementsByTagName("MotionProfile");
        Element mo, moB, moSv, moN;
        for (int ii=0; ii<moProfiles.getLength(); ii++)
        {
            mo = (Element)moProfiles.item(ii);
            moB = findChildElemByName(mo, "MotionBasis", null, null);
            moSv = findChildElemByName(mo, "Speed", "Value", spdStr);
            if (moSv != null &&
                moB.getFirstChild().getNodeValue().equalsIgnoreCase(motionBasis.toString()))
            {
                moN = findChildElemByName(mo, "Name", null, null);
                String moName = moN.getFirstChild().getNodeValue();
                return moName;
            }
        }
        return null;
    } // lookupMotionProfile

    private String lookupAccuracyProfile(int accuracy)
    {
        Element accuracyProfileList = super.getAccuracyProfileListElement();
        NodeList accuracyProfiles = accuracyProfileList.getElementsByTagName("AccuracyProfile");
        Element ac, acVal, acN;
        for (int ii=0; ii<accuracyProfiles.getLength(); ii++)
        {
            ac = (Element)accuracyProfiles.item(ii);
            acVal = findChildElemByName(ac, "AccuracyValue", "Value", String.valueOf(accuracy));
            if (acVal != null)
            {
                acN = findChildElemByName(ac, "Name", null, null);
                String acName = acN.getFirstChild().getNodeValue();
                return acName;
            }
        }
        return null;
    }// lookupAccuracyProfile

    private void processWaitTime(String line, Element activityList)
    {
        String [] s = line.trim().split("[\\s]+");
        String activityName = "RobotDelay." + delayCounter++;
        Double startTime = new Double(0.0);
        Double endTime = new Double(s[2].split(";")[0]);

        Element activity = super.createDelayActivity(activityList, activityName, startTime, endTime);

        addAttributeList(activity, "Pre");
    } // end processWaitTime

    private Element findChildElemByName(Element currentElem, String name, String attrib, String attribVal)
    {
        Element tmpElem;

        NodeList childNodes = currentElem.getChildNodes();
        for (int ii=0; ii<childNodes.getLength(); ii++)
        {
            tmpElem = (Element)childNodes.item(ii);
            if (tmpElem.getTagName().equals(name))
            {
                if (attrib == null)
                {
                    return (tmpElem);
                }
                else
                {
                    if (tmpElem.getAttribute(attrib).equals(attribVal))
                    {
                        return (tmpElem);
                    }
                }
            }
        }
        return (null);
    } // end findChildElemByName
}//end class
