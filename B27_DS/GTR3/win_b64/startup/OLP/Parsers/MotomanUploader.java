//DOM and Parser classes
import org.w3c.dom.*;

import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;
import java.lang.reflect.Array;

import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Enumeration;


//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;

import Kuka.util;

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class MotomanUploader extends DNBIgpOlpUploadBaseClass {

        //Constants
        private static final int JOINT = 1, CARTESIAN = 2;
        private static final int NumOfTools = 65, NumOfObjectProfiles = 26;
        private static final String VERSION = "Delmia Corp. Motoman MRC Inform II Uploader Version 5 Release 27 SP4.\n";
        private static final String COPYRIGHT = "Copyright Delmia Corp. 1986-2017, All Rights Reserved.\n";
        private static final String rbtLang = "Robot Language";
        private static final String varDef = "Variable Definition";
        
        //Member primitive variables
        private int m_targetType;
        private int m_numCStatements;
        private int m_numRobotAxes;
        private int m_numAuxAxes;
        private int m_numExtAxes;
        private int m_numWorkAxes;
        private int m_numDOF;
        private int m_commentCounter;
        private int m_rbtLangCounter;
        private int m_delayCounter;
        private int m_moveCounter;
        private int m_opMoveCounter;
        private int m_spotWeldCounter;
        private int m_spotDryCounter;
        private int m_spotPickCounter;
        private int m_spotPlaceCounter;
        private int programCallCount;
        private int robotCallNumber;
        private int commentCount;
        private int jobCount, arcStartCount, arcEndCount;
        private static boolean toolFileExists;
        private static boolean uframeFileExists;
        private int NumCircularMoves;
        private boolean EndOfArmToolingFirst;
        private double m_MotomanDefaultAccuracy = 100.0;
        private double m_MotomanDefaultJntAccuracy = 100.0;
        // zero-based             mPL 1      2     3    4     5    6    7    8
        private double [] mPL = {0.0, 0.025, 0.05, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5};

        // DLY Start 2009/05/02 add linetracking stuff
        private double _ConveyorInitialPosition = 0.0;
        private double _LastConveyorInitialPosition = 0.0;
        private double _ConveyorTaughtPosition = 0.0;
        private double _ConveyorOffset = 0.0;
        private ArrayList m_programLines;
        private int m_programLineNum = 0;
        // DLY End 2009/05/02
        private boolean m_ConveyorFound = false;
        private boolean m_trackProfileCreated = false;
        private boolean uploadCreateTags = false;
        
        private int [] m_bcAxisMapping;
        private int [] m_ecAxisMapping;
        private double [] m_resolution;
        private double [] m_zeroOffset;
        private double dCloseJ = Double.NaN;
        private boolean m_commentWaitSignal;
        private boolean m_commentRefp;
        private boolean m_commentSftOn;
        private String m_accuracyProfileName = "MotomanDefault";
        private String m_motionProfileName = "Default";
        private double arcWeldSpeed = -1.0;
        private double absSpeed = -1.0;

        private String ioName = "DO[";
        private String weldHome;
        private String closeHome;
        private String openHome;

        //Member objects
        private Document m_xmlDoc;
        private String m_currentTagPoint;
        private int m_currentToolNumber;
        private int m_currentUframeNumber;
        private String m_currentUframeName;
        private int xrc_pl = -1;
        private String m_mountedTool;
        private ArrayList mountedTools;
        private Pattern [] m_keywords;
        private String [] arcStartCond={"Current", "Voltage", "Timer", "Speed", "Retry"};
        private String [] arcEndCond={"Current", "Voltage", "Timer", "AntStk"};
        private String [] m_axisTypes;
        private String [] m_toolProfileNames;
        private Hashtable m_toolParameters;        
        private String [] m_uframeProfileNames;
        private Hashtable m_uframeParameters;        
        private int [] uframesAsFixedTCP;
        private ArrayList m_listOfRobotTargets;
        private ArrayList m_listOfAuxTargets;
        private ArrayList m_listOfExtTargets;
        private ArrayList m_listOfSpeedValues;
        private ArrayList m_listOfAccuracyValues;
        private ArrayList m_conveyorObjectFrames;
        private ArrayList programCallNames;
        private ArrayList commentLines;
        private ArrayList inputNameNum, outputNameNum;
        private int lastCnum = 0, lastPnum = 0;
        private int iDigit = 0, pvarTypeSet = 0, bpvarTypeSet = 0;
        private ArrayList alTargetAttr;
        private Hashtable m_targetAttr;
        private Hashtable m_targetTypes;
        private Hashtable m_targetTypesAux;
        private Hashtable m_targetTypesExt;
        private Hashtable m_targetTools;
        private Hashtable m_targetUframes;
        private Hashtable m_tagNames;
        private Hashtable m_toolNumberMapping;
        private Hashtable m_uframeNumberMapping;
        private ArrayList p_listOfRobotTargets;
        private ArrayList p_listOfAuxTargets;
        private ArrayList p_listOfExtTargets;
        private Hashtable p_targetAttr;
        private Hashtable p_targetTypes;
        private Hashtable p_targetTypesAux;
        private Hashtable p_targetTypesExt;
        private Hashtable p_targetTools;
        private Hashtable p_targetUframes;
        private Hashtable p_tagNames;
        private String prgFileEncoding = "US-ASCII";
        private String uploadStyle = "General";
        private String m_controller = "MRC";
        private util kku;
        private static BufferedWriter bw;

        private String [] saCfgTurns = {"0", "0", "0", "0", "0"};
        private ArrayList m_cfgTurns;
        private ArrayList p_cfgTurns;
        private Element m_firstRobotMotion = null;
        private Element m_lastRobotMotion = null;
        private int m_servoAttribsAdded = 0;
        private int m_servoUpload = 0;
        private int m_robotMotionCompleted = 0;
        private String attrSec = varDef;

        //Constructor
        public MotomanUploader(String [] parameters) throws ParserConfigurationException
        {
            super(parameters);
            m_targetType = 1; // default ///PULSE (JOINT)
            m_numCStatements = 0;
            m_commentCounter = 1;
            m_rbtLangCounter = 1;
            m_delayCounter = 1;
            m_moveCounter = 1;
            m_opMoveCounter = 1;
            m_spotWeldCounter = 1;
            m_spotDryCounter = 1;
            m_spotPickCounter = 1;
            m_spotPlaceCounter = 1;
            programCallCount = 0;
            robotCallNumber = 1;
            commentCount = 0;
            jobCount = 0;
            arcStartCount = 1;
            arcEndCount = 1;

            m_mountedTool = parameters[6];
            mountedTools = new ArrayList(1);
            mountedTools.add(0, m_mountedTool);

            String [] toolProfileNames = parameters[7].split("[\\t]+");
            String [] homeNames = parameters[9].split("[\\t]+");
            String [] objProfileNames = parameters[11].split("[\\t]+");

            //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1
            m_numRobotAxes = super.getNumberOfRobotAxes();
            m_numAuxAxes   = super.getNumberOfRailAuxiliaryAxes();
            m_numExtAxes   = super.getNumberOfToolAuxiliaryAxes();
            m_numWorkAxes  = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();
            m_numDOF = m_numRobotAxes + m_numAuxAxes + m_numExtAxes + m_numWorkAxes;

            m_bcAxisMapping = new int[m_numAuxAxes];
            m_ecAxisMapping = new int[m_numExtAxes + m_numWorkAxes];
            m_resolution = new double[m_numDOF];
            m_zeroOffset = new double[m_numDOF];

            m_currentTagPoint = new String("");
            m_currentToolNumber = 0;
            m_currentUframeNumber = 0;
            m_currentUframeName = null;

            m_keywords = new Pattern [300];
            m_axisTypes = new String[m_numDOF];
            m_axisTypes = super.getAxisTypes();
            m_listOfRobotTargets = new ArrayList(5);
            m_listOfAuxTargets = new ArrayList(2);
            m_listOfExtTargets = new ArrayList(2);
            m_listOfSpeedValues = new ArrayList(5);
            m_listOfAccuracyValues = new ArrayList(3);
            m_conveyorObjectFrames = new ArrayList(3);
            alTargetAttr = new ArrayList();
            m_targetAttr = new Hashtable();
            m_targetTypes = new Hashtable();
            m_targetTypesAux = new Hashtable();
            m_targetTypesExt = new Hashtable();
            m_cfgTurns = new ArrayList(5);
            p_cfgTurns = new ArrayList(5);
            m_targetTools = new Hashtable();
            m_targetUframes = new Hashtable();
            m_tagNames = new Hashtable();
            m_toolNumberMapping = new Hashtable();
            m_toolParameters = new Hashtable();
            m_uframeNumberMapping = new Hashtable();
            m_uframeParameters = new Hashtable();
            m_programLines = new ArrayList(1);

            p_listOfRobotTargets = new ArrayList(5);
            p_listOfAuxTargets = new ArrayList(2);
            p_listOfExtTargets = new ArrayList(2);
            p_targetAttr = new Hashtable();
            p_targetTypes = new Hashtable();
            p_targetTypesAux = new Hashtable();
            p_targetTypesExt = new Hashtable();
            p_targetTools = new Hashtable();
            p_targetUframes = new Hashtable();
            p_tagNames = new Hashtable();
            
            weldHome = new String("Home_1");
            closeHome = new String("Home_2");
            openHome = new String("Home_3");
            programCallNames = new ArrayList(1);
            commentLines = new ArrayList(2);
            inputNameNum = new ArrayList(1);
            outputNameNum = new ArrayList(1);
            
            kku = new util();
             
            m_xmlDoc = super.getDOMDocument();

            int ii;
            m_toolProfileNames = new String [NumOfTools];
            for (ii=0; ii<NumOfTools; ii++)
            {
                if (ii < toolProfileNames.length)
                    m_toolProfileNames[ii] = toolProfileNames[ii];
            }
            
            m_uframeProfileNames = new String [NumOfObjectProfiles];
            uframesAsFixedTCP = new int[NumOfObjectProfiles];
            for (ii=0; ii<NumOfObjectProfiles; ii++)
            {
                if (ii < objProfileNames.length)
                    m_uframeProfileNames[ii] = objProfileNames[ii];
                
                uframesAsFixedTCP[ii] = -1;
            }
            
            toolFileExists = false;
            uframeFileExists = false;
            NumCircularMoves = 0;
            EndOfArmToolingFirst = true;
            m_commentWaitSignal = true;
            m_commentRefp = false;
            m_commentSftOn = false;

            for (ii = 0; ii < m_numAuxAxes; ii++)
                m_bcAxisMapping[ii] = ii + m_numRobotAxes + 1;

            for (ii = 0; ii < m_numExtAxes + m_numWorkAxes; ii++)
                m_ecAxisMapping[ii] = ii + m_numRobotAxes + m_numAuxAxes + 1;

            for (ii = 0; ii < m_resolution.length; ii++)
                m_resolution[ii] = Double.NaN;
             
            for (ii = 0; ii < m_resolution.length; ii++)
                m_zeroOffset[ii] = Double.NaN;

            if (homeNames.length > 0)
                weldHome = homeNames[0];
            if (homeNames.length > 1)
                closeHome = homeNames[1];
            if (homeNames.length > 2)
                openHome = homeNames[2];

            Hashtable m_parameterData = super.getParameterData();
            Enumeration parameterKeys = m_parameterData.keys();
            Enumeration parameterValues = m_parameterData.elements();
            int index, arrayIndex;
            String pName, pValue, testString;
            while (parameterKeys.hasMoreElements() == true && parameterValues.hasMoreElements() == true)
            {
                pName = parameterKeys.nextElement().toString();
                pValue = parameterValues.nextElement().toString();
                index = pName.indexOf('.');
                testString = pName.substring(0, index + 1);

                try
                {
                    if(testString.equalsIgnoreCase("PulseValue.")) {
                        arrayIndex = Integer.parseInt(pName.substring(index + 1));
                        m_resolution[arrayIndex - 1] = Double.parseDouble(pValue);
                    }
                    else if (testString.equalsIgnoreCase("ZeroValue.")) {
                        arrayIndex = Integer.parseInt(pName.substring(index + 1));
                        m_zeroOffset[arrayIndex - 1] = Double.parseDouble(pValue);
                    }
                    else if (testString.equalsIgnoreCase("BC.")) {
                        arrayIndex = Integer.parseInt(pName.substring(index + 1));
                        m_bcAxisMapping[arrayIndex - 1] = Integer.parseInt(pValue);
                    }
                    else if (testString.equalsIgnoreCase("EC.")) {
                        arrayIndex = Integer.parseInt(pName.substring(index + 1));
                        m_ecAxisMapping[arrayIndex - 1] = Integer.parseInt(pValue);
                    }
                    else if (pName.equalsIgnoreCase("EndOfArmToolingFirst")) {
                        EndOfArmToolingFirst = new Boolean(pValue).booleanValue();
                    }
                    else if (testString.equalsIgnoreCase("Tool.")) {
                        arrayIndex = Integer.parseInt(pName.substring(index + 1));
                        if (mountedTools.size() > arrayIndex)
                        {
                            mountedTools.set(arrayIndex, pValue);
                        }
                        else
                        {
                            for (ii=mountedTools.size(); ii<arrayIndex; ii++)
                                mountedTools.add(ii, null);

                            mountedTools.add(arrayIndex, pValue);
                        }
                    }
                    else if(testString.equalsIgnoreCase("ToolProfile.")) {
                        String tool_ID = pName.substring(index + 1);
                        m_toolNumberMapping.put(tool_ID, pValue);
                    }
                    else if(testString.equalsIgnoreCase("ObjectProfile.")) {
                        String objProfile_ID = pName.substring(index + 1);
                        m_uframeNumberMapping.put(objProfile_ID, pValue);
                    }
                    else if(pName.equalsIgnoreCase("ProgramFileEncoding")) {
                        prgFileEncoding = pValue;
                    }
                    else if(pName.equalsIgnoreCase("OLPStyle")) {
                        uploadStyle = pValue;
                    }
                    else if(pName.equalsIgnoreCase("Controller")) {
                        m_controller = pValue;
                    }
                    else if(pName.equalsIgnoreCase("XRCPositionLevel")) {
                        xrc_pl = Integer.parseInt(pValue);
                    }
                    else if (pName.equalsIgnoreCase("CommentWaitSignal")) {
                        if (pValue.equalsIgnoreCase("false") == true)
                            m_commentWaitSignal = false;
                    }
                    else if (pName.equalsIgnoreCase("CommentRefp")) {
                        if (pValue.equalsIgnoreCase("true") == true)
                            m_commentRefp = true;
                    }
                    else if (pName.equalsIgnoreCase("IOName")) {
                        if (pValue.endsWith("[]"))
                            ioName = pValue.substring(0, pValue.lastIndexOf(']'));
                        else
                            ioName = pValue;
                    }
                    else if(pName.equalsIgnoreCase("ConveyorOffset")) {
                        _ConveyorOffset = Double.parseDouble(pValue);
                    }
                    else if(pName.equalsIgnoreCase("MotomanDefaultAccuracy")) {
                        m_MotomanDefaultAccuracy = Double.parseDouble(pValue);
                    }
                    else if(pName.equalsIgnoreCase("MotomanDefaultJntAccuracy")) {
                        m_MotomanDefaultJntAccuracy = Double.parseDouble(pValue);
                    }
                    else if(pName.startsWith("MotomanPL")) {
                        int PLNameLength = new String("MotomanPL").length();
                        int iPL=0;
                        if (pName.length() > PLNameLength)
                            iPL = Integer.parseInt(pName.substring(PLNameLength));
                        if (iPL > 0 && iPL < 9)
                            mPL[iPL] = Double.parseDouble(pValue)/1000;
                    }
                    else if (pName.equalsIgnoreCase("ServoGun")) {
                        m_servoUpload = Integer.valueOf(pValue).intValue();
                    }
                    else if (pName.equalsIgnoreCase("DownloadTarget")) {
                        if (pValue.equalsIgnoreCase("Cartesian") || pValue.equalsIgnoreCase("RECTAN"))
                            uploadCreateTags = true;
                    }
                }

                finally {
                    continue;
                }
            }

            for (ii = 0; ii < 300; ii++)
            {
                m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);
            }

            String sFlt  = "(\\d*\\.?\\d+|\\d+\\.?\\d*)";
            String timer = "\\s+T\\s*=\\s*" + sFlt;
            String port = "#\\s*\\(\\s*(\\d+)\\s*\\)\\s*";
            String portORvariable = "#\\s*\\(\\s*(\\d+|\\w+)\\s*\\)\\s*";
            String inputCond = "\\s+IN" + port + "=\\s*(ON|OFF)";
            String eqInt = "\\s*=\\s*(\\d+)\\s*";
            String eqIntOrVar = "\\s*=\\s*(\\d+|\\w+)\\s*";
            String maybeEqInt = "\\s*=?\\s*(\\d+)?\\s*";
            String svSpotOnly = "\\s*GUN" + port + "PRESS" + portORvariable + "WTM" + eqIntOrVar + "WST" + eqInt;

            m_keywords[0] = Pattern.compile("^///\\s*NPOS", Pattern.CASE_INSENSITIVE);
            m_keywords[1] = Pattern.compile("^///\\s*PULSE\\s*$", Pattern.CASE_INSENSITIVE);
            m_keywords[2] = Pattern.compile("^///\\s*RECTAN\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[3] = Pattern.compile("^C[0-9]+", Pattern.CASE_INSENSITIVE);
            m_keywords[4] = Pattern.compile("^BC[0-9]+", Pattern.CASE_INSENSITIVE);
            m_keywords[5] = Pattern.compile("^[SMY]*OV[LJC]", Pattern.CASE_INSENSITIVE);
            m_keywords[6] = Pattern.compile("^TIMER" + timer, Pattern.CASE_INSENSITIVE);
            m_keywords[7] = Pattern.compile("^'TAG", Pattern.CASE_INSENSITIVE);
            m_keywords[8] = Pattern.compile("^EC[0-9]+", Pattern.CASE_INSENSITIVE);
            m_keywords[9] = Pattern.compile("^DOUT\\s+OT" + port + "(ON|OFF)", Pattern.CASE_INSENSITIVE);
            m_keywords[10] = Pattern.compile("^PULSE\\s+OT" + port + "(" + timer + ")?", Pattern.CASE_INSENSITIVE);
            m_keywords[11] = Pattern.compile("^WAIT" + inputCond + "(" + timer + ")?", Pattern.CASE_INSENSITIVE);
            m_keywords[12] = Pattern.compile("^///\\s*TOOL\\s*([0-9]+)\\s*$", Pattern.CASE_INSENSITIVE);
            m_keywords[13] = Pattern.compile("^//\\s*NAME", Pattern.CASE_INSENSITIVE);
            m_keywords[14] = Pattern.compile("^SVSPOT" + svSpotOnly, Pattern.CASE_INSENSITIVE);
            m_keywords[15] = Pattern.compile("^SVGUNCL\\s*GUN" + port + "PRESSCL" + port, Pattern.CASE_INSENSITIVE);
            m_keywords[16] = Pattern.compile("^'\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[17] = Pattern.compile("^NOP\\s*$", Pattern.CASE_INSENSITIVE);
            m_keywords[18] = Pattern.compile("^END\\s*$", Pattern.CASE_INSENSITIVE);
            m_keywords[19] = Pattern.compile("^/JOB\\s*$", Pattern.CASE_INSENSITIVE);
            m_keywords[20] = Pattern.compile("^//POS\\s*$", Pattern.CASE_INSENSITIVE);
            m_keywords[21] = Pattern.compile("^//INST\\s*$", Pattern.CASE_INSENSITIVE);
            m_keywords[22] = Pattern.compile("^///DATE\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[23] = Pattern.compile("^///ATTR\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[24] = Pattern.compile("^///GROUP", Pattern.CASE_INSENSITIVE);
            m_keywords[25] = Pattern.compile("^///POSTYPE\\s+(PULSE|BASE|ROBOT|USER)", Pattern.CASE_INSENSITIVE);
            m_keywords[26] = Pattern.compile("^GUNCHG\\s*GUN" + port + "PICK", Pattern.CASE_INSENSITIVE);
            m_keywords[27] = Pattern.compile("^GUNCHG\\s*GUN" + port + "PLACE", Pattern.CASE_INSENSITIVE);
            m_keywords[28] = Pattern.compile("^CALL\\s+JOB:", Pattern.CASE_INSENSITIVE);
            //m_keywords[29] = Pattern.compile("^\\*\\s*$", Pattern.CASE_INSENSITIVE);
            //m_keywords[30] = Pattern.compile("^JUMP\\s+\\*\\w*\\s+IF" + inputCond, Pattern.CASE_INSENSITIVE);
            m_keywords[31] = Pattern.compile("^ARCON\\s+AC=", Pattern.CASE_INSENSITIVE);
            m_keywords[32] = Pattern.compile("^ARCON\\s+ASF#\\(\\d+\\)", Pattern.CASE_INSENSITIVE);
            m_keywords[33] = Pattern.compile("^ARCOF\\s+AC=", Pattern.CASE_INSENSITIVE);
            m_keywords[34] = Pattern.compile("^ARCOF\\s+AEF#\\(\\d+\\)", Pattern.CASE_INSENSITIVE);
            m_keywords[35] = Pattern.compile("^P[0-9]+", Pattern.CASE_INSENSITIVE);
            m_keywords[36] = Pattern.compile("^BP[0-9]+", Pattern.CASE_INSENSITIVE);
            m_keywords[37] = Pattern.compile("^EX[0-9]+", Pattern.CASE_INSENSITIVE);
            m_keywords[38] = Pattern.compile("^///COMM\\s*", Pattern.CASE_INSENSITIVE);
			m_keywords[39] = Pattern.compile("(PLIN)?" + maybeEqInt + "(PLOUT)?" + maybeEqInt + "CLF" + port + svSpotOnly + "(WGO)?" + maybeEqInt, Pattern.CASE_INSENSITIVE);
            m_keywords[40] = Pattern.compile("^///LIMIT\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[41] = Pattern.compile("///RCONF\\s+(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+)(?:,\\s*(\\d+))?", Pattern.CASE_INSENSITIVE);
            m_keywords[42] = Pattern.compile("^///USER\\s+(\\d+)", Pattern.CASE_INSENSITIVE);
            m_keywords[43] = Pattern.compile("^////FRAME\\s+", Pattern.CASE_INSENSITIVE);
			// DLY Start 2008/04/30 - added TSYNC support
			m_keywords[44] = Pattern.compile("^TSYNC\\s*([0-9]*)\\s*SNUM\\s*=\\s*([0-9]*)", Pattern.CASE_INSENSITIVE);
			// DLY End 2008/04/30
			// DLY Start 2008/08/10 - added REFP support
			m_keywords[45] = Pattern.compile("^[SR]*EFP", Pattern.CASE_INSENSITIVE);
			// DLY End
			m_keywords[46] = Pattern.compile("^SFTON\\s+P[0-9]+", Pattern.CASE_INSENSITIVE);
			m_keywords[47] = Pattern.compile("^SFTOF", Pattern.CASE_INSENSITIVE);
		    m_keywords[48] = Pattern.compile("^SYSTART\\s*(CV#\\([1-9]*\\))\\s*STP=([-.0-9]*)", Pattern.CASE_INSENSITIVE);
            m_keywords[49] = Pattern.compile("^EIMOV[LC]\\s+UF" + port, Pattern.CASE_INSENSITIVE);
        }

    	public static void main(String [] parameters) throws SAXException, ParserConfigurationException, TransformerConfigurationException, NumberFormatException, StringIndexOutOfBoundsException
        {
            
            MotomanUploader uploader = new MotomanUploader(parameters);
            String [] additionalProgramNames = null;
            if (parameters.length > 12 && parameters[12].trim().length() > 0)
                additionalProgramNames = parameters[12].split("[\\t]+");
            
            uploader.createAccuracyProfile("-1", "MotomanDefault");
            uploader.createAccuracyProfile("-2", "MotomanDefaultJnt");
            
            //Get the robot program and parse it line by line
            String robotProgramName = uploader.getPathToRobotProgramFile();
            File f = new File(robotProgramName);
            String path = f.getParent() + f.separator;
            String robotProgramNameOnly = f.getName();

            String uploadInfoFileName = uploader.getPathToErrorLogFile();
            Matcher match;

            try
            {
                bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));
                bw.write(VERSION);
                bw.write(COPYRIGHT);

                // Scan for EIMOVL|C (fixed TCP)
                uploader.scanForEIMOV(robotProgramName, additionalProgramNames);
                
                File toolCND = new File( path + "TOOL.CND" );
                if (toolCND.exists())
                {
                    bw.write("\nTool file TOOL.CND found.\n");
                    toolFileExists = true;
                    try
                    {
                        uploader.processToolFile(toolCND);
                    }
                    catch (NumberFormatException e)
                    {
                        bw.write(e.toString() + "\n");
                        bw.write("ERROR: The file format is not supported and the file is ignored.\n");
                        toolFileExists = false;
                    }
                    catch (Exception e)
                    {
                        bw.write(e.toString() + "\n");
                        bw.write("ERROR: The file format is not supported and the file is ignored.\n");
                        toolFileExists = false;
                    }
                    bw.write("\n");
                }

                File uframeCND = new File( path + "UFRAME.CND" );
                if (uframeCND.exists())
                {
                    bw.write("\nUFrame file UFRAME.CND found.\n");
                    uploader.processUframeFile(uframeCND);
                    uframeFileExists = true;
                }
                else
                {
		            Element objFrameProfileElem;
		            Element objfpList = uploader.getObjectFrameProfileListElement();			    	
		            double [] uframeValues = {0.0,0.0,0.0,0.0,0.0,0.0};		            
		            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType eObjFrameTypeWorld = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
		            objFrameProfileElem = uploader.createObjectFrameProfile(objfpList, "Default", eObjFrameTypeWorld, uframeValues);			    	                
                }
                
                int errCodeMain, errCode;
                errCodeMain = uploader.processRobotProgram(robotProgramName);

                String sAddProg = null;
                int index;
                if (additionalProgramNames != null)
                {
                    for (int ii=0; ii<additionalProgramNames.length; ii++)
                    {
                        f = new File(additionalProgramNames[ii]);
                        sAddProg = f.getName();
                        index = uploader.programCallNames.indexOf(sAddProg);
                        if (index < 0)
                        {
                            uploader.programCallNames.add(uploader.programCallCount, sAddProg);
                            uploader.programCallNames.trimToSize();
                            uploader.programCallCount++;
                        }
                    }
                }
                
                if (errCodeMain == 0)
                {
                    String calledProgName;
                    for (int ii=0; ii<uploader.programCallCount; ii++)
                    {
                        calledProgName = (String)uploader.programCallNames.get(ii);
                        if (calledProgName.equalsIgnoreCase(robotProgramNameOnly))
                            continue; // skip the "main" program already uploaded
                        
                        robotProgramName = path + calledProgName;
                        errCode = uploader.processRobotProgram(robotProgramName);
                    }
                }

                if (errCodeMain == 0)
                {
                    bw.write("\nRobot program upload successful.\n");
                }
                else
                {
                    bw.write("\nERROR-Robot program upload failed.\n");
                }

                bw.flush();
                bw.close();
            }//end try

            catch (IOException e) {
                e.getMessage();
            }
            Element motionProfileList = uploader.getMotionProfileListElement();
            if (motionProfileList.hasChildNodes() == false)
                uploader.createSpeedProfile("Default");

            if (uploader.m_toolParameters.size() > 0 || uploader.m_uframeParameters.size() > 0)
            {
	            Element resourceElement = uploader.getResourceElement();
	         
	            String [] parameterNames = new String [uploader.m_toolParameters.size()+uploader.m_uframeParameters.size()];
	            String [] parameterValues = new String [uploader.m_toolParameters.size()+uploader.m_uframeParameters.size()];
	
	        	int ii = 0;
	        	for (int toolcount = 0;toolcount < uploader.NumOfTools;toolcount++)
	        	{
	        		String parmName = "ToolProfile." + String.valueOf(toolcount);
	        		if (uploader.m_toolParameters.containsKey(parmName))
	        		{		 
	        			parameterNames[ii] = parmName;
	        			parameterValues[ii] = (String)uploader.m_toolParameters.get(parmName);
	        			ii++;
	        		}
	        	}
	    
	        	for (int uframecount = 0;uframecount < uploader.NumOfObjectProfiles;uframecount++)
	        	{
	        		String parmName = "ObjectProfile." + String.valueOf(uframecount);
	        		if (uploader.m_uframeParameters.containsKey(parmName))
	        		{		 
	        			parameterNames[ii] = parmName;
	        			parameterValues[ii] = (String)uploader.m_uframeParameters.get(parmName);
	        			ii++;
	        		}
	        	}
	        	
	            uploader.createParameterList(resourceElement, parameterNames, parameterValues);
            }
            
            try {
                uploader.writeXMLStreamToFile();
            }

            catch (IOException e) {
                e.getMessage();
            }
            catch (TransformerException e) {
                e.getMessage();
            }
            catch (Exception e) {
                e.getMessage();
            }
        }//end main

        private void scanForEIMOV(String robotProgramName, String[] additionalProgramNames)
        {
            BufferedReader rbt;
            Matcher match;
            int ii, jj, numSelProgs = 1;
            if (null != additionalProgramNames)
                numSelProgs += additionalProgramNames.length;
            String [] selectedPrograms = new String[numSelProgs];
            
            selectedPrograms[0] = robotProgramName;
            if (numSelProgs > 1)
                for (ii = 0; ii < numSelProgs - 1; ii++)
                    selectedPrograms[ii+1] = additionalProgramNames[ii];
            
            for (jj = 0; jj < numSelProgs; jj++)
            {
                try
                {
                    rbt = new BufferedReader(new InputStreamReader(new FileInputStream(selectedPrograms[jj]), prgFileEncoding));
                    String line = rbt.readLine();
                    while (line != null)
                    {
                        if (line.equals(""))
                        {
                            line = rbt.readLine();
                            continue;
                        }
                        if (line.equalsIgnoreCase("END"))
                            break;

                        for (ii = 0; ii < m_keywords.length; ii++)
                        {
                            match = m_keywords[ii].matcher(line);

                            if (match.find() == true)
                            {
                                switch (ii)
                                {
                                    case 28: // CALL JOB
                                        break;
                                    case 49: // EIMOVL|C (fixed TCP moves)
                                        uframesAsFixedTCP[Integer.parseInt(match.group(1))] = 1;
                                        break;
                                }
                            }
                        }

                        line = rbt.readLine();
                    } // while
                } // try

                catch (IOException e) {
                    try {
                        bw.write("ERROR: "+e.getMessage()+"\n");
                    }
                    catch (IOException bwe) {
                    }
                }
            }
        }
        
        private void addToArrayList(ArrayList arrayList, String str)
        {
            int index = arrayList.indexOf(str);
            if (index < 0)
            {
                int num = arrayList.size();
                if (num == 0)
                {
                    arrayList.add(0, str);
                }
                else
                {
                    int ii, val;
                    for (ii=0; ii<num; ii++)
                    {
                        val = Integer.parseInt((String)arrayList.get(ii));
                        if ( val > Integer.parseInt(str))
                        {
                            arrayList.add(ii, str);
                            break;
                        }
                        if (ii+1 == num)
                            arrayList.add(num, str);
                    }
                }
            }
        } // end addToArrayList

        private int processRobotProgram(String robotProgramName)
        {
            BufferedReader rbt;
            int errCode;

            try
            {
                rbt = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));
                
                bw.write("Translating program file \"");
                bw.write(robotProgramName);
                bw.write("\"\n");
                
                iDigit = 0; // each program file can have different number of variable digits
                errCode = processRobotProgramLines(rbt);
                if (errCode != 0)
                    return errCode;
            }//end try

            catch (IOException e) {
                try {
                    bw.write("ERROR: "+e.getMessage()+"\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            return 0;
        }//end processRobotProgram

        private int processRobotProgramLines(BufferedReader rbt)
        {
            String line, gunJointType, sVarNum;
            String [] components;
            Matcher match;
            NodeList actNodes;
            Element actElem, activityListElem=null, attributeListElem;
            int errCode, offset;

            Element resourceElem = super.getResourceElement();

            try
            {
                line = rbt.readLine();
                int lineNum = 0;
                while (line != null)
                {
                    if (line.equals(""))
                    {
                        line = rbt.readLine();
                    }
                    m_programLines.add(lineNum, line);
                    lineNum++;
                    if (line.equalsIgnoreCase("END"))
                    {
                         break;
                    }
                    line = rbt.readLine();
                } // while
                for (int kk=0;kk<m_programLines.size();kk++)
                {
                	m_programLineNum = kk;
                	line = (String)m_programLines.get(kk);
                    for (int ii = 0; ii < m_keywords.length; ii++)
                    {
                        match = m_keywords[ii].matcher(line);

                        if (match.find() == true)
                        {
                            //Call appropriate methods to handle the input
                            switch (ii)
                            {
                                case 0: processNposStatement(line);
                                    break;
                                case 1: m_targetType = JOINT;
                                    break;
                                case 2: m_targetType = CARTESIAN;
                                    break;
                                case 3: // Cvar definition
                                    if (iDigit == 0)
                                    {
                                        components = line.split("[=, \\t]+");
                                        sVarNum = components[0].substring(1).trim();
                                        iDigit = sVarNum.length();
                                        addAttribute(activityListElem, "PosVarDigits", String.valueOf(iDigit));
                                    }
                                    processCStatement(line);
                                    break;
                                case 4:
                                case 8: processBCorECStatement(line, ii);
                                    break;
                                case 5:
                                    if (0 != processMOVStatement(line, activityListElem))
                                        preProcessComment(line, activityListElem, rbtLang);
                                    break;
                                case 6: processTimerStatement(line, activityListElem);
                                    break;
                                case 7: processTagStatement(line);
                                    break;
                                case 9: processDOUTStatement(line, match,activityListElem, "0");
                                    break;
                                case 10: processPULSEStatement(line, match, activityListElem);
                                    break;
                                case 11: processWAITStatement(line, match, activityListElem);
                                    break;
                                case 12: m_currentToolNumber = Integer.parseInt(match.group(1));
                                    break;
                                case 13: // //NAME
                                    activityListElem = processNAMEStatement(line);
                                    break;
                                case 14: // SVSPOT GUN#(1)
                                    if (EndOfArmToolingFirst == true)
                                        offset = 0;
                                    else
                                        offset = m_numWorkAxes;
                                    // for throwing exception
                                    for (int jj=0; jj<m_numExtAxes; jj++)
                                        gunJointType = m_axisTypes[m_numRobotAxes + jj + offset];
                                    processSpotWeldStatement(line, match, activityListElem);
                                    break;
                                case 15:
                                    if (EndOfArmToolingFirst == true)
                                        offset = 0;
                                    else
                                        offset = m_numWorkAxes;
                                    // for throwing exception
                                    for (int jj=0; jj<m_numExtAxes; jj++)
                                        gunJointType = m_axisTypes[m_numRobotAxes + jj + offset];
                                    processSpotDryStatement(line, match, activityListElem);
                                    break;
                                case 16: processComment(line, activityListElem, "Comment");
                                    break;
                                case 17: // NOP
                                    if (lastCnum >= 0)
                                    {
                                        setTargetAttr(lastCnum, "C", "POST");
                                        lastCnum = -1;
                                    }
                                    if (lastPnum >= 0)
                                    {
                                        setTargetAttr(lastPnum, "P", "POST");
                                        lastPnum = -1;
                                    }
                                    addAttributeList(activityListElem, "Pre");
                                    break;
                                case 18: // END
                                    actNodes = activityListElem.getElementsByTagName("Activity");
                                    if (actNodes.getLength() > 0)
                                    {
                                    actElem = (Element)actNodes.item(actNodes.getLength()-1);
                                    setLastCircular(actElem);
                                    }
                                    addAttributeList(activityListElem, "Post");
                                    break;
                                case 19: // /JOB
                                    break;
                                case 20:
                                    break;
                                case 21:
                                    attrSec = rbtLang; // variable definition ends; instruction section starts
                                    break;
                                case 22:
                                    break;
                                case 23: // ///ATTR
                                case 24: // ///GROUP
                                case 38: // ///COMM
                                case 43: // ////FRAME
                                    commentLines.add(commentCount, rbtLang + ":" + line);
                                    commentCount++;
                                    break;
                                case 25: // ///POSTYPE (PULSE|BASE|ROBOT|USER)
                                    if (match.group(1).equalsIgnoreCase("ROBOT"))
                                        m_currentUframeNumber = 0;
                                    if (match.group(1).equalsIgnoreCase("BASE"))
                                        m_currentUframeNumber = 1;
                                    break;
                                case 26: processSpotToolPickStatement(line, match, activityListElem);
                                    break;
                                case 27: processSpotToolPlaceStatement(line, match, activityListElem);
                                    break;
                                case 28: processCallJobStatement(line, activityListElem);
                                    break;
                                case 31: 
                                case 32: processArcOn(line, activityListElem);
                                    break;
                                case 33:
                                case 34: processArcOff(line, activityListElem);
                                    break;
                                case 35: // P variable declaration
                                    if (pvarTypeSet == 0)
                                    {
                                        pvarTypeSet = 1;
                                        String sTmp = "PULSE";
                                        if (m_targetType == CARTESIAN)
                                            sTmp = "RECTAN";
                                        addAttribute(activityListElem, "DownloadPvar", sTmp);
                                    }
                                    processPStatement(line);
                                    break;
                                case 36: // BP variable declaration
                                    if (bpvarTypeSet == 0)
                                    {
                                        bpvarTypeSet = 1;
                                        String sTmp = "PULSE";
                                        if (m_targetType == CARTESIAN)
                                            sTmp = "RECTAN";
                                        addAttribute(activityListElem, "DownloadBPvar", sTmp);
                                    }
                                    processBPorEXStatement(line, ii);
                                    break;
                                case 37: // EX variable declaration
                                    processBPorEXStatement(line, ii);
                                    break;
                                case 39: processMOVStatement(line, activityListElem);
                                         if (EndOfArmToolingFirst == true)
                                         {
                                             offset = 0;
                                         }
                                         else
                                         {
                                             offset = m_numWorkAxes;
                                         }
                                         // for throwing exception
                                         for (int jj=0; jj<m_numExtAxes; jj++)
                                            gunJointType = m_axisTypes[m_numRobotAxes + jj + offset];
                                         processSpotWeldMovStatement(line, match, activityListElem);
                                    break;
                                case 40: alTargetAttr.add(line);
                                    break;
                                case 41: setConfigTurns(match);
                                    break;
                                case 42: m_currentUframeNumber = Integer.parseInt(match.group(1)) + 1;
                                    break;
                                // DLY Start 2008/08/10
								case 44: processTSYNCStatement(match, activityListElem);
									break;
								case 45:
									  // IR 067476 added robot language comment for REFP rather than a move
									  if (m_commentRefp == true)
									  {
						                  if (uploadStyle.equalsIgnoreCase("OperationComments") || uploadStyle.equalsIgnoreCase("Honda"))
						                  {
						                      processComment(line, activityListElem, rbtLang);
						                  }
						                  else
						                  {
						                      commentLines.add(commentCount, rbtLang + ":" + line);
						                      commentCount++;
						                  }
									  }
					                  else
					                  {
					                	  processMOVStatement(line, activityListElem); // REFP
					                  }
									break;
								// DLY End
                                case 46:
                                    m_commentSftOn = false;
                                    if (0 != processSFTON(line))
                                    {
                                        preProcessComment(line, activityListElem, rbtLang);
                                        m_commentSftOn = true;
                                    }
                                    break;
                                case 47:
                                    if (true == m_commentSftOn)
                                        preProcessComment(line, activityListElem, rbtLang);
                                    m_currentUframeName = null;
                                    break;
                                // DLY Start 2009/05/02 - added linetracking support
                                case 48: processSYSTARTStatement(line, match, activityListElem);
                                	break;
                                // DLY End 2009/05/02
                                case 49: // "EIMOV[LC]\\s+UF#\\(\\d+\\)" UFrame number set to Tool
                                    m_currentToolNumber = Integer.parseInt(match.group(1));
                                    if (0 != processMOVStatement(line, activityListElem))
                                        preProcessComment(line, activityListElem, rbtLang);
                                    break;
                            }//end switch

                            //match found & processed, break out for loop
                            break;
                        }//end if
                        else
                        {
                            if (ii == (m_keywords.length - 1))
                            {
                                if (uploadStyle.equalsIgnoreCase("OperationComments") || uploadStyle.equalsIgnoreCase("Honda"))
                                {
                                    if (attrSec.equals(varDef))
                                    {
                                        commentLines.add(commentCount, attrSec + ":" + line);
                                        commentCount++;
                                    }
                                    else
                                    {
                                        processComment(line, activityListElem, attrSec);
                                    }
                                }
                                else
                                {
                                    commentLines.add(commentCount, attrSec + ":" + line);
                                    commentCount++;
                                }
                            }
                        }
                    }//end for
                    
                    /* AFTER processing the END line(add PostComment)
                     * skip EndOfFile characters added by some system
                     */
                    if (line.equalsIgnoreCase("END"))
                        break;
                    
                    // line = rbt.readLine();

                }//end for
                rbt.close();
            }//end try

            catch (IOException e) {
                e.getMessage();
                return 1;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-Robot must have an external axis to upload Servo Gun program.\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            return 0;
        } // end of processRobotProgramLines

        private void processNposStatement(String line) throws NumberFormatException {
            //figure out the number of targets and aux axes
            String [] components = line.split("[, \\t]+");
            for(int ii = 0; ii <components.length; ii++)

            try {
            }

            catch (NumberFormatException e) {
                e.getMessage();
            }
        }

        private Element processNAMEStatement(String line) {
            int lastIndex = line.indexOf('E');
            String programName = line.substring(lastIndex + 1).trim();

            Element activityListElem = super.createActivityList(programName);

            Element resourceElem = super.getResourceElement();
            resourceElem.appendChild(activityListElem);

            return activityListElem;
        }

        private void processCStatement(String line)
        {
            int targetNumber = 0, arraySize, ii;
            Integer targetInteger;
            //parse the string into array
            String [] components = line.split("[=, \\t]+");
            String [] targetCfgTurns = {"0", "0", "0", "0", "0"};
            /* array of String targetCfgTurns must be local
             * and its values individually assigned for
             * ArrayList m_cfgTurns to work
             */
            for (ii=0; ii<targetCfgTurns.length; ii++)
                targetCfgTurns[ii] = saCfgTurns[ii];
            //Eliminate the starting letter 'C' in the target name
            components[0] = components[0].substring(1).trim();
            //Get the target number as an object
            targetInteger = new Integer(components[0]);
            targetNumber = Integer.parseInt(components[0]);

            //Add the target to a dynamic array
            arraySize = m_listOfRobotTargets.size();
            if (arraySize >= targetNumber+1)
            {
                m_listOfRobotTargets.set(targetNumber, components);
                m_cfgTurns.set(targetNumber, targetCfgTurns);
            }
            else
            {
                for (ii=arraySize; ii<targetNumber; ii++)
                {
                    m_listOfRobotTargets.add(ii, "Target Undefined");
                    m_cfgTurns.add(ii, "Config and Turns Undefined");
                }
                
                m_listOfRobotTargets.add(targetNumber, components);
                m_cfgTurns.add(targetNumber, targetCfgTurns);
            }
            //Resize the array
            m_listOfRobotTargets.trimToSize();
            m_cfgTurns.trimToSize();

            //Connect the array with the target type through hashtable
            m_targetTypes.put(targetInteger, new Integer(m_targetType));
            
            setTargetAttr(targetNumber, "C", "PRE");
            lastCnum = targetNumber;
            
            // associate Tool & Uframe with Target
            m_targetTools.put(targetInteger, new Integer(m_currentToolNumber));
            m_targetUframes.put(targetInteger, new Integer(m_currentUframeNumber));

            //Connect the array with tag names through hashtable
            if (!m_currentTagPoint.equals(""))
            {
                m_tagNames.put(targetInteger, m_currentTagPoint + ".");
                m_currentTagPoint = "";
            }
            else
            {
                String tpName = "C" + components[0];
                m_tagNames.put(targetInteger, tpName + ".");
            }
        } // end of processCStatement

        private void setTargetAttr(int iTgtNum, String sTgtType, String sPrePost)
        {
            int iKeyInc = 0, ii;
            String sKey = String.valueOf(iTgtNum * 1000) + sPrePost;
            if (sTgtType.equals("C"))
            {
                while (m_targetAttr.containsKey(sKey))
                {
                    iKeyInc++;
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + sPrePost;
                }
                // set target pre- and post-attributes
                for (ii=0; ii<alTargetAttr.size(); ii++)
                {
                    m_targetAttr.put(sKey, alTargetAttr.get(ii));
                    iKeyInc++;
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + sPrePost;
                }
            }
            else
            if (sTgtType.equals("P"))
            {
                while (p_targetAttr.containsKey(sKey))
                {
                    iKeyInc++;
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + sPrePost;
                }
                // set target pre- and post-attributes
                for (ii=0; ii<alTargetAttr.size(); ii++)
                {
                    p_targetAttr.put(sKey, alTargetAttr.get(ii));
                    iKeyInc++;
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + sPrePost;
                }
            }
            
            alTargetAttr.clear();
        } // setTargetAttr

        private void getTargetAttr(int iTgtNum, String sTgtType, Element actElem)
        {
            int iKeyInc = 0, ii, iTgtCount = 1;
            String sKey;
            String sTgtAttr = null;
            if (sTgtType.equals("C"))
            {
                sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "PRE";
                sTgtAttr = (String)m_targetAttr.get(sKey);
                while (sTgtAttr != null)
                {
                    iKeyInc++;
                    addAttribute(actElem, "PrePosAttr" + iKeyInc, sTgtAttr);
                    
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "PRE";
                    sTgtAttr = (String)m_targetAttr.get(sKey);
                }
                iKeyInc = 0;
                sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "POST";
                sTgtAttr = (String)m_targetAttr.get(sKey);
                while (sTgtAttr != null)
                {
                    iKeyInc++;
                    addAttribute(actElem, "PostPosAttr" + iKeyInc, sTgtAttr);
                    
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "POST";
                    sTgtAttr = (String)m_targetAttr.get(sKey);
                }
            }
            else
            if (sTgtType.equals("P"))
            {
                sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "PRE";
                sTgtAttr = (String)p_targetAttr.get(sKey);
                while (sTgtAttr != null)
                {
                    iKeyInc++;
                    addAttribute(actElem, "PrePosAttr" + iKeyInc, sTgtAttr);
                    
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "PRE";
                    sTgtAttr = (String)p_targetAttr.get(sKey);
                }
                iKeyInc = 0;
                sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "POST";
                sTgtAttr = (String)p_targetAttr.get(sKey);
                while (sTgtAttr != null)
                {
                    iKeyInc++;
                    addAttribute(actElem, "PostPosAttr" + iKeyInc, sTgtAttr);
                    
                    sKey = String.valueOf(iTgtNum * 1000 + iKeyInc) + "POST";
                    sTgtAttr = (String)p_targetAttr.get(sKey);
                }
            }
        }
        
        private void processPStatement(String line)
        {
            if (lastCnum >= 0)
            {
                setTargetAttr(lastCnum, "C", "POST");
                lastCnum = -1;
            }
            
            int targetNumber = 0, arraySize, ii;
            Integer targetInteger;
            //parse the string into array
            String [] components = line.split("[=, \\t]+");
            String [] targetCfgTurns = {"0", "0", "0", "0", "0"};
            /* array of String targetCfgTurns must be local
             * and its values individually assigned for
             * ArrayList p_cfgTurns to work
             */
            for (ii=0; ii<targetCfgTurns.length; ii++)
                targetCfgTurns[ii] = saCfgTurns[ii];
            //Eliminate the starting letter 'P' in the target name
            components[0] = components[0].substring(1).trim();
            //Get the target number as an object
            targetInteger = new Integer(components[0]);
            targetNumber = Integer.parseInt(components[0]);

            //Add the target to a dynamic array
            arraySize = p_listOfRobotTargets.size();
            if (arraySize >= targetNumber+1)
            {
                p_listOfRobotTargets.set(targetNumber, components);
                p_cfgTurns.set(targetNumber, targetCfgTurns);
            }
            else
            {
                for (ii=arraySize; ii<targetNumber; ii++)
                {
                    p_listOfRobotTargets.add(ii, "Target Undefined");
                    p_cfgTurns.add(ii, "Config and Turns Undefined");
                }
                
                p_listOfRobotTargets.add(targetNumber, components);
                    p_cfgTurns.add(targetNumber, targetCfgTurns);
                }
            //Resize the array
            p_listOfRobotTargets.trimToSize();
            p_cfgTurns.trimToSize();

            //Connect the array with the target type through hashtable
            p_targetTypes.put(targetInteger, new Integer(m_targetType));
            
            setTargetAttr(targetNumber, "P", "PRE");
            lastPnum = targetNumber;

            // associate Tool with Target
            p_targetTools.put(targetInteger, new Integer(m_currentToolNumber));
            p_targetUframes.put(targetInteger, new Integer(m_currentUframeNumber));

            //Connect the array with tag names through hashtable
            if (!m_currentTagPoint.equals(""))
            {
                p_tagNames.put(targetInteger, m_currentTagPoint + ".");
                m_currentTagPoint = "";
            }
            else
            {
                String tpName = "P" + components[0];
                p_tagNames.put(targetInteger, tpName + ".");
            }
        } // end of processPStatement

        private void processBCorECStatement(String line, int type)
        {
            if (lastCnum >= 0)
            {
                setTargetAttr(lastCnum, "C", "POST");
                lastCnum = -1;
            }
            
            int targetNumber = 0, arraySize, ii;
            //Parse the string into array
            Integer targetInteger;
            String [] components = line.split("[=, \\t]+");
            //Eliminate the starting letters 'BC' in the target name
            components[0] = components[0].substring(2);
            //Get the target number as an object
            targetInteger = new Integer(components[0]);
            targetNumber = Integer.parseInt(components[0].trim());
            //For BC axes
            if (type == 4)
            {
                //Add the target to a dynamic array
                arraySize = m_listOfAuxTargets.size();
                if (arraySize >= targetNumber+1)
                    m_listOfAuxTargets.set(targetNumber, components);
                else
                    for (ii=arraySize; ii<=targetNumber; ii++)
                        m_listOfAuxTargets.add(ii, components);

                //Resize the array
                m_listOfAuxTargets.trimToSize();
                
                m_targetTypesAux.put(targetInteger, new Integer(m_targetType));
            }
            //For EC axes
            else if (type == 8)
            {
                //Add the target to a dynamic array
                arraySize = m_listOfExtTargets.size();
                if (arraySize >= targetNumber+1)
                    m_listOfExtTargets.set(targetNumber, components);
                else
                    for (ii=arraySize; ii<=targetNumber; ii++)
                        m_listOfExtTargets.add(ii, components);

                //Resize the array
                m_listOfExtTargets.trimToSize();
                
                m_targetTypesExt.put(targetInteger, new Integer(m_targetType));
            }
        } // end of processBCorECStatement

        private void processBPorEXStatement(String line, int type)
        {
            if (lastCnum >= 0)
            {
                setTargetAttr(lastCnum, "C", "POST");
                lastCnum = -1;
            }
            if (lastPnum >= 0)
            {
                setTargetAttr(lastPnum, "P", "POST");
                lastPnum = -1;
            }
            
            int targetNumber = 0, arraySize, ii;
            //Parse the string into array
            String [] components = line.split("[=, \\t]+");
            //Eliminate the starting letters 'BP' or 'EX' in the target name
            components[0] = components[0].substring(2);
            //Get the target number as an object
            Integer targetInteger = new Integer(components[0]);
            targetNumber = Integer.parseInt(components[0].trim());
            //For BP axes
            if (type == 36)
            {
                //Add the target to a dynamic array
                arraySize = p_listOfAuxTargets.size();
                if (arraySize >= targetNumber+1)
                    p_listOfAuxTargets.set(targetNumber, components);
                else
                    for (ii=arraySize; ii<=targetNumber; ii++)
                        p_listOfAuxTargets.add(ii, components);

                //Resize the array
                p_listOfAuxTargets.trimToSize();
                p_targetTypesAux.put(targetInteger, new Integer(m_targetType));
            }
            //For EX axes
            else if (type == 37)
            {
                //Add the target to a dynamic array
                arraySize = p_listOfExtTargets.size();
                if (arraySize >= targetNumber+1)
                    p_listOfExtTargets.set(targetNumber, components);
                else
                    for (ii=arraySize; ii<=targetNumber; ii++)
                        p_listOfExtTargets.add(ii, components);

                //Resize the array
                p_listOfExtTargets.trimToSize();
                p_targetTypesExt.put(targetInteger, new Integer(m_targetType));
            }
        } // end of processBPorEXStatement

        private int processMOVStatement(String line, Element activityListElem)
        {
            String accuracyString = "";
            String [] spComponents, acComponents ;
            boolean isViaTarget = true;
           
            String lineMinusPlusSection = line;
            
            int indexOfPlus = line.indexOf('+');
            if (indexOfPlus >= 0)
            {
                lineMinusPlusSection = line.substring(0,indexOfPlus-1);
            }

            // ToolProfile name
            String [] components = line.split("[, \\t]+");
			String mrcVar_numStr = "0000";
			String posType = "C";
			int compStart = 1;
			for (int jj = 0; jj <= components.length; jj++)
			{
				Pattern ppos = Pattern.compile("(C|P)([0-9]+)", Pattern.CASE_INSENSITIVE);
				Matcher mpos = ppos.matcher(components[jj]);
				if (true == mpos.find())
				{
					posType = mpos.group(1);
					mrcVar_numStr = mpos.group(2);
					compStart = jj;
					break;
				}
			}
            Integer toolNumber, uframeNumber;
            Integer varInteger = new Integer(mrcVar_numStr);
            if (posType.startsWith("P"))
            {
                toolNumber = (Integer)p_targetTools.get(varInteger);
                uframeNumber = (Integer)p_targetUframes.get(varInteger);
            }
            else
            {
                toolNumber = (Integer)m_targetTools.get(varInteger);
                uframeNumber = (Integer)m_targetUframes.get(varInteger);
            }

            if (null == toolNumber || null == uframeNumber) // in case of MOVJ EC0000
                return 1;
            
            // m_currentToolNumber is set to UF#(number) when processing EIMOV[LC]
            if (false == line.startsWith("EIMOV"))
                m_currentToolNumber = toolNumber.intValue();
            
            String toolProfileName = getToolName();
            m_currentUframeNumber = uframeNumber.intValue();
            String objectProfileName = getUframeName();

            //System.out.print("PROCESS MOVE: " + line + "\n");
            Pattern pm = Pattern.compile("([SYMREIFPOVT]*[CJL]?)"); // need characters in SVSPOTMOV, EIMOV(C|J|L)
            Matcher mm = pm.matcher(lineMinusPlusSection);
           // comment out WWG stuff and add above instead Pattern pm = Pattern.compile("((S|EI)?MOV[CJL])");
           // comment out WWG Matcher mm = pm.matcher(line);
            
            String sActivityName = "RobotMotion." + m_moveCounter;
            if (true == mm.find() && mm.group(1) != null && !mm.group(1).equals(""))
            {
            	 if (mm.group(1).startsWith("SMOV") || mm.group(1).startsWith("SREFP") ||
                     mm.group(1).startsWith("EIMOV") || mm.group(1).startsWith("SVSPOTMOV"))
            	 {
            		 isViaTarget = false; // for a process target, motion activity and spot action will be combined and weld icon shown
            	 }

            	 //	System.out.print("FOUND MOVE: " + lineMinusPlusSection + "\n");
                 sActivityName = mm.group(1) + "." + m_moveCounter;
            }
            
            Element actElem = super.createMotionActivityHeader(activityListElem, sActivityName);
            // needs this if SVSPOT is proceeded by CallTask activity and followed by motion activity
            actElem.setAttribute("Operation", "Operation." + m_opMoveCounter);
            m_opMoveCounter++;
            
            if (m_robotMotionCompleted == 0) {
                m_firstRobotMotion = actElem;
            }
            m_lastRobotMotion = actElem;
            
            // MotionProfile name
            String motionProfileName0 = m_motionProfileName;
            String motionProfileName = m_motionProfileName;
            Matcher mv;
            Pattern [] pv = new Pattern [3];
            String sFlt  = "(\\d*\\.?\\d+|\\d+\\.?\\d*)"; // 2, 2., .2, 2.2
            pv[0] = Pattern.compile("V\\s*=\\s*" + sFlt, Pattern.CASE_INSENSITIVE);
            pv[1] = Pattern.compile("VJ\\s*=\\s*" + sFlt, Pattern.CASE_INSENSITIVE);
            pv[2] = Pattern.compile("VR\\s*=\\s*" + sFlt, Pattern.CASE_INSENSITIVE);
            for (int ii=0; ii<pv.length; ii++)
            {
                mv = pv[ii].matcher(lineMinusPlusSection);
	            if( true == mv.find() )
	            {
                    motionProfileName0 = mv.group();
                    motionProfileName0 = motionProfileName0.replaceAll(" ", "");

                    motionProfileName = createSpeedProfile(motionProfileName0);
                    m_motionProfileName = motionProfileName; // set current motion profile
	                
                    break; // match found & processed, break out FOR loop
	            }
            }
            
            Matcher matchVar;
            Pattern varPat = Pattern.compile("[VJR]*\\s*=\\s*[A-Z]+[0-9]*", Pattern.CASE_INSENSITIVE);
            matchVar = varPat.matcher(lineMinusPlusSection);
            if (true == matchVar.find())
            {
            	motionProfileName = matchVar.group();
            }            

            // AccuracyProfile name
            if(true == components[0].trim().endsWith("J"))
            	m_accuracyProfileName = "MotomanDefaultJnt";
            else
            	m_accuracyProfileName = "MotomanDefault";
            
            Pattern ppl = Pattern.compile("PLI?N?\\s?=\\s?[0-9]+", Pattern.CASE_INSENSITIVE);
            Matcher mpl = ppl.matcher(line);
            boolean createSPDL_attr = false;
            if (true == mpl.find())
            {
                accuracyString = mpl.group();
                acComponents = accuracyString.split("=");
                accuracyString = acComponents[1];
                accuracyString.trim();
                m_accuracyProfileName = "PL=" + accuracyString;

                boolean isInList = m_listOfAccuracyValues.contains(accuracyString);

                if (isInList == false)
                {
                    m_listOfAccuracyValues.add(accuracyString);
                    createAccuracyProfile(accuracyString, m_accuracyProfileName);
                }
            }
            else
            {
                Pattern pSpdL = Pattern.compile("SPDL\\s*=\\s*0", Pattern.CASE_INSENSITIVE);
                Matcher mSpdL = pSpdL.matcher(line);
                if (true == mSpdL.find())
                {
                    accuracyString = "0";
                    m_accuracyProfileName = "PL=0";
                    boolean isInList = m_listOfAccuracyValues.contains(accuracyString);
                    if (isInList == false)
                    {
                        m_listOfAccuracyValues.add(accuracyString);
                        createAccuracyProfile(accuracyString, m_accuracyProfileName);
                    }
                    createSPDL_attr = true; // AttributeList comes after MotionAttributes and Target
                }
            }
            
            // MotionType
            DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            if(true == components[0].trim().endsWith("J"))
            {
                if (NumCircularMoves > 0)
                    setLastCircular((Element)actElem.getPreviousSibling());
                NumCircularMoves = 0;
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            }
            else if(true == components[0].trim().endsWith("C"))
            {
                if (NumCircularMoves == 0) // the first MOVC is uploaded as MOVL
                {
                    NumCircularMoves = 1;
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
                }
                else
                if (NumCircularMoves == 1)
                {
                    // comment per WWG R20 changes NumCircularMoves = 2;
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
                }
                /*else
                {
                    eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
                }*/
                }
            else
            {
                if (NumCircularMoves > 0)
                    setLastCircular((Element)actElem.getPreviousSibling());
                NumCircularMoves = 0;
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
            }

            // DLY Start 2009/05/02 linetracking stuff
            double offset = 0.0;
            String conveyorNum = "";
            boolean trackAttributes = false;
            boolean createTaughtPosition = false;
            Pattern lpv = Pattern.compile("CV#\\(([1-9]*)\\)\\s*CTP=([-.0-9]*)", Pattern.CASE_INSENSITIVE);
			Matcher lmv = lpv.matcher(line);
			_ConveyorTaughtPosition = -1000.0;
			if (true == lmv.find())
			{
                // replace matched 'CV#(number) CTP=number' with blank,
                // so that no MotionOptions is created for this.
                String lineRemoveTrackAttr = lmv.replaceFirst("");
                lineMinusPlusSection = lineRemoveTrackAttr;
                indexOfPlus = lineRemoveTrackAttr.indexOf('+');
                if (indexOfPlus >= 0)
                {
                    lineMinusPlusSection = lineRemoveTrackAttr.substring(0,indexOfPlus-1);
                }

                conveyorNum = lmv.group(1);
				_ConveyorTaughtPosition = offset = Double.parseDouble(lmv.group(2))/1000.0;
				if (Math.abs(_ConveyorInitialPosition-_LastConveyorInitialPosition) > .00001)
				{
				    _LastConveyorInitialPosition = _ConveyorInitialPosition;
				    trackAttributes = true;
				}
				super.setTrackStatus(DNBIgpOlpUploadEnumeratedTypes.TrackType.TEACH_POS);
			    objectProfileName = "CV#(" + conveyorNum + ")";

			    // DLY 2009/11/04 - per Ali added sysStartFound so all lines prior to sysStart have MotomanDefaultTrack
		    	boolean sysStartFound = false;

			    for (int kk=m_programLineNum+1;kk<m_programLines.size();kk++)
			    {
				    String nextLine = (String)m_programLines.get(kk);
				    lmv = lpv.matcher(nextLine);
				    Matcher isMov = m_keywords[5].matcher(nextLine);
				    if (true == isMov.find() || nextLine.equalsIgnoreCase("END"))
				    {
					    if (false == lmv.find() || sysStartFound == true)
					    {
					        m_accuracyProfileName = "MotomanDefaultTrack";
					        if (m_trackProfileCreated == false)
					        {
					            createAccuracyProfile("0", "MotomanDefaultTrack");
					        }
					        m_trackProfileCreated = true;
					    }
					    break;
				    }
				    if (nextLine.trim().startsWith("SYSTART"))
				    {
				    	sysStartFound = true;
				    }
			    }
			    boolean conveyorObjectFrameExists = m_conveyorObjectFrames.contains(objectProfileName);
			    if (conveyorObjectFrameExists == false)
			    {
		            Element objFrameProfileElem;
		            Element objfpList = super.getObjectFrameProfileListElement();			    	
		            double [] uframeValues = {0.0,0.0,0.0,0.0,0.0,0.0};		            
		            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType eObjFrameTypeWorld = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
		            objFrameProfileElem = super.createObjectFrameProfile(objfpList, objectProfileName, eObjFrameTypeWorld, uframeValues);			    	
			        m_conveyorObjectFrames.add(objectProfileName);
			    }
			    m_ConveyorFound = true;
			}
			else
			{
				m_ConveyorFound = false;
			    super.setTrackStatus(DNBIgpOlpUploadEnumeratedTypes.TrackType.TRACK_OFF);
			    for (int kk=m_programLineNum+1;kk<m_programLines.size();kk++)
			    {
				    String nextLine = (String)m_programLines.get(kk);
				    lmv = lpv.matcher(nextLine);
				    Matcher isMov = m_keywords[5].matcher(nextLine);
				    if (true == isMov.find())
				    {
					    if (true == lmv.find())
					    {
					        m_accuracyProfileName = "MotomanDefaultTrack";
					        if (m_trackProfileCreated == false)
					        {
					            createAccuracyProfile("0", "MotomanDefaultTrack");
					        }
					        m_trackProfileCreated = true;
					    }
					    break;
				    }
			    }
			    for (int kk=1;kk<m_programLines.size();kk++)
			    {
				    String nextLine = (String)m_programLines.get(kk);
				    lmv = lpv.matcher(nextLine);
				    Matcher isMov = m_keywords[5].matcher(nextLine);
				    if (true == isMov.find() && true == lmv.find())
				    {
				        createTaughtPosition = true;
				        break;
				    }
			    }
			}
            // DLY End 2009/05/02
			
            Element moAttrElem = super.createMotionAttributes(actElem, motionProfileName, m_accuracyProfileName, toolProfileName, objectProfileName, eMotype);

            //Call the method to process the target
            if(0 != processTarget(line, actElem, isViaTarget))
                return 1;
            
            if (createSPDL_attr == true)
                addAttribute(actElem, "SPDL", "0");
            if (arcWeldSpeed > 0)
                addAttribute(actElem, "MotionProfile", motionProfileName0);
            
            Pattern pMoOptions = Pattern.compile("S?PD?L\\s*=\\s*[0-8]", Pattern.CASE_INSENSITIVE);
            Matcher mMoOptions = pMoOptions.matcher(lineMinusPlusSection);
            if (true == mMoOptions.find())
            {
                String sMoOptions = lineMinusPlusSection.substring(mMoOptions.end());
                if (sMoOptions.length() > 0)
                    addAttribute(actElem, "MotionOptions", sMoOptions);
            }
            else
            {
                pMoOptions = Pattern.compile("V[JR]?\\s*=\\s*" + sFlt, Pattern.CASE_INSENSITIVE);
                mMoOptions = pMoOptions.matcher(lineMinusPlusSection);
                if (true == mMoOptions.find())
                {
                    String lineSanV = lineMinusPlusSection.substring(mMoOptions.end());
                    if (lineSanV.length() > 0)
                    {
                        String sMoOptions = lineSanV;
                        // remove these from SVSPOTMOV
                        String port = "#\\s*\\(\\s*(\\d+)\\s*\\)\\s*";
                        String portORvariable = "#\\s*\\(\\s*(\\d+|\\w+)\\s*\\)\\s*";
                        String eqInt = "\\s*=\\s*(\\d+)\\s*";
                        String eqIntOrVar = "\\s*=\\s*(\\d+|\\w+)\\s*";
                        String maybeEqInt = "\\s*=?\\s*(\\d+)?\\s*";
                        String svSpotOnly = "\\s*GUN" + port + "PRESS" + portORvariable + "WTM" + eqIntOrVar + "WST" + eqInt;
                        String svSpotMov = "(PLIN)?" + maybeEqInt + "(PLOUT)?" + maybeEqInt + "CLF" + port + svSpotOnly + "(WGO)?" + maybeEqInt;
                        pMoOptions = Pattern.compile(svSpotMov, Pattern.CASE_INSENSITIVE);
                        mMoOptions = pMoOptions.matcher(lineSanV);
                        if (true == mMoOptions.find())
                            sMoOptions = " " + lineSanV.substring(mMoOptions.end());
                        if (sMoOptions.trim().length() > 0) 
                        	addAttribute(actElem, "MotionOptions", sMoOptions); 
                    }
                }
            }
            
			// DLY Start 2008/08/10
			if (line.indexOf("REFP") != -1)
			{
				Pattern rpv = Pattern.compile("REFP?\\s?([0-9]+)", Pattern.CASE_INSENSITIVE);
				Matcher rmv = rpv.matcher(line);
				if (true == rmv.find())
				{
				    String refpString = rmv.group(1);
					addAttribute(actElem, "REFP", refpString);
				}
			}
			// DLY End

			if (trackAttributes == true)
			{
				addAttribute(actElem, "ConveyorInitialPosition", String.valueOf(_ConveyorInitialPosition));
				addAttribute(actElem, "InBoundOffset", String.valueOf(_ConveyorOffset));
				addAttribute(actElem,"ConveyorNumber", conveyorNum);
			}
			
			if (Math.abs(_ConveyorTaughtPosition) < 999.0 || createTaughtPosition == true)
			{
				addAttribute(actElem, "ConveyorTaughtPosition", String.valueOf(_ConveyorTaughtPosition));
			}
			
            m_moveCounter++;
            Integer targetInteger = new Integer(mrcVar_numStr);
            int position = Integer.parseInt(mrcVar_numStr);
            int ii = compStart+1;
            if (posType.toUpperCase().startsWith("P"))
            {
            	if (components.length > ii)
            	{
	                addAttribute(actElem, "P", mrcVar_numStr);
	                if (components[ii].toUpperCase().startsWith("BP"))
	                {
	                    addAttribute(actElem, "BP", components[ii].substring(2));
	                    ii++;
	                }
	            	
	                if (components.length > ii)
	            	{
		                if (components[ii].toUpperCase().startsWith("EX"))
		                    addAttribute(actElem, "EX", components[ii].substring(2));
	            	}
	
	                // attributes for NRL
	                addAttribute(actElem, "PosType", "P");
	                addAttribute(actElem, "PosReg", mrcVar_numStr);
	
					ii = compStart + 1;
	                if (components[ii].toUpperCase().startsWith("BP"))
	                {
	                    addAttribute(actElem, "BType", "BP");
	                    ii++;
	                }
	                if (components.length > ii)
	            	{
		                if (components[ii].toUpperCase().startsWith("EX"))
	                		addAttribute(actElem, "EType", "EX");
                    }
                }
                getTargetAttr(position, "P", actElem);
            }
            else
            {
            	if (components.length > ii)
            	{
                    boolean BCvar = false;
	                if (components[ii].toUpperCase().startsWith("BC"))
	                {
	                    addAttribute(actElem, "BC", components[ii].substring(2));
                        BCvar = true;
	                    ii++;
	                }
	                if (components.length > ii)
	                {
	                	if (components[ii].toUpperCase().startsWith("EC"))
	                		addAttribute(actElem, "EC", components[ii].substring(2));
	                }
	                
	                // attributes for NRL
	                addAttribute(actElem, "PosType", "C");
	                addAttribute(actElem, "PosReg", mrcVar_numStr);

                    if (true == BCvar)
                        ii--; 
	                if (components[ii].toUpperCase().startsWith("BC"))
	                {
	                    addAttribute(actElem, "BType", "BC");
	                    ii++;
	                }
	                if (components.length > ii)
	                {
	                	if (components[ii].toUpperCase().startsWith("EC"))
	                		addAttribute(actElem, "EType", "EC");
                    }
                }
                
                getTargetAttr(position, "C", actElem);
            }
            addAttributeList(actElem, "Pre");
            // change the previous spot action's servo gun open joint value
            modifySpotOpenJoint(actElem);
            
            m_robotMotionCompleted = 1;            
            return 0;
        }//processMOVStatement

        /* This method is called when processing linear and joint moves and the END statement.
         * It looks for the last CircularVia move and changes it to Circular.
         */
        private void setLastCircular(Element actElem)
        {
            Element prevElem = actElem;
            if (null == prevElem)
                return;
            while (false == prevElem.getTagName().equals("Activity") || 
                   false == prevElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity"))
            {
                prevElem = (Element)prevElem.getPreviousSibling();
                if (null == prevElem)
                    return;
            }
            Element moAttr = (Element)prevElem.getElementsByTagName("MotionAttributes").item(0);
            if (null == moAttr)
                return;
            Element moType = (Element)moAttr.getElementsByTagName("MotionType").item(0);
            if (null == moType)
                return;
            String s = moType.getFirstChild().getNodeValue();
            if (s.equals("CircularVia"))
                moType.getFirstChild().setNodeValue("Circular");
        }        
        
        private void processTimerStatement(String line, Element activityListElem) {

            String [] components = line.split("=");

            Double dStartTime = new Double(0.0);
            Double dEndTime = Double.valueOf(components[1].trim());
            String sActivityName = "RobotDelay." + m_delayCounter;

            Element actElem = super.createDelayActivity(activityListElem, sActivityName, dStartTime, dEndTime);

            m_delayCounter++;
            
            addAttributeList(actElem, "Pre");
        } // end of processTimerStatement

        private void preProcessComment(String line, Element activityListElem, String attributeName)
        {
            if (uploadStyle.equalsIgnoreCase("OperationComments") || uploadStyle.equalsIgnoreCase("Honda"))
            {
                processComment(line, activityListElem, attributeName);
            }
            else
            {
                commentLines.add(commentCount, attributeName + ":" + line);
                commentCount++;
            }                
        }
        
        private void processComment(String line, Element activityListElem, String attributeName)
        {
            String comment = line.substring(line.indexOf('\'') + 1);
            if (uploadStyle.equalsIgnoreCase("OperationComments") || uploadStyle.equalsIgnoreCase("Honda"))
            {
                //Create DOM Nodes and set appropriate attributes
                Element actElem = m_xmlDoc.createElement("Activity");
                activityListElem.appendChild (actElem);

                actElem.setAttribute ("ActivityType", "Operation");

                Element activityName = m_xmlDoc.createElement("ActivityName");
                actElem.appendChild(activityName);
                String S_actName = "Comment.1";
                if (attributeName.equals("Comment"))
                    S_actName = attributeName + '.' + m_commentCounter++;
                else
                if (attributeName.equals(rbtLang))
                    S_actName = attributeName + '.' + m_rbtLangCounter++;

                Text activityNameTxt = m_xmlDoc.createTextNode(S_actName);
                activityName.appendChild(activityNameTxt);

                String [] attrNames = {attributeName};
                String [] attrValues = {comment};
                Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
            }
            else
            {
                commentLines.add(commentCount, comment);
                commentCount++;
            }
        } // end of processComment

        private void addAttributeList(Element actElem, String prefix)
        {
            if (commentCount == 0)
                return;

            String [] attrNames = new String[commentCount];
            String [] attrValues = new String[commentCount];
            for (int ii=0; ii<commentCount; ii++)
            {
                attrNames[ii] = prefix + "Comment" + String.valueOf(ii+1);
                attrValues[ii] = (String)commentLines.get(ii);
            }

            Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
            commentCount = 0;
        } // end addAttributeList

        private void addAttribute(Element actElem, String attrName, String attrVal)
        {
            String [] attrNames = {attrName};
            String [] attrValues = {attrVal};
            Element attributeListElem = super.createAttributeList(actElem, attrNames, attrValues);
        } // end addAttribute

        private void processTagStatement(String line)
        {
            //Parse the string into array
            String [] components = line.split("[ \\t]+");
            //Eliminate the starting letters 'BC' in the target name
            m_currentTagPoint = components[1];
        }

        private int processTarget(String line, Element actElem, boolean isViaTarget)
        {
            //Parse the string into array
            String [] components = line.split("[, \\t]+");

			String targetString = "0000";
			String posType = "C";
			for (int jj = 0; jj <= components.length; jj++)
			{
				Pattern ppos = Pattern.compile("(C|P)([0-9]+)", Pattern.CASE_INSENSITIVE);
				Matcher mpos = ppos.matcher(line);
				if (true == mpos.find())
				{
					posType = mpos.group(1);
					targetString = mpos.group(2);
					break;
				}
			}
            Integer targetInteger = new Integer(targetString);
            //Get the C or P position number
            int position = Integer.parseInt(targetString);
            //Get the target type
            //Get the C or P position target values
            Integer targetType;
            String [] targetValues;
            // C var type: Joint(Pulse) or Cartesian(Rectan)
			if (posType.startsWith("P") || posType.startsWith("p"))
            {
                targetType = (Integer) p_targetTypes.get(targetInteger);
                if (p_listOfRobotTargets.size() < position + 1 ||
                    p_listOfRobotTargets.get(position).toString().equals("Target Undefined"))
                {
                    try {
                        bw.write("ERROR: Target not defined or target definition not supported: " + components[1] + "\n");
                    }
                    catch (IOException bwe) {
                    }
                    return 1;
                }
                targetValues = (String []) p_listOfRobotTargets.get(position);
            }
            else
            {
                targetType = (Integer) m_targetTypes.get(targetInteger);
                if (m_listOfRobotTargets.size() < position + 1 ||
                    m_listOfRobotTargets.get(position).toString().equals("Target Undefined"))
                {
                    try {
                        bw.write("ERROR: Target not defined or target definition not supported: " + components[1] + "\n");
                    }
                    catch (IOException bwe) {
                    }
                    return 1;
                }
                targetValues = (String []) m_listOfRobotTargets.get(position);
            }
            
            int ii, jj;
            
            if (true == targetType.equals(new Integer(JOINT)))
            {
                ArrayList [] oaJointTarget = new ArrayList[m_numDOF];
                for (ii=0; ii<m_numDOF; ii++)
                    oaJointTarget[ii] = new ArrayList(1);

                for(ii = 1; ii < targetValues.length; ii++)
                    formatJointValueInPulses(targetValues[ii] , "Robot", ii, oaJointTarget[ii-1]);

                int targetNum, extAxisDOF;
                if (m_numAuxAxes > 0)
                {
                    for (ii = 1; ii < components.length; ii++)
                    {
                        targetValues = null;
                        if (components[ii].length() > 1 && components[ii].substring(0, 2).equals("BC"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])m_listOfAuxTargets.get(targetNum);
                        }
                        else
                        if (components[ii].length() > 1 && components[ii].substring(0, 2).equals("BP"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])p_listOfAuxTargets.get(targetNum);
                        }

                        if (targetValues == null)
                            continue;

                        for (jj = 1; jj < targetValues.length; jj++)
                        {
                            extAxisDOF = m_bcAxisMapping[jj-1];
                            if (extAxisDOF > m_numRobotAxes && extAxisDOF <= m_numDOF)
                                formatJointValueInPulses(targetValues[jj], "RailTrackGantry", extAxisDOF, oaJointTarget[extAxisDOF-1]);
                        }
                    }
                }

                if (m_numExtAxes + m_numWorkAxes > 0)
                {
                    String auxType;
                    for (ii = 1; ii < components.length; ii++)
                    {
                        targetValues = null;
                        if (components[ii].length() > 1 && components[ii].substring(0, 2).equals("EC"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])m_listOfExtTargets.get(targetNum);
                        }
                        else
                        if (components[ii].length() > 1 && components[ii].substring(0, 2).equals("EX"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])p_listOfExtTargets.get(targetNum);
                        }

                        if (targetValues == null)
                            continue;

                        for (jj = 1; jj < targetValues.length; jj++)
                        {
                            extAxisDOF = m_ecAxisMapping[jj-1];
                            if (extAxisDOF > m_numRobotAxes && extAxisDOF <= m_numDOF)
                            {
                                if (EndOfArmToolingFirst == true)
                                {
                                    if (extAxisDOF <= m_numRobotAxes + m_numAuxAxes + m_numExtAxes)
                                        auxType = "EndOfArmTooling";
                                    else
                                        auxType = "WorkpiecePositioner";
                                }
                                else
                                {
                                    if (extAxisDOF <= m_numRobotAxes + m_numAuxAxes + m_numWorkAxes)
                                        auxType = "WorkpiecePositioner";
                                    else
                                        auxType = "EndOfArmTooling";
                                }

                                formatJointValueInPulses(targetValues[jj], auxType, extAxisDOF, oaJointTarget[extAxisDOF-1]);
                            }
                        }
                    }
                }

                Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, isViaTarget);
                // DLY Start 2009/05/03 must create a tag so linetracking upload can work
                //                      values and config are meaningless here...the tag name is all that is used along
                //                      with the joint target below
                if (m_ConveyorFound == true || uploadCreateTags == true)
                {
                	double [] cartValues = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
                	String cfgName = "Config_1";
                	Element cartTargetElem = super.createCartesianTarget(targetElem, cartValues, cfgName);
                	super.createTag(cartTargetElem, posType + targetString);
                }
                // DLY End 2009/05/03
                Element jointTargetElem = super.createJointTarget(targetElem, oaJointTarget);
            }
            else
            if (true == targetType.equals(new Integer(CARTESIAN)))
            {
                Element targetElem = super.createTarget(actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, isViaTarget);
                double [] cartValues = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
                for (ii=1; ii<4; ii++) // element 0 is Cvar number
                    cartValues[ii-1] = Double.parseDouble(targetValues[ii])*0.001;
                for (ii=4; ii<7; ii++)
                    cartValues[ii-1] = Double.parseDouble(targetValues[ii]);
                String [] cfgTurns;
                String tagName = null;
				if (posType.startsWith("P") || posType.startsWith("p"))
                {
                    cfgTurns = (String [])p_cfgTurns.get(position);
                    tagName = "P" + targetValues[0];
                }
                else
                {
                    cfgTurns = (String [])m_cfgTurns.get(position);
                    tagName = "C" + targetValues[0];
                }
                String cfgName = "Config_" + cfgTurns[0];
                Element cartTargetElem = super.createCartesianTarget(targetElem, cartValues, cfgName);
                
                int [] turnNumbers = {0, 0, 0, 0};
                turnNumbers[1] = Integer.parseInt(cfgTurns[1]);
                turnNumbers[3] = Integer.parseInt(cfgTurns[2]);
                turnNumbers[0] = Integer.parseInt(cfgTurns[3]);
                turnNumbers[2] = 0; // turn number 5
                if (m_controller.equals("DX100") == false)
                    addAttribute(actElem, "RCONF L", cfgTurns[4]); // RCONF number L to determin config
                super.createTurnNumbers(cartTargetElem, turnNumbers);
                super.createTag(cartTargetElem, tagName);
                
                if (m_numAuxAxes == 0 && (m_numExtAxes + m_numWorkAxes == 0))
                    return 0;
                
                Element jointTargetElem = m_xmlDoc.createElement("JointTarget");
                targetElem.appendChild(jointTargetElem);
                
                int targetNum, extAxisDOF;
                if (m_numAuxAxes > 0)
                {
                    for (ii = 1; ii < components.length; ii++)
                    {
                        targetValues = null;
                        if (components[ii].length() > 1 && components[ii].substring(0, 2).equalsIgnoreCase("BC"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])m_listOfAuxTargets.get(targetNum);
                            targetType = (Integer) m_targetTypesAux.get(targetInteger);
                        }
                        else
                        if (components[ii].length() > 1 && components[ii].substring(0,2).equalsIgnoreCase("BP"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])p_listOfAuxTargets.get(targetNum);
                            targetType = (Integer) p_targetTypesAux.get(targetInteger);
                        }
                        
                        if (targetValues == null)
                            continue;
                        
                        for (jj = 1; jj < targetValues.length; jj++)
                        {
                            extAxisDOF = m_bcAxisMapping[jj-1];
                            if (extAxisDOF > m_numRobotAxes && extAxisDOF <= m_numDOF)
                                createAuxJointElem(jointTargetElem, extAxisDOF, "RailTrackGantry", targetValues[jj], targetType);
                        }
                    }
                }
                if (m_numExtAxes + m_numWorkAxes > 0)
                {
                    String auxType;
                    for (ii = 1; ii < components.length; ii++)
                    {
                        targetValues = null;
                        if (components[ii].length() > 1 && components[ii].substring(0, 2).equals("EC"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])m_listOfExtTargets.get(targetNum);
                            targetType = (Integer) m_targetTypesExt.get(targetInteger);

                        }
                        else
                        if (components[ii].length() > 1 && components[ii].substring(0, 2).equals("EX"))
                        {
                            targetNum = Integer.parseInt(components[ii].substring(2));
                            targetValues = (String [])p_listOfExtTargets.get(targetNum);
                            targetType = (Integer) p_targetTypesExt.get(targetInteger);
                        }

                        if (targetValues == null)
                            continue;

                        for (jj = 1; jj < targetValues.length; jj++)
                        {
                            extAxisDOF = m_ecAxisMapping[jj-1];
                            if (extAxisDOF > m_numRobotAxes && extAxisDOF <= m_numDOF)
                            {
                                if (EndOfArmToolingFirst == true)
                                {
                                    if (extAxisDOF <= m_numRobotAxes + m_numAuxAxes + m_numExtAxes)
                                        auxType = "EndOfArmTooling";
                                    else
                                        auxType = "WorkpiecePositioner";
                                }
                                else
                                {
                                    if (extAxisDOF <= m_numRobotAxes + m_numAuxAxes + m_numWorkAxes)
                                        auxType = "WorkpiecePositioner";
                                    else
                                        auxType = "EndOfArmTooling";
                                }

                                createAuxJointElem(jointTargetElem, extAxisDOF, auxType, targetValues[jj], targetType);
                                setSVJoint(targetValues[jj], auxType, extAxisDOF);
                            }
                        }
                    }
                }
            }
            
            return 0;
        }//end processTarget method
        
        private void createAuxJointElem(Element pElem, int auxNum, String auxType, String auxVal, Integer tgtType)
        {
            Element auxJointElem, jValElem;
            Text jVal;
            double auxJointVal = Double.valueOf(auxVal).doubleValue();

            auxJointElem = m_xmlDoc.createElement("AuxJoint");
            pElem.appendChild(auxJointElem);

            jValElem = m_xmlDoc.createElement("JointValue");
            auxJointElem.appendChild(jValElem);

            auxJointElem.setAttribute("Type", auxType);
            auxJointElem.setAttribute("DOFNumber", String.valueOf(auxNum));
            auxJointElem.setAttribute("JointName", "Joint " + String.valueOf(auxNum));
            int ii = auxNum - 1;
            auxJointElem.setAttribute("JointType", m_axisTypes[ii]);
            if (tgtType.equals(new Integer(JOINT)))
            {
                auxJointVal = (auxJointVal - m_zeroOffset[ii])/m_resolution[ii];
                if (m_axisTypes[ii].equals("Rotational"))
                    auxJointVal *= 360.0;
            }
            if (m_axisTypes[ii].equals("Rotational"))
            {
                auxJointElem.setAttribute("Units", "deg");
            }
            else
            {
                auxJointVal /= 1000.0;
                auxJointElem.setAttribute("Units", "m");
            }
            jVal = m_xmlDoc.createTextNode(String.valueOf(auxJointVal));
            jValElem.appendChild(jVal);
        } //end createAuxJointElem
	
	private void processTSYNCStatement(Matcher match, Element activityListElem)
	{
		String portNumber = match.group(1);
		String tsyncName = "TSYNC " + match.group(1) + " SNUM=" + match.group(2);

		Element actElem = super.createCallTaskActivity(activityListElem, tsyncName, tsyncName);

		addAttributeList(actElem, "Pre");

		Element tsyncActivityListElem = super.createActivityList(tsyncName);

		Element resourceElem = super.getResourceElement();
		resourceElem.appendChild(tsyncActivityListElem);

		String tsyncPortNumber = String.valueOf(Integer.parseInt(portNumber) + 100);
		String tsyncSignalName = "TSYNC" + tsyncPortNumber;

		Integer iPortNumber = Integer.valueOf(tsyncPortNumber);

		actElem = super.createSetIOActivity(tsyncActivityListElem, tsyncName + " SET ON", true, tsyncSignalName, iPortNumber, Double.valueOf("0.0"));

		actElem = super.createWaitForIOActivity(tsyncActivityListElem, tsyncName + "WAIT ON", true, tsyncSignalName, iPortNumber, Double.valueOf("0.0"));

		actElem = super.createSetIOActivity(tsyncActivityListElem, tsyncName + " SET OFF", false, tsyncSignalName, iPortNumber, Double.valueOf("0.0"));

		actElem = super.createWaitForIOActivity(tsyncActivityListElem, tsyncName + "WAIT OFF", false, tsyncSignalName, iPortNumber, Double.valueOf("0.0"));

	}
    
	private void processDOUTStatement(String line, Matcher match, Element activityListElem, String sTime) throws IllegalStateException
        {
            String portNumber = match.group(1);
            String status = match.group(2);

            boolean bSignalValue = false;
            if (status.equalsIgnoreCase("ON"))
                bSignalValue = true;

            Integer iPortNumber = Integer.valueOf(portNumber);
            Double dTime = Double.valueOf(sTime);

            String sSignalName = ioName + portNumber;
            if (ioName.endsWith("["))
                sSignalName += "]";
            
            Element actElem = super.createSetIOActivity(activityListElem, line, bSignalValue, sSignalName, iPortNumber, dTime);

            addAttributeList(actElem, "Pre");
        } // end of processDOUTStatement
        
        private void processPULSEStatement(String line, Matcher match, Element activityListElem) throws IllegalStateException{
            String portNumber = match.group(1);
            String [] components = line.split("=");

            String timeInSec = "0.3";
            if (components.length == 2)
                timeInSec = components[1];

            String doutOn = "DOUT OT#(" + portNumber + ") ON";

            Matcher matchDoutOn = this.m_keywords[9].matcher(doutOn);

            if(matchDoutOn.find() == true)
                processDOUTStatement(line, matchDoutOn, activityListElem, timeInSec);
        } // end of processPULSEStatement

       private void processWAITStatement(String line, Matcher match, Element activityListElem)
       {
            String portNumber = match.group(1);
            String status = match.group(2);
            String hasTime = match.group(3);

            String sTime = "0";
            if (hasTime != null)
                sTime = hasTime.split("=")[1].trim();
            Double dTime = Double.valueOf(sTime);

            Integer iPortNumber = Integer.valueOf(portNumber);

            boolean bSignalValue = false;
            if (status.equalsIgnoreCase("ON"))
                bSignalValue = true;
            if (m_commentWaitSignal == false)
            {
                Element actElem = super.createWaitForIOActivity(activityListElem, line, bSignalValue, portNumber, iPortNumber, dTime);

                addAttributeList(actElem, "Pre");
            }
            else
            {
                preProcessComment(line, activityListElem, rbtLang);
            }
        } // end of processWAITStatement

        private void processCallJobStatement(String line, Element activityListElem)
        {
            String callNameStr = line.substring(line.indexOf(':') + 1);
            callNameStr = callNameStr.trim();
            if (callNameStr.length() == 0)
            {
                preProcessComment(line, activityListElem, rbtLang);
                return;
            }
            
            String callTaskCondition = null;
            int conditionIndex = callNameStr.indexOf(' ');
            if (conditionIndex != -1)
            {
                callTaskCondition = callNameStr.substring(conditionIndex+1);
                callNameStr = callNameStr.substring(0, conditionIndex);
            }
            
            String progName = callNameStr + ".jbi";
            String activityName = "RobotCall." + robotCallNumber++;
            Element actElem = super.createCallTaskActivity(activityListElem, activityName, callNameStr);

            int index = programCallNames.indexOf(progName);
            if (index < 0)
            {
                programCallNames.add(programCallCount, progName);
                programCallCount++;
            }

            if (callTaskCondition != null)
            {
                commentLines.add(commentCount, "Call Task Condition:" + callTaskCondition);
                commentCount++;
            }

            addAttributeList(actElem, "Pre");
        } // end of processCallJobStatement

        private int setLastTargetAttributeViaPoint(Element activityListElem, String sViaBool)
        {
            /* If the last element is motion activity, set its target attribute ViaPoint to false
             */
            Element actElem = (Element)activityListElem.getLastChild();
            if (true == actElem.getTagName().equals("Activity") && 
                true == actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity"))
            {
                Element targetElem = (Element)actElem.getElementsByTagName("Target").item(0);
                targetElem.setAttribute("ViaPoint", sViaBool);
                return 0;
            }
            return 1;
        }
        // SVSPOT GUN#(1)
        private void processSpotWeldStatement(String line, Matcher match, Element activityListElem)
        {
            int returnCode = setLastTargetAttributeViaPoint(activityListElem, "false");
            
            String [] attributeList = {"GUN#", "PRESS#", "WTM", "WST", "PRESS_V#", "WTM_V"};
            String [] attributeValues = {"0", "-1", "-1", "0", "", ""};

            for (int ii=0; ii<4; ii++)
            {
                attributeValues[ii] = match.group(ii+1);
                if (1==ii || 2==ii)
                {
                    try {
                        int tmpint = Integer.parseInt(attributeValues[ii]);
                    }
                    catch (NumberFormatException e) {
                        attributeValues[ii+3] = attributeValues[ii];
                        attributeValues[ii] = "-1";
                    }
                }
            }

            int gunNum = Integer.parseInt(attributeValues[0]);
            if (mountedTools.size() > gunNum && mountedTools.get(gunNum) != null)
                m_mountedTool = (String)mountedTools.get(gunNum);

            String actionName = "MRCSpotWeld." + m_spotWeldCounter;
            Element actionElem = super.createActionHeader(activityListElem, actionName, "MRCSpotWeld", m_mountedTool);
            // if motion and SVSPOT is separated(by CallTask activity), set Operation attribute
            // otherwise the action is not uploaded when followed by a motion.
            if (returnCode != 0)
                actionElem.setAttribute("Operation", actionName);
            
            Element attribListElem = super.createAttributeList(actionElem, attributeList, attributeValues);

            if (m_servoUpload == 1) {                   
	              // SERVO GUN RRS ATTRIBUTES ADDED FOR R13 SP2
	              // R20SP4 create these always...motion planner will avoid simulating both RRS and action
	            if (m_servoAttribsAdded == 0 && m_firstRobotMotion != null) {
	                if (m_lastRobotMotion == m_firstRobotMotion) {
	                    setRRSServoAttribs( m_firstRobotMotion, attributeValues[1]);
	                }   
	                else {
	                    createRRSServoAttribs(m_firstRobotMotion);
	                }                
	            }
	            if (m_lastRobotMotion != null && m_lastRobotMotion != m_firstRobotMotion) {
	                setRRSServoAttribs(m_lastRobotMotion, attributeValues[1]);
	            }                	
            } // servo upload           

            addAttributeList(actionElem, "Pre");

            Element actionActivityListElem = super.createActivityListWithinAction(actionElem);

            //Close gun
            String actName = "Basic Motion Activity." + m_moveCounter++;
            Double startTime = new Double(0.0);
            Double endTime = new Double(1.0);
            String closeJ = weldHome;
            if (Double.isNaN(dCloseJ)==false)
            {
                closeJ = String.valueOf(dCloseJ);
                dCloseJ = Double.NaN;
            }

            // Get gun joint type
            int offset = 0;
            if (EndOfArmToolingFirst == true)
                offset = 0;
            else
                offset = m_numWorkAxes;

            int idx = m_numRobotAxes + gunNum - 1 + offset;
            while (idx > m_axisTypes.length - 1 && idx > m_numRobotAxes)
                idx--;
            String gunJointType = m_axisTypes[idx];

            kku.createActionMotionActivity(actionActivityListElem, actName, startTime, endTime, closeJ, gunJointType, "PROMPT", "DNBRobotMotionActivity", m_xmlDoc);
            //Weld for some time (default 1.5s)
            processTimerStatement("TIMER T=1.5", actionActivityListElem);
            //Open gun
            actName = "Basic Motion Activity." + m_moveCounter++;
            /* create a gun open activity with "home name", after processing the next move statement
             * use the aux joint value to change the servo gun open joint value
             */
            kku.createActionMotionActivity(actionActivityListElem, actName, startTime, endTime, closeHome, gunJointType, "PROMPT", "DNBRobotMotionActivity", m_xmlDoc);
            
            m_spotWeldCounter++;
        } // end processSpotWeldStatement

        private void processSpotWeldMovStatement(String line, Matcher match, Element activityListElem)
        {
            setLastTargetAttributeViaPoint(activityListElem, "false");
            
            String [] attributeList = {"PLIN", "PLOUT", "CLF#", "GUN#", "PRESS#", "WTM", "WST", "WGO"};
            String [] attributeValues = {"", "", "", "0", "0", "0", "0", ""};

            if (match.group(1) != null && match.group(2) != null)
            {
                attributeValues[0] = match.group(2);
            }
            if (match.group(3) != null && match.group(4) != null)
            {
                attributeValues[1] = match.group(4);
            }
            for (int ii=2; ii<7; ii++)
            {
                attributeValues[ii] = match.group(ii+3);
            }
            if (match.group(10) != null && match.group(11) != null)
            {
                attributeValues[7] = match.group(11);
            }            

            int gunNum = Integer.parseInt(attributeValues[3]);
            if (mountedTools.size() > gunNum && mountedTools.get(gunNum) != null)
                m_mountedTool = (String)mountedTools.get(gunNum);

            String actionName = "MRCSpotWeld." + m_spotWeldCounter;
            Element actionElem = super.createActionHeader(activityListElem, actionName, "MRCSpotWeld", m_mountedTool);
            Element attribListElem = super.createAttributeList(actionElem, attributeList, attributeValues);

            if (m_servoUpload == 1) {                   
	              // SERVO GUN RRS ATTRIBUTES ADDED FOR R13 SP2
	              // R20SP4 create these always...motion planner will avoid simulating both RRS and action
	            if (m_servoAttribsAdded == 0 && m_firstRobotMotion != null) {
	                if (m_lastRobotMotion == m_firstRobotMotion) {
	                    setRRSServoAttribs( m_firstRobotMotion, attributeValues[4]);
	                }   
	                else {
	                    createRRSServoAttribs(m_firstRobotMotion);
	                }                
	            }
	            if (m_lastRobotMotion != null && m_lastRobotMotion != m_firstRobotMotion) {
	                setRRSServoAttribs(m_lastRobotMotion, attributeValues[4]);
	            }                	
	        } // servo upload       
            
            addAttributeList(actionElem, "Pre");

            Element actionActivityListElem = super.createActivityListWithinAction(actionElem);

            //Close gun
            String actName = "Basic Motion Activity." + m_moveCounter++;
            Double startTime = new Double(0.0);
            Double endTime = new Double(1.0);
            String closeJ = weldHome;
            if (Double.isNaN(dCloseJ)==false)
            {
                closeJ = String.valueOf(dCloseJ);
                dCloseJ = Double.NaN;
            }

            // Get gun joint type
            int offset = 0;
            if (EndOfArmToolingFirst == true)
                offset = 0;
            else
                offset = m_numWorkAxes;

            int idx = m_numRobotAxes + gunNum - 1 + offset;
            while (idx > m_axisTypes.length - 1 && idx > m_numRobotAxes)
                idx--;
            String gunJointType = m_axisTypes[idx];

            kku.createActionMotionActivity(actionActivityListElem, actName, startTime, endTime, closeJ, gunJointType, "PROMPT", "DNBRobotMotionActivity", m_xmlDoc);
            //Weld for some time (default 1.5s)
            processTimerStatement("TIMER T=1.5", actionActivityListElem);
            //Open gun
            actName = "Basic Motion Activity." + m_moveCounter++;
            /* create a gun open activity with "home name", after processing the next move statement
             * use the aux joint value to change the servo gun open joint value
             */
            kku.createActionMotionActivity(actionActivityListElem, actName, startTime, endTime, closeHome, gunJointType, "PROMPT", "DNBRobotMotionActivity", m_xmlDoc);
            
            m_spotWeldCounter++;
        } // end processSpotWeldMovStatement
        
        private void processSpotDryStatement(String line, Matcher match, Element activityListElem)
        {
            setLastTargetAttributeViaPoint(activityListElem, "false");
            
            String [] attributeList = {"GUN#", "PRESSCL#", "TWC"};
            String [] attributeValues = {"0", "0", ""};

            for (int ii=0; ii<2; ii++)
            {
                attributeValues[ii] = match.group(ii+1);
            }
            int twcIndex = line.indexOf("TWC-");
            if ( twcIndex != -1 )
                attributeValues[2] = line.substring(twcIndex+4, twcIndex+5);

            int gunNum = Integer.parseInt(attributeValues[0]);
            if (mountedTools.size() > gunNum && mountedTools.get(gunNum) != null)
                m_mountedTool = (String)mountedTools.get(gunNum);

            String actionName = "MRCSpotDry." + m_spotDryCounter;
            Element actionElem = super.createActionHeader(activityListElem, actionName, "MRCSpotDry", m_mountedTool);
            Element attribListElem = super.createAttributeList(actionElem, attributeList, attributeValues);

            if (m_servoUpload == 1) {                   
	              // SERVO GUN RRS ATTRIBUTES ADDED FOR R13 SP2
	              // R20SP4 create these always...motion planner will avoid simulating both RRS and action
	            if (m_servoAttribsAdded == 0 && m_firstRobotMotion != null) {
	                if (m_lastRobotMotion == m_firstRobotMotion) {
	                    setRRSServoAttribs( m_firstRobotMotion, attributeValues[1]);
	                }   
	                else {
	                    createRRSServoAttribs(m_firstRobotMotion);
	                }                
	            }
	            if (m_lastRobotMotion != null && m_lastRobotMotion != m_firstRobotMotion) {
	                setRRSServoAttribs(m_lastRobotMotion, attributeValues[1]);
	            }                	
            } // servo upload           
            
            addAttributeList(actionElem, "Pre");
            
            Element actionActivityListElem = super.createActivityListWithinAction(actionElem);
                
            //Close gun
            String actName = "Basic Motion Activity." + m_moveCounter++;
            Double startTime = new Double(0.0);
            Double endTime = new Double(1.0);
            String closeJ = weldHome;
            if (Double.isNaN(dCloseJ)==false)
            {
                closeJ = String.valueOf(dCloseJ);
                dCloseJ = Double.NaN;
            }

            // Get gun joint type
            int offset = 0;
            if (EndOfArmToolingFirst == true)
                offset = 0;
            else
                offset = m_numWorkAxes;

            int idx = m_numRobotAxes + gunNum - 1 + offset;
            while (idx > m_axisTypes.length - 1 && idx > m_numRobotAxes)
                idx--;
            String gunJointType = m_axisTypes[idx];

            kku.createActionMotionActivity(actionActivityListElem, actName, startTime, endTime, closeJ, gunJointType, "PROMPT", "DNBRobotMotionActivity", m_xmlDoc);
            //Weld for some time (default 1.5s)
            processTimerStatement("TIMER T=1.5", actionActivityListElem);
            //Open gun
            actName = "Basic Motion Activity." + m_moveCounter++;
            /* create a gun open activity with "home name", after processing the next move statement
             * use the aux joint value to change the servo gun open joint value
             */
            kku.createActionMotionActivity(actionActivityListElem, actName, startTime, endTime, closeHome, gunJointType, "PROMPT", "DNBRobotMotionActivity", m_xmlDoc);
            
            m_spotDryCounter++;
        } // end processSpotDryStatement

        /* This method is called from processMOVStatement to check if the previous element is
         * a SPOT action. If it is, use the aux joint value from the move statement to change
         * the spot action's gun open joint values(servo gun).
         */
        private void modifySpotOpenJoint(Element actElem)
        {
            if (Double.isNaN(dCloseJ)==true)
                return;
            Element prevElem = (Element)actElem.getPreviousSibling();
            if (prevElem == null || prevElem.getTagName().equals("Action") == false)
                return;
            if (prevElem.getAttribute("ActionType").equals("MRCSpotWeld") == false &&
                prevElem.getAttribute("ActionType").equals("MRCSpotDry")  == false)
                return;
            Element openJElem = (Element)prevElem.getElementsByTagName("ActivityList").item(0).getLastChild();
            Element jTgt = (Element)openJElem.getElementsByTagName("JointTarget").item(0);
            Node HomeName = jTgt.getFirstChild();
            jTgt.removeChild(HomeName);
            Element j = m_xmlDoc.createElement("Joint");
            jTgt.appendChild(j);
            j.setAttribute("DOFNumber", "1");
            j.setAttribute("JointName", "Joint 1");

            // get gun joint type
            int offset = 0;
            if (EndOfArmToolingFirst == true)
                offset = 0;
            else
                offset = m_numWorkAxes;

            // if parameters Tool.* are not set, gunNum = 0
            int gunNum = mountedTools.lastIndexOf(m_mountedTool);
            if (gunNum > 0)
                gunNum--;
            String gunJointType = m_axisTypes[m_numRobotAxes + offset + gunNum];

            j.setAttribute("JointType", gunJointType);
            Text jvt;
            if (gunJointType.equals("Translational"))
            {
                j.setAttribute("Units", "m");
                jvt = m_xmlDoc.createTextNode(String.valueOf(dCloseJ/1000.0));
            }
            else
            {
                j.setAttribute("Units", "deg");
                jvt = m_xmlDoc.createTextNode(String.valueOf(dCloseJ));
            }
            Element jv = m_xmlDoc.createElement("JointValue");
            j.appendChild(jv);
            jv.appendChild(jvt);
        } // end of modifySpotOpenJoint

        private void processSpotToolPickStatement(String line, Matcher match, Element activityListElem)
        {
            setLastTargetAttributeViaPoint(activityListElem, "false");
            
            String [] attributeName = {"GUN#"};
            String [] attributeValue = {"0"};

            attributeValue[0] = match.group(1);

            int gunNum = Integer.parseInt(attributeValue[0]);
            if (mountedTools.size() > gunNum && mountedTools.get(gunNum) != null)
                m_mountedTool = (String)mountedTools.get(gunNum);

            String actionName = "MRCSpotToolPick." + m_spotPickCounter;
            Element actionElem = super.createActionHeader(activityListElem, actionName, "MRCSpotToolPick", m_mountedTool);
            Element attribListElem = super.createAttributeList(actionElem, attributeName, attributeValue);

            addAttributeList(actionElem, "Pre");

            Element actionActivityListElem = super.createActivityListWithinAction(actionElem);

            String activityName = "DNBIgpMountActivity." + m_spotPickCounter;
            String toolProfile = getToolName();
            Element actionActivityElem = super.createMountActivity(actionActivityListElem, activityName, m_mountedTool, toolProfile);

            attributeName[0] = "PROMPT";
            attributeValue[0] = "DNBIgpMountActivity";
            attribListElem = super.createAttributeList(actionActivityElem, attributeName, attributeValue);

            m_spotPickCounter++;
        } // end processSpotToolPickStatement

        private void processSpotToolPlaceStatement(String line, Matcher match, Element activityListElem)
        {
            setLastTargetAttributeViaPoint(activityListElem, "false");
            
            String [] attributeName = {"GUN#"};
            String [] attributeValue = {"0"};

            attributeValue[0] = match.group(1);

            int gunNum = Integer.parseInt(attributeValue[0]);
            if (mountedTools.size() > gunNum && mountedTools.get(gunNum) != null)
                m_mountedTool = (String)mountedTools.get(gunNum);

            String actionName = "MRCSpotToolPlace." + m_spotPlaceCounter;
            Element actionElem = super.createActionHeader(activityListElem, actionName, "MRCSpotToolPlace", m_mountedTool);
            Element attribListElem = super.createAttributeList(actionElem, attributeName, attributeValue);

            addAttributeList(actionElem, "Pre");

            Element actionActivityListElem = super.createActivityListWithinAction(actionElem);

            String activityName = "DNBIgpUnMountActivity." + m_spotPlaceCounter;
            String toolProfile = getToolName();
            Element actionActivityElem = super.createUnMountActivity(actionActivityListElem, activityName, m_mountedTool, toolProfile);

            attributeName[0] = "PROMPT";
            attributeValue[0] = "DNBIgpUnMountActivity";
            attribListElem = super.createAttributeList(actionActivityElem, attributeName, attributeValue);

            m_spotPlaceCounter++;
        } // end processSpotToolPlaceStatement

        private void processArcOn(String line, Element actList)
        {
            Element act = (Element)actList.getLastChild();
            Element moAttr = kku.findChildElemByName(act, "MotionAttributes", null, null);
            if (null == moAttr)
            {
                // activity is not motion, cannot set UserProfile ARCStart
                preProcessComment(line, actList, rbtLang);
                return;
            }
            /* <UserProfile Type="ARCStart"> cannot be empty
             * therefore, only create after motion activity is found
             */
            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", "ARCStart");
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", "ARCStart");
            }

            Element actName = kku.findChildElemByName(act, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue("ArcOn." + (m_moveCounter-1));
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", "ARCStart");

            Matcher m;
            Pattern [] p;
            p = new Pattern [2];
            String flt =   "(\\d+(?:\\.(?:\\d+)?)?)\\s+";
            String fltns = "(\\d+(?:\\.(?:\\d+)?)?)";
            String weldingSpeed = "(?:V=" + fltns + ")?";
            p[0] = Pattern.compile("^ARCON\\s+AC="+flt+"AVP="+flt+"T="+flt + weldingSpeed + "(\\s+RETRY)?", Pattern.CASE_INSENSITIVE);
            p[1] = Pattern.compile("^ARCON\\s+ASF#\\((\\d+)\\)", Pattern.CASE_INSENSITIVE);

            String uProfileName = "";
            String ac="200", avp="100", t="0.5", v="-1", retry="false";
            int uPInstNum = 0;
            for (int ii=0; ii<p.length; ii++)
            {
                m = p[ii].matcher(line);

                if (m.find() == true)
                {
                    switch(ii)
                    {
                        case 0:
                            ac = m.group(1);
                            avp = m.group(2);
                            t = m.group(3);
                            
                            if (m.group(4)!=null)
                            {
                                v = m.group(4);
                                arcWeldSpeed = Double.parseDouble(v);
                                
                                retry = (m.group(5)==null)?"false":"true";
                            }
                            else
                            {
                                if (line.trim().toUpperCase().endsWith(" RETRY"))
                                    retry = "true";
                            }
                            String [] arcStartData = {ac, avp, t, v, retry};
                            uPInstNum = uPInstExist(uP, arcStartData);
                            if (uPInstNum > 0)
                                uProfileName = "as" + uPInstNum;
                            else
                                uProfileName = "as" + arcStartCount++;
                            break;
                        case 1:
                            uProfileName = "file_as"+m.group(1);
                            String [] arcStartFile = {uProfileName};
                            uPInstNum = uPInstExist(uP, arcStartFile);
                            break;
                    }
                    break;
                }
            }
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return;

            LinkedList [] llaAttrData = new LinkedList[5];
            for (int ii=0; ii<5; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcStartCond[4]);
            llaAttrData[0].add("boolean");
            llaAttrData[0].add(retry);
            llaAttrData[1].add(arcStartCond[3]);
            llaAttrData[1].add("double");
            llaAttrData[1].add(v);
            llaAttrData[2].add(arcStartCond[0]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(ac);
            llaAttrData[3].add(arcStartCond[1]);
            llaAttrData[3].add("double");
            llaAttrData[3].add(avp);
            llaAttrData[4].add(arcStartCond[2]);
            llaAttrData[4].add("double");
            llaAttrData[4].add(t);
            uP = super.createUserProfile(uPList, "ARCStart", uProfileName, llaAttrData);
        } // end processArcOn

        private void processArcOff(String line, Element actList)
        {
            arcWeldSpeed = -1.0;
            
            Element act = (Element)actList.getLastChild();
            Element moAttr = kku.findChildElemByName(act, "MotionAttributes", null, null);
            if (null == moAttr)
            {
                // activity is not motion, cannot set UserProfile ARCEnd
                preProcessComment(line, actList, rbtLang);
                return;
            }
            /* <UserProfile Type="ARCEnd"> cannot be empty
             * therefore, only create after motion activity is found
             */
            Element uPList = super.getUserProfileListElement();
            Element uP = kku.findChildElemByName(uPList, "UserProfile", "Type", "ARCEnd");
            if (uP == null)
            {
                uP = m_xmlDoc.createElement("UserProfile");
                uPList.appendChild(uP);
                uP.setAttribute("Type", "ARCEnd");
            }

            Element actName = kku.findChildElemByName(act, "ActivityName", null, null);
            actName.getFirstChild().setNodeValue("ArcOf." + (m_moveCounter-1));
            Element moType = kku.findChildElemByName(moAttr, "MotionType", null, null);
            Element uPmot = m_xmlDoc.createElement("UserProfile");
            moAttr.insertBefore(uPmot, moType);
            uPmot.setAttribute("Type", "ARCEnd");

            Matcher m;
            Pattern [] p;
            p = new Pattern [2];
            String flt = "(\\d+(?:\\.(?:\\d+)?)?)\\s+";
            String fltns = "(\\d+(?:\\.(?:\\d+)?)?)";
            p[0] = Pattern.compile("^ARCOF\\s+AC="+flt+"AVP="+flt+"T="+fltns+"(\\s+ANTSTK)?", Pattern.CASE_INSENSITIVE);
            p[1] = Pattern.compile("^ARCOF\\s+AEF#\\((\\d+)\\)", Pattern.CASE_INSENSITIVE);

            String uProfileName = "";
            String ac="30", avp="90", t="0.5", antstk="false";
            int uPInstNum = 0;
            for (int ii=0; ii<p.length; ii++)
            {
                m = p[ii].matcher(line);

                if (m.find() == true)
                {
                    switch(ii)
                    {
                        case 0:
                            ac = m.group(1);
                            avp = m.group(2);
                            t = m.group(3);
                            antstk = (m.group(4)==null)?"false":"true";
                            String [] arcEndData = {ac, avp, t, antstk};
                            uPInstNum = uPInstExist(uP, arcEndData);
                            if (uPInstNum > 0)
                                uProfileName = "ae" + uPInstNum;
                            else
                                uProfileName = "ae" + arcEndCount++;
                            break;
                        case 1:
                            uProfileName = "file_ae"+m.group(1);
                            String [] arcEndFile = {uProfileName};
                            uPInstNum = uPInstExist(uP, arcEndFile);
                            break;
                    }
                    break;
                }
            }
            Text uPVal = m_xmlDoc.createTextNode(uProfileName);
            uPmot.appendChild(uPVal);

            if (uPInstNum > 0)
                return;

            LinkedList [] llaAttrData = new LinkedList[4];
            for (int ii=0; ii<4; ii++)
                llaAttrData[ii] = new LinkedList();
            llaAttrData[0].add(arcEndCond[3]);
            llaAttrData[0].add("boolean");
            llaAttrData[0].add(antstk);
            llaAttrData[1].add(arcEndCond[0]);
            llaAttrData[1].add("double");
            llaAttrData[1].add(ac);
            llaAttrData[2].add(arcEndCond[1]);
            llaAttrData[2].add("double");
            llaAttrData[2].add(avp);
            llaAttrData[3].add(arcEndCond[2]);
            llaAttrData[3].add("double");
            llaAttrData[3].add(t);
            uP = super.createUserProfile(uPList, "ARCEnd", uProfileName, llaAttrData);
        } // end processArcOff

        private int uPInstExist(Element uP, String [] arcStart)
        {
            Element uPInst;
            NodeList uDefAttrNodes;
            Element uDefAttr, name, val;
            int sameData, numParam;

            NodeList uPInstNodes = uP.getChildNodes();
            switch(arcStart.length)
            {
                case 1:
                    // ArcStartFile(ASF) & ArcEndFile(AEF)
                    for (int ii=0; ii<uPInstNodes.getLength(); ii++)
                    {
                        uPInst = (Element)uPInstNodes.item(ii);
                        if (uPInst.getAttribute("Name").equals(arcStart[0]))
                            return 1;
                    }
                    return 0;
                case 4:
                    // ArcOf
                    numParam=4;
                    for (int ii=0; ii<uPInstNodes.getLength(); ii++)
                    {
                        uPInst = (Element)uPInstNodes.item(ii);
                        if (uPInst.getAttribute("Name").startsWith("file_ae"))
                            continue;
                        uDefAttrNodes = uPInst.getChildNodes();
                        sameData = 0;
                        for (int jj=0; jj<uDefAttrNodes.getLength(); jj++)
                        {
                            uDefAttr = (Element)uDefAttrNodes.item(jj);

                            name = kku.findChildElemByName(uDefAttr, "DisplayName", null, null);
                            val = kku.findChildElemByName(uDefAttr, "Value", null, null);
                            for (int kk=0; kk<numParam; kk++)
                            {
                                if (name.getFirstChild().getNodeValue().equals(arcEndCond[kk]) &&
                                    val.getFirstChild().getNodeValue().equals(arcStart[kk]))
                                {
                                    sameData++;
                                    break;
                                }
                            }
                            if (sameData == numParam)
                                return ii+1;
                        }
                    }
                    return 0;
                case 5:
                    // ArcOn
                    numParam=5;
                    for (int ii=0; ii<uPInstNodes.getLength(); ii++)
                    {
                        uPInst = (Element)uPInstNodes.item(ii);
                        if (uPInst.getAttribute("Name").startsWith("file_as"))
                            continue;
                        uDefAttrNodes = uPInst.getChildNodes();
                        sameData = 0;
                        for (int jj=0; jj<uDefAttrNodes.getLength(); jj++)
                        {
                            uDefAttr = (Element)uDefAttrNodes.item(jj);

                            name = kku.findChildElemByName(uDefAttr, "DisplayName", null, null);
                            val = kku.findChildElemByName(uDefAttr, "Value", null, null);
                            for (int kk=0; kk<numParam; kk++)
                            {
                                if (name.getFirstChild().getNodeValue().equals(arcStartCond[kk]) &&
                                    val.getFirstChild().getNodeValue().equals(arcStart[kk]))
                                {
                                    sameData++;
                                    break;
                                }
                            }
                            if (sameData == numParam)
                                return ii+1;
                        }
                    }
                    return 0;
            }
            return 0;
        } // end uPInstExist
        
        private int processSFTON(String line)
        {
            Element objFrameProfileElem;
            Element objfpList = super.getObjectFrameProfileListElement();
            double [] uframeValues = {0.0,0.0,0.0,0.0,0.0,0.0};
            String [] strComp = line.split("[ ]+");
            String uframeName = strComp[1];
            
            int position = Integer.parseInt(uframeName.substring(1)); // skip 'P'
            if (p_listOfRobotTargets.size() < position + 1 ||
                p_listOfRobotTargets.get(position).toString().equals("Target Undefined"))
            {
                try {
                    bw.write("ERROR: Target not defined: " + uframeName + "\n");
                }
                catch (IOException bwe) {
                }
                return 1;
            }
            String [] uframeValStr = (String []) p_listOfRobotTargets.get(position);
            for (int ii=0; ii<6; ii++)
                uframeValues[ii] = Double.parseDouble(uframeValStr[ii+1])/1000.0;
            
            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType eObjFrameTypeWorld = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
            objFrameProfileElem = super.createObjectFrameProfile(objfpList, uframeName, eObjFrameTypeWorld, uframeValues, true);
            
            m_currentUframeName = uframeName;
            
            return 0;
        }

    	private void processSYSTARTStatement(String line, Matcher match, Element activityListElem)
    	{
    	    String conveyorStr = match.group(1);
    	    _ConveyorInitialPosition = Double.parseDouble(match.group(2))/1000.0;
            // hopefully program conveyor initial position STP is NOT -1000 so that
            // activity attribute ConveyorInitialPosition, InBoundOffset, and ConveyorNumber are created
    	    _LastConveyorInitialPosition = -1000.0;

            preProcessComment(line, activityListElem, rbtLang);
    	}
        
        private void processUframeFile(File uframef)
        {
            Element objFrameProfileElem;
            Element objfpList = super.getObjectFrameProfileListElement();
            BufferedReader uframe;
            String line, uframeName = "Object.1";
            String [] pos;
            int uframeNum = 1, ii;
            double [] uframeValues = {0.0,0.0,0.0,0.0,0.0,0.0};
            
            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType eObjFrameTypeWorld = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD;
            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType eObjFrameTypeRobot = DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE;
            objFrameProfileElem = super.createObjectFrameProfile(objfpList, "ROBOT", eObjFrameTypeWorld, uframeValues);
            // comment per WWG R20 changes uframeValues[0] = 0.000001;
            objFrameProfileElem = super.createObjectFrameProfile(objfpList, "BASE", eObjFrameTypeRobot, uframeValues);
            // comment per WWG R20 changes uframeValues[0] = 0.0;
            m_uframeProfileNames[0] = "ROBOT";
            m_uframeProfileNames[1] = "BASE";

            // line <//UFRAME 1>
            // line <///NAME UFrame1>
            // line <///TOOL 1>
            // line <///GROUP 1,0,0,0,0,0,0,0>
            // line <///PULSE>
            // line <////RORG C000= ...>
            // line <////BORG B000= ...>
            // line <////RXX C001= ...>
            // line <////BXX B001= ...>
            // line <////RYY C002= ...>
            // line <////BYY B002= ...>
            // line <////BUSER x,y,z,w,p,r>

            try
            {
                uframe = new BufferedReader(new FileReader(uframef));
                line = uframe.readLine();
                while (line != null)
                {
                    if (line.toUpperCase().startsWith("//UFRAME "))
                    {
                        uframeNum = Integer.parseInt(line.substring(9).trim());
                        uframeName = "Object." + uframeNum;
                        m_uframeProfileNames[uframeNum+1] = uframeName;
                    }
                    else
                    if (line.toUpperCase().startsWith("///NAME ") && line.length() > 8)
                    {
                        uframeName = line.substring(8).trim();
                        if (uframeName.length() == 0)
                            uframeName = "Object." + uframeNum;

                        m_uframeProfileNames[uframeNum+1] = uframeName;
                    }
                    else
                    if (line.toUpperCase().startsWith("////BUSER "))
                    {
                        pos = line.split("[ ,]");

                        for (ii=0; ii<3; ii++)
                            uframeValues[ii] = Double.parseDouble(pos[ii+1].trim())/1000;
                        for (ii=3; ii<6; ii++)
                            uframeValues[ii] = Double.parseDouble(pos[ii+1].trim());

                        objFrameProfileElem = super.createObjectFrameProfile(objfpList, uframeName, eObjFrameTypeRobot, uframeValues);

                        String uframeParameter = "ObjectProfile." + String.valueOf(uframeNum);
                        if (m_uframeParameters.containsKey(uframeParameter) == false)
                        {
                            m_uframeParameters.put(uframeParameter, uframeName);
                        }
                        
                        if (1==uframesAsFixedTCP[uframeNum])
                        {
                            Element tpfList = super.getToolProfileListElement();
                            Element toolProfileElem = super.createToolProfile(tpfList, uframeName, DNBIgpOlpUploadEnumeratedTypes.ToolType.STATIONARY, uframeValues, null, null, null);
                            m_toolProfileNames[uframeNum] = uframeName;
                            String toolParameter = "ToolProfile." + String.valueOf(uframeNum);
                            if (m_toolParameters.containsKey(toolParameter) == false)
                            {
                                m_toolParameters.put(toolParameter, uframeName);
                            }
                        }
                    }

                    line = uframe.readLine();
                }
            }
            catch (IOException e)
            {
                try {
                    bw.write( "ERROR: " + e.getMessage() + "\n" );
                }
                catch (IOException bwe) {
                }
                return;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-Caught ArrayIndexOutOfBoundsException: "+e.getMessage()+"\n");
                    bw.write("Uframe index over "); bw.write(String.valueOf(Integer.parseInt(e.getMessage())-2)); bw.write(" is not uploaded.\n");
                }
                catch (IOException bwe) {
                }
                return;
            }
        } // end of processUframeFile

        private void processToolFile(File toolf) throws NumberFormatException, Exception
        {
            Element toolProfileElem;
            Element tpfList = super.getToolProfileListElement();
            BufferedReader tool;
            String line, toolName;
            String [] pos;
            int toolNum, ii;
            double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};

            try
            {
                tool = new BufferedReader(new FileReader(toolf));
                line = tool.readLine(); // line <//TOOL 0>
                if (line==null)
                {
                	throw new Exception("Tool file is empty"); 
                }
                while (line != null)
                {
                    try
                    {
                    	toolNum = Integer.parseInt(line.substring(7).trim());
                    }
                    catch (NumberFormatException e)
                    {
                        throw new NumberFormatException(e.getMessage());
                    }
                    catch (Exception e)
                    {
                    	throw new Exception(e.getMessage()); 
                    }
                    toolName = "TOOL " + toolNum;
                    line = tool.readLine(); // line <///NAME TL1>
                    if (line.length() > 8)
                    {
                        toolName = line.substring(8).trim();
                        if (toolName.length() == 0)
                            toolName = "TOOL " + toolNum;
                    }

                    // DLY 2009/09/17 based on discussion with Wei the setting of this tool based on OLPStyle is not required since
                    // this entire method is bypsssed if the TOOL.CND file does not exits
                    // if (uploadStyle.equalsIgnoreCase("OperationComments") || uploadStyle.equalsIgnoreCase("Honda"))
                    
                    m_toolProfileNames[toolNum] = toolName;
                    
                    //  else
                    //    if (m_toolProfileNames[toolNum] == null)
                    //        m_toolProfileNames[toolNum] = toolName;

                    line = tool.readLine(); // line <x,y,z,w,p,r>
                    pos = line.split(",");

                    try
                    {
                    	for (ii=0; ii<3; ii++)
                    		toolValues[ii] = Double.parseDouble(pos[ii].trim())/1000;
                    	for (ii=3; ii<6; ii++)
                    		toolValues[ii] = Double.parseDouble(pos[ii].trim());
                    }
                    catch (NumberFormatException e)
                    {
                        throw new NumberFormatException(e.getMessage());
                    }

                    toolProfileElem = super.createToolProfile(tpfList, toolName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, null, null, null);

                    String toolParameter = "ToolProfile." + String.valueOf(toolNum);
                    if (m_toolParameters.containsKey(toolParameter) == false)
                    {
                        m_toolParameters.put(toolParameter, toolName);
                    }
                    
                    line = tool.readLine(); // line <centroid>
                    line = tool.readLine(); // line <mass>
                    line = tool.readLine(); // line <//TOOL ?> or <Inertia>
                    if (line != null)
                    {
                        if (line.startsWith("//TOOL "))
                        {
                            // next tool
                        }
                        else
                        {
                            line = tool.readLine(); // line <reserved>
                            line = tool.readLine(); // line <//TOOL ?>
                        }
                    }
                }
            }
            catch (IOException e)
            {
                try {
                    bw.write( "ERROR: " + e.getMessage() + "\n" );
                }
                catch (IOException bwe) {
                }
                return;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                try {
                    bw.write("ERROR-Caught ArrayIndexOutOfBoundsException: "+e.getMessage()+"\n");
                    bw.write("Tool index over "); bw.write(String.valueOf(Integer.parseInt(e.getMessage())-1)); bw.write(" is not uploaded.\n");
                }
                catch (IOException bwe) {
                }
                return;
            }
        } // end of processToolFile

        private String getToolName()
        {
            String toolName = null;
            if (toolFileExists == true)
            {
                toolName = m_toolProfileNames[m_currentToolNumber];
            }
            else
            {
                toolName = (String)m_toolNumberMapping.get(Integer.toString(m_currentToolNumber));
                if (toolName == null)
                {
                    if (m_currentToolNumber < m_toolProfileNames.length)
                        toolName = m_toolProfileNames[m_currentToolNumber];
                	// DLY Start 2009/06/19 use the toolnames sent as arguments if the correct name is found
                    String toolToFind = "TOOL " + Integer.toString(m_currentToolNumber);
                    for (int ii=0;ii<m_toolProfileNames.length;ii++)
                    {
                    	if (toolToFind.equals(m_toolProfileNames[ii]))
                    	{
                    		toolName = m_toolProfileNames[ii];
                    		break;
                    	}
                    }
                    // DLY End 2009/06/19
                }
                if (toolName == null)
                {
                	toolName = "Default";
                }
            }
            String toolParameter = "ToolProfile." + String.valueOf(m_currentToolNumber);
            if (m_toolParameters.containsKey(toolParameter) == false)
            {
                m_toolParameters.put(toolParameter, toolName);
            }
            return toolName;
        } // end of getToolName

        private String getUframeName()
        {
            String uframeName = null;
            if (null != m_currentUframeName)
            {
                uframeName = m_currentUframeName;
            }
            else
            {
                if (uframeFileExists == true)
                {
                    uframeName = m_uframeProfileNames[m_currentUframeNumber];
                }
                else
                {
                    uframeName = (String)m_uframeNumberMapping.get(Integer.toString(m_currentUframeNumber));
                    if (uframeName == null)
                        if (m_currentUframeNumber < m_uframeProfileNames.length)
                            uframeName = m_uframeProfileNames[m_currentUframeNumber];

                    if (uframeName == null)
                        uframeName = "Default";
                }
            }
            String uframeParameter = "ObjectProfile." + String.valueOf(m_currentUframeNumber);
            if (m_uframeParameters.containsKey(uframeParameter) == false)
            {
                m_uframeParameters.put(uframeParameter, uframeName);
            }
            return uframeName;
        } // end of getUframeName

        private void setConfigTurns(Matcher m)
        {
            if (m.group(7) != null)
                m_controller = "DX100";
            /* For DX100, configuration can be determined.
             * For other controllers, the actual config can be 2, 8, 4, or 6
             * and only be determined after calling inverse kinematics
             */
            int [] iaCfgMap = {1, 7, 3, 5, 2, 8, 4, 6};
            int L = Integer.parseInt(m.group(1));
            int M = Integer.parseInt(m.group(2));
            int N = Integer.parseInt(m.group(3));
            
            if (m_controller.equals("DX100"))
            {
                saCfgTurns[0] = String.valueOf(iaCfgMap[4*(1-L) + 2*M + N]);
            }
            else
            {
                saCfgTurns[0] = String.valueOf(iaCfgMap[2*M + N]);
            }
            saCfgTurns[1] = m.group(4); // RCONF O
            saCfgTurns[2] = m.group(5); // RCONF P
            saCfgTurns[3] = m.group(6); // RCONF Q
            saCfgTurns[4] = m.group(1); // RCONF L
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                     UTILITY FUNCTIONS
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////

        public double encoderPulsesToDegreesConversion(String jointType, double resolution, double zeroOffset, double pulseValue)
        {
            double degreeValue = 0.0;
            
            if(!Double.isNaN(resolution) && resolution != Double.MIN_VALUE && !Double.isNaN(zeroOffset) && !Double.isNaN(pulseValue)) {

                if(jointType.equals("Rotational"))
                   degreeValue = (pulseValue - zeroOffset)/resolution*360;
                else if(jointType.equals("Translational"))
                   degreeValue = (pulseValue - zeroOffset)/resolution;
                else
                    degreeValue = Double.NaN;
            }
            
            else if(!Double.isNaN(pulseValue))
                degreeValue = pulseValue;
            else
                degreeValue = Double.NaN;

            return degreeValue;
        } // end of encoderPulsesToDegreesConversion
        
        public void setSVJoint(String sTarget, String robotOrAuxJointType, int index)
        {
            double targetValue = 0.0;
            try {
                targetValue = Double.parseDouble(sTarget);
            }

            catch(NumberFormatException e) {
                e.getMessage();
                targetValue = Double.NaN;
            }

            int ii = index - 1;
            targetValue = this.encoderPulsesToDegreesConversion(m_axisTypes[ii], m_resolution[ii], m_zeroOffset[ii], targetValue);

            if (index > m_numRobotAxes && robotOrAuxJointType.equals("EndOfArmTooling"))
                dCloseJ = targetValue;
            else
                dCloseJ = Double.NaN;
        }
        
        public void formatJointValueInPulses(String sTarget, String robotOrAuxJointType, int index, ArrayList jTarget)
        {
            double targetValue = 0.0;
            try {
                targetValue = Double.parseDouble(sTarget);
                }

            catch(NumberFormatException e) {
                e.getMessage();
                targetValue = Double.NaN;
                }

            int ii = index - 1;
            targetValue = this.encoderPulsesToDegreesConversion(m_axisTypes[ii], m_resolution[ii], m_zeroOffset[ii], targetValue);

            if (index > m_numRobotAxes && robotOrAuxJointType.equals("EndOfArmTooling"))
                dCloseJ = targetValue;
            else
                dCloseJ = Double.NaN;

            if (m_axisTypes[ii].equals("Rotational"))
                targetValue = Math.toRadians(targetValue);
            else
                targetValue = targetValue * 0.001;

            String jName = "Joint " + index;
            jTarget.add(0, jName);
            jTarget.add(1, new Double(targetValue));
            jTarget.add(2, new Integer(index));

            DNBIgpOlpUploadEnumeratedTypes.DOFType jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL;
            if (m_axisTypes[ii].equals("Translational"))
                jType = DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL;
            jTarget.add(3, jType);

            if (robotOrAuxJointType.equals("RailTrackGantry") ||
                robotOrAuxJointType.equals("EndOfArmTooling") ||
                robotOrAuxJointType.equals("WorkpiecePositioner"))
            {
                DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxType;
                auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                if (robotOrAuxJointType.equals("RailTrackGantry"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY;
                else
                if (robotOrAuxJointType.equals("EndOfArmTooling"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING;
                else
                if (robotOrAuxJointType.equals("WorkpiecePositioner"))
                    auxType = DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER;

                jTarget.add(4, auxType);
            }
        } // end of formatJointValueInPulses

        public String createSpeedProfile(String profileName)
        {
            //<MotionProfile>
                //<Name>Default</Name>
                //<MotionBasis>Percent|Absolute</MotionBasis>
                //<Speed Units="%"|"m/s" Value="50" />
                //<Accel Units="%" Value="100" />
                //<AngularSpeedValue Units="%" Value="100" />
                //<AngularAccelValue Units="%" Value="100" />
            //</MotionProfile>

            Element motionProfileList = super.getMotionProfileListElement();

            DNBIgpOlpUploadEnumeratedTypes.MotionBasis moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT;
            double spd = 50.0, ang_spd = 100.0; // profileName is "Default"
            if (arcWeldSpeed > 0) // use ARCOn speed
            {
                moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
                spd = arcWeldSpeed/1000;
                profileName = "V=" + arcWeldSpeed;
                profileName = FixProfileName( profileName);
                
                boolean isInList = m_listOfSpeedValues.contains(profileName);
                if ( true == isInList )
                    return profileName;
                
                m_listOfSpeedValues.add(profileName);
            }
            else
            {
                profileName = FixProfileName(profileName);
                boolean isInList = m_listOfSpeedValues.contains(profileName);
                if ( true == isInList )
                    return profileName;
                
                m_listOfSpeedValues.add(profileName);
                
                if (profileName.startsWith("VJ="))
                {
                    spd = Double.parseDouble(profileName.substring(3));
                    // maximum linear speed is 1500 mm/s
                    // set absSpeed(mm/s) for ARC weld speed if V is omitted from ARCOn
                    absSpeed = spd * 1500/100;
                }
                if (profileName.startsWith("VR="))
                {
                    spd = 100.0;
                    absSpeed = spd * 1500/100;
                    ang_spd = Double.parseDouble(profileName.substring(3)); // degree per second in Motoman, but percentage in V5
                }
                if (profileName.startsWith("V="))
                {
                    moBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
                    absSpeed = Double.parseDouble(profileName.substring(2));
                    spd = absSpeed/1000;
                }
            }

            Element motionProfileElem = super.createMotionProfile(motionProfileList, profileName, moBasis, spd, 100.0, ang_spd, 100.0);
            
            return profileName;
        } // end of createSpeedProfile

        private String FixProfileName(String profileName)
        {
            // remove trailing '0' and '.'
            int nameLength;
            while ((profileName.endsWith("0") && profileName.indexOf('.') > 0) || profileName.endsWith("."))
            {
                nameLength = profileName.length();
                profileName = profileName.substring(0, nameLength-1);
            }
            
            return profileName;
        }
        
        public void createAccuracyProfile(String accuracyString, String profileName) throws NumberFormatException
        {
            //<AccuracyProfile>
                //<Name>Default</Name>
                //<FlyByMode>Off</FlyByMode>
                //<AccuracyType>Speed|Distance</AccuracyType>
                //<AccuracyValue Units="%" Value="0" />
            //</AccuracyProfile>

            Element accuracyProfileList = super.getAccuracyProfileListElement();

            boolean flyByMode = true;
            if (accuracyString.equals("0"))
                flyByMode = false;

            int pLevel = Integer.parseInt(accuracyString);
            double accuracyVal;
            Element accuracyProfileElem;
            
            switch (pLevel) {
                case -1:
                    accuracyVal = m_MotomanDefaultAccuracy;
                    accuracyProfileElem = super.createAccuracyProfile(
                                                    accuracyProfileList,
                                                    profileName,
                                                    DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                                                    flyByMode,
                                                    accuracyVal);
                    break;
                case -2:
                    accuracyVal = m_MotomanDefaultJntAccuracy;
                    accuracyProfileElem = super.createAccuracyProfile(
                                                    accuracyProfileList,
                                                    profileName,
                                                    DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                                                    flyByMode,
                                                    accuracyVal);
                    break;
                default:
                    accuracyVal = mPL[pLevel];
                    accuracyProfileElem = super.createAccuracyProfile(
                                                    accuracyProfileList,
                                                    profileName,
                                                    DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE,
                                                    flyByMode,
                                                    accuracyVal);
                    break;
            }
         } // end of createAccuracyProfile

        public void createRRSServoAttribs(Element actElem) {
    		String [] rrsAttribNames = {"_svspot", "_press_num","_thickness"};
    		String [] rrsAttribValues = {"0", "0", "0"};
    		String [] rrsAttribTypes = {"integer", "integer", "double"};
    		super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);

            m_servoAttribsAdded = 1;
        }
        
        public void setRRSServoAttribs( Element actElem, String pressure) {
    		String [] rrsAttribNames = {"_svspot", "_press_num","_thickness"};
    		String [] rrsAttribValues = {"1", "0", "0"};
    		String [] rrsAttribTypes = {"integer", "integer", "double"};
    		rrsAttribValues[1] = pressure;
    		super.createAttributeList(actElem, rrsAttribNames, rrsAttribValues, rrsAttribTypes);

        }

}//end class
