//COPYRIGHT DASSAULT SYSTEMES 2003

/* -----------------------------------------------------------------------------
 * creation: 11/11/2003 by aoh
 * modification 08/10/2004 by aoh
 *             added R15 support for device tasks, logic data, and user profiles
 * modifcation 12/22/2004 by dly
 *             remove R15 support for logic data
 * ---------------------------------------------------------------------------*/

package com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools;
//Classes that create DOM stream of data
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Text;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//Classes used to transform DOM stream to XML and to build DOM Document
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;
//IO classes used to write XML to file and to read from robot program file
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
//Collections and s used to store sets of data
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.LinkedList;
//DNBIgpOlpJavaBase Framework
import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.
                                                 DNBIgpOlpUploadEnumeratedTypes;

/**<CODE>DNBIgpOlpUploadBaseClass</CODE> is the top level class from which all 
 * the V5 OLP uploader classes should be derived. <P>It contains member methods 
 * that create OLP XML output, and member fields that are initialized with 
 * argument values sent from V5. <P>All the uploader classes should extend this 
 * class with methods which parse a robot program selected in V5, extract 
 * significant values from it, and use those values to invoke methods of this 
 * class to create the XML output. Certain number of string arguments 
 * is passed from V5 directly to the <CODE>main</CODE> method of the uploader 
 * class. Description of these arguments is available in the document 
 * <a href="V5ArgsToUploader.html">Description of V5 arguments passed to an 
 * uploader</a>. It is important that all of these arguments be passed to the 
 * super class constructor, from the uploader class constructor, so they can be
 * initialized.
 * <P>It is up to the uploader developer 
 * to create parsing methods, in order to fully utilize the XML 
 * creation methods of its super class. It is important to emphasize that if
 * certain rules are not followed, detailed through this class documentation, 
 * created XML output may not be a valid source for V5 upload. After creation,
 * and before the V5 upload, every XML file needs to be validated with 
 * <a href="Upload.xsd">OLP XML Validator</a>. Only if the XML file passes the
 * validation phase without any errors, will that file be uploaded successfully.
 * A documented example of a valid OLP XML file that contains all the supported 
 * OLP XML elements is also available for developer's reference: 
 * <a href="XMLFormat.xml">Example OLP XML template</a>.
 * <P>After the developer determines that his java class produces valid OLP XML
 * output, he/she should: set a <CODE>CLASSPATH</CODE> system variable to point 
 * to V5's "startup/OLP/Parsers/UploaderTools", directory, then create a jar 
 * file (out of all his/hers compiled uploader class files), save it in a 
 * selected directory, and set that directory path in V5's 
 * "Tools/Options/IGRIP/Offline Programming/Uploader Directory" text field. 
 * Then, upload command can be invoked in the same way as for delivered Delmia
 * translators. Valid OLP XML output file, after being uploaded, should result 
 * in creation of: all the supported V5 actions and activities within the 
 * selected V5 robot task, all the controller profiles, and all the tags and tag
 * groups.
 * <P>Not all the methods of this class, create XML elements with all of their
 * sub-elements. Some of the methods create just a top level element, and to 
 * create the sub-elements, more methods should be invoked. All of the methods, 
 * which do not create all of their descendent elements, are documented as such,
 * and html links in "See Also" sections are provided throughout the 
 * documentation to guide the developer to sequentialy call remaining methods, 
 * which would complete the XML structure.
 * <P><B>Example:</B><P>To create the XML output for a robot motion 
 * activity, series of methods should be called in correct order (this example 
 * is just one way to create a robot motion activity, but certainly not the only
 * way):<P>
 * createMotionActivityHeader(...)<BR>
 * createMotionAttributes(...)<BR>
 * createTarget(...)<BR>
 * createCartesianTarget(...)<BR>
 * createTurnNumbers(...)<BR>
 * createTag(...)<BR>
 * <P> On the other hand, only one method call is required to create a delay 
 * activity (which creates all the required sub-elements):<P>
 * createDelayActivity(...)<BR>
 * <P>All the robot program statements that do not have V5 representation, can 
 * still be preserved in specially named V5 attributes: 
 * "PreviousRobotStatements" and "FollowingRobotStatements" added to 
 * "AttributeList" sub-elements of "Action" and "Activity" XML elements to 
 * signify that there was some contents of the robot program, which was 
 * untranslatable between robot language statements that have their V5 matches.
 * Also, information about exceptions caught in the uploader class, or any type 
 * of irregularities discovered during parsing of robot language program, should
 * be written to the supplied error log file (its path is sent to the <CODE>main
 * </CODE> method of the uploader class, as a V5 argument), since the content of 
 * that file will be displayed in specially marked section of the OLP uploading 
 * report in V5.  
 * @author AOH
 * @version V5R14 12/12/2003
 * @since V5R14
 */

public class DNBIgpOlpUploadBaseClass {
    
    private static final int NUMBER_OF_AXIS_TYPES = 4;
    private static final int DEFAULT_NUMBER_OF_ROBOT_AXES = -1;
    private static final int DEFAULT_NUMBER_OF_RAIL_AUX_AXES = -1;
    private static final int DEFAULT_NUMBER_OF_TOOL_AUX_AXES = -1;
    private static final int DEFAULT_NUMBER_OF_POSITIONER_AUX_AXES = -1;
    private static final int NUMBER_OF_POS_AND_ORI_PARAMETERS = 6;
    private static final int TOTAL_TURN_NUMBERS = 4;
    private static final int TOTAL_TURN_SIGNS = 4;
    private static final int ONLY_ROBOT_AXES_IN_JOINT_TARGET = 4;
    private static final int ROBOT_AND_AUX_AXES_IN_JOINT_TARGET = 6;
    private static final int MIN_NUM_OF_INPUT_ARGUMENTS = 11;
    private static final int NUMBER_OF_CENTROID_PARAMETERS = 3;
    private static final int NUMBER_OF_INERTIA_PARAMETERS = 6;
    private static final int NUM__USER_PROFILE_ATT_PARAMETERS = 3;
    
    private Document m_Document = null;
    private Element m_OLPDataElement = null;
    private Element m_ResourceElement = null;
    private Element m_ControllerElement = null;
    private Element m_MotionProfileListElem = null;
    private Element m_AccuracyProfileListElem = null;
    private Element m_ToolProfileListElem = null;
    private Element m_ObjectFrameProfileListElem = null;
    private Element m_UserProfileListElem = null;

    private String m_PathToRobotProgramFile;
    private String m_PathToXMLOutputFile;
    private String m_PathToErrorLogFile;
    private String m_PathToXMLSchemaFile;
    private String [] m_AxisTypes = null;
    private Hashtable m_ParameterNameValuePairs = null;
    private int m_NumberOfRobotAxes = -1;
    private int m_NumberOfRailAuxiliaryAxes = -1;
    private int m_NumberOfToolAuxiliaryAxes = -1;
    private int m_NumberOfWorkpiecePositionerAuxiliaryAxes = -1;
    
    private DNBIgpOlpUploadEnumeratedTypes.TrackType m_TrackType = DNBIgpOlpUploadEnumeratedTypes.TrackType.TRACK_OFF;
    	
    /** Allocates a <CODE>DNBIgpOlpUploadBaseClass</CODE> object, initializes V5 
     * Igrip arguments, creates an instance of a DOM Document, instances of 
     * the following XML elements:
     * <CODE>OLPData</CODE> (root element), <CODE>Resource</CODE>, 
     * <CODE>Controller</CODE>, <CODE>MotionProfileList</CODE>, 
     * <CODE>ToolProfileList</CODE>, <CODE>AccuracyProfileList</CODE>, and 
     * <CODE>ObjectFrameProfileList</CODE>.
     * No child elements are created for the above elements through this 
     * constructor invocation. 
     * @param args array of Igrip V5 arguments, sent from Igrip V5 to the 
     * <CODE>main</CODE> method of the uploader class (derived from this class),
     * and passed to this class constructor by invoking the derived class super
     * constructor.
     *
     * <P>Example:
     *
     * <P>In the uploader class <CODE>main</CODE> method, create an instance
	 * of the uploader class in the following way:
	 * <PRE>public main(String [] args) {
	 *        ...
	 *        MyUploaderClass myInstanceOfUploaderClass = 
         *                                            new MyUploaderClass(args);
	 *        ...
	 *      }
     * </PRE>
     * Then, from the uploader class constructor call its super class 
	 * constructor (i.e. this class constructor) and pass the same array of 
	 * string arguments in the following way:
     * <PRE>public MyUploaderClass(String [] args) {
     *        super(args);
     *        ...
     *     }
     * </PRE>
	 * Invocation of the super class constructor must appear as the first 
	 * statement within the uploader class constructor method, even before 
	 * local variable declarations.
     * @throws ParserConfigurationException if DOM Document cannot be created
     * @since V5R14
     */    
    public DNBIgpOlpUploadBaseClass(String [] args) throws 
                                                  ParserConfigurationException {
        for (int ii=0; ii<args.length; ii++)
        {
            if (args[ii].startsWith("\""))
                args[ii] = args[ii].substring(1);
            if (args[ii].endsWith("\""))
                args[ii] = args[ii].substring(0, args[ii].length()-1);
        }

        if(args != null && args.length >= MIN_NUM_OF_INPUT_ARGUMENTS) {
            m_PathToRobotProgramFile = args[0];
            m_PathToXMLOutputFile = args[1];

            String [] sParameterNameValuePairs = args[2].split("[\\t]+");

            if (sParameterNameValuePairs != null && 
                                          sParameterNameValuePairs.length > 0) {
                m_ParameterNameValuePairs = new Hashtable();
                
                for (int ii = 0; ii < sParameterNameValuePairs.length; 
                                                                   ii = ii + 2){
                    if ( (ii + 1) <  sParameterNameValuePairs.length) {
                        m_ParameterNameValuePairs.put(
                                                   sParameterNameValuePairs[ii], 
                                              sParameterNameValuePairs[ii + 1]);
                    }
                    else {
                        continue;
                    }
                }
            }

            m_AxisTypes = args[4].split("[\\t]+");

            if (m_AxisTypes != null && m_AxisTypes.length > 0) {
                for (int jj = 0; jj < m_AxisTypes.length; jj++) {
                    if (! m_AxisTypes[jj].equals("Rotational") && (! 
                                     m_AxisTypes[jj].equals("Translational"))) {
                        m_AxisTypes = null;
                        break;
                    }
                }
            }

            String [] saNumberOfAxes = args[5].split("[\\t]+");

            if (saNumberOfAxes != null && saNumberOfAxes.length >= 
                                                         NUMBER_OF_AXIS_TYPES) {
                m_NumberOfRobotAxes = Integer.parseInt(saNumberOfAxes[0]);
                m_NumberOfRailAuxiliaryAxes = Integer.parseInt(
                                                             saNumberOfAxes[1]);
                m_NumberOfToolAuxiliaryAxes = Integer.parseInt(
                                                             saNumberOfAxes[2]);
                m_NumberOfWorkpiecePositionerAuxiliaryAxes = Integer.parseInt(
                                                             saNumberOfAxes[3]);
            }
            else {
                m_NumberOfRobotAxes = DEFAULT_NUMBER_OF_ROBOT_AXES;
                m_NumberOfToolAuxiliaryAxes = DEFAULT_NUMBER_OF_TOOL_AUX_AXES;
                m_NumberOfRailAuxiliaryAxes = DEFAULT_NUMBER_OF_RAIL_AUX_AXES;
                m_NumberOfWorkpiecePositionerAuxiliaryAxes = 
                                          DEFAULT_NUMBER_OF_POSITIONER_AUX_AXES;
            }

            m_PathToErrorLogFile = args[8];
            m_PathToXMLSchemaFile = args[10];

            //Get the xml document, by creating the parser first
            DocumentBuilderFactory oDocBuiderFactory = 
                                           DocumentBuilderFactory.newInstance();
            oDocBuiderFactory.setValidating(false);
            DocumentBuilder oDocumentBuilder = 
                                         oDocBuiderFactory.newDocumentBuilder();
            m_Document = oDocumentBuilder.newDocument();

            if (m_Document != null) {
                //Create the OLPData and Resource elements
                m_OLPDataElement = m_Document.createElement("OLPData");
                m_Document.appendChild(m_OLPDataElement);

                m_OLPDataElement.setAttribute
                     ("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                m_OLPDataElement.setAttribute("xsi:noNamespaceSchemaLocation", 
                                               m_PathToXMLSchemaFile);

                m_ResourceElement = m_Document.createElement("Resource");
                m_OLPDataElement.appendChild(m_ResourceElement);

                m_ControllerElement = m_Document.createElement("Controller");
                m_ResourceElement.appendChild(m_ControllerElement);

                m_MotionProfileListElem = 
                                  m_Document.createElement("MotionProfileList");
                m_ControllerElement.appendChild(m_MotionProfileListElem);

                m_AccuracyProfileListElem = 
                                m_Document.createElement("AccuracyProfileList");
                m_ControllerElement.appendChild(m_AccuracyProfileListElem);

                m_ToolProfileListElem = 
                                    m_Document.createElement("ToolProfileList");
                m_ControllerElement.appendChild(m_ToolProfileListElem);

                m_ObjectFrameProfileListElem = 
                             m_Document.createElement("ObjectFrameProfileList");
                m_ControllerElement.appendChild(m_ObjectFrameProfileListElem);
                
                m_UserProfileListElem = 
                                    m_Document.createElement("UserProfileList");
                m_ControllerElement.appendChild(m_UserProfileListElem);
            }
        }
    }
    
    /** Returns root XML element, named <CODE>OLPData</CODE>, or 
     * <CODE>null</CODE>, if <CODE>OLPData</CODE> XML element has not been 
     * created.
     * @return root XML element, or <CODE>null</CODE>, if
     * <CODE>OLPData</CODE> XML element has not been created.
     * @since V5R14
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>OLPData</CODE> XML element is created.
     */    
    public Element getRootElement() {
        return m_OLPDataElement;
    }
    
    /** Returns <CODE>Resource</CODE> XML element, or <CODE>null</CODE>, if
     * <CODE>Resource</CODE> XML element has not been created.
     * @return <CODE>Resource</CODE> XML element, or <CODE>null</CODE>, if 
     * <CODE>Resource</CODE> XML element has not been created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>Resource</CODE> XML element is created.
     * @since V5R14
     */    
    public Element getResourceElement() {
        return m_ResourceElement;
    }
    
    /** Returns <CODE>Controller</CODE> XML element, or <CODE>null</CODE>, if
     * <CODE>Controller</CODE> XML element has not been created.
     * @return <CODE>Controller</CODE> XML element, or <CODE>null</CODE>, if 
     * <CODE>Resource</CODE> XML element has not been created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>Controller</CODE> XML element is created.
     * @since V5R14
     */    
    
    public Element getControllerElement() {
        return m_ControllerElement;
    }
    
    /** Returns <CODE>MotionProfileList</CODE> XML element, or <CODE>null</CODE>
     * , if <CODE>MotionProfileList</CODE> XML element has not been created.
     * @return <CODE>MotionProfileList</CODE> XML element, or <CODE>null</CODE>,
     * if <CODE>MotionProfileList</CODE> XML element has not been created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>MotionProfileList</CODE> XML element is created.
     * @since V5R14
     */    
    
    public Element getMotionProfileListElement() {
        return m_MotionProfileListElem;
    }
        
    /** Returns <CODE>ObjectFrameProfileList</CODE> XML element, or <CODE>null
     * </CODE>, if <CODE>ObjectFrameProfileList</CODE> XML element has not been 
     * created.
     * @return <CODE>ObjectFrameProfileList</CODE> XML element, or <CODE>null
     * </CODE>, if <CODE>ObjectFrameProfileList</CODE> XML element has not been 
     * created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>ObjectFrameProfileList</CODE> XML element is created.
     * @since V5R14
     */  
    
    public Element getObjectFrameProfileListElement() {
        return m_ObjectFrameProfileListElem;
    }
        
    /** Returns <CODE>AccuracyProfileList</CODE> XML element, or <CODE>null
     * </CODE>, if <CODE>AccuracyProfileList</CODE> XML element has not been 
     * created.
     * @return <CODE>AccuracyProfileList</CODE> XML element, or <CODE>null
     * </CODE>, if <CODE>AccuracyProfileList</CODE> XML element has not been 
     * created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>AccuracyProfileList</CODE> XML element is created.
     * @since V5R14
     */    
    
    public Element getAccuracyProfileListElement() {
        return m_AccuracyProfileListElem;
    }
        
    /** Returns <CODE>ToolProfileList</CODE> XML element, or <CODE>null</CODE>
     * , if <CODE>ToolProfileList</CODE> XML element has not been created.
     * @return <CODE>ToolProfileList</CODE> XML element, or <CODE>null</CODE>,
     * if <CODE>ToolProfileList</CODE> XML element has not been created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>ToolProfileList</CODE> XML element is created.
     * @since V5R14
     */ 
    
    public Element getToolProfileListElement() {
        return m_ToolProfileListElem;
    }
    
    /** Returns <CODE>UserProfileList</CODE> XML element, or <CODE>null</CODE>
     * , if <CODE>UserProfileList</CODE> XML element has not been created.
     * @return <CODE>UserProfileList</CODE> XML element, or <CODE>null</CODE>,
     * if <CODE>UserProfileList</CODE> XML element has not been created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * <CODE>UserProfileList</CODE> XML element is created.
     * @since V5R15
     */ 
    
    public Element getUserProfileListElement() {
        return m_UserProfileListElem;
    }
    
    /** Returns DOM Document, or <CODE>null</CODE>, if DOM Document has not been
     * created.
     * @return DOM Document, or <CODE>null</CODE>, if DOM Document has not been 
     * created.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where 
     * DOM Document is created.
     * @since V5R14
     */    
    
    public Document getDOMDocument() {
        return m_Document;
    }
    
    /** Returns the file path of the robot program file to be uploaded, or an
     * empty string, if V5 Igrip failed to deliver that argument.
     * @return the string representation of the file path, or an empty string if 
     * failed.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @since V5R14
     */
    
    public String getPathToRobotProgramFile() {
        return m_PathToRobotProgramFile;
    }
        
    /** Returns the file path of the XML file created from the translated robot
     * program, or an empty string, if V5 Igrip failed to deliver that argument.
     * @return the string representation of the file path, or an empty string if 
     * failed.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @since V5R14
     */
    
    public String getPathToXMLOutputFile() {
        return m_PathToXMLOutputFile;
    }
    
    /** Returns the file path of an empty, temporary file, in which the result 
     * of OLP XML file creation is stored, or an empty string, if V5 Igrip 
     * failed to deliver that argument.
     * <P>The contents of this file will be displayed in "Uploading Info" dialog
     * box that appears at the end of execution of Upload command. This file 
     * should either contain the information that informs the user that OLP XML 
     * file has been created without errors after parsing the robot program file
     , or the list of unparsed statements should be written instead. 
     * @return the string representation of the file path, or an empty string if 
     * failed.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @since V5R14
     */
    
    public String getPathToErrorLogFile() {
        return m_PathToErrorLogFile;
    }
    
    /** Returns the file path of an XML Schema validator file, or an empty 
     * string, if V5 Igrip failed to deliver that argument.
     * <P>The validator file will determine whether XML file created after
     * parsing the robot program, complies with V5 OLP XML rules and standards.
     * In case of an invalid XML file, an error message will be shown in the 
     * "Uploading Info" dialog box that appears at the end of execution of 
     * Upload command. Similarly, if the XML file is valid, a user will be 
     * displayed that information in the same dialog box.
     * @return the string representation of the file path, or an empty string if 
     * failed.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @since V5R14
     */
    
    public String getPathToXMLSchemaFile() {
        return m_PathToXMLSchemaFile;
    }
    
    /** Returns the array of string representations of dof types for the 
     * uploading resource (including its robot, and auxiliary axis values). 
     * Supported array member values are "Rotational" and "Translational". 
     * @return an array that contains string representations of DOF types for 
     * all the joints, in order in which they appear in robot's kinematics
     * chain, or <CODE>null</CODE>, if a non-recognized dof type was attempted 
     * to be set as a member of this array.
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @see DNBIgpOlpUploadEnumeratedTypes.DOFType 
     * @since V5R14
     */
    
    public String [] getAxisTypes() {
        return m_AxisTypes;
    }
    
    /** Returns <CODE>Hashtable</CODE> object, whose keys are resource parameter
     * names, and values are resource parameter values. <P>The parameters are 
     * set by right-clicking on a selected resource node in the PPR tree, 
     * selecting the "Properties..." menu item, and then defining an arbitrary 
     * long list of parameter name/value pairs.
     * @return reference to greater then zero size <CODE>Hashtable</CODE> object
     * , or zero size <CODE>Hashtable</CODE> object if it fails
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @since V5R14
     */
    
     public  Hashtable getParameterData() {
        return m_ParameterNameValuePairs;
    }
     
    /** Returns the number of robot axes. <P>Robot axes are the axes in robot's
     * kinematics chain that cannot be detached from the robot, without 
     * impairing its basic mechanics structure. All the robots in Delmia's robot 
     * library contain robot axes only. In V5 terms, robot axes are all 
     * non-auxiliary axes in robot's kinematics chain.
     * @return number of robot axes, or -1 if it fails
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @since V5R14
     */
     
    public int getNumberOfRobotAxes() {
        return m_NumberOfRobotAxes;
    }
    
    /** Returns the number of rail auxiliary axes. <P>In V5 terms, rail 
     * auxiliary axes can be added to robot's basic mechanics structure by 
     * selecting <CODE>Insert/Robot Controller/ Define Aux Device</CODE> menu 
     * pick from <CODE>IGRIP/Robot Task Defonition</CODE> workbench, and they 
     * are always of type <CODE>AuxAxisType.RAIL_TRACK_GRANTRY</CODE>.
     * @return number of rail auxiliary axes, or -1 if it fails
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @see DNBIgpOlpUploadEnumeratedTypes.AuxAxisType
     * @since V5R14 
     */
    
    public int getNumberOfRailAuxiliaryAxes() {
        return m_NumberOfRailAuxiliaryAxes;
    }
    
    /** Returns the number of tool auxiliary axes. <P>In V5 terms, tool 
     * auxiliary axes can be added to robot's basic mechanics structure by 
     * selecting <CODE>Insert/Robot Controller/ Define Aux Device</CODE> menu 
     * pick from <CODE>IGRIP/Robot Task Defonition</CODE> workbench, and they 
     * are always of type <CODE>AuxAxisType.END_OF_ARM_TOOLING</CODE>.
     * @return number of tool auxiliary axes, or -1 if it fails
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @see DNBIgpOlpUploadEnumeratedTypes.AuxAxisType 
     * @since V5R14
     */
    
    public int getNumberOfToolAuxiliaryAxes() {
        return m_NumberOfToolAuxiliaryAxes;
    }
    
    /** Returns the number of workpiece positioner auxiliary axes. <P>In V5 
     * terms, workpiece positioner auxiliary axes can be added to robot's basic 
     * mechanics structure by selecting <CODE>Insert/Robot Controller/ Define 
     * Aux Device</CODE> menu pick from <CODE>IGRIP/Robot Task Defonition</CODE> 
     * workbench, and they are always of type <CODE>
     * AuxAxisType.WORKPIECE_POSITIONER</CODE>.
     * @return number of positioner axes, or -1 if it fails
     * @see #DNBIgpOlpUploadBaseClass(String []) Class constructor, where this
     * value gets initialized based on the corresponding argument sent from V5 
     * Igrip.
     * @see DNBIgpOlpUploadEnumeratedTypes.AuxAxisType
     * @since V5R14 
     */
    
    public int getNumberOfWorkpiecePositionerAuxiliaryAxes() {
        return m_NumberOfWorkpiecePositionerAuxiliaryAxes;
    }
    
    /** Returns created <CODE>ActivityList</CODE> XML element and sets the name 
     * for a robot task to be created in V5 Igrip. <P>The contents of this 
     * element, later to be created, will determine which actions and activities 
     * will be created in the specified robot task.
     * @param sRobotTaskName name of the robot task to be created in V5 Igrip
     * @return <CODE>ActivityList</CODE> XML element (no sub-elements will be
     * created), or <CODE>null</CODE> if <CODE>sRobotTaskName</CODE> argument is
     * an empty string.
     * @see DNBIgpOlpUploadBaseClass
     * @since V5R14
     */
    
    public Element createActivityList(String sRobotTaskName) {
        Element oActivityListElement = m_Document.createElement("ActivityList");
        m_ResourceElement.appendChild(oActivityListElement);
        
        if(sRobotTaskName != null && (! sRobotTaskName.equals(""))) {
            oActivityListElement.setAttribute("Task", sRobotTaskName);
        }
        else {
            return null;
        }
        
        return oActivityListElement;
    }
    
    /**Returns created <CODE>Action</CODE> XML element. <P>All the sub-elements
     * of the returned element will be created, except for <CODE>AttributeList
     * </CODE> and child <CODE>ActivityList</CODE> elements, which need to be 
     * created explicitly. 
     * Based on this XML description, an Action of specified type will be 
     * created in a selected Robot Task in V5 Igrip.
     * @param oParentActivityListElement parent XML element, named 
     * <CODE>ActivityList</CODE> (not to be confused with child <CODE>
     * ActivityList</CODE> element, which has the same tag name)
     * @param sActionName name of this action, as it will appear in V5 Igrip
     * @param sActionType type of the action to be created. Only the action
     * libraries that have been previously created in V5, and that have been 
     * declared through "Tools/Options/Igrip/Action Libraries" user interface,
     * contain individual actions authorized for creation through OLP uploading.
     * @param sResourceName name of the resource that performs the action. 
     * Usually, this is the name of a tool that has been declared as robot's aux 
     * axis.
     * @return created <CODE>Action</CODE> XML element, or <CODE>null
     * </CODE> if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE> if any of the passed string parameters is an empty 
     * string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createActionHeader(Element oParentActivityListElement, 
                                      String sActionName, 
                                      String sActionType, 
                                      String sResourceName) {
        Element oActionElem = null;
        
        if (oParentActivityListElement != null &&
            sActionName != null && (! sActionName.equals("")) &&
            sActionType != null && (! sActionType.equals("")) &&
            sResourceName != null && (! sResourceName.equals(""))) {
                
            oActionElem = m_Document.createElement("Action");
            oParentActivityListElement.appendChild(oActionElem);
            oActionElem.setAttribute("ActionType", sActionType);
            
            Element oActionNameElem = m_Document.createElement("ActionName");
            oActionElem.appendChild(oActionNameElem);
            
            Text oActionNameText = m_Document.createTextNode(sActionName);
            oActionNameElem.appendChild(oActionNameText);

            Element oToolResourceElem = m_Document.createElement
                                                               ("ToolResource");
            oActionElem.appendChild(oToolResourceElem);

            Element oResourceNameElem = m_Document.createElement
                                                               ("ResourceName");
            oToolResourceElem.appendChild(oResourceNameElem);
            
            Text oResourceNameText = m_Document.createTextNode(sResourceName);
            oResourceNameElem.appendChild(oResourceNameText);
        }
        
        return oActionElem;
    }
    
    /**Returns created <CODE>Action</CODE> XML element. <P>All the sub-elements
     * of the returned element will be created, except for <CODE>AttributeList
     * </CODE> and child <CODE>ActivityList</CODE> elements, which need to be 
     * created explicitly. 
     * Based on this XML description, an Action of specified type will be 
     * created in a selected Robot Task in V5 Igrip.<P>To be used with PPR Hub 
     * data only.
     * @param oParentActivityListElement parent XML element, named 
     * <CODE>ActivityList</CODE> (not to be confused with child <CODE>
     * ActivityList</CODE> element, which has the same tag name)
     * @param sActionName name of this action, as it will appear in V5 Igrip
     * @param sActionType type of the action to be created. Only the action
     * libraries that have been previously created in V5, and that have been 
     * declared through "Tools/Options/Igrip/Action Libraries" user interface,
     * contain individual actions authorized for creation through OLP uploading.
     * @param sResourceName name of the resource that performs the action. 
     * Usually, this is the name of a tool that has been declared as robot's aux 
     * axis.
     * @param sResourceParentName name of a V5 parent of the V5 resource
     * specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * resource needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @return created <CODE>Action</CODE> XML element, or <CODE>null
     * </CODE> if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE> if any of the passed string parameters is an empty 
     * string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createActionHeader(Element oParentActivityListElement, 
                                      String sActionName, 
                                      String sActionType, 
                                      String sResourceName, 
                                      String sResourceParentName) {
        
        Element oActionElem = createActionHeader(oParentActivityListElement, 
                                                 sActionName, 
                                                 sActionType, 
                                                 sResourceName);
            
        if (oActionElem != null && sResourceParentName != null &&
                                           (! sResourceParentName.equals(""))) {
                                               
            NodeList oResourceNameNodeList = 
                               oActionElem.getElementsByTagName("ResourceName");

            if (oResourceNameNodeList != null && 
                                        oResourceNameNodeList.getLength() > 0) {
                                            
                Node oResourceNameNode = oResourceNameNodeList.item(0);
                Element oResourceNameElem = (Element) oResourceNameNode;

                if (oResourceNameElem != null) {
                    oResourceNameElem.setAttribute("ParentName", 
                                                   sResourceParentName);
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        }
        
        return oActionElem;
    }
    
    /** Returns created <CODE>ActivityList</CODE> XML element as a child of the 
     * passed <CODE>Action</CODE> element.
     * @param oParentActionElem parent XML element, with tag name <CODE>Action
     * </CODE>  
     * @return <CODE>ActivityList</CODE> XML element (no sub-elements will be
     * created), or <CODE>null</CODE> if <CODE>oParentActionElem</CODE> argument
     * is a <CODE>null</CODE> reference.
     * @see DNBIgpOlpUploadBaseClass
     * @since V5R14
     */
    
    public Element createActivityListWithinAction(Element oParentActionElem) {
        Element oActivityListElement = m_Document.createElement("ActivityList");
        oParentActionElem.appendChild(oActivityListElement);
        
        return oActivityListElement;
    }
    
    /** Returns created <CODE>AttributeList</CODE> XML element. This element is
     * an authorised child of any <CODE>Action</CODE>, <CODE>Activity</CODE>, 
     * and top level <CODE>ActivityList</CODE> element. All the sub-elements of
     * the returned element will be created, based on the argument values. Based 
     * on values of parameter string arrays, corresponding V5 attribues will be 
     * created for specified V5 activities, actions, and robot tasks.
     * @param oParentElem parent element of the <CODE>AttributeList</CODE> 
     * element to be created
     * @param saAttributeNames <CODE>String</CODE> array, whose values 
     * represent V5 attribute names. Position of array elements matters, since 
     * an attribute name needs to have the same array index as its 
     * corresponding attribute value (stored in <CODE>saAttributeValues</CODE> 
     * parameter). 
     * @param saAttributeValues <CODE>String</CODE> array, whose values 
     * represent V5 attribute values. Position of array elements matters, since 
     * an attribute value needs to have the same array index as its 
     * corresponding attribute name (stored in <CODE>saAttributeNames</CODE> 
     * parameter). 
     * @return created <CODE>AttributeList</CODE> XML element (with its 
     * sub-elements), or <CODE>null</CODE>, if any of the argumets passed to 
     * this method have a <CODE>null</CODE> reference, or <CODE>null</CODE>, if
     * lengths of the parameter string arrays are not the same.
     * @see DNBIgpOlpUploadBaseClass
     * @since V5R14
     */
    
    public Element createAttributeList(Element oParentElem, 
                                       String [] saAttributeNames,
                                       String [] saAttributeValues) {
        Element oAttributeListElem = null;
        
        if (oParentElem != null) {
            
            NodeList oAttributeListNodeList = 
                              oParentElem.getElementsByTagName("AttributeList");
            if (oAttributeListNodeList != null) {
                oAttributeListElem = (Element)oAttributeListNodeList.item(0);
            }
            
            if (oAttributeListElem == null) {
               oAttributeListElem = m_Document.createElement("AttributeList");
               oParentElem.appendChild(oAttributeListElem);
            }
            
            if (saAttributeNames != null && saAttributeValues != null && 
                          saAttributeNames.length == saAttributeValues.length) {

                for(int ii = 0; ii < saAttributeNames.length && 
                                          ii < saAttributeValues.length; ii++) {
                    String sAttributeName = saAttributeNames[ii];
                    String sAttributeValue =  saAttributeValues[ii];
                    
                    Element oAttributeElem =
                                          m_Document.createElement("Attribute");
                    oAttributeListElem.appendChild(oAttributeElem);
                    
                    Element oAttributeNameElem = 
                                      m_Document.createElement("AttributeName");
                    oAttributeElem.appendChild(oAttributeNameElem);
                    
                    Text oAttributeNameText = 
                                      m_Document.createTextNode(sAttributeName);
                    oAttributeNameElem.appendChild(oAttributeNameText);
                    
                    Element oAttributeValueElem = 
                                     m_Document.createElement("AttributeValue");
                    oAttributeElem.appendChild(oAttributeValueElem);
                    
                    Text oAttributeValueText = 
                                     m_Document.createTextNode(sAttributeValue);
                    oAttributeValueElem.appendChild(oAttributeValueText);
                }
            }
            else {
                return null;
            }
        }
        
        return oAttributeListElem;
    }
    
    /** Returns created <CODE>AttributeList</CODE> XML element. This element is
     * an authorised child of any <CODE>Action</CODE>, <CODE>Activity</CODE>, 
     * and top level <CODE>ActivityList</CODE> element. All the sub-elements of
     * the returned element will be created, based on the argument values. Based 
     * on values of parameter string arrays, corresponding V5 attribues will be 
     * created for specified V5 activities, actions, and robot tasks.
     * @param oParentElem parent element of the <CODE>AttributeList</CODE> 
     * element to be created
     * @param saAttributeNames <CODE>String</CODE> array, whose values 
     * represent V5 attribute names. Position of array elements matters, since 
     * an attribute name needs to have the same array index as its 
     * corresponding attribute value (stored in <CODE>saAttributeValues</CODE> 
     * parameter). 
     * @param saAttributeValues <CODE>String</CODE> array, whose values 
     * represent V5 attribute values. Position of array elements matters, since 
     * an attribute value needs to have the same array index as its 
     * corresponding attribute name (stored in <CODE>saAttributeNames</CODE> 
     * parameter). 
     * @param saAttributeTypes <CODE>String</CODE> array, whose values 
     * represent V5 attribute types. Position of array elements matters, since 
     * an attribute type needs to have the same array index as its 
     * corresponding attribute name (stored in <CODE>saAttributeNames</CODE> 
     * parameter).  Valid values are <CODE>double</CODE>, <CODE>integer</CODE> 
     * and <CODE>string</CODE>.  Any other type will result in a <CODE>string</CODE>. 
     * @return created <CODE>AttributeList</CODE> XML element (with its 
     * sub-elements), or <CODE>null</CODE>, if any of the argumets passed to 
     * this method have a <CODE>null</CODE> reference, or <CODE>null</CODE>, if
     * lengths of the parameter string arrays are not the same.
     * @see DNBIgpOlpUploadBaseClass
     * @since V5R20SP4
     */
    
    public Element createAttributeList(Element oParentElem, 
                                       String [] saAttributeNames,
                                       String [] saAttributeValues,
                                       String [] saAttributeTypes) {
        Element oAttributeListElem = null;
        
        if (oParentElem != null) {
            
            NodeList oAttributeListNodeList = 
                              oParentElem.getElementsByTagName("AttributeList");
            if (oAttributeListNodeList != null) {
                oAttributeListElem = (Element)oAttributeListNodeList.item(0);
            }
            
            if (oAttributeListElem == null) {
               oAttributeListElem = m_Document.createElement("AttributeList");
               oParentElem.appendChild(oAttributeListElem);
            }
            
            if (saAttributeNames != null && saAttributeValues != null && saAttributeTypes != null &&
                          saAttributeNames.length == saAttributeValues.length && saAttributeNames.length == saAttributeTypes.length) {

                for(int ii = 0; ii < saAttributeNames.length && 
                                          ii < saAttributeValues.length; ii++) {
                    String sAttributeName = saAttributeNames[ii];
                    String sAttributeValue =  saAttributeValues[ii];
                    String sAttributeType = saAttributeTypes[ii];
                    if (!sAttributeType.equals("string") && !sAttributeType.equals("double") && !sAttributeType.equals("integer"))
                    {
                        sAttributeType = "string";
                    }
                    
                    Element oAttributeElem =
                                          m_Document.createElement("Attribute");
                    oAttributeListElem.appendChild(oAttributeElem);
                    
                    Element oAttributeNameElem = 
                                      m_Document.createElement("AttributeName");
                    oAttributeElem.appendChild(oAttributeNameElem);
                    
                    Text oAttributeNameText = 
                                      m_Document.createTextNode(sAttributeName);
                    oAttributeNameElem.appendChild(oAttributeNameText);
                    
                    Element oAttributeValueElem = 
                                     m_Document.createElement("AttributeValue");
                    oAttributeElem.appendChild(oAttributeValueElem);
                    
                    Text oAttributeValueText = 
                                     m_Document.createTextNode(sAttributeValue);
                    oAttributeValueElem.appendChild(oAttributeValueText);
                    
                    Element oAttributeTypeElem = 
                        m_Document.createElement("AttributeType");
			        oAttributeElem.appendChild(oAttributeTypeElem);
			       
			        Text oAttributeTypeText = 
			                        m_Document.createTextNode(sAttributeType);
			        oAttributeTypeElem.appendChild(oAttributeTypeText);                    
                }
            }
            else {
                return null;
            }
        }
        
        return oAttributeListElem;
    }    

    /** Returns created <CODE>ParameterList</CODE> XML element. This element is
     * an authorised child of the <CODE>Resource</CODE>, element. All the sub-elements
     * of the returned element will be created, based on the argument values. Based 
     * on values of parameter string arrays, corresponding V5 parameters will be 
     * created for the specified V5 resource.
     * @param oParentElem parent element of the <CODE>ParameterList</CODE> 
     * element to be created
     * @param saParameterNames <CODE>String</CODE> array, whose values 
     * represent V5 parameter names. Position of array elements matters, since 
     * a parameter name needs to have the same array index as its 
     * corresponding parameter value (stored in <CODE>saParameterValues</CODE> 
     * parameter). 
     * @param saParameterValues <CODE>String</CODE> array, whose values 
     * represent V5 parameter values. Position of array elements matters, since 
     * a parameter value needs to have the same array index as its 
     * corresponding parameter name (stored in <CODE>saParameterNames</CODE> 
     * parameter). 
     * @return created <CODE>ParameterList</CODE> XML element (with its 
     * sub-elements), or <CODE>null</CODE>, if any of the argumets passed to 
     * this method have a <CODE>null</CODE> reference, or <CODE>null</CODE>, if
     * lengths of the parameter string arrays are not the same.
     * @see DNBIgpOlpUploadBaseClass
     * @since V5R21SP6, V5R22SP4, V5R23
     */
    public Element createParameterList(Element oParentElem, 
                                       String [] saParameterNames,
                                       String [] saParameterValues) {
        Element oParameterListElem = null;
        
        if (oParentElem != null) {            
            NodeList oParameterListNodeList = 
                              oParentElem.getElementsByTagName("ParameterList");
            if (oParameterListNodeList != null) {
                oParameterListElem = (Element)oParameterListNodeList.item(0);
            }
            
            if (oParameterListElem == null) {
                NodeList oActivityNodeList = 
                    oParentElem.getElementsByTagName("ActivityList");            	
               oParameterListElem = m_Document.createElement("ParameterList");
               if (oActivityNodeList != null)
               {
                   Element activityListElem = (Element)oActivityNodeList.item(0);
                   oParentElem.insertBefore(oParameterListElem, activityListElem);
               }
               else
               {
            	   oParentElem.appendChild(oParameterListElem);
               }
            }
            
            if (saParameterNames != null && saParameterValues != null && 
                          saParameterNames.length == saParameterValues.length) {

                for(int ii = 0; ii < saParameterNames.length && 
                                          ii < saParameterValues.length; ii++) {
                    String sParameterName = saParameterNames[ii];
                    String sParameterValue =  saParameterValues[ii];
                    
                    Element oParameterElem =
                                          m_Document.createElement("Parameter");
                    oParameterListElem.appendChild(oParameterElem);
                    
                    Element oParameterNameElem = 
                                      m_Document.createElement("ParameterName");
                    oParameterElem.appendChild(oParameterNameElem);
                    
                    Text oParameterNameText = 
                                      m_Document.createTextNode(sParameterName);
                    oParameterNameElem.appendChild(oParameterNameText);
                    
                    Element oParameterValueElem = 
                                     m_Document.createElement("ParameterValue");
                    oParameterElem.appendChild(oParameterValueElem);
                    
                    Text oParameterValueText = 
                                     m_Document.createTextNode(sParameterValue);
                    oParameterValueElem.appendChild(oParameterValueText);
                }
            }
            else {
                return null;
            }
        }
        
        return oParameterListElem;
    }
        
    /** Returns created <CODE>Activity</CODE> XML element as a child of the 
     * passed <CODE>ActivityListElem</CODE> element.
     * @param oActivityListElem parent XML element
     * @param sActivityName XML activity name
     * @param sActivityType XML activity type
     * @see DNBIgpOlpUploadBaseClass
     * @since V5R16
     */
    
    public Element createActivityHeader(Element oActivityListElem, 
                                         String sActivityName, 
                                         String sActivityType) {
        Element oActivityElem = null;
        
        if (oActivityListElem != null && sActivityName != null && 
                                                 (! sActivityName.equals(""))) {
            
            oActivityElem = m_Document.createElement("Activity");
            oActivityListElem.appendChild(oActivityElem);
            oActivityElem.setAttribute("ActivityType", sActivityType);

            Element oActivityNameElem = 
                                       m_Document.createElement("ActivityName");
            oActivityElem.appendChild(oActivityNameElem);
            Text oActivityNameText = m_Document.createTextNode(sActivityName);
            oActivityNameElem.appendChild(oActivityNameText);
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBRobotMotionActivity</CODE>.<P>Only <CODE>ActivityName</CODE> XML
     * element will be created. This is a starting point for creation of XML
     * representation of atomic Robot Motion V5 Activity. Since structure of 
     * Robot Motion V5 Activity is complex in nature, and requires numerous 
     * parameters to be configured properly, XML creation will be done through 
     * several, rather then just one (as for the other atomic activities), 
     * method calls.
     * @param oActivityListElem parent XML element, with tag name 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of the Robot Motion Activity, whose header will
     * be created
     * @return created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBRobotMotionActivity</CODE>, along with CODE>ActivityName</CODE> 
     * XML sub-element, or <CODE>null</CODE>, if any of the passed arguments is 
     * a <CODE>null</CODE> reference, or <CODE>null</CODE>, if the passed string
     * argument is an empty string. All the other sub-elements need to be 
     * explicitly created.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @see #createMotionAttributes(Element, String, String, String, String, 
     * DNBIgpOlpUploadEnumeratedTypes.MotionType) Next step for joint motion
     * @see #createMotionAttributes(Element, String, String, String, String, 
     * DNBIgpOlpUploadEnumeratedTypes.MotionType, 
     * DNBIgpOlpUploadEnumeratedTypes.OrientMode) Next step for linear or circular motion
     * @since V5R14
     */
    
    public Element createMotionActivityHeader(Element oActivityListElem, 
                                              String sActivityName) {
        
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "DNBRobotMotionActivity");
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>MotionAttributes</CODE> XML element. All the 
     * subelements, will be created as well.
     * <P>This is the second step in creation of XML
     * representation of atomic Robot Motion V5 Activity. Since structure of 
     * Robot Motion V5 Activity is complex in nature, and requires numerous 
     * parameters to be configured properly, XML creation will be done through 
     * several, rather then just one (as for the other atomic activities), 
     * method calls.<P>This method should be called in case of joint motion.
     * @param oMotionActivityElem parent element, with tag name <CODE>Activity
     * </CODE> and type <CODE>DNBRobotMotionActivity</CODE>, of the XML element
     * to be created.
     * @param sMotionProfile name of motion profile to be used by this activity
     * during simulation. This motion profile needs to have its XML declaration
     * in one of /OLPData/Resource/Controller/MotionProfileList/MotionProfile
     * XML elements.
     * @param sAccuracyProfile name of accuracy profile to be used by this 
     * activity during simulation. This accuracy profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/AccuracyProfileList/
     * AccuracyProfile XML elements.
     * @param sToolProfile name of tool profile to be used by this 
     * activity during simulation. This tool profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/ToolProfileList/
     * ToolProfile XML elements.
     * @param sObjectFrameProfile name of object frame profile to be used by 
     * this activity during simulation. This object frame profile needs to have 
     * its XML declaration in one of /OLPData/Resource/Controller/
     * ObjectFrameProfileList/ObjectFrameProfile XML elements.
     * @param eMotionType type of robot motion. For this method, only meaningful 
     * value is <CODE>MotionType.JOINT_MOTION</CODE>
     * @return created <CODE>MotionAttributes</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createMotionActivityHeader(Element, String) Previous Step In Robot 
     * Motion Activity Creation
     * @see #createTarget(Element, 
     * DNBIgpOlpUploadEnumeratedTypes.TargetType, boolean) Next Step In Robot 
     * Motion Activity Creation
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @since V5R14
     */
    
    public Element createMotionAttributes(Element oMotionActivityElem, 
                                          String sMotionProfile, 
                                          String sAccuracyProfile, 
                                          String sToolProfile, 
                                          String sObjectFrameProfile,
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                       MotionType eMotionType) {
       Element oMotionAttributesElem = null; 
                                              
       if (oMotionActivityElem != null && eMotionType != null &&
           sMotionProfile != null && (! sMotionProfile.equals("")) &&
           sAccuracyProfile != null && (! sAccuracyProfile.equals("")) &&
           sToolProfile != null && (! sToolProfile.equals("")) &&
           sObjectFrameProfile != null && (! sObjectFrameProfile.equals(""))) {
            oMotionAttributesElem = m_Document.
                                              createElement("MotionAttributes");
            oMotionActivityElem.appendChild(oMotionAttributesElem);
            
            Element oMotionProfileElem = 
                                      m_Document.createElement("MotionProfile");
            oMotionAttributesElem.appendChild(oMotionProfileElem);
            Text oMotionProfileText = m_Document.createTextNode(sMotionProfile);
            oMotionProfileElem.appendChild(oMotionProfileText);
        
            Element oToolProfileElem = m_Document.createElement("ToolProfile");
            oMotionAttributesElem.appendChild(oToolProfileElem);
            Text oToolProfileText = m_Document.createTextNode(sToolProfile);
            oToolProfileElem.appendChild(oToolProfileText);
            
            Element oAccuracyProfileElem = 
                                    m_Document.createElement("AccuracyProfile");
            oMotionAttributesElem.appendChild(oAccuracyProfileElem);
            Text oAccuracyProfileText = 
                                    m_Document.createTextNode(sAccuracyProfile);
            oAccuracyProfileElem.appendChild(oAccuracyProfileText);
            
            Element oObjectFrameProfileElem = 
                                 m_Document.createElement("ObjectFrameProfile");
            oMotionAttributesElem.appendChild(oObjectFrameProfileElem);
            Text oObjectFrameProfileText = 
                                 m_Document.createTextNode(sObjectFrameProfile);
            oObjectFrameProfileElem.appendChild(oObjectFrameProfileText);
        
            Element oMotionTypeElem = m_Document.createElement("MotionType");
            oMotionAttributesElem.appendChild(oMotionTypeElem);
            Text oMotionTypeText = 
                              m_Document.createTextNode(eMotionType.toString());
            oMotionTypeElem.appendChild(oMotionTypeText);
        }
        
        return oMotionAttributesElem;
    }
    
    /**Returns created <CODE>MotionAttributes</CODE> XML element. All the 
     * subelements, will be created as well.
     * <P>This is the second step in creation of XML
     * representation of atomic Robot Motion V5 Activity. Since structure of 
     * Robot Motion V5 Activity is complex in nature, and requires numerous 
     * parameters to be configured properly, XML creation will be done through 
     * several, rather then just one (as for the other atomic activities), 
     * method calls.<P>This method should be called in case of joint motion.
     * @param oMotionActivityElem parent element, with tag name <CODE>Activity
     * </CODE> and type <CODE>DNBRobotMotionActivity</CODE>, of the XML element
     * to be created.
     * @param sMotionProfile name of motion profile to be used by this activity
     * during simulation. This motion profile needs to have its XML declaration
     * in one of /OLPData/Resource/Controller/MotionProfileList/MotionProfile
     * XML elements.
     * @param sAccuracyProfile name of accuracy profile to be used by this 
     * activity during simulation. This accuracy profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/AccuracyProfileList/
     * AccuracyProfile XML elements.
     * @param sToolProfile name of tool profile to be used by this 
     * activity during simulation. This tool profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/ToolProfileList/
     * ToolProfile XML elements.
     * @param sObjectFrameProfile name of object frame profile to be used by 
     * this activity during simulation. This object frame profile needs to have 
     * its XML declaration in one of /OLPData/Resource/Controller/
     * ObjectFrameProfileList/ObjectFrameProfile XML elements.
     * @param sUserProfiles UserProfile types and instance names.
     * This <CODE>Hashtable</CODE> keys must contain UserProfile Types, 
     * while their corresponding elements must designate instance  
     * names. If specified UserProfiles do not exist in V5, they will be 
     * created, otherwise the existing UserProfile will be referenced.
     * @param eMotionType type of robot motion. For this method, only meaningful 
     * value is <CODE>MotionType.JOINT_MOTION</CODE>
     * @return created <CODE>MotionAttributes</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createMotionActivityHeader(Element, String) Previous Step In Robot 
     * Motion Activity Creation
     * @see #createTarget(Element, 
     * DNBIgpOlpUploadEnumeratedTypes.TargetType, boolean) Next Step In Robot 
     * Motion Activity Creation
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @since V5R15
     */
    
    public Element createMotionAttributes(Element oMotionActivityElem, 
                                          String sMotionProfile, 
                                          String sAccuracyProfile, 
                                          String sToolProfile, 
                                          String sObjectFrameProfile,
                                          Hashtable sUserProfiles,
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                       MotionType eMotionType) {
       Element oMotionAttributesElem = null; 
                                              
       if (oMotionActivityElem != null && eMotionType != null &&
           sMotionProfile != null && (! sMotionProfile.equals("")) &&
           sAccuracyProfile != null && (! sAccuracyProfile.equals("")) &&
           sToolProfile != null && (! sToolProfile.equals("")) &&
           sObjectFrameProfile != null && (! sObjectFrameProfile.equals(""))) {
            oMotionAttributesElem = m_Document.
                                              createElement("MotionAttributes");
            oMotionActivityElem.appendChild(oMotionAttributesElem);
            
            Element oMotionProfileElem = 
                                      m_Document.createElement("MotionProfile");
            oMotionAttributesElem.appendChild(oMotionProfileElem);
            Text oMotionProfileText = m_Document.createTextNode(sMotionProfile);
            oMotionProfileElem.appendChild(oMotionProfileText);
            
            Element oToolProfileElem = m_Document.createElement("ToolProfile");
            oMotionAttributesElem.appendChild(oToolProfileElem);
            Text oToolProfileText = m_Document.createTextNode(sToolProfile);
            oToolProfileElem.appendChild(oToolProfileText);
            
            Element oAccuracyProfileElem = 
                                    m_Document.createElement("AccuracyProfile");
            oMotionAttributesElem.appendChild(oAccuracyProfileElem);
            Text oAccuracyProfileText = 
                                    m_Document.createTextNode(sAccuracyProfile);
            oAccuracyProfileElem.appendChild(oAccuracyProfileText);
            
            Element oObjectFrameProfileElem = 
                                 m_Document.createElement("ObjectFrameProfile");
            oMotionAttributesElem.appendChild(oObjectFrameProfileElem);
            Text oObjectFrameProfileText = 
                                 m_Document.createTextNode(sObjectFrameProfile);
            oObjectFrameProfileElem.appendChild(oObjectFrameProfileText);

            if (sUserProfiles != null) {
                Enumeration eUserProfileTypes = sUserProfiles.keys();
                Enumeration eUserProfileNames = sUserProfiles.elements();

                while(eUserProfileTypes.hasMoreElements() == true && 
                      eUserProfileNames.hasMoreElements() == true) {
                   Element oUserProfileElement = m_Document.createElement("UserProfile");
                   oMotionAttributesElem.appendChild(oUserProfileElement);

                   String sType = eUserProfileTypes.nextElement().toString();


                   oUserProfileElement.setAttribute("Type", sType);
                   Text oUserProfileInstanceText = m_Document.createTextNode(eUserProfileNames.nextElement().toString());
                   oUserProfileElement.appendChild(oUserProfileInstanceText);           
                }        
            }
        
            Element oMotionTypeElem = m_Document.createElement("MotionType");
            oMotionAttributesElem.appendChild(oMotionTypeElem);
            Text oMotionTypeText = 
                              m_Document.createTextNode(eMotionType.toString());
            oMotionTypeElem.appendChild(oMotionTypeText);
        }
        
        return oMotionAttributesElem;
    }
    
    /**Returns created <CODE>MotionAttributes</CODE> XML element. All the 
     * subelements, will be created as well.
     * <P>This is the second step in creation of XML
     * representation of atomic Robot Motion V5 Activity. Since structure of 
     * Robot Motion V5 Activity is complex in nature, and requires numerous 
     * parameters to be configured properly, XML creation will be done through 
     * several, rather then just one (as for the other atomic activities), 
     * method calls.<P>This method should be called in case of linear or
     * circular motion. 
     * @param oMotionActivityElem parent element, with tag name <CODE>Activity </CODE> 
     * and type <CODE>DNBRobotMotionActivity</CODE>, of the XML element to be created.
     * @param sMotionProfile name of motion profile to be used by this activity
     * during simulation. This motion profile needs to have its XML declaration
     * in one of /OLPData/Resource/Controller/MotionProfileList/MotionProfile
     * XML elements.
     * @param sAccuracyProfile name of accuracy profile to be used by this 
     * activity during simulation. This accuracy profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/AccuracyProfileList/
     * AccuracyProfile XML elements.
     * @param sToolProfile name of tool profile to be used by this 
     * activity during simulation. This tool profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/ToolProfileList/
     * ToolProfile XML elements.
     * @param sObjectFrameProfile name of object frame profile to be used by 
     * this activity during simulation. This object frame profile needs to have 
     * its XML declaration in one of /OLPData/Resource/Controller/
     * ObjectFrameProfileList/ObjectFrameProfile XML elements.
     * @param eMotionType type of robot motion. For this method, only meaningful 
     * value is <CODE>MotionType.LINEAR_MOTION</CODE>
     * @param eOrientMode type of orientation inetrpolation. Valid values are:
     * <CODE>OrientMode.ONE_AXIS</CODE>, <CODE>OrientMode.TWO_AXIS</CODE>,
     * <CODE>OrientMode.THREE_AXIS</CODE>, and <CODE>OrientMode.WRIST/CODE>.
     * @return created <CODE>MotionAttributes</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createMotionActivityHeader(Element, String) Previous Step In Robot 
     * Motion Activity Creation
     * @see #createTarget(Element, 
     * DNBIgpOlpUploadEnumeratedTypes.TargetType, boolean) Next Step In Robot 
     * Motion Activity Creation
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @see DNBIgpOlpUploadEnumeratedTypes.OrientMode
     * @since V5R14
     */
    
    public Element createMotionAttributes(Element oMotionActivityElem, 
                                          String sMotionProfile, 
                                          String sAccuracyProfile, 
                                          String sToolProfile, 
                                          String sObjectFrameProfile, 
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                         MotionType eMotionType, 
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                       OrientMode eOrientMode) {
        Element oMotionAttributesElem = createMotionAttributes(
                                                            oMotionActivityElem, 
                                                            sMotionProfile, 
                                                            sAccuracyProfile, 
                                                            sToolProfile, 
                                                            sObjectFrameProfile, 
                                                            eMotionType);
        
        if (oMotionAttributesElem != null && eOrientMode != null) {
            Element oOrientModeElem = m_Document.createElement("OrientMode");
            oMotionAttributesElem.appendChild(oOrientModeElem);
            Text oOrientModeText = m_Document.createTextNode(
                                                        eOrientMode.toString());
            oOrientModeElem.appendChild(oOrientModeText);
        }
        
        return oMotionAttributesElem;
    }

       /**Returns created <CODE>MotionAttributes</CODE> XML element. All the 
     * subelements, will be created as well.
     * <P>This is the second step in creation of XML
     * representation of atomic Robot Motion V5 Activity. Since structure of 
     * Robot Motion V5 Activity is complex in nature, and requires numerous 
     * parameters to be configured properly, XML creation will be done through 
     * several, rather then just one (as for the other atomic activities), 
     * method calls.<P>This method should be called in case of linear or circular motion.
     * @param oMotionActivityElem parent element, with tag name <CODE>Activity
     * </CODE> and type <CODE>DNBRobotMotionActivity</CODE>, of the XML element
     * to be created.
     * @param sMotionProfile name of motion profile to be used by this activity
     * during simulation. This motion profile needs to have its XML declaration
     * in one of /OLPData/Resource/Controller/MotionProfileList/MotionProfile
     * XML elements.
     * @param sAccuracyProfile name of accuracy profile to be used by this 
     * activity during simulation. This accuracy profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/AccuracyProfileList/
     * AccuracyProfile XML elements.
     * @param sToolProfile name of tool profile to be used by this 
     * activity during simulation. This tool profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/ToolProfileList/
     * ToolProfile XML elements.
     * @param sObjectFrameProfile name of object frame profile to be used by 
     * this activity during simulation. This object frame profile needs to have 
     * its XML declaration in one of /OLPData/Resource/Controller/
     * ObjectFrameProfileList/ObjectFrameProfile XML elements.
     * @param sUserProfiles UserProfile types and instance names.
     * This <CODE>Hashtable</CODE> keys must contain UserProfile Types, 
     * while their corresponding elements must designate instance  
     * names. If specified UserProfiles do not exist in V5, they will be 
     * created, otherwise the existing UserProfile will be referenced.
     * @param eMotionType type of robot motion. For this method, only meaningful 
     * values are <CODE>MotionType.LINEAR_MOTION</CODE>, <CODE>MotionType.CIRCULARVIA_MOTION</CODE>,
     * or <CODE>MotionType.CIRCULAR_MOTION</CODE>.
     * @param eOrientMode type of orientation inetrpolation. Valid values are:
     * <CODE>OrientMode.ONE_AXIS</CODE>, <CODE>OrientMode.TWO<CODE>MotionType.
     * <CODE>OrientMode.THREE_AXIS</CODE>, and <CODE>OrientMode.WRIST/CODE>.
     * @return created <CODE>MotionAttributes</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createMotionActivityHeader(Element, String) Previous Step In Robot 
     * Motion Activity Creation
     * @see #createTarget(Element, 
     * DNBIgpOlpUploadEnumeratedTypes.TargetType, boolean) Next Step In Robot 
     * Motion Activity Creation
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @see DNBIgpOlpUploadEnumeratedTypes.OrientMode
     * @since V5R15
     */ 
    public Element createMotionAttributes(Element oMotionActivityElem, 
                                          String sMotionProfile, 
                                          String sAccuracyProfile, 
                                          String sToolProfile, 
                                          String sObjectFrameProfile,
                                          Hashtable sUserProfiles,
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                         MotionType eMotionType, 
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                       OrientMode eOrientMode) {
        Element oMotionAttributesElem = createMotionAttributes(
                                                            oMotionActivityElem, 
                                                            sMotionProfile, 
                                                            sAccuracyProfile, 
                                                            sToolProfile, 
                                                            sObjectFrameProfile, 
                                                            sUserProfiles,
                                                            eMotionType);

        if (oMotionAttributesElem != null && eOrientMode != null) {
            Element oOrientModeElem = m_Document.createElement("OrientMode");
            oMotionAttributesElem.appendChild(oOrientModeElem);
            Text oOrientModeText = m_Document.createTextNode(
                                                        eOrientMode.toString());
            oOrientModeElem.appendChild(oOrientModeText);
        }
        return oMotionAttributesElem;
    }
    
    /**Returns created <CODE>MotionAttributes</CODE> XML element. All the 
     * subelements, will be created as well.
     * <P>This is the second step in creation of XML
     * representation of atomic Robot Motion V5 Activity. Since structure of 
     * Robot Motion V5 Activity is complex in nature, and requires numerous 
     * parameters to be configured properly, XML creation will be done through 
     * several, rather then just one (as for the other atomic activities), 
     * method calls.<P>This method should be called in case of linear or
     * circular motion. 
     * @param oMotionActivityElem parent element, with tag name <CODE>Activity </CODE> 
     * and type <CODE>DNBRobotMotionActivity</CODE>, of the XML element to be created.
     * @param sMotionProfile name of motion profile to be used by this activity
     * during simulation. This motion profile needs to have its XML declaration
     * in one of /OLPData/Resource/Controller/MotionProfileList/MotionProfile
     * XML elements.
     * @param sAccuracyProfile name of accuracy profile to be used by this 
     * activity during simulation. This accuracy profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/AccuracyProfileList/
     * AccuracyProfile XML elements.
     * @param sToolProfile name of tool profile to be used by this 
     * activity during simulation. This tool profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/ToolProfileList/
     * ToolProfile XML elements.
     * @param sObjectFrameProfile name of object frame profile to be used by 
     * this activity during simulation. This object frame profile needs to have 
     * its XML declaration in one of /OLPData/Resource/Controller/
     * ObjectFrameProfileList/ObjectFrameProfile XML elements.
     * @param eMotionType type of robot motion. For this method, only meaningful 
     * value is <CODE>MotionType.LINEAR_MOTION</CODE>
     * @param eOrientMode type of orientation inetrpolation. Valid values are:
     * <CODE>OrientMode.ONE_AXIS</CODE>, <CODE>OrientMode.TWO_AXIS</CODE>,
     * <CODE>OrientMode.THREE_AXIS</CODE>, and <CODE>OrientMode.WRIST/CODE>.
     * @param eInterpolationMode type of config/turn interpolation. Valid values are:
     * <CODE>InterpolationMode.NEARMODE</CODE>, <CODE>InterpolationMode.KEEPCFG_KEEPTURN</CODE>,
     * <CODE>InterpolationMode.KEEPCFG_SETTURN</CODE>, <CODE>InterpolationMode.SETCFG_KEEPTURN</CODE> 
     * <CODE>InterpolationMode.SETCFG_SETTURN</CODE> and <CODE>InterpolationMode.NEARSETCFG</CODE>. 
     * @return created <CODE>MotionAttributes</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createMotionActivityHeader(Element, String) Previous Step In Robot 
     * Motion Activity Creation
     * @see #createTarget(Element, 
     * DNBIgpOlpUploadEnumeratedTypes.TargetType, boolean) Next Step In Robot 
     * Motion Activity Creation
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @see DNBIgpOlpUploadEnumeratedTypes.OrientMode
     * @see DNBIgpOlpUploadEnumeratedTypes.InterpolationMode
     * @since V5R20SP4
     */
    
    public Element createMotionAttributes(Element oMotionActivityElem, 
                                          String sMotionProfile, 
                                          String sAccuracyProfile, 
                                          String sToolProfile, 
                                          String sObjectFrameProfile, 
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                         MotionType eMotionType, 
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                       OrientMode eOrientMode,
                                                       DNBIgpOlpUploadEnumeratedTypes.InterpolationMode eInterpolationMode) {
        Element oMotionAttributesElem = createMotionAttributes(
                                                            oMotionActivityElem, 
                                                            sMotionProfile, 
                                                            sAccuracyProfile, 
                                                            sToolProfile, 
                                                            sObjectFrameProfile, 
                                                            eMotionType,
                                                            eOrientMode);
        
        if (oMotionAttributesElem != null && eInterpolationMode != null) {
            Element oInterpolationModeElem = m_Document.createElement("InterpolationMode");
            oMotionAttributesElem.appendChild(oInterpolationModeElem);
            Text oInterpolationModeText = m_Document.createTextNode(
                                                        eInterpolationMode.toString());
            oInterpolationModeElem.appendChild(oInterpolationModeText);
        }
        
        return oMotionAttributesElem;
    }

     /**Returns created <CODE>MotionAttributes</CODE> XML element. All the 
     * subelements, will be created as well.
     * <P>This is the second step in creation of XML
     * representation of atomic Robot Motion V5 Activity. Since structure of 
     * Robot Motion V5 Activity is complex in nature, and requires numerous 
     * parameters to be configured properly, XML creation will be done through 
     * several, rather then just one (as for the other atomic activities), 
     * method calls.<P>This method should be called in case of linear or circular motion.
     * @param oMotionActivityElem parent element, with tag name <CODE>Activity
     * </CODE> and type <CODE>DNBRobotMotionActivity</CODE>, of the XML element
     * to be created.
     * @param sMotionProfile name of motion profile to be used by this activity
     * during simulation. This motion profile needs to have its XML declaration
     * in one of /OLPData/Resource/Controller/MotionProfileList/MotionProfile
     * XML elements.
     * @param sAccuracyProfile name of accuracy profile to be used by this 
     * activity during simulation. This accuracy profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/AccuracyProfileList/
     * AccuracyProfile XML elements.
     * @param sToolProfile name of tool profile to be used by this 
     * activity during simulation. This tool profile needs to have its XML 
     * declaration in one of /OLPData/Resource/Controller/ToolProfileList/
     * ToolProfile XML elements.
     * @param sObjectFrameProfile name of object frame profile to be used by 
     * this activity during simulation. This object frame profile needs to have 
     * its XML declaration in one of /OLPData/Resource/Controller/
     * ObjectFrameProfileList/ObjectFrameProfile XML elements.
     * @param sUserProfiles UserProfile types and instance names.
     * This <CODE>Hashtable</CODE> keys must contain UserProfile Types, 
     * while their corresponding elements must designate instance  
     * names. If specified UserProfiles do not exist in V5, they will be 
     * created, otherwise the existing UserProfile will be referenced.
     * @param eMotionType type of robot motion. For this method, only meaningful 
     * values are <CODE>MotionType.LINEAR_MOTION</CODE>, <CODE>MotionType.CIRCULARVIA_MOTION</CODE>,
     * or <CODE>MotionType.CIRCULAR_MOTION</CODE>.
     * @param eOrientMode type of orientation inetrpolation. Valid values are:
     * <CODE>OrientMode.ONE_AXIS</CODE>, <CODE>OrientMode.TWO<CODE>MotionType.
     * <CODE>OrientMode.THREE_AXIS</CODE>, and <CODE>OrientMode.WRIST/CODE>.
     * @param eInterpolationMode type of config/turn interpolation. Valid values are:
     * <CODE>InterpolationMode.NEARMODE</CODE>, <CODE>InterpolationMode.KEEPCFG_KEEPTURN</CODE>,
     * <CODE>InterpolationMode.KEEPCFG_SETTURN</CODE>, <CODE>InterpolationMode.SETCFG_KEEPTURN</CODE> 
     * <CODE>InterpolationMode.SETCFG_SETTURN</CODE> and <CODE>InterpolationMode.NEARSETCFG</CODE>. 
     * @return created <CODE>MotionAttributes</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference, 
     * or <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createMotionActivityHeader(Element, String) Previous Step In Robot 
     * Motion Activity Creation
     * @see #createTarget(Element, 
     * DNBIgpOlpUploadEnumeratedTypes.TargetType, boolean) Next Step In Robot 
     * Motion Activity Creation
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @see DNBIgpOlpUploadEnumeratedTypes.OrientMode
     * @see DNBIgpOlpUploadEnumeratedTypes.InterpolationMode
     * @since V5R20SP4
     */
    public Element createMotionAttributes(Element oMotionActivityElem, 
                                          String sMotionProfile, 
                                          String sAccuracyProfile, 
                                          String sToolProfile, 
                                          String sObjectFrameProfile,
                                          Hashtable sUserProfiles,
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                         MotionType eMotionType, 
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                       OrientMode eOrientMode,
                                          DNBIgpOlpUploadEnumeratedTypes.InterpolationMode eInterpolationMode) {
        Element oMotionAttributesElem = createMotionAttributes(
                                                            oMotionActivityElem, 
                                                            sMotionProfile, 
                                                            sAccuracyProfile, 
                                                            sToolProfile, 
                                                            sObjectFrameProfile, 
                                                            sUserProfiles,
                                                            eMotionType,
                                                            eOrientMode);

        if (oMotionAttributesElem != null && eInterpolationMode != null) {
            Element oInterpolationModeElem = m_Document.createElement("InterpolationMode");
            oMotionAttributesElem.appendChild(oInterpolationModeElem);
            Text oInterpolationModeText = m_Document.createTextNode(
                                                        eInterpolationMode.toString());
            oInterpolationModeElem.appendChild(oInterpolationModeText);
        }
        return oMotionAttributesElem;
    }
    /**Returns created <CODE>Target</CODE> XML element. <P> No sub-elements will 
     * be created during this method execution. They need to be explicitly 
     * created through other method invocation. This is the third step in 
     * creation of XML representation of atomic Robot Motion V5 Activity. Since 
     * structure of Robot Motion V5 Activity is complex in nature, and requires 
     * numerous parameters to be configured properly, XML creation will be done 
     * through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oMotionActivityElem parent element, with tag name <CODE>Activity
     * </CODE> and type <CODE>DNBRobotMotionActivity</CODE>, of the XML element
     * to be created.
     * @param eTargetType coordinate space in which this target is defined. 
     * Valid values are: <CODE>TargetType.JOINT</CODE> and <CODE>
     * TargetType.CARTESIAN</CODE>.
     * @param bIsViaPoint boolean that specifies whether this target is a V5 via 
     * point. Value <CODE>true</CODE> denotes a V5 via point, while value <CODE>
     * false </CODE> sets a V5 non-via point.
     * @return created <CODE>Target</CODE> XML element (none of the sub-elements
     * will be created), or <CODE>null</CODE>, if any of the passed arguments is 
     * a <CODE>null</CODE> reference. All the other sub-elements need to be 
     * explicitly created.
     * @see #createMotionAttributes(Element, String, String, String, String, 
     * DNBIgpOlpUploadEnumeratedTypes.MotionType) Previous Step In Robot Motion 
     * Activity Creation (joint moves)
     * @see #createMotionAttributes(Element, String, String, String, String, 
     * DNBIgpOlpUploadEnumeratedTypes.MotionType,
     * DNBIgpOlpUploadEnumeratedTypes.OrientMode) Previous Step In Robot Motion 
     * Activity Creation (linear or circular moves)
     * @see #createCartesianTarget(Element, double [], String) Next Step In 
     * Robot Motion Activity Creation (Cartesian target)
     * @see #createJointTarget(Element, ArrayList []) Next Step In Robot 
     * Motion Activity Creation (joint target)
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.TargetType
     * @since V5R14
     */
    
    public Element createTarget(Element oMotionActivityElem, 
                                DNBIgpOlpUploadEnumeratedTypes.
                                                         TargetType eTargetType, 
                                boolean bIsViaPoint) {
        Element oTargetElem = null;
        
        if (oMotionActivityElem != null && eTargetType != null) {
            oTargetElem = m_Document.createElement("Target");
            oMotionActivityElem.appendChild(oTargetElem);
            
            oTargetElem.setAttribute("Default", eTargetType.toString());
            
            if (bIsViaPoint == true) {
                oTargetElem.setAttribute("ViaPoint", "true");
            }
            else {
                oTargetElem.setAttribute("ViaPoint", "false");
            }
        }
        
        return oTargetElem;
    }
    
    /**Returns created <CODE>CartesianTarget</CODE> XML element. <P> Only <CODE>
     * Position</CODE> and  <CODE>Orientation</CODE> sub-elements will 
     * be created during this method execution. All the other sub-elements need 
     * to be explicitly created through other method invocation. This is the 
     * fourth step, only required if this target's coordinate space is Cartesian
     * , in creation of XML representation of atomic Robot Motion V5 Activity. 
     * Since structure of Robot Motion V5 Activity is complex in nature, and 
     * requires numerous parameters to be configured properly, XML creation will 
     * be done through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oTargetElem parent element, with tag name <CODE>Target</CODE>, of 
     * the XML element to be created.
     * @param daLocation target's position and orientation with respect to 
     * object frame set for this robot motion, or if object frame's offset is 
     * zero, with respect to the robot's base frame. This array must contain 6 
     * elements: X, Y, Z, Yaw, Pitch, and Roll respectively.
     * @param sConfigName string representation of robot's configuration for 
     * this target. V5 uses default configuration names for some robot models, 
     * although native robot language configuration strings are different. This 
     * needs to be checked before uploader creation.
     * @return created <CODE>CartesianTarget</CODE> XML element (Only <CODE>
     * Position</CODE> and  <CODE>Orientation</CODE> sub-elements will be 
     * created), or <CODE>null</CODE>, if any of the passed arguments is 
     * a <CODE>null</CODE> reference, or <CODE>null</CODE>, if <CODE>sConfigName
     * </CODE> argument is an empty string. All the other sub-elements need to 
     * be explicitly created.
     * @see #createTarget(Element, DNBIgpOlpUploadEnumeratedTypes.TargetType, 
     * boolean) Previous Step In Robot Motion Activity Creation
     * @see #createTurnNumbers(Element, int []) Next step, if turn numbers exist
     * @see #createTurnSigns(Element, DNBIgpOlpUploadEnumeratedTypes.TurnSign[])
     * Next step, if turn signs exist
     * @see #createTag(Element, String) Next step, if tag is to be created for 
     * this target
     * @see #createJointTarget(Element, ArrayList []) Next step, if this target 
     * has an auxiliary target defined
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createCartesianTarget(Element oTargetElem, 
                                         double [] daLocation, 
                                         String sConfigName) {
        Element oCartesianTargetElem = null; 
        
        if (oTargetElem != null && daLocation != null && 
                        daLocation.length == NUMBER_OF_POS_AND_ORI_PARAMETERS &&
                            sConfigName != null && (! sConfigName.equals(""))) {
                                
            oCartesianTargetElem = m_Document.createElement("CartesianTarget");
            oTargetElem.appendChild(oCartesianTargetElem);
            
            Element oPositionElem = m_Document.createElement("Position");
            oCartesianTargetElem.appendChild(oPositionElem);
            
            Element oOrientationElem = m_Document.createElement("Orientation");
            oCartesianTargetElem.appendChild(oOrientationElem);
            
            oPositionElem.setAttribute("X", new Double(
                                                     daLocation[0]).toString());
            oPositionElem.setAttribute("Y", new Double(
                                                     daLocation[1]).toString());
            oPositionElem.setAttribute("Z", new Double(
                                                     daLocation[2]).toString());

            oOrientationElem.setAttribute("Yaw", new Double(
                                                     daLocation[3]).toString());
            oOrientationElem.setAttribute("Pitch", new Double(
                                                     daLocation[4]).toString());
            oOrientationElem.setAttribute("Roll", new Double(
                                                     daLocation[5]).toString());
            
            Element oConfigElem = m_Document.createElement("Config");
            oCartesianTargetElem.appendChild(oConfigElem);
            
            oConfigElem.setAttribute("Name", sConfigName);
        }
        
        return oCartesianTargetElem;
    }
    
    /**Returns created <CODE>TurnNumber</CODE> XML element. <P> All the XML 
     * sub-elements will be created during this method execution. This is the 
     * fifth step, only required if the uploading resource supports turn numbers
     * , in creation of XML representation of atomic Robot Motion V5 Activity. 
     * Since structure of Robot Motion V5 Activity is complex in nature, and 
     * requires numerous parameters to be configured properly, XML creation will 
     * be done through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oCartesianTargetElem parent element, with tag name <CODE>
     * CartesianTarget</CODE>, of the XML element to be created.
     * @param iaTurnNumbers array of turn number values. V5 requires four turn
     * numbers to be specified for all the robot models that support turn 
     * numbers (for DOFs: 1, 4, 5, and 6), no matter if the real robot would 
     * support fewer turn numbers. For the values of all the redundant turn 
     * numbers, value 0 should be used. This array must contain 4 elements, turn 
     * number values for DOFs: 1, 4, 5, and 6 respectively.
     * @return created <CODE>TurnNumber</CODE> XML element (all the sub-elements
     * will be created), or <CODE>null</CODE>, if any of the passed arguments is 
     * a <CODE>null</CODE> reference.
     * @see #createCartesianTarget(Element, double [], String) Previous Step In
     * Robot Motion Activity Creation
     * @see #createTag(Element, String) Next step, if tag is to be created for 
     * this target
     * @see #createJointTarget(Element, ArrayList []) Next step, if this target 
     * has an auxiliary target defined
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.TurnSign
     * @since V5R14
     */
    
    public Element createTurnNumbers(Element oCartesianTargetElem, 
                                     int [] iaTurnNumbers) {
        Element oTurnNumberElem = null;    
        
        if (oCartesianTargetElem != null && iaTurnNumbers != null && 
                                   iaTurnNumbers.length == TOTAL_TURN_NUMBERS) {
            String sNumber = "";
            
            oTurnNumberElem = m_Document.createElement("TurnNumber");
            oCartesianTargetElem.appendChild(oTurnNumberElem);
            
            for (int ii = 0; ii < iaTurnNumbers.length; ii++) {
                switch (ii) {
                    case 0: sNumber = "1";
                            break;
                    case 1: sNumber = "4";
                            break;
                    case 2: sNumber = "5";
                            break;
                    case 3: sNumber = "6";
                            break;
                    default:
                            return null;
                }
                
                Element oTNJointElem = m_Document.createElement("TNJoint");
                oTurnNumberElem.appendChild(oTNJointElem);
                
                Integer sTurnNumberValue = new Integer(iaTurnNumbers[ii]);
                
                oTNJointElem.setAttribute("Number", sNumber);
                oTNJointElem.setAttribute("Value", sTurnNumberValue.toString());
            }
        }
        
        return oTurnNumberElem;
    }
    
    /**Returns created <CODE>TurnSign</CODE> XML element. <P> All the XML 
     * sub-elements will be created during this method execution. This is the 
     * fifth step, only required if the uploading resource supports turn signs
     * , in creation of XML representation of atomic Robot Motion V5 Activity. 
     * Since structure of Robot Motion V5 Activity is complex in nature, and 
     * requires numerous parameters to be configured properly, XML creation will 
     * be done through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oCartesianTargetElem parent element, with tag name <CODE>
     * CartesianTarget</CODE>, of the XML element to be created.
     * @param oaTurnSigns array of turn sign values. Valid turn sign values are:
     * <CODE>TurnSign.MINUS</CODE> and <CODE>TurnSign.PLUS</CODE>. V5 requires 
     * four turn signs to be specified for all the robot models that support 
     * turn signs (for DOFs: 1, 4, 5, and 6), no matter if the real robot would 
     * support fewer turn numbers. For the values of all the redundant turn 
     * signs, value <CODE>TurnSign.PLUS</CODE> should be used. This array must 
     * contain 4 elements, turn number values for DOFs: 1, 4, 5, and 6 
     * respectively.
     * @return created <CODE>TurnSign</CODE> XML element (all the sub-elements
     * will be created), or <CODE>null</CODE>, if any of the passed arguments is 
     * a <CODE>null</CODE> reference.
     * @see #createCartesianTarget(Element, double [], String) Previous Step In
     * Robot Motion Activity Creation
     * @see #createTag(Element, String) Next step, if tag is to be created for 
     * this target
     * @see #createJointTarget(Element, ArrayList []) Next step, if this target 
     * has an auxiliary target defined
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.TurnSign
     * @since V5R14
     */
  
    public Element createTurnSigns(Element oCartesianTargetElem, 
                                   DNBIgpOlpUploadEnumeratedTypes.
                                                      TurnSign [] oaTurnSigns) {
        Element oTurnSignElem = null;    
        
        if (oCartesianTargetElem != null && oaTurnSigns != null && 
                                       oaTurnSigns.length == TOTAL_TURN_SIGNS) {
            String sSign = "";
            
            oTurnSignElem = m_Document.createElement("TurnSign");
            oCartesianTargetElem.appendChild(oTurnSignElem);
            
            for (int ii = 0; ii < oaTurnSigns.length; ii++) {
              switch (ii) {
                    case 0: sSign = "1";
                            break;
                    case 1: sSign = "4";
                            break;
                    case 2: sSign = "5";
                            break;
                    case 3: sSign = "6";
                            break;
                    default:
                            return null;
                }
                
                Element oTSJointElem = m_Document.createElement("TSJoint");
                oTurnSignElem.appendChild(oTSJointElem);
                
                oTSJointElem.setAttribute("Number", sSign);
                oTSJointElem.setAttribute("Value", oaTurnSigns.toString());
            }
        }
        
        return oTurnSignElem;
    }
    
    /**Returns created <CODE>Tag</CODE> XML element. <P>This element has no 
     * child elements. This is the sixth step (if either turn signs or turn 
     * numbers are supported, otherwise it is the fifth step), only required if 
     * the Cartesian target needs to be represented with a V5 tag, in creation 
     * of XML representation of atomic Robot Motion V5 Activity. 
     * Since structure of Robot Motion V5 Activity is complex in nature, and 
     * requires numerous parameters to be configured properly, XML creation will 
     * be done through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oCartesianTargetElem parent element, with tag name <CODE>
     * CartesianTarget</CODE>, of the XML element to be created.
     * @param sTagName name of tag to be created in V5
     * @return created <CODE>Tag</CODE> XML element, or <CODE>null</CODE>, if 
     * any of the passed arguments is a <CODE>null</CODE> reference, or <CODE>
     * null</CODE>, if <CODE>sTagName</CODE> argument is an empty string.
     * @see #createCartesianTarget(Element, double [], String) Previous step (if
     * turn signs and numbers are not supported)
     * @see #createTurnNumbers(Element, int []) Previous step (if turn numbers 
     * are supported)
     * @see #createTurnSigns(Element, DNBIgpOlpUploadEnumeratedTypes.TurnSign[])
     * Previous step (if turn signs are supported)
     * @see #createJointTarget(Element, ArrayList []) Next step, if this target 
     * has an auxiliary target defined
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createTag(Element oCartesianTargetElem, String sTagName) {
        Element oTagElem = null;
        
        if (oCartesianTargetElem != null && sTagName != null && 
                                                   (! sTagName.equals(""))) {
            
            oTagElem = m_Document.createElement("Tag");
            oCartesianTargetElem.appendChild(oTagElem);
            
            Text oTagText = m_Document.createTextNode(sTagName);
            oTagElem.appendChild(oTagText);
            if (m_TrackType != DNBIgpOlpUploadEnumeratedTypes.TrackType.TRACK_OFF)
            {
            	oTagElem.setAttribute("TrackTag", "true");
            }
            else
            {
            	oTagElem.setAttribute("TrackTag", "false");
            }            
        }
        
        return oTagElem;
    }
    
    /**Returns created <CODE>Tag</CODE> XML element, and sets its tag group name
     * and part, which is attached to, name attributes. <P>This element has no 
     * child elements. This is the sixth step (if either turn signs or turn 
     * numbers are supported, otherwise it is the fifth step), only required if 
     * the Cartesian target needs to be represented with a V5 tag, in creation 
     * of XML representation of atomic Robot Motion V5 Activity. 
     * Since structure of Robot Motion V5 Activity is complex in nature, and 
     * requires numerous parameters to be configured properly, XML creation will 
     * be done through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oCartesianTargetElem parent element, with tag name <CODE>
     * CartesianTarget</CODE>, of the XML element to be created.
     * @param sTagName name of tag to be created in V5
     * @param sTagGroupName name of the V5 tag group, where this tag will be 
     * placed. This name needs to reference an existing tag group in the V5 
     * world, otherwise the tag will be placed in a default tag group located in 
     * V5's Resource context.
     * @param sPartName name of the V5 Product or Part, to which the above tag 
     * group is attached. This name needs to be a valid instance name of a V5 
     * Product or Part, which exists in either Product or Resource V5 context. 
     * One part can have multiple tag groups attached, so the above parameter is 
     * mandatory.
     * @return created <CODE>Tag</CODE> XML element, or <CODE>null</CODE>, if 
     * any of the passed arguments is a <CODE>null</CODE> reference, or <CODE>
     * null</CODE>, if <CODE>sTagName</CODE> argument is an empty string.
     * @see #createCartesianTarget(Element, double [], String) Previous step (if
     * turn signs and numbers are not supported)
     * @see #createTurnNumbers(Element, int []) Previous step (if turn numbers 
     * are supported)
     * @see #createTurnSigns(Element, DNBIgpOlpUploadEnumeratedTypes.TurnSign[])
     * Previous step (if turn signs are supported)
     * @see #createJointTarget(Element, ArrayList []) Next step, if this target 
     * has an auxiliary target defined
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
            
    public Element createTag(Element oCartesianTargetElem, 
                             String sTagName, 
                             String sTagGroupName, 
                             String sPartName) {
        Element oTagElem = createTag(oCartesianTargetElem, sTagName);
        
        if (oTagElem != null && 
            sTagGroupName != null && !sTagGroupName.equals("") &&
            sPartName != null) {
            oTagElem.setAttribute("TagGroup", sTagGroupName);
            if (!sPartName.equals(""))
                oTagElem.setAttribute("AttachedToPart", sPartName);
        }
        
        return oTagElem;
    }
    
    /**Returns created <CODE>JointTarget</CODE> XML element (given the home 
     * position name), including all of its sub-elements.<P>This is either the 
     * fourth step (if target's coordinate space is Joint), or the last step 
     * (if only auxiliary target needs to added to already created Cartesian 
     * target), in creation of XML representation of atomic Robot Motion V5 
     * Activity. 
     * Since structure of Robot Motion V5 Activity is complex in nature, and 
     * requires numerous parameters to be configured properly, XML creation will 
     * be done through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oTargetElem parent element, with tag name <CODE>Target</CODE>, of 
     * the XML element to be created.
     * @param sHomeName name of target home position. V5 resource parameters may
     * be used to map home positions defined in a robot program with the home 
     * position names defined for the robot model in V5.
     * @return created <CODE>JointTarget</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference,
     * or <CODE>null</CODE>, if <CODE>sHomeName</CODE> argument is an empty 
     * string.
     * @see #createTarget(Element, DNBIgpOlpUploadEnumeratedTypes.TargetType, 
     * boolean) Previous Step (if target's coordinate space is Joint)
     * @see #createCartesianTarget(Element, double [], String) Previous step (if
     * only aux values are needed, turns are not supported, and no tag)
     * @see #createTurnNumbers(Element, int []) Previous step (if only aux 
     * values are needed, turn numbers are supported, and no tag)
     * @see #createTurnSigns(Element, DNBIgpOlpUploadEnumeratedTypes.TurnSign[])
     * Previous step (if only aux values are needed, turn signs are supported, 
     * and no tag)
     * @see #createTag(Element, String) Previous step (if only aux values are 
     * needed, and tag is defined)
     * @see #getParameterData()
     * @since V5R14
     */
    
    public Element createJointTarget(Element oTargetElem, String sHomeName) {
        Element oJointTargetElem = null;
        
        if (oTargetElem != null &&
            sHomeName != null && (! sHomeName.equals(""))) {
            oJointTargetElem = m_Document.createElement("JointTarget");
            oTargetElem.appendChild(oJointTargetElem);
            
            Element oHomeNameElem = m_Document.createElement("HomeName");
            oJointTargetElem.appendChild(oHomeNameElem);
            
            Text oHomeNameText = m_Document.createTextNode(sHomeName);
            oHomeNameElem.appendChild(oHomeNameText);
        }
        
        return oJointTargetElem;
    }
    
    /**Returns created <CODE>JointTarget</CODE> XML element (given the joint 
     * values), including all of its sub-elements.<P>This is either the 
     * fourth step (if target's coordinate space is Joint), or the last step 
     * (if only auxiliary target needs to added to already created Cartesian 
     * target), in creation of XML representation of atomic Robot Motion V5 
     * Activity. 
     * Since structure of Robot Motion V5 Activity is complex in nature, and 
     * requires numerous parameters to be configured properly, XML creation will 
     * be done through several, rather then just one (as for the other atomic 
     * activities), method calls.
     * @param oTargetElem parent element, with tag name <CODE>Target</CODE>, of 
     * the XML element to be created.
     * @param oaJointTarget array of lists of data needed to configu. 
     * This array must have as many elements as there are DOFs for the robot 
     * resource (including auxiliary DOFs). Every element of this array must 
     * have either four components (first four, in exact order below), five 
     * components (all five, in exact order below), or six components (all six, in
     * exact order below):<BR>
     * - java.lang.String sJointName - descriptive name of the joint with 
     * specified DOF number, as defined in V5<BR>
     * - java.lang.Double dJointValue - joint value for the specified DOF number
     * , in <CODE>meter</CODE> units, if the fourth parameter is <CODE>
     * DOFType.TRANSLATIONAL</CODE>, or in <CODE>degree</CODE> units,  if the 
     * fourth parameter is <CODE>DOFType.ROTATIONAL</CODE><BR>
     * - java.lang.Integer iDOFNumber - DOF number as it appears in robot's 
     * kinematics chain<BR>
     * - DNBIgpOlpUploadEnumeratedTypes.DOFType eDOFNumber - type of motion
     * allowed for this DOF. Valid values are: <CODE>DOFType.TRANSLATIONAL
     * </CODE> and <CODE>DOFType.ROTATIONAL</CODE><BR>
     * - DNBIgpOlpUploadEnumeratedTypes.AuxAxisType eAuxAxisType - auxiliary 
     * axis type. Should be passed only if auxiliary axis is defined for the
     * specified DOF number. Valid values are: <CODE>
     * AuxAxisType.END_OF_ARM_TOOLING</CODE>, 
     * <CODE>AuxAxisType.RAIL_TRACK_GANTRY</CODE>, and <CODE>
     * AuxAxisType.WORKPIECE_POSITIONER</CODE><BR> 
     * - java.lang.Boolean auxAxesLock - Boolean.TRUE for auxiliary axes lock, 
     * Boolean.FALSE for not locked<BR>
     * @return created <CODE>JointTarget</CODE> XML element, or <CODE>null
     * </CODE>, if any of the passed arguments is a <CODE>null</CODE> reference.
     * @see #createTarget(Element, DNBIgpOlpUploadEnumeratedTypes.TargetType, 
     * boolean) Previous Step (if target's coordinate space is Joint)
     * @see #createCartesianTarget(Element, double [], String) Previous step (if
     * only aux values are needed, turns are not supported, and no tag)
     * @see #createTurnNumbers(Element, int []) Previous step (if only aux 
     * values are needed, turn numbers are supported, and no tag)
     * @see #createTurnSigns(Element, DNBIgpOlpUploadEnumeratedTypes.TurnSign[])
     * Previous step (if only aux values are needed, turn signs are supported, 
     * and no tag)
     * @see #createTag(Element, String) Previous step (if only aux values are 
     * needed, and tag is defined)
     * @see #getNumberOfRobotAxes()
     * @see #getNumberOfWorkpiecePositionerAuxiliaryAxes()
     * @see #getNumberOfToolAuxiliaryAxes()
     * @see #getNumberOfRailAuxiliaryAxes()
     * @see #getAxisTypes()
     * @see DNBIgpOlpUploadEnumeratedTypes.DOFType
     * @see DNBIgpOlpUploadEnumeratedTypes.AuxAxisType
     * @since V5R14
     */
    
    public Element createJointTarget(Element oTargetElem, 
                                     ArrayList [] oaJointTarget) {
        Element oJointTargetElem = null;
        
        if (oTargetElem != null && oaJointTarget != null) {
            for (int ii = 0; ii < oaJointTarget.length; ii++) {
                if (oaJointTarget[ii].size() == ONLY_ROBOT_AXES_IN_JOINT_TARGET ||
                   (oaJointTarget[ii].size() >  ONLY_ROBOT_AXES_IN_JOINT_TARGET &&
                    oaJointTarget[ii].size() <= ROBOT_AND_AUX_AXES_IN_JOINT_TARGET))
                {
                    String sJointName = (String) oaJointTarget[ii].get(0);
                    Double dJointValue = (Double) oaJointTarget[ii].get(1);
                    Integer iDOFNumber = (Integer) oaJointTarget[ii].get(2);
                    DNBIgpOlpUploadEnumeratedTypes.DOFType eDOFType = 
                                        (DNBIgpOlpUploadEnumeratedTypes.DOFType) 
                                         oaJointTarget[ii].get(3);
                    
                    DNBIgpOlpUploadEnumeratedTypes.AuxAxisType eAuxAxisType = null;
                    if (oaJointTarget[ii].size() >  ONLY_ROBOT_AXES_IN_JOINT_TARGET &&
                        oaJointTarget[ii].size() <= ROBOT_AND_AUX_AXES_IN_JOINT_TARGET)
                        eAuxAxisType = (DNBIgpOlpUploadEnumeratedTypes.AuxAxisType)oaJointTarget[ii].get(4);
                    
                    Boolean bIsLocked = Boolean.FALSE;
                    if (oaJointTarget[ii].size() == ROBOT_AND_AUX_AXES_IN_JOINT_TARGET)
                        bIsLocked = (Boolean)oaJointTarget[ii].get(5);
                
                    if (sJointName != null && dJointValue != null && iDOFNumber 
                                                  != null && eDOFType != null) {
                        NodeList oJointTargetNodeList = 
                                oTargetElem.getElementsByTagName("JointTarget");

                        if (oJointTargetNodeList != null && 
                                         oJointTargetNodeList.getLength() > 0) {
                            Node oJointTargetNode = 
                                                   oJointTargetNodeList.item(0);
                            oJointTargetElem = (Element) oJointTargetNode;
                        }
                        else {
                            oJointTargetElem = 
                                        m_Document.createElement("JointTarget");
                            oTargetElem.appendChild(oJointTargetElem);
                        }
                        
                        if (oJointTargetElem != null) {
                            Element oJointElem = null;
                            
                            if (oaJointTarget[ii].size() == 
                                              ONLY_ROBOT_AXES_IN_JOINT_TARGET) {
                                oJointElem = m_Document.createElement("Joint");
                                oJointTargetElem.appendChild(oJointElem);
                            }
                            else if (oaJointTarget[ii].size() >  ONLY_ROBOT_AXES_IN_JOINT_TARGET &&
                                     oaJointTarget[ii].size() <= ROBOT_AND_AUX_AXES_IN_JOINT_TARGET &&
                                     eAuxAxisType != null)
                            {
                                oJointElem = 
                                           m_Document.createElement("AuxJoint");
                                oJointTargetElem.appendChild(oJointElem);
                                
                                oJointElem.setAttribute("Type", 
                                                       eAuxAxisType.toString());
                                oJointElem.setAttribute("Locked", bIsLocked.toString());
                                if (m_TrackType == DNBIgpOlpUploadEnumeratedTypes.TrackType.TEACH_POS && eAuxAxisType == DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY)
                                {
                                	oJointElem.setAttribute("AbsoluteTaughtPosition", "true");
                                }                          
                            }
                             else {
                                 return null;
                             }
                            
                            oJointElem.setAttribute("JointName", sJointName);
                            oJointElem.setAttribute("JointType", 
                                                           eDOFType.toString());
                            oJointElem.setAttribute("DOFNumber", 
                                                         iDOFNumber.toString());

                            Element oJointValueElem = 
                                         m_Document.createElement("JointValue");
                            oJointElem.appendChild(oJointValueElem);
                            Text oJointValueText = 
                              m_Document.createTextNode(dJointValue.toString());
                            oJointValueElem.appendChild(oJointValueText);
                        }
                        else {
                            return null;
                        }
                    }
                    else {
                        return null;
                    }
                }
                else {
                    return null;
                }
            }
        }
 
        return oJointTargetElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * "DelayActivity".
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description a Delay Activity will be created in a 
     * selected Robot Task in V5.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param dStartTime point in simulation time when this activity will begin. 
     * If that information is not available, use 0 value.
     * @param dEndTime point in simulation time when this activity will end.
     * If that information is not available, use the total delay time.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if <CODE>sActivityName</CODE> argument is an empty
     * string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createDelayActivity(Element oActivityListElem,
                                       String sActivityName, 
                                       Double dStartTime, 
                                       Double dEndTime) {
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "DelayActivity");
        
        if (oActivityElem != null && dStartTime != null && dEndTime != null) {
             Element oActivityTimeElem = 
                                       m_Document.createElement("ActivityTime");
            oActivityElem.appendChild(oActivityTimeElem);
            
            Element oStartTimeElem = m_Document.createElement("StartTime");
            oActivityTimeElem.appendChild(oStartTimeElem);
            Text oStartTimeText = 
                               m_Document.createTextNode(dStartTime.toString());
            oStartTimeElem.appendChild(oStartTimeText);
            
            Element oEndTimeElem = m_Document.createElement("EndTime");
            oActivityTimeElem.appendChild(oEndTimeElem);
            Text oEndTimeText = m_Document.createTextNode(dEndTime.toString());
            oEndTimeElem.appendChild(oEndTimeText);
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBIgpCallRobotTask</CODE>.
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description a Call Robot Task Activity will be created
     * in a selected Robot Task in V5.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sCalledTaskName name of a Robot Task, as it will appear in V5
     * Igrip, which will be called from the current Robot Task.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or 
     * <CODE>sCalledTaskName</CODE> argument is an empty string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createCallTaskActivity(Element oActivityListElem, 
                                          String sActivityName, 
                                          String sCalledTaskName) {
        Element oActivityElem = createActivityHeader
                      (oActivityListElem, sActivityName, "DNBIgpCallRobotTask");
        
        if (oActivityElem != null && sCalledTaskName != null && 
                                               (! sCalledTaskName.equals(""))) {
            Element oCallNameElem = m_Document.createElement("CallName");
            oActivityElem.appendChild(oCallNameElem);
            Text oCallNameText = m_Document.createTextNode(sCalledTaskName);
            oCallNameElem.appendChild(oCallNameText);
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBSetSignalActivity</CODE>.
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description a Set IO Activity will be created
     * in a selected Robot Task in V5.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param bSignalValue boolean that determines signal value, <CODE>true
     * </CODE> is "ON", while <CODE>false</CODE> is "OFF".
     * @param sSignalName descriptive name of the output port
     * @param iPortNumber output port number. If the specified port number does 
     * not exist for the V5 scenario, new port number will be created 
     * for the uploading resource.
     * @param dSignalDuration amount of time that the specified output port will 
     * be emitting the signal
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or 
     * <CODE>sSignalName</CODE> argument is an empty string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createSetIOActivity(Element oActivityListElem, 
                                       String sActivityName, 
                                       boolean bSignalValue, 
                                       String sSignalName, 
                                       Integer iPortNumber, 
                                       Double dSignalDuration) {
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "DNBSetSignalActivity");
        
        if (oActivityElem != null && iPortNumber != null 
            && dSignalDuration != null && sSignalName != null 
            && (! sSignalName.equals(""))) {
            Element oSetIOAttributesElem = 
                                    m_Document.createElement("SetIOAttributes");
            oActivityElem.appendChild(oSetIOAttributesElem);
            
            Element oIOSetSignalElem = m_Document.createElement("IOSetSignal");
            oSetIOAttributesElem.appendChild(oIOSetSignalElem);
            
            String sSignalValue = "";
            
            if (bSignalValue == true) {
                sSignalValue = "On";
            }
            else {
                sSignalValue = "Off";
            }
            
            oIOSetSignalElem.setAttribute("SignalValue", sSignalValue);
            oIOSetSignalElem.setAttribute("SignalName", sSignalName);
            oIOSetSignalElem.setAttribute("PortNumber", iPortNumber.toString());
            oIOSetSignalElem.setAttribute("SignalDuration", 
                                                    dSignalDuration.toString());
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBWaitSignalActivity</CODE>.
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description a Wait For IO Activity will be created
     * in a selected Robot Task in V5.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param bSignalValue boolean that determines the signal value, <CODE>true
     * </CODE> is "ON", while <CODE>false</CODE> is "OFF".
     * @param sSignalName descriptive name of the input port
     * @param iPortNumber input port number. If the specified port number does 
     * not exist for the V5 scenario, new port number will NOT be 
     * created for the uploading resource, which in turn, will result in 
     * creation of an invalid activity.
     * @param dMaxWaitTime amount of time that the specified input port will 
     * be open for signal reception. If it is zero, robot will wait until it 
     * receives the signal (no time constraints will be imposed). 
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or 
     * <CODE>sSignalName</CODE> argument is an empty string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createWaitForIOActivity(Element oActivityListElem, 
                                           String sActivityName, 
                                           boolean bSignalValue, 
                                           String sSignalName, 
                                           Integer iPortNumber, 
                                           Double dMaxWaitTime) {
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "DNBWaitSignalActivity");
        
        if (oActivityElem != null && iPortNumber != null 
            && dMaxWaitTime != null && sSignalName != null 
            && (! sSignalName.equals(""))) {
            Element oWaitIOAttributesElem = 
                                   m_Document.createElement("WaitIOAttributes");
            oActivityElem.appendChild(oWaitIOAttributesElem);
            
            Element oIOWaitSignalElem = 
                                       m_Document.createElement("IOWaitSignal");
            oWaitIOAttributesElem.appendChild(oIOWaitSignalElem);
            
            String sSignalValue = "";
            
            if (bSignalValue == true) {
                sSignalValue = "On";
            }
            else {
                sSignalValue = "Off";
            }
            
            oIOWaitSignalElem.setAttribute("SignalValue", sSignalValue);
            oIOWaitSignalElem.setAttribute("SignalName", sSignalName);
            oIOWaitSignalElem.setAttribute("PortNumber", 
                                                        iPortNumber.toString());
            oIOWaitSignalElem.setAttribute("MaxWaitTime", 
                                                       dMaxWaitTime.toString());
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBEnterZoneActivity</CODE> or of type <CODE>DNBClearZoneActivity
     * </CODE>.
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description either Enter or Clear Zone Activity will be
     * created in a selected Robot Task in V5.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param eZoneType type of zone. Valid values are: <CODE>
     * ZoneActivityType.CLEAR_ZONE</CODE> and <CODE>ZoneActivityType.ENTER_ZONE
     * </CODE>. Depending on this parameter value, either Enter Zone Activity or 
     * Clear Zone Activity will be created in V5 Igrip.
     * @param sZoneName interference zone name, as it will appear in V5
     * Igrip. If the name does not exist in V5 context scenario, a new 
     * interference zone with the specified name will be created.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or 
     * <CODE>sZoneName</CODE> argument is an empty string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType
     * @since V5R14
     */
    
    public Element createZoneActivity(Element oActivityListElem, 
                                      String sActivityName, 
                                      DNBIgpOlpUploadEnumeratedTypes.
                                                     ZoneActivityType eZoneType, 
                                      String sZoneName) {
        Element oActivityElem = null;
        
        if (eZoneType != null) {
            String sActivityType = "";

            if (eZoneType == 
                   DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType.ENTER_ZONE) {
                sActivityType = "DNBEnterZoneActivity";
            }
            else {
                sActivityType = "DNBClearZoneActivity";
            }

            oActivityElem = createActivityHeader(oActivityListElem, 
                                                 sActivityName, 
                                                 sActivityType);

            if (oActivityElem != null && sZoneName != null && 
                                                     (! sZoneName.equals(""))) {
                Element oZoneDataElem = m_Document.createElement("ZoneData");
                oActivityElem.appendChild(oZoneDataElem);
 
                Element oZoneNameElem = m_Document.createElement("ZoneName");
                oZoneDataElem.appendChild(oZoneNameElem);
                Text oZoneNameText = m_Document.createTextNode(sZoneName);
                oZoneNameElem.appendChild(oZoneNameText);
            }
            else {
                return null;
            }
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBEnterZoneActivity</CODE> or of type <CODE>DNBClearZoneActivity
     * </CODE>.
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description either Enter or Clear Zone Activity will be
     * created in a selected Robot Task in V5.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param eZoneType type of zone. Valid values are: <CODE>
     * ZoneActivityType.CLEAR_ZONE</CODE> and <CODE>ZoneActivityType.ENTER_ZONE
     * </CODE>. Depending on this parameter value, either Enter Zone Activity or 
     * Clear Zone Activity will be created in V5 Igrip.
     * @param sZoneName interference zone name, as it will appear in V5
     * Igrip. If the name does not exist in V5 context scenario, a new 
     * interference zone with the specified name will be created.
     * @param saResourceNames array of all resources which will be monitored for
     * the specified interference zone, if this information can be retrieved 
     * from the robot program, which is to be uploaded.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or 
     * <CODE>sZoneName</CODE> argument is an empty string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType
     * @since V5R14
     */
    
    public Element createZoneActivity(Element oActivityListElem, 
                                      String sActivityName, 
                                      DNBIgpOlpUploadEnumeratedTypes.
                                                     ZoneActivityType eZoneType, 
                                      String sZoneName, 
                                      String [] saResourceNames) {
        Element oActivityElem = createZoneActivity(oActivityListElem, 
                                                   sActivityName, 
                                                   eZoneType, 
                                                   sZoneName);
        
        if (oActivityElem != null) {
            NodeList oZoneDataNodeList = 
                                 oActivityElem.getElementsByTagName("ZoneData");

            if (oZoneDataNodeList != null && oZoneDataNodeList.getLength() 
                                                                          > 0) {
                Node oZoneDataNode = oZoneDataNodeList.item(0);
                Element oZoneDataElem = (Element) oZoneDataNode;
                
                if (oZoneDataElem != null && saResourceNames != null && 
                                                     saResourceNames.length > 0)
                {
                    Element oZoneResourceListElem = 
                                   m_Document.createElement("ZoneResourceList");
                    oZoneDataElem.appendChild(oZoneResourceListElem);
                    
                    for (int ii = 0; ii < saResourceNames.length; ii++) {
                        String sResourceName = saResourceNames[ii];
                        
                        Element oZoneResourceElem = 
                                       m_Document.createElement("ZoneResource");
                        oZoneResourceListElem.appendChild(oZoneResourceElem);
                        oZoneResourceElem.setAttribute("Name", sResourceName);
                    }
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBEnterZoneActivity</CODE> or of type <CODE>DNBClearZoneActivity
     * </CODE>.
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description either Enter or Clear Zone Activity will be
     * created in a selected Robot Task in V5. <P>To be used with PPR Hub data 
     * only.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param eZoneType type of zone. Valid values are: <CODE>
     * ZoneActivityType.CLEAR_ZONE</CODE> and <CODE>ZoneActivityType.ENTER_ZONE
     * </CODE>. Depending on this parameter value, either Enter Zone Activity or 
     * Clear Zone Activity will be created in V5 Igrip.
     * @param sZoneName interference zone name, as it will appear in V5
     * Igrip. If the name does not exist in V5 context scenario, a new 
     * interference zone with the specified name will be created.
     * @param saResourceNames array of all resources which will be monitored for
     * the specified interference zone, if this information can be retrieved 
     * from the robot program, which is to be uploaded.
     * @param saResourceParentNames array of V5 parents of V5 resources 
     * specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * resources need to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or 
     * <CODE>sZoneName</CODE> argument is an empty string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType
     * @since V5R14
     */
                        
    public Element createZoneActivity(Element oActivityListElem, 
                                      String sActivityName, 
                                      DNBIgpOlpUploadEnumeratedTypes.
                                                     ZoneActivityType eZoneType, 
                                      String sZoneName, 
                                      String [] saResourceNames, 
                                      String [] saResourceParentNames) {
        Element oActivityElem = createZoneActivity(oActivityListElem, 
                                                   sActivityName, 
                                                   eZoneType, 
                                                   sZoneName, 
                                                   saResourceNames);
        
        if (oActivityElem != null && saResourceParentNames != null && 
                                     saResourceParentNames.length > 0) {
            NodeList oZoneResourceNodeList = 
                             oActivityElem.getElementsByTagName("ZoneResource");

            if (oZoneResourceNodeList != null && 
                oZoneResourceNodeList.getLength() > 0) {
                for (int ii = 0; ii < oZoneResourceNodeList.getLength(); ii++) {
                    Node oZoneResourceNode = oZoneResourceNodeList.item(ii);
                    Element oZoneResourceElem = (Element) oZoneResourceNode;
                    
                    if (oZoneResourceElem != null && 
                        saResourceParentNames.length > ii && 
                        saResourceParentNames[ii] != null) {
                        String sResourceParentName = saResourceParentNames[ii];
                        oZoneResourceElem.setAttribute("ParentName", 
                                                       sResourceParentName);
                    }
                    else {
                        return null;
                    }
                }
            }
            else {
                return null;
            }
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBRobotMotionActivity</CODE>, whose grand-parent XML element is 
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create robot motion activities embeded into V5 actions, and must not be
     * used to create an atomic V5 activities (activities that can exist 
     * independently in robot tasks, without being parts of any V5 actions).
     * Based on this XML description, a Robot Motion Activity will be created 
     * for specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which in turn is a child of <CODE>Action
     * </CODE> XML element. 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param dStartTime point in simulation time when this activity will begin. 
     * If that information is not available, use 0 value.
     * @param dEndTime point in simulation time when this activity will end.
     * If that information is not available, use the total motion time.
     * @param eMotionType type of motion. Valid values are: <CODE>
     * MotionType.JOINT_MOTION</CODE>, <CODE>MotionType.LINEAR_MOTION</CODE>,
     * <CODE>MotionType.CIRCULARVIA_MOTION</CODE> or <CODE>MotionType.CIRCULAR_MOTION</CODE>.
     * @param sHomeName target home position name of the resource that performs 
     * this action
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or <CODE>
     * sHomeName</CODE> argument is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @since V5R14
     */
    
    public Element createMotionActivityWithinAction(
                                                 Element oActivityListElem,
                                                 String sActivityName, 
                                                 Double dStartTime, 
                                                 Double dEndTime, 
                                                 DNBIgpOlpUploadEnumeratedTypes.
                                                         MotionType eMotionType, 
                                                 String sHomeName) {
                                                        
       Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                    sActivityName, 
                                                    "DNBRobotMotionActivity");
        
        if (oActivityElem != null && eMotionType != null && dStartTime != null
            && dEndTime != null && sHomeName != null && 
                                                     (! sHomeName.equals(""))) {
            Element oActivityTimeElem = 
                                       m_Document.createElement("ActivityTime");
            oActivityElem.appendChild(oActivityTimeElem);
            
            Element oStartTimeElem = m_Document.createElement("StartTime");
            oActivityTimeElem.appendChild(oStartTimeElem);
            Text oStartTimeText = 
                               m_Document.createTextNode(dStartTime.toString());
            oStartTimeElem.appendChild(oStartTimeText);
            
            Element oEndTimeElem = m_Document.createElement("EndTime");
            oActivityTimeElem.appendChild(oEndTimeElem);
            Text oEndTimeText = m_Document.createTextNode(dEndTime.toString());
            oEndTimeElem.appendChild(oEndTimeText);
            
            Element oMotionAttributesElem = 
                                   m_Document.createElement("MotionAttributes");
            oActivityElem.appendChild(oMotionAttributesElem);
            
            Element oMotionTypeElem = m_Document.createElement("MotionType");
            oMotionAttributesElem.appendChild(oMotionTypeElem);
            Text oMotionTypeText = 
                              m_Document.createTextNode(eMotionType.toString());
            oMotionTypeElem.appendChild(oMotionTypeText);
            
            Element oTargetElem = m_Document.createElement("Target");
            oActivityElem.appendChild(oTargetElem);
            oTargetElem.setAttribute("Default", "Joint");
            
            createJointTarget(oTargetElem, sHomeName);
        }
        
        return oActivityElem;
    }

    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBRobotMotionActivity</CODE>, whose grand-parent XML element is 
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create robot motion activities embeded into V5 actions, and must not be
     * used to create an atomic V5 activities (activities that can exist 
     * independently in robot tasks, without being parts of any V5 actions).
     * Based on this XML description, a Robot Motion Activity will be created 
     * for specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which in turn is a child of <CODE>Action
     * </CODE> XML element. 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param dStartTime point in simulation time when this activity will begin. 
     * If that information is not available, use 0 value.
     * @param dEndTime point in simulation time when this activity will end.
     * If that information is not available, use the total motion time.
     * @param eMotionType type of motion. Valid values are: <CODE>
     * MotionType.JOINT_MOTION</CODE>, <CODE>MotionType.LINEAR_MOTION</CODE>,
     * <CODE>MotionType.CIRCULARVIA_MOTION</CODE> or <CODE>MotionType.CIRCULAR_MOTION</CODE>.
     * @param oaJointTarget array of lists of data needed to configu. 
     * This array must have as many elements as there are DOFs for the robot 
     * resource (including auxiliary DOFs). Every element of this array must 
     * have either four components (first four, in exact order below), five 
     * components (all five, in exact order below), or six components (all six, in
     * exact order below):<BR>
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or <CODE>
     * sHomeName</CODE> argument is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionType
     * @since V5R14
     */
    
    public Element createMotionActivityWithinAction(
                                                 Element oActivityListElem,
                                                 String sActivityName, 
                                                 Double dStartTime, 
                                                 Double dEndTime, 
                                                 DNBIgpOlpUploadEnumeratedTypes.
                                                         MotionType eMotionType, 
                                                 ArrayList [] oaJointTarget) {
                                                        
       Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                    sActivityName, 
                                                    "DNBRobotMotionActivity");
        
        if (oActivityElem != null && eMotionType != null && dStartTime != null
            && dEndTime != null && (oaJointTarget != null)) {
            Element oActivityTimeElem = 
                                       m_Document.createElement("ActivityTime");
            oActivityElem.appendChild(oActivityTimeElem);
            
            Element oStartTimeElem = m_Document.createElement("StartTime");
            oActivityTimeElem.appendChild(oStartTimeElem);
            Text oStartTimeText = 
                               m_Document.createTextNode(dStartTime.toString());
            oStartTimeElem.appendChild(oStartTimeText);
            
            Element oEndTimeElem = m_Document.createElement("EndTime");
            oActivityTimeElem.appendChild(oEndTimeElem);
            Text oEndTimeText = m_Document.createTextNode(dEndTime.toString());
            oEndTimeElem.appendChild(oEndTimeText);
            
            Element oMotionAttributesElem = 
                                   m_Document.createElement("MotionAttributes");
            oActivityElem.appendChild(oMotionAttributesElem);
            
            Element oMotionTypeElem = m_Document.createElement("MotionType");
            oMotionAttributesElem.appendChild(oMotionTypeElem);
            Text oMotionTypeText = 
                              m_Document.createTextNode(eMotionType.toString());
            oMotionTypeElem.appendChild(oMotionTypeText);
            
            Element oTargetElem = m_Document.createElement("Target");
            oActivityElem.appendChild(oTargetElem);
            oTargetElem.setAttribute("Default", "Joint");
            
            createJointTarget(oTargetElem, oaJointTarget);
        }
        
        return oActivityElem;
    }
        
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>GrabActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create grab activities, since creation of atomic Grab Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Grab Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sGrabbingObject instance name of an object that performs grabbing 
     * action. That object can either be the action resource or just a part of 
     * it, which exists in V5 world (in either Resource or Product context),
     * and can be identified by this instance name.
     * @param sGrabbedObject instance name of an object that will be grabbed by 
     * the grabbing object above. Grabbed object can be any valid V5 Part or 
     * Product, which exists in V5 world (in either Resource or Product context)
     * , and can be identified by this instance name.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createGrabActivity(Element oActivityListElem,
                                      String sActivityName, 
                                      String sGrabbingObject, 
                                      String sGrabbedObject) {
        
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "GrabActivity");
        
        if (oActivityElem != null &&
            sGrabbingObject != null && (! sGrabbingObject.equals("")) &&
            sGrabbedObject != null && (! sGrabbedObject.equals(""))) {
               
            Element oGrabbingObjectElem = 
                                     m_Document.createElement("GrabbingObject");
            oActivityElem.appendChild(oGrabbingObjectElem);
            Text oGrabbingObjectText = 
                                     m_Document.createTextNode(sGrabbingObject);
            oGrabbingObjectElem.appendChild(oGrabbingObjectText);
            
            Element oGrabbedObjectElem = 
                                      m_Document.createElement("GrabbedObject");
            oActivityElem.appendChild(oGrabbedObjectElem);
            Text oGrabbedObjectText = 
                                      m_Document.createTextNode(sGrabbedObject);
            oGrabbedObjectElem.appendChild(oGrabbedObjectText);
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>GrabActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create grab activities, since creation of atomic Grab Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Grab Activity will be created for 
     * specified V5 Action.<P>To be used with PPR Hub data only.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sGrabbingObject instance name of an object that performs grabbing 
     * action. That object can either be the action resource or just a part of 
     * it, which exists in V5 world (in either Resource or Product context),
     * and can be identified by this instance name.
     * @param sGrabbedObject instance name of an object that will be grabbed by 
     * the grabbing object above. Grabbed object can be any valid V5 Part or 
     * Product, which exists in V5 world (in either Resource or Product context)
     * , and can be identified by this instance name.
     * @param sGrabbingObjectParent name of a V5 parent of the V5 grabbing 
     * object specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * object needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @param sGrabbedObjectParent name of a V5 parent of the V5 grabbed object
     * specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * object needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createGrabActivity(Element oActivityListElem, 
                                      String sActivityName, 
                                      String sGrabbingObject, 
                                      String sGrabbedObject, 
                                      String sGrabbingObjectParent, 
                                      String sGrabbedObjectParent) {
        
        Element oActivityElem = createGrabActivity(oActivityListElem, 
                                                   sActivityName, 
                                                   sGrabbingObject, 
                                                   sGrabbedObject);
        
        if (oActivityElem != null &&
            sGrabbingObjectParent != null && 
            (! sGrabbingObjectParent.equals("")) &&
            sGrabbedObjectParent != null && 
                                          (! sGrabbedObjectParent.equals(""))) {
                
            NodeList oGrabbingObjectNodeList = 
                           oActivityElem.getElementsByTagName("GrabbingObject");

            if (oGrabbingObjectNodeList != null && 
                                      oGrabbingObjectNodeList.getLength() > 0) {
                Node oGrabbingObjectNode = oGrabbingObjectNodeList.item(0);
                Element oGrabbingObjectElem = (Element) oGrabbingObjectNode;
                
                if (oGrabbingObjectElem != null) {
                    oGrabbingObjectElem.setAttribute("ParentName", 
                                                         sGrabbingObjectParent);
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
            
            NodeList oGrabbedObjectNodeList = 
                            oActivityElem.getElementsByTagName("GrabbedObject");

            if (oGrabbedObjectNodeList != null && 
                                       oGrabbedObjectNodeList.getLength() > 0) {
                Node oGrabbedObjectNode = oGrabbedObjectNodeList.item(0);
                Element oGrabbedObjectElem = (Element) oGrabbedObjectNode;
                
                if (oGrabbedObjectElem != null) {
                    oGrabbedObjectElem.setAttribute("ParentName", 
                                                    sGrabbedObjectParent);
                }
                else {
                    return null;
                }
            }
            else
                return null;
        }
        
        return oActivityElem;
    }

    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>GrabActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create grab activities, since creation of atomic Grab Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Grab Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sGrabbingObject instance name of an object that performs grabbing 
     * action. That object can either be the action resource or just a part of 
     * it, which exists in V5 world (in either Resource or Product context),
     * and can be identified by this instance name.
     * @param sGrabbedObject instance name(s) of object(s) that will be grabbed by 
     * the grabbing object above. Grabbed object(s) can be any valid V5 Part or 
     * Product, which exists in V5 world (in either Resource or Product context)
     * , and can be identified by this instance name.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R15
     */
    
    public Element createMultiGrabActivity(Element oActivityListElem,
                                      String sActivityName, 
                                      String sGrabbingObject, 
                                      String [] sGrabbedObject) {
        
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "GrabActivity");
        
        if (oActivityElem != null &&
            sGrabbingObject != null && (! sGrabbingObject.equals("")) &&
            sGrabbedObject != null) {
               
            Element oGrabbingObjectElem = 
                                     m_Document.createElement("GrabbingObject");
            oActivityElem.appendChild(oGrabbingObjectElem);
            Text oGrabbingObjectText = 
                                     m_Document.createTextNode(sGrabbingObject);
            oGrabbingObjectElem.appendChild(oGrabbingObjectText);
            
            Element oGrabbedObjectListElem = 
                                      m_Document.createElement("GrabbedObjectList");
            oActivityElem.appendChild(oGrabbedObjectListElem);
            
            for (int ii=0;ii<sGrabbedObject.length;ii++)
            {
                if (sGrabbedObject[ii] != null && (! sGrabbedObject[ii].equals("")))
                {
                    Element oGrabbedObjectElem = 
                                              m_Document.createElement("GrabbedObject");
                    oGrabbedObjectListElem.appendChild(oGrabbedObjectElem);
                    Text oGrabbedObjectText = 
                                              m_Document.createTextNode(sGrabbedObject[ii]);
                    oGrabbedObjectElem.appendChild(oGrabbedObjectText);
            
                }
            }
        }
        
        return oActivityElem;
    }

    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>GrabActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create grab activities, since creation of atomic Grab Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Grab Activity will be created for 
     * specified V5 Action.<P>To be used with PPR Hub data only.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sGrabbingObject instance name of an object that performs grabbing 
     * action. That object can either be the action resource or just a part of 
     * it, which exists in V5 world (in either Resource or Product context),
     * and can be identified by this instance name.
     * @param sGrabbedObject instance name(s) of object(s) that will be grabbed by 
     * the grabbing object above. Grabbed object(s) can be any valid V5 Part or 
     * Product, which exists in V5 world (in either Resource or Product context)
     * , and can be identified by this instance name(s).
     * @param sGrabbingObjectParent name of a V5 parent of the V5 grabbing 
     * object specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * object needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @param sGrabbedObjectParent name of a V5 parent of the V5 grabbed object
     * specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * object needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R15
     */
    
    public Element createMultiGrabActivity(Element oActivityListElem, 
                                      String sActivityName, 
                                      String sGrabbingObject, 
                                      String [] sGrabbedObject, 
                                      String sGrabbingObjectParent, 
                                      String sGrabbedObjectParent) {
        
        Element oActivityElem = createMultiGrabActivity(oActivityListElem, 
                                                   sActivityName, 
                                                   sGrabbingObject, 
                                                   sGrabbedObject);
        
        if (oActivityElem != null &&
            sGrabbingObjectParent != null && 
            (! sGrabbingObjectParent.equals("")) &&
            sGrabbedObjectParent != null) {
                
            NodeList oGrabbingObjectNodeList = 
                           oActivityElem.getElementsByTagName("GrabbingObject");

            if (oGrabbingObjectNodeList != null && 
                                      oGrabbingObjectNodeList.getLength() > 0) {
                Node oGrabbingObjectNode = oGrabbingObjectNodeList.item(0);
                Element oGrabbingObjectElem = (Element) oGrabbingObjectNode;
                
                if (oGrabbingObjectElem != null) {
                    oGrabbingObjectElem.setAttribute("ParentName", 
                                                         sGrabbingObjectParent);
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
            
            NodeList oGrabbedObjectNodeList = 
                            oActivityElem.getElementsByTagName("GrabbedObject");

            if (oGrabbedObjectNodeList != null && 
                                       oGrabbedObjectNodeList.getLength() > 0) {
                for (int ii=0;ii<oGrabbedObjectNodeList.getLength();ii++)
                {
                    Node oGrabbedObjectNode = oGrabbedObjectNodeList.item(ii);
                    Element oGrabbedObjectElem = (Element) oGrabbedObjectNode;

                    if (oGrabbedObjectElem != null) {
                        oGrabbedObjectElem.setAttribute("ParentName", 
                                                        sGrabbedObjectParent);
                    }
                    else {
                        return null;
                    }
                }
            }
            else
                return null;
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>GrabActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create grab activities, since creation of atomic Grab Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Grab Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sensorName instance name of a sensor that will automatically sense an
     * object(s).  SensorName can be any valid V5 sensor which exists in the V5 world.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R15
     */
    
    public Element createSensorGrabActivity(Element oActivityListElem,
                                      String sActivityName, 
                                      String sensorName) {
        
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "GrabActivity");
        

        if (oActivityElem != null && sensorName != null && 
           (! sensorName.equals(""))) {
           Element oSensorElem = 
               m_Document.createElement("Sensor");
           oActivityElem.appendChild(oSensorElem);
           oSensorElem.setAttribute("SensorName", sensorName); 
        }
        
        return oActivityElem;
    }
    
        
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>ReleaseActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create release activities, since creation of atomic Release Activities 
     * (not contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Release Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sReleasedObject instance name of an object that will be released,
     * or dropped by the grabbing object. Object to be released can be any valid
     * V5 Part or Product, which exists in V5 world (in either Resource or 
     * Product context), and can be identified by this instance name. 
     * For object to be released, prerequisite is that it has been previously 
     * grabbed.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
            
    public Element createReleaseActivity(Element oActivityListElem, 
                                         String sActivityName, 
                                         String sReleasedObject) {
        
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "ReleaseActivity");
        
        if (oActivityElem != null &&
            sReleasedObject != null && (! sReleasedObject.equals(""))) {
            
            Element oReleasedObjectElem = 
                                     m_Document.createElement("ReleasedObject");
            oActivityElem.appendChild(oReleasedObjectElem);
            Text oReleasedObjectText = 
                                     m_Document.createTextNode(sReleasedObject);
            oReleasedObjectElem.appendChild(oReleasedObjectText);
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>ReleaseActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create release activities, since creation of atomic Release Activities 
     * (not contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Release Activity will be created for 
     * specified V5 Action.<P>To be used with PPR Hub data only.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sReleasedObject instance name of an object that will be released,
     * or dropped by the grabbing object. Object to be released can be any valid
     * V5 Part or Product, which exists in V5 world (in either Resource or 
     * Product context), and can be identified by this instance name. 
     * For object to be released, prerequisite is that it has been previously 
     * grabbed.
     * @param sReleasedObjectParent name of a V5 parent of the V5 released 
     * object specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * object needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
                    
    public Element createReleaseActivity(Element oActivityListElem, 
                                         String sActivityName, 
                                         String sReleasedObject, 
                                         String sReleasedObjectParent) {
        Element oActivityElem = createReleaseActivity(oActivityListElem, 
                                                      sActivityName, 
                                                      sReleasedObject);
        
        if (oActivityElem != null &&
            sReleasedObjectParent != null && 
                                         (! sReleasedObjectParent.equals(""))) {
                
            NodeList oReleasedObjectNodeList = 
                           oActivityElem.getElementsByTagName("ReleasedObject");

            if (oReleasedObjectNodeList != null && 
                                      oReleasedObjectNodeList.getLength() > 0) {
                Node oReleasedObjectNode = oReleasedObjectNodeList.item(0);
                Element oReleasedObjectElem = (Element) oReleasedObjectNode;
                
                if (oReleasedObjectElem != null) {
                    oReleasedObjectElem.setAttribute("ParentName", 
                                                     sReleasedObjectParent);
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>ReleaseActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create release activities, since creation of atomic Release Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Release Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sReleasingObject instance name of an object that performs releasing 
     * action. That object can either be the action resource or just a part of 
     * it, which exists in V5 world (in either Resource or Product context),
     * and can be identified by this instance name.
     * @param sReleasedObject instance name(s) of object(s) that will be released by 
     * the releasing object above. released object(s) can be any valid V5 Part or 
     * Product, which exists in V5 world (in either Resource or Product context)
     * , and can be identified by this instance name.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R15
     */
    
    public Element createMultiReleaseActivity(Element oActivityListElem,
                                      String sActivityName, 
                                      String sReleasingObject, 
                                      String [] sReleasedObject) {
        
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "ReleaseActivity");
        
        if (oActivityElem != null &&
            sReleasingObject != null && (! sReleasingObject.equals("")) &&
            sReleasedObject != null) {
               
            Element oReleasingObjectElem = 
                                     m_Document.createElement("ReleasingObject");
            oActivityElem.appendChild(oReleasingObjectElem);
            Text oReleasingObjectText = 
                                     m_Document.createTextNode(sReleasingObject);
            oReleasingObjectElem.appendChild(oReleasingObjectText);
            
            Element oReleasedObjectListElem = 
                                      m_Document.createElement("ReleasedObjectList");
            oActivityElem.appendChild(oReleasedObjectListElem);
            
            for (int ii=0;ii<sReleasedObject.length;ii++)
            {
                if (sReleasedObject[ii] != null && (! sReleasedObject[ii].equals("")))
                {
                    Element oReleasedObjectElem = 
                                              m_Document.createElement("ReleasedObject");
                    oReleasedObjectListElem.appendChild(oReleasedObjectElem);
                    Text oReleasedObjectText = 
                                              m_Document.createTextNode(sReleasedObject[ii]);
                    oReleasedObjectElem.appendChild(oReleasedObjectText);
            
                }
            }
        }
        
        return oActivityElem;
    }

    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>ReleaseActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create Release activities, since creation of atomic Release Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Release Activity will be created for 
     * specified V5 Action.<P>To be used with PPR Hub data only.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sReleasingObject instance name of an object that performs Releasing 
     * action. That object can either be the action resource or just a part of 
     * it, which exists in V5 world (in either Resource or Product context),
     * and can be identified by this instance name.
     * @param sReleasedObject instance name(s) of object(s) that will be Released by 
     * the Releasing object above. Released object(s) can be any valid V5 Part or 
     * Product, which exists in V5 world (in either Resource or Product context)
     * , and can be identified by this instance name(s).
     * @param sReleasingObjectParent name of a V5 parent of the V5 Releasing 
     * object specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * object needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @param sReleasedObjectParent name of a V5 parent of the V5 Released object
     * specified above, if this information can be retrieved from the V5 
     * scenario. This information is important when specified 
     * object needs to be searched for in V5's Resource and Product contexts, 
     * and these contexts were populated with data retrieved from PPR Hub.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R15
     */
    
    public Element createMultiReleaseActivity(Element oActivityListElem, 
                                      String sActivityName, 
                                      String sReleasingObject, 
                                      String [] sReleasedObject, 
                                      String sReleasingObjectParent, 
                                      String sReleasedObjectParent) {
        
        Element oActivityElem = createMultiReleaseActivity(oActivityListElem, 
                                                   sActivityName, 
                                                   sReleasingObject, 
                                                   sReleasedObject);
        
        if (oActivityElem != null &&
            sReleasingObjectParent != null && 
            (! sReleasingObjectParent.equals("")) &&
            sReleasedObjectParent != null) {
                
            NodeList oReleasingObjectNodeList = 
                           oActivityElem.getElementsByTagName("ReleasingObject");

            if (oReleasingObjectNodeList != null && 
                                      oReleasingObjectNodeList.getLength() > 0) {
                Node oReleasingObjectNode = oReleasingObjectNodeList.item(0);
                Element oReleasingObjectElem = (Element) oReleasingObjectNode;
                
                if (oReleasingObjectElem != null) {
                    oReleasingObjectElem.setAttribute("ParentName", 
                                                         sReleasingObjectParent);
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
            
            NodeList oReleasedObjectNodeList = 
                            oActivityElem.getElementsByTagName("ReleasedObject");

            if (oReleasedObjectNodeList != null && 
                                       oReleasedObjectNodeList.getLength() > 0) {
                for (int ii=0;ii<oReleasedObjectNodeList.getLength();ii++)
                {
                    Node oReleasedObjectNode = oReleasedObjectNodeList.item(ii);
                    Element oReleasedObjectElem = (Element) oReleasedObjectNode;

                    if (oReleasedObjectElem != null) {
                        oReleasedObjectElem.setAttribute("ParentName", 
                                                        sReleasedObjectParent);
                    }
                    else {
                        return null;
                    }
                }
            }
            else
                return null;
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>ReleaseActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create release activities, since creation of atomic Release Activities (not 
     * contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Release Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sensorName instance name of a sensor that will automatically sense an
     * object(s).  SensorName can be any valid V5 sensor which exists in the V5 world.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R15
     */
    
    public Element createSensorReleaseActivity(Element oActivityListElem,
                                      String sActivityName, 
                                      String sensorName) {
        
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "ReleaseActivity");
        

        if (oActivityElem != null && sensorName != null && 
           (! sensorName.equals(""))) {
           Element oSensorElem = 
               m_Document.createElement("Sensor");
           oActivityElem.appendChild(oSensorElem);
           oSensorElem.setAttribute("SensorName", sensorName); 
        }
        
        return oActivityElem;
    }
    
        
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBIgpMountActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create mount activities, since creation of atomic Mount Activities 
     * (not contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a Mount Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sMountTool instance name of the tool to be mounted on the selected
     * robot. For this tool to become currently mounted tool, the prerequisite 
     * is that its name is in a list of Mounted Devices for the selected robot. 
     * Placing a tool in a list of mounted devices is done by executing <CODE>
     * Set Tool</CODE> command of <CODE>RobotTaskDefinition</CODE> workbench.
     * @param sSetToolProfile tool profile name, as it appears in V5 Controller/
     * ToolProfiles container, which will be set for the newly mounted tool as 
     * the current tool profile name.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createMountActivity(Element oActivityListElem, 
                                       String sActivityName, 
                                       String sMountTool, 
                                       String sSetToolProfile) {
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "DNBIgpMountActivity");
        
        if (oActivityElem != null &&
            sMountTool != null && (! sMountTool.equals("")) &&
            sSetToolProfile != null && (! sSetToolProfile.equals(""))) {
                
            Element oMountToolElem = m_Document.createElement("MountTool");
            oActivityElem.appendChild(oMountToolElem);
            Text oMountToolText = m_Document.createTextNode(sMountTool);
            oMountToolElem.appendChild(oMountToolText);
            
            Element oSetToolProfileElem = 
                                     m_Document.createElement("SetToolProfile");
            oActivityElem.appendChild(oSetToolProfileElem);
            Text oSetToolProfileText = 
                                     m_Document.createTextNode(sSetToolProfile);
            oSetToolProfileElem.appendChild(oSetToolProfileText);
        }
        
        return oActivityElem;
    }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>DNBIgpUnMountActivity</CODE>, whose grand-parent XML element is an
     * <CODE>Action</CODE> XML element. <P>This method should only be used to 
     * create unmount activities, since creation of atomic UnMount Activities 
     * (not contained in V5 Actions) is not allowed in V5.
     * Based on this XML description, a UnMount Activity will be created for 
     * specified V5 Action.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE> (which, in turn, is a child of <CODE>Action
     * </CODE> XML element). 
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sUnMountTool instance name of the tool to be unmounted from the 
     * selected robot. For this tool to be unmounted, the prerequisite 
     * is that has been currently mounted on the selected robot. 
     * @param sSetToolProfile tool profile name, as it appears in V5 Controller/
     * ToolProfiles container, which will be set for the selected robot, after 
     * unmounting the tool, as the current tool profile name.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if any of the passed string arguments is an empty
     * string.
     * @see #createActionHeader(Element, String, String, String)
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R14
     */
    
    public Element createUnMountActivity(Element oActivityListElem, 
                                         String sActivityName, 
                                         String sUnMountTool, 
                                         String sSetToolProfile) {
        Element oActivityElem = createActivityHeader(oActivityListElem, 
                                                     sActivityName, 
                                                     "DNBIgpUnMountActivity");
        
        if (oActivityElem != null &&
            sUnMountTool != null && (! sUnMountTool.equals("")) &&
            sSetToolProfile != null && (! sSetToolProfile.equals(""))) {
                
            Element oUnMountToolElem = m_Document.createElement("UnmountTool");
            oActivityElem.appendChild(oUnMountToolElem);
            Text oUnMountToolText = m_Document.createTextNode(sUnMountTool);
            oUnMountToolElem.appendChild(oUnMountToolText);
            
            Element oUnSetToolProfileElem = 
                                   m_Document.createElement("UnsetToolProfile");
            oActivityElem.appendChild(oUnSetToolProfileElem);
            Text oUnSetToolProfileText = 
                                   m_Document.createTextNode(sSetToolProfile);
            oUnSetToolProfileElem.appendChild(oUnSetToolProfileText);
        }
        
        return oActivityElem;
    }
    
    /**Creates the <CODE>/OLPData/Resource/Controller/ToolProfile</CODE> XML 
     * element and all of its subelements and returns it.
     * @param oToolProfileListElement <CODE>ToolProfileList</CODE> XML element,
     * a parent of <CODE>ToolProfile</CODE> XML element to be created. The 
     * parent XML element was automatically created when this class constructor 
     * was invoked.
     * @param sToolProfileName name of the tool profile to be created
     * @param eToolType tool mobility type. Valid values are: <CODE>
     * ToolType.STATIONARY</CODE> and <CODE>ToolType.ON_ROBOT</CODE>.
     * @param daToolOffset tool offset with respect to robot's mount plate. This 
     * array must contain 6 elements, in this order: X, Y, Z, Yaw, Pitch, and 
     * Roll.
     * @param dMass mass of the tool in kilograms. If there is no knowledge 
     * about the tool mass for this tool profile, <CODE>null</CODE> reference 
     * should be specified as the value of this parameter.
     * @param daCentroid tool centroid position in meters. This array must 
     * contain 3 elements, in this order: Cx, Cy, and Cz, which represent 
     * centroid position in X, Y, and Z directions, respectively. If there is no 
     * knowledge about the tool centroid position for this tool profile, 
     * <CODE>null</CODE> reference should be specified as the value of this 
     * parameter.
     * @param daInertia tool inertia values in kilograms per meter squared units
     * . This array must contain 6 elements, in this order: Ixx, Ixy, Iyy, Iyz, 
     * Izx, and Izz. If there is no knowledge about the 
     * tool inertia values for this tool profile, <CODE>null</CODE> reference 
     * should be specified as the value of this parameter.
     * @return created <CODE>ToolProfile</CODE> XML element, or <CODE>null
     * </CODE> if any of the passed arguments are <CODE>null</CODE>, or <CODE>
     * null</CODE> if <CODE>sToolProfileName</CODE> is an empty string, or 
     * <CODE>null</CODE> if <CODE>daToolOffset</CODE> array does not have 
     * exactly 6 elements.
     * @see #DNBIgpOlpUploadBaseClass(String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.ToolType
     * @since V5R14
     */
    
    public Element createToolProfile(Element oToolProfileListElement, 
                                     String sToolProfileName, 
                                     DNBIgpOlpUploadEnumeratedTypes.
                                                             ToolType eToolType, 
                                     double [] daToolOffset,
                                     Double dMass,
                                     double [] daCentroid,
                                     double [] daInertia) {
                                     
        Element oToolProfileElement = null;
        
        if (null != oToolProfileListElement && null != sToolProfileName && 
                                               (!sToolProfileName.equals(""))) {
            oToolProfileElement = m_Document.createElement("ToolProfile");
            oToolProfileListElement.appendChild(oToolProfileElement);
            
            Element oToolProfileNameElement = m_Document.createElement("Name");
            oToolProfileElement.appendChild(oToolProfileNameElement);
            
            Text oToolProfileNameText = 
                                    m_Document.createTextNode(sToolProfileName);
            oToolProfileNameElement.appendChild(oToolProfileNameText);
            
            Element oToolTypeElement = m_Document.createElement("ToolType");
            oToolProfileElement.appendChild(oToolTypeElement);
            
            String sToolType;
            
            if(eToolType != null) {
                sToolType = eToolType.toString();
            }
            else {
                return null;
            }
            
            if (sToolType.equals("Stationary") || sToolType.equals("OnRobot") 
                                       || sToolType.equals("StationaryRobot")) {
                Text oToolTypeText = m_Document.createTextNode(sToolType);
                oToolTypeElement.appendChild(oToolTypeText);
            }
            else {
                return null;
            }
            
            Element oTCPOffsetElement = m_Document.createElement("TCPOffset");
            oToolProfileElement.appendChild(oTCPOffsetElement);

            Element oTCPPositionElement = 
                                        m_Document.createElement("TCPPosition");
            oTCPOffsetElement.appendChild(oTCPPositionElement);
            
            Element oTCPOrientationElement = 
                                     m_Document.createElement("TCPOrientation");
            oTCPOffsetElement.appendChild(oTCPOrientationElement);
            
            if (daToolOffset != null && daToolOffset.length == 
                                             NUMBER_OF_POS_AND_ORI_PARAMETERS) {
                oTCPPositionElement.setAttribute("X", new 
                                            Double(daToolOffset[0]).toString());
                oTCPPositionElement.setAttribute("Y", new 
                                            Double(daToolOffset[1]).toString());
                oTCPPositionElement.setAttribute("Z", new 
                                            Double(daToolOffset[2]).toString());
                
                oTCPOrientationElement.setAttribute("Yaw", new 
                                            Double(daToolOffset[3]).toString());
                oTCPOrientationElement.setAttribute("Pitch", new 
                                            Double(daToolOffset[4]).toString());
                oTCPOrientationElement.setAttribute("Roll", new 
                                            Double(daToolOffset[5]).toString());
            }
            else {
                return null;
            }
            
            if(dMass != null) {
                Element oMassElement = m_Document.createElement("Mass");
                oToolProfileElement.appendChild(oMassElement);
                
                oMassElement.setAttribute("MassValue", dMass.toString());
            }
            
            if(daCentroid != null && 
                           daCentroid.length == NUMBER_OF_CENTROID_PARAMETERS) {
                Element oCentroidElement = m_Document.createElement("Centroid");
                oToolProfileElement.appendChild(oCentroidElement);       
                
                Element oCentroidPositionElement = m_Document.
                                              createElement("CentroidPosition");
                oCentroidElement.appendChild(oCentroidPositionElement);
                
                oCentroidPositionElement.setAttribute("Cx", new 
                                              Double(daCentroid[0]).toString());
                oCentroidPositionElement.setAttribute("Cy", new 
                                              Double(daCentroid[1]).toString());
                oCentroidPositionElement.setAttribute("Cz", new
                                              Double(daCentroid[2]).toString());
            }
    
            if(daInertia != null && 
                           daInertia.length == NUMBER_OF_INERTIA_PARAMETERS) {
                Element oInertiaElement = m_Document.createElement("Inertia");
                oToolProfileElement.appendChild(oInertiaElement);       
                
                oInertiaElement.setAttribute("Ixx", new 
                                               Double(daInertia[0]).toString());
                oInertiaElement.setAttribute("Ixy", new 
                                               Double(daInertia[1]).toString());
                oInertiaElement.setAttribute("Iyy", new
                                               Double(daInertia[2]).toString());
                oInertiaElement.setAttribute("Iyz", new 
                                               Double(daInertia[3]).toString());
                oInertiaElement.setAttribute("Izx", new 
                                               Double(daInertia[4]).toString());
                oInertiaElement.setAttribute("Izz", new
                                               Double(daInertia[5]).toString());
            }
        }
        
        return oToolProfileElement;
        
    }
    
    /**Creates the <CODE>/OLPData/Resource/Controller/MotionProfile</CODE> 
     * XML element and all of its subelements, and returns it.
     * @param oMotionProfileListElement <CODE>MotionProfileList</CODE> XML 
     * element, a parent of <CODE>MotionProfile</CODE> XML element to be 
     * created. 
     * The parent XML element was automatically created when this class 
     * constructor was invoked.
     * @param sMotionProfileName name of the motion profile to be created
     * @param eMotionBasis motion basis type. Valid values are: <CODE>
     * MotionBasis.ABSOLUTE</CODE>, <CODE>MotionBasis.PERCENT</CODE> and 
     * <CODE>MotionBasis.TIME</CODE>.
     * @param dSpeed robot's tcp speed in:<BR>- <CODE>meter per second</CODE> 
     * units, if <CODE>eMotionBasis</CODE> argument is 
     * <CODE>MotionBasis.ABSOLUTE</CODE>,<BR>-<CODE>percent</CODE> units of the 
     * maximum allowed tcp speed, if <CODE>eMotionBasis</CODE> argument is 
     * <CODE>MotionBasis.PERCENT</CODE>,<BR>- <CODE>second</CODE> units, if 
     * <CODE>eMotionBasis</CODE> argument is <CODE>MotionBasis.TIME</CODE>
     * @param dAcceleration robot's tcp acceleration, always in <CODE>percent
     * </CODE> units of the maximum allowed tcp acceleration
     * @param dAngularSpeed robot's tcp angular speed, always in <CODE>percent
     * </CODE> units of the maximum allowed tcp angular speed
     * @param dAngularAcceleration robot's tcp angular acceleration, always in 
     * <CODE>percent</CODE> units of the maximum allowed tcp angular 
     * acceleration 
     * @return created <CODE>MotionProfile</CODE> XML element, or <CODE>null
     * </CODE> if any of the passed object arguments are <CODE>null</CODE>, or 
     * <CODE>null</CODE> if <CODE>sMotionProfileName</CODE> is an empty string
     * @see #DNBIgpOlpUploadBaseClass(String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.MotionBasis
     * @since V5R14
     */
    
    public Element createMotionProfile(Element oMotionProfileListElement, 
                                       String sMotionProfileName, 
                                       DNBIgpOlpUploadEnumeratedTypes.
                                                       MotionBasis eMotionBasis, 
                                       double dSpeed, 
                                       double dAcceleration,
                                       double dAngularSpeed, 
                                       double dAngularAcceleration) {
        Element oMotionProfileElement = null;
        
        if (oMotionProfileListElement != null && sMotionProfileName != null 
                                          && (!sMotionProfileName.equals(""))) {
            oMotionProfileElement = m_Document.createElement("MotionProfile");
            oMotionProfileListElement.appendChild(oMotionProfileElement);
            
            Element oMotionProfileNameElement = 
                                               m_Document.createElement("Name");
            oMotionProfileElement.appendChild(oMotionProfileNameElement);
            
            Text oMotionProfileNameText = 
                                  m_Document.createTextNode(sMotionProfileName);
            oMotionProfileNameElement.appendChild(oMotionProfileNameText);
            
            Element oMotionBasisElement = 
                                        m_Document.createElement("MotionBasis");
            oMotionProfileElement.appendChild(oMotionBasisElement);
            
            String sMotionBasis;
            
            if(null != eMotionBasis){
                sMotionBasis = eMotionBasis.toString();
            }
            else {
                return null;
            }
                
            if (sMotionBasis.equals("Percent") || 
                sMotionBasis.equals("Absolute") || 
                sMotionBasis.equals("Time")) {
                Text oMotionBasisText = m_Document.createTextNode(sMotionBasis);
                oMotionBasisElement.appendChild(oMotionBasisText);
            }
            else {
                return null;
            }
                        
            Element oSpeedElement = m_Document.createElement("Speed");
            oMotionProfileElement.appendChild(oSpeedElement);
            
            oSpeedElement.setAttribute("Value", new Double(dSpeed).toString());
                        
            Element oAccelElement = m_Document.createElement("Accel");
            oMotionProfileElement.appendChild(oAccelElement); 
            
            oAccelElement.setAttribute("Value", new 
                                              Double(dAcceleration).toString());
            
            Element oAngularSpeedElement = 
                                  m_Document.createElement("AngularSpeedValue");
            oMotionProfileElement.appendChild(oAngularSpeedElement); 
            
            oAngularSpeedElement.setAttribute("Value", 
                                          new Double(dAngularSpeed).toString());
            
            Element oAngularAccelElement = 
                                  m_Document.createElement("AngularAccelValue");
            oMotionProfileElement.appendChild(oAngularAccelElement);
            
            oAngularAccelElement.setAttribute("Value", 
                                   new Double(dAngularAcceleration).toString());
        }
        
        return oMotionProfileElement;
    }
    
    /**Creates the <CODE>/OLPData/Resource/Controller/ObjectFrameProfile</CODE> 
     * XML element and all of its subelements, and returns it.
     * @param oObjectFrameProfileListElement <CODE>ObjectFrameProfileList</CODE> 
     * XML element, a parent of <CODE>ObjectFrameProfile</CODE> XML element to 
     * be created. The parent XML element was automatically created when this 
     * class constructor was invoked.
     * @param sObjectFrameProfileName name of the object frame profile to be 
     * created
     * @param eObjectFrameType frame of reference for this object frame. 
     * Valid values are: <CODE>ObjectFrameType.ROBOT_BASE</CODE> and <CODE>
     * ObjectFrameType.WORLD</CODE>.
     * @param daObjectFrameOffset object frame offset with respect to frame of 
     * reference determined by the parameter above. This array must contain 6 
     * elements: X, Y, Z, Yaw, Pitch, and Roll respectively.
     * @param applyOffsetToTags boolean determines if object frame offset will 
     * be applied to tags during robot motion in V5.
     * @return created <CODE>ObjectFrameProfile</CODE> XML element, or <CODE>
     * null</CODE> if any of the passed arguments are <CODE>null</CODE>, or 
     * <CODE>null</CODE> if <CODE>sObjectFrameProfileName</CODE> is an empty 
     * string, or <CODE>null</CODE> if <CODE>daObjectFrameOffset</CODE> array 
     * does not have exactly 6 elements.
     * @see #DNBIgpOlpUploadBaseClass(String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType
     * @since V5R15
     */
    
    public Element createObjectFrameProfile(Element oObjectFrameProfileListElement, 
                                            String sObjectFrameProfileName, 
                                            DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType eObjectFrameType, 
                                            double [] daObjectFrameOffset,
                                            boolean applyOffsetToTags)
    {
        Element oObjectFrameProfileElement = null;
        
        oObjectFrameProfileElement = createObjectFrameProfile( oObjectFrameProfileListElement,
            sObjectFrameProfileName, eObjectFrameType, daObjectFrameOffset);
                    
        if (oObjectFrameProfileElement != null)
               { 
                    String applyOffsetStr = "Off";
            if (applyOffsetToTags == true)
                        applyOffsetStr = "On";

            oObjectFrameProfileElement.setAttribute("ApplyOffsetToTags", applyOffsetStr);
               }
        
        return oObjectFrameProfileElement;
    }
    
    /**Creates the <CODE>/OLPData/Resource/Controller/ObjectFrameProfile</CODE> 
     * XML element and all of its subelements, and returns it.
     * @param oObjectFrameProfileListElement <CODE>ObjectFrameProfileList</CODE> 
     * XML element, a parent of <CODE>ObjectFrameProfile</CODE> XML element to 
     * be created. The parent XML element was automatically created when this 
     * class constructor was invoked.
     * @param sObjectFrameProfileName name of the object frame profile to be 
     * created
     * @param eObjectFrameType frame of reference for this object frame. 
     * Valid values are: <CODE>ObjectFrameType.ROBOT_BASE</CODE> and <CODE>
     * ObjectFrameType.WORLD</CODE>.
     * @param daObjectFrameOffset object frame offset with respect to frame of 
     * reference determined by the parameter above. This array must contain 6 
     * elements: X, Y, Z, Yaw, Pitch, and Roll respectively.
     * @return created <CODE>ObjectFrameProfile</CODE> XML element, or <CODE>
     * null</CODE> if any of the passed arguments are <CODE>null</CODE>, or 
     * <CODE>null</CODE> if <CODE>sObjectFrameProfileName</CODE> is an empty 
     * string, or <CODE>null</CODE> if <CODE>daObjectFrameOffset</CODE> array 
     * does not have exactly 6 elements.
     * @see #DNBIgpOlpUploadBaseClass(String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType
     * @since V5R14
     */
    
    public Element createObjectFrameProfile(Element 
                                                 oObjectFrameProfileListElement, 
                                            String sObjectFrameProfileName, 
                                            DNBIgpOlpUploadEnumeratedTypes.
                                               ObjectFrameType eObjectFrameType, 
                                            double [] daObjectFrameOffset) {
        Element oObjectFrameProfileElement = null;
        
        if (oObjectFrameProfileListElement != null && sObjectFrameProfileName 
                             != null && (!sObjectFrameProfileName.equals(""))) {
            oObjectFrameProfileElement = 
                                 m_Document.createElement("ObjectFrameProfile");
            oObjectFrameProfileListElement.
                                        appendChild(oObjectFrameProfileElement);
            
            Element oObjectFrameProfileNameElement = 
                                               m_Document.createElement("Name");
            oObjectFrameProfileElement.
                                    appendChild(oObjectFrameProfileNameElement);
            
            Text oObjectFrameProfileNameText = 
                             m_Document.createTextNode(sObjectFrameProfileName);
            oObjectFrameProfileNameElement.
                                       appendChild(oObjectFrameProfileNameText);
 
            Element oObjectFrameElement = 
                                        m_Document.createElement("ObjectFrame");
            oObjectFrameProfileElement.appendChild(oObjectFrameElement);
            
            String sObjectFrameType;
            
            if(eObjectFrameType != null) {
                sObjectFrameType = eObjectFrameType.toString();
            }
            else {
                return null;
            }
            
            if (sObjectFrameType.equals("World") || 
                                         sObjectFrameType.equals("RobotBase")) {
                oObjectFrameElement.setAttribute("ReferenceFrame", 
                                                 sObjectFrameType);
            }
            else {
                return null;
            }
            
            Element oObjectFramePositionElement = 
                                        m_Document.createElement("ObjectFramePosition");
            oObjectFrameElement.appendChild(oObjectFramePositionElement);
            
            Element oObjectFrameOrientationElement = 
                                     m_Document.createElement("ObjectFrameOrientation");
            oObjectFrameElement.appendChild(oObjectFrameOrientationElement);
            
            if (daObjectFrameOffset != null && daObjectFrameOffset.length == 
                                             NUMBER_OF_POS_AND_ORI_PARAMETERS) {
                oObjectFramePositionElement.setAttribute("X", 
                                 new Double(daObjectFrameOffset[0]).toString());
                oObjectFramePositionElement.setAttribute("Y", 
                                 new Double(daObjectFrameOffset[1]).toString());
                oObjectFramePositionElement.setAttribute("Z", 
                                 new Double(daObjectFrameOffset[2]).toString());
                
                oObjectFrameOrientationElement.setAttribute("Yaw", 
                                 new Double(daObjectFrameOffset[3]).toString());
                oObjectFrameOrientationElement.setAttribute("Pitch", 
                                 new Double(daObjectFrameOffset[4]).toString());
                oObjectFrameOrientationElement.setAttribute("Roll", 
                                 new Double(daObjectFrameOffset[5]).toString());
            }
            else {
                return null;
            }
        }
        
        return oObjectFrameProfileElement;
        
    }
    
    /**Creates the <CODE>/OLPData/Resource/Controller/AccuracyProfile</CODE> 
     * XML element and all of its subelements, and returns it.
     * @param oAccuracyProfileListElement <CODE>AccuracyProfileList</CODE> XML 
     * element, a parent of <CODE>AccuracyProfile</CODE> XML element to be 
     * created. The parent XML element was automatically created when this class
     * constructor was invoked.
     * @param sAccuracyProfileName name of the accuracy profile to be created
     * @param eAccuracyType rounding criterion
     * Valid values are: <CODE>AccuracyType.DISTANCE</CODE> and <CODE>
     * AccuracyType.SPEED</CODE>.
     * @param bIsFlyByOn boolean that switches rounding on or off. <CODE>true
     * </CODE> value turns rounding on, while <CODE>false</CODE> value turns 
     * rounding off.
     * @param dAccuracyValue accuracy value in <CODE>meter</CODE> units, if 
     * <CODE>eAccuracyType</CODE> equals <CODE>DISTANCE</CODE> (designates 
     * distance to the target when rounding needs to begin), or in <CODE>percent
     * </CODE> units, if <CODE>eAccuracyType</CODE> equals <CODE>SPEED</CODE> 
     * (designates percentage of the programmed speed, measured during 
     * deceleration, before robot reaches the target, when rounding needs to 
     * begin).
     * @return created <CODE>AccuracyProfile</CODE> XML element, or <CODE>
     * null</CODE> if any of the passed object arguments are <CODE>null</CODE>, 
     * or <CODE>null</CODE> if <CODE>sAccuracyProfileName</CODE> is an empty 
     * string.
     * @see #DNBIgpOlpUploadBaseClass(String [])
     * @see DNBIgpOlpUploadEnumeratedTypes.AccuracyType
     * @since V5R14
     */
    
     public Element createAccuracyProfile(Element oAccuracyProfileListElement, 
                                          String sAccuracyProfileName, 
                                          DNBIgpOlpUploadEnumeratedTypes.
                                                     AccuracyType eAccuracyType, 
                                          boolean bIsFlyByOn, 
                                          double dAccuracyValue) {
        Element oAccuracyProfileElement = null;
        
        if (oAccuracyProfileListElement != null && sAccuracyProfileName != null 
                                        && (!sAccuracyProfileName.equals(""))) {
            oAccuracyProfileElement = 
                                    m_Document.createElement("AccuracyProfile");
            oAccuracyProfileListElement.appendChild(oAccuracyProfileElement);
            
            Element oAccuracyProfileNameElement = 
                                               m_Document.createElement("Name");
            oAccuracyProfileElement.appendChild(oAccuracyProfileNameElement);
            
            Text oAccuracyProfileNameText = 
                                m_Document.createTextNode(sAccuracyProfileName);
            oAccuracyProfileNameElement.appendChild(oAccuracyProfileNameText);
            
            Element oFlyByModeElement = m_Document.createElement("FlyByMode");
            oAccuracyProfileElement.appendChild(oFlyByModeElement);
            
            if (bIsFlyByOn == true) {
                Text oFlyByModeText = m_Document.createTextNode("On");
                oFlyByModeElement.appendChild(oFlyByModeText);
            }
            else {
                Text oFlyByModeText = m_Document.createTextNode("Off");
                oFlyByModeElement.appendChild(oFlyByModeText);
            }
            
            Element oAccuracyTypeElement = 
                                       m_Document.createElement("AccuracyType");
            oAccuracyProfileElement.appendChild(oAccuracyTypeElement);
            
            String sAccuracyType;
            
            if(eAccuracyType != null) {
               sAccuracyType = eAccuracyType.toString();
            }
            else {
                return null;
            }
            
            if (sAccuracyType.equals("Speed") || 
                                             sAccuracyType.equals("Distance")) {
                Text oAccuracyTypeText = 
                                       m_Document.createTextNode(sAccuracyType);
                oAccuracyTypeElement.appendChild(oAccuracyTypeText);
            }
            else {
                return null;
            }
                        
            Element oAccuracyValueElement = 
                                      m_Document.createElement("AccuracyValue");
            oAccuracyProfileElement.appendChild(oAccuracyValueElement);
            
            oAccuracyValueElement.setAttribute("Value", 
                                         new Double(dAccuracyValue).toString());
            if (sAccuracyType.equals("Speed")) {
                oAccuracyValueElement.setAttribute("Units", "%");
            }
            else {
                oAccuracyValueElement.setAttribute("Units", "m");                
            }
         }
        
         return oAccuracyProfileElement;
    }

    /**Creates the <CODE>/OLPData/Resource/Controller/UserProfile</CODE> 
     * XML element and all of its subelements, and returns it.
     * @param oUserProfileListElement <CODE>oUserProfileList</CODE> 
     * XML element, a parent of <CODE>UserProfile</CODE> XML element to 
     * be created. The parent XML element was automatically created when this 
     * class constructor was invoked.
     * @param sUserProfileType any supported Applicative or Device parameter 
     * profile type, assigned by the user to robot controller
     * @param sUserProfileName name of the instance of Applicative or Device 
     * parameter profile to be created
     * @param llaAttributeData array of linked list elements, each representing
     * the user profile attribute. Each attribute must contain exactly 3 
     * elements. Attribute elements are (in this order): <CODE>attribute name
     * </CODE> (index 0), <CODE>attribute data type</CODE> (index 1), and 
     * <CODE>attribute value</CODE> (index 2).<CODE>attribute data type</CODE>
     * is any acceptable V5 data type (Integer, String, Double, Real, Length, 
     * Boolean, Mass, Angle, Time, Volume, Area, etc). Executing <CODE>f(x)
     * </CODE> command in V5 brings a dialog box that lists all the acceptable 
     * attribute types. <CODE>attribute value</CODE> may be represented either 
     * with a single value, or with a string composed of comma delimited 
     * multiple values.
     * @return created <CODE>UserProfile</CODE> XML element, or <CODE>
     * null</CODE> if any of the passed arguments are <CODE>null</CODE>, or 
     * <CODE>null</CODE> if <CODE>sUserProfileName</CODE> is an empty 
     * string, or <CODE>null</CODE> if <CODE>sUserProfileType</CODE> is an empty 
     * string, or <CODE>null</CODE> if every element of 
     * <CODE>llaAttributeData</CODE> linked list does not have exactly 3 
     * elements.
     * @see #DNBIgpOlpUploadBaseClass(String [])
     * @since V5R15
     */
     
     public Element createUserProfile(Element oUserProfileListElement,
                                      String sUserProfileType,  
                                      String sUserProfileName, 
                                      LinkedList [] llaAttributeData) {

        Element oUserProfileElement = null;

        if(sUserProfileType != null && (!(sUserProfileType.equals(""))) &&
           sUserProfileName != null && (!(sUserProfileName.equals(""))) && oUserProfileListElement != null) {
               
           NodeList oUserProfileList = 
                oUserProfileListElement.getElementsByTagName("UserProfile");

           if(oUserProfileList != null && oUserProfileList.getLength() > 0){

               for(int ii = 0; ii < oUserProfileList.getLength(); ii++) {
                   Node oUserProfileNode = oUserProfileList.item(ii);

                   if(oUserProfileNode != null) {
                       Element oExistingUserProfileElement = 
                                                 (Element) oUserProfileNode;
                        if(oExistingUserProfileElement != null)
                       {
                           String sExistingProfileType = 
                           oExistingUserProfileElement.getAttribute("Type");

                           if(sExistingProfileType.equals(sUserProfileType))
                           {
                               oUserProfileElement = 
                                                oExistingUserProfileElement;
                               break;
                           }
                       }
                   }
               }
            }                  

           if(oUserProfileElement == null) {
               oUserProfileElement = 
                                m_Document.createElement("UserProfile");
               oUserProfileListElement.appendChild(oUserProfileElement);
             oUserProfileElement.setAttribute("Type", sUserProfileType);
           }

           if(null != oUserProfileElement){
               Element oUserProfileInstanceElement = 
                        m_Document.createElement("UserProfileInstance");
               oUserProfileElement.
                               appendChild(oUserProfileInstanceElement);
               oUserProfileInstanceElement.
                                 setAttribute("Name", sUserProfileName);

                for(int jj = 0; jj < llaAttributeData.length; jj++) {

                    LinkedList llAttribute = llaAttributeData[jj];

                    if(llAttribute.size() == 
                                     NUM__USER_PROFILE_ATT_PARAMETERS) {

                        Element oAttributeElement =  
                       m_Document.createElement("UserDefinedAttribute");
                        oUserProfileInstanceElement.
                                         appendChild(oAttributeElement);                            

                        String sDisplayName = 
                                          llAttribute.get(0).toString();
                        String sDataType = 
                                          llAttribute.get(1).toString();
                        String sValue = llAttribute.get(2).toString();

                        Element oDispNameElem = 
                                m_Document.createElement("DisplayName");
                        oAttributeElement.appendChild(oDispNameElem); 

                        Text oDispNameText = 
                                m_Document.createTextNode(sDisplayName);
                        oDispNameElem.appendChild(oDispNameText);

                        Element oDataTypeElem = 
                                m_Document.createElement("DataType");
                        oAttributeElement.appendChild(oDataTypeElem); 

                        Text oDataTypeText = 
                                m_Document.createTextNode(sDataType);
                        oDataTypeElem.appendChild(oDataTypeText);

                        Element oValueElem = 
                                m_Document.createElement("Value");
                        oAttributeElement.appendChild(oValueElem); 

                        Text oValueText = 
                                m_Document.createTextNode(sValue);
                        oValueElem.appendChild(oValueText);
                    }
                }
           }
        }
               
        return oUserProfileElement;
     }
    
    /**Returns created <CODE>Activity</CODE> XML element of type 
     * <CODE>MoveHomeActivity</CODE>, which may appear in Device Tasks only. It
     * is illegal to place this activity within a Robot Task.
     * <P>All the sub-elements of the returned element will be created, except 
     * <CODE>AttributeList</CODE> element, which needs to be created explicitly.
     * Based on this XML description a Move To Device Home Activity will be 
     * created in a selected Device Task in V5.
     * @param oActivityListElem parent XML element, named 
     * <CODE>ActivityList</CODE>  
     * @param sActivityName name of this activity, as it will appear in V5 Igrip
     * @param sHomeName name of selected device's home position.
     * @return created <CODE>Activity</CODE> XML element, or <CODE>null</CODE>,
     * if any of the passed arguments is a <CODE>null</CODE> reference, or 
     * <CODE>null</CODE>, if either <CODE>sActivityName</CODE> or 
     * <CODE>sCalledTaskName</CODE> argument is an empty string.
     * @see #createActivityList(String)
     * @see #createAttributeList(Element, String [], String [])
     * @since V5R15
     */
    
    public Element createMoveHomeActivityInDeviceTask(Element oActivityListElem, 
                                                      String sActivityName, 
                                                      String sHomeName) {
        Element oActivityElem = createActivityHeader
                      (oActivityListElem, sActivityName, "MoveHomeActivity");
        
        if (oActivityElem != null && sHomeName != null && 
                                               (! sHomeName.equals(""))) {
            Element oMoveToHomeElem = m_Document.createElement("MoveToHome");
            oActivityElem.appendChild(oMoveToHomeElem);
            Text oMoveToHomeText = m_Document.createTextNode(sHomeName);
            oMoveToHomeElem.appendChild(oMoveToHomeText);
        }
        
        return oActivityElem;
    }
   
    /** Sets the current line tracking type 
     * @param eTrackType rounding criterion
     * Valid values are: <CODE>TrackType.DELTA_POS</CODE>, <CODE>
     * TrackType.TEACH_POS</CODE> and <CODE>TrackType.TRACK_OFF</CODE>.
     * @since V5R19SP5
     */  
    public void setTrackStatus(DNBIgpOlpUploadEnumeratedTypes.TrackType eTrackType)
    {
        m_TrackType = eTrackType;
    }
    
    /** Transforms created DOM Document into an XML stream, and then writes that
     * stream to an output file. <P>By default, the output file is named 
     * "simImport.xml", and is located in system's temporary directory. The file
     * path to the output file can be retrieved by calling 
     * <CODE>getPathToXMLOutputFile()</CODE> method of this class.<P>This method
     * should be overriden in a derived class, if the intention is to change the
     * name and/or location of the output file.
     * @throws TransformerConfigurationException may throw this exception
     * during the parse when it is constructing the Templates object and fails
     * @throws FileNotFoundException if the output file exists, but is a 
     * directory rather than a regular file, does not exist but cannot be 
     * created, or cannot be opened for any other reason 
     * @throws TransformerException if an unrecoverable error occurs during the 
     * course of transformation of DOM doucument to XML stream
     * @throws IOException if an I/O error occurs, while closing the output file 
     * @see #getPathToXMLOutputFile()
     * @since V5R14
     */
    
    public void writeXMLStreamToFile() throws IOException, 
                                              FileNotFoundException, 
                                              TransformerException, 
                                              TransformerConfigurationException{
        //Create Transformer to save DOM document to a file
        TransformerFactory oTransformerFactory = 
                                               TransformerFactory.newInstance();
        Transformer oTransformer = oTransformerFactory.newTransformer();
        oTransformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, 
                                                                         "yes");
        //Create a DOMSource out of DOM Document
        DOMSource oDOMSource = new DOMSource(m_Document);
        //Create an xml output file and assign it to an output stream
        File oXMLOutputFile = new File(m_PathToXMLOutputFile);
        FileOutputStream oFileOutputStream = new 
                                               FileOutputStream(oXMLOutputFile);
        //Create a result stream in which DOMSource document will be saved to
        StreamResult oStreamResult = new StreamResult(oFileOutputStream);
        //Save the DOM document to the file
        oTransformer.transform(oDOMSource, oStreamResult);
        //Close the FileOutputStream
        oFileOutputStream.close();
    }
}
