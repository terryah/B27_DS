package com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools;

/** This class lists all the enumerated types important from the aspect of V5 
 * OLP uploader creation. <P>Using enumerated types instead of their string 
 * representations allows that only supported values will be processed, and all 
 * the unsupported values will be caught at compile time.<P>
 * Since Java doesn't have enumerated data types, series of member classes have 
 * been created that virtually serve the same purpose. None of the member 
 * classes can be extended, nor can they be instantiated. Each of the classes 
 * has an overridden method <CODE>toString()</CODE> that extrapolates string
 * representation of enumerated values. 
 * @author AOH
 * @version V5R14 12/12/2003
 * @see DNBIgpOlpUploadBaseClass#DNBIgpOlpUploadBaseClass(String [])
 * @since V5R14
 */

public class DNBIgpOlpUploadEnumeratedTypes {
    
    /** Enumeration that defines supported tool types from the perspective of 
     * their mobility and location.<P>
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createToolProfile(Element, String, 
     * DNBIgpOlpUploadEnumeratedTypes.ToolType, double [], Double, double [],
     * double [])
     */
    public static final class ToolType {
        private String id;
        private ToolType(String anID) {this.id = anID; }
        
        /**Tool is mounted on the robot, and it moves along with it.
         * @since V5R14
         */
        public static final ToolType ON_ROBOT = new ToolType("OnRobot");
        
        /**Tool is detached from the robot and is fixed in space. 
         * @since V5R14
         */
        public static final ToolType STATIONARY = new ToolType("Stationary");

        /**Tool is detached from the robot and is fixed in space. 
         * @since V5R14
         */
        public static final ToolType STATIONARY_ROBOT = new ToolType("StationaryRobot");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "OnRobot" and "Stationary".
         *@return either "OnRobot" or "Stationary" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported units in which robot's tcp speed can 
     * be expressed.<P>
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createMotionProfile(Element, String, 
     * DNBIgpOlpUploadEnumeratedTypes.MotionBasis, double, double, double, 
     * double)
     */
    
    public static final class MotionBasis {
        private String id;
        private MotionBasis(String anID) {this.id = anID; }
        
        /**Robot's tcp speed is expressed as a percentage of the robot's maximum 
         * speed. 
         * @since V5R14
         */
        public static final MotionBasis PERCENT = new MotionBasis("Percent");
        
        /**Robot's tcp speed is expressed in <CODE>Meters per Second</CODE>.
         * @since V5R14
         */
        public static final MotionBasis ABSOLUTE = new MotionBasis("Absolute");
        
        /**Robot's tcp speed is calculated based on this motion time in seconds.
         * @since V5R14
         */
        public static final MotionBasis TIME = new MotionBasis("Time");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "Percent", "Absolute" and "Time".
         *@return either "Percent", "Absolute", and "Time" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported fly-by criteria to be used during 
     * rounding.<P>
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createAccuracyProfile(Element, String, 
     * DNBIgpOlpUploadEnumeratedTypes.AccuracyType, boolean, double)
     */
        
    public static final class AccuracyType {
        private String id;
        private AccuracyType(String anID) {this.id = anID; }
        
        /**Percentage of the programmed speed, measured during deceleration, 
         * before robot reaches the target, when rounding needs to begin.
         * @since V5R14
         */
        public static final AccuracyType SPEED = new AccuracyType("Speed");
        
        /**Distance, in meters, to the target when rounding needs to begin. 
         * @since V5R14
         */
        public static final AccuracyType DISTANCE = 
                                                   new AccuracyType("Distance");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "Speed" and "Distance".
         *@return either "Speed" or "Distance" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported frames of reference for object frames
     * (also known as user frames or UFRAMEs).<P>
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createObjectFrameProfile(Element, String, 
     * DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType, double [], boolean applyOffsetToTags)
     */
            
    public static final class ObjectFrameType {
        private String id;
        private ObjectFrameType(String anID) {this.id = anID; }
        
        /**World coordinate system. This is the frame of reference for 
         * corresponding object frame. 
         * @since V5R14
         */
        public static final ObjectFrameType WORLD = 
                                                   new ObjectFrameType("World");
        
        /**Robot's base coordinate system. This is the frame of reference for 
         * corresponding object frame. 
         * @since V5R14
         */
        public static final ObjectFrameType ROBOT_BASE = 
                                               new ObjectFrameType("RobotBase");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "World" and "RobotBase".
         *@return either "World" or "RobotBase" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported coordinates spaces in which robot 
     * targets can be defined.
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createTarget(Element,
     * DNBIgpOlpUploadEnumeratedTypes.TargetType, boolean)
     */
    
    public static final class TargetType {
        private String id;
        private TargetType(String anID) {this.id = anID; }
        
        /**Robot motion target defined in Cartesian coordinate space.
         * @since V5R14
         */
        public static final TargetType CARTESIAN = new TargetType("Cartesian");
        
        /**Robot motion target defined in Joint coordinate space.
         * @since V5R14
         */
        public static final TargetType JOINT = new TargetType("Joint");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "Cartesian" and "Joint".
         *@return either "Cartesian" or "Joint" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported motion interpolation types.
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createMotionAttributes(Element, String, 
     * String, String, String, DNBIgpOlpUploadEnumeratedTypes.MotionType)
     */
        
    public static final class MotionType {
        private String id;
        private MotionType(String anID) {this.id = anID; }
        
        /**Linear motion interpolation algorithm used to calculate the 
         * trajectory between start and target position.
         * @since V5R14
         */
        public static final MotionType LINEAR_MOTION = new MotionType("Linear");
        
        /**Joint motion interpolation algorithm used to calculate the 
         * trajectory between start and target position.
         * @since V5R14
         */
        public static final MotionType JOINT_MOTION = new MotionType("Joint");
 
        /**CircularVia motion interpolation algorithm used to calculate the 
         * trajectory between start and via position.
         * @since V5R15
         */
        public static final MotionType CIRCULARVIA_MOTION = new MotionType("CircularVia");
        
        /**Circular motion interpolation algorithm used to calculate the 
         * trajectory between via and target position.
         * @since V5R15
         */
        public static final MotionType CIRCULAR_MOTION = new MotionType("Circular");
                
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "Linear", "Joint", "CircularVia" and "Circular".
         *@return either "Linear", "Joint", "CircularVia" or "Circular" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported orientation interpolation modes.
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createMotionAttributes(Element, String, 
     * String, String, String, DNBIgpOlpUploadEnumeratedTypes.MotionType,
     * DNBIgpOlpUploadEnumeratedTypes.OrientMode)
     */
    
    public static final class OrientMode {
        private String id;
        private OrientMode(String anID) {this.id = anID; }
        
        /**Orientation interpolation mode is one-axis, or one-angle.
         * @since V5R14
         */
        public static final OrientMode ONE_AXIS = new OrientMode("1_Axis");
        
        /**Orientation interpolation mode is two-axis, or two-angle.
         * @since V5R14
         */
        public static final OrientMode TWO_AXIS = new OrientMode("2_Axis");
        
        /**Orientation interpolation mode is three-axis, or three-angle.
         * @since V5R14
         */
        public static final OrientMode THREE_AXIS = new OrientMode("3_Axis");
        
        /**Orientation interpolation mode is wrist-in-joint (wrist axes 
         * are interpolated in joint coordinate space).
         * @since V5R14
         */
        public static final OrientMode WRIST = new OrientMode("Wrist");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "1_Axis", "2_Axis", "3_Axis", and "Wrist".
         *@return either "1_Axis", "2_Axis", "3_Axis", and "Wrist" values
         *@since V5R14
         */
        public String toString() {return this.id; }
       
    }
    
    /** Enumeration that defines supported robot joint types.
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createJointTarget(Element, ArrayList []) 
     * @see DNBIgpOlpUploadBaseClass#getAxisTypes()
     */
    
    public static final class DOFType {
        private String id;
        private DOFType(String anID) {this.id = anID; }
        
        /**Robot joint's only unrestricted type of motion is rotation.
         * @since V5R14
         */
        public static final DOFType ROTATIONAL = new DOFType("Rotational");
        
        /**Robot joint's only unrestricted type of motion is translation.
         * @since V5R14
         */
        public static final DOFType TRANSLATIONAL = 
                                                   new DOFType("Translational");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "Rotational" and "Translational".
         *@return either "Rotational" or "Translational" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported auxiliary axis types.
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createJointTarget(Element, ArrayList []) 
     * @see DNBIgpOlpUploadBaseClass#getNumberOfRailAuxiliaryAxes()
     * @see DNBIgpOlpUploadBaseClass#getNumberOfToolAuxiliaryAxes()
     * @see 
     * DNBIgpOlpUploadBaseClass#getNumberOfWorkpiecePositionerAuxiliaryAxes()
     */
    
    public static final class AuxAxisType {
        private String id;
        private AuxAxisType(String anID) {this.id = anID; }
        
        /**End of arm tooling defined as robot's auxiliary axis.
         * @since V5R14
         */
        public static final AuxAxisType END_OF_ARM_TOOLING = 
                                             new AuxAxisType("EndOfArmTooling");
        
        /**Rail or track defined as robot's auxiliary axis.
         * @since V5R14
         */
        public static final AuxAxisType RAIL_TRACK_GANTRY = 
                                             new AuxAxisType("RailTrackGantry");
        
        /**Workpiece positioner defined as robot's auxiliary axis.
         * @since V5R14
         */
        public static final AuxAxisType WORKPIECE_POSITIONER = 
                                         new AuxAxisType("WorkpiecePositioner");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "EndOfArmTooling", "RailTrackGantry" and
         * "WorkpiecePositioner".
         *@return either "EndOfArmTooling", "RailTrackGantry", and
         * "WorkpiecePositioner" values
         *@since V5R14
         */
        public String toString() {return this.id; }
        
    }
    
    /** Enumeration that defines supported values for turn signs.
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createTurnSigns(Element,
     * DNBIgpOlpUploadEnumeratedTypes.TurnSign [])
     */
    
    public static final class TurnSign {
        private String id;
        private TurnSign(String anID) {this.id = anID; }
        
        /**Turn sign specifying non-negative DOF value.
         * @since V5R14
         */
        public static final TurnSign PLUS = new TurnSign("+");
        
        /**Turn sign specifying non-positive DOF value.
         * @since V5R14
         */
        
        public static final TurnSign MINUS = new TurnSign("-");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "+" and "-".
         *@return either "+" or "-" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    /** Enumeration that defines supported states of robot interference zones.
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author AOH
     * @version V5R14 12/12/2003
     * @see DNBIgpOlpUploadBaseClass#createZoneActivity(Element, String,
     * DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType, String)
     */
    
    public static final class ZoneActivityType {
        private String id;
        private ZoneActivityType(String anID) {this.id = anID; }
        
        /**Robot enters monitored interference zone.
         * @since V5R14
         */
        public static final ZoneActivityType ENTER_ZONE = new 
                                       ZoneActivityType("DNBEnterZoneActivity");
        
        /**Robot clears monitored interference zone.
         * @since V5R14
         */
        public static final ZoneActivityType CLEAR_ZONE = 
                                   new ZoneActivityType("DNBClearZoneActivity");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "DNBEnterZoneActivity" and "DNBClearZoneActivity".
         *@return either "DNBEnterZoneActivity" or "DNBClearZoneActivity" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }
    
    public static final class LogicOperatorType{
        private String id;
        private LogicOperatorType(String anID) {this.id = anID; }
        
        /**Robot enters monitored interference zone.
         * @since V5R14
         */
        public static final LogicOperatorType OR = new LogicOperatorType("or");
        
        /**Robot clears monitored interference zone.
         * @since V5R14
         */
        public static final LogicOperatorType AND = new LogicOperatorType("and");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "DNBEnterZoneActivity" and "DNBClearZoneActivity".
         *@return either "DNBEnterZoneActivity" or "DNBClearZoneActivity" values
         *@since V5R14
         */
        public String toString() {return this.id; }
    }    
    /** Enumeration that defines supported track types from the perspective of 
     * how the rail axes is used.<P>
     * Java doesn't have enumerated data types, therefore this class mimics that 
     * behavior.
     * @author DLY
     * @version V5R19SP5 05/03/2009
     * @see DNBIgpOlpUploadBaseClass#SetTrackStatus( 
     * DNBIgpOlpUploadEnumeratedTypes.TrackType)
     */
     public static final class TrackType {
        private String id;
        private TrackType(String anID) {this.id = anID; }
        
        /**Tracking upload uses relative joint value for rail axes.
         * @since V5R19SP5
         */
        public static final TrackType DELTA_POS = new TrackType("DeltaPos");
        
        /**Tracking upload uses absolute taught joint value for rail axes. 
         * @since V5R19SP5
         */
        public static final TrackType TEACH_POS = new TrackType("TeachPos");

        /**Tracking is turned off. 
         * @since V5R19SP5
         */
        public static final TrackType TRACK_OFF = new TrackType("TrackOff");
        
        /**Returns string representation of this class' static final fields.
         *<P>Valid values are "DeltaPos", "TeachPos" and "TrackOff".
         *@return either "DeltaPos", "TeachPos" or "TrackOff" values
         *@since V5R19SP5
         */
        public String toString() {return this.id; }
    }   
}

