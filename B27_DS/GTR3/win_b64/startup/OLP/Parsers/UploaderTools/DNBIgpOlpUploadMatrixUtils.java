package com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools;


import java.util.*;
import java.text.DecimalFormat;

public class DNBIgpOlpUploadMatrixUtils
{
        private double [][] dgMatrix;
        private double [][] dgCatMatrix;
        private double [][] dgInvertedMatrix;
        private double [][] tmpMatrix;
        private double cr;
        private double sr;
        private double cp;
        private double sp;
        private double cy;
        private double sy;
        private double xx;
        private double yy;
        private double zz;
        private double yaw;
        private double pitch;
        private double roll;
        private double q1;
        private double q2;
        private double q3;
        private double q4;
        private String [] components;
        private static final double TO_DEG = 180.0/Math.PI, TO_RAD = Math.PI/180.0;
        
        
        public DNBIgpOlpUploadMatrixUtils() {
                String xyzypr = "0.0 0.0 0.0 0.0 0.0 0.0";
                dgMatrix = new double[4][4];
                dgCatMatrix = new double[4][4];
                dgInvertedMatrix = new double[4][4];
                tmpMatrix = new double[4][4];
                components = xyzypr.split("\\t");
        }
/* main only for testing purposes
          public static void main(String [] parameters) {
            
              
              DNBIgpOlpUploadMatrixUtils matutil = new DNBIgpOlpUploadMatrixUtils();
              
              matutil.dgXyzyprToMatrix( parameters[0] );
             
              matutil.dgDisplayResult( "Input: ");
                            
              matutil.dgInvert();
              
              matutil.dgDisplayResult( "Inverted: ");             
              
              matutil.dgCatXyzyprMatrix( parameters[0] );
              
              matutil.dgDisplayResult( "Cat: ");
              
              
        }
*/
              
        public String dgXyzyprToMatrix(String XYZYPR)
        {
            components = XYZYPR.split(",");
            doTrig();
            dgMatrix[0][0] = cp*cr;
            dgMatrix[0][1] = cp*sr;
            dgMatrix[0][2] = -sp;
            dgMatrix[0][3] = 0.0;
            dgMatrix[1][0] = -cy*sr + sy*sp*cr;
            dgMatrix[1][1] = cy*cr + sy*sp*sr;
            dgMatrix[1][2] = sy*cp;
            dgMatrix[1][3] = 0.0;
            dgMatrix[2][0] = sy*sr + cy*sp*cr;
            dgMatrix[2][1] = -sy*cr + cy*sp*sr;
            dgMatrix[2][2] = cy*cp;
            dgMatrix[2][3] = 0.0;
            dgMatrix[3][0] = xx;
            dgMatrix[3][1] = yy;
            dgMatrix[3][2] = zz;
            dgMatrix[3][3] = 1.0;
            String result = dgBuildResult("dgXyzyprToMatrix result: ");
            return(result);
        }
        
        private  void doTrig()
        {
            xx = Double.valueOf(components[0]).doubleValue();
            yy = Double.valueOf(components[1]).doubleValue();
            zz = Double.valueOf(components[2]).doubleValue();
            yaw = Double.valueOf(components[3]).doubleValue();
            pitch = Double.valueOf(components[4]).doubleValue();
            roll = Double.valueOf(components[5]).doubleValue();
            
            cr = Math.cos(roll*Math.PI/180.0);
            sr = Math.sin(roll*Math.PI/180.0);
            cp = Math.cos(pitch*Math.PI/180.0);
            sp = Math.sin(pitch*Math.PI/180.0);
            cy = Math.cos(yaw*Math.PI/180.0);
            sy = Math.sin(yaw*Math.PI/180.0);
        }
        
        public String dgInvert() {
            dgInvertedMatrix[0][0] = dgMatrix[0][0];
            dgInvertedMatrix[1][0] = dgMatrix[0][1];
            dgInvertedMatrix[2][0] = dgMatrix[0][2];
            
            dgInvertedMatrix[0][1] = dgMatrix[1][0];
            dgInvertedMatrix[1][1] = dgMatrix[1][1];
            dgInvertedMatrix[2][1] = dgMatrix[1][2];
            
            dgInvertedMatrix[0][2] = dgMatrix[2][0];
            dgInvertedMatrix[1][2] = dgMatrix[2][1];
            dgInvertedMatrix[2][2] = dgMatrix[2][2];
            
            dgInvertedMatrix[0][3] = 0.0;
            dgInvertedMatrix[1][3] = 0.0;            
            dgInvertedMatrix[2][3] = 0.0;
            dgInvertedMatrix[3][3] = 1.0;     
            
            dgInvertedMatrix[3][0] = -dgInvertedMatrix[0][0]*dgMatrix[3][0]
                                     -dgInvertedMatrix[1][0]*dgMatrix[3][1]
                                     -dgInvertedMatrix[2][0]*dgMatrix[3][2];
            
            dgInvertedMatrix[3][1] = -dgInvertedMatrix[0][1]*dgMatrix[3][0]
                                     -dgInvertedMatrix[1][1]*dgMatrix[3][1]
                                     -dgInvertedMatrix[2][1]*dgMatrix[3][2];
            
            dgInvertedMatrix[3][2] = -dgInvertedMatrix[0][2]*dgMatrix[3][0]
                                     -dgInvertedMatrix[1][2]*dgMatrix[3][1]
                                     -dgInvertedMatrix[2][2]*dgMatrix[3][2];    
            
            /* now set dgMatrix equal to inverted */
            for (int ii=0;ii<4;ii++) {
                for (int kk=0;kk<4;kk++) {
                    dgMatrix[ii][kk] = dgInvertedMatrix[ii][kk];
                }
            }
            String result = dgBuildResult("dginvert result: ");
            return(result); 
        }
        
        public String dgCatXyzyprMatrix( String XYZYPR )
        {
                components = XYZYPR.split(",");
                doTrig();
                dgCatMatrix[0][0] = cp*cr;
                dgCatMatrix[0][1] = cp*sr;
                dgCatMatrix[0][2] = -sp;
                dgCatMatrix[0][3] = 0.0;
                dgCatMatrix[1][0] = -cy*sr + sy*sp*cr;
                dgCatMatrix[1][1] = cy*cr + sy*sp*sr;
                dgCatMatrix[1][2] = sy*cp;
                dgCatMatrix[1][3] = 0.0;
                dgCatMatrix[2][0] = sy*sr + cy*sp*cr;
                dgCatMatrix[2][1] = -sy*cr + cy*sp*sr;
                dgCatMatrix[2][2] = cy*cp;
                dgCatMatrix[2][3] = 0.0;
                dgCatMatrix[3][0] = xx;
                dgCatMatrix[3][1] = yy;
                dgCatMatrix[3][2] = zz;
                dgCatMatrix[3][3] = 1.0;    
                
                tmpMatrix[0][0] = dgCatMatrix[0][0]*dgMatrix[0][0]
                               + dgCatMatrix[0][1]*dgMatrix[1][0]
                               + dgCatMatrix[0][2]*dgMatrix[2][0]
                               + dgCatMatrix[0][3]*dgMatrix[3][0];
                
                 tmpMatrix[0][1] = dgCatMatrix[0][0]*dgMatrix[0][1]
                               + dgCatMatrix[0][1]*dgMatrix[1][1]
                               + dgCatMatrix[0][2]*dgMatrix[2][1]
                               + dgCatMatrix[0][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[0][2] = dgCatMatrix[0][0]*dgMatrix[0][2]
                               + dgCatMatrix[0][1]*dgMatrix[1][2]
                               + dgCatMatrix[0][2]*dgMatrix[2][2]
                               + dgCatMatrix[0][3]*dgMatrix[3][2];
                 
                 tmpMatrix[0][3] = dgCatMatrix[0][0]*dgMatrix[0][3]
                               + dgCatMatrix[0][1]*dgMatrix[1][3]
                               + dgCatMatrix[0][2]*dgMatrix[2][3]
                               + dgCatMatrix[0][3]*dgMatrix[3][3]; 
                 
                tmpMatrix[1][0] = dgCatMatrix[1][0]*dgMatrix[0][0]
                               + dgCatMatrix[1][1]*dgMatrix[1][0]
                               + dgCatMatrix[1][2]*dgMatrix[2][0]
                               + dgCatMatrix[1][3]*dgMatrix[3][0];
                
                 tmpMatrix[1][1] = dgCatMatrix[1][0]*dgMatrix[0][1]
                               + dgCatMatrix[1][1]*dgMatrix[1][1]
                               + dgCatMatrix[1][2]*dgMatrix[2][1]
                               + dgCatMatrix[1][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[1][2] = dgCatMatrix[1][0]*dgMatrix[0][2]
                               + dgCatMatrix[1][1]*dgMatrix[1][2]
                               + dgCatMatrix[1][2]*dgMatrix[2][2]
                               + dgCatMatrix[1][3]*dgMatrix[3][2];
                 
                 tmpMatrix[1][3] = dgCatMatrix[1][0]*dgMatrix[0][3]
                               + dgCatMatrix[1][1]*dgMatrix[1][3]
                               + dgCatMatrix[1][2]*dgMatrix[2][3]
                               + dgCatMatrix[1][3]*dgMatrix[3][3]; 
                 
                tmpMatrix[2][0] = dgCatMatrix[2][0]*dgMatrix[0][0]
                               + dgCatMatrix[2][1]*dgMatrix[1][0]
                               + dgCatMatrix[2][2]*dgMatrix[2][0]
                               + dgCatMatrix[2][3]*dgMatrix[3][0];
                
                 tmpMatrix[2][1] = dgCatMatrix[2][0]*dgMatrix[0][1]
                               + dgCatMatrix[2][1]*dgMatrix[1][1]
                               + dgCatMatrix[2][2]*dgMatrix[2][1]
                               + dgCatMatrix[2][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[2][2] = dgCatMatrix[2][0]*dgMatrix[0][2]
                               + dgCatMatrix[2][1]*dgMatrix[1][2]
                               + dgCatMatrix[2][2]*dgMatrix[2][2]
                               + dgCatMatrix[2][3]*dgMatrix[3][2];
                 
                 tmpMatrix[2][3] = dgCatMatrix[2][0]*dgMatrix[0][3]
                               + dgCatMatrix[2][1]*dgMatrix[1][3]
                               + dgCatMatrix[2][2]*dgMatrix[2][3]
                               + dgCatMatrix[2][3]*dgMatrix[3][3]; 
                 
                tmpMatrix[3][0] = dgCatMatrix[3][0]*dgMatrix[0][0]
                               + dgCatMatrix[3][1]*dgMatrix[1][0]
                               + dgCatMatrix[3][2]*dgMatrix[2][0]
                               + dgCatMatrix[3][3]*dgMatrix[3][0];
                
                 tmpMatrix[3][1] = dgCatMatrix[3][0]*dgMatrix[0][1]
                               + dgCatMatrix[3][1]*dgMatrix[1][1]
                               + dgCatMatrix[3][2]*dgMatrix[2][1]
                               + dgCatMatrix[3][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[3][2] = dgCatMatrix[3][0]*dgMatrix[0][2]
                               + dgCatMatrix[3][1]*dgMatrix[1][2]
                               + dgCatMatrix[3][2]*dgMatrix[2][2]
                               + dgCatMatrix[3][3]*dgMatrix[3][2];
                 
                 tmpMatrix[3][3] = dgCatMatrix[3][0]*dgMatrix[0][3]
                               + dgCatMatrix[3][1]*dgMatrix[1][3]
                               + dgCatMatrix[3][2]*dgMatrix[2][3]
                               + dgCatMatrix[3][3]*dgMatrix[3][3];  
                 
            /* now set dgMatrix equal to inverted */
            for (int ii=0;ii<4;ii++) {
                for (int kk=0;kk<4;kk++) {
                    dgMatrix[ii][kk] = tmpMatrix[ii][kk];
                }
            }
            
            String result = dgBuildResult("dgCatXyzyprMatrix result: ");
            return(result);
        }
        
        public String dgMatrixToQuaternions() {
            double d11 = dgMatrix[0][0];
            double d21 = dgMatrix[0][1];
            double d31 = dgMatrix[0][2];
            
            double d12 = dgMatrix[1][0];
            double d22 = dgMatrix[1][1];
            double d32 = dgMatrix[1][2];
            
            double d13 = dgMatrix[2][0];
            double d23 = dgMatrix[2][1];
            double d33 = dgMatrix[2][2];
            
            xx = dgMatrix[3][0];
            yy = dgMatrix[3][1];
            zz = dgMatrix[3][2];
            
            double trace = d11 + d22 + d33;
            if ( (trace + 1.0) > 0.0) {
                q1 = Math.sqrt( trace + 1.0 )/2.0 ;
            }
            else {
                q1 = 0;
            }
            
            if (Math.abs(q1) > .0002) {
                /* case where q1 is <> 0 */
                q2 = Math.sqrt( Math.abs(1.0 + 2.0*d11 -trace)) / 2.0;
                if (d32 < d23)
                    q2 = -1.0*q2;
                q3 = Math.sqrt( Math.abs(1.0 + 2.0*d22 -trace)) / 2.0;
                if (d13 < d31)
                    q3 = -1.0*q3;
                q4 = Math.sqrt( Math.abs(1.0 + 2.0*d33 -trace)) / 2.0;
                if (d21 < d12)
                    q4 = -1.0*q4;                
            }
            else {
                /* case where q1 is less than .002 and greater than -.0002 */
                q1 = 0.0;
                double work_qq[]= {0.0,0.0,0.0};
                work_qq[0] = Math.sqrt( Math.abs(d11+1.0)/2.0);
                work_qq[1] = Math.sqrt( Math.abs(d22+1.0)/2.0);
                work_qq[2] = Math.sqrt( Math.abs(d33+1.0)/2.0);
                int ind = 0;
                double max_val = work_qq[0];
                for (int ii=1; ii<3; ii++)
                {
                    if (max_val<work_qq[ii])
                    {
                        ind = ii;
                        max_val = work_qq[ii];
                    }
                }
                
                switch (ind)
                {
                    case 0:
                        /* q1 largest */
                        q2 = work_qq[0];
                        q3 = d21/(2.0*work_qq[0]);
                        q4 = d31/(2.0*work_qq[0]);
                        break;
                    case 1:
                        /* q2 largest */
                        q3 = work_qq[1];
                        q2 = d12/(2.0*work_qq[1]);
                        q4 = d32/(2.0*work_qq[1]);
                        break;  
                    case 2:
                        /* q3 largest */
                        q4 = work_qq[2];
                        q2 = d13/(2.0*work_qq[2]);
                        q3 = d23/(2.0*work_qq[2]);
                        break;                          
                }
                
            }
            
 
            String result = dgBuildQuaternionResult("Quaternions result:" );
            return(result); 
        }
        
        public String dgGetX() {
            if (Math.abs(dgMatrix[3][0]) < .001) {
                dgMatrix[3][0] = 0.0;
            }
            return(String.valueOf(dgMatrix[3][0]));
        }
        public String dgGetY() {
            if (Math.abs(dgMatrix[3][1]) < .001) {
                dgMatrix[3][1] = 0.0;
            }
            return(String.valueOf(dgMatrix[3][1]));
        }
        public String dgGetZ() {
             if (Math.abs(dgMatrix[3][2]) < .001) {
                dgMatrix[3][2] = 0.0;
            }
             return(String.valueOf(dgMatrix[3][2]));
        }
        public String dgGetQ1() {
            if (Math.abs(q1) < .0002) {
                q1 = 0.0;
            }
            return(String.valueOf(q1));
        }
        public String dgGetQ2() {
            if (Math.abs(q2) < .0002) {
                q2 = 0.0;
            }
            return(String.valueOf(q2));
        }
        public String dgGetQ3() {
            if (Math.abs(q3) < .0002) {
                q3 = 0.0;
            }
            return(String.valueOf(q3));
        }

        public String dgGetQ4() {
            if (Math.abs(q4) < .0002) {
                q4 = 0.0;
            }
            return(String.valueOf(q4));
        }
        
        public String dgGetYaw() {
            doYawPitchRoll();
            if (Math.abs(yaw) < .001) {
                yaw = 0.0;
            }
            return(String.valueOf(yaw));
        }
        public String dgGetPitch() {
            doYawPitchRoll();
             if (Math.abs(pitch) < .001) {
                pitch = 0.0;
            }
            return(String.valueOf(pitch));
        }
        public String dgGetRoll() {
            doYawPitchRoll();
            if (Math.abs(roll) < .001) {
                roll = 0.0;
            }
            return(String.valueOf(roll));
        }
        private  void doYawPitchRoll() {
            double r1;
            double cc;
            double ss;
            double p1;
            double y1;
            double r2;
            double p2;
            double y2;
            
            if ( Math.abs(dgMatrix[0][0]) >= .0001 || Math.abs(dgMatrix[0][1]) > .0001 ) {
                /*
                 *  first solution
                 */
                r1 = Math.atan2( dgMatrix[0][1], dgMatrix[0][0] );
                
                cc = Math.cos( r1 );
                ss = Math.sin( r1 );
                
                p1 = Math.atan2( -dgMatrix[0][2], cc*dgMatrix[0][0] + ss*dgMatrix[0][1] );
                
                y1 = Math.atan2( ss*dgMatrix[2][0] - cc*dgMatrix[2][1],
                                 cc*dgMatrix[1][1] - ss*dgMatrix[1][0]);
                
                /*
                 *  second solution
                 */       
                r2 = Math.atan2( -dgMatrix[0][1], -dgMatrix[0][0] );
                
                cc = Math.cos( r2 );
                ss = Math.sin( r2 );
                
                p2 = Math.atan2( -dgMatrix[0][2], cc*dgMatrix[0][0] + ss*dgMatrix[0][1] );
                
                y2 = Math.atan2( ss*dgMatrix[2][0] - cc*dgMatrix[2][1],
                                 cc*dgMatrix[1][1] - ss*dgMatrix[1][0]);               
            }
            else
            {
                r1 = 0.0;
                
                p1 = Math.atan2( -dgMatrix[0][2], 0.0);
                
                y1 = Math.atan2( -dgMatrix[2][1], dgMatrix[1][1] );
                
                r2 = r1;
                p2 = p1;
                y2 = y1;
            }
            
            if ( (r1*r1 + p1*p1 + y1*y1 ) <=
                 (r2*r2 + p2*p2 + y2*y2 ) )
            {
                roll = r1*180.0/Math.PI;
                pitch = p1*180.0/Math.PI;
                yaw = y1*180.0/Math.PI;
            }
            else
            {
                roll = r2*180.0/Math.PI;
                pitch = p2*180.0/Math.PI;
                yaw = y2*180.0/Math.PI;                
            }
                
        }
        
        private void dgDisplayResult(String Comment)
        {
              String value = Comment + dgGetX() + ", " + dgGetY() + ", " +
                  dgGetZ() + ", " + dgGetYaw() + ", " + dgGetPitch() + ", "
                  + dgGetRoll() + "\n";
              
              System.out.print(value);

        }
        private String dgBuildResult(String Comment)
        {
              String value = Comment + dgGetX() + ", " + dgGetY() + ", " +
                  dgGetZ() + ", " + dgGetYaw() + ", " + dgGetPitch() + ", "
                  + dgGetRoll() + "\n";
              
              return(value);
        }

        public String xformToZYZ()
        {
            double aa, bb, cc, sin_aa, cos_aa, sin_cc, cos_cc;

            aa = Math.atan2(dgMatrix[2][1], dgMatrix[2][0]);
            sin_aa = Math.sin(aa);
            cos_aa = Math.cos(aa);
            
            sin_cc = -dgMatrix[0][0]*sin_aa + dgMatrix[0][1]*cos_aa;
            cos_cc = -dgMatrix[1][0]*sin_aa + dgMatrix[1][1]*cos_aa;
            cc = Math.atan2(sin_cc, cos_cc);

            bb = Math.atan2(dgMatrix[1][2]*sin_cc - dgMatrix[0][2]*cos_cc, dgMatrix[2][2]);

            DecimalFormat df = new DecimalFormat("0.0######");
            String value = df.format(aa) + "," + df.format(bb) + "," + df.format(cc);
            return value;
        }
        
        public String dgXyzoatToMatrix(String XYZOAT)
        {
            double o_ang, a_ang, t_ang, c1, c2, c3, s1, s2, s3, xx, yy, zz;
            
            components = XYZOAT.split(",");
            xx = Double.valueOf(components[0]).doubleValue();
            yy = Double.valueOf(components[1]).doubleValue();
            zz = Double.valueOf(components[2]).doubleValue();
            o_ang = Double.valueOf(components[3]).doubleValue()*TO_RAD;
            a_ang = Double.valueOf(components[4]).doubleValue()*TO_RAD;
            t_ang = Double.valueOf(components[5]).doubleValue()*TO_RAD;
            
            c1 = Math.cos(o_ang);
            s1 = Math.sin(o_ang);
            c2 = Math.cos(a_ang);
            s2 = Math.sin(a_ang);
            c3 = Math.cos(t_ang);
            s3 = Math.sin(t_ang);
            dgMatrix[0][0] = c3*c2*c1 - s3*s1;
            dgMatrix[0][1] = c3*c2*s1 + s3*c1;
            dgMatrix[0][2] = -c3*s2;
            dgMatrix[0][3] = 0.0;
            dgMatrix[1][0] = -s3*c2*c1 - c3*s1;
            dgMatrix[1][1] = -s3*c2*s1 + c3*c1;
            dgMatrix[1][2] = s3*s2;
            dgMatrix[1][3] = 0.0;
            dgMatrix[2][0] = s2*c1;
            dgMatrix[2][1] = s2*s1;
            dgMatrix[2][2] = c2;
            dgMatrix[2][3] = 0.0;
            dgMatrix[3][0] = xx;
            dgMatrix[3][1] = yy;
            dgMatrix[3][2] = zz;
            dgMatrix[3][3] = 1.0;
            String result = dgBuildResult("");
            return(result);
        }

	private String dgBuildQuaternionResult(String Comment)
	{
	      String value = Comment + dgGetX() + ", " + dgGetY() + ", " +
		  dgGetZ() + ", " + dgGetQ1() + ", " + dgGetQ2() + ", "
		  + dgGetQ3() + "," + dgGetQ2() + "\n";
	      
	      return(value);

	}
}

