// --------------------------------------------------------
// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// NachiAWUploader.java
// 
//===================================================================
//// Usage notes:
//===================================================================
//  June 2004  Creation:										  seo
//===================================================================
//DOM and Parser classes
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;

// set CLASSPATH=D:\WRKSPS\R15WS\OLPWS\BSS_OLPR15Prj\DNBIgpOlpJavaBase\DNBIgpOlpUploaderTools.mj\src;%CLASSPATH%
import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class NachiAWUploader extends DNBIgpOlpUploadBaseClass 
{
    //Constants
    private static final int JOINT = 1;
    private static final double ZERO_TOL = .001;
    private static final int CARTESIAN = 2;
    private static final String VERSION = "Delmia Corp. NachiAW Uploader Version 5 Release 26 SP3.\n";
    private static final String COPYRIGHT = "Copyright Delmia Corp. 1986-2016, All Rights Reserved.\n";

    private static BufferedWriter bw;
    private static boolean unparsedEntitiesExist = false;
    private static boolean firstCircular;

    //Member primitive variables
    private Element m_toolProfileListElement;
    private Element m_motionProfileListElement;
    private Element m_accuracyProfileListElement;
    private Element m_objectProfileListElement;
    private boolean m_taskNameSet;
    private int m_logicProgramInputCount;
    private int m_logicProgramOutputCount;
    private int m_jobNum;
    private int m_AccuracyType = 1;	// Speed
    private int m_worldCoords = 0;
    private int m_targetType;
    private int m_targetNumber;
    private int m_targetTurnValue;
    private String m_intAxes ="False";
    private int m_numCStatements;
    private int m_numRobotAxes;
    private int m_numAuxAxes;
    private int m_numExtAxes;
    private int m_numWorkAxes;
    private int m_auxAxesCount;
    private int m_extAxesCount;
    private int m_workAxesCount;
    private int m_auxAxesStart;
    private int m_extAxesStart;
    private int m_workAxesStart;
    private int m_procCount;
    private int m_extAxesGroupNumber;
    private int m_auxAxesGroupNumber;
    private int m_workAxesGroupNumber;
    private int m_delayCounter;
    private int m_callCounter;
    private int m_calledProgramCounter;
    private int m_spotCounter;
    private int m_backupCounter;
    private int m_macroCounter;
    private int m_releaseCounter ;
    private int m_grabCounter;
    private int m_moveCounter;
    private int m_nIncr;
    private int m_gunmoveCounter = 1;
    private int m_mountCounter;
    private int m_unmountCounter;
    private int m_enterZoneCounter;
    private int m_clearZoneCounter;
    private int m_objectProfNum;
    private String m_logicProgram;
    private boolean m_inLogicProgram;
    private int m_logicProgramJobCount;
    private int m_logicProgramExpressionCount;
    private Element m_logicProgramElement;
    private Element m_logicProgramJobElement;
    private Element m_logicProgramJobsElement;
    private boolean m_createJointTarget;
    private boolean m_moduleFound;
    //private boolean m_arconcreated;
    //private boolean m_arcoffcreated;
    //private boolean m_arcweavecreated;
    private int lastTargetNumber = 0;
    private String ProgName="NACHIAWUPLOAD";
    private double  m_axis3Adjust;
    private boolean m_joint3Linked;
    private String m_cTagPrefix;
    private String m_jTagPrefix;
    private boolean m_commWaitSig;

    // member variables related to Pose File
    private String m_sControllerName;
    private String m_sRobotModelName;
    private String m_sProgPath;
    private String m_sProgName;
    private String m_schemaPath;
    private int m_nControllerCode;
    private int m_nPosNumbers;
    private int m_nNumberOfAxes;

    //Member objects
    private Document m_xmlDoc;
    private String m_currentTagPoint;
    private String m_currentToolNumber;
    private String m_currentObjectNumber;
    private String m_pickHome;
    private String m_dropHome;
    private String m_grabPart;
    private String m_partGrabbed;
    private String m_mountedToolName;
    private String m_prevAccuracyValue;
    private boolean railAxisIsJoint1;
    private String [] m_axisTypes;
    private String [] m_commentNames;
    private String [] m_commentValues;
    private static String [] m_mountedGunNames;
    private String prgFileEncoding = "US-ASCII";
    private String uploadStyle = "General";
    private String m_orientMode;
    private Element m_currentElement;
    private int m_commentCount;
    private int weaveCount = 1;
    private Pattern [] m_keywords;
    private ArrayList m_listOfRobotTargets;
    private ArrayList m_listOfAuxTargets;
    private ArrayList m_listOfExtTargets;
    private ArrayList m_listOfWorkTargets;
    private ArrayList m_listOfSpeedValues;
    private ArrayList m_listOfAccuracyValues;
    private ArrayList m_listOfCalledPrograms;
    private ArrayList m_listofHomeNames;
    private ArrayList m_listOfPoseValues;
    private ArrayList m_listOfSetM;
    private Hashtable m_auxAxesTypes;
    private Hashtable m_targetTypes;
    private Hashtable m_targetNumbers;
    private Hashtable m_weldScheduleHomes;
    private Hashtable m_weldScheduleDelays;
    private Hashtable m_macroCommands;
    private Hashtable m_backupHomes;
    private Hashtable m_tagNames;
    private Hashtable m_procNameList;
    private Hashtable m_toolNumberMapping;
    private Hashtable m_objectNumberMapping;
    private Hashtable m_parameterData;
    private Hashtable m_commentLines;

    public NachiAWUploader(String [] parameters) 
                            throws SAXException, 
                            ParserConfigurationException, 
                            TransformerConfigurationException, 
                            NumberFormatException
    {
        super(parameters);

        m_toolProfileListElement = null;
        m_accuracyProfileListElement = null;
        m_motionProfileListElement = null;
        m_objectProfileListElement = null;
        m_targetType = CARTESIAN;
        m_targetTurnValue = 0;
        m_targetNumber = 0;
        m_numCStatements = 0;
        m_delayCounter = 1;
        m_callCounter = 1;
        m_calledProgramCounter = 0;
        m_moveCounter = 1;
        m_nIncr = 0;
        m_spotCounter = 1;
        m_backupCounter = 1;
        m_procCount = 0;
        m_macroCounter = 1;
        m_grabCounter = 1;
        m_releaseCounter = 1;
        m_mountCounter = 1;
        m_unmountCounter = 1;
        m_enterZoneCounter = 1;
        m_clearZoneCounter = 1;
        m_numRobotAxes = super.getNumberOfRobotAxes();
        m_numAuxAxes = super.getNumberOfRailAuxiliaryAxes();
        m_numExtAxes = super.getNumberOfToolAuxiliaryAxes();
        m_numWorkAxes = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();
        m_auxAxesCount = 0;
        m_extAxesCount = 0;
        m_workAxesCount = 0;
        m_auxAxesStart = 1;
        m_extAxesStart = 1;
        m_workAxesStart = 1;
        m_commentCount = 0;
        m_moduleFound = false;
        m_auxAxesGroupNumber = 1;
        m_extAxesGroupNumber = 2;
        m_workAxesGroupNumber = 0;
        m_createJointTarget = false;
        m_logicProgram = "Program1";
        m_inLogicProgram = false;
        m_taskNameSet = false;
        m_logicProgramInputCount = 0;
        m_logicProgramOutputCount = 0;
        m_logicProgramJobCount = 1;
        m_logicProgramExpressionCount = 0;
        m_logicProgramElement = null;
        m_logicProgramJobElement = null;
        m_logicProgramJobsElement = null;
        m_axis3Adjust = 90.0;
        m_joint3Linked = false;
        m_cTagPrefix = "";
        m_jTagPrefix = "";
        m_commWaitSig = true;
        firstCircular = true;

        m_taskNameSet = false;
        m_logicProgramInputCount = 1;
        m_logicProgramOutputCount = 1;
        m_jobNum = 1;

        //m_arconcreated = false;
        //m_arcoffcreated = false;
        //m_arcweavecreated = false;

        m_orientMode = "2_axis";

        m_mountedGunNames = parameters[6].split("[\\t]+");
        //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1

        m_pickHome = new String("");
        m_dropHome = new String("");
        m_grabPart = new String("");
        m_partGrabbed = new String("");
        m_mountedToolName = new String("");
        m_prevAccuracyValue = new String("");
        railAxisIsJoint1 = false;

        m_currentTagPoint = new String("");
        m_currentToolNumber = new String("1");
        m_currentObjectNumber = new String("0");

        m_sProgPath = new String("");

        m_currentElement = null;
        m_keywords = new Pattern [300];
        m_axisTypes = new String [m_numRobotAxes+m_numExtAxes+m_numAuxAxes+m_numWorkAxes];
        m_axisTypes = super.getAxisTypes();
        m_listOfRobotTargets = new ArrayList(5);
        m_listOfAuxTargets = new ArrayList(2);
        m_listOfCalledPrograms = new ArrayList(1);
        m_listOfExtTargets = new ArrayList(2);
        m_listOfWorkTargets = new ArrayList(2);
        m_listOfSpeedValues = new ArrayList(5);
        m_listOfAccuracyValues = new ArrayList(3);
        m_listOfPoseValues = new ArrayList(6);
        m_listOfSetM = new ArrayList(2);
        m_auxAxesTypes = new Hashtable();
        m_targetTypes = new Hashtable();
        m_targetNumbers = new Hashtable();
        m_tagNames = new Hashtable();
        m_toolNumberMapping = new Hashtable();
        m_objectNumberMapping = new Hashtable();
        m_weldScheduleHomes = new Hashtable();
        m_weldScheduleDelays = new Hashtable();
        m_macroCommands = new Hashtable();
        m_procNameList = new Hashtable();
        m_backupHomes = new Hashtable();
        m_parameterData =  super.getParameterData();
        m_listofHomeNames = new ArrayList(2);
        m_commentLines = new Hashtable();
        m_commentNames = new String [1000];
        m_commentValues = new String [1000];
        clearComments();
        m_xmlDoc = super.getDOMDocument();
        m_toolProfileListElement = super.getToolProfileListElement();
        m_motionProfileListElement = super.getMotionProfileListElement();
        m_accuracyProfileListElement = super.getAccuracyProfileListElement();
        m_objectProfileListElement = super.getObjectFrameProfileListElement();

        String [] toolProfileNames = parameters[7].split("[\\t]+");
        for (int jj = 0; jj < toolProfileNames.length; jj++)
        {
            m_toolNumberMapping.put(String.valueOf(jj+1), toolProfileNames[jj]);
        }

        String [] objectProfileNames = parameters[11].split("[\\t]+");
        for (int jj = 0; jj < objectProfileNames.length; jj++)
        {
            m_objectNumberMapping.put(String.valueOf(jj), objectProfileNames[jj]);
        }

        String [] homeNames = parameters[9].split("[\\t]+");
        for (int jj=0; jj < homeNames.length; jj++ )
        {
                m_listofHomeNames.add(jj, homeNames[jj]);
        }

        Enumeration parameterKeys = m_parameterData.keys();
        Enumeration parameterValues = m_parameterData.elements();

        while(parameterKeys.hasMoreElements())
        {
            String parameterName = parameterKeys.nextElement().toString();			 
            String parameterValue = parameterValues.nextElement().toString();

            int index = parameterName.indexOf('.');
            String testString = parameterName.substring(0, index + 1);
			
            try 
            {
                if (testString.equals("ToolProfile."))
                {
                    String profileName = parameterName.substring(index + 1);
                    m_toolNumberMapping.put(profileName, parameterValue);
                }
                else if (testString.equals("ObjectProfile."))
                {
                    String profileName = parameterName.substring(index + 1);
                    m_objectNumberMapping.put(profileName, parameterValue);
                }                     
                else if (testString.equals("WeldHome."))
                {
                    int homeIndex = Integer.parseInt(parameterName.substring(index + 1));
                    if (homeIndex < 0)
                        homeIndex = 0;

                    if (homeIndex > homeNames.length)
                        homeIndex = homeNames.length-1;

                    String [] homeComponents = parameterValue.split(";");

                    for (int kk = 0; kk < homeComponents.length; kk++)
                    {
                        m_weldScheduleHomes.put(homeComponents[kk], homeNames[homeIndex-1]);
                    }
                }   
                else if (testString.equals("OpenHome."))
                {
                    int homeIndex = Integer.parseInt(parameterName.substring(index + 1));
                    if (homeIndex < 0)
                        homeIndex = 0;

                    if (homeIndex > homeNames.length)
                        homeIndex = homeNames.length-1;

                    String [] homeComponents = parameterValue.split(";");

                    for (int kk = 0; kk < homeComponents.length; kk++)
                        m_backupHomes.put(homeComponents[kk], homeNames[homeIndex-1]);
                }
                else if (testString.equals("WeldDelay."))
                {
                    double delayTime = Double.valueOf(parameterName.substring(index + 1)).doubleValue() * 0.001;

                    String [] delayComponents = parameterValue.split(";");

                    for (int kk=0;kk<delayComponents.length;kk++)
                        m_weldScheduleDelays.put(delayComponents[kk], String.valueOf(delayTime));
                }
                else if (testString.equals("Macro."))
                {
                    String macroAction = parameterName.substring(index+1);
                    String [] macroComponents = parameterValue.split(";");
                    for (int kk=0;kk<macroComponents.length;kk++)
                        m_macroCommands.put(macroComponents[kk], macroAction);
                }
                else if (parameterName.equals("PickHome"))
                {
                    m_pickHome = parameterValue;
                }
                else if (parameterName.equals("DropHome"))
                {
                    m_dropHome = parameterValue;
                }
                else if (parameterName.equals("GrabPart"))
                {
                    m_grabPart = parameterValue;
                }
                else if (parameterName.equals("PartGrabbed"))
                {
                    m_partGrabbed = parameterValue;
                }
                else if (parameterName.equals("MountedToolName"))
                {
                    m_mountedToolName = parameterValue;
                }
                else if (parameterName.equals("WorldCoords"))
                {
                    m_worldCoords = Integer.valueOf(parameterValue).intValue();
                }
                else if (parameterName.equals("Railgroup"))
                {
                    String [] auxAxesNumbers = parameterValue.split(";");
                    for (int kk=0;kk<auxAxesNumbers.length;kk++)
                    {
                        m_auxAxesTypes.put(auxAxesNumbers[kk], "RailTrackGantry");
                    }
                }
                else if (parameterName.equals("Toolgroup"))
                {
                    String [] auxAxesNumbers = parameterValue.split(";");
                    for (int kk=0;kk<auxAxesNumbers.length;kk++)
                        m_auxAxesTypes.put(auxAxesNumbers[kk], "EndOfArmTooling");
                }
                else if (parameterName.equals("Workgroup"))
                {
                    String [] auxAxesNumbers = parameterValue.split(";");
                    for (int kk=0;kk<auxAxesNumbers.length;kk++)
                        m_auxAxesTypes.put(auxAxesNumbers[kk], "WorkPositioner");
                }
                else if (parameterName.equals("Axis3Adjust"))
                {
                    Double tmpVar = new Double(parameterValue);

                    if (tmpVar.isNaN())
                        m_axis3Adjust = 90.0; // Default value
                    else
                        m_axis3Adjust = Double.parseDouble(parameterValue);
                }
                else if (parameterName.equals("Joint3Linked"))
                {
                    m_joint3Linked = new Boolean(parameterValue).booleanValue();
                }
                else if (parameterName.equals("LogicProgram"))
                {
                    m_logicProgram = parameterValue;
                }
                else if (parameterName.equals("CommentWaitSignal"))
                {
                    m_commWaitSig = new Boolean(parameterValue).booleanValue();
                }
                else if (parameterName.equals("RailAxisIsJoint1"))
                {
                    if (parameterValue.equalsIgnoreCase("TRUE"))
                        railAxisIsJoint1 = true;
                }
                else if (parameterName.equals("ProgramFileEncoding")) {
                    prgFileEncoding = parameterValue;
                }
                else if (parameterName.equals("OLPStyle")) {
                    uploadStyle = parameterValue;
                }
                else if (parameterName.equals("OrientMode")) {
                    m_orientMode = parameterValue;
                }
            }

            finally 
            {
                continue;
            }
        }
        
        for (int ii = 0; ii < 300; ii++)
            m_keywords[ii] = Pattern.compile("dontMatchNothing", Pattern.CASE_INSENSITIVE);

        m_keywords[0] = Pattern.compile("^(MOVEJ?)\\s*([P|L|C])\\s*,\\s*\\(([-?[0-9]*\\.?[0-9]*\\s*,?\\s*]*)\\)\\s*,?\\s*([S|T])?\\s*=?\\s*([0-9]*\\.?[0-9]*)?\\s*(?:,\\s*(A)\\s*=\\s*([0-9]*P?)\\s*)?(?:,\\s*(H)\\s*=\\s*([0-9]*)\\s*)?(?:,\\s*(G)([0-9]*))?", Pattern.CASE_INSENSITIVE);
        m_keywords[1] = Pattern.compile("^(MOVEJ?)\\s*([P|L|C])\\s*,\\s*P\\[([0-9]*)\\],\\s*([S|T])?\\s*=?\\s*([0-9]*)?\\s*,?\\s*(A)?\\s*=?\\s*([0-9]*P?)?\\s*,?\\s*(H)?\\s*=?\\s*([0-9]*)?\\s*", Pattern.CASE_INSENSITIVE);
        m_keywords[2] = Pattern.compile("^DELAY\\s+([0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);
        m_keywords[3] = Pattern.compile("^SETM\\s+([M|I|O])([0-9]*)\\s*,\\s*(1|0)\\s*(?:,\\s*([0-9]*\\.?[0-9]*)\\s*)?", Pattern.CASE_INSENSITIVE);
        m_keywords[4] = Pattern.compile("^SET\\s+(M)([0-9]*)\\s*(?:,\\s*(1|0))?\\s*$", Pattern.CASE_INSENSITIVE);
        m_keywords[5] = Pattern.compile("^RESET\\s+(M)([0-9]*)\\s*$", Pattern.CASE_INSENSITIVE);
        m_keywords[6] = Pattern.compile("^(WAIT[I|J])\\s+(I)([0-9]*)", Pattern.CASE_INSENSITIVE);
        m_keywords[7] = Pattern.compile("^WAIT\\s+(I)([0-9]*)\\s*,\\s*([0-9]*\\.?[0-9]*)(?:\\s*,\\s*([0-9]*)\\s*)?$", Pattern.CASE_INSENSITIVE);
        m_keywords[8] = Pattern.compile("^SPOT\\s*([0-9]*)\\s*,\\s*([0-9]*)\\s*,\\s*([0-9]*)\\s*,\\s*([0-9]*)", Pattern.CASE_INSENSITIVE);
        m_keywords[9] = Pattern.compile("^USE\\s*", Pattern.CASE_INSENSITIVE);
        m_keywords[10] = Pattern.compile("^'", Pattern.CASE_INSENSITIVE);
        m_keywords[11] = Pattern.compile("^REM\\s+", Pattern.CASE_INSENSITIVE);
        m_keywords[12] = Pattern.compile("^TOOL\\s*([0-9]*)\\s*$", Pattern.CASE_INSENSITIVE);
        m_keywords[13] = Pattern.compile("^WELDCND\\s+([0-9]*)*", Pattern.CASE_INSENSITIVE);
        m_keywords[14] = Pattern.compile("^END\\s*$", Pattern.CASE_INSENSITIVE);
        m_keywords[15] = Pattern.compile("CALLP\\s+(\\d+)", Pattern.CASE_INSENSITIVE);
        m_keywords[16] = Pattern.compile("ACC\\s+(\\d+)", Pattern.CASE_INSENSITIVE);
        //m_keywords[17] = Pattern.compile("^WAIT\\s+I([0-9]*)\\s*,\\s*([0-9]*)\\s*$", Pattern.CASE_INSENSITIVE);
        m_keywords[18] = Pattern.compile("^ARCON\\s*([0-9]*)", Pattern.CASE_INSENSITIVE);
        m_keywords[19] = Pattern.compile("^ARCOFF\\s*([0-9]*)", Pattern.CASE_INSENSITIVE);
        m_keywords[20] = Pattern.compile("^SWEAVE\\s+(\\d)\\s*,\\s*(\\d+)", Pattern.CASE_INSENSITIVE);
        m_keywords[21] = Pattern.compile("^\\s*(IF)\\s*([a-zA-Z_0-8\\[\\]\\(\\)=\\s]*)\\s*THEN\\s*\\*([\\p{Alnum}\\p{Punct}]*)\\s*(ELSE\\s*\\*([\\p{Alnum}\\p{Punct}]*)\\s*)?", Pattern.CASE_INSENSITIVE);
        m_keywords[22] = Pattern.compile("^(M)([1-8]*)\\s+([0|1])", Pattern.CASE_INSENSITIVE);
        m_keywords[23] = Pattern.compile("^\\*(.*)", Pattern.CASE_INSENSITIVE);
        m_keywords[24] = Pattern.compile("^(I)([0-9]+)", Pattern.CASE_INSENSITIVE);
        m_keywords[25] = Pattern.compile("^RETURN", Pattern.CASE_INSENSITIVE);
        m_keywords[26] = Pattern.compile("GOSUB\\s+\\*(.*)", Pattern.CASE_INSENSITIVE);
    } // end constructor

    public static void main(String [] parameters) 
                                throws SAXException, 
                                ParserConfigurationException, 
                                TransformerConfigurationException, 
                                NumberFormatException,
                                Exception
    {
        NachiAWUploader uploader = new NachiAWUploader(parameters);
        
        String [] additionalProgramNames = null;
        if (parameters.length > 12 && parameters[12].trim().length() > 0)
            additionalProgramNames = parameters[12].split("[\\t]+");
        
		//Get the robot program and parse it line by line
        String robotProgramName = uploader.getPathToRobotProgramFile();
        File f = new File(robotProgramName);
        String path = f.getParent() + f.separator;
        String robotProgramNameOnly = f.getName();
        String uploadInfoFileName = uploader.getPathToErrorLogFile();

        uploader.m_sProgPath = uploader.getPathToRobotProgramFile();
        
        boolean createTool = false;
        double xx = 0.0;
        double yy = 0.0;
        double zz = 0.0;
        double yaw = 0.0;
        double pitch = 0.0;
        double roll = 0.0;
        int toolCount = 0;
        int objectCount = 0;

        double [] objectFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};
        try
        {
            bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));
            bw.write(VERSION);
            bw.write(COPYRIGHT);
            bw.write("\nStart of java parsing.\n\n");

            uploader.processRobotProgram(robotProgramName);
            
            String sAddProg = null;
            int index;
            if (additionalProgramNames != null)
            {
                for (int ii=0; ii<additionalProgramNames.length; ii++)
                {
                    f = new File(additionalProgramNames[ii]);
                    sAddProg = f.getName();
                    index = uploader.m_listOfCalledPrograms.indexOf(sAddProg);
                    if (index < 0)
                    {
                        uploader.m_listOfCalledPrograms.add(uploader.m_calledProgramCounter, sAddProg);
                        uploader.m_listOfCalledPrograms.trimToSize();
                        uploader.m_calledProgramCounter++;
                    }
                }
            }

            String calledProgName;
            for (int ii = 0; ii < uploader.m_calledProgramCounter; ii++)
            {
                uploader.m_moveCounter = 1;
                if (uploader.m_inLogicProgram == false)
                {
                    calledProgName = (String)uploader.m_listOfCalledPrograms.get(ii);
                    if (calledProgName.equalsIgnoreCase(robotProgramNameOnly))
                        continue;
                    uploader.processRobotProgram(path+calledProgName);
                }
            }

            if (unparsedEntitiesExist == false)
                bw.write("All the program statements have been parsed.\n");
                
            bw.write("\nJava parsing completed successfully.\n");
            bw.write("\nEnd of java parsing.\n");
            bw.flush();
            bw.close();
        }

        catch (IOException e)
        {
            e.getMessage();
        }
        
        Element motionProfileList = uploader.getMotionProfileListElement();
        if (motionProfileList.hasChildNodes() == false)
            uploader.createMotionProfile(motionProfileList, "Default",
                DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT,
                50.0, 100.0, 100.0, 100.0);
        
        Element accuracyProfileList = uploader.getAccuracyProfileListElement();
        if (accuracyProfileList.hasChildNodes() == false)
            uploader.createAccuracyProfile(accuracyProfileList, "Default",
                DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                false, 0.0);

        try 
        {
            uploader.writeXMLStreamToFile();
        }
        catch (TransformerException e)
        {
            e.getMessage();
        }
        catch (IOException e)
        {
            e.getMessage();
        }

    } // end main

    private void processRobotProgram(String robotProgramName) throws Exception
    {
        Element activityListElem = null;
        
        File f = new File(robotProgramName);
        String taskName = f.getName();

        if (taskName.endsWith(".prg"))
        {
            int l = taskName.length();
            taskName = taskName.substring(0, l-4);
        }
        if (activityListElem == null)
        {
            activityListElem = super.createActivityList(taskName);
            super.createAttributeList(activityListElem, null, null);
            processComments(activityListElem, "Pre");
        }
        try 
        {
            BufferedReader inProcRead = new BufferedReader(new FileReader(robotProgramName));
            String procLine = inProcRead.readLine();
			
            while(procLine != null)
            {
                if (procLine.equals(""))
                {
                    procLine = inProcRead.readLine();
                    continue;
                }
                Matcher procMatch = m_keywords[15].matcher(procLine);
                if (procMatch.find() == true)
                {
                    String subProgNum = procMatch.group(1);
                    m_procCount++;
                    m_procNameList.put(subProgNum, String.valueOf(m_procCount));
                }
                //--------------------------------
                Matcher plcMatch = m_keywords[21].matcher(procLine);
                if (plcMatch.find() == true)
                {
                    m_inLogicProgram = true;
                }
                //--------------------------------
                procLine = inProcRead.readLine();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));
            boolean handle_attr = false;
            boolean handle_appl = false;
                    
            bw.write("Uploading program file: \"");
            bw.write(robotProgramName);
            bw.write("\":\n\n");
                
            String line = in.readLine();
            int lineNumber = 1;
                
            StringBuffer unparsedStatement = new StringBuffer();
            String unparsedString = new String();
                   
            while(line != null)
            {
                if (line.equals(""))
                {
                    line = in.readLine();
                    continue;
                }
                
                unparsedStatement.append("Line ").append(lineNumber).append(": ").append(line).append("\n");
                unparsedString = unparsedStatement.toString();

                m_nIncr = 0;
                boolean bStatus = isLineNumPresent(line);
                if (bStatus == true)
                {
                    line = preProcessLine(line);
                }
                
                for (int ii = 0; ii < m_keywords.length; ii++)
                {
                    Matcher match = m_keywords[ii].matcher(line);
                    int matchShift = 0;
                    
                    if (match != null && match.find() == true)
                    {
                        //Call appropriate methods to handle the input
                        switch(ii)
                        {
                            case 0: // MOVE
                                processMOVEStatement(match, activityListElem);
                                break;
                            case 1: // MOVE POSE
                                if (uploadStyle.equals("OperationComments"))
                                    creOpComm(line, false, activityListElem);
                                else
                                    addToComments(line, true);
                                /* POSE file is not supported yet
                                processPOSEMOVEStatement(match, activityListElem);
                                 */
                                break;
                            case 2: // DELAY
                                processDELAYStatement(match, activityListElem);
                                break;
                            case 3: // SETM
                                processSETMStatement(match, activityListElem);
                                break;
                            case 4: // SET M
                                processSETStatement(match, activityListElem, false);
                                break;
                            case 5: // RESET M
                                processSETStatement(match, activityListElem, true);
                                break;
                            case 6: // WAITI
                                if (m_commWaitSig == false)
                                {
                                    matchShift = 0;
                                    processWAITIStatement(match, matchShift, activityListElem);
                                }
                                else
                                {
                                    addToComments(line, true);
                                }
                                break;
                            case 7: // WAIT I
                                if (m_commWaitSig == false)
                                {
                                    if (match.group(4) != null)
                                    {
                                        bw.write("Escape Step in the following statement ignored.\n");
                                        bw.write(unparsedString);
                                        unparsedEntitiesExist = true;
                                    }
                                    processWAITStatement(match, activityListElem);
                                }
                                else
                                {
                                    addToComments(line, true);
                                }
                                break;
                            case 8: // SPOT
                                matchShift = 0;
                                processSPOTStatement(match, matchShift, activityListElem);
                                break;
                            case 9: // USE
                                if (uploadStyle.equals("OperationComments"))
                                    creOpComm(line, false, activityListElem);
                                else
                                    addToComments(line, true);
                                //processUSEStatement(match, activityListElem);
                                break;
                            case 10: // 'comment
                                String [] comment = line.split("'", 2);
                                if (uploadStyle.equals("OperationComments"))
                                    creOpComm(comment[1], false, activityListElem);
                                else
                                    addToComments(comment[1], false);
                                break;
                            case 11: // REM
                                comment = line.split(" ", 2);
                                if (uploadStyle.equals("OperationComments"))
                                    creOpComm(line, false, activityListElem);
                                else
                                    addToComments(line, false);
                                break;
                            case 12: // TOOL
                                processTOOLStatement(match.group(1));
                                break;
                            case 13: // WELDCND
                                creOpComm(line, true, activityListElem);
                                break;
                            case 14: // END
                                if (m_currentElement != null)
                                    processComments(m_currentElement, "Post");
                                else
                                    processComments(activityListElem, "Post");
                                break;
                            case 15: // CALL P
                                processCALLPStatement(match.group(1), activityListElem, robotProgramName);
                                break;
                            case 16: // ACC
                                processACCURACYStatement(match.group(1));
                                break;
                            case 18: // ARCON
                                processARCONStatement(match, activityListElem);
                                break;
                            case 19: // ARCOFF
                                processARCOFFStatement(match, activityListElem);
                                break;
                            case 20: // SWEAVE
                                processSWEAVEStatement(match, activityListElem);
                                break;
                            case 21: // IF
                                if (uploadStyle.equals("OperationComments"))
                                    creOpComm(line, false, activityListElem);
                                else
                                    addToComments(line, true);
                                break;
                            case 22: // M
                                processSETStatement(match, activityListElem, false);
                                break;
                            case 23: // *LABEL
                                ProgName = match.group(1);
                                activityListElem = super.createActivityList(ProgName);
                                break;
                            case 24: // I
                                int ioNum = Integer.valueOf(match.group(2)).intValue();
                                if (m_commWaitSig == false && ioNum < 25)
                                {
                                    matchShift = 1;
                                    processWAITIStatement(match, matchShift, activityListElem);
                                }
                                else
                                {
                                    if (uploadStyle.equals("OperationComments"))
                                    {
                                        creOpComm(line, true, activityListElem);
                                    }
                                    else
                                    {
                                        addToComments(line, true);
                                        bw.write(unparsedString);
                                        unparsedEntitiesExist = true;
                                    }
                                }
                                break;
                            case 25: // RETURN
                                processComments(m_currentElement, "Post");
                                break;
                            case 26: // GOSUB
                                processGOSUBStatement(match.group(1), activityListElem);
                                break;
                        } // switch
                        break;
                    } // if
                    else if (ii == (m_keywords.length - 1))
                    {
                        String macroCommand = line;
                        boolean macroFound = false;
                        macroCommand = macroCommand.trim();
                        if (macroCommand != null)
                        {
                            String macroAction = (String)(m_macroCommands.get( macroCommand ));
                            if (macroAction != null)
                            {
                                // TODO - handle macros
                                //processMacroStatement( macroCommand, macroAction, activityListElem, mountedGunName );
                                macroFound = true;
                            }
                        }
                        if (macroFound == false)// && m_moduleFound == true)
                        {
                            if (uploadStyle.equals("OperationComments"))
                            {
                                creOpComm(line, true, activityListElem);
                            }
                            else
                            {
                                addToComments(line, true);
                                bw.write(unparsedString);
                                unparsedEntitiesExist = true;
                            }
                        }
                    } // else if
                } // for
                    
                if (handle_attr == true || handle_appl == true)
                    addToComments(line, false);
                        
                /* AFTER processing the END line(add PostComment)
                 * skip EndOfFile characters added by some system
                 */
                if (line.equalsIgnoreCase("END"))
                    break;

                line = in.readLine();
                lineNumber++;
                    
                int numOfChar = unparsedStatement.length();
                unparsedStatement = unparsedStatement.delete(0, numOfChar);
            } // while
            in.close();
                
        } // try

        catch (IOException e)
        {
            e.getMessage();
        }
    } // end - processRobotProgram
    
    private void creOpComm(String line, boolean nativeLanguage, Element actListElem)
    {
        Element actElem = super.createActivityHeader(actListElem, line, "Operation");
        String [] nativeNames = {"Robot Language"}, nativeValues = {line};
        String [] commNames = {"Comment"}, commValues = {line};
        if (nativeLanguage == true)
            super.createAttributeList(actElem, nativeNames, nativeValues);
        else
            super.createAttributeList(actElem, commNames, commValues);
    }
    
    private void addToComments(String line, boolean nativeLanguage)
    {
        String commentName = "Comment" + String.valueOf(m_commentCount+1);
        m_commentNames[m_commentCount] = commentName;
        
        if (nativeLanguage == true)
            m_commentValues[m_commentCount] = "Robot Language:" + line;
        else 
            m_commentValues[m_commentCount] = line;

        m_commentCount++;
    } // end - addToComments

    private void processACCURACYStatement(String accuracyValue)
    {
        m_prevAccuracyValue = accuracyValue;
        boolean isInList = m_listOfAccuracyValues.contains(accuracyValue);
        if (!isInList)
        {
            m_listOfAccuracyValues.add(accuracyValue);

            boolean flyby = true;
            int idx = accuracyValue.indexOf("P");
            if (idx != -1)
                flyby = false;

            int pLevel = Integer.parseInt(accuracyValue.substring(0,1));

            int listIndex = m_listOfAccuracyValues.indexOf(accuracyValue);
            String accuracyProfileName = "Accuracy." + new Integer(listIndex + 1).toString();
            this.CreateAccuracyProfile(accuracyProfileName, flyby, m_AccuracyType, pLevel);
        }
    }

    private void processTOOLStatement(String toolNumber)
    {
        this.m_currentToolNumber = toolNumber;

        String toolProfileName;

        if (m_currentToolNumber != null || m_currentToolNumber != "")
        {
            toolProfileName = (String) m_toolNumberMapping.get(m_currentToolNumber);
            if (toolProfileName == null)
            {
                toolProfileName = "NachiTool" + String.valueOf(m_currentToolNumber);
                double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Double massValue = Double.valueOf("0.0");
                double [] cogValues = {0.0, 0.0, 0.0};
                double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Element tmpelem = super.createToolProfile(m_toolProfileListElement, toolProfileName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
            }
        }
        else
            toolProfileName = "Default";
    }
        
    private void processComments(Element addCommentElem, String commentPrefix)
    {   
        if (m_commentCount == 0)
        {
            return;
        }
        
        String [] commentNames = new String [m_commentCount];
        String [] commentValues = new String [m_commentCount];
		
        for (int ii = 0; ii < m_commentCount; ii++)
        {
            if (m_commentNames[ii].indexOf("Comment") == 0)
                commentNames[ii] = commentPrefix + m_commentNames[ii];
            else
                commentNames[ii] = m_commentNames[ii];
            commentValues[ii] = m_commentValues[ii];
        }
        super.createAttributeList(addCommentElem, commentNames, commentValues);
        m_commentCount = 0;
        clearComments();

    } // end - processComments

    private void clearComments()
    {
        for (int ii=0;ii<m_commentNames.length;ii++)
        {
            m_commentNames[ii] = "";
        }
        for (int ii=0;ii<m_commentValues.length;ii++)
        {
            m_commentValues[ii] = "";
        }
    } // end - clearComments

    private void processMOVEStatement(Matcher match, Element activityListElem)
    {
        //Create DOM Nodes and set appropriate attributes
        String activityName = "RobotMotion." + String.valueOf(m_moveCounter);
        Element actElem = super.createMotionActivityHeader(activityListElem, activityName);

        String toolProfileName = "H=1";
        String objProfileName = "";
        String motionProfileName = "Motion.";
        String concStr = "";
        String eoffStr = "";

        //String moveComponent = match.group(1);
        String motionType = match.group(2);
        String speedTimeComp = match.group(4);
        int listIndex = -1;

        //---------------------------------------------------------------------------------------
        // Sept 6th - Start
        int targetNumber = 0;
        targetNumber = m_targetNumber;

        int numAxesProcess = m_numRobotAxes+m_numExtAxes+m_numAuxAxes+m_numWorkAxes;
        String [] positionValues = new String [numAxesProcess];
        String components = match.group(3);
        String [] sPosValues = components.split(",");
        int numAxesProgram = sPosValues.length;
        int numAxesLess = numAxesProcess < numAxesProgram ? numAxesProcess : numAxesProgram;
        int numAxesMore = numAxesProcess > numAxesProgram ? numAxesProcess : numAxesProgram;
        for (int ii = 0; ii < numAxesProcess; ii++)
        {
            if (ii < numAxesLess)
                positionValues[ii] = sPosValues[ii].trim();
            else
                positionValues[ii] = "0.0";
        }

        //Add the target to a dynamic array
        m_listOfRobotTargets.add(targetNumber, positionValues);
        //Resize the array
        m_listOfRobotTargets.trimToSize();

        //Connect the array with the target type through hashtable 
        Integer tmpInt = new Integer(targetNumber);
        m_targetTypes.put(tmpInt.toString(), new Integer(m_targetType));

        //Connect the array with tag names through hashtable
        if (!m_currentTagPoint.equals(""))
        {   
            m_tagNames.put(tmpInt.toString()+1, m_currentTagPoint + ".");
            m_currentTagPoint = "";
        }
        else 
        { 
            String tpName = tmpInt.toString();
            m_tagNames.put(tmpInt.toString()+1, tpName + ".");
        }
        if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)
        {
            String [] auxAxisValues = {"0", "0", "0", "0", "0", "0"};
            // copy all the aux-axes values into the auxAxisValues array
            for (int ii = m_numRobotAxes, jj = 0; ii < numAxesProcess; ii++, jj++)
            {
                auxAxisValues[jj] = positionValues[ii];
            }

            //Get the target number as an object
            targetNumber = m_targetNumber;

            //Add the target to a dynamic array
            if (m_numAuxAxes > 0)
            {
                m_listOfAuxTargets.add(m_targetNumber, auxAxisValues);
                m_listOfAuxTargets.trimToSize();
            }
            if (m_numExtAxes > 0)
            {
                m_listOfExtTargets.add(m_targetNumber, auxAxisValues);
                m_listOfExtTargets.trimToSize();
            }
            if (m_numWorkAxes > 0)
            {
                m_listOfWorkTargets.add(m_targetNumber, auxAxisValues);
                m_listOfWorkTargets.trimToSize();
            }
        }
        // Sept 6th - End
        //---------------------------------------------------------------------------------------
        // Speed component is mandatory in Nachi MOVE statement
        if (speedTimeComp != null) // must be non-null
        {
            boolean isInList = false;
            String speedTimeValue = match.group(5);
            if (speedTimeValue != null) // must be non-null
            {
                isInList = m_listOfSpeedValues.contains(speedTimeValue);
                if (!isInList)
                    m_listOfSpeedValues.add(speedTimeValue);

                listIndex = m_listOfSpeedValues.indexOf(speedTimeValue);
            }

            motionProfileName += new Integer(listIndex + 1).toString();

            if (!isInList)
            {
                if (speedTimeComp.equalsIgnoreCase("S"))	//Speed
                {
                    Double speedVal = Double.valueOf(speedTimeValue);
                    double speed = speedVal.doubleValue()/1000.0;
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speed,
                                         100.0, 100.0, 100.0);
                }
                else	// Time
                {
                    Double timeVal = Double.valueOf(speedTimeValue);
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeVal.doubleValue(),
                                         100.0, 100.0, 100.0);
                }
            }
        }
        //----------------------------------------------------------------------------------------
        // Tool component in Nachi MOVE statement is optional, if omitted, use the previous value
        String toolComp = match.group(8);
        if (toolComp != null)
        {
            String toolVal = match.group(9);
            if (toolVal != null)
            {
                this.m_currentToolNumber = toolVal;
                toolProfileName = "H=" + toolVal;
            }
        }

        if (m_currentToolNumber != null || m_currentToolNumber != "")
        {
            if (m_toolNumberMapping.contains(toolProfileName) == false)
                toolProfileName = (String) m_toolNumberMapping.get(m_currentToolNumber);

            if (toolProfileName == null)
            {
                toolProfileName = "NachiTool" + String.valueOf(m_currentToolNumber);
                double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Double massValue = Double.valueOf("0.0");
                double [] cogValues = {0.0, 0.0, 0.0};
                double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Element tmpelem = super.createToolProfile(m_toolProfileListElement, toolProfileName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
                m_toolNumberMapping.put(String.valueOf(m_currentToolNumber), toolProfileName);
            }
        }
        else
            toolProfileName = "Default";

        //----------------------------------------------------------------------------------------
        // Accuracy component in Nachi MOVE statement is optional, if omitted, previous value is taken
        String accuracyProfileName = "Accuracy.";
        String accComp = match.group(6);

        if (accComp != null)
        {
            String accuracyValue = match.group(7);
            if (accuracyValue != null)
            {
                boolean isInList = m_listOfAccuracyValues.contains(accuracyValue);
                if (!isInList)
                    m_listOfAccuracyValues.add(accuracyValue);

                listIndex = m_listOfAccuracyValues.indexOf(accuracyValue);
                accuracyProfileName += new Integer(listIndex + 1).toString();

                if (!isInList)
                {
                    boolean flyby = true;
                    int idx = accuracyValue.indexOf("P");
                    if (idx != -1)
                        flyby = false;

                    int pLevel = Integer.parseInt(accuracyValue.substring(0,1));

                    this.CreateAccuracyProfile(accuracyProfileName, flyby, m_AccuracyType, pLevel);
                }

                m_prevAccuracyValue = accuracyValue;
            }
        }
        else	// use previous accuracy value
        {
            if (m_prevAccuracyValue != null)
            {
                listIndex = m_listOfAccuracyValues.indexOf(m_prevAccuracyValue);
                accuracyProfileName += new Integer(listIndex + 1).toString();
            }
        }
        //---------------------------------------------------------------------------------------
        // Tool component in Nachi MOVE statement is optional, if omitted, use the previous value
        /*
        String toolComp = match.group(8);
        if (toolComp != null)
        {
            String toolVal = match.group(9);
            if (toolVal != null)
                this.m_currentToolNumber = toolVal;
        }

        if (m_currentToolNumber != null || m_currentToolNumber != "")
        {
            toolProfileName = (String) m_toolNumberMapping.get(m_currentToolNumber);
            if (toolProfileName == null)
            {
                toolProfileName = "Default";
            }
        }
        else
            toolProfileName = "Default";

        double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
        Double massValue = Double.valueOf("0.0");
        double [] cogValues = {0.0, 0.0, 0.0};
        double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};

        Element tmpelem = super.createToolProfile(m_toolProfileListElement, toolProfileName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
        */
        //------------------------------------------------------------------------------------------
        // ObjectProfile
        if (m_currentObjectNumber != null || m_currentObjectNumber != "")
        {
            objProfileName = (String) m_objectNumberMapping.get(m_currentObjectNumber);
            if (objProfileName == null)
                objProfileName = "ObjectProfile." + m_currentObjectNumber;
        }
        else
            objProfileName = "ObjectProfile." + m_currentObjectNumber;

        //------------------------------------------------------------------------------------------
        DNBIgpOlpUploadEnumeratedTypes.OrientMode eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS;
        if (m_orientMode.equalsIgnoreCase("1_axis") == true )
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS;
        if (m_orientMode.equalsIgnoreCase("3_axis") == true )
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.THREE_AXIS;
        if (m_orientMode.equalsIgnoreCase("wrist") == true )
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.WRIST;
        
        DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
        if (motionType.equalsIgnoreCase("P"))
        {
            firstCircular = true;
            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype);
        }
        else if (motionType.equalsIgnoreCase("L"))
        {
            firstCircular = true;
            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype, eOrientType);
        }
        else
        {
            if (firstCircular == true)
            {
                firstCircular = false;
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
            }
            else
            {
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
            }
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype, eOrientType);
        }
 
        //Call the method to process the target
        processTarget(actElem, match);
        
        // NRL attribute
        String [] attrNames = {"PosReg"};
        String [] attrValues = {String.valueOf(m_moveCounter)};
        super.createAttributeList(actElem, attrNames, attrValues);
        
        processComments(actElem, "Pre");
        m_currentElement = actElem;
        m_moveCounter++;
        m_targetNumber = m_targetNumber + 1;
		
        if (match.groupCount() > 10 && match.group(11) != null)
        {
            int matchShift = 10;
            processSPOTStatement(match, matchShift, activityListElem);
        }
        
    } // end - processMOVEStatement

    /* If RailAxisIsJoint1 is TRUE, move targetValues[0] to targetValues[6] and
     * targetValues[1]~targetValues[6] up one element
     */
    private void swapTargetValues(String [] targetValues)
    {
        if (m_numAuxAxes <= 0 || railAxisIsJoint1 == false)
            return;

        String s = targetValues[0];
        for (int ii=0; ii<6; ii++)
            targetValues[ii] = targetValues[ii+1];
        targetValues[6] = s;
    }

    private void processPOSEMOVEStatement(Matcher match, Element activityListElem)
    {
        //System.out.println("P#### POSE #####");
        String tgtType = match.group(1);
        String motType = match.group(2);
        String speedProf = match.group(4);
        String speedProfVal = match.group(5);
        String AccProf = match.group(6);
        String accProfVal = match.group(7);
        String ToolProf = match.group(8);
        String toolProfVal = match.group(9);

        //Add the target to a dynamic array
        String poseNumStr = match.group(3);

        Integer poseNum = new Integer(poseNumStr);
        int idx = poseNum.intValue() - 1; // Pos index starts from '1', here the starting index is '0'

        //Connect the array with the target type through hashtable 
        Integer tmpInt = new Integer(idx);

        //Connect the array with tag names through hashtable
        if (!m_currentTagPoint.equals(""))
        {   
            m_tagNames.put(tmpInt.toString()+1, m_currentTagPoint + ".");
            m_currentTagPoint = "";
        }
        else 
        { 
            String tpName = tmpInt.toString();
            m_tagNames.put(tmpInt.toString()+1, tpName + ".");
        }

        //Create DOM Nodes and set appropriate attributes
        String activityName = "RobotMotion." + String.valueOf(m_moveCounter);
        Element actElem = super.createMotionActivityHeader(activityListElem, activityName);

		String toolProfileName = "";
        String objProfileName = "";
        String motionProfileName = "Motion.";
        String concStr = "";
        String eoffStr = "";

        String motionType = match.group(2);
        String speedTimeComp = match.group(4);
        int listIndex = -1;
        //---------------------------------------------------------------------------------------
        // Speed component is mandatory in Nachi MOVE statement
        if (speedTimeComp != null) // must be non-null
        {
            boolean isInList = false;
            String speedTimeValue = match.group(5);
            if (speedTimeValue != null) // must be non-null
            {
                isInList = m_listOfSpeedValues.contains(speedTimeValue);
                if (!isInList)
                    m_listOfSpeedValues.add(speedTimeValue);

                listIndex = m_listOfSpeedValues.indexOf(speedTimeValue);
            }

            motionProfileName += new Integer(listIndex + 1).toString();

            if (!isInList)
            {
                if (speedTimeComp.equalsIgnoreCase("S"))	//Speed
                {
                    Double speedVal = Double.valueOf(speedTimeValue);
                    double speed = speedVal.doubleValue()/1000.0;
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speed, 
                                         100.0, 100.0, 100.0);
                }
                else	// Time
                {
                    Double timeVal = Double.valueOf(speedTimeValue);
                    createMotionProfile( m_motionProfileListElement, motionProfileName, 
                                         DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeVal.doubleValue(),
                                         100.0, 100.0, 100.0);
                }
            }
        }
        //---------------------------------------------------------------------------------------
        // Accuracy component in Nachi MOVE statement is optional, if omitted, previous value is taken
        String accuracyProfileName = "Accuracy.";
        String accComp = match.group(6);
        if (accComp != null)
        {
            String accuracyValue = match.group(7);
            if (accuracyValue != null)
            {
                boolean isInList = m_listOfAccuracyValues.contains(accuracyValue);
                if (!isInList)
                    m_listOfAccuracyValues.add(accuracyValue);

                listIndex = m_listOfAccuracyValues.indexOf(accuracyValue);
                    accuracyProfileName += new Integer(listIndex + 1).toString();

                if (!isInList)
                {
                    boolean flyby = true;
                    int Idx = accuracyValue.indexOf("P");
                    if (Idx != -1)
                        flyby = false;

                    int pLevel = Integer.parseInt(accuracyValue.substring(0,1));

                    this.CreateAccuracyProfile(accuracyProfileName, flyby, m_AccuracyType, pLevel);
                }

                m_prevAccuracyValue = accuracyValue;
            }
        }
        else	// use previous accuracy value
        {
            if (m_prevAccuracyValue != null)
            {
                listIndex = m_listOfAccuracyValues.indexOf(m_prevAccuracyValue);
                accuracyProfileName += new Integer(listIndex + 1).toString();
            }
        }
        //---------------------------------------------------------------------------------------
        // Tool component in Nachi MOVE statement is optional, if omitted, use the previous value
        String toolComp = match.group(8);
        if (toolComp != null)
        {
            String toolVal = match.group(9);
            if (toolVal != null)
                this.m_currentToolNumber = toolVal;
        }

        if (m_currentToolNumber != null || m_currentToolNumber != "")
        {
            toolProfileName = (String) m_toolNumberMapping.get(m_currentToolNumber);
            if (toolProfileName == null)
            {
                toolProfileName = "NachiTool" + String.valueOf(m_currentToolNumber);
                double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Double massValue = Double.valueOf("0.0");
                double [] cogValues = {0.0, 0.0, 0.0};
                double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Element tmpelem = super.createToolProfile(m_toolProfileListElement, toolProfileName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
                m_toolNumberMapping.put(String.valueOf(m_currentToolNumber), toolProfileName);
            }
        }
        else
            toolProfileName = "Default";

        //------------------------------------------------------------------------------------------
        // ObjectProfile
        if (m_currentObjectNumber != null || m_currentObjectNumber != "")
        {
            objProfileName = (String) m_objectNumberMapping.get(m_currentObjectNumber);
            if (objProfileName == null)
                objProfileName = "ObjectProfile." + m_currentObjectNumber;
        }
        else
            objProfileName = "ObjectProfile." + m_currentObjectNumber;

        //------------------------------------------------------------------------------------------
        DNBIgpOlpUploadEnumeratedTypes.OrientMode eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.TWO_AXIS;
        if (m_orientMode.equalsIgnoreCase("1_axis") == true )
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS;
        if (m_orientMode.equalsIgnoreCase("3_axis") == true )
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.THREE_AXIS;
        if (m_orientMode.equalsIgnoreCase("wrist") == true )
            eOrientType = DNBIgpOlpUploadEnumeratedTypes.OrientMode.WRIST;

        DNBIgpOlpUploadEnumeratedTypes.MotionType eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
        if (motionType.equalsIgnoreCase("P"))
        {
            firstCircular = true;
            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION;
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype);
        }
        else if (motionType.equalsIgnoreCase("L"))
        {
            firstCircular = true;
            eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION;
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype, eOrientType);
        }
        else
        {
            if (firstCircular == true)
            {
                firstCircular = false;
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION;
            }
            else
            {
                eMotype = DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION;
            }
            super.createMotionAttributes(actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, eMotype);
        }
            
        //Call the method to process the target
        processTarget(actElem, match);

        processComments(actElem, "Pre");
        m_currentElement = actElem;
        m_moveCounter++;
        m_targetNumber = m_targetNumber + 1;
    } // end - processPOSEMOVEStatement

    private void processUSEStatement(Matcher match, Element activityListElem) throws Exception
    {
        String posFileName = m_sProgPath.replaceFirst(".awc", ".awp");
        BufferedReader posIn = new BufferedReader(new FileReader(posFileName));

        String posLine = posIn.readLine();

        // Read the header info first
        try
        {
            while(posLine != null)
            {
                if (posLine.equals(""))
                {
                    posLine = posIn.readLine();
                    continue;
                }
                processHeaderInfoLine(posLine);

                posLine = posIn.readLine();
                break;
            }
        }
        catch(Exception e)
        {
            e.getMessage();
        }

        // Read the position data now-
        try
        {
            while(posLine != null)
            {
                if (posLine.equals(""))
                {
                    posLine = posIn.readLine();
                    continue;
                }

                processPoseLine(posLine);

                posLine = posIn.readLine();
            }
            posIn.close();
        }

        catch(Exception e)
        {
            e.getMessage();
        }
    } // end - processUSEStatement

    private void processHeaderInfoLine(String line) throws Exception
    {
        try
        {
            String [] PosComps = line.split("\\s+");
            int Len = PosComps.length;

            m_sControllerName = PosComps[0].trim();
            m_nPosNumbers = Integer.parseInt(PosComps[2].trim());
            m_sRobotModelName = PosComps[3].trim();
            m_nNumberOfAxes = Integer.parseInt(PosComps[4].trim());

        }
        catch (Exception e)
        {
            e.getMessage();
        }
    }

    private void processPoseLine(String posLine) throws Exception
    {
        try
        {
            String [] posComps = posLine.split("\\s+");
            int Len = posComps.length;

            String [] posData = new String[m_numRobotAxes];

            for (int jj = 0; jj < m_numRobotAxes; jj++)
            {
                posData[jj] = posComps[jj + 1];
            }

            // Add the pulse values to a dynamic array
            int idx = Integer.parseInt(posComps[0]) - 1;

            m_listOfPoseValues.add(idx, posData);
            m_listOfPoseValues.trimToSize();

            //Add the target to a dynamic array
            m_listOfRobotTargets.add(idx,posData);

            //Resize the array
            m_listOfRobotTargets.trimToSize();

            //Connect the array with the target type through hashtable 
            Integer tmpInt = new Integer(idx);
            m_targetTypes.put(tmpInt.toString(),new Integer(m_targetType));

            if (m_numAuxAxes > 0)
            {
                String [] auxAxesValues = new String[m_numAuxAxes];

                for (int jj = 0; jj < m_numAuxAxes; jj++)
                {
                    auxAxesValues[jj] = posComps[m_numRobotAxes + 1 + jj];
                }
                m_listOfAuxTargets.add(idx, auxAxesValues);
                m_listOfAuxTargets.trimToSize();
            }

            if (m_numExtAxes > 0)
            {
                String [] extAxesValues = new String[m_numExtAxes];

                for (int jj = 0; jj < m_numExtAxes; jj++)
                {
                    extAxesValues[jj] = posComps[jj + m_numRobotAxes + m_numAuxAxes + 1];
                }
                m_listOfExtTargets.add(idx, extAxesValues);
                m_listOfExtTargets.trimToSize();
            }
            if (m_numWorkAxes > 0)
            {
                String [] workAxesValues = new String[m_numWorkAxes];
                for (int jj = 0; jj < m_numWorkAxes; jj++)
                {
                    workAxesValues[jj] = posComps[jj + m_numRobotAxes + m_numAuxAxes + m_numExtAxes + 1];
                }
                m_listOfWorkTargets.add(idx, workAxesValues);
                m_listOfWorkTargets.trimToSize();
            }
        }// try 

        catch (IndexOutOfBoundsException e)
        {
            e.getMessage();
        }
        catch (Exception e)
        {
            e.getMessage();
        }
    } // end - processPoseLine

    private void processDELAYStatement (Matcher match, Element activityListElem)
    {
        String timeInSec = match.group(1);
        createTimerStatement(timeInSec, "RobotDelay.", activityListElem, true);
        //String delayActName = "RobotDelay." + String.valueOf(m_delayCounter);
        //Element actElem = super.createDelayActivity(activityListElem, delayActName, Double.valueOf("0.0"), Double.valueOf(timeInSec));
        //processComments( actElem, "Pre");
        //m_currentElement = actElem;
        //m_delayCounter++;
    }

    private void processSETStatement(Matcher match, Element activityListElem, boolean bReset)
    {
        String signalVariable = match.group(1);
        String portNumber = match.group(2);
        String signalName = signalVariable + portNumber;
        String signal = null;
        if (match.groupCount() > 2)
            signal = match.group(3);
        
        boolean signalValue = true;
        String status = "ON";
        if ((signal != null && signal.equals("0")) || bReset)
        {
            signalValue = false;
            status = "OFF";
        }

        String activityName = "Set-" + signalName + " = " + status;
        String duration = "0";

        Element actElem = super.createSetIOActivity(activityListElem, activityName, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(duration));

        createBooleanAttributeList("OutputSETM", false, m_listOfSetM, activityListElem, actElem);
        processComments(actElem, "Pre");
        m_currentElement = actElem;
    }

    private void processSETMStatement(Matcher match, Element activityListElem)
    {
        String signalVariable = match.group(1);
        String portNumber = match.group(2);
        String condition = match.group(3);

        boolean signalValue = false;
        String status = "OFF";

        if (condition.equals("1"))
        {
            signalValue = true;
            status = "ON";
        }
        else
        {
            signalValue = false;
            status = "OFF";
        }

        String signalName = signalVariable + portNumber;
        String ioname;
        String duration = match.group(4);

        ioname = "Set-" + signalName + " = " + status;
        if (duration != null)
            ioname += " [" + duration + "s]";
        else
            duration = "0";

        Element actElem = super.createSetIOActivity(activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(duration));

        createBooleanAttributeList("OutputSETM", true, m_listOfSetM, activityListElem, actElem);
        processComments(actElem, "Pre");
        m_currentElement = actElem;
    }

    private void createBooleanAttributeList(String attrName, boolean attrValue, ArrayList alist, Element activityListElem, Element actElem)
    {
        String [] attrNames = new String[1];
        String [] attrValues = new String[1];
        attrNames[0] = attrName;
        attrValues[0] = String.valueOf(attrValue);
        Boolean obj = Boolean.valueOf(attrValue);
        // first one, create on ActivityList
        // subsequent ones, not in the list, create on Activity;
        //                  already in list, different than the first one, create on Activity
        if (alist.size() == 0)
        {
            alist.add(obj);
            super.createAttributeList(activityListElem, attrNames, attrValues);
        }
        else
        {
            boolean isInList = alist.contains(obj);
            if (isInList == false)
            {
                alist.add(obj);
                super.createAttributeList(actElem, attrNames, attrValues);
            }
            else
            {
                // The first status (SetM true|false) is created on ActivityList
                // If the current status is not the first on the list (index 0), create on Activity
                if (alist.indexOf(obj) != 0)
                    super.createAttributeList(actElem, attrNames, attrValues);
            }
        }
    }

    private void processWAITStatement(Matcher match, Element activityListElem)
    {
        String signalVariable = match.group(1);
        String portNumber = match.group(2);
        String maxTime = match.group(3);
        String status = "ON";

        boolean signalValue = true;

        String signalName = signalVariable + portNumber;
        String actName = "Wait Until " + signalName + " = " + status;
            
        Element actElem = super.createWaitForIOActivity( activityListElem, actName, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(maxTime));
           
        processComments(actElem, "Pre");
        m_currentElement = actElem;
    }

    private void processWAITIStatement(Matcher match, int matchShift, Element activityListElem)
    {
        String waitCommand = "WAITI";
        String signalVariable = "I";
        String portNumber = "1";
        switch (matchShift)
        {
            case 1:
                waitCommand = "WAITI";
                signalVariable = match.group(1);
                portNumber = match.group(2);
                break;
            default:
                waitCommand = match.group(1);
                signalVariable = match.group(2);
                portNumber = match.group(3);
        }

        String maxTime = "0";	// max time = 0 for WAITI command
        String status = "ON";
        boolean signalValue = true;
        if (waitCommand.equalsIgnoreCase("WAITJ"))
        {
            status = "OFF";
            signalValue = false;
        }

        String signalName = signalVariable + portNumber;
        String activityName = "Wait Until " + signalName + " = " + status;

        Element actElem = super.createWaitForIOActivity(activityListElem, activityName, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(maxTime));

        processComments(actElem, "Pre");
        m_currentElement = actElem;
    }

    private void processSPOTStatement(Matcher match, int matchShift, Element activityListElem)
    {
        String [] attrList = {"Gun_Number", "Weld_Cond_Number", "Weld_Seq_Number", "Weld_Point_Number"};
        String [] attrValues = {"-1", "-1", "-1", "-1"};
        String gunNumber = "0";
        String weldCondNumber = "0";
        String weldSeqNumber = "0";
        String weldPointNumber = "0";
        String actionName = "AWSpotWeld";
        switch(matchShift)
        {
            case 10:
                attrValues[0] = match.group(1+matchShift);
                //actionName += "Multi";
                break;
            default:
                for (int ii=0; ii<4; ii++)
                    attrValues[ii] = match.group(ii+1);
                
                gunNumber = match.group(1);
                weldCondNumber = match.group(2);
                weldSeqNumber = match.group(3);
                weldPointNumber = match.group(4);
        }
        
        String spotActionName = actionName + String.valueOf(m_spotCounter);

        Element actionElem = super.createActionHeader( activityListElem, spotActionName, actionName, m_mountedGunNames[0]);
        Element attrListElem = super.createAttributeList(actionElem, attrList, attrValues);
        
        processComments(actionElem, "Pre");
        m_currentElement = activityListElem;

        String weldHomeName = (String)m_weldScheduleHomes.get(weldCondNumber);
        String openHomeName = (String)m_backupHomes.get(weldSeqNumber);
        String weldScheduleDelay = (String)m_weldScheduleDelays.get(weldCondNumber);

        String delayTime = "1.5";
        if (weldScheduleDelay != null)
            delayTime = weldScheduleDelay;

        // weld home move
        if (weldHomeName == null)
            weldHomeName = (String)m_listofHomeNames.get(0);

        Element spotActivityListElem = super.createActivityListWithinAction(actionElem);

        processSpotHomeMove(weldHomeName, spotActivityListElem);

        // delay
        createTimerStatement(delayTime, "DelayActivity.", spotActivityListElem, false);

        // open/backup home move

        if (openHomeName == null)
        {
            if (m_listofHomeNames.size() > 1)
            {
                openHomeName = (String)m_listofHomeNames.get(1);
            }
            else
            {
                openHomeName = "Home_2";
            }
        }

        processSpotHomeMove(openHomeName, spotActivityListElem);
 
        // TODO - retract statement?
        //if (retractStr != null && !retractStr.equals(""))
        //{
            //processBackupStatement( activityListElem, gunName, gundata);
        //}

        m_spotCounter++;
    }
    
    private void processCALLPStatement(String subProgNum, Element activityListElem, String robotProgramName)
    {
        File f = new File(robotProgramName);
        String nameNoPath = f.getName();

        while (subProgNum.length() < 3)
            subProgNum = "0" + subProgNum;
        
        int iLength = nameNoPath.length();
        String progName = nameNoPath.substring(0, iLength-3) + subProgNum;

        String callActName = "RobotCall." + String.valueOf(m_callCounter);
        Element actElem = super.createCallTaskActivity(activityListElem, callActName, progName);
        processComments( actElem, "Pre");
        m_currentElement = actElem;
        m_callCounter++;
        
        int index = m_listOfCalledPrograms.indexOf(progName);
        if (index < 0)
        {
            m_listOfCalledPrograms.add(m_calledProgramCounter, progName);
            m_listOfCalledPrograms.trimToSize();
            m_calledProgramCounter++;
        }
    } // end processCALLPStatement
    
    private void processGOSUBStatement(String sLabel, Element activityListElem)
    {
        String callActName = "RobotCall." + String.valueOf(m_callCounter);
        Element actElem = super.createCallTaskActivity(activityListElem, callActName, sLabel);
        processComments( actElem, "Pre");
        m_currentElement = actElem;
        m_callCounter++;
    } // end processGOSUBStatement

    private void CreateAccuracyProfile(String accuracyProfileName, boolean flyby, 
                                       int accuracyType, int pLevel)
    {
        double accuracyValue;

        if (accuracyType == 0)
        {
            switch (pLevel)
            {
                case 1:
                    accuracyValue = 0;
                    break;
                case 2:
                    accuracyValue = 5;
                    break;
                case 3:
                    accuracyValue = 10;
                    break;
                case 4:
                    accuracyValue = 25;
                    break;
                case 5:
                    accuracyValue = 50;
                    break;
                case 6:
                    accuracyValue = 100;
                    break;
                case 7:
                    accuracyValue = 250;
                    break;
                case 8:
                default:
                    accuracyValue = 500;
                    break;
            }
            createAccuracyProfile(m_accuracyProfileListElement, accuracyProfileName,
                                  DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE,
                                  flyby, accuracyValue);
        }
        else
        {
            switch (pLevel)
            {
                case 1:
                    accuracyValue = 0;
                    break;
                case 2:
                    accuracyValue = 12.5;
                    break;
                case 3:
                    accuracyValue = 25;
                    break;
                case 4:
                    accuracyValue = 37.5;
                    break;
                case 5:
                    accuracyValue = 50;
                    break;
                case 6:
                    accuracyValue = 62.5;
                    break;
                case 7:
                    accuracyValue = 75;
                    break;
                case 8:
                default:
                    accuracyValue = 87.5;
                    break;
            }
            createAccuracyProfile(m_accuracyProfileListElement, accuracyProfileName,
                                  DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED,
                                  flyby, accuracyValue);
        }

    } // end - CreateAccuracyProfile

    private void processTarget(Element robotMotionElem, Matcher match)
    {
        String [] targetValues = (String [])m_listOfRobotTargets.get(m_targetNumber);

        String moveComponent = match.group(1);
        swapTargetValues(targetValues);

        Integer targetType = Integer.getInteger("0");
        if (moveComponent.equalsIgnoreCase("MOVEJ"))
        {
            targetType = new Integer(JOINT);
        }
        if (moveComponent.equalsIgnoreCase("MOVE"))
        {
            targetType = new Integer(CARTESIAN);
        }

        //Process the joint target
        if (targetType.equals(new Integer(JOINT)))
        {
            if (m_joint3Linked == true)
            {
                // Do not account for the linking of axis 2 & axis 3
            }
            else	// By default account for Axis3Adjust
            {	
                Double robot_axes_3 = new Double(targetValues[2]);
                Double robot_axes_2 = new Double(targetValues[1]);

                double igrip_axis_3 = robot_axes_3.doubleValue() - robot_axes_2.doubleValue()
                                        + m_axis3Adjust;

                Double new_igrip_axes_3 = new Double(igrip_axis_3);
                targetValues[2] = new_igrip_axes_3.toString();
            }

            Element targetElem = super.createTarget(robotMotionElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false);
            m_createJointTarget = true;
            ArrayList [] jointTargetArrayList = new ArrayList[m_numRobotAxes+m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
            //Print out the target (joint) values
            for (int ii = 0; ii < 6; ii++)
                formatJointValueInPulses(jointTargetArrayList, targetValues[ii] , "Joint", null, ii);
                
            //If there are auxiliary axes present, print out their joint values
            if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)
            {
                Integer extendedTargetNum = new Integer(m_targetNumber);
                String Position = extendedTargetNum.toString();
                if (m_numAuxAxes > 0)//&& m_auxAxesCount == m_numAuxAxes)
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfAuxTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes, targetValues);
                if (m_numExtAxes > 0)//&& m_extAxesCount == m_numExtAxes)
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING , m_numExtAxes, targetValues);
                if (m_numWorkAxes > 0)//&& m_workAxesCount == m_numWorkAxes)
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes, targetValues);
            }

            super.createJointTarget(targetElem, jointTargetArrayList);

            // TODO - does NachiAX have any use of the below lines of code?
            //if (explicit == false)
            //{
                    //m_commentNames[m_commentCount] = "Tag Name";
                    //m_commentValues[m_commentCount] = tagcomponent;
                    //m_commentCount++;
            //}
        }
        else if (targetType.equals(new Integer(CARTESIAN)))
        {
            double m_targetYaw;
            double m_targetPitch;
            double m_targetRoll;
            
            Element targetElem = super.createTarget(robotMotionElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, false);
            m_createJointTarget = false;
            ArrayList [] jointTargetArrayList = new ArrayList[m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
                
            double [] cartValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            cartValues[0] = Double.valueOf(targetValues[0]).doubleValue() * 0.001;
            cartValues[1] = Double.valueOf(targetValues[1]).doubleValue() * 0.001;
            cartValues[2] = Double.valueOf(targetValues[2]).doubleValue() * 0.001;

            m_targetRoll = Double.valueOf(targetValues[3]).doubleValue();
            m_targetPitch = Double.valueOf(targetValues[4]).doubleValue();
            m_targetYaw = Double.valueOf(targetValues[5]).doubleValue();
                
            cartValues[3] = m_targetYaw;
            cartValues[4] = m_targetPitch;
            cartValues[5] = m_targetRoll;

            // TODO- do not know as of today..how config is specified in Nachi MOVE
            String cfgName = "Config_1";
            Element cartTargetElem = super.createCartesianTarget(targetElem, cartValues, cfgName);
                
            if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)
            {
                Integer extendedTargetNum = new Integer(m_targetNumber);
                String Position = extendedTargetNum.toString();
                if (m_numAuxAxes > 0)//&& m_numAuxAxes == m_auxAxesCount )
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfAuxTargets,  DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes, targetValues);
                if (m_numExtAxes > 0)//&& m_numExtAxes == m_extAxesCount )
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING, m_numExtAxes, targetValues);
                if (m_numWorkAxes > 0)//&& m_numWorkAxes == m_workAxesCount )
                    processExtendedTarget(jointTargetArrayList, Position, m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes, targetValues);

                super.createJointTarget( targetElem, jointTargetArrayList);
            } //end if (m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)
 
            int [] turnNumbers = {0,0,0,0};
            //turnNumbers[0] = getTurnValue(Integer.valueOf(targetValues[7]).intValue());
            //turnNumbers[1] = getTurnValue(Integer.valueOf(targetValues[8]).intValue());
            //turnNumbers[3] = getTurnValue(Integer.valueOf(targetValues[9]).intValue());

            //super.createTurnNumbers( cartTargetElem, turnNumbers);
            String tagcomp = "P" + m_moveCounter + ".";
            super.createTag( cartTargetElem, tagcomp);
            tagcomp = "";
        } //end else if
		
    }	// end - processTarget

    private void processExtendedTarget(ArrayList [] jointTargetArrayList, 
                                       String sExtendedTarget, 
                                       ArrayList listOfExtendedTargets,  
                                       DNBIgpOlpUploadEnumeratedTypes.AuxAxisType extendedAxisType, 
                                       int len, 
                                       String [] targetValues)
    {
        Integer extendedTargetNum = new Integer(sExtendedTarget);
        String [] extendedValues;
        extendedValues = new String[6];

        int start = 1;

        String extendedAxisStrType = extendedAxisType.toString();

        for (int ii = 0; ii < targetValues.length - 6; ii++)
        {
            extendedValues[ii] = targetValues[ii+6];
        }
        //else 
        /*
        {
            extendedValues = (String []) listOfExtendedTargets.get(extendedTargetNum.intValue());
        }
        */
        /*
        if (extendedAxisStrType.equals("RailTrackGantry"))
        {
            start = m_auxAxesStart;
        }
        else if (extendedAxisStrType.equals("EndOfArmTooling"))
        {
            start = m_extAxesStart;
        }
        else 
        {
            start = m_workAxesStart;
        }
        */
        //int start = 1;
        if (extendedAxisStrType.equals("RailTrackGantry"))
            start = 1;
        else if (extendedAxisStrType.equals("EndOfArmTooling"))
            start = m_numAuxAxes + 1;
        else
            start = m_numAuxAxes + m_numExtAxes + 1;

        for (int ii = start-1; ii < start + len - 1; ii++)
        {
            formatJointValueInPulses(jointTargetArrayList, extendedValues[ii], "AuxJoint", extendedAxisType, ii);
        } //end for
    }

    public void formatJointValueInPulses(ArrayList [] jointTargetArrayList, 
                                         String sTarget, 
                                         String robotOrAuxJoint, 
                                         DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxJointType, 
                                         int ii)
    {
        double targetScale = 1.0;
        String jointName = "";
        String axesType = "";
        int index = ii;
        int arrayIndex = ii;

        if (auxJointType != null)
            index = ii+m_numRobotAxes;

        if (m_createJointTarget == true)
            arrayIndex = index;

        axesType = m_axisTypes[index];
        if (axesType.equals("Translational"))
            targetScale = .001;
        jointName = "Joint" + String.valueOf(index+1);

        if (auxJointType != null)
        {
            jointTargetArrayList[arrayIndex] = new ArrayList(5);
        }
        else 
        {
            jointTargetArrayList[arrayIndex] = new ArrayList(4);
        }

        jointTargetArrayList[arrayIndex].add( 0, jointName);

        double targetValue = 0.0;

        try 
        {
            targetValue = Double.parseDouble(sTarget);
            targetValue = targetValue*targetScale;
        }

        catch(NumberFormatException e)
        {
            e.getMessage();
            targetValue = Double.NaN;
        }

        if (axesType.equals("Rotational"))
            targetValue = Math.toRadians(targetValue);

        String scaledTarget = String.valueOf(targetValue);

        jointTargetArrayList[arrayIndex].add(1, Double.valueOf(scaledTarget));

        String dofNum = String.valueOf(index+1);

        jointTargetArrayList[arrayIndex].add(2, Integer.valueOf(dofNum));

        if (axesType.equals("Rotational"))
        {
            jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL);
        }
        else 
        {
            jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL);
        }

        if (auxJointType != null)
            jointTargetArrayList[arrayIndex].add(4, auxJointType);
    }

    private int getTurnValue(int axCfg)
    {
        int turn = 0;
        switch(axCfg)
        {
            case 0:
            case 1:
            case -1:
            case -2: 
                turn = 0;
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                turn = 1;
                break;
            case -3:
            case -4:
            case -5:
            case -6:
                turn = -1;
        }
        return(turn);
    }

    private void createTimerStatement( String timeInSec, String sActName, Element activityListElem, boolean doComments )
    {   
        //Create DOM Nodes and set appropriate attributes
        String delayActName = sActName + String.valueOf(m_delayCounter);
        Element actElem = super.createDelayActivity(activityListElem, delayActName, Double.valueOf("0.0"), Double.valueOf(timeInSec));
        if (doComments == true)
        {
            processComments(actElem, "Pre");
            m_currentElement = actElem;
        }
        m_delayCounter++;
    }

    private void processSpotHomeMove(String homeName, Element actListElem )
    {
        String activityName = "RobotMotion." + String.valueOf(m_gunmoveCounter++);
 
        super.createMotionActivityWithinAction( actListElem, activityName, Double.valueOf("0.0"), Double.valueOf("1.0"), DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,  homeName);
    }

    private boolean isLineNumPresent(String line)
    {
        String testStr = new String(line);
        char [] charArray = testStr.toCharArray();

        Character CharObj = new Character(charArray[0]);

        int strLen = line.length();

        while (((CharObj.isDigit(charArray[m_nIncr]) == true) ||
               (CharObj.isWhitespace(charArray[m_nIncr]) == true)) && 
               (m_nIncr < strLen))
        {
            m_nIncr = m_nIncr + 1;
        }

        if (m_nIncr == 0)
            return false;
        else
            return true;
    }

    //-------------------------------------------------------------------
    // preProcessLine
    //-------------------------------------------------------------------
    private String preProcessLine(String line)
    {
        int strLen = line.length();

        char [] charArray = line.toCharArray();
        String newStr = new String(charArray, m_nIncr, strLen-m_nIncr);

        newStr = newStr.trim();

        m_nIncr = 0;

        return newStr;
    }
    
    private void processARCONStatement(Matcher match, Element activityListElem)
    {
        String startCondNum = match.group(1);
        String sUserProfileType = "NAWArcONTable." + startCondNum;
        
        Element lastMovActivityElem = findLastMoveActivityElem(activityListElem);
        if (lastMovActivityElem == null)
            return;
        
        lastMovActivityElem.setNodeValue("ARCON");
        Element actName = findChildElemByName(lastMovActivityElem, "ActivityName", null, null);
        Element moAttr = findChildElemByName(lastMovActivityElem, "MotionAttributes", null, null);
        Element moType = findChildElemByName(moAttr, "MotionType", null, null);
        Element uPmot = m_xmlDoc.createElement("UserProfile");
        moAttr.insertBefore(uPmot, moType);
        uPmot.setAttribute("Type", sUserProfileType);
        String uProfileName = sUserProfileType + ".1";
        Text uPVal = m_xmlDoc.createTextNode(uProfileName);
        uPmot.appendChild(uPVal);
        
        Element uPList = getUserProfileListElement();
        Element uP = findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
        if (uP != null)
            return;

        LinkedList [] llarcOnDataUserProfile = new LinkedList[4];
        for (int ii = 0; ii < 4; ii++)
            llarcOnDataUserProfile[ii] = new LinkedList();

        llarcOnDataUserProfile[0].add(0,"WeldSpeed");
        llarcOnDataUserProfile[1].add(0,"StartDwellTime");
        llarcOnDataUserProfile[2].add(0,"Voltage");
        llarcOnDataUserProfile[3].add(0,"Current");

        llarcOnDataUserProfile[0].add(1,"double");
        llarcOnDataUserProfile[1].add(1,"double");
        llarcOnDataUserProfile[2].add(1,"double");
        llarcOnDataUserProfile[3].add(1,"double");

        String weldspeedvalue = "0";
        String startdwelltimevalue = "0";
        String voltagevalue = "0";
        String currentvalue = "0";

        llarcOnDataUserProfile[0].add(2,weldspeedvalue);
        llarcOnDataUserProfile[1].add(2,startdwelltimevalue);
        llarcOnDataUserProfile[2].add(2,voltagevalue);
        llarcOnDataUserProfile[3].add(2,currentvalue);

        createUserProfile(uPList, sUserProfileType, uProfileName, llarcOnDataUserProfile);
    }

    private void processARCOFFStatement(Matcher match, Element activityListElem)
    {
        String endCondNum = match.group(1);
        String sUserProfileType = "NAWArcOFFTable." + endCondNum;
        
        Element lastMovActivityElem = findLastMoveActivityElem(activityListElem);
        if (lastMovActivityElem == null)
            return;
        
        lastMovActivityElem.setNodeValue("ARCOFF");
        Element actName = findChildElemByName(lastMovActivityElem, "ActivityName", null, null);
        Element moAttr = findChildElemByName(lastMovActivityElem, "MotionAttributes", null, null);
        Element moType = findChildElemByName(moAttr, "MotionType", null, null);
        Element uPmot = m_xmlDoc.createElement("UserProfile");
        moAttr.insertBefore(uPmot, moType);
        uPmot.setAttribute("Type", sUserProfileType);
        String uProfileName = sUserProfileType + ".1";
        Text uPVal = m_xmlDoc.createTextNode(uProfileName);
        uPmot.appendChild(uPVal);
        
        Element uPList = getUserProfileListElement();
        Element uP = findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
        if (uP != null)
            return;
        
        LinkedList [] llarcOffDataUserProfile = new LinkedList[4];
        for (int ii = 0; ii < 4; ii++)
            llarcOffDataUserProfile[ii] = new LinkedList();

        llarcOffDataUserProfile[0].add(0,"WeldSpeed");
        llarcOffDataUserProfile[1].add(0,"EndDwellTime");
        llarcOffDataUserProfile[2].add(0,"Voltage");
        llarcOffDataUserProfile[3].add(0,"Current");

        llarcOffDataUserProfile[0].add(1,"double");
        llarcOffDataUserProfile[1].add(1,"double");
        llarcOffDataUserProfile[2].add(1,"double");
        llarcOffDataUserProfile[3].add(1,"double");

        String weldspeedvalue = "0";
        String enddwelltimevalue = "0";
        String voltagevalue = "0";
        String currentvalue = "0";

        llarcOffDataUserProfile[0].add(2,weldspeedvalue);
        llarcOffDataUserProfile[1].add(2,enddwelltimevalue);
        llarcOffDataUserProfile[2].add(2,voltagevalue);
        llarcOffDataUserProfile[3].add(2,currentvalue);

        createUserProfile(uPList, sUserProfileType, uProfileName, llarcOffDataUserProfile);
    }

    private void processSWEAVEStatement(Matcher match, Element activityListElem)
    {
        String sUserProfileType = "NAWWeaveTable";
        String weavestatevalue = "false";
        if (match.group(1).equals("1"))
            weavestatevalue = "true";
        String subprognum = match.group(2);

        Element uPList = getUserProfileListElement();
        Element uP = findChildElemByName(uPList, "UserProfile", "Type", sUserProfileType);
        if (uP == null)
        {
            uP = m_xmlDoc.createElement("UserProfile");
            uPList.appendChild(uP);
            uP.setAttribute("Type", sUserProfileType);
        }

        Element lastMovActivityElem = findLastMoveActivityElem(activityListElem);
        if (lastMovActivityElem == null)
            return;
        
        Element actName = findChildElemByName(lastMovActivityElem, "ActivityName", null, null);
        Element moAttr = findChildElemByName(lastMovActivityElem, "MotionAttributes", null, null);
        Element moType = findChildElemByName(moAttr, "MotionType", null, null);
        Element uPmot = m_xmlDoc.createElement("UserProfile");
        moAttr.insertBefore(uPmot, moType);
        uPmot.setAttribute("Type", sUserProfileType);
        
        String uProfileName = "";
        int uPInstNum = 0;
        String weaveState = "WeaveState", subProgNumber = "SubProgNumber";
        String [] saWeaveData = {weaveState, subProgNumber};
        String [] saWeaveVal = {weavestatevalue, subprognum};
        uPInstNum = uPInstExist(uP, saWeaveVal, saWeaveData);
        if (uPInstNum > 0)
            uProfileName = sUserProfileType + "." + uPInstNum;
        else
            uProfileName = sUserProfileType + "." + weaveCount++;
        
        Text uPVal = m_xmlDoc.createTextNode(uProfileName);
        uPmot.appendChild(uPVal);

        if (uPInstNum > 0)
            return;
        
        LinkedList [] llarcWeaveDataUserProfile = new LinkedList[2];
        for (int ii = 0; ii < 2; ii++)
            llarcWeaveDataUserProfile[ii] = new LinkedList();

        llarcWeaveDataUserProfile[0].add(0, weaveState);
        llarcWeaveDataUserProfile[1].add(0, subProgNumber);

        llarcWeaveDataUserProfile[0].add(1, "boolean");
        llarcWeaveDataUserProfile[1].add(1, "integer");

        llarcWeaveDataUserProfile[0].add(2, weavestatevalue);
        llarcWeaveDataUserProfile[1].add(2, subprognum);

        createUserProfile(uPList, sUserProfileType, uProfileName, llarcWeaveDataUserProfile);
    }

    public Element findLastMoveActivityElem(Element activityListElem)
    {
        Element actElem = (Element)activityListElem.getLastChild();
        while (null != actElem)
        {
            if (actElem.getAttribute("ActivityType").equals("DNBRobotMotionActivity") &&
                null != findChildElemByName(actElem, "MotionAttributes", null, null))
            {
                return actElem;
            }

            actElem = (Element)actElem.getPreviousSibling();
        }
        
        return null;
    }
    
    public Element findChildElemByName(Element currentElem, String name, String attrib, String attribVal)
    {
        Element tmpElem;

        NodeList childNodes = currentElem.getChildNodes();
        for (int ii=0; ii<childNodes.getLength(); ii++)
        {
            tmpElem = (Element)childNodes.item(ii);
            if (tmpElem.getTagName().equals(name))
            {
                if (attrib == null)
                {
                    return (tmpElem);
                }
                else
                {
                    String [] a = attrib.split(";");
                    String [] av = attribVal.split(";");
                    int jj;
                    for (jj=0; jj<a.length; jj++)
                    {
                        if (tmpElem.getAttribute(a[jj]).equals(av[jj]))
                            continue;
                        else
                            break;
                    }
                    if (jj==a.length)
                        return (tmpElem);
                }
            }
        }
        return (null);
    } // end findChildElemByName
    
    private int uPInstExist(Element uP, String [] arcData, String [] arcDataRef)
    {
        Element uPInst;
        NodeList uDefAttrNodes;
        Element uDefAttr, name, val;
        int sameData, numParam;

        NodeList uPInstNodes = uP.getChildNodes();
        numParam = arcData.length;
        for (int ii=0; ii<uPInstNodes.getLength(); ii++)
        {
            uPInst = (Element)uPInstNodes.item(ii);
            uDefAttrNodes = uPInst.getChildNodes();
            sameData = 0;
            for (int jj=0; jj<uDefAttrNodes.getLength(); jj++)
            {
                uDefAttr = (Element)uDefAttrNodes.item(jj);

                name = findChildElemByName(uDefAttr, "DisplayName", null, null);
                val = findChildElemByName(uDefAttr, "Value", null, null);
                // in case the user attributes' sequence differs, loop
                for (int kk=0; kk<numParam; kk++)
                {
                    if (name.getFirstChild().getNodeValue().equals(arcDataRef[kk]) &&
                        val.getFirstChild().getNodeValue().equals(arcData[kk]))
                    {
                        sameData++;
                        break;
                    }
                } 

                if (sameData == numParam)
                    return ii+1; // return instance number
            }
        }

        return 0;
    } // end uPInstExist

} // end class

