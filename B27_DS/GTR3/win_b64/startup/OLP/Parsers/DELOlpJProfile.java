/*
 * DELOlpJProfile.java
 *
 * Created on February 13, 2008, 10:51 PM
 */

/**
 *
 * @author  NIZ
 */

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

public class DELOlpJProfile extends java.lang.Object {
	
private String m_sName = "not set";
private boolean m_bSetName = false; // the field has been set 

private double m_dSpeed = 0.0;
private boolean m_bSetSpeed = false; // the field has been set 
private double m_dAcceleration = 0.0;
private boolean m_bSetAcceleration = false; // the field has been set 

private DNBIgpOlpUploadEnumeratedTypes.MotionBasis m_eMotionBasis = DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE;
private boolean m_bSetMotionBasis = false; // the field has been set 

	/** Creates a new instance of DELOlpJProfile */
	public DELOlpJProfile() {
	}
	
	public DELOlpJProfile(String Name, double Speed, double Acceleration, DNBIgpOlpUploadEnumeratedTypes.MotionBasis Basis )	{
		m_sName = Name;
		m_dSpeed = Speed;
		m_dAcceleration = Acceleration;
		m_eMotionBasis = Basis;
		
		// Set?
	}
	
	public int hashCode(){
		return m_sName.hashCode();
	}
	
	public boolean equals(Object obj) { 
		//return (this == obj); 
		
		return ( obj instanceof DELOlpJProfile 
				&& Name()         == ((DELOlpJProfile) obj).Name()
				&& Speed()        == ((DELOlpJProfile) obj).Speed()
				&& Acceleration() == ((DELOlpJProfile) obj).Acceleration()
				&& MotionBasis()  == ((DELOlpJProfile) obj).MotionBasis()
				);

	}
	
	public String Name(){
		return m_sName;
	}
	
	public void Name(String Name){
		m_sName = Name;
		m_bSetName = true;
	}
	
	public boolean NameSet()
	{
		return m_bSetName;
	}
	
	public double Speed(){
		return m_dSpeed;
	}
		
	public void Speed(double Speed){
		m_dSpeed = Speed;
		m_bSetSpeed = true;
	}
	
	public boolean SpeedSet()
	{
		return m_bSetSpeed;
	}
	
	public double Acceleration(){
		return m_dAcceleration;
	}
	
	public void Acceleration(double Acceleration){
		m_dAcceleration = Acceleration;
		m_bSetAcceleration = true;
	}
	
	public boolean AccelerationSet(){
		return m_bSetAcceleration;
	}
	
	public DNBIgpOlpUploadEnumeratedTypes.MotionBasis MotionBasis(){
		return m_eMotionBasis;
	}
	
	public  void MotionBasis(DNBIgpOlpUploadEnumeratedTypes.MotionBasis MotionBasis){
		m_eMotionBasis = MotionBasis;
		m_bSetMotionBasis = true;
	}
	
	public boolean MotionBasisSet(){
		return m_bSetMotionBasis;
	}

}
