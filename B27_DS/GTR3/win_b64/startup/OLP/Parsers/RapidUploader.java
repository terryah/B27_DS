// DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2003-2006
import org.w3c.dom.*;
import javax.xml.parsers.*;

//SAX classes used for error handling by JAXP
import org.xml.sax.*;

//Regular expression classesm_list
import java.util.regex.*;

//IO classes
import java.io.*;

//Java Exception classes
import java.io.IOException;
import java.io.FileNotFoundException;
import javax.xml.transform.TransformerException;

//XML Transform classes
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Enumeration;

//Java text classes
import java.text.NumberFormat;

import com.dassault_systemes.DNBIgpOlpJavaBase.DNBIgpOlpUploaderTools.*;

//Classes that create DOM stream of data
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Text;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RapidUploader extends DNBIgpOlpUploadBaseClass {

        //Constants
        private static final int JOINT = 1;
        private static final int CARTESIAN = 2;
        private static final int ecode_TargetUndefined = 1;
        private static final int ecode_SpeedDataUndefined = 2;
        private static final double ZERO_TOL = .001;
        private static final String VERSION = "\nDelmia Corp. RAPID Uploader Version 5 Release 26 SP1.\n";
        private static final String COPYRIGHT = "Copyright Delmia Corp. 2003-2012, All Rights Reserved.\n";
        
        //Member primitive variables
        private Element m_toolProfileListElement;
        private Element m_motionProfileListElement;
        private Element m_accuracyProfileListElement;
        private Element m_objectProfileListElement;
        private boolean m_firstInfoLine = true;
        private ArrayList m_toolProfilesProcessed;
        private ArrayList m_objectProfilesProcessed;        
        private int m_worldCoords = 0;
        private double m_targetYaw;
        private double m_targetPitch;
        private double m_targetRoll;
        private int m_targetType;
        private int m_targetNumber;
        private int m_targetTurnValue;
        private int m_newvalue;
        private String m_intAxes ="False";
        private int m_numCStatements;
        private int m_numRobotAxes;
        private int m_numAuxAxes;
        private int m_numExtAxes;
        private int m_numWorkAxes;
        private int m_auxAxesCount;
        private int m_extAxesCount;
        private int m_workAxesCount;
        private int m_auxAxesStart;
        private int m_extAxesStart;
        private int m_workAxesStart;    
        private int m_procCount;
        private int m_extAxesGroupNumber;
        private int m_auxAxesGroupNumber;
        private int m_railAxesIndex;
        private int m_workAxesGroupNumber;        
        private int m_delayCounter;
        private int m_callCounter;
        private int m_calledProgramCounter;
        private int m_spotCounter;
        private int m_backupCounter;
        private int m_macroCounter;
        private int m_releaseCounter ;
        private int m_grabCounter;
        private int m_moveCounter;
        private int m_gunmoveCounter;
        private int m_mountCounter;
        private int m_unmountCounter;
        private int m_enterZoneCounter;
        private int m_clearZoneCounter;
        private int m_accProfNum;
        private int m_motionProfNum;
        private int m_toolProfNum;
        private int m_objectProfNum;
        private int m_explicitCount;
        private int m_SignalCount = 1;
        private int m_commentWaitSignal = 1;
        private String m_OLPStyle = "";
        private int m_operationCommentCount = 1;
        private int m_robotLanguageCommentCount = 1;
        private ArrayList []  m_EndOfArmToolValues;

        
        private String m_mocConfigFileName;
 
        private boolean m_createJointTarget;
        private boolean m_moduleFound;
        private int lastTargetNumber = 0;
        private String ProgName="RAPIDUPLOAD";
        private String m_moduleName="";
        private String m_moduleCall="";

        //Member objects
        private Document m_xmlDoc;
        private String m_currentTagPoint;
        private String m_currentToolNumber;
        private String m_currentObjectNumber;
        private Element m_currentElement;
        private String m_pickHome;
        private String m_dropHome;
        private String m_grabPart;
        private String m_partGrabbed;
        private String m_mountedToolName;
        private String m_dropToolProfileName;        
        private int    m_commentCount;

        private Pattern [] m_keywords;
        private Pattern [] m_moveKeywords;
        private Pattern [] moc_keywords;
        private String prgFileEncoding = "UTF-8";

        private String [] m_axisTypes;
        private String [] m_TbaseFrame;
  
        private ArrayList m_listOfRobotTargets;
        private ArrayList m_listOfAuxTargets;
        private ArrayList m_listOfExtTargets;
        private ArrayList m_listOfWorkTargets;
        private ArrayList m_listOfSpeedValues;
        private ArrayList m_listOfSpeedData;
        private ArrayList m_listOfAccuracyValues;
        private ArrayList m_listOfCalledPrograms;
        private Hashtable m_auxAxesTypes;

        private Hashtable m_targetTypes;
        private Hashtable m_targetNumbers;
        private Hashtable m_targetDeclType;
        private Hashtable m_weldScheduleHomes;
        private Hashtable m_portNumberNames;
        private Hashtable m_weldScheduleDelays;
        private Hashtable m_macroCommands;
        private String m_originalMacro;
        private Hashtable m_backupHomes;
        private Hashtable m_tagNames;
        private Hashtable m_procNameList;
        private Hashtable m_toolNumberMapping;
        private Hashtable m_objectNumberMapping;
        // private Hashtable m_objectTrackOffsetMapping;        
        private Hashtable m_parameterData;
        private Hashtable m_commentLines;
        private String [] m_commentNames;
        private String [] m_commentValues;
        private ArrayList m_listofHomeNames;
        private ArrayList m_listOfUndefTargetNames;
        private String [] m_mountedGunName;
        // DLY Start 2007/05/07 added version and home names
        private double m_versionNumber;
        private String m_closeHLiftHome;
        private String m_openHLiftHome;
        // DLY End 2007/05/07
        
        private ArrayList m_listOfSpotData;
        private ArrayList m_listOfC5SpotData;
        private ArrayList m_listOfGunData;
        private ArrayList m_listOfWeldData;
        private ArrayList m_listOfWeaveData;
        private ArrayList m_listOfSeamData; 
        
        private Element m_wobjActivityListElement;
        
        private double m_conveyorInitialPosition = 0.0;
        private double m_inBoundOffset = 0.0;
        private double m_lastInBoundOffset = 0.0;
        private double m_wobjOffset = 0.0;
        private boolean m_waitWobj = false;
        private boolean m_objectFrameMove = false;
        private Hashtable m_numVariables;
        private Hashtable m_objectFrameMovable;
        private Hashtable m_callTaskInBoundOffset;

        public RapidUploader ( String [] parameters, String [] profileNumbers, String[] toolProfileNames, String[] homeNames, String[] objectProfileNames) throws ParserConfigurationException {

             super(parameters);

             m_toolProfileListElement = null;
             m_accuracyProfileListElement = null;
             m_motionProfileListElement = null;
             m_objectProfileListElement = null;
             m_targetType = CARTESIAN;
             m_targetTurnValue = 0;
             m_targetYaw = 0.0;
             m_targetPitch = 0.0;
             m_targetRoll = 0.0;
             m_targetNumber = 0;
             m_numCStatements = 0;
             m_delayCounter = 1;
             m_callCounter = 1;
             m_calledProgramCounter = 0;
             m_moveCounter = 1;
             m_spotCounter = 1;
             m_backupCounter = 1;
             m_procCount = 0;

             m_macroCounter = 1;
             m_grabCounter = 1;
             m_releaseCounter = 1;
             m_mountCounter = 1;
             m_unmountCounter = 1;
             m_enterZoneCounter = 1;
             m_clearZoneCounter = 1;
             m_numRobotAxes = super.getNumberOfRobotAxes();
             m_numAuxAxes = super.getNumberOfRailAuxiliaryAxes();
             m_numExtAxes = super.getNumberOfToolAuxiliaryAxes();
             m_numWorkAxes = super.getNumberOfWorkpiecePositionerAuxiliaryAxes();
             m_auxAxesCount = 0;
             m_extAxesCount = 0;
             m_workAxesCount = 0;
             m_auxAxesStart = -1;
             m_extAxesStart = -1;
             m_workAxesStart = -1;  
             m_railAxesIndex = 1; 
             m_commentCount = 0;
             m_moduleFound = false;
             m_auxAxesGroupNumber = 1;
             m_extAxesGroupNumber = 2;
             m_workAxesGroupNumber = 0;
             m_createJointTarget = false;
             m_mocConfigFileName = "";
             m_explicitCount = 1;
             m_commentWaitSignal = 1;

             m_motionProfNum = Integer.parseInt(profileNumbers[0]);
             m_accProfNum = Integer.parseInt(profileNumbers[1]);
             m_toolProfNum = Integer.parseInt(profileNumbers[2]);

             //Num of aux and ext axes can be 0, therefore to avoid exception an array is initialized to #axes + 1

             m_pickHome = new String("Home_1");
             m_dropHome = new String("Home_2");
             m_grabPart = new String("");
             m_partGrabbed = new String("");
             m_mountedToolName = new String("");
             m_dropToolProfileName = new String("Default");

             m_currentTagPoint = new String("");
             m_currentToolNumber = new String("1");
             m_currentObjectNumber = new String("0");

             m_currentElement = null;
             m_keywords = new Pattern [27];
             m_moveKeywords = new Pattern [46];
             moc_keywords = new Pattern [10];
             m_axisTypes = new String [m_numRobotAxes+m_numExtAxes+m_numAuxAxes+m_numWorkAxes];
             m_axisTypes = super.getAxisTypes();
             m_TbaseFrame = new String[7];
  
             m_listOfRobotTargets = new ArrayList(5);
             m_listOfAuxTargets = new ArrayList(2);
             m_listOfCalledPrograms = new ArrayList(1);
             m_listOfExtTargets = new ArrayList(2);
             m_listOfWorkTargets = new ArrayList(2);
             m_listOfSpeedValues = new ArrayList(5);
             m_listOfAccuracyValues = new ArrayList(3);
             m_auxAxesTypes = new Hashtable();
             m_toolProfilesProcessed = new ArrayList(1);
             m_objectProfilesProcessed = new ArrayList(1);
             
             int auxii;
             for (auxii=1;auxii<=m_numAuxAxes;auxii++)
             {
             	m_auxAxesTypes.put(String.valueOf(auxii), "RailTrackGantry");
             }
             for (auxii=auxii;auxii<=(m_numAuxAxes + m_numExtAxes);auxii++)
             {
             	m_auxAxesTypes.put(String.valueOf(auxii), "EndOfArmTooling");
             }
             for (auxii=auxii;auxii<=(m_numAuxAxes + m_numExtAxes + m_numWorkAxes);auxii++)
             {
             	m_auxAxesTypes.put(String.valueOf(auxii), "WorkPositioner");
             }
             m_targetTypes = new Hashtable();
             m_targetNumbers = new Hashtable();
             m_targetDeclType = new Hashtable();
             m_tagNames = new Hashtable();
             m_toolNumberMapping = new Hashtable();
             m_objectNumberMapping = new Hashtable();
            //  m_objectTrackOffsetMapping = new Hashtable();
             m_weldScheduleHomes = new Hashtable();
             m_portNumberNames = new Hashtable();
             m_portNumberNames.put("DUMMY_ZERO_VAL", "0");
             m_weldScheduleDelays = new Hashtable();
             m_macroCommands = new Hashtable();
             m_originalMacro = "";
             m_procNameList = new Hashtable();
             m_backupHomes = new Hashtable();
             m_parameterData =  super.getParameterData();
             m_listofHomeNames = new ArrayList(2);
             m_commentLines = new Hashtable();
             m_commentNames = new String [1000];
             m_commentValues = new String [1000];
             m_listOfUndefTargetNames = new ArrayList(2);
             clearComments();
             m_xmlDoc = super.getDOMDocument();     
             m_toolProfileListElement = super.getToolProfileListElement();
             m_motionProfileListElement = super.getMotionProfileListElement();
             m_accuracyProfileListElement = super.getAccuracyProfileListElement();
             m_objectProfileListElement = super.getObjectFrameProfileListElement();            
             m_listOfSpeedData = new ArrayList(2);
             m_wobjActivityListElement = null;
             // DLY Start 2007/05/07 added m_versionNumber and close/open homes
             m_versionNumber = 0.0;
             m_closeHLiftHome = "CloseHLift";
             m_openHLiftHome = "OpenHLift";
             // DLY End 2007/05/07
             
             m_listOfSpotData = new ArrayList(2);
             m_listOfC5SpotData = new ArrayList(2);
             m_listOfGunData = new ArrayList(2);
             m_listOfSeamData = new ArrayList(2);
             m_listOfWeldData = new ArrayList(2);
             m_listOfWeaveData = new ArrayList(2);
              
             m_mountedGunName = parameters[6].split("\\t");
             String [] speeddata = {"v5", "v10", "v20", "v30", "v40", "v50", "v60", "v80", "v100", "v150", "v200", "v300", "v400", "v500",
                                    "v600", "v800", "v1000", "v1500", "v2000", "v2500", "v3000", "v4000", "vmax", "v6000", "v7000"};
             for (int ii=0; ii<speeddata.length; ii++)
                 m_listOfSpeedData.add(speeddata[ii]);

             for(int jj = 0; jj < toolProfileNames.length; jj++) {
                         m_toolNumberMapping.put(String.valueOf(jj), toolProfileNames[jj]);  
             }
 
             for(int jj = 0; jj < objectProfileNames.length; jj++) {
                         m_objectNumberMapping.put(String.valueOf(jj), objectProfileNames[jj]);
             }            

             for (int jj=0; jj < homeNames.length; jj++ ) {
                m_listofHomeNames.add(jj, homeNames[jj]);
             }

             for (int ii=0;ii<7;ii++)
                 m_TbaseFrame[ii] = "0.0";                                 
  
                                    
             m_TbaseFrame[3] = "1.0";
                                    
             m_numVariables = new Hashtable();
             m_objectFrameMovable = new Hashtable();
             m_callTaskInBoundOffset = new Hashtable();
             
             Enumeration parameterKeys = m_parameterData.keys();
             Enumeration parameterValues = m_parameterData.elements();

             while(parameterKeys.hasMoreElements()) {

                 String parameterName = parameterKeys.nextElement().toString();
                 String parameterValue = parameterValues.nextElement().toString();

               //  System.out.println("PARAMETER NAME: " + parameterName + " PARAMETER VALUE: " + parameterValue + "\n");
                 int index = parameterName.indexOf('.');
                 String testString = parameterName.substring(0, index + 1);

                 try {
                     if(testString.equalsIgnoreCase("ToolProfile.")) {
                         String profileName = parameterName.substring(index + 1);
                         m_toolNumberMapping.put(profileName, parameterValue);
                     }
                     else if(testString.equalsIgnoreCase("ObjectProfile.")) {
                         String profileName = parameterName.substring(index + 1);
                         m_objectNumberMapping.put(profileName, parameterValue);
                     }                     
                      else if(testString.equalsIgnoreCase("WeldHome.")) {
                        
                          int homeIndex = 0;
                          try {
                              homeIndex = Integer.parseInt(parameterName.substring(index + 1));
                          }
                          catch (NumberFormatException e) {
                             e.getMessage();
                          }
                         
                         if (homeIndex == 0)
                         {
                             homeNames[0] = parameterName.substring(index + 1);
                         }
                      
                         if (homeIndex <= 0)
                         {
                             homeIndex = 1;
                         }
                       
                         if (homeIndex > homeNames.length)
                         {
                             homeIndex = homeNames.length-1;
                         }
                      
                         String [] homeComponents = parameterValue.split(";");
                         
                        
                         
                         if (homeComponents.length > 0)
                         {
                             for (int kk=0;kk<homeComponents.length;kk++)
                             {    
                                // System.out.println("HOME VARAIBLE: " + homeComponents[kk] + " HOME NAME: " + homeNames[homeIndex-1] + "\n");                                 
                                  m_weldScheduleHomes.put(homeComponents[kk], homeNames[homeIndex-1]);
                             }
                         }
                         else
                         {
                           //  System.out.println("HOME VARAIBLE: " + parameterValue + " HOME NAME: " + homeNames[homeIndex-1] + "\n");
                             m_weldScheduleHomes.put( parameterValue, homeNames[homeIndex-1]);
                         }
                     }   
                     else if(testString.equalsIgnoreCase("OpenHome.")) {
                          int homeIndex = 0;
                          try {
                              homeIndex = Integer.parseInt(parameterName.substring(index + 1));
                          }
                          catch (NumberFormatException e) {
                             e.getMessage();
                          }
                         
                   

                        
                         if (homeIndex == 0)
                         {
                             homeNames[0] = parameterName.substring(index + 1);
                         }
                      
                         if (homeIndex <= 0)
                         {
                             homeIndex = 1;
                         }
                         
                         if (homeIndex > homeNames.length)
                         {
                             homeIndex = homeNames.length;
                         }
                      
                         String [] homeComponents = parameterValue.split(";");                         
                        
                         if (homeComponents.length > 0)
                         {
                             for (int kk=0;kk<homeComponents.length;kk++)
                             {    
                                 m_backupHomes.put(homeComponents[kk], homeNames[homeIndex-1]); 
                                //  System.out.println("OPEN HOME VARAIBLE: " + homeComponents[kk] + " HOME NAME: " + homeNames[homeIndex-1] + "\n");
                             }                         
                         }
                         else
                         {
                                m_backupHomes.put(parameterValue, homeNames[homeIndex-1]); 
                              //  System.out.println("OPEN HOME VARAIBLE: " + parameterValue + " HOME NAME: " + homeNames[homeIndex-1] + "\n");                                
                         }
                     }
                      else if(testString.equalsIgnoreCase("WeldDelay.")) {                        
                         double delayTime = Double.valueOf(parameterName.substring(index + 1)).doubleValue() * 0.001;                    
                         
                         String [] delayComponents = parameterValue.split(";");
                         
                         if (delayComponents.length > 0)
                         {
                            for (int kk=0;kk<delayComponents.length;kk++)
                                m_weldScheduleDelays.put(delayComponents[kk], String.valueOf(delayTime));
                         }
                         else
                         {
                             m_weldScheduleDelays.put(parameterValue, String.valueOf(delayTime));
                         }
                      }   
                      else if (testString.equalsIgnoreCase("Macro.")) {
                          String macroAction = parameterName.substring(index+1);
                          String [] macroComponents = parameterValue.split(";");
                          if (macroComponents.length > 0)
                          {
                              for (int kk=0;kk<macroComponents.length;kk++)
                                  m_macroCommands.put(macroComponents[kk], macroAction);
                          }
                          else
                          {
                              m_macroCommands.put(parameterValue, macroAction);
                          }
                      }
                      else if (parameterName.equalsIgnoreCase("PickHome")) {
                          m_pickHome = parameterValue;
                      }
                       else if (parameterName.equalsIgnoreCase("DropHome")) {
                          m_dropHome = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("GrabPart")) {                          
                          m_grabPart = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("PartGrabbed")) {
                          m_partGrabbed = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("MountedToolName")) {
                          m_mountedToolName = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("WeldGunName")) {
                          m_mountedGunName[0] = parameterValue;
                      }
                      else if (parameterName.equalsIgnoreCase("WorldCoords")) {
                          if (parameterValue.equalsIgnoreCase("true") || parameterValue.equals("1"))
                          {
                              m_worldCoords = 1;
                          }
                      }
                      else if (parameterName.equalsIgnoreCase("Railgroup")) {
                          String [] auxAxesNumbers = parameterValue.split(";");
                          removeHashTableTypes("RailTrackGantry", m_auxAxesTypes); 
                          if (auxAxesNumbers.length > 0)
                          {
                              for (int kk=0;kk<auxAxesNumbers.length;kk++) {
                                  if (kk == 0)
                                  {
                                      m_railAxesIndex = Integer.parseInt(auxAxesNumbers[kk]);
                                  }
                                  m_auxAxesTypes.put(auxAxesNumbers[kk], "RailTrackGantry");
                              }
                          }
                          else
                          {
                              m_railAxesIndex = Integer.parseInt(parameterValue);
                              m_auxAxesTypes.put(parameterValue, "RailTrackGantry");
                          }
                      }
                      else if (parameterName.equalsIgnoreCase("Toolgroup")) {
                          String [] auxAxesNumbers = parameterValue.split(";");
                          removeHashTableTypes("EndOfArmTooling", m_auxAxesTypes);
                          if (auxAxesNumbers.length > 0)
                          {
                              for (int kk=0;kk<auxAxesNumbers.length;kk++)
                                  m_auxAxesTypes.put(auxAxesNumbers[kk], "EndOfArmTooling");                             
                          }
                          else
                          {
                              m_auxAxesTypes.put(parameterValue, "EndOfArmTooling");  
                          }
                    }
                    else if (parameterName.equalsIgnoreCase("Workgroup")) {
                        String [] auxAxesNumbers = parameterValue.split(";");
                        removeHashTableTypes("WorkPositioner", m_auxAxesTypes);
                        if (auxAxesNumbers.length > 0)
                        {                        
                            for (int kk=0;kk<auxAxesNumbers.length;kk++)
                                m_auxAxesTypes.put(auxAxesNumbers[kk], "WorkPositioner");
                        }
                        else
                        {
                            m_auxAxesTypes.put(parameterValue, "WorkPositioner");  
                        }                            
                    }
                    else if (parameterName.equalsIgnoreCase("ProgramFileEncoding")) {
                        prgFileEncoding = parameterValue;
                    }
                    else if (parameterName.equalsIgnoreCase("MOC_ConfigFileName")) {
                        m_mocConfigFileName = parameterValue;
                    }
                    else if (parameterName.equalsIgnoreCase("DropToolProfileName")) {
                        m_dropToolProfileName = parameterValue;
                    } 
                    else if (parameterName.equalsIgnoreCase("OLPStyle")) {
                        m_OLPStyle = parameterValue;
                    }                    
                    else if (parameterName.equalsIgnoreCase("CommentWaitSignal")) {
                          if (parameterValue.equalsIgnoreCase("false") == true)
                          {
                              m_commentWaitSignal = 0;
                          }
                   }              
                    else if (parameterName.equals("Version")) {
                        m_versionNumber = Double.parseDouble(parameterValue);
                    }
                    else if (parameterName.equalsIgnoreCase("WobjOffset")){
                        m_wobjOffset = Double.parseDouble(parameterValue)/1000.0;
                    }
                    else if (parameterName.equalsIgnoreCase("ConveyorOffset")){
                        m_conveyorInitialPosition = Double.parseDouble(parameterValue)/1000.0;
                    }                     
                }

                finally {
                    continue;
                } 
            }

            //                      ISO-8859-1 Upper   ->|ISO-8859-1 Lower   ->|
            String letter = "[A-Za-z\\xC0-\\xD6\\xD8-\\xDF\\xE0-\\xF6\\xF8-\\xFF]";
            //String letter = "[A-Za-z\u00C0-\u00D6\u00D8-\u00DF\u00E0-\u00F6\u00F8-\u00FF]";
            String id  = "\\s*(" + letter + "(?:" + letter + "|" + "[0-9_]" + ")*)";
            String num = "([0-9Ee\\-\\.\\+]*)";
            String n3 = "\\s*\\[\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*\\]\\s*";
            String n4 = "\\s*\\[\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*\\]\\s*";
            String n6 = "\\s*\\[\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*,\\s*" + num + "\\s*\\]\\s*";
            String cfg = "\\s*\\[(-?[0-9]*)\\s*,\\s*(-?[0-9]*)\\s*,\\s*(-?[0-9]*)\\s*,\\s*(-?[0-9]*)\\s*\\]\\s*";
            String rbtgt = "\\s*\\[\\s*" + n3 + "\\s*,\\s*" + n4 + "\\s*,\\s*" + cfg + "\\s*,\\s*" + n6 + "\\s*\\]\\s*";
            String declType = "[TASK|LOCAL|PERS|CONST|VAR|\\s]*";
            String event = "(\\Q\\e\\E\\d+:=\\[[0-9Ee\\-\\.\\+]*,[0-9Ee\\-\\.\\+]*,[0-9Ee\\-\\.\\+]*\\])?";
            String events = new String();
            for (int ii=0; ii<10; ii++)
                events = events + event;
              
            moc_keywords[0] = Pattern.compile("^\\s*ROBOT:", Pattern.CASE_INSENSITIVE);
            moc_keywords[1] = Pattern.compile("^\\s*SINGLE:", Pattern.CASE_INSENSITIVE);
            moc_keywords[2] = Pattern.compile("^\\s*SINGLE_TYPE:", Pattern.CASE_INSENSITIVE);
            moc_keywords[3] = Pattern.compile("\\s*-base_frame_pos_x\\s*(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);
            moc_keywords[4] = Pattern.compile("\\s*-base_frame_pos_y\\s*(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);             
            moc_keywords[5] = Pattern.compile("\\s*-base_frame_pos_z\\s*(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);           
            moc_keywords[6] = Pattern.compile("\\s*-base_frame_orient_u0\\s*(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);
            moc_keywords[7] = Pattern.compile("\\s*-base_frame_orient_u1\\s*(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);             
            moc_keywords[8] = Pattern.compile("\\s*-base_frame_orient_u2\\s*(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);
            moc_keywords[9] = Pattern.compile("\\s*-base_frame_orient_u3\\s*(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE);            
 
            // DLY Start 2007/05/04 - added ID:= for C5 controller and added ArcLStart, ArcCStart, ArcLEnd, ArcCEnd, SpotJ, SpotML, SpotMJ, 
            //                        moved StudL and PaintL next to other move statements and added explicit version of StudL and PaintL
            // motion statements
            // non-explicit moves here
            m_moveKeywords[0] = Pattern.compile("^\\s*(MoveJ)\\s*(\\Q\\Conc\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[1] = Pattern.compile("^\\s*(MoveL)\\s*(\\Q\\Conc\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[2] = Pattern.compile("^\\s*(MoveC)\\s*(\\Q\\Conc\\E)?,?" + id + "," + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?," + id + "(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[3] = Pattern.compile("^\\s*(SpotL)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\InPos\\E)?(\\Q\\NoConc\\E)?(\\Q\\Retract\\E)?\\s*," + id + "\\s*(\\Q\\Inpos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[4] = Pattern.compile("^\\s*(SpotJ)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\InPos\\E)?(\\Q\\NoConc\\E)?(\\Q\\Retract\\E)?\\s*," + id + "\\s*(\\Q\\Inpos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[5] = Pattern.compile("^\\s*(SpotML)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*,\\s*(\\Q\\G1:=\\E)?" + id + "?" + "\\s*(\\Q\\G2:=\\E)?" + id + "?" + "\\s*(\\Q\\G3:=\\E)?" + id + "?" + "\\s*(\\Q\\G4:=\\E)?" + id + "?" + "\\s*(\\Q\\InPos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[6] = Pattern.compile("^\\s*(SpotMJ)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*,\\s*(\\Q\\G1:=\\E)?" + id + "?" + "\\s*(\\Q\\G2:=\\E)?" + id + "?" + "\\s*(\\Q\\G3:=\\E)?" + id + "?" + "\\s*(\\Q\\G4:=\\E)?" + id + "?" + "\\s*(\\Q\\InPos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[7] = Pattern.compile("^\\s*(ArcL)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[8] = Pattern.compile("^\\s*(ArcLStart)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[9] = Pattern.compile("^\\s*(ArcLEnd)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[10] = Pattern.compile("^\\s*(ArcC)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*," + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[11] = Pattern.compile("^\\s*(ArcCStart)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[12] = Pattern.compile("^\\s*(ArcCEnd)\\s*" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[13] = Pattern.compile("^\\s*(ArcL1)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[14] = Pattern.compile("^\\s*(ArcL2)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[15] = Pattern.compile("^\\s*(ArcL)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[16] = Pattern.compile("^\\s*(ArcC)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*," + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[17] = Pattern.compile("^\\s*(StudL)\\s+" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*,\\s*(\\d+)(\\Q\\Gun\\E[1-3])?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?\\s*;", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[18] = Pattern.compile("^\\s*(PaintL)\\s+" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "" + events + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\WObj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[19] = Pattern.compile("^\\s*(MoveAbsJ)\\s*(\\Q\\Conc\\E)?\\s*,?" + id + "\\s*(\\Q\\NoEOffs\\E)?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[20] = Pattern.compile("^\\s*(DispJ)\\s*(\\Q\\Conc\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[21] = Pattern.compile("^\\s*(DispL)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\D:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[22] = Pattern.compile("^\\s*(DispC)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + id + "\\s*," + id + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\D:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);

            // explicit moves here
            m_moveKeywords[23] = Pattern.compile("^\\s*(MoveJ)\\s*(\\Q\\Conc\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[24] = Pattern.compile("^\\s*(MoveL)\\s*(\\Q\\Conc\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[25] = Pattern.compile("^\\s*(MoveC)\\s*(\\Q\\Conc\\E)?,?" + rbtgt + "," + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?," + id + "(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[26] = Pattern.compile("^\\s*(SpotL)\\s*" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\InPos\\E)?(\\Q\\NoConc\\E)?(\\Q\\Retract\\E)?\\s*," + id + "\\s*(\\Q\\Inpos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[27] = Pattern.compile("^\\s*(SpotJ)\\s*" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\InPos\\E)?(\\Q\\NoConc\\E)?(\\Q\\Retract\\E)?\\s*," + id + "\\s*(\\Q\\Inpos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[28] = Pattern.compile("^\\s*(SpotML)\\s*" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*,\\s*(\\Q\\G1:=\\E)?" + id + "?" + "\\s*(\\Q\\G2:=\\E)?" + id + "?" + "\\s*(\\Q\\G3:=\\E)?" + id + "?" + "\\s*(\\Q\\G4:=\\E)?" + id + "?" + "\\s*(\\Q\\InPos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[29] = Pattern.compile("^\\s*(SpotMJ)\\s*" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*,\\s*(\\Q\\G1:=\\E)?" + id + "?" + "\\s*(\\Q\\G2:=\\E)?" + id + "?" + "\\s*(\\Q\\G3:=\\E)?" + id + "?" + "\\s*(\\Q\\G4:=\\E)?" + id + "?" + "\\s*(\\Q\\InPos\\E)?(\\Q\\Conc\\E)?(\\Q\\OpenHLift\\E)?(\\Q\\CloseHLift\\E)?(\\Q\\QuickRelease\\E)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[30] = Pattern.compile("^\\s*(ArcL)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[31] = Pattern.compile("^\\s*(ArcLStart)\\s*" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[32] = Pattern.compile("^\\s*(ArcLEnd)\\s*" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[33] = Pattern.compile("^\\s*(ArcC)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*," + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[34] = Pattern.compile("^\\s*(ArcCStart)\\s*" + rbtgt + "," + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[35] = Pattern.compile("^\\s*(ArcCEnd)\\s*" + rbtgt + "," + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[36] = Pattern.compile("^\\s*(ArcL1)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[37] = Pattern.compile("^\\s*(ArcL2)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[38] = Pattern.compile("^\\s*(ArcL)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[39] = Pattern.compile("^\\s*(ArcC)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*," + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[40] = Pattern.compile("^\\s*(StudL)\\s+" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*,\\s*(\\d+)(\\Q\\Gun\\E[1-3])?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?\\s*;", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[41] = Pattern.compile("^\\s*(PaintL)\\s+" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "" + events + "\\s*," + id + "\\s*," + id + "\\s*(\\Q\\WObj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[42] = Pattern.compile("^\\s*(MoveAbsJ)\\s*(\\Q\\Conc\\E)?\\s*,?\\[" + n6+ "," + n6 + "\\s*\\]\\s*(\\Q\\NoEOffs\\E)?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[43] = Pattern.compile("^\\s*(DispJ)\\s*(\\Q\\Conc\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\V:=\\E|\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[44] = Pattern.compile("^\\s*(DispL)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?\\s*" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\D:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            m_moveKeywords[45] = Pattern.compile("^\\s*(DispC)\\s*(\\Q\\On\\E|\\Q\\Off\\E)?\\s*,?" + rbtgt + "\\s*," + rbtgt + "\\s*,?\\s*(\\Q\\ID:=\\E)?" + id + "?\\s*," + id + "\\s*(\\Q\\T:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\D:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Z:=\\E)?(-?[0-9]*\\.?[0-9]*)?\\s*," + id + "\\s*(\\Q\\Wobj:=\\E)?" + id + "?", Pattern.CASE_INSENSITIVE);
            // End moves
            
            // non motion statements
            m_keywords[0] = Pattern.compile("^\\s*(" + declType + ")\\s*robtarget\\s*" + id + "\\s*:=\\s*" + rbtgt, Pattern.CASE_INSENSITIVE);
            m_keywords[1] = Pattern.compile("^\\s*(" + declType + ")\\s*jointtarget\\s*" + id + "\\s*:=\\s*\\[" + n6 + "," + n6 + "\\]", Pattern.CASE_INSENSITIVE);                         
            m_keywords[2] = Pattern.compile("^\\s*SetDO" + id + "\\s*,\\s*(on|off|1|0)", Pattern.CASE_INSENSITIVE);
            m_keywords[3] = Pattern.compile("^\\s*Set" + id + "\\s*;", Pattern.CASE_INSENSITIVE);
            m_keywords[4] = Pattern.compile("^\\s*PulseDO\\s*(\\Q\\PLength:=\\E)?\\s*(-?[0-9]*\\.?[0-9]*)\\s*,?" + id, Pattern.CASE_INSENSITIVE);
            m_keywords[5] = Pattern.compile("^\\s*MODULE" + id, Pattern.CASE_INSENSITIVE);
            m_keywords[6] = Pattern.compile("^\\s*(LOCAL)?\\s*PROC" + id + "\\s*\\(\\s*\\)", Pattern.CASE_INSENSITIVE);
            m_keywords[7] = Pattern.compile("^\\s*ENDPROC", Pattern.CASE_INSENSITIVE);
            m_keywords[8] = Pattern.compile("^\\s*ENDMODULE", Pattern.CASE_INSENSITIVE); 
            m_keywords[9] = Pattern.compile("^\\s*!", Pattern.CASE_INSENSITIVE); 
            m_keywords[10] = Pattern.compile("^\\s*" + declType + "\\s*tooldata\\s*" + id + "\\s*:=\\s*\\[(TRUE|FALSE),\\s*\\[\\[([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*)\\],\\[([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*)\\]\\],\\[([0-9Ee\\-\\.\\+]*),\\[([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*)\\],\\[([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*)\\],([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*)\\]\\]", Pattern.CASE_INSENSITIVE);
            m_keywords[11] = Pattern.compile("^\\s*" + declType + "\\s*wobjdata\\s*" + id + "\\s*:=\\s*\\[(TRUE|FALSE),(TRUE|FALSE),\"" + id + "?\",\\[" + n3 + "," + n4 + "\\],\\[" + n3 + "," + n4 + "\\]\\]", Pattern.CASE_INSENSITIVE);            
            m_keywords[12] = Pattern.compile("^\\s*" + declType + "\\s*zonedata\\s*" + id + "\\s*:=\\s*\\[(TRUE|FALSE|true|false),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*),([0-9Ee\\-\\.\\+]*)\\]", Pattern.CASE_INSENSITIVE); 
            m_keywords[13] = Pattern.compile("^\\s*" + declType + "\\s*speeddata\\s*" + id + "\\s*:=\\s*\\[(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*)\\]", Pattern.CASE_INSENSITIVE); 
            m_keywords[14] = Pattern.compile("^\\s*" + declType + "\\s*spotdata\\s*" + id + "\\s*:=\\s*\\[([0-9]*)," + num + "," + num + "," + num + ",?" + "([0-9]*)?\\]", Pattern.CASE_INSENSITIVE); 
            m_keywords[15] = Pattern.compile("^\\s*" + declType + "\\s*gundata\\s*" + id + "\\s*:=\\s*\\[([0-9]*),([0-9]*),(TRUE|FALSE|true|false),(TRUE|FALSE|true|false),([0-9]*),([0-9]*),([0-9]*),([0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*)\\]", Pattern.CASE_INSENSITIVE); 
            m_keywords[16] = Pattern.compile("^\\s*" + declType + "\\s*seamdata\\s*" + id + "\\s*:=\\s*\\[(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),([0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),([0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),([0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),([0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*)\\]", Pattern.CASE_INSENSITIVE); 
            m_keywords[17] = Pattern.compile("^\\s*" + declType + "\\s*welddata\\s*" + id + "\\s*:=\\s*\\[([0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*)\\]", Pattern.CASE_INSENSITIVE); 
            m_keywords[18] = Pattern.compile("^\\s*" + declType + "\\s*weavedata\\s*" + id + "\\s*:=\\s*\\[([0-9]*),([0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),(-?[0-9]*\\.?[0-9]*),([0-9]*)\\]", Pattern.CASE_INSENSITIVE); 
            m_keywords[19] = Pattern.compile("^\\s*WaitDI" + id + "\\s*,\\s*(on|off|1|0)\\s*(\\Q\\MaxTime:=\\E)?(-?[0-9]*\\.?[0-9]*)?", Pattern.CASE_INSENSITIVE); 
            m_keywords[20] = Pattern.compile("^\\s*WaitTime\\s*(\\Q\\InPos\\E)?\\s*,\\s*?(-?[0-9]*\\.?[0-9]*)", Pattern.CASE_INSENSITIVE); 
            m_keywords[21] = Pattern.compile("^\\s*(LOCAL)?\\s*PROC" + id + "\\s*\\(", Pattern.CASE_INSENSITIVE);
            m_keywords[22] = Pattern.compile("^\\s*(GunOpen|GunSemiOpen|GunClose)\\s+" + id + "\\s*," + id + "(?:\\s*," + id + "\\s*)?\\s*;", Pattern.CASE_INSENSITIVE);
            m_keywords[23] = Pattern.compile("^\\s*VERSION\\s*:\\s*([0-9]*)", Pattern.CASE_INSENSITIVE);
            m_keywords[24] = Pattern.compile("^" + id + "\\s*" + id + "?\\s*,?" + id + "?\\s*,?" + id + "?\\s*,?" + id + "?\\s*,?" + id + "?\\s*,?" + id + "?\\s*,?" + id + "?\\s*,?" + id + "?\\s*,?\\s*;", Pattern.CASE_INSENSITIVE);            
            m_keywords[25] = Pattern.compile("^\\s*WaitWObj\\s*", Pattern.CASE_INSENSITIVE);
            m_keywords[26] = Pattern.compile("^\\s*" + id + "\\s*:=\\s*" + num + "\\s*;", Pattern.CASE_INSENSITIVE);
            // DLY End 2007/05/04
        }

    	public static void main(String [] parameters) throws SAXException, ParserConfigurationException, TransformerConfigurationException, NumberFormatException {
                
               //Create the object from the class 
                String [] parameterData = parameters[2].split("\\t");
                String [] profileNumbers = parameters[3].split("\\t");
                String [] axisTypes = parameters[4].split("\\t");
                String [] numOfAxis = parameters[5].split("\\t");
                String [] toolProfileNames = parameters[7].split("\\t");
                String [] homeNames = parameters[9].split("\\t");
                String schemaName = parameters[10];

                String [] objectProfileNames = parameters[11].split("\\t");

                
                String [] additionalProgramNames = null;
                if (parameters.length > 12)
                {
                    additionalProgramNames = parameters[12].split("\\t");
                }
                double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                Double massValue = Double.valueOf("0.0");
                double [] cogValues = {0.0, 0.0, 0.0};
                double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};
                double [] objectFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};

                RapidUploader uploader = new RapidUploader(parameters, profileNumbers, toolProfileNames, homeNames, objectProfileNames);
                
              //  uploader.createMotionProfile( uploader.m_motionProfileListElement, "Default", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.PERCENT,100.0,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v5", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.005,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v10", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.010,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v20", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.020,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v30", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.030,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v40", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.040,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v50", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.050,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v60", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.060,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v80", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.080,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v100", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.100,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v150", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.150,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v200", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.200,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v300", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.300,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v400", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.400,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v500", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.500,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v600", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.600,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v800", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,.800,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v1000", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,1.0,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v1500", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,1.5,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v2000", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,2.0,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v2500", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,2.5,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v3000", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,3.0,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v4000", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,4.0,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "vmax", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,5.0,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v6000", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,6.0,100.0,100.0, 100.0);
                uploader.createMotionProfile( uploader.m_motionProfileListElement, "v7000", DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE,7.0,100.0,100.0, 100.0);

              //  uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "Default", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.SPEED, false, 0.0);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "fine", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, false, 0.0);
				uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z0", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .0003);
			    uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z1", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .001);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z5", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .005);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z10", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .01);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z15", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .015);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z20", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .020);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z30", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .030);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z40", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .04);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z50", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .05);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z60", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .06);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z80", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .08);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z100", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .100);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z150", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .150);
                uploader.createAccuracyProfile( uploader.m_accuracyProfileListElement, "z200", DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, .200);
                
                uploader.createToolProfile( uploader.m_toolProfileListElement, "tool0", DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);          
           
                Element objectFrameElement = null;
                if (uploader.m_worldCoords == 1)
                {
                    objectFrameValues[0] = .0000001;
                    objectFrameElement = uploader.createObjectFrameProfile( uploader.m_objectProfileListElement, "wobj0", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD, objectFrameValues);  
                   // objectFrameElement = uploader.createObjectFrameProfile( uploader.m_objectProfileListElement, "wobj0.selectpart", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD, objectFrameValues);
                }
                else
                {
                    objectFrameElement = uploader.createObjectFrameProfile( uploader.m_objectProfileListElement, "wobj0", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE, objectFrameValues);                    
                   // objectFrameElement = uploader.createObjectFrameProfile( uploader.m_objectProfileListElement, "wobj0.selectpart", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE, objectFrameValues);
                }  
                
                if (objectFrameElement != null)
                {
                   // objectFrameElement.setAttribute("Replaced", "wobj0");
                }
                
                //Get the robot program and parse it line by line
                String robotProgramName = uploader.getPathToRobotProgramFile();
                String uploadInfoFileName = uploader.getPathToErrorLogFile();
                
                
                boolean createTool = false;
                double xx = 0.0;
                double yy = 0.0;
                double zz = 0.0;
                double yaw = 0.0;
                double pitch = 0.0;
                double roll = 0.0; 

                if (additionalProgramNames != null)
                {
                    for (int jj = 0; jj<additionalProgramNames.length;jj++)
                    {
                        int index = uploader.m_listOfCalledPrograms.indexOf(additionalProgramNames[jj]);
                        if (index < 0) {
                            uploader.m_listOfCalledPrograms.add(uploader.m_calledProgramCounter, additionalProgramNames[jj] );
                            uploader.m_listOfCalledPrograms.trimToSize(); 
                            uploader.m_calledProgramCounter++;
                        }
                    }
                }
                
                String mainProgram = new String(robotProgramName);
                uploader.processRobotProgram( robotProgramName, uploadInfoFileName, uploader.m_mountedGunName[0]); 
                for (int ii = 0; ii<uploader.m_calledProgramCounter; ii++) {
                    robotProgramName = (String)uploader.m_listOfCalledPrograms.get(ii);
                    if (!mainProgram.equals(robotProgramName))
                    {
                    	uploader.processRobotProgram( robotProgramName, uploadInfoFileName, uploader.m_mountedGunName[0] );
                    }
                }
                
               if (uploader.m_wobjActivityListElement == null)
               {
                    uploader.m_wobjActivityListElement = uploader.createActivityList("wobjdata_task");

                    uploader.CreateWobjdataTag("wobj0", false);  // create tag at all 0,0,0,0,0,0 relative to wobj0
               }                
                try {
                    uploader.writeXMLStreamToFile();
                }
                catch (TransformerException e) {
                    e.getMessage();
                }
                catch (IOException e) {
                    e.getMessage();
                }
        }

    private void processRobotProgram( String robotProgramName, String uploadInfoFileName, String mountedGunName )
    {
        int ecode = 0;
        Element activityListElem = null;
        m_moduleFound = false;
        
        try {

             int slash_index = robotProgramName.lastIndexOf('/');
             int backslash_index = robotProgramName.lastIndexOf('\\');

             if (slash_index > backslash_index)
                 backslash_index = slash_index;

             if (backslash_index < 0)
                 backslash_index = 0;

            if (m_mocConfigFileName.equals(""))
            {
                m_mocConfigFileName = robotProgramName.substring( 0, backslash_index+1) + "MOC.cfg";
            }
            //BufferedReader inMOCRead = new BufferedReader(new FileReader(moc_cfg_name));
            BufferedReader inMOCRead = new BufferedReader(new InputStreamReader(new FileInputStream(m_mocConfigFileName), prgFileEncoding));        
            
            String mocLine;
            boolean mocRobot = false;
            boolean mocSingle = false;

            mocLine = inMOCRead.readLine();
            while(mocLine != null) {
                if(mocLine.equals("")) {
                    mocLine = inMOCRead.readLine();
                    continue;
                } 
                 for(int ii = 0; ii < moc_keywords.length; ii++) {
                    Matcher match = moc_keywords[ii].matcher(mocLine);

                    if(match.find() == true) {
                        //Call appropriate methods to handle the input
                        switch(ii) {
                            case 0:
                                    mocRobot = true;
                                break;
                            case 1:
                                    mocRobot = false;
                                    mocSingle = true;
                                break;
                            case 2:
                                    mocRobot = false;
                                    mocSingle = false;
                                break;
                            case 3:
                                if (mocSingle == true)
                                {
                                    m_TbaseFrame[0] = match.group(1);
                                }
                                break;
                            case 4:
                                if (mocSingle == true)
                                {
                                    m_TbaseFrame[1] = match.group(1);
                                }                               
                                break;
                            case 5:
                                if (mocSingle == true)
                                {
                                    m_TbaseFrame[2] = match.group(1);
                                }                                                               
                                break;
                            case 6: 
                                if (mocRobot == true)
                                {
                                    m_TbaseFrame[3] = match.group(1);
                                }                                                                           
                                break;
                            case 7:   
                                if (mocRobot == true)
                                {
                                    m_TbaseFrame[4] = match.group(1);
                                }                                                              
                                break;
                            case 8: 
                                if (mocRobot == true)
                                {
                                    m_TbaseFrame[5] = match.group(1);
                                }                                                              
                                break;
                            case 9:
                                if (mocRobot == true)
                                {
                                    m_TbaseFrame[6] = match.group(1);
                                }                                                              
                                break;
                        } // switch
                    } // if
                } // for

                mocLine = inMOCRead.readLine();           
            } // while
        }
        catch (IOException e) {
            e.getMessage();
        }        
        
        try {
            
            /* first time read for procedures called and rob/joint-targets defined */
            //BufferedReader inProcRead = new BufferedReader(new FileReader(robotProgramName));
            BufferedReader inProcRead = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));
            Matcher procMatch;
            String procLine, procName;
            int grpCount;

            procLine = inProcRead.readLine();
            while(procLine != null) {
                if(procLine.equals("")) {
                    procLine = inProcRead.readLine();
                    continue;
                }
                procMatch = m_keywords[21].matcher(procLine);
                if (procMatch.find() == true) {
					String localPrefix = "";
					if (procMatch.group(1) != null && !procMatch.group(1).equals(""))
					{
						localPrefix = "LOCAL_";
					}
                    procName = localPrefix + procMatch.group(2);
                    m_procCount++;
                    m_procNameList.put(procName, String.valueOf(m_procCount));
                }
                procMatch = m_keywords[0].matcher(procLine);
                if (procMatch.find() == true) {
                    m_targetType = CARTESIAN;
                    processPosStatement(procMatch);
                }
                procMatch = m_keywords[1].matcher(procLine);
                if (procMatch.find() == true) {
                    m_targetType = JOINT;
                    processPosStatement(procMatch);
                }
                procLine = inProcRead.readLine();
            }

            /******** DLY 2007/05/20 object frame track offset no longer used since V5 handles aux. axes properly now
            /* second time read for objectframe track offset 
            //BufferedReader inProcRead = new BufferedReader(new FileReader(robotProgramName));
            BufferedReader inObjRead = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));
            Matcher objMatch;
            String objLine;

            objLine = inObjRead.readLine();
            while(objLine != null) {
                
                if(objLine.equals("")) {
                    objLine = inObjRead.readLine();
                    continue;
                }
                
                // DLY Start 2007/05/05 added m_moveKeywords
                for(int ii = 0; ii < m_moveKeywords.length; ii++) {
                    objMatch = m_moveKeywords[ii].matcher(objLine);

                    if(objMatch.find() == true) {
                        //Call appropriate methods to handle the input

                        switch(ii) {
                            case 0:
                            case 1:
                            case 3:
                            case 4:
                            case 5:
                            case 6:                                
                            case 7:
                            case 8:
                            case 9:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                                // non-explicit and non-circular                                
                                ecode = setupObjectFrameTrackOffset(objMatch, false, 0);
                                break;
                            case 2:
                            case 10:
                            case 11:
                            case 12:
                                // non-explicit with circular
                                ecode = setupObjectFrameTrackOffset(objMatch, false, 1);
                                break;
                            case 18:
                            case 19:
                            case 21:
                            case 22:
                            case 23:
                            case 24:                                
                            case 25:
                            case 26:
                            case 27:
                            case 31:
                            case 32:
                            case 33:
                            case 34:
                            case 35:
                                // explicit and non-circular                                
                                ecode = setupObjectFrameTrackOffset(objMatch, true, 0);
                                break;
                            case 20:
                            case 28:
                            case 29:
                            case 30:
                                // explicit and circular
                                ecode = setupObjectFrameTrackOffset(objMatch, true, 1);
                                break;                                
                        } // switch                        
                    } // if 
                } // for 
                // DLY End 2007/05/05
                
                objLine = inObjRead.readLine();
            }
            **************/
            
            /* second time read all statements */
            // BufferedReader in = new BufferedReader(new FileReader(robotProgramName));
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(robotProgramName), prgFileEncoding));
            BufferedWriter bw = null;
            boolean handle_attr = false;
            boolean handle_appl = false;
       
            if (m_firstInfoLine == true) {
                bw = new BufferedWriter(new FileWriter(uploadInfoFileName, false));    
            }
            else {
                bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
            }
            
            
            // System.out.println("START THE PARSER");
              
            bw.write(VERSION);
            bw.write(COPYRIGHT);
            bw.write("\nStart of java parsing.\n\n");
            bw.write("File: \"");
            bw.write(robotProgramName);
            bw.write("\":\n\n");
            bw.flush();            
            m_firstInfoLine = false;
            
            String line = in.readLine();
            int lineNumber = 1;
            boolean unparsedEntitiesExist = false;

            StringBuffer unparsedStatement = new StringBuffer();
            String unparsedString = new String();

            while(line != null) {

            	// System.out.println("Parse:" + line + "\n");
                if(line.equals("")) {
                    line = in.readLine();
                    lineNumber++;
                    continue;
                }

                unparsedStatement.append("Line ").append(lineNumber).append(": ").append(line).append("\n");
                unparsedString = unparsedStatement.toString();

           // DLY Start 2007/05/05  added m_moveKeywords for move commands
                boolean moveFound = false;
                
                for(int ii = 0; ii < m_moveKeywords.length; ii++) {
                    Matcher match = m_moveKeywords[ii].matcher(line);

                    if(match.find() == true) {
                         bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
                         bw.write("Parsing line number " + String.valueOf(lineNumber) + ": " + line + "\n");
                         bw.flush();
                        //Call appropriate methods to handle the input
                        switch(ii) {
                            case 0:
                            case 1:
                            case 7:
                            case 13:
                            case 14:
                            case 17:
                            case 18:
                            case 19:
                            case 20:
                            case 21:
                                // non-explicit and non-circular
                                ecode = processMOVStatement(match, activityListElem, false, 0, false);
                                moveFound = true;
                                break;
                            case 8:
                            case 9:                                
                            case 15: 
                                 // non-explicit and non-circular C5 ArcL
                                ecode = processMOVStatement(match, activityListElem, false, 0, true);
                                moveFound = true;                        
                                break;
                            case 2:
                            case 10:
                            case 22:
                                // non-explicit with circular
                                ecode = processMOVStatement(match, activityListElem, false, 1, false);
                                ecode = processMOVStatement(match, activityListElem, false, 2, false);
                                moveFound = true;
                                break;
                            case 11:
                            case 12:                                
                            case 16:
                                // non-explicit with circular C5 ArcC
                                ecode = processMOVStatement(match, activityListElem, false, 1, true);
                                ecode = processMOVStatement(match, activityListElem, false, 2, true);
                                moveFound = true;
                                break;                                
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                // non-explicit with spot
                                ecode = processMOVStatement(match, activityListElem, false, 0, false);
                                processSpotStatement(match, activityListElem, mountedGunName, false);
                                moveFound = true;
                                break;
                            case 23:
                            case 24:
                            case 30:
                            case 36:
                            case 37:
                            case 40:
                            case 41:
                            case 42:
                            case 43:
                            case 44:
                                // explicit and non-ciruclar
                                ecode = processMOVStatement(match, activityListElem, true, 0, false);
                                moveFound = true;
                                break;
                            case 31:
                            case 32:                                    
                            case 38:
                                // explicit and non-circular C5 ArcL
                                ecode = processMOVStatement(match, activityListElem, true, 0, true);
                                moveFound = true;
                                break;
                            case 25:
                            case 33:
                            case 45:
                                // explicit and circular
                                ecode = processMOVStatement(match, activityListElem, true, 1, false);
                                ecode = processMOVStatement(match, activityListElem, true, 2, false);
                                moveFound = true;
                                break;
                            case 34:
                            case 35:                                
                            case 39:
                                // explicit and circular C5 ArcC
                                ecode = processMOVStatement(match, activityListElem, true, 1, true);
                                ecode = processMOVStatement(match, activityListElem, true, 2, true);
                                moveFound = true;
                                break;                                
                            case 26:
                            case 27:
                            case 28:
                            case 29:
                                // explicit and Spot
                                ecode = processMOVStatement(match, activityListElem, true, 0, false);
                                processSpotStatement(match, activityListElem, mountedGunName, true);
                                moveFound = true;
                                break;                                
                        } // switch
                        break;
                    } // if
                } // for move match
           
                if (moveFound == false)
                {
                    for(int ii = 0; ii < m_keywords.length; ii++) {
                        Matcher match = m_keywords[ii].matcher(line);

                        if(match.find() == true && (activityListElem != null || ii < 2 || (ii > 4 && ii<7) || (ii > 8 && ii < 19)) ) {
                             bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
                             bw.write("Parsing line number " + String.valueOf(lineNumber) + ": " + line + "\n");
                             bw.flush();
                            //Call appropriate methods to handle the input
                            switch(ii) {
                                case 0:
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    processDOUTStatement(match,activityListElem,true);
                                    break;
                                case 3:
                                    processDOUTStatement(match,activityListElem,false);
                                    break;
                                case 4: 
                                    processPULSEStatement(match,activityListElem);
                                    break;
                                case 5:
                                    m_moduleFound = true;
                                    m_moduleName = match.group(1);
                                    break;
                                case 6:                  
                                    String localPrefix = "";
                                    if (m_moduleName.equals(""))
                                    {
                                        if (match.group(1) != null && !match.group(1).equals(""))
                                        {
                                            localPrefix = "LOCAL_";
                                    }
                                        ProgName = localPrefix + match.group(2);
                                    }
                                    else
                                    {
                                        ProgName = match.group(2) + "#" + m_moduleName;
                                        m_moduleCall = ProgName;
                                    }
                                    m_lastInBoundOffset = 0.0;
                                    String inBoundStr = (String)m_callTaskInBoundOffset.get(ProgName);
                                    if (inBoundStr != null && !inBoundStr.equals(""))
                                    {
                                    	m_inBoundOffset = Double.parseDouble(inBoundStr);
                                    }
                                    activityListElem = super.createActivityList(ProgName);
                                    // AttributeList appears first
                                    super.createAttributeList(activityListElem, null, null);
                                    processComments(activityListElem, "Pre");
                                    m_moduleName = "";
                                    break;
                                case 7:
                                    processComments(m_currentElement, "Post");
                                    break;
                                case 8:
                                    processComments(activityListElem, "Post");
                                    break;
                                case 9: 
                                    addToComments(activityListElem, line, false, false);
                                    break;
                                case 10:
                                    processToolFrameStatement(match);
                                    break;
                                case 11:
                                    processObjectFrameStatement(match);
                                    break;
                                case 12:
                                    processZoneDataStatement( match );
                                    break;
                                case 13:
                                    processSpeedDataStatement( match );
                                    break;
                                case 14:
                                    processSpotDataStatement( match, null, false );
                                    break;
                                case 15:
                                    processGunDataStatement( match, null );
                                    break;
                                case 16:
                                    processSeamDataStatement( match, null );
                                    break;
                                case 17:
                                    processWeldDataStatement( match, null );
                                    break;
                                case 18:
                                    processWeaveDataStatement( match, null );
                                    break;
                                case 19:
                                    processWAITStatement(match, line, activityListElem);
                                    break; 
                                case 20:
                                    processTimerStatement(match, activityListElem);
                                    break;                                                                    
                                case 21: // PROC procName(
                                    addToComments(activityListElem, line, true, true);
                                    while (true)
                                    {
                                        line = in.readLine();
                                        addToComments(activityListElem, line, true, true);
                                        lineNumber++;

                                        if (line.trim().equalsIgnoreCase("ENDPROC"))
                                            break;
                                    }
                                    if (null != activityListElem)
                                        processComments(activityListElem, "Post");
                                    break;
                                case 22: // GunOpen|GunSemiOpen|GunClose
                                    processGunBackupStatement(match, activityListElem, mountedGunName);
                                    break;
                                case 23: // version
                                   // m_versionNumber = Double.parseDouble(match.group(1));
                                case 24:
                                     boolean callProcessed = processCallStatement( match.group(1), activityListElem, robotProgramName, mountedGunName);
                                     if (callProcessed == false && m_moduleFound == true)
                                     {
                                        line = line.trim(); 
                                        if (m_OLPStyle.equals("OperationComments") || m_OLPStyle.equals("Honda"))
                                            addToComments(activityListElem, line, false, true);
                                        else
                                            addToComments(activityListElem, line, true, true);
                                        
                                         bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
                                         bw.write("WARNING: Parsed PROC call placed in Robot Language at ");
                                         bw.write(unparsedString);
                                         bw.flush();
                                         // unparsedEntitiesExist = true;
                                     }
                                    break;
                                case 25:
                                	processWaitWobj(line);
                                    line = line.trim();
                                    if (m_OLPStyle.equals("OperationComments") || m_OLPStyle.equals("Honda"))
                                        addToComments(activityListElem, line, false, true);
                                    else
                                        addToComments(activityListElem, line, true, true);
                                	break;
                                case 26:
                                	processNumVariable(match);
                                    if (m_OLPStyle.equals("OperationComments") || m_OLPStyle.equals("Honda"))
                                        addToComments(activityListElem, line, false, true);
                                    else
                                        addToComments(activityListElem, line, true, true);                                	
                                	break;
                            } // switch
                            break;
                        } // if
                        else if(ii == (m_keywords.length - 1) || (activityListElem == null && match.find() == true) )
                        {
                            String macroCommand = line;
                            boolean macroFound = false;
                            m_originalMacro = macroCommand;
                            macroCommand = macroCommand.replace(';',' ').trim();
                            if (macroCommand != null && activityListElem != null) {
                                String macroAction = (String)(m_macroCommands.get( macroCommand ));
                                if (macroAction != null)
                                {
                                    bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
                                    bw.write("Parsing Macro in line number " + String.valueOf(lineNumber) + ": " + line + " MACRO: " + macroAction + "\n");
                                    bw.flush();
                                    processMacroStatement( macroCommand, macroAction, activityListElem, mountedGunName );
                                    macroFound = true;
                                }
                                  	   }
                            if (macroFound == false && m_moduleFound == true)  {
                                line = line.trim();
                                if (m_OLPStyle.equals("OperationComments") || m_OLPStyle.equals("Honda"))
                                    addToComments(activityListElem, line, false, true);
                                else
                                    addToComments(activityListElem, line, true, true);
                                bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
                                bw.write("WARNING Unparsed Entity at ");
                                bw.write(unparsedString);
                                bw.flush();
                                unparsedEntitiesExist = true;
                            }
                        } // else if
                    } // for
                } // moveFound
                
                if (handle_attr == true || handle_appl == true)
                     addToComments(activityListElem, line, true, false);
                if ((ecode & ecode_TargetUndefined) == ecode_TargetUndefined)
                {
                	ecode = 0;
                    String tagName = (String)m_listOfUndefTargetNames.get(m_listOfUndefTargetNames.size()-1);
                    bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
                    bw.write("Line " + lineNumber + ": Target <" + tagName + "> undefined. Dummy target created.\n");
                    bw.flush();
                }
                if ((ecode & ecode_SpeedDataUndefined) == ecode_SpeedDataUndefined)
                {
                    String speeddataName = (String)m_listOfSpeedData.get(m_listOfSpeedData.size()-1);
                    // bw.write("Line " + lineNumber + ": SpeedData <" + speeddataName + "> undefined. Dummy MotionProfile created.\n");
                }

                line = in.readLine();
                lineNumber++;

                int numOfChar = unparsedStatement.length();
                unparsedStatement = unparsedStatement.delete(0, numOfChar);
            } // while
            in.close();

            bw  = new BufferedWriter(new FileWriter(uploadInfoFileName, true));
            if(unparsedEntitiesExist == false)
                bw.write("All the program statements have been parsed.\n");

            bw.write("\nJava parsing completed successfully.\n");
            bw.write("\nEnd of java parsing.\n");
            bw.flush();
            bw.close();
        } // try

        catch (IOException e) {
            e.getMessage();
        }
    }


        private void addToComments( Element actListElem, String line, boolean header, boolean nativeLanguage )
        {
            String [] commentComponents = line.split("!");
            String  newLine = line;
            String commentConcat = "";
          Pattern unparsedLine;

		  //  DLY Start 2008/03/05 - this code removes the ]; for the non native language case...simply use the input line or commentConcat...eliminate the other crud
		   for (int jj = 1; jj < commentComponents.length; jj++)
		   {
			   commentConcat = commentConcat.concat(commentComponents[jj]);
		   }         
		
		   /*****************
		   if (nativeLanguage ==  true) {
			   newLine = line;
		   }
		   else {
			   if (header == false) {
					 for (int jj=1;jj< commentComponents.length;jj++)
					 {
						 commentConcat = commentConcat.concat(commentComponents[jj]);
					 }
					 String commaCheck = commentConcat;
					 if ( commaCheck.trim().lastIndexOf(';') > 0 && commaCheck.trim().lastIndexOf(';') == (commaCheck.trim().length()-1) )
					 {
						 int commaIndex = commentConcat.lastIndexOf(';');
						 commaCheck = commentConcat.substring(0,commaIndex-1);
						 commentConcat = commaCheck;
					 }
					
					 newLine = line;
			   }
		   }
			  DLY End 2008/03/05 **************/
          
          //  while (line.indexOf("<") >= 0) {
          //       String newCommentValue = new String("");
          //       newCommentValue = line.substring(0, line.indexOf("<")) + "&lt;" + line.substring(line.indexOf("<")+1,line.length());
          //         line = newCommentValue; 
          //  }
          //  while (line.indexOf(">") >= 0) {
          //       String newCommentValue = new String("");
          //       newCommentValue = line.substring(0, line.indexOf(">")) + "&gt;" + line.substring(line.indexOf(">")+1, line.length());
          //         line = newCommentValue; 
          //  }
          
            if ((m_OLPStyle.equals("OperationComments") || m_OLPStyle.equals("Honda"))  && header == false) {
                String actName = "Comment." + String.valueOf(m_operationCommentCount++);
                if (nativeLanguage == true) {
                    actName = "Robot Language." + String.valueOf(m_robotLanguageCommentCount++);
                }
                Element operationElem = super.createActivityHeader( actListElem, actName, "Operation");
                
                String [] commentNames = {"Comment"};
                if (nativeLanguage == true) {
                    commentNames[0] = "Robot Language";
                }
                
                String [] commentValues = {line};
                
                super.createAttributeList(operationElem, commentNames, commentValues);
                
            }
            else {

                if (nativeLanguage)
                    newLine = "Robot Language:" + line;

                String commentName = "Comment" + String.valueOf(m_commentCount+1);
                if (header == true)
                    m_commentValues[m_commentCount] = newLine;
                else 
                    m_commentValues[m_commentCount] = commentConcat;
                m_commentNames[m_commentCount] = commentName;
                m_commentCount++;
            }
        }        


        private void processComments(Element addCommentElem, String commentPrefix) { 

            if (m_commentCount == 0)
                return;

            String [] commentNames = new String [m_commentCount];
            String [] commentValues = new String [m_commentCount];
            for (int ii=0;ii<m_commentCount;ii++) {
                if (m_commentNames[ii].indexOf("Comment") == 0)
                    commentNames[ii] = commentPrefix + m_commentNames[ii];
                else
                    commentNames[ii] = m_commentNames[ii]; 
                commentValues[ii] = m_commentValues[ii];
            }
            super.createAttributeList(addCommentElem, commentNames, commentValues);

            m_commentCount = 0;
            clearComments();
        }

        private void clearComments() {
            for (int ii=0;ii<m_commentNames.length;ii++) {
                m_commentNames[ii] = "";
            }
            for (int ii=0;ii<m_commentValues.length;ii++) {
                m_commentValues[ii] = "";
            }
        }

        private void processToolFrameStatement( Matcher match) {

             String tooldataName = match.group(1);
             String robhold = match.group(2);
             String xstr = match.group(3);
             String ystr = match.group(4);
             String zstr = match.group(5);
             String q1 = match.group(6);
             String q2 = match.group(7);
             String q3 = match.group(8);
             String q4 = match.group(9);
             String mass = match.group(10);
             String cogX = match.group(11);
             String cogY = match.group(12);
             String cogZ = match.group(13);
             String cogQ1 = match.group(14);
             String cogQ2 = match.group(15);
             String cogQ3 = match.group(16);
             String cogQ4 = match.group(17);
             String iXX = match.group(18);
             String iYY = match.group(19);
             String iZZ = match.group(20);
             
             double xx = Double.valueOf(xstr).doubleValue();
             double yy = Double.valueOf(ystr).doubleValue();
             double zz = Double.valueOf(zstr).doubleValue();
             double [] qq = {0.0, 0.0, 0.0, 0.0};
             qq[0] = Double.valueOf(q1).doubleValue();
             qq[1] = Double.valueOf(q2).doubleValue();
             qq[2] = Double.valueOf(q3).doubleValue();
             qq[3] = Double.valueOf(q4).doubleValue();
             
             quaternionToYPR( qq );
             
             double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
             Double massValue = Double.valueOf(mass);
             double [] cogValues = {0.0, 0.0, 0.0};
             double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};

             toolValues[0] = xx*.001;
             toolValues[1] = yy*.001;
             toolValues[2] = zz*.001;
             toolValues[3] = m_targetYaw;
             toolValues[4] = m_targetPitch;
             toolValues[5] = m_targetRoll;

             cogValues[0] = Double.valueOf(cogX).doubleValue()/1000.0;
             cogValues[1] = Double.valueOf(cogY).doubleValue()/1000.0;
             cogValues[2] = Double.valueOf(cogZ).doubleValue()/1000.0;

             inertiaValues[0] = Double.valueOf(iXX).doubleValue();
             inertiaValues[2] = Double.valueOf(iYY).doubleValue();
             inertiaValues[5] = Double.valueOf(iZZ).doubleValue();
             
            if (robhold.equalsIgnoreCase("TRUE")) {
                super.createToolProfile( m_toolProfileListElement, tooldataName, DNBIgpOlpUploadEnumeratedTypes.ToolType.ON_ROBOT, toolValues, massValue, cogValues, inertiaValues);
            }
            else {
                if (m_worldCoords == 1)
                    super.createToolProfile( m_toolProfileListElement, tooldataName, DNBIgpOlpUploadEnumeratedTypes.ToolType.STATIONARY, toolValues, massValue, cogValues, inertiaValues);
                else
                    super.createToolProfile( m_toolProfileListElement, tooldataName, DNBIgpOlpUploadEnumeratedTypes.ToolType.STATIONARY_ROBOT, toolValues, massValue, cogValues, inertiaValues);
            }
            m_toolProfilesProcessed.add(tooldataName);
        }
        private void processZoneDataStatement( Matcher match) {
             String zonedataName = match.group(1);
             String flyby = match.group(2);
             String pzoneTcp = match.group(3);
             
             double zoneValue = Double.parseDouble(pzoneTcp)*.001;
             
             if (flyby.equalsIgnoreCase("false")) {
                 createAccuracyProfile( m_accuracyProfileListElement, zonedataName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, zoneValue);                
             }
             else {
                 createAccuracyProfile( m_accuracyProfileListElement, zonedataName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, false, 0.0);                
             }
        }
        private void processSeamDataStatement( Matcher match, String seamName) {
            
             Element userProfileListElem = getUserProfileListElement();
             
             String  seamDataName = "";
             String purge_timeValue = "0";
             String preflow_timeValue = "0";
             String ign_schedValue = "0";
             String ign_voltageValue = "0";
             String ign_wirefeedValue = "0";
             String ign_currentValue = "0";
             String ign_volt_adjValue = "0";
             String ign_curr_adjValue = "0";
             String ign_move_delayValue = "0";
             String scrape_startValue = "0";
             String heat_speedValue = "0";
             String heat_timeValue = "0";
             String heat_distanceValue = "0";
             String heat_schedValue = "0";
             String heat_voltageValue = "0";
             String heat_wirefeedValue = "0";
             String heat_currentValue = "0";
             String heat_volt_adjValue = "0";
             String heat_curr_adjValue = "0";
             String cool_timeValue = "0";
             String fill_timeValue = "0";
             String bback_timeValue = "0";
             String rback_timeValue = "0";
             String postflow_timeValue = "0";
             String fill_schedValue = "0";
             String fill_voltageValue = "0";
             String fill_wirefeedValue = "0";
             String fill_currentValue = "0";
             String fill_volt_adjValue = "0";
             String fill_curr_adjValue = "0";
             
             if (seamName != null)
             {
                 seamDataName = seamName;
             }

             if (match != null)
             {
                 seamDataName = match.group(1);
                 purge_timeValue = match.group(2);
                 preflow_timeValue = match.group(3);
                 ign_schedValue = match.group(4);
                 ign_voltageValue = match.group(5);
                 ign_wirefeedValue = match.group(6);
                 ign_currentValue = match.group(7);
                 ign_volt_adjValue = match.group(8);
                 ign_curr_adjValue = match.group(9);
                 ign_move_delayValue = match.group(10);
                 scrape_startValue = match.group(11);
                 heat_speedValue = match.group(12);
                 heat_timeValue = match.group(13);
                 heat_distanceValue = match.group(14);
                 heat_schedValue = match.group(15);
                 heat_voltageValue = match.group(16);
                 heat_wirefeedValue = match.group(17);
                 heat_currentValue = match.group(18);
                 heat_volt_adjValue = match.group(19);
                 heat_curr_adjValue = match.group(20);
                 cool_timeValue = match.group(21);
                 fill_timeValue = match.group(22);
                 bback_timeValue = match.group(23);
                 rback_timeValue = match.group(24);
                 postflow_timeValue = match.group(25);
                 fill_schedValue = match.group(26);
                 fill_voltageValue = match.group(27);
                 fill_wirefeedValue = match.group(28);
                 fill_currentValue = match.group(29);
                 fill_volt_adjValue = match.group(30);
                 fill_curr_adjValue = match.group(31);
             }
             
             if (m_listOfSeamData.contains(seamDataName) == false && !seamDataName.equals(""))
             {             
                 LinkedList [] llSeamDataUserProfile = new LinkedList[30];
                 for (int ii=0;ii<30;ii++)
                     llSeamDataUserProfile[ii] = new LinkedList();
                 llSeamDataUserProfile[0].add(0,"purgetime");
                 llSeamDataUserProfile[1].add(0,"preflowtime");
                 llSeamDataUserProfile[2].add(0,"ignsched");
                 llSeamDataUserProfile[3].add(0,"ignvoltage");
                 llSeamDataUserProfile[4].add(0,"ignwirefeed");
                 llSeamDataUserProfile[5].add(0,"igncurrent");
                 llSeamDataUserProfile[6].add(0,"ignvoltadj");
                 llSeamDataUserProfile[7].add(0,"igncurradj");
                 llSeamDataUserProfile[8].add(0,"ignmovedelay");
                 llSeamDataUserProfile[9].add(0,"scrapestart");
                 llSeamDataUserProfile[10].add(0,"heatspeed");
                 llSeamDataUserProfile[11].add(0,"heattime");
                 llSeamDataUserProfile[12].add(0,"heatdistance");
                 llSeamDataUserProfile[13].add(0,"heatsched");
                 llSeamDataUserProfile[14].add(0,"heatvoltage");
                 llSeamDataUserProfile[15].add(0,"heatwirefeed");
                 llSeamDataUserProfile[16].add(0,"heatcurrent");
                 llSeamDataUserProfile[17].add(0,"heatvoltadj");
                 llSeamDataUserProfile[18].add(0,"heatcurradj");
                 llSeamDataUserProfile[19].add(0,"cooltime");
                 llSeamDataUserProfile[20].add(0,"filltime");
                 llSeamDataUserProfile[21].add(0,"bbacktime");
                 llSeamDataUserProfile[22].add(0,"rbacktime");
                 llSeamDataUserProfile[23].add(0,"postflowtime");
                 llSeamDataUserProfile[24].add(0,"fillsched");
                 llSeamDataUserProfile[25].add(0,"fillvoltage");
                 llSeamDataUserProfile[26].add(0,"fillwirefeed");
                 llSeamDataUserProfile[27].add(0,"fillcurrent");
                 llSeamDataUserProfile[28].add(0,"fillvoltadj");
                 llSeamDataUserProfile[29].add(0,"fillcurradj"); 
                 llSeamDataUserProfile[0].add(1,"double");
                 llSeamDataUserProfile[1].add(1,"double");
                 llSeamDataUserProfile[2].add(1,"integer");
                 llSeamDataUserProfile[3].add(1,"double");
                 llSeamDataUserProfile[4].add(1,"double");
                 llSeamDataUserProfile[5].add(1,"double");
                 llSeamDataUserProfile[6].add(1,"double");
                 llSeamDataUserProfile[7].add(1,"double");
                 llSeamDataUserProfile[8].add(1,"double");
                 llSeamDataUserProfile[9].add(1,"integer");
                 llSeamDataUserProfile[10].add(1,"double");
                 llSeamDataUserProfile[11].add(1,"double");
                 llSeamDataUserProfile[12].add(1,"double");
                 llSeamDataUserProfile[13].add(1,"integer");
                 llSeamDataUserProfile[14].add(1,"double");
                 llSeamDataUserProfile[15].add(1,"integer");
                 llSeamDataUserProfile[16].add(1,"double");
                 llSeamDataUserProfile[17].add(1,"double");
                 llSeamDataUserProfile[18].add(1,"double");
                 llSeamDataUserProfile[19].add(1,"double");
                 llSeamDataUserProfile[20].add(1,"double");
                 llSeamDataUserProfile[21].add(1,"double");
                 llSeamDataUserProfile[22].add(1,"double");
                 llSeamDataUserProfile[23].add(1,"double");
                 llSeamDataUserProfile[24].add(1,"integer");
                 llSeamDataUserProfile[25].add(1,"double");
                 llSeamDataUserProfile[26].add(1,"double");
                 llSeamDataUserProfile[27].add(1,"double");
                 llSeamDataUserProfile[28].add(1,"double");
                 llSeamDataUserProfile[29].add(1,"double");            
                 llSeamDataUserProfile[0].add(2,purge_timeValue);
                 llSeamDataUserProfile[1].add(2,preflow_timeValue);
                 llSeamDataUserProfile[2].add(2,ign_schedValue);
                 llSeamDataUserProfile[3].add(2,ign_voltageValue);
                 llSeamDataUserProfile[4].add(2,ign_wirefeedValue);
                 llSeamDataUserProfile[5].add(2,ign_currentValue);
                 llSeamDataUserProfile[6].add(2,ign_volt_adjValue);
                 llSeamDataUserProfile[7].add(2,ign_curr_adjValue);
                 llSeamDataUserProfile[8].add(2,ign_move_delayValue);
                 llSeamDataUserProfile[9].add(2,scrape_startValue);
                 llSeamDataUserProfile[10].add(2,heat_speedValue);
                 llSeamDataUserProfile[11].add(2,heat_timeValue);
                 llSeamDataUserProfile[12].add(2,heat_distanceValue);
                 llSeamDataUserProfile[13].add(2,heat_schedValue);
                 llSeamDataUserProfile[14].add(2,heat_voltageValue);
                 llSeamDataUserProfile[15].add(2,heat_wirefeedValue);
                 llSeamDataUserProfile[16].add(2,heat_currentValue);
                 llSeamDataUserProfile[17].add(2,heat_volt_adjValue);
                 llSeamDataUserProfile[18].add(2,heat_curr_adjValue);
                 llSeamDataUserProfile[19].add(2,cool_timeValue);
                 llSeamDataUserProfile[20].add(2,fill_timeValue);
                 llSeamDataUserProfile[21].add(2,bback_timeValue);
                 llSeamDataUserProfile[22].add(2,rback_timeValue);
                 llSeamDataUserProfile[23].add(2,postflow_timeValue);
                 llSeamDataUserProfile[24].add(2,fill_schedValue);
                 llSeamDataUserProfile[25].add(2,fill_voltageValue);
                 llSeamDataUserProfile[26].add(2,fill_wirefeedValue);
                 llSeamDataUserProfile[27].add(2,fill_currentValue);
                 llSeamDataUserProfile[28].add(2,fill_volt_adjValue);
                 llSeamDataUserProfile[29].add(2,fill_curr_adjValue);      

                 createUserProfile( userProfileListElem, "ABBseamdata", seamDataName, llSeamDataUserProfile);
                 m_listOfSeamData.add(seamDataName);
             }
        } 
        
        private void processWeldDataStatement( Matcher match, String weldName) {
            
             Element userProfileListElem = getUserProfileListElement();
             
             String  weldDataName = "";
             String weld_schedValue = "0";
             String weld_speedValue = "0";
             String weld_voltageValue = "0";
             String weld_wirefeedValue = "0";
             String weld_currentValue = "0";
             String delay_distanceValue = "0";
             String weld_volt_adjValue = "0";
             String weld_curr_adjValue = "0";
             String org_weld_speedValue = "0";
             String org_weld_voltageValue = "0";
             String org_weld_wfeedValue = "0";
     
             if (weldName != null)
             {
                 weldDataName = weldName;
             }
             
             if (match != null)
             {
                 weldDataName = match.group(1);
                 weld_schedValue = match.group(2);
                 weld_speedValue = match.group(3);
                 weld_voltageValue = match.group(4);
                 weld_wirefeedValue = match.group(5);
                 weld_currentValue = match.group(6);
                 delay_distanceValue = match.group(7);
                 weld_volt_adjValue = match.group(8);
                 weld_curr_adjValue = match.group(9);
                 org_weld_speedValue = match.group(10);
                 org_weld_voltageValue = match.group(11);
                 org_weld_wfeedValue = match.group(12);
             }
             
             if (m_listOfWeldData.contains(weldDataName) == false && !weldDataName.equals(""))
             {                          
                 LinkedList [] llweldDataUserProfile = new LinkedList[11];
                 for (int ii=0;ii<11;ii++)
                     llweldDataUserProfile[ii] = new LinkedList();
                 llweldDataUserProfile[0].add(0,"weldsched");
                 llweldDataUserProfile[1].add(0,"weldspeed");
                 llweldDataUserProfile[2].add(0,"weldvoltage");
                 llweldDataUserProfile[3].add(0,"weldwirefeed");
                 llweldDataUserProfile[4].add(0,"weldcurrent");
                 llweldDataUserProfile[5].add(0,"delaydistance");
                 llweldDataUserProfile[6].add(0,"weldvoltadj");
                 llweldDataUserProfile[7].add(0,"weldcurradj");
                 llweldDataUserProfile[8].add(0,"orgweldspeed");
                 llweldDataUserProfile[9].add(0,"orgweldvoltage");
                 llweldDataUserProfile[10].add(0,"orgweldwfeed");

                 llweldDataUserProfile[0].add(1,"integer");
                 llweldDataUserProfile[1].add(1,"double");
                 llweldDataUserProfile[2].add(1,"double");
                 llweldDataUserProfile[3].add(1,"double");
                 llweldDataUserProfile[4].add(1,"double");
                 llweldDataUserProfile[5].add(1,"double");
                 llweldDataUserProfile[6].add(1,"double");
                 llweldDataUserProfile[7].add(1,"double");
                 llweldDataUserProfile[8].add(1,"double");
                 llweldDataUserProfile[9].add(1,"double");
                 llweldDataUserProfile[10].add(1,"double");

                 llweldDataUserProfile[0].add(2,weld_schedValue);
                 llweldDataUserProfile[1].add(2,weld_speedValue);
                 llweldDataUserProfile[2].add(2,weld_voltageValue);
                 llweldDataUserProfile[3].add(2,weld_wirefeedValue);
                 llweldDataUserProfile[4].add(2,weld_currentValue);
                 llweldDataUserProfile[5].add(2,delay_distanceValue);
                 llweldDataUserProfile[6].add(2,weld_volt_adjValue);
                 llweldDataUserProfile[7].add(2,weld_curr_adjValue);
                 llweldDataUserProfile[8].add(2,org_weld_speedValue);
                 llweldDataUserProfile[9].add(2,org_weld_voltageValue);
                 llweldDataUserProfile[10].add(2,org_weld_wfeedValue);

                 createUserProfile( userProfileListElem, "ABBwelddata", weldDataName, llweldDataUserProfile);
                 m_listOfWeldData.add(weldDataName);
             }
        }        

        private void processWeaveDataStatement( Matcher match, String weaveName) {
            
             Element userProfileListElem = getUserProfileListElement();
            
             String weaveDataName = "";
             String weave_shapeValue = "0";
             String weave_typeValue = "0";
             String weave_lengthValue = "0";
             String weave_widthValue = "0";
             String weave_heightValue = "0";
             String dwell_leftValue = "0";
             String dwell_centerValue = "0";
             String dwell_rightValue = "0";
             String weave_dirValue = "0";
             String weave_tiltValue = "0";
             String weave_oriValue = "0";
             String weave_biasValue = "0";
             String weave_sync_leftValue = "0";
             String weave_sync_rightValue = "0";
             String wg_track_onValue = "0";
             
             if (weaveName != null)
             {
                 weaveDataName = weaveName;
             }
             
             if (match != null)
             {
                 weaveDataName = match.group(1);
                 weave_shapeValue = match.group(2);
                 weave_typeValue = match.group(3);
                 weave_lengthValue = match.group(4);
                 weave_widthValue = match.group(5);
                 weave_heightValue = match.group(6);
                 dwell_leftValue = match.group(7);
                 dwell_centerValue = match.group(8);
                 dwell_rightValue = match.group(9);
                 weave_dirValue = match.group(10);
                 weave_tiltValue = match.group(11);
                 weave_oriValue = match.group(12);
                 weave_biasValue = match.group(13);
                 weave_sync_leftValue = match.group(14);
                 weave_sync_rightValue = match.group(15);
                 wg_track_onValue = match.group(16);
             }
             
             if (m_listOfWeaveData.contains(weaveDataName) == false && !weaveDataName.equals(""))
             {             
                 LinkedList [] llweaveDataUserProfile = new LinkedList[15];
                 for (int ii=0;ii<15;ii++)
                     llweaveDataUserProfile[ii] = new LinkedList();
                 llweaveDataUserProfile[0].add(0,"weaveshape");
                 llweaveDataUserProfile[1].add(0,"weavetype");
                 llweaveDataUserProfile[2].add(0,"weavelength");
                 llweaveDataUserProfile[3].add(0,"weavewidth");
                 llweaveDataUserProfile[4].add(0,"weaveheight");
                 llweaveDataUserProfile[5].add(0,"dwellleft");
                 llweaveDataUserProfile[6].add(0,"dwellcenter");
                 llweaveDataUserProfile[7].add(0,"dwellright");
                 llweaveDataUserProfile[8].add(0,"weavedir");
                 llweaveDataUserProfile[9].add(0,"weavetilt");
                 llweaveDataUserProfile[10].add(0,"weaveori");
                 llweaveDataUserProfile[11].add(0,"weavebias");
                 llweaveDataUserProfile[12].add(0,"weavesyncleft");
                 llweaveDataUserProfile[13].add(0,"weavesyncright");
                 llweaveDataUserProfile[14].add(0,"wgtrackon");

                 llweaveDataUserProfile[0].add(1,"integer");
                 llweaveDataUserProfile[1].add(1,"integer");
                 llweaveDataUserProfile[2].add(1,"double");
                 llweaveDataUserProfile[3].add(1,"double");
                 llweaveDataUserProfile[4].add(1,"double");
                 llweaveDataUserProfile[5].add(1,"double");
                 llweaveDataUserProfile[6].add(1,"double");
                 llweaveDataUserProfile[7].add(1,"double");
                 llweaveDataUserProfile[8].add(1,"double");
                 llweaveDataUserProfile[9].add(1,"double");
                 llweaveDataUserProfile[10].add(1,"double");
                 llweaveDataUserProfile[11].add(1,"double");
                 llweaveDataUserProfile[12].add(1,"double");
                 llweaveDataUserProfile[13].add(1,"double");
                 llweaveDataUserProfile[14].add(1,"integer");

                 llweaveDataUserProfile[0].add(2,weave_shapeValue);
                 llweaveDataUserProfile[1].add(2,weave_typeValue);
                 llweaveDataUserProfile[2].add(2,weave_lengthValue);
                 llweaveDataUserProfile[3].add(2,weave_widthValue);
                 llweaveDataUserProfile[4].add(2,weave_heightValue);
                 llweaveDataUserProfile[5].add(2,dwell_leftValue);
                 llweaveDataUserProfile[6].add(2,dwell_centerValue);
                 llweaveDataUserProfile[7].add(2,dwell_rightValue);
                 llweaveDataUserProfile[8].add(2,weave_dirValue);
                 llweaveDataUserProfile[9].add(2,weave_tiltValue);
                 llweaveDataUserProfile[10].add(2,weave_oriValue);
                 llweaveDataUserProfile[11].add(2,weave_biasValue);
                 llweaveDataUserProfile[12].add(2,weave_sync_leftValue);
                 llweaveDataUserProfile[13].add(2,weave_sync_rightValue);
                 llweaveDataUserProfile[14].add(2,wg_track_onValue);

                 createUserProfile( userProfileListElem, "ABBweavedata", weaveDataName, llweaveDataUserProfile);
                 m_listOfWeaveData.add(weaveDataName);
             }
        }        

        private void processGunDataStatement( Matcher match, String gunName) {
            
             Element userProfileListElem = getUserProfileListElement();
             String  gunDataName = "";
             String nof_tipsValue = "0";
             String nof_plevelsValue = "0";
             String close_requestValue = "false";
             String open_requestValue = "false";             
             String tip1_counterValue = "0";
             String tip2_counterValue = "0";
             String tip1_maxValue = "0";
             String tip2_maxValue = "0";
             String close_time1Value = "0";
             String close_time2Value = "0";
             String close_time3Value = "0";
             String close_time4Value = "0";
             String build_up_p1Value = "0";
             String build_up_p2Value = "0";
             String build_up_p3Value = "0";
             String build_up_p4Value = "0";
             String open_timeValue = "0";
             
             if (gunName != null)
             {
                 gunDataName = gunName;
             }
             
             if (match != null)
             {
                 gunDataName = match.group(1);
                 nof_tipsValue = match.group(2);
                 nof_plevelsValue = match.group(3);             
                if (match.group(4).equalsIgnoreCase("TRUE"))
                     close_requestValue = "true";
                if (match.group(5).equalsIgnoreCase("TRUE"))
                     open_requestValue = "true";
                 tip1_counterValue = match.group(6);
                 tip2_counterValue = match.group(7);
                 tip1_maxValue = match.group(8);
                 tip2_maxValue = match.group(9);
                 close_time1Value = match.group(10);
                 close_time2Value = match.group(11);
                 close_time3Value = match.group(12);
                 close_time4Value = match.group(13);
                 build_up_p1Value = match.group(14);
                 build_up_p2Value = match.group(15);
                 build_up_p3Value = match.group(16);
                 build_up_p4Value = match.group(17);
                 open_timeValue = match.group(18);
             }
 
             if (m_listOfGunData.contains(gunDataName) == false && !gunDataName.equals(""))
             {                          
                 LinkedList [] llgunDataUserProfile = new LinkedList[17];
                 for (int ii=0;ii<17;ii++)
                     llgunDataUserProfile[ii] = new LinkedList();
                 llgunDataUserProfile[0].add(0,"noftips");
                 llgunDataUserProfile[1].add(0,"nofplevels");
                 llgunDataUserProfile[2].add(0,"closerequest");
                 llgunDataUserProfile[3].add(0,"openrequest");
                 llgunDataUserProfile[4].add(0,"tip1counter");
                 llgunDataUserProfile[5].add(0,"tip2counter");
                 llgunDataUserProfile[6].add(0,"tip1max");
                 llgunDataUserProfile[7].add(0,"tip2max");
                 llgunDataUserProfile[8].add(0,"closetime1");
                 llgunDataUserProfile[9].add(0,"closetime2");
                 llgunDataUserProfile[10].add(0,"closetime3");
                 llgunDataUserProfile[11].add(0,"closetime4");
                 llgunDataUserProfile[12].add(0,"buildupp1");
                 llgunDataUserProfile[13].add(0,"buildupp2");
                 llgunDataUserProfile[14].add(0,"buildupp3");
                 llgunDataUserProfile[15].add(0,"buildupp4");
                 llgunDataUserProfile[16].add(0,"opentime");

                 llgunDataUserProfile[0].add(1,"integer");
                 llgunDataUserProfile[1].add(1,"integer");
                 llgunDataUserProfile[2].add(1,"boolean");
                 llgunDataUserProfile[3].add(1,"boolean");
                 llgunDataUserProfile[4].add(1,"integer");
                 llgunDataUserProfile[5].add(1,"integer");
                 llgunDataUserProfile[6].add(1,"integer");
                 llgunDataUserProfile[7].add(1,"integer");
                 llgunDataUserProfile[8].add(1,"double");
                 llgunDataUserProfile[9].add(1,"double");
                 llgunDataUserProfile[10].add(1,"double");
                 llgunDataUserProfile[11].add(1,"double");
                 llgunDataUserProfile[12].add(1,"double");
                 llgunDataUserProfile[13].add(1,"double");
                 llgunDataUserProfile[14].add(1,"double");
                 llgunDataUserProfile[15].add(1,"double");
                 llgunDataUserProfile[16].add(1,"double");

                 llgunDataUserProfile[0].add(2,nof_tipsValue);
                 llgunDataUserProfile[1].add(2,nof_plevelsValue);
                 llgunDataUserProfile[2].add(2,close_requestValue);
                 llgunDataUserProfile[3].add(2,open_requestValue);
                 llgunDataUserProfile[4].add(2,tip1_counterValue);
                 llgunDataUserProfile[5].add(2,tip2_counterValue);
                 llgunDataUserProfile[6].add(2,tip1_maxValue);
                 llgunDataUserProfile[7].add(2,tip2_maxValue);
                 llgunDataUserProfile[8].add(2,close_time1Value);
                 llgunDataUserProfile[9].add(2,close_time2Value);
                 llgunDataUserProfile[10].add(2,close_time3Value);
                 llgunDataUserProfile[11].add(2,close_time4Value);
                 llgunDataUserProfile[12].add(2,build_up_p1Value);
                 llgunDataUserProfile[13].add(2,build_up_p2Value);
                 llgunDataUserProfile[14].add(2,build_up_p3Value);
                 llgunDataUserProfile[15].add(2,build_up_p4Value);
                 llgunDataUserProfile[16].add(2,open_timeValue);


                 createUserProfile( userProfileListElem, "ABBgundata", gunDataName, llgunDataUserProfile);
                 m_listOfGunData.add(gunDataName);                 
             }
        }
        
        private void processSpotDataStatement( Matcher match, String spotName, boolean isC5 ) {
            
             Element userProfileListElem = getUserProfileListElement();
             
             String spotDataName = "";
             String prog_noValue = "0";
             String tip_noValue = "0";
             String gun_pressureValue = "0";
             String timer_noValue = "0";
             String attrib_5 = null;
             
             if (isC5 == true)
             {
                 attrib_5 = "0";
             }
  
             if (spotName != null)
             {
                 spotDataName = spotName;
             }
             
             if (match != null)
             {
                 spotDataName = match.group(1);
                 prog_noValue = match.group(2);
                 tip_noValue = match.group(3);
                 gun_pressureValue = match.group(4);
                 timer_noValue = match.group(5);
                 attrib_5 = match.group(6);
             }
             
             if (attrib_5 == null || attrib_5.equals(""))
             {
                if (m_listOfSpotData.contains(spotDataName) == false && !spotDataName.equals(""))
                {                             
                     LinkedList [] llspotDataUserProfile = new LinkedList[4];
                     for (int ii=0;ii<4;ii++)
                         llspotDataUserProfile[ii] = new LinkedList();
                     llspotDataUserProfile[0].add(0,"progno");
                     llspotDataUserProfile[1].add(0,"tipno");
                     llspotDataUserProfile[2].add(0,"gunpressure");
                     llspotDataUserProfile[3].add(0,"timerno");


                     llspotDataUserProfile[0].add(1,"integer");
                     llspotDataUserProfile[1].add(1,"integer");
                     llspotDataUserProfile[2].add(1,"integer");
                     llspotDataUserProfile[3].add(1,"integer");


                     llspotDataUserProfile[0].add(2,prog_noValue);
                     llspotDataUserProfile[1].add(2,tip_noValue);
                     llspotDataUserProfile[2].add(2,gun_pressureValue);
                     llspotDataUserProfile[3].add(2,timer_noValue);

                     createUserProfile( userProfileListElem, "ABBspotdata", spotDataName, llspotDataUserProfile);
                     m_listOfSpotData.add(spotDataName);      
                } 
             }
             else
             {
                if (m_listOfC5SpotData.contains(spotDataName) == false && !spotDataName.equals(""))
                {                         
                     LinkedList [] llspotDataUserProfile = new LinkedList[5];
                     for (int ii=0;ii<5;ii++)
                         llspotDataUserProfile[ii] = new LinkedList();
                     llspotDataUserProfile[0].add(0,"progno");
                     llspotDataUserProfile[1].add(0,"tipforce");
                     llspotDataUserProfile[2].add(0,"platethickness");
                     llspotDataUserProfile[3].add(0,"platetolerance");
                     llspotDataUserProfile[4].add(0,"eqpressure");


                     llspotDataUserProfile[0].add(1,"integer");
                     llspotDataUserProfile[1].add(1,"double");
                     llspotDataUserProfile[2].add(1,"double");
                     llspotDataUserProfile[3].add(1,"double");
                     llspotDataUserProfile[4].add(1,"integer");

                     llspotDataUserProfile[0].add(2,prog_noValue);
                     llspotDataUserProfile[1].add(2,tip_noValue);
                     llspotDataUserProfile[2].add(2,gun_pressureValue);
                     llspotDataUserProfile[3].add(2,timer_noValue);
                     llspotDataUserProfile[4].add(2,attrib_5);

                     createUserProfile( userProfileListElem, "ABBspotdataC5", spotDataName, llspotDataUserProfile);
                     m_listOfC5SpotData.add(spotDataName);      
                } 
             }
        }        
        
        
        private void processSpeedDataStatement( Matcher match) {
             
             String speeddataName = match.group(1);
             String  vTcp = match.group(2);
             
             double speedValue = Double.parseDouble(vTcp)*.001;
             
             createMotionProfile( m_motionProfileListElement, speeddataName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speedValue,100.0,100.0, 100.0);
             m_listOfSpeedData.add(speeddataName);
        }

        private void processWaitWobj(String line)
        {
            //                      ISO-8859-1 Upper   ->|ISO-8859-1 Lower   ->|
            String letter = "[A-Za-z\\xC0-\\xD6\\xD8-\\xDF\\xE0-\\xF6\\xF8-\\xFF]";
            //String letter = "[A-Za-z\u00C0-\u00D6\u00D8-\u00DF\u00E0-\u00F6\u00F8-\u00FF]";
            String id  = "\\s*(" + letter + "(?:" + letter + "|" + "[0-9_]" + ")*)";
            String num = "([0-9Ee\\-\\.\\+]*)";

        	Pattern idpat = Pattern.compile("^\\s*WaitWObj\\s*" + id + "(\\Q\\RelDist:=\\E)?" + id, Pattern.CASE_INSENSITIVE);
            Pattern numpat = Pattern.compile("^\\s*WaitWObj\\s*" + id + "(\\Q\\RelDist:=\\E)?" + num, Pattern.CASE_INSENSITIVE);
            Matcher matchID = idpat.matcher(line);
            Matcher matchNUM = numpat.matcher(line);
            if (matchID.find() == true)
            {
            	String varStr = matchID.group(3);
            	if (varStr != null && !varStr.equals(""))
            	{
            		String offsetStr = (String)m_numVariables.get(varStr);
                	if (offsetStr != null && !offsetStr.equals(""))
                	{
                		m_inBoundOffset = Double.parseDouble(offsetStr);
                		m_waitWobj = true;
                	}
            	}           	
            }
            if (matchNUM.find() == true)
            {
            	String offsetStr = matchNUM.group(3);
            	if (offsetStr != null && !offsetStr.equals(""))
            	{
            		m_inBoundOffset = Double.parseDouble(offsetStr);
            		m_waitWobj = true;
            	}
            }
        }
        
        private void processNumVariable(Matcher match)
        {
            String idStr = match.group(1);
            String numStr = match.group(2);
            if (idStr != null && !idStr.equals("") && numStr != null && !numStr.equals(""))
            {
            	m_numVariables.put(idStr, numStr);
            }
        }
        
        private void processObjectFrameStatement(Matcher match)
        {
            String wobjdataName = match.group(1);
            String robhold = match.group(2);
            String ufprog = match.group(3);
            
            
            DNBIgpOlpUploadMatrixUtils matrixUtilities = new DNBIgpOlpUploadMatrixUtils();            

            double [] dTbase = {0.0, 0.0, 0.0, 0.0};
            for (int ii=0; ii<4; ii++)
                dTbase[ii] = Double.valueOf(m_TbaseFrame[ii+3]).doubleValue();        
            
            quaternionToYPR( dTbase );
            
            // get initial track value for this object profile and adjust if appropriate
            
            /********************* with Alek's changes don't adjust based on aux. axes value 
            String trackValue;
            trackValue = (String)m_objectTrackOffsetMapping.get(wobjdataName);
             
            if (trackValue != null)
            {
                double trackValueDbl = Double.valueOf(trackValue).doubleValue();
                double tbaseX = Double.valueOf(m_TbaseFrame[0]).doubleValue();
                m_TbaseFrame[0] = String.valueOf(trackValueDbl+tbaseX);
            }
            **********************/
            
            // get robot with respect to Tbase
            matrixUtilities.dgXyzyprToMatrix(m_TbaseFrame[0] + "," + m_TbaseFrame[1] + "," + m_TbaseFrame[2] + "," +
                            String.valueOf(m_targetYaw) + "," + String.valueOf(m_targetPitch) + "," + String.valueOf(m_targetRoll));            
        
            // Tbase with respect to robot
            matrixUtilities.dgInvert();
            
            String xInvertTbase = String.valueOf(matrixUtilities.dgGetX());
            String yInvertTbase = String.valueOf(matrixUtilities.dgGetY());
            String zInvertTbase = String.valueOf(matrixUtilities.dgGetZ());
            String yawInvertTbase = String.valueOf(matrixUtilities.dgGetYaw());
            String pitchInvertTbase = String.valueOf(matrixUtilities.dgGetPitch());
            String rollInvertTbase = String.valueOf(matrixUtilities.dgGetRoll());
            
            matrixUtilities.dgXyzyprToMatrix(xInvertTbase + "," + yInvertTbase + "," + zInvertTbase + "," +
                            yawInvertTbase + "," + pitchInvertTbase + "," + rollInvertTbase);      
                       
            String [] sUframe = new String [7];
            for (int ii=0; ii<7; ii++)
                sUframe[ii] = match.group(ii+5);

            double [] dUframeQ = {0.0, 0.0, 0.0, 0.0};
            for (int ii=0; ii<4; ii++)
                dUframeQ[ii] = Double.valueOf(sUframe[ii+3]).doubleValue();           

            quaternionToYPR( dUframeQ );

            // concat wobj with respect to Tbase so    Tbase/robot * wobj/Tbase = wobj/robot (for robot coords)
            matrixUtilities.dgCatXyzyprMatrix(sUframe[0] + "," + sUframe[1] + "," + sUframe[2] + "," +
                            String.valueOf(m_targetYaw) + "," + String.valueOf(m_targetPitch) + "," + String.valueOf(m_targetRoll));
  
            double [] objectFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};
            
            // wobjdata oframe [x,y,z],[q1,q2,q3,q4]
            double dSqrSum = 0.0, dTmp;
            String [] sOframe = new String [7];
            for (int ii=0; ii<7; ii++)
            {
                sOframe[ii] = match.group(ii+12);

                dTmp = Double.valueOf(sOframe[ii]).doubleValue();
                dSqrSum = dSqrSum + dTmp * dTmp;
            }
            boolean createUframe = false;
            if (Math.abs(dSqrSum-1) > ZERO_TOL)
            {
            	createUframe = true;
            }
            if (Math.abs(Double.valueOf(sOframe[3]).doubleValue()-1) > ZERO_TOL)
            {
            	createUframe = true;
            }
            if (Math.abs(Double.valueOf(sOframe[4]).doubleValue()) > ZERO_TOL)
            {
            	createUframe = true;
            }
            if (Math.abs(Double.valueOf(sOframe[5]).doubleValue()) > ZERO_TOL)
            {
            	createUframe = true;
            }
            if (Math.abs(Double.valueOf(sOframe[6]).doubleValue()) > ZERO_TOL)
            {
            	createUframe = true;
            }
           

            if (createUframe || robhold.equalsIgnoreCase("TRUE"))
            {
                objectFrameValues[0] = Double.valueOf(matrixUtilities.dgGetX()).doubleValue() * 0.001;
                objectFrameValues[1] = Double.valueOf(matrixUtilities.dgGetY()).doubleValue() * 0.001;
                objectFrameValues[2] = Double.valueOf(matrixUtilities.dgGetZ()).doubleValue() * 0.001;
                objectFrameValues[3] = Double.valueOf(matrixUtilities.dgGetYaw()).doubleValue();
                objectFrameValues[4] = Double.valueOf(matrixUtilities.dgGetPitch()).doubleValue();
                objectFrameValues[5] = Double.valueOf(matrixUtilities.dgGetRoll()).doubleValue();
 
                if (m_worldCoords == 1 || robhold.equalsIgnoreCase("TRUE"))
                {
                    if ((Math.abs(objectFrameValues[0]) + Math.abs(objectFrameValues[1]) + Math.abs(objectFrameValues[2])) < .0000001)
                    {
                        objectFrameValues[0] = .0000001;
                    }
                    super.createObjectFrameProfile( m_objectProfileListElement, wobjdataName + "_uframe", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD, objectFrameValues);               
                }
                else
                {
                    super.createObjectFrameProfile( m_objectProfileListElement, wobjdataName + "_uframe", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE, objectFrameValues);             
                }

                matrixUtilities.dgXyzyprToMatrix(xInvertTbase + "," + yInvertTbase + "," + zInvertTbase + "," +
                                yawInvertTbase + "," + pitchInvertTbase + "," + rollInvertTbase);             

                quaternionToYPR( dUframeQ );

                // concat wobj with respect to Tbase so    Tbase/robot * wobj/Tbase = wobj/robot (for robot coords)
                matrixUtilities.dgCatXyzyprMatrix(sUframe[0] + "," + sUframe[1] + "," + sUframe[2] + "," +
                                String.valueOf(m_targetYaw) + "," + String.valueOf(m_targetPitch) + "," + String.valueOf(m_targetRoll));
                
                double [] dOframeQ = {0.0, 0.0, 0.0, 0.0};
                for (int ii=0; ii<4; ii++)
                    dOframeQ[ii] = Double.valueOf(sOframe[ii+3]).doubleValue();

                quaternionToYPR( dOframeQ );

                matrixUtilities.dgCatXyzyprMatrix(sOframe[0] + "," + sOframe[1] + "," + sOframe[2] + "," +
                                String.valueOf(m_targetYaw) + "," + String.valueOf(m_targetPitch) + "," + String.valueOf(m_targetRoll));
            }

            objectFrameValues[0] = Double.valueOf(matrixUtilities.dgGetX()).doubleValue() * 0.001;
            objectFrameValues[1] = Double.valueOf(matrixUtilities.dgGetY()).doubleValue() * 0.001;
            objectFrameValues[2] = Double.valueOf(matrixUtilities.dgGetZ()).doubleValue() * 0.001;
            objectFrameValues[3] = Double.valueOf(matrixUtilities.dgGetYaw()).doubleValue();
            objectFrameValues[4] = Double.valueOf(matrixUtilities.dgGetPitch()).doubleValue();
            objectFrameValues[5] = Double.valueOf(matrixUtilities.dgGetRoll()).doubleValue();

            String wobjHold = "";
            if (ufprog.equalsIgnoreCase("FALSE"))
            {
                wobjHold = "_ufprog";
                m_objectFrameMovable.put(wobjdataName, "true");
            }
            else
            {
               m_objectFrameMovable.put(wobjdataName, "false");
            }


            Element objectFrameElement = null;
            if (m_worldCoords == 1 || robhold.equalsIgnoreCase("TRUE"))
            {
                if ((Math.abs(objectFrameValues[0]) + Math.abs(objectFrameValues[1]) + Math.abs(objectFrameValues[2])) < .0000001)
                {
                    objectFrameValues[0] = .0000001;
                }
                objectFrameElement = super.createObjectFrameProfile( m_objectProfileListElement, wobjdataName + wobjHold, DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD, objectFrameValues);          
                // objectFrameElement = super.createObjectFrameProfile( m_objectProfileListElement, wobjdataName + ".selectpart", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.WORLD, objectFrameValues);
            }
            else
            {
                objectFrameElement = super.createObjectFrameProfile( m_objectProfileListElement, wobjdataName + wobjHold, DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE, objectFrameValues);          
                // objectFrameElement = super.createObjectFrameProfile( m_objectProfileListElement, wobjdataName + ".selectpart", DNBIgpOlpUploadEnumeratedTypes.ObjectFrameType.ROBOT_BASE, objectFrameValues);
            }

            if (robhold.equalsIgnoreCase("TRUE"))
            {
                CreateWobjdataTag(wobjdataName, true);  // create tag at all 0,0,0,0,0,0 relative to current wobjdataName_uframe  
            }
            else
            {
                CreateWobjdataTag(wobjdataName, false);  // create tag at all 0,0,0,0,0,0 relative to current wobjdataName_uframe                      
            }            
              
            if (objectFrameElement != null)
            {
              //  objectFrameElement.setAttribute("Replaced", wobjdataName);
            }
            m_objectProfilesProcessed.add(wobjdataName);
        }

   private void CreateWobjdataTag(String wobjdataName, boolean held)
   {
       if (m_wobjActivityListElement == null)
       {
            m_wobjActivityListElement = createActivityList("wobjdata_task");

            CreateWobjdataTag("wobj0", false);  // create tag at all 0,0,0,0,0,0 relative to wobj0
       }
   
       Element actElem = super.createMotionActivityHeader(m_wobjActivityListElement, "MOVE_FOR_" + wobjdataName);
       if (held)
       {
            double [] toolValues = {0.0,0.0,0.0,0.0,0.0,0.0};
            Double massValue = Double.valueOf("0.0");
            double [] cogValues = {0.0, 0.0, 0.0};
            double [] inertiaValues = {0.0,0.0,0.0,0.0,0.0,0.0};
            double [] objectFrameValues = {0.0,0.0,0.0,0.0,0.0,0.0};           
           super.createToolProfile( m_toolProfileListElement, "tool0_stat", DNBIgpOlpUploadEnumeratedTypes.ToolType.STATIONARY, toolValues, massValue, cogValues, inertiaValues);          
           super.createMotionAttributes( actElem, "v1000", "fine", "tool0_stat", wobjdataName,  DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
       }
       else
       {
           super.createMotionAttributes( actElem, "v1000", "fine", "tool0", wobjdataName,  DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);           
       }
       
       Element targetElem = super.createTarget( actElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, false);
       double [] cartValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

       String cfgName = "Config_1";
                
       Element cartTargetElem = super.createCartesianTarget( targetElem, cartValues, cfgName);
       int [] turnNumbers = {0,0,0,0};
 
       super.createTurnNumbers( cartTargetElem, turnNumbers);
       super.createTag( cartTargetElem, "TAG_FOR_" + wobjdataName);
    }
        
        
        private void processPosStatement(Matcher match) throws NumberFormatException {
  
            String [] target_components;
            target_components = new String [17];
            int auxIndexOffset = 0;
            String targetDeclType = match.group(1);
            String tag_component = match.group(2);

            target_components[0] = match.group(3);
            target_components[1] = match.group(4);
            target_components[2] = match.group(5);
            target_components[3] = match.group(6);
            target_components[4] = match.group(7);
            target_components[5] = match.group(8);

            // only handle the robtarget/jointtarget if the first target component is valid...otherwise treat it as a dummy target
        	if (target_components[0].indexOf("9E+0") != 0)
            {            
	            //Connect the array with the target type through hashtable
	            m_targetTypes.put(tag_component,new Integer(m_targetType));   
	            m_targetNumbers.put(tag_component, new Integer(m_targetNumber));
	            m_targetDeclType.put(tag_component, targetDeclType);

   
	            if (m_targetType == CARTESIAN) {
	                target_components[6] = match.group(9);
	                target_components[7] = match.group(10);
	                target_components[8] = match.group(11);
	                target_components[9] = match.group(12);
	                target_components[10] = match.group(13);
	
	                auxIndexOffset = 0;
	            }
	            else {
	                auxIndexOffset = 5;
	            }
	            
	           //Add the target to a dynamic array
            	ResizeArrayList(m_listOfRobotTargets,m_targetNumber);	            
	            m_listOfRobotTargets.add(m_targetNumber, target_components);
	            //Resize the array
	            m_listOfRobotTargets.trimToSize();            
	
	            String [] extended_components;
	            extended_components = new String[6];
	
	            int work_defined = 0;
	            int ext_defined = 0;
	            int aux_defined = 0;
	            m_extAxesCount = 0;
	            m_auxAxesCount = 0;
	            m_workAxesCount = 0;
	
	            int rbtgtAuxStart = 14;
	            for (int ii=rbtgtAuxStart; ii<rbtgtAuxStart+6; ii++) {
	                int aux_offset = aux_defined+ext_defined+work_defined;
	                String axesValue = match.group(ii-auxIndexOffset);
	                // for linetracking last value is always the magical eax_f track offset
	                String eax_fStr = match.group(rbtgtAuxStart+5-auxIndexOffset);
	                double eax_f = 0.0;
	                if (eax_fStr != null && !eax_fStr.equals(""))
	                {
	                    eax_f = Double.valueOf(eax_fStr).doubleValue();
	                    if (eax_f < 9E+08)
	                    {
	                    	extended_components[5] = eax_fStr;
	                    }
	                }
	                double dAxesValue = Double.valueOf(axesValue).doubleValue();
	                if (axesValue != null && dAxesValue < 9E+08)
	                {
	                    String auxAxesType = (String)m_auxAxesTypes.get(String.valueOf(ii+1-rbtgtAuxStart));
	                    if (auxAxesType != null) {
	                        if (auxAxesType.equalsIgnoreCase("RailTrackGantry")) {
	                            extended_components[aux_offset] = axesValue;
	                            aux_defined++;
	                            m_auxAxesCount++;
	                            if (m_auxAxesStart < 0)
	                                m_auxAxesStart = aux_offset + 1;                           
	                        }
	                        else if ( auxAxesType.equalsIgnoreCase("EndOfArmTooling")) {
	                            extended_components[aux_offset] = axesValue;
	                            ext_defined++;
	                            m_extAxesCount++;
	                            if (m_extAxesStart < 0)
	                                m_extAxesStart = aux_offset + 1;                            
	                        }
	                        else if ( auxAxesType.equalsIgnoreCase("WorkPositioner")) {
	                            extended_components[aux_offset] = axesValue;
	                            work_defined++;
	                            m_workAxesCount++;
	                            if (m_workAxesStart < 0)
	                                m_workAxesStart = aux_offset + 1;                            
	                        }
	                    }
	                    else {
	                        extended_components[aux_offset] = axesValue;
	                        ext_defined++;
	                        m_extAxesCount++;
	                        if (m_extAxesStart < 0)
	                            m_extAxesStart = aux_offset + 1;
	                    }
	                }
	            }
	
	            if (ext_defined > 0) {
	                //Add the target to a dynamic array
	            	ResizeArrayList(m_listOfExtTargets, m_targetNumber);
	                m_listOfExtTargets.add(m_targetNumber, extended_components);
	                //Resize the array
	                m_listOfExtTargets.trimToSize();
	            }
	
	            if (aux_defined > 0) {
	                //Add the target to a dynamic array
	            	ResizeArrayList(m_listOfAuxTargets, m_targetNumber);	            	
	                m_listOfAuxTargets.add(m_targetNumber, extended_components);
	                //Resize the array
	                m_listOfAuxTargets.trimToSize();
	            }
	
	            if (work_defined > 0) { 
	                //Add the target to a dynamic array
	            	ResizeArrayList(m_listOfWorkTargets, m_targetNumber);	            	
	                m_listOfWorkTargets.add(m_targetNumber, extended_components);
	                //Resize the array
	                m_listOfWorkTargets.trimToSize();
	            }
	
	            m_targetNumber++;            
            } /* component 0 9E+00 */
        }

        // DLY Start 2007/05/05 added multi gun spot, added ID: to all except MoveAbsJ
        private int processMOVStatement(Matcher match,  Element activityListElem, boolean explicit, int circular, boolean C5Arc) {
            int ecode = 0;
            String movecomponent;

            movecomponent = match.group(1);

            //Create DOM Nodes and set appropriate attributes
            String activityName = movecomponent;
            Element actElem = super.createMotionActivityHeader(activityListElem, activityName);

            String toolProfileName;
            String objProfileName;
            String motionProfileName;
            String accuracyProfileName;
            String onOffStr = "";
            String seamProfileName = "";
            String weldProfileName = "";
            String weaveProfileName = "";
            String gunProfileName = "";
            String spotProfileName = "";
            String slashTimeVelocityValue = "";
            String slashTimeVelocity = "";
            String slashAccuracy = "";
            String slashAccuracyValue = "";
            String concStr = "";
            String eoffStr = "";
            String studProg = "", studGun = "";
            String idName = "";
            String beadName = "";
            String multiSpot1Name = "";
            String multiSpot2Name = "";
            String multiSpot3Name = "";
            String multiSpot4Name = "";

            int group_offset = 0;
            if (explicit == true)
            {
                if (circular > 0)
                    group_offset = 33; 
                else
                    group_offset = 16;
                    
                if (movecomponent.equalsIgnoreCase("MoveAbsJ")) {
                    group_offset = 11;
                }
            }
            else
            {
                if (circular > 0)
                    group_offset++;
            }
            if (movecomponent.equalsIgnoreCase("MoveAbsJ")) {
                eoffStr = match.group(4+group_offset);
                group_offset++;                                    
            }
            
            if (movecomponent.equalsIgnoreCase("SpotL") || movecomponent.equalsIgnoreCase("SpotJ"))
            {
                idName = match.group(4+group_offset);
                motionProfileName = match.group(5+group_offset);
                accuracyProfileName = "fine";
                if (m_versionNumber > 1.0)
                {
                    gunProfileName = match.group(6+group_offset);
                    spotProfileName = match.group(10+group_offset);                    
                }
                else
                {
                    spotProfileName = match.group(6+group_offset);
                    gunProfileName = match.group(10+group_offset);
                }
                
                if ( (m_listOfSpotData.contains(gunProfileName) == true || m_listOfC5SpotData.contains(gunProfileName) == true) && m_listOfGunData.contains(spotProfileName) == true)
                {
                       String tmpName = gunProfileName;
                       gunProfileName = spotProfileName;
                       spotProfileName = tmpName;
                }

                String teststr;
                for (int jj=1;jj<=17;jj++)
                {
                    teststr = match.group(jj+group_offset);
                }
                toolProfileName = match.group(16+group_offset);
                objProfileName = match.group(18+group_offset);
            }
            else if (movecomponent.equalsIgnoreCase("SpotML") || movecomponent.equalsIgnoreCase("SpotMJ"))
            {
                idName = match.group(4+group_offset);
                motionProfileName = match.group(5+group_offset);
                multiSpot1Name = match.group(7+group_offset);
                multiSpot2Name = match.group(9+group_offset);
                multiSpot3Name = match.group(11+group_offset);
                multiSpot4Name = match.group(13+group_offset);
                accuracyProfileName = "fine";
                toolProfileName = match.group(19+group_offset);
                objProfileName = match.group(21+group_offset);
            }            
            else if (movecomponent.equalsIgnoreCase("StudL"))
            {
                idName = match.group(4+group_offset);
                motionProfileName = match.group(5+group_offset);
                accuracyProfileName = "fine";
                studProg = match.group(6+group_offset);
                studGun  = match.group(7+group_offset);
                m_commentNames[m_commentCount] = "Stud Prog";
                m_commentValues[m_commentCount] = studProg;
                m_commentCount++;
                m_commentNames[m_commentCount] = "Stud Gun";
                m_commentValues[m_commentCount] = studGun;
                m_commentCount++;
                toolProfileName = match.group(8+group_offset);
                objProfileName = match.group(10+group_offset);
            }
            else if (movecomponent.equalsIgnoreCase("PaintL"))
            {
                idName = match.group(4+group_offset);                
                motionProfileName = match.group(5+group_offset);
                boolean isInList = m_listOfSpeedData.contains(motionProfileName);
                if (isInList == false)
                {
                    ecode = ecode + ecode_SpeedDataUndefined;
                    m_listOfSpeedData.add(motionProfileName);
                    // createMotionProfile(m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, 1.0, 100.0, 100.0, 100.0);
                }
                accuracyProfileName = match.group(16+group_offset);
                toolProfileName = match.group(17+group_offset);
                objProfileName = match.group(19+group_offset);

                String eventdata = new String("");
                for (int ii=6; ii<16; ii++)
                {
                    if (match.group(ii+group_offset) == null)
                        break;
                    eventdata = eventdata + match.group(ii+group_offset);
                }
                m_commentNames[m_commentCount] = "EventData";
                m_commentValues[m_commentCount] = eventdata;
                m_commentCount++;
            }
            else if (movecomponent.equalsIgnoreCase("ArcL") || movecomponent.equalsIgnoreCase("ArcL1") || movecomponent.equalsIgnoreCase("ArcL2") || movecomponent.equalsIgnoreCase("ArcC"))
            {
                if (C5Arc == false)
                {
                    onOffStr = match.group(2);
                }

                idName = match.group(5+group_offset);                
                motionProfileName = match.group(6+group_offset);
                slashTimeVelocity = match.group(7+group_offset);
                slashTimeVelocityValue = match.group(8+group_offset);
                if (slashTimeVelocity != null && slashTimeVelocity.equalsIgnoreCase("\\T:=") ) {
                    motionProfileName = "Time" + slashTimeVelocityValue;
                    boolean isInList = m_listOfSpeedValues.contains(motionProfileName);
                    double timeValue = Double.parseDouble(slashTimeVelocityValue)*.001;

                    if(isInList == false) {
                      m_listOfSpeedValues.add(motionProfileName);
                      createMotionProfile( m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeValue,100.0,100.0, 100.0);
                    }                    
                }
                seamProfileName = match.group(9+group_offset);
                weldProfileName = match.group(10+group_offset);
                if (C5Arc == true)
                {
                    // no weave data for C5 so decrement offset after reading seam/weld data
                    group_offset--;
                }
                else
                {
                    weaveProfileName = match.group(11+group_offset);
                }
                
                accuracyProfileName = match.group(12+group_offset);
                slashAccuracy = match.group(13+group_offset);
                slashAccuracyValue = match.group(14+group_offset);
                if (slashAccuracy != null && slashAccuracy.equalsIgnoreCase("\\Z:=")) {
                    accuracyProfileName = "Speed" + slashAccuracyValue;
                    boolean isInList = m_listOfAccuracyValues.contains(accuracyProfileName);
                    double accuracyValue = Double.parseDouble(slashAccuracyValue)*.001;

                        if(isInList == false) {
                          m_listOfAccuracyValues.add(accuracyProfileName);  
                        createAccuracyProfile( m_accuracyProfileListElement, accuracyProfileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, accuracyValue);                
                    }
                }

                toolProfileName = match.group(15+group_offset);
                objProfileName = match.group(17+group_offset);                                

            }
            else if (movecomponent.equalsIgnoreCase("ArcLStart") || movecomponent.equalsIgnoreCase("ArcLEnd") || movecomponent.equalsIgnoreCase("ArcCStart") || movecomponent.equalsIgnoreCase("ArcCEnd"))
            {
                idName = match.group(4+group_offset);                
                motionProfileName = match.group(5+group_offset);
                slashTimeVelocity = match.group(6+group_offset);
                slashTimeVelocityValue = match.group(7+group_offset);
                if (slashTimeVelocity != null && slashTimeVelocity.equalsIgnoreCase("\\T:=") ) {
                    motionProfileName = "Time" + slashTimeVelocityValue;
                    boolean isInList = m_listOfSpeedValues.contains(motionProfileName);
                    double timeValue = Double.parseDouble(slashTimeVelocityValue)*.001;

                    if(isInList == false) {
                      m_listOfSpeedValues.add(motionProfileName);
                      createMotionProfile( m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeValue,100.0,100.0, 100.0);
                    }                    
                }
                seamProfileName = match.group(8+group_offset);
                weldProfileName = match.group(9+group_offset);
                accuracyProfileName = match.group(10+group_offset);
                slashAccuracy = match.group(11+group_offset);
                slashAccuracyValue = match.group(12+group_offset);
                if (slashAccuracy != null && slashAccuracy.equalsIgnoreCase("\\Z:=")) {
                    accuracyProfileName = "Speed" + slashAccuracyValue;
                    boolean isInList = m_listOfAccuracyValues.contains(accuracyProfileName);
                    double accuracyValue = Double.parseDouble(slashAccuracyValue)*.001;

                        if(isInList == false) {
                          m_listOfAccuracyValues.add(accuracyProfileName);  
                        createAccuracyProfile( m_accuracyProfileListElement, accuracyProfileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, accuracyValue);                
                    }
                }

                toolProfileName = match.group(13+group_offset);
                objProfileName = match.group(15+group_offset);                
            }
            else if(movecomponent.equalsIgnoreCase("DispL") || movecomponent.equalsIgnoreCase("DispC"))            
            {
                onOffStr = match.group(2);

                idName = match.group(5+group_offset);                
                motionProfileName = match.group(6+group_offset);
                slashTimeVelocity = match.group(7+group_offset);
                slashTimeVelocityValue = match.group(8+group_offset);
                if (slashTimeVelocity != null && slashTimeVelocity.equalsIgnoreCase("\\T:=") ) {
                    motionProfileName = "Time" + slashTimeVelocityValue;
                    boolean isInList = m_listOfSpeedValues.contains(motionProfileName);
                    double timeValue = Double.parseDouble(slashTimeVelocityValue)*.001;

                    if(isInList == false) {
                      m_listOfSpeedValues.add(motionProfileName);
                      createMotionProfile( m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeValue,100.0,100.0, 100.0);
                    }                    
                }
                beadName = match.group(9+group_offset);
                String slashDelay = match.group(10+group_offset);
                String delayTime = match.group(11+group_offset);
                if (slashDelay != null && !slashDelay.equals(""))
                {
                	beadName += slashDelay + delayTime;
                }
                accuracyProfileName = match.group(12+group_offset);
                slashAccuracy = match.group(13+group_offset);
                slashAccuracyValue = match.group(14+group_offset);
                if (slashAccuracy != null && slashAccuracy.equalsIgnoreCase("\\Z:=")) {
                    accuracyProfileName = "Speed" + slashAccuracyValue;
                    boolean isInList = m_listOfAccuracyValues.contains(accuracyProfileName);
                    double accuracyValue = Double.parseDouble(slashAccuracyValue)*.001;

                        if(isInList == false) {
                          m_listOfAccuracyValues.add(accuracyProfileName);  
                        createAccuracyProfile( m_accuracyProfileListElement, accuracyProfileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, accuracyValue);                
                    }
                }

                toolProfileName = match.group(15+group_offset);
                objProfileName = match.group(17+group_offset);                                
            }
            else if (movecomponent.equalsIgnoreCase("MoveAbsJ"))
            {
                concStr = match.group(2);
                idName = "";                
                motionProfileName = match.group(4+group_offset);
                slashTimeVelocity = match.group(5+group_offset);
                slashTimeVelocityValue = match.group(6+group_offset);
                if (slashTimeVelocity != null && slashTimeVelocity.equalsIgnoreCase("\\T:=") ) {
                    motionProfileName = "Time" + slashTimeVelocityValue;
                    boolean isInList = m_listOfSpeedValues.contains(motionProfileName);
                    double timeValue = Double.parseDouble(slashTimeVelocityValue)*.001;

                    if(isInList == false) {
                      m_listOfSpeedValues.add(motionProfileName);
                      createMotionProfile( m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeValue,100.0,100.0, 100.0);
                    }                    
                }
                else if (slashTimeVelocity != null && slashTimeVelocity.equalsIgnoreCase("\\V:=") ) {
                    motionProfileName = "Speed" + slashTimeVelocityValue;
                    boolean isInList = m_listOfSpeedValues.contains(motionProfileName);
                    double speedValue = Double.parseDouble(slashTimeVelocityValue)*.001;

                    if(isInList == false) {
                      m_listOfSpeedValues.add(motionProfileName);
                      createMotionProfile( m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speedValue,100.0,100.0, 100.0);
                    }                            
                }
                
                accuracyProfileName = match.group(7+group_offset);
                slashAccuracy = match.group(8+group_offset);
                slashAccuracyValue = match.group(9+group_offset);
                if (slashAccuracy != null && slashAccuracy.equalsIgnoreCase("\\Z:=")) {
                    accuracyProfileName = "Speed" + slashAccuracyValue;
                    boolean isInList = m_listOfAccuracyValues.contains(accuracyProfileName);
                    double accuracyValue = Double.parseDouble(slashAccuracyValue)*.001;

                        if(isInList == false) {
                          m_listOfAccuracyValues.add(accuracyProfileName);  
                        createAccuracyProfile( m_accuracyProfileListElement, accuracyProfileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, accuracyValue);                
                    }
                }

                toolProfileName = match.group(10+group_offset);
                objProfileName = match.group(12+group_offset);
            }
            else
            {
                concStr = match.group(2);
                idName = match.group(5+group_offset);                
                motionProfileName = match.group(6+group_offset);
                slashTimeVelocity = match.group(7+group_offset);
                slashTimeVelocityValue = match.group(8+group_offset);
                if (slashTimeVelocity != null && slashTimeVelocity.equalsIgnoreCase("\\T:=") ) {
                    motionProfileName = "Time" + slashTimeVelocityValue;
                    boolean isInList = m_listOfSpeedValues.contains(motionProfileName);
                    double timeValue = Double.parseDouble(slashTimeVelocityValue)*.001;

                    if(isInList == false) {
                      m_listOfSpeedValues.add(motionProfileName);
                      createMotionProfile( m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.TIME, timeValue,100.0,100.0, 100.0);
                    }                    
                }
                else if (slashTimeVelocity != null && slashTimeVelocity.equalsIgnoreCase("\\V:=") ) {
                    motionProfileName = "Speed" + slashTimeVelocityValue;
                    boolean isInList = m_listOfSpeedValues.contains(motionProfileName);
                    double speedValue = Double.parseDouble(slashTimeVelocityValue)*.001;

                    if(isInList == false) {
                      m_listOfSpeedValues.add(motionProfileName);
                      createMotionProfile( m_motionProfileListElement, motionProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionBasis.ABSOLUTE, speedValue,100.0,100.0, 100.0);
                    }                            
                }
                
                accuracyProfileName = match.group(9+group_offset);
                slashAccuracy = match.group(10+group_offset);
                slashAccuracyValue = match.group(11+group_offset);
                if (slashAccuracy != null && slashAccuracy.equalsIgnoreCase("\\Z:=")) {
                    accuracyProfileName = "Speed" + slashAccuracyValue;
                    boolean isInList = m_listOfAccuracyValues.contains(accuracyProfileName);
                    double accuracyValue = Double.parseDouble(slashAccuracyValue)*.001;

                        if(isInList == false) {
                          m_listOfAccuracyValues.add(accuracyProfileName);  
                        createAccuracyProfile( m_accuracyProfileListElement, accuracyProfileName, DNBIgpOlpUploadEnumeratedTypes.AccuracyType.DISTANCE, true, accuracyValue);                
                    }
                }

                toolProfileName = match.group(12+group_offset);
                objProfileName = match.group(14+group_offset);
            }

            // older ABB RAPID S4 controllers are not case sensitive...if a match is not found for tool/wobj check without case sensitivity
            if (m_toolProfilesProcessed.contains(toolProfileName) == false) 
            {
            	for (int kk=0;kk<m_toolProfilesProcessed.size();kk++)
            	{
            		String ignoreCaseTool = (String)m_toolProfilesProcessed.get(kk);
            		if (ignoreCaseTool.equalsIgnoreCase(toolProfileName))
            		{
            			toolProfileName = ignoreCaseTool;
            		}
            	}
            }
            if (m_objectProfilesProcessed.contains(objProfileName) == false) 
            {
            	for (int kk=0;kk<m_objectProfilesProcessed.size();kk++)
            	{
            		String ignoreCaseObject = (String)m_objectProfilesProcessed.get(kk);
            		if (ignoreCaseObject.equalsIgnoreCase(objProfileName))
            		{
            			objProfileName = ignoreCaseObject;
            		}
            	}
            }                
            
            if (objProfileName == null || objProfileName.equals(""))
                objProfileName = "wobj0";

            String wobjtrack = (String)m_objectFrameMovable.get(objProfileName);
            if (wobjtrack != null && wobjtrack.equals("true"))
            {
                objProfileName += "_ufprog";
                m_objectFrameMove = true;
                if (m_waitWobj == true)
                {
                     super.setTrackStatus(DNBIgpOlpUploadEnumeratedTypes.TrackType.DELTA_POS);   	
                }
            }
            else
            {
                m_objectFrameMove = false;
                super.setTrackStatus(DNBIgpOlpUploadEnumeratedTypes.TrackType.TRACK_OFF);
            }
            
            Element motionAttribsElem = null;
            if (movecomponent.equalsIgnoreCase("MoveJ") || movecomponent.equalsIgnoreCase("MoveAbsJ") || movecomponent.equalsIgnoreCase("GripJ"))
                motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);  
             else if (movecomponent.equalsIgnoreCase("ArcL") || movecomponent.equalsIgnoreCase("ArcL1") || movecomponent.equalsIgnoreCase("ArcL2") || movecomponent.equalsIgnoreCase("ArcC") || movecomponent.equalsIgnoreCase("ArcLEnd") || movecomponent.equalsIgnoreCase("ArcLStart") || movecomponent.equalsIgnoreCase("ArcCEnd") || movecomponent.equalsIgnoreCase("ArcCStart")) {           
                Hashtable userProfileTable = new Hashtable();
                userProfileTable.put("ABBseamdata",seamProfileName);
                processSeamDataStatement(null,seamProfileName);
                if (C5Arc == false)
                {
                    userProfileTable.put("ABBweavedata",weaveProfileName);
                    processWeaveDataStatement(null, weaveProfileName);
                }
                userProfileTable.put("ABBwelddata",weldProfileName);
                processWeldDataStatement(null, weldProfileName);
                switch (circular)
                {
                    case 0:
                        motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileTable, DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
                        break;
                    case 1:
                        motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileTable, DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
                        break;
                    case 2:
                        motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileTable, DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
                        break;
                }
            }
            else if (movecomponent.equalsIgnoreCase("SpotL") || movecomponent.equals("SpotJ")) {
                Hashtable userProfileTable = new Hashtable();
                if (m_listOfC5SpotData.contains(spotProfileName) == true)
                {
                    userProfileTable.put("ABBspotdataC5",spotProfileName);
                }
                else
                {
                    userProfileTable.put("ABBspotdata",spotProfileName);
                }
                
                processSpotDataStatement(null, spotProfileName, m_listOfC5SpotData.contains(spotProfileName));
                userProfileTable.put("ABBgundata",gunProfileName);
                processGunDataStatement(null, gunProfileName);
                if (movecomponent.equalsIgnoreCase("SpotL"))
                    motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileTable, DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
                else
                    motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileTable, DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);
            }
            else if (movecomponent.equalsIgnoreCase("SpotML") || movecomponent.equalsIgnoreCase("SpotMJ"))
            {
                Hashtable userProfileTable = new Hashtable();  
                if (multiSpot1Name != null && !multiSpot1Name.equals(""))
                {
                    if (m_listOfC5SpotData.contains(multiSpot1Name) == true)
                    {
                        userProfileTable.put("ABBspotdataC5",multiSpot1Name);
                    }
                    else
                    {
                        userProfileTable.put("ABBspotdata",multiSpot1Name);
                    }                    
                }
                if (multiSpot2Name != null && !multiSpot2Name.equals(""))
                {
                    if (m_listOfC5SpotData.contains(multiSpot2Name) == true)
                    {
                        userProfileTable.put("ABBspotdataC5.1",multiSpot2Name);
                    }
                    else
                    {
                        userProfileTable.put("ABBspotdata.1",multiSpot2Name);
                    }
                }
                if (multiSpot3Name != null && !multiSpot3Name.equals(""))
                {
                    if (m_listOfC5SpotData.contains(multiSpot3Name) == true)
                    {
                        userProfileTable.put("ABBspotdataC5.2",multiSpot3Name);
                    }
                    else
                    {
                        userProfileTable.put("ABBspotdata.2",multiSpot3Name);
                    }
                }
                if (multiSpot4Name != null && !multiSpot4Name.equals(""))
                {
                    if (m_listOfC5SpotData.contains(multiSpot4Name) == true)
                    {
                        userProfileTable.put("ABBspotdataC5.3",multiSpot4Name);
                    }
                    else
                    {
                        userProfileTable.put("ABBspotdata.3",multiSpot4Name);
                    }
                }
                if (movecomponent.equalsIgnoreCase("SpotML"))
                    motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileTable, DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);               
                else
                    motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, userProfileTable, DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION);                                   
            }
            else if (movecomponent.equalsIgnoreCase("StudL")) {
                motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
            }
            else if (movecomponent.equalsIgnoreCase("PaintL")) {
                motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName, DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
            }
            else
            {
                switch (circular)
                {
                    case 0:
                        motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName,  DNBIgpOlpUploadEnumeratedTypes.MotionType.LINEAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
                        break;
                    case 1:
                        motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName,  DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULARVIA_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
                        break;
                    case 2:
                        motionAttribsElem = super.createMotionAttributes( actElem, motionProfileName, accuracyProfileName, toolProfileName, objProfileName,  DNBIgpOlpUploadEnumeratedTypes.MotionType.CIRCULAR_MOTION, DNBIgpOlpUploadEnumeratedTypes.OrientMode.ONE_AXIS);
                        break;
                }
            }

            if (motionAttribsElem != null)
            {
               NodeList oObjectFrameProfileNodeList = 
                        motionAttribsElem.getElementsByTagName("ObjectFrameProfile");

                if (oObjectFrameProfileNodeList != null && 
                                 oObjectFrameProfileNodeList.getLength() > 0) {
                    Node oObjectFrameProfileNode = 
                                           oObjectFrameProfileNodeList.item(0);
                    Element oObjectFrameProfileElem = (Element) oObjectFrameProfileNode;
                    if (oObjectFrameProfileElem != null)
                    {
                       // oObjectFrameProfileElem.setAttribute("Replaced", objProfileName);
                    }
                }                
            }
            //Call the method to process the target

			if (movecomponent.equalsIgnoreCase("MoveC") || movecomponent.equalsIgnoreCase("ArcC") || movecomponent.equalsIgnoreCase("ArcCStart") || movecomponent.equalsIgnoreCase("ArcCEnd") || movecomponent.equalsIgnoreCase("DispC"))
			{
               ecode = ecode + processTarget( actElem, match, explicit, circular);
            }
            else
            {
               ecode = ecode + processTarget(actElem,  match, explicit, 0);
            }
            
            if (concStr != null && !concStr.equals("")) {
                 m_commentNames[m_commentCount] = "Conc";
                 m_commentValues[m_commentCount] = "1" ;
                 m_commentCount++;
            }
            if (eoffStr != null && !eoffStr.equals("")) {
                 m_commentNames[m_commentCount] = "NoEOffs";
                 m_commentValues[m_commentCount] = "1";
                 m_commentCount++;
            }
            if (onOffStr != null && !onOffStr.equals("")) {
                 m_commentNames[m_commentCount] = "Gun";
                 m_commentValues[m_commentCount] = onOffStr;
                 m_commentCount++;                
            }
            if (idName != null && !idName.equals(""))
            {
                m_commentNames[m_commentCount] = "ID";
                m_commentValues[m_commentCount] = idName;
                m_commentCount++;
            }
            if (beadName != null && !beadName.equals(""))
            {
                m_commentNames[m_commentCount] = "BeadData";
                m_commentValues[m_commentCount] = beadName;
                m_commentCount++;
            }            
            if (m_waitWobj == true && Math.abs(m_inBoundOffset-m_lastInBoundOffset) > .001)
            {
            	m_commentNames[m_commentCount] = "ConveyorInitialPosition";
            	m_commentValues[m_commentCount] = String.valueOf(m_conveyorInitialPosition);
            	m_commentCount++;
            	m_commentNames[m_commentCount] = "InBoundOffset";
            	m_commentValues[m_commentCount] = String.valueOf(m_inBoundOffset/1000.0);
            	m_commentCount++;            	
            	m_lastInBoundOffset = m_inBoundOffset;
            }
            
            processComments(actElem, "Pre");
            m_currentElement = actElem;
            
            m_moveCounter++;

            return ecode;
        }
        // DLY End 2007/05/05
        
        private void processTimerStatement(Matcher match, Element activityListElem) {

            String timeInSec = match.group(2);

            createTimerStatement( timeInSec, activityListElem, true );
        }
       
        private void createTimerStatement( String timeInSec, Element activityListElem, boolean doComments )
        {
        
            //Create DOM Nodes and set appropriate attributes

            String delayActName = "RobotDelay." + String.valueOf(m_delayCounter);               
            if (doComments == false)
            {
                delayActName = "WeldTime";
            }
 
            Element actElem = super.createDelayActivity(activityListElem, delayActName, Double.valueOf("0.0"), Double.valueOf(timeInSec));
            if (doComments == true) {
                processComments( actElem, "Pre");
                m_currentElement = actElem;
            }
            m_delayCounter++;
        }
 
        private boolean processCallStatement( String progName, Element activityListElem, String robotProgramName, String mountedGunName)
        {
            boolean macroFound = false;
            boolean progNameProcessed = false;
            progName = progName.trim();
            if (progName != null) {
              String macroAction = (String)(m_macroCommands.get( progName ));
              if (macroAction != null)
              {
                    m_originalMacro = progName + ";";
                    processMacroStatement( progName, macroAction, activityListElem, mountedGunName );

                    macroFound = true;
                    progNameProcessed = true;
                }
            }
            if (macroFound == false) {
                String procNumber = (String)m_procNameList.get(progName);
				String localPrefix = "";
				if (procNumber == null)
                {
					procNumber = (String)m_procNameList.get("LOCAL_" + progName);
					if (procNumber != null)
              	   {
						localPrefix = "LOCAL_";
              	   }
              	}                                            	   
                if (procNumber != null) {
                    String callActName = "RobotCall." + String.valueOf(m_callCounter);
                    String callTask =  localPrefix + progName;
                    if (m_moduleCall.startsWith(localPrefix+progName+"#"))
                    {
                        callTask = m_moduleCall;	
                    }
                    Element actElem = super.createCallTaskActivity( activityListElem, callActName, callTask);
                    processComments( actElem, "Pre");
                    m_currentElement = actElem;
                    m_callCounter++;
            
                    progNameProcessed = true;
                    m_callTaskInBoundOffset.put(callTask,String.valueOf(m_inBoundOffset));
                }
            }
            return(progNameProcessed);
        }
        
        private void processCallStatement( String progName, Element activityListElem)
        {
            progName = progName.trim();

            String procNumber = (String)m_procNameList.get(progName);
			String localPrefix = "";
			if (procNumber == null)
			{
				procNumber = (String)m_procNameList.get("LOCAL_" + progName);
				if (procNumber != null)
				{
					localPrefix = "LOCAL_";
				}
			}
            if (procNumber != null) {
                String callActName = "RobotCall." + String.valueOf(m_callCounter);
                String callTask =  localPrefix + progName;
                if (m_moduleCall.startsWith(localPrefix+progName+"#"))
                {
                    callTask = m_moduleCall;	
                }
                
                Element actElem = super.createCallTaskActivity( activityListElem, callActName, callTask);
                processComments( actElem, "Pre");
                m_currentElement = actElem;
                m_callCounter++;
            }
        }

        private int processTarget(Element robotMotionElem, Matcher match, boolean explicit, int circular) {
            int ecode = 0;
            //Parse the string into array
            String [] components;
            String tagcomponent = null;
            String movecomponent = null;
            movecomponent = match.group(1);
            Integer targetType = Integer.getInteger("0");
            Integer targetNumber = Integer.getInteger("0");
            String  targetDeclType = "PERS";
            String Position = "-1";
            String explicitStr = "1";
            String [] targetValues = {"0.0", "0.0", "0.0",  "0.0", "0.0", "0.0", "0.0",  "0", "0", "0", "0",
                                      "9E+09", "9E+09", "9E+09", "9E+09", "9E+09", "9E+09"};

            if (explicit == true) {
                int rbtgtAuxStart = 11;
                if (movecomponent.equalsIgnoreCase("MoveAbsJ") ) {
                    for (int ii=3;ii<15;ii++) {
                        targetValues[ii-3] = match.group(ii);
                    }
                    targetType = new Integer(JOINT);
                    explicitStr = String.valueOf(m_explicitCount);
                    m_explicitCount++;
                    rbtgtAuxStart = 6;
                }
                else {
 
                    int valueOffset = 0;
                    if (circular == 2) 
                        valueOffset = 17;
                    if (movecomponent.equalsIgnoreCase("SpotL") || movecomponent.equalsIgnoreCase("SpotJ") || movecomponent.equalsIgnoreCase("SpotML") || movecomponent.equalsIgnoreCase("SpotMJ") || movecomponent.equalsIgnoreCase("StudL") || movecomponent.equalsIgnoreCase("PaintL")) {
                         for (int ii=2+valueOffset;ii<19+valueOffset;ii++) {
                            targetValues[ii-2-valueOffset] = match.group(ii);
                        }                       
                    }
                    else {
                        for (int ii=3+valueOffset;ii<20+valueOffset;ii++) {
                            targetValues[ii-3-valueOffset] = match.group(ii);
                        }
                    }
                    targetType = new Integer(CARTESIAN);
                    tagcomponent = "abb_explicit_" + String.valueOf(m_explicitCount);
                    explicitStr = String.valueOf(m_explicitCount);
                    m_explicitCount++;
                    
                }

               // DLY Start 2008/06/11 -- offset aux. axes groups (i.e. Workgroup 2;3) not working for explicit targets
                String [] extended_components;
                extended_components = new String[6];

                int work_defined = 0;
                int ext_defined = 0;
                int aux_defined = 0;
                m_extAxesCount = 0;
                m_auxAxesCount = 0;
                m_workAxesCount = 0;

                for (int ii=rbtgtAuxStart; ii<rbtgtAuxStart+6; ii++) {
                    int aux_offset = aux_defined+ext_defined+work_defined;
                    String axesValue = targetValues[ii];
                    double dAxesValue = Double.valueOf(axesValue).doubleValue();
                    // for linetracking last value is always the magical eax_f track offset
                    String eax_fStr = targetValues[rbtgtAuxStart+5];
                    double eax_f = 0.0;
                    if (eax_fStr != null && !eax_fStr.equals(""))
                    {
                        eax_f = Double.valueOf(eax_fStr).doubleValue();
                        if (eax_f < 9E+08)
                        {
                        	extended_components[5] = eax_fStr;
                        }
                    }
                    if (axesValue != null && dAxesValue < 9E+08)
                    {
                        String auxAxesType = (String)m_auxAxesTypes.get(String.valueOf(ii+1-rbtgtAuxStart));
                        if (auxAxesType != null) {
                            if (auxAxesType.equalsIgnoreCase("RailTrackGantry")) {
                                extended_components[aux_offset] = axesValue;
                                aux_defined++;
                                m_auxAxesCount++;
                                if (m_auxAxesStart < 0)
                                    m_auxAxesStart = aux_offset + 1;                           
                            }
                            else if ( auxAxesType.equalsIgnoreCase("EndOfArmTooling")) {
                                extended_components[aux_offset] = axesValue;
                                ext_defined++;
                                m_extAxesCount++;
                                if (m_extAxesStart < 0)
                                    m_extAxesStart = aux_offset + 1;                            
                            }
                            else if ( auxAxesType.equalsIgnoreCase("WorkPositioner")) {
                                extended_components[aux_offset] = axesValue;
                                work_defined++;
                                m_workAxesCount++;
                                if (m_workAxesStart < 0)
                                    m_workAxesStart = aux_offset + 1;                            
                            }
                        }
                        else {
                            extended_components[aux_offset] = axesValue;
                            ext_defined++;
                            m_extAxesCount++;
                            if (m_extAxesStart < 0)
                                m_extAxesStart = aux_offset + 1;
                        }
                    }
                }

                Position = String.valueOf(m_targetNumber);

                if (ext_defined > 0) {
                    //Add the target to a dynamic array
	            	ResizeArrayList(m_listOfExtTargets, m_targetNumber);                	
                    m_listOfExtTargets.add(m_targetNumber, extended_components);
                    //Resize the array
                    m_listOfExtTargets.trimToSize();
                }

                if (aux_defined > 0) {
                    //Add the target to a dynamic array
	            	ResizeArrayList(m_listOfAuxTargets, m_targetNumber);                	
                    m_listOfAuxTargets.add(m_targetNumber, extended_components);
                    //Resize the array
                    m_listOfAuxTargets.trimToSize();
                }

                if (work_defined > 0) { 
                    //Add the target to a dynamic array
	            	ResizeArrayList(m_listOfWorkTargets, m_targetNumber);                	
                    m_listOfWorkTargets.add(m_targetNumber, extended_components);
                    //Resize the array
                    m_listOfWorkTargets.trimToSize();
                }

                m_targetNumber++;
                /******** the old way does not work for Workgroup 2;3
                int aux_defined = 0;
                int ext_defined = 0;
                int work_defined = 0;
                for (int ii=rbtgtAuxStart; ii<rbtgtAuxStart+6; ii++) {
                    int aux_offset = aux_defined+ext_defined+work_defined;
                    String axesValue = targetValues[ii];
                    double dAxesValue = Double.valueOf(axesValue).doubleValue();
                    if (axesValue != null && dAxesValue < 9E+08)
                    {
                        String auxAxesType = (String)m_auxAxesTypes.get(String.valueOf(ii+1-rbtgtAuxStart));
                        if (auxAxesType != null) {
                                if (auxAxesType.equalsIgnoreCase("RailTrackGantry")) {
                                        // extended_components[aux_offset] = axesValue;
                                        aux_defined++;
                                        m_auxAxesCount++;
                                        if (m_auxAxesStart < 0)
                                                m_auxAxesStart = aux_offset + 1;                           
                                }
                                else if ( auxAxesType.equalsIgnoreCase("EndOfArmTooling")) {
                                        // extended_components[aux_offset] = axesValue;
                                        ext_defined++;
                                        m_extAxesCount++;
                                        if (m_extAxesStart < 0)
                                                m_extAxesStart = aux_offset + 1;                            
                                }
                                else if ( auxAxesType.equalsIgnoreCase("WorkPositioner")) {
                                        // extended_components[aux_offset] = axesValue;
                                        work_defined++;
                                        m_workAxesCount++;
                                        if (m_workAxesStart < 0)
                                                m_workAxesStart = aux_offset + 1;                            
                                }
                        }
                        else {
                        // extended_components[aux_offset] = axesValue;
                        ext_defined++;
                        m_extAxesCount++;
                        if (m_extAxesStart < 0)
                                        m_extAxesStart = aux_offset + 1;
                        }
                    } 
                } // for loop
                ************/

                // DLY End 2008/06/11
                
                m_commentNames[m_commentCount] = "TargetDeclarationType";
                m_commentValues[m_commentCount] = "abb_explicit";
                m_commentCount++;
                m_commentNames[m_commentCount] = "PosReg";
                m_commentValues[m_commentCount] = explicitStr;
                m_commentCount++;                
            }
            else {
                
                if (movecomponent.equalsIgnoreCase("SpotL") || movecomponent.equalsIgnoreCase("SpotJ") || movecomponent.equalsIgnoreCase("SpotML") || movecomponent.equalsIgnoreCase("SpotMJ"))
                    tagcomponent = match.group(2);
                else if (movecomponent.equalsIgnoreCase("ArcLStart") || movecomponent.equalsIgnoreCase("ArcLEnd"))
                    tagcomponent = match.group(2);
                else if (movecomponent.equalsIgnoreCase("ArcCStart") || movecomponent.equalsIgnoreCase("ArcCEnd"))
				{
					if (circular == 2)
					{
						tagcomponent = match.group(3 + circular);
					}
					else
					{
						tagcomponent = match.group(1 + circular);
					}
				}
                else if (movecomponent.equalsIgnoreCase("StudL"))
                    tagcomponent = match.group(2);
                else if (movecomponent.equalsIgnoreCase("PaintL"))
                    tagcomponent = match.group(2);
				else if (movecomponent.equalsIgnoreCase("ArcL"))
                    tagcomponent = match.group(3);
                else if (movecomponent.equalsIgnoreCase("ArcC") || movecomponent.equalsIgnoreCase("MoveC") || movecomponent.equalsIgnoreCase("DispC")) 
					tagcomponent = match.group(2 + circular);
                else
                    tagcomponent = match.group(3);
                
                //Get the target type
                targetType = (Integer) m_targetTypes.get(tagcomponent);
                if (targetType == null)
                {
                    if (m_listOfUndefTargetNames.contains(tagcomponent)==false)
                    {
                        ecode = ecode_TargetUndefined;
                        m_listOfUndefTargetNames.add(m_listOfUndefTargetNames.size(), tagcomponent);
                    }
                    if (movecomponent.equalsIgnoreCase("MoveAbsJ"))
                    {
                        targetType = new Integer(JOINT);
                    }
                    else
                        targetType = new Integer(CARTESIAN);
                }
                targetNumber = (Integer) m_targetNumbers.get(tagcomponent);
                if (targetNumber == null)
                {
                    if (m_listOfUndefTargetNames.contains(tagcomponent)==false)
                    {
                        ecode = ecode_TargetUndefined;
                        m_listOfUndefTargetNames.add(m_listOfUndefTargetNames.size(), tagcomponent);
                    }
                }
                else
                {
                    Position = targetNumber.toString();
                    targetValues = (String []) m_listOfRobotTargets.get(targetNumber.intValue());
                }
                targetDeclType = (String) m_targetDeclType.get(tagcomponent);
                if (targetDeclType == null)
                {
                    if (m_listOfUndefTargetNames.contains(tagcomponent)==false)
                    {
                        ecode = ecode_TargetUndefined;
                        m_listOfUndefTargetNames.add(m_listOfUndefTargetNames.size(), tagcomponent);
                    }
                    // no type since we don't want this to get downloaded again
                    targetDeclType = "";
                }
                m_commentNames[m_commentCount] = "TargetDeclarationType";
                m_commentValues[m_commentCount] = targetDeclType.trim();
                m_commentCount++;
            }

             boolean noJointTarget = false;    
             
             //Process the joint target
             if(targetType.equals(new Integer(JOINT))) {
                 Element targetElem = super.createTarget( robotMotionElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.JOINT, false); 
                 m_createJointTarget = true;
                 ArrayList [] jointTargetArrayList = new ArrayList [ m_numRobotAxes+m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
                //Print out the target (joint) values
                for(int ii = 0; ii < 6; ii++)
                    formatJointValueInPulses( jointTargetArrayList, targetValues[ii] , "Joint", null, ii);

                 //If there are auxiliary axes present, print out their joint values
                if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {
                     if(m_numAuxAxes > 0 && m_auxAxesCount == m_numAuxAxes)
                        processExtendedTarget(jointTargetArrayList, Position, m_listOfAuxTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes, targetValues, explicit, false);
                     if(m_numExtAxes > 0 && m_extAxesCount == m_numExtAxes)
                        processExtendedTarget(jointTargetArrayList, Position, m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING , m_numExtAxes, targetValues, explicit, false);
                     if(m_numWorkAxes > 0 && m_workAxesCount == m_numWorkAxes)
                        processExtendedTarget(jointTargetArrayList, Position, m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes, targetValues, explicit, false);
                     if(m_numAuxAxes > 0 && m_auxAxesCount != m_numAuxAxes)
                         noJointTarget = true;
                     if(m_numExtAxes > 0 && m_extAxesCount != m_numExtAxes)
                       noJointTarget = true;
                     if(m_numWorkAxes > 0 && m_workAxesCount != m_numWorkAxes)
                       noJointTarget = true;
                }
                if (noJointTarget == false)
                    super.createJointTarget(targetElem, jointTargetArrayList);
                if (explicit == false) {
                    m_commentNames[m_commentCount] = "Tag Name";
                    m_commentValues[m_commentCount] = tagcomponent;
                    m_commentCount++;
                }
             }
             else if(targetType.equals(new Integer(CARTESIAN))) {
                Element targetElem = super.createTarget( robotMotionElem, DNBIgpOlpUploadEnumeratedTypes.TargetType.CARTESIAN, false);
                m_createJointTarget = false;
                ArrayList [] jointTargetArrayList = new ArrayList [ m_numAuxAxes+m_numExtAxes+m_numWorkAxes];
                
                double [] cartValues = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                cartValues[0] = Double.valueOf(targetValues[0]).doubleValue() * 0.001;
                cartValues[1] = Double.valueOf(targetValues[1]).doubleValue() * 0.001;
                cartValues[2] = Double.valueOf(targetValues[2]).doubleValue() * 0.001;
                double [] qq = {0.0, 0.0, 0.0, 0.0};
                qq[0] = Double.valueOf(targetValues[3]).doubleValue();
                qq[1] = Double.valueOf(targetValues[4]).doubleValue();
                qq[2] = Double.valueOf(targetValues[5]).doubleValue();
                qq[3] = Double.valueOf(targetValues[6]).doubleValue();
                
                quaternionToYPR( qq );
                
                cartValues[3] = m_targetYaw;
                cartValues[4] = m_targetPitch;
                cartValues[5] = m_targetRoll;

 
                String cfgName = "Config_1";
                
                if (targetValues[10].equals("0"))
                {
                    cfgName = "Config_1";
                }
                if (targetValues[10].equals("1"))
                {
                    cfgName = "Config_2";
                }
                if (targetValues[10].equals("2"))
                {
                    cfgName = "Config_3";
                }
                if (targetValues[10].equals("3"))
                {
                    cfgName = "Config_4";
                }
                if (targetValues[10].equals("6"))
                {
                    cfgName = "Config_5";
                }
                if (targetValues[10].equals("7"))
                {
                    cfgName = "Config_6";
                }
                if (targetValues[10].equals("4"))
                {
                    cfgName = "Config_7";
                }
                if (targetValues[10].equals("5"))
                {
                    cfgName = "Config_8";
                }                

                Element cartTargetElem = super.createCartesianTarget( targetElem, cartValues, cfgName);

                if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0) {                        
                    if(m_numAuxAxes > 0 && m_numAuxAxes == m_auxAxesCount )
                        processExtendedTarget(jointTargetArrayList, Position,  m_listOfAuxTargets,  DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.RAIL_TRACK_GANTRY, m_numAuxAxes, targetValues, explicit, true);
                    if(m_numExtAxes > 0 && m_numExtAxes == m_extAxesCount )
                        processExtendedTarget(jointTargetArrayList, Position,  m_listOfExtTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING, m_numExtAxes, targetValues, explicit, true);
                    if(m_numWorkAxes > 0 && m_numWorkAxes == m_workAxesCount )
                        processExtendedTarget(jointTargetArrayList, Position,  m_listOfWorkTargets, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.WORKPIECE_POSITIONER, m_numWorkAxes, targetValues, explicit, true);
                     if(m_numAuxAxes > 0 && m_auxAxesCount != m_numAuxAxes)
                         noJointTarget = true;
                     if(m_numExtAxes > 0 && m_extAxesCount != m_numExtAxes)
                       noJointTarget = true;
                     if(m_numWorkAxes > 0 && m_workAxesCount != m_numWorkAxes)
                       noJointTarget = true;
                    if (noJointTarget == false)
                        super.createJointTarget( targetElem, jointTargetArrayList);
                }//end if(m_numAuxAxes > 0 || m_numExtAxes > 0 || m_numWorkAxes > 0)
 
                int [] turnNumbers = {0,0,0,0};
                
                turnNumbers[0] = getTurnValue(Integer.valueOf(targetValues[7]).intValue());
                turnNumbers[1] = getTurnValue(Integer.valueOf(targetValues[8]).intValue());
                turnNumbers[3] = getTurnValue(Integer.valueOf(targetValues[9]).intValue());
                
                super.createTurnNumbers( cartTargetElem, turnNumbers);
                super.createTag( cartTargetElem, tagcomponent);
                processAttribute( robotMotionElem, "Rapid cf1", targetValues[7]);
                processAttribute( robotMotionElem, "Rapid cf4", targetValues[8]);
                processAttribute( robotMotionElem, "Rapid cf6", targetValues[9]);
                processAttribute( robotMotionElem, "Rapid cfx", targetValues[10]);
            }//end else if

            return ecode;
        }//end processTarget method

        private int getTurnValue(int rpdCfg) {
                int turn = 0;
                switch(rpdCfg)
                {
                    case 0:
                    case 1:
                    case -1:
                    case -2: 
                        turn = 0;
                    break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        turn = 1;
                    break;
                    case -3:
                    case -4:
                    case -5:
                    case -6:
                        turn = -1;
                }
                return(turn);
        }
        private void processExtendedTarget(ArrayList [] jointTargetArrayList, String sExtendedTarget, ArrayList listOfExtendedTargets,  DNBIgpOlpUploadEnumeratedTypes.AuxAxisType extendedAxisType, int len, String [] targetValues, boolean explicit, boolean cartesian) {
            
            Integer extendedTargetNum = new Integer(sExtendedTarget);
            String [] extendedValues = {"9E+09", "9E+09", "9E+09", "9E+09", "9E+09", "9E+09"};
            
            int start = 1;
            
            String extendedAxisStrType = extendedAxisType.toString();
            // DLY Start 2008/06/11 use same method for both explicit and non-explicit
            /**** old way explicit handled differently than non-explicit
            if (explicit == true) {
                int offset = 6;
                if (cartesian == true)
                    offset = 11;
                for (int ii=0;ii<6;ii++) {
                    extendedValues[ii] = targetValues[ii+offset];
                }
            }
             ******/
            // DLY End 2008/06/11
            if (extendedTargetNum.intValue() != -1) {
                extendedValues = (String []) listOfExtendedTargets.get(extendedTargetNum.intValue());
            }
            
            if (extendedAxisStrType.equalsIgnoreCase("RailTrackGantry")) {
                start = m_auxAxesStart;
                if (m_waitWobj == true && m_objectFrameMove == true)
                {
                    double eax_f = Double.valueOf(extendedValues[5]).doubleValue();
                    if (eax_f < 9E+08)
                    {
                    	// Just like D5 when linetracking is used the eax_f value is used to adjust the eax_a value during upload
                    	// NOTE that for download the eax_f value is always 0.0
                    	// The m_wobjOffset was created to allow the user to make an additional adjustment to the eax_a value
                        double eax_a = Double.valueOf(extendedValues[start]).doubleValue();
                        if (eax_a < 9E+08)
                        {
                        	eax_a += (m_wobjOffset-eax_f);
                        	extendedValues[start] = String.valueOf(eax_a);
                        }
                    }
                }
            }
            else if (extendedAxisStrType.equalsIgnoreCase("EndOfArmTooling")) {
                start = m_extAxesStart;
            }
            else {
                start = m_workAxesStart;
            }

            for( int ii = start-1; ii < start + len - 1; ii++) {
                    formatJointValueInPulses(jointTargetArrayList, extendedValues[ii], "AuxJoint", extendedAxisType, ii);
            }//end for
            
            if (extendedAxisType == DNBIgpOlpUploadEnumeratedTypes.AuxAxisType.END_OF_ARM_TOOLING)
            {
                boolean tempjointtargetValue = m_createJointTarget;
                m_createJointTarget = false;
                m_EndOfArmToolValues = new ArrayList [ len ];
                for( int ii = 0; ii < len; ii++) {
                        formatJointValueInPulses(m_EndOfArmToolValues, extendedValues[ii+start-1], "Joint", extendedAxisType, ii);
                        m_EndOfArmToolValues[ii].remove(4);
                        m_EndOfArmToolValues[ii].set(2, Integer.valueOf(String.valueOf(ii+1)));
                }//end for                
                m_createJointTarget = tempjointtargetValue;
            }
        }       

        private void processDOUTStatement(Matcher match, Element activityListElem, boolean setDO) throws IllegalStateException{
 
            String signalName = match.group(1);
            String portNumber = "";
            if (signalName != null)
            {
                portNumber = (String)m_portNumberNames.get( signalName );
                if (portNumber == null)
                {
                   portNumber = Integer.toString(m_SignalCount++); 
                   m_portNumberNames.put( signalName, portNumber);
                }
                String status = "on";
                if (setDO == true)
                {
                    status = match.group(2);
                }
                String  ioname = "";
                String  time = "0";

                // System.out.println("DOUT matched" + ", " + portNumber + ", " + status);

                boolean signalValue = false;

                if (status != null)
                {
                    if (status.equalsIgnoreCase("on") || status.equals("1")) 
                        signalValue = true;
                    else
                        signalValue = false;

                        ioname = "Set-" + signalName + " = " + status;

                    Element actElem = super.createSetIOActivity(activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(time));
                    processComments(actElem, "Pre");
                    m_currentElement = actElem;
                }
            }
        }

        private void processPULSEStatement(Matcher match, Element activityListElem) throws IllegalStateException{
        
            String signalName = match.group(3);
            String portNumber = "";

            if (signalName != null)
            {
                portNumber = (String)m_portNumberNames.get( signalName );
                if (portNumber == null)
                {
                   portNumber = Integer.toString(m_SignalCount++); 
                   m_portNumberNames.put( signalName, portNumber); 
                }
                String status = "on";
                String  ioname;
                String time = match.group(2);

                if (time == null)
                    time = ".2";

                // System.out.println("DOUT matched" + ", " + portNumber + ", " + status);

                boolean signalValue = false;

                if (status != null)
                {
                    if (status.equalsIgnoreCase("on") || status.equals("1")) 
                        signalValue = true;
                    else
                        signalValue = false;

                    if(!time.equals("0"))
                        ioname = "Set-" + signalName + " = " + status + " [" + time + "s]";
                    else
                        ioname = "Set-" + signalName + " = " + status;

                    Element actElem = super.createSetIOActivity(activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(time));
                    processComments(actElem, "Pre");
                    m_currentElement = actElem;
                }
            }
        }        

        private void processWAITStatement(Matcher match, String line, Element activityListElem) {

            String signalName = match.group(1);
            
            if (signalName != null)
            {
                String portNumber = "";
                portNumber = (String)m_portNumberNames.get( signalName );
                if (portNumber == null)
                {
                   portNumber = Integer.toString(m_SignalCount++); 
                   m_portNumberNames.put( signalName, portNumber);
                }

                String status = match.group(2);
                String maxTime = match.group(4);

                if (maxTime == null || maxTime.equals(""))
                    maxTime = "0.0";

                boolean signalValue = false;

                if (status != null)
                {
                    if (status.equalsIgnoreCase("on") || status.equals("1"))
                        signalValue = true;
                    else
                        signalValue = false;

                    // System.out.println("WAIT matched" + ", " + portNumber + ", " + status);

                    String ioname = "Wait Until " + signalName + " = " + status;

                    if (m_commentWaitSignal == 0)
                    {                    
                        Element actElem = super.createWaitForIOActivity( activityListElem, ioname, signalValue, signalName, Integer.valueOf(portNumber), Double.valueOf(maxTime)); 
                        processComments(actElem, "Pre");
                        m_currentElement = actElem;
                    }
                    else
                    {
                            line = line.trim();
                            if (m_OLPStyle.equals("OperationComments") || m_OLPStyle.equals("Honda"))
                                addToComments(activityListElem, line, false, true);
                            else
                                addToComments(activityListElem, line, true, true);                        
                    }
                    

                }
            }
        }

        private void processSpotStatement(Matcher match, Element activityListElem, String gunName, boolean explicit ) {

            int group_offset = 0;
            if (explicit == true)
                group_offset = 16;

            String spotType = match.group(1);
            String spotdata = "";
            String inposStr = "";
            String noconcStr = "";
            String concStr = "";
            String retractStr = "";
            String gundata = "";
            String openHLift = "";
            String closeHLift = "";
            String quickRelease = "";
            String multiSpot1Name = "";
            String multiSpot2Name = "";
            String multiSpot3Name = "";
            String multiSpot4Name = "";
            
            if (spotType.equalsIgnoreCase("SpotJ") || spotType.equalsIgnoreCase("SpotL"))
            {
                if (m_versionNumber > 1.0)
                {
                    // SpotL t1,v500,gun1,spot1 \\Inpos\\Conc
                    gundata = match.group(6+group_offset);
                    spotdata = match.group(10+group_offset);
                    inposStr = match.group(11+group_offset);
                    concStr = match.group(12+group_offset);
                    openHLift = match.group(13+group_offset);
                    closeHLift = match.group(14+group_offset);
                    quickRelease = match.group(15+group_offset);
                }
                else
                {
                    // SpotL t1,v500,spot1 \\Inpos\\NoConc\\Retract, gun1
                    spotdata = match.group(6+group_offset);
                    inposStr = match.group(7+group_offset);
                    noconcStr = match.group(8+group_offset);
                    retractStr = match.group(9+group_offset);
                    gundata = match.group(10+group_offset);                                   
                }                
            }
            else if (spotType.equalsIgnoreCase("SpotMJ") || spotType.equalsIgnoreCase("SpotML"))
            {
                // SpotML t1,v500 \G1:=spot1 \G2:=spot2 \Inpos \Conc \OpenHLift \CloseHLift
                multiSpot1Name = match.group(7+group_offset);
                multiSpot2Name = match.group(9+group_offset);
                multiSpot3Name = match.group(11+group_offset);
                multiSpot4Name = match.group(13+group_offset);
                inposStr = match.group(14+group_offset);
                concStr = match.group(15+group_offset);
                openHLift = match.group(16+group_offset);
                closeHLift = match.group(17+group_offset);
                quickRelease = match.group(18+group_offset);
            }
 
            //Create DOM Nodes and set appropriate attributes
            String spotActionName = "RAPIDSpotWeld." + String.valueOf(m_spotCounter);
            Element actionElem = super.createActionHeader( activityListElem, spotActionName, "RAPIDSpotWeld", gunName); 

            m_commentNames[m_commentCount] = "spotdata";
            // following done to allow C5 format which has gun first to be handled without a parameter for the Version
            if ( ((m_listOfSpotData.contains(gundata) == true || m_listOfC5SpotData.contains(gundata) == true) && m_listOfGunData.contains(spotdata) == true))
            {
                m_commentValues[m_commentCount] = gundata;
            }
            else
            {
                m_commentValues[m_commentCount] = spotdata;
            }
            m_commentCount++;
            if (inposStr != null && !inposStr.equals("")) {
                m_commentNames[m_commentCount] = "InPos";
                m_commentValues[m_commentCount] = "1";
                m_commentCount++;
            }
            if (noconcStr != null && !noconcStr.equals("")) {
                m_commentNames[m_commentCount] = "NoConc";
                m_commentValues[m_commentCount] = "1";                                       
                m_commentCount++;
            }
            if (concStr != null && !concStr.equals("")) {
                m_commentNames[m_commentCount] = "Conc";
                m_commentValues[m_commentCount] = "1";                                       
                m_commentCount++;
            }
            if (openHLift != null && !openHLift.equals("")) {
                m_commentNames[m_commentCount] = "OpenHLift";
                m_commentValues[m_commentCount] = "1";
                m_commentCount++;
            }
            if (closeHLift != null && !closeHLift.equals("")) {
                m_commentNames[m_commentCount] = "CloseHLift";
                m_commentValues[m_commentCount] = "1";
                m_commentCount++;
            }            

            if (quickRelease != null && !quickRelease.equals("")) {
                m_commentNames[m_commentCount] = "QuickRelease";
                m_commentValues[m_commentCount] = "1";
                m_commentCount++;
            }            
            
            m_commentNames[m_commentCount] = "gundata";
           // following done to allow C5 format which has gun first to be handled without a parameter for the Version
            if ( ((m_listOfSpotData.contains(gundata) == true || m_listOfC5SpotData.contains(gundata) == true) && m_listOfGunData.contains(spotdata) == true))
            {
                m_commentValues[m_commentCount] = spotdata;
            }
            else
            {
                m_commentValues[m_commentCount] = gundata;
            }
            m_commentCount++;

            processComments(actionElem, "Pre");
            m_currentElement = actionElem;

            String weldHomeName = (String)m_weldScheduleHomes.get( spotdata );
            String backupHomeName = (String)m_backupHomes.get( gundata );
            String weldScheduleDelay = (String)m_weldScheduleDelays.get( spotdata );

            String delayTime = ".5";
            if (weldScheduleDelay != null)
                delayTime = weldScheduleDelay;
                
            /* weld home move */
            if (weldHomeName == null)
            weldHomeName = (String)m_listofHomeNames.get( 0 );

            Element spotActivityListElem = super.createActivityListWithinAction( actionElem );

            processSpotHomeMove( weldHomeName, spotActivityListElem, "CloseGun" );

            /* delay */
            createTimerStatement( delayTime, spotActivityListElem, false );

            /* open/backup home move */
            if (backupHomeName == null) {
                if (m_listofHomeNames.size() > 1) {
                backupHomeName = (String)m_listofHomeNames.get( 1 );
                }
                else{
                    backupHomeName = "Home_2";
                }
            }
            if (m_numExtAxes > 0)
            {
                processSpotHomeMove( m_EndOfArmToolValues, spotActivityListElem, "OpenGun" );
            }
            else
            {
                processSpotHomeMove( backupHomeName, spotActivityListElem, "OpenGun" );                
            }
            if ((retractStr != null && !retractStr.equals("")) )
            {
                processBackupStatement( activityListElem, gunName, "Retract", null);                
            }
            else if( (openHLift != null && !openHLift.equals("")) )
            {
                processBackupStatement( activityListElem, gunName, "OpenHLift", null);                
            }                
            else if( (closeHLift != null && !closeHLift.equals("")) ) 
            {
                processBackupStatement( activityListElem, gunName, "CloseHLift", null);                
            }

            m_spotCounter++;
        }

        private void processBackupStatement(Element activityListElem, String gunName, String gundata, Element actionActivityList ) {

            Element backupActivityListElem = actionActivityList;
            
            // System.out.println("LOOKING FOR HOME NAME " + gundata);
	    String backupHomeName = (String)m_backupHomes.get( gundata );
           
            if (backupActivityListElem == null)
            {
                //Create DOM Nodes and set appropriate attributes
            	String backupActionName = "RAPIDSpotRetract." + String.valueOf(m_backupCounter);
            	Element actionElem = super.createActionHeader( activityListElem, backupActionName, "RAPIDSpotRetract", gunName); 

            	backupActivityListElem = super.createActivityListWithinAction( actionElem );
            }
            else
            {
                // System.out.println("MUST BE FROM A MACRO");
            }

            /* open/backup home move */
            if (backupHomeName == null) {
                // System.out.println("BACKUP HOME NAME NULL...USING LIST OF HOME NAMES");
                if (m_listofHomeNames.size() > 1) {
                    backupHomeName = (String)m_listofHomeNames.get( 1 );
                }
                else{
                    backupHomeName = "Home_2";
                }
            }
            if (actionActivityList != null)
            {
                processSpotHomeMove( backupHomeName, actionActivityList, "RetractGun" );
            }
            else
            {
                processSpotHomeMove( backupHomeName, backupActivityListElem, "RetractGun" );
            }
            m_backupCounter++;
        }

    private void processGunBackupStatement(Matcher match, Element activityListElem, String gunName)
    {
        String backupActionName = "RAPIDSpotRetract." + m_backupCounter;
        Element actionElem = super.createActionHeader( activityListElem, backupActionName, "RAPIDSpotRetract", gunName);
        Element backupActivityListElem = super.createActivityListWithinAction( actionElem );

        String backupHomeName = "Open";
        String gunCommand = match.group(1);
        if (gunCommand.equalsIgnoreCase("GunOpen") ||
            gunCommand.equalsIgnoreCase("GunSemiOpen") ||
            gunCommand.equalsIgnoreCase("GunClose"))
            backupHomeName = gunCommand.substring(3);

        processSpotHomeMove(backupHomeName, backupActivityListElem, "RetractGun");

        String gunExec, gunCheck, toolName;
        if (gunCommand.equalsIgnoreCase("GunClose"))
        {
            gunExec = "";
            gunCheck = match.group(2);
            toolName = match.group(3);
        }
        else
        {
            gunExec = match.group(2);
            gunCheck = match.group(3);
            toolName = match.group(4);
        }
        m_commentNames[m_commentCount] = "GunExec";
        m_commentValues[m_commentCount] = gunExec;
        m_commentCount++;
        m_commentNames[m_commentCount] = "GunCheck";
        m_commentValues[m_commentCount] = gunCheck;
        m_commentCount++;
        m_commentNames[m_commentCount] = "Toolname";
        m_commentValues[m_commentCount] = toolName;
        m_commentCount++;
        processComments(actionElem, "Pre");

        m_backupCounter++;
    }

     private void processMacroStatement(String macroStr, String macroAction, Element activityListElem, String gunName ) {
            Element zoneAct = null;
            Element actElem = null;
            Element actInActionElem = null;
            Element actionActivityList = null;
            Element toolResource = null;
            String callName = "";

            int index = 0;
            index = macroAction.indexOf('.');

            if (index >= 0) {
                 String macroActionStore = new String("");
                 macroActionStore = macroAction;
                 callName = gunName = macroAction.substring(index + 1, macroActionStore.length());
                 macroAction = macroActionStore.substring(0, index);
            }

            if (macroAction.length() >= 20 && macroAction.substring(0,20).equalsIgnoreCase("DNBEnterZoneActivity")) {
                //Create DOM Nodes and set appropriate attributes
                String activityName = "DNBEnterZoneActivity." + String.valueOf(m_enterZoneCounter);
                actElem = super.createZoneActivity( activityListElem, activityName, DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType.ENTER_ZONE, macroStr);


                m_enterZoneCounter++;
            }
            else if (macroAction.length() >= 20 && macroAction.substring(0,20).equalsIgnoreCase("DNBClearZoneActivity")) {
                String activityName = "DNBClearZoneActivity." + String.valueOf(m_clearZoneCounter);
                //Create DOM Nodes and set appropriate attributes
                actElem = super.createZoneActivity( activityListElem, activityName, DNBIgpOlpUploadEnumeratedTypes.ZoneActivityType.CLEAR_ZONE, macroStr);
                m_clearZoneCounter++;
            }
            else if (macroAction.length() >= 12 && macroAction.substring(0,12).equalsIgnoreCase("CallActivity")) {
                 if (callName.equals(""))
                 {
                     callName = macroStr;
                 }
                 processCallStatement( callName, activityListElem);
            }             
            else
            {
                //Create DOM Nodes and set appropriate attributes

                String activityName = macroAction.trim() + "." + String.valueOf(m_macroCounter);

                actElem = super.createActionHeader( activityListElem, activityName, macroAction.trim(), gunName);
                actionActivityList = super.createActivityListWithinAction( actElem);
            }


            if (macroAction.length() >= 13 && macroAction.substring(0,13).equalsIgnoreCase("RAPIDSpotPick")) {

                 if (m_pickHome.length() > 0) {
                    processSpotHomeMove( m_pickHome, actionActivityList, "CloseGripper");
                 }

               //Create DOM Nodes and set appropriate attributes

                String grabActName = "GrabActivity."  + String.valueOf(m_grabCounter);

                actInActionElem = super.createGrabActivity( actionActivityList, grabActName, m_grabPart, m_partGrabbed);
                m_grabCounter++;

            }
            else if (macroAction.length() >= 13 && macroAction.substring(0,13).equalsIgnoreCase("RAPIDSpotDrop")) {

                  if (m_dropHome.length() > 0) {
                    processSpotHomeMove( m_dropHome, actionActivityList, "OpenGripper");
                 }

                 String releaseActName = "ReleaseActivity."  + String.valueOf(m_releaseCounter);

                 actInActionElem = super.createReleaseActivity( actionActivityList, releaseActName, m_partGrabbed);

                m_releaseCounter++;
            }
            else if (macroAction.length() >= 13 && macroAction.substring(0,13).equalsIgnoreCase("RAPIDToolPick")) {

               //Create DOM Nodes and set appropriate attributes

                String mountActivityName = "DNBIgpMountActivity."  + String.valueOf(m_mountCounter);
                String localMountedToolName = new String(m_mountedToolName);
                if (localMountedToolName.equals(""))
                    localMountedToolName = gunName;

                
                String mountToolProfileName = localMountedToolName + ".ToolFrame";

                for(int jj = 0; jj < m_toolNumberMapping.size(); jj++) {
                    String currentToolName = (String)m_toolNumberMapping.get(String.valueOf(jj));
                    String compName = localMountedToolName;
                    int dotindex = 0;
                    dotindex = localMountedToolName.indexOf('.');

                    if (dotindex >= 0) {
                        compName = localMountedToolName.substring(0, dotindex);                   
                    }
                    if (currentToolName.startsWith(compName))
                    {
                        mountToolProfileName = currentToolName;
                        break;
                    }
                }

                actInActionElem = super.createMountActivity( actionActivityList, mountActivityName, localMountedToolName, mountToolProfileName);
                m_mountCounter++;    
            }
            else if (macroAction.length() >= 13 && macroAction.substring(0,13).equalsIgnoreCase("RAPIDToolDrop")) {
                //Create DOM Nodes and set appropriate attributes

                String unmountActivityName = "DNBIgpUnMountActivity."  + String.valueOf(m_unmountCounter);

                String localMountedToolName = new String(m_mountedToolName);
                if (localMountedToolName.equals(""))
                    localMountedToolName = gunName;

                actInActionElem = super.createUnMountActivity( actionActivityList, unmountActivityName, localMountedToolName, m_dropToolProfileName);
            }
            else if (macroAction.length() >= 16 && macroAction.substring(0,16).equalsIgnoreCase("RAPIDSpotRetract")) {
                 processBackupStatement( activityListElem, gunName, macroStr, actionActivityList);
            } 

            m_commentNames[m_commentCount] = "Macro";
            m_commentValues[m_commentCount] = m_originalMacro;
            m_commentCount++;

            processComments(actElem, "Pre");
            m_currentElement = actElem;

            m_macroCounter++;  
     }    
    

        private void processSpotHomeMove(String homeName, Element actListElem, String activityName ) {
                        
            Element actElem;
            actElem = super.createMotionActivityWithinAction( actListElem, activityName, Double.valueOf("0.0"), Double.valueOf("0.0"), DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION,  homeName);  
        }
       
        private void processSpotHomeMove(ArrayList [] oaJointTarget, Element actListElem, String activityName ) {
                        
            Element actElem;
            actElem = super.createMotionActivityWithinAction( actListElem, activityName, Double.valueOf("0.0"), Double.valueOf("0.0"), DNBIgpOlpUploadEnumeratedTypes.MotionType.JOINT_MOTION, oaJointTarget);
        }
        
       private void processAttribute(Element actElem, String attribNameValue, String attribVal ) {
            String [] attribNames = new String [1];
            String [] attribValues = new String [1];
            attribNames[0] = attribNameValue;
            attribValues[0] = attribVal;
            super.createAttributeList(actElem, attribNames, attribValues);
       }        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                     UTILITY FUNCTIONS
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////
       
       public void formatJointValueInPulses(ArrayList [] jointTargetArrayList, String sTarget, String robotOrAuxJoint, DNBIgpOlpUploadEnumeratedTypes.AuxAxisType auxJointType, int ii) {
           
           double targetScale = 1.0;
           String jointName = "";
           String axesType = "";
           int index = ii;
           int arrayIndex = ii;

            if (auxJointType != null)
                index = ii+m_numRobotAxes;
                
            if (m_createJointTarget == true)
                arrayIndex = index;
           
            axesType = m_axisTypes[index];
            if (axesType.equalsIgnoreCase("Translational") )
                targetScale = .001;   
            jointName = "Joint" + String.valueOf(index+1);
            
            // System.out.println("JOINT TARGET ARRAY INDEX: " + String.valueOf(arrayIndex) + " TYPE: " + auxJointType + "\n");
            if(auxJointType != null) {
                jointTargetArrayList[arrayIndex] = new ArrayList(5);
            }
            else {
                jointTargetArrayList[arrayIndex] = new ArrayList(4);
            }
            
            jointTargetArrayList[arrayIndex].add( 0, jointName);

            double targetValue = 0.0;
                    
            try {
                targetValue = Double.parseDouble(sTarget);
                targetValue = targetValue*targetScale;
            }
                    
            catch(NumberFormatException e) {
                e.getMessage();
                targetValue = Double.NaN;
            }
             
            if (axesType.equalsIgnoreCase("Rotational"))
                targetValue = Math.toRadians(targetValue);
            
            String scaledTarget = String.valueOf(targetValue);
             
            jointTargetArrayList[arrayIndex].add(1, Double.valueOf(scaledTarget)); 
            
            String dofNum = String.valueOf(index+1);
            
            jointTargetArrayList[arrayIndex].add(2, Integer.valueOf(dofNum));
            
            if (axesType.equalsIgnoreCase("Rotational") ) {
                jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.ROTATIONAL);
            }
            else {
                jointTargetArrayList[arrayIndex].add(3, DNBIgpOlpUploadEnumeratedTypes.DOFType.TRANSLATIONAL);
            }
       
            if (auxJointType != null)
                jointTargetArrayList[arrayIndex].add(4, auxJointType);

                        
        }
       
        public String formatDouble(String input, String axisType, int decCount) {
            String formattedValue = "";
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(decCount);
            
            if(axisType.equalsIgnoreCase("Translational"))
                formattedValue = nf.format(Double.parseDouble(input)*0.001);
            else if(axisType.equalsIgnoreCase("Rotational"))
                formattedValue = nf.format(Double.parseDouble(input));
            else
               formattedValue = "No_axis_type"; 
            
               return formattedValue;
        }



         public void quaternionToYPR(double [] qq ) {
               double [][] xform;
               xform = new double[4][4];
               
               xform[0][0] = 2.0*(qq[0]*qq[0]+qq[1]*qq[1]) - 1.0;
               xform[0][1] = 2.0*(qq[1]*qq[2]+qq[0]*qq[3]);
                xform[0][2] = 2.0*(qq[1]*qq[3]-qq[0]*qq[2]);
                xform[0][3] = 0.0;

                xform[1][0] = 2.0*(qq[1]*qq[2]-qq[0]*qq[3]);
                xform[1][1] = 2.0*(qq[0]*qq[0]+qq[2]*qq[2]) - 1.0;
                xform[1][2] = 2.0*(qq[2]*qq[3]+qq[0]*qq[1]);
                xform[1][3] = 0.0;

                xform[2][0] = 2.0*(qq[1]*qq[3]+qq[0]*qq[2]);
                xform[2][1] = 2.0*(qq[2]*qq[3]-qq[0]*qq[1]);
                xform[2][2] = 2.0*(qq[0]*qq[0]+qq[3]*qq[3]) - 1.0;
                xform[2][3] = 0.0;

                xform[3][0] = 0.0;
                xform[3][1] = 0.0;
                xform[3][2] = 0.0;
                xform[3][3] = 1.0;
                doYawPitchRoll(xform);
         }
         
         private  void doYawPitchRoll(double [][] dgMatrix) {
            double r1;
            double cc;
            double ss;
            double p1;
            double y1;
            double r2;
            double p2;
            double y2;
            
            if ( Math.abs(dgMatrix[0][0]) >= .0001 || Math.abs(dgMatrix[0][1]) > .0001 ) {
                /*
                 *  first solution
                 */
                r1 = Math.atan2( dgMatrix[0][1], dgMatrix[0][0] );
                
                cc = Math.cos( r1 );
                ss = Math.sin( r1 );
                
                p1 = Math.atan2( -dgMatrix[0][2], cc*dgMatrix[0][0] + ss*dgMatrix[0][1] );
                
                y1 = Math.atan2( ss*dgMatrix[2][0] - cc*dgMatrix[2][1],
                                 cc*dgMatrix[1][1] - ss*dgMatrix[1][0]);
                
                /*
                 *  second solution
                 */       
                r2 = Math.atan2( -dgMatrix[0][1], -dgMatrix[0][0] );
                
                cc = Math.cos( r2 );
                ss = Math.sin( r2 );
                
                p2 = Math.atan2( -dgMatrix[0][2], cc*dgMatrix[0][0] + ss*dgMatrix[0][1] );
                
                y2 = Math.atan2( ss*dgMatrix[2][0] - cc*dgMatrix[2][1],
                                 cc*dgMatrix[1][1] - ss*dgMatrix[1][0]);               
            }
            else
            {
                r1 = 0.0;
                
                p1 = Math.atan2( -dgMatrix[0][2], 0.0);
                
                y1 = Math.atan2( -dgMatrix[2][1], dgMatrix[1][1] );
                
                r2 = r1;
                p2 = p1;
                y2 = y1;
            }
            
            if ( (r1*r1 + p1*p1 + y1*y1 ) <=
                 (r2*r2 + p2*p2 + y2*y2 ) )
            {
                m_targetRoll = r1*180.0/Math.PI;
                m_targetPitch = p1*180.0/Math.PI;
                m_targetYaw = y1*180.0/Math.PI;
            }
            else
            {
                m_targetRoll = r2*180.0/Math.PI;
                m_targetPitch = p2*180.0/Math.PI;
                m_targetYaw = y2*180.0/Math.PI;                
            }
                
        }  
        private  void setCurrentTurn(int targetValue) {
        switch( targetValue )
       {
        case  0:
        case  1:
        case -1:
        case -2:
            m_targetTurnValue = 0;
            break;
        case  2:
        case  3:
        case  4:
        case  5:
            m_targetTurnValue = 1;
            break;
        case -3:
        case -4:
        case -5:
        case -6:
            m_targetTurnValue = -1;
            break;
       }
    }

        /********
     private int setupObjectFrameTrackOffset(Matcher match, boolean explicit, int circular) {
        int ecode = 0;
        String movecomponent;

        movecomponent = match.group(1);

 
        String tagcomponent = "";
        String objProfileName;
 

        
        int group_offset = 0;
        if (explicit == true)
        {
            if (circular > 0)
                group_offset = 32; 
            else
                group_offset = 16;

            if (movecomponent.equalsIgnoreCase("MoveAbsJ")) {
                group_offset = 11;
            }
        }
        else
        {
            if (circular >0)
                group_offset++;
        }
        if (movecomponent.equalsIgnoreCase("MoveAbsJ")) {
            group_offset++;                                    
        }        
        

        if (movecomponent.equalsIgnoreCase("SpotL") || movecomponent.equalsIgnoreCase("SpotJ"))
        {
            objProfileName = match.group(17+group_offset);
        }
        else if (movecomponent.equalsIgnoreCase("SpotML") || movecomponent.equalsIgnoreCase("SpotMJ"))
        {
            objProfileName = match.group(20+group_offset);
        }        
        else if (movecomponent.equalsIgnoreCase("StudL"))
        {
            objProfileName = match.group(10+group_offset);
        }
        else if (movecomponent.equalsIgnoreCase("PaintL"))
        {
            objProfileName = match.group(19+group_offset);
        }
        else if (movecomponent.equalsIgnoreCase("ArcL") || movecomponent.equalsIgnoreCase("ArcL1") || movecomponent.equalsIgnoreCase("ArcL2") || movecomponent.equalsIgnoreCase("ArcC"))
        {
            objProfileName = match.group(17+group_offset);                
        }
        else if (movecomponent.equalsIgnoreCase("ArcLStart") || movecomponent.equalsIgnoreCase("ArcLEnd") || movecomponent.equalsIgnoreCase("ArcCEnd") || movecomponent.equalsIgnoreCase("ArcCStart"))
        {
            objProfileName = match.group(16+group_offset);                
        }        
        else
        {
            objProfileName = match.group(14+group_offset);
        }

        if (objProfileName == null || objProfileName.equals(""))
            objProfileName = "wobj0";

        Integer targetType = new Integer(JOINT);
        String [] targetValues = {"0.0", "0.0", "0.0",  "0.0", "0.0", "0.0", "0.0",  "0", "0", "0", "0",
                                  "9E+09", "9E+09", "9E+09", "9E+09", "9E+09", "9E+09"};                                 
        
        if (explicit == true)
        {
                if (movecomponent.equalsIgnoreCase("MoveAbsJ") ) {
                    for (int ii=3;ii<15;ii++) {
                        targetValues[ii-3] = match.group(ii);
                    }
                }
                else {
                    int valueOffset = 0;

                    for (int ii=3+valueOffset;ii<20+valueOffset;ii++) {
                        targetValues[ii-3] = match.group(ii);
                    }
                    targetType = new Integer(CARTESIAN);
                }                     
        }
        else
        {
                if (movecomponent.equalsIgnoreCase("SpotL") )
                    tagcomponent = match.group(2);
                else if (movecomponent.equalsIgnoreCase("StudL"))
                    tagcomponent = match.group(2);
                else if (movecomponent.equalsIgnoreCase("PaintL"))
                    tagcomponent = match.group(2);
                else if (movecomponent.equalsIgnoreCase("ArcL") )
                    tagcomponent = match.group(3);
                else if (movecomponent.equalsIgnoreCase("ArcC") || movecomponent.equalsIgnoreCase("MoveC")) {
                    tagcomponent = match.group(2);
                }
                else
                    tagcomponent = match.group(3);
                
                if (!movecomponent.equalsIgnoreCase("MoveAbsJ"))
                    targetType = new Integer(CARTESIAN);
                
        }
 
        String offsetValue = (String)m_objectTrackOffsetMapping.get( objProfileName );

        if ( (offsetValue == null) && (targetType.equals(new Integer(CARTESIAN))) && (m_railAxesIndex >= 1) && (m_numAuxAxes >= m_railAxesIndex) )
        {
            if (explicit == true)
            {
                if ( targetValues[m_railAxesIndex+6].equals("9E+09") != true )
                {
                    m_objectTrackOffsetMapping.put(objProfileName, targetValues[m_railAxesIndex+6]);
                }
            }
            else
            {
                  Integer targetNumber = (Integer)m_targetNumbers.get(tagcomponent);
                  if (targetNumber != null && m_listOfAuxTargets.isEmpty() == false)
                  {
                      String [] extendedValues = {"9E+09", "9E+09", "9E+09", "9E+09", "9E+09", "9E+09"};
                      extendedValues = (String []) m_listOfAuxTargets.get(targetNumber.intValue());
                      if ( extendedValues[m_railAxesIndex-1].equals("9E+09") != true )
                      {
                          m_objectTrackOffsetMapping.put(objProfileName, extendedValues[m_railAxesIndex-1]);
                      }
                  }
            }
        }
        
        return ecode;
    }
         ************/
    
    void removeHashTableTypes(String isType, Hashtable iTable)
    {
        for (int ii=1;ii<iTable.size();ii++)
        {
      	  String currentType = (String)iTable.get(String.valueOf(ii));
      	  if (currentType != null && currentType.equals(isType))
      	  {
      		  iTable.remove(String.valueOf(ii));  
      	  }
        }
    }       
    void ResizeArrayList(ArrayList iArray, int iSize)
    {
    	for (int ii=iArray.size();ii<iSize;ii++)
    	{
    		iArray.add(ii, null);
    	}
    }
    
}//end class
