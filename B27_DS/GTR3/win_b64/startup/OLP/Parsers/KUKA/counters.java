package Kuka;

public class counters
{
    public int move, delay;
    public int spotWeld, spotRetract;
    public int grpOpen, grpClose, grpCheck;
    public int svgCouple, svgDecouple, svgInit, svgWeld, svgTipDress;
    public String grabbingObj, grabbedObj;

    public counters()
    {
        move=1;
        delay=1;
        spotWeld=1;
        spotRetract=1;
        grpOpen=1;
        grpClose=1;
        grpCheck=1;
        svgCouple=1;
        svgDecouple=1;
        svgInit=1;
        svgWeld=1;
        svgTipDress=1;
        
        grabbingObj = "unset";
        grabbedObj  = "unset";
    }
}
