package Kuka;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import Kuka.util;
import Kuka.counters;
/* setSpotData
 * createSpotRetractAction
 * createSpotWeldAction
 * convertBackupName
 * getHomeName
 * splitGunInfo
 */
public class spot
{
    private String name, gun, pair, retr, clo_tm, pgno1, pgno2, press1, press2;
    private String gunName, openHome, closeHome, weldHome;
    private util    kku;

    public spot()
    {
        name   = new String("SSDEFAULT");
        gun    = new String("1");
        pair   = new String("#FIRST");
        retr   = new String("#CLO");
        clo_tm = new String("SSDAT1.CLO_TM");
        pgno1  = new String("SSDAT1.PGNO1");
        pgno2  = new String("SSDEFAULT.PGNO2");
        press1 = new String("SSDAT1.PRESS1");
        press2 = new String("SSDEFAULT.PRESS2");

        gunName   = new String("Gun_Name");
        openHome  = new String("Home_3");
        closeHome = new String("Home_2");
        weldHome  = new String("Home_1");
        kku = new util();
    }

    /* S_ACT.* = *
     */
    public void setSpotData(String line)
    {
        int index = line.indexOf('=');
        String val = line.substring(index+1).trim();
        String [] component = line.split("[\\.=]");
        if (component[1].equalsIgnoreCase("GUN"))
            gun = val;
        if (component[1].equalsIgnoreCase("PAIR"))
            pair = val;
        if (component[1].equalsIgnoreCase("RETR"))
            retr = val;
        if (component[1].equalsIgnoreCase("CLO_TM"))
            clo_tm = val;
        if (component[1].equalsIgnoreCase("PGNO1"))
            pgno1 = val;
        if (component[1].equalsIgnoreCase("PRESS1"))
            press1 = val;
    }

    /* This method creates the Action element for Spot Weld Gun Retract
     */
    public void createSpotRetractAction( String gunInfo, counters kkc, Document kXmlDoc, Element actListElem )
    {
        splitGunInfo(gunInfo);

        Element actionElem = kXmlDoc.createElement("Action");
        actListElem.appendChild(actionElem);
        actionElem.setAttribute("ActionType", "KukaSpotRetract");

        Element actionName = kXmlDoc.createElement("ActionName");
        actionElem.appendChild(actionName);
        Text actionNameValue = kXmlDoc.createTextNode("KukaSpotRetract." + kkc.spotRetract++);
        actionName.appendChild(actionNameValue);

        Element toolResElem = kXmlDoc.createElement("ToolResource");
        actionElem.appendChild(toolResElem);

        toolResElem.setAttribute("ResourceType", "DNBBasicDevice3D");

        Element resNameElem = kXmlDoc.createElement("ResourceName");
        toolResElem.appendChild(resNameElem);
        Text resNameValue = kXmlDoc.createTextNode(gunName);
        resNameElem.appendChild(resNameValue);

        Element attributeListElem = kXmlDoc.createElement("AttributeList");
        actionElem.appendChild(attributeListElem);

        Element attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        Element attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        Text attribNameNode = kXmlDoc.createTextNode("Gun");
        attributeNameElem.appendChild(attribNameNode);

        Element attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        Text attribValNode = kXmlDoc.createTextNode(gun);
        attributeValueElem.appendChild(attribValNode);

        attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        attribNameNode = kXmlDoc.createTextNode("Retract");
        attributeNameElem.appendChild(attribNameNode);

        attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        attribValNode = kXmlDoc.createTextNode(convertBackupName(retr));
        attributeValueElem.appendChild(attribValNode);

        Element activityListElem = kXmlDoc.createElement("ActivityList");
        actionElem.appendChild(activityListElem);

        String actName = "Basic Motion Activity." + kkc.move++;
        Double startTime = new Double(0.0);
        Double endTime = new Double(1.0);
        kku.createActionMotionActivity(activityListElem, actName, startTime, endTime, getHomeName(retr), "", "PROMPT", "DNBRobotMotionActivity", kXmlDoc);
    } //end createSpotRetractAction

    /* This method creates the Action element for Spot Weld
     */
    public void createSpotWeldAction( String gunInfo, counters kkc, Document kXmlDoc, Element actListElem )
    {
        splitGunInfo(gunInfo);

        Element actionElem = kXmlDoc.createElement("Action");
        actListElem.appendChild(actionElem);
        actionElem.setAttribute("ActionType", "KukaSpotWeld");

        Element actionName = kXmlDoc.createElement("ActionName");
        actionElem.appendChild(actionName);
        Text actionNameValue = kXmlDoc.createTextNode("KukaSpotWeld." + kkc.spotWeld++);
        actionName.appendChild(actionNameValue);

        Element toolResElem = kXmlDoc.createElement("ToolResource");
        actionElem.appendChild(toolResElem);

        toolResElem.setAttribute("ResourceType", "DNBBasicDevice3D");

        Element resNameElem = kXmlDoc.createElement("ResourceName");
        toolResElem.appendChild(resNameElem);
        Text resNameValue = kXmlDoc.createTextNode(gunName);
        resNameElem.appendChild(resNameValue);

        Element attributeListElem = kXmlDoc.createElement("AttributeList");
        actionElem.appendChild(attributeListElem);

        Element attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        Element attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        Text attribNameNode = kXmlDoc.createTextNode("Gun");
        attributeNameElem.appendChild(attribNameNode);

        Element attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        Text attribValNode = kXmlDoc.createTextNode(gun);
        attributeValueElem.appendChild(attribValNode);

        attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        attribNameNode = kXmlDoc.createTextNode("Retract");
        attributeNameElem.appendChild(attribNameNode);

        attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        attribValNode = kXmlDoc.createTextNode(convertBackupName(retr));
        attributeValueElem.appendChild(attribValNode);

        attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        attribNameNode = kXmlDoc.createTextNode("Early Closing Time");
        attributeNameElem.appendChild(attribNameNode);

        attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        attribValNode = kXmlDoc.createTextNode(clo_tm);
        attributeValueElem.appendChild(attribValNode);

        attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        attribNameNode = kXmlDoc.createTextNode("Timer Program Number");
        attributeNameElem.appendChild(attribNameNode);

        attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        attribValNode = kXmlDoc.createTextNode(pgno1);
        attributeValueElem.appendChild(attribValNode);

        attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        attribNameNode = kXmlDoc.createTextNode("Pressure");
        attributeNameElem.appendChild(attribNameNode);

        attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        attribValNode = kXmlDoc.createTextNode(press1);
        attributeValueElem.appendChild(attribValNode);

        Element activityListElem = kXmlDoc.createElement("ActivityList");
        actionElem.appendChild(activityListElem);

        String actName = "Basic Motion Activity." + kkc.move++;
        Double startTime = new Double(0.0);
        Double endTime = new Double(1.0);
        kku.createActionMotionActivity(activityListElem, actName, startTime, endTime, weldHome, "", "PROMPT", "DNBRobotMotionActivity", kXmlDoc);

        actName = "DelayActivity." + kkc.delay++;
        endTime = new Double(1.5);
        kku.createDelayActivity(activityListElem, actName, startTime, endTime, "PROMPT", "DelayActivity", kXmlDoc);

        actName = "Basic Motion Activity." + kkc.move++;
        endTime = new Double(1.0);
        kku.createActionMotionActivity(activityListElem, actName, startTime, endTime, getHomeName(retr), "", "PROMPT", "DNBRobotMotionActivity", kXmlDoc);
    } //end createSpotWeldAction

    private String convertBackupName( String backupName )
    {
        if (backupName.equalsIgnoreCase("#OPN"))
            return "Open";
        if (backupName.equalsIgnoreCase("#CLO"))
            return "Close";
        return "Open";
    }

    private String getHomeName( String backupName )
    {
        if (backupName.equalsIgnoreCase("#OPN"))
            return openHome;
        if (backupName.equalsIgnoreCase("#CLO"))
            return closeHome;
        return openHome;
    }
    private void splitGunInfo( String gunInfo )
    {
        String [] gunNmOpCl = gunInfo.split(";");
        gunName   = gunNmOpCl[0];
        openHome  = gunNmOpCl[1];
        closeHome = gunNmOpCl[2];
        weldHome  = gunNmOpCl[3];
    }

    /* DECL SPOT_TYPE ...
     */
    public void spotDataDecl(String line)
    {
        String [] components = line.split(",");
        name   = components[0].split("[ =]")[2];
        clo_tm = components[3].split(" ")[1];
        pgno1  = components[4].split(" ")[1];
        press1 = components[5].split(" ")[1];
        return;
    }

    /* following setting spot data, go through action elements and modify
     * weld attributes
     */
    public void modifyWeldAttributes(Element resourceElem)
    {
        String sdatStr;
        Element actListElem, actionElem, attribListElem, attribValElem;
        NodeList attribValNodeList;

        actListElem = (Element)resourceElem.getFirstChild();
        while (actListElem != null)
        {
            if (actListElem.getTagName().equals("ActivityList"))
            {
                actionElem = (Element)actListElem.getFirstChild();
                while (actionElem != null)
                {
                    if (actionElem.getTagName().equals("Action") &&
                        actionElem.getAttribute("ActionType").equals("KukaSpotWeld"))
                    {
                        attribListElem = kku.findChildElemByName(actionElem, "AttributeList", null, null);
                        attribValNodeList = attribListElem.getElementsByTagName("AttributeValue");
                        for (int ii=0; ii<attribValNodeList.getLength(); ii++)
                        {
                            attribValElem = (Element)attribValNodeList.item(ii);
                            sdatStr = attribValElem.getFirstChild().getNodeValue();
                            if (sdatStr.equalsIgnoreCase(name+".CLO_TM"))
                                attribValElem.getFirstChild().setNodeValue(clo_tm);
                            if (sdatStr.equalsIgnoreCase(name+".PGNO1"))
                                attribValElem.getFirstChild().setNodeValue(pgno1);
                            if (sdatStr.equalsIgnoreCase(name+".PRESS1"))
                                attribValElem.getFirstChild().setNodeValue(press1);
                        }
                    }
                    actionElem = (Element)actionElem.getNextSibling();
                }
            }
            actListElem = (Element)actListElem.getNextSibling();
        }
    }
}//end class spot
