package Kuka;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/* findChildElemByName
 */
public class util {
    private String  railConfig = "None";
    private float zOffset = 0;
    private static final String railvar1 = "$ET1_TA1KR";
    private static final String railvar3 = "$ET1_TFLA3";
    
    /* This method searches through the child nodes of currentElem and returns the first element
     * of name 'name' with attribute 'attrib' whose value is 'attribVal'
     * it returns null if doesn't find it
     * Set attrib to null if name is only interested
     */
    public Element findChildElemByName(Element currentElem, String name, String attrib, String attribVal)
    {
        Element tmpElem;

        NodeList childNodes = currentElem.getChildNodes();
        for (int ii=0; ii<childNodes.getLength(); ii++)
        {
            tmpElem = (Element)childNodes.item(ii);
            if (tmpElem.getTagName().equals(name))
            {
                if (attrib == null)
                {
                    return (tmpElem);
                }
                else
                {
                    String [] a = attrib.split(";");
                    String [] av = attribVal.split(";");
                    int jj;
                    for (jj=0; jj<a.length; jj++)
                    {
                        if (tmpElem.getAttribute(a[jj]).equals(av[jj]))
                            continue;
                        else
                            break;
                    }
                    if (jj==a.length)
                        return (tmpElem);
                }
            }
        }
        return (null);
    } // end findChildElemByName

    public boolean listHasProfile(Element listElem, String profile, String name)
    {
        Element profileElem, nameElem;
        NodeList profileNodes = listElem.getChildNodes();
        for (int ii=0; ii<profileNodes.getLength(); ii++)
        {
            profileElem = (Element)profileNodes.item(ii);
            if (profileElem.getTagName().equals(profile))
            {
                nameElem = (Element)profileElem.getFirstChild();
                if (nameElem.getFirstChild().getNodeValue().equals(name))
                    return true;
            }
        }

        return false;
    }

    public String field(String line, int field)
    {
        String fieldStr = String.valueOf(field) + ":";
        String [] subStr = line.substring(line.indexOf("%P ")+3).split(",");
        for (int ii=0; ii<subStr.length; ii++)
        {
            if (subStr[ii].trim().startsWith(fieldStr))
                return subStr[ii].substring(subStr[ii].indexOf(':')+1);
        }

        return null;
    }

    public Element createDelayActivity(Element actList, String actName,
                                        Double startTime, Double endTime,
                                        String attrName, String attrVal,
                                        Document kXmlDoc)
    {
        Element actElem = kXmlDoc.createElement("Activity");
        actList.appendChild(actElem);
        actElem.setAttribute("ActivityType", "DelayActivity");

        Element actNameElem = kXmlDoc.createElement("ActivityName");
        actElem.appendChild(actNameElem);
        Text actNameValue = kXmlDoc.createTextNode(actName);
        actNameElem.appendChild(actNameValue);

        Element actTimeElem = kXmlDoc.createElement("ActivityTime");
        actElem.appendChild(actTimeElem);

        Element startTimeElem = kXmlDoc.createElement("StartTime");
        actTimeElem.appendChild(startTimeElem);
        Text startTimeValue = kXmlDoc.createTextNode(startTime.toString());
        startTimeElem.appendChild(startTimeValue);

        Element endTimeElem = kXmlDoc.createElement("EndTime");
        actTimeElem.appendChild(endTimeElem);
        Text endTimeValue = kXmlDoc.createTextNode(endTime.toString());
        endTimeElem.appendChild(endTimeValue);

        if (attrName!=null && attrVal!=null)
        {
            Element attrListElem = kXmlDoc.createElement("AttributeList");
            actElem.appendChild(attrListElem);

            createAttribute(attrName, attrVal, attrListElem, kXmlDoc);
        }

        return actElem;
    }

    public void createActionMotionActivity(Element actList, String actName,
                                        Double startTime, Double endTime,
                                        String homeName, String jType,
                                        String attrName, String attrVal,
                                        Document kXmlDoc)
    {
        Element activityElem = kXmlDoc.createElement("Activity");
        actList.appendChild(activityElem);
        activityElem.setAttribute("ActivityType", "DNBRobotMotionActivity");

        Element activityName = kXmlDoc.createElement("ActivityName");
        activityElem.appendChild(activityName);
        Text activityNameValue = kXmlDoc.createTextNode(actName);
        activityName.appendChild(activityNameValue);

        if (endTime.doubleValue() > 0)
        {
            Element actTimeElem = kXmlDoc.createElement("ActivityTime");
            activityElem.appendChild(actTimeElem);

            Element startTimeElem = kXmlDoc.createElement("StartTime");
            actTimeElem.appendChild(startTimeElem);
            Text startTimeValue = kXmlDoc.createTextNode(startTime.toString());
            startTimeElem.appendChild(startTimeValue);

            Element endTimeElem = kXmlDoc.createElement("EndTime");
            actTimeElem.appendChild(endTimeElem);
            Text endTimeValue = kXmlDoc.createTextNode(endTime.toString());
            endTimeElem.appendChild(endTimeValue);
        }

        Element moAttribElem = kXmlDoc.createElement("MotionAttributes");
        activityElem.appendChild(moAttribElem);
        Element moTypeElem = kXmlDoc.createElement("MotionType");
        moAttribElem.appendChild(moTypeElem);
        Text moTypeValue = kXmlDoc.createTextNode("Joint");
        moTypeElem.appendChild(moTypeValue);

        Element targetElem = kXmlDoc.createElement("Target");
        activityElem.appendChild(targetElem);
        targetElem.setAttribute("Default", "Joint");

        Element jointTargetElem = kXmlDoc.createElement("JointTarget");
        targetElem.appendChild(jointTargetElem);

        try
        {
            double jointVal = Double.parseDouble(homeName);
            Text jvt;

            Element j = kXmlDoc.createElement("Joint");
            jointTargetElem.appendChild(j);
            j.setAttribute("DOFNumber", "1");
            j.setAttribute("JointName", "Joint 1");
            j.setAttribute("JointType", jType);
            if (jType.equals("Translational"))
            {
                j.setAttribute("Units", "m");
                jvt = kXmlDoc.createTextNode(String.valueOf(jointVal/1000.0));
            }
            else
            {
                j.setAttribute("Units", "deg");
                jvt = kXmlDoc.createTextNode(homeName);
            }
            Element jv = kXmlDoc.createElement("JointValue");
            j.appendChild(jv);
            jv.appendChild(jvt);
        }
        catch(NumberFormatException e)
        {
            Element homeNameElem = kXmlDoc.createElement("HomeName");
            jointTargetElem.appendChild(homeNameElem);

            Text homeNameValue = kXmlDoc.createTextNode(homeName);
            homeNameElem.appendChild(homeNameValue);
        }

        if (attrName!=null && attrVal!=null)
        {
            Element attrListElem = kXmlDoc.createElement("AttributeList");
            activityElem.appendChild(attrListElem);

            createAttribute(attrName, attrVal, attrListElem, kXmlDoc);
        }
    }

    public void createGrabActivity(Element actList, String actName,
                                        String attrName, String attrVal,
                                        counters kkc, Document kXmlDoc)
    {
        Element activityElem = kXmlDoc.createElement("Activity");
        actList.appendChild(activityElem);
        activityElem.setAttribute("ActivityType", "GrabActivity");

        Element activityName = kXmlDoc.createElement("ActivityName");
        activityElem.appendChild(activityName);
        Text activityNameValue = kXmlDoc.createTextNode(actName);
        activityName.appendChild(activityNameValue);

        Element objElem, objList;
        Text    objValue;
        objElem = kXmlDoc.createElement("GrabbingObject");
        activityElem.appendChild(objElem);
        objValue = kXmlDoc.createTextNode(kkc.grabbingObj);
        objElem.appendChild(objValue);

        objList = kXmlDoc.createElement("GrabbedObjectList");
        activityElem.appendChild(objList);

        objElem = kXmlDoc.createElement("GrabbedObject");
        objList.appendChild(objElem);
        objValue = kXmlDoc.createTextNode(kkc.grabbedObj);
        objElem.appendChild(objValue);

        if (attrName!=null && attrVal!=null)
        {
            Element attrListElem = kXmlDoc.createElement("AttributeList");
            activityElem.appendChild(attrListElem);

            createAttribute(attrName, attrVal, attrListElem, kXmlDoc);
        }
    } // end createGrabActivity

    public void createReleaseActivity(Element actList, String actName,
                                        String attrName, String attrVal,
                                        counters kkc, Document kXmlDoc)
    {
        Element activityElem = kXmlDoc.createElement("Activity");
        actList.appendChild(activityElem);
        activityElem.setAttribute("ActivityType", "ReleaseActivity");

        Element activityName = kXmlDoc.createElement("ActivityName");
        activityElem.appendChild(activityName);
        Text activityNameValue = kXmlDoc.createTextNode(actName);
        activityName.appendChild(activityNameValue);

        Element objElem, objList;
        Text    objValue;
        objList = kXmlDoc.createElement("ReleasedObjectList");
        activityElem.appendChild(objList);

        objElem = kXmlDoc.createElement("ReleasedObject");
        objList.appendChild(objElem);
        objValue = kXmlDoc.createTextNode(kkc.grabbedObj);
        objElem.appendChild(objValue);

        if (attrName!=null && attrVal!=null)
        {
            Element attrListElem = kXmlDoc.createElement("AttributeList");
            activityElem.appendChild(attrListElem);

            createAttribute(attrName, attrVal, attrListElem, kXmlDoc);
        }
    } // end createReleaseActivity

    public void createAttribute(String attrName, String attrVal, Element attrListElem, Document kXmlDoc)
    {
        Element attrElem = kXmlDoc.createElement("Attribute");
        attrListElem.appendChild(attrElem);

        Element attrNameElem = kXmlDoc.createElement("AttributeName");
        attrElem.appendChild(attrNameElem);

        Text attrNameNode = kXmlDoc.createTextNode(attrName);
        attrNameElem.appendChild(attrNameNode);

        Element attrValueElem = kXmlDoc.createElement("AttributeValue");
        attrElem.appendChild(attrValueElem);

        Text attrValNode = kXmlDoc.createTextNode(attrVal);
        attrValueElem.appendChild(attrValNode);
    }

    public void readMachineDat(String filePath)
    {
        try
        {
            BufferedReader br= new BufferedReader(new FileReader(filePath));

            String [] components = new String[3];
            String valStr;
            int qstart, qend;
            int b1=0, c1=0, b3=0, c3=0;
            boolean railvar1Found = false;
            boolean railvar3Found = false;
            
            String line = br.readLine();
            while (line != null)
            {
                line = line.trim();
                if (line.equals(""))
                {
                    line = br.readLine();
                    continue;
                }

                if (line.indexOf(railvar1) > 0)
                {
                    components = line.split("[{}]", 3);

                    qstart = components[1].indexOf('Z')+1;
                    qend   = components[1].indexOf(',', qstart);
                    zOffset = Float.parseFloat(components[1].substring(qstart, qend).trim());
                    
                    qstart = components[1].indexOf('B')+1;
                    qend   = components[1].indexOf(',', qstart);
                    valStr = components[1].substring(qstart, qend).trim();
                    b1 = new Float(valStr).intValue();
                    
                    qstart = components[1].indexOf('C')+1;
                    valStr = components[1].substring(qstart).trim();
                    c1 = new Float(valStr).intValue();
                    
                    railvar1Found = true;
                }
                
                if (line.indexOf(railvar3) > 0)
                {
                    components = line.split("[{}]", 3);
                    
                    qstart = components[1].indexOf('B')+1;
                    qend   = components[1].indexOf(',', qstart);
                    valStr = components[1].substring(qstart, qend).trim();
                    b3 = new Float(valStr).intValue();
                    
                    qstart = components[1].indexOf('C')+1;
                    valStr = components[1].substring(qstart).trim();
                    c3 = new Float(valStr).intValue();
                    
                    railvar3Found = true;
                }
                
                if (railvar1Found && railvar3Found)
                {
                    if (b1 == 90 && c1 == 0 && b3 == -90 && c3 == 0)
                    {
                        railConfig = "A";
                        return;
                    }
                    else
                    if (b1 == -90 && c1 == 0 && b3 == 90 && c3 == 0)
                    {
                        railConfig = "C";
                        return;
                    }
                    else
                    if (b1 == 0 && c1 == -90 && b3 == 0 && c3 == 90)
                    {
                        railConfig = "B";
                        return;
                    }
                    else
                    if (b1 == 0 && c1 == 90 && b3 == 0 && c3 == -90)
                    {
                        railConfig = "D";
                        return;
                    }
                    else
                    if (b1 == 0 && c1 == -90 && b3 == -45 && c3 == 90)
                    {
                        railConfig = "H";
                        return;
                    }
                    else
                    if (b1 == 0 && c1 == -90 && b3 == 45 && c3 == 90)
                    {
                        railConfig = "I";
                        return;
                    }
                    else
                    if (b1 == 0 && c1 == 90 && b3 == 45 && c3 == -90)
                    {
                        railConfig = "J";
                        return;
                    }
                    else
                    if (b1 == 0 && c1 == 90 && b3 == -45 && c3 == -90)
                    {
                        railConfig = "K";
                        return;
                    }
                    else
                        return;
                }
                
                line = br.readLine();
            } // while

        } // try
        catch (IOException e)
        {
            return;
        }
        
        return;
    } // end readMachineDat

    public String getRailConfig()
    {
        return railConfig;
    }
    
    public float getZOffset()
    {
        return zOffset;
    }
}
