package Kuka;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import Kuka.util;

public class gripper
{
    private String  [] grpInfo;
    private String  [] attrValues = {"1", "0", "0", "0", "0", "0", "0", "0", "0"};
    private String  currGDAT = "GDAT1";
    private util    kku;

    public gripper(String gripperInfo)
    {
        grpInfo = gripperInfo.split(";");
        // grpInfo[0]: gripper name
        // grpInfo[1]: open
        // grpInfo[2]: close
        // grpInfo[3]: mid

        kku = new util();
    }

    public Element processFoldGripper(String line, counters kkc, Document kXmlDoc, Element actListElem)
    {
        Element actionElem = null;

        int ii = line.indexOf("%MKUKATPGRP");
        String grpfields = line.substring(ii);
        String [] grpop = grpfields.split(",", 4);
        if (grpop[1].equalsIgnoreCase("%CGRP"))
            actionElem = setGrp(grpfields, kkc, kXmlDoc, actListElem);
        if (grpop[1].equalsIgnoreCase("%CCHK_GRP"))
            actionElem = chkGrp(grpfields, kkc, kXmlDoc, actListElem);

        return actionElem;
    }

    private Element chkGrp(String grpfields, counters kkc, Document kXmlDoc, Element actListElem)
    {
        Element actionElem = kXmlDoc.createElement("Action");
        actListElem.appendChild(actionElem);
        actionElem.setAttribute("ActionType", "KukaGripperCheck");

        Element actionNameElem = kXmlDoc.createElement("ActionName");
        actionElem.appendChild(actionNameElem);
        String actName = "KukaGripperCheck." + kkc.grpCheck++;
        Text actionNameValue = kXmlDoc.createTextNode(actName);
        actionNameElem.appendChild(actionNameValue);

        Element toolResElem = kXmlDoc.createElement("ToolResource");
        actionElem.appendChild(toolResElem);

        Element resNameElem = kXmlDoc.createElement("ResourceName");
        toolResElem.appendChild(resNameElem);
        Text resNameValue = kXmlDoc.createTextNode(grpInfo[0]);
        resNameElem.appendChild(resNameValue);

        String [] attrNames = {"GripperNumber", "GripperState", "StartEnd", "Delay"};
        for (int ii=0; ii<4; ii++)
            attrValues[ii] = kku.field(grpfields, 2*(ii+1));

        grpAttributes(attrNames, attrValues, kXmlDoc, actionElem);

        Element actionActList = kXmlDoc.createElement("ActivityList");
        actionElem.appendChild(actionActList);

        actName = "Basic Motion Activity." + kkc.move++;
        String stateName = grpInfo[Integer.valueOf(attrValues[1]).intValue()];
        Double oTime = new Double(-1.0);
        kku.createActionMotionActivity(actionActList, actName, oTime, oTime, stateName, "", "PROMPT", "DNBRobotMotionActivity", kXmlDoc);

        return actionElem;
    }

    private Element setGrp(String grpfields, counters kkc, Document kXmlDoc, Element actListElem)
    {
        String actName;
        String [] attrNames  = {"GripperNumber", "StartEnd", "Delay", "G_APO", "CTRL", "DLY", "DST", "WaitTime", "Cont"};
        attrValues[0] = kku.field(grpfields, 2); // gripper number
        attrValues[1] = kku.field(grpfields, 8); // @ start or end
        attrValues[2] = kku.field(grpfields, 10); // delay

        if (kku.field(grpfields, 5).equalsIgnoreCase("#YES"))
            attrValues[8] = "1";
        else
            attrValues[8] = "0";

        // set G_APO to gdat number for later search and modify
        attrValues[3] = kku.field(grpfields, 6);

        String grpState, stateName, actionType;
        grpState = kku.field(grpfields, 4);
        stateName = grpInfo[Integer.valueOf(grpState).intValue()];
        if (grpState.equals("2"))
        {
            actionType = "KukaGripperClose";
            actName = actionType + "." + kkc.grpClose;
        }
        else
        {
            actionType = "KukaGripperOpen"; // mid position
            actName = actionType + "." + kkc.grpOpen;
        }

        Element actionElem = kXmlDoc.createElement("Action");
        actListElem.appendChild(actionElem);
        actionElem.setAttribute("ActionType", actionType);

        Element actionNameElem = kXmlDoc.createElement("ActionName");
        actionElem.appendChild(actionNameElem);
        Text actionNameValue = kXmlDoc.createTextNode(actName);
        actionNameElem.appendChild(actionNameValue);

        Element toolResElem = kXmlDoc.createElement("ToolResource");
        actionElem.appendChild(toolResElem);

        Element resNameElem = kXmlDoc.createElement("ResourceName");
        toolResElem.appendChild(resNameElem);
        Text resNameValue = kXmlDoc.createTextNode(grpInfo[0]);
        resNameElem.appendChild(resNameValue);

        grpAttributes(attrNames, attrValues, kXmlDoc, actionElem);

        Element actionActList = kXmlDoc.createElement("ActivityList");
        actionElem.appendChild(actionActList);

        actName = "Basic Motion Activity." + kkc.move++;
        Double oTime = new Double(-1.0);
        kku.createActionMotionActivity(actionActList, actName, oTime, oTime, stateName, "", "PROMPT", "DNBRobotMotionActivity", kXmlDoc);

        if (grpState.equals("2"))
        {
            actName = "GrabActivity." + kkc.grpClose++;
            kku.createGrabActivity(actionActList, actName, "PROMPT", "GrabActivity", kkc, kXmlDoc);
        }
        else
        {
            actName = "ReleaseActivity." + kkc.grpOpen++;
            kku.createReleaseActivity(actionActList, actName, "PROMPT", "ReleaseActivity", kkc, kXmlDoc);
        }

        return actionElem;
    }

    private void grpAttributes(String [] attrN, String [] attrV, Document kXmlDoc, Element parent)
    {
        Element attributeListElem = kXmlDoc.createElement("AttributeList");
        parent.appendChild(attributeListElem);

        Element attributeElem, attributeNameElem, attributeValueElem;
        Text attribNameNode, attribValNode;

        for (int ii=0; ii<attrN.length; ii++)
        {
            attributeElem = kXmlDoc.createElement("Attribute");
            attributeListElem.appendChild(attributeElem);

            attributeNameElem = kXmlDoc.createElement("AttributeName");
            attributeElem.appendChild(attributeNameElem);

            attribNameNode = kXmlDoc.createTextNode(attrN[ii]);
            attributeNameElem.appendChild(attribNameNode);

            attributeValueElem = kXmlDoc.createElement("AttributeValue");
            attributeElem.appendChild(attributeValueElem);

            attribValNode = kXmlDoc.createTextNode(attrV[ii]);
            attributeValueElem.appendChild(attribValNode);
        }
    }

    /* DECL GRP_TYP ...
     */
    public void grpDataDecl(String line)
    {
        String tmpStr;
        String [] components = line.split("[{,}]");
        for (int ii=0; ii<components.length; ii++)
            components[ii] = components[ii].trim();

        currGDAT = components[0].split("[ =]")[2].substring(1);

        tmpStr = components[1].split(" ")[1]; // G_APO
        if (tmpStr.equalsIgnoreCase("#YES"))
            attrValues[3] = "1";
        else
            attrValues[3] = "0";

        tmpStr = components[3].split(" ")[1]; // CTRL
        if (tmpStr.equalsIgnoreCase("#ON"))
            attrValues[4] = "1";
        else
            attrValues[4] = "0";

        attrValues[5] = components[4].split(" ")[1]; // DLY
        attrValues[6] = components[5].split(" ")[1]; // DST
        attrValues[7] = components[2].split(" ")[1]; // WaitTime

        return;
    }

    /* following setting gripper data, go through action elements and modify gripper attributes
     */
    public void modifyGrpAttributes(Element resourceElem)
    {
        String gdatStr;
        Element actListElem, actionElem, attrListElem, attrElem, attrNameElem, attrValElem;
        NodeList attrNodeList;

        actListElem = (Element)resourceElem.getFirstChild();
        while (actListElem != null)
        {
            if (actListElem.getTagName().equals("ActivityList"))
            {
                actionElem = (Element)actListElem.getFirstChild();
                while (actionElem != null)
                {
                    if (actionElem.getTagName().equals("Action") &&
                       (actionElem.getAttribute("ActionType").equals("KukaGripperOpen") ||
                        actionElem.getAttribute("ActionType").equals("KukaGripperClose")))
                    {
                        attrListElem = kku.findChildElemByName(actionElem, "AttributeList", null, null);
                        attrNodeList = attrListElem.getElementsByTagName("Attribute");
                        for (int ii=0; ii<attrNodeList.getLength(); ii++)
                        {
                            attrElem = (Element)attrNodeList.item(ii);
                            attrNameElem = kku.findChildElemByName(attrElem, "AttributeName", null, null);
                            gdatStr = attrNameElem.getFirstChild().getNodeValue();
                            if (gdatStr.equalsIgnoreCase("G_APO"))
                            {
                                attrValElem = kku.findChildElemByName(attrElem, "AttributeValue", null, null);
                                gdatStr = attrValElem.getFirstChild().getNodeValue();
                                if (gdatStr.equalsIgnoreCase(currGDAT))
                                    doModGrpAttr(attrNodeList);
                                else
                                    break;
                            }
                        }
                    }
                    actionElem = (Element)actionElem.getNextSibling();
                }
            }
            actListElem = (Element)actListElem.getNextSibling();
        }
    }

    private void doModGrpAttr(NodeList attrNodeList)
    {
        String attrName;
        Element attrElem, attrNameElem, attrValElem;

        for (int ii=0; ii<attrNodeList.getLength(); ii++)
        {
            attrElem = (Element)attrNodeList.item(ii);
            attrNameElem = kku.findChildElemByName(attrElem, "AttributeName", null, null);
            attrValElem  = kku.findChildElemByName(attrElem, "AttributeValue", null, null);
            attrName  = attrNameElem.getFirstChild().getNodeValue();
            if (attrName.equalsIgnoreCase("G_APO"))
                attrValElem.getFirstChild().setNodeValue(attrValues[3]);
            if (attrName.equalsIgnoreCase("CTRL"))
                attrValElem.getFirstChild().setNodeValue(attrValues[4]);
            if (attrName.equalsIgnoreCase("DLY"))
                attrValElem.getFirstChild().setNodeValue(attrValues[5]);
            if (attrName.equalsIgnoreCase("DST"))
                attrValElem.getFirstChild().setNodeValue(attrValues[6]);
            if (attrName.equalsIgnoreCase("WaitTime"))
                attrValElem.getFirstChild().setNodeValue(attrValues[7]);
        }
    }
}
