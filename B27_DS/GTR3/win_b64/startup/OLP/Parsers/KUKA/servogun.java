package Kuka;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import Kuka.util;

public class servogun
{
    private String  gun, gunJointType;
    private String  gunName, openHome, closeHome, weldHome;
    private util    kku;
    private boolean svgNeg;

    public servogun(String gunInfo)
    {
        String [] gunNmOpCl = gunInfo.split(";");
        gunName   = gunNmOpCl[0];
        openHome  = gunNmOpCl[1];
        closeHome = gunNmOpCl[2];
        weldHome  = gunNmOpCl[3];
        gunJointType  = gunNmOpCl[4];
        if (gunNmOpCl[5].equals("true"))
            svgNeg = true;
        else
            svgNeg = false;

        gun = new String("1");

        kku = new util();
    }

    public Element processFoldServoGun(String line, counters kkc, Document kXmlDoc, Element actListElem)
    {
        Element actionElem = null;

        int ii = line.indexOf("%MKUKATPSERVOTECH");
        String svgfields = line.substring(ii);
        String [] svgop = svgfields.split(",", 4);
        if (svgop[1].equalsIgnoreCase("%CGun"))
            actionElem = coupleDecouple(svgfields, kkc, kXmlDoc, actListElem);
        if (svgop[1].equalsIgnoreCase("%CInitGun"))
            actionElem = init(svgfields, kkc, kXmlDoc, actListElem);
        if (svgop[1].equalsIgnoreCase("%CSpotP") ||
            svgop[1].equalsIgnoreCase("%CTippDress"))
            actionElem = spotTipDress(svgfields, kkc, kXmlDoc, actListElem);

        return actionElem;
    }

    public String motion(String line)
    {
        String moveType, tagName, profileName=null;
        int ii = line.indexOf("MKUKATPSERVOTECH");
        String svgfields = line.substring(ii);
        String [] svgop = svgfields.split(",", 4);

        if (svgop[1].equalsIgnoreCase("%CSpotP"))
        {
            moveType = kku.field(svgfields, 1);
            tagName = "X" + kku.field(svgfields, 2);
            if (moveType.equalsIgnoreCase("PTP") || moveType.equalsIgnoreCase("LIN"))
                profileName = kku.field(svgfields, 6);

            if (moveType.equalsIgnoreCase("CIRC"))
            {
                tagName = tagName + ", X" + kku.field(svgfields, 3);
                profileName = kku.field(svgfields, 7);
            }

            return moveType + " " + tagName + ";" + profileName;
        }

        if (svgop[1].equalsIgnoreCase("%CTippDress"))
        {
            moveType = kku.field(svgfields, 2);
            tagName = "X" + kku.field(svgfields, 3);
            profileName = kku.field(svgfields, 7);
            if (moveType.equalsIgnoreCase("NormalSG"))
                moveType = "PTP ";
            if (moveType.equalsIgnoreCase("NormalSGLIN"))
                moveType = "LIN ";

            return moveType + tagName + ";" + profileName;
        }

        return null;
    }

    private Element init(String svgfields, counters kkc, Document kXmlDoc, Element actListElem)
    {
        String initType = "Same";
        gun = kku.field(svgfields, 2);
        initType = kku.field(svgfields, 3);
        if (initType.equalsIgnoreCase("INIT_FIRST"))
            initType = "New";

        Element actionElem = kXmlDoc.createElement("Action");
        actListElem.appendChild(actionElem);
        actionElem.setAttribute("ActionType", "KukaServoGunInit");

        Element actionNameElem = kXmlDoc.createElement("ActionName");
        actionElem.appendChild(actionNameElem);
        Text actionNameValue = kXmlDoc.createTextNode("KukaServoGunInit" + "." + kkc.svgInit++);
        actionNameElem.appendChild(actionNameValue);

        Element toolResElem = kXmlDoc.createElement("ToolResource");
        actionElem.appendChild(toolResElem);

        Element resNameElem = kXmlDoc.createElement("ResourceName");
        toolResElem.appendChild(resNameElem);
        Text resNameValue = kXmlDoc.createTextNode(gunName);
        resNameElem.appendChild(resNameValue);

        Element attributeListElem = kXmlDoc.createElement("AttributeList");
        actionElem.appendChild(attributeListElem);

        Element attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        Element attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        Text attribNameNode = kXmlDoc.createTextNode("GunNumber");
        attributeNameElem.appendChild(attribNameNode);

        Element attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        Text attribValNode = kXmlDoc.createTextNode(gun);
        attributeValueElem.appendChild(attribValNode);

        attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        attribNameNode = kXmlDoc.createTextNode("NewSame");
        attributeNameElem.appendChild(attribNameNode);

        attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        attribValNode = kXmlDoc.createTextNode(initType);
        attributeValueElem.appendChild(attribValNode);

        
        Element actionActList = kXmlDoc.createElement("ActivityList");
        actionElem.appendChild(actionActList);

        String actName = "DelayActivity." + kkc.delay++;
        Double startTime = new Double(0.0), endTime = new Double(0.5);
        kku.createDelayActivity(actionActList, actName, startTime, endTime, "PROMPT", "SimulationActivity", kXmlDoc);

        return actionElem;
    }

    private Element coupleDecouple(String svgfields, counters kkc, Document kXmlDoc, Element actListElem)
    {
        String actionType, actionName, subActivityType, subActivityName;
        String [] svgop = svgfields.split(",", 4);
        gun = kku.field(svgfields, 2);

        if (svgop[2].equalsIgnoreCase("%Vcouple"))
        {
            actionType = "KukaServoGunCouple";
            subActivityType = "DNBIgpMountActivity";
            actionName = actionType + "." + kkc.svgCouple;
            subActivityName = subActivityType + "." + kkc.svgCouple++;
        }
        else
        {
            actionType = "KukaServoGunDecouple";
            subActivityType = "DNBIgpUnMountActivity";
            actionName = actionType + "." + kkc.svgDecouple;
            subActivityName = subActivityType + "." + kkc.svgDecouple++;
        }

        Element actionElem = kXmlDoc.createElement("Action");
        actListElem.appendChild(actionElem);
        actionElem.setAttribute("ActionType", actionType);

        Element actionNameElem = kXmlDoc.createElement("ActionName");
        actionElem.appendChild(actionNameElem);
        Text actionNameValue = kXmlDoc.createTextNode(actionName);
        actionNameElem.appendChild(actionNameValue);

        Element toolResElem = kXmlDoc.createElement("ToolResource");
        actionElem.appendChild(toolResElem);

        Element resNameElem = kXmlDoc.createElement("ResourceName");
        toolResElem.appendChild(resNameElem);
        Text resNameValue = kXmlDoc.createTextNode(gunName);
        resNameElem.appendChild(resNameValue);

        Element attributeListElem = kXmlDoc.createElement("AttributeList");
        actionElem.appendChild(attributeListElem);

        Element attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        Element attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        Text attribNameNode = kXmlDoc.createTextNode("GunNumber");
        attributeNameElem.appendChild(attribNameNode);

        Element attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        Text attribValNode = kXmlDoc.createTextNode(gun);
        attributeValueElem.appendChild(attribValNode);

        Element activityListElem = kXmlDoc.createElement("ActivityList");
        actionElem.appendChild(activityListElem);

        Element activityElem = kXmlDoc.createElement("Activity");
        activityListElem.appendChild(activityElem);
        activityElem.setAttribute("ActivityType", subActivityType);

        Element activityNameElem = kXmlDoc.createElement("ActivityName");
        activityElem.appendChild(activityNameElem);
        Text activityNameValue = kXmlDoc.createTextNode(subActivityName);
        activityNameElem.appendChild(activityNameValue);

        Element tmpElem;
        Text    tmpText;
        if (svgop[2].equalsIgnoreCase("%Vcouple"))
            tmpElem = kXmlDoc.createElement("MountTool");
        else
            tmpElem = kXmlDoc.createElement("UnmountTool");
        activityElem.appendChild(tmpElem);
        tmpText = kXmlDoc.createTextNode(gunName);
        tmpElem.appendChild(tmpText);

        if (svgop[2].equalsIgnoreCase("%Vcouple"))
            tmpElem = kXmlDoc.createElement("SetToolProfile");
        else
            tmpElem = kXmlDoc.createElement("UnsetToolProfile");
        activityElem.appendChild(tmpElem);
        tmpText = kXmlDoc.createTextNode("Default");
        tmpElem.appendChild(tmpText);

        attributeListElem = kXmlDoc.createElement("AttributeList");
        activityElem.appendChild(attributeListElem);

        attributeElem = kXmlDoc.createElement("Attribute");
        attributeListElem.appendChild(attributeElem);

        attributeNameElem = kXmlDoc.createElement("AttributeName");
        attributeElem.appendChild(attributeNameElem);

        attribNameNode = kXmlDoc.createTextNode("PROMPT");
        attributeNameElem.appendChild(attribNameNode);

        attributeValueElem = kXmlDoc.createElement("AttributeValue");
        attributeElem.appendChild(attributeValueElem);

        attribValNode = kXmlDoc.createTextNode(subActivityType);
        attributeValueElem.appendChild(attribValNode);

        return actionElem;
    }

    private Element spotTipDress(String svgfields, counters kkc, Document kXmlDoc, Element actListElem)
    {
        int tipDress = 0, svgField = 8;
        String actionType, actionName;
        String [] svgAttr = new String [6];
        String [] svgop = svgfields.split(",", 4);

        if (svgop[1].equalsIgnoreCase("%CSpotP"))
        {
            actionType = "KukaServoGunWeld";
            actionName = actionType + "." + kkc.svgWeld++;

            if (svgop[2].equalsIgnoreCase("%VCIRC"))
                svgField++; // starting from field 9

            svgAttr[0] = kku.field(svgfields, svgField); // 8/9, gunNumber
            svgField = svgField + 2;
            svgAttr[1] = kku.field(svgfields, svgField); // 10/11, cont
        }
        else
        {
            // if (svgop[1].equalsIgnoreCase("%CTippDress"))
            actionType = "KukaServoGunTipDress";
            actionName = actionType + "." + kkc.svgTipDress++;

            svgAttr[0] = kku.field(svgfields, 9); // gunNumber
            svgAttr[1] = null; // no cont for TipDress
            svgField = 10;
            tipDress = 1;
        }

        svgField = svgField + 2; // 12:(Spot(PTP/LIN),TipDress)/13:Spot(CIRC)
        svgAttr[2] = kku.field(svgfields, svgField); // part
        svgField = svgField + 2; // 14/15
        svgAttr[3] = kku.field(svgfields, svgField); // force
        svgField = svgField + 3; // 17/18
        svgAttr[4] = kku.field(svgfields, svgField); // comp
        svgField = svgField + 2; // 19/20
        svgAttr[5] = kku.field(svgfields, svgField); // trigger

        Element actionElem = kXmlDoc.createElement("Action");
        actListElem.appendChild(actionElem);
        actionElem.setAttribute("ActionType", actionType);

        Element actionNameElem = kXmlDoc.createElement("ActionName");
        actionElem.appendChild(actionNameElem);
        Text actionNameValue = kXmlDoc.createTextNode(actionName);
        actionNameElem.appendChild(actionNameValue);

        Element toolResElem = kXmlDoc.createElement("ToolResource");
        actionElem.appendChild(toolResElem);

        Element resNameElem = kXmlDoc.createElement("ResourceName");
        toolResElem.appendChild(resNameElem);
        Text resNameValue = kXmlDoc.createTextNode(gunName);
        resNameElem.appendChild(resNameValue);

        svgAttributes(tipDress, svgAttr, kXmlDoc, actionElem);

        Element actionActList = kXmlDoc.createElement("ActivityList");
        actionElem.appendChild(actionActList);

        String actName = "Basic Motion Activity." + kkc.move++;
        Double startTime = new Double(0.0);
        Double endTime = new Double(1.0);
        String closeJ = svgAttr[2];
        if (svgNeg)
            closeJ = "-" + svgAttr[2];
        kku.createActionMotionActivity(actionActList, actName, startTime, endTime, closeJ, gunJointType, "PROMPT", "DNBRobotMotionActivity", kXmlDoc);

        actName = "DelayActivity." + kkc.delay++;
        endTime = new Double(1.5);
        kku.createDelayActivity(actionActList, actName, startTime, endTime, "PROMPT", "DelayActivity", kXmlDoc);

        actName = "Basic Motion Activity." + kkc.move++;
        endTime = new Double(1.0);
        kku.createActionMotionActivity(actionActList, actName, startTime, endTime, closeHome, gunJointType, "PROMPT", "DNBRobotMotionActivity", kXmlDoc);

        return actionElem;
    }

    private void svgAttributes(int tipDress, String [] svgAttr, Document kXmlDoc, Element parentElem)
    {
        String [] attrNames = {"GunNumber", "Cont", "Part", "Force", "Comp", "Trigger"};

        Element attributeListElem = kXmlDoc.createElement("AttributeList");
        parentElem.appendChild(attributeListElem);

        Element attributeElem, attributeNameElem, attributeValueElem;
        Text attribNameNode, attribValNode;

        for (int ii=0; ii<attrNames.length; ii++)
        {
            if (tipDress == 1 && ii == 1)
                continue; // skip attribute Cont

            attributeElem = kXmlDoc.createElement("Attribute");
            attributeListElem.appendChild(attributeElem);

            attributeNameElem = kXmlDoc.createElement("AttributeName");
            attributeElem.appendChild(attributeNameElem);

            attribNameNode = kXmlDoc.createTextNode(attrNames[ii]);
            attributeNameElem.appendChild(attribNameNode);

            attributeValueElem = kXmlDoc.createElement("AttributeValue");
            attributeElem.appendChild(attributeValueElem);

            attribValNode = kXmlDoc.createTextNode(svgAttr[ii]);
            attributeValueElem.appendChild(attribValNode);
        }
    }

    public void modifySVGOpenJoint(Element actElem, String gunJointType, String extAxisVal, Document kXmlDoc)
    {
        Element nextElem = (Element)actElem.getNextSibling();
        if (nextElem == null || nextElem.getTagName().equals("Action") == false)
            return;
        if (nextElem.getAttribute("ActionType").equals("KukaServoGunWeld") == false &&
            nextElem.getAttribute("ActionType").equals("KukaServoGunTipDress")  == false)
            return;
        Element openJElem = (Element)nextElem.getElementsByTagName("ActivityList").item(0).getLastChild();
        Element jTgt = (Element)openJElem.getElementsByTagName("JointTarget").item(0);
        Node HomeName = jTgt.getFirstChild();
        jTgt.removeChild(HomeName);
        Element j = kXmlDoc.createElement("Joint");
        jTgt.appendChild(j);
        j.setAttribute("DOFNumber", "1");
        j.setAttribute("JointName", "Joint 1");
        j.setAttribute("JointType", gunJointType);
        Text jvt;
        if (gunJointType.equals("Translational"))
        {
            j.setAttribute("Units", "m");
            jvt = kXmlDoc.createTextNode(String.valueOf(Double.parseDouble(extAxisVal)/1000.0));
        }
        else
        {
            j.setAttribute("Units", "deg");
            jvt = kXmlDoc.createTextNode(extAxisVal);
        }
        Element jv = kXmlDoc.createElement("JointValue");
        j.appendChild(jv);
        jv.appendChild(jvt);
    }
}
