//DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2002-2011

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

import javax.xml.transform.TransformerException;

//Java Util classes
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class motomandressup {

    private static final int maxNumRobotJoints = 12;
    private static String sTsk = "START TASK:";
    private static String eTsk = "END TASK:";
    private static String nl = "\r\n";
    private String vrcTargetDir = "C:\\tmp";
    private int numRobotJoints;
    private ArrayList m_NRLProgram = new ArrayList(1);
    private ArrayList m_V5Properties = new ArrayList(1);
    private String m_ProgramName, mainProgramName;
    private int auxAxes = 0, extAxes = 0, posAxes = 0;
    private int auxAxisStart = -1, extAxisStart = -1;
    private double [] resolution = new double[maxNumRobotJoints];
    private double [] zeroOffset = new double[maxNumRobotJoints];
    private String [] jointType = new String[maxNumRobotJoints];
    private String param_attr = null; // user set parameter "SC,RW" without "///ATTR "
    private String header_attr = "///ATTR SC,RW";
    private String header_group = "///GROUP1 RB1,ST1";
    private String firstTool = "";
    private ArrayList toolProfileList = new ArrayList(1);
    private boolean outputTools = false;
    private int xrc_pl = 0;
    private int posVarDigits = 4;
    private int largestTagNum = -1;

    //Constructor
    public motomandressup(String OutputFileName, String InputFileName, String LogFileName)
    {
        // OutputFileName: NRLProgramFile.tmp
        // InputFileName: motParam.tmp

        for (int ii = 0; ii < resolution.length; ii++)
            resolution[ii] = Double.NaN;

        for (int ii = 0; ii < zeroOffset.length; ii++)
            zeroOffset[ii] = Double.NaN;

        for (int ii = 0; ii < jointType.length; ii++)
            jointType[ii] = "Rotational";

        File f = new File(OutputFileName);
        String NRLProgramFileName = f.getParent() + f.separator + "NRLProgramFileOriginal.tmp";

        try
        {
            String line, toolName;
            int lineno, idx;
            String [] propertyComponents = {"", "", ""}, lineParts = {"", ""};

            BufferedReader outputFile = new BufferedReader(new FileReader(OutputFileName)); 
            BufferedReader inputFile  = new BufferedReader(new FileReader(InputFileName));
            BufferedWriter bw = new BufferedWriter(new FileWriter(NRLProgramFileName, false));

            line = outputFile.readLine();
            lineno = 0;
            while (line != null)
            {
                bw.write(line + nl); // write to NRLProgramFileOriginal.tmp

                if (line.equals(""))
                {
                    line = outputFile.readLine();
                    continue;
                }
                
                m_NRLProgram.add(lineno, line);

                lineno++;
                line = outputFile.readLine();
            } // while
            outputFile.close();
            bw.close();

            // now open the output file and overwrite the contents
            bw = new BufferedWriter(new FileWriter(OutputFileName, false));

            Pattern p;
            Matcher m;
            line = inputFile.readLine();
            lineno = 0;
            while (line != null)
            {
                if (line.equals(""))
                {
                    line = inputFile.readLine();
                    continue;
                }
                m_V5Properties.add(lineno, line);

                if (line.startsWith("ResourceParameter."))
                {
                    idx = line.indexOf('.');
                    line = line.substring(idx+1);
                    if (line.startsWith("PulseValue."))
                    {
                        idx = line.indexOf('.');
                        propertyComponents = line.substring(idx+1).split(":", 2);
                        idx = Integer.parseInt(propertyComponents[0]);
                        if (idx > 0)
                        {
                            try
                            {
                                resolution[idx-1] = Double.parseDouble(propertyComponents[1]);
                            }
                            catch (NumberFormatException e)
                            {
                                bw.write("ERROR INFO START" + nl);
                                bw.write("Number format error: " + e.getMessage() + nl );
                                bw.write("ERROR INFO END" + nl);
                            }
                        }
                        else
                        {
                            bw.write("ERROR INFO START" + nl);
                            bw.write("Parameter PulseValue should start from 1." + nl);
                            bw.write("ERROR INFO END" + nl);
                        }
                    }
                    if (line.startsWith("ZeroValue."))
                    {
                        idx = line.indexOf('.');
                        propertyComponents = line.substring(idx+1).split(":", 2);
                        idx = Integer.parseInt(propertyComponents[0]);
                        if (idx > 0)
                        {
                            try
                            {
                                zeroOffset[idx-1] = Double.parseDouble(propertyComponents[1]);
                            }
                            catch (NumberFormatException e)
                            {
                                bw.write("ERROR INFO START" + nl);
                                bw.write("Number format error: " + e.getMessage() + nl );
                                bw.write("ERROR INFO END" + nl);
                            }
                        }
                        else
                        {
                            bw.write("ERROR INFO START" + nl);
                            bw.write("Parameter ZeroValue should start from 1." + nl);
                            bw.write("ERROR INFO END" + nl);
                        }
                    }
                    if (line.startsWith("Header_Attr:"))
                    {
                        idx = line.indexOf(':');
                        param_attr = line.substring(idx+1);
                    }
                    if (line.startsWith("OutputTools:"))
                    {
                        idx = line.indexOf(':');
                        if (line.substring(idx+1).equalsIgnoreCase("TRUE"))
                            outputTools = true;
                    }
                    if (line.startsWith("XRCPositionLevel:"))
                    {
                        idx = line.indexOf(':');
                        xrc_pl = Integer.parseInt(line.substring(idx+1));
                    }
                    if (line.startsWith("VRCTargetDirectory:"))
                    {
                        idx = line.indexOf(':');
                        vrcTargetDir = line.substring(idx+1);
                        File vrcF = new File(vrcTargetDir);
                        vrcTargetDir = vrcF.getPath();
                    }
                    if (line.startsWith("PosVarDigits"))
                    {
                        idx = line.indexOf(':');
                        posVarDigits = Integer.parseInt(line.substring(idx+1));
                    }
                }
                else
                if (line.startsWith("RobotJoint.") || line.startsWith("AuxJoint."))
                {
                    lineParts = line.split(":", 2);

                    propertyComponents = lineParts[0].split("\\.", 2);
                    idx = Integer.parseInt(propertyComponents[1]);

                    propertyComponents = lineParts[1].split(",");
                    jointType[idx-1] = propertyComponents[1];

                    if (line.startsWith("RobotJoint."))
                        numRobotJoints = idx;

                    if (line.startsWith("AuxJoint."))
                    {
                        if (propertyComponents[2].equals("RailTrackGantry"))
                        {
                            auxAxes++;
                            if (auxAxisStart == -1)
                                auxAxisStart = idx - 1;
                        }
                        if (propertyComponents[2].equals("EndOfArmTooling"))
                        {
                            extAxes++;
                            if (extAxisStart == -1)
                                extAxisStart = idx - 1;
                        }
                        if (propertyComponents[2].equals("WorkpiecePositioner"))
                        {
                            posAxes++;
                            if (extAxisStart == -1)
                                extAxisStart = idx - 1;
                        }
                    }
                }
                else
                if (line.startsWith("DNBRobotTask.Name"))
                {
                    lineParts = line.split(":", 2);
                    String sTmp = lineParts[1].trim();
                    if (sTmp.length() > 8)
                        mainProgramName = sTmp.substring(0, 8);
                    else
                        mainProgramName = sTmp;
                }
                else
                if (line.startsWith("DNBRobotTask.Header:Robot Language:///ATTR "))
                {
                    lineParts = line.split("Robot Language:");
                    header_attr = lineParts[1].trim();
                }
                else
                if (line.startsWith("DNBRobotTask.Header:Robot Language:///GROUP1 "))
                {
                    lineParts = line.split("Robot Language:");
                    header_group = lineParts[1].trim();
                }
                else
                if (line.startsWith("DNBRobotMotionActivity@PosReg:"))
                {
                    lineParts = line.split(":", 2);
                    String sTmp = lineParts[1].trim();
                    if (sTmp != null && sTmp.length() > 0)
                    {
                        int iPosReg = Integer.parseInt(sTmp);
                        if (iPosReg > largestTagNum)
                            largestTagNum = iPosReg;
                    }
                }
                else
                if (line.startsWith("DNBRobotMotionActivity.TagName:"))
                {
                    lineParts = line.split(":", 2);
                    String sTmp = lineParts[1].trim().substring(1);
                    int iTagNum = Integer.parseInt(sTmp);
                    if (iTagNum > largestTagNum)
                        largestTagNum = iTagNum;
                }
                else
                {
                    p = Pattern.compile("ToolProfile\\.\\d+:", Pattern.CASE_INSENSITIVE);
                    m = p.matcher(line);
                    if (m.find() == true)
                    {
                        idx = line.indexOf(':');
                        toolName = line.substring(idx+1);
                        toolProfileList.add(toolName);
                    }
                }

                lineno++;
                line = inputFile.readLine();
            } // while                    
            inputFile.close();

            Double tmpDouble = new Double(0.0);
            for (int ii=1; ii<numRobotJoints+auxAxes+extAxes+posAxes+1; ii++)
            {
                if (tmpDouble.isNaN(resolution[ii-1]))
                {
                    bw.write("ERROR INFO START" + nl);
                    bw.write("Parameter PulseValue." + ii + " is not defined." + nl);
                    bw.write("ERROR INFO END" + nl);

                    break;
                }
                if (tmpDouble.isNaN(zeroOffset[ii-1]))
                {
                    bw.write("ERROR INFO START" + nl);
                    bw.write("Parameter ZeroValue." + ii + " is not defined." + nl);
                    bw.write("ERROR INFO END" + nl);

                    break;
                }
            }

            WriteVersion(bw);
            boolean bRRS = false;
            
            for (int ii=0; ii<m_NRLProgram.size(); ii++)
            {
                line = (String)m_NRLProgram.get(ii);
                if (line.startsWith(sTsk))
                {
                    m_ProgramName = line.split(":", 2)[1].trim();
                    
                    WriteHeader(bw, bRRS);
                    WritePositions(bw, bRRS);
                    WriteProgram(bw, bRRS);
                }
            }
            if (outputTools == true)
                WriteToolCND(bw, bRRS);
            bw.close();
            
            // Create RRS-II download log file
            if ( LogFileName != null )
            {
                bRRS = true;
                String rrsProgramName;
                for (int ii=0; ii<m_NRLProgram.size(); ii++)
                {
                    line = (String)m_NRLProgram.get(ii);
                    if (line.startsWith(sTsk))
                    {
                        m_ProgramName = line.split(":", 2)[1].trim();
                        rrsProgramName = f.getParent() + f.separator + m_ProgramName + ".JBI";
                        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsProgramName, false), "UTF-8"));
                        WriteHeader(bw, bRRS);
                        WritePositions(bw, bRRS);
                        WriteProgram(bw, bRRS);
                        bw.close();
                    }
                }

                if (outputTools == true)
                {
                    String rrsToolFile = f.getParent() + f.separator + "TOOL.CND";
                    bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsToolFile, false), "UTF-8"));
                    WriteToolCND(bw, bRRS);
                    bw.close();
                }
                
                WriteRRSLog(LogFileName);
            }
            
        } // try
        catch (IOException e) {
            e.getMessage();
        }
        catch (ArrayIndexOutOfBoundsException e) {
            e.getMessage();
        }
    } // constructor

    public static void main(String [] parameters) throws  NumberFormatException
    {
            String outputFileName = parameters[0];
            String inputFileName = parameters[1];
            String logFileName = null;
            if (parameters.length >= 3)
                logFileName = parameters[2];

            motomandressup downloader = new motomandressup(outputFileName, inputFileName, logFileName);
    }

    public void WriteVersion(BufferedWriter bw)
    {
        try {
            bw.write("VERSION INFO START" + nl);
            bw.write("DELMIA CORP. NRL TEACH Motoman DRESSUP DOWNLOADER VERSION 5 RELEASE 21" + nl);
            bw.write("COPYRIGHT DELMIA CORP. 1986-2011, ALL RIGHTS RESERVED" + nl);
            bw.write("VERSION INFO END" + nl);
        }
        catch (IOException e) {
            e.getMessage();
         }
    }

    public void WriteHeader(BufferedWriter bw, boolean bRRS)
    {
        int ii, cvar = 0, pvar = 0, bcvar = 0, bpvar = 0, ecvar = 0, exvar = 0;
        String line, currentProc = null;
        String propertyValue = "";
        String [] propertyComponents = {"", ""};
        int tagNum = largestTagNum+1;
        String tagName = "", posType = "", posReg = "";
        ArrayList cvarList = new ArrayList(1);
        ArrayList pvarList = new ArrayList(1);
        
        try {
            if (bRRS == false)
                bw.write("DATA FILE START " + m_ProgramName + ".JBI" + nl);
            bw.write("/JOB" + nl);
            bw.write("//NAME " + m_ProgramName + nl);
            bw.write("//POS" + nl);
            
            for (ii=0; ii<m_V5Properties.size(); ii++)
            {
                line = (String)m_V5Properties.get(ii);
                if (line.startsWith(sTsk))
                {
                    currentProc = line.split(":", 2)[1].trim();
                    firstTool = "";
                    continue;
                }
                if (currentProc.equals(m_ProgramName) && line.startsWith("DNBRobotMotionActivity.ActivityName:"))
                {
                    posType = "";
                    posReg  = "";
                    for (ii=ii+1; ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith("DNBRobotMotionActivity.ActivityName:"); ii++)
                    {
                        line = (String)m_V5Properties.get(ii);
                        propertyComponents = line.split(":", 2);
                        propertyValue = propertyComponents[1].trim();
                        
                        if (line.equalsIgnoreCase("DNBRobotMotionActivity@PosType:P"))
                        {
                            posType = "P";
                        }
                        else
                        if (line.equalsIgnoreCase("DNBRobotMotionActivity@PosType:C") ||
                            line.equalsIgnoreCase("DNBRobotMotionActivity@PosType:"))
                        {
                            posType = "C";
                        }
                        else
                        if (propertyComponents[0].equalsIgnoreCase("DNBRobotMotionActivity@PosReg"))
                        {
                            posReg = propertyValue;
                        }
                        else
                        if (line.equalsIgnoreCase("DNBRobotMotionActivity@BType:BC"))
                        {
                            bcvar++;
                        }
                        else
                        if (line.equalsIgnoreCase("DNBRobotMotionActivity@BType:BP"))
                        {
                            bpvar++;
                        }
                        else
                        if (line.equalsIgnoreCase("DNBRobotMotionActivity@EType:EC"))
                        {
                            ecvar++;
                        }
                        else
                        if (line.equalsIgnoreCase("DNBRobotMotionActivity@EType:EX"))
                        {
                            exvar++;
                        }
                        else
                        if (line.startsWith("ToolProfile.ProfileName:") && firstTool.equals(""))
                        {
                            firstTool = line.split(":", 2)[1];
                        }
                    }
                    ii--; // backup 1: the next robotMotionActProperty is already read
                    if (posType.equals("C"))
                    {
                        if (posReg.length() > 0)
                        {
                            while (posReg.length() < posVarDigits)
                                posReg = "0" + posReg;
                            
                            tagName = "C" + posReg;
                        }
                        if (tagName.equals(""))
                        {
                            String sNum = String.valueOf(tagNum++);
                            while (sNum.length() < posVarDigits)
                                sNum = "0" + sNum;
                            tagName = "C" + sNum;
                        }
                        if (cvarList.contains(tagName) == false)
                        {
                            cvarList.add(tagName);
                            cvar++;
                        }
                    }
                    else
                    if (posType.equals("P"))
                    {
                        if (posReg.length() > 0)
                        {
                            tagName = "P" + posReg;
                        }
                        if (pvarList.contains(tagName) == false)
                        {
                            pvarList.add(tagName);
                            pvar++;
                        }
                    }
                }
            }

            if (auxAxes > 0)
            {
                if (bcvar != cvar)
                    bcvar = cvar;
                if (bpvar != pvar)
                    bpvar = pvar;
            }
            if (extAxes + posAxes > 0)
            {
                if (ecvar != cvar)
                    ecvar = cvar;
                if (exvar != pvar)
                    exvar = pvar;
            }
            
            bw.write("///NPOS ");
            bw.write(cvar + "," + bcvar + "," + ecvar + ",");
            bw.write(pvar + "," + bpvar + "," + exvar + nl);

            if (Pattern.compile("TOOL \\d+").matcher(firstTool).find() == true)
            {
                // if tool profile name is in the form of "TOOL num", output the number
                bw.write("///TOOL " + firstTool.split(" ")[1] + nl);
            }
            else
            {
                // otherwise output its index
                bw.write("///TOOL " + toolProfileList.indexOf(firstTool) + nl);
            }
            bw.write("///POSTYPE PULSE" + nl);
            bw.write("///PULSE" + nl);
        }
         catch (IOException e) {
            e.getMessage();
        }
    } // WriteHeader

    public void WriteProgram(BufferedWriter bw, boolean bRRS)
    {
        try
        {
            SimpleDateFormat mrcDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm");
            Date d = new Date();
            String d_str = mrcDateFormat.format(d);

            bw.write("//INST" + nl);
            bw.write("///DATE " + d_str + nl);
            if (param_attr != null)
                bw.write("///ATTR " + param_attr + nl);
            else
                bw.write(header_attr + nl);
            bw.write(header_group + nl);
            bw.write("NOP" + nl);

            Pattern p = Pattern.compile("MOV[LJC]\\s+", Pattern.CASE_INSENSITIVE);
            Matcher m;
            String line, currentProc = null;
            String [] lineParts = {"", "", ""};
            for (int ii=0; ii<m_NRLProgram.size(); ii++)
            {
                line = (String)m_NRLProgram.get(ii);
                if (line.startsWith(sTsk))
                {
                    currentProc = line.split(":", 2)[1].trim();
                    continue;
                }
                if (line.startsWith(eTsk))
                    continue;

                if (currentProc.equals(m_ProgramName))
                {
                    m = p.matcher(line);
                    if (m.find() == true)
                    {
                        lineParts = line.split("[ \\t]+", 3);
                        bw.write(lineParts[0] + " " + lineParts[1]);

                        if (auxAxes + extAxes + posAxes > 0)
                        {
                            if (auxAxes > 0 && (lineParts[2].indexOf("BC") == -1 && lineParts[2].indexOf("BP") == -1))
                            {
                                if (lineParts[1].startsWith("P"))
                                    bw.write(" BP");
                                else
                                    bw.write(" BC");
                                bw.write(lineParts[1].substring(1));
                            }
                            if ((extAxes > 0 || posAxes > 0) && (lineParts[2].indexOf("EC") == -1 && lineParts[2].indexOf("EX") == -1))
                            {
                                if (lineParts[1].startsWith("P"))
                                    bw.write(" EX");
                                else
                                    bw.write(" EC");
                                bw.write(lineParts[1].substring(1));
                            }
                        }

                        int iArc = lineParts[2].indexOf("ARCO");
                        if (iArc > 0)
                        {
                            bw.write(" " + lineParts[2].substring(0, iArc) + nl);
                            bw.write(lineParts[2].substring(iArc) + nl);
                        }
                        else
                        {
                            bw.write(" " + lineParts[2] + nl);
                        }
                    }
                    else
                    {
                        bw.write(line + nl);
                    }
                }
            }
            bw.write("END" + nl);
            if (bRRS == false)
                bw.write("DATA FILE END" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
    } // WriteProgram

    public void WritePositions(BufferedWriter bw, boolean bRRS)
    {
        String robotMotionActProperty = "DNBRobotMotionActivity.ActivityName:";
        String jointPosProperty = "DNBRobotMotionActivity.JointPosition:";
        String tagNameProperty = "DNBRobotMotionActivity.TagName:";
        String posTypeProperty = "DNBRobotMotionActivity@PosType:";
        String BTypeProperty = "DNBRobotMotionActivity@BType:";
        String ETypeProperty = "DNBRobotMotionActivity@EType:";
        String posRegProperty = "DNBRobotMotionActivity@PosReg:";
        String pVarIndexProperty = "DNBRobotMotionActivity@P:";
        String bcVarIndexProperty = "DNBRobotMotionActivity@BC:";
        String bpVarIndexProperty = "DNBRobotMotionActivity@BP:";
        String ecVarIndexProperty = "DNBRobotMotionActivity@EC:";
        String exVarIndexProperty = "DNBRobotMotionActivity@EX:";
        String prePosAttr = "DNBRobotMotionActivity@PrePosAttr";
        String postPosAttr = "DNBRobotMotionActivity@PostPosAttr";
        String toolProfileProperty = "ToolProfile.ProfileName:";
        ArrayList alPrePosAttr = new ArrayList();
        ArrayList alPostPosAttr = new ArrayList();
        String tool = firstTool, lastTool = firstTool;
        String propertyValue = "";
        double dJointVal;
        String posType = "", bPosType = "", ePosType = "";
        String posReg = "", bPosReg = "", ePosReg = "";
        String tagName = "";
        String [] saAxisValues = new String[numRobotJoints];
        ArrayList tagsOutput = new ArrayList(1);
        boolean outputPos = false, cvaroutput = false, pvaroutput = false;
        String currentLine, currentProc = "";
        String [] propertyComponents = {"", ""};
        int tagNum;

        for (int ii=0; ii<numRobotJoints; ii++)
            saAxisValues[ii] = "";

        try
        {
            // output C variables
            tagNum = largestTagNum+1;
            for (int ii=0; ii<m_V5Properties.size(); ii++)
            {
                currentLine = (String)m_V5Properties.get(ii);

                propertyComponents = currentLine.split(":", 2);
                propertyValue = propertyComponents[1].trim();
                
                if (currentLine.startsWith(sTsk))
                {
                    currentProc = propertyValue;
                    continue;
                }

                if (currentLine.startsWith(robotMotionActProperty) && currentProc.equals(m_ProgramName))
                {
                    outputPos = false;
                    tagName = "";
                    posType = "";
                    posReg  = "";
                    for (ii=ii+1; ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty); ii++)
                    {
                        currentLine = (String)m_V5Properties.get(ii);

                        propertyComponents = currentLine.split(":", 2);
                        propertyValue = propertyComponents[1].trim();
                        
                        if (currentLine.startsWith(eTsk))
                            break;
                        if (currentLine.startsWith(tagNameProperty))
                        {
                            tagName = propertyValue;
                        }
                        if (currentLine.startsWith(posRegProperty))
                        {
                            posReg = propertyValue;
                        }
                        if (currentLine.startsWith(posTypeProperty))
                        {
                            if (propertyValue.equalsIgnoreCase("P"))
                                posType = "P";
                            else
                                posType = "C";
                        }
                        if (currentLine.startsWith(prePosAttr))
                        {
                            alPrePosAttr.add(propertyValue);
                        }
                        if (currentLine.startsWith(postPosAttr))
                        {
                            alPostPosAttr.add(propertyValue);
                        }
                        if (currentLine.startsWith(jointPosProperty))
                        {
                            String [] values = propertyValue.split(" ");
                            for (int jj=0; jj<numRobotJoints; jj++)
                            {
                                dJointVal = Double.parseDouble(values[jj]);
                                saAxisValues[jj] = JointToEncoderConversion(jj+1, dJointVal);
                            }
                            outputPos = true;
                        }
                        if (currentLine.startsWith(toolProfileProperty))
                        {
                            tool = propertyValue;
                        }
                    }
                    ii--; // backup 1: the next robotMotionActProperty is already read
                    // if V5 properties posType and posReg are set, overwrite tagName
                    if (false == posType.equalsIgnoreCase("P"))
                    {
                        if (posReg.length() > 0)
                        {
                            while (posReg.length() < posVarDigits)
                                posReg = "0" + posReg;

                            tagName = "C" + posReg;
                        }
                        // no tagname, no properties, default to Cvar
                        if (tagName.equals(""))
                        {
                            String sNum = String.valueOf(tagNum++);
                            while (sNum.length() < posVarDigits)
                                sNum = "0" + sNum;
                            tagName = "C" + sNum;
                        }
                    }
                    if (!tagName.equals("") && false == posType.equalsIgnoreCase("P") && true == outputPos) // JointPosition is set
                    {
                        if (tagsOutput.contains(tagName) == false)
                        {
                            tagsOutput.add(tagName);

                            if (tool.equals(lastTool) == false)
                            {
                                if (Pattern.compile("TOOL \\d+").matcher(tool).find() == true)
                                {
                                    bw.write("///TOOL " + tool.split(" ")[1] + nl);
                                }
                                else
                                {
                                    bw.write("///TOOL " + toolProfileList.indexOf(tool) + nl);
                                }
                                lastTool = tool;
                            }
                            for (int jj=0; jj<alPrePosAttr.size(); jj++)
                                bw.write((String)alPrePosAttr.get(jj) + nl);
                            
                            bw.write(tagName + "=");
                            bw.write(saAxisValues[0]);
                            for (int jj=1; jj<numRobotJoints; jj++)
                                bw.write("," + saAxisValues[jj]);
                            bw.write(nl);
                            
                            for (int jj=0; jj<alPostPosAttr.size(); jj++)
                                bw.write((String)alPostPosAttr.get(jj) + nl);
                            
                            alPrePosAttr.clear();
                            alPostPosAttr.clear();
                            
                            cvaroutput = true;
                        }
                    }
                }
            }

            // output BC variables
            if (auxAxes > 0)
            {
                tagNum = largestTagNum+1;
                if (cvaroutput == true && lastTool.equalsIgnoreCase("TOOL 0") == false)
                {
                    bw.write("///TOOL 0" + nl);
                    lastTool = "TOOL 0";
                }
                tagsOutput.clear();

                for (int ii=0; ii<m_V5Properties.size(); ii++)
                {
                    currentLine = (String)m_V5Properties.get(ii);

                    propertyComponents = currentLine.split(":", 2);
                    propertyValue = propertyComponents[1].trim();
                    
                    if (currentLine.startsWith(sTsk))
                    {
                        currentProc = propertyValue;
                        continue;
                    }

                    if (currentLine.startsWith(robotMotionActProperty) && currentProc.equals(m_ProgramName))
                    {
                        outputPos = false;
                        tagName = "";
                        bPosType = "";
                        bPosReg  = "";
                        posType = "";
                        posReg  = "";
                        for (ii=ii+1; ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty); ii++)
                        {
                            currentLine = (String)m_V5Properties.get(ii);

                            propertyComponents = currentLine.split(":", 2);
                            propertyValue = propertyComponents[1].trim();
                            
                            if (currentLine.startsWith(eTsk))
                                break;
                            if (currentLine.startsWith(posTypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("P"))
                                    posType = "P";
                                else
                                    posType = "C";
                            }
                            if (currentLine.startsWith(BTypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("BP"))
                                    bPosType = "BP"; // set to BP, pass here, output later in the BP section
                                else
                                    bPosType = "BC";
                            }
                            if (currentLine.startsWith(bcVarIndexProperty))
                            {
                                bPosReg = propertyValue;
                            }
                            if (currentLine.startsWith(posRegProperty))
                            {
                                posReg = propertyValue;
                            }
                            if (currentLine.startsWith(pVarIndexProperty))
                            {
                                posReg = propertyValue;
                            }
                            if (currentLine.startsWith(jointPosProperty))
                            {
                                String [] values = propertyValue.split(" ");
                                for (int jj=auxAxisStart; jj<auxAxes+auxAxisStart; jj++)
                                {
                                    dJointVal = Double.parseDouble(values[jj]);
                                    saAxisValues[jj-auxAxisStart] = JointToEncoderConversion(jj+1, dJointVal);
                                }
                            }
                        }
                        ii--; // backup 1: the next robotMotionActProperty is already read
                        if (false == posType.equalsIgnoreCase("P"))
                        {
                            if (bPosType.equalsIgnoreCase("BC") && bPosReg.length() > 0)
                            {
                                while (bPosReg.length() < posVarDigits)
                                    bPosReg = "0" + bPosReg;
                                
                                tagName = "BC" + bPosReg;
                            }
                            if (false == bPosType.equals("BP") && posReg.length() > 0)
                            {
                                while (posReg.length() < posVarDigits)
                                    posReg = "0" + posReg;
                                
                                tagName = "BC" + posReg;
                            }
                            if (tagName.equals("") && false == bPosType.equalsIgnoreCase("BP"))
                            {
                                String sNum = String.valueOf(tagNum++);
                                while (sNum.length() < posVarDigits)
                                    sNum = "0" + sNum;
                                tagName = "BC" + sNum;
                            }
                        }
                        if (!tagName.equals(""))
                        {
                            if (tagsOutput.contains(tagName) == false)
                            {
                                tagsOutput.add(tagName);
                                
                                bw.write(tagName + "=");
                                bw.write(saAxisValues[0]);
                                for (int jj=1; jj<auxAxes; jj++)
                                    bw.write("," + saAxisValues[jj]);
                                bw.write(nl);
                            }
                        }
                    }
                }
            }
            // output EC variables
            if (extAxes + posAxes > 0)
            {
                tagNum = largestTagNum+1;
                if (auxAxes == 0)
                {
                    bw.write("///TOOL 0" + nl);
                    lastTool = "TOOL 0";
                }
                tagsOutput.clear();

                for (int ii=0; ii<m_V5Properties.size(); ii++)
                {
                    currentLine = (String)m_V5Properties.get(ii);

                    propertyComponents = currentLine.split(":", 2);
                    propertyValue = propertyComponents[1].trim();

                    if (currentLine.startsWith(sTsk))
                    {
                        currentProc = propertyValue;
                        continue;
                    }
                    if (currentLine.startsWith(robotMotionActProperty) && currentProc.equals(m_ProgramName))
                    {
                        outputPos = false;
                        tagName = "";
                        ePosType = "";
                        ePosReg  = "";
                        posType = "";
                        posReg = "";
                        for (ii=ii+1; ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty); ii++)
                        {
                            currentLine = (String)m_V5Properties.get(ii);

                            propertyComponents = currentLine.split(":", 2);
                            propertyValue = propertyComponents[1].trim();
                            
                            if (currentLine.startsWith(eTsk))
                                break;
                            if (currentLine.startsWith(posTypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("P"))
                                    posType = "P";
                                else
                                    posType = "C";
                            }
                            if (currentLine.startsWith(ETypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("EX"))
                                    ePosType = "EX";
                                else
                                    ePosType = "EC";
                            }
                            if (currentLine.startsWith(ecVarIndexProperty))
                            {
                                ePosReg = propertyValue;
                            }
                            if (currentLine.startsWith(posRegProperty))
                            {
                                posReg = propertyValue;
                            }
                            if (currentLine.startsWith(pVarIndexProperty))
                            {
                                posReg = propertyValue;
                            }
                            if (currentLine.startsWith(jointPosProperty))
                            {
                                String [] values = propertyValue.split(" ");
                                for (int jj=extAxisStart; jj<extAxes + posAxes + extAxisStart; jj++)
                                {
                                    dJointVal = Double.parseDouble(values[jj]);
                                    saAxisValues[jj-extAxisStart] = JointToEncoderConversion(jj+1, dJointVal);
                                }
                            }
                        }
                        ii--; // backup 1: the next robotMotionActProperty is already read
                        if (false == posType.equalsIgnoreCase("P"))
                        {
                            if (ePosType.equalsIgnoreCase("EC") && ePosReg.length() > 0)
                            {
                                while (ePosReg.length() < posVarDigits)
                                    ePosReg = "0" + ePosReg;
                                
                                tagName = "EC" + ePosReg;
                            }
                            if (false == ePosType.equals("EX") && posReg.length() > 0)
                            {
                                while (posReg.length() < posVarDigits)
                                    posReg = "0" + posReg;

                                tagName = "EC" + posReg;
                            }
                            if (tagName.equals("") && false == ePosType.equalsIgnoreCase("EX"))
                            {
                                String sNum = String.valueOf(tagNum++);
                                while (sNum.length() < posVarDigits)
                                    sNum = "0" + sNum;
                                tagName = "EC" + sNum;
                            }
                        }
                        if (!tagName.equals(""))
                        {
                            if (tagsOutput.contains(tagName) == false)
                            {
                                tagsOutput.add(tagName);

                                bw.write(tagName + "=");
                                bw.write(saAxisValues[0]);
                                for (int jj=1; jj<extAxes+posAxes; jj++)
                                    bw.write("," + saAxisValues[jj]);
                                bw.write(nl);
                            }
                        }
                    }
                }
            }
            // output P variables
            for (int ii=0; ii<m_V5Properties.size(); ii++)
            {
                currentLine = (String)m_V5Properties.get(ii);

                propertyComponents = currentLine.split(":", 2);
                propertyValue = propertyComponents[1].trim();
                
                if (currentLine.startsWith(sTsk))
                {
                    currentProc = propertyValue;
                    continue;
                }

                if (currentLine.startsWith(robotMotionActProperty) && currentProc.equals(m_ProgramName))
                {
                    outputPos = false;
                    tagName = "";
                    posType = "";
                    posReg  = "";
                    for (ii=ii+1; ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty); ii++)
                    {
                        currentLine = (String)m_V5Properties.get(ii);

                        propertyComponents = currentLine.split(":", 2);
                        propertyValue = propertyComponents[1].trim();
                        
                        if (currentLine.startsWith(eTsk))
                            break;
                        if (currentLine.startsWith(pVarIndexProperty))
                        {
                            posReg = propertyValue;
                        }
                        if (currentLine.startsWith(posTypeProperty))
                        {
                            posType = propertyValue;
                        }
                        if (currentLine.startsWith(jointPosProperty))
                        {
                            String [] values = propertyValue.split(" ");
                            for (int jj=0; jj<numRobotJoints; jj++)
                            {
                                dJointVal = Double.parseDouble(values[jj]);
                                saAxisValues[jj] = JointToEncoderConversion(jj+1, dJointVal);
                            }
                            outputPos = true;
                        }
                        if (currentLine.startsWith(toolProfileProperty))
                        {
                            tool = propertyValue;
                        }
                    }
                    ii--; // backup 1: the next robotMotionActProperty is already read
                    if (posType.equalsIgnoreCase("P") && posReg.length() > 0)
                    {
                        while (posReg.length() < posVarDigits)
                            posReg = "0" + posReg;
                        
                        tagName = posType + posReg;
                    }
                    if (!tagName.equals("") && true == outputPos)
                    {
                        if (tagsOutput.contains(tagName) == false)
                        {
                            tagsOutput.add(tagName);

                            if (tool.equals(lastTool) == false)
                            {
                                if (Pattern.compile("TOOL \\d+").matcher(tool).find() == true)
                                {
                                    bw.write("///TOOL " + tool.split(" ")[1] + nl);
                                }
                                else
                                {
                                    bw.write("///TOOL " + toolProfileList.indexOf(tool) + nl);
                                }
                                lastTool = tool;
                            }
                            
                            bw.write(tagName + "=");
                            bw.write(saAxisValues[0]);
                            for (int jj=1; jj<numRobotJoints; jj++)
                                bw.write("," + saAxisValues[jj]);
                            bw.write(nl);
                            
                            pvaroutput = true;
                        }
                    }
                }
            }
            // output BP variables
            if (auxAxes > 0)
            {
                if (pvaroutput == true && lastTool.equalsIgnoreCase("TOOL 0") == false)
                {
                    bw.write("///TOOL 0" + nl);
                    lastTool = "TOOL 0";
                }
                tagsOutput.clear();

                for (int ii=0; ii<m_V5Properties.size(); ii++)
                {
                    currentLine = (String)m_V5Properties.get(ii);

                    propertyComponents = currentLine.split(":", 2);
                    propertyValue = propertyComponents[1].trim();
                    
                    if (currentLine.startsWith(sTsk))
                    {
                        currentProc = propertyValue;
                        continue;
                    }

                    if (currentLine.startsWith(robotMotionActProperty) && currentProc.equals(m_ProgramName))
                    {
                        outputPos = false;
                        tagName = "";
                        bPosType = "";
                        bPosReg  = "";
                        posType = "";
                        posReg  = "";
                        for (ii=ii+1; ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty); ii++)
                        {
                            currentLine = (String)m_V5Properties.get(ii);

                            propertyComponents = currentLine.split(":", 2);
                            propertyValue = propertyComponents[1].trim();
                            
                            if (currentLine.startsWith(eTsk))
                                break;
                            if (currentLine.startsWith(posTypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("P"))
                                    posType = "P";
                                else
                                    posType = "C";
                            }
                            if (currentLine.startsWith(BTypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("BC"))
                                    bPosType = "BC";
                                else
                                    bPosType = "BP";
                            }
                            if (currentLine.startsWith(bpVarIndexProperty))
                            {
                                bPosReg = propertyValue;
                            }
                            if (currentLine.startsWith(posRegProperty) && propertyValue.length() > 0 && bPosReg.length() == 0)
                            {
                                bPosReg = propertyValue;
                            }
                            if (currentLine.startsWith(pVarIndexProperty) && bPosReg.length() == 0)
                            {
                                bPosReg = propertyValue;
                            }
                            if (currentLine.startsWith(jointPosProperty))
                            {
                                String [] values = propertyValue.split(" ");
                                for (int jj=auxAxisStart; jj<auxAxes+auxAxisStart; jj++)
                                {
                                    dJointVal = Double.parseDouble(values[jj]);
                                    saAxisValues[jj-auxAxisStart] = JointToEncoderConversion(jj+1, dJointVal);
                                }
                            }
                        }
                        ii--; // backup 1: the next robotMotionActProperty is already read
                        if (true == posType.equalsIgnoreCase("P"))
                        {
                            if (bPosType.equalsIgnoreCase("BP") && bPosReg.length() > 0)
                            {
                                while (bPosReg.length() < posVarDigits)
                                    bPosReg = "0" + bPosReg;
                                
                                tagName = bPosType + bPosReg;
                            }
                            if (false == bPosType.equalsIgnoreCase("BC") && posReg.length() > 0)
                            {
                                while (posReg.length() < posVarDigits)
                                    posReg = "0" + posReg;
                                
                                tagName = "BP" + posReg;
                            }
                            if (tagName.equals("") && false == bPosType.equalsIgnoreCase("BC"))
                            {
                                String sNum = String.valueOf(tagNum++);
                                while (sNum.length() < posVarDigits)
                                    sNum = "0" + sNum;
                                tagName = "BP" + sNum;
                            }
                        }
                        if (!tagName.equals(""))
                        {
                            if (tagsOutput.contains(tagName) == false)
                            {
                                tagsOutput.add(tagName);
                                
                                bw.write(tagName + "=");
                                bw.write(saAxisValues[0]);
                                for (int jj=1; jj<auxAxes; jj++)
                                    bw.write("," + saAxisValues[jj]);
                                bw.write(nl);
                            }
                        }
                    }
                }
            }
            // output EX variables
            if (extAxes + posAxes > 0)
            {
                if (auxAxes == 0 && pvaroutput == true)
                {
                    bw.write("///TOOL 0" + nl);
                    lastTool = "TOOL 0";
                }
                
                tagsOutput.clear();

                for (int ii=0; ii<m_V5Properties.size(); ii++)
                {
                    currentLine = (String)m_V5Properties.get(ii);

                    propertyComponents = currentLine.split(":", 2);
                    propertyValue = propertyComponents[1].trim();

                    if (currentLine.startsWith(sTsk))
                    {
                        currentProc = propertyValue;
                        continue;
                    }
                    if (currentLine.startsWith(robotMotionActProperty) && currentProc.equals(m_ProgramName))
                    {
                        outputPos = false;
                        tagName = "";
                        ePosType = "";
                        ePosReg  = "";
                        posType = "";
                        posReg  = "";
                        for (ii=ii+1; ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty); ii++)
                        {
                            currentLine = (String)m_V5Properties.get(ii);

                            propertyComponents = currentLine.split(":", 2);
                            propertyValue = propertyComponents[1].trim();
                            
                            if (currentLine.startsWith(eTsk))
                                break;
                            if (currentLine.startsWith(posTypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("P"))
                                    posType = "P";
                                else
                                    posType = "C";
                            }
                            if (currentLine.startsWith(ETypeProperty))
                            {
                                if (propertyValue.equalsIgnoreCase("EC"))
                                    ePosType = "EC";
                                else
                                    ePosType = "EX";
                            }
                            if (currentLine.startsWith(exVarIndexProperty))
                            {
                                ePosReg = propertyValue;
                            }
                            if (currentLine.startsWith(posRegProperty) && propertyValue.length() > 0 && ePosReg.length() == 0)
                            {
                                ePosReg = propertyValue;
                            }
                            if (currentLine.startsWith(pVarIndexProperty) && ePosReg.length() == 0)
                            {
                                ePosReg = propertyValue;
                            }
                            if (currentLine.startsWith(jointPosProperty))
                            {
                                String [] values = propertyValue.split(" ");
                                for (int jj=extAxisStart; jj<extAxes + posAxes + extAxisStart; jj++)
                                {
                                    dJointVal = Double.parseDouble(values[jj]);
                                    saAxisValues[jj-extAxisStart] = JointToEncoderConversion(jj+1, dJointVal);
                                }
                            }
                        }
                        ii--; // backup 1: the next robotMotionActProperty is already read
                        if (true == posType.equalsIgnoreCase("P"))
                        {
                            if (ePosType.equalsIgnoreCase("EX") && ePosReg.length() > 0)
                            {
                                while (ePosReg.length() < posVarDigits)
                                    ePosReg = "0" + ePosReg;
                                
                                tagName = ePosType + ePosReg;
                            }
                            if (false == ePosType.equalsIgnoreCase("EC") && posReg.length() > 0)
                            {
                                while (posReg.length() < posVarDigits)
                                    posReg = "0" + posReg;
                                
                                tagName = "EX" + posReg;
                            }
                            if (tagName.equals("") && false == ePosType.equalsIgnoreCase("EC"))
                            {
                                String sNum = String.valueOf(tagNum++);
                                while (sNum.length() < posVarDigits)
                                    sNum = "0" + sNum;
                                tagName = "EX" + sNum;
                            }
                        }
                        if (!tagName.equals(""))
                        {
                            if (tagsOutput.contains(tagName) == false)
                            {
                                tagsOutput.add(tagName);
                                
                                bw.write(tagName + "=");
                                bw.write(saAxisValues[0]);
                                for (int jj=1; jj<extAxes+posAxes; jj++)
                                    bw.write("," + saAxisValues[jj]);
                                bw.write(nl);
                            }
                        }
                    }
                }
            }
        }

        catch (IOException e) { 
            e.getMessage();
        }            
    } // WritePositions
    
    public void WriteToolCND(BufferedWriter bw, boolean bRRS)
    {
        String toolProfileName = "ToolProfile.ProfileName:";
        String toolProfilePosition = "ToolProfile.CartesianPosition:";
        String toolProfileMass = "ToolProfile.ToolMass:";
        String toolProfileCentroid = "ToolProfile.ToolCentroid:";
        String toolProfileInertia = "ToolProfile.ToolInertia:";
        String line;
        String [] propertyComponents = {"", ""};
        String propertyValue = "";
        ArrayList toolList         = new ArrayList(24);
        ArrayList toolPosList      = new ArrayList(24);
        ArrayList toolMassList     = new ArrayList(24);
        ArrayList toolCentroidList = new ArrayList(24);
        ArrayList toolInertiaList  = new ArrayList(24);
        int toolNum = 0;
        boolean newTool = false;
        boolean [] toolListSet = new boolean[24];
        for (int ii = 0; ii < toolListSet.length; ii++)
            toolListSet[ii] = false;
        String [] tmpStr6 = {"0", "0", "0", "0", "0", "0"};
        String [] tmpStr3 = {"0", "0", "0"};
        
        for (int ii=0; ii<24; ii++)
        {
            toolList.add(ii, "TOOL " + ii);
            toolPosList.add(ii, tmpStr6);
            toolCentroidList.add(ii, tmpStr3);
            toolMassList.add(ii, "0");
            toolInertiaList.add(ii, tmpStr6);
        }
        
        // go through m_V5Properties and store tool info
        for (int ii=0; ii<m_V5Properties.size(); ii++)
        {
            line = (String)m_V5Properties.get(ii);

            propertyComponents = line.split(":", 2);
            propertyValue = propertyComponents[1];

            if (line.startsWith(toolProfileName))
            {
                toolNum = Integer.parseInt(propertyValue.split(" ")[1]);
                if (toolListSet[toolNum] == false)
                {
                    toolList.set(toolNum, propertyValue);
                    newTool = true;
                }
            }
            if (newTool==true && line.startsWith(toolProfilePosition))
                toolPosList.set(toolNum, propertyValue.split(" "));
            if (newTool==true && line.startsWith(toolProfileMass))
                toolMassList.set(toolNum, propertyValue);
            if (newTool==true && line.startsWith(toolProfileCentroid))
                toolCentroidList.set(toolNum, propertyValue.split(" "));
            if (newTool==true && line.startsWith(toolProfileInertia))
            {
                toolInertiaList.set(toolNum, propertyValue.split(" "));
                toolListSet[toolNum] = true;
                newTool = false;
            }
        }
        
        DecimalFormat format2d = new DecimalFormat("0.00");
        DecimalFormat format3d = new DecimalFormat("0.000");
        
        try
        {
            if (bRRS == false)
                bw.write("DATA FILE START TOOL.CND" + nl);
            
            for (int ii=0; ii<toolList.size(); ii++)
            {
                bw.write("//" + toolList.get(ii) + nl);
                bw.write("///NAME " + toolList.get(ii) + nl);
                
                String [] sXYZWPR = (String [])(toolPosList.get(ii));
                double x = Double.parseDouble(sXYZWPR[0])*1000.0;
                double y = Double.parseDouble(sXYZWPR[1])*1000.0;
                double z = Double.parseDouble(sXYZWPR[2])*1000.0;
                double w = Double.parseDouble(sXYZWPR[3]);
                double p = Double.parseDouble(sXYZWPR[4]);
                double r = Double.parseDouble(sXYZWPR[5]);
                bw.write(format3d.format(x) + ',' + format3d.format(y) + ',' + format3d.format(z) + ',');
                bw.write(format2d.format(w) + ',' + format2d.format(p) + ',' + format2d.format(r) + nl);
                
                sXYZWPR = (String [])toolCentroidList.get(ii);
                x = Double.parseDouble(sXYZWPR[0])*1000.0;
                y = Double.parseDouble(sXYZWPR[1])*1000.0;
                z = Double.parseDouble(sXYZWPR[2])*1000.0;
                bw.write(format3d.format(x) + ',' + format3d.format(y) + ',' + format3d.format(z) + nl);
                
                bw.write(format3d.format(Double.parseDouble((String)toolMassList.get(ii))) + nl);
                
                if (xrc_pl > 0)
                {
                    sXYZWPR = (String [])toolInertiaList.get(ii);
                    x = Double.parseDouble(sXYZWPR[0]);
                    y = Double.parseDouble(sXYZWPR[1]);
                    z = Double.parseDouble(sXYZWPR[2]);
                    bw.write(format3d.format(x) + ',' + format3d.format(y) + ',' + format3d.format(z) + nl);
                    bw.write("0,0" + nl);
                }
            }
            
            if (bRRS == false)
                bw.write("DATA FILE END" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
    } // WriteToolCND

    public String JointToEncoderConversion(int iDOFnumber, double dJointVal)
    {
        double dval = 0;

        if (jointType[iDOFnumber-1].equals("Rotational"))
            dval = dJointVal * resolution[iDOFnumber-1] / 360.0  + zeroOffset[iDOFnumber-1];
        else
        if (jointType[iDOFnumber-1].equals("Translational"))
            dval = dJointVal * resolution[iDOFnumber-1] * 1000.0 + zeroOffset[iDOFnumber-1];

        int iVal;
        if (dval > 0)
            iVal = new Double(dval + 0.5).intValue();
        else
            iVal = new Double(dval - 0.5).intValue();
        
        String sPulse = String.valueOf(iVal);
        return sPulse;
    }
    
    public void WriteRRSLog(String rrsLogFileName)
    {
        if (rrsLogFileName == null)
            return;
        
        String line, currentProc;
        File f = new File(rrsLogFileName);
        
        try
        {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsLogFileName, false), "UTF-8"));
            if (vrcTargetDir.equalsIgnoreCase(f.getParent()) == false)
            {
                bw.write("[FILE_TRANSFER]" + nl);
                for (int ii=0; ii<m_NRLProgram.size(); ii++)
                {
                    line = (String)m_NRLProgram.get(ii);
                    if (line.startsWith(sTsk))
                    {
                        currentProc = line.split(":", 2)[1].trim();
                        bw.write(currentProc + ".JBI=" + vrcTargetDir + nl);
                    }
                }
                if (outputTools == true)
                    bw.write("TOOL.CND=" + vrcTargetDir + nl);
            }

            bw.write("[VRC_PROGRAM]" + nl);
            bw.write(vrcTargetDir + f.separator + mainProgramName + ".JBI=" + mainProgramName + nl);
            
            bw.close();
        }
        catch (IOException e)
        {
            e.getMessage();
        }
    }
    
} // end class
