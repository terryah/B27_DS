//DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2002-2008

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

import javax.xml.transform.TransformerException;

//Java Util classes
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class nachiAXdressup
{
	private static final int JOINT = 1;
	private static final int CARTESIAN = 2;
	private int _PosRegStart = 0;
	private int _PosRegNext = 0;

    private ArrayList m_NRLProgram = new ArrayList(1);
    private boolean j3Linked = false;
    private boolean printLineNumbers = true;
    // array specifies number of joints in each device; up to 4 devices
    private int MechSpec[] = {0, 0, 0, 0};
    // array specifies joint types of each device;
    // 0: Rotational; 1: Translational; up to 6 joints in each device
    private int JointType[][] = {{0, 0, 0, 0, 0, 0},
    							 {1, 0, 0, 0, 0, 0},
    							 {0, 0, 0, 0, 0, 0},
    							 {0, 0, 0, 0, 0, 0}};
    private Hashtable [] MechSpeed, MechTool;
    private String m_ProgramName, mainProgramName;
    private Hashtable jointVal;
    private Hashtable cartVal;
    private Hashtable tagType;
    private static String sTsk = "START TASK:";
    private static String eTsk = "END TASK:";
    private static String nl = "\r\n";
    private String vrcTargetDir = "C:\\tmp";

    //Constructor
    public nachiAXdressup(String OutputFileName, String InputFileName, String LogFileName)
    {
        // OutputFileName: NRLProgramFile.tmp
        // InputFileName: motParam.tmp

        File f = new File(OutputFileName);
        String NRLProgramFileName = f.getParent() + File.separator + "NRLProgramFileOriginal.tmp";
        jointVal = new Hashtable();
        cartVal = new Hashtable();
        tagType = new Hashtable();
        MechSpeed = new Hashtable[] {new Hashtable(), new Hashtable(), new Hashtable()};
        MechTool = new Hashtable[] {new Hashtable(), new Hashtable(), new Hashtable()};

        try
        {
            String line;
            int lineno;

            BufferedReader outputFile = new BufferedReader(new FileReader(OutputFileName)); 
            BufferedReader inputFile  = new BufferedReader(new FileReader(InputFileName));
            BufferedWriter bw = new BufferedWriter(new FileWriter(NRLProgramFileName, false));

            line = outputFile.readLine();
            lineno = 0;
            while (line != null)
            {
                bw.write(line + nl); // write to NRLProgramFileOriginal.tmp

                if (line.equals(""))
                {
                    line = outputFile.readLine();
                    continue;
                }
                m_NRLProgram.add(lineno, line);

                lineno++;
                line = outputFile.readLine();
            } // while
            outputFile.close();
            bw.close();

            // now open the output file and overwrite the contents
            bw = new BufferedWriter(new FileWriter(OutputFileName, false));
            GetPosRegStart(inputFile);
            inputFile = new BufferedReader(new FileReader(InputFileName));
            StorePositions(inputFile, bw);

            boolean bRRS = false;
            WriteVersion(bw);
            WriteProgram(bw, bRRS);
            bw.close();
            
            // Create RRS-II download log file
            if (LogFileName != null)
            {
                bRRS = true;
                String rrsProgramName;
                for (int ii=0; ii<m_NRLProgram.size(); ii++)
                {
                    line = (String)m_NRLProgram.get(ii);
                    if (line.startsWith(sTsk))
                    {
                        m_ProgramName = line.split(":", 2)[1].trim();
                        rrsProgramName = f.getParent() + File.separator + m_ProgramName;
                        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsProgramName, false), "UTF-8"));
                        WriteProgram(bw, bRRS);
                        bw.close();
                    }
                }
                WriteRRSLog(LogFileName);
            }
            
        } // try
        catch (IOException e) {
            e.getMessage();
        }
        catch (ArrayIndexOutOfBoundsException e) {
            e.getMessage();
        }
    }

    public static void main(String [] parameters) throws  NumberFormatException
    {
            String outputFileName = parameters[0];
            String inputFileName = parameters[1];
            String logFileName = null;
            if (parameters.length >= 3)
                logFileName = parameters[2];

            nachiAXdressup downloader = new nachiAXdressup(outputFileName, inputFileName, logFileName);
    }

    public void WriteVersion(BufferedWriter bw)
    {
        try {
            bw.write("VERSION INFO START" + nl);
            bw.write("DELMIA CORP. NRL TEACH Nachi AX DRESSUP DOWNLOADER VERSION 5 RELEASE 27 SP5" + nl);
            bw.write("COPYRIGHT DELMIA CORP. 1986-2018, ALL RIGHTS RESERVED" + nl);
            bw.write("VERSION INFO END" + nl);
        }
        catch (IOException e) {
            e.getMessage();
         }
    }

    public void WriteProgram(BufferedWriter bw, boolean bRRS)
    {
        DecimalFormat jvalFormat = new DecimalFormat("0.#####");
        double dJVal, dJ2 = 0;
        int numLines, lineNum=1, iTagType=JOINT;
        int iProgNum = 1;
        String sProgNum = null;
        _PosRegNext = _PosRegStart; // initialize
        
    	int numDevices = 1; // Robot is device number 1
    	iProgNum = MechSpec.length;
    	for (int ii=0; ii<MechSpec.length; ii++)
    	{
    		if (MechSpec[ii] > 0)
    			numDevices = ii+1;
    		else
    			break;
    	}
    	
        try
        {
            // MOVEJ P, P0, S=100, A=1, H=1
            Pattern p = Pattern.compile("(MOVEX\\s+(?:A=[1-8]P?,)?\\s*(?:AC=[0-3],)?\\s*(?:SM=[0-3],)?\\s*(?:HM=1,)?\\s*(?:M[12](?:J|X)),\\s*(?:P|L|C1|C2),)\\s*(\\w+)(.+)", Pattern.CASE_INSENSITIVE);
            Matcher m;
            String line, currentProc = null, tagName, sTargetType;
            String [] sTagVal = {"", ""};
            String [] sJointVal = {"", ""};
            String hashTblKey = "";
            numLines = m_NRLProgram.size();
            for (int ii=0; ii<numLines; ii++)
            {
                line = (String)m_NRLProgram.get(ii);
                m = p.matcher(line);
                if (m.find() == true)
                {
                    bw.write(LineNumber(lineNum) + m.group(1) + " (");
                    lineNum++;
                    tagName = m.group(2);
                    hashTblKey = currentProc + ":" + tagName;
                    sTargetType = (String)tagType.get(hashTblKey);
                    if (sTargetType == null)
                    {
                    	_PosRegNext++;
                    	tagName = "P" + _PosRegNext;
                    	hashTblKey = currentProc + ":" + tagName;
                    	sTargetType = (String)tagType.get(hashTblKey);
                    }
                    try
                    {
                        if (sTargetType.equalsIgnoreCase("joint"))
                            iTagType = JOINT;
                        else
                            iTagType = CARTESIAN;
                        // both Joint and Cartesian values are stored regardless of target type 
                        sJointVal = ((String)jointVal.get(hashTblKey)).split(" ");
                        sTagVal = ((String)cartVal.get(hashTblKey)).split(" ");
                    }
                    catch (NullPointerException npe) {
                    	try
                    	{
                    		bw.write("DATA FILE END" + nl);
                    		bw.write("ERROR INFO START" + nl);
                    		bw.write("ERROR " + npe.getMessage() + nl);
                    		bw.write("ERROR INFO END" + nl);
                    		
                    		continue;
                    	}
                    	catch (Exception e2) {
                    		e2.getMessage();
                    	}
                    }
                    if (iTagType == CARTESIAN)
                    {
                        int kk;
                        for (int jj = 0; jj < sTagVal.length; jj++)
                        {
                        	kk = jj;
	                        /* cartPos: X Y Z Yaw Pitch Roll
	                         * need to swap Roll and Yaw
	                         */
	                        if (jj == 3)
	                            kk = 5;
	                        if (jj == 5)
	                            kk = 3;
	                        dJVal = Double.valueOf(sTagVal[kk]).doubleValue();
                            if (jj < 3)
                                dJVal *= 1000.0; // mm
	                        bw.write(jvalFormat.format(dJVal));
	                        if (jj < sTagVal.length - 1)
	                            bw.write(", ");
                        }
                    }

                    if (iTagType == JOINT)
                    {
                    	// assuming robot joints are all rotational
                    	for (int iJnt = 0; iJnt < MechSpec[0]; iJnt++)
                    	{
                    		dJVal = Double.valueOf(sJointVal[iJnt]).doubleValue();
                    		
                            if (j3Linked == false)
                            {
                                if (iJnt == 1)
                                    dJ2 = dJVal;
                                if (iJnt == 2)
                                    dJVal = dJVal + dJ2 - 90;
                            }
                    		
	                        bw.write(jvalFormat.format(dJVal));
	                        if (iJnt < MechSpec[0] - 1)
	                            bw.write(", ");
                    	}
                    }
                    bw.write(")" + m.group(3));
                    // Aux devices
                    int jIdx = MechSpec[0];
                	for (int iDev = 1; iDev < numDevices; iDev++)
                	{
                		// output mechanism specification, "M2J, P, (1, 2)" etc.
                		bw.write(", M" + String.valueOf(iDev+1) + "J, P, (");
                		
                		int numJoints = MechSpec[iDev];
                        for (int iJnt = 0; iJnt < numJoints; iJnt++)
                        {
                        	dJVal = Double.valueOf(sJointVal[jIdx]).doubleValue();
                        	if (JointType[iDev][iJnt]==1)
                        		dJVal *= 1000.0;
                        	
                        	bw.write(jvalFormat.format(dJVal));
                        	if (iJnt < numJoints - 1)
                        		bw.write(", ");

                        	jIdx++;
                        }
                        
                        // output mechanism speed & tool specifications, "R=100, H=1" etc.
                        String sSpeed = (String)MechSpeed[iDev-1].get(hashTblKey);
                        String sTool = (String)MechTool[iDev-1].get(hashTblKey);
                        bw.write("), " + sSpeed + ", " + sTool);
                	}
                	
                	bw.write(nl);
                }
                else
                {
                    if (line.startsWith(sTsk))
                    {
                        if (bRRS == false)
                        {
                            currentProc = line.split(":", 2)[1].trim();
                            bw.write("DATA FILE START " + currentProc + nl);
                        }
                    }
                    else
                    if (line.startsWith(eTsk))
                    {
                   		bw.write(LineNumber(lineNum) + "END" + nl);
                        
                        if (bRRS == false)
                            bw.write("DATA FILE END" + nl);
                        
                        lineNum = 1;
                    }
                    else
                    if (line.startsWith("CALLP ") && bRRS == false)
                    {
                   		bw.write(LineNumber(lineNum) + line + nl);
                        lineNum++;
                    }
                    else
                    {
                   		bw.write(LineNumber(lineNum) + line + nl);
                        lineNum++;
                    }
                }
            }
        }
        catch (IOException ioe) {
            ioe.getMessage();
        }
        catch (NullPointerException npe) {
        	try
        	{
        		bw.write("DATA FILE END" + nl);
        		bw.write("ERROR INFO START" + nl);
        		bw.write("ERROR " + npe.getMessage() + nl);
        		bw.write("ERROR INFO END" + nl);
        	}
        	catch (Exception e) {
        		e.getMessage();
        	}
        }
    } // WriteProgram

	private String LineNumber(int LineNum)
    {
    	if (printLineNumbers)
    		return LineNum + " ";
    	else
    		return "";
    }
    
    // Finds the largest PosReg number set and the MotionActivity count.
	// Because "tag name" in NRL Teach window is incremented by 1 from 0, they are shown with small numbers.
	// When motion activity count exceeds PosReg set, using small-number tag name as key finds wrong values from Hash-table. 
    private void GetPosRegStart(BufferedReader br)
    {
    	String posRegProperty = "DNBRobotMotionActivity@PosReg:";
    	String robotMotionActProperty = "DNBRobotMotionActivity.ActivityName:";
    	String sPosReg;
    	int iPosReg, iActivityCount = 0;
    	String [] propertyComponents = {"", ""};
    	String line;
    	    	
        try
        {
            line = br.readLine();
            while (line != null)
            {
                if (line.equals(""))
                {
                    line = br.readLine();
                    continue;
                }

                if (line.startsWith(posRegProperty))
                {
                	propertyComponents = line.split(":", 2);
            		sPosReg = propertyComponents[1];
            		try
            		{
            			iPosReg = Integer.parseInt(sPosReg);
            			if (_PosRegStart < iPosReg)
            				_PosRegStart = iPosReg;
            		}
            		catch (NumberFormatException e) {
            		}
                }
                else if (line.startsWith(robotMotionActProperty))
                	iActivityCount++;
                
                line = br.readLine();
            } // while
            br.close();
        }
        catch (IOException e) {
            e.getMessage();
        }
        
        if (_PosRegStart < iActivityCount)
        	_PosRegStart = iActivityCount;
        
        _PosRegNext = _PosRegStart;
    } // GetPosRegStart
    
    public void StorePositions(BufferedReader br, BufferedWriter bw)
    {
        String robotMotionActProperty = "DNBRobotMotionActivity.ActivityName:";
        String cartPositionProperty   = "DNBRobotMotionActivity.CartesianPosition:";
        String jointPositionProperty  = "DNBRobotMotionActivity.JointPosition:";
        String tagNameProperty        = "DNBRobotMotionActivity.TagName:";
        String posRegProperty         = "DNBRobotMotionActivity@PosReg:";
        String targetTypeProperty     = "DNBRobotMotionActivity.TargetType:";
        String motionProfileName      = "MotionProfile.ProfileName:";
        String toolProfileName        = "ToolProfile.ProfileName:";
        String actAttrMechSpeed       = "DNBRobotMotionActivity@MechanismSpeed";
        String actAttrMechTool        = "DNBRobotMotionActivity@MechanismTool";
        String [][] actAttrMech = {{"", "", ""}, {"", "", ""}}; // row 1: Speed; row 2: Tool
        boolean storeJointVal = false, storeCartVal = false, storeTargetType = false;
        String [] propertyComponents = {"", ""};
        String jointValString = "", cartValString = "";
        String tagName = "";
        String line, currentProc = null;
        String sTargetType = "";
        String CurrentActivityName = "";
    	int numDevices = 0;
    	int numJoints = 0;
        int idx;
        _PosRegNext = _PosRegStart; // initialize

        try
        {
            line = br.readLine();
            while (line != null)
            {
                if (line.equals(""))
                {
                    line = br.readLine();
                    continue;
                }

                if (line.startsWith(robotMotionActProperty))
                {
                    if (storeJointVal || storeCartVal || storeTargetType)
                    {
                        // at start of another Robot Motion Activity
                    	// joint/Cartesian values not stored, PosReg field not created
            			try
            			{
                            bw.write("ERROR INFO START\n");
                            bw.write("ERROR Attribute @PosReg for Robot Motion Activity is missing at " + CurrentActivityName + nl);
                            bw.write("ERROR Please relaunch NRL Teach, then Download" + nl);
                            bw.write("ERROR INFO END\n");
            			}
            			catch (Exception e) {
            				e.getMessage();
            			}
                    }

                    tagName = "";
                    sTargetType = "";
                    storeJointVal = false;
                    storeCartVal = false;
                    storeTargetType = false;
                    // save current activity name for error reporting
                    CurrentActivityName = line.split(":", 2)[1];
                    for (int iRow=0; iRow<2; iRow++)
                    {
                    	for (int iCol=0; iCol<3; iCol++)
                    		actAttrMech[iRow][iCol] = "";
                    }
                    actAttrMech = new String [][] {{"", "", ""}, {"", "", ""}};
                }
                if (line.startsWith(sTsk))
                {
                    currentProc = line.split(":", 2)[1].trim();
                }
                if (line.startsWith(eTsk) && (storeJointVal || storeCartVal || storeTargetType))
                {
                    // at "End Task:" joint/Cartesian values not stored, PosReg field not created
        			try
        			{
                        bw.write("ERROR INFO START\n");
                        bw.write("ERROR Attribute @PosReg for Robot Motion Activity is missing at " + CurrentActivityName + nl);
                        bw.write("ERROR Please relaunch NRL Teach, then Download" + nl);
                        bw.write("ERROR INFO END\n");
        			}
        			catch (Exception e) {
        				e.getMessage();
        			}
                }

                propertyComponents = line.split(":", 2);
                if (line.startsWith(targetTypeProperty))
                {
                    if (tagName.length() > 0)
                    {
                        tagType.put(currentProc + ":" + tagName, propertyComponents[1]);
                        storeTargetType = false;
                    }
                    else
                    {
                        sTargetType = propertyComponents[1];
                        storeTargetType = true;
                    }
                }
                if (line.startsWith(cartPositionProperty))
                {
                    if (tagName.length() > 0)
                    {
                        cartVal.put(currentProc + ":" + tagName, propertyComponents[1]);
                        storeCartVal = false;
                    }
                    else
                    {
                        cartValString = propertyComponents[1];
                        storeCartVal = true;
                    }
                }
                if (line.startsWith(jointPositionProperty))
                {
                    if (tagName.length() > 0)
                    {
                        jointVal.put(currentProc + ":" + tagName, propertyComponents[1]);
                        storeJointVal = false;
                    }
                    else
                    {
                        jointValString = propertyComponents[1];
                        storeJointVal = true;
                    }
                }
                if (line.startsWith(motionProfileName))
                {
                	// store motion profile name to all mechanisms
                	for (int iSpeed=0; iSpeed<3; iSpeed++)
                		actAttrMech[0][iSpeed] = propertyComponents[1];
                }
                else if (line.startsWith(toolProfileName))
                {
                	// store tool profile name to all mechanisms
                	for (int iTool=0; iTool<3; iTool++)
                		actAttrMech[1][iTool] = propertyComponents[1];
                }
                else if (line.startsWith(actAttrMechSpeed))
                {
                	// overwrite with specific mechanism speed
                	String sProperty = propertyComponents[0];
                	sProperty = sProperty.substring(sProperty.length()-1);
                	int iSpeed = Integer.parseInt(sProperty);
                	actAttrMech[0][iSpeed-2] = propertyComponents[1];
                }
                else if (line.startsWith(actAttrMechTool))
                {
                	// overwrite with specific mechanism tool
                	String sProperty = propertyComponents[0];
                	sProperty = sProperty.substring(sProperty.length()-1);
                	int iTool = Integer.parseInt(sProperty);
                	actAttrMech[1][iTool-2] = propertyComponents[1];
                }
                	
                if (line.startsWith(tagNameProperty))
                {
                    tagName = propertyComponents[1];
                    if (storeJointVal)
                    {
                        jointVal.put(currentProc + ":" + tagName, jointValString);
                        storeJointVal = false;
                    }
                    if (storeCartVal)
                    {
                        cartVal.put(currentProc + ":" + tagName, cartValString);
                        storeCartVal = false;
                    }
                    if (storeTargetType)
                    {
                        tagType.put(currentProc + ":" + tagName, sTargetType);
                        storeTargetType = false;
                    }
                    
                    for (int ii=0; ii<3; ii++)
                    {
                        MechSpeed[ii].put(currentProc + ":" + tagName, actAttrMech[0][ii]);
                        MechTool[ii].put(currentProc + ":" + tagName, actAttrMech[1][ii]);
                    }
                }
                if (line.startsWith(posRegProperty))
                {
                	// Joint and Cartesian targets need @PosReg number for target name.
                	// Catch exception if it's not a number or blank
                	// storeTargetType is RESET when TagName is found.
                	if (storeTargetType)
                	{
                		String sPosReg = propertyComponents[1];
                		try
                		{
                			Integer.parseInt(sPosReg);
                		}
                		catch (NumberFormatException e) {
                			try
                			{
                                bw.write("ERROR INFO START\n");
                                String tmpTargetType = sTargetType; // joint | cartesian
                                tmpTargetType.substring(0).toUpperCase();
                                String sMsg = "must be a number.";
                                if (sPosReg.length()==0)
                                	sMsg = "cannot be empty.";
                                bw.write("ERROR Attribute @PosReg for " + tmpTargetType + " Target " + sMsg + nl);
                                bw.write("ERROR at " + CurrentActivityName + nl);
                                bw.write("ERROR INFO END\n");
                                
                                // auto generate PosReg number
                                _PosRegNext++;
                                propertyComponents[1] = String.valueOf(_PosRegNext);
                			}
                			catch (Exception e2) {
                				e.getMessage();
                			}
                		}
                	}
                	
                    tagName = "P" + propertyComponents[1];
                    if (storeJointVal)
                    {
                        jointVal.put(currentProc + ":" + tagName, jointValString);
                        storeJointVal = false;
                    }
                    if (storeCartVal)
                    {
                        cartVal.put(currentProc + ":" + tagName, cartValString);
                        storeCartVal = false;
                    }
                    if (storeTargetType)
                    {
                        tagType.put(currentProc + ":" + tagName, sTargetType);
                        storeTargetType = false;
                    }
                    if (!sTargetType.equals("tag")) // stored already for Tag target type
                    {
                        for (int ii=0; ii<3; ii++)
                        {
                            MechSpeed[ii].put(currentProc + ":" + tagName, actAttrMech[0][ii]);
                            MechTool[ii].put(currentProc + ":" + tagName, actAttrMech[1][ii]);
                        }
                    }
                }
                if (line.startsWith("DNBRobotTask.Name:")) // task NRL displays
                    mainProgramName = propertyComponents[1];

                if (line.startsWith("ResourceParameter.Joint3Linked"))
                    j3Linked = new Boolean(propertyComponents[1]).booleanValue();
                if (line.startsWith("ResourceParameter.PrintLineNumbers"))
                {
                	printLineNumbers = new Boolean(propertyComponents[1]).booleanValue();
                }
                if (line.startsWith("RobotJoint."))
                {
                	// RobotJoint.1:Joint 1,Rotational
                	propertyComponents = line.split(":|,|\\s", 4);
            		String sJoint = propertyComponents[2];
            		int iJoint;
            		try
            		{
            			iJoint = Integer.parseInt(sJoint);
        				MechSpec[0] = iJoint;
        				if (propertyComponents[3].equals("Rotational"))
        					JointType[0][iJoint-1] = 0;
        				else
        					JointType[0][iJoint-1] = 1;
            		}
            		catch (NumberFormatException e) {
            		}
                }
                if (line.startsWith("AuxJoint."))
                {
                	// AuxJoint.8:Joint 2,Rotational,EndOfArmTooling
                	propertyComponents = line.split(":|,|\\s", 5);
            		String sJoint = propertyComponents[2];
            		int iJoint;
            		try
            		{
            			iJoint = Integer.parseInt(sJoint);
            			if (iJoint == 1) 
            			{
            				numDevices++;
            				numJoints = 1;
            			}
            			else
            				numJoints++;
            			
            			if (propertyComponents[3].equals("Rotational"))
            				JointType[numDevices][numJoints-1] = 0;
            			else
            				JointType[numDevices][numJoints-1] = 1;
            			
            			try
            			{
            				MechSpec[numDevices] = numJoints;
            			}
            			catch (ArrayIndexOutOfBoundsException e) {
            				e.getMessage();
            			}
            		}
            		catch (NumberFormatException e) {
        				e.getMessage();
            		}
                }
                
                if (line.startsWith("ResourceParameter.VRCTargetDirectory:"))
                {
                    idx = line.indexOf(':');
                    vrcTargetDir = line.substring(idx+1);
                    File vrcF = new File(vrcTargetDir);
                    vrcTargetDir = vrcF.getPath();
                }

                line = br.readLine();
            } // while                    
            br.close();
        }

        catch (IOException e) {
            e.getMessage();
        }
    } // StorePositions

    public void WriteRRSLog(String rrsLogFileName)
    {
        if (rrsLogFileName == null)
            return;

        String line, currentProc;
        File f = new File(rrsLogFileName);
        
        try
        {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsLogFileName, false), "UTF-8"));
            if (vrcTargetDir.equalsIgnoreCase(f.getParent()) == false)
            {
                bw.write("[FILE_TRANSFER]" + nl);
                for (int ii=0; ii<m_NRLProgram.size(); ii++)
                {
                    line = (String)m_NRLProgram.get(ii);
                    if (line.startsWith(sTsk))
                    {
                        currentProc = line.split(":", 2)[1].trim();
                        bw.write(currentProc + "=" + vrcTargetDir + nl);
                    }
                }
            }
            
            bw.write("[VRC_PROGRAM]" + nl);
            bw.write(vrcTargetDir + File.separator + mainProgramName + "=" + mainProgramName + nl);
            
            bw.close();
        }
        catch (IOException e)
        {
            e.getMessage();
        }
    }
    
} // end class
