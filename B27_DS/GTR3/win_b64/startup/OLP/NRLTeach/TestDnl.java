// COPYRIGHT DELMIA CORP. 2006
//===================================================================
//  Author  Date        Purpose
//  ------  ----        -------
//  lhg     11/22/2006  Initial implementation
//===================================================================

import java.util.*;
import java.io.*;

public class TestDnl {

    // m_JointPositions[0] contains (typically) 6 radian values of joint angles
    private Vector m_JointPositions;
    private Vector m_MotionTypes;
    private Vector m_SpeedPercent;

    public void mytrace(String str)
    {
        System.out.println(str);
    }

    //Constructor
    public TestDnl(String NrlProgramName, String MotParamName, String LogFileName )  {
        m_JointPositions = new Vector();
        m_MotionTypes = new Vector();
        m_SpeedPercent = new Vector();

        try {
            BufferedReader MotParamFile = new BufferedReader(new InputStreamReader(new FileInputStream(MotParamName), "UTF-8"));

            mytrace("parameters: " + NrlProgramName + ", " + MotParamName + ", " + LogFileName);
            mytrace("motparamfile: " + MotParamName);
            String line = MotParamFile.readLine();
            int lineno = 0;
            while (line != null) 
            {
                if (line.equals("")) {
                    line = MotParamFile.readLine();
                    continue;
                }

                String[] propertyComponents=line.split(":");
                if (propertyComponents.length >= 2 )
                {
                    if (propertyComponents[0].equals("DNBRobotMotionActivity.ActivityName") )
                    {
                    }
                    else if (propertyComponents[0].equals("MotionProfile.ProfileName") )
                    {
                        String propertyValue = propertyComponents[1];

                        m_SpeedPercent.addElement(propertyValue);

                    }
                    else if (propertyComponents[0].equals("DNBRobotMotionActivity.MotionType") )
                    {
                        String propertyValue = propertyComponents[1];

                        m_MotionTypes.addElement(propertyValue);
                    }
                    else if (propertyComponents[0].equals("DNBRobotMotionActivity.JointPosition") )
                    {
                        String propertyValue = propertyComponents[1];

                        m_JointPositions.addElement(propertyValue);
                    }
                }
                lineno++;
                line = MotParamFile.readLine();
            } // while
            mytrace("writing it to m_JointPositions, a vector");

            MotParamFile.close();

            BufferedWriter dbg = null;
            // now open the output file and overwrite the contents           NrlProgramName
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(NrlProgramName, false), "UTF-8"));
            WriteProgram(bw, dbg);
            bw.close();
            
            // create log file
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(LogFileName, false), "UTF-8"));
            WriteLog(NrlProgramName, bw, dbg);
            bw.close();
        } // try
        catch (IOException e) { 
            e.getMessage();
        }            
    }

    public static void main(String [] parameters) throws  NumberFormatException {

        if (parameters.length >= 3)
        {
            String NrlProgramName = parameters[0];
            String MotParamName = parameters[1];
            String LogFileName = parameters[2];

            TestDnl uploader = new TestDnl( NrlProgramName, MotParamName, LogFileName);
        }

    }

    public void WriteLog(String NrlProgramName, BufferedWriter bw, BufferedWriter dbg)
    {
        try { 
            int lastBSlash = NrlProgramName.lastIndexOf("\\");
            String NrlProgNameOnly = NrlProgramName.substring(lastBSlash + 1);
            String VrcTargetDir = new String("E:\\vrc");

            bw.write("[FILE_TRANSFER]\r\n");
            bw.write(NrlProgNameOnly + "=" + VrcTargetDir + "\r\n");
            bw.write("[VRC_PROGRAM]\r\n");
            bw.write(VrcTargetDir + "\\" + NrlProgNameOnly + "=main_prog\r\n");
            
            if (null != dbg)
            {
                dbg.write("[FILE_TRANSFER]\r\n");
                dbg.write(NrlProgNameOnly + "=" + VrcTargetDir + "\r\n");
                dbg.write("[VRC_PROGRAM]\r\n");
                dbg.write(VrcTargetDir + "\\" + NrlProgNameOnly + "=main_prog\r\n");
            }

            mytrace("[FILE_TRANSFER]");
            mytrace(NrlProgNameOnly + "=" + VrcTargetDir);
            mytrace("[VRC_PROGRAM]");
            mytrace(VrcTargetDir + "\\" + NrlProgNameOnly + "=main_prog");
        }
        catch (IOException e) { 
            e.getMessage();
        }                       
    }        

    public void WriteProgram(BufferedWriter bw, BufferedWriter dbg)
    {
        try { 
            bw.write("P main_prog\r\n");
            bw.write("N SHORTEST_ANGLES\r\n");

            if (null != dbg)
            {
                dbg.write("P main_prog\r\n");
                dbg.write("N SHORTEST_ANGLES\r\n");
            }

            int sJP = m_JointPositions.size();
            int sMT = m_MotionTypes.size();
            int sSP = m_SpeedPercent.size();
            if (sJP == sMT && sMT == sSP)
            {
                Double dratio = new Double(3.14159 / 180.0);
                float ratio = dratio.floatValue();
                for (int ii = 0; ii < m_MotionTypes.size(); ii++)
                {
                    String type = (String)m_MotionTypes.get(ii);
                    if (type.equals("joint"))
                    {
                        String whole = (String)m_SpeedPercent.get(ii);
                        int pidx = whole.indexOf("%");
                        if (pidx > 1)
                        {
                            String percent = whole.substring(0, pidx);
                            float val = Float.parseFloat(percent);
                            if (val > 0)
                            {
                                val /= 100;
                                Float fo = new Float(val);
                                percent = fo.toString();
                                // J percent (jointvalues)
                                String [] vStr = ((String)m_JointPositions.get(ii)).split(" ");
                                float [] vFlt = new float[vStr.length];
                                int nnn = 0;
                                for (nnn = 0; nnn < vFlt.length; ++nnn)
                                {
                                    vFlt[nnn] = Float.parseFloat(vStr[nnn]);
                                    vFlt[nnn] *= ratio;
                                    fo = new Float(vFlt[nnn]);
                                    vStr[nnn] = fo.toString();
                                }

                                String stuff = new String("J " + percent);
                                for (nnn = 0; nnn < vFlt.length; ++nnn)
                                {
                                    stuff += " " + vFlt[nnn];
                                }

                                bw.write(stuff + "\r\n");
                                if (null != dbg)
                                {
                                    dbg.write(stuff + "\r\n");
                                }
                            }
                        }
                        else
                        {
                            mytrace(type + "not a relative speed");
                        }

                    }
                    else
                    {
                        mytrace(type + ": Not a joint motion");
                    }
                }
            }
            else
            {
                mytrace("**** sizes of motion type/speed/angle not same");
            }
            // end of the DNL program
            bw.write("E\r\n");
            if (null != dbg)
            {
                dbg.write("E\r\n");
            }
        }
        catch (IOException e) { 
            e.getMessage();
        }                       
    }        

} // end class
