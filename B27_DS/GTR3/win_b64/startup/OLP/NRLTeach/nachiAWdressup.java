//DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2002-2008

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

import javax.xml.transform.TransformerException;

//Java Util classes
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class nachiAWdressup
{
    Hashtable progNameNumHTab = new Hashtable();
    int progNum = 1;

    private ArrayList m_NRLProgram = new ArrayList(1);
    private boolean j3Linked = false;
    private String m_ProgramName, mainProgramName;
    private Hashtable jVal;
    private static String sTsk = "START TASK:";
    private static String eTsk = "END TASK:";
    private static String nl = "\r\n";
    private String vrcTargetDir = "C:\\tmp";
    private String robotName = null;

    //Constructor
    public nachiAWdressup(String OutputFileName, String InputFileName, String LogFileName)
    {
        // OutputFileName: NRLProgramFile.tmp
        // InputFileName: motParam.tmp

        File f = new File(OutputFileName);
        String NRLProgramFileName = f.getParent() + f.separator + "NRLProgramFileOriginal.tmp";
        jVal = new Hashtable();

        try
        {
            String line;
            int lineno;

            BufferedReader outputFile = new BufferedReader(new FileReader(OutputFileName)); 
            BufferedReader inputFile  = new BufferedReader(new FileReader(InputFileName));
            BufferedWriter bw = new BufferedWriter(new FileWriter(NRLProgramFileName, false));

            line = outputFile.readLine();
            lineno = 0;
            while (line != null)
            {
                bw.write(line + nl); // write to NRLProgramFileOriginal.tmp

                if (line.equals(""))
                {
                    line = outputFile.readLine();
                    continue;
                }
                m_NRLProgram.add(lineno, line);

                lineno++;
                line = outputFile.readLine();
            } // while
            outputFile.close();
            bw.close();

            StorePositions(inputFile);

            // now open the output file and overwrite the contents
            bw = new BufferedWriter(new FileWriter(OutputFileName, false));

            boolean bRRS = false;
            WriteVersion(bw);
            WriteProgram(bw, bRRS);
            bw.close();
            
            // Create RRS-II download log file
            if (LogFileName != null)
            {
                bRRS = true;
                String rrsProgramName;
                for (int ii=0; ii<m_NRLProgram.size(); ii++)
                {
                    line = (String)m_NRLProgram.get(ii);
                    if (line.startsWith(sTsk))
                    {
                        m_ProgramName = line.split(":", 2)[1].trim();
                        rrsProgramName = f.getParent() + f.separator + m_ProgramName + ".prg";
                        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsProgramName, false), "UTF-8"));
                        WriteProgram(bw, bRRS);
                        bw.close();
                    }
                }
                WriteRRSLog(LogFileName);
            }
            
        } // try
        catch (IOException e) {
            e.getMessage();
        }
        catch (ArrayIndexOutOfBoundsException e) {
            e.getMessage();
        }
    }

    public static void main(String [] parameters) throws  NumberFormatException
    {
            String outputFileName = parameters[0];
            String inputFileName = parameters[1];
            String logFileName = null;
            if (parameters.length >= 3)
                logFileName = parameters[2];

            nachiAWdressup downloader = new nachiAWdressup(outputFileName, inputFileName, logFileName);
    }

    public void WriteVersion(BufferedWriter bw)
    {
        try {
            bw.write("VERSION INFO START" + nl);
            bw.write("DELMIA CORP. NRL TEACH Nachi AW DRESSUP DOWNLOADER VERSION 5 RELEASE 19 SP2" + nl);
            bw.write("COPYRIGHT DELMIA CORP. 1986-2008, ALL RIGHTS RESERVED" + nl);
            bw.write("VERSION INFO END" + nl);
        }
        catch (IOException e) {
            e.getMessage();
         }
    }

    public void WriteProgram(BufferedWriter bw, boolean bRRS)
    {
        DecimalFormat jvalFormat = new DecimalFormat("0.#####");
        double dJVal, dJ2 = 0;
        int numLines, lineNum=1;
        String sProgNum = null;

        try
        {
            // MOVEJ P, P0, S=100, A=1, H=1
            Pattern p = Pattern.compile("MOVEJ\\s+", Pattern.CASE_INSENSITIVE);
            Matcher m;
            String line, currentProc = null, tagName, jStr;
            String calledProc = null;
            String [] jvalString;
            String [] lineParts = {"", "", "", ""};
            numLines = m_NRLProgram.size();
            for (int ii=0; ii<numLines; ii++)
            {
                line = (String)m_NRLProgram.get(ii);
                m = p.matcher(line);
                if (m.find() == true)
                {
                    lineParts = line.split("[, \\t]+", 4);
                    bw.write(lineNum + " " + lineParts[0] + " " + lineParts[1] + ", (");
                    lineNum++;
                    tagName = lineParts[2];
                    jvalString = ((String)jVal.get(currentProc + ":" + tagName)).split(" ");
                    for (int jj = 0; jj < jvalString.length; jj++)
                    {
                        dJVal = Double.valueOf(jvalString[jj]).doubleValue();
                        if (j3Linked == false)
                        {
                            if (jj == 1)
                                dJ2 = dJVal;
                            if (jj == 2)
                                dJVal = dJVal + dJ2 - 90;
                        }
                        bw.write(jvalFormat.format(dJVal));
                        if (jj < jvalString.length - 1)
                            bw.write(", ");
                    }

                    bw.write("), " + lineParts[3] + nl);
                }
                else
                {
                    if (line.startsWith(sTsk))
                    {
                        if (bRRS == false)
                        {
                            bw.write("DATA FILE START ");
                            currentProc = line.split(":", 2)[1].trim();
                            if (currentProc.matches("\\p{ASCII}+-A\\.\\d{3}"))
                            {
                                bw.write(currentProc);
                                sProgNum = getProgramNum(currentProc);
                            }
                            else
                            if (robotName == null)
                            {
                                bw.write(currentProc + ".prg");
                            }
                            else
                            {
                                sProgNum = getProgramNum(currentProc);
                                while (sProgNum.length() < 3)
                                    sProgNum = "0" + sProgNum;
                                bw.write(robotName + "-A." + sProgNum);
                            }
                            bw.write(nl);
                        }
                    }
                    else
                    if (line.startsWith(eTsk))
                    {
                        bw.write(lineNum + " END" + nl);
                        if (bRRS == false)
                            bw.write("DATA FILE END" + nl);
                        
                        lineNum = 1;
                    }
                    else
                    if (line.startsWith("CALLP ") && bRRS == false)
                    {
                        calledProc = line.split(" ", 2)[1].trim();
                        sProgNum = getProgramNum(calledProc);
                        bw.write(lineNum + " " + "CALLP " + sProgNum + nl);
                        lineNum++;
                    }
                    else
                    {
                        bw.write(lineNum + " " + line + nl);
                        lineNum++;
                    }
                }
            }
        }
        catch (IOException e) {
            e.getMessage();
        }
    } // WriteProgram

    public void StorePositions(BufferedReader br)
    {
        String robotMotionActProperty = "DNBRobotMotionActivity.ActivityName:";
        String jointPositionProperty  = "DNBRobotMotionActivity.JointPosition:";
        String tagNameProperty        = "DNBRobotMotionActivity.TagName:";
        String posRegProperty         = "DNBRobotMotionActivity@PosReg:";
        boolean storeJVal = false;
        String [] propertyComponents = {"", ""};
        String jValString = "";
        String tagName = "";
        String line, currentProc = null;
        int idx;

        try
        {
            line = br.readLine();
            while (line != null)
            {
                if (line.equals(""))
                {
                    line = br.readLine();
                    continue;
                }

                if (line.startsWith(robotMotionActProperty))
                {
                    tagName = "";
                    storeJVal = false;
                }
                if (line.startsWith(sTsk))
                {
                    currentProc = line.split(":", 2)[1].trim();
                }

                propertyComponents = line.split(":", 2);
                if (line.startsWith(jointPositionProperty))
                {
                    if (tagName.length() > 0)
                    {
                        jVal.put(currentProc + ":" + tagName, propertyComponents[1]);
                        storeJVal = false;
                    }
                    else
                    {
                        jValString = propertyComponents[1];
                        storeJVal = true;
                    }
                }
                if (line.startsWith(tagNameProperty))
                {
                    tagName = propertyComponents[1];
                    if (storeJVal)
                    {
                        jVal.put(currentProc + ":" + tagName, jValString);
                        storeJVal = false;
                    }
                }
                if (line.startsWith(posRegProperty))
                {
                    tagName = "P" + propertyComponents[1];
                    if (storeJVal)
                    {
                        jVal.put(currentProc + ":" + tagName, jValString);
                        storeJVal = false;
                    }
                }
                if (line.startsWith("DNBRobotTask.Name:")) // task NRL displays
                    mainProgramName = propertyComponents[1].replaceAll("\\.", "");

                if (line.startsWith("ResourceParameter.Joint3Linked"))
                    j3Linked = new Boolean(propertyComponents[1]).booleanValue();

                if (line.startsWith("ResourceParameter.VRCTargetDirectory:"))
                {
                    idx = line.indexOf(':');
                    vrcTargetDir = line.substring(idx+1);
                    File vrcF = new File(vrcTargetDir);
                    vrcTargetDir = vrcF.getPath();
                }
                
                if (line.startsWith("ResourceParameter.ResourceName"))
                    robotName = propertyComponents[1];
                
                line = br.readLine();
            } // while                    
            br.close();
        }

        catch (IOException e) { 
            e.getMessage();
        }            
    }

    public void WriteRRSLog(String rrsLogFileName)
    {
        if (rrsLogFileName == null)
            return;

        String line, currentProc;
        File f = new File(rrsLogFileName);
        
        try
        {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsLogFileName, false), "UTF-8"));
            if (vrcTargetDir.equalsIgnoreCase(f.getParent()) == false)
            {
                bw.write("[FILE_TRANSFER]" + nl);
                for (int ii=0; ii<m_NRLProgram.size(); ii++)
                {
                    line = (String)m_NRLProgram.get(ii);
                    if (line.startsWith(sTsk))
                    {
                        currentProc = line.split(":", 2)[1].trim();
                        bw.write(currentProc + ".prg=" + vrcTargetDir + nl);
                    }
                }
            }
            
            bw.write("[VRC_PROGRAM]" + nl);
            bw.write(vrcTargetDir + f.separator + mainProgramName + ".prg=" + mainProgramName + nl);
            
            bw.close();
        }
        catch (IOException e)
        {
            e.getMessage();
        }
    }
    
    public String getProgramNum(String progName)
    {
        if (progNameNumHTab.containsKey(progName) == false)
        {
            while (progNameNumHTab.containsValue(String.valueOf(progNum)) == true)
                progNum++;
            progNameNumHTab.put(progName, String.valueOf(progNum++));
        }
        return (String)progNameNumHTab.get(progName);
    }
} // end class
