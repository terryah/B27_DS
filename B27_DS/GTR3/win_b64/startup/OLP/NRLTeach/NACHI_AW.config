# Example CCF file for creation of a NACHI AW style teach pendant
#
# set the dressup downloader executable...in this case a jar file
# could be any executable
# arguments are:
#            Output filename
#            profile/motion filename
DOWNLOAD_EXECUTABLE = "nachiAWdressup.jar"
#
# Program execution group
GROUP = "Execute"
GROUP_LOCATION =  (1,1)
#  Single Step from current line 
	BUTTON = "Step Fwd"
		BUTTON_LOCATION =  (1,1)
		BUTTON_CONFIGURATION=PERFORM_STEP_FORWARD
	BUTTON_END

# Single Step backward from current line
	BUTTON = "Step Bwd"
		BUTTON_LOCATION =  (2,1)
		BUTTON_CONFIGURATION=PERFORM_STEP_BACKWARD
	BUTTON_END

# Execute to end of program from current line
	BUTTON = "Run"
		BUTTON_LOCATION =  (3,1)
		BUTTON_CONFIGURATION =PERFORM_RUN
	BUTTON_END

# Stop Execution of run or step
	BUTTON = "Hold"
		BUTTON_LOCATION =  (4,1)
		BUTTON_CONFIGURATION=PERFORM_PAUSE
	BUTTON_END
GROUP_END

VARIABLE hide = TRUE

# Declare <termType> and associate it with
# AccuracyProfile.AccuracyValue
# AccuracyProfile.AccuracyType
# AccuracyProfile.FlyByMode
#:AccuracyProfile.AccuracyValue{"0", "14.29", "28.58", "42.87", "57.16", "71.45", "85.74", "100"}
ENUM termType={"A=1", "A=2", "A=3", "A=4", "A=5", "A=6", "A=7", "A=8"}
:AccuracyProfile.AccuracyValue{"0", "14", "28", "42", "57", "71", "85", "100"}
ENUM flyBy = {"", "P"}
:AccuracyProfile.FlyByMode{ON, OFF}
:AccuracyProfile.AccuracyType{ACCURACY_TYPE_SPEED, ACCURACY_TYPE_SPEED_OFF}
# Declare accuracyValue and associate it with AccuracyProfile.AccuracyValue
VARIABLE accuracyValue=0:AccuracyProfile.AccuracyValue
VARIABLE accuracyValueDistance=0:AccuracyProfile.AccuracyValue*UNITS_DISTANCE_MM
# Setup AccuracyProfile naming convention A=1, A=2, A=8P, etc...
ACCURACYPROFILE_CONFIGURATION = <termType> <flyBy>

# Declare <motionType> and associate it with DNBRobotMotionActivity.MotionType
# ENUM motionType={"P", "L", "C", "C Via"}:DNBRobotMotionActivity.MotionType{MOTION_TYPE_JOINT, MOTION_TYPE_LINEAR, MOTION_TYPE_CIRCULAR, MOTION_TYPE_CIRCULAR_VIA}

# <absoluteSpeedValue> <timeValue>
# VARIABLE speedValueJoint=100:MotionProfile.SpeedValue
VARIABLE speedValueLinear=2300:MotionProfile.SpeedValue*UNITS_SPEED_MM_SEC
VARIABLE speedValuePercentLinear=2300:MotionProfile.SpeedValue*23.0
# VARIABLE speedValueLinearPercent=100:MotionProfile.SpeedValue*50.0
VARIABLE timeValue = 1:MotionProfile.SpeedValue
ENUM storeMotionBasis={UNITS_SPEED_MM_SEC, UNITS_SPEED_SEC, UNITS_PERCENT}:MotionProfile.MotionBasis{MOTION_BASIS_ABSOLUTE, MOTION_BASIS_TIME, MOTION_BASIS_PERCENT}

# <motionBasis>
ENUM motionBasis = {"S=", "T="}:MotionProfile.MotionBasis{MOTION_BASIS_ABSOLUTE, MOTION_BASIS_TIME}
# Setup MotionProfile naming convention
MOTIONPROFILE_CONFIGURATION = <motionBasis>
?motionBasis == "S=" AND ?storeMotionBasis == UNITS_PERCENT <speedValuePercentLinear>
?motionBasis == "S=" AND ?storeMotionBasis == UNITS_SPEED_MM_SEC <speedValueLinear>
?motionBasis == "T=" <timeValue>

# Declare <positionIndex> and associate it with DNBRobotMotionActivity 
VARIABLE positionIndex="0%01d":DNBRobotMotionActivity@"PosReg"
# Setup tag name and index
TAG_NAME_CONFIGURATION = "P" <positionIndex>
INDEX_INC "DNBRobotMotionActivity.TagName" = 1:DNBRobotMotionActivity@"PosReg"
#INDEX_INC "ToolProfile.ProfileName" = 1:ToolProfile

# Setup Tool and ObjectFrame profile naming convention
VARIABLE toolIndex = 1:ToolProfile
TOOLPROFILE_LABEL = "H="
TOOLPROFILE_CONFIGURATION = "H=" <toolIndex>
#VARIABLE objectFrameIndex = 0:ObjectFrameProfile
#OBJECTFRAMEPROFILE_LABEL = "OBJECTFRAME"
#OBJECTFRAMEPROFILE_CONFIGURATION = "OBJECTFRAME " <objectFrameIndex>
OBJECTFRAMEPROFILE_CONFIGURATION = "Default"

# Create Motion Group with two buttons 
GROUP = "Motion Instructions"
GROUP_LOCATION = (1,2)
    #  Button for e.g. <MOVEJ interpolation, pose, speed, accuracy, acceleration, smoothness, tool>
    BUTTON = "MOVEJ P"
		BUTTON_LOCATION = (1,1)
		BUTTON_CONFIGURATION = "MOVEJ P, "
			TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_JOINT
		ObjectFrameProfile.ProfileName = "Default"
		USE_MOTION_OPTIONS
    BUTTON_END
    BUTTON = "MOVEJ L"
		BUTTON_LOCATION = (2,1)
		BUTTON_CONFIGURATION = "MOVEJ L, "
			TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_LINEAR
		ObjectFrameProfile.ProfileName = "Default"
		USE_MOTION_OPTIONS
    BUTTON_END
    BUTTON = "MOVEJ C"
		BUTTON_LOCATION = (3,1)
		BUTTON_CONFIGURATION = "MOVEJ C, "
			TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_CIRCULAR
		ObjectFrameProfile.ProfileName = "Default"
		USE_MOTION_OPTIONS
    BUTTON_END
    BUTTON = "MOVEJ C Via"
		BUTTON_LOCATION = (4,1)
		BUTTON_CONFIGURATION = "MOVEJ C, "
			TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_CIRCULAR_VIA
		ObjectFrameProfile.ProfileName = "Default"
		USE_MOTION_OPTIONS
    BUTTON_END
GROUP_END

# the following group is a special group that will never be displayed in the teach dialog groups but can be used to handle special conditions such as
# a LINEAR or CIRUCLAR MOVE with a percentage speed (not allowed in MOTOMAN) or a JOINT move with an ABSOLUTE speed (also not allowed in MOTOMAN)
# these situations require assignments of the MotionType/MotionBasis (in this case) which do not comply with MOTOMAN standards and therefore are blocked
# from ever being displayed in the Teach dialog groups....it is important to point out that these buttons ARE used to generate NRL text from V5 entities
GROUP = "GENERATE_FROM_TASK_ONLY"
GROUP_LOCATION = (1,2)
    #  Button for e.g. <MOVEJ interpolation, pose, speed, accuracy, acceleration, smoothness, tool>
    BUTTON = "MOVEJ P Alt"
        BUTTON_LOCATION = (1,1)
		BUTTON_CONFIGURATION = "MOVEJ P, " TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_JOINT
		ObjectFrameProfile.ProfileName = "Default"
        USE_MOTION_OPTIONS
    BUTTON_END
    BUTTON = "MOVEJ L Alt"
        BUTTON_LOCATION = (2,1)
		BUTTON_CONFIGURATION = "MOVEJ L, " TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_LINEAR
		ObjectFrameProfile.ProfileName = "Default"
        USE_MOTION_OPTIONS
    BUTTON_END
    BUTTON = "MOVEJ C Alt"
        BUTTON_LOCATION = (3,1)
		BUTTON_CONFIGURATION = "MOVEJ C, " TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_CIRCULAR
		ObjectFrameProfile.ProfileName = "Default"
        USE_MOTION_OPTIONS
    BUTTON_END
    BUTTON = "MOVEJ C Via Alt"
        BUTTON_LOCATION = (4,1)
		BUTTON_CONFIGURATION = "MOVEJ C, " TAG_NAME_CONFIGURATION ", "
			MOTIONPROFILE_CONFIGURATION ", "
			ACCURACYPROFILE_CONFIGURATION ", "
			TOOLPROFILE_CONFIGURATION
		DNBRobotMotionActivity.MotionType = MOTION_TYPE_CIRCULAR_VIA
		ObjectFrameProfile.ProfileName = "Default"
        USE_MOTION_OPTIONS
    BUTTON_END
GROUP_END

# Declare VARIABLE and ENUM data for signal buttons

# create <signalNumber> and <waitsignalNumber> VARIABLE
VARIABLE signalNumber     = 1:DNBSetSignalActivity.PortNumber
VARIABLE waitsignalNumber = 1:DNBWaitSignalActivity.PortNumber

# create <outputSignal> ENUM
ENUM outputSignal = {"1", "0"}:DNBSetSignalActivity.SignalValue{ON, OFF}
# <signalDuration>
VARIABLE signalDuration = 1.0:DNBSetSignalActivity.SignalDuration
VARIABLE waitSignalDuration = 1.0:DNBWaitSignalActivity.WaitSignalMaxTime

# <delayTime>
VARIABLE delayTime=0.5:DelayActivity.WaitTime

SIGNALOUTPUT_CONFIGURATION = "Output" <signalNumber>
SIGNALINPUT_CONFIGURATION  = "Input" <waitsignalNumber>

# create the Signals Group
GROUP = "I/O Signals"
GROUP_LOCATION = (1,3)
      #  Button for e.g. <SET M1>
	BUTTON = "SET M"
           BUTTON_LOCATION = (1,1)
           BUTTON_CONFIGURATION = "SET M" <signalNumber>
           DNBSetSignalActivity.SignalValue = ON
           DNBSetSignalActivity.SignalDuration = 0
      BUTTON_END
      #  Button for e.g. <RESET M1>
	BUTTON = "RESET M"
           BUTTON_LOCATION = (2,1)
           BUTTON_CONFIGURATION = "RESET M" <signalNumber>
           DNBSetSignalActivity.SignalValue = OFF
           DNBSetSignalActivity.SignalDuration = 0
      BUTTON_END
      #  Button for e.g. <SETM M1 1, 3.5>
	  BUTTON = "SETM"
           BUTTON_LOCATION = (3,1)
           BUTTON_CONFIGURATION = "SETM M" <signalNumber> ", " <outputSignal> ", " <signalDuration>
      BUTTON_END
      #  Button for e.g. <WAITI I1>
      BUTTON = "WAITI"
           BUTTON_LOCATION = (4,1)
           BUTTON_CONFIGURATION = "WAITI I" <waitsignalNumber>
           DNBWaitSignalActivity.WaitSignalMaxTime = 0
           DNBWaitSignalActivity.SignalValue = ON
      BUTTON_END
      #  Button for e.g. <WAITJ I1>
      BUTTON = "WAITJ"
           BUTTON_LOCATION = (5,1)
           BUTTON_CONFIGURATION = "WAITJ I" <waitsignalNumber>
           DNBWaitSignalActivity.WaitSignalMaxTime = 0
           DNBWaitSignalActivity.SignalValue = OFF
      BUTTON_END
      #  Button for e.g. <WAIT I1, 1.5>
      BUTTON = "WAIT"
           BUTTON_LOCATION = (6,1)
           BUTTON_CONFIGURATION = "WAIT I" <waitsignalNumber> ", " <waitSignalDuration>
           DNBWaitSignalActivity.SignalValue = ON
      BUTTON_END
GROUP_END

# Create Actions group
# 
VARIABLE gunNumber	=1:UserAction.AWSpotWeld@"Gun_Number"
VARIABLE weldCondNumber	=1:UserAction.AWSpotWeld@"Weld_Cond_Number"
VARIABLE weldSeqNumber	=1:UserAction.AWSpotWeld@"Weld_Seq_Number"
VARIABLE weldPointNumber=1:UserAction.AWSpotWeld@"Weld_Point_Number"

GROUP = "Actions"
GROUP_LOCATION = (1,4)
      #button for e.g. <SPOT GUN#, CONDITION#, WELDINGSEQ#, WELDINGPOINT#>
      BUTTON = "SPOT"
	      BUTTON_LOCATION = (1,1)
          BUTTON_CONFIGURATION = "SPOT " <gunNumber> ", " <weldCondNumber> ", " <weldSeqNumber> ", " <weldPointNumber>
          UserAction = "AWSpotWeld"
          MoveHomeActivity.MoveToHome="Home_1"
          MoveHomeActivity.MotionTime=".5"
          DelayActivity.WaitTime=".5"
          MoveHomeActivity.MoveToHome="Home_2"
          MoveHomeActivity.MotionTime=".4"
      BUTTON_END   
GROUP_END

# Declare VARIABLE and ENUM data for misc. buttons
# create <callTaskName> VARIABLE
VARIABLE callTaskName="":DNBIgpCallRobotTask.CallName

# create <comment> & <robotcommand> VARIABLES
VARIABLE comment="":Operation@"Comment"
VARIABLE robotcommand="":Operation@"Robot Language"

# Create the Misc. Group
GROUP = "Misc."
GROUP_LOCATION = (1,5)
      BUTTON = "DELAY"
           BUTTON_LOCATION = (1,1)
           BUTTON_CONFIGURATION = "DELAY " <delayTime>
           DelayActivity.ActivityName = "WaitDelayTime"
      BUTTON_END
      BUTTON = "CALLP"
          BUTTON_LOCATION = (2,1) 
          BUTTON_CONFIGURATION = "CALLP " <callTaskName>
      BUTTON_END
      BUTTON = "Comment"
		  BUTTON_LOCATION = (3,1)
		  BUTTON_CONFIGURATION = "'" <comment>
      BUTTON_END
      BUTTON = "Robot Language"
          BUTTON_LOCATION = (4,1)
          BUTTON_CONFIGURATION = <robotcommand>
      BUTTON_END
GROUP_END
