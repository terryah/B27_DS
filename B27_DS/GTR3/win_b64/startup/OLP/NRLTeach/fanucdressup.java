//DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2002-2005

//Regular expression classes
import java.util.*;
import java.text.*;
import java.util.regex.*;


//IO classes
import java.io.*;
//Java Exception classes

import java.io.IOException;
import java.io.FileNotFoundException;
import javax.xml.transform.TransformerException;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Enumeration;
import java.util.Collections;


//Java text classes
import java.text.NumberFormat;

public class fanucdressup {

        private ArrayList m_NRLProgram;
        private ArrayList m_V5Properties;
        private ArrayList m_NRLHeader;
        private String m_ProgramName;
        private String m_FileExtension;
        private String m_toolGroup;
        private String m_railGroup;
        private String m_workGroup;
        private int[] m_auxGroup;
        private boolean[] m_auxTrans;
	private boolean m_sysvarsoutput;
        private String[] m_auxType;
        private boolean m_auxAxesgroup;
        
        //Constructor
        public fanucdressup(String OutputFileName, String InputFileName )  {
            
             m_NRLProgram = new ArrayList(1);
             m_V5Properties = new ArrayList(1);
             m_NRLHeader = new ArrayList(1);
             m_auxType = new String[6];
             m_auxTrans = new boolean[6];
             m_auxGroup = new int[6];
             m_toolGroup = new String("2");
             m_railGroup = new String("1");
             m_workGroup = new String("3");
			 m_sysvarsoutput = true;
     
             for (int ii=0;ii<6;ii++)
             {
                 m_auxType[ii] = "Translational";
                 m_auxGroup[ii] = 0;
                 m_auxTrans[ii] = true;
             }
             m_auxAxesgroup = false;
         
             m_ProgramName = new String("TPETASK");
             m_FileExtension = new String("ls");
             
             try {
                BufferedReader outputFile = new BufferedReader(new InputStreamReader(new FileInputStream(OutputFileName), "UTF-8"));
                BufferedReader inputFile = new BufferedReader(new InputStreamReader(new FileInputStream(InputFileName), "UTF-8"));
                
                String line = outputFile.readLine();
                int lineno = 0;
                while (line != null) {
                    if (line.equals("")) {
                        line = outputFile.readLine();
                        continue;
                    }
                    m_NRLProgram.add( lineno, line);
                    lineno++;
                    line = outputFile.readLine();
                } // while
                outputFile.close();
                
                line = inputFile.readLine();
                lineno = 0;
                
                while (line != null) {
                    if (line.equals("")) {
                        line = inputFile.readLine();
                        continue;
                    }
                    m_V5Properties.add( lineno, line);
                    String[] propertyComponents=line.split(":");
                    if (propertyComponents.length >= 2)
                    {
                         String propertyValue = "";
                         
                         for (int ii=1;ii<propertyComponents.length;ii++)
                         {
                            propertyValue += propertyComponents[ii];
                            if (ii < (propertyComponents.length-1))
                                propertyValue += ":";
                         }
                       
                         if (propertyComponents[0].equals("DNBRobotMotionActivity.JointPosition"))
                         {
                             String[] values = propertyValue.split(" ");
                             if (values.length > 6)
                                 m_auxAxesgroup = true;
                         } 
                         if (propertyComponents[0].equals("DNBRobotTask.Name"))
                         {
                             m_ProgramName = propertyValue;
                             m_ProgramName = doRemove(m_ProgramName,"\\.");
                         }
                         if (propertyComponents[0].equals("ResourceParameter.FileExtension"))
                         {
                             m_FileExtension = propertyValue;
                         }
                         if (propertyComponents[0].equals("ResourceParameter.Toolgroup"))
                         {
                             m_toolGroup = propertyValue;
                         }              
                         if (propertyComponents[0].equals("ResourceParameter.Railgroup"))
                         {
                             m_railGroup = propertyValue;
                         }              
                         if (propertyComponents[0].equals("ResourceParameter.Workgroup"))
                         {
                             m_workGroup = propertyValue;
                         }
                         if (propertyComponents[0].equals("ResourceParameter.SysvarOutput"))
                         {
                                 if (!propertyValue.equalsIgnoreCase("true"))
                                         m_sysvarsoutput = false;
                         }                                                             
                         if (propertyComponents[0].indexOf("AuxJoint.") == 0)
                         {
                             int dotIndex = propertyComponents[0].indexOf('.');
                             int auxIndex = 7;
                             if (dotIndex > 0)
                             {
                                 auxIndex = Integer.parseInt(propertyComponents[0].substring(dotIndex+1));
                             }
                             int index = propertyValue.indexOf("Rotational");
                             if (auxIndex > 6)
                             {
                            	 if (index > 0)
                            	 {
                                     m_auxTrans[auxIndex-7] = false;                            		 
                            	 }
                            	 else
                            	 {
                                     m_auxTrans[auxIndex-7] = true;	 
                            	 }
                                 int commaIndex = propertyValue.lastIndexOf(',');
                                 if (commaIndex > 0)
                                 {
                                     m_auxType[auxIndex-7] = propertyValue.substring(commaIndex+1);
                                 }
                                 if (m_auxType[auxIndex-7].equals("EndOfArmTooling"))
                                 {
                                     m_auxGroup[auxIndex-7] = Integer.parseInt(m_toolGroup);
                                 }
                                 if (m_auxType[auxIndex-7].equals("RailTrackGantry"))
                                 {
                                     m_auxGroup[auxIndex-7] = Integer.parseInt(m_railGroup);
                                 }
                                 if (m_auxType[auxIndex-7].equals("WorkpiecePositioner"))
                                 {
                                     m_auxGroup[auxIndex-7] = Integer.parseInt(m_workGroup);
                                 }                                                                                                   
                             }                          
                         }                         
                         
                    }
                    lineno++;
                    line = inputFile.readLine();
                } // while                    
                inputFile.close();
                
                // now open the output file and overwrite the contents           
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(OutputFileName,false), "UTF-8"));
                WriteVersion(bw);
                
                String currentProc = "";
                for (int ii=0;ii<m_NRLProgram.size();ii++)
                {                
                    String currentLine = (String)m_NRLProgram.get(ii);
                    if (currentLine.startsWith("START TASK:"))
                    {
                        String[] propertyComponents=currentLine.split(":");
                        if (propertyComponents.length == 2)
                        {
                            m_ProgramName = propertyComponents[1];
                            m_ProgramName = doRemove(m_ProgramName,"\\.");
                            int headerno = 0; 
                            m_NRLHeader.clear();
                            for (int jj=0;jj<m_V5Properties.size();jj++)
                            { 
                                currentLine = (String)m_V5Properties.get(jj);
                                if (currentLine.startsWith("START TASK:"))
                                {
                                    propertyComponents=currentLine.split(":");
                                    if (propertyComponents.length == 2)
                                    {
                                        currentProc = propertyComponents[1]; 
                                        currentProc = doRemove(currentProc,"\\.");
                                    }
                                    continue;
                                }
                                if (currentProc.equals(m_ProgramName))
                                {                               
                                    String[] headerPropertyComponents=currentLine.split(":");
                                    if (headerPropertyComponents.length >= 2)
                                    {
                                         String propertyValue = headerPropertyComponents[1];

                                         if (headerPropertyComponents[0].equals("DNBRobotTask.Header"))
                                         {
                                             m_NRLHeader.add( headerno, propertyValue);
                                             headerno++;
                                         }   
                                    }
                                }
                            }
                             WriteHeader(bw);
                             WriteProgram(bw);
                             WritePositions(bw);
                        }                        
                        continue;
                    }
                    if (currentLine.startsWith("END TASK:"))
                    {
                        continue;
                    } 
                }
                
                if (m_sysvarsoutput == true)
                {
                        WriteSysvars(bw);
                }
                
                bw.close();

            } // try
            catch (IOException e) { 
                e.getMessage();
            }            
        }

    	public static void main(String [] parameters) throws  NumberFormatException {

                String outputFileName = parameters[0];
                String inputFileName = parameters[1];

                fanucdressup uploader = new fanucdressup( outputFileName,
                    inputFileName);

        }

        public void WriteVersion(BufferedWriter bw)
        {
             try {
                  bw.write("VERSION INFO START\n");
 	          bw.write("DELMIA CORP. NRL TEACH FANUC DRESSUP DOWNLOADER VERSION 5 RELEASE 27\n");
	          bw.write("COPYRIGHT DELMIA CORP. 1986-2006, ALL RIGHTS RESERVED\n");  
                  bw.write("VERSION INFO END\n");
             }
             catch (IOException e) { 
                e.getMessage();
             }                       
        }
        public void WriteHeader(BufferedWriter bw)
        {   
            try {
                bw.write("DATA FILE START " + m_ProgramName + "." + m_FileExtension + "\n");
                bw.write("/PROG " + m_ProgramName + "\n"); 
                if (m_NRLHeader.size() > 0)
                {
                    for (int ii=0;ii<m_NRLHeader.size();ii++)
                    {
                       String currentLine = (String)m_NRLHeader.get(ii);
                       bw.write(currentLine + "\n");                       
                    }
                }
                else
                {
                    bw.write("/ATTR\n");
                    bw.write("OWNER           = MNEDITOR;\n");
                    bw.write("COMMENT         = \"DELMIA OFFLINE PROGRAM -  Resource Id \";\n");
                    bw.write("PROG_SIZE       = 0;\n");
                    Date dNow = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat("'= DATE 'yyyy'-' MM'-' dd 'TIME   'hh:mm:ss';\n'");
                    bw.write("CREATE          ");
                    bw.write(ft.format(dNow));
                    bw.write("MODIFIED        ");
                    bw.write(ft.format(dNow));                    
                    bw.write("FILE_NAME       = ;\n");
                    bw.write("VERSION         = 0;\n");
                    bw.write("LINE_COUNT      = 0;\n");
                    bw.write("MEMORY_SIZE     = 0;\n");
                    bw.write("PROTECT         = READ_WRITE;\n");
                    bw.write("TCD:  STACK_SIZE        = 0,\n");
                    bw.write("      TASK_PRIORITY     = 50,\n");
                    bw.write("     TIME_SLICE        = 0,\n");
                    bw.write("     BUSY_LAMP_OFF     = 0,\n");
                    bw.write("     ABORT_REQUEST     = 0,\n");
                    bw.write("     PAUSE_REQUEST     = 0;\n");
                    if (m_auxAxesgroup == true)
                    {
                            bw.write("DEFAULT_GROUP   = 1,");
		                    for (int ii=2;ii<6;ii++)
		                    {
	                           if (FindAuxGroup(ii))
	                           {
	                              bw.write("1");
	                           }
	                           else
	                           {
	                              bw.write("*");
	                           }
	                           if (ii<5)
	                           {
	                        	   bw.write(",");
	                           }
		                    }
		                    bw.write(";\n");
                    }
                    else
                    {
                      bw.write("DEFAULT_GROUP   = 1,*,*,*,*;\n");
                    }
                        bw.write("CONTROL_CODE    = 00000000 00000000;\n");
                }
            }
             catch (IOException e) { 
                e.getMessage();
            }                       
        }
         
        public boolean FindAuxGroup(int iGroupNum)
        {
        	boolean auxFound = false;
            for (int ii=0;ii<6;ii++)
            {
               if (m_auxGroup[ii] == iGroupNum)
               {
            	   auxFound = true;
               }
            }        	
        	return auxFound;
        }
        
        public void WriteProgram(BufferedWriter bw)
        {
            try { 
                bw.write("/MN\n");
                String currentProc = "";
                int lineno = 0;
                boolean prevLineCircular = false;
                for (int ii=0;ii<m_NRLProgram.size();ii++)
                {
                    String currentLine = (String)m_NRLProgram.get(ii);
                    if (currentLine.startsWith("START TASK:"))
                    {
                        String[] propertyComponents=currentLine.split(":");
                        if (propertyComponents.length == 2)
                        {
                             currentProc = propertyComponents[1];
                             currentProc = doRemove(currentProc,"\\.");
                        }                        
                        continue;
                    }
                    if (currentLine.startsWith("END TASK:"))
                    {
                        continue;
                    } 
                    if (currentProc.equals(m_ProgramName))
                    {
                        lineno++;
                        String lineStr = "   " + String.valueOf(lineno);
                        if (lineno>9)
                            lineStr = "  " + String.valueOf(lineno);
                        if (lineno>99)
                            lineStr = " " + String.valueOf(lineno);
                        if (lineno>999)
                            lineStr = String.valueOf(lineno);

                        currentLine = (String)m_NRLProgram.get(ii);
                        if (prevLineCircular == true)
                        {
                            bw.write("     " + currentLine + "\n");
                        }
                        else
                        {
                            bw.write(lineStr + ":" + currentLine + "\n");
                        }
                        prevLineCircular = false;
                        if (currentLine.startsWith("C P[") || currentLine.startsWith("C PR["))
                        {
                            prevLineCircular = true;
                        }
                    }
                }
            }
             catch (IOException e) { 
                e.getMessage();
            }                       
        }        

        public  void WritePositions(BufferedWriter bw)
        {
            try {
                bw.write("/POS\n");
                
                String robotMotionActProperty = "DNBRobotMotionActivity.ActivityName";
                String turnNumber1Property = "DNBRobotMotionActivity.TurnNumber1:";
                String turnNumber4Property = "DNBRobotMotionActivity.TurnNumber4:";
                String turnNumber5Property = "DNBRobotMotionActivity.TurnNumber5:";
                String turnNumber6Property = "DNBRobotMotionActivity.TurnNumber6:";
                String configNameProperty = "DNBRobotMotionActivity.ConfigName:";
                String utoolProperty = "ToolProfile.ProfileName:";
                String uframeProperty = "ObjectFrameProfile.ProfileName:";
                String cartPosProperty = "DNBRobotMotionActivity.CartesianPosition:";
                String jointPosProperty = "DNBRobotMotionActivity.JointPosition:";
                String posRegProperty = "DNBRobotMotionActivity@PosReg:";
                String posTypeProperty = "DNBRobotMotionActivity@PosType:";
                String posCommentProperty = "DNBRobotMotionActivity@Pos Comment:";
                String tagNameProperty = "DNBRobotMotionActivity.TagName:";
                String targetTypeProperty = "DNBRobotMotionActivity.TargetType:";
                String propertyValue = "";
                ArrayList posRegisters = new ArrayList(1);
                for (int ii=0;ii<10000;ii++)
                    posRegisters.add(ii, "");

                String currentProc = "";
                // first store position registers in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                   String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
                    if (currentLine.startsWith("START TASK:"))
                    {
                        currentProc = propertyValue;
                        currentProc = doRemove(currentProc,"\\.");
                        continue;
                    }
                    if (currentLine.startsWith(robotMotionActProperty) && currentProc.equals(m_ProgramName))
                    {
                        String posType = "";
                        String posReg = "";                       
                        
                        for (ii=ii+1;ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty);ii++)
                        {
                            currentLine = (String)m_V5Properties.get(ii);
                           
                            propertyComponents=currentLine.split(":");
                            if (propertyComponents.length == 2)
                            {
                                 propertyValue = propertyComponents[1];
                            }  
                            if (currentLine.startsWith("START TASK:"))
                            {
                                currentProc = propertyValue;
                                currentProc = doRemove(currentProc,"\\.");
                                continue;
                            }                            
                            if (currentLine.startsWith(posTypeProperty))
                            {
                                posType = propertyValue;
                            }
                            if (currentLine.startsWith(posRegProperty))
                            {
                                posReg = propertyValue;
                                if (!posType.startsWith("PR"))
                                {
                                     posRegisters.set(Integer.parseInt(posReg), posType);
                                    // System.out.println("POSITION REGISTER SET:" + posReg + " POS TYPE: " +  posType);
                                     posType = "";
                                     posReg = "";
                                }                                
                            }                  
                        }
                        ii--;
                    }
                }
                
                for (int kk=0;kk<posRegisters.size();kk++)
                {
                    String posTypeCheck = (String)posRegisters.get(kk);
                    String posRegCheck = String.valueOf(kk);
                    if (posTypeCheck.equals("P"))
                    {
                        for (int ii=0;ii<m_V5Properties.size();ii++)
                        {
                            String currentLine = (String)m_V5Properties.get(ii);

                            String[] propertyComponents=currentLine.split(":");
                            if (propertyComponents.length == 2)
                            {
                                 propertyValue = propertyComponents[1];
                            }
                            if (currentLine.startsWith(robotMotionActProperty))
                            {
                                String posReg = "";
                                String posType = "";
                                String posComment = "";
                                String uf = "";
                                String ut = "";
                                String config = "";
                                String turn1 = "";
                                String turn4 = "";
                                String turn6 = "";
                                String xval = "";
                                String yval = "";
                                String zval = "";
                                String yaw = "";
                                String pitch = "";
                                String roll = "";
                                String[] auxAxes = {"","","","","",""};
                                String[] mainAxes = {"","","","","",""};
                                String tagName = "";
                                String targetType = "";

                                boolean registerMatch = false;
                                for (ii=ii+1;ii<m_V5Properties.size() && !((String)m_V5Properties.get(ii)).startsWith(robotMotionActProperty);ii++)
                                {
                                    currentLine = (String)m_V5Properties.get(ii);

                                    propertyComponents=currentLine.split(":");
                                    if (propertyComponents.length == 2)
                                    {
                                         propertyValue = propertyComponents[1];
                                    }
                                    if (currentLine.startsWith(tagNameProperty))
                                    {
                                        tagName = propertyValue;
                                    }   
                                    if (currentLine.startsWith(posRegProperty))
                                    {
                                        posReg = propertyValue;
                                      //  System.out.println("NEW POS REG CHECK: " + posRegCheck + " CURRENT: " + posReg);
                                      //  System.out.println("NEW POS TYPE CHECK: " + posTypeCheck + " CURRENT: " + posType);
                                        if (posReg.equals(posRegCheck) && posType.equals(posTypeCheck))
                                        {
                                            registerMatch = true;
                                            // set this register to DONE so it will not get output more than once
                                            posRegCheck = "DONE";
                                        }
                                    }
                                    if (currentLine.startsWith(posTypeProperty))
                                    {
                                        posType = propertyValue;
                                    }        
                                    if (currentLine.startsWith(posCommentProperty))
                                    {
                                        posComment = propertyValue;
                                    }
                                    if (currentLine.startsWith(turnNumber1Property))
                                    {
                                        turn1 = propertyValue;
                                    }
                                    if (currentLine.startsWith(turnNumber4Property))
                                    {
                                        turn4 = propertyValue;
                                    }
                                    if (currentLine.startsWith(turnNumber6Property))
                                    {
                                        turn6 = propertyValue;
                                    }
                                    if (currentLine.startsWith(configNameProperty))
                                    {
                                        if (propertyValue.length() == 1)
                                           config = propertyValue;
                                        if (propertyValue.length() == 2)
                                           config = propertyValue.substring(0,1) + " " + propertyValue.substring(1,2);
                                        if (propertyValue.length() == 3)
                                           config = propertyValue.substring(0,1) + " " + propertyValue.substring(1,2) + " " + propertyValue.substring(2,3);
                                    }
                                    if (currentLine.startsWith(utoolProperty))
                                    {
                                        if (propertyValue.startsWith("UTOOL_NUM = "))
                                            ut = propertyValue.substring(12,13);
                                        else
                                            ut = "1";
                                    }
                                    if (currentLine.startsWith(uframeProperty))
                                    {
                                        if (propertyValue.startsWith("UFRAME_NUM = "))
                                            uf = propertyValue.substring(13, 14);
                                        else
                                            uf = "0";                                
                                    }
                                    if (currentLine.startsWith(cartPosProperty))
                                    {
                                        String[] values = propertyValue.split(" ");
                                        if (values.length == 6)
                                        {
                                            xval = formatValue(values[0], true);
                                            yval = formatValue(values[1], true);
                                            zval = formatValue(values[2], true);
                                            yaw = formatValue(values[3], false);
                                            pitch = formatValue(values[4], false);
                                            roll = formatValue(values[5], false);
                                        }
                                    }
                                    if (currentLine.startsWith(jointPosProperty))
                                    {
                                        String[] values = propertyValue.split(" ");
                                        for (int jj=0;jj<6;jj++)
                                        {
                                            mainAxes[jj] = formatValue(values[jj], false);
                                        }
                                        for (int jj=6;jj<values.length;jj++)
                                        {
                                            if (m_auxTrans[jj-6] == true)
                                            {
                                                double dblVal = Double.parseDouble(values[jj])*1000.0;
                                                values[jj] = String.valueOf(dblVal);
                                            }
                                            auxAxes[jj-6] = formatValue(values[jj], false);
                                        }
                                    }
                                    if (currentLine.startsWith(targetTypeProperty))
                                    {
                                        targetType = propertyValue;
                                    }                                                      
                                }
                                ii--;
                                if (registerMatch == false)
                                    continue;
                                if (!posReg.equals("") && !uf.equals("") && !ut.equals("") && !config.equals("") && !turn1.equals("") && !turn4.equals("") && !turn6.equals("") && !xval.equals("")
                                     && !yval.equals("") && !zval.equals("") && !yaw.equals("") && !pitch.equals("") && !roll.equals("") && !targetType.equals("joint") && !tagName.startsWith("PR["))
                                {
                                    if (!posComment.equals(""))
                                        bw.write("P[" + posReg + " : " + posComment + "] {\n");
                                    else
                                        bw.write("P[" + posReg + "] {\n");

                                    bw.write("   GP1:\n");
                                    bw.write("  UF : " + uf + ", UT : " + ut + ",           CONFIG : '" + config + ",  "  + turn1 + ", " + turn4 + ", " + turn6 + "',\n");
                                    bw.write("  X = " + xval + "  mm,  Y = " + yval + "  mm,  Z = " + zval + "  mm,\n");
                                    bw.write("  W = " + yaw + "  deg,  P = " + pitch + "  deg,  R = " + roll + "  deg");

                                    int lastGroup = 1;
                                    int jointNumber = 0;
                                    for (int jj = 0;jj<3;jj++)
                                    {
                                        if (!auxAxes[jj].equals(""))
                                        {
                                            String jType = "  J";
                                            if (m_auxGroup[jj] != lastGroup)
                                            {
                                            	lastGroup = m_auxGroup[jj];
                                                bw.write("\n   GP" + m_auxGroup[jj] + ":\n");
                                                bw.write("  UF : " + uf + ", UT : " + ut + ",\n");
                                                jointNumber = 1;
                                            }
                                            else
                                            {
                                            	if (m_auxGroup[jj] == 1)
                                            	{
                                            		jType = "  E";
                                            		bw.write(",\n");
                                            	}
                                            	jointNumber += 1;
                                            }
                                            
                                            String jNumber = String.valueOf(jointNumber);
                                            
                                            if (m_auxTrans[jj] == true)
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " mm");
                                            else
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " deg");
                                            if (!auxAxes[jj+1].equals(""))
                                            {
                                            	if (m_auxGroup[jj] == m_auxGroup[jj+1])
                                            	{
                                                    bw.write(",");		
                                            	}
                                            }
                                            else
                                            {
                                                bw.write("\n");
                                                break;
                                            }
                                            if (jj == 2)
                                                bw.write("\n");
                                        }
                                    }
                                    for (int jj = 3;jj<6;jj++)
                                    {
                                        if (!auxAxes[jj].equals("") && !auxAxes[0].equals(""))
                                        {
                                            String jType = "  J";
                                            
                                            if (m_auxGroup[jj] != lastGroup)
                                            {
                                            	lastGroup = m_auxGroup[jj];
                                                bw.write("   GP" + m_auxGroup[jj] + ":\n");
                                                bw.write("  UF : " + uf + ", UT : " + ut + ",\n");
                                                jointNumber = 1;
                                            }
                                            else
                                            {
                                            	if (m_auxGroup[jj] == 1)
                                            	{
                                            		jType = "  E";
                                            		bw.write(",\n");
                                            	}
                                            	jointNumber += 1;
                                            }
                                            
                                            String jNumber = String.valueOf(jointNumber);
                                            
                                            
                                            if (m_auxTrans[jj] == true)
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " mm");
                                            else
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " deg");                                   
                                            
                                            if (jj == 5 || auxAxes[jj+1].equals(""))
                                            {
                                                bw.write("\n");
                                                break;                                       
                                            }
                                            else
                                            {
                                                if (!auxAxes[jj+1].equals("") && m_auxGroup[jj] == m_auxGroup[jj+1])
                                                {
                                                    bw.write(",");
                                                }
                                            }
                                        }
                                    }                           
                                    bw.write("};\n");
                                }
                                else if (!posReg.equals("") && !uf.equals("") && !ut.equals("") && !config.equals("") && !turn1.equals("") && !turn4.equals("") && !turn6.equals("") && !mainAxes[0].equals("")
                                     && !mainAxes[1].equals("") && !mainAxes[2].equals("") && !mainAxes[3].equals("") && !mainAxes[4].equals("") && !mainAxes[5].equals("") && targetType.equals("joint"))
                                {
                                    if (!posComment.equals(""))
                                        bw.write("P[" + posReg + " : \"" + posComment + "\"] {\n");
                                    else
                                        bw.write("P[" + posReg + "] {\n");

                                    bw.write("   GP1:\n");
                                    bw.write("  UF : " + uf + ", UT : " + ut + ",           CONFIG : '" + config + ",  "  + turn1 + ", " + turn4 + ", " + turn6 + "',\n");
                                    bw.write("  J1 = " + mainAxes[0] + "  deg,  J2 = " + mainAxes[1] + "  deg,  J3 = " + mainAxes[3] + "  deg,\n");
                                    if (!auxAxes[0].equals(""))
                                        bw.write("  J4 = " + mainAxes[3] + "  deg,  J5 = " + mainAxes[4] + "  deg,  J6 = " + mainAxes[5] + "  deg");
                                    else
                                        bw.write("  J4 = " + mainAxes[3] + "  deg,  J5 = " + mainAxes[4] + "  deg,  J6 = " + mainAxes[5] + "  deg"); 

                                    
                                    int lastGroup = 1;
                                    int jointNumber = 0;
                                    for (int jj = 0;jj<3;jj++)
                                    {
                                        if (!auxAxes[jj].equals(""))
                                        {
                                            String jType = "  J";
                                            if (m_auxGroup[jj] != lastGroup)
                                            {
                                            	lastGroup = m_auxGroup[jj];
                                                bw.write("\n   GP" + m_auxGroup[jj] + ":\n");
                                                bw.write("  UF : " + uf + ", UT : " + ut + ",\n");
                                                jointNumber = 1;
                                            }
                                            else
                                            {
                                            	if (m_auxGroup[jj] == 1)
                                            	{
                                            		jType = "  E";
                                            		bw.write(",\n");
                                             	}
                                            	jointNumber += 1;
                                            }
                                            
                                            String jNumber = String.valueOf(jointNumber);
                                            
                                            if (m_auxTrans[jj] == true)
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " mm");
                                            else
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " deg");
                                            if (!auxAxes[jj+1].equals(""))
                                            {
                                            	if (m_auxGroup[jj] == m_auxGroup[jj+1])
                                            	{
                                                    bw.write(",");		
                                            	}
                                            }
                                            else
                                            {
                                                bw.write("\n");
                                                break;
                                            }
                                            if (jj == 2)
                                                bw.write("\n");
                                        }
                                    }
                                    for (int jj = 3;jj<6;jj++)
                                    {
                                        if (!auxAxes[jj].equals("") && !auxAxes[0].equals(""))
                                        {
                                            String jType = "  J";
                                            
                                            if (m_auxGroup[jj] != lastGroup)
                                            {
                                            	lastGroup = m_auxGroup[jj];
                                                bw.write("   GP" + m_auxGroup[jj] + ":\n");
                                                bw.write("  UF : " + uf + ", UT : " + ut + ",\n");
                                                jointNumber = 1;
                                            }
                                            else
                                            {
                                            	if (m_auxGroup[jj] == 1)
                                            	{
                                            		jType = "  E";
                                            		bw.write(",\n");
                                            	}
                                            	jointNumber += 1;
                                            }
                                            
                                            String jNumber = String.valueOf(jointNumber);
                                            
                                            
                                            if (m_auxTrans[jj] == true)
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " mm");
                                            else
                                                bw.write(jType + jNumber + "= " + auxAxes[jj] + " deg");                                   
                                            
                                            if (jj == 5 || auxAxes[jj+1].equals(""))
                                            {
                                                bw.write("\n");
                                                break;                                       
                                            }
                                            else
                                            {
                                                if (!auxAxes[jj+1].equals("") && m_auxGroup[jj] == m_auxGroup[jj+1])
                                                {
                                                    bw.write(",");
                                                }
                                            }
                                        }
                                    }                                         
                                    bw.write("};\n");
                                }
                                else
                                {
                                    if (!tagName.startsWith("PR["))
                                    {
                                        bw.write("ERROR INFO START\n");
                                        bw.write("ERROR DOWNLOADING POSITION INDEX: " + posReg + " COMMENT: " + posComment + "\n");
                                        bw.write("ERROR INFO END\n");
                                    }
                                }
                            }
                        }  
                    }
                }

               bw.write("/END\n");
               bw.write("DATA FILE END\n");
            }
 
            catch (IOException e) { 
                e.getMessage();
            }            
            
        }

        public String doRemove( String input, String removeStr)
        {
            String result = "";
            String [] toKeep = input.split(removeStr);
            for (int ii=0;ii<toKeep.length;ii++)
            {
                result += toKeep[ii];
            }
            return(result);
        }
	public void WriteSysvars(BufferedWriter bw)
	{

		try
		{
			bw.write("DATA FILE START sysvarsoutput.ls\n");

			String objectFrameCartPosProperty = "ObjectFrameProfile.CartesianPosition";
			String objectFrameNameProperty = "ObjectFrameProfile.ProfileName";
			String toolCartPosProperty = "ToolProfile.CartesianPosition";
			String toolNameProperty = "ToolProfile.ProfileName";
			String toolTypeProperty = "ToolProfile.ToolType";

			String objectFrameCartPos = "";
			String objectFrameName = "";
			String toolCartPos = "";
			String toolName = "";

			String propertyValue = "";
			ArrayList toolRegisters = new ArrayList(1);
			for (int ii = 0; ii < 12; ii++)
				toolRegisters.add(ii, "");
			ArrayList typeRegisters = new ArrayList(1);
			for (int ii = 0; ii < 12; ii++)
				typeRegisters.add(ii, "OnRobot");
			ArrayList objRegisters = new ArrayList(1);
			for (int ii = 0; ii < 12; ii++)
				objRegisters.add(ii, "");

			int profileIndex = -1;
			// first store tool/obj registers in array list
			for (int ii = 0; ii < m_V5Properties.size(); ii++)
			{
				String currentLine = (String)m_V5Properties.get(ii);

				String[] propertyComponents = currentLine.split(":");
				if (propertyComponents.length == 2)
				{
					propertyValue = propertyComponents[1];
				}
				if (currentLine.startsWith(toolNameProperty))
				{
                    String[] valueComponents = propertyValue.split("=");
					if (valueComponents.length == 2)
					{
					     String profileNumStr = valueComponents[1].trim();
						 profileIndex = Integer.parseInt(profileNumStr);
					}
					else
					{
						profileIndex = -1;
					}
				}
				if (currentLine.startsWith(objectFrameNameProperty))
				{
					String[] valueComponents = propertyValue.split("=");
					if (valueComponents.length == 2)
					{
						String profileNumStr = valueComponents[1].trim();
						profileIndex = Integer.parseInt(profileNumStr);
					}
					else
					{
						profileIndex = -1;
					}
				}
				if (currentLine.startsWith(toolCartPosProperty))
				{
					if (profileIndex >= 0)
					{
						toolRegisters.set(profileIndex, propertyValue);
						profileIndex = -1;
					}
				}
				if (currentLine.startsWith(toolTypeProperty))
				{
					if (profileIndex >= 0)
					{
						typeRegisters.set(profileIndex, propertyValue);
					}
				}
				if (currentLine.startsWith(objectFrameCartPosProperty))
				{
					if (profileIndex >= 0)
					{
						objRegisters.set(profileIndex, propertyValue);
						profileIndex = -1;
					}
				}
			}

		    String xval = "";
			String yval = "";
			String zval = "";
			String yaw = "";
			String pitch = "";
			String roll = "";

            bw.write("[*SYSTEM*] $MNUFRAME  Storage: CMOS  Access: RW  : ARRAY[1,9] OF POSITION =\n");
			
			for (int kk = 0; kk < objRegisters.size(); kk++)
			{
				String cartPos = (String)objRegisters.get(kk);
				String objIndex = String.valueOf(kk);
                String[] values = cartPos.split(" ");
                if (values.length == 6)
                {
                    xval = formatValue(values[0], true);
                    yval = formatValue(values[1], true);
                    zval = formatValue(values[2], true);
                    yaw = formatValue(values[3], false);
                    pitch = formatValue(values[4], false);
                    roll = formatValue(values[5], false);
					bw.write("[1," + objIndex + "]    Group: 1   Config: F R U T, 0, 0, 0\n");
					bw.write("X:   " + xval + "    Y:   " + yval +  "    Z:   " + zval + "\n");
					bw.write("W:   " + yaw + "    P:   " + pitch +  "    R:   " + roll + "\n");
                }
			}

            bw.write("[*SYSTEM*] $MNUTOOL  Storage: CMOS  Access: RW  : ARRAY[1,9] OF POSITION =\n");
			for (int kk = 1; kk < toolRegisters.size(); kk++)
			{
				String cartPos = (String)toolRegisters.get(kk);
				String toolIndex = String.valueOf(kk);
                String[] values = cartPos.split(" ");
                if (values.length == 6)
                {
                    xval = formatValue(values[0], true);
                    yval = formatValue(values[1], true);
                    zval = formatValue(values[2], true);
                    yaw = formatValue(values[3], false);
                    pitch = formatValue(values[4], false);
                    roll = formatValue(values[5], false);
					bw.write("[1," + toolIndex + ":" + typeRegisters.get(kk) + "]    Group: 1   Config: F R U T, 0, 0, 0\n");
					bw.write("X:   " + xval + "    Y:   " + yval +  "    Z:   " + zval + "\n");
					bw.write("W:   " + yaw + "    P:   " + pitch +  "    R:   " + roll + "\n");
                }
			}
			bw.write("DATA FILE END\n");
		}

		catch (IOException e)
		{
			e.getMessage();
		}

	}
        
        public String formatValue(String value, boolean meters)
        {
            if (meters)
            {
                double val = Double.parseDouble(value);
                val = val*1000.0;
                value = String.valueOf(val);
            }
            String newvalue = value;
           
            int index = value.indexOf(".");
            
            if (index >= 0)
            {
                if (value.length() >= index+4)
                {
                    newvalue = value.substring(0,index) + value.substring(index, index+4);
                }
            }
            else
            {
                newvalue = value + ".0";
            }
            return(newvalue);
        }
       

} // end class
