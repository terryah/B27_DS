//DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2002-2008

//Regular expression classes
import java.util.regex.*;

//IO classes
import java.io.*;

import javax.xml.transform.TransformerException;

//Java Util classes
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Locale;

//Java text classes
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class kukadressup
{
    private static final int JOINT = 1;
    private static final int CARTESIAN = 2;

    private ArrayList m_NRLProgram = new ArrayList(1);
    private ArrayList m_V5Properties = new ArrayList(1);
    private boolean j3Linked = false;
    private String m_ProgramName, mainProgramName, softwareV = "4.1.5", pointFormat = "E6POS";
    private String templateSrcFileName = "c:\\tmp\\vorgabe.src";
    private String templateDatFileName = "c:\\tmp\\vorgabe.dat";
    private int moveCounter = 1, pdatCounter = 1, cpdatCounter = 1;
    private static String sTsk = "START TASK:";
    private static String eTsk = "END TASK:";
    private static String nl = "\r\n";
    private String vrcTargetDir = "C:\\KRC\\ROBOTER\\KRC\\R1\\Program";

    //Constructor
    public kukadressup(String OutputFileName, String InputFileName, String LogFileName)
    {
        // OutputFileName: NRLProgramFile.tmp
        // InputFileName: motParam.tmp

        File f = new File(OutputFileName);
        String NRLProgramFileName = f.getParent() + f.separator + "NRLProgramFileOriginal.tmp";

        try
        {
            String line;
            int lineno, idx;
            String [] propertyComponents = {"", ""};

            BufferedReader outputFile = new BufferedReader(new FileReader(OutputFileName)); 
            BufferedReader inputFile  = new BufferedReader(new FileReader(InputFileName));
            BufferedWriter bw = new BufferedWriter(new FileWriter(NRLProgramFileName, false));

            line = outputFile.readLine();
            lineno = 0;
            while (line != null)
            {
                bw.write(line + nl); // write to NRLProgramFileOriginal.tmp

                if (line.equals(""))
                {
                    line = outputFile.readLine();
                    continue;
                }
                m_NRLProgram.add(lineno, line);

                lineno++;
                line = outputFile.readLine();
            } // while
            outputFile.close();
            bw.close();

            line = inputFile.readLine();
            lineno = 0;
            while (line != null)
            {
                if (line.equals(""))
                {
                    line = inputFile.readLine();
                    continue;
                }
                m_V5Properties.add(lineno, line);
                
                if (line.startsWith("DNBRobotTask.Name:")) // task NRL displays
                {
                    propertyComponents = line.split(":", 2);
                    mainProgramName = propertyComponents[1].replaceAll("\\.", "");
                }
                if (line.startsWith("ResourceParameter."))
                {
                    idx = line.indexOf('.');
                    line = line.substring(idx+1);
                    if (line.startsWith("SoftwareVersion"))
                    {
                        idx = line.indexOf(':');
                        softwareV = line.substring(idx+1);
                    }
                    if (line.startsWith("PointFormat"))
                    {
                        idx = line.indexOf(':');
                        pointFormat = line.substring(idx+1);
                    }
                    if (line.startsWith("TemplateFile"))
                    {
                        idx = line.indexOf(':');
                        templateSrcFileName = line.substring(idx+1)+".src";
                        templateDatFileName = line.substring(idx+1)+".dat";
                    }
                    if (line.startsWith("VRCTargetDirectory:"))
                    {
                        idx = line.indexOf(':');
                        vrcTargetDir = line.substring(idx+1);
                        File vrcF = new File(vrcTargetDir);
                        vrcTargetDir = vrcF.getPath();
                    }
                }
                
                lineno++;
                line = inputFile.readLine();
            } // while

            // Locale
            Locale userLocale = Locale.getDefault();
            Locale en_US_Locale = new Locale("en_US");
            Locale.setDefault(en_US_Locale);
            
            // now open the output file and overwrite the contents
            bw = new BufferedWriter(new FileWriter(OutputFileName, false));

            boolean bRRS = false;
            WriteVersion(bw);
            WriteProgram_SRC(bw, f, bRRS);
            WriteProgram_DAT(bw, f, bRRS);
            bw.close();
            
            // Create RRS-II download log file
            if (LogFileName != null)
            {
                bRRS = true;
                WriteProgram_SRC(bw, f, bRRS);
                WriteProgram_DAT(bw, f, bRRS);
                
                WriteRRSLog(LogFileName);
            }
            
            // change back to user Locale
            Locale.setDefault(userLocale);
        } // try
        catch (IOException e) {
            e.getMessage();
        }
        catch (ArrayIndexOutOfBoundsException e) {
            e.getMessage();
        }
    }

    public static void main(String [] parameters) throws  NumberFormatException
    {
            String outputFileName = parameters[0];
            String inputFileName = parameters[1];
            String logFileName = null;
            if (parameters.length >= 3)
                logFileName = parameters[2];

            kukadressup downloader = new kukadressup(outputFileName, inputFileName, logFileName);
    }

    public void WriteVersion(BufferedWriter bw)
    {
        try {
            bw.write("VERSION INFO START" + nl);
            bw.write("DELMIA CORP. NRL TEACH KUKA DRESSUP DOWNLOADER VERSION 5 RELEASE 25 SP6" + nl);
            bw.write("COPYRIGHT DELMIA CORP. 1986-2017, ALL RIGHTS RESERVED" + nl);
            bw.write("VERSION INFO END" + nl);
        }
        catch (IOException e) {
            e.getMessage();
         }
    }
    
    private void writeProgramHeader(BufferedWriter bw)
    {
        try
        {
            bw.write("&ACCESS" + nl);
            bw.write("&PARAM EDITMASK = *" + nl);
        }
        catch (IOException e)
        {
            e.getMessage();
        }
    }
    
    private void WriteProgram_DAT(BufferedWriter bw, File f, boolean bRRS)
    {
        DecimalFormat cartFormat = new DecimalFormat("0.#######");
        double tmpdouble;
        
        try
        {
            String line, currentProc = null;
            
            ArrayList tagOutput = new ArrayList(1);
            String tagName = "", config = "0";
            String sAccuracyValue = "0";
            float accuracyValue = new Float(0).floatValue();
            String toolName, baseName, toolType = "ONROBOT";
            int toolNum = 0, baseNum = 0;
            int [] turnbits = {0,0,0,0,0,0};
            int turn = 0, factor = 1, targetType = CARTESIAN, motype = CARTESIAN;
            String [] property;
            String [] jointval = {"", "", "", "", "", "", "", "", "", "", "", ""};
            String [] cartesian = {"", "", "", "", "", ""};
            float speed = new Float(0).floatValue();
            String motionBasis = "PERCENT";

            int ii=0, jj=0, len = m_V5Properties.size();
            line = (String)m_V5Properties.get(ii++);
            while (ii < len)
            {
                if (line.startsWith("DNBRobotMotionActivity.ActivityName:"))
                {
                    line = (String)m_V5Properties.get(ii++);
                    while (true)
                    {
                        if (line.startsWith("DNBRobotMotionActivity.ActivityName:") ||
                            line.startsWith(eTsk) ||
                            line.startsWith("ResourceParameter.") ||
                            line.startsWith("RobotJoint.") ||
                            line.startsWith("DNBRobotTask.Name:"))
                            break;

                        property = line.split(":", 2);
                        if (line.startsWith("DNBRobotMotionActivity.TagName:"))
                            tagName = property[1];
                        if (line.startsWith("DNBRobotMotionActivity@PosReg:"))
                            if (tagName.length() == 0)
                                tagName = "P" + property[1];
                        if (line.startsWith("DNBRobotMotionActivity.ConfigName:"))
                            config = property[1].substring(1);
                        if (line.startsWith("DNBRobotMotionActivity.JointPosition:"))
                        {
                            jointval = property[1].split("[\\s+]");
                            turn = 0;
                            factor = 1;
                            for (jj=0; jj<6; jj++)
                            {
                                if (jointval[jj].startsWith("-"))
                                    turnbits[jj] = 1;
                                else
                                    turnbits[jj] = 0;

                                turn += turnbits[jj]*factor;
                                factor *= 2;
                            }
                        }
                        if (line.startsWith("DNBRobotMotionActivity.CartesianPosition:"))
                        {
                            cartesian = property[1].split("[\\s+]");
                        }
                        if (line.startsWith("DNBRobotMotionActivity.TargetType:"))
                        {
                            if (property[1].equalsIgnoreCase("JOINT"))
                                targetType = JOINT;
                            else
                                targetType = CARTESIAN;
                        }
                        if (line.startsWith("DNBRobotMotionActivity.MotionType:"))
                        {
                            if (property[1].equalsIgnoreCase("JOINT"))
                                motype = JOINT;
                            else
                                motype = CARTESIAN;
                        }
                        if (line.startsWith("AccuracyProfile.AccuracyValue:"))
                            sAccuracyValue = property[1];
                        if (line.startsWith("ObjectFrameProfile.ProfileName:"))
                        {
                            baseName = property[1];
                            baseNum = Integer.parseInt(baseName.split("[\\[\\]]")[1]);
                        }
                        if (line.startsWith("ToolProfile.ProfileName:"))
                        {
                            toolName = property[1];
                            toolNum = Integer.parseInt(toolName.split("[\\[\\]]")[1]);
                        }
                        if (line.startsWith("ToolProfile.ToolType:"))
                            toolType = property[1].toUpperCase();
                        if (line.startsWith("MotionProfile.MotionBasis:"))
                            motionBasis = property[1].toUpperCase();
                        if (line.startsWith("MotionProfile.SpeedValue:"))
                            speed = Float.parseFloat(property[1]);
                        
                        // read another line
                        line = (String)m_V5Properties.get(ii++);
                    }
                    // output to DAT file
                    if (!tagOutput.contains(tagName))
                    {
                        bw.write("DECL ");
                        if (pointFormat.equalsIgnoreCase("POS"))
                            bw.write("POS");
                        if (pointFormat.equalsIgnoreCase("E3POS"))
                            bw.write("E3POS");
                        if (pointFormat.equalsIgnoreCase("E6POS"))
                            bw.write("E6POS");
                        bw.write(" X" + tagName);
                        bw.write("={x " + cartFormat.format(Double.parseDouble(cartesian[0])*1000));
                        bw.write(",y " + cartFormat.format(Double.parseDouble(cartesian[1])*1000));
                        bw.write(",z " + cartFormat.format(Double.parseDouble(cartesian[2])*1000));
                        bw.write(",a " + cartFormat.format(Double.parseDouble(cartesian[5])));
                        bw.write(",b " + cartFormat.format(Double.parseDouble(cartesian[4])));
                        bw.write(",c " + cartFormat.format(Double.parseDouble(cartesian[3])));
                        bw.write(", s " + config + ",t " + turn);
                        if (pointFormat.equalsIgnoreCase("E3POS"))
                            bw.write(",e1 0.0,e2 0.0,e3 0.0");
                        if (pointFormat.equalsIgnoreCase("E6POS"))
                            bw.write(",e1 0.0,e2 0.0,e3 0.0,e4 0.0,e5 0.0,e6 0.0");
                        bw.write("}" + nl);
                        
                        bw.write("DECL FDAT F" + tagName + "={TOOL_NO " + toolNum);
                        bw.write(",BASE_NO " + baseNum + ",IPO_FRAME #BASE}" + nl);
                        
                        tagOutput.add(tagName);
                    }
                    // speed and accuracy
                    // DECL PDAT|LDAT PPDAT1|LCPDAT1={VEL 100.0|1.5,ACC 100.0,APO_DIST ...
                    bw.write("DECL ");
                    if (motype == JOINT)
                        bw.write("PDAT PPDAT" + pdatCounter++);
                    else
                        bw.write("LDAT LCPDAT" + cpdatCounter++);
                    bw.write("={VEL " + speed + ",ACC 100.0,APO_DIST ");
                    if (motype != JOINT)
                    {
                        accuracyValue = Float.parseFloat(sAccuracyValue)*1000;
                        sAccuracyValue = String.valueOf(accuracyValue);
                    }
                    bw.write(sAccuracyValue);
                    if (motype != JOINT)
                        bw.write(",APO_FAC 50.0,ORI_TYP #VAR");
                    bw.write("}" + nl);
                }
                else if (line.startsWith(sTsk))
                {
                    currentProc = line.split(":", 2)[1].trim().replaceAll("\\.", "");
                    if (bRRS == true)
                    {
                        String rrsProgramName = f.getParent() + f.separator + currentProc + ".dat";
                        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsProgramName, false), "UTF-8"));
                    }
                    else
                    {
                        bw.write("DATA FILE START " + currentProc + ".dat" + nl);
                    }
                    
                    writeProgramHeader(bw);
                    bw.write("DEFDAT " + currentProc + nl);
                    
                    try
                    {
                        BufferedReader templateDAT = new BufferedReader(new FileReader(templateDatFileName));
                        line = templateDAT.readLine();
                        while (line != null)
                        {
                            bw.write(line + nl);
                            line = templateDAT.readLine();
                        }
                        templateDAT.close();
                    }
                    catch (IOException e)
                    {
                        if (bRRS == false)
                        {
                            bw.write("ERROR INFO START" + nl);
                            bw.write("ERROR: "+e.getMessage()+nl);
                            bw.write("ERROR INFO END" + nl);
                        }
                    }
                    
                    pdatCounter = 1;
                    cpdatCounter = 1;
                    tagOutput.clear();
                    
                    line = (String)m_V5Properties.get(ii++);
                }
                else if (line.startsWith(eTsk))
                {
                    bw.write("ENDDAT" + nl);
                    if (bRRS == true)
                        bw.close();
                    else
                        bw.write("DATA FILE END" + nl);
                    
                    line = (String)m_V5Properties.get(ii++);
                }
                else
                {
                    line = (String)m_V5Properties.get(ii++);
                }
            }
        }
        catch (IOException e) {
            e.getMessage();
        }
    } // WriteProgram_DAT
    
    private void WriteProgram_SRC(BufferedWriter bw, File f, boolean bRRS)
    {
        int numLines;
        Matcher m;
        Pattern [] p;
        p = new Pattern [501];
        for (int ii = 0; ii < p.length; ii++)
            p[ii] = Pattern.compile("DontMatchNothing", Pattern.CASE_INSENSITIVE);
        // group                       (1   )    (2       )         (3     )         (4   )           (5  )              (6  )
        p[0] = Pattern.compile("PTP\\s+(\\w+)\\s+(CONT\\s+)?Vel=\\s*([0-9]+)\\s*%\\s+(\\w+)\\s+Tool\\[(\\d)\\]\\s+Base\\[(\\d)\\]", Pattern.CASE_INSENSITIVE);
        // group                       (1   )    (2       )         (3               )           (4   )           (5  )              (6  )
        p[1] = Pattern.compile("LIN\\s+(\\w+)\\s+(CONT\\s+)?Vel=\\s*([0-9]+\\.?[0-9]*)\\s*M/S\\s+(\\w+)\\s+Tool\\[(\\d)\\]\\s+Base\\[(\\d)\\]", Pattern.CASE_INSENSITIVE);
        p[2] = Pattern.compile("OUT\\s+([0-9]+)\\s+'([^']+)'\\s+STATE=\\s*(TRUE|FALSE)\\s+CONT", Pattern.CASE_INSENSITIVE);
        p[3] = Pattern.compile("PULSE\\s+([0-9]+)\\s+'([^']+)'\\s+STATE=\\s*(TRUE|FALSE)\\s+CONT\\s+TIME=\\s*([0-9]+\\.?[0-9]*)\\s+SEC", Pattern.CASE_INSENSITIVE);
        p[4] = Pattern.compile("WAIT\\s+TIME=\\s*([0-9]+\\.?[0-9]*)\\s+SEC", Pattern.CASE_INSENSITIVE);
        p[5] = Pattern.compile("WAIT\\s+FOR\\s+(NOT\\s+)?\\(\\s*IN\\s+([0-9]+)\\s+'([^']+)'\\s*\\)", Pattern.CASE_INSENSITIVE);
        p[6] = Pattern.compile("^;.*$", Pattern.CASE_INSENSITIVE);
        
        /* call subprogram must be the last pattern to not match any keywords,
         * do not actually need it, will be output literally
        p[500] = Pattern.compile("[a-zA-Z_]\\w*\\s*\\(\\s*\\)", Pattern.CASE_INSENSITIVE);
         */
        
        try
        {
            String line, currentProc = null;
            
            String tagName, sTagType;
            String [] sTagVal;
            numLines = m_NRLProgram.size();
            for (int ii=0; ii<numLines; ii++)
            {
                line = (String)m_NRLProgram.get(ii);
                for (int jj=0; jj<p.length; jj++)
                {
                    m = p[jj].matcher(line);
                    if (m.find() == true)
                    {
                        switch (jj)
                        {
                            case 0:
                                outputPTP(m, bw);
                                break;
                            case 1:
                                outputLIN(m, bw);
                                break;
                            case 2:
                                outputSigOUT(m, bw);
                                break;
                            case 3:
                                outputSigPULSE(m, bw);
                                break;
                            case 4:
                                outputWaitTime(m, bw);
                                break;
                            case 5:
                                outputWaitFor(m, bw);
                                break;
                            case 6:
                                outputComment(line, bw);
                                break;
                        }
                        
                        // match found & processed, break out FOR loop
                        break;
                    }
                    else
                    {
                        if (jj == p.length-1)
                        {
                            if (line.startsWith(sTsk))
                            {
                                currentProc = line.split(":", 2)[1].trim().replaceAll("\\.", "");
                                
                                if (bRRS == true)
                                {
                                    String rrsProgramName = f.getParent() + f.separator + currentProc + ".src";
                                    bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsProgramName, false), "UTF-8"));
                                }
                                else
                                {
                                    bw.write("DATA FILE START " + currentProc + ".src" + nl);
                                }
                                

                                writeProgramHeader(bw);
                                bw.write("DEF " + currentProc + "( )" + nl);
                                
                                try
                                {
                                    BufferedReader templateSRC = new BufferedReader(new FileReader(templateSrcFileName));
                                    line = templateSRC.readLine();
                                    while (line != null)
                                    {
                                        bw.write(line + nl);
                                        line = templateSRC.readLine();
                                    }
                                    templateSRC.close();
                                }
                                catch (IOException e)
                                {
                                    if (bRRS == false)
                                    {
                                        bw.write("ERROR INFO START" + nl);
                                        bw.write("ERROR: "+e.getMessage()+nl);
                                        bw.write("ERROR INFO END" + nl);
                                    }
                                }
                                
                                pdatCounter = 1;
                                cpdatCounter = 1;
                            }
                            else if (line.startsWith(eTsk))
                            {
                                bw.write("END" + nl);
                                if (bRRS == true)
                                    bw.close();
                                else
                                    bw.write("DATA FILE END" + nl);
                            }
                            else
                            {
                                bw.write(line + nl);
                            }
                        }
                    }
                }
            }
        }
        catch (IOException e) {
            e.getMessage();
        }
    } // WriteProgram_SRC
    
    private void outputWaitFor(Matcher m, BufferedWriter bw)
    {
        String negate = m.group(1);
        String ioNum = m.group(2);
        String ioName = m.group(3);
        try
        {
            bw.write(";FOLD " + m.group());
            bw.write(";%{PE}%R " + softwareV);
            bw.write(",%MKUKATPBASIS,%CEXT_WAIT_FOR,%VEXT_WAIT_FOR,%P 2:");
            if (negate != null)
                bw.write("NOT");
            bw.write(", 4:, 5:$IN, 6:");
            bw.write(ioNum + ", 7:" + ioName + ", 9:" + nl);
            bw.write("WAIT FOR ");
            if (negate != null)
                bw.write("NOT ");
            bw.write("( $IN[" + ioNum + "] )" + nl);
            bw.write(";ENDFOLD" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
    }
    
    private void outputComment(String comm, BufferedWriter bw)
    {
        try
        {
            bw.write(";FOLD " + comm);
            bw.write(";%{PE}%R " + softwareV + ",%MKUKATPBASIS,%CCOMMENT,%VNORMAL,%P 2:");
            bw.write(comm.substring(1) + nl);
            bw.write(";ENDFOLD" + nl);
        }
        catch (IOException e){
            e.getMessage();
        }
    }
    
    private void outputWaitTime(Matcher m, BufferedWriter bw)
    {
        String time = m.group(1);
        try
        {
            bw.write(";FOLD " + m.group());
            bw.write(";%{PE}%R " + softwareV + ",%MKUKATPBASIS,%CWAIT,%VWAIT,%P 2:");
            bw.write(time + nl);
            bw.write("WAIT SEC " + time + nl);
            bw.write(";ENDFOLD" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
    }
    
    private void outputSigOUT(Matcher m, BufferedWriter bw)
    {
        int ioNum = Integer.parseInt(m.group(1));
        String ioName = m.group(2);
        String ioState = m.group(3);
        
        try
        {
            bw.write(";FOLD " + m.group());
            bw.write(";%{PE}%R " + softwareV + ",%MKUKATPBASIS,%COUT,%VOUTX,%P 2:");
            bw.write(ioNum + ", 3:" + ioName + ", 5:" + ioState + ", 6:CONTINUE" + nl);
            bw.write("CONTINUE" + nl);
            bw.write("$OUT[" + ioNum + "]=" + ioState + nl);
            bw.write(";ENDFOLD" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
    } // outputSigOUT
    
    private void outputSigPULSE(Matcher m, BufferedWriter bw)
    {
        int ioNum = Integer.parseInt(m.group(1));
        String ioName = m.group(2);
        String ioState = m.group(3);
        String time = m.group(4);
        
        try
        {
            bw.write(";FOLD " + m.group());
            bw.write(";%{PE}%R " + softwareV + ",%MKUKATPBASIS,%COUT,%VPULSE,%P 2:");
            bw.write(ioNum + ", 3:" + ioName + ", 5:" + ioState + ", 6:CONTINUE, 8:");
            bw.write(time + nl);
            bw.write("CONTINUE" + nl);
            bw.write("PULSE($OUT[" + ioNum + "], " + ioState + ", " + time + ")" + nl);
            bw.write(";ENDFOLD" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
    } // outputSigPULSE
    
    private void outputPTP(Matcher m, BufferedWriter bw)
    {
        String tagName = m.group(1);
        String cont = m.group(2);
        int iSpeed = Integer.parseInt(m.group(3));
        int iToolNum = Integer.parseInt(m.group(5));
        int iBaseNum = Integer.parseInt(m.group(6));
        
        int moBlocks = 0, lineNum = 0;
        String line;
        
        for (int ii=0; ii<m_V5Properties.size(); ii++)
        {
            line = (String)m_V5Properties.get(ii);
            if (line.startsWith("DNBRobotMotionActivity.ActivityName:"))
                moBlocks++;
            if (moBlocks == moveCounter)
            {
                lineNum = ii+1;
                break;
            }
        }
        
        boolean flyBy = true;
        float accuracyVal = new Float(0).floatValue();
        String accuracyType = "speed";
        
        for (int ii=lineNum; ii<m_V5Properties.size(); ii++)
        {
            line = (String)m_V5Properties.get(ii);
            if (line.startsWith("AccuracyProfile.FlyByMode:"))
            {
                if (line.split(":")[1].equalsIgnoreCase("OFF"))
                    flyBy = false;
            }
            if (line.startsWith("AccuracyProfile.AccuracyType:"))
                accuracyType = line.split(":")[1];
            if (line.startsWith("AccuracyProfile.AccuracyValue:"))
                accuracyVal = Float.parseFloat(line.split(":")[1]);
            
            if (line.startsWith("DNBRobotMotionActivity.ActivityName:") ||
                line.startsWith("ResourceParameter.") ||
                line.startsWith("RobotJoint." ) ||
                line.startsWith("DNBRobotTask.Name:"))
                break;
        }
        
        try
        {
            bw.write(";FOLD PTP " + tagName);
            if (cont != null)
                bw.write(" " + cont);
            bw.write(" Vel= " + iSpeed + " % PDAT" + pdatCounter);
            bw.write(" Tool[" + iToolNum + "] Base[" + iBaseNum + "]");
            bw.write(";%{PE}%R " + softwareV);
            bw.write(",%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:" + tagName);
            bw.write(", 3:");
            if (flyBy == true)
            {
                bw.write("C_PTP");
                if (accuracyType.equalsIgnoreCase("SPEED"))
                    bw.write(" C_VEL");
            }
            bw.write(", 5:" + iSpeed + ", 7:PDAT" + pdatCounter + nl);
            bw.write("$BWDSTART = FALSE" + nl);
            bw.write("PDAT_ACT=PPDAT" + pdatCounter + nl);
            bw.write("FDAT_ACT=F" + tagName + nl);
            bw.write("BAS(#PTP_PARAMS," + iSpeed + ")" + nl);
            bw.write("PTP X" + tagName);
            if (flyBy == true)
            {
                bw.write(" C_PTP");
                if (accuracyType.equalsIgnoreCase("SPEED"))
                    bw.write(" C_VEL");
            }
            bw.write(nl + ";ENDFOLD" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
        
        moveCounter++;
        pdatCounter++;
    } // outputPTP
    
    private void outputLIN(Matcher m, BufferedWriter bw)
    {
        String tagName = m.group(1);
        String cont = m.group(2);
        float fSpeed = Float.parseFloat(m.group(3));
        int iToolNum = Integer.parseInt(m.group(5));
        int iBaseNum = Integer.parseInt(m.group(6));
        
        int moBlocks = 0, lineNum = 0;
        String line;
        
        for (int ii=0; ii<m_V5Properties.size(); ii++)
        {
            line = (String)m_V5Properties.get(ii);
            if (line.startsWith("DNBRobotMotionActivity.ActivityName:"))
                moBlocks++;
            if (moBlocks == moveCounter)
            {
                lineNum = ii+1;
                break;
            }
        }
        
        boolean flyBy = true;
        float accuracyVal = new Float(0).floatValue();
        String accuracyType = "speed";
        
        for (int ii=lineNum; ii<m_V5Properties.size(); ii++)
        {
            line = (String)m_V5Properties.get(ii);
            if (line.startsWith("AccuracyProfile.FlyByMode:"))
            {
                if (line.split(":")[1].equalsIgnoreCase("OFF"))
                    flyBy = false;
            }
            if (line.startsWith("AccuracyProfile.AccuracyType:"))
                accuracyType = line.split(":")[1];
            if (line.startsWith("AccuracyProfile.AccuracyValue:"))
                accuracyVal = Float.parseFloat(line.split(":")[1]);
            
            if (line.startsWith("DNBRobotMotionActivity.ActivityName:") ||
                line.startsWith("ResourceParameter.") ||
                line.startsWith("RobotJoint." ) ||
                line.startsWith("DNBRobotTask.Name:"))
                break;
        }
        
        try
        {
            bw.write(";FOLD LIN " + tagName);
            if (cont != null)
                bw.write(" " + cont);
            bw.write(" Vel= " + fSpeed + " m/s CPDAT" + cpdatCounter);
            bw.write(" Tool[" + iToolNum + "] Base[" + iBaseNum + "]");
            bw.write(";%{PE}%R " + softwareV);
            bw.write(",%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:LIN, 2:" + tagName);
            bw.write(", 3:");
            if (flyBy == true)
            {
                if (accuracyType.equalsIgnoreCase("DISTANCE"))
                    bw.write("C_DIS");
                else
                    bw.write("C_VEL");
            }
            bw.write(", 5:" + fSpeed + ", 7:CPDAT" + cpdatCounter + nl);
            bw.write("$BWDSTART = FALSE" + nl);
            bw.write("LDAT_ACT=LCPDAT" + cpdatCounter + nl);
            bw.write("FDAT_ACT=F" + tagName + nl);
            bw.write("BAS(#CP_PARAMS," + fSpeed + ")" + nl);
            bw.write("LIN X" + tagName);
            if (flyBy == true)
            {
                if (accuracyType.equalsIgnoreCase("DISTANCE"))
                    bw.write(" C_DIS");
                else
                    bw.write(" C_VEL");
            }
            bw.write(nl + ";ENDFOLD" + nl);
        }
        catch (IOException e) {
            e.getMessage();
        }
        
        moveCounter++;
        cpdatCounter++;
    } // outputLIN

    public void WriteRRSLog(String rrsLogFileName)
    {
        if (rrsLogFileName == null)
            return;

        String line, currentProc;
        File f = new File(rrsLogFileName);
        
        try
        {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rrsLogFileName, false), "UTF-8"));
            if (vrcTargetDir.equalsIgnoreCase(f.getParent()) == false)
            {
                bw.write("[FILE_TRANSFER]" + nl);
                for (int ii=0; ii<m_NRLProgram.size(); ii++)
                {
                    line = (String)m_NRLProgram.get(ii);
                    if (line.startsWith(sTsk))
                    {
                        currentProc = line.split(":", 2)[1].trim().replaceAll("\\.", "");
                        bw.write(currentProc + ".src=" + vrcTargetDir + nl);
                        bw.write(currentProc + ".dat=" + vrcTargetDir + nl);
                    }
                }
            }

            bw.write("[VRC_PROGRAM]" + nl);
            bw.write(vrcTargetDir + f.separator + mainProgramName + ".src=" + mainProgramName + nl);
            
            bw.close();
        }
        catch (IOException e)
        {
            e.getMessage();
        }
    }
    
} // end class
