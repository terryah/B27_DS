//DOM and Parser classes
// COPYRIGHT DELMIA CORP. 2002-2005

//Regular expression classes
import java.util.regex.*;


//IO classes
import java.io.*;
//Java Exception classes

import java.io.IOException;
import java.io.FileNotFoundException;
import javax.xml.transform.TransformerException;

//Java Util classes
import java.util.ArrayList;
import java.util.Hashtable;


//Java text classes
import java.text.DecimalFormat;

public class rapiddressup {

        private ArrayList m_NRLProgram;
        private ArrayList m_V5Properties;
        private ArrayList m_NRLHeader;
        private String m_ProgramName;
        private String m_ModuleName;
        private String m_FileExtension;
        private String m_toolGroup;
        private String m_railGroup;
        private String m_workGroup;
        private int[] m_auxGroup;
        private boolean[] m_auxTrans;
        private String[] m_auxType;
        private boolean m_auxAxesgroup;
      
        private double [][] dgMatrix;
        private double [][] dgCatMatrix;
        private double [][] dgInvertedMatrix;
        private double [][] tmpMatrix;
        private double cr;
        private double sr;
        private double cp;
        private double sp;
        private double cy;
        private double sy;
        private double xx;
        private double yy;
        private double zz;
        private double yaw;
        private double pitch;
        private double roll;
        private double q1;
        private double q2;
        private double q3;
        private double q4;
        private String [] components;
        
        //Constructor
        public rapiddressup(String OutputFileName, String InputFileName )  {
            
             m_NRLProgram = new ArrayList(1);
             m_V5Properties = new ArrayList(1);
             m_NRLHeader = new ArrayList(1);
             m_auxType = new String[6];
             m_auxTrans = new boolean[6];
             m_auxGroup = new int[6];
             m_toolGroup = new String("2");
             m_railGroup = new String("1");
             m_workGroup = new String("2");
             String xyzypr = "0.0   0.0 0.0 0.0 0.0 0.0";
             dgMatrix = new double[4][4];
             dgCatMatrix = new double[4][4];
             dgInvertedMatrix = new double[4][4];
             tmpMatrix = new double[4][4];
             components = xyzypr.split("\\t");        
             
             for (int ii=0;ii<6;ii++)
             {
                 m_auxType[ii] = "Translational";
                 m_auxGroup[ii] = 2;
                 m_auxTrans[ii] = true;
             }
             m_auxAxesgroup = false;
         
             m_ProgramName = new String("rapid_proc");
             m_ModuleName = new String("rapid_mod");
             
             m_FileExtension = new String("ls");
             
             try {
                BufferedReader outputFile = new BufferedReader(new InputStreamReader(new FileInputStream(OutputFileName), "UTF-8"));
                BufferedReader inputFile = new BufferedReader(new InputStreamReader(new FileInputStream(InputFileName), "UTF-8"));
                
                String line = outputFile.readLine();
                int lineno = 0;
                while (line != null) {
                   // if (line.equals("")) {
                   //     line = outputFile.readLine();
                   //     continue;
                   // }
                    line = doRemove(line, "\\.");
                    if (!line.startsWith("START TASK:") && !line.startsWith("END TASK:"))
                    {
                        line = line.replace('#','_');
                    }
                    m_NRLProgram.add( lineno, line);
                    lineno++;
                    line = outputFile.readLine();
                } // while
                outputFile.close();
                
                line = inputFile.readLine();
                lineno = 0;
                int headerno = 0;
                while (line != null) {
                  //  if (line.equals("")) {
                  //      line = inputFile.readLine();
                  //      continue;
                  //  }
                    m_V5Properties.add( lineno, line);
                    String[] propertyComponents=line.split(":");
                    if (propertyComponents.length >= 2)
                    {
                         String propertyValue = "";
                         
                         for (int ii=1;ii<propertyComponents.length;ii++)
                         {
                            propertyValue += propertyComponents[ii];
                            if (ii < (propertyComponents.length-1))
                                propertyValue += ":";
                         }
                       
                         if (propertyComponents[0].equals("DNBRobotMotionActivity.JointPosition"))
                         {
                             String[] values = propertyValue.split(" ");
                             if (values.length > 6)
                                 m_auxAxesgroup = true;
                         } 
                         if (propertyComponents[0].equals("DNBRobotTask.Header"))
                         {
                             m_NRLHeader.add( headerno, propertyValue);
                             headerno++;
                         }
                         if (propertyComponents[0].equals("DNBRobotTask.Name"))
                         {
                             propertyValue = doRemove(propertyValue, "\\.");
                             m_ProgramName = propertyValue;
                             m_ModuleName = propertyValue + "_mod";
                             
                             if (propertyValue.indexOf("#") > 0)
                             {
                                  String [] taskValues = propertyValue.split("#");
                                  if (taskValues.length == 2)
                                  {
                                       m_ProgramName = taskValues[0];
                                       m_ModuleName = taskValues[1];
                                  }
                             }
                         }
                         if (propertyComponents[0].equals("ResourceParameter.FileExtension"))
                         {
                             m_FileExtension = propertyValue;
                         }
                         if (propertyComponents[0].equals("ResourceParameter.Toolgroup"))
                         {
                             m_toolGroup = propertyValue;
                         }              
                         if (propertyComponents[0].equals("ResourceParameter.Railgroup"))
                         {
                             m_railGroup = propertyValue;
                         }              
                         if (propertyComponents[0].equals("ResourceParameter.Workgroup"))
                         {
                             m_workGroup = propertyValue;
                         }                                       
                         if (propertyComponents[0].equals("AuxJoint."))
                         {
                             int dotIndex = propertyComponents[0].indexOf('.');
                             int auxIndex = 7;
                             if (dotIndex > 0)
                             {
                                 auxIndex = Integer.parseInt(propertyComponents[0].substring(dotIndex+1));
                             }
                             int index = propertyValue.indexOf("Rotational");
                             if (index > 0 && auxIndex > 6)
                             {
                                 m_auxTrans[auxIndex-7] = false;
                                 int commaIndex = propertyValue.lastIndexOf(',');
                                 if (commaIndex > 0)
                                 {
                                     m_auxType[auxIndex-7] = propertyValue.substring(commaIndex+1);
                                 }
                                 if (m_auxType[auxIndex-7].equals("EndOfArmTooling"))
                                 {
                                     m_auxGroup[auxIndex-7] = Integer.parseInt(m_toolGroup);
                                 }
                                 if (m_auxType[auxIndex-7].equals("RailTrackGantry"))
                                 {
                                     m_auxGroup[auxIndex-7] = Integer.parseInt(m_railGroup);
                                 }
                                 if (m_auxType[auxIndex-7].equals("WorkpiecePositioner"))
                                 {
                                     m_auxGroup[auxIndex-7] = Integer.parseInt(m_workGroup);
                                 }                                                                                                   
                             }                          
                         }                         
                         
                    }
                    lineno++;
                    line = inputFile.readLine();
                } // while                    
                inputFile.close();
                
                // now open the output file and overwrite the contents           
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(OutputFileName,false), "UTF-8"));
                WriteVersion(bw);
                WriteHeader(bw);
                // write global robtargets
                WriteRobtargets(bw, "");
                WriteTooldata(bw);
                WriteWobjdata(bw);
                WriteSpeeddata(bw);
                WriteZonedata(bw);
                WriteSpotdata(bw);
                WriteGundata(bw);
                WriteSeamdata(bw);
                WriteWeavedata(bw);
                WriteWelddata(bw);
                WritePrograms(bw);
                bw.write("ENDMODULE\n");
                bw.write("DATA FILE END\n");
                bw.close();
            } // try
            catch (IOException e) { 
                e.getMessage();
            }            
        }

    	public static void main(String [] parameters) throws  NumberFormatException {
                String outputFileName = parameters[0];
                String inputFileName = parameters[1];

                rapiddressup uploader = new rapiddressup( outputFileName,
                    inputFileName);

        }

        public void WriteVersion(BufferedWriter bw)
        {
             try {
                  bw.write("VERSION INFO START\n");
 	          bw.write("DELMIA CORP. NRL TEACH RAPID DRESSUP DOWNLOADER VERSION 5 RELEASE 21 SP1\n");
	          bw.write("COPYRIGHT DELMIA CORP. 1986-2011, ALL RIGHTS RESERVED\n");  
                  bw.write("VERSION INFO END\n");
             }
             catch (IOException e) { 
                e.getMessage();
             }                       
        }
        
        public void WriteHeader(BufferedWriter bw)
        {   
            try {
                bw.write("DATA FILE START " + m_ProgramName + ".mod\n");
                if (m_NRLHeader.size() > 0)
                {
                    for (int ii=0;ii<m_NRLHeader.size();ii++)
                    {
                       String currentLine = (String)m_NRLHeader.get(ii);
                       bw.write(currentLine + "\n");                       
                    }
                }
                else
                {
                    bw.write("%%%\n");
                    bw.write("VERSION:1\n");
                    bw.write("LANGUAGE:ENGLISH\n");
                    bw.write("%%%\n");
                }
                bw.write("MODULE " + m_ModuleName + "\n"); 
            }
             catch (IOException e) { 
                e.getMessage();
            }                       
        }
 
        public void WritePrograms(BufferedWriter bw)
        {
            try { 
                String currentProc = "";
                boolean procDone = false;
                boolean endprocDone = false;
                String prevLine = "";
                int explicitCount = 0;
                for (int ii=0;ii<m_NRLProgram.size();ii++)
                {
                    String currentLine = (String)m_NRLProgram.get(ii);
                    if (currentLine.startsWith("START TASK:"))
                    {
                        String[] propertyComponents=currentLine.split(":");
                        if (propertyComponents.length == 2)
                        {
                            propertyComponents[1] = doRemove(propertyComponents[1], "\\.");
                            currentProc = propertyComponents[1];
                            int poundIndex = propertyComponents[1].indexOf("#");
                            if (poundIndex > 0)
                            {
                                currentProc = propertyComponents[1].substring(0,poundIndex);
                            }
                            else
                            {
                                currentProc = propertyComponents[1];
                            }                 
                             bw.write("  PROC " + currentProc + "()\n");
                             procDone = true;
                             WriteRobtargets(bw, currentProc);
                        }                        
                        continue;
                    }
                    if (currentLine.startsWith("END TASK:"))
                    {
                        procDone = false;
                        bw.write("  ENDPROC\n");
                        endprocDone = true;
                        continue;
                    }
                    if (currentLine.startsWith("! V5 DISPLAY ONLY:"))
                    {
                        continue;
                    }
                    if (currentProc.equals(""))
                    {
                        if (procDone == false)
                        {
                            bw.write("  PROC " +  m_ProgramName + "()\n");
                            procDone = true;
                        }
                    }
                    if (currentLine.indexOf("abb_explicit_") > 0)
                    {
                         explicitCount++;
                        currentLine = ReplaceExplicitTagNames(currentLine, currentProc, explicitCount);
                    }  
                    int contdIndex = currentLine.indexOf("Cont'd");
                    if (contdIndex == 0)
                    {
                        int prevcontdIndex = prevLine.indexOf(" Cont'd");
                        if (prevcontdIndex > 0)
                        {
                            bw.write("    " + prevLine.substring(0,prevcontdIndex) + currentLine.substring(6) + "\n"); 
                        }
                    }
                    else if (contdIndex > 0)
                    {
                        prevLine = currentLine;
                    }
                    else
                    {
                        bw.write("    " + currentLine + "\n");
                    }
                }
                if (procDone == true && endprocDone == false)
                {
                   bw.write("  ENDPROC\n"); 
                }
                
            }
            catch (IOException e) { 
                e.getMessage();
            }                       
        }        

        public  String ReplaceExplicitTagNames(String inputLine, String procName, int inputExplicitCount)
        {
            String startProcedureName = "START TASK";
            String robotMotionActProperty = "DNBRobotMotionActivity.ActivityName";
            String turnNumber1Property = "DNBRobotMotionActivity.TurnNumber1:";
            String turnNumber4Property = "DNBRobotMotionActivity.TurnNumber4:";
            String turnNumber5Property = "DNBRobotMotionActivity.TurnNumber5:";
            String turnNumber6Property = "DNBRobotMotionActivity.TurnNumber6:";
            String configNameProperty = "DNBRobotMotionActivity.ConfigName:";
            String utoolProperty = "ToolProfile.ProfileName:";
            String uframeProperty = "ObjectFrameProfile.ProfileName:";
            String cartPosProperty = "DNBRobotMotionActivity.CartesianPosition:";
            String jointPosProperty = "DNBRobotMotionActivity.JointPosition:";
            String posCommentProperty = "DNBRobotMotionActivity@Pos Comment:";
            String tagNameProperty = "DNBRobotMotionActivity.TagName:";
            String targetDeclTypeProperty = "DNBRobotMotionActivity@TargetDeclarationType";
            String targetTypeProperty = "DNBRobotMotionActivity.TargetType";
            String propertyValue = "";

            String tagName = "";
            String currentProcName = "";
            String cartesianPosition = "";
            String jointPosition = "";
            String config = "";
            String targetDeclType = "";
            String targetType = "";
            
            String outputLine = "";
            int explicitIndex = inputLine.indexOf("abb_explicit_");
            int commaIndex = inputLine.indexOf(",",explicitIndex+12);            
            String explicitNumStr = inputLine.substring(explicitIndex+13, commaIndex);
            String explicitTagName = inputLine.substring(explicitIndex, commaIndex);
            // System.out.println("EXPLICIT LINE: " +  inputLine + "\n");
            // System.out.println("EXPLICIT TAG NAME: " +  explicitTagName + "\n");
            // System.out.println("ExplicitNumStr: " + explicitNumStr + "\n");
            // System.out.println("PROC NAME: " + procName + "\n");
            int explicitNum = Integer.parseInt(explicitNumStr);
            int lastExplicitIndex = 0;
            int explicitCount = 0;
            
            while (explicitIndex > 0 && explicitNum > 0)
            {
                
                outputLine += inputLine.substring(lastExplicitIndex,explicitIndex); // everything up to the explicit reference
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }

                    if (currentLine.startsWith(cartPosProperty))
                    {
                        cartesianPosition = propertyValue;                       
                    }

                    if (currentLine.startsWith(jointPosProperty))
                    {
                        jointPosition = propertyValue;
                    }

                    if (currentLine.startsWith(configNameProperty))
                    {
                        config = propertyValue;                       
                    } 

                    if (currentLine.startsWith(startProcedureName))
                    {
                        propertyValue = doRemove(propertyValue, "\\.");
                        int poundIndex = propertyValue.indexOf("#");
                        if (poundIndex > 0)
                        {
                            currentProcName = propertyValue.substring(0,poundIndex);
                        }
                        else
                        {
                            currentProcName = propertyValue;
                        }
                    }

                    if (currentLine.startsWith(tagNameProperty))
                    {
                        tagName = propertyValue;                       
                    }

                    if (currentLine.startsWith(targetDeclTypeProperty))
                    {
                        targetDeclType = propertyValue;
                    }
                   
                    if (currentLine.startsWith(targetTypeProperty))
                    {
                        targetType = propertyValue;
                    }
                    
                    if (currentLine.equals(""))
                    {
                         if (tagName.startsWith("abb_explicit_"))
                         {
                             explicitCount++;
                         }
                         if ( procName.equals(currentProcName) && (tagName.equals(explicitTagName) || tagName.equals("")) )
                         {
                            if (tagName.equals(explicitTagName))     
                            {
                                String explicitStr = downloadRobtargetExplicitPortion(cartesianPosition, jointPosition, config);
                                outputLine += explicitStr;
                            }
                            else if (targetType.equals("joint"))
                            {
             //                    System.out.println("Explicit Count: " + String.valueOf(explicitCount) + "Input ExplicitCount: " +  String.valueOf(inputExplicitCount));
                                explicitCount++;
                                if (explicitCount == inputExplicitCount)
                                {
                                    String explicitStr = downloadJointtargetExplicitPortion(jointPosition);
                                    outputLine += explicitStr;
                                }                  
                            }
                         }
                         tagName = "";
                    }  
                }
                
                lastExplicitIndex = commaIndex;
                explicitIndex = inputLine.indexOf("abb_explicit_", explicitIndex+12);
                commaIndex = inputLine.indexOf(",",explicitIndex+12);
                inputExplicitCount++;
            }
            outputLine += inputLine.substring(lastExplicitIndex, inputLine.length());
            return(outputLine);
        }
        
        public  void WriteRobtargets(BufferedWriter bw, String procName)
        {
            String startProcedureName = "START TASK";
            String robotMotionActProperty = "DNBRobotMotionActivity.ActivityName";
            String turnNumber1Property = "DNBRobotMotionActivity.TurnNumber1:";
            String turnNumber4Property = "DNBRobotMotionActivity.TurnNumber4:";
            String turnNumber5Property = "DNBRobotMotionActivity.TurnNumber5:";
            String turnNumber6Property = "DNBRobotMotionActivity.TurnNumber6:";
            String configNameProperty = "DNBRobotMotionActivity.ConfigName:";
            String utoolProperty = "ToolProfile.ProfileName:";
            String uframeProperty = "ObjectFrameProfile.ProfileName:";
            String cartPosProperty = "DNBRobotMotionActivity.CartesianPosition:";
            String jointPosProperty = "DNBRobotMotionActivity.JointPosition:";
            String posCommentProperty = "DNBRobotMotionActivity@Pos Comment:";
            String jointTargetNameProperty = "DNBRobotMotionActivity@Tag Name";
            String tagNameProperty = "DNBRobotMotionActivity.TagName:";
            String targetTypeProperty = "DNBRobotMotionActivity.TargetType";
            String targetDeclTypeProperty = "DNBRobotMotionActivity@TargetDeclarationType";
            String propertyValue = "";

            String tagName = "";
            String jointTargetName = "";
            String currentProcName = "";
            String cartesianPosition = "";
            String jointPosition = "";
            String config = "";
            String targetDeclType = "";
            String targetType = "";

            Hashtable targetsDone = new Hashtable();

            // first store declaration type in array list
            for (int ii=0;ii<m_V5Properties.size();ii++)
            {
                String currentLine = (String)m_V5Properties.get(ii);

                String[] propertyComponents=currentLine.split(":");
                if (propertyComponents.length == 2)
                {
                     propertyValue = propertyComponents[1];
                }

                if (currentLine.startsWith(cartPosProperty))
                {
                    cartesianPosition = propertyValue;                       
                }

                if (currentLine.startsWith(jointPosProperty))
                {
                    jointPosition = propertyValue;
                }

                if (currentLine.startsWith(configNameProperty))
                {
                    config = propertyValue;                       
                } 


                if (currentLine.startsWith(startProcedureName))
                {
                    int poundIndex = propertyValue.indexOf("#");
                    if (poundIndex > 0)
                    {
                        currentProcName = propertyValue.substring(0,poundIndex);
                    }
                    else
                    {
                        currentProcName = propertyValue;
                    }                                           
                }

                if (currentLine.startsWith(tagNameProperty))
                {
                    tagName = propertyValue;                       
                }

                if (currentLine.startsWith(jointTargetNameProperty))
                {
                    jointTargetName = propertyValue;                       
                }

                if (currentLine.startsWith(targetDeclTypeProperty))
                {
                    targetDeclType = propertyValue;
                }

                if (currentLine.startsWith(targetTypeProperty))
                {
                    targetType = propertyValue;
                }
                
                if (currentLine.equals(""))
                {
                     if (!targetDeclType.equals("") && !targetDeclType.equals("PERS"))
                     {
                         // System.out.println("PROCNAME: " + currentProcName + " PROCNAME: " + procName + " JOINT TARGET NAME: " + jointTargetName + " TARGET TYPE: " + targetType + "\n");
                        if (currentProcName.equals(procName) && !tagName.startsWith("abb_explicit_") )
                        {
                            if (!jointTargetName.equals("") && targetType.equals("joint"))
                            {
                                String targetDone = (String)targetsDone.get(jointTargetName);
                                if (targetDone == null || !targetDone.equals("DONE"))
                                {
                                    downloadJointtargetDecl(bw, jointTargetName, targetDeclType, jointPosition);
                                    targetsDone.put(tagName,"DONE");
                                    jointTargetName = "";
                                }                                    
                            }
                            else if (!tagName.equals(""))    
                            {
                                String targetDone = (String)targetsDone.get(tagName);
                                if (targetDone == null || !targetDone.equals("DONE"))
                                {
                                   //  System.out.println("Proc Name: " + procName + " Robtarget Decl TagName: " +  tagName + " TYPE: " + targetDeclType);
                                    downloadRobtargetDecl(bw, tagName, targetDeclType, cartesianPosition, jointPosition, config);
                                    targetsDone.put(tagName,"DONE");
                                    tagName = "";
                                }
                            }
                        }
                     }
                     else if ( procName.equals("") && !tagName.startsWith("abb_explicit_") )
                     {
                    	targetDeclType = "PERS";
                        if (!tagName.equals(""))     
                        {
                            String targetDone = (String)targetsDone.get(tagName);
                            if (targetDone == null || !targetDone.equals("DONE"))
                            {       
                                // System.out.println("Proc Name: " + procName + " Robtarget Decl TagName: " +  tagName + " TYPE: " + targetDeclType);
                                downloadRobtargetDecl(bw, tagName, targetDeclType, cartesianPosition, jointPosition, config);
                                targetsDone.put(tagName, "DONE");
                            }
                        }
                        else if (!jointTargetName.equals(""))
                        {
                                String targetDone = (String)targetsDone.get(jointTargetName);
                                if (targetDone == null || !targetDone.equals("DONE"))
                                {
                                    downloadJointtargetDecl(bw, jointTargetName, targetDeclType, jointPosition);
                                    targetsDone.put(tagName,"DONE");
                                    jointTargetName = "";
                                }                                                                    
                        }
                     }
                     targetDeclType = "";
                }
            }
            
        }
        
        public void downloadRobtargetDecl( BufferedWriter bw, String tagName, String declType, String cartPos, String jointPos, String config)
        {
            try
            {
                 bw.write( "  " + declType + " robtarget " + tagName + ":=");
                 String explicitStr = downloadRobtargetExplicitPortion(cartPos, jointPos, config);
                 bw.write(explicitStr);
                 bw.write(";\n");
            }
            catch (IOException e) { 
                e.getMessage();
            }                        
        }
 
        public String downloadRobtargetExplicitPortion(String cartPos, String jointPos, String config)
        {
             String cartStr = downloadXYZQuaternion(cartPos);  
             String configStr = downloadRapidConfigs(jointPos, config);
             String auxStr = downloadAuxAxes(jointPos);
             String outputStr = "[" + cartStr + "," + configStr + auxStr + "]";
             return(outputStr);
        }        
        
        public void downloadJointtargetDecl( BufferedWriter bw, String jointTargetName, String declType, String jointPos)
        {
            try
            {
                 bw.write( "  " + declType + " jointtarget " + jointTargetName + ":=");
                 String explicitStr = downloadJointtargetExplicitPortion(jointPos);
                 bw.write(explicitStr);
                 bw.write(";\n");
            }
            catch (IOException e) { 
                e.getMessage();
            }                        
        }
        
        public String downloadJointtargetExplicitPortion(String jointPos)
        {
             String mainStr = downloadMainAxes(jointPos);
             String auxStr = downloadAuxAxes(jointPos);
             String outputStr = "[" + mainStr + "," + auxStr + "]";
             return(outputStr);
        }                
   
        public String downloadXYZQuaternion(String cartPos)
        {
             String[] cartValues = cartPos.split(" ");
             String outputStr = "";
             if (cartValues.length == 6)
             {
                 double cartX = Double.parseDouble(cartValues[0]);
                 double cartY = Double.parseDouble(cartValues[1]);
                 double cartZ = Double.parseDouble(cartValues[2]);

                 String xyzwpr = Double.toString(cartX) + "," + Double.toString(cartY) + "," + Double.toString(cartZ) + "," +
                     cartValues[3] + "," + cartValues[4] + "," + cartValues[5];

                 dgXyzyprToMatrix( xyzwpr );

                 dgMatrixToQuaternions();

                 String quat1 = dgGetQ1();
                 String quat2 = dgGetQ2();
                 String quat3 = dgGetQ3();
                 String quat4 = dgGetQ4();

                 outputStr = "[" + formatValue(Double.toString(cartX),true) + "," + formatValue(Double.toString(cartY), true) + "," + formatValue(Double.toString(cartZ), true) + "],[" + formatValue(quat1,false) + "," + formatValue(quat2,false) + "," + formatValue(quat3,false) + "," + formatValue(quat4,false) + "]";
 
             }
             
             return(outputStr);
   
        }        
        
        public String downloadRapidConfigs(String jointPos, String config)
        {
             String[] jointValue = jointPos.split(" ");
             String cfg1 = "0";
             String cfg4 = "0";
             String cfg5 = "0";
             String cfg6 = "0";
             String cfgx = "0";
             int cfg_val = 0;
             double jnt_val = 0.0;

             if (jointValue.length >= 0)
             {
                 jnt_val = Double.parseDouble(jointValue[0]);
                 cfg_val = (int)(jnt_val/90.0);
                 if (jnt_val < 0.0)
                 {
                     cfg_val -= 1;
                 }                    
                 cfg1 = Integer.toString(cfg_val);
             }
             if (jointValue.length >= 4)
             {
                 jnt_val = Double.parseDouble(jointValue[3]);
                 cfg_val = (int)(jnt_val/90.0);
                 if (jnt_val < 0.0)
                 {
                     cfg_val -= 1;
                 }                    
                 cfg4 = Integer.toString(cfg_val);
             }
             if (jointValue.length >= 6)
             {
                 jnt_val = Double.parseDouble(jointValue[5]);
                 cfg_val = (int)(jnt_val/90.0);
                 if (jnt_val < 0.0)
                 {
                     cfg_val -= 1;
                 }                                  
                 cfg6 = Integer.toString(cfg_val);  
             }       
             if (config.equals("Config_1"))
             {
                 cfgx = "0";
             }
             if (config.equals("Config_2"))
             {
                 cfgx = "1";
             }
             if (config.equals("Config_3"))
             {
                 cfgx = "2";
             }
             if (config.equals("Config_4"))
             {
                 cfgx = "3";
             }
             if (config.equals("Config_5"))
             {
                 cfgx = "6";
             }
             if (config.equals("Config_6"))
             {
                 cfgx = "7";
             }
             if (config.equals("Config_7"))
             {
                 cfgx = "4";
             }
             if (config.equals("Config_8"))
             {
                 cfgx = "5";
             }

             String outputStr = "[" + cfg1 + "," + cfg4 + "," + cfg6 + "," + cfgx + "],";
             return(outputStr);
        }                

        public String downloadAuxAxes(String jointPos)
        {

            String outputStr = "[";
            String[] jointVals = jointPos.split(" ");
            int count = 6;
            for (int ii=6;ii<jointVals.length;ii++)
            {
                outputStr += Double.toString(Double.parseDouble(jointVals[ii]));
                if (ii<11)
                {
                    outputStr += ",";
                }
                count++;
            }
            for (int ii=count;ii<12;ii++)
            {
                outputStr += "9E+09";
                if (ii<11)
                {
                    outputStr += ",";
                }
            }
            outputStr += "]";
            return(outputStr);
        }        
        
        public String downloadMainAxes(String jointPos)
        {

            String outputStr = "[";
            String[] jointVals = jointPos.split(" ");
            for (int ii=0;ii<6;ii++)
            {
                outputStr += Double.toString(Double.parseDouble(jointVals[ii]));
                if (ii<5)
                {
                    outputStr += ",";
                }
            }
            outputStr += "]";
            return(outputStr);
        }        
        
        public void WriteTooldata(BufferedWriter bw)
        {
            try
            {
                String toolNameProperty = "ToolProfile.ProfileName";
                String cartesianPositionProperty = "ToolProfile.CartesianPosition";
                String toolMassProperty = "ToolProfile.ToolMass";
                String toolTypeProperty = "ToolProfile.ToolType";
                String toolCentroidProperty = "ToolProfile.ToolCentroid";
                String toolInertiaProperty = "ToolProfile.ToolInertia";
                
                String propertyValue = "";

                String toolName = "";
                String toolType = "";
                String cartesianPosition = "";
                String toolMass = "";
                String toolCentroid = "";
                String toolInertia = "";    
                
                Hashtable toolsDone = new Hashtable();
                
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
        
                    if (currentLine.startsWith(toolNameProperty))
                    {
                        toolName = doRemove(propertyValue, "\\.");
                        toolName = toolName.replace('#','_');
                    }
                    
                    if (currentLine.startsWith(toolTypeProperty))
                    {
                        toolType = propertyValue;
                    }
                    
                    if (currentLine.startsWith(cartesianPositionProperty))
                    {
                        cartesianPosition = propertyValue;      
                    }
                    if (currentLine.startsWith(toolMassProperty))
                    {
                        toolMass = propertyValue;                       
                    }
                    if (currentLine.startsWith(toolCentroidProperty))
                    {
                        toolCentroid = propertyValue;                       
                    }
                    if (currentLine.startsWith(toolInertiaProperty))
                    {
                        toolInertia = propertyValue;

                        String toolDone = (String)toolsDone.get(toolName);
  
                        if (!toolName.equals("tool0") && (toolDone == null || !toolDone.equals("DONE")) )
                        {
                            toolsDone.put(toolName,"DONE");
                            
                            bw.write("  PERS tooldata " + toolName + ":=[");
                            if (toolType.equals("onrobot"))
                            {
                                bw.write("TRUE,[");
                            }
                            else
                            {
                                bw.write("FALSE,[");
                            }
                            String xyzStr = downloadXYZQuaternion(cartesianPosition);
                            bw.write(xyzStr + "],[" + toolMass + ",[");
                            String[] centroidVals = toolCentroid.split(" ");
                            String[] inertiaVals = toolInertia.split(" ");
                            if (centroidVals.length == 3)
                            {
                                bw.write(centroidVals[0] + "," + centroidVals[1] + "," + centroidVals[2] + "],[1,0,0,0],");
                            }
                            if (inertiaVals.length == 6)
                            {
                                bw.write(inertiaVals[0] + "," + inertiaVals[2] + "," + inertiaVals[5]);
                            }
                            bw.write("]];\n");
                        }                                                

                    }                    
                }
            }
           catch (IOException e) { 
                e.getMessage();
            }                                         
        }
 
        public void WriteWobjdata(BufferedWriter bw)
        {
            try
            {
                String objectFrameNameProperty = "ObjectFrameProfile.ProfileName";
                String cartesianPositionProperty = "ObjectFrameProfile.CartesianPosition";
                String toolTypeProperty = "ToolProfile.ToolType";
                
                String propertyValue = "";

                String objectFrameName = "";
                String toolType = "";
                String cartesianPosition = "";
                
                Hashtable wobjsDone = new Hashtable();
                
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
        
                    if (currentLine.startsWith(objectFrameNameProperty))
                    {
                        objectFrameName = doRemove(propertyValue, "\\.");
                        objectFrameName = objectFrameName.replace('#','_');
                    }
                    
                    if (currentLine.startsWith(toolTypeProperty))
                    {
                        toolType = propertyValue;
                    }
                    
                    if (currentLine.startsWith(cartesianPositionProperty))
                    {
                        cartesianPosition = propertyValue;      

                        String wobjDone = (String)wobjsDone.get(objectFrameName);
  
                        if (!objectFrameName.equals("wobj0") && (wobjDone == null || !wobjDone.equals("DONE")) )
                        {
                            wobjsDone.put(objectFrameName,"DONE");
                            
                            bw.write("  PERS wobjdata " + objectFrameName + ":=[");
                            if (toolType.equals("onrobot"))
                            {
                                bw.write("FALSE,TRUE,\"\",[");
                            }
                            else
                            {
                                bw.write("TRUE,TRUE,\"\",[");
                            }
                            String xyzStr = downloadXYZQuaternion(cartesianPosition);
                            bw.write(xyzStr + "],[[0,0,0],[1,0,0,0]]];\n");
                        }                                                
                    }                    
                }
            }            
 
           catch (IOException e) { 
                e.getMessage();
            }                                      
        }
       
        public void WriteSpeeddata(BufferedWriter bw)
        {
            try
            {
                bw.write("");                  
            }
           catch (IOException e) { 
                e.getMessage();
            }                                      
        }
        
        public void WriteZonedata(BufferedWriter bw)
        {
            try
            {
                bw.write("");                          
            }
            catch (IOException e) { 
                e.getMessage();
            }                                     
        }
   
        public void WriteSpotdata(BufferedWriter bw)
        {
            try
            {
                String spotNameProperty = "UserProfile.ABBspotdata";
                String progNoProperty = "UserProfile.ABBspotdata@progno";
                String tipNoProperty = "UserProfile.ABBspotdata@tipno";
                String gunPressureProperty = "UserProfile.ABBspotdata@gunpressure";
                String timerNoProperty = "UserProfile.ABBspotdata@timerno";
                
                String propertyValue = "";

                String spotName = "";
                String progNo = "";
                String tipNo = "";
                String gunPressure = "";
                String timerNo = "";

                
                Hashtable spotsDone = new Hashtable();
                
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
        
                    if (currentLine.startsWith(spotNameProperty) && spotName.equals(""))
                    {
                        spotName = propertyValue;
                       // System.out.println("V5 PROPS SPOT NAME: " +  spotName);
                    }
                    
                    if (currentLine.startsWith(progNoProperty))
                    {
                        progNo = propertyValue;
                    }
                    
                    if (currentLine.startsWith(tipNoProperty))
                    {
                        tipNo = propertyValue;      
                    }
                    if (currentLine.startsWith(gunPressureProperty))
                    {
                        gunPressure = propertyValue;                       
                    }
                    
                    if (currentLine.startsWith(timerNoProperty))
                    {
                        timerNo = propertyValue;

                        String spotDone = (String)spotsDone.get(spotName);
  
                        if (spotDone == null || !spotDone.equals("DONE"))
                        {
                            spotsDone.put(spotName,"DONE");
                            
                            bw.write("  PERS spotdata " + spotName + ":=[");
                           
                            bw.write( progNo + "," + tipNo + "," + gunPressure + "," + timerNo);
                            
                            bw.write("];\n");
                            spotName = "";
                        }                                                
                    }                    
                }
            }
            catch (IOException e) { 
                e.getMessage();
            }                                 
        }
 
        public void WriteGundata(BufferedWriter bw)
        {
            try
            {
                String gunNameProperty = "UserProfile.ABBgundata";                
                String noftipsProperty = "UserProfile.ABBgundata@noftips";
                String nofplevelsProperty = "UserProfile.ABBgundata@nofplevels";
                String closerequestProperty = "UserProfile.ABBgundata@closerequest";
                String openrequestProperty = "UserProfile.ABBgundata@openrequest";
                String tip1counterProperty = "UserProfile.ABBgundata@tip1counter";
                String tip2counterProperty = "UserProfile.ABBgundata@tip2counter";
                String tip1maxProperty = "UserProfile.ABBgundata@tip1max";
                String tip2maxProperty = "UserProfile.ABBgundata@tip2max";
                String closetime1Property = "UserProfile.ABBgundata@closetime1";
                String closetime2Property = "UserProfile.ABBgundata@closetime2";
                String closetime3Property = "UserProfile.ABBgundata@closetime3";
                String closetime4Property = "UserProfile.ABBgundata@closetime4";
                String buildupp1Property = "UserProfile.ABBgundata@buildupp1";
                String buildupp2Property = "UserProfile.ABBgundata@buildupp2";
                String buildupp3Property = "UserProfile.ABBgundata@buildupp3";
                String buildupp4Property = "UserProfile.ABBgundata@buildupp4";
                String opentimeProperty = "UserProfile.ABBgundata@opentime";                
                
                String propertyValue = "";

                String gunName = "";                
                String noftips = "";
                String nofplevels = "";
                String closerequest = "";
                String openrequest = "";
                String tip1counter = "";
                String tip2counter = "";
                String tip1max = "";
                String tip2max = "";
                String closetime1 = "";
                String closetime2 = "";
                String closetime3 = "";
                String closetime4 = "";
                String buildupp1 = "";
                String buildupp2 = "";
                String buildupp3 = "";
                String buildupp4 = "";
                String opentime = "";       
                
                Hashtable gunsDone = new Hashtable();
                
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
        
                    if (currentLine.startsWith(gunNameProperty) && gunName.equals(""))
                    {
                        
                        gunName = propertyValue;
                      //  System.out.println("V5 PROPS GUN NAME: " +  gunName);
                    }
                    
                    if (currentLine.startsWith(noftipsProperty))
                    {
                        noftips = propertyValue;
                    }
                    
                    if (currentLine.startsWith(nofplevelsProperty))
                    {
                        nofplevels = propertyValue;      
                    }
                    
                    if (currentLine.startsWith(closerequestProperty))
                    {
                        closerequest = propertyValue;                       
                    }
                    
                    if (currentLine.startsWith(openrequestProperty))
                    {
                        openrequest = propertyValue;
                    }
                    
                    if (currentLine.startsWith(tip1counterProperty))
                    {
                        tip1counter = propertyValue;
                    }
                    
                    if (currentLine.startsWith(tip2counterProperty))
                    {
                        tip2counter = propertyValue;
                    }
                    
                    if (currentLine.startsWith(tip1maxProperty))
                    {
                        tip1max = propertyValue;
                    }
                    
                    if (currentLine.startsWith(tip2maxProperty))
                    {
                        tip2max = propertyValue;
                    }
                    
                    if (currentLine.startsWith(closetime1Property))
                    {
                        closetime1 = propertyValue;
                    }
                    
                    if (currentLine.startsWith(closetime2Property))
                    {
                        closetime2 = propertyValue;
                    }
                    
                    if (currentLine.startsWith(closetime3Property))
                    {
                        closetime3 = propertyValue;
                    }
                    
                    if (currentLine.startsWith(closetime4Property))
                    {
                        closetime4 = propertyValue;
                    }

                    if (currentLine.startsWith(buildupp1Property))
                    {
                        buildupp1 = propertyValue;
                    }
                    
                    if (currentLine.startsWith(buildupp2Property))
                    {
                        buildupp2 = propertyValue;
                    }
                    
                    if (currentLine.startsWith(buildupp3Property))
                    {
                        buildupp3 = propertyValue;
                    }
                    
                    if (currentLine.startsWith(buildupp4Property))
                    {
                        buildupp4 = propertyValue;
                    }
                    
                    if (currentLine.startsWith(opentimeProperty))
                    {
                        opentime = propertyValue;

                        String gunDone = (String)gunsDone.get(gunName);
  
                        if (gunDone == null || !gunDone.equals("DONE"))
                        {
                            gunsDone.put(gunName,"DONE");
                            
                            bw.write("  PERS gundata " + gunName + ":=[");
                           
                            bw.write( noftips + "," + nofplevels + "," + closerequest + "," + openrequest + "," + tip1counter + "," + tip2counter + "," + tip1max + "," + tip2max + "," + 
                                      closetime1 + "," + closetime2 + "," + closetime3 + "," + closetime4 + "," + buildupp1 + "," + buildupp2 + "," + buildupp3 + "," + buildupp4 + "," + opentime);
                            
                            bw.write("];\n");
                            
                            gunName = "";
                        }                                                
                    }                    
                }                                        
            }
           catch (IOException e) { 
                e.getMessage();
            }                                      
        }         
 
        public void WriteWelddata(BufferedWriter bw)
        {
            try
            {
                String weldNameProperty = "UserProfile.ABBwelddata";                
                String weldschedProperty = "UserProfile.ABBwelddata@weldsched";
                String weldspeedProperty = "UserProfile.ABBwelddata@weldspeed";
                String weldvoltageProperty = "UserProfile.ABBwelddata@weldvoltage";
                String weldcurrentProperty = "UserProfile.ABBwelddata@weldcurrent";
                String weldwirefeedProperty = "UserProfile.ABBwelddata@weldwirefeed";
                String delaydistanceProperty = "UserProfile.ABBwelddata@delaydistance";
                String weldvoltadjProperty = "UserProfile.ABBwelddata@weldvoltadj";
                String weldcurradjProperty = "UserProfile.ABBwelddata@weldcurradj";
                String orgweldspeedProperty = "UserProfile.ABBwelddata@orgweldspeed";
                String orgweldvoltageProperty = "UserProfile.ABBwelddata@orgweldvoltage";
                String orgweldwfeedProperty = "UserProfile.ABBwelddata@orgweldwfeed";           
                
                String propertyValue = "";
               
                String weldName = "";                
                String weldsched = "UserProfile.ABBwelddata@weldsched";
                String weldspeed = "UserProfile.ABBwelddata@weldspeed";
                String weldvoltage = "UserProfile.ABBwelddata@weldvoltage";
                String weldcurrent = "UserProfile.ABBwelddata@weldcurrent";
                String weldwirefeed = "UserProfile.ABBwelddata@weldwirefeed";
                String delaydistance = "UserProfile.ABBwelddata@delaydistance";
                String weldvoltadj = "UserProfile.ABBwelddata@weldvoltadj";
                String weldcurradj = "UserProfile.ABBwelddata@weldcurradj";
                String orgweldspeed = "UserProfile.ABBwelddata@orgweldspeed";
                String orgweldvoltage = "UserProfile.ABBwelddata@orgweldvoltage";
                String orgweldwfeed = "UserProfile.ABBwelddata@orgweldwfeed";           
                
                Hashtable weldsDone = new Hashtable();
                
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
        
                    if (currentLine.startsWith(weldNameProperty) && weldName.equals(""))
                    {
                        
                        weldName = propertyValue;
                      //  System.out.println("V5 PROPS weld NAME: " +  weldName);
                    }
                    
                    if (currentLine.startsWith(weldschedProperty))
                    {
                        weldsched = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weldspeedProperty))
                    {
                        weldspeed = propertyValue;      
                    }
                    
                    if (currentLine.startsWith(weldvoltageProperty))
                    {
                        weldvoltage = propertyValue;                       
                    }
                    
                    if (currentLine.startsWith(weldwirefeedProperty))
                    {
                        weldwirefeed = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weldcurrentProperty))
                    {
                        weldcurrent = propertyValue;
                    }
                    
                    if (currentLine.startsWith(delaydistanceProperty))
                    {
                        delaydistance = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weldvoltadjProperty))
                    {
                        weldvoltadj = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weldcurradjProperty))
                    {
                        weldcurradj = propertyValue;
                    }
                    
                    if (currentLine.startsWith(orgweldspeedProperty))
                    {
                        orgweldspeed = propertyValue;
                    }
                    
                    if (currentLine.startsWith(orgweldvoltageProperty))
                    {
                        orgweldvoltage = propertyValue;
                    }
                    
                    if (currentLine.startsWith(orgweldwfeedProperty))
                    {
                        orgweldwfeed = propertyValue;

                        String weldDone = (String)weldsDone.get(weldName);
  
                        if (weldDone == null || !weldDone.equals("DONE"))
                        {
                            weldsDone.put(weldName,"DONE");
                            
                            bw.write("  PERS welddata " + weldName + ":=[");
                           
                            bw.write( weldsched + "," + weldspeed + "," + weldvoltage + "," + weldwirefeed + "," + weldcurrent + "," + delaydistance + "," + weldvoltadj + "," + 
                                      weldcurradj + "," + orgweldspeed + "," + orgweldvoltage + "," + orgweldwfeed);
                            
                            bw.write("];\n");
                            
                            weldName = "";
                        }                                                
                    }                    
                }                                        
            }
           catch (IOException e) { 
                e.getMessage();
            }                                      
        }
 
         public void WriteSeamdata(BufferedWriter bw)
        {
            try
            {
                String seamNameProperty = "UserProfile.ABBseamdata";                
                String purgetimeProperty = "UserProfile.ABBseamdata@purgetime";
                String preflowtimeProperty = "UserProfile.ABBseamdata@preflowtime";
                String ignschedProperty = "UserProfile.ABBseamdata@ignsched";
                String ignvoltageProperty = "UserProfile.ABBseamdata@ignvoltage";
                String ignwirefeedProperty = "UserProfile.ABBseamdata@ignwirefeed";
                String igncurrentProperty = "UserProfile.ABBseamdata@igncurrent";
                String ignvoltadjProperty = "UserProfile.ABBseamdata@ignvoltadj";
                String igncurradjProperty = "UserProfile.ABBseamdata@igncurradj";
                String ignmovedelayProperty = "UserProfile.ABBseamdata@ignmovedelay";
                String scrapestartProperty = "UserProfile.ABBseamdata@scrapestart";
                String heatspeedProperty = "UserProfile.ABBseamdata@heatspeed";
                String heattimeProperty = "UserProfile.ABBseamdata@heattime";
                String heatdistanceProperty = "UserProfile.ABBseamdata@heatdistance";
                String heatschedProperty = "UserProfile.ABBseamdata@heatsched";
                String heatvoltageProperty = "UserProfile.ABBseamdata@heatvoltage";
                String heatwirefeedProperty = "UserProfile.ABBseamdata@heatwirefeed";
                String heatcurrentProperty = "UserProfile.ABBseamdata@heatcurrent";
                String heatvoltadjProperty = "UserProfile.ABBseamdata@heatvoltadj";
                String heatcurradjProperty = "UserProfile.ABBseamdata@heatcurradj";
                String cooltimeProperty = "UserProfile.ABBseamdata@cooltime";
                String filltimeProperty = "UserProfile.ABBseamdata@filltime";
                String bbacktimeProperty = "UserProfile.ABBseamdata@bbacktime";
                String rbacktimeProperty = "UserProfile.ABBseamdata@rbacktime";
                String postflowtimeProperty = "UserProfile.ABBseamdata@postflowtime";
                String fillschedProperty = "UserProfile.ABBseamdata@fillsched";
                String fillvoltageProperty = "UserProfile.ABBseamdata@fillvoltage";
                String fillwirefeedProperty = "UserProfile.ABBseamdata@fillwirefeed";
                String fillcurrentProperty = "UserProfile.ABBseamdata@fillcurrent";
                String fillvoltadjProperty = "UserProfile.ABBseamdata@fillvoltadj";
                String fillcurradjProperty = "UserProfile.ABBseamdata@fillcurradj";                
                
                String propertyValue = "";

                String seamName = "";                
                String purgetime = "";
                String preflowtime = "";
                String ignsched = "";
                String ignvoltage = "";
                String ignwirefeed = "";
                String igncurrent = "";
                String ignvoltadj = "";
                String igncurradj = "";
                String ignmovedelay = "";
                String scrapestart = "";
                String heatspeed = "";
                String heattime = "";
                String heatdistance = "";
                String heatsched = "";
                String heatvoltage = "";
                String heatwirefeed = "";
                String heatcurrent = "UserProfile.ABBseamdata@heatcurrent";
                String heatvoltadj = "UserProfile.ABBseamdata@heatvoltadj";
                String heatcurradj = "UserProfile.ABBseamdata@heatcurradj";
                String cooltime = "UserProfile.ABBseamdata@cooltime";
                String filltime = "UserProfile.ABBseamdata@filltime";
                String bbacktime = "UserProfile.ABBseamdata@bbacktime";
                String rbacktime = "UserProfile.ABBseamdata@rbacktime";
                String postflowtime = "UserProfile.ABBseamdata@postflowtime";
                String fillsched = "UserProfile.ABBseamdata@fillsched";
                String fillvoltage = "UserProfile.ABBseamdata@fillvoltage";
                String fillwirefeed = "UserProfile.ABBseamdata@fillwirefeed";
                String fillcurrent = "UserProfile.ABBseamdata@fillcurrent";
                String fillvoltadj = "UserProfile.ABBseamdata@fillvoltadj";
                String fillcurradj = "";       
                
                Hashtable seamsDone = new Hashtable();
                
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
        
                    if (currentLine.startsWith(seamNameProperty) && seamName.equals(""))
                    {
                        
                        seamName = propertyValue;
                      //  System.out.println("V5 PROPS seam NAME: " +  seamName);
                    }
                    
                    if (currentLine.startsWith(purgetimeProperty))
                    {
                        purgetime = propertyValue;
                    }
                    
                    if (currentLine.startsWith(preflowtimeProperty))
                    {
                        preflowtime = propertyValue;      
                    }
                    
                    if (currentLine.startsWith(ignschedProperty))
                    {
                        ignsched = propertyValue;                       
                    }
                    
                    if (currentLine.startsWith(ignvoltageProperty))
                    {
                        ignvoltage = propertyValue;
                    }
                    
                    if (currentLine.startsWith(ignwirefeedProperty))
                    {
                        ignwirefeed = propertyValue;
                    }
                    
                    if (currentLine.startsWith(igncurrentProperty))
                    {
                        igncurrent = propertyValue;
                    }
                    
                    if (currentLine.startsWith(ignvoltadjProperty))
                    {
                        ignvoltadj = propertyValue;
                    }
                    
                    if (currentLine.startsWith(igncurradjProperty))
                    {
                        igncurradj = propertyValue;
                    }
                    
                    if (currentLine.startsWith(ignmovedelayProperty))
                    {
                        ignmovedelay = propertyValue;
                    }
                    
                    if (currentLine.startsWith(scrapestartProperty))
                    {
                        scrapestart = propertyValue;
                    }
                    
                    if (currentLine.startsWith(heatspeedProperty))
                    {
                        heatspeed = propertyValue;
                    }
                    
                    if (currentLine.startsWith(heattimeProperty))
                    {
                        heattime = propertyValue;
                    }

                    if (currentLine.startsWith(heatdistanceProperty))
                    {
                        heatdistance = propertyValue;
                    }
                    
                    if (currentLine.startsWith(heatschedProperty))
                    {
                        heatsched = propertyValue;
                    }
                    
                    if (currentLine.startsWith(heatvoltageProperty))
                    {
                        heatvoltage = propertyValue;
                    }
                    
                    if (currentLine.startsWith(heatwirefeedProperty))
                    {
                        heatwirefeed = propertyValue;
                    }
 
                    if (currentLine.startsWith(heatcurrentProperty))
                    {
                        heatcurrent = propertyValue;
                    }
                    
                    if (currentLine.startsWith(heatvoltadjProperty))
                    {
                        heatvoltadj = propertyValue;      
                    }
                    
                    if (currentLine.startsWith(heatcurradjProperty))
                    {
                        heatcurradj = propertyValue;                       
                    }
                    
                    if (currentLine.startsWith(cooltimeProperty))
                    {
                        cooltime = propertyValue;
                    }
                    
                    if (currentLine.startsWith(filltimeProperty))
                    {
                        filltime = propertyValue;
                    }
                    
                    if (currentLine.startsWith(bbacktimeProperty))
                    {
                        bbacktime = propertyValue;
                    }
                    
                    if (currentLine.startsWith(rbacktimeProperty))
                    {
                        rbacktime = propertyValue;
                    }
                    
                    if (currentLine.startsWith(postflowtimeProperty))
                    {
                        postflowtime = propertyValue;
                    }
                    
                    if (currentLine.startsWith(fillschedProperty))
                    {
                        fillsched = propertyValue;
                    }
                    
                    if (currentLine.startsWith(fillvoltageProperty))
                    {
                        fillvoltage = propertyValue;
                    }
                    
                    if (currentLine.startsWith(fillwirefeedProperty))
                    {
                        fillwirefeed = propertyValue;
                    }
                    
                    if (currentLine.startsWith(fillcurrentProperty))
                    {
                        fillcurrent = propertyValue;
                    }

                    if (currentLine.startsWith(fillvoltadjProperty))
                    {
                        fillvoltadj = propertyValue;
                    }
                    
                    if (currentLine.startsWith(fillcurradjProperty))
                    {
                        fillcurradj = propertyValue;

                        String seamDone = (String)seamsDone.get(seamName);
  
                        if (seamDone == null || !seamDone.equals("DONE"))
                        {
                            seamsDone.put(seamName,"DONE");
                            
                            bw.write("  PERS seamdata " + seamName + ":=[");
                           
                            bw.write( purgetime + "," + preflowtime + "," + ignsched + "," + ignvoltage + "," + ignwirefeed + "," + igncurrent + "," + ignvoltadj + "," + igncurradj + "," + 
                                      ignmovedelay + "," + scrapestart + "," + heatspeed + "," + heattime + "," + heatdistance + "," + heatsched + "," + heatvoltage + "," + heatwirefeed + "," + heatcurrent + ","  + heatvoltadj + "," + heatcurradj + "," + cooltime + "," + filltime + "," + bbacktime + "," + rbacktime + "," + 
                                      postflowtime + "," + fillsched + "," + fillvoltage + "," + fillwirefeed + "," + fillcurrent + "," + fillvoltadj + "," + fillcurradj);
                            
                            bw.write("];\n");
                            
                            seamName = "";
                        }                                                
                    }                    
                }                                        
            }
           catch (IOException e) { 
                e.getMessage();
            }                                      
        }
       
        public void WriteWeavedata(BufferedWriter bw)
        {
            try
            {
                String weaveNameProperty = "UserProfile.ABBweavedata";                
                String weaveshapeProperty = "UserProfile.ABBweavedata@weaveshape";
                String weavetypeProperty = "UserProfile.ABBweavedata@weavetype";
                String weavelengthProperty = "UserProfile.ABBweavedata@weavelength";
                String weavewidthProperty = "UserProfile.ABBweavedata@weavewidth";
                String weaveheightProperty = "UserProfile.ABBweavedata@weaveheight";
                String dwellleftProperty = "UserProfile.ABBweavedata@dwellleft";
                String dwellcenterProperty = "UserProfile.ABBweavedata@dwellcenter";
                String dwellrightProperty = "UserProfile.ABBweavedata@dwellright";
                String weavedirProperty = "UserProfile.ABBweavedata@weavedir";
                String weavetiltProperty = "UserProfile.ABBweavedata@weavetilt";
                String weaveoriProperty = "UserProfile.ABBweavedata@weaveori";
                String weavebiasProperty = "UserProfile.ABBweavedata@weavebias";
                String weavesyncleftProperty = "UserProfile.ABBweavedata@weavesyncleft";
                String weavesyncrightProperty = "UserProfile.ABBweavedata@weavesyncright";
                String wgtrackonProperty = "UserProfile.ABBweavedata@wgtrackon";
         
                
                String propertyValue = "";
                
                Hashtable weavesDone = new Hashtable();

                String weaveName = "";                
                String weaveshape = "";
                String weavetype = "";
                String weavelength = "UserProfile.ABBweavedata@weavelength";
                String weavewidth = "UserProfile.ABBweavedata@weavewidth";
                String weaveheight = "UserProfile.ABBweavedata@weaveheight";
                String dwellleft = "UserProfile.ABBweavedata@dwellleft";
                String dwellcenter = "UserProfile.ABBweavedata@dwellcenter";
                String dwellright = "UserProfile.ABBweavedata@dwellright";
                String weavedir = "UserProfile.ABBweavedata@weavedir";
                String weavetilt = "UserProfile.ABBweavedata@weavetilt";
                String weaveori = "UserProfile.ABBweavedata@weaveori";
                String weavebias = "UserProfile.ABBweavedata@weavebias";
                String weavesyncleft = "UserProfile.ABBweavedata@weavesyncleft";
                String weavesyncright = "UserProfile.ABBweavedata@weavesyncright";
                String wgtrackon = "UserProfile.ABBweavedata@wgtrackon";
                
                // first store declaration type in array list
                for (int ii=0;ii<m_V5Properties.size();ii++)
                {
                    String currentLine = (String)m_V5Properties.get(ii);

                    String[] propertyComponents=currentLine.split(":");
                    if (propertyComponents.length == 2)
                    {
                         propertyValue = propertyComponents[1];
                    }
        
                    if (currentLine.startsWith(weaveNameProperty) && weaveName.equals(""))
                    {
                        
                        weaveName = propertyValue;
                      //  System.out.println("V5 PROPS weave NAME: " +  weaveName);
                    }
                    
                    if (currentLine.startsWith(weaveshapeProperty))
                    {
                        weaveshape = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weavetypeProperty))
                    {
                        weavetype = propertyValue;      
                    }
                    
                    if (currentLine.startsWith(weavelengthProperty))
                    {
                        weavelength = propertyValue;                       
                    }
                    
                    if (currentLine.startsWith(weavewidthProperty))
                    {
                        weavewidth = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weaveheightProperty))
                    {
                        weaveheight = propertyValue;
                    }
                    
                    if (currentLine.startsWith(dwellleftProperty))
                    {
                        dwellleft = propertyValue;
                    }
                    
                    if (currentLine.startsWith(dwellcenterProperty))
                    {
                        dwellcenter = propertyValue;
                    }
                    
                    if (currentLine.startsWith(dwellrightProperty))
                    {
                        dwellright = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weavedirProperty))
                    {
                        weavedir = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weavetiltProperty))
                    {
                        weavetilt = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weaveoriProperty))
                    {
                        weaveori = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weavebiasProperty))
                    {
                        weavebias = propertyValue;
                    }

                    if (currentLine.startsWith(weavesyncleftProperty))
                    {
                        weavesyncleft = propertyValue;
                    }
                    
                    if (currentLine.startsWith(weavesyncrightProperty))
                    {
                        weavesyncright = propertyValue;
                    }
                    
                    if (currentLine.startsWith(wgtrackonProperty))
                    {
                        wgtrackon = propertyValue;

                        String weaveDone = (String)weavesDone.get(weaveName);
  
                        if (weaveDone == null || !weaveDone.equals("DONE"))
                        {
                            weavesDone.put(weaveName,"DONE");
                            
                            bw.write("  PERS weavedata " + weaveName + ":=[");
                           
                            bw.write( weaveshape + "," + weavetype + "," + weavelength + "," + weavewidth + "," + weaveheight + "," + dwellleft + "," + dwellcenter + "," + 
                                      dwellright + "," + weavedir + "," + weavetilt + "," + weaveori + "," + weavebias + "," + weavesyncleft + "," + weavesyncright + "," + wgtrackon);
                            
                            bw.write("];\n");
                            
                            weaveName = "";
                        }                                                
                    }                    
                }                                                               
            }
           catch (IOException e) { 
                e.getMessage();
            }                                      
        }         
        
        public String formatValue(String value, boolean meters)
        {
            double val = Double.parseDouble(value);
            if (meters)
            {
                val = val*1000.0;
            }
            
            DecimalFormat df1 = new DecimalFormat("#####0.000");
            
            if (meters == false)
            {
            	df1.applyPattern("#####0.000000");
            }

            String newvalue = df1.format(val);

            if (Math.abs(val) < .000001)
            {
            	newvalue = "0.0";
            }
            return(newvalue);
        }
        
 	public String dgXyzyprToMatrix(String XYZYPR) {
            
  
                components = XYZYPR.split(",");  
                doTrig();
                dgMatrix[0][0] = cp*cr;
                dgMatrix[0][1] = cp*sr;
                dgMatrix[0][2] = -sp;
                dgMatrix[0][3] = 0.0;
                dgMatrix[1][0] = -cy*sr + sy*sp*cr;
                dgMatrix[1][1] = cy*cr + sy*sp*sr;
                dgMatrix[1][2] = sy*cp;
                dgMatrix[1][3] = 0.0;
                dgMatrix[2][0] = sy*sr + cy*sp*cr;
                dgMatrix[2][1] = -sy*cr + cy*sp*sr;
                dgMatrix[2][2] = cy*cp;
                dgMatrix[2][3] = 0.0;
                dgMatrix[3][0] = xx;
                dgMatrix[3][1] = yy;
                dgMatrix[3][2] = zz;
                dgMatrix[3][3] = 1.0;
            String result = "";
            return(result);  
        }
        
         public String dgCatXyzyprMatrix( String XYZYPR ){
            
  
                components = XYZYPR.split(",");  
                doTrig();
                dgCatMatrix[0][0] = cp*cr;
                dgCatMatrix[0][1] = cp*sr;
                dgCatMatrix[0][2] = -sp;
                dgCatMatrix[0][3] = 0.0;
                dgCatMatrix[1][0] = -cy*sr + sy*sp*cr;
                dgCatMatrix[1][1] = cy*cr + sy*sp*sr;
                dgCatMatrix[1][2] = sy*cp;
                dgCatMatrix[1][3] = 0.0;
                dgCatMatrix[2][0] = sy*sr + cy*sp*cr;
                dgCatMatrix[2][1] = -sy*cr + cy*sp*sr;
                dgCatMatrix[2][2] = cy*cp;
                dgCatMatrix[2][3] = 0.0;
                dgCatMatrix[3][0] = xx;
                dgCatMatrix[3][1] = yy;
                dgCatMatrix[3][2] = zz;
                dgCatMatrix[3][3] = 1.0;    
                
                tmpMatrix[0][0] = dgCatMatrix[0][0]*dgMatrix[0][0]
                               + dgCatMatrix[0][1]*dgMatrix[1][0]
                               + dgCatMatrix[0][2]*dgMatrix[2][0]
                               + dgCatMatrix[0][3]*dgMatrix[3][0];
                
                 tmpMatrix[0][1] = dgCatMatrix[0][0]*dgMatrix[0][1]
                               + dgCatMatrix[0][1]*dgMatrix[1][1]
                               + dgCatMatrix[0][2]*dgMatrix[2][1]
                               + dgCatMatrix[0][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[0][2] = dgCatMatrix[0][0]*dgMatrix[0][2]
                               + dgCatMatrix[0][1]*dgMatrix[1][2]
                               + dgCatMatrix[0][2]*dgMatrix[2][2]
                               + dgCatMatrix[0][3]*dgMatrix[3][2];
                 
                 tmpMatrix[0][3] = dgCatMatrix[0][0]*dgMatrix[0][3]
                               + dgCatMatrix[0][1]*dgMatrix[1][3]
                               + dgCatMatrix[0][2]*dgMatrix[2][3]
                               + dgCatMatrix[0][3]*dgMatrix[3][3]; 
                 
                tmpMatrix[1][0] = dgCatMatrix[1][0]*dgMatrix[0][0]
                               + dgCatMatrix[1][1]*dgMatrix[1][0]
                               + dgCatMatrix[1][2]*dgMatrix[2][0]
                               + dgCatMatrix[1][3]*dgMatrix[3][0];
                
                 tmpMatrix[1][1] = dgCatMatrix[1][0]*dgMatrix[0][1]
                               + dgCatMatrix[1][1]*dgMatrix[1][1]
                               + dgCatMatrix[1][2]*dgMatrix[2][1]
                               + dgCatMatrix[1][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[1][2] = dgCatMatrix[1][0]*dgMatrix[0][2]
                               + dgCatMatrix[1][1]*dgMatrix[1][2]
                               + dgCatMatrix[1][2]*dgMatrix[2][2]
                               + dgCatMatrix[1][3]*dgMatrix[3][2];
                 
                 tmpMatrix[1][3] = dgCatMatrix[1][0]*dgMatrix[0][3]
                               + dgCatMatrix[1][1]*dgMatrix[1][3]
                               + dgCatMatrix[1][2]*dgMatrix[2][3]
                               + dgCatMatrix[1][3]*dgMatrix[3][3]; 
                 
                tmpMatrix[2][0] = dgCatMatrix[2][0]*dgMatrix[0][0]
                               + dgCatMatrix[2][1]*dgMatrix[1][0]
                               + dgCatMatrix[2][2]*dgMatrix[2][0]
                               + dgCatMatrix[2][3]*dgMatrix[3][0];
                
                 tmpMatrix[2][1] = dgCatMatrix[2][0]*dgMatrix[0][1]
                               + dgCatMatrix[2][1]*dgMatrix[1][1]
                               + dgCatMatrix[2][2]*dgMatrix[2][1]
                               + dgCatMatrix[2][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[2][2] = dgCatMatrix[2][0]*dgMatrix[0][2]
                               + dgCatMatrix[2][1]*dgMatrix[1][2]
                               + dgCatMatrix[2][2]*dgMatrix[2][2]
                               + dgCatMatrix[2][3]*dgMatrix[3][2];
                 
                 tmpMatrix[2][3] = dgCatMatrix[2][0]*dgMatrix[0][3]
                               + dgCatMatrix[2][1]*dgMatrix[1][3]
                               + dgCatMatrix[2][2]*dgMatrix[2][3]
                               + dgCatMatrix[2][3]*dgMatrix[3][3]; 
                 
                tmpMatrix[3][0] = dgCatMatrix[3][0]*dgMatrix[0][0]
                               + dgCatMatrix[3][1]*dgMatrix[1][0]
                               + dgCatMatrix[3][2]*dgMatrix[2][0]
                               + dgCatMatrix[3][3]*dgMatrix[3][0];
                
                 tmpMatrix[3][1] = dgCatMatrix[3][0]*dgMatrix[0][1]
                               + dgCatMatrix[3][1]*dgMatrix[1][1]
                               + dgCatMatrix[3][2]*dgMatrix[2][1]
                               + dgCatMatrix[3][3]*dgMatrix[3][1];   
                 
                 tmpMatrix[3][2] = dgCatMatrix[3][0]*dgMatrix[0][2]
                               + dgCatMatrix[3][1]*dgMatrix[1][2]
                               + dgCatMatrix[3][2]*dgMatrix[2][2]
                               + dgCatMatrix[3][3]*dgMatrix[3][2];
                 
                 tmpMatrix[3][3] = dgCatMatrix[3][0]*dgMatrix[0][3]
                               + dgCatMatrix[3][1]*dgMatrix[1][3]
                               + dgCatMatrix[3][2]*dgMatrix[2][3]
                               + dgCatMatrix[3][3]*dgMatrix[3][3];  
                 
            /* now set dgMatrix equal to inverted */
            for (int ii=0;ii<4;ii++) {
                for (int kk=0;kk<4;kk++) {
                    dgMatrix[ii][kk] = tmpMatrix[ii][kk];
                }
            }
            
            String result = "";
            return(result);
        }
        
        public String dgMatrixToQuaternions() {
            double d11 = dgMatrix[0][0];
            double d21 = dgMatrix[0][1];
            double d31 = dgMatrix[0][2];
            
            double d12 = dgMatrix[1][0];
            double d22 = dgMatrix[1][1];
            double d32 = dgMatrix[1][2];
            
            double d13 = dgMatrix[2][0];
            double d23 = dgMatrix[2][1];
            double d33 = dgMatrix[2][2];
            
            xx = dgMatrix[3][0];
            yy = dgMatrix[3][1];
            zz = dgMatrix[3][2];
            
            double trace = d11 + d22 + d33;
            if ( (trace + 1.0) > 0.0) {
                q1 = Math.sqrt( trace + 1.0 )/2.0 ;
            }
            else {
                q1 = 0;
            }
            
            if (Math.abs(q1)  >  .002) {
                /* case where q1 is <> 0 */
                q2 = Math.sqrt( Math.abs(1.0 + 2.0*d11 -trace)) / 2.0;
                if (d32 < d23)
                    q2 = -1.0*q2;
                q3 = Math.sqrt( Math.abs(1.0 + 2.0*d22 -trace)) / 2.0;
                if (d13 < d31)
                    q3 = -1.0*q3;
                q4 = Math.sqrt( Math.abs(1.0 + 2.0*d33 -trace)) / 2.0;
                if (d21 < d12)
                    q4 = -1.0*q4;                
            }
            else {
                /* case where q1 is less than .002 and greater than -.002 */
                q1 = 0.0;
                double work_qq[]= {0.0,0.0,0.0};
                work_qq[0] = Math.sqrt( Math.abs(d11+1.0)/2.0);
                work_qq[1] = Math.sqrt( Math.abs(d22+1.0)/2.0);
                work_qq[2] = Math.sqrt( Math.abs(d33+1.0)/2.0);
                int ind = 0;
                double max_val = work_qq[0];
                for (int ii=1; ii<3; ii++)
                {
                    if (max_val<work_qq[ii])
                    {
                        ind = ii;
                        max_val = work_qq[ii];
                    }
                }
                
                switch (ind)
                {
                    case 0:
                        /* q1 largest */
                        q2 = work_qq[0];
                        q3 = d21/(2.0*work_qq[0]);
                        q4 = d31/(2.0*work_qq[0]);
                        break;
                    case 1:
                        /* q2 largest */
                        q3 = work_qq[1];
                        q2 = d12/(2.0*work_qq[1]);
                        q4 = d32/(2.0*work_qq[1]);
                        break;  
                    case 2:
                        /* q3 largest */
                        q4 = work_qq[2];
                        q2 = d13/(2.0*work_qq[2]);
                        q3 = d23/(2.0*work_qq[2]);
                        break;                          
                }
                
            }
            String result = "";
            return(result); 
        }       
        public String dgGetQ1() {
            if (Math.abs(q1) < .0002) {
                q1 = 0.0;
            }
            return(String.valueOf(q1));
        }
        public String dgGetQ2() {
            if (Math.abs(q2) < .0002) {
                q2 = 0.0;
            }
            return(String.valueOf(q2));
        }
        public String dgGetQ3() {
            if (Math.abs(q3) < .0002) {
                q3 = 0.0;
            }
            return(String.valueOf(q3));
        }

        public String dgGetQ4() {
            if (Math.abs(q4) < .0002) {
                q4 = 0.0;
            }
            return(String.valueOf(q4));
        }
        
        private  void doTrig() {
                           
                xx = Double.valueOf(components[0]).doubleValue();
                yy = Double.valueOf(components[1]).doubleValue();                
                zz = Double.valueOf(components[2]).doubleValue();
                yaw = Double.valueOf(components[3]).doubleValue();     
                pitch = Double.valueOf(components[4]).doubleValue();
                roll = Double.valueOf(components[5]).doubleValue();
                
                cr = Math.cos(roll*Math.PI/180.0);
                sr = Math.sin(roll*Math.PI/180.0);
                cp = Math.cos(pitch*Math.PI/180.0);
                sp = Math.sin(pitch*Math.PI/180.0);
                cy = Math.cos(yaw*Math.PI/180.0);
                sy = Math.sin(yaw*Math.PI/180.0);
        }  
        
        public String doRemove( String input, String removeStr)
        {
            String result = "";
            String [] toKeep = input.split(removeStr);
            for (int ii=0;ii<toKeep.length;ii++)
            {
                result += toKeep[ii];
            }
            return(result);
        }       

} // end class
