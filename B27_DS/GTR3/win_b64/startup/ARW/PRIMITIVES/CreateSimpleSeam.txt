HEADER "Seam Creation Primitive"
	COMMENT "This primitive can be used to create Paths"

  SEAM 
	-- Seam Creation
	SeamStart FREE "Auto Seam" 
	SeamEnd FREE "Auto Seam" 
  END SEAM
END
