HEADER "Basic Auxiliary Axes Definition"
	

  SEAM 
  	-- Auto Seam
	SeamStart FIXED "Auto Seam" 
	SeamEnd FIXED "Auto Seam"

	StretchMin FIXED 0
	StretchMax FIXED 1000

	-- Aux Axes Variables 
	AutoPositioner FREE "Yes"
	AutoPositionerDirection {0 0 -1}
	AuxAxis1Mode FREE "Moving"
    	AzimuthZero FREE "Tag Point"
    	StartAzimuth FREE 60
    	MiddleAzimuth FREE 60
    	EndAzimuth FREE 60

    	StartStretch FREE 70
    	MiddleStretch FREE 70
    	EndStretch FREE 70

    	StartElevation FREE 60
    	MiddleElevation FREE 60
    	EndElevation FREE 60

  END SEAM
END
