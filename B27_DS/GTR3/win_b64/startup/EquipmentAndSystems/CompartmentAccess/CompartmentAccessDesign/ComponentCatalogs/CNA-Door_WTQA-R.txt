PartNumber	ClearOpeningWidth (in)	ClearOpeningHeight (in)	SwingType	NumberOfDogs	MaxPressure (psi)
D-WTQA-18X36-R6-15	18	36	Right-hand	6	15
D-WTQA-18X36-R6-30	18	36	Right-hand	6	30
D-WTQA-26X45-R6-10	26	45	Right-hand	6	10
D-WTQA-26X45-R8-20	26	45	Right-hand	8	20
D-WTQA-26X45-R10-30	26	45	Right-hand	10	30
D-WTQA-26X54-R8-15	26	54	Right-hand	8	15
D-WTQA-26X54-R10-30	26	54	Right-hand	10	30
D-WTQA-26X57-R8-5	26	57	Right-hand	8	5
D-WTQA-26X57-R10-12.5	26	57	Right-hand	10	12.5
D-WTQA-26X66-R8-10	26	66	Right-hand	8	10
D-WTQA-26X66-R10-10	26	66	Right-hand	10	10
D-WTQA-26X66-R10-15	26	66	Right-hand	10	15
D-WTQA-30X66-R8-5	30	66	Right-hand	8	5
D-WTQA-30X66-R10-10	30	66	Right-hand	10	10
D-WTQA-30X66-R10-15	30	66	Right-hand	10	15
D-WTQA-36X66-R10-5	36	66	Right-hand	10	5
D-WTQA-36X66-R10-10	36	66	Right-hand	10	10
