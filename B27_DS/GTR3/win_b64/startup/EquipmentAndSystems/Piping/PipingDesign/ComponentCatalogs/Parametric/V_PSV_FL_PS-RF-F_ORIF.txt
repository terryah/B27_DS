PartNumber	NominalSize	NominalSize2	Rating	OutsideDiameter (in)	OutsideDiameter2 (in)	CenterToFaceLength (in)	CenterToFaceLength2 (in)	Height (in)	EndDiameter (in)	EndThickness (in)	EndDiameter2 (in)	EndThickness2 (in)	
V-PSV-FL-RF-PS-F-150-1.5in-2in	1 1/2in	2in	150#	1.9	2.375	4.75	4.875	13.875	5	0.689	6	0.752	
V-PSV-FL-RF-PS-F-300-1.5in-2in	1 1/2in	2in	300#	1.9	2.375	6	4.875	14.375	6.118	0.811	6.5	0.882	
V-PSV-FL-RF-PS-F-600-1.5in-2in	1 1/2in	2in	600#	1.9	2.375	6	4.875	15.125	6.118	0.882	6.5	1	
V-PSV-FL-RF-PS-F-900-1.5in-2.5in	1 1/2in	2 1/2in	900#	1.9	2.875	6	4.875	18.625	7	1.252	9.618	1.618	
V-PSV-FL-RF-PS-F-1500-1.5in-2.5in	1 1/2in	2 1/2in	1500#	1.9	2.875	6	4.875	18.625	7	1.252	9.618	1.618	
V-PSV-FL-RF-PS-F-2500-1.5in-2.5in	1 1/2in	2 1/2in	2500#	1.9	2.875	6.5	5.5	22	8	1.752	10.5	2.252	
