PartType1	Standard1	EndStyle1	PartType2	Standard2	EndStyle2	AutomaticPartType
Not-PipingGasket	ASTL	RAISED FACE	Not-PipingGasket	ASTL	RAISED FACE	PipingGasket
Not-PipingGasket	ASTL	RING JOIN	Not-PipingGasket	ASTL	RING JOIN	PipingGasket
Not-PipingGasket	ASTL	BUTT WELD	Not-PipingGasket	ASTL	BUTT WELD	MldInLineWeld
Not-PipingGasket	ASTL	WELD SET ON	Not-PipingGasket	ASTL	WELD SET ON	CATMldBranchWeld
All	ASTL	BUTT WELD	Not-PipingGasket	ASTL	RAISED FACE	PipingNonReduceFlange
All	ASTL	BUTT WELD	Not-PipingGasket	ASTL	RING JOIN	PipingNonReduceFlange
All	ASTL	PLAIN END	Not-PipingGasket	ASTL	RAISED FACE	PipingNonReduceFlange
All	ASTL	PLAIN END	Not-PipingGasket	ASTL	RING JOIN	PipingNonReduceFlange
All	ASTL	THREADED MALE	Not-PipingGasket	ASTL	RAISED FACE	PipingNonReduceFlange
All	ASTL	THREADED MALE	Not-PipingGasket	ASTL	RING JOIN	PipingNonReduceFlange
Not-PipingGasket	ASTL	BUTT WELD	FreeEnd			MldInLineWeld
All	ASTL	BUTT WELD	FreeEnd			PipingNonReduceFlange

