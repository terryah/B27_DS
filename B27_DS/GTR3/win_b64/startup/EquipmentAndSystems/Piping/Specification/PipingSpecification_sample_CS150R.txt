PartType	Standard	PartName	MaterialCategory	MaterialCode	Schedule	EndStyle	EndStyle2	MinOutsideDiameter(in)	MaxOutsideDiameter(in)	SymbolName	Rating	NamingRuleKW
Tee	ASTL	TEE	Carbon steel	A234-WPB	STANDARD	BUTT WELD		0.5	49	B_TEE_WM		
Reducing Tee	ASTL	TEE REDUCING	Carbon steel	A234-WPB	STANDARD	BUTT WELD		0.5	49	B_TEER_WM		PartNumber
Elbow	ASTL		Carbon steel	A234-WPB	STANDARD	BUTT WELD		0.5	49			
Cap	ASTL		Carbon steel	A234-WPB	STANDARD	BUTT WELD		0.5	49	F_CAP_WM		
Concentric Reducer	ASTL	REDUCER CONCENTRIC	Carbon steel	A234-WPB	STANDARD	BUTT WELD		0.75	49	F_REDCON_WM		
Eccentric Reducer	ASTL	REDUCER ECCENTRIC	Carbon steel	A234-WPB	STANDARD	BUTT WELD		0.75	49	F_REDECC_WM		
Flange	ASTL	FLANGE WELDNECK	Carbon steel	A105	STANDARD		BUTT WELD	0.5	49		150#	
Pipe	ASTL	PIPE	Carbon steel	ASTM_A_53		BUTT WELD		0.125	49	PIPE_STRAIGHT		
Pipe with bends	ASTL	PIPE	Carbon steel	ASTM_A_53		BUTT WELD		0.125	49	PIPE_BENDABLE		
Ball Valve	ASTL	BALL VALVE	Carbon steel	A216-WCB				0.5	25		150#	
Check Valve	ASTL	CHECK VALVE. SWING	Carbon steel	A216-WCB				0.5	25		150#	
Gate Valve	ASTL	GATE VALVE. HAND WHEEL	Carbon steel	A216-WCB				0.5	25		150#	
Globe Valve	ASTL	GLOBE VALVE. HAND WHEEL	Carbon steel	A216-WCB				0.5	25		150#	
Gasket	ASTL		Carbon steel	SPIRALW_CS							150#	
Branch Weld										WELD		
In-Line Weld							 	 		WELD
Olet	ASTL		Carbon steel	A105		WELD SET ON	BUTT WELD	2	25	
