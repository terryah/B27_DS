PartNumber	NominalSize	OutsideHeight (in)	OutsideWidth (in)	InsideHeight (in)	InsideWidth (in)	RailFlangeThickness (in)	RailFlangeWidth (in)	RailWallThickness (in)	RungHeight (in)	RungLength (in)	FloorSectionSpacing (in)	Angle (deg)	BendRadius (in)	TangentLength (in)	Series	PartName
B34AL-06-30VI12	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-09-30VI12	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-12-30VI12	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-18-30VI12	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-24-30VI12	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-30-30VI12	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-36-30VI12	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-42-30VI12	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	30	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 12in rad
B34AL-06-30VI24	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-09-30VI24	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-12-30VI24	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-18-30VI24	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-24-30VI24	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-30-30VI24	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-36-30VI24	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-42-30VI24	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	30	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 30 deg 24in rad
B34AL-06-45VI12	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-09-45VI12	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-12-45VI12	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-18-45VI12	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-24-45VI12	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-30-45VI12	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-36-45VI12	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-42-45VI12	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	45	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 12in rad
B34AL-06-45VI24	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-09-45VI24	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-12-45VI24	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-18-45VI24	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-24-45VI24	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-30-45VI24	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-36-45VI24	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-42-45VI24	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	45	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 45 deg 24in rad
B34AL-06-60VI12	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-09-60VI12	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-12-60VI12	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-18-60VI12	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-24-60VI12	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-30-60VI12	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-36-60VI12	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-42-60VI12	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	60	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 12in rad
B34AL-06-60VI24	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-09-60VI24	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-12-60VI24	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-18-60VI24	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-24-60VI24	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-30-60VI24	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-36-60VI24	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-42-60VI24	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	60	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 60 deg 24in rad
B34AL-06-90VI12	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-09-90VI12	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-12-90VI12	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-18-90VI12	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-24-90VI12	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-30-90VI12	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-36-90VI12	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-42-90VI12	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	90	12	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 12in rad
B34AL-06-90VI24	B34-06in	4.2	7.5	3.08	6	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B34AL-09-90VI24	B34-09in	4.2	10.5	3.08	9	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B34AL-12-90VI24	B34-12in	4.2	13.5	3.08	12	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B34AL-18-90VI24	B34-18in	4.2	19.5	3.08	18	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B34AL-24-90VI24	B34-24in	4.2	25.5	3.08	24	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B34AL-30-90VI24	B34-30in	4.2	31.5	3.08	30	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B34AL-36-90VI24	B34036in	4.2	37.5	3.08	36	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B34AL-42-90VI24	B34-42in	4.2	43.5	3.08	42	0.12	1.75	0.1	1	1	9	90	24	3	B Series 34  Aluminum 4in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-06-30VI12	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-09-30VI12	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-12-30VI12	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-18-30VI12	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-24-30VI12	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-30-30VI12	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-36-30VI12	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-42-30VI12	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	30	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 12in rad
B35AL-06-30VI24	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-09-30VI24	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-12-30VI24	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-18-30VI24	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-24-30VI24	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-30-30VI24	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-36-30VI24	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-42-30VI24	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	30	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 30 deg 24in rad
B35AL-06-45VI12	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-09-45VI12	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-12-45VI12	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-18-45VI12	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-24-45VI12	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-30-45VI12	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-36-45VI12	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-42-45VI12	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	45	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 12in rad
B35AL-06-45VI24	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-09-45VI24	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-12-45VI24	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-18-45VI24	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-24-45VI24	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-30-45VI24	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-36-45VI24	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-42-45VI24	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	45	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 45 deg 24in rad
B35AL-06-60VI12	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-09-60VI12	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-12-60VI12	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-18-60VI12	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-24-60VI12	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-30-60VI12	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-36-60VI12	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-42-60VI12	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	60	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 12in rad
B35AL-06-60VI24	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-09-60VI24	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-12-60VI24	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-18-60VI24	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-24-60VI24	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-30-60VI24	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-36-60VI24	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-42-60VI24	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	60	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 60 deg 24in rad
B35AL-06-90VI12	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-09-90VI12	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-12-90VI12	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-18-90VI12	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-24-90VI12	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-30-90VI12	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-36-90VI12	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-42-90VI12	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	90	12	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 12in rad
B35AL-06-90VI24	B35-06in	5.06	7.49	3.96	6	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-09-90VI24	B35-09in	5.06	10.49	3.96	9	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-12-90VI24	B35-12in	5.06	13.49	3.96	12	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-18-90VI24	B35-18in	5.06	19.49	3.96	18	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-24-90VI24	B35-24in	5.06	25.49	3.96	24	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-30-90VI24	B35-30in	5.06	31.49	3.96	30	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-36-90VI24	B35036in	5.06	37.49	3.96	36	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B35AL-42-90VI24	B35-42in	5.06	43.49	3.96	42	0.1	1.75	0.09	1	1	9	90	24	3	B Series 35  Aluminum 5in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-06-30VI12	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-09-30VI12	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-12-30VI12	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-18-30VI12	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-24-30VI12	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-30-30VI12	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-36-30VI12	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-42-30VI12	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	30	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 12in rad
B36AL-06-30VI24	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-09-30VI24	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-12-30VI24	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-18-30VI24	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-24-30VI24	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-30-30VI24	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-36-30VI24	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-42-30VI24	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	30	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 30 deg 24in rad
B36AL-06-45VI12	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-09-45VI12	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-12-45VI12	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-18-45VI12	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-24-45VI12	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-30-45VI12	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-36-45VI12	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-42-45VI12	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	45	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 12in rad
B36AL-06-45VI24	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-09-45VI24	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-12-45VI24	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-18-45VI24	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-24-45VI24	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-30-45VI24	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-36-45VI24	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-42-45VI24	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	45	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 45 deg 24in rad
B36AL-06-60VI12	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-09-60VI12	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-12-60VI12	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-18-60VI12	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-24-60VI12	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-30-60VI12	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-36-60VI12	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-42-60VI12	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	60	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 12in rad
B36AL-06-60VI24	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-09-60VI24	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-12-60VI24	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-18-60VI24	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-24-60VI24	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-30-60VI24	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-36-60VI24	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-42-60VI24	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	60	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 60 deg 24in rad
B36AL-06-90VI12	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-09-90VI12	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-12-90VI12	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-18-90VI12	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-24-90VI12	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-30-90VI12	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-36-90VI12	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-42-90VI12	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	90	12	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 12in rad
B36AL-06-90VI24	B36-06in	6.17	7.45	5.06	6	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-09-90VI24	B36-09in	6.17	10.45	5.06	9	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-12-90VI24	B36-12in	6.17	13.45	5.06	12	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-18-90VI24	B36-18in	6.17	19.45	5.06	18	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-24-90VI24	B36-24in	6.17	25.45	5.06	24	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-30-90VI24	B36-30in	6.17	31.45	5.06	30	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-36-90VI24	B36036in	6.17	37.45	5.06	36	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B36AL-42-90VI24	B36-42in	6.17	43.45	5.06	42	0.11	2	0.075	1	1	9	90	24	3	B Series 36  Aluminum 6in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-06-30VI12	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-09-30VI12	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-12-30VI12	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-18-30VI12	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-24-30VI12	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-30-30VI12	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-36-30VI12	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-42-30VI12	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	30	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 12in rad
B37AL-06-30VI24	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-09-30VI24	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-12-30VI24	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-18-30VI24	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-24-30VI24	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-30-30VI24	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-36-30VI24	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-42-30VI24	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	30	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 30 deg 24in rad
B37AL-06-45VI12	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-09-45VI12	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-12-45VI12	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-18-45VI12	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-24-45VI12	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-30-45VI12	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-36-45VI12	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-42-45VI12	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	45	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 12in rad
B37AL-06-45VI24	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-09-45VI24	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-12-45VI24	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-18-45VI24	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-24-45VI24	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-30-45VI24	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-36-45VI24	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-42-45VI24	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	45	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 45 deg 24in rad
B37AL-06-60VI12	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-09-60VI12	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-12-60VI12	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-18-60VI12	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-24-60VI12	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-30-60VI12	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-36-60VI12	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-42-60VI12	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	60	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 12in rad
B37AL-06-60VI24	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-09-60VI24	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-12-60VI24	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-18-60VI24	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-24-60VI24	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-30-60VI24	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-36-60VI24	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-42-60VI24	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	60	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 60 deg 24in rad
B37AL-06-90VI12	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-09-90VI12	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-12-90VI12	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-18-90VI12	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-24-90VI12	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-30-90VI12	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-36-90VI12	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-42-90VI12	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	90	12	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 12in rad
B37AL-06-90VI24	B37-06in	7.14	7.5	6.05	6	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-09-90VI24	B37-09in	7.14	10.5	6.05	9	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-12-90VI24	B37-12in	7.14	13.5	6.05	12	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-18-90VI24	B37-18in	7.14	19.5	6.05	18	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-24-90VI24	B37-24in	7.14	25.5	6.05	24	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-30-90VI24	B37-30in	7.14	31.5	6.05	30	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-36-90VI24	B37037in	7.14	37.5	6.05	36	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
B37AL-42-90VI24	B37-42in	7.14	43.5	6.05	42	0.09	2	0.075	1	1	9	90	24	3	B Series 37  Aluminum 7in rails	Vertical Inside Bend 90 deg 24in rad
