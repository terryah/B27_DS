FunctionName	MinNominalSize	MaxNominalSize	PhysicalName
TubingBranchFunc	3/16in	3in	TubingTee
TubingBranchFunc	3/16in	3in	TubingCross
TubingBranchFunc	3/16in	3in	TubingAdaptor
TubingBranchFunc	3/16in	3in	TubingHalfCoupling
