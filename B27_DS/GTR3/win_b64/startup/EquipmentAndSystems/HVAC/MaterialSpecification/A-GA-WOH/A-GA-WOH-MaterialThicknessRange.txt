MinSize(in)	MaxSize(in)	DuctThickness(in)	MaterialCategory
3	12	.037	Perforated Aluminum
0	18	.053	Aluminum Sheet
18.001	29.999	.065	Aluminum Sheet
30	99	.085	Aluminum Sheet
0	18 	.043	Stainless Steel Sheet
18.001	29.999	.055	Stainless Steel Sheet
30	99	.075	Stainless Steel Sheet
0	29.999	.058	Galvanized Sheet
30	99	.08	Galvanized Sheet
