FunctionName	MinSize	MaxSize	PhysicalPart
HVACDuctFunc	1in	48in	HVACFltOvlStDuct
HVACDuctFunc	1in	48in	HVACFltOvlNReducElbow
HVACDuctFunc	1in	48in	HVACFltOvlCollar
HVACDuctFunc	1in	48in	HVACFltOvlCoupling
HVACDuctFunc	1in	48in	HVACFltOvlEndPlate
HVACDuctFunc	1in	48in	HVACFltOvlLFlange
HVACDuctFunc	1in	48in	HVACFltOvlIFlange
HVACDuctFunc	1in	48in	HVACFltOvlGasket
HVACDuctFunc	1in	48in	HVACFltOvlToOvlOffset
HVACDuctFunc	1in	48in	HVACFltOvlOgee
HVACDuctFunc	1in	48in	HVACFltOvlToOvl
HVACDuctFunc	1in	48in	HVACFltOvlToRnd
HVACDuctFunc	1in	48in	HVACFltOvlToRect



