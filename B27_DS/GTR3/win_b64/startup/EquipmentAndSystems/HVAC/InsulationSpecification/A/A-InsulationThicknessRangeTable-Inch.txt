MinSize(in)	MaxSize(in)	MinTemperature(Fdeg)	MaxTemperature(Fdeg)	InsulationThickness(in)
0	6.0	50	100	0.0625in
6.01	10.0 	70	100	0.125in
10.01	30 	80	100	0.1875in
30.01	72 	90	100	0.25in

