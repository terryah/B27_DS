PartNumber	NominalSize	InsideWidth (in)	InsideHeight (in)	Width (in)	Height (in)	RidgeWidth (in)	RidgeToRidgeOffset (in)	FlangeOutsideWidth (in)	FlangeOutsideHeight (in)	FlangeThickness (in)	FlangeChamfer (in)	FaceToFaceLength (in)	GasketWidth (in)	GasketHeight (in)	GasketCrossSectionWidth (in)	GasketThickness (in)
RD_FILTER_CEGE-WRD180	WRD180	0.288	0.134	0.368	0.214	0.072	0.057	0.88	0.88	0.19	0.075	2.1	0.373	0.219	0.074	0.048
RD_FILTER_CEGE-WRD110	WRD110	0.471	0.219	0.551	0.299	0.118	0.093	1	1	0.19	0.075	2.1	0.558	0.306	0.074	0.048
RD_FILTER_CEGE-WRD750	WRD750	0.691	0.321	0.791	0.421	0.173	0.136	1.38	1.38	0.25	0.12	2.5	0.81	0.44	0.092	0.058
RD_FILTER_CEGE-WRD700	WRD700	0.686	0.31	0.786	0.41	0.173	0.105	1.38	1.38	0.25	0.12	2.5	0.81	0.44	0.092	0.058
RD_FILTER_CEGE-WRD650	WRD650	0.72	0.321	0.82	0.421	0.173	0.101	1.38	1.38	0.25	0.12	2.5	0.81	0.44	0.094	0.058
RD_FILTER_CEGE-WRD580	WRD580	0.78	0.37	0.88	0.47	0.2	0.12	1.38	1.38	0.25	0.12	2.5	0.9	0.5	0.092	0.058
RD_FILTER_CEGE-WRD500	WRD500	0.752	0.323	0.852	0.423	0.188	0.063	1.69	1.25	0.25	0.25	2.8	0.872	0.443	0.094	0.056
RD_FILTER_CEGE-WRD475	WRD475	1.09	0.506	1.19	0.606	0.272	0.215	1.97	1.39	0.25	0.25	2.8	1.21	0.63	0.094	0.058
RD_FILTER_CEGE-WRD350	WRD350	1.48	0.688	1.608	0.816	0.37	0.292	2.75	2	0.25	0.25	2.8	1.75	0.96	0.11	0.048
