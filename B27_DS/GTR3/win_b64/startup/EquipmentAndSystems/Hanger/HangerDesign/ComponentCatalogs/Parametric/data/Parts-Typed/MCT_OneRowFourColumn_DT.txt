PartNumber	Size	MCT_InternalHeight (mm)	MCT_FrameThickness (mm)	MCT_InternalWidth (mm)	No_Of_Rows	No_Of_Columns	EndPackingHeight (mm)	MCT_Frame_Depth (mm)
MCT_RGS_2x4	2	60	10	120	1	4	40	60
MCT_RGS_4x4	4	120	10	120	1	4	40	60
MCT_RGS_6x4	6	180	10	120	1	4	40	60
MCT_RGS_8x4	8	240	10	120	1	4	40	60
