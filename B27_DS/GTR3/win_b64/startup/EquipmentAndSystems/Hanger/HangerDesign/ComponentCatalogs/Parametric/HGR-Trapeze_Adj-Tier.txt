TierPartNumber	TierMaterial	TierLength (in)	TierHeight (in)	TierDepth (in)	TierThickness (in)
3CT-06	Aluminum	6	1.25	2	0.125
3CT-07	Aluminum	7	1.25	2	0.125
3CT-08	Aluminum	8	1.25	2	0.125
3CT-09	Aluminum	9	1.25	2	0.125
3CT-10	Aluminum	10	1.25	2	0.125
3CT-11	Aluminum	11	1.25	2	0.125
3CT-12	Aluminum	12	1.25	2	0.125
3CT-13	Aluminum	13	1.25	2	0.125
3CT-14	Aluminum	14	1.25	2	0.125
3CT-15	Aluminum	15	1.25	2	0.125
3CT-16	Aluminum	16	1.25	2	0.125
3CT-17	Aluminum	17	1.25	2	0.125
3CTS-06	Steel	6	1.125	1.75	0.125
3CTS-07	Steel	7	1.125	1.75	0.125
3CTS-08	Steel	8	1.125	1.75	0.125
3CTS-09	Steel	9	1.125	1.75	0.125
3CTS-10	Steel	10	1.125	1.75	0.125
3CTS-11	Steel	11	1.125	1.75	0.125
3CTS-12	Steel	12	1.125	1.75	0.125
3CTS-13	Steel	13	1.125	1.75	0.125
3CTS-14	Steel	14	1.125	1.75	0.125
3CTS-15	Steel	15	1.125	1.75	0.125
3CTS-16	Steel	16	1.125	1.75	0.125
3CTS-17	Steel	17	1.125	1.75	0.125
3CTSS-06	Stainless Steel	6	1.125	1.75	0.125
3CTSS-07	Stainless Steel	7	1.125	1.75	0.125
3CTSS-08	Stainless Steel	8	1.125	1.75	0.125
3CTSS-09	Stainless Steel	9	1.125	1.75	0.125
3CTSS-10	Stainless Steel	10	1.125	1.75	0.125
3CTSS-11	Stainless Steel	11	1.125	1.75	0.125
3CTSS-12	Stainless Steel	12	1.125	1.75	0.125
3CTSS-13	Stainless Steel	13	1.125	1.75	0.125
3CTSS-14	Stainless Steel	14	1.125	1.75	0.125
3CTSS-15	Stainless Steel	15	1.125	1.75	0.125
3CTSS-16	Stainless Steel	16	1.125	1.75	0.125
3CTSS-17	Stainless Steel	17	1.125	1.75	0.125
