LegPartNumber	LegMaterial	LegLength (in)	LegDiameter (in)
3ROD-S-03	Steel	3	0.75
3ROD-S-04	Steel	4	0.75
3ROD-S-06	Steel	6	0.75
3ROD-S-08	Steel	8	0.75
3ROD-S-10	Steel	10	0.75
3ROD-S-12	Steel	12	0.75
3ROD-S-14	Steel	14	0.75
3ROD-S-16	Steel	16	0.75
3ROD-S-18	Steel	18	0.75
3ROD-S-20	Steel	20	0.75
3ROD-S-22	Steel	22	0.75
3ROD-S-24	Steel	24	0.75
3ROD-S-26	Steel	26	0.75
3ROD-S-28	Steel	28	0.75
3ROD-S-30	Steel	30	0.75
