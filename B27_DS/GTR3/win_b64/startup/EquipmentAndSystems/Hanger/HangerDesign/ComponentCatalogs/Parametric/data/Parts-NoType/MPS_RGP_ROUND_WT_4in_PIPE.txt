PartNumber	MPS_InsideSize (mm)	MPS_Diameter (mm)	MPS_Length (mm)	Pipe_Length (mm)	Pipe_OutsideDiameter (mm)
MPS_RGP_50mm_4in	20	50	72	101.6	60.325
MPS_RGP_70mm_4in	40	70	72	101.6	88.9
MPS_RGP_100mm_4in	60	100	74	101.6	114.3
MPS_RGP_150mm_4in	90	150	80	101.6	168.275
MPS_RGP_200mm_4in	120	200	80	101.6	219.075
MPS_RGP_300mm_4in	180	300	80	101.6	323.85

