LegPartNumber	RodLegDiameter	RodEyeDiameter (in)	RodInsideWidth (in)	RodDiameter (in)	RodTakeOut (in)	RodTopWidth (in)	RodTopHeight (in)	RodNutWidth (in)	RodNutHeight (in)	
ROD-WITH-EYENUT-00.75in	0.75in	1.5	1.1875	0.5	2	1.375	0.6875	0.875	0.5	
ROD-WITH-EYENUT-01.00in	1in	2	1.6875	0.75	2.625	1.9375	1	1.5	0.625	
ROD-WITH-EYENUT-01.50in	1.5in	2.5	1.8125	1	3.375	2.375	1.25	2	0.625	
