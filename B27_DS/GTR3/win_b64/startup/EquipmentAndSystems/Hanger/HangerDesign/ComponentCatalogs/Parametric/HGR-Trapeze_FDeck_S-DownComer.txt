DownComerPartNumber	DownComerMaterial	DownComerLength (in)	DownComerWidth (in)	DownComerThickness (in)
3ADA-04	Aluminum	4	1	0.125
3ADA-06	Aluminum	6	1	0.125
3ADA-08	Aluminum	8	1	0.125
3ADA-10	Aluminum	10	1	0.125
3ADA-12	Aluminum	12	1	0.125
3ADA-14	Aluminum	14	1	0.125
3ADA-16	Aluminum	16	1	0.125
3ADA-18	Aluminum	18	1	0.125
3ADA-20	Aluminum	20	1	0.125
3ADA-22	Aluminum	22	1	0.125
3ADA-24	Aluminum	24	1	0.125
3ADA-26	Aluminum	26	1	0.125
3ADA-28	Aluminum	28	1	0.125
3ADA-30	Aluminum	30	1	0.125
3ADA-32	Aluminum	32	1	0.125
3ADA-34	Aluminum	34	1	0.125
3ADA-36	Aluminum	36	1	0.125
3ADA-38	Aluminum	38	1	0.125
3ADA-40	Aluminum	40	1	0.125
3ADA-42	Aluminum	42	1	0.125
3ADA-44	Aluminum	44	1	0.125
3ADA-46	Aluminum	46	1	0.125
3ADA-48	Aluminum	48	1	0.125
3ADA-50	Aluminum	50	1	0.125
3ADA-52	Aluminum	52	1	0.125
3ADA-54	Aluminum	54	1	0.125
3ADA-56	Aluminum	56	1	0.125
3ADA-58	Aluminum	58	1	0.125
3ADA-60	Aluminum	60	1	0.125
3ADS-04	Steel	4	1	0.125
3ADS-06	Steel	6	1	0.125
3ADS-08	Steel	8	1	0.125
3ADS-10	Steel	10	1	0.125
3ADS-12	Steel	12	1	0.125
3ADS-14	Steel	14	1	0.125
3ADS-16	Steel	16	1	0.125
3ADS-18	Steel	18	1	0.125
3ADS-20	Steel	20	1	0.125
3ADS-22	Steel	22	1	0.125
3ADS-24	Steel	24	1	0.125
3ADS-26	Steel	26	1	0.125
3ADS-28	Steel	28	1	0.125
3ADS-30	Steel	30	1	0.125
3ADS-32	Steel	32	1	0.125
3ADS-34	Steel	34	1	0.125
3ADS-36	Steel	36	1	0.125
3ADS-38	Steel	38	1	0.125
3ADS-40	Steel	40	1	0.125
3ADS-42	Steel	42	1	0.125
3ADS-44	Steel	44	1	0.125
3ADS-46	Steel	46	1	0.125
3ADS-48	Steel	48	1	0.125
3ADS-50	Steel	50	1	0.125
3ADS-52	Steel	52	1	0.125
3ADS-54	Steel	54	1	0.125
3ADS-56	Steel	56	1	0.125
3ADS-58	Steel	58	1	0.125
3ADS-60	Steel	60	1	0.125
3ADS-62	Steel	62	1	0.125
3ADS-64	Steel	64	1	0.125
3ADS-66	Steel	66	1	0.125
3ADS-68	Steel	68	1	0.125
3ADS-70	Steel	70	1	0.125
3ADS-72	Steel	72	1	0.125
3ADS-74	Steel	74	1	0.125
3ADS-76	Steel	76	1	0.125
3ADS-78	Steel	78	1	0.125
3ADS-80	Steel	80	1	0.125
3ADS-82	Steel	82	1	0.125
3ADS-84	Steel	84	1	0.125
3ADS-86	Steel	86	1	0.125
3ADS-88	Steel	88	1	0.125
3ADS-90	Steel	90	1	0.125
3ADSS-04	Stainless Steel	4	1	0.125
3ADSS-06	Stainless Steel	6	1	0.125
3ADSS-08	Stainless Steel	8	1	0.125
3ADSS-10	Stainless Steel	10	1	0.125
3ADSS-12	Stainless Steel	12	1	0.125
3ADSS-14	Stainless Steel	14	1	0.125
3ADSS-16	Stainless Steel	16	1	0.125
3ADSS-18	Stainless Steel	18	1	0.125
3ADSS-20	Stainless Steel	20	1	0.125
3ADSS-22	Stainless Steel	22	1	0.125
3ADSS-24	Stainless Steel	24	1	0.125
3ADSS-26	Stainless Steel	26	1	0.125
3ADSS-28	Stainless Steel	28	1	0.125
3ADSS-30	Stainless Steel	30	1	0.125
3ADSS-32	Stainless Steel	32	1	0.125
3ADSS-34	Stainless Steel	34	1	0.125
3ADSS-36	Stainless Steel	36	1	0.125
