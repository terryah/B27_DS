PartNumber	NominalSize	OutsideDiameter (in)	StrapWidth (in)	StrapLength (in)	ThruHoleWidth (in)	CenterToTopHeight (in)	StrapHeight (in)	StrapThickness (in)	ThruHoleDiameter (in)	
STRAP-00.50in	1/2in	0.84	3.9375	1.125	2.6875	0.5	1.125	0.125	0.25	
STRAP-00.75in	3/4in	1.05	4.25	1.125	3	0.6875	1.4375	0.125	0.25	
STRAP-01.00in	1in	1.315	4.5	1.125	3.25	0.75	1.625	0.125	0.25	
STRAP-01.25in	1 1/4in	1.66	4.9375	1.125	3.6875	1.0625	2.125	0.125	0.25	
STRAP-01.50in	1 1/2in	1.9	5.4375	1.125	4.1875	1.25	2.4375	0.125	0.25	
STRAP-02.00in	2in	2.375	6	1.125	4.75	1.375	2.8125	0.125	0.25	
STRAP-02.50in	2 1/2in	2.875	6.5	1.5	5.25	1.75	3.5625	0.25	0.25	
STRAP-03.00in	3in	3.5	7.125	1.5	5.875	1.875	4	0.25	0.25	
STRAP-03.50in	3 1/2in	4	7.625	1.5	6.375	2.1875	4.5625	0.25	0.25	
STRAP-04.00in	4in	4.5	8.25	1.5	7	2.4372	5.0625	0.25	0.25	
