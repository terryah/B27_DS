PartNumber	NominalSize	Diameter (in)	ClampWidth (in)	ClampHeight (in)	ClampLength (in)	BoltHoleDiameter (in)
TUBE-CLAMP4-16S	1in	1	7.75	1.5	3	0.5
TUBE-CLAMP4-20S	1 1/4in	1.25	7.75	1.5	3	0.5
TUBE-CLAMP4-24S	1 1/2in	1.5	8.5	2	3	0.5
TUBE-CLAMP4-32S	2in	2	8.5	2.5	3	0.5
