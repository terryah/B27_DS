VendorsPartNumber	Material	TierWidth (in)
TBHT150A	Aluminum	1.5
TBHT250A	Aluminum	2.5
TBHT350A	Aluminum	3.5
TBHT450A	Aluminum	4.5
TBHT550A	Aluminum	5.5
TBHT650A	Aluminum	6.5
TBHT750A	Aluminum	7.5
TBHT150	Steel	1.5
TBHT250	Steel	2.5
TBHT350	Steel	3.5
TBHT450	Steel	4.5
TBHT550	Steel	5.5
TBHT650	Steel	6.5
TBHT750	Steel	7.5
TBHT150S	Stainless Steel	1.5
TBHT250S	Stainless Steel	2.5
TBHT350S	Stainless Steel	3.5
TBHT450S	Stainless Steel	4.5
TBHT550S	Stainless Steel	5.5
TBHT650S	Stainless Steel	6.5
TBHT750S	Stainless Steel	7.5
