V4 Table Type,V4 Table Name,V5 Folder Name
Nominal OD,ASTL-Nominal_OD,ASTL
Schedule,ASTL-Schedule,ASTL
Wall Thickness,ASTL-Wall_Thickness,ASTL
End Style,ASTL-End_Style,ASTL
Rating,ASTL-Rating,ASTL
Material Category,ASTL-Material_Category,ASTL
Part Name - Pipe,ASTL-Part_Name_-_Pipe,ASTL
Part Name - Branches,ASTL-Part_Name_-_Branches,ASTL
Part Name - Elbows,ASTL-Part_Name_-_Elbows,ASTL
Part Name - Fittings,ASTL-Part_Name_-_Fittings    ,ASTL
Part Name - Flanges,ASTL-Part_Name_-_Flanges ,ASTL
Part Name - Gasket,ASTL-Part_Name_-_Gasket   ,ASTL
Part Name - Miscellaneous,ASTL-Part_Name_-_Miscellaneous    ,ASTL
Part Name - Accessory,ASTL-Part_Name_-_Accessory  ,ASTL
Part Name - In-line Instrument,ASTL-Part_Name_-_In-line_Instrument    ,ASTL
Part Name - Valve Operator,ASTL-Part_Name_-_Valve_Operator  ,ASTL
Part Name - Valve,ASTL-Part_Name_-_Valve,ASTL
Material Code - Pipe,ASTL-Material_Code_-_Pipe    ,ASTL
Material Code - Branches,ASTL-Material_Code_-_Branches ,ASTL
Material Code - Elbows,ASTL-Material_Code_-_Elbows,ASTL
Material Code - Fittings,ASTL-Material_Code_-_Fittings ,ASTL
Material Code - Flanges,ASTL-Material_Code_-_Flanges   ,ASTL
Material Code - Gasket,ASTL-Material_Code_-_Gasket,ASTL
Material Code - Miscellaneous,ASTL-Material_Code_-_Miscellaneous ,ASTL
Material Code - Accessory,ASTL-Material_Code_-_Accessory    ,ASTL
Material Code - In-line Instrument,ASTL-Material_Code_-_In-line_Instrument ,ASTL
Material Code - Valve Operator,ASTL-Material_Code_-_Valve_Operator    ,ASTL
Material Code - Valve,ASTL-Material_Code_-_Valve  ,ASTL
Pipe Dimension,ASTL-Pipe_Dimension ,ASTL
3D Part Function - Branches,FUNCTION_TABLE_BRANCHES_CS150R  ,CS150R
3D Part Function - Branches,FUNCTION_TABLE_BRANCHES_CS300R  ,CS300R
3D Part Function - Branches,FUNCTION_TABLE_BRANCHES_CS600R  ,CS600R
3D Part Function - Branches,FUNCTION_TABLE_BRANCHES_SS150R  ,SS150R
3D Part Function - Elbows,FUNCTION_TABLE_ELBOWS_CS150R ,CS150R
3D Part Function - Elbows,FUNCTION_TABLE_ELBOWS_CS300R ,CS300R
3D Part Function - Elbows,FUNCTION_TABLE_ELBOWS_CS600R ,CS600R
3D Part Function - Elbows,FUNCTION_TABLE_ELBOWS_SS150R ,SS150R
3D Part Function - Fittings,FUNCTION_TABLE_FITTINGS_CS150R  ,CS150R
3D Part Function - Fittings,FUNCTION_TABLE_FITTINGS_CS300R  ,CS300R
3D Part Function - Fittings,FUNCTION_TABLE_FITTINGS_CS600R  ,CS600R
3D Part Function - Fittings,FUNCTION_TABLE_FITTINGS_SS150R  ,SS150R
3D Part Function - Flanges,FUNCTION_TABLE_FLANGES_CS150R    ,CS150R
3D Part Function - Flanges,FUNCTION_TABLE_FLANGES_CS300R    ,CS300R
3D Part Function - Flanges,FUNCTION_TABLE_FLANGES_CS600R    ,CS600R
3D Part Function - Flanges,FUNCTION_TABLE_FLANGES_SS150R    ,SS150R
3D Part Function - Gasket,FUNCTION_TABLE_GASKETS_CS150R,CS150R
3D Part Function - Gasket,FUNCTION_TABLE_GASKETS_CS300R,CS300R
3D Part Function - Gasket,FUNCTION_TABLE_GASKETS_CS600R,CS600R
3D Part Function - Gasket,FUNCTION_TABLE_GASKETS_SS150R,SS150R
3D Part Function - Miscellaneous,FUNCTION_TABLE_MISCELLANEOUS_CS150R  ,CS150R
3D Part Function - Miscellaneous,FUNCTION_TABLE_MISCELLANEOUS_CS300R  ,CS300R
3D Part Function - Miscellaneous,FUNCTION_TABLE_MISCELLANEOUS_CS600R  ,CS600R
3D Part Function - Miscellaneous,FUNCTION_TABLE_MISCELLANEOUS_SS150R  ,SS150R
3D Part Function - In-line Instrument,FUNCTION_TABLE_INLINE_INSTRUMENT,CS150R
3D Part Function - In-line Instrument,FUNCTION_TABLE_INLINE_INSTRUMENT,CS300R
3D Part Function - In-line Instrument,FUNCTION_TABLE_INLINE_INSTRUMENT,CS600R
3D Part Function - In-line Instrument,FUNCTION_TABLE_INLINE_INSTRUMENT,SS150R
3D Part Function - Valve,FUNCTION_TABLE_VALVES_CS150R  ,CS150R
3D Part Function - Valve,FUNCTION_TABLE_VALVES_CS300R  ,CS300R
3D Part Function - Valve,FUNCTION_TABLE_VALVES_CS600R  ,CS600R
3D Part Function - Valve,FUNCTION_TABLE_VALVES_SS150R  ,SS150R
Branch,BRH_STEEL_45 ,Default
Branch,BRH_STEEL_90 ,Default
Weld,WELD TABLE A   ,Default
Shop-Fabrication,SHOP_FABRICATION_TABLE_A    ,Default
Bolt,BOLT_TABLE_A   
Bolt,BOLT_TABLE_B   ,Default
Bolt,BOLT_TABLE_C   
Design Condition,ASTL-Design_Condition  ,ASTL
Transparent Auto Parts,Transparent_Auto_Parts,DesignRules
Compatibility Rules,Compatibility_Rules ,DesignRules
Process function mapping rules,Process_function_mapping_rules    ,DesignRules
Process func mapping by size,PROCESS_FUNCTION_CS300R   ,CS300R
Process func mapping by size,PROCESS_FUNCTION_CS150R   ,CS150R
Process func mapping by size,PROCESS_FUNCTION_CS600R   ,CS600R
Process func mapping by size,PROCESS_FUNCTION_SS150R   ,SS150R
Process func mapping by size,PROCESS_FUNCTION_CSTEST
Nominal OD,AIRN-Nominal_OD    ,AIRN
Schedule,AIRN-Schedule   ,AIRN
Wall Thickness,AIRN-Wall_Thickness ,AIRN
End Style,AIRN-End_Style ,AIRN
Rating,AIRN-Rating  ,AIRN
Material Category,AIRN-Material_Category,AIRN
Part Name - Pipe,AIRN-Part_Name_-_Pipe  ,AIRN
Part Name - Branches,AIRN-Part_Name_-_Branches    ,AIRN
Part Name - Elbows,AIRN-Part_Name_-_Elbows   ,AIRN
Part Name - Fittings,AIRN-Part_Name_-_Fittings    ,AIRN
Part Name - Flanges,AIRN-Part_Name_-_Flanges ,AIRN
Part Name - Gasket,AIRN-Part_Name_-_Gasket   ,AIRN
Part Name - Miscellaneous,AIRN-Part_Name_-_Miscellaneous    ,AIRN
Part Name - Accessory,AIRN-Part_Name_-_Accessory  ,AIRN
Part Name - In-line Instrument,AIRN-Part_Name_-_In-line_Instrument    ,AIRN
Part Name - Valve Operator,AIRN-Part_Name_-_Valve_Operator  ,AIRN
Part Name - Valve,AIRN-Part_Name_-_Valve,AIRN
Material Code - Pipe,AIRN-Material_Code_-_Pipe    ,AIRN
Material Code - Branches,AIRN-Material_Code_-_Branches ,AIRN
Material Code - Elbows,AIRN-Material_Code_-_Elbows,AIRN
Material Code - Fittings,AIRN-Material_Code_-_Fittings ,AIRN
Material Code - Flanges,AIRN-Material_Code_-_Flanges   ,AIRN
Material Code - Gasket,AIRN-Material_Code_-_Gasket,AIRN
Material Code - Miscellaneous,AIRN-Material_Code_-_Miscellaneous ,AIRN
Material Code - Accessory,AIRN-Material_Code_-_Accessory    ,AIRN
Material Code - In-line Instrument,AIRN-Material_Code_-_In-line_Instrument ,AIRN
Material Code - Valve Operator,AIRN-Material_Code_-_Valve_Operator    ,AIRN
Material Code - Valve,AIRN-Material_Code_-_Valve  ,AIRN
Pipe Dimension,AIRN-Pipe_Dimension ,AIRN
Design Condition,AIRN-Design_Condition  ,AIRN
