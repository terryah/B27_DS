Shape	MinSize(in)	MaxSize(in)	TurnPartType
Round	1in	80in	HVACRndSmoothElbow
Rectangular	1in	100in	HVACRectNReducElbow
FlatOval	1in	100in	HVACFltOvlNReducElbow
