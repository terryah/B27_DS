FunctionName	MinSize	MaxSize	PhysicalPart
HVACBranchFunc	1in	80in	HVACRndTap
HVACBranchFunc	1in	80in	HVACRndTee
HVACBranchFunc	1in	80in	HVACRndReduceTee
HVACBranchFunc	1in	80in	HVACRndLtrlTee
HVACBranchFunc	1in	80in	HVACRndLtrlReduceTee
HVACBranchFunc	1in	80in	HVACRndCross
HVACBranchFunc	1in	80in	HVACRndWye
