FunctionName	PhysicalPart
PipingPipeFunc	PipingBendablePipe
PipingPipeFunc	PipingNonReduceElbow
PipingPipeFunc	PipingReduceElbow
PipingPipeFunc	PipingNonReduceFlange
PipingPipeFunc	PipingReducingFlange
PipingPipeFunc	PipingBlindFlange
PipingPipeFunc	PipingOrificeFlange
PipingPipeFunc	PipingGasket
PipingPipeFunc	PipingUnion
PipingPipeFunc	PipingCoupling
PipingPipeFunc	PipingHalfCoupling
PipingPipeFunc	PipingNipple
PipingPipeFunc	PipingStubEnd
PipingPipeFunc	PipingExpansion
PipingPipeFunc	PipingSleeve
PipingPipeFunc	MldInLineWeld
PipingMiscFunc	PipingBlind
PipingMiscFunc	PipingSpacer
PipingMiscFunc	PipingSpectacle
PipingMiscFunc	PipingStrainer
PipingAdaptorFunc_Sample	PipingAdaptor
PipingContrlValveFunc_Sample	PipingControlValve
PipingContrlValveFunc_Sample	PipingGateValve
PipingContrlValveFunc_Sample	PipingGlobeValve
PipingBranchFunc	PipingTee
PipingBranchFunc	PipingReducingTee
PipingBranchFunc	PipingBoss
PipingBranchFunc	PipingCross
PipingBranchFunc	PipingElbowOlet
PipingBranchFunc	PipingNippleOlet
PipingBranchFunc	PipingOlet
PipingBranchFunc	PipingLateral
PipingBranchFunc	PipingHalfCoupling
PipingBranchFunc	PipingReducingCross
PipingBranchFunc	PipingReducingLateral
PipingBranchFunc	PipingWye
PipingBranchFunc	PipingLateralOlet
PipingBlockValveFunc_Sample	PipingPlugValve
PipingBlockValveFunc_Sample	PipingGateValve
PipingBlockValveFunc_Sample	PipingBallValve
PipingBlockValveFunc_Sample	PipingButterflyValve
PipingCheckValveFunc_Sample	PipingAngleCheck
PipingCheckValveFunc_Sample	PipingCheckValve
EquipChemReactorFunc_Sample	No Mapping
EquipCompressorFunc_Sample	EquipAxialCompressor
EquipCompressorFunc_Sample	EquipReciprCompressor
EquipCompressorFunc_Sample	EquipBlower
EquipCompressorFunc_Sample	EquipFan
EquipCompressorFunc_Sample	EquipCentriCompressor
EquipCompressorFunc_Sample	EquipRadialCompressor
EquipCompressorFunc_Sample	EquipRotaryCompressor
EquipDriverFunc_Sample	EquipGasTurbine
EquipDriverFunc_Sample	EquipElectricalMotor
EquipDriverFunc_Sample	EquipGasDieselEngine
EquipDriverFunc_Sample	EquipSteamCylinder
EquipDriverFunc_Sample	EquipSteamTurbine
EquipDryerFunc_Sample	Equipment
PipingExpanderFunc_Sample	PipingExpansion
EquipFurnaceFunc_Sample	EquipFurnace
EquipHeatExchFunc_Sample	No Mapping
EquipHeatExchFunc_Sample	EquipHeatExchanger
PipingInlineInstrFunc	PipingInLineInstrt
PipingInlineInstrFunc	PipingOrifice
PipingIsolatValveFunc_Sample	PipingButterflyValve
PipingIsolatValveFunc_Sample	PipingPlugValve
PipingIsolatValveFunc_Sample	PipingGateValve
PipingIsolatValveFunc_Sample	PipingBallValve
EquipMixerFunc_Sample	EquipMixer
EquipMaterialHndlFunc_Sample	Equipment
EquipPipingNozzleFunc	EquipManway
EquipPipingNozzleFunc	EquipUtility
EquipPipingNozzleFunc	EquipProcess
EquipPipingNozzleFunc	No Mapping
PipingReliefValveFunc_Sample	PipingSafetyValve
EquipVesselFunc_Sample	EquipVessel
EquipPumpFunc_Sample	EquipPump
EquipPumpFunc_Sample	EquipRotaryPump
EquipPumpFunc_Sample	EquipReciprPump
EquipPumpFunc_Sample	EquipCentriPump
PipingSafetyValveFunc_Sample	PipingSafetyValve
EquipSeparatorFunc_Sample	Equipment
PipingReducerFunc	PipingEcentricReducer
PipingReducerFunc	PipingCncntricReducer
PipingReducerFunc	PipingReduceCoupling
PipingReducerFunc	PipingReducingBushing
PipingReducerFunc	PipingReducingInsert
EquipStorageFunc_Sample	EquipVessel
PipingStopCkValveFunc_Sample	PipingStopCheckValve
PipingStopCkValveFunc_Sample	PipingAngleStopCheck
PipingSwitchValveFunc_Sample	Piping3WayValve
PipingSwitchValveFunc_Sample	Piping4WayValve
PipingTerminatorFunc_Sample	PipingPlug
PipingTerminatorFunc_Sample	PipingCap
PipingTerminatorFunc_Sample	PipingBlindFlange
PipingThrotValveFunc_Sample	PipingGlobeValve
PipingThrotValveFunc_Sample	PipingAngleDiaphragm
PipingThrotValveFunc_Sample	PipingNeedleValve
PipingThrotValveFunc_Sample	PipingDiaphragmValve
PipingThrotValveFunc_Sample	PipingAngleGlobe
PipingThrotValveFunc_Sample	PipingButterflyValve
PipingThrotValveFunc_Sample	PipingAngleNeedle
EquipTowerFunc_Sample	No Mapping
EquipMiscFunc_Sample	Equipment
PipingValveOperFunc_Sample	PipingPistonActuator
PipingValveOperFunc_Sample	PipingManualActuator
PipingValveOperFunc_Sample	PipingMotorActuator
PipingValveOperFunc_Sample	PipingDiaphrmActuator
