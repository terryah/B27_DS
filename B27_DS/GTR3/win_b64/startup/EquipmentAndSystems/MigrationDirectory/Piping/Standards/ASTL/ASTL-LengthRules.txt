MaterialCategory	MinNominalSize	MaxNominalSize	MinStraightLength(in)	MaxLength(in)	MaxBendAngle(deg)	StandardLength(in)
Carbon steel	1/8in	4in	0.0000	240.0000	145.0000	120.000000
Carbon steel	5in	48in	0.0000	240.0000	120.0000	120.000000
Stainless steel	1/8in	4in	0.0000	240.0000	145.0000	120.000000
Stainless steel	5in	48in	0.0000	240.0000	120.0000	120.000000
