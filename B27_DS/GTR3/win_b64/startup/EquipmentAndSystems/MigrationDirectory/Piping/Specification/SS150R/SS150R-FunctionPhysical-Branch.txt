FunctionName	MinNominalSize	MaxNominalSize	PhysicalName
PipingBranchFunc	1 1/2in	1 1/2in	PipingElbowOlet
PipingBranchFunc	1/2in	30in	PipingLateral
PipingBranchFunc	1/2in	14in	PipingOlet
PipingBranchFunc	1/2in	30in	PipingReducingLateral
PipingBranchFunc	1/2in	30in	PipingReducingTee
PipingBranchFunc	1/2in	30in	PipingTee
