'***************************************************************************
' Purpose:  To call GetAttributes API using script.
'
'	        This script is only a sample script. This needs customization 
'			by the user. This script cannot be used as it is. The required 
'			changes have been commented appropriately.
'
' Script Version: 1
'
' Language  :    VBScript
' V5 Version:    V5R17 or above
' Reg. Settings: English (United States)
' Creation     : RPX
'***************************************************************************
' History
' Tri		Date(mm/dd/yyyy)	Reason
' RPX		06/16/2006			ODT		
'***************************************************************************

' Language="VBSCRIPT"

'***************************************************************************
' Main method. 
'***************************************************************************

Sub CATMain()
On Error Resume Next   ' Defer error handling.

Application.DisplayAlerts = False
Set app = CreateObject("DNBSOR.Application")

Set objS = app.GetItem("SORObject")

'***************************************************************************
'	SetE5LoginDetails() - This function sets the E5 login details
'***************************************************************************
'	@param Login : Login
'	@param Password : Password
'***************************************************************************

Dim Login As String
Dim Password As String

'---------------------------------------------------------------------------
'	Set login Details
'---------------------------------------------------------------------------
Login = "admin"
Password = "admin"
'---------------------------------------------------------------------------

str1 = objS.SetE5LoginDetails(Login, Password)
If Err Then
	ErrorHandler Err
End If

'***************************************************************************
' GetAttributes() - This method gets the list of attribute(s) value against 
'					the list of attribute(s) name
'***************************************************************************
' @param E5ObjectID
'   The E5 Object ID(OID) whose attribute(s) is(are) to be valuated
' @param AttrName
'    The attribute(s) name list
' @param AttrVal
'    The attribute(s) value list
'***************************************************************************

Dim E5ObjectID As String
ReDim AttrName(2) As Variant
ReDim AttrVal(2) As Variant

'---------------------------------------------------------------------------
'	Set GetAttributes() parameters
'---------------------------------------------------------------------------

E5ObjectID = "$id$(0:0-193058#0, 231)"
AttrName(0) = "name"
AttrName(1) = "nameshort"
AttrName(2) = "_goalinvestmentcost"

'---------------------------------------------------------------------------

str2 = objS.GetAttributes(E5ObjectID, AttrName, AttrVal)
If Err Then 
	ErrorHandler Err
Else
	MsgBox "Attribute Name:	" & AttrName(0) & vbCrLf & "Attribute Value:	" & CStr(AttrVal(0)) & vbCrLf & vbCrLf & "Attribute Name:	" & AttrName(1) & vbCrLf & "Attribute Value:	" & CStr(AttrVal(1)) & vbCrLf & vbCrLf & "Attribute Name:	" & AttrName(2) & vbCrLf & "Attribute Value:	" & CStr(AttrVal(2))
End If

str3 = objS.EndE5Transaction()
str4 = app.Quit()
End Sub

Sub ErrorHandler (iErrObj As ErrObject)
	If iErrObj.Number <> 0 Then
		MsgBox iErrObj.Description & vbCrLf & "Error Number: " & CStr(iErrObj.Number) & vbCrLf & "Error Source: " & iErrObj.Source & vbCrLf & "Error Log: " & iErrObj.HelpFile & vbCrLf & "Error Help Context: " & iErrObj.HelpContext
	End If
End Sub
