
' ******************************************************************************************************************************
' Purpose:  To report the Shop Accountability. Given the CS and a Linenumber 
'                 (Linenumber is a single digit integer), this script will travers through the tree
'                 and list out all the IP that correspond to the given Linenumber and the 
'                 IP status.
'
' Script Version			: 1
'
' Language					: VBScript
' V5 Version				: V5R17 or above
' Reg. Settings				: English (United States)
' Creation					: RIH
' Date						: 12/28/2005 (mm/dd/yyyy)
' ***************************************************************************************************************************
' History
' Tri		Date(mm/dd/yyyy)	Reason
' ***************************************************************************************************************************



' ***************************************************************************************************************************
' HOW TO USE THIS SCRIPT?
' 
' This script can either be executed manually (on a Control Station node) 
' or can be executed through batch mode (by passing the Control Station Object ID
' and the script ID).
'
' Before this script is eecuted (by either way), the user has to make sure the following:
' 1. The "Linenumber.xml" is updated with the Linenumber for which the Report is to be generated.
' 2. Provide the location where the above script recides to the input parameter "LNumberXmlFileLocation"
'
'
' HOW IS THE REPORT GENERATED?
'
' The Accountability Report will be generated in the form of an XML named: "CS_LN_ShopPrintAccountability.xml"
' where: CS -- Control Station Name
'        LN -- Requested Linenumber
' This output XML will be generated where the Linenumber.xml recides.
' This output XML will comply to ShopPrintAccountability.xsd
'
'
' NOTE: The following samples will be available with the installation at "...\intel_a\VBScript\DPM_Work\"
' 1. Linenumber.xml
' 2. ShopPrintAccountability.xml
' ***************************************************************************************************************************



'********************************************************************************************************************************
'* Read from the Linenumber XML from the given path
'********************************************************************************************************************************
	Dim LNumberXmlFileLocation, TempXmlFilesPath

	LNumberXmlFileLocation = "D:\Temp"

	TempXmlFilesPath = LNumberXmlFileLocation & "\Linenumber.xml"

	set xmlDoc=CreateObject("Microsoft.XMLDOM")
	xmlDoc.async="false"
	xmlDoc.load(TempXmlFilesPath)

	Dim RequestLinenumber, TempXmlElement

	for each NODE in xmlDoc.documentElement.childNodes
		TempXmlElement = NODE.text
		'MsgBox "RequestLinenumber: " & TempXmlElement
	next 

	RequestLinenumber = Cint(TempXmlElement)
	'MsgBox "RequestLinenumber: " & RequestLinenumber
'********************************************************************************************************************************

'**********************************************************************************
'	Open the Text File
'**********************************************************************************
Function OpenFile(OutFile)
	Dim indexf, indexfs

	Set indexfs = CreateObject("Scripting.FileSystemObject")
	Set indexf = Indexfs.OpenTextFile(OutFile, 8, True)
	Set OpenFile = indexf 

End Function

'**********************************************************************************
'*
'* CreateNodeElement subroutine
'* Arguments:
'* 	OutFile,
'* 	RootNode, 
'* 	Value
'**********************************************************************************

Sub CreateNodeElement (OutFile, RootNode,  Value)
	Dim indexf
	Set indexf = OpenFile(OutFile)
	indexf.WriteLine "<" & RootNode & ">" & Value & "</" & RootNode & ">"  
	indexf.Close
End Sub


'***************************************************************
' main:    Main method
' param @ id: Object id the node on which this script 
' is to be executed
'***************************************************************
sub main(id)

	Dim CSname, OutFile 

	CSname = Data.GetAttributebyId(id, "nameshort")
	OutFile = LNumberXmlFileLocation & "\" & CSname & "_" & RequestLinenumber & "_" & "ShopPrintAccountability.xml"

	'******* Create the XMLFile	*************** 
	'******************************************     
	Set indexfs = CreateObject("Scripting.FileSystemObject")
	Set indexf = Indexfs.CreateTextFile(OutFile , True)    

	indexf.WriteLine "<?xml version=""" & "1.0" & """" & "?>"
	indexf.WriteLine "<ShopPrintAccountability xsi:noNamespaceSchemaLocation=" & """" &"ShopPrintAccountability.xsd" & """" & ">"
	indexf.Close

	CreateNodeElement OutFile, "ControlStationName", CSname
	CreateNodeElement OutFile, "LineNumber", RequestLinenumber

	childlistname = "nodes"
	call Data.SetOrderAttribute("name", "ASC")
	count = Data.GetChildrenCount(id, childlistname)

	child_id = Data.GetFirstChild(id,childlistname)

	Do while child_id <> ""
		name = Data.GetAttributebyId(child_id, "name")
		'MsgBox "Name: " & name

		child_base_id = Data.GetAttributebyId(child_id, "ergocompbase")
		if(child_base_id <> "")then
			Dim IPName, IPNumber, IPStatus, CompareState

			IPName = Data.GetAttributebyId(child_id, "name")
			IPNumber = Data.GetAttributebyId(child_id, "nameshort")
			IPStatus = "Not Released"
			CompareState = 0

			LineNumber = Data.GetAttributebyId(child_base_id, "tailnumber")
			'MsgBox "Line number: " & LineNumber
			if(LineNumber <> "") then		'*** Linenumber is set on IP
				Set objIPLinenumberDynArray = New DynamicArray
				GetTailnumberList Linenumber, objIPLinenumberDynArray

				CompareTailnumbers RequestLinenumber, objIPLinenumberDynArray, CompareState                               
				'MsgBox "CompareState : " & CompareState 

				if(CompareState = 1) then		'**** This IP is valid
					Dim release_table_id
					GetReleaseTableId child_id, release_table_id
					if (release_table_id <> "") then
						GetSOIState release_table_id, IPName, IPStatus
						'MsgBox "SOIState: " & IPStatus
					else
						'MsgBox "release table not found"
					end if
					Set indexf = OpenFile(OutFile)
					indexf.WriteLine "<InstallationPlan>"
					indexf.Close

					CreateNodeElement OutFile, "IPTitle", IPNumber 
					CreateNodeElement OutFile, "IPNumber", IPName
					CreateNodeElement OutFile, "IPSatus", IPStatus

					Set indexf = OpenFile(OutFile)
					indexf.WriteLine "</InstallationPlan>"
					indexf.Close
				else
					'MsgBox "IP is invalid. SOI state in Not Released"
				end if                                
			else		'*** Linenumber is not set on the IP. But this is considered as a valid IP.
				Dim RT_table_id
				GetReleaseTableId child_id, RT_table_id
				if (RT_table_id <> "") then
					GetSOIState RT_table_id, IPName, IPStatus
					MsgBox "SOIState: " & IPStatus
				else
					IPStatus = "Not Released"
					'MsgBox "release table not found"
				end if      

				Set indexf = OpenFile(OutFile)
				indexf.WriteLine "<InstallationPlan>"
				indexf.Close

				CreateNodeElement OutFile, "IPTitle", IPNumber
				CreateNodeElement OutFile, "IPNumber", IPName 
				CreateNodeElement OutFile, "IPSatus", IPStatus

				Set indexf = OpenFile(OutFile)
				indexf.WriteLine "</InstallationPlan>"
				indexf.Close
			end if
		end if 
		child_id = Data.GetNextChild(id, childlistname)
	Loop

	Set indexf = OpenFile(OutFile)
	indexf.WriteLine "</ShopPrintAccountability>"
	indexf.Close

end sub

'***************************************************************
' GetTailnumberList
' param @ Linenumber
'                 Requested Linenumber
' param @ objDynArray
'                 An array of the complete tail numbers
'***************************************************************
Public function GetTailnumberList(Linenumber, objDynArray)
	Dim StrLen, TailNumber, ComPos, HypenPos, BracePos
	BracePos = 0
	StrLen = Len(Linenumber)
	'MsgBox StrLen

	BracePos = InStr(1, Linenumber, "{")
	'MsgBox "BracePos: " & BracePos

	if(BracePos = 0) then
		'*************Do not strip braces from the Linenumber*****
		TailNumber = Linenumber
	else
		'*************Strip braces from the Linenumber*************
		TailNumber = Mid(Linenumber,2,StrLen-2)
		'MsgBox TailNumber
		'*************Strip braces from the Linenumber*************
	end if

	'*************Strip spaces from the Tailnumber*************
	Dim StrippedTailnumber
	StrippedTailnumber = Replace (TailNumber, " ", "")
	'MsgBox StrippedTailnumber  & " after stripping spaces"
	'*************Strip spaces from the Tailnumber*************

	'*************Tokenize the Tailnumber*************
	Dim sepcom(1)
	Dim SepHypen(1)
	Dim EffRange
	Dim IndEff
	Dim objArrIndex
	Dim StartIndex
	Dim StopIndex

	objArrIndex = 0
	StartIndex = 1

	sepcom(0) = ","
	sephypen(0) = "-"

	EffRange = Tokenize(StrippedTailnumber,sepcom)

	for i=1 to UBound(EffRange)		
		IndEff = Tokenize(EffRange(i-1),sephypen)
		StartIndex = Cint(IndEff(0))		
		StopIndex = IndEff(UBound(IndEff)-1) - StartIndex + 1
		
		for j = 1 to StopIndex                          
			'msgbox IndEff(j-1)
			if(j=1) then
				objDynArray.Data(objArrIndex) = StartIndex
				objArrIndex = objArrIndex + 1
			else 
				objDynArray.Data(objArrIndex) = StartIndex + j-1
				'objDynArray(objArrIndex) =  IndEff(j-1)
				objArrIndex = objArrIndex + 1
			end if
		next
	next
	'*************Tokenize the Tailnumber*************

	'GetTailnumberList = objDynArray
end function


'***********************************************************************
' CompareTailnumbers
' param @ requestlinenumber
'                 Requested Linenumber
' param @ releasetablelinenumber
'                 tail numbers obtained form RT
' return  @ comparestate
'                 result of the comparision
'                 comparestate = 1 if the comparision is succesful
'                 comparestate = 0 if the comparision fails      
'***********************************************************************
Public function CompareTailnumbers(requestlinenumber, releasetablelinenumber, comparestate)

	'MsgBox "Inside CompareTailnumbers"
	Dim RTLbound, RTUbound

	'For j = releasetablelinenumber.StartIndex() to releasetablelinenumber.StopIndex()  
	'MsgBox j & ".) " & releasetablelinenumber.Data(j) & ""
	'Next

	RTLbound =  releasetablelinenumber.StartIndex()	
	RTUbound = releasetablelinenumber.StopIndex() 

	'MsgBox "RTLbound: " & RTLbound
	'MsgBox "RTUbound: " & RTUbound

	Dim reqTailnumber 
	Dim RTTailnumber 
	reqTailnumber = requestlinenumber
	RTTailnumber = 0

	for j = 0 to RTUbound
		RTTailnumber = releasetablelinenumber.Data(j)

		'MsgBox "reqTailnumber: " & reqTailnumber  & " RTTailnumber: " & RTTailnumber 
		'MsgBox "comparing RTTailnumber & requestlinenumber gives: " & (reqTailnumber = releasetablelinenumber.Data(j))
		if (RTTailnumber = reqTailnumber) Then		
			comparestate = 1
			'MsgBox  "linenumbers match"
			Exit Function
		else 
			comparestate = 0
			'MsgBox  "linenumbers do not match"
		end if
	next
end function


'***************************************************************
' get the id of the releasetable
' param @ old_sci_id
'                 Object id of the base node
' param @ release_table_id
'                 id of the obtained Release Table.
'                 If the Release Table is not found, the id will 
'                 be null.
'***************************************************************
private Function GetReleaseTableId(old_sci_id, release_table_id)
	dim old_base_id
	dim old_parent_id  
	old_base_id = Data.GetAttributebyId(old_sci_id, "ergocompbase")
	if old_base_id <> "" then
		old_parent_id = Data.GetAttributebyId(old_sci_id, "ergocompbase_parent")
		if old_parent_id <> "" then
			release_table_id = Version.GetReleaseTable(old_base_id )
		end if
	end if
end Function

'***************************************************************
' GetSOIState
' param @ release_table_id
'                 release table id
' param @ VName
' param @ SOIState
'                 the return value of SOI state
'***************************************************************
private Function GetSOIState(release_table_id, VName, SOIState)

	dim vSA	'SafeArray  
	vSA = Data.GetAttributebyId(release_table_id, "releasetableentries")
	If (Err.Number <> 0) Then
	'MsgBox("Could not get releasetableentries.  " + Err.Description)
	Err.Clear
	Exit Function
	end if

	if(IsArray(vSA) <> true ) then
		'MsgBox("No releasetableentries found.")
		Exit Function
	else
	end if

	dim llowerBY  
	dim llowerBX 
	dim lUperBY 
	dim lUperBX 

	llowerBY  = lbound(vSA, 1)
	llowerBX = lbound(vSA, 2) 
	lUperBY = ubound(vSA, 1)
	lUperBX = ubound(vSA, 2)

	dim SACol
	SACol = lUperBX -llowerBX+1
	if( 3 <> SACol ) then
		'MsgBox("array has wrong no of columns." & SACol)
		Exit Function
	end if
	dim entries
	entries = lUperBY - llowerBY+1
	if (entries < 1) then
		'MsgBox("no entries in reltab")
		Exit Function
	else
		'MsgBox(entries & "  Entries in ReleaseTable")
	end if    


	dim i
	Dim EE
	dim SOI
	dim PDXML

	For i = 0 to lUperBY
		EE = vSA(i, 0)
		SOI = vSA(i, 1)
		PDXML = vSA(i,2)
		'MsgBox "EE: " & EE
		'MsgBox "SOI: " & SOI

		Set objEEDynArray = New DynamicArray
		GetTailnumberList EE, objEEDynArray

		'For j = objEEDynArray.StartIndex() to objEEDynArray.StopIndex()  
		'MsgBox j & ".) " & objEEDynArray.Data(j) & ""
		'Next

		Dim CompState
		CompState = 0

		CompareTailnumbers RequestLinenumber, objEEDynArray, CompState
		'MsgBox "CompState: " & CompState
		if(CompState = 1) then
			SOIState = SOI
			Exit Function
		end if
	Next

	if(CompState = 0) then
		SOIState = "Not Released"
	end if
end Function


'***************************************************************
' ERROR: Error method
'***************************************************************
private sub ERROR
	MsgBox("An Error occured")
end sub

'***************************************************************
' DynamicArray Class
'***************************************************************
Class DynamicArray
	'************** Properties **************
	Private aData
	'****************************************

	'*********** Event Handlers *************
	Private Sub Class_Initialize()
		Redim aData(0)
	End Sub
	'****************************************

	'************ Property Get **************
	Public Property Get Data(iPos)
		'Make sure the end developer is not requesting an
		'"out of bounds" array element
		If iPos < LBound(aData) or iPos > UBound(aData) then
			Exit Property    'Invalid range
		End If

		Data = aData(iPos)
	End Property

	Public Property Get DataArray()
		DataArray = aData
	End Property
	'****************************************

	'************ Property Let **************
	Public Property Let Data(iPos, varValue)
		'Make sure iPos >= LBound(aData)
		If iPos < LBound(aData) Then Exit Property

		If iPos > UBound(aData) then
			'We need to resize the array
			Redim Preserve aData(iPos)
			aData(iPos) = varValue
		Else
			'We don't need to resize the array
			aData(iPos) = varValue
		End If
	End Property
	'****************************************


	'************** Methods *****************
	Public Function StartIndex()
		StartIndex = LBound(aData)
	End Function

	Public Function StopIndex()
		StopIndex = UBound(aData)
	End Function

	Public Sub Delete(iPos)
		'Make sure iPos is within acceptable ranges
		If iPos < LBound(aData) or iPos > UBound(aData) then
			Exit Sub    'Invalid range
		End If

		Dim iLoop
		For iLoop = iPos to UBound(aData) - 1
			aData(iLoop) = aData(iLoop + 1)
		Next

		Redim Preserve aData(UBound(aData) - 1)
	End Sub
	'****************************************
End Class


'***************************************************************
' RecursivelyDelete the given element
' param @ objDynArray
'                 Array object from which the given 
'                 string is to be stripped
' param @ StrToStrip
'                 The string to be stripped
'***************************************************************
private sub RecursivelyDelete(objDynArray, StrToStrip)

	For i = objDynArray.StartIndex() to objDynArray.StopIndex()  
		if(objDynArray.Data(i) = StrToStrip)then
			objDynArray.Delete i
			'MsgBox "deleted the " & i & "th element"
			RecursivelyDelete objDynArray, StrToStrip
		end if
		'MsgBox objDynArray.StopIndex()
	Next
end sub

'***************************************************************
' Tokenize
' param @ TokenString
'                 The string that is to be tokenized
' param @ TokenSeparators()
'                 An array of the delimiters used to Tokenize
'***************************************************************
Private Function Tokenize(byVal TokenString, byRef TokenSeparators())

	Dim NumWords, a()
	NumWords = 0

	Dim NumSeps
	NumSeps = UBound(TokenSeparators)

	Do 
		Dim SepIndex, SepPosition
		SepPosition = 0
		SepIndex    = -1

		for i = 0 to NumSeps-1
			' Find location of separator in the string
			Dim pos
			pos = InStr(TokenString, TokenSeparators(i))

			' Is the separator present, and is it closest to the beginning of the string?
			If pos > 0 and ( (SepPosition = 0) or (pos < SepPosition) ) Then
			SepPosition = pos
			SepIndex    = i
			End If
		Next

		' Did we find any separators?	
		if SepIndex < 0 Then

			' None found - so the token is the remaining string
			redim preserve a(NumWords+1)
			a(NumWords) = TokenString

		else

			' Found a token - pull out the substring		
			Dim substr
			substr = Trim(Left(TokenString, SepPosition-1))

			' Add the token to the list
			redim preserve a(NumWords+1)
			a(NumWords) = substr

			' Cutoff the token we just found
			Dim TrimPosition
			TrimPosition = SepPosition+Len(TokenSeparators(SepIndex))
			TokenString = Trim(Mid(TokenString, TrimPosition))

		End If	

		NumWords = NumWords + 1
	loop while (SepIndex >= 0)

	Tokenize = a

End Function

