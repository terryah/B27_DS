'***************************************************************************
' Purpose:  To call UpdateReleaseTable API using script.
'
'	        This script is only a sample script. This needs customization 
'			by the user. This script cannot be used as it is. The required 
'			changes have been commented appropriately.
'
' Script Version: 1
'
' Language  :    VBScript
' V5 Version:    V5R17 or above
' Reg. Settings: English (United States)
' Creation     : RPX
'***************************************************************************
' History
' Tri		Date(mm/dd/yyyy)	Reason
' RPX		06/16/2006			ODT		
'***************************************************************************

' Language="VBSCRIPT"

'***************************************************************************
' Main method. 
'***************************************************************************

Sub CATMain()
On Error Resume Next   ' Defer error handling.
Application.DisplayAlerts = False
Set app = CreateObject("DNBSOR.Application")

Set objS = app.GetItem("SORObject")

'***************************************************************************
'	SetE5LoginDetails() - This function sets the E5 login details
'***************************************************************************
'	@param Login : Login
'	@param Password : Password
'***************************************************************************

Dim Login As String
Dim Password As String

'---------------------------------------------------------------------------
'	Set login Details
'---------------------------------------------------------------------------
Login = "admin"
Password = "admin"
'---------------------------------------------------------------------------

str1 = objS.SetE5LoginDetails(Login, Password)
If Err Then
	ErrorHandler Err
End If

'***************************************************************************
'	UpdateReleaseTable() - Update Entry In the Release Table
'***************************************************************************
'	@param E5ProjOID
'		The E5 Project OID on which the release Table is Updated
'		(must be the latest Version)
'	@param E5JobOID
'		The E5 Job OID on which the release Table is Updated
'		(must be the latest Version)
'	@param ExtendedEffectivity
'		Value of Extended Effectivity
'		NOTE : The values Should be like "{1-5}", "{100-105}"
'	@param SOIState
'		Value of Planning State Type (SOI State)
'		NOTE : The values Should be one of :
'				{"Undefined", "Working", "Integrate", "Released"}.
'	@param PDXMLFlag
'		Value whether it has been created(TRUE) or not(FALSE)
'***************************************************************************

Dim E5ProjOID As String
Dim E5JobOID As String
Dim ExtendedEffectivity As String
Dim SOIState As String
Dim PDXMLFlag As boolean

'---------------------------------------------------------------------------
'	Set UpdateReleaseTable() parameters
'---------------------------------------------------------------------------

E5ProjOID = "$id$(0:0-188722#0, 168)"
E5JobOID = "$id$(0:0-188735#0, 231)"
ExtendedEffectivity = "(100-105)"
SOIState = "Working"
PDXMLFlag = 1

'---------------------------------------------------------------------------

str2 = objS.UpdateReleaseTable(E5ProjOID, E5JobOID, ExtendedEffectivity, SOIState, PDXMLFlag)
If Err Then
	ErrorHandler Err
Else
	str3 = objS.CommitE5Transaction()
	MsgBox "UpdateReleaseTable() - Done"
End If

str4 = objS.EndE5Transaction()
str5 = app.Quit()
End Sub

Sub ErrorHandler (iErrObj As ErrObject)
	If iErrObj.Number <> 0 Then
		MsgBox iErrObj.Description & vbCrLf & "Error Number: " & CStr(iErrObj.Number) & vbCrLf & "Error Source: " & iErrObj.Source & vbCrLf & "Error Log: " & iErrObj.HelpFile & vbCrLf & "Error Help Context: " & iErrObj.HelpContext
	End If
End Sub
