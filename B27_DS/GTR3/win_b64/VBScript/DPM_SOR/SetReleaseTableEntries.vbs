' *******************************************************************************
' Purpose:  To Set Entries to an already existing Release Table
'	        upon the node on which the script is called in E5.
'
'			This script is only a sample script. This needs customization by 
'			the user. This script cannot be used as it is. The required changes
'			have been commented appropriately.
'
' Script Version: 1
'
' Language     : VBScript
' V5 Version   : V5R16 or above
' Reg. Settings: English (United States)
' Creation     : RIH
' Date		   : 11/21/2005 (mm/dd/yyyy)
' ****************************************************************************
' History
' Tri		Date(mm/dd/yyyy)	Reason
' ****************************************************************************

Dim col
Dim row
Dim n
Dim row_loop
Dim excel

'*********************************************************************
' Main method.
' This script when called on a particular node, checks if there exists
' any Release Table already created on that node. If it finds any 
' Release Table,it sets all the Entries specified by the user and prints
' them to an Excel Sheet.
' 
' @param old_sci_id: the OID of the node on which the script is called.
'*********************************************************************

public sub main(old_sci_id)

	'**************************************************************
	'* Read from the Temperary XML from the given path	
	'**************************************************************
	Dim TempXmlFilesPath

	'*******************************************
	'Provide the appropriate path here	
	'*******************************************
	TempXmlFilesPath = "D:\temp\Effectivity.xml"	
			
	set xmlDoc=CreateObject("Microsoft.XMLDOM")
	xmlDoc.async="false"
	xmlDoc.load(TempXmlFilesPath)	

    '***********************************************************	
    '* Create an array to store the values from temp xml
	'* TempXMLElements(0) - ExtEff
	'* TempXMLElements(1) - soiState
	'* TempXMLElements(2) - pdmXML
	'***********************************************************	
	ReDim TempXMLElements(2)   	

	Dim NodeCount
	NodeCount = 1
                
	for each NODE in xmlDoc.documentElement.childNodes
		TempXMLElements(NodeCount-1) = NODE.text		
		NodeCount = NodeCount + 1		
	next          

	'********************************************		

 	dim WshShell 
 	Set WshShell = CreateObject("WScript.Shell")
 	dim release_table_id
 	GetReleaseTableId old_sci_id, release_table_id	
 
      if (release_table_id <> "") then
          if (TempXMLElements(2)  = "TRUE")  then
                      SetReleaseTableEntry release_table_id, "{" & TempXMLElements(0) & "}", TempXMLElements(1), TRUE
           else
                      SetReleaseTableEntry release_table_id, "{" & TempXMLElements(0) & "}", TempXMLElements(1), FALSE
           end if

	Dim versName
           versName = Data.GetAttributebyId(old_sci_id, "name")
           GetReleaseTableEntries release_table_id, versName

      else
            MsgBox("No ReleaseTable found.")
      end if   
end sub

'***************************************************************************
' To get the Release Table ID
' @param  old_sci_id: The OID of the node on which Release Table is created.
' @param  release_table_id: Output param. The value of the Release Table ID.
'***************************************************************************
private Function GetReleaseTableId(old_sci_id, release_table_id)
 dim old_base_id
 dim old_parent_id  
  old_base_id = Data.GetAttributebyId(old_sci_id, "ergocompbase")
  if old_base_id <> "" then
    old_parent_id = Data.GetAttributebyId(old_sci_id, "ergocompbase_parent")
    if old_parent_id <> "" then
      release_table_id = Version.GetReleaseTable(old_base_id )
    end if
  end if
end Function

'***************************************************************************
' To Set Entries to the Release Table
' @param  release_table_id: The value of the Release Table ID.
' @param  extEff: Value of the Extended Effectivity
' @param  soiState: The SOI State
' @param  bPDXML: Boolean. PDXML Flag. (TRUE/FALSE) should be in upper case
'***************************************************************************
private sub SetReleaseTableEntry(release_table_id, extEff, soiState, bPDXML)
      dim vSA(0,2)	'SafeArray
      
      vSA(0,0) = extEff 
      vSA(0,1) = soiState
      vSA(0,2) = bPDXML
            
      call Data.SetAttributebyId(release_table_id, "releasetableentries", vSA) 
end sub

'***************************************************************************
' To print the results to the Excel sheet.
'***************************************************************************
private sub PrintToExcel(extEff, SOIstate, bpdxml, excel)

  Dim strBool
  If (1 = bpdxml) Then
  	strBool = "True"
  Else
  	strBool = "False"
  End if
  
	With excel
		.Cells(row,col-1)= row-2
		.Cells(row,col)= extEff
		.Cells(row,col+1)= SOIstate
		.Cells(row,col+2)= strBool
	End With
	row = row + 1
end sub

'*********************************************************************
' Instantiate the Excel Sheet
'*********************************************************************   
Private Sub InitiateExcelSheet(VersName,NoOfRTE)
  'create Headers
  Set excel = CreateObject("Excel.Application")
  excel.Workbooks.Add
  excel.Visible = True
  With excel
    .Cells(1, 1).Value = "ReleaseTableEntries of " & VersName
    .Cells(2, 1).Value = "No"
    .Cells(2, 2).Value = "Extended Effectivity"
    .Cells(2, 3).Value = "SOI State"
	.Cells(2, 4).Value = "pdxml file created"
	.Cells(1, 4).Value = Date
 End With
	
 excel.Range("A1:D2").Font.Bold = True
 excel.Range("A1:D2").Font.Size = 12
 
 row = 3
 col = 2
 Dim num
 num = NoOfRTE + row
 
 excel.Range("A1:A" & num).Font.Bold = True
	
End Sub

'*********************************************************************
' To get the Release Table Entries for the given id.
' @param release_table_id: The Release Table ID for which the entries 
'                           are to be fetched.
' @param VName: Name of the node on which the script is called.
'*********************************************************************
private sub GetReleaseTableEntries(release_table_id, VName)
 
  dim vSA	'SafeArray  
  vSA = Data.GetAttributebyId(release_table_id, "releasetableentries")
  If (Err.Number <> 0) Then
        MsgBox("Could not get releasetableentries.  " + Err.Description)
        Err.Clear
        Exit Sub
 end if
    
  if(IsArray(vSA) <> true ) then
        MsgBox("No releasetableentries found.")
        Exit Sub
  else
  end if

  dim llowerBY  
  dim llowerBX 
  dim lUperBY 
  dim lUperBX 
  
  llowerBY  = lbound(vSA, 1)
  llowerBX = lbound(vSA, 2) 
  lUperBY = ubound(vSA, 1)
  lUperBX = ubound(vSA, 2)
  
 dim SACol
  SACol = lUperBX -llowerBX+1
  if( 3 <> SACol ) then
    MsgBox("array has wrong no of columns." & SACol)
    Exit Sub
  end if
 dim entries
 entries = lUperBY - llowerBY+1
 if (entries < 1) then
    MsgBox("no entries in reltab")
    Exit Sub
 else
   MsgBox(entries & "  Entries in ReleaseTable")
 end if    

 InitiateExcelSheet VName, entries    
  dim i
  Dim EE
  dim SOI
  dim PDXML
  
 For i = 0 to lUperBY
	EE = vSA(i, 0)
	SOI = vSA(i, 1)
 	PDXML = vSA(i,2)
 	PrintToExcel EE,SOI,PDXML,excel
  Next   
 excel.Columns("A:D").Select
 excel.Columns("A:D").EntireColumn.AutoFit
 excel.Range("A1").Select
 
  excel.Visible = True
  Set excel = Nothing

end sub

'****************************************************************
' Print ERROR
'****************************************************************
private sub ERROR
 	MsgBox("An Error occured")
end sub

