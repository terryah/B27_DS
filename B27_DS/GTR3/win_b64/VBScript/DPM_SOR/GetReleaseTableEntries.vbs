' *******************************************************************************
' Purpose:  To get the Release Table Entries of an already existing Release Table
'	        upon the node on which the script is called in E5.
'
' Script Version: 1
'
' Language  :    VBScript
' V5 Version:    V5R16 or above
' Reg. Settings: English (United States)
' Creation     : RIH
' Date		   : 11/21/2005 (mm/dd/yyyy)
' ****************************************************************************
' History
' Tri		Date(mm/dd/yyyy)	Reason
' ****************************************************************************



Language="VBSCRIPT"

Dim col
Dim row
Dim n
Dim row_loop
Dim excel

'*********************************************************************
' Main method.
' This script when called on a particular node, checks if there exists
' any Release Table already created on that node. If it finds any 
' Release Table,it gets all the Entries present in that Table and 
' prints them to an Excel Sheet.
' 
' @param old_sci_id: the OID of the node on which the script is called.
'*********************************************************************
public sub main(old_sci_id)

 dim WshShell 
 Set WshShell = CreateObject("WScript.Shell")
 dim release_table_id
 GetReleaseTableId old_sci_id, release_table_id
 
      if (release_table_id <> "") then
           Dim versName
           versName = Data.GetAttributebyId(old_sci_id, "name")
           GetReleaseTableEntries release_table_id, versName
      else
            MsgBox("No ReleaseTable found.")
      end if    
end sub

'*********************************************************************
' To print the obtained results to the Excel sheet.
'*********************************************************************
private sub PrintToExcel(extEff, SOIstate, bpdxml, excel)

  Dim strBool
  If (1 = bpdxml) Then
  	strBool = "True"
  Else
  	strBool = "False"
  End if
  
	With excel
		.Cells(row,col-1)= row-2
		.Cells(row,col)= extEff
		.Cells(row,col+1)= SOIstate
		.Cells(row,col+2)= strBool
	End With
	row = row + 1
end sub
 
'*********************************************************************
' Instantiate the Excel Sheet
'********************************************************************* 
Private Sub InitiateExcelSheet(VersName,NoOfRTE)
  'create Headers
  Set excel = CreateObject("Excel.Application")
  excel.Workbooks.Add
  excel.Visible = True
  With excel
    .Cells(1, 1).Value = "ReleaseTableEntries of " & VersName
    .Cells(2, 1).Value = "No"
    .Cells(2, 2).Value = "Extended Effectivity"
    .Cells(2, 3).Value = "SOI State"
	.Cells(2, 4).Value = "pdxml file created"
	.Cells(1, 4).Value = Date
 End With
	
 excel.Range("A1:D2").Font.Bold = True
 excel.Range("A1:D2").Font.Size = 12
 
 row = 3
 col = 2
 Dim num
 num = NoOfRTE + row
 
 excel.Range("A1:A" & num).Font.Bold = True
	
End Sub

'*********************************************************************
' To get the Release Table Entries for the given id.
' @param release_table_id: The Release Table ID for which the entries 
'                           are to be fetched.
' @param VName: Name of the node on which the script is called.
'*********************************************************************
private sub GetReleaseTableEntries(release_table_id, VName)
 
  dim vSA	'SafeArray  
  vSA = Data.GetAttributebyId(release_table_id, "releasetableentries")
  If (Err.Number <> 0) Then
        MsgBox("Could not get releasetableentries.  " + Err.Description)
        Err.Clear
        Exit Sub
 end if
    
  if(IsArray(vSA) <> true ) then
        MsgBox("No releasetableentries found.")
        Exit Sub
  else
  end if

  dim llowerBY  
  dim llowerBX 
  dim lUperBY 
  dim lUperBX 
  
  llowerBY  = lbound(vSA, 1)
  llowerBX = lbound(vSA, 2) 
  lUperBY = ubound(vSA, 1)
  lUperBX = ubound(vSA, 2)
  
 dim SACol
  SACol = lUperBX -llowerBX+1
  if( 3 <> SACol ) then
    MsgBox("array has wrong no of columns." & SACol)
    Exit Sub
  end if
 dim entries
 entries = lUperBY - llowerBY+1
 if (entries < 1) then
    MsgBox("no entries in reltab")
    Exit Sub
 else
   MsgBox(entries & "  Entries in ReleaseTable")
 end if    

 InitiateExcelSheet VName, entries    
  dim i
  Dim EE
  dim SOI
  dim PDXML
  
 For i = 0 to lUperBY
	EE = vSA(i, 0)
	SOI = vSA(i, 1)
 	PDXML = vSA(i,2)
 	PrintToExcel EE,SOI,PDXML,excel
  Next   
 excel.Columns("A:D").Select
 excel.Columns("A:D").EntireColumn.AutoFit
 excel.Range("A1").Select
 
  excel.Visible = True
  Set excel = Nothing

end sub

'****************************************************************
' Print ERROR
'****************************************************************
private sub ERROR
 	MsgBox("An Error occured")
end sub

'***************************************************************************
' get the id of the releasetable 
' @param  old_sci_id: The OID of the node on which Release Table is created.
' @param  release_table_id: Output param. The value of the Release Table ID.
'***************************************************************************
private Function GetReleaseTableId(old_sci_id, release_table_id)
 dim old_base_id
 dim old_parent_id  
  old_base_id = Data.GetAttributebyId(old_sci_id, "ergocompbase")
  if old_base_id <> "" then
    old_parent_id = Data.GetAttributebyId(old_sci_id, "ergocompbase_parent")
    if old_parent_id <> "" then
      release_table_id = Version.GetReleaseTable(old_base_id )
    end if
  end if
end Function
