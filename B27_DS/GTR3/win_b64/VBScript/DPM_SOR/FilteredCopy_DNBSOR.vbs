' *******************************************************************************
' Purpose:  To call Filtered COPY API using VB script.
'
'           This script is only a sample script. This needs customization by
'           the user. This script cannot be used as it is. The required changes
'           have been commented appropriately.
'
' Script Version: 1
'
' Language  :    VB Script
' V5 Version:    V5R17SP1 or above
' Reg. Settings: English (United States)
' Creation     : MZP
' ****************************************************************************
' History
' Tri       Date(mm/dd/yyyy)    Reason
' ****************************************************************************


Sub main()
On Error Resume Next
Application.DisplayAlerts = False
Set app = CreateObject("DNBSOR.Application")

Set objS = app.GetItem("SORObject")
'*************************************************
' @param Login: Login to E5
' @param PassWord : PassWord to E5
' Customer is required to modify these value
'*************************************************
str1 = objS.SetE5LoginDetails("admin", "admin")

'*************************************************
' Specifies the filters to be applied while loading the project.
'
' In case of P,P,R filters, either
'     (a) A Filter string can be supplied OR
'     (b) A Calculation Model ID can be supplied
' NOTE: Calculation Model filters MUST only be supplied through their IDs. 
'
' In case of extended filters, either
'     (a) A Filter string can be supplied OR
'     (b) A Calculation Model ID can be supplied OR
'     (c) Both string AND Model ID can be supplied (Combined effectivity case)
' NOTE: Calculation Model filters MUST only be supplied through their IDs. 
'
' @param bsProjId [in] : Project ID of the PPR Based Project
' @param bsEffectivity [in] : Filter String containing the filters
'   The filters to be set. This is a list of values separated by XML delimiters
'   The following delimiters are supported 
'     <ProcessFilter>Filter Value</ProcessFilter>
'     <ProcessFilterCalcModelID>ID of Calculation Model Filter</ProcessFilterCalcModelID>  
'     <ProductFilter>Filter Value</ProductFilter>
'     <ProductFilterCalcModelID>ID of Calculation Model Filter</ProductFilterCalcModelID>
'     <ResourceFilter>Filter Value</ResourceFilter>
'     <ResourceFilterCalcModelID>ID of Calculation Model Filter</ResourceFilterCalcModelID>
'     <StartDateEffectivityFilter>Filter Value</StartDateEffectivityFilter>
'     <EndDateEffectivityFilter>Filter Value</EndDateEffectivityFilter>
'     <LineNumberFilter>Filter Value</LineNumberFilter>
'     <LabelFilter>Filter Value</LabelFilter>
'     <ActionProxyFilter>Filter Value</ActionProxyFilter>
'     <ModStatementFilterID>Filter Value</ModStatementFilterID>
'     <PlanningStateOwnerFilterID>ID of Planning State</PlanningStateOwnerFilterID>
'     <PlanningStateOtherFilterID>ID of Planning State</PlanningStateOtherFilterID>
'     <ApplyImplicitFilter>TRUE or FALSE</ApplyImplicitFilter>
'
'     <ProcessExtendedEffectivityFilter>Filter Value</ProcessExtendedEffectivityFilter>
'     <ProcessExtendedEffectivityFilterCalcModelID>ID of Calculation Model Filter</ProcessExtendedEffectivityFilterCalcModelID>
'     <ProductExtendedEffectivityFilter>Filter Value</ProductExtendedEffectivityFilter>
'     <ProductExtendedEffectivityFilterCalcModelID>ID of Calculation Model Filter</ProductExtendedEffectivityFilterCalcModelID>
'     <ResourceExtendedEffectivityFilter>Filter Value</ResourceExtendedEffectivityFilter> 
'     <ResourceExtendedEffectivityFilterCalcModelID>ID of Calculation Model Filter</ResourceExtendedEffectivityFilterCalcModelID>
'
' @example
'   <ProcessFilter>Process Type A</ProcessFilter><LabelFilter>Label B</LabelFilter><LineNumberFilter>10</LineNumberFilter>
'   The order is NOT important. e.g., "<LabelFilter>...</LabelFilter>" can come before "<ProcessFilter>...</ProcessFilter>"
'
' @UseCase: SetFilters("$id$(0:0-196827#0, 168)","<ProcessFilter>TypeA</ProcessFilter><LabelFilter>LabelB</LabelFilter><LineNumber>67</LineNumber>")
'
' @return
'    S_OK on success
'    E_FAIL if error
'
' Customer is required to modify these value
'*************************************************

str2 = objS.SetFilters("$id$(0:0-196827#0, 168)", "<LineNumberFilter>1</LineNumberFilter>")

'*************************************************
' @param Project ID : Project ID of the PPR Based Project
' @param Parent ID : Target Parent Object ID
' @param Source ID : Source Object ID
' @param Copy Option : Copy Option
'           SOR_NORMAL = 1
'           SOR_COPY_FLAT = 2
'           SOR_COPY_DEEP = 4
'           SOR_COPY_INTER_PROJECT = 8
'
' @return Target Object ID: Target Object ID
' Customer is required to modify these value
'*************************************************
str2 = objS.FilteredCopy("$id$(0:0-196827#0, 168)", "$id$(0:0-196837#0, 245)", "$id$(0:0-196840#0, 231)", 2)
Msgbox Err.Description
str5 = objS.CommitE5Transaction()
str6 = objS.EndE5Transaction()
MsgBox str2
str3 = app.Quit()
End Sub


