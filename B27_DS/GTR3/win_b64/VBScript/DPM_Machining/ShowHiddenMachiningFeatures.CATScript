Option Explicit
' Language="VBSCRIPT"
' ****************************************************************************
' Purpose:       To Show hidden machinable features in the Manufacturing View
'				 This script will add a "HIDDEN" suffix to names of Machinable
'				 Area features, if they do not have one already.
'				 
' Assumptions:   
'
' Script Version: 1
'
' Languages:     VBScript
' V5 Version:    V5R10 GA
' Reg. Settings: English (United States)
' ****************************************************************************
'*******************************************************************************
'*
'* Global variables
'*
'*******************************************************************************

' these are the general manufacturing variables retrieve by InitMfg()
Dim MfgDoc As Document '           the current document
Dim MfgNbOfPO As Long '            the number of Part Operation
Dim Mfg1stPO As ManufacturingSetup ' the first Part Operation in the process

Dim MfgFeatures As ManufacturingFeatures 'Manufacturing Features

'*******************************************************************************************
'*
'* Main Entry  Point
'*  
'* It calls the InitMfg Subprogram which initialyze the general variables of the macro.
'* It scans the products of the PPR tree, and for each product : calls the ProcessProdcutInstance Subprogram
'*******************************************************************************************
Sub CATMain()

	' initialize all the manufacturing variables
	InitMfg
	If IsEmpty(MfgDoc) Then
		MsgBox "No current document !", vbCritical + vbOKOnly
		Exit Sub
	End If
	If MfgNbOfPO = 0 Then
		MsgBox "No Part Operation in the document :  " & MfgDoc.Name & " !", vbCritical + vbOKOnly
		Exit Sub
	End If
	If IsEmpty(MfgFeatures) Then
		MsgBox "Manufacturing Features not retrieved !", vbCritical + vbOKOnly
		Exit Sub
	End If
	If Err Then
		MsgBox "MfgInit Failed" & vbLf & "Error # " & Err.Number & " " & Err.Description
		Exit Sub
	End If

	' End ofinitialization
	'-----------------------------------------------------------------------------


'**********************************************************************************
'*
'* Body of the macro
'*  
'* It scans Machinable Area Features.
'* If they are hidden, it makes them visible, and add the "HIDDEN " suffix to their name.
'**********************************************************************************

	Dim CurMfgFeat As ManufacturingFeature

	Dim I As Long
	For I=1 To MfgFeatures.Count
		Set CurMfgFeat = MfgFeatures.Item(I)
		If (TypeName(CurMfgFeat) = "ManufacturingMachinableArea") Then

			Dim MyMachArea As ManufacturingMachinableArea
            Set MyMachArea = CurMfgFeat
            
			Dim bVisibleInMfgView As boolean
            bVisibleInMfgView = MyMachArea.VisibleInMfgView

			If (bVisibleInMfgView = FALSE) Then
				
				bVisibleInMfgView = TRUE
				MyMachArea.VisibleInMfgView = bVisibleInMfgView

				'Suffix
				dim Suffix As CATBSTR
				Suffix = "HIDDEN "
			
				'Name of the feature
				dim Name As CATBSTR 
				Name = CurMfgFeat.Name	

				'Does the feature have the suffix already. If no, add it.
				
				dim MyPos As long
				MyPos =  InStr (1, Name, Suffix, 1)
	
				If Not (MyPos = 1) Then
					dim NewName As CATBSTR
					NewName = Suffix + Name
					CurMfgFeat.Name = NewName
				End If
			End If
		End If
	Next		

	' OK, bye-bye
	MsgBox "Macro ended OK !", vbInformation + vbOKOnly

End Sub	
' End of main program

'*****************************************************************************
'*
'* InitMfg
'*
'*****************************************************************************
Sub InitMfg()

	MfgDoc = Empty
	MfgNbOfPO = 0
	MfgFeatures = Empty

	' Get current Document
	On Error Resume Next
	Set MfgDoc = CATIA.ActiveDocument
	If Err Then
		Exit Sub
	End If
	
	'**********************************
	' Retrieve current Process Root
	'**********************************

	' Retrieve the current PPR Document
	Dim PPRDoc As PPRDocument
	PPRDoc = Empty	
	Set PPRDoc = MfgDoc.PPRDocument
	If Err Then
		Exit Sub
	End If
	If IsEmpty(PPRDoc) Then
		Exit Sub
	End If

	' Obtain the list of "Process" nodes in the PPR Document
	Dim RootProcesses as Activities
	set RootProcesses = PPRDoc.Processes

	' Retrieve the first "Process" node from the list
	Dim ActivityRef As AnyObject
	Set ActivityRef = RootProcesses.Item(1)

	' Search for a Part Operation anywhere under the "Process" node
	FindPartOperation( ActivityRef)
	
	' get the manufacturing view
	Dim MfgView As ManufacturingView
	Set MfgView = Mfg1stPO.GetManufacturingView
	If IsEmpty(MfgView) Then
		MsgBox "Manufacturing View not retrieved !", vbCritical + vbOKOnly
		Exit Sub
	End If

	' get the list of manufacturing features
	Set MfgFeatures = MfgView.ManufacturingFeatures

End Sub


'**********************************************************************************
'*
'* FindPartOperation subprogram
'*		Finds the Part Operation at any level in the process 
'* Arguments are RootActivity (usually this is the "Process" node) 
'* 
'**********************************************************************************
		
Sub FindPartOperation (ByRef RootActivity As AnyObject)

	' Retrieve first Part Operation	
	If (RootActivity.IsSubTypeOf("PhysicalActivity")) Then

		Dim childActivities As Activities
		Set childActivities = RootActivity.ChildrenActivities
		if childActivities.Count <= 0 then
			'MsgBox "No children for activiity" & RootActivity.Name
			Exit Sub
		End if
		
		'MsgBox "Number of children: " & childActivities.Count

		Dim I As Long
		Dim child As Activity
		
		' Look for a Part Operation in the current children list
		For I=1 To childActivities.Count
			Set child = childActivities.Item(I)
			'MsgBox "Current child: " & child.Name
			If (child.IsSubTypeOf("ManufacturingSetup")) Then
				Set Mfg1stPO = child
				MfgNbOfPO = MfgNbOfPO + 1
				Exit For
			End If
		Next

		' If a Part Operation was found, then exit
		If MfgNbOfPO > 0 Then
			'MsgBox "Found Part OP. Exiting."
			Exit Sub
		End If
		
		' If no Part Operation was found, then check the children of the next level
		For I=1 To childActivities.Count
			' Check if the the child activity is a Part Operation
			Set child = childActivities.Item(I)		

			if ( child.IsSubTypeOf("PhysicalActivity")) Then
				FindPartOperation(child)			
			End If
		Next
	End If

End Sub

