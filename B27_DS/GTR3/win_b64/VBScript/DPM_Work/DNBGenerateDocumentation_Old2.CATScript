' ****************************************************************************
' Purpose:  Import the XML file and generate the necessary documention for each operation
' QQW: Kept as Backup:
' This is old file which was working for Excel 2003. 
' This file had problems for Excel 2007 refer IR A0639781 . Hence modified file was created and promoted.
' This file is kept as backup as the performance with this file is better the new file. 
' The changed file DNBGenerateDocumentation.CATScript was promoted for IR A0639781 but has some performance issues.
' ****************************************************************************

Language = "VBSCRIPT"

'====================================================================================
' Audit Trail:
'--------------------------------------------------------------------
'   Author     : HOL
'   Date       : Jan 2007
'
'   Modifier   : HOL
'   Mods       : If there are more activities, the details are placed in two columns
'				 This leads to better usage of avaialable space. 
'   Date       : March 2007
'
'   Modifier   : HOL
'   Mods       : To address IR A0578462
'				 Upto 99 Operation sheets can be generated and each Operation Sheet
'				 can have 99 Activities.
'   Date       : April 2007
'
'   Modifier   : HOL
'   Mods       : To Add Cover Sheet Image
'   Date       : July 2007
'====================================================================================
' The current version of the script takes in four arguments.
'
' In:  XLSTemplateLoc - string, Excel Template Location
'      An example is - D:\tmp\Template.xls
'
' In:  XMLFileLoc - string, Temporarily generated XML file, XML location
'      An example is - D:\tmp\Template.xml
'
' In:  GenXLSFileLoc - string, Indicates where to save the generated XLS file,
'      An example is - D:\tmp\GenerateDocForProcess1.xls
'
' In:  DisplayXLS - boolean, Indicates wheather the Excel file has to be opened at the end.
'      if True,  Open the Excel file for user to edit
'	   if False, Display the location of the Stored Excel file
'
' Result: Import the XML file and generate the necessary documention for each operation
'====================================================================================

Dim WrkBook 'Excel WorkBook
Dim xlsSheet 'Excel Sheet
Dim xlsRefSheet 'Excel Sheet refering to "Reference" sheet

Dim xlsTempSheet 'Will be mainly used for copying
Dim HasManyOperation As Boolean 'HasManyOperation = True, if there exist more then ONE Operation

Dim xmlDoc ' DOM Object
Dim XSDMapName As String 'Stores the name of the Map and usually it will be RootNodeName_Map

Dim oWriteXMLNode 'for creating sub XML files

Dim StartRow As Integer 'Starting Point for Unstructured Elements
Dim StartX, StartY As Double 'Starting Position for placing the Shape Objects
Dim StartFlag As Boolean ' ShapeHeight = True if StartX and StartY have been found
Dim PitchText, PitchImage As Double 'Distance between two Shape Objects
Dim NextX, NextY As Double 'NextX = StartX + PitchText or NextX = StartX + PitchImage
Dim ShapeWidth, ShapeHeight As Double 'Width  & Height of the Shape Objects
Dim TxtWidth, TxtHeight As Double 'Width & Height of the Text
Dim UsableShtHeight As Double 'useful to calculate the height of the image
' PitchX between two shape objects - Vertical - Number of Columns
' PitchY between two shape objects - Horizontal - Number of Rows, in Points
Dim PitchX, PitchY As Integer
Dim MinImageHeight, MaxImageHeight As Double
Dim UsableShtHeightFromRow1 As Double '=StartY (in Points) + UsableShtHeight

Dim TopLftRow, TopLftCol, UsdRow, UsdCol

Dim XSDFileLoc As String ' Location of XSD obtained from Excel Template file
Dim XMLFile As String    ' Location of temporary XML file

Dim PreviousAct As String 'Will help us to determing the distance between two shape objects
Dim OpName As String 'Stores the Name of the Operation

Dim TempFileLoc As String 'Stores the temporary files location for XSD and XML

Dim ImageDir As String

Dim ErrWarnMsgs, ErrInfo, WarnInfo, MsgInfo As String
Dim OpInfoAdd As String
Dim MappingExistMsg As Boolean

'For Operation + Activity Mapping
Dim xlsOpActSheet
Dim CurrentOpCnt, CurrentActCnt As Integer
Dim OpActRow As Integer 'Stores the current Row for OP_ACT_Mapping

'For Cover Sheet Image
Dim CoverImageWidth, CoverImageHeight As Double 'Width & Height of the Cover Sheet Image in terms of Pixcels
Dim CoverImageX, CoverImageY As Integer 'X and Y position of the Cover Sheet Image in terms of number of Columns and Rows respectively.
Dim HasCoverSheetImagePlaced As Boolean
Dim CoverSheetName As String

'--------------------------------------------------------------------------------------------------
'--------------------------------------------------------------------------------------------------
Function GenerateDocumentation(XLSTemplateLoc, XMLFileLoc, GenXLSFileLoc, DisplayXLS)
    
    MsgBox "Inside DNBGenerateDocumentation.CATScript"
    'MsgBox "Excel Template file is ... " & XLSTemplateLoc
    'MsgBox "XML file is ... " & XMLFileLoc
    '--------------------------------------------------------------
    'BASED UPON THE NEEDS LIKE IMAGE SIZE OR LOCATION WHERE TO START
    'THE PLACING THE IMAGES/TEXT, THE USER CAN MODIFY
    'THE FOLLOWING VARIABLES
    '--------------------------------------------------------------
    StartY = 10 'INDICATES THE ROW FROM WHERE TO START
    StartX = 3  'INDICATES THE COLUMN FROM WHERE TO START
    TxtHeight = 45
    TxtWidth = 250
    UsableShtHeight = 420 'in terms of Points or 35 rows with height 12 Points
    PitchX = 9 'Columns
    PitchY = 25 'or 2 Row Points
    MinImageHeight = 75 'Points
    MaxImageHeight = 360 'Points
    '--------------------------------------------------------------
    
    '--------------------------------------------------------------
    'For Cover Sheet Image
    CoverImageWidth = 160
    CoverImageHeight = CoverImageWidth * 3 / 4
    CoverImageX = 20
    CoverImageY = 33
    HasCoverSheetImagePlaced = False
    CoverSheetName = "COVER_SHEET_IMAGE"
    '--------------------------------------------------------------
    
    'Initializing the Messages
    ErrWarnMsgs = ""
    ErrInfo = "ERRORS:"
    WarnInfo = "WARNINGS:"
    MsgInfo = "MESSAGES:"
    OpInfoAdd = "" '"The following Operations are Added to the Excel file" & vbNewLine
    
    MappingExistMsg = False
    ImageDir = ""
    
    UsableShtHeightFromRow1 = 0
    
    Dim oFile
    Dim FileExist As Boolean
    Set oFile = CreateObject("Scripting.FileSystemObject")
    
    Dim XLSDir As String
    'Check if the Excel Template File exist
    FileExist = oFile.FileExists(XLSTemplateLoc)
    If FileExist = False Then
        'Call MsgBox("Selected Excel Template does not EXIST!!!", vbCritical, "Generate - ERROR")
        ErrInfo = ErrInfo & "Selected Excel Template does not EXIST!!!" & vbNewLine
        Set oFile = Nothing
        DeleteFiles (XMLFileLoc)
        Exit Function
    End If
    'Check if the FOLDER exist for Saving the Generated Doc
    XLSDir = GetXLSFileDir(GenXLSFileLoc)
    If oFile.FolderExists(XLSDir) = False Then
        ErrInfo = ErrInfo & "Folder " & XLSDir & " does not EXIST." & vbNewLine & _
                            "Generated Excel File CANNOT be saved!!!"
        Set oFile = Nothing
        DeleteFiles (XMLFileLoc)
        ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
        GenerateDocumentation = ErrWarnMsgs
    End If
    '--------------------------------------------------------------
    'Setting some INITIAL Values
    HasManyOperation = False
    XSDFileLoc = ""
    StartFlag = False
    'Maybe, the below 2 values can be calculated based upon number of activities
    NextX = 0
    NextY = 0
    PreviousAct = ""
    'Need to Obtain the Directory where to save the file
    TempFileLoc = GetXLSFileDir(GenXLSFileLoc)
    
    'Create an Excel Object
    Set ExcelObj = CreateObject("Excel.Application")
    ExcelObj.DisplayAlerts = False
    ExcelObj.Visible = False
    Set WrkBook = ExcelObj.Workbooks.Open(XLSTemplateLoc)
    
    'Start with DOM
    Set xmlDoc = CreateObject("Microsoft.XMLDOM")
    xmlDoc.async = "false"
    xmlDoc.Load (XMLFileLoc)
    Dim XMLMappings As XmlMaps
    Set XMLMappings = WrkBook.XmlMaps
    'If there are more then one schema files, them print ERROR message
    If XMLMappings.Count > 1 Then
        ErrInfo = ErrInfo & "There exist more then ONE Schema Maps in the selected Excel Template!!!" & vbNewLine & _
                            "Please make sure that there is only ONE Schema Map in the Excel Template File!!!"
        'Closing the Excel stuffs
        ExcelObj.ActiveWorkbook.Close
        ExcelObj.Application.Quit
        Set ExcelObj = Nothing
        'Deleting the files
        DeleteFiles (XMLFileLoc)
        ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
        GenerateDocumentation = ErrWarnMsgs
        Exit Function
    ElseIf XMLMappings.Count = 0 Then
        WarnInfo = WarnInfo & "There exist NO Schema Map in the selected Excel Template!!! " & vbNewLine & _
                              "Header Information won't be filled." & vbNewLine & vbNewLine
    End If
    'Get the "Reference" Sheet
    On Error Resume Next
    Set xlsRefSheet = WrkBook.Worksheets("Reference")
    If IsEmpty(xlsRefSheet) Or IsNull(xlsRefSheet) Then
        ErrInfo = ErrInfo & "Reference Sheet Not Found in the selected Excel Template file!!!" & vbNewLine & _
                            "Reference Sheet is the second sheet in the Excel Template file."
        ExcelObj.ActiveWorkbook.Close
        ExcelObj.Application.Quit
        Set ExcelObj = Nothing
        Set xmlDoc = Nothing
        DeleteFiles (XMLFileLoc)
        ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
        GenerateDocumentation = ErrWarnMsgs
        Exit Function
    End If
    'Starting Position of the used row and column
    TopLftRow = xlsRefSheet.UsedRange.Row
    TopLftCol = xlsRefSheet.UsedRange.Column
    'Last used row and column
    UsdRow = xlsRefSheet.UsedRange.Rows.Count
    UsdCol = xlsRefSheet.UsedRange.Columns.Count
    'Adding Operation_Activity Mapping Sheet
    'Moving it to the beginning
    Set xlsOpActSheet = WrkBook.Sheets.Add ' (WrkBook.Sheets.Count + 1)
    xlsOpActSheet.Name = "Op_Activity_Mapping"
    xlsOpActSheet.Move WrkBook.Sheets(1)
    CurrentOpCnt = 0
    CurrentActCnt = 0
    OpActRow = 0
    For Each Node In xmlDoc.documentElement.childNodes
        Select Case Node.nodeName
            Case "WISheet"
                'MsgBox "In WISheet Case"
                CurrentActCnt = 0
                CreateOperationSheet Node
        End Select
    Next
    'Once all the Operation Sheets are created, the following tasks has to be carried out
    '1. Move the Sheet at the end
    'xlsRefSheet.Move After:=WrkBook.Sheets(WrkBook.Sheets.Count)
    xlsRefSheet.Move , WrkBook.Sheets(WrkBook.Sheets.Count)
    '2. Rename the sheet with the Operation Name
    xlsRefSheet.Name = GetCodedOperationName(OpName)
    '3. Move the mapping sheet at the end
    xlsOpActSheet.Move , WrkBook.Sheets(WrkBook.Sheets.Count)
    '4. Add Reference Sheet Index
    AddRefernceToMapping
    '5. Autofit the Columns and Lock the sheet from editing
    For i = 1 To 6
        xlsOpActSheet.Columns(i).AutoFit
    Next
    xlsOpActSheet.Protect "HOL_7230" ', AllowSorting = True
    '6. Set the Cover Sheet as Active Sheet
    'After importing the code, I will autofit the columns
    For i = 1 To UsdCol
        WrkBook.Sheets(1).Columns(i).AutoFit
    Next
    WrkBook.Sheets(1).Select 'Indicates Cover sheet
    If DisplayXLS = True Then
        WrkBook.SaveAs GenXLSFileLoc
        MsgInfo = MsgInfo & "Documentation file is Generated." & vbNewLine & _
                            "If necessary, Please do reordering and add further information." & vbNewLine & _
                            "Opening the Excel file ... "
        ExcelObj.Visible = True
    Else
        'Closing the Excel stuffs
        WrkBook.SaveAs GenXLSFileLoc
        ExcelObj.ActiveWorkbook.Close
        ExcelObj.Application.Quit
        Set ExcelObj = Nothing
        MsgInfo = MsgInfo & "Documentation file is Generated." & vbNewLine & _
                            "File is stored at ... " & GenXLSFileLoc & vbNewLine & _
                            "If necessary, please do reordering and add further information."
    End If
    'Deleting the files
    DeleteFiles (XSDFileLoc)
    DeleteFiles (XMLFileLoc)
    If ImageDir <> "" Then
        DeleteImageDir (ImageDir)
    End If
    
    If OpInfoAdd <> "" Then
        OpInfoAdd = vbNewLine & "The following Operations Sheets are created in Excel Document" & vbNewLine & OpInfoAdd & vbNewLine
        MsgInfo = MsgInfo & vbNewLine & OpInfoAdd
    End If
    
    ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
    GenerateDocumentation = ErrWarnMsgs
End Function

'--------------------------------------------------------------------------------------------------
'This method deletes the Image directory
Function DeleteImageDir(ImageDir)
    Dim tempFolderName As String
    Dim Result As Integer
    Dim tempStr As String
    tempStr = StrReverse(ImageDir)
    Result = InStr(tempStr, "\")
    tempFolderName = Right(ImageDir, Result - 1)
    tempStr = Left(ImageDir, (Len(ImageDir) - Result))
    If tempFolderName <> "DocImages" Then
        tempStr = DeleteImageDir(tempStr)
    End If
    Dim fso 'FileSystemObjecy
    Set fso = CreateObject("Scripting.FileSystemObject")
    On Error Resume Next
    fso.DeleteFolder ImageDir, True
    Set fso = Nothing
End Function

'--------------------------------------------------------------------------------------------------
'Input: Image File Location Path
'Output: Image Root Directory Path => Example: "D:\tmp\"
Function GetDirName(XLSTemplateLoc)
    Dim Result As Integer
    Dim tempStr As String
    tempStr = StrReverse(XLSTemplateLoc)
    Result = InStr(tempStr, "\")
    GetDirName = Left(XLSTemplateLoc, (Len(XLSTemplateLoc) - Result))
End Function

'--------------------------------------------------------------------------------------------------
'This method should be called only once to
' 1. Check if there are more then one Schema file mapping
'       -> ERROR if there are more then one mapping
' 2. If there is only one schema file, write the schema to a file
'Output: String pointing to the Schema file
Function GetSchemaFromTemplate()
    Dim XMLMappings As XmlMaps
    Set XMLMappings = WrkBook.XmlMaps
    'MsgBox "Number of XMLMaps = " & XMLMappings.Count
    If XMLMappings.Count = 1 Then
        Dim XMLMapping As xmlMap
        Set XMLMapping = XMLMappings.Item(1) '("Documentation_Map")
        XSDMapName = XMLMapping.Name 'XSDMapName = "Documentation_Map"
        Dim XSDSchemas As XmlSchemas
        Set XSDSchemas = XMLMapping.Schemas
        'MsgBox "Number of XMLSChemas = " & XSDSchemas.Count
        Dim XSDSchema As XmlSchema
        Set XSDSchema = XSDSchemas.Item(1)
        Dim XSDString As String
        XSDString = XSDSchema.XML
        'MsgBox XSDString
        If XSDString <> "" Then
            XSDFileLoc = TempFileLoc & "_TempSchema.XSD"
            'MsgBox "Schema File Location is " & XSDFileLoc
            Set oWriteXSD = CATIA.FileSystem.CreateFile(XSDFileLoc, True).OpenAsTextStream("ForWriting")
            oWriteXSD.Write XSDString
            oWriteXSD.Close
            Set oWriteXSD = Nothing
        End If
    Else
        'MsgBox "Warning: More then one XML Maps exist in the Wxcel WorkBook"
    End If
    GetSchemaFromTemplate = XSDFileLoc
End Function

'--------------------------------------------------------------------------------------------------
'This method is called 'n' times depending upon the number of operation
'The method does the following
' 1. Imports the XML file conserned to an Operation
' 2. Determines the start position, this has to be done only once
' 3. Creates Shape objects related to Activities
Sub CreateOperationSheet(Node)
    ' Call method to create XML file and Import the same
    Dim CodedOpName As String
    If HasManyOperation Then
        'Need to perform the following tasks
        '1. Copy and the Paste the Reference sheet at the end
        '2. Rename the last sheet with Operation Name
        '3. Delete all the Shape Objects in the Reference sheet
        '4. Set active cell in Reference Sheet at StartY,StartX
        'Task 1
        'Before copying the sheet, store the name of the ActiveSheet
        Dim CurShtName As String
        CurShtName = WrkBook.ActiveSheet.Name
        'MsgBox "Curr sheet name is ... " & CurShtName
        'MsgBox "Sheets count " & WrkBook.Sheets.Count
        'xlsRefSheet.Copy After:=WrkBook.Sheets(WrkBook.Sheets.Count)
        xlsRefSheet.Copy , WrkBook.Sheets(WrkBook.Sheets.Count)
        Set xlsTempSheet = WrkBook.Sheets(WrkBook.Sheets.Count)
        'Task 2
        'MsgBox "op name is ... " & OpName
        CodedOpName = GetCodedOperationName(OpName)
        'MsgBox "Coded op name is ... " & CodedOpName
        xlsTempSheet.Name = CodedOpName
        'MsgBox "temp sheet name is ... " & xlsTempSheet.Name
        'xlsTempSheet = Nothing
        'Task 3
        DeleteActObjects Node.previousSibling
        'Task 4
        WrkBook.Sheets("Reference").Select
        'xlsRefSheet.Cells(StartY, StartX).Select
    End If
    ImportOperationInfo Node
    'After importing the code, I will autofit the columns
    For i = 1 To UsdCol
        xlsRefSheet.Columns(i).AutoFit
    Next
    'ADDED: I WILL DETERMINE THE SIZE OF THE IMAGE
    '       I WILL FIX THE TEXT'S HEIGHT AND WIDTH
    CaluculateImageHeight Node
    If StartFlag = False Then
        WrkBook.Sheets("Reference").Select
        'xlsRefSheet.Cells(StartY, StartX).Select
        StartFlag = True
    End If
    CreateActObjects Node
    If HasManyOperation = False Then
        HasManyOperation = True
    End If
    PreviousAct = ""
End Sub

'--------------------------------------------------------------------------------------------------
'This method determines the Image height.
'This will be called only During Create Operation sheet.
'For other cases, we are going to set the height to default value
Sub CaluculateImageHeight(OpNodeInfo)
    '----------------
    ShapeHeight = 0 'HEIGHT OF THE SHAPE OBJECT, PREFERABLE IMAGE
    ShapeWidth = 0 'ShapeHeight * 4 / 3 'Here, we ASSUME that AspectRatio is 4:3
    'NOTE:IF HEIGHT VARIABLE IS CHANGED, THEN SUITABLE VALUES HAVE TO BE SET
    '     FOR PitchY VARIABLE. OTHERWISE, IMAGES/TEXT WILL OVERLAP
    'PitchText = 4  'PITCH OF THE NEXT OBJECT WHEN THE PREVIOUS OBJECT IS TEXT
    'PitchImage = 12 'PITCH OF THE NEXT OBJECT WHEN THE PREVIOUS OBJECT IS IMAGE
    'PitchX = 9 'Columns
    'PitchY = 25 ' or 2 Row Points
    '----------------
    'Task 1: Determine the number of Text and Image based activity for a given operation
    Dim nTextAct, nImageAct, nTotalAct As Integer
    nTextAct = 0
    nImageAct = 0
    nTotalAct = 0
    If OpNodeInfo.hasChildNodes Then
        For Each SubNode In OpNodeInfo.childNodes
            If SubNode.nodeName = "WorkIllustrations" Then
                For Each LeafNode In SubNode.childNodes
                    If LeafNode.nodeName = "ActivityType" Then
                        Select Case LeafNode.Text
                            Case "Text"
                                nTextAct = nTextAct + 1
                            Case "Image"
                                nImageAct = nImageAct + 1
                        End Select
                    End If
                Next
            End If
        Next
    End If
    'Perform the following task if there EXIT images
    If nImageAct > 0 Then
        'Task 2: Calculate Total Number of Activities
        nTotalAct = nImageAct + nTextAct
        'Task 3: Determine the height of the Image
        ShapeHeight = (UsableShtHeight - ((nTotalAct - 1) * PitchY) - (nTextAct * TxtHeight)) / (nImageAct)
        If ShapeHeight < MinImageHeight Then
            ShapeHeight = MinImageHeight
        ElseIf ShapeHeight > MaxImageHeight Then
            ShapeHeight = MaxImageHeight
        End If
        ShapeWidth = ShapeHeight * 4 / 3
        TxtWidth = ShapeWidth
    End If
End Sub

'--------------------------------------------------------------------------------------------------
'This method deletes the shape objects in Reference Sheet
'Objects are associated with the previous activity
Sub DeleteActObjects(Node)
    Dim OrignalOpName, ActName, CodedOpName, CodedActName As String
    If Node.hasChildNodes Then
        On Error Resume Next
        For Each SubNode In Node.childNodes
            If SubNode.nodeName = "OperationName" Then
                OrignalOpName = SubNode.Text
                CodedOpName = GetCodedOperationName(OrignalOpName)
            ElseIf SubNode.nodeName = "WorkIllustrations" Then
                For Each LeafNode In SubNode.childNodes
                    If LeafNode.nodeName = "ActivityName" Then
                        ActName = LeafNode.Text
                        CodedActName = GetCodedActivityName(OrignalOpName, ActName)
                        For Each ShapeObj In xlsRefSheet.Shapes
                            'MsgBox OpName & ", Got ... " & "ACT_" & ActName & vbNewLine & OpName & ", Got ... " & ShapeObj.Name
                            If ShapeObj.Name = CodedActName Then
                                'MsgBox OpName & ", Deleting ... " & ShapeObj.Name
                                ShapeObj.Delete
                                Exit For
                            End If
                        Next
                    End If
                Next
            End If
        Next
    End If
End Sub

'--------------------------------------------------------------------------------------------------
'This method extracts the required info for each activity
'and creates the shape object with the activity name
Sub CreateActObjects(Node)
    Dim ActName, ActLabel, ActType, ActTextData, ActImageLoc As String
    Dim OpLabel, CodedOpName, CodedActName As String
    NextX = StartX
    NextY = StartY
    'Determine the maximum limit wher I can place the Shape Objects
    Dim tmpYLoc As Double
    tmpYLoc = 0
    For i = 1 To StartY - 1
        tmpYLoc = tmpYLoc + xlsRefSheet.Rows(i).Height
    Next
    UsableShtHeightFromRow1 = tmpYLoc + UsableShtHeight
    'MsgBox "UsableShtHeightFromRow1 = " & UsableShtHeightFromRow1
    If Node.hasChildNodes Then
        For Each SubNode In Node.childNodes
            Select Case SubNode.nodeName
                Case "OperationName"
                'I think this has to appear only ONCE
                    OpName = SubNode.Text
                    'xlsRefSheet.Name = OpName
                    'CodedOpName = SetCodedOperationName(OpName)
                Case "OperationLabel"
                    OpLabel = SubNode.Text
                    CodedOpName = SetCodedOperationName(OpName, OpLabel)
                    OpInfoAdd = OpInfoAdd & "   " & OpLabel & vbNewLine
                Case "ImageForCoverSheet"
                    If HasCoverSheetImagePlaced = False Then
                        HasCoverSheetImagePlaced = True
                        Dim ImageLocation As String
                        ImageLocation = SubNode.Text
                        CreateImageForCoverSheet ImageLocation
                    End If
                Case "WorkIllustrations"
                    'MsgBox "Found an Activity =>>" & SubNode.Text
                    If SubNode.hasChildNodes Then
                        'MsgBox "Found an Activity =>>" & SubSubNode.Text
                        For Each LeafNode In SubNode.childNodes
                            Select Case LeafNode.nodeName
                                Case "ActivityName"
                                    ActName = LeafNode.Text
                                    CodedActName = SetCodedActivityName(OpName, ActName)
                                Case "ActivityLabel"
                                    ActLabel = LeafNode.Text
                                    SetActivityLabelInMapping CodedActName, ActLabel
                                Case "ActivityType"
                                    ActType = LeafNode.Text
                                Case "TextData"
                                    ActTextData = LeafNode.Text
                                Case "ImageLocation"
                                    ActImageLoc = LeafNode.Text
                            End Select
                        Next
                    End If
                    CreateActShapes CodedActName, ActType, ActTextData, ActImageLoc
                    CodedActName = ""
                    ActType = ""
                    ActTextData = ""
                    ActImageLoc = ""
            End Select
        Next
    End If
End Sub

'--------------------------------------------------------------------------------------------------
'This method places the Shape objects depending upon the type
Sub CreateImageForCoverSheet(ImageLocation)
    On Error Resume Next
    Set xlsCoverSheet = WrkBook.Worksheets("Cover")
    If Err > 0 Then
        HasCoverSheetImagePlaced = False
        Exit Sub
    End If
    xlsCoverSheet.Select
    Dim myRange As Range
    set myRange = xlsCoverSheet.Cells(CoverImageY , CoverImageX)
    If ImageLocation <> "" Then
        With xlsCoverSheet.Pictures.Insert(ImageLocation)
            .Top = MyRange.Top
            .Left = MyRange.Left
            .Height = CoverImageHeight            
            .Width = CoverImageWidth
            .Name = CoverSheetName            
        End With
    End If
    'xlsCoverSheet.Cells(StartY - 1, StartX - 1).Select
    xlsRefSheet.Select
End Sub
'--------------------------------------------------------------------------------------------------
'This method places the Shape objects depending upon the type
Sub CreateActShapes(ActName, ActType, ActTextData, ActImageLoc)
    Dim NextHeight As Double
    NextHeight = 0
    'Dim RowHeight as Double
    'RowHeight = xlsRefSheet.Rows(NextY).Height
    Select Case PreviousAct
        Case ""
            'First Shape Object to be Placed
        Case "Text"
            NextY = NextY + Round((PitchY + TxtHeight) / 12) 'RowHeight)
        Case "Image"
            NextY = NextY + Round((PitchY + ShapeHeight) / 12) 'RowHeight)
    End Select
    NextHeight = ConvertRowHeightToPoint(NextY)
    If NextHeight >= UsableShtHeightFromRow1 - 5 Then
        NextX = NextX + PitchX
        NextY = StartY
    End If
    Select Case ActType
        Case "Text"
            PreviousAct = "Text"
            Dim XLoc, YLoc As Double
            YLoc = ConvertRowHeightToPoint(NextY - 1)
            'MsgBox "UsableShtHeightFromRow1 = " & UsableShtHeightFromRow1 & vbnewline & "NextHeight = " & NextHeight & vbnewline & "YLoc = " & YLoc
            XLoc = 0
            For i = 1 To NextX - 1
                XLoc = XLoc + xlsRefSheet.Columns(i).Width
            Next
            Set newTextBox = xlsRefSheet.Shapes.AddShape(1, XLoc, YLoc, TxtWidth, TxtHeight)
            newTextBox.Line.Visible = False
            'newTextBox.LockAspectRatio = msoTrue
            'newTextBox.ShapeRange.ZOrder = msoBringToFront
            With newTextBox.TextFrame
                .Characters.Text = ActTextData
                .Characters(1).Font.Bold = True
                .Characters(1).Font.Size = 11
            End With
            newTextBox.Name = ActName
        Case "Image"
            PreviousAct = "Image"
            Dim myRange1 As Range
            set myRange1 = xlsRefSheet.Cells(NextY, NextX)
            If ActImageLoc <> "" Then
               With xlsRefSheet.Pictures.Insert(ActImageLoc)
                     .Top = MyRange1.Top
                     .Left = MyRange1.Left
                     .Height = ShapeHeight
                     .Width = ShapeWidth
                     .Name = ActName
                End With
                ImageDir = ActImageLoc
            Else
                Dim Str As String
                Str = GetOrignalName(ActName)
                WarnInfo = WarnInfo & "Image Not Generated for activity, " & Str
            End If
    End Select
End Sub
'--------------------------------------------------------------------------------------------------
'This method creates a XML file from the main XML file and import the same to an Operation Sheet
Sub ImportOperationInfo(Node)
    Dim SubXMLData As String
    SubXMLData = ""
    SubXMLData = SubXMLData & "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine
    'Obtaining the XSD file from Excel Template file
    Dim XSDStr As String
    XSDStr = GetSchemaFromTemplate()
    TempXSDStr = """" & XSDStr & """" 'To obtain "D:\temp\temp.xml"
    SubXMLData = SubXMLData & "<WIDocRoot  xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:noNamespaceSchemaLocation=" & TempXSDStr & ">" & vbNewLine
    SubXMLData = SubXMLData & vbNewLine
    SubXMLData = SubXMLData & Node.XML ' Going to write the OperationInfo and Node = /Documentation/OperationInfo
    SubXMLData = SubXMLData & vbNewLine
    SubXMLData = SubXMLData & "</WIDocRoot >"
    Dim Result
    On Error Resume Next
    Result = WrkBook.XmlImportXml(SubXMLData, WrkBook.XmlMaps(XSDMapName), True)
    If Err > 0 Then
        If MappingExistMsg = False Then
            MappingExistMsg = True
            WarnInfo = WarnInfo & "There exist NO Schema Mapping onto the Cells in the selected Excel Template!!! " & vbNewLine
            Err.Clear
        End If
    End If
End Sub

'--------------------------------------------------------------------------------------------------
'This method Converts the Row Height to POINTS
Function ConvertRowHeightToPoint(RowNum)
    Dim PntY As Double
    Dim i As Integer
    i = 0
    UsdRow = xlsRefSheet.UsedRange.Rows.Count
    If RowNum < UsdRow Then
        For i = TopLftRow To RowNum
            PntY = PntY + xlsRefSheet.Rows(i).Height
        Next
    End If
    ConvertRowHeightToPoint = PntY
End Function

'--------------------------------------------------------------------------------------------------
'This method should be called at the end to delete all the temporary created file
Sub DeleteFiles(FileToDelete)
    Dim fso 'FileSystemObjecy
    Set fso = CreateObject("Scripting.FileSystemObject")
    fso.DeleteFile (FileToDelete)
    Set fso = Nothing
End Sub

'--------------------------------------------------------------------------------------------------
'Input: XML File Location Path   => Example: "D:\tmp\Template.xls"
'Output: XML File Directory Path => Example: "D:\tmp\"
Function GetXLSFileDir(XLSTemplateLoc)
    Dim Result As Integer
    Dim tempStr As String
    tempStr = StrReverse(XLSTemplateLoc)
    Result = InStr(tempStr, "\")
    GetXLSFileDir = Left(XLSTemplateLoc, (Len(XLSTemplateLoc) - Result + 1))
End Function

'--------------------------------------------------------------------------------------------------
'This method codes the Operation name
Function SetCodedOperationName(OrignalOpName, OrignalOpLabel)
    Dim CodedOpName As String
    Dim MaxOpSheetNameLen As Integer
    MaxOpSheetNameLen = 24
    CurrentOpCnt = CurrentOpCnt + 1
    OpActRow = OpActRow + 1
    'MsgBox "OPERATION_" & CurrentOpCnt & " for " & OrignalOpName
    If Len(OrignalOpLabel) <= MaxOpSheetNameLen Then
        CodedOpName = ChkOpExit(OrignalOpLabel)
        xlsOpActSheet.Cells(OpActRow, 1).Value = CodedOpName
    ElseIf Len(OrignalOpLabel) > MaxOpSheetNameLen Then
        CodedOpName = ChkOpExit(Left(OrignalOpLabel, MaxOpSheetNameLen))
        xlsOpActSheet.Cells(OpActRow, 1).Value = CodedOpName
    End If
    xlsOpActSheet.Cells(OpActRow, 2).Value = OrignalOpLabel
    xlsOpActSheet.Cells(OpActRow, 3).Value = OrignalOpName
    SetCodedOperationName = xlsOpActSheet.Cells(OpActRow, 1).Value
End Function

'--------------------------------------------------------------------------------------------------
'This method ...
'1. Checks if the operation label exist
'2. If the operation label exist, returns the new coded name
Function ChkOpExit(OpLabel)
    Dim tmpStr, tmpStr1, tmpStr2, CodedOpName As String
    CodedOpName = OpLabel
    Dim OpIndx As Integer
    OpIndx = 1
    For iRow = xlsOpActSheet.UsedRange.Row To xlsOpActSheet.UsedRange.Rows.Count
        tmpStr = xlsOpActSheet.Cells(iRow, 1).Value
        If tmpStr = OpLabel Then
            'there exist only one instance
            CodedOpName = Left(OpLabel, 21) & "_" & OpIndx
            'OpIndx = OpIndx + 1
        ElseIf Left(tmpStr, 21) = Left(OpLabel, 21) Then
            'Get the index
            'tmpStr1 = Mid(tmpStr, 23, Len(tmpStr))
            'Instr(start, string1, string2, comparetype)
            If InStr(tmpStr, "_") = 0 Then
                CodedOpName = Left(OpLabel, 21) & "_" & OpIndx
            Else
                tmpStr1 = Mid(tmpStr, InStr(tmpStr, "_") + 1, Len(tmpStr))
                If InStr(tmpStr1, "_") = 0 Then
                    OpIndx = CInt(tmpStr1) + 1
                    CodedOpName = Left(OpLabel, 21) & "_" & OpIndx
                End If
            End If
        End If
    Next
    ChkOpExit = CodedOpName
End Function

'--------------------------------------------------------------------------------------------------
'This method codes the Activity name based upon Operation name and Orignal ActivityName
Function SetCodedActivityName(OrignalOpName, OrignalActName)
    CurrentActCnt = CurrentActCnt + 1
    'MsgBox "OPERATION_?_ACT_" & CurrentActCnt & " for " & OrignalActName
    CodedOpName = GetCodedOperationName(OrignalOpName)
    If CodedOpName = "" Then
        Exit Function
    End If
    OpActRow = OpActRow + 1
    xlsOpActSheet.Cells(OpActRow, 1).Value = CodedOpName & "_Act_" & CurrentActCnt
    xlsOpActSheet.Cells(OpActRow, 3).Value = OrignalActName
    SetCodedActivityName = xlsOpActSheet.Cells(OpActRow, 1).Value
End Function

'--------------------------------------------------------------------------------------------------
'This method Sets the Activity Label to Op_Activity_Mapping Sheet
Sub SetActivityLabelInMapping(CodedActName, ActLabel)
    For iRow = xlsOpActSheet.UsedRange.Row To xlsOpActSheet.UsedRange.Rows.Count
        If xlsOpActSheet.Cells(iRow, 1).Value = CodedActName Then
            xlsOpActSheet.Cells(iRow, 4).Value = ActLabel
        Exit Sub
        End If
    Next
End Sub

'--------------------------------------------------------------------------------------------------
'This method return the coded name for the Operation name
Function GetCodedOperationName(OrignalOpName)
    Dim CodedOpName As String
    For iRow = xlsOpActSheet.UsedRange.Row To xlsOpActSheet.UsedRange.Rows.Count
        If xlsOpActSheet.Cells(iRow, 3).Value = OrignalOpName Then
            CodedOpName = xlsOpActSheet.Cells(iRow, 1).Value
            FindACT = InStr(CodedActName, "_Act_")
            If FindAct = 0 Then
				Exit For
			End If
        End If
    Next
    GetCodedOperationName = CodedOpName
End Function

'--------------------------------------------------------------------------------------------------
'This method returns the coded Activity name for a given Operation name
Function GetCodedActivityName(OrignalOpName, OrignalActName)
    Dim CodedActName As String
    Dim CodedOpName As String
    Dim FndLen As Integer
    Dim Str As String
    If IsCodedActivityPresent(OrignalOpName, OrignalActName) = True Then
        CodedOpName = GetCodedOperationName(OrignalOpName)
        'For iRow = xlsOpActSheet.UsedRange.Row To xlsOpActSheet.UsedRange.Rows.Count
        For iRow = xlsOpActSheet.UsedRange.Rows.Count To xlsOpActSheet.UsedRange.Row Step -1
            If xlsOpActSheet.Cells(iRow, 3).Value = OrignalActName Then
                CodedActName = xlsOpActSheet.Cells(iRow, 1).Value
                'Instr(start, string1, string2, comparetype)
                FndLen = InStr(CodedActName, CodedOpName)
                If FndLen > 0 Then
                    'CodedActName = Mid(CodedActName, FndLen + Len(CodedOpName) + 1, Len(CodedActName))
                    GetCodedActivityName = xlsOpActSheet.Cells(iRow, 1).Value
                    Exit For
                End If
            End If
        Next
    End If
    'GetCodedActivityName = CodedActName
End Function

'--------------------------------------------------------------------------------------------------
'This method returns ...
'TRUE, if Activity is created before for a given operation
'FALSE, otherwise
Function IsCodedActivityPresent(OrignalOpName, OrignalActName)
    Dim ActivityPresent As Boolean
    ActivityPresent = False
    Dim CodedOpName As String
    Dim CodedActName As String
    Dim FndLen As Integer
    If IsCodedOperationPresent(OrignalOpName) = True Then
        CodedOpName = GetCodedOperationName(OrignalOpName)
        For iRow = xlsOpActSheet.UsedRange.Row To xlsOpActSheet.UsedRange.Rows.Count
            If xlsOpActSheet.Cells(iRow, 3).Value = OrignalActName Then
                CodedActName = xlsOpActSheet.Cells(iRow, 1).Value
                'Instr(start, string1, string2, comparetype)
                FndLen = InStr(CodedActName, CodedOpName)
                If FndLen > 0 Then
                    ActivityPresent = True
                    Exit For
                End If
            End If
        Next
    End If
    IsCodedActivityPresent = ActivityPresent
End Function

'--------------------------------------------------------------------------------------------------
'This method returns ...
'TRUE, if Operation is created before for a given operation
'FALSE, otherwise
Function IsCodedOperationPresent(OrignalOpName)
    Dim OperationPresent As Boolean
    OperationPresent = False
    For iRow = xlsOpActSheet.UsedRange.Row To xlsOpActSheet.UsedRange.Rows.Count
        If xlsOpActSheet.Cells(iRow, 3).Value = OrignalOpName Then
            OperationPresent = True
            Exit For
        End If
    Next
    IsCodedOperationPresent = OperationPresent
End Function

'--------------------------------------------------------------------------------------------------
'This method adds the refernce sheet to Op_ACtivity_Mapping Sheet
'Example: Cell(A1) = Reference & Cell(B1) = 4 (Denotes the Sheet number)
Sub AddRefernceToMapping()
    OpActRow = OpActRow + 1
    xlsOpActSheet.Cells(OpActRow, 1).Value = "Reference"
    xlsOpActSheet.Cells(OpActRow, 2).Value = xlsRefSheet.Index
    xlsOpActSheet.Cells(OpActRow, 4).Value = xlsRefSheet.Name
End Sub

'--------------------------------------------------------------------------------------------------
'This method returns orignal operation name for a given coded operation name
Function GetOrignalName(CodedName)
    Dim OrignalName As String
    For iRow = xlsOpActSheet.UsedRange.Row To xlsOpActSheet.UsedRange.Rows.Count
        If xlsOpActSheet.Cells(iRow, 1).Value = CodedName Then
            OrignalName = xlsOpActSheet.Cells(iRow, 3).Value
            Exit For
        End If
    Next
    GetOrignalName = OrignalName
End Function

'--------------------------------------------------------------------------------------------------
