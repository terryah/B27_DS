' ****************************************************************************
' Purpose:  To ADD the Schema File into Excel Template File
' ****************************************************************************

Language = "VBSCRIPT"

'====================================================================================
' Audit Trail:
'--------------------------------------------------------------------
'   Author     : HOL
'   Date       : Jan 2007
'
'   Modifier   : QGD
'   Mods       : To address A0663274
'				 Added a new function "GetExcelFileFormat" to get correct file format
'				 to save the document always as "97-2003" format. 
'   Date       : 2009:06:16
'
'   Modifier   : QGD
'   Mods       : To fix the IR-040846 for .xlsx extension(Excel 2007) support.
'                Saving the excel file based on the extension selected by the user.
'   Date       : 2010:03:05
'====================================================================================
' The current version of the script takes in 3 arguments.
'
' In:  XLSFileLoc - string, Excel Template Location
'      An example is - D:\tmp\Template.xls
'
' In:  XSDFileLoc - string, Temporarily generated schema file, XSD, location
'      An example is - D:\tmp\Template.xsd
'
' In:  DisplayXLS - boolean, Indicates whether to open the Excel file at the end of command execution
'
' Result: Add the XSD file to the Excel file and save the XLS file at XLSFileLoc
'====================================================================================

Function GenerateTemplate(XLSFileLoc, XSDFileLoc, DisplayXLS)

    'MsgBox "Inside GenerateTemplate"
    
    Dim ErrWarnMsgs, ErrInfo, WarnInfo, MsgInfo As String
    ErrWarnMsgs = ""
    ErrInfo = "ERRORS:"
    WarnInfo = "WARNINGS:"
    MsgInfo = "MESSAGES:"
    
    Dim oFile
    Dim FileExist As Boolean
    Set oFile = CreateObject("Scripting.FileSystemObject")
    
    Dim XLSDir As String
    XLSDir = GetXLSFileDir(XLSFileLoc)
    'Check if the FOLDER exist
    If oFile.FolderExists(XLSDir) = False Then
        'Call MsgBox("Selected Folder does not EXIST!!!", vbCritical, "Generate Template - ERROR")
        ErrInfo = ErrInfo & "Selected Folder does not EXIST!!!" & vbNewLine
        MsgInfo = MsgInfo & "Schema File Not added to the selected Excel File" & vbNewLine
        Set oFile = Nothing
        DeleteFiles (XSDFileLoc)
        ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
        GenerateTemplate = ErrWarnMsgs
        Exit Function
    End If
    FileExist = oFile.FileExists(XLSFileLoc)
    If FileExist Then
        Set ExcelObj = CreateObject("Excel.Application")
        ExcelObj.DisplayAlerts = False
        ExcelObj.Visible = False
        Set WrkBook = ExcelObj.Workbooks.Open(XLSFileLoc)
    Else
        WarnInfo = WarnInfo & "Creating new Excel Template file ... " & XLSFileLoc & vbNewLine
        Set ExcelObj = CreateObject("Excel.Application")
        ExcelObj.DisplayAlerts = False
        ExcelObj.Visible = False
        Set WrkBook = ExcelObj.Workbooks.Add
    End If
	
	'Get the file extension selected by user .xls or .xlsx??
	Dim XLSFileExtn As String
	XLSFileExtn = GetExcelFileExtn(XLSFileLoc)
	'.xlsx is invalid on a machine which doesn't have 2007 installation
	If IsAValidExcelExtn(ExcelObj, XLSFileExtn) = False Then
        'Call MsgBox(".xlsx is not a valid file format!!!", vbCritical, "Generate Template - ERROR")
        ErrInfo = ErrInfo & ".xlsx is not a valid file format!!!" & vbNewLine
        Set oFile = Nothing
        DeleteFiles (XSDFileLoc)
        ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
        GenerateTemplate = ErrWarnMsgs
        Exit Function
    End If
    
    'Related to Schema file
    Dim XMLMappings As XmlMaps
    Set XMLMappings = WrkBook.XmlMaps
    Dim Result As Integer
    If XMLMappings.Count > 0 Then
		If XMLMappings.Count = 1 Then
			WarnInfo = WarnInfo & "Excel Template file contains a Schema Map!!!" & vbNewLine & _
				                  "This Schema Map is DELETED."
		Else
			WarnInfo = WarnInfo & "Excel Template file contains " & XMLMappings.Count & " Schema Map(s)!!!" & vbNewLine & _
				                  "These Schema Maps are DELETED."
        End If
                     
        For Each XMLMapping In XMLMappings
            XMLMapping.Delete
        Next
    End If
    
    Dim xmlMap1 As xmlMap
    On Error Resume Next
    Set xmlMap1 = WrkBook.XmlMaps.Add(XSDFileLoc)
    If IsEmpty(xmlMap1) Or IsNull(xmlMap1) Then
        'Call MsgBox("XML Schema not Added to the selected Excel Document!!!" & vbNewLine & _
        '            "Please make sure that Attribute Names does not contain SPACES in between words." & vbNewLine & _
        '            "Example: ""Assembly Process"" should be ""AsseblyProcess"".", vbCritical, "Generate Template - ERROR")
        ErrInfo = ErrInfo & "XML Schema not Added to the selected Excel Document!!!" & vbNewLine & _
                    "Please make sure that Attribute Names do not contain SPECIAL CHARACTERS or SPACES." & vbNewLine & _
                    "Example: ""Assembly Process"" should be ""AssemblyProcess""" 
        ExcelObj.ActiveWorkbook.Close
        ExcelObj.Application.Quit
        Set ExcelObj = Nothing
        DeleteFiles (XSDFileLoc)
        ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
        GenerateTemplate = ErrWarnMsgs
        Exit Function
    End If
    
    'Get the format corresponding to the file extension(.xls or .xlsx)
    Dim FileFormatNum As Integer
    FileFormatNum = GetExcelFileFormat(ExcelObj, XLSFileExtn)

    'If everything is correct, I will save the Excel file
    'And, depending upon the user, I will open the Excel file
    If DisplayXLS = True Then
        'Call MsgBox("Schema file is added to the Excel Template File." & vbNewLine & _
        '        "Excel Template file is stored at ... " & XLSFileLoc & vbNewLine & _
        '        "Please perform manual layout, and drag and drop the elements onto Excel cells to create XML Map." & vbNewLine & _
        '        "Opening the Excel document ... ", vbInformation, "Generate Template - DONE")
        MsgInfo = MsgInfo & "Schema file is added to the Excel Template File." & vbNewLine & _
                              "Excel Template file is stored at ... " & XLSFileLoc & vbNewLine & _
                              "Please perform manual layout, and drag and drop the elements onto Excel cells to create XML Map." & vbNewLine & _
                              "Opening the Excel document ... "
        WrkBook.SaveAs XLSFileLoc, FileFormatNum
        DeleteFiles (XSDFileLoc)
        ExcelObj.Visible = True
    Else
        'Closing the Excel stuffs
        WrkBook.SaveAs XLSFileLoc, FileFormatNum
        ExcelObj.ActiveWorkbook.Close
        ExcelObj.Application.Quit
        Set ExcelObj = Nothing
        DeleteFiles (XSDFileLoc)
        'Call MsgBox("Schema file is added to the Excel Template File." & vbNewLine & _
        '        "Excel Template file is stored at ... " & XLSFileLoc & vbNewLine & _
        '        "Please perform manual layout, and drag and drop the elements onto Excel cells to create XML Map.", vbInformation, "Generate Template - DONE")
        MsgInfo = MsgInfo & "Schema file is added to the Excel Template File." & vbNewLine & _
                              "Excel Template file is stored at ... " & XLSFileLoc & vbNewLine & _
                              "Please perform manual layout, and drag and drop the elements onto Excel cells to create XML Map."
    End If
    
    ErrWarnMsgs = ErrInfo & WarnInfo & MsgInfo
    'MsgBox ErrWarnMsgs
    GenerateTemplate = ErrWarnMsgs

End Function

'--------------------------------------------------------------------------------------------------
'Input: XML File Location Path   => Example: "D:\tmp\Template.xls"
'Output: XML File Directory Path => Example: "D:\tmp\"
Function GetXLSFileDir(XLSFileLoc)
    Dim Result As Integer
    Dim TempStr, TempFileLoc As String
    TempStr = StrReverse(XLSFileLoc)
    Result = InStr(TempStr, "\")
    TempFileLoc = Left(XLSFileLoc, (Len(XLSFileLoc) - Result + 1))
    
    GetXLSFileDir = TempFileLoc
    
End Function

'--------------------------------------------------------------------------------------------------
'This method should be called at the end to delete all the temporary created file
Sub DeleteFiles(FileToDelete)
    Dim fso 'FileSystemObjecy
    Set fso = CreateObject("Scripting.FileSystemObject")
    On Error Resume Next
    fso.DeleteFile (FileToDelete)
    Set fso = Nothing
End Sub

'--------------------------------------------------------------------------------------------------
'This method returns File format based on its extension
Function GetExcelFileFormat(ExcelObj, XLSFileExtn)
    
    Dim FileFormatNum As Integer
    Dim sExcelVersion As String
	sExcelVersion = ExcelObj.Version
	Dim nExcelVersion As Double
	nExcelVersion = CDbl(sExcelVersion)
		
	If nExcelVersion < 12 Then
		'You use Excel 97-2003, so do default save
		FileFormatNum = -4143
	Else
		'You use Excel 2007, so save is dependent on extension
		If XLSFileExtn = ".xls" Then
			FileFormatNum = 56
		Else
			FileFormatNum = 51
		End If
	End If
    GetExcelFileFormat = FileFormatNum
    
End Function

'--------------------------------------------------------------------------------------------------
'Input: XML File Location Path   => Example: "D:\tmp\Template.xlsx"
'Output: XML File Extn => Example: "xlsx"
Function GetExcelFileExtn(XLSFileLoc)
	
	Dim Location As Integer
    Dim FileExtn As String
    Dim TempStr As String
    TempStr = StrReverse(XLSFileLoc)
    Location = InStr(TempStr, ".")
    FileExtn = Right(XLSFileLoc, Location)
	'MsgBox("Extension is :")&FileExtn
    
    GetExcelFileExtn = FileExtn
    
End Function

'--------------------------------------------------------------------------------------------------
'".xlsx" file extension is invalid on machine which has "97-2003" format
Function IsAValidExcelExtn(ExcelObj, XLSFileExtn)

    Dim sExcelVersion As String
	sExcelVersion = ExcelObj.Version
	Dim nExcelVersion As Double
	nExcelVersion = CDbl(sExcelVersion)
		
	If ( (nExcelVersion < 12) And ( ".xlsx" = XLSFileExtn ) ) Then
	IsAValidExcelExtn = False
	Exit Function
	End If
	
    IsAValidExcelExtn = True
	
End Function

