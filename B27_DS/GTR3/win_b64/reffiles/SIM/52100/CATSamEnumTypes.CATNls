// CATSamValue
Value_CHARACTER = "Character" ; // 0
Value_SHORT = "Short integer" ; // 1
Value_INTEGER = "Integer" ; // 2
Value_REAL = "Real" ; // 3
Value_DOUBLE = "Double precision real" ; // 4
Value_COMPLEX = "Complex" ; // 5
Value_COMPLEX_DOUBLE = "Double precision complex" ; // 6
Value_POINTER = "Pointer" ; // 7
Value_NODE = "Node" ; // 8
Value_ELEMENT = "Element" ; // 9
Value_MATERIAL = "Material" ; // 10
Value_AXIS = "Axis system" ; // 11
Value_PROPERTY = "Property" ; // 12
Value_RESTRAINT = "Restraint" ; // 13
Value_LOAD = "Load" ; // 14
Value_MASS = "Mass" ; // 15
Value_SET = "Set" ; // 16
Value_GROUP = "Group" ; // 17
Value_IEXPRESSION = "IExpression" ; // 18
Value_VIRTUAL_NODE = "Virtual node" ; // 19
Value_VIRTUAL_ELEMENT = "Virtual element" ; // 20
Value_BOUNDARY_CONDITION = "Boundary condition" ; // 21
Value_INVALID_VALUE = "Invalid type of value" ; // 22

// CATSamMathType
MathType_SCALAR = "Scalar" ; // 0
MathType_VECTOR = "Vector" ; // 1
MathType_SYMMETRICAL_TENSOR = "Symmetrical tensor" ; // 2
MathType_TENSOR = "Tensor" ; // 3
MathType_EIGEN_VALUES = "Eigen values" ; // 4
MathType_BIT_ARRAY = "Bit array" ; // 5
MathType_INVALID_MATH_TYPE = "Invalid mathematical type" ; // 6

// CATSamListType
ListType_CHILDREN = "Children" ; // 0
ListType_PARENT = "Parent" ; // 1
ListType_APPLY_TO = "Apply to" ; // 2
ListType_APPLY_BY = "Apply by" ; // 3
ListType_MEMBERS = "Members of a physical type" ; // 4
ListType_USER = "User" ; // 5
ListType_INVALID_LIST_TYPE = "Invalid type of list" ; // 6

// CATSamPosition
Position_NODE = "Node" ; // 0
Position_ELEMENT = "Element" ; // 1
Position_CENTER = "Center of element" ; // 2
Position_DOF = "Degree of freedom" ; // 3
Position_NODE_OF_ELEMENT = "Node of element" ; // 4
Position_EDGE = "Edge of element" ; // 5
Position_FACE = "Face of element" ; // 6
Position_GAUSS = "Gauss point of element" ; // 7
Position_VIRTUAL_NODE = "Virtual Node" ; // 8
Position_VIRTUAL_ELEMENT = "Virtual Element" ; // 9
Position_USER_DEFINED = "User Defined" ; // 10
Position_GROUP = "Group" ; // 11
Position_SET = "Set" ; // 12
Position_INVALID_POSITION_TYPE = "Invalid type of position" ; // 13

// CATSamDof
Dof_TRANSLATION_X = "X translation" ; // 1
Dof_TRANSLATION_Y = "Y translation" ; // 2
Dof_TRANSLATION_Z = "Z translation" ; // 4
Dof_ROTATION_X = "X rotation" ; // 8
Dof_ROTATION_Y = "Y rotation" ; // 16
Dof_ROTATION_Z = "Z rotation" ; // 32
Dof_TRANSLATION_R = "R translation" ; // 64
Dof_ROTATION_RZ = "RZ rotation" ; // 128
Dof_TEMPERATURE = "Temperature" ; // 256
Dof_PRESSURE = "Pressure" ; // 512
Dof_ELECTRIC_POTENTIAL = "Electric potential" ; // 1024
Dof_DENSITY = "Density" ; // 2048
Dof_VELOCITY_X = "X velocity" ; // 4096
Dof_VELOCITY_Y = "Y velocity" ; // 8192
Dof_VELOCITY_Z = "Z velocity" ; // 16384
Dof_BALL_JOIN = "Ball join" ; // 7
Dof_PINNED = "Pinned join" ; // 56
Dof_CLAMP = "Clamp" ; // 63
Dof_PIVOT = "Pivot" ; // 31
Dof_SLIDING_PIVOT = "Sliding pivot" ; // 27
Dof_SURFACE_SLIDER = "Surface slider" ; // 60
Dof_SLIDER = "Slider" ; // 59
Dof_FLUID = "Fluid" ; // 31488
Dof_INVALID_DOF = "Invalid degree of freedom" ; // 32768

// CATSamDataType
DataType_SET = "Set" ; // 0
DataType_ENTITY = "Entity" ; // 1
DataType_CHARACTERISTIC = "Characteristic" ; // 2
DataType_LIST = "List" ; // 3
DataType_MATHEMATICAL = "Mathematical" ; // 4
DataType_APPLY = "Apply" ; // 5
DataType_INVALID_DATA_TYPE = "Invalid type of data" ; // 6

// CATSamStatus
Status_DISPLAYED = "Displayed" ; // 1
Status_EXT_POINTER = "External pointer" ; // 2
Status_DELETED = "Deleted" ; // 4
Status_DEAD = "Dead" ; // 8
Status_ISOLATED = "Isolated" ; // 16
Status_CHECKING = "Check" ; // 32
Status_DESACTIVATED = "Desactivated" ; // 64
Status_EXTERNAL_LINKED = "Linked on an other model" ; // 128
Status_CONDENSED_MESH = "Condensed Mesh Entity" ; // 256
Status_INVALID_STATUS = "Invalid status" ; // 512

// CATSamCompareType
CompareType_POINTER = "Pointer" ; // 0
CompareType_TAG = "Tag" ; // 1
CompareType_CATEGORY = "Category" ; // 2
CompareType_USER_NUMBER = "User number" ; // 3
CompareType_PHYSICAL_TYPE = "Physical type" ; // 4
CompareType_SOLVER_NAME = "Solver name" ; // 5
CompareType_USER_NAME = "User name" ; // 6
CompareType_EXTERNAL_POINTER = "External pointer" ; // 7
CompareType_SEQUENTIAL_NUMBER = "Sequential number" ; // 8
CompareType_DATA_TYPE = "Data type" ; // 9
CompareType_EXTERNAL_TAG = "External tag" ; // 10
CompareType_INVALID_COMPARE_TYPE = "Invalid type of compare" ; // 11

// CATSamCheckType
CheckType_POINTER = "Pointer" ; // 1
CheckType_CHILD = "Child" ; // 2
CheckType_APPLY_TO = "Apply to" ; // 4
CheckType_RECURSE = "Recurse" ; // 8
CheckType_VALID = "Valid" ; // 16
CheckType_INVALID_CHECK_TYPE = "Invalid type of check" ; // 32

// CATSamRefFrame
RefFrame_LOCAL = "Local" ; // 0
RefFrame_GLOBAL = "Global" ; // 1
RefFrame_NONE = "None" ; // 2
RefFrame_INVALID_REF_FRAME = "Invalid reference frame" ; // 3

// CATSamRefFrameType
RefFrameType_RECTANGULAR = "Rectangular" ; // 0
RefFrameType_CYLINDRICAL = "Cylindrical" ; // 1
RefFrameType_CYLINDRICAL_INWARD = "Cylindrical inward" ; // 2
RefFrameType_SPHERICAL = "Spherical" ; // 3
RefFrameType_SPHERICAL_INWARD = "Spherical inward" ; // 4
RefFrameType_INVALID_REF_FRAME_TYPE = "Invalid reference frame type" ; // 5

// CATSamApplyQualifier
ApplyQualifier_UPPER = "Upper" ; // 0
ApplyQualifier_MIDDLE = "Middle" ; // 1
ApplyQualifier_LOWER = "Lower" ; // 2
ApplyQualifier_MASTER = "Master" ; // 3
ApplyQualifier_SLAVE = "Slave" ; // 4
ApplyQualifier_NODE_OF_ELEMENT = "Node of element" ; // 5
ApplyQualifier_NONE = "None" ; // 6
ApplyQualifier_INVALID_APPLY_QUALIFIER = "Invalid apply qualifier" ; // 7

// CATSamSpace1DType
Space1DType_VECTOR = "Vector" ; // 0
Space1DType_CONTINUOUS = "Continuous" ; // 1
Space1DType_SPARSE = "Sparse" ; // 2
Space1DType_INVALID_1DSPACE_TYPE = "Invalid 1D space type" ; // 3

// CATSamSpace2DType
Space2DType_MATRIX = "Matrix" ; // 0
Space2DType_CONTINUOUS = "Continuous" ; // 1
Space2DType_SPARSE = "Sparse" ; // 2
Space2DType_SYMMETRIC = "Symmetric" ; // 3
Space2DType_INVALID_2DSPACE_TYPE = "Invalid 2D space type" ; // 4

// CATSamSpace3DType
Space3DType_TENSOR = "Tensor" ; // 0
Space3DType_CONTINUOUS = "Continuous" ; // 1
Space3DType_SPARSE = "Sparse" ; // 2
Space3DType_INVALID_3DSPACE_TYPE = "Invalid 3D space type" ; // 3

// CATSamSpaceVectorType
SpaceVectorType_NOMINAL = "Nominal" ; // 0
SpaceVectorType_ORDINAL = "Ordinal" ; // 1
SpaceVectorType_INTERVAL = "Interval" ; // 2
SpaceVectorType_RATIO = "Ratio" ; // 3
SpaceVectorType_CONTINUOUS = "Continuous" ; // 4
SpaceVectorType_INVALID_SPACEVECTOR_TYPE = "Invalid type of vector" ; // 5

// CATSamAggregationMode
AggregationMode_ADD = "Add" ; // 0
AggregationMode_REPLACE = "Replace" ; // 1
AggregationMode_MULTIPLY = "Multiply" ; // 2
AggregationMode_SUPPRESS = "Suppress" ; // 3
AggregationMode_JUXTAPOSE = "Juxtapose" ; // 4
AggregationMode_INVALID_AGGREGATION_MODE = "Invalid aggregation mode" ; // 5

// CATSamMeshType
MeshType_PHYSICAL = "Physical" ; // 1
MeshType_VIRTUAL = "Virtual" ; // 2
MeshType_INVALID_MESH_TYPE = "Invalid type of mesh" ; // 4

// CATSamSubType
SubType_NOMINAL = "Nominal" ; // 0
SubType_LOGARITHMIC = "Logarithmic" ; // 1
SubType_ELASTIC = "Elastic" ; // 2
SubType_INELASTIC = "Inelastic" ; // 3
SubType_THERMAL = "Thermal" ; // 4
SubType_PLASTIC = "Plastic" ; // 5
SubType_INVALID_SUBTYPE = "Invalid subtype" ; // 6

// CATSamComponent
Component_NONE = "None" ; // 0
Component_C11 = "C11" ; // 1
Component_C12 = "C12" ; // 2
Component_C13 = "C13" ; // 4
Component_C21 = "C21" ; // 8
Component_C22 = "C22" ; // 16
Component_C23 = "C23" ; // 32
Component_C31 = "C31" ; // 64
Component_C32 = "C32" ; // 128
Component_C33 = "C33" ; // 256
Component_C1 = "C1" ; // 1
Component_C2 = "C2" ; // 16
Component_C3 = "C3" ; // 256
Component_ALL = "All" ; // 511
Component_INVALID_COMPONENT = "Invalid component" ; // 512

// CATSamBendingType
BendingType_CENTER_OF_CURVATURE = "Center of curvature" ; // 0
BendingType_TANGENCY = "Tangency" ; // 1
BendingType_BEND_RADIUS = "Bend radius" ; // 2
BendingType_ARC_ANGLE = "Arc angle" ; // 3
BendingType_INVALID_BENDING_TYPE = "Invalid bending type" ; // 4

// CATSamWallType
WallType_INFINITE_PLANE = "Infinite plane" ; // 0
WallType_LIMITED_PLANE = "Limited plane" ; // 1
WallType_CYLINDRICAL = "Cylindrical" ; // 2
WallType_SPHERICAL = "Spherical" ; // 3
WallType_INVALID_WALL_TYPE = "Invalid wall type" ; // 4

// CATSamEntityType
EntityType_NODE = "Node" ; // 0
EntityType_ELEMENT = "Element" ; // 1
EntityType_GEOMETRY = "Geometry" ; // 2
EntityType_PREPROCESSOR_ENTITY = "Preprocessor entity" ; // 3
EntityType_SET = "Set" ; // 4
EntityType_INVALID_ENTITY_TYPE = "Invalid type of entity" ; // 5
// CATSamEvalType
EvalType_VALIDITY = "Validity" ; // 0
EvalType_COMBINABILITY = "Combinability" ; // 1
EvalType_INVALID_EVAL_TYPE = "Invalid criterion" ; // 2
// CATSamPositionData
PositionData_MAIN_NODES = "Main nodes" ; // 0
PositionData_ALL_NODES = "All nodes" ; // 1
