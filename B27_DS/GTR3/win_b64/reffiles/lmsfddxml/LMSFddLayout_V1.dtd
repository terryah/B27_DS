<!ELEMENT layout (textstyle?, display*)>
<!ELEMENT display (textstyle?, backgroundcolor*, graph*, screenaxis*, screenaxis3D*, grid3D*, legend*, emptycell*, freetitle?)>
<!ELEMENT graph ((axis|axisref)*, datagroup+, cursorgroup?, cursorgroupref?, graphtitle?, textstyle?, cursorbox*, floatingtextbox*)>
<!ELEMENT screenaxis (textstyle?)>
<!ELEMENT emptycell EMPTY>
<!ELEMENT screenaxis3D (textstyle?)>
<!ELEMENT axis (limits?, colorscale?, customcolorscale?, grid2D?)>
<!ELEMENT axisref EMPTY>
<!ELEMENT limits (limit*)>
<!ELEMENT limit EMPTY>
<!ELEMENT datagroup (linestylescheme?, plot*)>
<!ELEMENT linestylescheme (linestyleentry*)>
<!ELEMENT linestyleentry (linestyleentry*)>
<!ELEMENT colorscale EMPTY>
<!ELEMENT customcolorscale (colorscaleentry*)>
<!ELEMENT colorscaleentry EMPTY>
<!ELEMENT cursorgroup (activecursor?, cursormarker*)>
<!ELEMENT cursorgroupref EMPTY>
<!ELEMENT cursormarker (cursormarkerpart+)>
<!ELEMENT activecursor (cursormarkerpart+)>
<!ELEMENT cursormarkerpart EMPTY>
<!ELEMENT grid3D EMPTY>
<!ELEMENT textstyle EMPTY>
<!ELEMENT plot (linestyleentry?)>
<!ELEMENT legend (textstyle?)>
<!ELEMENT backgroundcolor EMPTY>
<!ELEMENT graphtitle EMPTY>
<!ELEMENT freetitle (textstyle?)>
<!ELEMENT grid2D EMPTY>
<!ELEMENT cursorbox EMPTY>
<!ELEMENT floatingtextbox (textstyle?)>


<!ATTLIST layout id ID #IMPLIED
                 layoutmode (UIMode|PaperMode) #IMPLIED
                 paperwidth  CDATA #IMPLIED
                 paperheight CDATA #IMPLIED>

<!ATTLIST plot r CDATA #IMPLIED
               g CDATA #IMPLIED
               b CDATA #IMPLIED
               t CDATA #IMPLIED
               w CDATA #IMPLIED
               symbol CDATA #IMPLIED
               lineon CDATA #IMPLIED
               interpolationstyle CDATA #IMPLIED
               interpolatey CDATA #IMPLIED
               interpolatex CDATA #IMPLIED
               orderbasedaccuracy CDATA #IMPLIED
               drawtext CDATA #IMPLIED>


<!ATTLIST display id ID #IMPLIED
                  col CDATA #REQUIRED
                  row CDATA #REQUIRED
                  colspan CDATA #IMPLIED
                  rowspan CDATA #IMPLIED
                  x CDATA #IMPLIED
                  y CDATA #IMPLIED
                  w CDATA #IMPLIED
                  h CDATA #IMPLIED
                  vgl_prefvalues_pm CDATA #IMPLIED
                  vgl_prefvalues_ui CDATA #IMPLIED
                  hgl_prefvalues_pm CDATA #IMPLIED
                  hgl_prefvalues_ui CDATA #IMPLIED
                  automaticlegend (yes|no) #IMPLIED>

<!ATTLIST graph id ID #REQUIRED
                type CDATA #REQUIRED
                orientation CDATA #IMPLIED
                col CDATA #IMPLIED
                row CDATA #IMPLIED
                colspan CDATA #IMPLIED
                rowspan CDATA #IMPLIED
                spaceoption CDATA #IMPLIED
                unittype (MKS|DataDefined|OptionsDefined) #IMPLIED
                weighting CDATA #IMPLIED
                username CDATA #IMPLIED>


<!ATTLIST emptycell id     ID    #IMPLIED
                    col    CDATA #REQUIRED
                    row    CDATA #REQUIRED
                    height CDATA "-1"
                    width  CDATA "-1">

<!ATTLIST screenaxis id ID #IMPLIED
                     graphs CDATA #REQUIRED
                     graphaxes CDATA #REQUIRED
                     direction CDATA #IMPLIED
                     col CDATA #REQUIRED
                     row CDATA #REQUIRED
                     colspan CDATA #IMPLIED
                     rowspan CDATA #IMPLIED
                     showticks (yes|no) "yes"
                     showlabels (yes|no) "yes"
                     showlimits (yes|no) "yes"
                     showunit (yes|no) #IMPLIED
                     showformat (yes|no) #IMPLIED
                     showtitle (yes|no) #IMPLIED
                     align (Rectangle|Left|Right|Bottom|Top) #REQUIRED
                     preferredsize (NoPS|AutomaticPS) #REQUIRED>

<!ATTLIST screenaxis3D id ID #REQUIRED
                       graphs IDREFS #REQUIRED
                       graphaxes CDATA #REQUIRED
                       direction CDATA #IMPLIED
                       showticks (yes|no) "yes"
                       showlabels (yes|no) "yes"
                       showlimits (yes|no) "yes"
                       showunit (yes|no) #IMPLIED
                       showformat (yes|no) #IMPLIED
                       showtitle (yes|no) #IMPLIED
                       maximumnumberlabels CDATA "8"
                       linenumber (X1|X2|X3|X4|Y1|Y2|Y3|Y4|Z1|Z2|Z3|Z4) #REQUIRED>

<!ATTLIST axisref idref IDREF #REQUIRED
                  dofusage CDATA #REQUIRED>

<!ATTLIST axis id ID #IMPLIED 
               dofusage CDATA #REQUIRED
               format CDATA #REQUIRED
               whitespace_values CDATA #IMPLIED
               whitespace_traces CDATA #IMPLIED
               username CDATA #IMPLIED
               usedefaultname (yes|no) "yes"
               resolution (one|two|four|five|ten|automatic) "automatic"
               majors (one|two|four|five|ten|automatic) "automatic">

<!ATTLIST limits limitcalc (FreeLimits|OptimizedLimits|FixedLimits|ADLimits|AbsoluteLimits) #REQUIRED>

<!ATTLIST limit role CDATA #REQUIRED
                 limitcalc (Fixed|ApplicationDefined|Absolute) #REQUIRED
                 ll CDATA #IMPLIED
                 ul CDATA #IMPLIED
                 dl CDATA #IMPLIED
                 units_per_mm CDATA #IMPLIED>


<!ATTLIST datagroup id ID #REQUIRED
                    username CDATA #IMPLIED
                    show_scenario_username CDATA #IMPLIED>

<!ATTLIST linestylescheme id ID #IMPLIED
                          idref IDREF #IMPLIED
                          scheme CDATA #IMPLIED>

<!ATTLIST linestyleentry red CDATA #REQUIRED
                         green CDATA #REQUIRED
                         blue CDATA #REQUIRED
                         linetype CDATA #REQUIRED
                         linethickness CDATA #REQUIRED
                         symbol CDATA #REQUIRED
                         noline CDATA #REQUIRED>

<!ATTLIST colorscale scheme CDATA #REQUIRED>

<!ATTLIST colorscaleentry  red CDATA #REQUIRED
                           green CDATA #REQUIRED
                           blue CDATA #REQUIRED>

<!ATTLIST cursorgroup id ID #REQUIRED>
<!ATTLIST cursorgroupref idref IDREF #REQUIRED>

<!ATTLIST cursormarker id ID #REQUIRED
                       cdd CDATA #REQUIRED>

<!ATTLIST activecursor id ID #REQUIRED
                       cdd CDATA #REQUIRED>

<!ATTLIST cursormarkerpart part CDATA #REQUIRED
                           values CDATA #IMPLIED
                           discretevalues CDATA #IMPLIED
                           discreteplotnbrs CDATA #IMPLIED>

<!ATTLIST grid3D linenumbers (X1|X2|X3|X4|Y1|Y2|Y3|Y4|Z1|Z2|Z3|Z4) #REQUIRED
                 screenaxes IDREFS #REQUIRED>

<!ATTLIST grid2D majors (on|off) #IMPLIED
                 minors (on|off) #IMPLIED
                 majorr CDATA #IMPLIED
                 majorg CDATA #IMPLIED
                 majorb CDATA #IMPLIED
                 majort CDATA #IMPLIED
                 majorw CDATA #IMPLIED
                 minorr CDATA #IMPLIED
                 minorg CDATA #IMPLIED
                 minorb CDATA #IMPLIED
                 minort CDATA #IMPLIED
                 minorw CDATA #IMPLIED>

<!ATTLIST textstyle role CDATA #IMPLIED
                    font CDATA #REQUIRED
                    size CDATA #REQUIRED
                    inherit (yes|no) #REQUIRED>

<!ATTLIST legend id ID #IMPLIED
                 col CDATA #REQUIRED
                 row CDATA #REQUIRED
                 colspan CDATA "1"
                 rowspan CDATA "1"
                 graphs IDREFS #IMPLIED
                 showlegend (yes|no) #REQUIRED
                 visibleattributes CDATA #IMPLIED
                 availableattributes CDATA #IMPLIED
				 showplotswithnodata CDATA #IMPLIED>

<!ATTLIST backgroundcolor displayon CDATA #REQUIRED
                          displayR CDATA #REQUIRED
                          displayG CDATA #REQUIRED
                          displayB CDATA #REQUIRED
                          graphon CDATA #REQUIRED
                          graphR CDATA #REQUIRED
                          graphG CDATA #REQUIRED
                          graphB CDATA #REQUIRED
                          cursoron CDATA #IMPLIED
                          cursorBackgroundR CDATA #IMPLIED
                          cursorBackgroundG CDATA #IMPLIED
                          cursorBackgroundB CDATA #IMPLIED
                          cursorR CDATA #IMPLIED
                          cursorG CDATA #IMPLIED
                          cursorB CDATA #IMPLIED>

<!ATTLIST graphtitle showtitle (yes|no) "yes"
                     horposition (Left|Width|Right) "Left"
                     verposition (Bottom|Height|Top) "Top"
                     horrel CDATA #IMPLIED
                     verrel CDATA #IMPLIED
                     includedatagroupnames (yes|no) "yes">

<!ATTLIST freetitle col CDATA #REQUIRED
                    row CDATA #REQUIRED
                    colspan CDATA #REQUIRED
                    rowspan CDATA #REQUIRED
                    text CDATA #REQUIRED
                    id CDATA #IMPLIED>

<!ATTLIST cursorbox type CDATA #REQUIRED
                    cursortype CDATA #REQUIRED
                    markerid CDATA #REQUIRED
                    partdescr CDATA #REQUIRED
                    inputdescr CDATA #REQUIRED
                    grapharea CDATA #REQUIRED
                    datagroup CDATA #REQUIRED
                    plot CDATA #REQUIRED
                    outputperplot CDATA #REQUIRED
                    xrel CDATA #REQUIRED
                    yrel CDATA #REQUIRED
                    >
<!ATTLIST floatingtextbox metaattrs CDATA #IMPLIED
                          text  CDATA #IMPLIED
                          plotnbr  CDATA #IMPLIED
                          datagroupid CDATA #IMPLIED
                          xrel  CDATA #REQUIRED
                          yrel  CDATA #REQUIRED>
