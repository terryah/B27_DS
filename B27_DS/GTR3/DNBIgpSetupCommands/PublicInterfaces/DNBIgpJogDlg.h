#ifndef DNBIgpJogDlg_H
#define DNBIgpJogDlg_H

// COPYRIGHT DASSAULT SYSTEMES 2006

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBSystemBase.h"
#include "DNBSystemDefs.h"

// DNBDeviceModel
#include "DNBBasicJoint3D.h"

// DBNWorldModel
#include "DNBBasicEntity.h"

// DNBRobotModel
#include "DNBBasicRobot3D.h"

//DNBRobotBase
#include <DNBSimRobotObserver.h>

// DNBMath
#include "DNBXform3D.h"

#include "DNBCATBase.h"
#include "CATIProduct.h"
// ApplicationFrame
#include "CATDlgDialog.h"

// CATIAApplicationFrame
#include "CATI3DCompass.h"

// Mathematics
#include "CATMathDirection.h"
#include "CATMathTransformation.h"
//DNBSimulationControl
#include "DNBBasicSimEvent.h"
// System
#include "CATBooleanDef.h"
#include "CATListOfCATBaseUnknown.h"
#include "CATListOfCATUnicodeString.h"
#include "CATListOfInt.h"
#include "DNBCATDefs.h"

//DNBIgpSetupCommands
#include "DNBIgpJogAndTeach.h"
#include "DNBIgpStack.h"
#include "CATMathPointf.h"

#include <scl_map.h>


// Visualization
class CAT3DManipulator;

// DNBSimulationCommands
class DNBBasicSimCmd;

// Dialog
class CATDlgFrame;
class CATDlgTabContainer;
class CATDlgCheckButton;
class CATDlgLabel;  
class CATDlgTabPage;  
class CATDlgContainer;   
class CATDlgCombo;
class CATDlgMultiList;
class CATDlgRadioButton;

// DNBRobotModel
class DNBRobConfiguration; 

// DNBIgpSetupCommands
class DNBIgpCartesianFrame;
class DNBIgpDOFTabPage;
class DNBIgpUndoRedoEvtPub;     
class DNBIgpTCPGoFrame;
class DNBIgpRubberLine;
class DNBIgpMTJogFrame;
class DNBIgpJointNameRep ; 
class CATMathPointf ;

//----------------------------------------------------------------------

/**
* Class to create a Jog Window.
*/

class ExportedByDNBIgpJogAndTeach DNBIgpJogDlg: public CATDlgDialog
{
   // Allows customization/internationalization of command's messages
   // ---------------------------------------------------------------
   DeclareResource( DNBIgpJogDlg, CATDlgDialog )
      
public:
   
      /**
    * JogTabs modes.
    * @param IgpJogDOF
    *   Jog Panel with Joints Tab Page
    * @param IgpJogDOFNoHome
    *   Jog Panel without Home Selection Frame in the Joints Tab page
    * @param IgpJogCartesian
    *   Jog Panel with Cartesian Tab Page
    * @param IgpJogRobot
    *   Jog Panel with Robot Tab Page
    * @param IgpJogAuxAxes
    *   Jog Panel with Auxilliary Axis Tab Page
    * @param IgpJogMountedDevices
    *   Jog Panel with Mounted Devices Tab Page
    */
   enum                                JogTabs {IgpJogDOF = 1, 
                                                IgpJogDOFNoHome = 2,
                                                IgpJogCartesian = 4,
                                                IgpJogRobot = 5,
                                                IgpJogAuxAxes = 8,
                                                IgpJogMountedDevices=16};

      /**
    * UserAction modes. Action performed by the User on the dialog
    * @param None
    *   For No Action or Reset.
    * @param Joint
    *   For home combo drop down, joint spinner modifications, device manipulators.
    * @param Tcp
    *   For action performed on Cartesian Tab Page, compass manipulations.
    * @param Home
    *   For action performed on home combo selection.
    * @param Config
    *   For action performed on config multilist.
    * @param Mount
    *   For action performed on mount combo.
    */
   enum                                UserAction {None, Joint, Tcp, Home, Config,Mount};

      /**
    * @nodoc
    * TCP Orientation modes. Orientation of the compass at the current TCP position.
    * @param World
    *   Compass at current TCP keeps its orientation as World.
    * @param Local
    *   Compass at current TCP keeps its orientation as Current axis.
    * @param Device
    *   Compass at current TCP keeps its orientation as device base.
    * @param Tool
    *   Compass at current TCP keeps its orientation as selected tool profile.
    * @param User
    *   Compass at current TCP keeps its orientation as user defined.
    */
   enum                                CoorSys { World, Local, Device, Tool, User };

/**
    * @nodoc
    * Reference Frames modes. The reference frame for the TCP definition(Position & Orientation)  
    * @param WorldRef
    *   World frame is the reference for the TCP
    * @param LocalRef
    *   Current axis is the reference for the TCP
    * @param BaseRef
    *   Base of the robot is the reference for the TCP
    * @param UserRef
    *   User defined frame is the reference for the TCP
    * @param ObjectRef
    *   Object frame is the reference for the TCP
    */
   enum                                ReferenceFrame { WorldRef, LocalRef, BaseRef, UserRef, ObjectRef };

   /**
    * @nodoc
    * To determine the manipulated device.
    * @param MainRobot
    *   The Robot.
    * @param RailTrackGantry
    *   Auxilliary device type is Rail.
    * @param EndOfArmTooling
    *   Auxilliary device type is End Of Arm Tooling.
    * @param WorkpiecePositioner
    *   Auxilliary device type is Workpiece Positioner.
    */
   enum                                AuxDeviceType { MainRobot, RailTrackGantry, EndOfArmTooling, WorkpiecePositioner };

   /**
    * @nodoc
    * Update Type.
    * @param FromOtherControl
    *   If the device position is updated from controls other than spinner and compass.
    * @param FromSpinner
    *   If the device position is updated from Spinner
    * @param FromCompass
    *   If the device position is updated from compass
    */
   enum                                UpdateType { FromOtherControl, FromSpinner, FromCompass };

   /**
    * @nodoc
    * DNBTrackTCPMode modes.
    * @param DNBTCPTrackDisabled
    *   Track TCP is disabled.
    * @param DNBTCPTrackCartesian
    *   Track TCP works only for Cartesian(CartesianTabPage + Compass) Moves.
    * @param DNBTCPTrackJoint
    *   Track TCP works only for Joint(DOFTabPage + Manipulator) Moves.
    * @param DNBTCPTrackAll
    *   Track TCP works for All (Joint and Cartesian) Moves.
    */
   enum                                DNBTrackTCPMode {DNBTCPTrackDisabled = 0,  
                                                        DNBTCPTrackCartesian = 1,
                                                        DNBTCPTrackJoint = 2,
                                                        DNBTCPTrackAll = 3};

   /**
    * @nodoc
    * DNBTCPFreezeMode modes.
    * @param DNBFreezeDisabled
    *   Track TCP is disabled.
    * @param DNBFreezeX
    *   Only the X Component of TCP is freezed.
    * @param DNBFreezeY
    *   Only the Y Component of TCP is freezed.
    * @param DNBFreezeZ
    *   Only the Z Component of TCP is freezed.
    * @param DNBFreezeYaw
    *   Only the Yaw Component of TCP is freezed.
    * @param DNBFreezePitch
    *   Only the Pitch Component of TCP is freezed.
    * @param DNBFreezeRoll
    *   Only the Roll Component of TCP is freezed.
    * @param DNBFreezeTranslation
    *   Only the translation(X, Y, Z) of TCP is freezed.
    * @param DNBFreezeRotation
    *   Only the rotation(Yaw, Pitch, Roll) of TCP is freezed.
    * @param DNBFreezeAll
    *   All the six components of TCP are freezed.
    */
   enum                                DNBTCPFreezeMode {DNBFreezeDisabled = 0, 
                                                         DNBFreezeX = 1, 
                                                         DNBFreezeY = 2, 
                                                         DNBFreezeZ = 4, 
                                                         DNBFreezeYaw = 8, 
                                                         DNBFreezePitch = 16, 
                                                         DNBFreezeRoll = 32, 
                                                         DNBFreezeTranslation = 7, 
                                                         DNBFreezeRotation = 56, 
                                                         DNBFreezeAll = 63};

   /**
   * @These Translation Modes Are used in the context MT Jog
   * @param DNBDefaultManipulation
   *   No Translation
   * @param DNBRobotBaseManipulation
   *   The Robot Base Moves while the Robot TCP is Fixed at the Tag
   * @param DNBTagManipulation
   *   The Tracking Tag moves and the robot follows.
   * @param DNBRobotTCPManipulation
   *   The Robot TCP Moves while the Robot Base is Fixed. Simple IK Jogging
   * @param DNBProductManipulation
   *   The Product moves and the robot tracks the Tracked Tag stored it it moves with the product.
   */
   enum                                DNBMTManipulationMode{DNBDefaultManipulation =0 ,
                                                            DNBRobotBaseManipulation =1,
                                                            DNBTagManipulation =2,
                                                            DNBRobotTCPManipulation =4,
                                                            DNBProductManipulation =8};


   /** 
    * Constructs a Jog Dialog.
    * @param iBaseCommand
    *   The Basic Simulation command.
    * @param iMecanism
    *   The Mechanism.
    * @param iContext
    *   Context to build Tab pages of jog dialog. 
        Jog Tab page modes(Ref enum JogTabs).
    * @param iDlgName
    *   Name of the dialog object.
    * @param ID
    *   Dialog object ID.
    * @param spFatherProd
    *   Product for which this jog dialog needs to be created.
    * @param iSimContext
    *   Simulation Context. 
    */
   DNBIgpJogDlg( DNBBasicSimCmd *iBaseCommand,
       CATBaseUnknown_var iMecanism, 
       int iContext = IgpJogRobot|IgpJogAuxAxes,
       const CATString & DlgName = "IgripJog", 
       int ID = 0, 
       CATIProduct_var spFatherProd = NULL_var, 
       int iSimContext = FALSE);
   
   virtual                             ~DNBIgpJogDlg();

   /**
    * WARNING
    * The cancel method has to be called before the panel deletion
    * Some processings about the compass are implemented here.
    */
   void                                Cancel();

   /**
    * Show or hide the home position combo box.
    * @param iState
    * The visibility state to set.
    *   <br>
    *   <b>Legal values</b> : It can be set to either :
    *   <dl>
    *   <dt><tt>CATDlgShow</tt><dd> to show the dialog object,
    *   <dt><tt>CATDlgHide</tt><dd> to hide the dialog object.
    *   </dl>
    */
   void                                SetHomeVisibility(CATULong iState);

   /**
    * Creates and returns the applicative frame. 
    * This frame is located in the back of the panel.
    */
   CATDlgFrame*                        GetApplicativeFrame();

   /**
    * Returns the last action performed by the user for Tab Page corresponding to iMechanism
    * if NULL, it gives the Last User Action irrespective of the device.
    * RETURN TYPE : (None, Joint, Tcp, Home, Config)
    * @param iMechanism
    * The pointer to the iMechanism.
    */
   UserAction                          GetLastUserAction(CATBaseUnknown * iMechanism = NULL);

    /** 
    * This method returns the home position name of the given mechanism.
    * If iMechanism is NULL, it returns the home position of the base mechanism.
    * @param iMechanism
    * The pointer to the iMechanism.
    */
   CATUnicodeString                    GetLastSelectedHome(CATBaseUnknown *iMechanism=NULL);
   
    /** 
    * Joint page can display Home and Joint values, if FlushInPanel is called by TeachDlg or RobotMotionDlg, then target is known.
    * We need indicate if the target is Home or Joint and set right last user action
    * @param iUpdateHomeCombo
    * Home combo is updated if TRUE.
    * @param iUpdateToolProfCombo
    * Tool profile combo is updated if TRUE.
    * @param iTargetTypes
    * List of Target types(For internal use).
    */
   void                                FlushInPanel( boolean UpdateHomeCombo = TRUE, boolean UpdateToolProfCombo = TRUE, CATListOfCATUnicodeString * TargetType = NULL );

    /**
    * This method updates the Jog Panel without flushing. It has been done exclusively for Teach
    * to take care of Robot Move and prevent any thread deadlock
    * @param DNBSimObserveRobot
    *  TCP Observer for DNBBasicDevice3D objects (robots only).
    */
   void                                UpdatePanelonRobotMove(DNBSimObserveRobot &data);
    
   /** 
    * Register Undo Transactions
    * @param iNewTrans
    * Creates new transaction if TRUE.
    */
   void                                RegisterUndo(const CATBoolean &iNewTrans);


    /** 
    * To Manage Undo in Jog Panel. 
    * To avail undo feature ,this should be called from the Undo method of 
    * the calling command.
    */
   static void                         UndoJog(void *);

      /** 
    * To Manage Redo in Jog Panel. 
    * To avail Redo feature ,this should be called from the Redo method of 
    * the calling command
    */
   static void                         RedoJog(void *);

   /** 
    * Creates Manipulators on the device. 
    */
   void                                AttachManipulators();

/** 
    * Enable all the elements of Jog panel. 
    */
   void                                ActivateMoveControl();

   /** 
    * Disable all the elements of Jog panel. 
    */
   void                                DesactivateMoveControl();

    /** 
    * Returns Jog Notification.
    */
    const char *                        GetJogNotification();

   /** 
    * Returns position of the compass.
    */
   DNBXform3D                          GetCompassPosition();

   /** 
    * Returns the current tool profile name set in the Tool Profile combo
    */
   CATUnicodeString                    GetToolProfName(); 

   /** 
    * Resets and sets an empty line in Home combo
    */
   void                                ResetHomeCombo(); 

   /**To toggle TCP Tracking.
   * @param iTrackMode
    * Defines the moves for which TCP will be tracked.
    * @param iFreezeMode
    * Defines the components of TCP to be freezed.
    */
   void                                SetTrackTCPMode(DNBTrackTCPMode iTrackMode, DNBTCPFreezeMode iFreezeMode);

   /** 
    * To get TCP Tracking Mode.
    */
   DNBTrackTCPMode                     GetTCPTrackMode();

   /** 
    * To get TCP Freeze Mode.
    */
   DNBTCPFreezeMode                    GetTCPFreezeMode();


   void                                DettachManipulators();
   void                                AddCallBacks();
   
    /** 
    * @nodoc
    */
   void                                SetUserCoorSys( DNBXform3D &userFrame, int value );
   
    /** 
    * @nodoc
    */
   void                                UpdateToolProfCombo();
   
   /** 
    * @nodoc
    */
   void                                RefreshHomeCombo(CATBaseUnknown * iMechanism); 

   /** 
    * @nodoc
    */
   void                                SetTrackTCPTag( CATBaseUnknown_var spTag, CATBoolean iJumpToTag = FALSE );

   void                                SetCurrentTCPTag( CATBaseUnknown_var spTag);//The Method is used to update the compass location 
   void                                SetNotUpdateToolProfileCombo();  

   void                                UpdateHomeInPanel();
   void                                CreateHomeInPanel(CATBaseUnknown * iMechanism);
   void                                SelectAuxTabPage( const int index );
   void                                EnableAllTabPage();
   DNBXform3D                          GetRobotTCP();
   void                                GetResourceOnJog(CATBaseUnknown_var &ospMechanism, 
                                                         CATIProduct_var &ospFatherProd);
   void                                SetRobotTarget(DNBXform3D& iCompassLocation,  DNBKinStatus & oStatus);
   CATBoolean                          CheckVRCTurn(const int &iDOFIndex, const int &iTurn);
   const char *                        GetDefinePlaneNotifFromTCPGoFrame();
  
   void                                AddAnalyseNotification(DNBIgpTCPGoFrame *pTCPGoFrame);
   void                                UpdateRubberLine(const CATMathTransformation startPosition, const CATMathTransformation endPosition);
   void                                GetCartesianLinearAndAngularStepSize(double &oLinear, double &oAngular);
   void                                UpdateDeviceDOFTabPage();
void                                OnManipulatorPreactivate(CATCommand *from,CATNotification *notif,CATCommandClientData data);
   void                                RemoveTextFromISO(CATCommand *from,CATNotification *notif,CATCommandClientData data);
   //Specific for T Jog
   /** 
   * Set The Mode before Setting product or Tag
   */
   //Sets the product to translate using Jog
   void                                SetProductforTJog(CATBaseUnknown_var spProduct);
   //Sets the Tag to translate using Jog
   void                                SetTagforTJog(CATBaseUnknown_var spTag);
   //Set the T Jog Frame and Add CallBacks
   void                                SetTJogApplicativeFrame(DNBIgpMTJogFrame *pTJogFrame);

   CATBoolean	                        GetSelectProductIndicator();
	void	                              SetSelectProductIndicator(CATBoolean SelectProductIndicator);

private:
   void                                Init(CATBaseUnknown_var iMecanism, int iContext, const CATString & DlgName);
   void                                Build ();
   void                                DetermineAuxDeviceType( int deviceNo );
   void                                UpdateRobotJogPanel();
   struct cbData 
   {
      int                              deviceIndex;    // The DeviceIndex to determine the device
                                                       // on which the manipulator is.
                                                       // From 0 to _nbDevice-1
      int                              index;          // The DOF index in the device
      int                              directIndex;    // The equivalent index in the DirectDrive vector 
      DNBBasicJoint3D::DOFType         dofType;        // The DOF type (Linear, Angular)
      CAT3DManipulator                 *manip;         // The Visualization manipulator
      CATMathDirection                 lineAct;        // The line of action
   };


    // BaseCommand
    // ===========
   DNBBasicSimCmd                       *_BaseCommand;

    // Dialog
    // ======
   CATDlgFrame                         *_MoveFrame;
   CATDlgTabContainer                  *_TabPageContainer;
   CATDlgFrame                         *_ModeFrame;
  
   CATDlgCheckButton                   *_ImmediateButton;
   CATDlgCheckButton                   *_AutoUpdateButton;

   CATDlgFrame                         *_ApplicativeFrame;

   UserAction                          _LastUserAction;
   UserAction                          _LastUserActionOnBaseDevice;

   void                                OnImmediateButtonNotification
                                          (CATCommand * cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnCompassButtonNotification
                                          (CATCommand * cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnResetNotification
                                          (CATCommand * cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnApplyNotification
                                          (CATCommand * cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnCloseNotification
                                          (CATCommand * cmd, CATNotification* evt,
                                                   CATCommandClientData data);

   void                                OnAutomaticUpdateButtonNotification
                                          (CATCommand * cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnTabPageSelectModification(CATCommand* cmd,
                                               CATNotification* evt, 
                                               CATCommandClientData data);
void                                OnTcpStepSpinnerNotification(CATCommand* cmd,
                                               CATNotification* evt, 
                                               CATCommandClientData data);
   // =========================================================================
   // = JOINTS
   // =========================================================================

   int                                 _nbSysDOF;
   int                                 _nbDOF;
   int                                 _context;
   boolean                             _hasToBeUpdated;

   
   void                                ResetMountCombo();


   DNBIgpDOFTabPage                    **_DOFTabPage;

   void                                OnTabPageModifyNotification
                                          (CATCommand *, CATNotification* , 
                                                      CATCommandClientData data);
   
   // =========================================================================
   // = CARTESIAN
   // =========================================================================
   CATDlgTabPage                       * _CartesianTabPage;
   DNBIgpCartesianFrame                * _CartesianFrame;

   CATDlgFrame                         * _ConfigFrame;
   CATDlgMultiList                     * _ConfigList;

   void                                OnTcpModificationNotification
                                          (CATCommand *cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnConfigSelectionNotification
                                          (CATCommand *cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnCompassBeginMove
                                          (CATCallbackEvent, void *, CATNotification*,
                                                CATSubscriberData, CATCallback);
   void                                OnCompassMove
                                          (CATCallbackEvent, void *, CATNotification*,
                                                CATSubscriberData, CATCallback);
   void                                OnCompassEndMove
                                          (CATCallbackEvent, void *, CATNotification*,
                                                CATSubscriberData, CATCallback);
   void                                OnCompassEndDrag
                                          (CATCallbackEvent, void *, CATNotification*,
                                                CATSubscriberData, CATCallback);
   void                                OnCoorSysComboModificationNotification
                                          (CATCommand *cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   
   void                                OnToolComboModificationNotification
                                           (CATCommand *cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnUserPlaneDefineNotif(CATCommand* cmd,
                                                 CATNotification* evt, CATCommandClientData data);
   
   void                                OnReferenceComboSelectNotification
                                          (CATCommand *cmd, CATNotification* evt,
                                                   CATCommandClientData data);
   void                                OnUserReferenceDefineNotif(CATCommand* cmd,
                                                 CATNotification* evt, CATCommandClientData data);
   DNBXform3D                          CalculateTCPMatrix(DNBXform3D iTrans, ReferenceFrame iReferenceFrame, boolean iWRTWorld);

   DNBXform3D                          ConvertWorldToBase(DNBXform3D iTrans);
   DNBXform3D                          ConvertBaseToWorld(DNBXform3D iTrans);

   void                                ApplyFreezeMode(DNBXform3D &ioRobotTCP, DNBXform3D iSavedTCP);
   DNBXform3D                          GetRobotTarget();
   void                                SetRobotTarget( DNBXform3D& iCompassLocation, 
                                                       DNBRobConfiguration & iPose, DNBKinStatus & oStatus);

   //if displacement <> NULL, use this one to apply turn number, else read from robot as before
   void                                CalculNewTurnNumber( DNBMathVector * displacement = NULL );
   void                                OnDefinePlaneNotifFromTCPGoFrame(CATCommand* cmd,
                                               CATNotification* evt, 
                                               CATCommandClientData data);

   void FlushInWDM();
   void GetRobotConfiguration( DNBRobConfiguration *RobConf, 
                               DNBBasicRobot3D::Posture posture,
                               boolean UseTurn = TRUE );
   void                                OnControlComboNotification
                                          (CATCommand *, CATNotification* ,
                                                   CATCommandClientData data);
   void                                DNBDisplayTCPVal( DNBNotificationAgent::EventSet &event,
                                                DNBSimObserveRobot &data );

   // Applicative
   // ===========
   // Device
   boolean                             _IsTheDeviceARobot;
   DNBTrackTCPMode                     _trackTCPMode;  // defines the moves(Joint/Cartesian) for which TCP Tracking is enabled.
   DNBTCPFreezeMode                    _TCPFreezeMode; // defines the components of TCP to be freezed.
   DNBXform3D                          _savedTCP;  // saves the TCP value for trackTCP
   DNBBasicEntity::Handle              _spMechanism;  
   DNBBasicDevice3D::Vector            _devices_list;
   int                                 _MountDevCount;
   boolean                             _WorldAxes; // TRUE if the Axes system is the world origin
                                                   // FALSE if the Axes system is the robotbase origin
   boolean                             _Absolute;    //TRUE display Absolute value
                                                     //FALSE display relative value
   boolean                             _IsNCMachine;
   UpdateType                          _IsUpdateFrom;
   CATMathTransformation               _transInitSnapRef; // needed to set the compass position in CompassEndMove.
   CATI3DCompass_var                   _spCompass;
   CATCallback                         _CB_BeginMove;
   CATCallback                         _CB_Move;
   CATCallback                         _CB_EndMove;
   CATCallback                         _CB_EndDrag;
   // Joints manipulation
   DNBMathVector                       _InitialJointValue;
   double                              *_InitialTcpValue;
   DNBXform3D                           _InitialBaseLocation;
   cbData                              *_manipArray;
   int                                 _manipArraySize;
   int                                 *_manipArraySizeOfDevice;

   void                                OnJog ( CATCommand *, CATNotification *,
                                                         CATCommandClientData );
   void                                OnEndManipulate( CATCommand *, CATNotification *,
                                                        CATCommandClientData );
   void                                ComputeJointAxis();

   // Turn Number control
   CATDlgFrame                         *_TurnNumberFrame;
   CATDlgFrame                         *_TurnSignFrame;
   CATDlgFrame                         *_TurnFrame;
   double                              *_PrevJointValue;
   int                                 *_TurnNumber;
   CATDlgMultiList                     *_TurnNumberList;

   boolean                             *_TurnEnable;
   DNBBasicRobot3D::TurnMode           _TurnMode;
   DNBBasicDevice3D::DOFTypeDB         _DOFTypeVector;
   
   void                                OnMultiListCellModificationNotification (CATCommand *, 
                                          CATNotification*,CATCommandClientData data);

   int                                 _nbDevice;
   boolean                             IsJointsTabPageSelected(CATDlgTabPage *iTabPagePtr,
                                                                       int *oTabPageNo=NULL);
   
   void                                SaveDOFValues( boolean newTransac = TRUE);

   void                             OnUndo(CATCallbackEvent evt, void * from,
                                             CATNotification * notif,CATSubscriberData info,
                                                                     CATCallback callback );
   void                             OnRedo(CATCallbackEvent evt, void * from,
                                              CATNotification * notif,CATSubscriberData info,
                                                                     CATCallback callback );
   
   

   void                                UpdateDOF(CATRawColldouble  * pTempList);

   void                                DisplayError(const CATUnicodeString &iErrorMessage, 
                                                    CATDlgStyle iStyle = CATDlgNfyWarning|CATDlgNfyOK );

   void                                SetReferenceComboData();

   void                                ManangeAngularDOFWithinVRCTurnLimits();

   //CATDiaNoEngine                      *_pNoEngine;
   DNBIgpStack                         *_undoRedoList;
   static DNBIgpUndoRedoEvtPub         *_pStaticPub;
   static int                          _refCount;
   unsigned                            _UndoNo;
   CATCallback                         _callbackId1;
   CATCallback                         _callbackId2;
   boolean                             _controlLocation; // if TRUE the Location of TCP is not changed in Compass drag
   boolean                             _controlOrientation; // if TRUE the Orientation of TCP is not changed in Compass drag
   CATDlgFrame                         *_CompassFrame;
   CATDlgCheckButton                   *_ControlLocationCheckButton;
   CATDlgCheckButton                   *_ControlOrientationCheckButton;
   boolean                             _ActivateMove; 
   CATDlgFrame                         *_TCPFrame;
   CATDlgCombo                         *_CoorSysCombo;
   CATDlgLabel                         *_CoorSysLabel;
   CATDlgCombo                         *_ToolProfCombo;
   CATDlgLabel                         *_ToolProfLabel;
   CoorSys                              _CoorSystem;
   DNBXform3D                           _userFrame;
   DNBXform3D                           _SaveUserFrame;
   CATBaseUnknown_var                   _spDeviceOrRobot;
   DNBXform3D                           _toolToOther;
   DNBXform3D                           _XformBeforeDrag;
   
   CATBaseUnknown_var                   _spTrackTCPTag;
   CATBaseUnknown_var                   _spCurrentTCPTag;//NPO:This variable is used to update the Compass position on actvity /Tag select.
   int                                  _JogDlgID; 
   AuxDeviceType                        _AuxDeviceType;
   boolean                              _UpdateHomeCombo;
   boolean                              _UpdateToolProfCombo;
   DNBIgpRubberLine                    *_RubberLine;
   CATMathTransformation                _RubberLineStartXform;

   DNBXform3D                           _RuntimeCartValues;
   CATIProduct_var                      _spFatherProd;
  
   DNBXform3D                           _lastCompassLoc;
   DNBXform3D                           _savedNCMachineBase;  // saves the NCMachineBase location
   int                                  _simContext;
   CATListOfInt                         _vrc_min_turn_limits;
   CATListOfInt                         _vrc_max_turn_limits;
   CATBoolean                           _isVRCEnabled;
   CATBoolean                           _UseVRCTurnLimits;

   ReferenceFrame                       _pReferenceFrame;
   DNBXform3D                           _pReferenceUser;
   DNBBasicRobot3D::TCPType            _TcpType;
   //=====================
   //T Jog Specifics
   //-------------------
   //Params
   int                                   _TJogManipulationMode;
   CATBaseUnknown_var                     _spProdtoTranslate;
   DNBIgpMTJogFrame                     *_TJogFrame;
   
   int									_TjogUndoRedoIndex;
   CATBoolean                             _SelectProductIndicator;
   int                                    _startUndoNum;
   
   typedef scl_map<int, DNBXform3D> TJogWDMTagsXFormMap;
   typedef TJogWDMTagsXFormMap::iterator TJogWDMTagsXFormMapIter;
   typedef scl_map<CATBaseUnknown_var, TJogWDMTagsXFormMap> TJogWDMTagsMap;
   typedef TJogWDMTagsMap::iterator TJogWDMTagsMapIter;
   TJogWDMTagsMap _TJogWDMTagsMap;

	typedef scl_map<int, DNBXform3D> TJogWDMProductXFormMap;
	typedef TJogWDMProductXFormMap::iterator TJogWDMProductXFormMapIter;
	typedef scl_map<CATBaseUnknown_var, TJogWDMProductXFormMap> TJogWDMProductMap;
	typedef TJogWDMProductMap::iterator TJogWDMProductMapIter;
	TJogWDMProductMap _TJogWDMProductMap;

   typedef scl_map<int, CATMathAxis> TJogCompassPositionMap;
   typedef TJogCompassPositionMap::iterator TJogCompassPositionMapIter;
   
   TJogCompassPositionMap _TJogCompassPositionMap;

   //Enable/Disable TCP/Target Tracking
   void                                  SetTrackingParameters(CATMathAxis &oCompassLocation);
   //Call Backs
   void OnSwitchTJogMode(CATCommand* fromClient, CATNotification*  Notif,CATCommandClientData  data);
   void OnSelectTagforTJog(CATCommand* fromClient, CATNotification*  Notif,CATCommandClientData  data);
   void ResetTag(int index = 0);
   void ResetProduct(int index = 0);

};

//----------------------------------------------------------------------

#endif

