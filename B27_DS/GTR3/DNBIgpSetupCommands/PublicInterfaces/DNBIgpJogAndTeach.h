// COPYRIGHT DASSAULT SYSTEMES 2006

/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

#ifdef _WINDOWS_SOURCE
#ifdef __DNBIgpJogAndTeach
#define ExportedByDNBIgpJogAndTeach  __declspec(dllexport)
#else
#define ExportedByDNBIgpJogAndTeach  __declspec(dllimport)
#endif
#else
#define ExportedByDNBIgpJogAndTeach
#endif
