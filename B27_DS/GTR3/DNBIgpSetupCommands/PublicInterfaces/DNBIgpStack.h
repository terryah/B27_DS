#ifndef DNBIgpStack_H
#define DNBIgpStack_H

// COPYRIGHT DASSAULT SYSTEMES 2006

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @collection DNBIgpStack
 * Collection class for pointers to CATRawColldouble.
 * Only the following methods of pointer collection classes are available:
 * <ul>
 * <li><tt>Append</tt></li>
 * <li><tt>Size</tt></li>
 * <li><tt>Operator[]</tt></li>
 * <li><tt>RemovePosition</tt></li>
 * <li><tt>RemoveAll</tt></li>
 * <li><tt>InsertAt</tt></li>
 * </ul>
 * Refer to the articles dealing with collections in the encyclopedia.
 */


// clean previous functions requests
#include  <CATLISTP_Clean.h>
#include "DNBIgpJogAndTeach.h"

// require needed functions
#define CATLISTP_Append
#define CATLISTP_RemovePosition
#define CATLISTP_RemoveAll
#define CATLISTP_InsertAt

// get macros
#include  <CATLISTP_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef  CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy    ExportedByDNBIgpJogAndTeach



/**
 * @nodoc
 */
CATLISTP_DECLARE(CATRawColldouble)
typedef CATLISTP(CATRawColldouble) DNBIgpStack ;
#endif

