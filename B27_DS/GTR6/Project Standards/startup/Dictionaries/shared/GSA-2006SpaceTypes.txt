1) ANSI Category 01: Office
ADP - Automatic Data Processing
ADP
ANT - Antenna
ANT
AUD - Auditorium
AUD
CFT - Conference Training
CFT
CLD - Childcare
CLD
CRJ - Courtroom/Judiciary
CRJ
FDS - Food Service
FDS
FIT - Fitness Center
FIT
HUT - Health Unit
HUT
INS - Light Industrial
INS
JCC - Judicial Chambers
JCC
JHR � Judicial Hearing Room
JHR
LAB � Laboratory
LAB
PTL � Private Toilet
PTL
QRR � Quarters/Residence
QRR
STC � Structurally Changed
STC
TFC � Tenant Floor Cut
TFC
TTO � Total Office
TTO
WRH � Warehouse
WRH
2) ANSI Categories 02 & 03: Building and Floor Common
CRH � Circulation Horizontal
CRH
CST � Custodial
CST
MCH � Mechanical
MCH
TLT � Public Toilet
TLT
3) ANSI Category 04: Vertical Penetrations
CRV � Circulation Vertical
CRV
4) Category 05: PBS Specific
CON � Construction
CON
LND � Land
LND
STP � Structured Parking
STP
UFO � Unsuitable for Occupancy
UFO