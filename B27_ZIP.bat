@echo off
@title "AUTO-ZIP"
set MyPath=%~dp0
cd /d %~dp0
@echo delete 7z

if exist %MyPath%\B27_DS.7z del %MyPath%\B27_DS.7z
if exist %MyPath%\B27_GT.7z del %MyPath%\B27_GT.7z

"C:\Program Files\7-Zip\7z.exe" a ".\B27_DS.7z"  ".\B27_DS"
"C:\Program Files\7-Zip\7z.exe" a ".\B27_GT.7z"  ".\B27_GT"

@echo all done
timeout /t 5